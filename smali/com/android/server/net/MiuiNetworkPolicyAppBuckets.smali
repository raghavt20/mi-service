.class public Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyAppBuckets.java"


# static fields
.field private static final ACTION_CLOUD_TELE_FEATURE_INFO_CHANGED:Ljava/lang/String; = "com.android.phone.intent.action.CLOUD_TELE_FEATURE_INFO_CHANGED"

.field private static final ACTION_GESTURE_WHITE_APP_SCENE:Ljava/lang/String; = "com.android.phone.intent.action.GESTURE_WHITE_APP_SCENE"

.field private static final ACTION_SPEED_WHITE_LIST:Ljava/lang/String; = "com.android.phone.intent.action.SPEED_WHITE_LIST"

.field private static final ACTION_THERMAL_SPECIAL_APP_SCENE:Ljava/lang/String; = "com.android.phone.intent.action.THERMAL_SPECIAL_APP_SCENE"

.field private static final ACTION_THROTTLE_SPECIAL_APP_SCENE:Ljava/lang/String; = "com.android.phone.intent.action.THROTTLE_SPECIAL_APP_SCENE"

.field private static final ACTION_THROTTLE_WHITE_APP_SCENE:Ljava/lang/String; = "com.android.phone.intent.action.THROTTLE_WHITE_APP_SCENE"

.field private static final CLOUD_CATEGORY_THERMAL_THORTTLE:Ljava/lang/String; = "TelephonyThermalThrottle"

.field private static final CLOUD_KEY_THROTTLE_WHITE_APP:Ljava/lang/String; = "Params2"

.field private static final CONNECTION_EX:Ljava/lang/String; = "enableConnectionExtension"

.field private static final CON_DISABLED:I = 0x0

.field private static final CON_ENABLED:I = 0x1

.field private static final DEBUG:Z = true

.field private static final GESTURE_WHITE_APP_EXTRA:Ljava/lang/String; = "gestureWhiteAppExtra"

.field private static final LATENCY_ACTION_CHANGE_LEVEL:Ljava/lang/String; = "com.android.phone.intent.action.CHANGE_LEVEL"

.field private static final LOCAL_GESTURE_WHITELIST_APP_LIST:[Ljava/lang/String;

.field private static final LOCAL_HONGBAO_APP_LIST:[Ljava/lang/String;

.field private static LOCAL_THROTTLE_WHITE_APP_LIST:[Ljava/lang/String; = null

.field private static final LOCAL_TPUT_TOOL_APP_LIST:[Ljava/lang/String;

.field private static final NOTIFACATION_RECEIVER_PACKAGE:Ljava/lang/String; = "com.android.phone"

.field private static final OPTIMIZATION_ENABLED:Ljava/lang/String; = "optimizationEnabled"

.field private static final TAG:Ljava/lang/String; = "MiuiNetworkPolicyAppBuckets"

.field private static final THERMAL_SPECIAL_APP_EXTRA:Ljava/lang/String; = "thermalSpecialAppExtra"

.field private static final THROTTLE_SPECIAL_APP_EXTRA:Ljava/lang/String; = "throttleSpecialAppExtra"

.field private static final THROTTLE_WHITE_APP_EXTRA:Ljava/lang/String; = "throttleWhiteAppExtra"

.field private static final TPUT_TEST_APP_OPTIMIZATION:Ljava/lang/String; = "com.android.phone.intent.action.TPUT_OPTIMIZATION"

.field private static final WHITE_LIST_PACKAGE_NAME:Ljava/lang/String; = "whiteListPackageName"

.field private static final WHITE_LIST_STATE_TOP:Ljava/lang/String; = "whiteListStateTop"


# instance fields
.field private LOCAL_THERMAL_SPECIAL_APP_LIST:[Ljava/lang/String;

.field private LOCAL_THROTTLE_SPECIAL_APP_LIST:[Ljava/lang/String;

.field private mAppUid:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mAppsPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCm:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mIsGestureWhiteAppOn:Z

.field private mIsHongbaoAppOn:Z

.field private mIsMobileNwOn:Z

.field private mIsMobileTcEnabledListOn:Z

.field private mIsSpeedWhiteListOn:Z

.field private mIsThermalSpecialAppOn:Z

.field private mIsThrottleSpecialAppOn:Z

.field private mIsTputTestAppOn:Z

.field private mIsWhiteAppOn:Z

.field private mLastHongbaoApp:Z

.field private mLastMobileNw:Z

.field private mLastMobileTcEnabledListOn:Z

.field private mLastSpeedWhiteList:Z

.field private mLastThermalSpecialApp:Z

.field private mLastThrottleSpecialApp:Z

.field private mLastThrottleWhiteAppList:Ljava/lang/String;

.field private mLastTputTestApp:Z

.field private mLastWhiteApp:Z

.field private mMobileTcEnabledList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mMobileTcEnabledListObserver:Landroid/database/ContentObserver;

.field private mMobileTcEnabledPkgName:Ljava/lang/String;

.field final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSpeedWhiteList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSpeedWhiteListObserver:Landroid/database/ContentObserver;

.field private mUidMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWhiteListPkgName:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmIsMobileNwOn(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastMobileNw(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmIsMobileNwOn(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastMobileNw(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetMobileLinkIface(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getMobileLinkIface()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdate5gPowerWhiteApplist(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->update5gPowerWhiteApplist(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAppList(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateHongbaoModeStatus(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateHongbaoModeStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateMobileTcEnabledListStatus(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateMobileTcEnabledListStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSpeedWhiteListStatus(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateSpeedWhiteListStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateTputTestAppStatus(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateTputTestAppStatus()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 9

    .line 66
    const-string v0, "com.tencent.mm"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_HONGBAO_APP_LIST:[Ljava/lang/String;

    .line 71
    const-string v1, "org.zwanoo.android.speedtest"

    const-string v2, "org.zwanoo.android.speedtest.china"

    const-string v3, "cn.nokia.speedtest5g"

    const-string v4, "org.zwanoo.android.speedtest.gworld"

    const-string v5, "cn.speedtest.lite"

    const-string v6, "cn.lezhi.speedtest"

    filled-new-array/range {v1 .. v6}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_TPUT_TOOL_APP_LIST:[Ljava/lang/String;

    .line 81
    const-string v1, "com.smile.gifmaker"

    const-string v2, "com.tencent.mm"

    const-string v3, "com.ss.android.ugc.aweme"

    const-string v4, "com.taobao.taobao"

    const-string v5, "com.ss.android.article.news"

    const-string/jumbo v6, "tv.danmaku.bili"

    const-string v7, "com.duowan.kiwi"

    const-string v8, "air.tv.douyu.android"

    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_GESTURE_WHITELIST_APP_LIST:[Ljava/lang/String;

    .line 102
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THROTTLE_WHITE_APP_LIST:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z

    .line 52
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z

    .line 53
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsHongbaoAppOn:Z

    .line 54
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastHongbaoApp:Z

    .line 55
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsTputTestAppOn:Z

    .line 56
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastTputTestApp:Z

    .line 57
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsSpeedWhiteListOn:Z

    .line 58
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastSpeedWhiteList:Z

    .line 59
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileTcEnabledListOn:Z

    .line 60
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileTcEnabledListOn:Z

    .line 63
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 98
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsWhiteAppOn:Z

    .line 99
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastWhiteApp:Z

    .line 100
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsGestureWhiteAppOn:Z

    .line 108
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleWhiteAppList:Ljava/lang/String;

    .line 111
    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THERMAL_SPECIAL_APP_LIST:[Ljava/lang/String;

    .line 113
    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THROTTLE_SPECIAL_APP_LIST:[Ljava/lang/String;

    .line 121
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThermalSpecialAppOn:Z

    .line 122
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThermalSpecialApp:Z

    .line 123
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThrottleSpecialAppOn:Z

    .line 124
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleSpecialApp:Z

    .line 142
    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mWhiteListPkgName:Ljava/lang/String;

    .line 143
    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledPkgName:Ljava/lang/String;

    .line 169
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$1;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mSpeedWhiteListObserver:Landroid/database/ContentObserver;

    .line 183
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;

    invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;-><init>(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 595
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$3;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$3;-><init>(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledListObserver:Landroid/database/ContentObserver;

    .line 146
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    .line 147
    iput-object p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mHandler:Landroid/os/Handler;

    .line 148
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppUid:Ljava/util/Set;

    .line 149
    return-void
.end method

.method private addUidToMap(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 326
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    :cond_0
    return-void
.end method

.method private appBucketsForUidStateChanged(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "appBucketsForUidStateChanged uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 376
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isAppBucketsEnabledForUid(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processHongbaoAppIfNeed(IZ)V

    .line 378
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processTputTestAppIfNeed(IZ)V

    .line 379
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processSpeedWhiteListIfNeed(IZ)V

    .line 380
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processMobileTcEnabledListIfNeed(IZ)V

    .line 381
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThrottleWhiteAppIfNeed(IZ)V

    .line 382
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThermalSpecialAppIfNeed(IZ)V

    .line 383
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThrottleSpecialAppIfNeed(IZ)V

    .line 384
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processGestureWhiteAppIfNeed(IZ)V

    goto :goto_0

    .line 386
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processHongbaoAppIfNeed(IZ)V

    .line 387
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processTputTestAppIfNeed(IZ)V

    .line 388
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processSpeedWhiteListIfNeed(IZ)V

    .line 389
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processMobileTcEnabledListIfNeed(IZ)V

    .line 390
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThrottleWhiteAppIfNeed(IZ)V

    .line 391
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThermalSpecialAppIfNeed(IZ)V

    .line 392
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThrottleSpecialAppIfNeed(IZ)V

    .line 393
    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processGestureWhiteAppIfNeed(IZ)V

    .line 395
    :goto_0
    return-void
.end method

.method private enableHongbaoMode(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableHongbaoMode enable"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 464
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 465
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.phone.intent.action.CHANGE_LEVEL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 466
    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    const-string v1, "enableConnectionExtension"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 468
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 469
    return-void
.end method

.method private gestureAppNotification(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "extra"    # Ljava/lang/String;
    .param p3, "enable"    # Z

    .line 732
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gestureAppNotification action = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enable = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 733
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 734
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 735
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 736
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 737
    return-void
.end method

.method private getAllAppsPN()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 229
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 231
    .local v0, "appList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isHongbaoModeAllowed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 232
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_HONGBAO_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 233
    aget-object v2, v2, v1

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 232
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 237
    .end local v1    # "i":I
    :cond_0
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isCommonSceneRecognitionAllowed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 238
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_TPUT_TOOL_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 239
    aget-object v2, v2, v1

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 238
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 242
    .end local v1    # "i":I
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->fetchSpeedAppWhiteList()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mSpeedWhiteList:Ljava/util/Set;

    .line 243
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 244
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mSpeedWhiteList:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 248
    :cond_2
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    .line 249
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x11030067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 250
    .local v1, "deviceArray":[Ljava/lang/String;
    const-string v2, "ro.product.name"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 251
    .local v2, "product":Ljava/lang/String;
    const/4 v3, 0x0

    .line 252
    .local v3, "isDeviceSupportWhitelist":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v5, v1

    if-ge v4, v5, :cond_4

    .line 253
    aget-object v5, v1, v4

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 254
    const/4 v3, 0x1

    .line 255
    goto :goto_3

    .line 252
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 258
    .end local v4    # "i":I
    :cond_4
    :goto_3
    if-eqz v3, :cond_5

    .line 259
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_4
    sget-object v5, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_GESTURE_WHITELIST_APP_LIST:[Ljava/lang/String;

    array-length v6, v5

    if-ge v4, v6, :cond_5

    .line 260
    aget-object v5, v5, v4

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 259
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 264
    .end local v4    # "i":I
    :cond_5
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileTcFeatureAllowed()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 266
    invoke-virtual {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->fetchMobileTcEnabledList()Ljava/util/Set;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledList:Ljava/util/Set;

    .line 267
    if-eqz v4, :cond_6

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 268
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledList:Ljava/util/Set;

    invoke-interface {v0, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 272
    :cond_6
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_5
    sget-object v5, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THROTTLE_WHITE_APP_LIST:[Ljava/lang/String;

    array-length v6, v5

    if-ge v4, v6, :cond_7

    .line 273
    aget-object v5, v5, v4

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 272
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 276
    .end local v4    # "i":I
    :cond_7
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_6
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THERMAL_SPECIAL_APP_LIST:[Ljava/lang/String;

    array-length v6, v5

    if-ge v4, v6, :cond_8

    .line 277
    aget-object v5, v5, v4

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 276
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 279
    .end local v4    # "i":I
    :cond_8
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_7
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THROTTLE_SPECIAL_APP_LIST:[Ljava/lang/String;

    array-length v6, v5

    if-ge v4, v6, :cond_9

    .line 280
    aget-object v5, v5, v4

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 279
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 283
    .end local v4    # "i":I
    :cond_9
    return-object v0
.end method

.method private getAppListFromCloud(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "Category"    # Ljava/lang/String;
    .param p2, "Key"    # Ljava/lang/String;

    .line 718
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/telephony/TelephonyManagerEx;->getFeatureInfoIntentByCloud(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 719
    .local v0, "featureInfoIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method private getMobileLinkIface()Ljava/lang/String;
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mCm:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mCm:Landroid/net/ConnectivityManager;

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mCm:Landroid/net/ConnectivityManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    move-result-object v0

    .line 216
    .local v0, "prop":Landroid/net/LinkProperties;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 219
    :cond_1
    invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 217
    :cond_2
    :goto_0
    const-string v1, ""

    return-object v1
.end method

.method private getUidFromMap(Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 332
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 333
    const/4 v0, -0x1

    return v0

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private hasUidFromGestureWhiteAppMap(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 621
    const/4 v0, 0x0

    .line 622
    .local v0, "rst":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_GESTURE_WHITELIST_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 623
    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 624
    const/4 v0, 0x1

    .line 625
    goto :goto_1

    .line 622
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 628
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return v0
.end method

.method private hasUidFromHongbaoMap(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 407
    const/4 v0, 0x0

    .line 408
    .local v0, "rst":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_HONGBAO_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 409
    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 410
    const/4 v0, 0x1

    .line 408
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 413
    .end local v1    # "i":I
    :cond_1
    return v0
.end method

.method private hasUidFromMobileTcEnabledListMap(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 564
    const/4 v0, 0x0

    .line 565
    .local v0, "rst":Z
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledList:Ljava/util/Set;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 568
    :cond_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledList:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 569
    .local v2, "packageName":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 570
    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledPkgName:Ljava/lang/String;

    .line 571
    const/4 v0, 0x1

    .line 572
    goto :goto_1

    .line 574
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 575
    :cond_2
    :goto_1
    return v0

    .line 566
    :cond_3
    :goto_2
    const/4 v1, 0x0

    return v1
.end method

.method private hasUidFromSpeedWhiteListMap(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 428
    const/4 v0, 0x0

    .line 429
    .local v0, "rst":Z
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mSpeedWhiteList:Ljava/util/Set;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 432
    :cond_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mSpeedWhiteList:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 433
    .local v2, "packageName":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 434
    iput-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mWhiteListPkgName:Ljava/lang/String;

    .line 435
    const/4 v0, 0x1

    .line 436
    goto :goto_1

    .line 438
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 439
    :cond_2
    :goto_1
    return v0

    .line 430
    :cond_3
    :goto_2
    const/4 v1, 0x0

    return v1
.end method

.method private hasUidFromThermalSpecialAppMap(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 654
    const/4 v0, 0x0

    .line 655
    .local v0, "rst":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THERMAL_SPECIAL_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 656
    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 657
    const/4 v0, 0x1

    .line 658
    goto :goto_1

    .line 655
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 661
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return v0
.end method

.method private hasUidFromThrottleSpecialAppMap(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 676
    const/4 v0, 0x0

    .line 677
    .local v0, "rst":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THROTTLE_SPECIAL_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 678
    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 679
    const/4 v0, 0x1

    .line 680
    goto :goto_1

    .line 677
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 683
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return v0
.end method

.method private hasUidFromThrottleWhiteAppMap(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 632
    const/4 v0, 0x0

    .line 633
    .local v0, "rst":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THROTTLE_WHITE_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 634
    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 635
    const/4 v0, 0x1

    .line 636
    goto :goto_1

    .line 633
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 639
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return v0
.end method

.method private hasUidFromTputTestMap(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 417
    const/4 v0, 0x0

    .line 418
    .local v0, "rst":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_TPUT_TOOL_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 419
    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 420
    const/4 v0, 0x1

    .line 421
    goto :goto_1

    .line 418
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 424
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return v0
.end method

.method private initReceiver()V
    .locals 4

    .line 177
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 178
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 179
    const-string v1, "com.android.phone.intent.action.CLOUD_TELE_FEATURE_INFO_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 180
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 181
    return-void
.end method

.method private isAppBucketsEnabledForUid(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 371
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppUid:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isCommonSceneRecognitionAllowed()Z
    .locals 2

    .line 527
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    .line 529
    const-string v1, "crux"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    .line 530
    const-string v1, "andromeda"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 527
    :goto_0
    return v0
.end method

.method private static isHongbaoModeAllowed()Z
    .locals 1

    .line 535
    const/4 v0, 0x0

    return v0
.end method

.method private static isUidValidForQos(I)Z
    .locals 1
    .param p0, "uid"    # I

    .line 225
    invoke-static {p0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    return v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .line 740
    const-string v0, "MiuiNetworkPolicyAppBuckets"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    return-void
.end method

.method private mobileTcEnabledStatusChanged(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mobileTcEnabledStatusChanged enable"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mMobileTcEnabledPkgName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledPkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 549
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mHandler:Landroid/os/Handler;

    .line 551
    nop

    .line 549
    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 552
    return-void
.end method

.method private processGestureWhiteAppIfNeed(IZ)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "enabled"    # Z

    .line 612
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromGestureWhiteAppMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processGestureWhiteAppIfNeed enabled = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 614
    const-string v0, "gestureWhiteAppExtra"

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsGestureWhiteAppOn:Z

    const-string v2, "com.android.phone.intent.action.GESTURE_WHITE_APP_SCENE"

    invoke-direct {p0, v2, v0, p2, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateGestureWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 616
    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsGestureWhiteAppOn:Z

    .line 618
    :cond_0
    return-void
.end method

.method private processHongbaoAppIfNeed(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "enabled"    # Z

    .line 454
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromHongbaoMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processHongbaoAppIfNeed Hongbao"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 456
    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsHongbaoAppOn:Z

    .line 457
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateHongbaoModeStatus()V

    .line 458
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsHongbaoAppOn:Z

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastHongbaoApp:Z

    .line 460
    :cond_0
    return-void
.end method

.method private processMobileTcEnabledListIfNeed(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "enabled"    # Z

    .line 555
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromMobileTcEnabledListMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processMobileTcEnabledListIfNeed enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 557
    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileTcEnabledListOn:Z

    .line 558
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateMobileTcEnabledListStatus()V

    .line 559
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileTcEnabledListOn:Z

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileTcEnabledListOn:Z

    .line 561
    :cond_0
    return-void
.end method

.method private processSpeedWhiteListIfNeed(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "enabled"    # Z

    .line 499
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromSpeedWhiteListMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processSpeedWhiteListIfNeed enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 501
    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsSpeedWhiteListOn:Z

    .line 502
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateSpeedWhiteListStatus()V

    .line 503
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsSpeedWhiteListOn:Z

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastSpeedWhiteList:Z

    .line 505
    :cond_0
    return-void
.end method

.method private processThermalSpecialAppIfNeed(IZ)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "enabled"    # Z

    .line 643
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromThermalSpecialAppMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processThermalSpecialAppIfNeed enabled = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 645
    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThermalSpecialAppOn:Z

    .line 646
    const-string v2, "com.android.phone.intent.action.THERMAL_SPECIAL_APP_SCENE"

    const-string v3, "com.android.phone"

    const-string/jumbo v4, "thermalSpecialAppExtra"

    iget-boolean v6, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThermalSpecialApp:Z

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->update5gPowerWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 649
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThermalSpecialAppOn:Z

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThermalSpecialApp:Z

    .line 651
    :cond_0
    return-void
.end method

.method private processThrottleSpecialAppIfNeed(IZ)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "enabled"    # Z

    .line 665
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromThrottleSpecialAppMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processThrottleSpecialAppIfNeed enabled = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 667
    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThrottleSpecialAppOn:Z

    .line 668
    const-string v2, "com.android.phone.intent.action.THROTTLE_SPECIAL_APP_SCENE"

    const-string v3, "com.android.phone"

    const-string/jumbo v4, "throttleSpecialAppExtra"

    iget-boolean v6, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleSpecialApp:Z

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->update5gPowerWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 671
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThrottleSpecialAppOn:Z

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleSpecialApp:Z

    .line 673
    :cond_0
    return-void
.end method

.method private processThrottleWhiteAppIfNeed(IZ)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "enabled"    # Z

    .line 603
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromThrottleWhiteAppMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processThrottleWhiteAppIfNeed enabled = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 605
    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsWhiteAppOn:Z

    .line 606
    const-string v2, "com.android.phone.intent.action.THROTTLE_WHITE_APP_SCENE"

    const-string v3, "com.android.phone"

    const-string/jumbo v4, "throttleWhiteAppExtra"

    iget-boolean v6, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastWhiteApp:Z

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->update5gPowerWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 607
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsWhiteAppOn:Z

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastWhiteApp:Z

    .line 609
    :cond_0
    return-void
.end method

.method private processTputTestAppIfNeed(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "enabled"    # Z

    .line 472
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromTputTestMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "processTputTestAppIfNeed TputTest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 474
    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsTputTestAppOn:Z

    .line 475
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateTputTestAppStatus()V

    .line 476
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsTputTestAppOn:Z

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastTputTestApp:Z

    .line 478
    :cond_0
    return-void
.end method

.method private registerMobileTcEnabledList()V
    .locals 4

    .line 590
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 591
    const-string v1, "mobile_tc_enabled_list_pkg_name"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mMobileTcEnabledListObserver:Landroid/database/ContentObserver;

    .line 590
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 593
    return-void
.end method

.method private registerSpeedWhiteList()V
    .locals 4

    .line 164
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 165
    const-string v1, "fiveg_speed_white_list_pkg_name"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mSpeedWhiteListObserver:Landroid/database/ContentObserver;

    .line 164
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 167
    return-void
.end method

.method private removeUidFromMap(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 339
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    return-void
.end method

.method private speedWhiteListAppNotification(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 517
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "speedWhiteListAppNotification enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; mWhiteListPkgName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mWhiteListPkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 518
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 519
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.phone.intent.action.SPEED_WHITE_LIST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 520
    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 521
    const-string/jumbo v1, "whiteListStateTop"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 522
    const-string/jumbo v1, "whiteListPackageName"

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mWhiteListPkgName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 523
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 524
    return-void
.end method

.method private tputTestAppNotification(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 490
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "tputTestAppNotification enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 491
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 492
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.phone.intent.action.TPUT_OPTIMIZATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 493
    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    const-string v1, "optimizationEnabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 495
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 496
    return-void
.end method

.method private uidRemoveAll()V
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 344
    return-void
.end method

.method private declared-synchronized update5gPowerWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "newStatusOn"    # Z
    .param p5, "oldStatusOn"    # Z

    monitor-enter p0

    .line 703
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "update5gPowerWhiteAppStatus newStatusOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",oldStatusOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 704
    if-eq p4, p5, :cond_0

    .line 705
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->whiteAppNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 707
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
    :cond_0
    monitor-exit p0

    return-void

    .line 702
    .end local p1    # "action":Ljava/lang/String;
    .end local p2    # "pkg":Ljava/lang/String;
    .end local p3    # "extra":Ljava/lang/String;
    .end local p4    # "newStatusOn":Z
    .end local p5    # "oldStatusOn":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private update5gPowerWhiteApplist(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "Category"    # Ljava/lang/String;
    .param p2, "Key"    # Ljava/lang/String;

    .line 687
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "update5gPowerWhiteApplist Category = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 688
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getAppListFromCloud(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 689
    .local v0, "cloudAppList":Ljava/lang/String;
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleWhiteAppList:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 691
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 692
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update5gPowerWhiteApplist newWhitelist = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 693
    const-string v1, "TelephonyThermalThrottle"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 694
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THROTTLE_WHITE_APP_LIST:[Ljava/lang/String;

    .line 695
    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleWhiteAppList:Ljava/lang/String;

    .line 697
    :cond_1
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppList()V

    .line 699
    :cond_2
    return-void
.end method

.method private updateAppList()V
    .locals 10

    .line 298
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 299
    .local v0, "um":Landroid/os/UserManager;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 300
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v2

    .line 301
    .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getAllAppsPN()Ljava/util/Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppsPN:Ljava/util/Set;

    .line 302
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->uidRemoveAll()V

    .line 303
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppsPN:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 304
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/UserInfo;

    .line 305
    .local v4, "user":Landroid/content/pm/UserInfo;
    const/4 v5, 0x0

    iget v6, v4, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v1, v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;

    move-result-object v5

    .line 306
    .local v5, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageInfo;

    .line 307
    .local v7, "app":Landroid/content/pm/PackageInfo;
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppsPN:Ljava/util/Set;

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 308
    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 309
    iget v8, v4, Landroid/content/pm/UserInfo;->id:I

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v8, v9}, Landroid/os/UserHandle;->getUid(II)I

    move-result v8

    .line 310
    .local v8, "uid":I
    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v9, v8}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->addUidToMap(Ljava/lang/String;I)V

    .line 312
    .end local v7    # "app":Landroid/content/pm/PackageInfo;
    .end local v8    # "uid":I
    :cond_0
    goto :goto_1

    .line 313
    .end local v4    # "user":Landroid/content/pm/UserInfo;
    .end local v5    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_1
    goto :goto_0

    .line 314
    :cond_2
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateUidFromWholeAppMap()V

    .line 316
    :cond_3
    return-void
.end method

.method private declared-synchronized updateGestureWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "extra"    # Ljava/lang/String;
    .param p3, "newStatusOn"    # Z
    .param p4, "oldStatusOn"    # Z

    monitor-enter p0

    .line 711
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateGestureWhiteAppStatus newStatusOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",oldStatusOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 712
    if-eq p3, p4, :cond_0

    .line 713
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->gestureAppNotification(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 715
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
    :cond_0
    monitor-exit p0

    return-void

    .line 710
    .end local p1    # "action":Ljava/lang/String;
    .end local p2    # "extra":Ljava/lang/String;
    .end local p3    # "newStatusOn":Z
    .end local p4    # "oldStatusOn":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized updateHongbaoModeStatus()V
    .locals 4

    monitor-enter p0

    .line 398
    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsHongbaoAppOn:Z

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
    :cond_0
    move v0, v2

    .line 399
    .local v0, "isNewStatusOn":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastHongbaoApp:Z

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 400
    .local v1, "isOldStatusOn":Z
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateHongbaoModeStatus isNewStatusOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",isOldStatusOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 401
    if-eq v0, v1, :cond_2

    .line 402
    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->enableHongbaoMode(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    :cond_2
    monitor-exit p0

    return-void

    .line 397
    .end local v0    # "isNewStatusOn":Z
    .end local v1    # "isOldStatusOn":Z
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized updateMobileTcEnabledListStatus()V
    .locals 4

    monitor-enter p0

    .line 539
    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileTcEnabledListOn:Z

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
    :cond_0
    move v0, v2

    .line 540
    .local v0, "isNewStatusOn":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileTcEnabledListOn:Z

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 541
    .local v1, "isOldStatusOn":Z
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateMobileTcEnabledListStatus isNewStatusOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",isOldStatusOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 542
    if-eq v0, v1, :cond_2

    .line 543
    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mobileTcEnabledStatusChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    :cond_2
    monitor-exit p0

    return-void

    .line 538
    .end local v0    # "isNewStatusOn":Z
    .end local v1    # "isOldStatusOn":Z
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateSpecialAppList()V
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    .line 320
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110300d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THERMAL_SPECIAL_APP_LIST:[Ljava/lang/String;

    .line 321
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    .line 322
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110300d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->LOCAL_THROTTLE_SPECIAL_APP_LIST:[Ljava/lang/String;

    .line 323
    return-void
.end method

.method private declared-synchronized updateSpeedWhiteListStatus()V
    .locals 4

    monitor-enter p0

    .line 508
    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsSpeedWhiteListOn:Z

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
    :cond_0
    move v0, v2

    .line 509
    .local v0, "isNewStatusOn":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastSpeedWhiteList:Z

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 510
    .local v1, "isOldStatusOn":Z
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateSpeedWhiteListStatus isNewStatusOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",isOldStatusOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 511
    if-eq v0, v1, :cond_2

    .line 512
    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->speedWhiteListAppNotification(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    :cond_2
    monitor-exit p0

    return-void

    .line 507
    .end local v0    # "isNewStatusOn":Z
    .end local v1    # "isOldStatusOn":Z
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized updateTputTestAppStatus()V
    .locals 4

    monitor-enter p0

    .line 481
    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsTputTestAppOn:Z

    .line 482
    .local v0, "isNewStatusOn":Z
    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastTputTestApp:Z

    .line 483
    .local v1, "isOldStatusOn":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateTputTestAppStatus isNewStatusOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",isOldStatusOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 484
    if-eq v0, v1, :cond_0

    .line 485
    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->tputTestAppNotification(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 487
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
    :cond_0
    monitor-exit p0

    return-void

    .line 480
    .end local v0    # "isNewStatusOn":Z
    .end local v1    # "isOldStatusOn":Z
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateUidFromWholeAppMap()V
    .locals 5

    .line 443
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppUid:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 444
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppsPN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppsPN:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 446
    .local v1, "pn":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I

    move-result v2

    .line 447
    .local v2, "uid":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 448
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppUid:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 450
    .end local v1    # "pn":Ljava/lang/String;
    .end local v2    # "uid":I
    :cond_1
    goto :goto_0

    .line 451
    :cond_2
    return-void
.end method

.method private whiteAppNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "enable"    # Z

    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "whiteAppNotification action = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enable = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 724
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 725
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 726
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 727
    invoke-virtual {v0, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 728
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 729
    return-void
.end method


# virtual methods
.method public fetchMobileTcEnabledList()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 579
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mobile_tc_enabled_list_pkg_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 581
    .local v0, "pkgNames":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fetchMobileTcEnabledList  pkgNames="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 582
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 583
    new-instance v1, Ljava/util/HashSet;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 584
    .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    return-object v1

    .line 586
    .end local v1    # "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    return-object v1
.end method

.method public fetchSpeedAppWhiteList()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 287
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "fiveg_speed_white_list_pkg_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    .local v0, "pkgNames":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fetchSpeedAppWhiteList  pkgNames="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 290
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 291
    new-instance v1, Ljava/util/HashSet;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 292
    .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    return-object v1

    .line 294
    .end local v1    # "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    return-object v1
.end method

.method public systemReady()V
    .locals 1

    .line 152
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateSpecialAppList()V

    .line 153
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppList()V

    .line 154
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->initReceiver()V

    .line 155
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isCommonSceneRecognitionAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->registerSpeedWhiteList()V

    .line 158
    :cond_0
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileTcFeatureAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->registerMobileTcEnabledList()V

    .line 161
    :cond_1
    return-void
.end method

.method public updateAppBucketsForUidStateChange(III)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "oldUidState"    # I
    .param p3, "newUidState"    # I

    .line 361
    invoke-static {p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isUidValidForQos(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 362
    return-void

    .line 364
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isAppBucketsEnabledForUid(II)Z

    move-result v0

    .line 365
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isAppBucketsEnabledForUid(II)Z

    move-result v1

    if-eq v0, v1, :cond_1

    .line 366
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->appBucketsForUidStateChanged(II)V

    .line 368
    :cond_1
    return-void
.end method

.method public updateAppPN(Ljava/lang/String;IZ)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "installed"    # Z

    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateAppPN packageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",installed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mAppsPN:Ljava/util/Set;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    if-eqz p3, :cond_0

    .line 351
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->addUidToMap(Ljava/lang/String;I)V

    goto :goto_0

    .line 353
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->removeUidFromMap(Ljava/lang/String;)V

    .line 355
    :goto_0
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateUidFromWholeAppMap()V

    .line 357
    :cond_1
    return-void
.end method
