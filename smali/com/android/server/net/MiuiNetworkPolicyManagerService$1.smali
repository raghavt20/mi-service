.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 354
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 5
    .param p1, "network"    # Landroid/net/Network;

    .line 357
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onAvailable(Landroid/net/Network;)V

    .line 358
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmConnectivityManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/ConnectivityManager;

    move-result-object v0

    .line 359
    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v0

    .line 360
    .local v0, "networkCapabilities":Landroid/net/NetworkCapabilities;
    if-nez v0, :cond_0

    return-void

    .line 361
    :cond_0
    const/4 v1, 0x0

    .line 362
    .local v1, "isCompatible":Z
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z

    move-result v2

    .line 363
    .local v2, "isVpn":Z
    if-eqz v2, :cond_1

    .line 364
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmVpnNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;)V

    .line 365
    const/4 v1, 0x1

    goto :goto_0

    .line 367
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkCapabilities;->getTransportInfo()Landroid/net/TransportInfo;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiInfo;

    .line 368
    .local v3, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->isPrimary()Z

    move-result v4

    if-nez v4, :cond_2

    .line 369
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmSlaveWifiNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;)V

    .line 370
    const/4 v1, 0x1

    .line 373
    .end local v3    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    :cond_2
    :goto_0
    if-eqz v1, :cond_4

    .line 374
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHasAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 375
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 376
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmHasAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 378
    :cond_3
    const-string v3, "MiuiNetworkPolicy"

    const-string v4, "Incompatible network, disable auto forward"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    return-void

    .line 381
    :cond_4
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mcheckEnableAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/NetworkCapabilities;Landroid/net/Network;)V

    .line 382
    return-void
.end method

.method public onCapabilitiesChanged(Landroid/net/Network;Landroid/net/NetworkCapabilities;)V
    .locals 1
    .param p1, "network"    # Landroid/net/Network;
    .param p2, "networkCapabilities"    # Landroid/net/NetworkCapabilities;

    .line 386
    invoke-super {p0, p1, p2}, Landroid/net/ConnectivityManager$NetworkCallback;->onCapabilitiesChanged(Landroid/net/Network;Landroid/net/NetworkCapabilities;)V

    .line 387
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0, p2, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mcheckEnableAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/NetworkCapabilities;Landroid/net/Network;)V

    .line 388
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 3
    .param p1, "network"    # Landroid/net/Network;

    .line 392
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onLost(Landroid/net/Network;)V

    .line 393
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSlaveWifiNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/Network;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "MiuiNetworkPolicy"

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmSlaveWifiNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;)V

    .line 395
    const-string v0, "Slave wifi Network is lost"

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmVpnNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/Network;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmVpnNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;)V

    .line 398
    const-string/jumbo v0, "vpn Network is lost"

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmCurrentNetwork(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/net/Network;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHasAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 400
    const-string v0, "network is lost, remove autoforward rule"

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableAutoForward(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 403
    :cond_2
    :goto_0
    return-void
.end method
