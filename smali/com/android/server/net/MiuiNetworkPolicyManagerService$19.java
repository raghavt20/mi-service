class com.android.server.net.MiuiNetworkPolicyManagerService$19 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerDefaultInputMethodChangedObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$19 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1387 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 13 */
/* .param p1, "selfChange" # Z */
/* .line 1390 */
/* invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V */
/* .line 1391 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v0 );
/* const-string/jumbo v1, "user" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/UserManager; */
/* .line 1392 */
/* .local v0, "um":Landroid/os/UserManager; */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 1393 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
(( android.os.UserManager ) v0 ).getUsers ( ); // invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;
/* .line 1395 */
/* .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;" */
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mgetDefaultInputMethod ( v3 );
/* .line 1396 */
/* .local v3, "inputMethodPkgName":Ljava/lang/String; */
final String v4 = ""; // const-string v4, ""
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_4 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmDefaultInputMethod ( v4 );
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_4 */
/* .line 1397 */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_3
/* check-cast v5, Landroid/content/pm/UserInfo; */
/* .line 1398 */
/* .local v5, "user":Landroid/content/pm/UserInfo; */
/* iget v6, v5, Landroid/content/pm/UserInfo;->id:I */
int v7 = 0; // const/4 v7, 0x0
(( android.content.pm.PackageManager ) v1 ).getInstalledPackagesAsUser ( v7, v6 ); // invoke-virtual {v1, v7, v6}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;
/* .line 1399 */
/* .local v6, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_2
/* check-cast v9, Landroid/content/pm/PackageInfo; */
/* .line 1400 */
/* .local v9, "app":Landroid/content/pm/PackageInfo; */
v10 = this.packageName;
if ( v10 != null) { // if-eqz v10, :cond_1
	 v10 = this.applicationInfo;
	 if ( v10 != null) { // if-eqz v10, :cond_1
		 /* .line 1401 */
		 /* iget v10, v5, Landroid/content/pm/UserInfo;->id:I */
		 v11 = this.applicationInfo;
		 /* iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
		 v10 = 		 android.os.UserHandle .getUid ( v10,v11 );
		 /* .line 1402 */
		 /* .local v10, "uid":I */
		 v11 = this.packageName;
		 v12 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmDefaultInputMethod ( v12 );
		 v11 = 		 (( java.lang.String ) v11 ).equals ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v11 != null) { // if-eqz v11, :cond_0
			 /* .line 1403 */
			 v11 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmUnRestrictApps ( v11 );
			 java.lang.Integer .valueOf ( v10 );
			 /* .line 1404 */
			 v11 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkManager ( v11 );
			 (( com.android.server.net.MiuiNetworkManagementService ) v11 ).whiteListUid ( v10, v7 ); // invoke-virtual {v11, v10, v7}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z
			 /* .line 1405 */
		 } // :cond_0
		 v11 = this.packageName;
		 v11 = 		 (( java.lang.String ) v11 ).equals ( v3 ); // invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v11 != null) { // if-eqz v11, :cond_1
			 /* .line 1406 */
			 v11 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmUnRestrictApps ( v11 );
			 java.lang.Integer .valueOf ( v10 );
			 /* .line 1407 */
			 v11 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkManager ( v11 );
			 int v12 = 1; // const/4 v12, 0x1
			 (( com.android.server.net.MiuiNetworkManagementService ) v11 ).whiteListUid ( v10, v12 ); // invoke-virtual {v11, v10, v12}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z
			 /* .line 1410 */
		 } // .end local v9 # "app":Landroid/content/pm/PackageInfo;
	 } // .end local v10 # "uid":I
} // :cond_1
} // :goto_2
/* .line 1411 */
} // .end local v5 # "user":Landroid/content/pm/UserInfo;
} // .end local v6 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_2
/* .line 1413 */
} // :cond_3
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmDefaultInputMethod ( v4,v3 );
/* .line 1415 */
} // :cond_4
return;
} // .end method
