class com.android.server.net.MiuiNetworkPolicyTrafficLimit$8 implements java.lang.Runnable {
	 /* .source "MiuiNetworkPolicyTrafficLimit.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateWhiteListUidForMobileTraffic(IZ)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyTrafficLimit this$0; //synthetic
final Boolean val$add; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyTrafficLimit$8 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 354 */
this.this$0 = p1;
/* iput p2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->val$uid:I */
/* iput-boolean p3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->val$add:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 357 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmNetMgrService ( v0 );
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->val$uid:I */
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;->val$add:Z */
v0 = (( com.android.server.net.MiuiNetworkManagementService ) v0 ).whiteListUidForMobileTraffic ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUidForMobileTraffic(IZ)Z
/* .line 358 */
/* .local v0, "rst":Z */
v1 = this.this$0;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateWhiteListUidForMobileTraffic rst=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mlog ( v1,v2 );
/* .line 359 */
return;
} // .end method
