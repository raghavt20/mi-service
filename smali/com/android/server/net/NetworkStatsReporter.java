public class com.android.server.net.NetworkStatsReporter implements android.app.AlarmManager$OnAlarmListener {
	 /* .source "NetworkStatsReporter.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long AlARM_INTERVAL;
private static final java.lang.String DEVICE_REGION;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Long HOUR_IN_MILLIS;
private static final java.lang.String MOBILE_RX_BYTES;
private static final java.lang.String MOBILE_TOTAL_BYTES;
private static final java.lang.String MOBILE_TX_BYTES;
private static final Integer MSG_TIMER;
private static final java.lang.String NETWORK_RX_BYTES;
private static final java.lang.String NETWORK_STATS_EVENT_NAME;
private static final java.lang.String NETWORK_TOTAL_BYTES;
private static final java.lang.String NETWORK_TX_BYTES;
private static final java.lang.String ONETRACK_APP_ID;
private static final java.lang.String ONETRACK_PACKAGE;
private static final java.lang.String PACKAGE_NAME;
private static final java.lang.String TAG;
private static final java.lang.String WIFI_RX_BYTES;
private static final java.lang.String WIFI_TOTAL_BYTES;
private static final java.lang.String WIFI_TX_BYTES;
private static com.android.server.net.NetworkStatsReporter sSelf;
/* # instance fields */
private android.app.AlarmManager mAlarmManager;
private android.content.Context mContext;
private android.os.Handler mHandler;
private android.os.Handler$Callback mHandlerCallback;
private android.net.NetworkTemplate mMobileTemplate;
java.util.HashMap mNetworkStatsMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mStartAlarmTime;
private android.net.INetworkStatsService mStatsService;
private android.net.NetworkTemplate mWifiTemplate;
/* # direct methods */
static void -$$Nest$mclear ( com.android.server.net.NetworkStatsReporter p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->clear()V */
return;
} // .end method
static void -$$Nest$mreportNetworkStats ( com.android.server.net.NetworkStatsReporter p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->reportNetworkStats()V */
return;
} // .end method
static void -$$Nest$mstartAlarm ( com.android.server.net.NetworkStatsReporter p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->startAlarm()V */
return;
} // .end method
static void -$$Nest$mupdateNetworkStats ( com.android.server.net.NetworkStatsReporter p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->updateNetworkStats()V */
return;
} // .end method
static com.android.server.net.NetworkStatsReporter ( ) {
/* .locals 2 */
/* .line 27 */
/* const-class v0, Lcom/android/server/net/NetworkStatsReporter; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 28 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
final String v1 = "CN"; // const-string v1, "CN"
android.os.SystemProperties .get ( v0,v1 );
return;
} // .end method
private com.android.server.net.NetworkStatsReporter ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 58 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mStartAlarmTime:J */
/* .line 56 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mNetworkStatsMap = v0;
/* .line 233 */
/* new-instance v0, Lcom/android/server/net/NetworkStatsReporter$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/net/NetworkStatsReporter$1;-><init>(Lcom/android/server/net/NetworkStatsReporter;)V */
this.mHandlerCallback = v0;
/* .line 59 */
/* if-nez p1, :cond_0 */
/* .line 60 */
v0 = com.android.server.net.NetworkStatsReporter.TAG;
final String v1 = "context is null, NetworkStatsReporter init failed"; // const-string v1, "context is null, NetworkStatsReporter init failed"
android.util.Log .e ( v0,v1 );
/* .line 61 */
return;
/* .line 64 */
} // :cond_0
this.mContext = p1;
/* .line 65 */
/* new-instance v0, Landroid/os/HandlerThread; */
v1 = com.android.server.net.NetworkStatsReporter.TAG;
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 66 */
/* .local v0, "thread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 67 */
/* new-instance v1, Landroid/os/Handler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
v3 = this.mHandlerCallback;
/* invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V */
this.mHandler = v1;
/* .line 68 */
/* nop */
/* .line 69 */
final String v1 = "netstats"; // const-string v1, "netstats"
android.os.ServiceManager .getService ( v1 );
/* .line 68 */
android.net.INetworkStatsService$Stub .asInterface ( v1 );
this.mStatsService = v1;
/* .line 70 */
android.net.NetworkTemplate .buildTemplateWifiWildcard ( );
this.mWifiTemplate = v1;
/* .line 71 */
android.net.NetworkTemplate .buildTemplateMobileWildcard ( );
this.mMobileTemplate = v1;
/* .line 72 */
v1 = this.mContext;
final String v2 = "alarm"; // const-string v2, "alarm"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/AlarmManager; */
this.mAlarmManager = v1;
/* .line 74 */
/* invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->startAlarm()V */
/* .line 75 */
return;
} // .end method
private void clear ( ) {
/* .locals 1 */
/* .line 207 */
v0 = this.mNetworkStatsMap;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 208 */
return;
} // .end method
private void getBackgroundNetworkStats ( android.net.NetworkTemplate p0, android.net.INetworkStatsSession p1 ) {
/* .locals 11 */
/* .param p1, "networkTemplate" # Landroid/net/NetworkTemplate; */
/* .param p2, "networkStatsSession" # Landroid/net/INetworkStatsSession; */
/* .line 110 */
/* if-nez p1, :cond_0 */
/* .line 111 */
v0 = com.android.server.net.NetworkStatsReporter.TAG;
final String v1 = "networkTemplate is null"; // const-string v1, "networkTemplate is null"
android.util.Log .e ( v0,v1 );
/* .line 112 */
return;
/* .line 115 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 116 */
/* .local v0, "now":J */
int v9 = 0; // const/4 v9, 0x0
/* .line 118 */
/* .local v9, "networkStats":Landroid/net/NetworkStats; */
try { // :try_start_0
/* iget-wide v4, p0, Lcom/android/server/net/NetworkStatsReporter;->mStartAlarmTime:J */
int v8 = 0; // const/4 v8, 0x0
/* move-object v2, p2 */
/* move-object v3, p1 */
/* move-wide v6, v0 */
/* invoke-interface/range {v2 ..v8}, Landroid/net/INetworkStatsSession;->getSummaryForAllUid(Landroid/net/NetworkTemplate;JJZ)Landroid/net/NetworkStats; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 123 */
} // .end local v9 # "networkStats":Landroid/net/NetworkStats;
/* .local v2, "networkStats":Landroid/net/NetworkStats; */
/* nop */
/* .line 124 */
if ( v2 != null) { // if-eqz v2, :cond_7
v3 = (( android.net.NetworkStats ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/net/NetworkStats;->size()I
/* if-nez v3, :cond_1 */
/* .line 128 */
} // :cond_1
/* new-instance v3, Landroid/net/NetworkStats$Entry; */
/* invoke-direct {v3}, Landroid/net/NetworkStats$Entry;-><init>()V */
/* .line 129 */
/* .local v3, "entry":Landroid/net/NetworkStats$Entry; */
v4 = (( android.net.NetworkStats ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/net/NetworkStats;->size()I
/* .line 130 */
/* .local v4, "size":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* if-ge v5, v4, :cond_6 */
/* .line 131 */
(( android.net.NetworkStats ) v2 ).getValues ( v5, v3 ); // invoke-virtual {v2, v5, v3}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;
/* .line 132 */
if ( v3 != null) { // if-eqz v3, :cond_5
/* iget v6, v3, Landroid/net/NetworkStats$Entry;->set:I */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 133 */
/* .line 136 */
} // :cond_2
/* iget v6, v3, Landroid/net/NetworkStats$Entry;->uid:I */
/* .line 137 */
/* .local v6, "uid":I */
/* const/16 v7, 0x3e8 */
/* if-gt v6, v7, :cond_3 */
/* .line 138 */
/* .line 140 */
} // :cond_3
/* invoke-direct {p0, v6}, Lcom/android/server/net/NetworkStatsReporter;->getPackageNameByUid(I)Ljava/lang/String; */
/* .line 142 */
/* .local v7, "packageName":Ljava/lang/String; */
v8 = this.mNetworkStatsMap;
java.lang.Integer .valueOf ( v6 );
v8 = (( java.util.HashMap ) v8 ).containsKey ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 143 */
v8 = this.mNetworkStatsMap;
java.lang.Integer .valueOf ( v6 );
(( java.util.HashMap ) v8 ).get ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats; */
/* .local v8, "backgroundNetworkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats; */
/* .line 145 */
} // .end local v8 # "backgroundNetworkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
} // :cond_4
/* new-instance v8, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats; */
/* invoke-direct {v8, p0, v6, v7}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;-><init>(Lcom/android/server/net/NetworkStatsReporter;ILjava/lang/String;)V */
/* .line 147 */
/* .restart local v8 # "backgroundNetworkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats; */
} // :goto_1
(( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats ) v8 ).updateBytes ( p1, v3 ); // invoke-virtual {v8, p1, v3}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->updateBytes(Landroid/net/NetworkTemplate;Landroid/net/NetworkStats$Entry;)V
/* .line 148 */
v9 = this.mNetworkStatsMap;
java.lang.Integer .valueOf ( v6 );
(( java.util.HashMap ) v9 ).put ( v10, v8 ); // invoke-virtual {v9, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 130 */
} // .end local v6 # "uid":I
} // .end local v7 # "packageName":Ljava/lang/String;
} // .end local v8 # "backgroundNetworkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
} // :cond_5
} // :goto_2
/* add-int/lit8 v5, v5, 0x1 */
/* .line 150 */
} // .end local v5 # "i":I
} // :cond_6
return;
/* .line 125 */
} // .end local v3 # "entry":Landroid/net/NetworkStats$Entry;
} // .end local v4 # "size":I
} // :cond_7
} // :goto_3
return;
/* .line 120 */
} // .end local v2 # "networkStats":Landroid/net/NetworkStats;
/* .restart local v9 # "networkStats":Landroid/net/NetworkStats; */
/* :catch_0 */
/* move-exception v2 */
/* .line 121 */
/* .local v2, "e":Ljava/lang/Exception; */
v3 = com.android.server.net.NetworkStatsReporter.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "getSummaryForAllUid exception"; // const-string v5, "getSummaryForAllUid exception"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* .line 122 */
return;
} // .end method
private java.lang.String getPackageNameByUid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 212 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackagesForUid ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 213 */
/* .local v0, "pkgs":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v1, v0 */
/* if-lez v1, :cond_0 */
/* .line 214 */
int v1 = 0; // const/4 v1, 0x0
/* aget-object v1, v0, v1 */
/* .local v1, "packageName":Ljava/lang/String; */
/* .line 216 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // :cond_0
java.lang.Integer .toString ( p1 );
/* .line 218 */
/* .restart local v1 # "packageName":Ljava/lang/String; */
} // :goto_0
} // .end method
public static com.android.server.net.NetworkStatsReporter make ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 78 */
v0 = com.android.server.net.NetworkStatsReporter.sSelf;
/* if-nez v0, :cond_0 */
/* .line 79 */
/* new-instance v0, Lcom/android/server/net/NetworkStatsReporter; */
/* invoke-direct {v0, p0}, Lcom/android/server/net/NetworkStatsReporter;-><init>(Landroid/content/Context;)V */
/* .line 82 */
} // :cond_0
v0 = com.android.server.net.NetworkStatsReporter.sSelf;
} // .end method
private void reportNetworkStats ( ) {
/* .locals 7 */
/* .line 153 */
v0 = this.mNetworkStatsMap;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 154 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;>;" */
/* check-cast v2, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats; */
/* .line 155 */
/* .local v2, "networkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats; */
if ( v2 != null) { // if-eqz v2, :cond_0
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmPackageName ( v2 );
v3 = (( com.android.server.net.NetworkStatsReporter ) p0 ).isNumericOrNull ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/net/NetworkStatsReporter;->isNumericOrNull(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 156 */
/* .line 159 */
} // :cond_1
/* new-instance v3, Landroid/os/Bundle; */
/* invoke-direct {v3}, Landroid/os/Bundle;-><init>()V */
/* .line 160 */
/* .local v3, "params":Landroid/os/Bundle; */
final String v4 = "background_package"; // const-string v4, "background_package"
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmPackageName ( v2 );
(( android.os.Bundle ) v3 ).putString ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 161 */
/* const-string/jumbo v4, "wifi_tx_bytes" */
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmWifiTxBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 162 */
/* const-string/jumbo v4, "wifi_rx_bytes" */
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmWifiRxBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 163 */
/* const-string/jumbo v4, "wifi_total_bytes" */
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmWifiTotalBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 164 */
final String v4 = "mobile_tx_bytes"; // const-string v4, "mobile_tx_bytes"
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmMobileTxBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 165 */
final String v4 = "mobile_rx_bytes"; // const-string v4, "mobile_rx_bytes"
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmMobileRxBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 166 */
final String v4 = "mobile_total_bytes"; // const-string v4, "mobile_total_bytes"
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmMobileTotalBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 167 */
final String v4 = "network_tx_bytes"; // const-string v4, "network_tx_bytes"
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmTotalTxBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 168 */
final String v4 = "network_rx_bytes"; // const-string v4, "network_rx_bytes"
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmTotalRxBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 169 */
final String v4 = "network_total_bytes"; // const-string v4, "network_total_bytes"
com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats .-$$Nest$fgetmTotalBytes ( v2 );
/* move-result-wide v5 */
(( android.os.Bundle ) v3 ).putLong ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 170 */
v4 = this.mContext;
final String v5 = "background_network_statistics"; // const-string v5, "background_network_statistics"
/* invoke-direct {p0, v4, v5, v3}, Lcom/android/server/net/NetworkStatsReporter;->reportNetworkStatsEvent(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V */
/* .line 171 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;>;"
} // .end local v2 # "networkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
} // .end local v3 # "params":Landroid/os/Bundle;
/* goto/16 :goto_0 */
/* .line 172 */
} // :cond_2
return;
} // .end method
private void reportNetworkStatsEvent ( android.content.Context p0, java.lang.String p1, android.os.Bundle p2 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "eventName" # Ljava/lang/String; */
/* .param p3, "params" # Landroid/os/Bundle; */
/* .line 175 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = com.android.server.net.NetworkStatsReporter.DEVICE_REGION;
final String v1 = "CN"; // const-string v1, "CN"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 179 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 180 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 181 */
final String v1 = "APP_ID"; // const-string v1, "APP_ID"
final String v2 = "31000000072"; // const-string v2, "31000000072"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 182 */
final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
final String v2 = "com.android.server.net"; // const-string v2, "com.android.server.net"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 183 */
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 184 */
(( android.content.Intent ) v0 ).putExtras ( p3 ); // invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 185 */
int v1 = 2; // const/4 v1, 0x2
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 186 */
(( android.content.Context ) p1 ).startService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 189 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 187 */
/* :catch_0 */
/* move-exception v0 */
/* .line 188 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 190 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 176 */
} // :cond_1
} // :goto_1
return;
} // .end method
private void startAlarm ( ) {
/* .locals 9 */
/* .line 193 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mStartAlarmTime:J */
/* .line 194 */
v2 = this.mAlarmManager;
int v3 = 2; // const/4 v3, 0x2
/* .line 195 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* const-wide/32 v4, 0x2932e00 */
/* add-long/2addr v4, v0 */
v6 = com.android.server.net.NetworkStatsReporter.TAG;
v8 = this.mHandler;
/* .line 194 */
/* move-object v7, p0 */
/* invoke-virtual/range {v2 ..v8}, Landroid/app/AlarmManager;->set(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 197 */
return;
} // .end method
private void updateNetworkStats ( ) {
/* .locals 5 */
/* .line 86 */
final String v0 = "open session exception:"; // const-string v0, "open session exception:"
int v1 = 0; // const/4 v1, 0x0
/* .line 88 */
/* .local v1, "networkStatsSession":Landroid/net/INetworkStatsSession; */
try { // :try_start_0
v2 = this.mStatsService;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* move-object v1, v2 */
/* .line 92 */
/* nop */
/* .line 93 */
/* if-nez v1, :cond_0 */
/* .line 94 */
v0 = com.android.server.net.NetworkStatsReporter.TAG;
final String v2 = "networkStatsSession is null"; // const-string v2, "networkStatsSession is null"
android.util.Log .e ( v0,v2 );
/* .line 95 */
return;
/* .line 98 */
} // :cond_0
v2 = this.mMobileTemplate;
/* invoke-direct {p0, v2, v1}, Lcom/android/server/net/NetworkStatsReporter;->getBackgroundNetworkStats(Landroid/net/NetworkTemplate;Landroid/net/INetworkStatsSession;)V */
/* .line 99 */
v2 = this.mWifiTemplate;
/* invoke-direct {p0, v2, v1}, Lcom/android/server/net/NetworkStatsReporter;->getBackgroundNetworkStats(Landroid/net/NetworkTemplate;Landroid/net/INetworkStatsSession;)V */
/* .line 102 */
try { // :try_start_1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 105 */
/* .line 103 */
/* :catch_0 */
/* move-exception v2 */
/* .line 104 */
/* .local v2, "e":Ljava/lang/Exception; */
v3 = com.android.server.net.NetworkStatsReporter.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v0 );
/* .line 106 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 89 */
/* :catch_1 */
/* move-exception v2 */
/* .line 90 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
v3 = com.android.server.net.NetworkStatsReporter.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v0 );
/* .line 91 */
return;
} // .end method
/* # virtual methods */
public Boolean isNumericOrNull ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 222 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 223 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* if-ge v0, v1, :cond_1 */
/* .line 224 */
v1 = (( java.lang.String ) p1 ).charAt ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C
v1 = java.lang.Character .isDigit ( v1 );
/* if-nez v1, :cond_0 */
/* .line 225 */
int v1 = 0; // const/4 v1, 0x0
/* .line 223 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 230 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void onAlarm ( ) {
/* .locals 2 */
/* .line 201 */
v0 = this.mHandler;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 202 */
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 204 */
} // :cond_0
return;
} // .end method
