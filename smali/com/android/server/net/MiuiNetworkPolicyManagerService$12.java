class com.android.server.net.MiuiNetworkPolicyManagerService$12 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerNetworkProrityModeChangedObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$12 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1111 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 1114 */
if ( p2 != null) { // if-eqz p2, :cond_1
	 final String v0 = "cloud_network_priority_enabled"; // const-string v0, "cloud_network_priority_enabled"
	 android.provider.Settings$System .getUriFor ( v0 );
	 v0 = 	 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 /* if-nez v0, :cond_0 */
	 /* .line 1115 */
	 final String v0 = "cloud_mtk_wifi_traffic_priority_mode"; // const-string v0, "cloud_mtk_wifi_traffic_priority_mode"
	 android.provider.Settings$System .getUriFor ( v0 );
	 v0 = 	 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 1116 */
	 } // :cond_0
	 v0 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mnetworkPriorityCloudControl ( v0 );
	 /* .line 1118 */
} // :cond_1
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mnetworkPriorityMode ( v0 );
/* .line 1119 */
/* .local v0, "networkPriority":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "networkPriorityMode = "; // const-string v2, "networkPriorityMode = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mNetworkPriorityMode ="; // const-string v2, " mNetworkPriorityMode ="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mWifiConnected="; // const-string v2, " mWifiConnected="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .i ( v2,v1 );
/* .line 1121 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v1 );
/* if-eq v0, v1, :cond_2 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 1122 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableNetworkPriority ( v1,v0 );
	 /* .line 1124 */
} // :cond_2
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mregisterWifiNetworkListener ( v1 );
/* .line 1125 */
return;
} // .end method
