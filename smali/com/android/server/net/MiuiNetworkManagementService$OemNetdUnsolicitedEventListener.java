class com.android.server.net.MiuiNetworkManagementService$OemNetdUnsolicitedEventListener extends com.android.internal.net.IOemNetdUnsolicitedEventListener$Stub {
	 /* .source "MiuiNetworkManagementService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkManagementService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "OemNetdUnsolicitedEventListener" */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkManagementService this$0; //synthetic
/* # direct methods */
private com.android.server.net.MiuiNetworkManagementService$OemNetdUnsolicitedEventListener ( ) {
/* .locals 0 */
/* .line 357 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;-><init>()V */
return;
} // .end method
 com.android.server.net.MiuiNetworkManagementService$OemNetdUnsolicitedEventListener ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;-><init>(Lcom/android/server/net/MiuiNetworkManagementService;)V */
return;
} // .end method
/* # virtual methods */
public void onFirewallBlocked ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "code" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 365 */
java.lang.Integer .valueOf ( p1 );
/* filled-new-array {v0, p2}, [Ljava/lang/Object; */
final String v1 = "code=%d, pkg=%s"; // const-string v1, "code=%d, pkg=%s"
java.lang.String .format ( v1,v0 );
final String v1 = "NetworkManagement"; // const-string v1, "NetworkManagement"
android.util.Log .d ( v1,v0 );
/* .line 366 */
/* const/16 v0, 0x2bb */
/* if-ne v0, p1, :cond_0 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 367 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.FIREWALL"; // const-string v1, "miui.intent.action.FIREWALL"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 368 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 369 */
final String v1 = "pkg"; // const-string v1, "pkg"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 370 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkManagementService .-$$Nest$fgetmContext ( v1 );
v2 = android.os.UserHandle.ALL;
final String v3 = "com.miui.permission.FIREWALL"; // const-string v3, "com.miui.permission.FIREWALL"
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
/* .line 372 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
return;
} // .end method
public void onRegistered ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 360 */
final String v0 = "NetworkManagement"; // const-string v0, "NetworkManagement"
final String v1 = "onRegistered"; // const-string v1, "onRegistered"
android.util.Log .d ( v0,v1 );
/* .line 361 */
return;
} // .end method
public void onUnreachedPort ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 0 */
/* .param p1, "port" # I */
/* .param p2, "ip" # I */
/* .param p3, "interfaceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 376 */
return;
} // .end method
