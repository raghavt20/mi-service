public class com.android.server.net.MiuiNetworkPolicyServiceSupport {
	 /* .source "MiuiNetworkPolicyServiceSupport.java" */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.app.IActivityManager mActivityManager;
	 private final android.content.Context mContext;
	 private final android.os.Handler mHandler;
	 private android.net.wifi.MiuiWifiManager mMiuiWifiManager;
	 private final android.app.IUidObserver mUidObserver;
	 /* # direct methods */
	 static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.net.MiuiNetworkPolicyServiceSupport p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mHandler;
	 } // .end method
	 public com.android.server.net.MiuiNetworkPolicyServiceSupport ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .line 28 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 68 */
		 /* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport$1; */
		 /* invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;)V */
		 this.mUidObserver = v0;
		 /* .line 29 */
		 this.mContext = p1;
		 /* .line 30 */
		 this.mHandler = p2;
		 /* .line 31 */
		 android.app.ActivityManagerNative .getDefault ( );
		 this.mActivityManager = v0;
		 /* .line 32 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void enablePowerSave ( Boolean p0 ) {
		 /* .locals 2 */
		 /* .param p1, "enabled" # Z */
		 /* .line 45 */
		 v0 = this.mMiuiWifiManager;
		 /* if-nez v0, :cond_0 */
		 /* .line 46 */
		 v0 = this.mContext;
		 final String v1 = "MiuiWifiService"; // const-string v1, "MiuiWifiService"
		 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/net/wifi/MiuiWifiManager; */
		 this.mMiuiWifiManager = v0;
		 /* .line 48 */
	 } // :cond_0
	 v0 = this.mMiuiWifiManager;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 49 */
		 (( android.net.wifi.MiuiWifiManager ) v0 ).enablePowerSave ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/wifi/MiuiWifiManager;->enablePowerSave(Z)V
		 /* .line 51 */
	 } // :cond_1
	 return;
} // .end method
public void registerUidObserver ( ) {
	 /* .locals 5 */
	 /* .line 36 */
	 try { // :try_start_0
		 v0 = this.mActivityManager;
		 v1 = this.mUidObserver;
		 int v2 = -1; // const/4 v2, -0x1
		 int v3 = 0; // const/4 v3, 0x0
		 int v4 = 3; // const/4 v4, 0x3
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 41 */
		 /* .line 39 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 42 */
	 } // :goto_0
	 return;
} // .end method
public java.lang.String updateIface ( java.lang.String p0 ) {
	 /* .locals 5 */
	 /* .param p1, "iface" # Ljava/lang/String; */
	 /* .line 54 */
	 v0 = this.mContext;
	 /* const-string/jumbo v1, "wifi" */
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/net/wifi/WifiManager; */
	 /* .line 55 */
	 /* .local v0, "wm":Landroid/net/wifi/WifiManager; */
	 v1 = this.mContext;
	 final String v2 = "connectivity"; // const-string v2, "connectivity"
	 (( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v1, Landroid/net/ConnectivityManager; */
	 /* .line 56 */
	 /* .local v1, "cm":Landroid/net/ConnectivityManager; */
	 (( android.net.wifi.WifiManager ) v0 ).getCurrentNetwork ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;
	 (( android.net.ConnectivityManager ) v1 ).getLinkProperties ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;
	 /* .line 57 */
	 /* .local v2, "lp":Landroid/net/LinkProperties; */
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 58 */
		 (( android.net.LinkProperties ) v2 ).getInterfaceName ( ); // invoke-virtual {v2}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
		 /* .line 59 */
		 /* .local v3, "newIface":Ljava/lang/String; */
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 v4 = 			 android.text.TextUtils .equals ( p1,v3 );
			 /* if-nez v4, :cond_0 */
			 /* .line 60 */
			 com.android.server.net.MiuiNetworkManagementService .getInstance ( );
			 (( com.android.server.net.MiuiNetworkManagementService ) v4 ).updateIface ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->updateIface(Ljava/lang/String;)Z
			 /* .line 61 */
			 /* .line 65 */
		 } // .end local v3 # "newIface":Ljava/lang/String;
	 } // :cond_0
} // .end method
