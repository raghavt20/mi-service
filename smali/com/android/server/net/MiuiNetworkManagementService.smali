.class public Lcom/android/server/net/MiuiNetworkManagementService;
.super Ljava/lang/Object;
.source "MiuiNetworkManagementService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;,
        Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;,
        Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
    }
.end annotation


# static fields
.field private static final ALLOW:I = 0x0

.field private static final MIUI_FIREWALL_RESPONSE_CODE:I = 0x2bb

.field private static final POWER_SAVE_IDLETIMER_LABEL:I = 0x76

.field private static final REJECT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "NetworkManagement"

.field private static final TYPE_ALL:I = 0x3

.field private static final TYPE_MOBILE:I = 0x0

.field private static final TYPE_ROAMING:I = 0x2

.field private static final TYPE_WIFI:I = 0x1

.field private static sInstance:Lcom/android/server/net/MiuiNetworkManagementService;


# instance fields
.field private mBinder:Landroid/os/IBinder;

.field private final mContext:Landroid/content/Context;

.field private mCurrentNetworkState:I

.field private mListenedIdleTimerType:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mListenter:Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;

.field private final mLock:Ljava/lang/Object;

.field private mMiuiFireRuleCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/Set<",
            "Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNetd:Lcom/android/internal/net/IOemNetd;

.field private mNms:Landroid/os/INetworkManagementService;

.field private mObserver:Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;

.field private mStateSetterPid:I

.field private mStateSetterUid:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/net/MiuiNetworkManagementService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNms:Landroid/os/INetworkManagementService;

    .line 551
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mLock:Ljava/lang/Object;

    .line 552
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mCurrentNetworkState:I

    iput v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterUid:I

    iput v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterPid:I

    .line 72
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mListenedIdleTimerType:Ljava/util/Set;

    .line 75
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method static declared-synchronized Init(Landroid/content/Context;)Lcom/android/server/net/MiuiNetworkManagementService;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/android/server/net/MiuiNetworkManagementService;

    monitor-enter v0

    .line 63
    :try_start_0
    new-instance v1, Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkManagementService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/net/MiuiNetworkManagementService;->sInstance:Lcom/android/server/net/MiuiNetworkManagementService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit v0

    return-object v1

    .line 62
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private closeSocketsForFirewall(Ljava/lang/String;[IZ)V
    .locals 11
    .param p1, "fwType"    # Ljava/lang/String;
    .param p2, "uids"    # [I
    .param p3, "isWhitelistUid"    # Z

    .line 217
    const-string v0, "fw_doze"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 218
    nop

    .line 219
    const/16 v0, 0x2710

    const v2, 0x7fffffff

    invoke-static {v0, v2}, Lcom/android/server/net/MiuiNetworkManagementService;->makeUidRangeParcel(II)Landroid/net/UidRangeParcel;

    move-result-object v0

    filled-new-array {v0}, [Landroid/net/UidRangeParcel;

    move-result-object v0

    .line 223
    .local v0, "ranges":[Landroid/net/UidRangeParcel;
    move-object v2, p2

    .local v2, "exemptUids":[I
    goto :goto_1

    .line 225
    .end local v0    # "ranges":[Landroid/net/UidRangeParcel;
    .end local v2    # "exemptUids":[I
    :cond_0
    array-length v0, p2

    new-array v0, v0, [Landroid/net/UidRangeParcel;

    .line 226
    .restart local v0    # "ranges":[Landroid/net/UidRangeParcel;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p2

    if-ge v2, v3, :cond_1

    .line 227
    aget v3, p2, v2

    aget v4, p2, v2

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->makeUidRangeParcel(II)Landroid/net/UidRangeParcel;

    move-result-object v3

    aput-object v3, v0, v2

    .line 226
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 229
    .end local v2    # "i":I
    :cond_1
    new-array v2, v1, [I

    .line 235
    .local v2, "exemptUids":[I
    :goto_1
    :try_start_0
    const-string v3, "com.android.internal.net.IOemNetd$Stub"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 236
    .local v3, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "asInterface"

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Class;

    const-class v7, Landroid/os/IBinder;

    aput-object v7, v6, v1

    invoke-virtual {v3, v4, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 237
    .local v4, "asInterface":Ljava/lang/reflect/Method;
    new-array v6, v5, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mBinder:Landroid/os/IBinder;

    aput-object v7, v6, v1

    const/4 v7, 0x0

    invoke-virtual {v4, v7, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 238
    .local v6, "IOemNetdObj":Ljava/lang/Object;
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string/jumbo v8, "socketDestroy"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Class;

    const-class v10, [Landroid/net/UidRangeParcel;

    aput-object v10, v9, v1

    const-class v1, [I

    aput-object v1, v9, v5

    invoke-virtual {v7, v8, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 239
    .local v1, "socketDestroy":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_2

    .line 240
    filled-new-array {v0, v2}, [Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v1, v6, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    .end local v1    # "socketDestroy":Ljava/lang/reflect/Method;
    .end local v3    # "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "asInterface":Ljava/lang/reflect/Method;
    .end local v6    # "IOemNetdObj":Ljava/lang/Object;
    :cond_2
    goto :goto_2

    .line 241
    :catch_0
    move-exception v1

    .line 242
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "closeSocketsForFirewall Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "NetworkManagement"

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method private convertTypeToLable(I)Ljava/lang/String;
    .locals 3
    .param p1, "type"    # I

    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, "lable":Ljava/lang/String;
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 350
    const-string/jumbo v1, "wifi.interface"

    const-string/jumbo v2, "wlan0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 351
    :cond_0
    const/16 v1, 0x76

    if-le p1, v1, :cond_1

    .line 352
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 354
    :cond_1
    :goto_0
    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/android/server/net/MiuiNetworkManagementService;
    .locals 2

    const-class v0, Lcom/android/server/net/MiuiNetworkManagementService;

    monitor-enter v0

    .line 68
    :try_start_0
    sget-object v1, Lcom/android/server/net/MiuiNetworkManagementService;->sInstance:Lcom/android/server/net/MiuiNetworkManagementService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 68
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private getNmsService()V
    .locals 1

    .line 465
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNms:Landroid/os/INetworkManagementService;

    if-nez v0, :cond_0

    .line 466
    nop

    .line 467
    const-string v0, "network_management"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 466
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNms:Landroid/os/INetworkManagementService;

    .line 469
    :cond_0
    return-void
.end method

.method private static makeUidRangeParcel(II)Landroid/net/UidRangeParcel;
    .locals 1
    .param p0, "start"    # I
    .param p1, "stop"    # I

    .line 208
    new-instance v0, Landroid/net/UidRangeParcel;

    invoke-direct {v0, p0, p1}, Landroid/net/UidRangeParcel;-><init>(II)V

    .line 209
    .local v0, "range":Landroid/net/UidRangeParcel;
    return-object v0
.end method

.method private setCurrentNetworkState(III)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "callerUid"    # I
    .param p3, "callerPid"    # I

    .line 573
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 574
    :try_start_0
    iput p1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mCurrentNetworkState:I

    .line 575
    iput p2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterUid:I

    .line 576
    iput p3, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterPid:I

    .line 577
    monitor-exit v0

    .line 578
    return-void

    .line 577
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setMiuiFirewallRule(Ljava/lang/String;IIIII)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "rule"    # I
    .param p4, "type"    # I
    .param p5, "callerUid"    # I
    .param p6, "callerPid"    # I

    .line 556
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 557
    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mMiuiFireRuleCache:Landroid/util/SparseArray;

    if-nez v1, :cond_0

    .line 558
    new-instance v1, Landroid/util/SparseArray;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mMiuiFireRuleCache:Landroid/util/SparseArray;

    .line 560
    :cond_0
    new-instance v1, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;

    move-object v2, v1

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p5

    move v7, p6

    invoke-direct/range {v2 .. v7}, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;-><init>(Ljava/lang/String;IIII)V

    .line 561
    .local v1, "fireRule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mMiuiFireRuleCache:Landroid/util/SparseArray;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v2, p4, v3}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 562
    .local v2, "typeRule":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;>;"
    const/4 v3, 0x2

    if-nez p3, :cond_1

    if-eq p4, v3, :cond_2

    :cond_1
    if-eqz p3, :cond_3

    if-eq p4, v3, :cond_3

    .line 564
    :cond_2
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 566
    :cond_3
    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 568
    :goto_0
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mMiuiFireRuleCache:Landroid/util/SparseArray;

    invoke-virtual {v3, p4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 569
    .end local v1    # "fireRule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
    .end local v2    # "typeRule":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;>;"
    monitor-exit v0

    .line 570
    return-void

    .line 569
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public addMiuiFirewallSharedUid(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 178
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->addMiuiFirewallSharedUid(I)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 183
    const/4 v1, 0x0

    return v1
.end method

.method public closeSocketForAurogon([I)V
    .locals 3
    .param p1, "uids"    # [I

    .line 485
    const-string v0, "NetworkManagement"

    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNms:Landroid/os/INetworkManagementService;

    if-nez v1, :cond_0

    .line 486
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkManagementService;->getNmsService()V

    .line 488
    :cond_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNms:Landroid/os/INetworkManagementService;

    if-eqz v1, :cond_1

    .line 489
    const-string v1, "call socket destroy!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    :cond_1
    goto :goto_0

    .line 493
    :catch_0
    move-exception v1

    .line 494
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "failed to close socket for aurogon!"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public doDesSocketForUid(Ljava/lang/String;[IZ)V
    .locals 12
    .param p1, "fwType"    # Ljava/lang/String;
    .param p2, "uids"    # [I
    .param p3, "isWhitelistUid"    # Z

    .line 247
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :try_start_0
    const-string v0, "com.android.internal.net.IOemNetd$Stub"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 253
    .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v1, "asInterface"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Landroid/os/IBinder;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 254
    .local v1, "asInterface":Ljava/lang/reflect/Method;
    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mBinder:Landroid/os/IBinder;

    aput-object v5, v4, v6

    const/4 v5, 0x0

    invoke-virtual {v1, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 255
    .local v4, "IOemNetdObj":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v7, "doFireWallForUid"

    const/4 v8, 0x3

    new-array v9, v8, [Ljava/lang/Class;

    const-class v10, Ljava/lang/String;

    aput-object v10, v9, v6

    const-class v10, [I

    aput-object v10, v9, v3

    sget-object v10, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v11, 0x2

    aput-object v10, v9, v11

    invoke-virtual {v5, v7, v9}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 256
    .local v5, "doFireWallForUid":Ljava/lang/reflect/Method;
    if-eqz v5, :cond_0

    .line 257
    new-array v7, v8, [Ljava/lang/Object;

    aput-object p1, v7, v6

    aput-object p2, v7, v3

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v7, v11

    invoke-virtual {v5, v4, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    .end local v0    # "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "asInterface":Ljava/lang/reflect/Method;
    .end local v4    # "IOemNetdObj":Ljava/lang/Object;
    .end local v5    # "doFireWallForUid":Ljava/lang/reflect/Method;
    :cond_0
    goto :goto_0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doDesSocketForUid Exception: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkManagementService;->closeSocketsForFirewall(Ljava/lang/String;[IZ)V

    .line 263
    return-void
.end method

.method public doRestoreSockForUid(Ljava/lang/String;)V
    .locals 9
    .param p1, "fwType"    # Ljava/lang/String;

    .line 266
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :try_start_0
    const-string v0, "com.android.internal.net.IOemNetd$Stub"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 272
    .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v1, "asInterface"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Landroid/os/IBinder;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 273
    .local v1, "asInterface":Ljava/lang/reflect/Method;
    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mBinder:Landroid/os/IBinder;

    aput-object v5, v4, v6

    const/4 v5, 0x0

    invoke-virtual {v1, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 274
    .local v4, "IOemNetdObj":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v7, "doRestoreSockForUid"

    new-array v3, v3, [Ljava/lang/Class;

    const-class v8, Ljava/lang/String;

    aput-object v8, v3, v6

    invoke-virtual {v5, v7, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 275
    .local v3, "doRestoreSockForUid":Ljava/lang/reflect/Method;
    if-eqz v3, :cond_0

    .line 276
    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    .end local v0    # "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "asInterface":Ljava/lang/reflect/Method;
    .end local v3    # "doRestoreSockForUid":Ljava/lang/reflect/Method;
    .end local v4    # "IOemNetdObj":Ljava/lang/Object;
    :cond_0
    goto :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doRestoreSockForUid Exception: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 7
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 512
    if-nez p1, :cond_0

    .line 513
    return-void

    .line 515
    :cond_0
    const-string v0, "========================================MiuiFireRule DUMP BEGIN========================================"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 517
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mLock:Ljava/lang/Object;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 518
    :try_start_1
    iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mCurrentNetworkState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 519
    const-string v1, "Didn\'t cache the current network state!"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 521
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current Network State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mCurrentNetworkState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", it\'s setter uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mStateSetterPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 523
    :goto_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mMiuiFireRuleCache:Landroid/util/SparseArray;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_3

    .line 526
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mMiuiFireRuleCache:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 527
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mMiuiFireRuleCache:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 528
    .local v2, "rules":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 529
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rule type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mMiuiFireRuleCache:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 530
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;

    .line 531
    .local v4, "rule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "  Rule detail: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 532
    .end local v4    # "rule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
    goto :goto_2

    .line 526
    .end local v2    # "rules":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;>;"
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 524
    .end local v1    # "i":I
    :cond_4
    :goto_3
    const-string v1, "Didn\'t cache the miui fire rule!"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 536
    :cond_5
    monitor-exit v0

    .line 539
    goto :goto_4

    .line 536
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkManagementService;
    .end local p1    # "pw":Ljava/io/PrintWriter;
    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 537
    .restart local p0    # "this":Lcom/android/server/net/MiuiNetworkManagementService;
    .restart local p1    # "pw":Ljava/io/PrintWriter;
    :catch_0
    move-exception v0

    .line 538
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "NetworkManagement"

    const-string v2, "dump"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 540
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_4
    const-string v0, "========================================MiuiFireRule DUMP END========================================"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 541
    return-void
.end method

.method public enableAutoForward(Ljava/lang/String;IZ)I
    .locals 2
    .param p1, "addr"    # Ljava/lang/String;
    .param p2, "fwmark"    # I
    .param p3, "enabled"    # Z

    .line 501
    const/4 v0, -0x1

    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    if-eqz v1, :cond_0

    .line 502
    invoke-interface {v1, p1, p2, p3}, Lcom/android/internal/net/IOemNetd;->enableAutoForward(Ljava/lang/String;IZ)I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 507
    :cond_0
    nop

    .line 508
    return v0

    .line 504
    :catch_0
    move-exception v1

    .line 505
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 506
    return v0
.end method

.method public enableIptablesRestore(Z)Z
    .locals 3
    .param p1, "enabled"    # Z

    .line 137
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->enableIptablesRestore(Z)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 142
    const/4 v1, 0x0

    return v1
.end method

.method public enableLimitter(Z)Z
    .locals 3
    .param p1, "enabled"    # Z

    .line 97
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->enableLimitter(Z)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 102
    const/4 v1, 0x0

    return v1
.end method

.method public enableMobileTrafficLimit(ZLjava/lang/String;)Z
    .locals 3
    .param p1, "enabled"    # Z
    .param p2, "iface"    # Ljava/lang/String;

    .line 402
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/net/IOemNetd;->enableMobileTrafficLimit(ZLjava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 405
    :catch_0
    move-exception v0

    .line 406
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 407
    const/4 v1, 0x0

    return v1
.end method

.method public enableQos(Z)Z
    .locals 3
    .param p1, "enabled"    # Z

    .line 382
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->enableQos(Z)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 385
    :catch_0
    move-exception v0

    .line 386
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 387
    const/4 v1, 0x0

    return v1
.end method

.method public enableRps(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "iface"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .line 311
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/net/IOemNetd;->enableRps(Ljava/lang/String;Z)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 314
    :catch_0
    move-exception v0

    .line 315
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 316
    const/4 v1, 0x0

    return v1
.end method

.method public enableWmmer(Z)Z
    .locals 3
    .param p1, "enabled"    # Z

    .line 87
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->enableWmmer(Z)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 92
    const/4 v1, 0x0

    return v1
.end method

.method filterExtendEvent(ILjava/lang/String;[Ljava/lang/String;)Z
    .locals 1
    .param p1, "code"    # I
    .param p2, "raw"    # Ljava/lang/String;
    .param p3, "cooked"    # [Ljava/lang/String;

    .line 325
    const/4 v0, 0x0

    return v0
.end method

.method public getMiuiSlmVoipUdpAddress(I)J
    .locals 3
    .param p1, "uid"    # I

    .line 442
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->getMiuiSlmVoipUdpAddress(I)J

    move-result-wide v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    .line 445
    :catch_0
    move-exception v0

    .line 446
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 447
    const-wide/16 v1, 0x0

    return-wide v1
.end method

.method public getMiuiSlmVoipUdpPort(I)I
    .locals 3
    .param p1, "uid"    # I

    .line 452
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->getMiuiSlmVoipUdpPort(I)I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 455
    :catch_0
    move-exception v0

    .line 456
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 457
    const/4 v1, 0x0

    return v1
.end method

.method public getShareStats(I)J
    .locals 3
    .param p1, "type"    # I

    .line 621
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->getShareStats(I)J

    move-result-wide v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    .line 622
    :catch_0
    move-exception v0

    .line 623
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 624
    const-wide/16 v1, -0x1

    return-wide v1
.end method

.method public listenUidDataActivity(IIIIZ)Z
    .locals 7
    .param p1, "protocol"    # I
    .param p2, "uid"    # I
    .param p3, "type"    # I
    .param p4, "timeout"    # I
    .param p5, "listen"    # Z

    .line 148
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/android/internal/net/IOemNetd;->listenUidDataActivity(IIIIZ)Z

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    .local v1, "res":Z
    nop

    .line 155
    if-eqz p5, :cond_0

    .line 156
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mListenedIdleTimerType:Ljava/util/Set;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 158
    :cond_0
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mListenedIdleTimerType:Ljava/util/Set;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 160
    :goto_0
    return v0

    .line 151
    .end local v1    # "res":Z
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 153
    return v0
.end method

.method miuiNotifyInterfaceClassActivity(IZJIZ)Z
    .locals 8
    .param p1, "type"    # I
    .param p2, "isActive"    # Z
    .param p3, "tsNanos"    # J
    .param p5, "uid"    # I
    .param p6, "fromRadio"    # Z

    .line 330
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mListenedIdleTimerType:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 331
    return v1

    .line 334
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->convertTypeToLable(I)Ljava/lang/String;

    move-result-object v0

    .line 335
    .local v0, "lable":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 336
    return v1

    .line 338
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <====> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NetworkManagement"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mObserver:Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;

    if-eqz v2, :cond_2

    .line 341
    move-object v3, v0

    move v4, p5

    move v5, p2

    move-wide v6, p3

    invoke-interface/range {v2 .. v7}, Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;->uidDataActivityChanged(Ljava/lang/String;IZJ)V

    .line 344
    :cond_2
    const/4 v1, 0x1

    return v1
.end method

.method public modifySuspendBaseTime()V
    .locals 9

    .line 283
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :try_start_0
    const-string/jumbo v0, "suspend_control"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 286
    .local v0, "mBinder":Landroid/os/IBinder;
    const-string v1, "android.system.suspend.ISuspendControlService$Stub"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 287
    .local v1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "asInterface"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, Landroid/os/IBinder;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 288
    .local v3, "asInterface":Ljava/lang/reflect/Method;
    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 289
    .local v4, "ISuspendControlObj":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v7, "modifySuspendTime"

    new-array v8, v6, [Ljava/lang/Class;

    invoke-virtual {v5, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 290
    .local v5, "modifySuspendTime":Ljava/lang/reflect/Method;
    if-eqz v5, :cond_0

    .line 291
    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v5, v4, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    :cond_0
    const-string v6, "modify Suspend Base Time"

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    nop

    .end local v0    # "mBinder":Landroid/os/IBinder;
    .end local v1    # "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "asInterface":Ljava/lang/reflect/Method;
    .end local v4    # "ISuspendControlObj":Ljava/lang/Object;
    .end local v5    # "modifySuspendTime":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 293
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 295
    const-string v1, "SuspendControlService reflection Exception"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setCurrentNetworkState(I)Z
    .locals 3
    .param p1, "state"    # I

    .line 300
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->setCurrentNetworkState(III)V

    .line 303
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->setCurrentNetworkState(I)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 304
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 306
    const/4 v1, 0x0

    return v1
.end method

.method public setLimit(ZJ)Z
    .locals 3
    .param p1, "enabled"    # Z
    .param p2, "rate"    # J

    .line 127
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/net/IOemNetd;->setLimit(ZJ)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 132
    const/4 v1, 0x0

    return v1
.end method

.method public setMiuiFirewallRule(Ljava/lang/String;III)Z
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "rule"    # I
    .param p4, "type"    # I

    .line 197
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    move-object v3, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v3 .. v9}, Lcom/android/server/net/MiuiNetworkManagementService;->setMiuiFirewallRule(Ljava/lang/String;IIIII)V

    .line 200
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/internal/net/IOemNetd;->setMiuiFirewallRule(Ljava/lang/String;III)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 203
    const/4 v1, 0x0

    return v1
.end method

.method public setMiuiSlmBpfUid(I)V
    .locals 3
    .param p1, "uid"    # I

    .line 432
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1}, Lcom/android/internal/net/IOemNetd;->setMiuiSlmBpfUid(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    goto :goto_0

    .line 436
    :catch_0
    move-exception v0

    .line 437
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 439
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setMobileTrafficLimit(ZJ)Z
    .locals 3
    .param p1, "enabled"    # Z
    .param p2, "rate"    # J

    .line 412
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/net/IOemNetd;->setMobileTrafficLimit(ZJ)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 417
    const/4 v1, 0x0

    return v1
.end method

.method public setNetworkEventObserver(Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;)V
    .locals 0
    .param p1, "observer"    # Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;

    .line 321
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mObserver:Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;

    .line 322
    return-void
.end method

.method protected setOemNetd(Landroid/os/IBinder;)V
    .locals 2
    .param p1, "ib"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 79
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mBinder:Landroid/os/IBinder;

    .line 81
    invoke-static {p1}, Lcom/android/internal/net/IOemNetd$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/net/IOemNetd;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    .line 82
    new-instance v0, Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;-><init>(Lcom/android/server/net/MiuiNetworkManagementService;Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener-IA;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mListenter:Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;

    .line 83
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v1, v0}, Lcom/android/internal/net/IOemNetd;->registerOemUnsolicitedEventListener(Lcom/android/internal/net/IOemNetdUnsolicitedEventListener;)V

    .line 84
    return-void
.end method

.method public setPidForPackage(Ljava/lang/String;II)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "pid"    # I
    .param p3, "uid"    # I

    .line 188
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/net/IOemNetd;->setPidForPackage(Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    goto :goto_0

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 194
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public setQos(IIIZ)Z
    .locals 3
    .param p1, "protocol"    # I
    .param p2, "uid"    # I
    .param p3, "tos"    # I
    .param p4, "add"    # Z

    .line 392
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/internal/net/IOemNetd;->setQos(IIIZ)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 395
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 397
    const/4 v1, 0x0

    return v1
.end method

.method public updateAurogonUidRule(IZ)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "allow"    # Z

    .line 472
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    nop

    .line 480
    nop

    .line 481
    return-void
.end method

.method public updateIface(Ljava/lang/String;)Z
    .locals 4
    .param p1, "iface"    # Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v1, p1}, Lcom/android/internal/net/IOemNetd;->updateIface(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    .local v1, "res":Z
    nop

    .line 172
    const-string/jumbo v2, "wlan"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 173
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mListenedIdleTimerType:Ljava/util/Set;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_0
    return v0

    .line 168
    .end local v1    # "res":Z
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 170
    return v0
.end method

.method public updateWmm(II)Z
    .locals 3
    .param p1, "uid"    # I
    .param p2, "wmm"    # I

    .line 107
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/net/IOemNetd;->updateWmm(II)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 112
    const/4 v1, 0x0

    return v1
.end method

.method public whiteListUid(IZ)Z
    .locals 3
    .param p1, "uid"    # I
    .param p2, "add"    # Z

    .line 117
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/net/IOemNetd;->whiteListUid(IZ)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 122
    const/4 v1, 0x0

    return v1
.end method

.method public whiteListUidForMobileTraffic(IZ)Z
    .locals 3
    .param p1, "uid"    # I
    .param p2, "add"    # Z

    .line 422
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.CONNECTIVITY_INTERNAL"

    const-string v2, "NetworkManagement"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService;->mNetd:Lcom/android/internal/net/IOemNetd;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/net/IOemNetd;->whiteListUidForMobileTraffic(IZ)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 425
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 427
    const/4 v1, 0x0

    return v1
.end method
