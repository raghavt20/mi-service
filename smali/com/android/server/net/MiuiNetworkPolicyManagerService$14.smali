.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;
.super Landroid/content/BroadcastReceiver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 1143
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1146
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1147
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mWifiStateReceiver onReceive: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicy"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    .line 1149
    const-string v1, "networkInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 1150
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v4

    .line 1151
    .local v4, "wasConnected":Z
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const/4 v6, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_0

    move v3, v6

    :cond_0
    invoke-static {v5, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1152
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "wasConnected = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mWifiConnected = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mNetworkPriorityMode ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1154
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eq v3, v4, :cond_4

    .line 1155
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menablePowerSave(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1156
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1157
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSupport(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmInterfaceName(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->updateIface(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmInterfaceName(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V

    .line 1158
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mcheckRouterMTK(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v5

    invoke-static {v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmIsMtkRouter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1159
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mgetRouterModel(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmConnectedApModel(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V

    .line 1160
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "router is mtk: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmIsMtkRouter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1162
    :cond_1
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableWmmer(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 1163
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mnetworkPriorityMode(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)I

    move-result v2

    .line 1164
    .local v2, "networkPriority":I
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$misLimitterEnabled(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1165
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWifiConnected(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v2

    goto :goto_0

    :cond_2
    const/16 v5, 0xff

    :goto_0
    invoke-static {v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableNetworkPriority(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V

    goto :goto_1

    .line 1168
    .end local v1    # "netInfo":Landroid/net/NetworkInfo;
    .end local v2    # "networkPriority":I
    .end local v4    # "wasConnected":Z
    :cond_3
    const-string v1, "android.net.conn.NETWORK_CONDITIONS_MEASURED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1169
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmIsCaptivePortal(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v1

    .line 1170
    .local v1, "wasCaptivePortal":Z
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const-string v5, "extra_is_captive_portal"

    invoke-virtual {p2, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v4, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmIsCaptivePortal(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 1171
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "network was: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " captive portal, and now is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmIsCaptivePortal(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmIsCaptivePortal(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v2

    if-eq v1, v2, :cond_5

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmWmmerEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1173
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mupdateRuleGlobal(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    goto :goto_2

    .line 1168
    .end local v1    # "wasCaptivePortal":Z
    :cond_4
    :goto_1
    nop

    .line 1176
    :cond_5
    :goto_2
    return-void
.end method
