class com.android.server.net.MiuiNetworkPolicyManagerService$5 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerTrustMiuiAddDnsObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 945 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 948 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
if ( p2 != null) { // if-eqz p2, :cond_1
	 final String v1 = "cloud_trust_miui_add_dns"; // const-string v1, "cloud_trust_miui_add_dns"
	 android.provider.Settings$System .getUriFor ( v1 );
	 v2 = 	 (( android.net.Uri ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 950 */
		 v2 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v2 );
		 (( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 android.provider.Settings$System .getString ( v2,v1 );
		 /* .line 952 */
		 /* .local v1, "trustMiuiAddDns":Ljava/lang/String; */
		 /* const-string/jumbo v2, "v1" */
		 v2 = 		 (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 final String v2 = "1"; // const-string v2, "1"
		 } // :cond_0
		 final String v2 = "0"; // const-string v2, "0"
		 /* .line 955 */
		 /* .local v2, "sysProVal":Ljava/lang/String; */
	 } // :goto_0
	 try { // :try_start_0
		 final String v3 = "persist.device_config.netd_native.trust_miui_add_dns"; // const-string v3, "persist.device_config.netd_native.trust_miui_add_dns"
		 android.os.SystemProperties .set ( v3,v2 );
		 /* .line 956 */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v4, "trust miui add dns: " */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v0,v3 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 960 */
		 /* .line 957 */
		 /* :catch_0 */
		 /* move-exception v3 */
		 /* .line 958 */
		 /* .local v3, "e":Ljava/lang/Exception; */
		 /* new-instance v4, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v5, "set system properties error:" */
		 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .e ( v0,v4 );
		 /* .line 959 */
		 return;
		 /* .line 962 */
	 } // .end local v1 # "trustMiuiAddDns":Ljava/lang/String;
} // .end local v2 # "sysProVal":Ljava/lang/String;
} // .end local v3 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
return;
} // .end method
