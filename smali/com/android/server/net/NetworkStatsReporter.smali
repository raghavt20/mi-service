.class public Lcom/android/server/net/NetworkStatsReporter;
.super Ljava/lang/Object;
.source "NetworkStatsReporter.java"

# interfaces
.implements Landroid/app/AlarmManager$OnAlarmListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
    }
.end annotation


# static fields
.field private static final AlARM_INTERVAL:J = 0x2932e00L

.field private static final DEVICE_REGION:Ljava/lang/String;

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final HOUR_IN_MILLIS:J = 0x36ee80L

.field private static final MOBILE_RX_BYTES:Ljava/lang/String; = "mobile_rx_bytes"

.field private static final MOBILE_TOTAL_BYTES:Ljava/lang/String; = "mobile_total_bytes"

.field private static final MOBILE_TX_BYTES:Ljava/lang/String; = "mobile_tx_bytes"

.field private static final MSG_TIMER:I = 0x0

.field private static final NETWORK_RX_BYTES:Ljava/lang/String; = "network_rx_bytes"

.field private static final NETWORK_STATS_EVENT_NAME:Ljava/lang/String; = "background_network_statistics"

.field private static final NETWORK_TOTAL_BYTES:Ljava/lang/String; = "network_total_bytes"

.field private static final NETWORK_TX_BYTES:Ljava/lang/String; = "network_tx_bytes"

.field private static final ONETRACK_APP_ID:Ljava/lang/String; = "31000000072"

.field private static final ONETRACK_PACKAGE:Ljava/lang/String; = "com.android.server.net"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "background_package"

.field private static final TAG:Ljava/lang/String;

.field private static final WIFI_RX_BYTES:Ljava/lang/String; = "wifi_rx_bytes"

.field private static final WIFI_TOTAL_BYTES:Ljava/lang/String; = "wifi_total_bytes"

.field private static final WIFI_TX_BYTES:Ljava/lang/String; = "wifi_tx_bytes"

.field private static sSelf:Lcom/android/server/net/NetworkStatsReporter;


# instance fields
.field private mAlarmManager:Landroid/app/AlarmManager;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerCallback:Landroid/os/Handler$Callback;

.field private mMobileTemplate:Landroid/net/NetworkTemplate;

.field mNetworkStatsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;",
            ">;"
        }
    .end annotation
.end field

.field private mStartAlarmTime:J

.field private mStatsService:Landroid/net/INetworkStatsService;

.field private mWifiTemplate:Landroid/net/NetworkTemplate;


# direct methods
.method static bridge synthetic -$$Nest$mclear(Lcom/android/server/net/NetworkStatsReporter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->clear()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreportNetworkStats(Lcom/android/server/net/NetworkStatsReporter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->reportNetworkStats()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartAlarm(Lcom/android/server/net/NetworkStatsReporter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->startAlarm()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateNetworkStats(Lcom/android/server/net/NetworkStatsReporter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->updateNetworkStats()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 27
    const-class v0, Lcom/android/server/net/NetworkStatsReporter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    .line 28
    const-string v0, "ro.miui.region"

    const-string v1, "CN"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/NetworkStatsReporter;->DEVICE_REGION:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mStartAlarmTime:J

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mNetworkStatsMap:Ljava/util/HashMap;

    .line 233
    new-instance v0, Lcom/android/server/net/NetworkStatsReporter$1;

    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkStatsReporter$1;-><init>(Lcom/android/server/net/NetworkStatsReporter;)V

    iput-object v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mHandlerCallback:Landroid/os/Handler$Callback;

    .line 59
    if-nez p1, :cond_0

    .line 60
    sget-object v0, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    const-string v1, "context is null, NetworkStatsReporter init failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    return-void

    .line 64
    :cond_0
    iput-object p1, p0, Lcom/android/server/net/NetworkStatsReporter;->mContext:Landroid/content/Context;

    .line 65
    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 67
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/net/NetworkStatsReporter;->mHandlerCallback:Landroid/os/Handler$Callback;

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/android/server/net/NetworkStatsReporter;->mHandler:Landroid/os/Handler;

    .line 68
    nop

    .line 69
    const-string v1, "netstats"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    .line 68
    invoke-static {v1}, Landroid/net/INetworkStatsService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkStatsService;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/net/NetworkStatsReporter;->mStatsService:Landroid/net/INetworkStatsService;

    .line 70
    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateWifiWildcard()Landroid/net/NetworkTemplate;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/net/NetworkStatsReporter;->mWifiTemplate:Landroid/net/NetworkTemplate;

    .line 71
    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateMobileWildcard()Landroid/net/NetworkTemplate;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/net/NetworkStatsReporter;->mMobileTemplate:Landroid/net/NetworkTemplate;

    .line 72
    iget-object v1, p0, Lcom/android/server/net/NetworkStatsReporter;->mContext:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    iput-object v1, p0, Lcom/android/server/net/NetworkStatsReporter;->mAlarmManager:Landroid/app/AlarmManager;

    .line 74
    invoke-direct {p0}, Lcom/android/server/net/NetworkStatsReporter;->startAlarm()V

    .line 75
    return-void
.end method

.method private clear()V
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mNetworkStatsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 208
    return-void
.end method

.method private getBackgroundNetworkStats(Landroid/net/NetworkTemplate;Landroid/net/INetworkStatsSession;)V
    .locals 11
    .param p1, "networkTemplate"    # Landroid/net/NetworkTemplate;
    .param p2, "networkStatsSession"    # Landroid/net/INetworkStatsSession;

    .line 110
    if-nez p1, :cond_0

    .line 111
    sget-object v0, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    const-string v1, "networkTemplate is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    return-void

    .line 115
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 116
    .local v0, "now":J
    const/4 v9, 0x0

    .line 118
    .local v9, "networkStats":Landroid/net/NetworkStats;
    :try_start_0
    iget-wide v4, p0, Lcom/android/server/net/NetworkStatsReporter;->mStartAlarmTime:J

    const/4 v8, 0x0

    move-object v2, p2

    move-object v3, p1

    move-wide v6, v0

    invoke-interface/range {v2 .. v8}, Landroid/net/INetworkStatsSession;->getSummaryForAllUid(Landroid/net/NetworkTemplate;JJZ)Landroid/net/NetworkStats;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    .end local v9    # "networkStats":Landroid/net/NetworkStats;
    .local v2, "networkStats":Landroid/net/NetworkStats;
    nop

    .line 124
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/net/NetworkStats;->size()I

    move-result v3

    if-nez v3, :cond_1

    goto :goto_3

    .line 128
    :cond_1
    new-instance v3, Landroid/net/NetworkStats$Entry;

    invoke-direct {v3}, Landroid/net/NetworkStats$Entry;-><init>()V

    .line 129
    .local v3, "entry":Landroid/net/NetworkStats$Entry;
    invoke-virtual {v2}, Landroid/net/NetworkStats;->size()I

    move-result v4

    .line 130
    .local v4, "size":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v4, :cond_6

    .line 131
    invoke-virtual {v2, v5, v3}, Landroid/net/NetworkStats;->getValues(ILandroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    move-result-object v3

    .line 132
    if-eqz v3, :cond_5

    iget v6, v3, Landroid/net/NetworkStats$Entry;->set:I

    if-eqz v6, :cond_2

    .line 133
    goto :goto_2

    .line 136
    :cond_2
    iget v6, v3, Landroid/net/NetworkStats$Entry;->uid:I

    .line 137
    .local v6, "uid":I
    const/16 v7, 0x3e8

    if-gt v6, v7, :cond_3

    .line 138
    goto :goto_2

    .line 140
    :cond_3
    invoke-direct {p0, v6}, Lcom/android/server/net/NetworkStatsReporter;->getPackageNameByUid(I)Ljava/lang/String;

    move-result-object v7

    .line 142
    .local v7, "packageName":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/net/NetworkStatsReporter;->mNetworkStatsMap:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 143
    iget-object v8, p0, Lcom/android/server/net/NetworkStatsReporter;->mNetworkStatsMap:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;

    .local v8, "backgroundNetworkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
    goto :goto_1

    .line 145
    .end local v8    # "backgroundNetworkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
    :cond_4
    new-instance v8, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;

    invoke-direct {v8, p0, v6, v7}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;-><init>(Lcom/android/server/net/NetworkStatsReporter;ILjava/lang/String;)V

    .line 147
    .restart local v8    # "backgroundNetworkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
    :goto_1
    invoke-virtual {v8, p1, v3}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->updateBytes(Landroid/net/NetworkTemplate;Landroid/net/NetworkStats$Entry;)V

    .line 148
    iget-object v9, p0, Lcom/android/server/net/NetworkStatsReporter;->mNetworkStatsMap:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    .end local v6    # "uid":I
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v8    # "backgroundNetworkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
    :cond_5
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 150
    .end local v5    # "i":I
    :cond_6
    return-void

    .line 125
    .end local v3    # "entry":Landroid/net/NetworkStats$Entry;
    .end local v4    # "size":I
    :cond_7
    :goto_3
    return-void

    .line 120
    .end local v2    # "networkStats":Landroid/net/NetworkStats;
    .restart local v9    # "networkStats":Landroid/net/NetworkStats;
    :catch_0
    move-exception v2

    .line 121
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSummaryForAllUid exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    return-void
.end method

.method private getPackageNameByUid(I)Ljava/lang/String;
    .locals 2
    .param p1, "uid"    # I

    .line 212
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "pkgs":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 214
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .local v1, "packageName":Ljava/lang/String;
    goto :goto_0

    .line 216
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 218
    .restart local v1    # "packageName":Ljava/lang/String;
    :goto_0
    return-object v1
.end method

.method public static make(Landroid/content/Context;)Lcom/android/server/net/NetworkStatsReporter;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 78
    sget-object v0, Lcom/android/server/net/NetworkStatsReporter;->sSelf:Lcom/android/server/net/NetworkStatsReporter;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/android/server/net/NetworkStatsReporter;

    invoke-direct {v0, p0}, Lcom/android/server/net/NetworkStatsReporter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/net/NetworkStatsReporter;->sSelf:Lcom/android/server/net/NetworkStatsReporter;

    .line 82
    :cond_0
    sget-object v0, Lcom/android/server/net/NetworkStatsReporter;->sSelf:Lcom/android/server/net/NetworkStatsReporter;

    return-object v0
.end method

.method private reportNetworkStats()V
    .locals 7

    .line 153
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mNetworkStatsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 154
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;

    .line 155
    .local v2, "networkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmPackageName(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/net/NetworkStatsReporter;->isNumericOrNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    goto :goto_0

    .line 159
    :cond_1
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 160
    .local v3, "params":Landroid/os/Bundle;
    const-string v4, "background_package"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmPackageName(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string/jumbo v4, "wifi_tx_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmWifiTxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 162
    const-string/jumbo v4, "wifi_rx_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmWifiRxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 163
    const-string/jumbo v4, "wifi_total_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmWifiTotalBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 164
    const-string v4, "mobile_tx_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmMobileTxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 165
    const-string v4, "mobile_rx_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmMobileRxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 166
    const-string v4, "mobile_total_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmMobileTotalBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 167
    const-string v4, "network_tx_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmTotalTxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 168
    const-string v4, "network_rx_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmTotalRxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 169
    const-string v4, "network_total_bytes"

    invoke-static {v2}, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->-$$Nest$fgetmTotalBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 170
    iget-object v4, p0, Lcom/android/server/net/NetworkStatsReporter;->mContext:Landroid/content/Context;

    const-string v5, "background_network_statistics"

    invoke-direct {p0, v4, v5, v3}, Lcom/android/server/net/NetworkStatsReporter;->reportNetworkStatsEvent(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 171
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;>;"
    .end local v2    # "networkStats":Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
    .end local v3    # "params":Landroid/os/Bundle;
    goto/16 :goto_0

    .line 172
    :cond_2
    return-void
.end method

.method private reportNetworkStatsEvent(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventName"    # Ljava/lang/String;
    .param p3, "params"    # Landroid/os/Bundle;

    .line 175
    if-eqz p1, :cond_1

    sget-object v0, Lcom/android/server/net/NetworkStatsReporter;->DEVICE_REGION:Ljava/lang/String;

    const-string v1, "CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 179
    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 180
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const-string v1, "APP_ID"

    const-string v2, "31000000072"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    const-string v1, "PACKAGE"

    const-string v2, "com.android.server.net"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    const-string v1, "EVENT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 185
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 186
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    nop

    .end local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 190
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 176
    :cond_1
    :goto_1
    return-void
.end method

.method private startAlarm()V
    .locals 9

    .line 193
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mStartAlarmTime:J

    .line 194
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsReporter;->mAlarmManager:Landroid/app/AlarmManager;

    const/4 v3, 0x2

    .line 195
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/32 v4, 0x2932e00

    add-long/2addr v4, v0

    sget-object v6, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/server/net/NetworkStatsReporter;->mHandler:Landroid/os/Handler;

    .line 194
    move-object v7, p0

    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->set(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 197
    return-void
.end method

.method private updateNetworkStats()V
    .locals 5

    .line 86
    const-string v0, "open session exception:"

    const/4 v1, 0x0

    .line 88
    .local v1, "networkStatsSession":Landroid/net/INetworkStatsSession;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsReporter;->mStatsService:Landroid/net/INetworkStatsService;

    invoke-interface {v2}, Landroid/net/INetworkStatsService;->openSession()Landroid/net/INetworkStatsSession;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v1, v2

    .line 92
    nop

    .line 93
    if-nez v1, :cond_0

    .line 94
    sget-object v0, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    const-string v2, "networkStatsSession is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    return-void

    .line 98
    :cond_0
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsReporter;->mMobileTemplate:Landroid/net/NetworkTemplate;

    invoke-direct {p0, v2, v1}, Lcom/android/server/net/NetworkStatsReporter;->getBackgroundNetworkStats(Landroid/net/NetworkTemplate;Landroid/net/INetworkStatsSession;)V

    .line 99
    iget-object v2, p0, Lcom/android/server/net/NetworkStatsReporter;->mWifiTemplate:Landroid/net/NetworkTemplate;

    invoke-direct {p0, v2, v1}, Lcom/android/server/net/NetworkStatsReporter;->getBackgroundNetworkStats(Landroid/net/NetworkTemplate;Landroid/net/INetworkStatsSession;)V

    .line 102
    :try_start_1
    invoke-interface {v1}, Landroid/net/INetworkStatsSession;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 105
    goto :goto_0

    .line 103
    :catch_0
    move-exception v2

    .line 104
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 89
    :catch_1
    move-exception v2

    .line 90
    .restart local v2    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/android/server/net/NetworkStatsReporter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    return-void
.end method


# virtual methods
.method public isNumericOrNull(Ljava/lang/String;)Z
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .line 222
    if-eqz p1, :cond_1

    .line 223
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 224
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    const/4 v1, 0x0

    return v1

    .line 223
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onAlarm()V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsReporter;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 202
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 204
    :cond_0
    return-void
.end method
