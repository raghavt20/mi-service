.class Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;
.super Landroid/content/BroadcastReceiver;
.source "MiuiNetworkPolicyQosUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    .line 506
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 509
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 510
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    return-void

    .line 511
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive action"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicyQosUtils"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 513
    const-string v1, "networkInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 514
    .local v1, "nwInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    if-nez v3, :cond_3

    .line 515
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    const-string v5, "mIsMobileDataOn="

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 516
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fputmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V

    .line 517
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 518
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 519
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fputmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V

    .line 520
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmQosInfo(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmQosInfo(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getQosStatus()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 522
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmQosInfo(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getUid()I

    move-result v3

    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmQosInfo(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getProtocol()I

    move-result v5

    invoke-static {v2, v4, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$msetQos(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZII)V

    goto :goto_0

    .line 526
    .end local v1    # "nwInfo":Landroid/net/NetworkInfo;
    :cond_2
    const-string v1, "com.android.phone.intent.action.CLOUD_MEETING_LIST_DONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 527
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$mprocessMeetingListChanged(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V

    goto :goto_1

    .line 526
    :cond_3
    :goto_0
    nop

    .line 529
    :goto_1
    return-void
.end method
