.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;
.super Landroid/content/BroadcastReceiver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 1656
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1659
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1660
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1661
    .local v1, "data":Landroid/net/Uri;
    if-nez v1, :cond_0

    .line 1662
    return-void

    .line 1664
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    .line 1665
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1666
    return-void

    .line 1669
    :cond_1
    const-string v3, "android.intent.extra.UID"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1670
    .local v3, "uid":I
    if-ne v3, v4, :cond_2

    .line 1671
    return-void

    .line 1674
    :cond_2
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v5, "MiuiNetworkPolicy"

    if-eqz v4, :cond_9

    .line 1675
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_PACKAGE_ADDED uid = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1676
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmUnRestrictAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1677
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmUnRestrictApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1679
    :cond_3
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1680
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmLowLatencyApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1682
    :cond_4
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNeedRestrictPowerSaveAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1683
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNeedRestrictPowerSaveApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1685
    :cond_5
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$smisMobileLatencyAllowed()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1686
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1687
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileLowLatencyApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1690
    :cond_6
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmQosUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    move-result-object v4

    const/4 v5, 0x1

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmQosUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateAppPN(Ljava/lang/String;IZ)V

    .line 1691
    :cond_7
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmAppBuckets(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    move-result-object v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmAppBuckets(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppPN(Ljava/lang/String;IZ)V

    .line 1692
    :cond_8
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileTcUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    move-result-object v4

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileTcUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateAppPN(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    .line 1693
    :cond_9
    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1694
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_PACKAGE_REMOVED uid = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1695
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmUnRestrictAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1696
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmUnRestrictApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1698
    :cond_a
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1699
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmLowLatencyApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1701
    :cond_b
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNeedRestrictPowerSaveAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1702
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNeedRestrictPowerSaveApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1704
    :cond_c
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$smisMobileLatencyAllowed()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1705
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    if-eqz v4, :cond_d

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileLowLatencyAppsPN(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1706
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileLowLatencyApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1709
    :cond_d
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmQosUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    move-result-object v4

    const/4 v5, 0x0

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmQosUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateAppPN(Ljava/lang/String;IZ)V

    .line 1710
    :cond_e
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmAppBuckets(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    move-result-object v4

    if-eqz v4, :cond_f

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmAppBuckets(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppPN(Ljava/lang/String;IZ)V

    .line 1711
    :cond_f
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileTcUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    move-result-object v4

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmMobileTcUtils(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateAppPN(Ljava/lang/String;IZ)V

    .line 1713
    :cond_10
    :goto_0
    return-void
.end method
