class com.android.server.net.MiuiNetworkPolicyTrafficLimit$6 implements java.lang.Runnable {
	 /* .source "MiuiNetworkPolicyTrafficLimit.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTrafficLimit(Z)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyTrafficLimit this$0; //synthetic
final Boolean val$enabled; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyTrafficLimit$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 311 */
this.this$0 = p1;
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->val$enabled:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 314 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmNetMgrService ( v0 );
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->val$enabled:Z */
v2 = this.this$0;
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmIface ( v2 );
v0 = (( com.android.server.net.MiuiNetworkManagementService ) v0 ).enableMobileTrafficLimit ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/net/MiuiNetworkManagementService;->enableMobileTrafficLimit(ZLjava/lang/String;)Z
/* .line 315 */
/* .local v0, "rst":Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 316 */
v1 = this.this$0;
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;->val$enabled:Z */
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mupdateTrafficLimitStatus ( v1,v2 );
/* .line 318 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmMobileTcDropEnabled ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmNetMgrService ( v1 );
	 int v2 = 1; // const/4 v2, 0x1
	 /* const-wide/16 v3, 0x0 */
	 v1 = 	 (( com.android.server.net.MiuiNetworkManagementService ) v1 ).setMobileTrafficLimit ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->setMobileTrafficLimit(ZJ)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 319 */
		 v1 = this.this$0;
		 int v2 = 3; // const/4 v2, 0x3
		 com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mupdateTrafficLimitStatus ( v1,v2 );
		 /* .line 322 */
	 } // :cond_0
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fputmProcessEnableLimit ( v1,v2 );
	 /* .line 323 */
	 v1 = this.this$0;
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "enableMobileTrafficLimit rst="; // const-string v3, "enableMobileTrafficLimit rst="
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mlog ( v1,v2 );
	 /* .line 324 */
	 return;
} // .end method
