class com.android.server.net.MiuiNetworkPolicyTrafficLimit$4 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyTrafficLimit.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->registerMobileTrafficControllerChangedObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyTrafficLimit this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyTrafficLimit$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 269 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 272 */
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mgetUserMobileTcMode ( v0 );
/* .line 273 */
/* .local v0, "tcMode":I */
v1 = this.this$0;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "tcMode = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " mLastMobileTcMode ="; // const-string v3, " mLastMobileTcMode ="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmLastMobileTcMode ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " mIsMobileNwOn="; // const-string v3, " mIsMobileNwOn="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmIsMobileNwOn ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mlog ( v1,v2 );
/* .line 275 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmLastMobileTcMode ( v1 );
/* if-eq v0, v1, :cond_0 */
/* .line 276 */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$menableMobileTcMode ( v1,v0 );
/* .line 278 */
} // :cond_0
return;
} // .end method
