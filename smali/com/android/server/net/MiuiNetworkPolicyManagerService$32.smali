.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;
.super Landroid/content/BroadcastReceiver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 2097
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 2100
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2101
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.miui.powerkeeper_sleep_changed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const-string v3, "MiuiNetworkPolicySleepMode"

    if-eqz v1, :cond_1

    .line 2102
    const-string/jumbo v1, "state"

    const/4 v4, -0x1

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2103
    .local v1, "state":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "broadcastReceiver state is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2104
    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeWhitelistUids(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    .line 2105
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2106
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 2107
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$msetSleepModeWhitelistUidRules(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 2108
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableSleepModeChain(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    goto :goto_0

    .line 2109
    :cond_0
    if-eq v1, v3, :cond_2

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2110
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 2111
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mclearSleepModeWhitelistUidRules(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 2112
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableSleepModeChain(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    goto :goto_0

    .line 2114
    .end local v1    # "state":I
    :cond_1
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2115
    const-string v1, "ACTION_SCREEN_ON"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2116
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2117
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 2118
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mclearSleepModeWhitelistUidRules(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 2119
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableSleepModeChain(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    goto :goto_1

    .line 2114
    :cond_2
    :goto_0
    nop

    .line 2122
    :cond_3
    :goto_1
    return-void
.end method
