.class public Lcom/android/server/net/NetworkTimeUpdateServiceImpl;
.super Ljava/lang/Object;
.source "NetworkTimeUpdateServiceImpl.java"

# interfaces
.implements Lcom/android/server/net/NetworkTimeUpdateServiceStub;


# static fields
.field private static CN_NTP_SERVER:Ljava/lang/String; = null

.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "NetworkTimeUpdateService"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDefaultNtpServer:Ljava/lang/String;

.field private mNtpServers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTime:Landroid/util/NtpTrustedTime;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    const-string v0, "pool.ntp.org"

    sput-object v0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->CN_NTP_SERVER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mNtpServers:Ljava/util/ArrayList;

    return-void
.end method

.method private initDefaultNtpServer(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 57
    if-nez p1, :cond_0

    return-void

    .line 59
    :cond_0
    iput-object p1, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mContext:Landroid/content/Context;

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 61
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 62
    .local v1, "res":Landroid/content/res/Resources;
    const-string/jumbo v2, "time.android.com"

    .line 64
    .local v2, "defaultServer":Ljava/lang/String;
    const-string v3, "ntp_server"

    invoke-static {v0, v3}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 66
    .local v3, "secureServer":Ljava/lang/String;
    sget-boolean v4, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v4, :cond_2

    .line 67
    if-eqz v3, :cond_1

    move-object v4, v3

    goto :goto_0

    :cond_1
    const-string/jumbo v4, "time.android.com"

    :goto_0
    iput-object v4, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mDefaultNtpServer:Ljava/lang/String;

    goto :goto_1

    .line 69
    :cond_2
    sget-object v4, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->CN_NTP_SERVER:Ljava/lang/String;

    iput-object v4, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mDefaultNtpServer:Ljava/lang/String;

    .line 71
    :goto_1
    return-void
.end method

.method private refreshNtpServer(ILandroid/net/Network;)Z
    .locals 5
    .param p1, "tryCounter"    # I
    .param p2, "network"    # Landroid/net/Network;

    .line 81
    const/4 v0, 0x0

    .line 82
    .local v0, "result":Z
    iget-object v1, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mNtpServers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    rem-int v1, p1, v1

    .line 83
    .local v1, "index":I
    iget-object v2, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mNtpServers:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 84
    .local v2, "ntpServer":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "tryCounter = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",ntpServers = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "NetworkTimeUpdateService"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-direct {p0, v2}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->setNtpServer(Ljava/lang/String;)V

    .line 87
    iget-object v3, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mTime:Landroid/util/NtpTrustedTime;

    invoke-virtual {v3, p2}, Landroid/util/NtpTrustedTime;->forceRefresh(Landroid/net/Network;)Z

    move-result v0

    .line 88
    iget-object v3, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mDefaultNtpServer:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->setNtpServer(Ljava/lang/String;)V

    .line 90
    return v0
.end method

.method private setNtpServer(Ljava/lang/String;)V
    .locals 2
    .param p1, "ntpServer"    # Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "ntp_server"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 78
    :cond_0
    return-void
.end method


# virtual methods
.method public initNtpServers(Landroid/content/Context;Landroid/util/NtpTrustedTime;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "trustedTime"    # Landroid/util/NtpTrustedTime;

    .line 30
    iput-object p2, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mTime:Landroid/util/NtpTrustedTime;

    .line 31
    invoke-direct {p0, p1}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->initDefaultNtpServer(Landroid/content/Context;)V

    .line 32
    iget-object v0, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mNtpServers:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mDefaultNtpServer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x11030081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "globalNtpServers":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1103001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "chinaNtpServers":[Ljava/lang/String;
    array-length v2, v0

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_0

    aget-object v5, v0, v4

    .line 38
    .local v5, "ntpServer":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mNtpServers:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    .end local v5    # "ntpServer":Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 40
    :cond_0
    sget-boolean v2, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v2, :cond_1

    .line 41
    array-length v2, v1

    :goto_1
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 42
    .local v4, "ntpServer":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mNtpServers:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    .end local v4    # "ntpServer":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 45
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "the servers are "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mNtpServers:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NetworkTimeUpdateService"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    return-void
.end method

.method public switchNtpServer(ILandroid/util/NtpTrustedTime;Landroid/net/Network;)Z
    .locals 1
    .param p1, "tryCounter"    # I
    .param p2, "trustedTime"    # Landroid/util/NtpTrustedTime;
    .param p3, "network"    # Landroid/net/Network;

    .line 50
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->refreshNtpServer(ILandroid/net/Network;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/android/server/net/NetworkTimeUpdateServiceImpl;->mTime:Landroid/util/NtpTrustedTime;

    invoke-virtual {v0, p3}, Landroid/util/NtpTrustedTime;->forceRefresh(Landroid/net/Network;)Z

    move-result v0

    return v0

    .line 53
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
