.class public Lcom/android/server/net/MiuiNetworkManager;
.super Landroid/net/IMiuiNetworkManager$Stub;
.source "MiuiNetworkManager.java"


# static fields
.field private static sSelf:Lcom/android/server/net/MiuiNetworkManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Landroid/net/IMiuiNetworkManager$Stub;-><init>()V

    return-void
.end method

.method public static final get()Lcom/android/server/net/MiuiNetworkManager;
    .locals 1

    .line 10
    sget-object v0, Lcom/android/server/net/MiuiNetworkManager;->sSelf:Lcom/android/server/net/MiuiNetworkManager;

    if-nez v0, :cond_0

    .line 11
    new-instance v0, Lcom/android/server/net/MiuiNetworkManager;

    invoke-direct {v0}, Lcom/android/server/net/MiuiNetworkManager;-><init>()V

    sput-object v0, Lcom/android/server/net/MiuiNetworkManager;->sSelf:Lcom/android/server/net/MiuiNetworkManager;

    .line 13
    :cond_0
    sget-object v0, Lcom/android/server/net/MiuiNetworkManager;->sSelf:Lcom/android/server/net/MiuiNetworkManager;

    return-object v0
.end method


# virtual methods
.method public getMiuiSlmVoipUdpAddress(I)J
    .locals 2
    .param p1, "uid"    # I

    .line 38
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getMiuiSlmVoipUdpAddress(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMiuiSlmVoipUdpPort(I)I
    .locals 1
    .param p1, "uid"    # I

    .line 43
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getMiuiSlmVoipUdpPort(I)I

    move-result v0

    return v0
.end method

.method public getShareStats(I)J
    .locals 2
    .param p1, "type"    # I

    .line 48
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getShareStats(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public onSleepModeWhitelistChange(IZ)Z
    .locals 1
    .param p1, "appId"    # I
    .param p2, "added"    # Z

    .line 53
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->onSleepModeWhitelistChange(IZ)Z

    move-result v0

    return v0
.end method

.method public setMiuiSlmBpfUid(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 33
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setMiuiSlmBpfUid(I)Z

    move-result v0

    return v0
.end method

.method public setMobileTrafficLimit(ZJ)I
    .locals 1
    .param p1, "enabled"    # Z
    .param p2, "rate"    # J

    .line 28
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setTrafficControllerForMobile(ZJ)I

    move-result v0

    return v0
.end method

.method public setNetworkTrafficPolicy(I)Z
    .locals 1
    .param p1, "mode"    # I

    .line 18
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setNetworkTrafficPolicy(I)Z

    move-result v0

    return v0
.end method

.method public setRpsStatus(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .line 23
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->get()Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setRpsStatus(Z)Z

    move-result v0

    return v0
.end method
