.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;
.super Landroid/content/BroadcastReceiver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 1769
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1772
    const-string v0, "apnType"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1773
    .local v0, "apnType":Ljava/lang/String;
    const-string v1, "default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1774
    return-void

    .line 1776
    :cond_0
    const-class v1, Lcom/android/internal/telephony/PhoneConstants$DataState;

    .line 1777
    const-string/jumbo v2, "state"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1776
    invoke-static {v1, v2}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/PhoneConstants$DataState;

    .line 1778
    .local v1, "state":Lcom/android/internal/telephony/PhoneConstants$DataState;
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmConnectState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    move-result-object v2

    if-ne v2, v1, :cond_1

    .line 1779
    return-void

    .line 1781
    :cond_1
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmConnectState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Lcom/android/internal/telephony/PhoneConstants$DataState;)V

    .line 1782
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mMobileNwReceiver mConnectState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmConnectState(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/internal/telephony/PhoneConstants$DataState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiNetworkPolicy"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1783
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    if-ne v1, v2, :cond_2

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$sfgetmMobileLatencyState()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    :cond_2
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    if-ne v1, v2, :cond_4

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$sfgetmMobileLatencyState()I

    move-result v2

    if-nez v2, :cond_4

    .line 1785
    :cond_3
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1787
    :cond_4
    return-void
.end method
