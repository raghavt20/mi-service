.class public Lcom/android/server/net/NetworkPolicyManagerServiceInjector;
.super Ljava/lang/Object;
.source "NetworkPolicyManagerServiceInjector.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    const-string v0, "NetworkPolicyManagerServiceInjector"

    sput-object v0, Lcom/android/server/net/NetworkPolicyManagerServiceInjector;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkPolicyForNetwork(Landroid/net/NetworkPolicy;)Z
    .locals 2
    .param p0, "policy"    # Landroid/net/NetworkPolicy;

    .line 44
    iget-object v0, p0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    invoke-virtual {v0}, Landroid/net/NetworkTemplate;->getMatchRule()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 45
    const/4 v0, 0x1

    return v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static updateNetworkRules(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Landroid/net/NetworkPolicy;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 19
    .local p0, "networkRules":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/NetworkPolicy;[Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 20
    .local v0, "findWifi":Z
    const/4 v1, 0x0

    .line 21
    .local v1, "findWifiWildCard":Z
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x4

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/NetworkPolicy;

    .line 22
    .local v3, "policy":Landroid/net/NetworkPolicy;
    iget-wide v5, v3, Landroid/net/NetworkPolicy;->limitBytes:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    .line 23
    .local v5, "hasLimit":Z
    :goto_1
    if-nez v5, :cond_1

    iget-boolean v6, v3, Landroid/net/NetworkPolicy;->metered:Z

    if-eqz v6, :cond_3

    .line 24
    :cond_1
    iget-object v6, v3, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    invoke-virtual {v6}, Landroid/net/NetworkTemplate;->getMatchRule()I

    move-result v6

    if-ne v6, v4, :cond_2

    .line 25
    const/4 v1, 0x1

    goto :goto_2

    .line 26
    :cond_2
    iget-object v6, v3, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    invoke-virtual {v6}, Landroid/net/NetworkTemplate;->getMatchRule()I

    move-result v6

    if-ne v6, v4, :cond_3

    .line 27
    const/4 v0, 0x1

    .line 30
    .end local v3    # "policy":Landroid/net/NetworkPolicy;
    .end local v5    # "hasLimit":Z
    :cond_3
    :goto_2
    goto :goto_0

    .line 31
    :cond_4
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    .line 32
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 33
    .local v2, "newNetworkRules":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/NetworkPolicy;[Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/NetworkPolicy;

    .line 34
    .local v5, "policy":Landroid/net/NetworkPolicy;
    iget-object v6, v5, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    invoke-virtual {v6}, Landroid/net/NetworkTemplate;->getMatchRule()I

    move-result v6

    if-eq v6, v4, :cond_5

    .line 35
    invoke-interface {p0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    .end local v5    # "policy":Landroid/net/NetworkPolicy;
    :cond_5
    goto :goto_3

    .line 38
    :cond_6
    invoke-interface {p0}, Ljava/util/Map;->clear()V

    .line 39
    invoke-interface {p0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 41
    .end local v2    # "newNetworkRules":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/NetworkPolicy;[Ljava/lang/String;>;"
    :cond_7
    return-void
.end method
