class com.android.server.net.MiuiNetworkPolicyServiceSupport$1 extends android.app.IUidObserver$Stub {
	 /* .source "MiuiNetworkPolicyServiceSupport.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyServiceSupport; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyServiceSupport this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyServiceSupport$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyServiceSupport; */
/* .line 68 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IUidObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onUidActive ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 83 */
return;
} // .end method
public void onUidCachedChanged ( Integer p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "cached" # Z */
/* .line 92 */
return;
} // .end method
public void onUidGone ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "disabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 78 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyServiceSupport .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyServiceSupport .-$$Nest$fgetmHandler ( v1 );
int v2 = 2; // const/4 v2, 0x2
int v3 = 0; // const/4 v3, 0x0
(( android.os.Handler ) v1 ).obtainMessage ( v2, p1, v3 ); // invoke-virtual {v1, v2, p1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 80 */
return;
} // .end method
public void onUidIdle ( Integer p0, Boolean p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "disabled" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 86 */
return;
} // .end method
public void onUidProcAdjChanged ( Integer p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "adj" # I */
/* .line 89 */
return;
} // .end method
public void onUidStateChanged ( Integer p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "procState" # I */
/* .param p3, "procStateSeq" # J */
/* .param p5, "capability" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 72 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyServiceSupport .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyServiceSupport .-$$Nest$fgetmHandler ( v1 );
int v2 = 1; // const/4 v2, 0x1
(( android.os.Handler ) v1 ).obtainMessage ( v2, p1, p2 ); // invoke-virtual {v1, v2, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 74 */
return;
} // .end method
