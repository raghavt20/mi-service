.class Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;
.super Landroid/database/ContentObserver;
.source "MiuiNetworkPolicyTrafficLimit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->registerMobileTrafficControllerChangedObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 269
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 272
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mgetUserMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)I

    move-result v0

    .line 273
    .local v0, "tcMode":I
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tcMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mLastMobileTcMode ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmLastMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mIsMobileNwOn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmIsMobileNwOn(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmLastMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v1, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$menableMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;I)V

    .line 278
    :cond_0
    return-void
.end method
