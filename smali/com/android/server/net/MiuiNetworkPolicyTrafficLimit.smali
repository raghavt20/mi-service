.class public Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyTrafficLimit.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final IS_QCOM:Z

.field private static final MOBILE_TC_MODE_CLOSED:I = 0x0

.field private static final MOBILE_TC_MODE_DROP:I = 0x1

.field public static final RULE_RESULT_ERROR_EXIST:I = 0x3

.field public static final RULE_RESULT_ERROR_IFACE:I = 0x6

.field public static final RULE_RESULT_ERROR_OFF:I = 0x2

.field public static final RULE_RESULT_ERROR_ON:I = 0x4

.field public static final RULE_RESULT_ERROR_PARAMS:I = 0x5

.field public static final RULE_RESULT_ERROR_PROCESSING_ENABLE_LIMIT:I = 0x7

.field public static final RULE_RESULT_ERROR_PROCESSING_SET_LIMIT:I = 0x8

.field public static final RULE_RESULT_INIT:I = 0x0

.field public static final RULE_RESULT_SUCCESS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MiuiNetworkPolicyTrafficLimit"

.field private static final TRAFFIC_LIMIT_INIT:I = -0x1

.field private static final TRAFFIC_LIMIT_OFF:I = 0x0

.field private static final TRAFFIC_LIMIT_ON:I = 0x1

.field private static final TRAFFIC_LIMIT_RULE_OFF:I = 0x2

.field private static final TRAFFIC_LIMIT_RULE_ON:I = 0x3


# instance fields
.field private mCm:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private mDefaultInputMethod:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mIface:Ljava/lang/String;

.field private mIsMobileNwOn:Z

.field private mLastMobileTcMode:I

.field private mMobileLimitEnabled:Z

.field private mMobileTcDropEnabled:Z

.field private mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

.field private mProcessEnableLimit:Z

.field private mProcessSetLimit:Z

.field final mReceiver:Landroid/content/BroadcastReceiver;

.field private mTrafficLimitMode:I

.field private mWhiteListAppsPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWhiteListAppsUid:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmIface(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsMobileNwOn(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)I
    .locals 0

    iget p0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMobileTcDropEnabled(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileTcDropEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetMgrService(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Lcom/android/server/net/MiuiNetworkManagementService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mDefaultInputMethod:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmProcessEnableLimit(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessEnableLimit:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmProcessSetLimit(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessSetLimit:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$menableMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTcMode(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getDefaultInputMethod()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetUserMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getUserMobileTcMode()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misAllowedToEnableMobileTcMode(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isAllowedToEnableMobileTcMode()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateMobileTcWhiteList(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateMobileTcWhiteList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateTrafficLimitStatus(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateTrafficLimitStatus(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 89
    const-string/jumbo v0, "vendor"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "qcom"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->IS_QCOM:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z

    .line 94
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileLimitEnabled:Z

    .line 97
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileTcDropEnabled:Z

    .line 423
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$9;

    invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$9;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 100
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    .line 101
    iput-object p2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    .line 102
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->initParams()V

    .line 103
    return-void
.end method

.method private enableMobileTcMode(I)V
    .locals 5
    .param p1, "mode"    # I

    .line 249
    const/4 v0, 0x0

    .line 250
    .local v0, "isNeedUpdate":Z
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isLimitterEnabled()Z

    move-result v1

    .line 251
    .local v1, "oldLimitterEnabled":Z
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isLimitterEnabled(I)Z

    move-result v2

    .line 252
    .local v2, "newLimitterEnabled":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableMobileTcMode oldLimitterEnabled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",newLimitterEnabled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 253
    if-eqz v1, :cond_0

    if-nez v2, :cond_0

    .line 254
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTrafficLimit(Z)V

    .line 255
    iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I

    goto :goto_0

    .line 256
    :cond_0
    if-nez v1, :cond_1

    if-eqz v2, :cond_1

    iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z

    if-eqz v3, :cond_1

    .line 257
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTrafficLimit(Z)V

    .line 258
    iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I

    .line 259
    const/4 v0, 0x1

    .line 262
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 263
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 266
    :cond_2
    return-void
.end method

.method private declared-synchronized enableMobileTrafficLimit(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    monitor-enter p0

    .line 305
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableMobileTrafficLimit enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mIface="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 310
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessEnableLimit:Z

    .line 311
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;

    invoke-direct {v1, p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    monitor-exit p0

    return-void

    .line 307
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    :cond_1
    :goto_0
    :try_start_1
    const-string v0, "enableMobileTrafficLimit return by invalid value!!!"

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 308
    monitor-exit p0

    return-void

    .line 304
    .end local p1    # "enabled":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private getDefaultInputMethod()Ljava/lang/String;
    .locals 4

    .line 149
    const-string v0, ""

    .line 150
    .local v0, "defaultInputMethod":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "default_input_method"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "inputMethodId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 153
    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 154
    .local v2, "inputMethodIds":[Ljava/lang/String;
    array-length v3, v2

    if-lez v3, :cond_0

    .line 155
    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 158
    .end local v2    # "inputMethodIds":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getMobileLinkIface()Ljava/lang/String;
    .locals 2

    .line 438
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mCm:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mCm:Landroid/net/ConnectivityManager;

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mCm:Landroid/net/ConnectivityManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    move-result-object v0

    .line 442
    .local v0, "prop":Landroid/net/LinkProperties;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 445
    :cond_1
    invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 443
    :cond_2
    :goto_0
    const-string v1, ""

    return-object v1
.end method

.method private getUserMobileTcMode()I
    .locals 3

    .line 299
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mobile_tc_user_enable"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 301
    .local v0, "enable":I
    return v0
.end method

.method private initParams()V
    .locals 2

    .line 120
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    .line 121
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessEnableLimit:Z

    .line 123
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessSetLimit:Z

    .line 124
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsUid:Ljava/util/Set;

    .line 125
    iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I

    .line 126
    return-void
.end method

.method private initReceiver()V
    .locals 3

    .line 418
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 419
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 420
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 421
    return-void
.end method

.method private isAllowToSetTrafficRule(ZLjava/lang/String;J)I
    .locals 3
    .param p1, "enabled"    # Z
    .param p2, "iface"    # Ljava/lang/String;
    .param p3, "rate"    # J

    .line 369
    const/4 v0, 0x0

    .line 370
    .local v0, "rst":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAllowToSetTrafficRule enabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 371
    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessEnableLimit:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    return v1

    .line 372
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessSetLimit:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    return v1

    .line 373
    :cond_1
    if-eqz p1, :cond_2

    .line 374
    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I

    packed-switch v1, :pswitch_data_0

    .line 386
    goto :goto_0

    .line 383
    :pswitch_0
    const/4 v0, 0x3

    .line 384
    goto :goto_0

    .line 380
    :pswitch_1
    const/4 v0, 0x1

    .line 381
    goto :goto_0

    .line 376
    :pswitch_2
    const/4 v0, 0x2

    .line 377
    goto :goto_0

    .line 389
    :cond_2
    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 400
    :pswitch_3
    const/4 v0, 0x1

    .line 401
    goto :goto_0

    .line 397
    :pswitch_4
    const/4 v0, 0x3

    .line 398
    goto :goto_0

    .line 394
    :pswitch_5
    const/4 v0, 0x4

    .line 395
    goto :goto_0

    .line 391
    :pswitch_6
    const/4 v0, 0x2

    .line 392
    nop

    .line 407
    :goto_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 408
    invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidMobileIface(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 409
    const/4 v0, 0x6

    goto :goto_1

    .line 410
    :cond_3
    invoke-direct {p0, p3, p4}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidBandWidth(J)Z

    move-result v1

    if-nez v1, :cond_4

    .line 411
    const/4 v0, 0x5

    .line 414
    :cond_4
    :goto_1
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private declared-synchronized isAllowedToEnableMobileTcMode()V
    .locals 5

    monitor-enter p0

    .line 489
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getMobileLinkIface()Ljava/lang/String;

    move-result-object v0

    .line 490
    .local v0, "iface":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAllowedToEnableMobileTcMode iface="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mIface="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 491
    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z

    .line 492
    .local v1, "wasMobileNwOn":Z
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z

    .line 493
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 494
    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    .line 497
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    :cond_1
    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z

    if-eq v2, v1, :cond_3

    .line 498
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getUserMobileTcMode()I

    move-result v2

    .line 499
    .local v2, "tcMode":I
    invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isLimitterEnabled(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 500
    iget-boolean v4, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z

    if-eqz v4, :cond_2

    move v3, v2

    :cond_2
    invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTcMode(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    .end local v2    # "tcMode":I
    :cond_3
    monitor-exit p0

    return-void

    .line 488
    .end local v0    # "iface":Ljava/lang/String;
    .end local v1    # "wasMobileNwOn":Z
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private isLimitterEnabled()Z
    .locals 1

    .line 241
    iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isLimitterEnabled(I)Z

    move-result v0

    return v0
.end method

.method private isLimitterEnabled(I)Z
    .locals 1
    .param p1, "mode"    # I

    .line 245
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isUidValidForMobileTrafficLimit(I)Z
    .locals 1
    .param p0, "uid"    # I

    .line 229
    invoke-static {p0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    return v0
.end method

.method private isValidBandWidth(J)Z
    .locals 2
    .param p1, "rate"    # J

    .line 449
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isValidMobileIface(Ljava/lang/String;)Z
    .locals 2
    .param p1, "iface"    # Ljava/lang/String;

    .line 453
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->IS_QCOM:Z

    if-eqz v0, :cond_0

    const-string v0, "rmnet"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 456
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 454
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private isValidStateForWhiteListApp(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 223
    if-ltz p2, :cond_1

    const/4 v0, 0x4

    if-le p2, v0, :cond_0

    const/16 v0, 0x13

    if-ge p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsUid:Ljava/util/Set;

    .line 224
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 223
    :goto_0
    return v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .line 506
    const-string v0, "MiuiNetworkPolicyTrafficLimit"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    return-void
.end method

.method private registerDefaultInputMethodObserver()V
    .locals 5

    .line 162
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$3;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$3;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/os/Handler;)V

    .line 170
    .local v0, "dimObserver":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 171
    const-string v2, "default_input_method"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 170
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 173
    return-void
.end method

.method private registerMobileTrafficControllerChangedObserver()V
    .locals 5

    .line 269
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/os/Handler;)V

    .line 280
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 281
    const-string v2, "mobile_tc_user_enable"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 280
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 284
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$5;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$5;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 290
    return-void
.end method

.method private registerWhiteListAppsChangedObserver()V
    .locals 5

    .line 129
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$1;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/os/Handler;)V

    .line 136
    .local v0, "observer":Landroid/database/ContentObserver;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 137
    const-string v2, "mobile_tc_white_list_pkg_name"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 136
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 140
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$2;

    invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$2;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/database/ContentObserver;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 146
    return-void
.end method

.method private declared-synchronized setMobileTrafficLimit(ZJ)V
    .locals 2
    .param p1, "enabled"    # Z
    .param p2, "rate"    # J

    monitor-enter p0

    .line 329
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setMobileTrafficLimit "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",rate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    if-nez v0, :cond_0

    .line 331
    const-string/jumbo v0, "setMobileTrafficLimit return by invalid value!!!"

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    monitor-exit p0

    return-void

    .line 334
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessSetLimit:Z

    .line 335
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;ZJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    monitor-exit p0

    return-void

    .line 328
    .end local p1    # "enabled":Z
    .end local p2    # "rate":J
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized updateMobileTcWhiteList()V
    .locals 11

    monitor-enter p0

    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 177
    .local v0, "um":Landroid/os/UserManager;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 178
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v2

    .line 179
    .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-virtual {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->fetchMobileTcEnabledList()Ljava/util/Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsPN:Ljava/util/Set;

    .line 180
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mDefaultInputMethod:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsPN:Ljava/util/Set;

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mDefaultInputMethod:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 181
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateMobileTcWhiteList add pkgNames="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mDefaultInputMethod:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 182
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsPN:Ljava/util/Set;

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mDefaultInputMethod:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 184
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    :cond_0
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsUid:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 185
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsPN:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 186
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/UserInfo;

    .line 187
    .local v4, "user":Landroid/content/pm/UserInfo;
    iget v5, v4, Landroid/content/pm/UserInfo;->id:I

    const/4 v6, 0x0

    invoke-virtual {v1, v6, v5}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;

    move-result-object v5

    .line 188
    .local v5, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageInfo;

    .line 189
    .local v7, "app":Landroid/content/pm/PackageInfo;
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v8, :cond_1

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsPN:Ljava/util/Set;

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 190
    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 191
    iget v8, v4, Landroid/content/pm/UserInfo;->id:I

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v8, v9}, Landroid/os/UserHandle;->getUid(II)I

    move-result v8

    .line 192
    .local v8, "uid":I
    iget-object v9, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsUid:Ljava/util/Set;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    .end local v7    # "app":Landroid/content/pm/PackageInfo;
    .end local v8    # "uid":I
    :cond_1
    goto :goto_1

    .line 195
    .end local v4    # "user":Landroid/content/pm/UserInfo;
    .end local v5    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_2
    goto :goto_0

    .line 197
    :cond_3
    monitor-exit p0

    return-void

    .line 175
    .end local v0    # "um":Landroid/os/UserManager;
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    .end local v2    # "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateTrafficLimitStatus(I)V
    .locals 2
    .param p1, "mode"    # I

    .line 364
    iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I

    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateTrafficLimitStatus mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 366
    return-void
.end method

.method private declared-synchronized updateWhiteListUidForMobileTraffic(IZ)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "add"    # Z

    monitor-enter p0

    .line 349
    :try_start_0
    const-string v0, "MiuiNetworkPolicyTrafficLimit"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateWhiteListUidForMobileTraffic uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",add="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    if-nez v0, :cond_0

    .line 351
    const-string/jumbo v0, "updateWhiteListUidForMobileTraffic return by invalid value!!!"

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    monitor-exit p0

    return-void

    .line 354
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;IZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 361
    monitor-exit p0

    return-void

    .line 348
    .end local p1    # "uid":I
    .end local p2    # "add":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method public fetchMobileTcEnabledList()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 200
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mobile_tc_white_list_pkg_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "pkgNames":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fetchMobileTcEnabledList  pkgNames="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 203
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 204
    new-instance v1, Ljava/util/HashSet;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 205
    .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    return-object v1

    .line 207
    .end local v1    # "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    return-object v1
.end method

.method public getMobileIface()Ljava/lang/String;
    .locals 1

    .line 471
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    return-object v0
.end method

.method public getMobileLimitStatus()Z
    .locals 1

    .line 475
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileLimitEnabled:Z

    return v0
.end method

.method public declared-synchronized setMobileTrafficLimitForGame(ZJ)I
    .locals 3
    .param p1, "enabled"    # Z
    .param p2, "rate"    # J

    monitor-enter p0

    .line 460
    const/4 v0, 0x1

    if-eqz p1, :cond_0

    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-nez v1, :cond_0

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    :try_start_0
    iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileTcDropEnabled:Z

    .line 461
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIface:Ljava/lang/String;

    invoke-direct {p0, p1, v1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isAllowToSetTrafficRule(ZLjava/lang/String;J)I

    move-result v1

    .line 462
    .local v1, "rst":I
    if-eq v1, v0, :cond_1

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMobileTrafficLimitForGame rst="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ",mMobileTcDropEnabled="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileTcDropEnabled:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    monitor-exit p0

    return v1

    .line 466
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    :cond_1
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->setMobileTrafficLimit(ZJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 467
    monitor-exit p0

    return v0

    .line 459
    .end local v1    # "rst":I
    .end local p1    # "enabled":Z
    .end local p2    # "rate":J
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public systemReady(Lcom/android/server/net/MiuiNetworkManagementService;)V
    .locals 2
    .param p1, "networkMgr"    # Lcom/android/server/net/MiuiNetworkManagementService;

    .line 106
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    .line 107
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mCm:Landroid/net/ConnectivityManager;

    .line 108
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->initReceiver()V

    .line 109
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getDefaultInputMethod()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mDefaultInputMethod:Ljava/lang/String;

    .line 110
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->registerDefaultInputMethodObserver()V

    .line 111
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->registerWhiteListAppsChangedObserver()V

    .line 112
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->registerMobileTrafficControllerChangedObserver()V

    .line 113
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableLimitter(Z)Z

    .line 114
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateMobileTcWhiteList()V

    .line 115
    const-string/jumbo v0, "systemReady"

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 116
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isAllowedToEnableMobileTcMode()V

    .line 117
    return-void
.end method

.method public declared-synchronized updateAppPN(Ljava/lang/String;IZ)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "installed"    # Z

    monitor-enter p0

    .line 212
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateAppPN packageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",installed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsPN:Ljava/util/Set;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    if-eqz p3, :cond_0

    .line 215
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsUid:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mWhiteListAppsUid:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 211
    .end local p1    # "packageName":Ljava/lang/String;
    .end local p2    # "uid":I
    .end local p3    # "installed":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public updateMobileLimit(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateMobileLimit enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V

    .line 480
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileLimitEnabled:Z

    if-eq v0, p1, :cond_0

    .line 481
    const-wide/16 v0, 0x0

    .line 483
    .local v0, "bwBps":J
    invoke-direct {p0, p1, v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->setMobileTrafficLimit(ZJ)V

    .line 484
    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileLimitEnabled:Z

    .line 486
    .end local v0    # "bwBps":J
    :cond_0
    return-void
.end method

.method public updateRulesForUidStateChange(III)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "oldUidState"    # I
    .param p3, "newUidState"    # I

    .line 233
    invoke-static {p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isUidValidForMobileTrafficLimit(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidStateForWhiteListApp(II)Z

    move-result v0

    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidStateForWhiteListApp(II)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 235
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidStateForWhiteListApp(II)Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateWhiteListUidForMobileTraffic(IZ)V

    .line 238
    :cond_0
    return-void
.end method
