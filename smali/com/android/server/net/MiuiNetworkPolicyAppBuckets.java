public class com.android.server.net.MiuiNetworkPolicyAppBuckets {
	 /* .source "MiuiNetworkPolicyAppBuckets.java" */
	 /* # static fields */
	 private static final java.lang.String ACTION_CLOUD_TELE_FEATURE_INFO_CHANGED;
	 private static final java.lang.String ACTION_GESTURE_WHITE_APP_SCENE;
	 private static final java.lang.String ACTION_SPEED_WHITE_LIST;
	 private static final java.lang.String ACTION_THERMAL_SPECIAL_APP_SCENE;
	 private static final java.lang.String ACTION_THROTTLE_SPECIAL_APP_SCENE;
	 private static final java.lang.String ACTION_THROTTLE_WHITE_APP_SCENE;
	 private static final java.lang.String CLOUD_CATEGORY_THERMAL_THORTTLE;
	 private static final java.lang.String CLOUD_KEY_THROTTLE_WHITE_APP;
	 private static final java.lang.String CONNECTION_EX;
	 private static final Integer CON_DISABLED;
	 private static final Integer CON_ENABLED;
	 private static final Boolean DEBUG;
	 private static final java.lang.String GESTURE_WHITE_APP_EXTRA;
	 private static final java.lang.String LATENCY_ACTION_CHANGE_LEVEL;
	 private static final java.lang.String LOCAL_GESTURE_WHITELIST_APP_LIST;
	 private static final java.lang.String LOCAL_HONGBAO_APP_LIST;
	 private static java.lang.String LOCAL_THROTTLE_WHITE_APP_LIST;
	 private static final java.lang.String LOCAL_TPUT_TOOL_APP_LIST;
	 private static final java.lang.String NOTIFACATION_RECEIVER_PACKAGE;
	 private static final java.lang.String OPTIMIZATION_ENABLED;
	 private static final java.lang.String TAG;
	 private static final java.lang.String THERMAL_SPECIAL_APP_EXTRA;
	 private static final java.lang.String THROTTLE_SPECIAL_APP_EXTRA;
	 private static final java.lang.String THROTTLE_WHITE_APP_EXTRA;
	 private static final java.lang.String TPUT_TEST_APP_OPTIMIZATION;
	 private static final java.lang.String WHITE_LIST_PACKAGE_NAME;
	 private static final java.lang.String WHITE_LIST_STATE_TOP;
	 /* # instance fields */
	 private java.lang.String LOCAL_THERMAL_SPECIAL_APP_LIST;
	 private java.lang.String LOCAL_THROTTLE_SPECIAL_APP_LIST;
	 private java.util.Set mAppUid;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.Set mAppsPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.net.ConnectivityManager mCm;
private final android.content.Context mContext;
private final android.os.Handler mHandler;
private Boolean mIsGestureWhiteAppOn;
private Boolean mIsHongbaoAppOn;
private Boolean mIsMobileNwOn;
private Boolean mIsMobileTcEnabledListOn;
private Boolean mIsSpeedWhiteListOn;
private Boolean mIsThermalSpecialAppOn;
private Boolean mIsThrottleSpecialAppOn;
private Boolean mIsTputTestAppOn;
private Boolean mIsWhiteAppOn;
private Boolean mLastHongbaoApp;
private Boolean mLastMobileNw;
private Boolean mLastMobileTcEnabledListOn;
private Boolean mLastSpeedWhiteList;
private Boolean mLastThermalSpecialApp;
private Boolean mLastThrottleSpecialApp;
private java.lang.String mLastThrottleWhiteAppList;
private Boolean mLastTputTestApp;
private Boolean mLastWhiteApp;
private java.util.Set mMobileTcEnabledList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
android.database.ContentObserver mMobileTcEnabledListObserver;
private java.lang.String mMobileTcEnabledPkgName;
final android.content.BroadcastReceiver mReceiver;
private java.util.Set mSpeedWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
android.database.ContentObserver mSpeedWhiteListObserver;
private java.util.concurrent.ConcurrentHashMap mUidMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mWhiteListPkgName;
/* # direct methods */
static Boolean -$$Nest$fgetmIsMobileNwOn ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z */
} // .end method
static Boolean -$$Nest$fgetmLastMobileNw ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z */
} // .end method
static void -$$Nest$fputmIsMobileNwOn ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z */
return;
} // .end method
static void -$$Nest$fputmLastMobileNw ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z */
return;
} // .end method
static java.lang.String -$$Nest$mgetMobileLinkIface ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getMobileLinkIface()Ljava/lang/String; */
} // .end method
static void -$$Nest$mlog ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mupdate5gPowerWhiteApplist ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0, java.lang.String p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->update5gPowerWhiteApplist(Ljava/lang/String;Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mupdateAppList ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppList()V */
return;
} // .end method
static void -$$Nest$mupdateHongbaoModeStatus ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateHongbaoModeStatus()V */
return;
} // .end method
static void -$$Nest$mupdateMobileTcEnabledListStatus ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateMobileTcEnabledListStatus()V */
return;
} // .end method
static void -$$Nest$mupdateSpeedWhiteListStatus ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateSpeedWhiteListStatus()V */
return;
} // .end method
static void -$$Nest$mupdateTputTestAppStatus ( com.android.server.net.MiuiNetworkPolicyAppBuckets p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateTputTestAppStatus()V */
return;
} // .end method
static com.android.server.net.MiuiNetworkPolicyAppBuckets ( ) {
/* .locals 9 */
/* .line 66 */
final String v0 = "com.tencent.mm"; // const-string v0, "com.tencent.mm"
/* filled-new-array {v0}, [Ljava/lang/String; */
/* .line 71 */
final String v1 = "org.zwanoo.android.speedtest"; // const-string v1, "org.zwanoo.android.speedtest"
final String v2 = "org.zwanoo.android.speedtest.china"; // const-string v2, "org.zwanoo.android.speedtest.china"
final String v3 = "cn.nokia.speedtest5g"; // const-string v3, "cn.nokia.speedtest5g"
final String v4 = "org.zwanoo.android.speedtest.gworld"; // const-string v4, "org.zwanoo.android.speedtest.gworld"
final String v5 = "cn.speedtest.lite"; // const-string v5, "cn.speedtest.lite"
final String v6 = "cn.lezhi.speedtest"; // const-string v6, "cn.lezhi.speedtest"
/* filled-new-array/range {v1 ..v6}, [Ljava/lang/String; */
/* .line 81 */
final String v1 = "com.smile.gifmaker"; // const-string v1, "com.smile.gifmaker"
final String v2 = "com.tencent.mm"; // const-string v2, "com.tencent.mm"
final String v3 = "com.ss.android.ugc.aweme"; // const-string v3, "com.ss.android.ugc.aweme"
final String v4 = "com.taobao.taobao"; // const-string v4, "com.taobao.taobao"
final String v5 = "com.ss.android.article.news"; // const-string v5, "com.ss.android.article.news"
/* const-string/jumbo v6, "tv.danmaku.bili" */
final String v7 = "com.duowan.kiwi"; // const-string v7, "com.duowan.kiwi"
final String v8 = "air.tv.douyu.android"; // const-string v8, "air.tv.douyu.android"
/* filled-new-array/range {v1 ..v8}, [Ljava/lang/String; */
/* .line 102 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.net.MiuiNetworkPolicyAppBuckets ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 145 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z */
/* .line 52 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z */
/* .line 53 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsHongbaoAppOn:Z */
/* .line 54 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastHongbaoApp:Z */
/* .line 55 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsTputTestAppOn:Z */
/* .line 56 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastTputTestApp:Z */
/* .line 57 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsSpeedWhiteListOn:Z */
/* .line 58 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastSpeedWhiteList:Z */
/* .line 59 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileTcEnabledListOn:Z */
/* .line 60 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileTcEnabledListOn:Z */
/* .line 63 */
/* new-instance v1, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mUidMap = v1;
/* .line 98 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsWhiteAppOn:Z */
/* .line 99 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastWhiteApp:Z */
/* .line 100 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsGestureWhiteAppOn:Z */
/* .line 108 */
int v1 = 0; // const/4 v1, 0x0
this.mLastThrottleWhiteAppList = v1;
/* .line 111 */
/* new-array v2, v0, [Ljava/lang/String; */
this.LOCAL_THERMAL_SPECIAL_APP_LIST = v2;
/* .line 113 */
/* new-array v2, v0, [Ljava/lang/String; */
this.LOCAL_THROTTLE_SPECIAL_APP_LIST = v2;
/* .line 121 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThermalSpecialAppOn:Z */
/* .line 122 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThermalSpecialApp:Z */
/* .line 123 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThrottleSpecialAppOn:Z */
/* .line 124 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleSpecialApp:Z */
/* .line 142 */
this.mWhiteListPkgName = v1;
/* .line 143 */
this.mMobileTcEnabledPkgName = v1;
/* .line 169 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$1; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Landroid/os/Handler;)V */
this.mSpeedWhiteListObserver = v0;
/* .line 183 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;-><init>(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V */
this.mReceiver = v0;
/* .line 595 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$3; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$3;-><init>(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Landroid/os/Handler;)V */
this.mMobileTcEnabledListObserver = v0;
/* .line 146 */
this.mContext = p1;
/* .line 147 */
this.mHandler = p2;
/* .line 148 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mAppUid = v0;
/* .line 149 */
return;
} // .end method
private void addUidToMap ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 326 */
v0 = this.mUidMap;
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 327 */
v0 = this.mUidMap;
java.lang.Integer .valueOf ( p2 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 329 */
} // :cond_0
return;
} // .end method
private void appBucketsForUidStateChanged ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 375 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "appBucketsForUidStateChanged uid="; // const-string v1, "appBucketsForUidStateChanged uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",state="; // const-string v1, ",state="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 376 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isAppBucketsEnabledForUid(II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 377 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processHongbaoAppIfNeed(IZ)V */
/* .line 378 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processTputTestAppIfNeed(IZ)V */
/* .line 379 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processSpeedWhiteListIfNeed(IZ)V */
/* .line 380 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processMobileTcEnabledListIfNeed(IZ)V */
/* .line 381 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThrottleWhiteAppIfNeed(IZ)V */
/* .line 382 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThermalSpecialAppIfNeed(IZ)V */
/* .line 383 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThrottleSpecialAppIfNeed(IZ)V */
/* .line 384 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processGestureWhiteAppIfNeed(IZ)V */
/* .line 386 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processHongbaoAppIfNeed(IZ)V */
/* .line 387 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processTputTestAppIfNeed(IZ)V */
/* .line 388 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processSpeedWhiteListIfNeed(IZ)V */
/* .line 389 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processMobileTcEnabledListIfNeed(IZ)V */
/* .line 390 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThrottleWhiteAppIfNeed(IZ)V */
/* .line 391 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThermalSpecialAppIfNeed(IZ)V */
/* .line 392 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processThrottleSpecialAppIfNeed(IZ)V */
/* .line 393 */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->processGestureWhiteAppIfNeed(IZ)V */
/* .line 395 */
} // :goto_0
return;
} // .end method
private void enableHongbaoMode ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 463 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enableHongbaoMode enable"; // const-string v1, "enableHongbaoMode enable"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 464 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 465 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.phone.intent.action.CHANGE_LEVEL"; // const-string v1, "com.android.phone.intent.action.CHANGE_LEVEL"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 466 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 467 */
final String v1 = "enableConnectionExtension"; // const-string v1, "enableConnectionExtension"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 468 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 469 */
return;
} // .end method
private void gestureAppNotification ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "extra" # Ljava/lang/String; */
/* .param p3, "enable" # Z */
/* .line 732 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "gestureAppNotification action = "; // const-string v1, "gestureAppNotification action = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", enable = "; // const-string v1, ", enable = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 733 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 734 */
/* .local v0, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v0 ).setAction ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 735 */
(( android.content.Intent ) v0 ).putExtra ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 736 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 737 */
return;
} // .end method
private java.util.Set getAllAppsPN ( ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 229 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 231 */
/* .local v0, "appList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v1 = com.android.server.net.MiuiNetworkPolicyAppBuckets .isHongbaoModeAllowed ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 232 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = com.android.server.net.MiuiNetworkPolicyAppBuckets.LOCAL_HONGBAO_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_0 */
/* .line 233 */
/* aget-object v2, v2, v1 */
/* .line 232 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 237 */
} // .end local v1 # "i":I
} // :cond_0
v1 = com.android.server.net.MiuiNetworkPolicyAppBuckets .isCommonSceneRecognitionAllowed ( );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 238 */
int v1 = 0; // const/4 v1, 0x0
/* .restart local v1 # "i":I */
} // :goto_1
v2 = com.android.server.net.MiuiNetworkPolicyAppBuckets.LOCAL_TPUT_TOOL_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_1 */
/* .line 239 */
/* aget-object v2, v2, v1 */
/* .line 238 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 242 */
} // .end local v1 # "i":I
} // :cond_1
(( com.android.server.net.MiuiNetworkPolicyAppBuckets ) p0 ).fetchSpeedAppWhiteList ( ); // invoke-virtual {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->fetchSpeedAppWhiteList()Ljava/util/Set;
this.mSpeedWhiteList = v1;
/* .line 243 */
v1 = if ( v1 != null) { // if-eqz v1, :cond_2
/* if-lez v1, :cond_2 */
/* .line 244 */
v1 = this.mSpeedWhiteList;
/* .line 248 */
} // :cond_2
v1 = this.mContext;
/* .line 249 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x11030067 */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 250 */
/* .local v1, "deviceArray":[Ljava/lang/String; */
final String v2 = "ro.product.name"; // const-string v2, "ro.product.name"
android.os.SystemProperties .get ( v2 );
/* .line 251 */
/* .local v2, "product":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 252 */
/* .local v3, "isDeviceSupportWhitelist":Z */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_2
/* array-length v5, v1 */
/* if-ge v4, v5, :cond_4 */
/* .line 253 */
/* aget-object v5, v1, v4 */
v5 = (( java.lang.String ) v5 ).equals ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 254 */
int v3 = 1; // const/4 v3, 0x1
/* .line 255 */
/* .line 252 */
} // :cond_3
/* add-int/lit8 v4, v4, 0x1 */
/* .line 258 */
} // .end local v4 # "i":I
} // :cond_4
} // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 259 */
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
} // :goto_4
v5 = com.android.server.net.MiuiNetworkPolicyAppBuckets.LOCAL_GESTURE_WHITELIST_APP_LIST;
/* array-length v6, v5 */
/* if-ge v4, v6, :cond_5 */
/* .line 260 */
/* aget-object v5, v5, v4 */
/* .line 259 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 264 */
} // .end local v4 # "i":I
} // :cond_5
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .isMobileTcFeatureAllowed ( );
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 266 */
(( com.android.server.net.MiuiNetworkPolicyAppBuckets ) p0 ).fetchMobileTcEnabledList ( ); // invoke-virtual {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->fetchMobileTcEnabledList()Ljava/util/Set;
this.mMobileTcEnabledList = v4;
/* .line 267 */
v4 = if ( v4 != null) { // if-eqz v4, :cond_6
/* if-lez v4, :cond_6 */
/* .line 268 */
v4 = this.mMobileTcEnabledList;
/* .line 272 */
} // :cond_6
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
} // :goto_5
v5 = com.android.server.net.MiuiNetworkPolicyAppBuckets.LOCAL_THROTTLE_WHITE_APP_LIST;
/* array-length v6, v5 */
/* if-ge v4, v6, :cond_7 */
/* .line 273 */
/* aget-object v5, v5, v4 */
/* .line 272 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 276 */
} // .end local v4 # "i":I
} // :cond_7
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
} // :goto_6
v5 = this.LOCAL_THERMAL_SPECIAL_APP_LIST;
/* array-length v6, v5 */
/* if-ge v4, v6, :cond_8 */
/* .line 277 */
/* aget-object v5, v5, v4 */
/* .line 276 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 279 */
} // .end local v4 # "i":I
} // :cond_8
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
} // :goto_7
v5 = this.LOCAL_THROTTLE_SPECIAL_APP_LIST;
/* array-length v6, v5 */
/* if-ge v4, v6, :cond_9 */
/* .line 280 */
/* aget-object v5, v5, v4 */
/* .line 279 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 283 */
} // .end local v4 # "i":I
} // :cond_9
} // .end method
private java.lang.String getAppListFromCloud ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "Category" # Ljava/lang/String; */
/* .param p2, "Key" # Ljava/lang/String; */
/* .line 718 */
miui.telephony.TelephonyManagerEx .getDefault ( );
(( miui.telephony.TelephonyManagerEx ) v0 ).getFeatureInfoIntentByCloud ( p1 ); // invoke-virtual {v0, p1}, Lmiui/telephony/TelephonyManagerEx;->getFeatureInfoIntentByCloud(Ljava/lang/String;)Landroid/content/Intent;
/* .line 719 */
/* .local v0, "featureInfoIntent":Landroid/content/Intent; */
if ( v0 != null) { // if-eqz v0, :cond_0
(( android.content.Intent ) v0 ).getStringExtra ( p2 ); // invoke-virtual {v0, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private java.lang.String getMobileLinkIface ( ) {
/* .locals 2 */
/* .line 212 */
v0 = this.mCm;
/* if-nez v0, :cond_0 */
/* .line 213 */
v0 = this.mContext;
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.mCm = v0;
/* .line 215 */
} // :cond_0
v0 = this.mCm;
int v1 = 0; // const/4 v1, 0x0
(( android.net.ConnectivityManager ) v0 ).getLinkProperties ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;
/* .line 216 */
/* .local v0, "prop":Landroid/net/LinkProperties; */
if ( v0 != null) { // if-eqz v0, :cond_2
(( android.net.LinkProperties ) v0 ).getInterfaceName ( ); // invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
v1 = android.text.TextUtils .isEmpty ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 219 */
} // :cond_1
(( android.net.LinkProperties ) v0 ).getInterfaceName ( ); // invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
/* .line 217 */
} // :cond_2
} // :goto_0
final String v1 = ""; // const-string v1, ""
} // .end method
private Integer getUidFromMap ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 332 */
v0 = this.mUidMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* if-nez v0, :cond_0 */
/* .line 333 */
int v0 = -1; // const/4 v0, -0x1
/* .line 335 */
} // :cond_0
v0 = this.mUidMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // .end method
private Boolean hasUidFromGestureWhiteAppMap ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 621 */
int v0 = 0; // const/4 v0, 0x0
/* .line 622 */
/* .local v0, "rst":Z */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = com.android.server.net.MiuiNetworkPolicyAppBuckets.LOCAL_GESTURE_WHITELIST_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_1 */
/* .line 623 */
/* aget-object v2, v2, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v2, p1, :cond_0 */
/* .line 624 */
int v0 = 1; // const/4 v0, 0x1
/* .line 625 */
/* .line 622 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 628 */
} // .end local v1 # "i":I
} // :cond_1
} // :goto_1
} // .end method
private Boolean hasUidFromHongbaoMap ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 407 */
int v0 = 0; // const/4 v0, 0x0
/* .line 408 */
/* .local v0, "rst":Z */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = com.android.server.net.MiuiNetworkPolicyAppBuckets.LOCAL_HONGBAO_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_1 */
/* .line 409 */
/* aget-object v2, v2, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v2, p1, :cond_0 */
/* .line 410 */
int v0 = 1; // const/4 v0, 0x1
/* .line 408 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 413 */
} // .end local v1 # "i":I
} // :cond_1
} // .end method
private Boolean hasUidFromMobileTcEnabledListMap ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 564 */
int v0 = 0; // const/4 v0, 0x0
/* .line 565 */
/* .local v0, "rst":Z */
v1 = this.mMobileTcEnabledList;
v1 = if ( v1 != null) { // if-eqz v1, :cond_3
/* if-nez v1, :cond_0 */
/* .line 568 */
} // :cond_0
v1 = this.mMobileTcEnabledList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 569 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v3, p1, :cond_1 */
/* .line 570 */
this.mMobileTcEnabledPkgName = v2;
/* .line 571 */
int v0 = 1; // const/4 v0, 0x1
/* .line 572 */
/* .line 574 */
} // .end local v2 # "packageName":Ljava/lang/String;
} // :cond_1
/* .line 575 */
} // :cond_2
} // :goto_1
/* .line 566 */
} // :cond_3
} // :goto_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean hasUidFromSpeedWhiteListMap ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 428 */
int v0 = 0; // const/4 v0, 0x0
/* .line 429 */
/* .local v0, "rst":Z */
v1 = this.mSpeedWhiteList;
v1 = if ( v1 != null) { // if-eqz v1, :cond_3
/* if-nez v1, :cond_0 */
/* .line 432 */
} // :cond_0
v1 = this.mSpeedWhiteList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/String; */
/* .line 433 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v3, p1, :cond_1 */
/* .line 434 */
this.mWhiteListPkgName = v2;
/* .line 435 */
int v0 = 1; // const/4 v0, 0x1
/* .line 436 */
/* .line 438 */
} // .end local v2 # "packageName":Ljava/lang/String;
} // :cond_1
/* .line 439 */
} // :cond_2
} // :goto_1
/* .line 430 */
} // :cond_3
} // :goto_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
private Boolean hasUidFromThermalSpecialAppMap ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 654 */
int v0 = 0; // const/4 v0, 0x0
/* .line 655 */
/* .local v0, "rst":Z */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.LOCAL_THERMAL_SPECIAL_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_1 */
/* .line 656 */
/* aget-object v2, v2, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v2, p1, :cond_0 */
/* .line 657 */
int v0 = 1; // const/4 v0, 0x1
/* .line 658 */
/* .line 655 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 661 */
} // .end local v1 # "i":I
} // :cond_1
} // :goto_1
} // .end method
private Boolean hasUidFromThrottleSpecialAppMap ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 676 */
int v0 = 0; // const/4 v0, 0x0
/* .line 677 */
/* .local v0, "rst":Z */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.LOCAL_THROTTLE_SPECIAL_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_1 */
/* .line 678 */
/* aget-object v2, v2, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v2, p1, :cond_0 */
/* .line 679 */
int v0 = 1; // const/4 v0, 0x1
/* .line 680 */
/* .line 677 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 683 */
} // .end local v1 # "i":I
} // :cond_1
} // :goto_1
} // .end method
private Boolean hasUidFromThrottleWhiteAppMap ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 632 */
int v0 = 0; // const/4 v0, 0x0
/* .line 633 */
/* .local v0, "rst":Z */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = com.android.server.net.MiuiNetworkPolicyAppBuckets.LOCAL_THROTTLE_WHITE_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_1 */
/* .line 634 */
/* aget-object v2, v2, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v2, p1, :cond_0 */
/* .line 635 */
int v0 = 1; // const/4 v0, 0x1
/* .line 636 */
/* .line 633 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 639 */
} // .end local v1 # "i":I
} // :cond_1
} // :goto_1
} // .end method
private Boolean hasUidFromTputTestMap ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 417 */
int v0 = 0; // const/4 v0, 0x0
/* .line 418 */
/* .local v0, "rst":Z */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = com.android.server.net.MiuiNetworkPolicyAppBuckets.LOCAL_TPUT_TOOL_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_1 */
/* .line 419 */
/* aget-object v2, v2, v1 */
v2 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v2, p1, :cond_0 */
/* .line 420 */
int v0 = 1; // const/4 v0, 0x1
/* .line 421 */
/* .line 418 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 424 */
} // .end local v1 # "i":I
} // :cond_1
} // :goto_1
} // .end method
private void initReceiver ( ) {
/* .locals 4 */
/* .line 177 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 178 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 179 */
final String v1 = "com.android.phone.intent.action.CLOUD_TELE_FEATURE_INFO_CHANGED"; // const-string v1, "com.android.phone.intent.action.CLOUD_TELE_FEATURE_INFO_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 180 */
v1 = this.mContext;
v2 = this.mReceiver;
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 181 */
return;
} // .end method
private Boolean isAppBucketsEnabledForUid ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 371 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
v0 = this.mAppUid;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isCommonSceneRecognitionAllowed ( ) {
/* .locals 2 */
/* .line 527 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
v0 = miui.os.Build.DEVICE;
/* .line 529 */
final String v1 = "crux"; // const-string v1, "crux"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
v0 = miui.os.Build.DEVICE;
/* .line 530 */
final String v1 = "andromeda"; // const-string v1, "andromeda"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 527 */
} // :goto_0
} // .end method
private static Boolean isHongbaoModeAllowed ( ) {
/* .locals 1 */
/* .line 535 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static Boolean isUidValidForQos ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "uid" # I */
/* .line 225 */
v0 = android.os.UserHandle .isApp ( p0 );
} // .end method
private void log ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "s" # Ljava/lang/String; */
/* .line 740 */
final String v0 = "MiuiNetworkPolicyAppBuckets"; // const-string v0, "MiuiNetworkPolicyAppBuckets"
android.util.Log .d ( v0,p1 );
/* .line 741 */
return;
} // .end method
private void mobileTcEnabledStatusChanged ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 548 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mobileTcEnabledStatusChanged enable"; // const-string v1, "mobileTcEnabledStatusChanged enable"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",mMobileTcEnabledPkgName="; // const-string v1, ",mMobileTcEnabledPkgName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mMobileTcEnabledPkgName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 549 */
v0 = this.mHandler;
/* .line 551 */
/* nop */
/* .line 549 */
/* const/16 v1, 0xd */
int v2 = 0; // const/4 v2, 0x0
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1, v2 ); // invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 552 */
return;
} // .end method
private void processGestureWhiteAppIfNeed ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "enabled" # Z */
/* .line 612 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromGestureWhiteAppMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 613 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processGestureWhiteAppIfNeed enabled = "; // const-string v1, "processGestureWhiteAppIfNeed enabled = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 614 */
final String v0 = "gestureWhiteAppExtra"; // const-string v0, "gestureWhiteAppExtra"
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsGestureWhiteAppOn:Z */
final String v2 = "com.android.phone.intent.action.GESTURE_WHITE_APP_SCENE"; // const-string v2, "com.android.phone.intent.action.GESTURE_WHITE_APP_SCENE"
/* invoke-direct {p0, v2, v0, p2, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateGestureWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;ZZ)V */
/* .line 616 */
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsGestureWhiteAppOn:Z */
/* .line 618 */
} // :cond_0
return;
} // .end method
private void processHongbaoAppIfNeed ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "enabled" # Z */
/* .line 454 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromHongbaoMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 455 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processHongbaoAppIfNeed Hongbao"; // const-string v1, "processHongbaoAppIfNeed Hongbao"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 456 */
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsHongbaoAppOn:Z */
/* .line 457 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateHongbaoModeStatus()V */
/* .line 458 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsHongbaoAppOn:Z */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastHongbaoApp:Z */
/* .line 460 */
} // :cond_0
return;
} // .end method
private void processMobileTcEnabledListIfNeed ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "enabled" # Z */
/* .line 555 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromMobileTcEnabledListMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 556 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processMobileTcEnabledListIfNeed enabled="; // const-string v1, "processMobileTcEnabledListIfNeed enabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 557 */
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileTcEnabledListOn:Z */
/* .line 558 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateMobileTcEnabledListStatus()V */
/* .line 559 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileTcEnabledListOn:Z */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileTcEnabledListOn:Z */
/* .line 561 */
} // :cond_0
return;
} // .end method
private void processSpeedWhiteListIfNeed ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "enabled" # Z */
/* .line 499 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromSpeedWhiteListMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 500 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processSpeedWhiteListIfNeed enabled="; // const-string v1, "processSpeedWhiteListIfNeed enabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 501 */
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsSpeedWhiteListOn:Z */
/* .line 502 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateSpeedWhiteListStatus()V */
/* .line 503 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsSpeedWhiteListOn:Z */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastSpeedWhiteList:Z */
/* .line 505 */
} // :cond_0
return;
} // .end method
private void processThermalSpecialAppIfNeed ( Integer p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "enabled" # Z */
/* .line 643 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromThermalSpecialAppMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 644 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processThermalSpecialAppIfNeed enabled = "; // const-string v1, "processThermalSpecialAppIfNeed enabled = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 645 */
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThermalSpecialAppOn:Z */
/* .line 646 */
final String v2 = "com.android.phone.intent.action.THERMAL_SPECIAL_APP_SCENE"; // const-string v2, "com.android.phone.intent.action.THERMAL_SPECIAL_APP_SCENE"
final String v3 = "com.android.phone"; // const-string v3, "com.android.phone"
/* const-string/jumbo v4, "thermalSpecialAppExtra" */
/* iget-boolean v6, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThermalSpecialApp:Z */
/* move-object v1, p0 */
/* move v5, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->update5gPowerWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V */
/* .line 649 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThermalSpecialAppOn:Z */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThermalSpecialApp:Z */
/* .line 651 */
} // :cond_0
return;
} // .end method
private void processThrottleSpecialAppIfNeed ( Integer p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "enabled" # Z */
/* .line 665 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromThrottleSpecialAppMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 666 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processThrottleSpecialAppIfNeed enabled = "; // const-string v1, "processThrottleSpecialAppIfNeed enabled = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 667 */
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThrottleSpecialAppOn:Z */
/* .line 668 */
final String v2 = "com.android.phone.intent.action.THROTTLE_SPECIAL_APP_SCENE"; // const-string v2, "com.android.phone.intent.action.THROTTLE_SPECIAL_APP_SCENE"
final String v3 = "com.android.phone"; // const-string v3, "com.android.phone"
/* const-string/jumbo v4, "throttleSpecialAppExtra" */
/* iget-boolean v6, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleSpecialApp:Z */
/* move-object v1, p0 */
/* move v5, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->update5gPowerWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V */
/* .line 671 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsThrottleSpecialAppOn:Z */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastThrottleSpecialApp:Z */
/* .line 673 */
} // :cond_0
return;
} // .end method
private void processThrottleWhiteAppIfNeed ( Integer p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "enabled" # Z */
/* .line 603 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromThrottleWhiteAppMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 604 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processThrottleWhiteAppIfNeed enabled = "; // const-string v1, "processThrottleWhiteAppIfNeed enabled = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 605 */
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsWhiteAppOn:Z */
/* .line 606 */
final String v2 = "com.android.phone.intent.action.THROTTLE_WHITE_APP_SCENE"; // const-string v2, "com.android.phone.intent.action.THROTTLE_WHITE_APP_SCENE"
final String v3 = "com.android.phone"; // const-string v3, "com.android.phone"
/* const-string/jumbo v4, "throttleWhiteAppExtra" */
/* iget-boolean v6, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastWhiteApp:Z */
/* move-object v1, p0 */
/* move v5, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->update5gPowerWhiteAppStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V */
/* .line 607 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsWhiteAppOn:Z */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastWhiteApp:Z */
/* .line 609 */
} // :cond_0
return;
} // .end method
private void processTputTestAppIfNeed ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "enabled" # Z */
/* .line 472 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->hasUidFromTputTestMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 473 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "processTputTestAppIfNeed TputTest="; // const-string v1, "processTputTestAppIfNeed TputTest="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 474 */
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsTputTestAppOn:Z */
/* .line 475 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateTputTestAppStatus()V */
/* .line 476 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsTputTestAppOn:Z */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastTputTestApp:Z */
/* .line 478 */
} // :cond_0
return;
} // .end method
private void registerMobileTcEnabledList ( ) {
/* .locals 4 */
/* .line 590 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 591 */
final String v1 = "mobile_tc_enabled_list_pkg_name"; // const-string v1, "mobile_tc_enabled_list_pkg_name"
android.provider.Settings$Global .getUriFor ( v1 );
v2 = this.mMobileTcEnabledListObserver;
/* .line 590 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 593 */
return;
} // .end method
private void registerSpeedWhiteList ( ) {
/* .locals 4 */
/* .line 164 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 165 */
final String v1 = "fiveg_speed_white_list_pkg_name"; // const-string v1, "fiveg_speed_white_list_pkg_name"
android.provider.Settings$Global .getUriFor ( v1 );
v2 = this.mSpeedWhiteListObserver;
/* .line 164 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 167 */
return;
} // .end method
private void removeUidFromMap ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 339 */
v0 = this.mUidMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 340 */
return;
} // .end method
private void speedWhiteListAppNotification ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 517 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "speedWhiteListAppNotification enable=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = "; mWhiteListPkgName="; // const-string v1, "; mWhiteListPkgName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mWhiteListPkgName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 518 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 519 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.phone.intent.action.SPEED_WHITE_LIST"; // const-string v1, "com.android.phone.intent.action.SPEED_WHITE_LIST"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 520 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 521 */
/* const-string/jumbo v1, "whiteListStateTop" */
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 522 */
/* const-string/jumbo v1, "whiteListPackageName" */
v2 = this.mWhiteListPkgName;
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 523 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendStickyBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 524 */
return;
} // .end method
private void tputTestAppNotification ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 490 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "tputTestAppNotification enable=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 491 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 492 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.phone.intent.action.TPUT_OPTIMIZATION"; // const-string v1, "com.android.phone.intent.action.TPUT_OPTIMIZATION"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 493 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 494 */
final String v1 = "optimizationEnabled"; // const-string v1, "optimizationEnabled"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 495 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 496 */
return;
} // .end method
private void uidRemoveAll ( ) {
/* .locals 1 */
/* .line 343 */
v0 = this.mUidMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 344 */
return;
} // .end method
private synchronized void update5gPowerWhiteAppStatus ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Boolean p3, Boolean p4 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .param p3, "extra" # Ljava/lang/String; */
/* .param p4, "newStatusOn" # Z */
/* .param p5, "oldStatusOn" # Z */
/* monitor-enter p0 */
/* .line 703 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "update5gPowerWhiteAppStatus newStatusOn=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",oldStatusOn="; // const-string v1, ",oldStatusOn="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 704 */
/* if-eq p4, p5, :cond_0 */
/* .line 705 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->whiteAppNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 707 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 702 */
} // .end local p1 # "action":Ljava/lang/String;
} // .end local p2 # "pkg":Ljava/lang/String;
} // .end local p3 # "extra":Ljava/lang/String;
} // .end local p4 # "newStatusOn":Z
} // .end local p5 # "oldStatusOn":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private void update5gPowerWhiteApplist ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "Category" # Ljava/lang/String; */
/* .param p2, "Key" # Ljava/lang/String; */
/* .line 687 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "update5gPowerWhiteApplist Category = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", Key = "; // const-string v1, ", Key = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 688 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getAppListFromCloud(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 689 */
/* .local v0, "cloudAppList":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = this.mLastThrottleWhiteAppList;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 691 */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_2 */
/* .line 692 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "update5gPowerWhiteApplist newWhitelist = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 693 */
final String v1 = "TelephonyThermalThrottle"; // const-string v1, "TelephonyThermalThrottle"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 694 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 695 */
this.mLastThrottleWhiteAppList = v0;
/* .line 697 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppList()V */
/* .line 699 */
} // :cond_2
return;
} // .end method
private void updateAppList ( ) {
/* .locals 10 */
/* .line 298 */
v0 = this.mContext;
/* const-string/jumbo v1, "user" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/UserManager; */
/* .line 299 */
/* .local v0, "um":Landroid/os/UserManager; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 300 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
(( android.os.UserManager ) v0 ).getUsers ( ); // invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;
/* .line 301 */
/* .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;" */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getAllAppsPN()Ljava/util/Set; */
this.mAppsPN = v3;
/* .line 302 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->uidRemoveAll()V */
/* .line 303 */
v3 = v3 = this.mAppsPN;
/* if-nez v3, :cond_3 */
/* .line 304 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/content/pm/UserInfo; */
/* .line 305 */
/* .local v4, "user":Landroid/content/pm/UserInfo; */
int v5 = 0; // const/4 v5, 0x0
/* iget v6, v4, Landroid/content/pm/UserInfo;->id:I */
(( android.content.pm.PackageManager ) v1 ).getInstalledPackagesAsUser ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;
/* .line 306 */
/* .local v5, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_1
/* check-cast v7, Landroid/content/pm/PackageInfo; */
/* .line 307 */
/* .local v7, "app":Landroid/content/pm/PackageInfo; */
v8 = this.packageName;
if ( v8 != null) { // if-eqz v8, :cond_0
v8 = this.applicationInfo;
if ( v8 != null) { // if-eqz v8, :cond_0
v8 = this.mAppsPN;
v9 = this.packageName;
v8 = /* .line 308 */
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 309 */
/* iget v8, v4, Landroid/content/pm/UserInfo;->id:I */
v9 = this.applicationInfo;
/* iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I */
v8 = android.os.UserHandle .getUid ( v8,v9 );
/* .line 310 */
/* .local v8, "uid":I */
v9 = this.packageName;
/* invoke-direct {p0, v9, v8}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->addUidToMap(Ljava/lang/String;I)V */
/* .line 312 */
} // .end local v7 # "app":Landroid/content/pm/PackageInfo;
} // .end local v8 # "uid":I
} // :cond_0
/* .line 313 */
} // .end local v4 # "user":Landroid/content/pm/UserInfo;
} // .end local v5 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_1
/* .line 314 */
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateUidFromWholeAppMap()V */
/* .line 316 */
} // :cond_3
return;
} // .end method
private synchronized void updateGestureWhiteAppStatus ( java.lang.String p0, java.lang.String p1, Boolean p2, Boolean p3 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "extra" # Ljava/lang/String; */
/* .param p3, "newStatusOn" # Z */
/* .param p4, "oldStatusOn" # Z */
/* monitor-enter p0 */
/* .line 711 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateGestureWhiteAppStatus newStatusOn=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",oldStatusOn="; // const-string v1, ",oldStatusOn="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 712 */
/* if-eq p3, p4, :cond_0 */
/* .line 713 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->gestureAppNotification(Ljava/lang/String;Ljava/lang/String;Z)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 715 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 710 */
} // .end local p1 # "action":Ljava/lang/String;
} // .end local p2 # "extra":Ljava/lang/String;
} // .end local p3 # "newStatusOn":Z
} // .end local p4 # "oldStatusOn":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private synchronized void updateHongbaoModeStatus ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 398 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsHongbaoAppOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
} // :cond_0
/* move v0, v2 */
/* .line 399 */
/* .local v0, "isNewStatusOn":Z */
} // :goto_0
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastHongbaoApp:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_1
/* move v1, v2 */
/* .line 400 */
/* .local v1, "isOldStatusOn":Z */
} // :goto_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateHongbaoModeStatus isNewStatusOn=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ",isOldStatusOn="; // const-string v3, ",isOldStatusOn="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 401 */
/* if-eq v0, v1, :cond_2 */
/* .line 402 */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->enableHongbaoMode(Z)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 404 */
} // :cond_2
/* monitor-exit p0 */
return;
/* .line 397 */
} // .end local v0 # "isNewStatusOn":Z
} // .end local v1 # "isOldStatusOn":Z
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private synchronized void updateMobileTcEnabledListStatus ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 539 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileTcEnabledListOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
} // :cond_0
/* move v0, v2 */
/* .line 540 */
/* .local v0, "isNewStatusOn":Z */
} // :goto_0
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileTcEnabledListOn:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_1
/* move v1, v2 */
/* .line 541 */
/* .local v1, "isOldStatusOn":Z */
} // :goto_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateMobileTcEnabledListStatus isNewStatusOn=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ",isOldStatusOn="; // const-string v3, ",isOldStatusOn="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 542 */
/* if-eq v0, v1, :cond_2 */
/* .line 543 */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mobileTcEnabledStatusChanged(Z)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 545 */
} // :cond_2
/* monitor-exit p0 */
return;
/* .line 538 */
} // .end local v0 # "isNewStatusOn":Z
} // .end local v1 # "isOldStatusOn":Z
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private void updateSpecialAppList ( ) {
/* .locals 2 */
/* .line 319 */
v0 = this.mContext;
/* .line 320 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110300d5 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
this.LOCAL_THERMAL_SPECIAL_APP_LIST = v0;
/* .line 321 */
v0 = this.mContext;
/* .line 322 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110300d7 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
this.LOCAL_THROTTLE_SPECIAL_APP_LIST = v0;
/* .line 323 */
return;
} // .end method
private synchronized void updateSpeedWhiteListStatus ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 508 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsMobileNwOn:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsSpeedWhiteListOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
} // :cond_0
/* move v0, v2 */
/* .line 509 */
/* .local v0, "isNewStatusOn":Z */
} // :goto_0
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastMobileNw:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastSpeedWhiteList:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
} // :cond_1
/* move v1, v2 */
/* .line 510 */
/* .local v1, "isOldStatusOn":Z */
} // :goto_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateSpeedWhiteListStatus isNewStatusOn=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ",isOldStatusOn="; // const-string v3, ",isOldStatusOn="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 511 */
/* if-eq v0, v1, :cond_2 */
/* .line 512 */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->speedWhiteListAppNotification(Z)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 514 */
} // :cond_2
/* monitor-exit p0 */
return;
/* .line 507 */
} // .end local v0 # "isNewStatusOn":Z
} // .end local v1 # "isOldStatusOn":Z
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private synchronized void updateTputTestAppStatus ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 481 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mIsTputTestAppOn:Z */
/* .line 482 */
/* .local v0, "isNewStatusOn":Z */
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->mLastTputTestApp:Z */
/* .line 483 */
/* .local v1, "isOldStatusOn":Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "updateTputTestAppStatus isNewStatusOn=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ",isOldStatusOn="; // const-string v3, ",isOldStatusOn="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 484 */
/* if-eq v0, v1, :cond_0 */
/* .line 485 */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->tputTestAppNotification(Z)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 487 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 480 */
} // .end local v0 # "isNewStatusOn":Z
} // .end local v1 # "isOldStatusOn":Z
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private void updateUidFromWholeAppMap ( ) {
/* .locals 5 */
/* .line 443 */
v0 = this.mAppUid;
/* .line 444 */
v0 = v0 = this.mAppsPN;
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 445 */
} // :cond_0
v0 = this.mAppsPN;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 446 */
/* .local v1, "pn":Ljava/lang/String; */
v2 = /* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->getUidFromMap(Ljava/lang/String;)I */
/* .line 447 */
/* .local v2, "uid":I */
int v3 = -1; // const/4 v3, -0x1
/* if-eq v2, v3, :cond_1 */
/* .line 448 */
v3 = this.mAppUid;
java.lang.Integer .valueOf ( v2 );
/* .line 450 */
} // .end local v1 # "pn":Ljava/lang/String;
} // .end local v2 # "uid":I
} // :cond_1
/* .line 451 */
} // :cond_2
return;
} // .end method
private void whiteAppNotification ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Boolean p3 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .param p3, "extra" # Ljava/lang/String; */
/* .param p4, "enable" # Z */
/* .line 723 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "whiteAppNotification action = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", enable = "; // const-string v1, ", enable = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 724 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 725 */
/* .local v0, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v0 ).setAction ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 726 */
(( android.content.Intent ) v0 ).setPackage ( p2 ); // invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 727 */
(( android.content.Intent ) v0 ).putExtra ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 728 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 729 */
return;
} // .end method
/* # virtual methods */
public java.util.Set fetchMobileTcEnabledList ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 579 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "mobile_tc_enabled_list_pkg_name"; // const-string v1, "mobile_tc_enabled_list_pkg_name"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 581 */
/* .local v0, "pkgNames":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "fetchMobileTcEnabledList pkgNames="; // const-string v2, "fetchMobileTcEnabledList pkgNames="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 582 */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 583 */
/* new-instance v1, Ljava/util/HashSet; */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 584 */
/* .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .line 586 */
} // .end local v1 # "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :cond_0
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
} // .end method
public java.util.Set fetchSpeedAppWhiteList ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 287 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "fiveg_speed_white_list_pkg_name"; // const-string v1, "fiveg_speed_white_list_pkg_name"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 289 */
/* .local v0, "pkgNames":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "fetchSpeedAppWhiteList pkgNames="; // const-string v2, "fetchSpeedAppWhiteList pkgNames="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 290 */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 291 */
/* new-instance v1, Ljava/util/HashSet; */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 292 */
/* .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .line 294 */
} // .end local v1 # "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :cond_0
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
} // .end method
public void systemReady ( ) {
/* .locals 1 */
/* .line 152 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateSpecialAppList()V */
/* .line 153 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppList()V */
/* .line 154 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->initReceiver()V */
/* .line 155 */
v0 = com.android.server.net.MiuiNetworkPolicyAppBuckets .isCommonSceneRecognitionAllowed ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 156 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->registerSpeedWhiteList()V */
/* .line 158 */
} // :cond_0
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .isMobileTcFeatureAllowed ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 159 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->registerMobileTcEnabledList()V */
/* .line 161 */
} // :cond_1
return;
} // .end method
public void updateAppBucketsForUidStateChange ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "oldUidState" # I */
/* .param p3, "newUidState" # I */
/* .line 361 */
v0 = com.android.server.net.MiuiNetworkPolicyAppBuckets .isUidValidForQos ( p1 );
/* if-nez v0, :cond_0 */
/* .line 362 */
return;
/* .line 364 */
} // :cond_0
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isAppBucketsEnabledForUid(II)Z */
/* .line 365 */
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->isAppBucketsEnabledForUid(II)Z */
/* if-eq v0, v1, :cond_1 */
/* .line 366 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->appBucketsForUidStateChanged(II)V */
/* .line 368 */
} // :cond_1
return;
} // .end method
public void updateAppPN ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "installed" # Z */
/* .line 348 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateAppPN packageName=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",uid="; // const-string v1, ",uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",installed="; // const-string v1, ",installed="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->log(Ljava/lang/String;)V */
/* .line 349 */
v0 = this.mAppsPN;
v0 = if ( v0 != null) { // if-eqz v0, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 350 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 351 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->addUidToMap(Ljava/lang/String;I)V */
/* .line 353 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->removeUidFromMap(Ljava/lang/String;)V */
/* .line 355 */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateUidFromWholeAppMap()V */
/* .line 357 */
} // :cond_1
return;
} // .end method
