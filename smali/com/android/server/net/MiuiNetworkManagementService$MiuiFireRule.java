class com.android.server.net.MiuiNetworkManagementService$MiuiFireRule {
	 /* .source "MiuiNetworkManagementService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkManagementService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "MiuiFireRule" */
} // .end annotation
/* # instance fields */
java.lang.String pkgName;
Integer rule;
Integer setterPid;
Integer setterUid;
Integer uid;
/* # direct methods */
public com.android.server.net.MiuiNetworkManagementService$MiuiFireRule ( ) {
/* .locals 0 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "rule" # I */
/* .param p4, "setterUid" # I */
/* .param p5, "setterPid" # I */
/* .line 587 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 588 */
this.pkgName = p1;
/* .line 589 */
/* iput p2, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->uid:I */
/* .line 590 */
/* iput p3, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->rule:I */
/* .line 591 */
/* iput p4, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->setterUid:I */
/* .line 592 */
/* iput p5, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->setterPid:I */
/* .line 593 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 607 */
/* if-ne p0, p1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 608 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_2
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* if-eq v0, v1, :cond_1 */
/* .line 609 */
} // :cond_1
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule; */
/* .line 610 */
/* .local v0, "fireRule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule; */
v1 = this.pkgName;
v2 = this.pkgName;
v1 = java.util.Objects .equals ( v1,v2 );
/* .line 608 */
} // .end local v0 # "fireRule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
} // :cond_2
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer hashCode ( ) {
/* .locals 1 */
/* .line 615 */
v0 = this.pkgName;
/* filled-new-array {v0}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 597 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "{pkgName=\'" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.pkgName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", uid="; // const-string v1, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->uid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", rule="; // const-string v1, ", rule="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->rule:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", setterUid="; // const-string v1, ", setterUid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->setterUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", setterPid="; // const-string v1, ", setterPid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->setterPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
