class com.android.server.net.MiuiNetworkPolicyManagerService$31 implements java.lang.Runnable {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->onSleepModeWhitelistChange(IZ)Z */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
final Integer val$appId; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$31 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 2057 */
this.this$0 = p1;
/* iput p2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;->val$appId:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 6 */
/* .line 2060 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmUserManager ( v0 );
(( android.os.UserManager ) v0 ).getUsers ( ); // invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;
/* .line 2061 */
v1 = /* .local v0, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;" */
/* add-int/lit8 v1, v1, -0x1 */
/* .local v1, "ui":I */
} // :goto_0
/* if-ltz v1, :cond_0 */
/* .line 2062 */
/* check-cast v2, Landroid/content/pm/UserInfo; */
/* .line 2063 */
/* .local v2, "user":Landroid/content/pm/UserInfo; */
/* iget v3, v2, Landroid/content/pm/UserInfo;->id:I */
/* iget v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;->val$appId:I */
v3 = android.os.UserHandle .getUid ( v3,v4 );
/* .line 2064 */
/* .local v3, "uid":I */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeWhitelistUids ( v4 );
java.lang.Integer .valueOf ( v3 );
/* .line 2061 */
} // .end local v2 # "user":Landroid/content/pm/UserInfo;
} // .end local v3 # "uid":I
/* add-int/lit8 v1, v1, -0x1 */
/* .line 2066 */
} // .end local v1 # "ui":I
} // :cond_0
return;
} // .end method
