.class Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyQosUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setQos(ZII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

.field final synthetic val$add:Z

.field final synthetic val$protocol:I

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;IIZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 328
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    iput p2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$protocol:I

    iput p3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$uid:I

    iput-boolean p4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 331
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmNetMgrService(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$protocol:I

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$uid:I

    const/16 v3, 0xb8

    iget-boolean v4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->setQos(IIIZ)Z

    move-result v0

    .line 332
    .local v0, "rst":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setQos rst="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",add="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$uid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",protocol="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$protocol:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicyQosUtils"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    if-eqz v0, :cond_0

    .line 334
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$mupdateDscpStatus(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V

    .line 335
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$add:Z

    iget v3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$uid:I

    iget v4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;->val$protocol:I

    invoke-static {v1, v2, v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$mupdateQosInfo(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZII)V

    .line 337
    :cond_0
    return-void
.end method
