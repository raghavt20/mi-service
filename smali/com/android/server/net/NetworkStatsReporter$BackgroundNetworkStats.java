class com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats {
	 /* .source "NetworkStatsReporter.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/NetworkStatsReporter; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BackgroundNetworkStats" */
} // .end annotation
/* # instance fields */
private Long mMobileRxBytes;
private Long mMobileTotalBytes;
private Long mMobileTxBytes;
private java.lang.String mPackageName;
private Long mTotalBytes;
private Long mTotalRxBytes;
private Long mTotalTxBytes;
private Integer mUid;
private Long mWifiRxBytes;
private Long mWifiTotalBytes;
private Long mWifiTxBytes;
final com.android.server.net.NetworkStatsReporter this$0; //synthetic
/* # direct methods */
static Long -$$Nest$fgetmMobileRxBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileRxBytes:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmMobileTotalBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTotalBytes:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmMobileTxBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTxBytes:J */
/* return-wide v0 */
} // .end method
static java.lang.String -$$Nest$fgetmPackageName ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageName;
} // .end method
static Long -$$Nest$fgetmTotalBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalBytes:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmTotalRxBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalRxBytes:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmTotalTxBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalTxBytes:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmWifiRxBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiRxBytes:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmWifiTotalBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTotalBytes:J */
/* return-wide v0 */
} // .end method
static Long -$$Nest$fgetmWifiTxBytes ( com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTxBytes:J */
/* return-wide v0 */
} // .end method
public com.android.server.net.NetworkStatsReporter$BackgroundNetworkStats ( ) {
/* .locals 0 */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 266 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 267 */
/* iput p2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mUid:I */
/* .line 268 */
this.mPackageName = p3;
/* .line 269 */
return;
} // .end method
/* # virtual methods */
public void updateBytes ( android.net.NetworkTemplate p0, android.net.NetworkStats$Entry p1 ) {
/* .locals 4 */
/* .param p1, "networkTemplate" # Landroid/net/NetworkTemplate; */
/* .param p2, "entry" # Landroid/net/NetworkStats$Entry; */
/* .line 272 */
v0 = (( android.net.NetworkTemplate ) p1 ).getMatchRule ( ); // invoke-virtual {p1}, Landroid/net/NetworkTemplate;->getMatchRule()I
/* sparse-switch v0, :sswitch_data_0 */
/* .line 274 */
/* :sswitch_0 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTxBytes:J */
/* iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->txBytes:J */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTxBytes:J */
/* .line 275 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiRxBytes:J */
/* iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->rxBytes:J */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiRxBytes:J */
/* .line 276 */
/* iget-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTxBytes:J */
/* add-long/2addr v2, v0 */
/* iput-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTotalBytes:J */
/* .line 277 */
/* .line 279 */
/* :sswitch_1 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTxBytes:J */
/* iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->txBytes:J */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTxBytes:J */
/* .line 280 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileRxBytes:J */
/* iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->rxBytes:J */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileRxBytes:J */
/* .line 281 */
/* iget-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTxBytes:J */
/* add-long/2addr v2, v0 */
/* iput-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTotalBytes:J */
/* .line 282 */
/* nop */
/* .line 287 */
} // :goto_0
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalTxBytes:J */
/* iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->txBytes:J */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalTxBytes:J */
/* .line 288 */
/* iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalRxBytes:J */
/* iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->rxBytes:J */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalRxBytes:J */
/* .line 289 */
/* iget-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalTxBytes:J */
/* add-long/2addr v2, v0 */
/* iput-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalBytes:J */
/* .line 290 */
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1 */
/* 0x4 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
