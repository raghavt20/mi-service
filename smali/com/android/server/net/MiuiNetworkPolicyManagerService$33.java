class com.android.server.net.MiuiNetworkPolicyManagerService$33 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSleepModeEnableControlObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$33 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 2144 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .line 2147 */
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$msleepModeEnableControl ( v0 );
final String v1 = "MiuiNetworkPolicySleepMode"; // const-string v1, "MiuiNetworkPolicySleepMode"
if ( v0 != null) { // if-eqz v0, :cond_0
	 v0 = this.this$0;
	 v0 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnable ( v0 );
	 /* if-nez v0, :cond_0 */
	 /* .line 2148 */
	 v0 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mregisterSleepModeReceiver ( v0 );
	 /* .line 2149 */
	 v0 = this.this$0;
	 int v2 = 1; // const/4 v2, 0x1
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmSleepModeEnable ( v0,v2 );
	 /* .line 2150 */
	 final String v0 = "Sleep mode receiver register"; // const-string v0, "Sleep mode receiver register"
	 android.util.Log .d ( v1,v0 );
	 /* .line 2151 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnable ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 2152 */
	 v0 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$munregisterSleepModeReceiver ( v0 );
	 /* .line 2153 */
	 v0 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmSleepModeEnable ( v0,v2 );
	 /* .line 2154 */
	 v0 = this.this$0;
	 v0 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnter ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 2155 */
		 v0 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmSleepModeEnter ( v0,v2 );
		 /* .line 2156 */
		 v0 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mclearSleepModeWhitelistUidRules ( v0 );
		 /* .line 2157 */
		 v0 = this.this$0;
		 v2 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnter ( v0 );
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableSleepModeChain ( v0,v2 );
		 /* .line 2159 */
	 } // :cond_1
	 final String v0 = "Sleep mode receiver unregister"; // const-string v0, "Sleep mode receiver unregister"
	 android.util.Log .d ( v1,v0 );
	 /* .line 2161 */
} // :cond_2
} // :goto_0
return;
} // .end method
