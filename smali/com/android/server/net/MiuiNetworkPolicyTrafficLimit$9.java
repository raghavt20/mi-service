class com.android.server.net.MiuiNetworkPolicyTrafficLimit$9 extends android.content.BroadcastReceiver {
	 /* .source "MiuiNetworkPolicyTrafficLimit.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyTrafficLimit this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyTrafficLimit$9 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit; */
/* .line 423 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 426 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 427 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 428 */
	 final String v1 = "networkType"; // const-string v1, "networkType"
	 int v2 = 0; // const/4 v2, 0x0
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 429 */
	 /* .local v1, "networkType":I */
	 /* if-nez v1, :cond_0 */
	 /* .line 430 */
	 v2 = this.this$0;
	 final String v3 = "BroadcastReceiver TYPE_MOBILE"; // const-string v3, "BroadcastReceiver TYPE_MOBILE"
	 com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mlog ( v2,v3 );
	 /* .line 431 */
	 v2 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$misAllowedToEnableMobileTcMode ( v2 );
	 /* .line 434 */
} // .end local v1 # "networkType":I
} // :cond_0
return;
} // .end method
