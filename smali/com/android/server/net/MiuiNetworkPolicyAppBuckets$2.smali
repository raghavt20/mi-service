.class Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;
.super Landroid/content/BroadcastReceiver;
.source "MiuiNetworkPolicyAppBuckets.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    .line 183
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 186
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "action":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    const-string v2, "BroadcastReceiver action is null!"

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Ljava/lang/String;)V

    .line 189
    return-void

    .line 191
    :cond_0
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    const-string v1, "networkType"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 193
    .local v1, "networkType":I
    if-nez v1, :cond_2

    .line 194
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$mgetMobileLinkIface(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)Ljava/lang/String;

    move-result-object v2

    .line 195
    .local v2, "iface":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BroadcastReceiver iface="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Ljava/lang/String;)V

    .line 196
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$fputmIsMobileNwOn(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Z)V

    .line 197
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$fgetmLastMobileNw(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)Z

    move-result v3

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$fgetmIsMobileNwOn(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)Z

    move-result v4

    if-eq v3, v4, :cond_2

    .line 198
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$mupdateHongbaoModeStatus(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V

    .line 199
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$mupdateTputTestAppStatus(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V

    .line 200
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$mupdateSpeedWhiteListStatus(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V

    .line 201
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$mupdateMobileTcEnabledListStatus(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)V

    .line 202
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$fgetmIsMobileNwOn(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;)Z

    move-result v4

    invoke-static {v3, v4}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$fputmLastMobileNw(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Z)V

    goto :goto_0

    .line 205
    .end local v1    # "networkType":I
    .end local v2    # "iface":Ljava/lang/String;
    :cond_1
    const-string v1, "com.android.phone.intent.action.CLOUD_TELE_FEATURE_INFO_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 206
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;

    const-string v2, "TelephonyThermalThrottle"

    const-string v3, "Params2"

    invoke-static {v1, v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->-$$Nest$mupdate5gPowerWhiteApplist(Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 205
    :cond_2
    :goto_0
    nop

    .line 208
    :goto_1
    return-void
.end method
