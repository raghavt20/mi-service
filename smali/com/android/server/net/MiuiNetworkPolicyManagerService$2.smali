.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    .line 405
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 2
    .param p1, "network"    # Landroid/net/Network;

    .line 408
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onAvailable(Landroid/net/Network;)V

    .line 409
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$monVpnNetworkChanged(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;Z)V

    .line 410
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 2
    .param p1, "network"    # Landroid/net/Network;

    .line 414
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onLost(Landroid/net/Network;)V

    .line 415
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$monVpnNetworkChanged(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/net/Network;Z)V

    .line 416
    return-void
.end method
