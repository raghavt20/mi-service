class com.android.server.net.MiuiNetworkPolicyManagerService$26 extends android.content.BroadcastReceiver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$26 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .line 1656 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1659 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1660 */
/* .local v0, "action":Ljava/lang/String; */
(( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 1661 */
/* .local v1, "data":Landroid/net/Uri; */
/* if-nez v1, :cond_0 */
/* .line 1662 */
return;
/* .line 1664 */
} // :cond_0
(( android.net.Uri ) v1 ).getSchemeSpecificPart ( ); // invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
/* .line 1665 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1666 */
return;
/* .line 1669 */
} // :cond_1
final String v3 = "android.intent.extra.UID"; // const-string v3, "android.intent.extra.UID"
int v4 = -1; // const/4 v4, -0x1
v3 = (( android.content.Intent ) p2 ).getIntExtra ( v3, v4 ); // invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 1670 */
/* .local v3, "uid":I */
/* if-ne v3, v4, :cond_2 */
/* .line 1671 */
return;
/* .line 1674 */
} // :cond_2
final String v4 = "android.intent.action.PACKAGE_ADDED"; // const-string v4, "android.intent.action.PACKAGE_ADDED"
v4 = (( java.lang.String ) v4 ).equals ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v5 = "MiuiNetworkPolicy"; // const-string v5, "MiuiNetworkPolicy"
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 1675 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "ACTION_PACKAGE_ADDED uid = "; // const-string v6, "ACTION_PACKAGE_ADDED uid = "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v4 );
/* .line 1676 */
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmUnRestrictAppsPN ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1677 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmUnRestrictApps ( v4 );
java.lang.Integer .valueOf ( v3 );
/* .line 1679 */
} // :cond_3
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLowLatencyAppsPN ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1680 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLowLatencyApps ( v4 );
java.lang.Integer .valueOf ( v3 );
/* .line 1682 */
} // :cond_4
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNeedRestrictPowerSaveAppsPN ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 1683 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNeedRestrictPowerSaveApps ( v4 );
java.lang.Integer .valueOf ( v3 );
/* .line 1685 */
} // :cond_5
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$smisMobileLatencyAllowed ( );
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 1686 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileLowLatencyAppsPN ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_6
	 v4 = this.this$0;
	 v4 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileLowLatencyAppsPN ( v4 );
	 if ( v4 != null) { // if-eqz v4, :cond_6
		 /* .line 1687 */
		 v4 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileLowLatencyApps ( v4 );
		 java.lang.Integer .valueOf ( v3 );
		 /* .line 1690 */
	 } // :cond_6
	 v4 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmQosUtils ( v4 );
	 int v5 = 1; // const/4 v5, 0x1
	 if ( v4 != null) { // if-eqz v4, :cond_7
		 v4 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmQosUtils ( v4 );
		 (( com.android.server.net.MiuiNetworkPolicyQosUtils ) v4 ).updateAppPN ( v2, v3, v5 ); // invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateAppPN(Ljava/lang/String;IZ)V
		 /* .line 1691 */
	 } // :cond_7
	 v4 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmAppBuckets ( v4 );
	 if ( v4 != null) { // if-eqz v4, :cond_8
		 v4 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmAppBuckets ( v4 );
		 (( com.android.server.net.MiuiNetworkPolicyAppBuckets ) v4 ).updateAppPN ( v2, v3, v5 ); // invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppPN(Ljava/lang/String;IZ)V
		 /* .line 1692 */
	 } // :cond_8
	 v4 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileTcUtils ( v4 );
	 if ( v4 != null) { // if-eqz v4, :cond_10
		 v4 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileTcUtils ( v4 );
		 (( com.android.server.net.MiuiNetworkPolicyTrafficLimit ) v4 ).updateAppPN ( v2, v3, v5 ); // invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateAppPN(Ljava/lang/String;IZ)V
		 /* goto/16 :goto_0 */
		 /* .line 1693 */
	 } // :cond_9
	 final String v4 = "android.intent.action.PACKAGE_REMOVED"; // const-string v4, "android.intent.action.PACKAGE_REMOVED"
	 v4 = 	 (( java.lang.String ) v4 ).equals ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v4 != null) { // if-eqz v4, :cond_10
		 /* .line 1694 */
		 /* new-instance v4, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v6 = "ACTION_PACKAGE_REMOVED uid = "; // const-string v6, "ACTION_PACKAGE_REMOVED uid = "
		 (( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .i ( v5,v4 );
		 /* .line 1695 */
		 v4 = this.this$0;
		 v4 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmUnRestrictAppsPN ( v4 );
		 if ( v4 != null) { // if-eqz v4, :cond_a
			 /* .line 1696 */
			 v4 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmUnRestrictApps ( v4 );
			 java.lang.Integer .valueOf ( v3 );
			 /* .line 1698 */
		 } // :cond_a
		 v4 = this.this$0;
		 v4 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLowLatencyAppsPN ( v4 );
		 if ( v4 != null) { // if-eqz v4, :cond_b
			 /* .line 1699 */
			 v4 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLowLatencyApps ( v4 );
			 java.lang.Integer .valueOf ( v3 );
			 /* .line 1701 */
		 } // :cond_b
		 v4 = this.this$0;
		 v4 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNeedRestrictPowerSaveAppsPN ( v4 );
		 if ( v4 != null) { // if-eqz v4, :cond_c
			 /* .line 1702 */
			 v4 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNeedRestrictPowerSaveApps ( v4 );
			 java.lang.Integer .valueOf ( v3 );
			 /* .line 1704 */
		 } // :cond_c
		 v4 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$smisMobileLatencyAllowed ( );
		 if ( v4 != null) { // if-eqz v4, :cond_d
			 /* .line 1705 */
			 v4 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileLowLatencyAppsPN ( v4 );
			 if ( v4 != null) { // if-eqz v4, :cond_d
				 v4 = this.this$0;
				 v4 = 				 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileLowLatencyAppsPN ( v4 );
				 if ( v4 != null) { // if-eqz v4, :cond_d
					 /* .line 1706 */
					 v4 = this.this$0;
					 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileLowLatencyApps ( v4 );
					 java.lang.Integer .valueOf ( v3 );
					 /* .line 1709 */
				 } // :cond_d
				 v4 = this.this$0;
				 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmQosUtils ( v4 );
				 int v5 = 0; // const/4 v5, 0x0
				 if ( v4 != null) { // if-eqz v4, :cond_e
					 v4 = this.this$0;
					 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmQosUtils ( v4 );
					 (( com.android.server.net.MiuiNetworkPolicyQosUtils ) v4 ).updateAppPN ( v2, v3, v5 ); // invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateAppPN(Ljava/lang/String;IZ)V
					 /* .line 1710 */
				 } // :cond_e
				 v4 = this.this$0;
				 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmAppBuckets ( v4 );
				 if ( v4 != null) { // if-eqz v4, :cond_f
					 v4 = this.this$0;
					 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmAppBuckets ( v4 );
					 (( com.android.server.net.MiuiNetworkPolicyAppBuckets ) v4 ).updateAppPN ( v2, v3, v5 ); // invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppPN(Ljava/lang/String;IZ)V
					 /* .line 1711 */
				 } // :cond_f
				 v4 = this.this$0;
				 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileTcUtils ( v4 );
				 if ( v4 != null) { // if-eqz v4, :cond_10
					 v4 = this.this$0;
					 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMobileTcUtils ( v4 );
					 (( com.android.server.net.MiuiNetworkPolicyTrafficLimit ) v4 ).updateAppPN ( v2, v3, v5 ); // invoke-virtual {v4, v2, v3, v5}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateAppPN(Ljava/lang/String;IZ)V
					 /* .line 1713 */
				 } // :cond_10
			 } // :goto_0
			 return;
		 } // .end method
