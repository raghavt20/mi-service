class com.android.server.net.MiuiNetworkPolicyQosUtils$2 implements java.lang.Runnable {
	 /* .source "MiuiNetworkPolicyQosUtils.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableQos(Z)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyQosUtils this$0; //synthetic
final Boolean val$enable; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyQosUtils$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyQosUtils; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 312 */
this.this$0 = p1;
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->val$enable:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 315 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmNetMgrService ( v0 );
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->val$enable:Z */
v0 = (( com.android.server.net.MiuiNetworkManagementService ) v0 ).enableQos ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableQos(Z)Z
/* .line 316 */
/* .local v0, "rst":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enableQos rst="; // const-string v2, "enableQos rst="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicyQosUtils"; // const-string v2, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v2,v1 );
/* .line 317 */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = this.this$0;
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->val$enable:Z */
com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$mupdateDscpStatus ( v1,v2 );
/* .line 318 */
} // :cond_0
return;
} // .end method
