.class public Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyQosUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;,
        Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;
    }
.end annotation


# static fields
.field private static final ACTION_CLOUD_MEETING_LIST_DONE:Ljava/lang/String; = "com.android.phone.intent.action.CLOUD_MEETING_LIST_DONE"

.field private static final ACTIVITY_STATUS:Ljava/lang/String; = "com.android.phone.intent.action.ACTIVITY_STATUS"

.field private static final AN_WECHAT_SCAN_UI:Ljava/lang/String; = "com.tencent.mm.plugin.scanner.ui.BaseScanUI"

.field private static final DEBUG:Z = true

.field private static final DSCP_DEFAULT_STATE:I = 0x0

.field private static final DSCP_INIT_STATE:I = 0x1

.field private static final DSCP_QOS_STATE:I = 0x2

.field private static final DSCP_TOS_EF:I = 0xb8

.field private static final EF_AN_TIKTOK:Ljava/lang/String; = "com.ss.android.ugc.aweme.main.MainActivity"

.field private static final EF_AN_WECHAT:Ljava/lang/String; = "com.tencent.mm.plugin.voip.ui.VideoActivity"

.field private static final EF_MIMO_AN:[Ljava/lang/String;

.field private static final EF_MIMO_PN:[Ljava/lang/String;

.field private static final EF_PN_TIKTOK:Ljava/lang/String; = "com.ss.android.ugc.aweme"

.field private static final EF_PN_WECHAT:Ljava/lang/String; = "com.tencent.mm"

.field private static final GAME_ENABLED:Ljava/lang/String; = "gameEnabled"

.field private static final GAME_SCENE:Ljava/lang/String; = "com.android.phone.intent.action.GAME_SCENE"

.field private static final IP:I

.field public static final IS_QCOM:Z

.field private static final LATENCY_ACTION_CHANGE_LEVEL:Ljava/lang/String; = "com.android.phone.intent.action.CHANGE_LEVEL"

.field private static final LATENCY_KEY_SCENE:Ljava/lang/String; = "scene"

.field private static final LOCAL_EF_APP_LIST:[Ljava/lang/String;

.field private static final MEETING_OPTIMIZATION_WHITE_LIST_DEFAULT:[Ljava/lang/String;

.field private static final MEETING_VOIP_ENABLED:Ljava/lang/String; = "meetingVoipEnabled"

.field private static final MEETING_VOIP_SCENE:Ljava/lang/String; = "com.android.phone.intent.action.MEETING_VOIP_SCENE"

.field private static final MIMO_SCENE_EXTRA:Ljava/lang/String; = "mimo_scene"

.field private static final NOTIFACATION_RECEIVER_PACKAGE:Ljava/lang/String; = "com.android.phone"

.field private static final SCENE_DEFAULT:I = -0x1

.field private static final SCENE_VOIP:I = 0x3

.field private static final SCENE_WECHAT_SCAN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MiuiNetworkPolicyQosUtils"

.field private static final UDP:I

.field private static final WECHAT_VOIP_ENABLED:Ljava/lang/String; = "wechatVoipEnabled"

.field private static final WECHAT_VOIP_SCENE:Ljava/lang/String; = "com.android.phone.intent.action.WECHAT_VOIP_SCENE"

.field private static final mActivityStatusScene:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;",
            ">;"
        }
    .end annotation
.end field

.field private static mMeetingAppsAN:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mMeetingAppsPN:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

.field private final mContext:Landroid/content/Context;

.field private mDscpState:I

.field private mEFAppsPN:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsMobileDataOn:Z

.field private mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

.field private mQosInfo:Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

.field private mUidMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmNetMgrService(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Lcom/android/server/net/MiuiNetworkManagementService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmQosInfo(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mQosInfo:Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$menableLLMMode(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableLLMMode(ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetUidFromMap(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->getUidFromMap(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$minitMeetingWhiteList(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->initMeetingWhiteList(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$misActivityNameContains(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isActivityNameContains(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mprocessMeetingListChanged(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->processMeetingListChanged()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mregisterActivityChangeListener(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->registerActivityChangeListener()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetQos(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setQos(ZII)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDscpStatus(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateDscpStatus(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateQosInfo(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosInfo(ZII)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateVoipStatus(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateVoipStatus(Ljava/lang/String;ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetUDP()I
    .locals 1

    sget v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->UDP:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmActivityStatusScene()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mActivityStatusScene:Ljava/util/ArrayList;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmMeetingAppsAN()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsAN:Ljava/util/List;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetmMeetingAppsPN()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 11

    .line 75
    const-string v0, "com.tencent.tmgp.pubgmhd"

    const-string v1, "com.tencent.lolm"

    const-string v2, "com.tencent.tmgp.sgame"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->LOCAL_EF_APP_LIST:[Ljava/lang/String;

    .line 86
    const-string v0, "com.tencent.mm"

    const-string v1, "com.ss.android.ugc.aweme"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->EF_MIMO_PN:[Ljava/lang/String;

    .line 89
    const-string v1, "com.tencent.mm.plugin.voip.ui.VideoActivity"

    const-string v2, "com.ss.android.ugc.aweme.main.MainActivity"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->EF_MIMO_AN:[Ljava/lang/String;

    .line 100
    sget v2, Landroid/system/OsConstants;->IPPROTO_IP:I

    sput v2, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->IP:I

    .line 101
    sget v2, Landroid/system/OsConstants;->IPPROTO_UDP:I

    sput v2, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->UDP:I

    .line 104
    const-string/jumbo v2, "vendor"

    invoke-static {v2}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "qcom"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->IS_QCOM:Z

    .line 118
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mActivityStatusScene:Ljava/util/ArrayList;

    .line 120
    new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;

    const-string v4, "mimo_scene"

    invoke-direct {v3, v0, v1, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;-><init>([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsAN:Ljava/util/List;

    .line 141
    const-string v1, "com.tencent.mm"

    const-string v2, "com.tencent.mm.plugin.voip.ui.VideoActivity"

    const-string v3, "com.tencent.wemeet.app"

    const-string v4, "com.tencent.wemeet.sdk.meeting.inmeeting.InMeetingActivity"

    const-string v5, "com.ss.android.lark"

    const-string v6, "com.ss.android.vc.meeting.module.multi.ByteRTCMeetingActivityInstance"

    const-string v7, "com.alibaba.android.rimet"

    const-string v8, "com.alibaba.android.teleconf.mozi.activity.TeleVideoConfActivity"

    const-string v9, "com.ss.android.lark.kami"

    const-string v10, "com.ss.android.vc.meeting.module.multi.ByteRTCMeetingActivity"

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->MEETING_OPTIMIZATION_WHITE_LIST_DEFAULT:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z

    .line 160
    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;

    invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

    .line 506
    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;

    invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 155
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    .line 156
    iput-object p2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mHandler:Landroid/os/Handler;

    .line 157
    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V

    .line 158
    return-void
.end method

.method private addUidToMap(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 398
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    :cond_0
    return-void
.end method

.method private enableLLMMode(ZI)V
    .locals 3
    .param p1, "enable"    # Z
    .param p2, "scene"    # I

    .line 617
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableLLMMode enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",scene="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicyQosUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 619
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.phone.intent.action.CHANGE_LEVEL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 620
    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 621
    const-string v1, "scene"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 622
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 623
    return-void
.end method

.method private declared-synchronized enableQos(Z)V
    .locals 2
    .param p1, "enable"    # Z

    monitor-enter p0

    .line 308
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosReadyForEnable(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    if-nez v0, :cond_0

    goto :goto_0

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;

    invoke-direct {v1, p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    monitor-exit p0

    return-void

    .line 309
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
    :cond_1
    :goto_0
    :try_start_1
    const-string v0, "MiuiNetworkPolicyQosUtils"

    const-string v1, "enableQos return by invalid value!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    monitor-exit p0

    return-void

    .line 307
    .end local p1    # "enable":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private gameSceneNotification(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 638
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    return-void

    .line 639
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gameSceneNotification enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicyQosUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 641
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.phone.intent.action.GAME_SCENE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 642
    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 643
    const-string v1, "gameEnabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 644
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 645
    return-void
.end method

.method private getEFApps()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 370
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 371
    .local v0, "appList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->LOCAL_EF_APP_LIST:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 372
    aget-object v2, v2, v1

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 371
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 374
    .end local v1    # "i":I
    :cond_0
    return-object v0
.end method

.method private getUidFromMap(Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 404
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 405
    const/4 v0, -0x1

    return v0

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private hasUidFromMap(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 411
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->LOCAL_EF_APP_LIST:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 412
    aget-object v1, v1, v0

    invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->getUidFromMap(Ljava/lang/String;)I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 413
    const/4 v1, 0x1

    return v1

    .line 411
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 416
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private initDefaultMeetingWhiteList()V
    .locals 6

    .line 256
    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->MEETING_OPTIMIZATION_WHITE_LIST_DEFAULT:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 257
    .local v0, "defaultName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 258
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 259
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 260
    .local v2, "flag":Z
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 261
    .local v4, "str":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 262
    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 264
    :cond_0
    const/4 v2, 0x1

    .line 266
    :goto_1
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    .end local v4    # "str":Ljava/lang/String;
    goto :goto_0

    .line 268
    :cond_1
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 269
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 268
    const-string v5, "meeting_optimization_white_list_pkg_name"

    invoke-static {v3, v5, v4}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 271
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "flag":Z
    :cond_2
    return-void
.end method

.method private initMeetingWhiteList(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 234
    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsAN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 235
    :cond_0
    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 236
    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsAN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 238
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "meeting_optimization_white_list_pkg_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "pkgNames":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 240
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->initDefaultMeetingWhiteList()V

    .line 241
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initMeetingWhiteList pkgNames="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicyQosUtils"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, "meetings":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-gt v2, v3, :cond_4

    .line 246
    rem-int/lit8 v3, v2, 0x2

    if-nez v3, :cond_3

    .line 247
    sget-object v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 249
    :cond_3
    sget-object v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsAN:Ljava/util/List;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 252
    .end local v2    # "i":I
    :cond_4
    return-void
.end method

.method private isActivityNameContains(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 2
    .param p1, "anString"    # Ljava/lang/String;
    .param p2, "sceneString"    # [Ljava/lang/String;

    .line 211
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_1

    .line 212
    aget-object v1, p2, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    const/4 v1, 0x1

    return v1

    .line 211
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 216
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private isQosEnabledForUid(II)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 449
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->hasUidFromMap(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isQosFeatureEnabled()Z
    .locals 4

    .line 588
    const-string v0, "cepheus"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    const-string v0, "raphael"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 589
    const-string v3, "davinci"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "crux"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 590
    const-string/jumbo v3, "tucana"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "cmi"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "umi"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 591
    const-string v3, "picasso"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "phoenix"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "lmi"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 592
    const-string v3, "lmipro"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "vangogh"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "cas"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 593
    const-string v3, "apollo"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v2

    .line 594
    .local v0, "deviceRst":Z
    :goto_1
    sget-boolean v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->IS_QCOM:Z

    if-eqz v3, :cond_2

    move v3, v2

    goto :goto_2

    :cond_2
    move v3, v1

    .line 595
    .local v3, "isQosAllowed":Z
    :goto_2
    if-nez v0, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    move v1, v2

    :cond_4
    return v1
.end method

.method private isQosReadyForEnable(Z)Z
    .locals 3
    .param p1, "action"    # Z

    .line 350
    const/4 v0, 0x0

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I

    if-eqz p1, :cond_0

    if-nez v2, :cond_1

    goto :goto_0

    :cond_0
    if-ne v2, v1, :cond_1

    :goto_0
    move v0, v1

    :cond_1
    return v0
.end method

.method private isQosReadyForSetValue(Z)Z
    .locals 4
    .param p1, "action"    # Z

    .line 346
    const/4 v0, 0x0

    const/4 v1, 0x1

    iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I

    if-eqz p1, :cond_0

    if-ne v2, v1, :cond_1

    goto :goto_0

    :cond_0
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :goto_0
    move v0, v1

    :cond_1
    return v0
.end method

.method private static isUidValidForQos(I)Z
    .locals 1
    .param p0, "uid"    # I

    .line 304
    invoke-static {p0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    return v0
.end method

.method private meetingVoipNotification(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "meetingVoipNotification enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicyQosUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 580
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.phone.intent.action.MEETING_VOIP_SCENE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 581
    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 582
    const-string v1, "meetingVoipEnabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 583
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 584
    return-void
.end method

.method private mobileTcEnabledStatusChanged(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 626
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mobileTcEnabledStatusChanged for wechat enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicyQosUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mHandler:Landroid/os/Handler;

    .line 629
    nop

    .line 627
    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 630
    return-void
.end method

.method private processMeetingListChanged()V
    .locals 2

    .line 533
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$5;

    invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$5;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 540
    return-void
.end method

.method private registerActivityChangeListener()V
    .locals 6

    .line 274
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

    if-eqz v0, :cond_7

    .line 275
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 276
    .local v0, "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v1, "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mActivityStatusScene:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;

    .line 278
    .local v3, "as":Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v5, v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->EF_PN:[Ljava/lang/String;

    array-length v5, v5

    if-ge v4, v5, :cond_1

    .line 279
    iget-object v5, v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->EF_PN:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 280
    iget-object v5, v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->EF_PN:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 278
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 283
    .end local v4    # "i":I
    :cond_1
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    iget-object v5, v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->EF_AN:[Ljava/lang/String;

    array-length v5, v5

    if-ge v4, v5, :cond_3

    .line 284
    iget-object v5, v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->EF_AN:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 285
    iget-object v5, v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->EF_AN:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 288
    .end local v3    # "as":Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;
    .end local v4    # "i":I
    :cond_3
    goto :goto_0

    .line 290
    :cond_4
    sget-object v2, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 291
    .local v3, "meetingAppsPN":Ljava/lang/String;
    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 292
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    .end local v3    # "meetingAppsPN":Ljava/lang/String;
    :cond_5
    goto :goto_3

    .line 295
    :cond_6
    const-string v2, "com.tencent.mm.plugin.scanner.ui.BaseScanUI"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

    invoke-static {v2}, Lmiui/process/ProcessManager;->unregisterActivityChanageListener(Lmiui/process/IActivityChangeListener;)V

    .line 297
    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mActivityChangeListener:Lmiui/process/IActivityChangeListener$Stub;

    invoke-static {v1, v0, v2}, Lmiui/process/ProcessManager;->registerActivityChangeListener(Ljava/util/List;Ljava/util/List;Lmiui/process/IActivityChangeListener;)V

    .line 300
    .end local v0    # "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_7
    return-void
.end method

.method private removeAll()V
    .locals 1

    .line 424
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 425
    return-void
.end method

.method private removeUidFromMap(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 420
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mUidMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    return-void
.end method

.method private setDscpStatus(I)V
    .locals 0
    .param p1, "dscpStatus"    # I

    .line 342
    iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I

    .line 343
    return-void
.end method

.method private declared-synchronized setQos(ZII)V
    .locals 2
    .param p1, "add"    # Z
    .param p2, "uid"    # I
    .param p3, "protocol"    # I

    monitor-enter p0

    .line 323
    :try_start_0
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosFeatureEnabled()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    .line 324
    :cond_0
    :try_start_1
    invoke-static {p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isUidValidForQos(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosReadyForSetValue(Z)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 328
    :cond_1
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;

    invoke-direct {v1, p0, p3, p2, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;IIZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 339
    monitor-exit p0

    return-void

    .line 325
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
    :cond_2
    :goto_0
    :try_start_2
    const-string v0, "MiuiNetworkPolicyQosUtils"

    const-string/jumbo v1, "setQos return by invalid value!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 326
    monitor-exit p0

    return-void

    .line 322
    .end local p1    # "add":Z
    .end local p2    # "uid":I
    .end local p3    # "protocol":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private updateDscpStatus(Z)V
    .locals 2
    .param p1, "action"    # Z

    .line 354
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 355
    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I

    if-nez v1, :cond_0

    .line 356
    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V

    goto :goto_0

    .line 358
    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V

    goto :goto_0

    .line 361
    :cond_1
    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I

    if-ne v1, v0, :cond_2

    .line 362
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V

    goto :goto_0

    .line 364
    :cond_2
    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V

    .line 367
    :goto_0
    return-void
.end method

.method private updateQosForUidState(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "state"    # I

    .line 453
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosEnabledForUid(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z

    if-eqz v0, :cond_0

    .line 454
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateQosForUidState mIsMobileDataOn"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicyQosUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->gameSceneNotification(Z)V

    .line 456
    sget v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->IP:I

    invoke-direct {p0, v0, p1, v1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setQos(ZII)V

    goto :goto_0

    .line 458
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->gameSceneNotification(Z)V

    .line 459
    sget v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->IP:I

    invoke-direct {p0, v0, p1, v1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setQos(ZII)V

    .line 461
    :goto_0
    return-void
.end method

.method private updateQosInfo(ZII)V
    .locals 2
    .param p1, "enabled"    # Z
    .param p2, "uid"    # I
    .param p3, "protocol"    # I

    .line 543
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mQosInfo:Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    if-nez v0, :cond_0

    return-void

    .line 544
    :cond_0
    const-string v1, "MiuiNetworkPolicyQosUtils"

    if-eqz p1, :cond_2

    .line 545
    invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getQosStatus()Z

    move-result v0

    if-nez v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mQosInfo:Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->updateAll(ZII)V

    goto :goto_0

    .line 548
    :cond_1
    const-string/jumbo v0, "updateQosInfo enabled but already true"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 551
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getQosStatus()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 552
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mQosInfo:Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->clear()V

    goto :goto_0

    .line 554
    :cond_3
    const-string/jumbo v0, "updateQosInfo disable but already false"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    :goto_0
    return-void
.end method

.method private updateQosUid()V
    .locals 10

    .line 378
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 379
    .local v0, "um":Landroid/os/UserManager;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 380
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v2

    .line 381
    .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->getEFApps()Ljava/util/Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mEFAppsPN:Ljava/util/Set;

    .line 382
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 383
    :cond_0
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->removeAll()V

    .line 384
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/UserInfo;

    .line 385
    .local v4, "user":Landroid/content/pm/UserInfo;
    const/4 v5, 0x0

    iget v6, v4, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v1, v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;

    move-result-object v5

    .line 386
    .local v5, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageInfo;

    .line 387
    .local v7, "app":Landroid/content/pm/PackageInfo;
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v8, :cond_2

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mEFAppsPN:Ljava/util/Set;

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 388
    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    sget-object v8, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 389
    :cond_1
    iget v8, v4, Landroid/content/pm/UserInfo;->id:I

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v8, v9}, Landroid/os/UserHandle;->getUid(II)I

    move-result v8

    .line 390
    .local v8, "uid":I
    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v9, v8}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->addUidToMap(Ljava/lang/String;I)V

    .line 392
    .end local v7    # "app":Landroid/content/pm/PackageInfo;
    .end local v8    # "uid":I
    :cond_2
    goto :goto_1

    .line 393
    .end local v4    # "user":Landroid/content/pm/UserInfo;
    .end local v5    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_3
    goto :goto_0

    .line 395
    :cond_4
    return-void
.end method

.method private declared-synchronized updateVoipStatus(Ljava/lang/String;ZI)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "enableStatus"    # Z
    .param p3, "index"    # I

    monitor-enter p0

    .line 599
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsAN:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 600
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileTcFeatureAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mobileTcEnabledStatusChanged(Z)V

    .line 603
    .end local p0    # "this":Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
    :cond_0
    const-string v0, "com.tencent.mm.plugin.voip.ui.VideoActivity"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 604
    invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->weChatVoipNotification(Z)V

    goto :goto_0

    .line 606
    :cond_1
    invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->meetingVoipNotification(Z)V

    .line 609
    :cond_2
    :goto_0
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->isPlatform8550()Z

    move-result v0

    if-nez v0, :cond_3

    .line 610
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->isPlatform8650()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 611
    :cond_3
    const-string v0, "MiuiNetworkPolicyQosUtils"

    const-string v1, "8550 platform DE3.0"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    if-eqz p2, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_4
    const/4 v0, -0x1

    :goto_1
    invoke-direct {p0, p2, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableLLMMode(ZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614
    :cond_5
    monitor-exit p0

    return-void

    .line 598
    .end local p1    # "name":Ljava/lang/String;
    .end local p2    # "enableStatus":Z
    .end local p3    # "index":I
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private weChatVoipNotification(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .line 566
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "weChatVoipNotification enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicyQosUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 568
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.phone.intent.action.WECHAT_VOIP_SCENE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 569
    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 570
    const-string/jumbo v1, "wechatVoipEnabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 571
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 572
    return-void
.end method


# virtual methods
.method public systemReady(Lcom/android/server/net/MiuiNetworkManagementService;)V
    .locals 5
    .param p1, "networkMgr"    # Lcom/android/server/net/MiuiNetworkManagementService;

    .line 220
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mNetMgrService:Lcom/android/server/net/MiuiNetworkManagementService;

    .line 221
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->initMeetingWhiteList(Landroid/content/Context;)V

    .line 222
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableQos(Z)V

    .line 223
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->registerActivityChangeListener()V

    .line 224
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableQos(Z)V

    .line 225
    invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosUid()V

    .line 226
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 227
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 228
    const-string v1, "com.android.phone.intent.action.CLOUD_MEETING_LIST_DONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 229
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 230
    new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V

    iput-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mQosInfo:Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;

    .line 231
    return-void
.end method

.method public updateAppPN(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "action"    # Z

    .line 428
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mEFAppsPN:Ljava/util/Set;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mMeetingAppsPN:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 429
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 430
    :cond_1
    if-eqz p3, :cond_2

    .line 431
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->addUidToMap(Ljava/lang/String;I)V

    goto :goto_0

    .line 433
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->removeUidFromMap(Ljava/lang/String;)V

    .line 436
    :cond_3
    :goto_0
    return-void
.end method

.method public updateQosForUidStateChange(III)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "oldUidState"    # I
    .param p3, "newUidState"    # I

    .line 439
    invoke-static {p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isUidValidForQos(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 440
    return-void

    .line 442
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosEnabledForUid(II)Z

    move-result v0

    .line 443
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosEnabledForUid(II)Z

    move-result v1

    if-eq v0, v1, :cond_1

    .line 444
    invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosForUidState(II)V

    .line 446
    :cond_1
    return-void
.end method
