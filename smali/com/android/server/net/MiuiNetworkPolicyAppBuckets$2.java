class com.android.server.net.MiuiNetworkPolicyAppBuckets$2 extends android.content.BroadcastReceiver {
	 /* .source "MiuiNetworkPolicyAppBuckets.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyAppBuckets; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyAppBuckets this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyAppBuckets$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyAppBuckets; */
/* .line 183 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 186 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 187 */
/* .local v0, "action":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 188 */
	 v1 = this.this$0;
	 final String v2 = "BroadcastReceiver action is null!"; // const-string v2, "BroadcastReceiver action is null!"
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$mlog ( v1,v2 );
	 /* .line 189 */
	 return;
	 /* .line 191 */
} // :cond_0
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 192 */
	 final String v1 = "networkType"; // const-string v1, "networkType"
	 int v2 = 0; // const/4 v2, 0x0
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 193 */
	 /* .local v1, "networkType":I */
	 /* if-nez v1, :cond_2 */
	 /* .line 194 */
	 v2 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$mgetMobileLinkIface ( v2 );
	 /* .line 195 */
	 /* .local v2, "iface":Ljava/lang/String; */
	 v3 = this.this$0;
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "BroadcastReceiver iface="; // const-string v5, "BroadcastReceiver iface="
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$mlog ( v3,v4 );
	 /* .line 196 */
	 v3 = this.this$0;
	 v4 = 	 android.text.TextUtils .isEmpty ( v2 );
	 /* xor-int/lit8 v4, v4, 0x1 */
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$fputmIsMobileNwOn ( v3,v4 );
	 /* .line 197 */
	 v3 = this.this$0;
	 v3 = 	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$fgetmLastMobileNw ( v3 );
	 v4 = this.this$0;
	 v4 = 	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$fgetmIsMobileNwOn ( v4 );
	 /* if-eq v3, v4, :cond_2 */
	 /* .line 198 */
	 v3 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$mupdateHongbaoModeStatus ( v3 );
	 /* .line 199 */
	 v3 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$mupdateTputTestAppStatus ( v3 );
	 /* .line 200 */
	 v3 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$mupdateSpeedWhiteListStatus ( v3 );
	 /* .line 201 */
	 v3 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$mupdateMobileTcEnabledListStatus ( v3 );
	 /* .line 202 */
	 v3 = this.this$0;
	 v4 = 	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$fgetmIsMobileNwOn ( v3 );
	 com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$fputmLastMobileNw ( v3,v4 );
	 /* .line 205 */
} // .end local v1 # "networkType":I
} // .end local v2 # "iface":Ljava/lang/String;
} // :cond_1
final String v1 = "com.android.phone.intent.action.CLOUD_TELE_FEATURE_INFO_CHANGED"; // const-string v1, "com.android.phone.intent.action.CLOUD_TELE_FEATURE_INFO_CHANGED"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 206 */
v1 = this.this$0;
final String v2 = "TelephonyThermalThrottle"; // const-string v2, "TelephonyThermalThrottle"
final String v3 = "Params2"; // const-string v3, "Params2"
com.android.server.net.MiuiNetworkPolicyAppBuckets .-$$Nest$mupdate5gPowerWhiteApplist ( v1,v2,v3 );
/* .line 205 */
} // :cond_2
} // :goto_0
/* nop */
/* .line 208 */
} // :goto_1
return;
} // .end method
