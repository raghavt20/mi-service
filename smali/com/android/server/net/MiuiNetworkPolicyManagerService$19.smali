.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;
.super Landroid/database/ContentObserver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerDefaultInputMethodChangedObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1387
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 13
    .param p1, "selfChange"    # Z

    .line 1390
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 1391
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmContext(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 1392
    .local v0, "um":Landroid/os/UserManager;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmContext(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1393
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v2

    .line 1395
    .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    iget-object v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mgetDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;

    move-result-object v3

    .line 1396
    .local v3, "inputMethodPkgName":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1397
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/UserInfo;

    .line 1398
    .local v5, "user":Landroid/content/pm/UserInfo;
    iget v6, v5, Landroid/content/pm/UserInfo;->id:I

    const/4 v7, 0x0

    invoke-virtual {v1, v7, v6}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;

    move-result-object v6

    .line 1399
    .local v6, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/PackageInfo;

    .line 1400
    .local v9, "app":Landroid/content/pm/PackageInfo;
    iget-object v10, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v10, :cond_1

    iget-object v10, v9, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v10, :cond_1

    .line 1401
    iget v10, v5, Landroid/content/pm/UserInfo;->id:I

    iget-object v11, v9, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v11, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v10, v11}, Landroid/os/UserHandle;->getUid(II)I

    move-result v10

    .line 1402
    .local v10, "uid":I
    iget-object v11, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iget-object v12, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v12}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1403
    iget-object v11, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v11}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmUnRestrictApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v11

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1404
    iget-object v11, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v11}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v11

    invoke-virtual {v11, v10, v7}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z

    goto :goto_2

    .line 1405
    :cond_0
    iget-object v11, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1406
    iget-object v11, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v11}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmUnRestrictApps(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v11

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1407
    iget-object v11, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v11}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmNetworkManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v10, v12}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z

    .line 1410
    .end local v9    # "app":Landroid/content/pm/PackageInfo;
    .end local v10    # "uid":I
    :cond_1
    :goto_2
    goto :goto_1

    .line 1411
    .end local v5    # "user":Landroid/content/pm/UserInfo;
    .end local v6    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_2
    goto :goto_0

    .line 1413
    :cond_3
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmDefaultInputMethod(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V

    .line 1415
    :cond_4
    return-void
.end method
