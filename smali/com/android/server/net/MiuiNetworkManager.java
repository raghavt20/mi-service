public class com.android.server.net.MiuiNetworkManager extends android.net.IMiuiNetworkManager$Stub {
	 /* .source "MiuiNetworkManager.java" */
	 /* # static fields */
	 private static com.android.server.net.MiuiNetworkManager sSelf;
	 /* # direct methods */
	 public com.android.server.net.MiuiNetworkManager ( ) {
		 /* .locals 0 */
		 /* .line 6 */
		 /* invoke-direct {p0}, Landroid/net/IMiuiNetworkManager$Stub;-><init>()V */
		 return;
	 } // .end method
	 public static final com.android.server.net.MiuiNetworkManager get ( ) {
		 /* .locals 1 */
		 /* .line 10 */
		 v0 = com.android.server.net.MiuiNetworkManager.sSelf;
		 /* if-nez v0, :cond_0 */
		 /* .line 11 */
		 /* new-instance v0, Lcom/android/server/net/MiuiNetworkManager; */
		 /* invoke-direct {v0}, Lcom/android/server/net/MiuiNetworkManager;-><init>()V */
		 /* .line 13 */
	 } // :cond_0
	 v0 = com.android.server.net.MiuiNetworkManager.sSelf;
} // .end method
/* # virtual methods */
public Long getMiuiSlmVoipUdpAddress ( Integer p0 ) {
	 /* .locals 2 */
	 /* .param p1, "uid" # I */
	 /* .line 38 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .get ( );
	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).getMiuiSlmVoipUdpAddress ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getMiuiSlmVoipUdpAddress(I)J
	 /* move-result-wide v0 */
	 /* return-wide v0 */
} // .end method
public Integer getMiuiSlmVoipUdpPort ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "uid" # I */
	 /* .line 43 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .get ( );
	 v0 = 	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).getMiuiSlmVoipUdpPort ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getMiuiSlmVoipUdpPort(I)I
} // .end method
public Long getShareStats ( Integer p0 ) {
	 /* .locals 2 */
	 /* .param p1, "type" # I */
	 /* .line 48 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .get ( );
	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).getShareStats ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getShareStats(I)J
	 /* move-result-wide v0 */
	 /* return-wide v0 */
} // .end method
public Boolean onSleepModeWhitelistChange ( Integer p0, Boolean p1 ) {
	 /* .locals 1 */
	 /* .param p1, "appId" # I */
	 /* .param p2, "added" # Z */
	 /* .line 53 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .get ( );
	 v0 = 	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).onSleepModeWhitelistChange ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->onSleepModeWhitelistChange(IZ)Z
} // .end method
public Boolean setMiuiSlmBpfUid ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "uid" # I */
	 /* .line 33 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .get ( );
	 v0 = 	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).setMiuiSlmBpfUid ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setMiuiSlmBpfUid(I)Z
} // .end method
public Integer setMobileTrafficLimit ( Boolean p0, Long p1 ) {
	 /* .locals 1 */
	 /* .param p1, "enabled" # Z */
	 /* .param p2, "rate" # J */
	 /* .line 28 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .get ( );
	 v0 = 	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).setTrafficControllerForMobile ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setTrafficControllerForMobile(ZJ)I
} // .end method
public Boolean setNetworkTrafficPolicy ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "mode" # I */
	 /* .line 18 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .get ( );
	 v0 = 	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).setNetworkTrafficPolicy ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setNetworkTrafficPolicy(I)Z
} // .end method
public Boolean setRpsStatus ( Boolean p0 ) {
	 /* .locals 1 */
	 /* .param p1, "enable" # Z */
	 /* .line 23 */
	 com.android.server.net.MiuiNetworkPolicyManagerService .get ( );
	 v0 = 	 (( com.android.server.net.MiuiNetworkPolicyManagerService ) v0 ).setRpsStatus ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setRpsStatus(Z)Z
} // .end method
