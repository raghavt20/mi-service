class com.android.server.net.MiuiNetworkPolicyManagerService$1 extends android.net.ConnectivityManager$NetworkCallback {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;-><init>(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .line 354 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAvailable ( android.net.Network p0 ) {
/* .locals 5 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 357 */
/* invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onAvailable(Landroid/net/Network;)V */
/* .line 358 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmConnectivityManager ( v0 );
/* .line 359 */
(( android.net.ConnectivityManager ) v0 ).getNetworkCapabilities ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;
/* .line 360 */
/* .local v0, "networkCapabilities":Landroid/net/NetworkCapabilities; */
/* if-nez v0, :cond_0 */
return;
/* .line 361 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 362 */
/* .local v1, "isCompatible":Z */
int v2 = 4; // const/4 v2, 0x4
v2 = (( android.net.NetworkCapabilities ) v0 ).hasTransport ( v2 ); // invoke-virtual {v0, v2}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z
/* .line 363 */
/* .local v2, "isVpn":Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 364 */
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmVpnNetwork ( v3,p1 );
/* .line 365 */
int v1 = 1; // const/4 v1, 0x1
/* .line 367 */
} // :cond_1
(( android.net.NetworkCapabilities ) v0 ).getTransportInfo ( ); // invoke-virtual {v0}, Landroid/net/NetworkCapabilities;->getTransportInfo()Landroid/net/TransportInfo;
/* check-cast v3, Landroid/net/wifi/WifiInfo; */
/* .line 368 */
/* .local v3, "wifiInfo":Landroid/net/wifi/WifiInfo; */
if ( v3 != null) { // if-eqz v3, :cond_2
v4 = (( android.net.wifi.WifiInfo ) v3 ).isPrimary ( ); // invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->isPrimary()Z
/* if-nez v4, :cond_2 */
/* .line 369 */
v4 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmSlaveWifiNetwork ( v4,p1 );
/* .line 370 */
int v1 = 1; // const/4 v1, 0x1
/* .line 373 */
} // .end local v3 # "wifiInfo":Landroid/net/wifi/WifiInfo;
} // :cond_2
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 374 */
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHasAutoForward ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 375 */
v3 = this.this$0;
int v4 = 0; // const/4 v4, 0x0
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableAutoForward ( v3,v4 );
/* .line 376 */
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmHasAutoForward ( v3,v4 );
/* .line 378 */
} // :cond_3
final String v3 = "MiuiNetworkPolicy"; // const-string v3, "MiuiNetworkPolicy"
final String v4 = "Incompatible network, disable auto forward"; // const-string v4, "Incompatible network, disable auto forward"
android.util.Log .w ( v3,v4 );
/* .line 379 */
return;
/* .line 381 */
} // :cond_4
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mcheckEnableAutoForward ( v3,v0,p1 );
/* .line 382 */
return;
} // .end method
public void onCapabilitiesChanged ( android.net.Network p0, android.net.NetworkCapabilities p1 ) {
/* .locals 1 */
/* .param p1, "network" # Landroid/net/Network; */
/* .param p2, "networkCapabilities" # Landroid/net/NetworkCapabilities; */
/* .line 386 */
/* invoke-super {p0, p1, p2}, Landroid/net/ConnectivityManager$NetworkCallback;->onCapabilitiesChanged(Landroid/net/Network;Landroid/net/NetworkCapabilities;)V */
/* .line 387 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mcheckEnableAutoForward ( v0,p2,p1 );
/* .line 388 */
return;
} // .end method
public void onLost ( android.net.Network p0 ) {
/* .locals 3 */
/* .param p1, "network" # Landroid/net/Network; */
/* .line 392 */
/* invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onLost(Landroid/net/Network;)V */
/* .line 393 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSlaveWifiNetwork ( v0 );
v0 = (( android.net.Network ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 394 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmSlaveWifiNetwork ( v0,v1 );
/* .line 395 */
final String v0 = "Slave wifi Network is lost"; // const-string v0, "Slave wifi Network is lost"
android.util.Log .v ( v2,v0 );
/* .line 396 */
} // :cond_0
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmVpnNetwork ( v0 );
v0 = (( android.net.Network ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 397 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmVpnNetwork ( v0,v1 );
/* .line 398 */
/* const-string/jumbo v0, "vpn Network is lost" */
android.util.Log .v ( v2,v0 );
/* .line 399 */
} // :cond_1
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmCurrentNetwork ( v0 );
v0 = (( android.net.Network ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Landroid/net/Network;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.this$0;
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHasAutoForward ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 400 */
final String v0 = "network is lost, remove autoforward rule"; // const-string v0, "network is lost, remove autoforward rule"
android.util.Log .v ( v2,v0 );
/* .line 401 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableAutoForward ( v0,v1 );
/* .line 403 */
} // :cond_2
} // :goto_0
return;
} // .end method
