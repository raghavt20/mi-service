public class com.android.server.net.NetworkManagermentServiceImpl implements com.android.server.net.NetworkManagementServiceStub {
	 /* .source "NetworkManagermentServiceImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.net.NetworkManagermentServiceImpl ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean addMiuiFirewallSharedUid ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "uid" # I */
		 /* .line 46 */
		 com.android.server.net.MiuiNetworkManagementService .getInstance ( );
		 v0 = 		 (( com.android.server.net.MiuiNetworkManagementService ) v0 ).addMiuiFirewallSharedUid ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->addMiuiFirewallSharedUid(I)Z
	 } // .end method
	 public void dump ( java.io.PrintWriter p0 ) {
		 /* .locals 1 */
		 /* .param p1, "pw" # Ljava/io/PrintWriter; */
		 /* .line 36 */
		 com.android.server.net.MiuiNetworkManagementService .getInstance ( );
		 (( com.android.server.net.MiuiNetworkManagementService ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->dump(Ljava/io/PrintWriter;)V
		 /* .line 37 */
		 return;
	 } // .end method
	 public android.os.IBinder getMiuiNetworkManager ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 com.android.server.net.MiuiNetworkManager .get ( );
	 } // .end method
	 public Boolean setCurrentNetworkState ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "state" # I */
		 /* .line 51 */
		 com.android.server.net.MiuiNetworkManagementService .getInstance ( );
		 v0 = 		 (( com.android.server.net.MiuiNetworkManagementService ) v0 ).setCurrentNetworkState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->setCurrentNetworkState(I)Z
	 } // .end method
	 public Boolean setMiuiFirewallRule ( java.lang.String p0, Integer p1, Integer p2, Integer p3 ) {
		 /* .locals 1 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .param p2, "uid" # I */
		 /* .param p3, "rule" # I */
		 /* .param p4, "type" # I */
		 /* .line 26 */
		 com.android.server.net.MiuiNetworkManagementService .getInstance ( );
		 v0 = 		 (( com.android.server.net.MiuiNetworkManagementService ) v0 ).setMiuiFirewallRule ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/net/MiuiNetworkManagementService;->setMiuiFirewallRule(Ljava/lang/String;III)Z
	 } // .end method
	 public void setPidForPackage ( java.lang.String p0, Integer p1, Integer p2 ) {
		 /* .locals 1 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .param p2, "pid" # I */
		 /* .param p3, "uid" # I */
		 /* .line 21 */
		 com.android.server.net.MiuiNetworkManagementService .getInstance ( );
		 (( com.android.server.net.MiuiNetworkManagementService ) v0 ).setPidForPackage ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkManagementService;->setPidForPackage(Ljava/lang/String;II)V
		 /* .line 22 */
		 return;
	 } // .end method
	 public void showLogin ( android.content.Context p0, android.content.Intent p1, java.lang.String p2 ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "intent" # Landroid/content/Intent; */
		 /* .param p3, "ssid" # Ljava/lang/String; */
		 /* .line 32 */
		 return;
	 } // .end method
	 public void updateAurogonUidRule ( Integer p0, Boolean p1 ) {
		 /* .locals 1 */
		 /* .param p1, "uid" # I */
		 /* .param p2, "allow" # Z */
		 /* .line 41 */
		 com.android.server.net.MiuiNetworkManagementService .getInstance ( );
		 (( com.android.server.net.MiuiNetworkManagementService ) v0 ).updateAurogonUidRule ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/net/MiuiNetworkManagementService;->updateAurogonUidRule(IZ)V
		 /* .line 42 */
		 return;
	 } // .end method
