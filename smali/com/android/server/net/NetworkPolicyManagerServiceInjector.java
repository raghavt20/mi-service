public class com.android.server.net.NetworkPolicyManagerServiceInjector {
	 /* .source "NetworkPolicyManagerServiceInjector.java" */
	 /* # static fields */
	 private static java.lang.String TAG;
	 /* # direct methods */
	 static com.android.server.net.NetworkPolicyManagerServiceInjector ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 final String v0 = "NetworkPolicyManagerServiceInjector"; // const-string v0, "NetworkPolicyManagerServiceInjector"
		 return;
	 } // .end method
	 public com.android.server.net.NetworkPolicyManagerServiceInjector ( ) {
		 /* .locals 0 */
		 /* .line 15 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Boolean checkPolicyForNetwork ( android.net.NetworkPolicy p0 ) {
		 /* .locals 2 */
		 /* .param p0, "policy" # Landroid/net/NetworkPolicy; */
		 /* .line 44 */
		 v0 = this.template;
		 v0 = 		 (( android.net.NetworkTemplate ) v0 ).getMatchRule ( ); // invoke-virtual {v0}, Landroid/net/NetworkTemplate;->getMatchRule()I
		 int v1 = 4; // const/4 v1, 0x4
		 /* if-eq v0, v1, :cond_0 */
		 /* .line 45 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 47 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public static void updateNetworkRules ( java.util.Map p0 ) {
	 /* .locals 9 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/util/Map<", */
	 /* "Landroid/net/NetworkPolicy;", */
	 /* "[", */
	 /* "Ljava/lang/String;", */
	 /* ">;)V" */
	 /* } */
} // .end annotation
/* .line 19 */
/* .local p0, "networkRules":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/NetworkPolicy;[Ljava/lang/String;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 20 */
/* .local v0, "findWifi":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 21 */
/* .local v1, "findWifiWildCard":Z */
v3 = } // :goto_0
int v4 = 4; // const/4 v4, 0x4
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Landroid/net/NetworkPolicy; */
/* .line 22 */
/* .local v3, "policy":Landroid/net/NetworkPolicy; */
/* iget-wide v5, v3, Landroid/net/NetworkPolicy;->limitBytes:J */
/* const-wide/16 v7, -0x1 */
/* cmp-long v5, v5, v7 */
if ( v5 != null) { // if-eqz v5, :cond_0
	 int v5 = 1; // const/4 v5, 0x1
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
/* .line 23 */
/* .local v5, "hasLimit":Z */
} // :goto_1
/* if-nez v5, :cond_1 */
/* iget-boolean v6, v3, Landroid/net/NetworkPolicy;->metered:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 24 */
} // :cond_1
v6 = this.template;
v6 = (( android.net.NetworkTemplate ) v6 ).getMatchRule ( ); // invoke-virtual {v6}, Landroid/net/NetworkTemplate;->getMatchRule()I
/* if-ne v6, v4, :cond_2 */
/* .line 25 */
int v1 = 1; // const/4 v1, 0x1
/* .line 26 */
} // :cond_2
v6 = this.template;
v6 = (( android.net.NetworkTemplate ) v6 ).getMatchRule ( ); // invoke-virtual {v6}, Landroid/net/NetworkTemplate;->getMatchRule()I
/* if-ne v6, v4, :cond_3 */
/* .line 27 */
int v0 = 1; // const/4 v0, 0x1
/* .line 30 */
} // .end local v3 # "policy":Landroid/net/NetworkPolicy;
} // .end local v5 # "hasLimit":Z
} // :cond_3
} // :goto_2
/* .line 31 */
} // :cond_4
if ( v0 != null) { // if-eqz v0, :cond_7
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 32 */
com.google.android.collect.Maps .newHashMap ( );
/* .line 33 */
/* .local v2, "newNetworkRules":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/NetworkPolicy;[Ljava/lang/String;>;" */
v5 = } // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_6
/* check-cast v5, Landroid/net/NetworkPolicy; */
/* .line 34 */
/* .local v5, "policy":Landroid/net/NetworkPolicy; */
v6 = this.template;
v6 = (( android.net.NetworkTemplate ) v6 ).getMatchRule ( ); // invoke-virtual {v6}, Landroid/net/NetworkTemplate;->getMatchRule()I
/* if-eq v6, v4, :cond_5 */
/* .line 35 */
/* check-cast v6, [Ljava/lang/String; */
/* .line 37 */
} // .end local v5 # "policy":Landroid/net/NetworkPolicy;
} // :cond_5
/* .line 38 */
} // :cond_6
/* .line 39 */
/* .line 41 */
} // .end local v2 # "newNetworkRules":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/NetworkPolicy;[Ljava/lang/String;>;"
} // :cond_7
return;
} // .end method
