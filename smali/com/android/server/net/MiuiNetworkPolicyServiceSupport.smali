.class public Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyServiceSupport.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "MiuiNetworkPolicySupport"


# instance fields
.field private final mActivityManager:Landroid/app/IActivityManager;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

.field private final mUidObserver:Landroid/app/IUidObserver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport$1;

    invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;)V

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mUidObserver:Landroid/app/IUidObserver;

    .line 29
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mContext:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mHandler:Landroid/os/Handler;

    .line 31
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mActivityManager:Landroid/app/IActivityManager;

    .line 32
    return-void
.end method


# virtual methods
.method public enablePowerSave(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .line 45
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mContext:Landroid/content/Context;

    const-string v1, "MiuiWifiService"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/MiuiWifiManager;

    iput-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz v0, :cond_1

    .line 49
    invoke-virtual {v0, p1}, Landroid/net/wifi/MiuiWifiManager;->enablePowerSave(Z)V

    .line 51
    :cond_1
    return-void
.end method

.method public registerUidObserver()V
    .locals 5

    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mActivityManager:Landroid/app/IActivityManager;

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mUidObserver:Landroid/app/IUidObserver;

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-interface {v0, v1, v4, v2, v3}, Landroid/app/IActivityManager;->registerUidObserver(Landroid/app/IUidObserver;IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    .line 42
    :goto_0
    return-void
.end method

.method public updateIface(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "iface"    # Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 55
    .local v0, "wm":Landroid/net/wifi/WifiManager;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 56
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;

    move-result-object v2

    .line 57
    .local v2, "lp":Landroid/net/LinkProperties;
    if-eqz v2, :cond_0

    .line 58
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, "newIface":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 60
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManagementService;->getInstance()Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->updateIface(Ljava/lang/String;)Z

    .line 61
    return-object v3

    .line 65
    .end local v3    # "newIface":Ljava/lang/String;
    :cond_0
    return-object p1
.end method
