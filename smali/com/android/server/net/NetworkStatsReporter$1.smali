.class Lcom/android/server/net/NetworkStatsReporter$1;
.super Ljava/lang/Object;
.source "NetworkStatsReporter.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkStatsReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/NetworkStatsReporter;


# direct methods
.method constructor <init>(Lcom/android/server/net/NetworkStatsReporter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/NetworkStatsReporter;

    .line 233
    iput-object p1, p0, Lcom/android/server/net/NetworkStatsReporter$1;->this$0:Lcom/android/server/net/NetworkStatsReporter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1, "message"    # Landroid/os/Message;

    .line 236
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 238
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsReporter$1;->this$0:Lcom/android/server/net/NetworkStatsReporter;

    invoke-static {v0}, Lcom/android/server/net/NetworkStatsReporter;->-$$Nest$mupdateNetworkStats(Lcom/android/server/net/NetworkStatsReporter;)V

    .line 239
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsReporter$1;->this$0:Lcom/android/server/net/NetworkStatsReporter;

    invoke-static {v0}, Lcom/android/server/net/NetworkStatsReporter;->-$$Nest$mreportNetworkStats(Lcom/android/server/net/NetworkStatsReporter;)V

    .line 240
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsReporter$1;->this$0:Lcom/android/server/net/NetworkStatsReporter;

    invoke-static {v0}, Lcom/android/server/net/NetworkStatsReporter;->-$$Nest$mstartAlarm(Lcom/android/server/net/NetworkStatsReporter;)V

    .line 241
    iget-object v0, p0, Lcom/android/server/net/NetworkStatsReporter$1;->this$0:Lcom/android/server/net/NetworkStatsReporter;

    invoke-static {v0}, Lcom/android/server/net/NetworkStatsReporter;->-$$Nest$mclear(Lcom/android/server/net/NetworkStatsReporter;)V

    .line 242
    nop

    .line 246
    :goto_0
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
