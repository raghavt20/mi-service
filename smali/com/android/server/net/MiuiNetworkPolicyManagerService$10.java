class com.android.server.net.MiuiNetworkPolicyManagerService$10 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerP2PHCChangeObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$10 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1070 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .line 1073 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v1 );
/* .line 1075 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1074 */
final String v2 = "cloud_p2phc_enabled"; // const-string v2, "cloud_p2phc_enabled"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 1079 */
/* .local v1, "enableValue":Ljava/lang/String; */
try { // :try_start_0
	 v2 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v2 );
	 final String v4 = "MiuiWifiService"; // const-string v4, "MiuiWifiService"
	 (( android.content.Context ) v3 ).getSystemService ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v3, Landroid/net/wifi/MiuiWifiManager; */
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmMiuiWifiManager ( v2,v3 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 1082 */
	 /* .line 1080 */
	 /* :catch_0 */
	 /* move-exception v2 */
	 /* .line 1081 */
	 /* .local v2, "e":Ljava/lang/Exception; */
	 final String v3 = "Failed to set p2phc cloud support config"; // const-string v3, "Failed to set p2phc cloud support config"
	 android.util.Log .e ( v0,v3,v2 );
	 /* .line 1083 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
v2 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMiuiWifiManager ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1084 */
v2 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmMiuiWifiManager ( v2 );
final String v3 = "on"; // const-string v3, "on"
v4 = android.text.TextUtils .equals ( v3,v1 );
(( android.net.wifi.MiuiWifiManager ) v2 ).setP2PHCEnable ( v4 ); // invoke-virtual {v2, v4}, Landroid/net/wifi/MiuiWifiManager;->setP2PHCEnable(I)V
/* .line 1086 */
try { // :try_start_1
	 /* const-string/jumbo v2, "vendor.miui.wifi.p2phc" */
	 v3 = 	 android.text.TextUtils .equals ( v3,v1 );
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 final String v3 = "1"; // const-string v3, "1"
	 } // :cond_0
	 final String v3 = "0"; // const-string v3, "0"
} // :goto_1
android.os.SystemProperties .set ( v2,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 1089 */
/* .line 1087 */
/* :catch_1 */
/* move-exception v2 */
/* .line 1088 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
final String v3 = "Failed to set p2phc_enable config"; // const-string v3, "Failed to set p2phc_enable config"
android.util.Log .e ( v0,v3,v2 );
/* .line 1090 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "p2phc enabled = "; // const-string v3, "p2phc enabled = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 1092 */
} // :cond_1
return;
} // .end method
