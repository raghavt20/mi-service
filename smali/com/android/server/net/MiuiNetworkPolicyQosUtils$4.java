class com.android.server.net.MiuiNetworkPolicyQosUtils$4 extends android.content.BroadcastReceiver {
	 /* .source "MiuiNetworkPolicyQosUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyQosUtils this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyQosUtils$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyQosUtils; */
/* .line 506 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 509 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 510 */
/* .local v0, "action":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
return;
/* .line 511 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive action"; // const-string v2, "receive action"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicyQosUtils"; // const-string v2, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v2,v1 );
/* .line 512 */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 513 */
final String v1 = "networkInfo"; // const-string v1, "networkInfo"
(( android.content.Intent ) p2 ).getParcelableExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v1, Landroid/net/NetworkInfo; */
/* .line 514 */
/* .local v1, "nwInfo":Landroid/net/NetworkInfo; */
if ( v1 != null) { // if-eqz v1, :cond_3
	 v3 = 	 (( android.net.NetworkInfo ) v1 ).getType ( ); // invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I
	 /* if-nez v3, :cond_3 */
	 /* .line 515 */
	 (( android.net.NetworkInfo ) v1 ).getState ( ); // invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;
	 v4 = android.net.NetworkInfo$State.CONNECTED;
	 final String v5 = "mIsMobileDataOn="; // const-string v5, "mIsMobileDataOn="
	 /* if-ne v3, v4, :cond_1 */
	 v3 = this.this$0;
	 v3 = 	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmIsMobileDataOn ( v3 );
	 /* if-nez v3, :cond_1 */
	 /* .line 516 */
	 v3 = this.this$0;
	 int v4 = 1; // const/4 v4, 0x1
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fputmIsMobileDataOn ( v3,v4 );
	 /* .line 517 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v4 = this.this$0;
	 v4 = 	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmIsMobileDataOn ( v4 );
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .i ( v2,v3 );
	 /* .line 518 */
} // :cond_1
(( android.net.NetworkInfo ) v1 ).getState ( ); // invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;
v4 = android.net.NetworkInfo$State.DISCONNECTED;
/* if-ne v3, v4, :cond_3 */
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmIsMobileDataOn ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_3
	 /* .line 519 */
	 v3 = this.this$0;
	 int v4 = 0; // const/4 v4, 0x0
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fputmIsMobileDataOn ( v3,v4 );
	 /* .line 520 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v5 = this.this$0;
	 v5 = 	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmIsMobileDataOn ( v5 );
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .i ( v2,v3 );
	 /* .line 521 */
	 v2 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmQosInfo ( v2 );
	 if ( v2 != null) { // if-eqz v2, :cond_3
		 v2 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmQosInfo ( v2 );
		 v2 = 		 (( com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ) v2 ).getQosStatus ( ); // invoke-virtual {v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getQosStatus()Z
		 if ( v2 != null) { // if-eqz v2, :cond_3
			 /* .line 522 */
			 v2 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmQosInfo ( v2 );
			 v3 = 			 (( com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ) v3 ).getUid ( ); // invoke-virtual {v3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getUid()I
			 v5 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$fgetmQosInfo ( v5 );
			 v5 = 			 (( com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ) v5 ).getProtocol ( ); // invoke-virtual {v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getProtocol()I
			 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$msetQos ( v2,v4,v3,v5 );
			 /* .line 526 */
		 } // .end local v1 # "nwInfo":Landroid/net/NetworkInfo;
	 } // :cond_2
	 final String v1 = "com.android.phone.intent.action.CLOUD_MEETING_LIST_DONE"; // const-string v1, "com.android.phone.intent.action.CLOUD_MEETING_LIST_DONE"
	 v1 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_3
		 /* .line 527 */
		 v1 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyQosUtils .-$$Nest$mprocessMeetingListChanged ( v1 );
		 /* .line 526 */
	 } // :cond_3
} // :goto_0
/* nop */
/* .line 529 */
} // :goto_1
return;
} // .end method
