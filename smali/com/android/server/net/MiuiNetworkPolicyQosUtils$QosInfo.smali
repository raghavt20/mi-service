.class Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyQosUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QosInfo"
.end annotation


# instance fields
.field private enabled:Z

.field private protocol:I

.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

.field private uid:I


# direct methods
.method public constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V
    .locals 0

    .line 469
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470
    invoke-virtual {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->clear()V

    .line 471
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 492
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->enabled:Z

    .line 493
    iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->uid:I

    .line 494
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->protocol:I

    .line 495
    return-void
.end method

.method public getProtocol()I
    .locals 1

    .line 488
    iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->protocol:I

    return v0
.end method

.method public getQosStatus()Z
    .locals 1

    .line 480
    iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->enabled:Z

    return v0
.end method

.method public getUid()I
    .locals 1

    .line 484
    iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->uid:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 499
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->uid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " protocol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->protocol:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateAll(ZII)V
    .locals 0
    .param p1, "enabled"    # Z
    .param p2, "uid"    # I
    .param p3, "protocol"    # I

    .line 474
    iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->enabled:Z

    .line 475
    iput p2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->uid:I

    .line 476
    iput p3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->protocol:I

    .line 477
    return-void
.end method
