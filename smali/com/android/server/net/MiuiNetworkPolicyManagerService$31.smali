.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->onSleepModeWhitelistChange(IZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

.field final synthetic val$appId:I


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 2057
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    iput p2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;->val$appId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 2060
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmUserManager(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object v0

    .line 2061
    .local v0, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .local v1, "ui":I
    :goto_0
    if-ltz v1, :cond_0

    .line 2062
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    .line 2063
    .local v2, "user":Landroid/content/pm/UserInfo;
    iget v3, v2, Landroid/content/pm/UserInfo;->id:I

    iget v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;->val$appId:I

    invoke-static {v3, v4}, Landroid/os/UserHandle;->getUid(II)I

    move-result v3

    .line 2064
    .local v3, "uid":I
    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeWhitelistUids(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2061
    .end local v2    # "user":Landroid/content/pm/UserInfo;
    .end local v3    # "uid":I
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2066
    .end local v1    # "ui":I
    :cond_0
    return-void
.end method
