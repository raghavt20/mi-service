.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$6;
.super Landroid/database/ContentObserver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerHappyEyeballsChangeObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1006
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 1009
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mhappyEyeballsEnableCloudControl(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 1010
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$6;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mhappyEyeballsMasterServerPriorityTimeCloudControl(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 1011
    return-void
.end method
