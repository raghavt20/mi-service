class com.android.server.net.MiuiNetworkPolicyManagerService$29 extends android.content.BroadcastReceiver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$29 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .line 1769 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1772 */
final String v0 = "apnType"; // const-string v0, "apnType"
(( android.content.Intent ) p2 ).getStringExtra ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 1773 */
/* .local v0, "apnType":Ljava/lang/String; */
final String v1 = "default"; // const-string v1, "default"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 1774 */
return;
/* .line 1776 */
} // :cond_0
/* const-class v1, Lcom/android/internal/telephony/PhoneConstants$DataState; */
/* .line 1777 */
/* const-string/jumbo v2, "state" */
(( android.content.Intent ) p2 ).getStringExtra ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 1776 */
java.lang.Enum .valueOf ( v1,v2 );
/* check-cast v1, Lcom/android/internal/telephony/PhoneConstants$DataState; */
/* .line 1778 */
/* .local v1, "state":Lcom/android/internal/telephony/PhoneConstants$DataState; */
v2 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmConnectState ( v2 );
/* if-ne v2, v1, :cond_1 */
/* .line 1779 */
return;
/* .line 1781 */
} // :cond_1
v2 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmConnectState ( v2,v1 );
/* .line 1782 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mMobileNwReceiver mConnectState = "; // const-string v3, "mMobileNwReceiver mConnectState = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmConnectState ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiNetworkPolicy"; // const-string v3, "MiuiNetworkPolicy"
android.util.Log .i ( v3,v2 );
/* .line 1783 */
v2 = com.android.internal.telephony.PhoneConstants$DataState.DISCONNECTED;
/* if-ne v1, v2, :cond_2 */
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$sfgetmMobileLatencyState ( );
int v3 = 1; // const/4 v3, 0x1
/* if-eq v2, v3, :cond_3 */
} // :cond_2
v2 = com.android.internal.telephony.PhoneConstants$DataState.CONNECTED;
/* if-ne v1, v2, :cond_4 */
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$sfgetmMobileLatencyState ( );
/* if-nez v2, :cond_4 */
/* .line 1785 */
} // :cond_3
v2 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v2 );
/* const/16 v3, 0x9 */
(( android.os.Handler ) v2 ).sendEmptyMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 1787 */
} // :cond_4
return;
} // .end method
