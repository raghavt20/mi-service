public class com.android.server.net.MiuiNetworkPolicyManagerService {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_NETWORK_CONDITIONS_MEASURED;
private static final java.lang.String ACTION_SLEEP_MODE_CHANGED;
private static final AVOID_CHANNEL_IE;
private static final Long BG_MIN_BANDWIDTH;
private static final java.lang.String CLOUD_AUTO_FORWARD_ENABLED;
private static final java.lang.String CLOUD_DNS_HAPPY_EYEBALLS_ENABLED;
private static final java.lang.String CLOUD_DNS_HAPPY_EYEBALLS_PRIORITY_TIME;
private static final java.lang.String CLOUD_LOW_LATENCY_APPLIST_FOR_MOBILE;
private static final java.lang.String CLOUD_LOW_LATENCY_WHITELIST;
private static final java.lang.String CLOUD_MTK_WIFI_TRAFFIC_PRIORITY_MODE;
private static final java.lang.String CLOUD_MTK_WMMER_ENABLED;
private static final java.lang.String CLOUD_NETWORK_PRIORITY_ENABLED;
private static final java.lang.String CLOUD_NETWORK_TRUST_MIUI_ADD_DNS;
private static final java.lang.String CLOUD_P2PHC_ENABLED;
private static final java.lang.String CLOUD_RESTRICT_WIFI_POWERSAVE_APPLIST;
public static final java.lang.String CLOUD_SLEEPMODE_NETWORKPOLICY_ENABLED;
private static final java.lang.String CLOUD_SMARTDNS_ENABLED;
private static final java.lang.String CLOUD_WMMER_ENABLED;
private static final java.lang.String CLOUD_WMMER_ROUTER_WHITELIST;
private static final Boolean DEBUG;
private static final Integer DISABLE_LIMIT_TIMEOUT_LEVEL_1;
private static final Integer DISABLE_LIMIT_TIMEOUT_LEVEL_2;
private static final Integer DISABLE_LIMIT_TIMEOUT_LEVEL_3;
private static final Integer DISABLE_POWER_SAVE_TIMEOUT;
public static final java.lang.String EARTHQUAKE_WARNING_FLAG;
private static final Integer EARTH_QUACK_ON;
private static final Integer EID_VSA;
private static final Integer ENABLE_LIMIT_TIMEOUT_LEVEL_1;
private static final Integer ENABLE_LIMIT_TIMEOUT_LEVEL_2;
private static final Integer ENABLE_LIMIT_TIMEOUT_LEVEL_3;
private static final java.lang.String EXTRA_IS_CAPTIVE_PORTAL;
private static final java.lang.String EXTRA_STATE;
private static final Long FG_MAX_BANDWIDTH;
private static final Long HISTORY_BANDWIDTH_MIN;
private static final Integer HISTORY_BANDWIDTH_SIZE;
private static final Boolean IS_QCOM;
private static final java.lang.String LATENCY_ACTION_CHANGE_LEVEL;
private static final Integer LATENCY_DEFAULT;
private static final java.lang.String LATENCY_KEY_LEVEL_DL;
private static final java.lang.String LATENCY_KEY_LEVEL_UL;
private static final java.lang.String LATENCY_KEY_RAT_TYPE;
private static final Integer LATENCY_OFF;
private static final Integer LATENCY_ON;
private static final Long LATENCY_VALUE_L1;
private static final Long LATENCY_VALUE_L2;
private static final Long LATENCY_VALUE_L3;
private static final Long LATENCY_VALUE_L4;
private static final Long LATENCY_VALUE_WLAN;
private static final Long LATENCY_VALUE_WWAN;
private static final java.lang.String LOCAL_NETWORK_PRIORITY_WHITELIST;
private static final Long MAX_ROUTER_DETECT_TIME;
private static final Integer MIN_THIRDAPP_UID;
private static final MODEL_NAME;
private static final Integer MSG_BANDWIDTH_POLL;
private static final Integer MSG_CHECK_ROUTER_MTK;
private static final Integer MSG_DISABLE_LIMIT_TIMEOUT;
private static final Integer MSG_DISABLE_POWER_SAVE_TIMEOUT;
private static final Integer MSG_ENABLE_LIMIT_TIMEOUT;
private static final Integer MSG_MOBILE_LATENCY_CHANGED;
public static final Integer MSG_SET_LIMIT_FOR_MOBILE;
private static final Integer MSG_SET_RPS_STATS;
public static final Integer MSG_SET_THERMAL_POLICY;
private static final Integer MSG_SET_TRAFFIC_POLICY;
private static final Integer MSG_UID_DATA_ACTIVITY_CHANGED;
public static final Integer MSG_UID_STATE_CHANGED;
public static final Integer MSG_UID_STATE_GONED;
public static final Integer MSG_UPDATE_RULE_FOR_MOBILE_TRAFFIC;
private static final MTK_OUI1;
private static final MTK_OUI2;
private static final MTK_OUI3;
private static final MTK_OUI4;
private static final Integer NETWORK_PRIORITY_MODE_CLOSED;
private static final Integer NETWORK_PRIORITY_MODE_FAST;
private static final Integer NETWORK_PRIORITY_MODE_NORMAL;
private static final Integer NETWORK_PRIORITY_MODE_WMM;
private static final java.lang.String NETWORK_PRIORITY_WHITELIST;
private static final java.lang.String NOTIFACATION_RECEIVER_PACKAGE;
private static final java.lang.String PERSIST_DEVICE_CONFIG_NETD_ENABLED_VALUE;
private static final java.lang.String PERSIST_DEVICE_CONFIG_NETD_PRIORITY_TIME;
private static final java.lang.String PERSIST_DEVICE_CONFIG_NETD_SMARTDNS_ENABLED_VALUE;
private static final java.lang.String PERSIST_DEVICE_CONFIG_NETD_VALUE;
private static final java.lang.String PERSIST_WPA_SUPPLICANT_P2PHC_ENABLE;
private static final Integer POLL_BANDWIDTH_INTERVAL_SECS;
private static final Integer POWER_SAVE_IDLETIMER_LABEL;
private static final Integer POWER_SAVE_IDLETIMER_TIMEOUT;
private static final java.lang.String SLEEPMODE_TAG;
private static final Integer SLEEP_MODE_ON;
private static final java.lang.String TAG;
private static final Integer THERMAL_FAST_LEVEL_1;
private static final Integer THERMAL_FAST_LEVEL_2;
private static final Integer THERMAL_FAST_LEVEL_3;
private static final Integer THERMAL_FIRST_MODE;
private static final Integer THERMAL_LAST_MODE;
private static final Integer THERMAL_NORMAL_MODE;
private static final java.lang.String THERMAL_STATE_FILE;
private static final Integer WMM_AC_BE;
private static final Integer WMM_AC_VI;
private static final Integer WMM_AC_VO;
private static final WPA2_AKM_PSK_IE;
private static final WPA_AKM_EAP_IE;
private static final WPA_AKM_PSK_IE;
private static Integer mMobileLatencyState;
private static com.android.server.net.MiuiNetworkPolicyManagerService sSelf;
/* # instance fields */
private Long ROUTER_DETECT_TIME;
private android.net.ConnectivityManager cm;
private com.android.server.net.MiuiNetworkPolicyAppBuckets mAppBuckets;
private Boolean mCloudWmmerEnable;
private com.android.internal.telephony.PhoneConstants$DataState mConnectState;
private java.lang.String mConnectedApModel;
private android.net.ConnectivityManager mConnectivityManager;
private final android.content.Context mContext;
private android.net.Network mCurrentNetwork;
private java.lang.String mDefaultInputMethod;
private final android.os.Handler mHandler;
private android.os.Handler$Callback mHandlerCallback;
private Boolean mHasAutoForward;
private Boolean mHasListenWifiNetwork;
private java.util.Deque mHistoryBandWidth;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Deque<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mInterfaceName;
private Boolean mIsCaptivePortal;
private Boolean mIsMtkRouter;
private java.lang.String mLastIpAddress;
private Integer mLastNetId;
private Long mLastRxBytes;
private Boolean mLimitEnabled;
private java.util.Set mLowLatencyApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mLowLatencyAppsPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.net.wifi.MiuiWifiManager mMiuiWifiManager;
private java.util.Set mMobileLowLatencyApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mMobileLowLatencyAppsPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.BroadcastReceiver mMobileNwReceiver;
private com.android.server.net.MiuiNetworkPolicyTrafficLimit mMobileTcUtils;
private java.util.Set mNeedRestrictPowerSaveApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mNeedRestrictPowerSaveAppsPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.xiaomi.NetworkBoost.NetworkBoostService mNetworkBoostService;
private com.android.server.net.MiuiNetworkManagementService mNetworkManager;
private Integer mNetworkPriorityMode;
private final android.content.BroadcastReceiver mPackageReceiver;
private Boolean mPowerSaveEnabled;
private com.android.server.net.MiuiNetworkPolicyQosUtils mQosUtils;
private Boolean mRpsEnabled;
private android.net.Network mSlaveWifiNetwork;
private Boolean mSleepModeEnable;
private Boolean mSleepModeEnter;
private final android.content.BroadcastReceiver mSleepModeReceiver;
private java.util.Set mSleepModeWhitelistUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.net.MiuiNetworkPolicyServiceSupport mSupport;
private Integer mThermalForceMode;
private com.android.server.net.MiuiNetworkPolicyManagerService$ThermalStateListener mThermalStateListener;
private Integer mTrafficPolicyMode;
private com.android.server.net.MiuiNetworkManagementService$NetworkEventObserver mUidDataActivityObserver;
final android.util.SparseIntArray mUidState;
private java.util.Set mUnRestrictApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mUnRestrictAppsPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.UserManager mUserManager;
private android.net.ConnectivityManager$NetworkCallback mVPNNetworkCallback;
private android.net.NetworkRequest mVPNNetworkRequest;
private android.net.Network mVpnNetwork;
private Boolean mWifiConnected;
private android.net.ConnectivityManager$NetworkCallback mWifiNetworkCallback;
private android.net.NetworkRequest mWifiNetworkRequest;
private final android.content.BroadcastReceiver mWifiStateReceiver;
private Boolean mWmmerEnable;
private java.util.HashSet mWmmerRouterWhitelist;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.net.MiuiNetworkPolicyAppBuckets -$$Nest$fgetmAppBuckets ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAppBuckets;
} // .end method
static Boolean -$$Nest$fgetmCloudWmmerEnable ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCloudWmmerEnable:Z */
} // .end method
static com.android.internal.telephony.PhoneConstants$DataState -$$Nest$fgetmConnectState ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mConnectState;
} // .end method
static android.net.ConnectivityManager -$$Nest$fgetmConnectivityManager ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mConnectivityManager;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.net.Network -$$Nest$fgetmCurrentNetwork ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurrentNetwork;
} // .end method
static java.lang.String -$$Nest$fgetmDefaultInputMethod ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDefaultInputMethod;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmHasAutoForward ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z */
} // .end method
static java.lang.String -$$Nest$fgetmInterfaceName ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mInterfaceName;
} // .end method
static Boolean -$$Nest$fgetmIsCaptivePortal ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsCaptivePortal:Z */
} // .end method
static Boolean -$$Nest$fgetmIsMtkRouter ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsMtkRouter:Z */
} // .end method
static Boolean -$$Nest$fgetmLimitEnabled ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z */
} // .end method
static java.util.Set -$$Nest$fgetmLowLatencyApps ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLowLatencyApps;
} // .end method
static java.util.Set -$$Nest$fgetmLowLatencyAppsPN ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLowLatencyAppsPN;
} // .end method
static android.net.wifi.MiuiWifiManager -$$Nest$fgetmMiuiWifiManager ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiWifiManager;
} // .end method
static java.util.Set -$$Nest$fgetmMobileLowLatencyApps ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMobileLowLatencyApps;
} // .end method
static java.util.Set -$$Nest$fgetmMobileLowLatencyAppsPN ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMobileLowLatencyAppsPN;
} // .end method
static com.android.server.net.MiuiNetworkPolicyTrafficLimit -$$Nest$fgetmMobileTcUtils ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMobileTcUtils;
} // .end method
static java.util.Set -$$Nest$fgetmNeedRestrictPowerSaveApps ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNeedRestrictPowerSaveApps;
} // .end method
static java.util.Set -$$Nest$fgetmNeedRestrictPowerSaveAppsPN ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNeedRestrictPowerSaveAppsPN;
} // .end method
static com.xiaomi.NetworkBoost.NetworkBoostService -$$Nest$fgetmNetworkBoostService ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetworkBoostService;
} // .end method
static com.android.server.net.MiuiNetworkManagementService -$$Nest$fgetmNetworkManager ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetworkManager;
} // .end method
static Integer -$$Nest$fgetmNetworkPriorityMode ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I */
} // .end method
static Boolean -$$Nest$fgetmPowerSaveEnabled ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z */
} // .end method
static com.android.server.net.MiuiNetworkPolicyQosUtils -$$Nest$fgetmQosUtils ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mQosUtils;
} // .end method
static Boolean -$$Nest$fgetmRpsEnabled ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mRpsEnabled:Z */
} // .end method
static android.net.Network -$$Nest$fgetmSlaveWifiNetwork ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSlaveWifiNetwork;
} // .end method
static Boolean -$$Nest$fgetmSleepModeEnable ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnable:Z */
} // .end method
static Boolean -$$Nest$fgetmSleepModeEnter ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnter:Z */
} // .end method
static java.util.Set -$$Nest$fgetmSleepModeWhitelistUids ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSleepModeWhitelistUids;
} // .end method
static com.android.server.net.MiuiNetworkPolicyServiceSupport -$$Nest$fgetmSupport ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSupport;
} // .end method
static Integer -$$Nest$fgetmThermalForceMode ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
} // .end method
static Integer -$$Nest$fgetmTrafficPolicyMode ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I */
} // .end method
static java.util.Set -$$Nest$fgetmUnRestrictApps ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUnRestrictApps;
} // .end method
static java.util.Set -$$Nest$fgetmUnRestrictAppsPN ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUnRestrictAppsPN;
} // .end method
static android.os.UserManager -$$Nest$fgetmUserManager ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUserManager;
} // .end method
static android.net.Network -$$Nest$fgetmVpnNetwork ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mVpnNetwork;
} // .end method
static Boolean -$$Nest$fgetmWifiConnected ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiConnected:Z */
} // .end method
static Boolean -$$Nest$fgetmWmmerEnable ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerEnable:Z */
} // .end method
static void -$$Nest$fputmCloudWmmerEnable ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCloudWmmerEnable:Z */
return;
} // .end method
static void -$$Nest$fputmConnectState ( com.android.server.net.MiuiNetworkPolicyManagerService p0, com.android.internal.telephony.PhoneConstants$DataState p1 ) { //bridge//synthethic
/* .locals 0 */
this.mConnectState = p1;
return;
} // .end method
static void -$$Nest$fputmConnectedApModel ( com.android.server.net.MiuiNetworkPolicyManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mConnectedApModel = p1;
return;
} // .end method
static void -$$Nest$fputmDefaultInputMethod ( com.android.server.net.MiuiNetworkPolicyManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mDefaultInputMethod = p1;
return;
} // .end method
static void -$$Nest$fputmHasAutoForward ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z */
return;
} // .end method
static void -$$Nest$fputmInterfaceName ( com.android.server.net.MiuiNetworkPolicyManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mInterfaceName = p1;
return;
} // .end method
static void -$$Nest$fputmIsCaptivePortal ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsCaptivePortal:Z */
return;
} // .end method
static void -$$Nest$fputmIsMtkRouter ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsMtkRouter:Z */
return;
} // .end method
static void -$$Nest$fputmLowLatencyAppsPN ( com.android.server.net.MiuiNetworkPolicyManagerService p0, java.util.Set p1 ) { //bridge//synthethic
/* .locals 0 */
this.mLowLatencyAppsPN = p1;
return;
} // .end method
static void -$$Nest$fputmMiuiWifiManager ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.net.wifi.MiuiWifiManager p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMiuiWifiManager = p1;
return;
} // .end method
static void -$$Nest$fputmMobileLowLatencyAppsPN ( com.android.server.net.MiuiNetworkPolicyManagerService p0, java.util.Set p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMobileLowLatencyAppsPN = p1;
return;
} // .end method
static void -$$Nest$fputmNeedRestrictPowerSaveAppsPN ( com.android.server.net.MiuiNetworkPolicyManagerService p0, java.util.Set p1 ) { //bridge//synthethic
/* .locals 0 */
this.mNeedRestrictPowerSaveAppsPN = p1;
return;
} // .end method
static void -$$Nest$fputmRpsEnabled ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mRpsEnabled:Z */
return;
} // .end method
static void -$$Nest$fputmSlaveWifiNetwork ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.net.Network p1 ) { //bridge//synthethic
/* .locals 0 */
this.mSlaveWifiNetwork = p1;
return;
} // .end method
static void -$$Nest$fputmSleepModeEnable ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnable:Z */
return;
} // .end method
static void -$$Nest$fputmSleepModeEnter ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnter:Z */
return;
} // .end method
static void -$$Nest$fputmThermalForceMode ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
return;
} // .end method
static void -$$Nest$fputmTrafficPolicyMode ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I */
return;
} // .end method
static void -$$Nest$fputmUnRestrictAppsPN ( com.android.server.net.MiuiNetworkPolicyManagerService p0, java.util.Set p1 ) { //bridge//synthethic
/* .locals 0 */
this.mUnRestrictAppsPN = p1;
return;
} // .end method
static void -$$Nest$fputmVpnNetwork ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.net.Network p1 ) { //bridge//synthethic
/* .locals 0 */
this.mVpnNetwork = p1;
return;
} // .end method
static void -$$Nest$fputmWifiConnected ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiConnected:Z */
return;
} // .end method
static void -$$Nest$mcalculateBandWidth ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->calculateBandWidth()V */
return;
} // .end method
static void -$$Nest$mcheckEnableAutoForward ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.net.NetworkCapabilities p1, android.net.Network p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->checkEnableAutoForward(Landroid/net/NetworkCapabilities;Landroid/net/Network;)V */
return;
} // .end method
static Boolean -$$Nest$mcheckRouterMTK ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->checkRouterMTK()Z */
} // .end method
static void -$$Nest$mcheckWifiLimit ( com.android.server.net.MiuiNetworkPolicyManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->checkWifiLimit(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mclearSleepModeWhitelistUidRules ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->clearSleepModeWhitelistUidRules()V */
return;
} // .end method
static void -$$Nest$menableAutoForward ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableAutoForward(Z)V */
return;
} // .end method
static void -$$Nest$menableNetworkPriority ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableNetworkPriority(I)V */
return;
} // .end method
static void -$$Nest$menablePowerSave ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enablePowerSave(Z)V */
return;
} // .end method
static void -$$Nest$menableRps ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableRps(Z)V */
return;
} // .end method
static void -$$Nest$menableSleepModeChain ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableSleepModeChain(Z)V */
return;
} // .end method
static void -$$Nest$menableWmmer ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableWmmer()V */
return;
} // .end method
static void -$$Nest$menableWmmer ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableWmmer(Z)V */
return;
} // .end method
static java.lang.String -$$Nest$mgetDefaultInputMethod ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getDefaultInputMethod()Ljava/lang/String; */
} // .end method
static Integer -$$Nest$mgetDisableLimitTimeout ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getDisableLimitTimeout()I */
} // .end method
static Integer -$$Nest$mgetEnableLimitTimeout ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getEnableLimitTimeout()I */
} // .end method
static java.util.Set -$$Nest$mgetLowLatencyApps ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getLowLatencyApps(Landroid/content/Context;)Ljava/util/Set; */
} // .end method
static java.util.Set -$$Nest$mgetMobileLowLatencyApps ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getMobileLowLatencyApps(Landroid/content/Context;)Ljava/util/Set; */
} // .end method
static java.util.Set -$$Nest$mgetNeedRestrictPowerSaveApps ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getNeedRestrictPowerSaveApps(Landroid/content/Context;)Ljava/util/Set; */
} // .end method
static java.lang.String -$$Nest$mgetRouterModel ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getRouterModel()Ljava/lang/String; */
} // .end method
static java.util.Set -$$Nest$mgetUnRestrictedApps ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getUnRestrictedApps(Landroid/content/Context;)Ljava/util/Set; */
} // .end method
static void -$$Nest$mhappyEyeballsEnableCloudControl ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->happyEyeballsEnableCloudControl()V */
return;
} // .end method
static void -$$Nest$mhappyEyeballsMasterServerPriorityTimeCloudControl ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->happyEyeballsMasterServerPriorityTimeCloudControl()V */
return;
} // .end method
static Boolean -$$Nest$misLimitterEnabled ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled(I)Z */
} // .end method
static void -$$Nest$mnetworkPriorityCloudControl ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->networkPriorityCloudControl()V */
return;
} // .end method
static Integer -$$Nest$mnetworkPriorityMode ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->networkPriorityMode()I */
} // .end method
static void -$$Nest$monVpnNetworkChanged ( com.android.server.net.MiuiNetworkPolicyManagerService p0, android.net.Network p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->onVpnNetworkChanged(Landroid/net/Network;Z)V */
return;
} // .end method
static void -$$Nest$mregisterSleepModeReceiver ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSleepModeReceiver()V */
return;
} // .end method
static void -$$Nest$mregisterWifiNetworkListener ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWifiNetworkListener()V */
return;
} // .end method
static void -$$Nest$mremoveUidState ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->removeUidState(I)V */
return;
} // .end method
static void -$$Nest$msetSleepModeWhitelistUidRules ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setSleepModeWhitelistUidRules()V */
return;
} // .end method
static Boolean -$$Nest$msleepModeEnableControl ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->sleepModeEnableControl()Z */
} // .end method
static void -$$Nest$munregisterSleepModeReceiver ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->unregisterSleepModeReceiver()V */
return;
} // .end method
static void -$$Nest$mupdateLimit ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateLimit(Z)V */
return;
} // .end method
static void -$$Nest$mupdateMobileLatency ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatency()V */
return;
} // .end method
static void -$$Nest$mupdatePowerSaveForUidDataActivityChanged ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Integer p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updatePowerSaveForUidDataActivityChanged(IZ)V */
return;
} // .end method
static void -$$Nest$mupdateRuleForMobileTc ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRuleForMobileTc()V */
return;
} // .end method
static void -$$Nest$mupdateRuleGlobal ( com.android.server.net.MiuiNetworkPolicyManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRuleGlobal()V */
return;
} // .end method
static void -$$Nest$mupdateUidState ( com.android.server.net.MiuiNetworkPolicyManagerService p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateUidState(II)V */
return;
} // .end method
static Integer -$$Nest$sfgetmMobileLatencyState ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static Boolean -$$Nest$smisMobileLatencyAllowed ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .isMobileLatencyAllowed ( );
} // .end method
static com.android.server.net.MiuiNetworkPolicyManagerService ( ) {
/* .locals 7 */
/* .line 134 */
final String v0 = "com.tencent.mm"; // const-string v0, "com.tencent.mm"
final String v1 = "com.tencent.mobileqq"; // const-string v1, "com.tencent.mobileqq"
final String v2 = "com.xiaomi.xmsf"; // const-string v2, "com.xiaomi.xmsf"
final String v3 = "com.google.android.gms"; // const-string v3, "com.google.android.gms"
final String v4 = "com.google.android.dialer"; // const-string v4, "com.google.android.dialer"
final String v5 = "com.whatsapp"; // const-string v5, "com.whatsapp"
final String v6 = "com.miui.vpnsdkmanager"; // const-string v6, "com.miui.vpnsdkmanager"
/* filled-new-array/range {v0 ..v6}, [Ljava/lang/String; */
/* .line 195 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v1, v0, [B */
/* fill-array-data v1, :array_0 */
/* .line 196 */
/* new-array v1, v0, [B */
/* fill-array-data v1, :array_1 */
/* .line 197 */
/* new-array v1, v0, [B */
/* fill-array-data v1, :array_2 */
/* .line 198 */
/* new-array v0, v0, [B */
/* fill-array-data v0, :array_3 */
/* .line 199 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v1, v0, [B */
/* fill-array-data v1, :array_4 */
/* .line 200 */
/* new-array v1, v0, [B */
/* fill-array-data v1, :array_5 */
/* .line 201 */
/* new-array v1, v0, [B */
/* fill-array-data v1, :array_6 */
/* .line 202 */
/* new-array v0, v0, [B */
/* fill-array-data v0, :array_7 */
/* .line 203 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [B */
/* fill-array-data v0, :array_8 */
/* .line 269 */
int v0 = -1; // const/4 v0, -0x1
/* .line 271 */
/* const-string/jumbo v0, "vendor" */
miui.util.FeatureParser .getString ( v0 );
final String v1 = "qcom"; // const-string v1, "qcom"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.android.server.net.MiuiNetworkPolicyManagerService.IS_QCOM = (v0!= 0);
return;
/* nop */
/* :array_0 */
/* .array-data 1 */
/* 0x0t */
/* -0x60t */
/* -0x3at */
/* 0x0t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* 0x0t */
/* 0x50t */
/* -0xet */
/* 0x1t */
} // .end array-data
/* :array_2 */
/* .array-data 1 */
/* 0x0t */
/* 0x50t */
/* -0xet */
/* 0x2t */
} // .end array-data
/* :array_3 */
/* .array-data 1 */
/* 0x0t */
/* 0x50t */
/* -0xet */
/* 0x4t */
} // .end array-data
/* :array_4 */
/* .array-data 1 */
/* 0x0t */
/* 0xct */
/* -0x19t */
} // .end array-data
/* :array_5 */
/* .array-data 1 */
/* 0x0t */
/* 0xat */
/* 0x0t */
} // .end array-data
/* :array_6 */
/* .array-data 1 */
/* 0x0t */
/* 0xct */
/* 0x43t */
} // .end array-data
/* :array_7 */
/* .array-data 1 */
/* 0x0t */
/* 0x17t */
/* -0x5bt */
} // .end array-data
/* :array_8 */
/* .array-data 1 */
/* 0x10t */
/* 0x23t */
} // .end array-data
} // .end method
private com.android.server.net.MiuiNetworkPolicyManagerService ( ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 334 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 129 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mWmmerRouterWhitelist = v0;
/* .line 204 */
/* const-wide/16 v0, 0x2710 */
/* iput-wide v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J */
/* .line 234 */
final String v0 = ""; // const-string v0, ""
this.mDefaultInputMethod = v0;
/* .line 238 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z */
/* .line 239 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mRpsEnabled:Z */
/* .line 242 */
/* new-instance v2, Landroid/util/SparseIntArray; */
/* invoke-direct {v2}, Landroid/util/SparseIntArray;-><init>()V */
this.mUidState = v2;
/* .line 270 */
v2 = com.android.internal.telephony.PhoneConstants$DataState.DISCONNECTED;
this.mConnectState = v2;
/* .line 280 */
int v2 = 0; // const/4 v2, 0x0
this.mNetworkBoostService = v2;
/* .line 289 */
/* iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z */
/* .line 290 */
/* iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z */
/* .line 1143 */
/* new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14; */
/* invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$14;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V */
this.mWifiStateReceiver = v3;
/* .line 1522 */
/* new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24; */
/* invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$24;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V */
this.mHandlerCallback = v3;
/* .line 1645 */
/* new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$25; */
/* invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$25;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V */
this.mUidDataActivityObserver = v3;
/* .line 1656 */
/* new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26; */
/* invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$26;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V */
this.mPackageReceiver = v3;
/* .line 1769 */
/* new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29; */
/* invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$29;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V */
this.mMobileNwReceiver = v3;
/* .line 2048 */
/* iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnter:Z */
/* .line 2049 */
/* iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mSleepModeEnable:Z */
/* .line 2097 */
/* new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32; */
/* invoke-direct {v3, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$32;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V */
this.mSleepModeReceiver = v3;
/* .line 2240 */
/* iput v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
/* .line 2241 */
this.mThermalStateListener = v2;
/* .line 335 */
this.mContext = p1;
/* .line 336 */
/* new-instance v2, Landroid/os/HandlerThread; */
final String v3 = "MiuiNetworkPolicy"; // const-string v3, "MiuiNetworkPolicy"
/* invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 337 */
/* .local v2, "thread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v2 ).start ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V
/* .line 338 */
/* new-instance v4, Landroid/os/Handler; */
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
v6 = this.mHandlerCallback;
/* invoke-direct {v4, v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V */
this.mHandler = v4;
/* .line 339 */
/* new-instance v5, Ljava/util/HashSet; */
/* invoke-direct {v5}, Ljava/util/HashSet;-><init>()V */
this.mUnRestrictApps = v5;
/* .line 340 */
/* new-instance v5, Ljava/util/HashSet; */
/* invoke-direct {v5}, Ljava/util/HashSet;-><init>()V */
this.mLowLatencyApps = v5;
/* .line 341 */
/* new-instance v5, Ljava/util/HashSet; */
/* invoke-direct {v5}, Ljava/util/HashSet;-><init>()V */
this.mNeedRestrictPowerSaveApps = v5;
/* .line 342 */
/* new-instance v5, Ljava/util/HashSet; */
/* invoke-direct {v5}, Ljava/util/HashSet;-><init>()V */
this.mMobileLowLatencyApps = v5;
/* .line 343 */
/* new-instance v5, Ljava/util/LinkedList; */
/* invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V */
this.mHistoryBandWidth = v5;
/* .line 344 */
/* const/16 v5, 0xff */
/* iput v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I */
/* .line 345 */
/* iput v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I */
/* .line 346 */
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport; */
/* invoke-direct {v1, p1, v4}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
this.mSupport = v1;
/* .line 347 */
/* new-instance v1, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v1}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 348 */
(( android.net.NetworkRequest$Builder ) v1 ).addTransportType ( v0 ); // invoke-virtual {v1, v0}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 349 */
int v1 = 4; // const/4 v1, 0x4
(( android.net.NetworkRequest$Builder ) v0 ).addTransportType ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 350 */
/* const/16 v5, 0xf */
(( android.net.NetworkRequest$Builder ) v0 ).removeCapability ( v5 ); // invoke-virtual {v0, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
(( android.net.NetworkRequest$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
this.mWifiNetworkRequest = v0;
/* .line 351 */
/* new-instance v0, Landroid/net/NetworkRequest$Builder; */
/* invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V */
/* .line 352 */
(( android.net.NetworkRequest$Builder ) v0 ).addTransportType ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;
/* .line 353 */
(( android.net.NetworkRequest$Builder ) v0 ).removeCapability ( v5 ); // invoke-virtual {v0, v5}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;
(( android.net.NetworkRequest$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;
this.mVPNNetworkRequest = v0;
/* .line 354 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V */
this.mWifiNetworkCallback = v0;
/* .line 405 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$2;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V */
this.mVPNNetworkCallback = v0;
/* .line 418 */
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .isQosFeatureAllowed ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 419 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils; */
/* invoke-direct {v0, p1, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
this.mQosUtils = v0;
/* .line 421 */
} // :cond_0
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets; */
/* invoke-direct {v0, p1, v4}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
this.mAppBuckets = v0;
/* .line 422 */
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .isMobileTcFeatureAllowed ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 423 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit; */
/* invoke-direct {v0, p1, v4}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;-><init>(Landroid/content/Context;Landroid/os/Handler;)V */
this.mMobileTcUtils = v0;
/* .line 428 */
} // :cond_1
try { // :try_start_0
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener; */
final String v1 = "/sys/class/thermal/thermal_message/wifi_limit"; // const-string v1, "/sys/class/thermal/thermal_message/wifi_limit"
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Ljava/lang/String;)V */
this.mThermalStateListener = v0;
/* .line 429 */
(( com.android.server.net.MiuiNetworkPolicyManagerService$ThermalStateListener ) v0 ).startWatching ( ); // invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$ThermalStateListener;->startWatching()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 433 */
/* .line 431 */
/* :catch_0 */
/* move-exception v0 */
/* .line 432 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Thremal state listener init failed:"; // const-string v4, "Thremal state listener init failed:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v1 );
/* .line 434 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void addHistoryBandWidth ( Long p0 ) {
/* .locals 2 */
/* .param p1, "bwBps" # J */
/* .line 710 */
v0 = v0 = this.mHistoryBandWidth;
/* const/16 v1, 0xa */
/* if-lt v0, v1, :cond_0 */
/* .line 711 */
v0 = this.mHistoryBandWidth;
/* .line 713 */
} // :cond_0
v0 = this.mHistoryBandWidth;
java.lang.Long .valueOf ( p1,p2 );
/* .line 714 */
return;
} // .end method
private java.lang.String bytesToString ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "bytes" # [B */
/* .line 2031 */
/* if-nez p1, :cond_0 */
/* .line 2032 */
final String v0 = ""; // const-string v0, ""
/* .line 2034 */
} // :cond_0
/* new-instance v0, Ljava/lang/String; */
/* invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V */
} // .end method
private void calculateBandWidth ( ) {
/* .locals 10 */
/* .line 689 */
v0 = this.mInterfaceName;
android.net.TrafficStats .getRxBytes ( v0 );
/* move-result-wide v0 */
/* .line 690 */
/* .local v0, "rxBytes":J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v4, v0, v2 */
final String v5 = "MiuiNetworkPolicy"; // const-string v5, "MiuiNetworkPolicy"
/* if-ltz v4, :cond_0 */
/* iget-wide v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J */
/* cmp-long v4, v6, v0 */
/* if-lez v4, :cond_1 */
/* .line 691 */
} // :cond_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "rxByte: "; // const-string v6, "rxByte: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0, v1 ); // invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v6 = ", mLastRxBytes: "; // const-string v6, ", mLastRxBytes: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J */
(( java.lang.StringBuilder ) v4 ).append ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v4 );
/* .line 692 */
/* iput-wide v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J */
/* .line 694 */
} // :cond_1
/* iget-wide v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J */
/* cmp-long v4, v6, v2 */
/* if-nez v4, :cond_2 */
/* cmp-long v2, v0, v2 */
/* if-ltz v2, :cond_2 */
/* .line 695 */
/* iput-wide v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J */
/* .line 696 */
return;
/* .line 698 */
} // :cond_2
/* sub-long v2, v0, v6 */
/* const-wide/16 v6, 0x3 */
/* div-long/2addr v2, v6 */
/* .line 699 */
/* .local v2, "bwBps":J */
/* const-wide/32 v6, 0x30d40 */
/* cmp-long v4, v2, v6 */
/* if-ltz v4, :cond_3 */
/* .line 700 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->addHistoryBandWidth(J)V */
/* .line 703 */
} // :cond_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "bandwidth: "; // const-string v6, "bandwidth: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-wide/16 v6, 0x3e8 */
/* div-long v8, v2, v6 */
(( java.lang.StringBuilder ) v4 ).append ( v8, v9 ); // invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = " KB/s, Max bandwidth: "; // const-string v8, " KB/s, Max bandwidth: "
(( java.lang.StringBuilder ) v4 ).append ( v8 ); // invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mHistoryBandWidth;
/* .line 704 */
java.util.Collections .max ( v8 );
/* check-cast v8, Ljava/lang/Long; */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
/* div-long/2addr v8, v6 */
(( java.lang.StringBuilder ) v4 ).append ( v8, v9 ); // invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v6 = " KB/s"; // const-string v6, " KB/s"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 703 */
android.util.Log .i ( v5,v4 );
/* .line 706 */
/* iput-wide v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastRxBytes:J */
/* .line 707 */
return;
} // .end method
private void checkEnableAutoForward ( android.net.NetworkCapabilities p0, android.net.Network p1 ) {
/* .locals 6 */
/* .param p1, "networkCapabilities" # Landroid/net/NetworkCapabilities; */
/* .param p2, "network" # Landroid/net/Network; */
/* .line 462 */
/* nop */
/* .line 463 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 462 */
int v1 = 1; // const/4 v1, 0x1
/* xor-int/2addr v0, v1 */
final String v2 = "persist.sys.miui_optimization"; // const-string v2, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v2,v0 );
/* xor-int/2addr v0, v1 */
/* .line 464 */
/* .local v0, "isCtsMode":Z */
if ( p1 != null) { // if-eqz p1, :cond_4
/* if-nez v0, :cond_4 */
v2 = this.mSlaveWifiNetwork;
/* if-nez v2, :cond_4 */
v2 = this.mVpnNetwork;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 468 */
} // :cond_0
this.mCurrentNetwork = p2;
/* .line 469 */
/* const/16 v2, 0xc */
v2 = (( android.net.NetworkCapabilities ) p1 ).hasCapability ( v2 ); // invoke-virtual {p1, v2}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 470 */
/* const/16 v2, 0x10 */
v2 = (( android.net.NetworkCapabilities ) p1 ).hasCapability ( v2 ); // invoke-virtual {p1, v2}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v1 */
} // :cond_1
/* move v2, v3 */
/* .line 471 */
/* .local v2, "isValidated":Z */
} // :goto_0
final String v4 = "MiuiNetworkPolicy"; // const-string v4, "MiuiNetworkPolicy"
/* if-nez v2, :cond_2 */
/* iget-boolean v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z */
/* if-nez v5, :cond_2 */
/* .line 472 */
final String v3 = "add auto forward rule "; // const-string v3, "add auto forward rule "
android.util.Log .d ( v4,v3 );
/* .line 473 */
/* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableAutoForward(Z)V */
/* .line 474 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 475 */
/* const-string/jumbo v1, "validation pass, remove auto forward rule" */
android.util.Log .d ( v4,v1 );
/* .line 476 */
/* invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableAutoForward(Z)V */
/* .line 478 */
} // :cond_3
} // :goto_1
return;
/* .line 466 */
} // .end local v2 # "isValidated":Z
} // :cond_4
} // :goto_2
return;
} // .end method
private Boolean checkIEMTK ( java.util.List p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/net/wifi/ScanResult$InformationElement;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1203 */
/* .local p1, "infoElements":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult$InformationElement;>;" */
int v0 = 1; // const/4 v0, 0x1
v1 = if ( p1 != null) { // if-eqz p1, :cond_6
/* if-nez v1, :cond_0 */
/* .line 1207 */
} // :cond_0
v2 = } // :goto_0
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_5
/* check-cast v2, Landroid/net/wifi/ScanResult$InformationElement; */
/* .line 1210 */
/* .local v2, "ie":Landroid/net/wifi/ScanResult$InformationElement; */
v4 = (( android.net.wifi.ScanResult$InformationElement ) v2 ).getId ( ); // invoke-virtual {v2}, Landroid/net/wifi/ScanResult$InformationElement;->getId()I
/* const/16 v5, 0xdd */
/* if-eq v4, v5, :cond_1 */
/* .line 1212 */
} // :cond_1
int v4 = 3; // const/4 v4, 0x3
/* new-array v5, v4, [B */
/* .line 1214 */
/* .local v5, "oui":[B */
try { // :try_start_0
(( android.net.wifi.ScanResult$InformationElement ) v2 ).getBytes ( ); // invoke-virtual {v2}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;
(( java.nio.ByteBuffer ) v6 ).get ( v5, v3, v4 ); // invoke-virtual {v6, v5, v3, v4}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1218 */
/* nop */
/* .line 1220 */
v4 = com.android.server.net.MiuiNetworkPolicyManagerService.MTK_OUI1;
v4 = java.util.Arrays .equals ( v5,v4 );
/* if-nez v4, :cond_2 */
v4 = com.android.server.net.MiuiNetworkPolicyManagerService.MTK_OUI2;
v4 = java.util.Arrays .equals ( v5,v4 );
/* if-nez v4, :cond_2 */
v4 = com.android.server.net.MiuiNetworkPolicyManagerService.MTK_OUI3;
/* .line 1221 */
v4 = java.util.Arrays .equals ( v5,v4 );
/* if-nez v4, :cond_2 */
v4 = com.android.server.net.MiuiNetworkPolicyManagerService.MTK_OUI4;
v4 = java.util.Arrays .equals ( v5,v4 );
if ( v4 != null) { // if-eqz v4, :cond_3
} // :cond_2
/* move v3, v0 */
/* .line 1222 */
/* .local v3, "isMtk":Z */
} // :cond_3
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1223 */
/* .line 1225 */
} // .end local v2 # "ie":Landroid/net/wifi/ScanResult$InformationElement;
} // .end local v3 # "isMtk":Z
} // .end local v5 # "oui":[B
} // :cond_4
/* .line 1215 */
/* .restart local v2 # "ie":Landroid/net/wifi/ScanResult$InformationElement; */
/* .restart local v5 # "oui":[B */
/* :catch_0 */
/* move-exception v0 */
/* .line 1216 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
final String v4 = "Failed to set network priority support config"; // const-string v4, "Failed to set network priority support config"
android.util.Log .e ( v1,v4,v0 );
/* .line 1217 */
/* .line 1227 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v2 # "ie":Landroid/net/wifi/ScanResult$InformationElement;
} // .end local v5 # "oui":[B
} // :cond_5
/* .line 1204 */
} // :cond_6
} // :goto_1
} // .end method
private Boolean checkRouterMTK ( ) {
/* .locals 9 */
/* .line 1180 */
v0 = this.mContext;
/* const-string/jumbo v1, "wifi" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
/* .line 1181 */
/* .local v0, "wifiManager":Landroid/net/wifi/WifiManager; */
(( android.net.wifi.WifiManager ) v0 ).getConnectionInfo ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
/* .line 1182 */
/* .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo; */
(( android.net.wifi.WifiInfo ) v1 ).getBSSID ( ); // invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;
/* .line 1183 */
/* .local v2, "bssid":Ljava/lang/String; */
(( android.net.wifi.WifiManager ) v0 ).getScanResults ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;
/* .line 1186 */
/* .local v3, "scanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;" */
int v4 = 1; // const/4 v4, 0x1
v5 = if ( v2 != null) { // if-eqz v2, :cond_0
if ( v5 != null) { // if-eqz v5, :cond_1
} // :cond_0
/* iget-wide v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J */
/* const-wide/32 v7, 0x2dc6c0 */
/* cmp-long v7, v5, v7 */
/* if-gez v7, :cond_1 */
/* .line 1187 */
v7 = this.mHandler;
/* const/16 v8, 0xb */
(( android.os.Handler ) v7 ).sendEmptyMessageDelayed ( v8, v5, v6 ); // invoke-virtual {v7, v8, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 1188 */
/* iget-wide v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J */
/* const-wide/16 v7, 0x2 */
/* mul-long/2addr v5, v7 */
/* iput-wide v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J */
/* .line 1189 */
/* .line 1191 */
} // :cond_1
/* const-wide/16 v5, 0x2710 */
/* iput-wide v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->ROUTER_DETECT_TIME:J */
/* .line 1193 */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_3
/* check-cast v6, Landroid/net/wifi/ScanResult; */
/* .line 1194 */
/* .local v6, "scanResult":Landroid/net/wifi/ScanResult; */
v7 = this.BSSID;
v7 = android.text.TextUtils .equals ( v2,v7 );
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 1195 */
(( android.net.wifi.ScanResult ) v6 ).getInformationElements ( ); // invoke-virtual {v6}, Landroid/net/wifi/ScanResult;->getInformationElements()Ljava/util/List;
v7 = /* invoke-direct {p0, v7}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->checkIEMTK(Ljava/util/List;)Z */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 1196 */
/* .line 1198 */
} // .end local v6 # "scanResult":Landroid/net/wifi/ScanResult;
} // :cond_2
/* .line 1199 */
} // :cond_3
int v4 = 0; // const/4 v4, 0x0
} // .end method
private void checkWifiLimit ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 2359 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
int v1 = 0; // const/4 v1, 0x0
/* .line 2361 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
int v2 = 0; // const/4 v2, 0x0
/* .line 2362 */
/* .local v2, "line":Ljava/lang/String; */
try { // :try_start_0
final String v3 = "/sys/class/thermal/thermal_message/wifi_limit"; // const-string v3, "/sys/class/thermal/thermal_message/wifi_limit"
/* invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->readLine(Ljava/lang/String;)Ljava/lang/String; */
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 2363 */
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 2364 */
/* .local v3, "str_temp":Ljava/lang/String; */
v4 = java.lang.Integer .parseInt ( v3 );
/* .line 2365 */
/* .local v4, "wifi_limit":I */
/* move v5, v4 */
/* .line 2366 */
/* .local v5, "thermalMode":I */
int v6 = 0; // const/4 v6, 0x0
/* .line 2368 */
/* .local v6, "ret":Z */
/* const/16 v7, 0xa */
/* if-le v4, v7, :cond_0 */
/* .line 2369 */
v7 = /* invoke-direct {p0, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getThermalWifiLimitMode(I)I */
/* move v5, v7 */
/* .line 2372 */
} // :cond_0
v7 = /* invoke-direct {p0, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->setThermalPolicy(I)Z */
/* if-nez v7, :cond_1 */
/* .line 2373 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "setThermalPolicy failed: " */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " path: "; // const-string v8, " path: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .w ( v0,v7 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2379 */
} // .end local v2 # "line":Ljava/lang/String;
} // .end local v3 # "str_temp":Ljava/lang/String;
} // .end local v4 # "wifi_limit":I
} // .end local v5 # "thermalMode":I
} // .end local v6 # "ret":Z
} // :cond_1
/* .line 2377 */
/* :catch_0 */
/* move-exception v2 */
/* .line 2378 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "checkWifiLimit:"; // const-string v3, "checkWifiLimit:"
android.util.Log .e ( v0,v3,v2 );
/* .line 2380 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void clearSleepModeWhitelistUidRules ( ) {
/* .locals 3 */
/* .line 2194 */
v0 = v0 = this.mSleepModeWhitelistUids;
/* if-nez v0, :cond_1 */
/* .line 2195 */
v0 = this.mSleepModeWhitelistUids;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 2196 */
/* .local v1, "uid":I */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateSleepModeWhitelistUidRules(IZ)V */
/* .line 2197 */
} // .end local v1 # "uid":I
/* .line 2198 */
} // :cond_0
v0 = this.mSleepModeWhitelistUids;
/* .line 2200 */
} // :cond_1
return;
} // .end method
private void enableAutoForward ( Boolean p0 ) {
/* .locals 10 */
/* .param p1, "enable" # Z */
/* .line 306 */
/* if-nez p1, :cond_0 */
/* .line 307 */
v0 = this.mNetworkManager;
v1 = this.mLastIpAddress;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastNetId:I */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).enableAutoForward ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->enableAutoForward(Ljava/lang/String;IZ)I
/* .line 308 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z */
/* .line 309 */
return;
/* .line 311 */
} // :cond_0
v0 = this.mCurrentNetwork;
v0 = (( android.net.Network ) v0 ).getNetId ( ); // invoke-virtual {v0}, Landroid/net/Network;->getNetId()I
/* .line 312 */
/* .local v0, "netId":I */
v1 = this.mConnectivityManager;
v2 = this.mCurrentNetwork;
(( android.net.ConnectivityManager ) v1 ).getLinkProperties ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;
/* .line 313 */
/* .local v1, "linkProperties":Landroid/net/LinkProperties; */
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
/* if-nez v1, :cond_1 */
/* .line 314 */
final String v3 = "link properties is null"; // const-string v3, "link properties is null"
android.util.Log .d ( v2,v3 );
/* .line 315 */
return;
/* .line 317 */
} // :cond_1
(( android.net.LinkProperties ) v1 ).getLinkAddresses ( ); // invoke-virtual {v1}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;
/* .line 318 */
/* .local v3, "curAddr":Ljava/util/List;, "Ljava/util/List<Landroid/net/LinkAddress;>;" */
(( android.net.LinkProperties ) v1 ).getInterfaceName ( ); // invoke-virtual {v1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
/* .line 320 */
/* .local v4, "iface":Ljava/lang/String; */
/* const-string/jumbo v5, "wlan0" */
v5 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
v5 = if ( v3 != null) { // if-eqz v3, :cond_3
/* if-lez v5, :cond_3 */
/* .line 321 */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_3
/* check-cast v6, Landroid/net/LinkAddress; */
/* .line 322 */
/* .local v6, "address":Landroid/net/LinkAddress; */
(( android.net.LinkAddress ) v6 ).getAddress ( ); // invoke-virtual {v6}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;
/* instance-of v7, v7, Ljava/net/Inet4Address; */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 323 */
(( android.net.LinkAddress ) v6 ).getAddress ( ); // invoke-virtual {v6}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v7 ).getHostAddress ( ); // invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* .line 324 */
/* .local v7, "inet4Addr":Ljava/lang/String; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "enable autoforward, current address: "; // const-string v9, "enable autoforward, current address: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ", netid = "; // const-string v9, ", netid = "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v8 );
/* .line 325 */
v8 = this.mNetworkManager;
int v9 = 1; // const/4 v9, 0x1
(( com.android.server.net.MiuiNetworkManagementService ) v8 ).enableAutoForward ( v7, v0, v9 ); // invoke-virtual {v8, v7, v0, v9}, Lcom/android/server/net/MiuiNetworkManagementService;->enableAutoForward(Ljava/lang/String;IZ)I
/* .line 326 */
this.mLastIpAddress = v7;
/* .line 327 */
/* iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLastNetId:I */
/* .line 328 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z */
/* .line 330 */
} // .end local v6 # "address":Landroid/net/LinkAddress;
} // .end local v7 # "inet4Addr":Ljava/lang/String;
} // :cond_2
/* .line 332 */
} // :cond_3
return;
} // .end method
private void enableBandwidthPoll ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 717 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 718 */
v0 = this.mHistoryBandWidth;
/* .line 719 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 720 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 721 */
/* const-wide/32 v0, 0x30d40 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->addHistoryBandWidth(J)V */
/* .line 723 */
} // :cond_0
return;
} // .end method
private void enableMobileLowLatency ( Boolean p0 ) {
/* .locals 9 */
/* .param p1, "enable" # Z */
/* .line 1804 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enableMobileLowLatency enable = "; // const-string v1, "enableMobileLowLatency enable = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",mMobileLatencyState="; // const-string v1, ",mMobileLatencyState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .i ( v1,v0 );
/* .line 1806 */
/* move v0, p1 */
/* .line 1807 */
/* .local v0, "mobileState":I */
/* if-eq v1, v0, :cond_2 */
/* .line 1808 */
/* .line 1809 */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 1810 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "com.android.phone.intent.action.CHANGE_LEVEL"; // const-string v2, "com.android.phone.intent.action.CHANGE_LEVEL"
(( android.content.Intent ) v1 ).setAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1811 */
final String v2 = "com.android.phone"; // const-string v2, "com.android.phone"
(( android.content.Intent ) v1 ).setPackage ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1812 */
final String v2 = "Rat_type"; // const-string v2, "Rat_type"
/* const-wide/16 v3, 0x0 */
(( android.content.Intent ) v1 ).putExtra ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 1813 */
/* const-wide/16 v2, 0x4 */
/* const-wide/16 v4, 0x1 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* move-wide v6, v2 */
} // :cond_0
/* move-wide v6, v4 */
} // :goto_0
final String v8 = "Level_UL"; // const-string v8, "Level_UL"
(( android.content.Intent ) v1 ).putExtra ( v8, v6, v7 ); // invoke-virtual {v1, v8, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 1814 */
if ( p1 != null) { // if-eqz p1, :cond_1
} // :cond_1
/* move-wide v2, v4 */
} // :goto_1
final String v4 = "Level_DL"; // const-string v4, "Level_DL"
(( android.content.Intent ) v1 ).putExtra ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 1815 */
v2 = this.mContext;
(( android.content.Context ) v2 ).sendBroadcast ( v1 ); // invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 1817 */
} // .end local v1 # "intent":Landroid/content/Intent;
} // :cond_2
return;
} // .end method
private void enableNetworkPriority ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "mode" # I */
/* .line 849 */
int v0 = 0; // const/4 v0, 0x0
/* .line 850 */
/* .local v0, "isNeedUpdate":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled()Z */
/* .line 851 */
/* .local v1, "wasLimitterEnabled":Z */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled(I)Z */
/* .line 852 */
/* .local v2, "isLimitterEnabled":Z */
/* iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I */
/* .line 854 */
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
/* if-ne p1, v4, :cond_0 */
/* .line 855 */
/* invoke-direct {p0, v4}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableBandwidthPoll(Z)V */
/* .line 857 */
} // :cond_0
/* invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableBandwidthPoll(Z)V */
/* .line 859 */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* if-nez v2, :cond_1 */
/* .line 860 */
v4 = this.mHandler;
int v5 = 3; // const/4 v5, 0x3
(( android.os.Handler ) v4 ).obtainMessage ( v5, v3, v3 ); // invoke-virtual {v4, v5, v3, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v4 ).sendMessage ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 861 */
v4 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v4 ).enableLimitter ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->enableLimitter(Z)Z
/* .line 862 */
return;
/* .line 863 */
} // :cond_1
/* if-nez v1, :cond_2 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 864 */
v3 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v3 ).enableLimitter ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->enableLimitter(Z)Z
/* .line 865 */
int v0 = 1; // const/4 v0, 0x1
/* .line 866 */
v3 = this.mHandler;
int v4 = 5; // const/4 v4, 0x5
/* const-wide/16 v5, 0x0 */
(( android.os.Handler ) v3 ).sendEmptyMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 867 */
} // :cond_2
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 868 */
v3 = this.mHandler;
int v4 = 4; // const/4 v4, 0x4
(( android.os.Handler ) v3 ).removeMessages ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V
/* .line 869 */
v3 = this.mHandler;
(( android.os.Handler ) v3 ).sendEmptyMessage ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 871 */
} // :cond_3
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 872 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRuleGlobal()V */
/* .line 875 */
} // :cond_4
v3 = this.mNetworkBoostService;
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 876 */
/* iget v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I */
/* iget v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I */
/* iget v6, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
(( com.xiaomi.NetworkBoost.NetworkBoostService ) v3 ).onNetworkPriorityChanged ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onNetworkPriorityChanged(III)V
/* .line 878 */
} // :cond_5
return;
} // .end method
private void enablePowerSave ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .line 670 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enable ps, mPS = "; // const-string v1, "enable ps, mPS = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", enable = "; // const-string v1, ", enable = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .i ( v1,v0 );
/* .line 671 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 672 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z */
/* .line 673 */
v0 = this.mSupport;
(( com.android.server.net.MiuiNetworkPolicyServiceSupport ) v0 ).enablePowerSave ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->enablePowerSave(Z)V
/* .line 675 */
} // :cond_0
return;
} // .end method
private void enableRps ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 1842 */
v0 = this.mSupport;
v1 = this.mInterfaceName;
(( com.android.server.net.MiuiNetworkPolicyServiceSupport ) v0 ).updateIface ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->updateIface(Ljava/lang/String;)Ljava/lang/String;
/* .line 1843 */
/* .local v0, "iface":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enableRps interface = "; // const-string v2, "enableRps interface = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "enable = "; // const-string v2, "enable = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .d ( v2,v1 );
/* .line 1844 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1845 */
v1 = this.mNetworkManager;
v2 = this.mInterfaceName;
(( com.android.server.net.MiuiNetworkManagementService ) v1 ).enableRps ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableRps(Ljava/lang/String;Z)Z
/* .line 1847 */
} // :cond_0
return;
} // .end method
private void enableSleepModeChain ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "enable" # Z */
/* .line 2218 */
final String v0 = "MiuiNetworkPolicySleepMode"; // const-string v0, "MiuiNetworkPolicySleepMode"
try { // :try_start_0
v1 = this.cm;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2219 */
final String v1 = "android.net.ConnectivityManager"; // const-string v1, "android.net.ConnectivityManager"
java.lang.Class .forName ( v1 );
/* .line 2220 */
/* .local v1, "clazz":Ljava/lang/Class; */
final String v2 = "enableSleepModeChain"; // const-string v2, "enableSleepModeChain"
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
v5 = java.lang.Boolean.TYPE;
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
(( java.lang.Class ) v1 ).getDeclaredMethod ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 2221 */
/* .local v2, "method":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 2222 */
v4 = this.cm;
/* new-array v3, v3, [Ljava/lang/Object; */
java.lang.Boolean .valueOf ( p1 );
/* aput-object v5, v3, v6 */
(( java.lang.reflect.Method ) v2 ).invoke ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 2223 */
/* .local v3, "result":Ljava/lang/Object; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "enableSleepModeChain enable:"; // const-string v5, "enableSleepModeChain enable:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = " result:"; // const-string v5, " result:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2227 */
} // .end local v1 # "clazz":Ljava/lang/Class;
} // .end local v2 # "method":Ljava/lang/reflect/Method;
} // .end local v3 # "result":Ljava/lang/Object;
} // :cond_0
/* .line 2225 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2226 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "enableSleepModeChain error!"; // const-string v2, "enableSleepModeChain error!"
android.util.Log .e ( v0,v2,v1 );
/* .line 2228 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void enableWmmer ( ) {
/* .locals 1 */
/* .line 881 */
v0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isWmmerEnabled()Z */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableWmmer(Z)V */
/* .line 882 */
return;
} // .end method
private void enableWmmer ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enable" # Z */
/* .line 885 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerEnable:Z */
/* if-eq v0, p1, :cond_1 */
/* .line 886 */
v0 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).enableWmmer ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableWmmer(Z)Z
/* .line 887 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 888 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRuleGlobal()V */
/* .line 890 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerEnable:Z */
/* .line 892 */
} // :cond_1
return;
} // .end method
public static com.android.server.net.MiuiNetworkPolicyManagerService get ( ) {
/* .locals 2 */
/* .line 298 */
v0 = com.android.server.net.MiuiNetworkPolicyManagerService.sSelf;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 299 */
/* .line 301 */
} // :cond_0
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "MiuiNetworkPolicyManagerService has not been initialized "; // const-string v1, "MiuiNetworkPolicyManagerService has not been initialized "
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
private java.lang.String getDefaultInputMethod ( ) {
/* .locals 4 */
/* .line 1374 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "default_input_method"; // const-string v1, "default_input_method"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 1376 */
/* .local v0, "inputMethod":Ljava/lang/String; */
final String v1 = ""; // const-string v1, ""
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1377 */
android.content.ComponentName .unflattenFromString ( v0 );
/* .line 1378 */
/* .local v2, "inputMethodCptName":Landroid/content/ComponentName; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1379 */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1380 */
/* .local v3, "inputMethodPkgName":Ljava/lang/String; */
/* if-nez v3, :cond_0 */
} // :cond_0
/* move-object v1, v3 */
} // :goto_0
/* .line 1383 */
} // .end local v2 # "inputMethodCptName":Landroid/content/ComponentName;
} // .end local v3 # "inputMethodPkgName":Ljava/lang/String;
} // :cond_1
} // .end method
private Integer getDisableLimitTimeout ( ) {
/* .locals 3 */
/* .line 2273 */
/* iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 2274 */
/* const/16 v0, 0x1388 */
/* .line 2278 */
} // :cond_0
/* const/16 v0, 0x1388 */
/* .line 2279 */
/* .local v0, "timeout":I */
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
/* packed-switch v1, :pswitch_data_0 */
/* .line 2293 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getDisableLimitTimeout when thermal mode is:"; // const-string v2, "getDisableLimitTimeout when thermal mode is:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .i ( v2,v1 );
/* .line 2289 */
/* :pswitch_0 */
/* const/16 v0, 0x1388 */
/* .line 2290 */
/* .line 2285 */
/* :pswitch_1 */
/* const/16 v0, 0x1d4c */
/* .line 2286 */
/* .line 2281 */
/* :pswitch_2 */
/* const/16 v0, 0x3a98 */
/* .line 2282 */
/* nop */
/* .line 2296 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Integer getEnableLimitTimeout ( ) {
/* .locals 3 */
/* .line 2245 */
/* iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 2246 */
/* const/16 v0, 0x61a8 */
/* .line 2250 */
} // :cond_0
/* const/16 v0, 0x61a8 */
/* .line 2251 */
/* .local v0, "timeout":I */
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
/* packed-switch v1, :pswitch_data_0 */
/* .line 2265 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getEnableLimitTimeout when thermal mode is:"; // const-string v2, "getEnableLimitTimeout when thermal mode is:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .i ( v2,v1 );
/* .line 2261 */
/* :pswitch_0 */
/* const/16 v0, 0x61a8 */
/* .line 2262 */
/* .line 2257 */
/* :pswitch_1 */
/* const/16 v0, 0x57e4 */
/* .line 2258 */
/* .line 2253 */
/* :pswitch_2 */
/* const/16 v0, 0x3a98 */
/* .line 2254 */
/* nop */
/* .line 2268 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private java.util.Set getLowLatencyApps ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* ")", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1424 */
/* nop */
/* .line 1425 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1424 */
final String v1 = "cloud_lowlatency_whitelist"; // const-string v1, "cloud_lowlatency_whitelist"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1426 */
/* .local v0, "whiteString":Ljava/lang/String; */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 1427 */
/* .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_0 */
/* .line 1428 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1429 */
/* .local v2, "packages":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1430 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_0 */
/* .line 1431 */
/* aget-object v4, v2, v3 */
/* .line 1430 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1435 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_0
} // .end method
private java.util.Set getMobileLowLatencyApps ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* ")", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1753 */
/* nop */
/* .line 1754 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1753 */
final String v1 = "cloud_block_scan_applist_for_mobile"; // const-string v1, "cloud_block_scan_applist_for_mobile"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1755 */
/* .local v0, "appString":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getMobileLowLatencyApps appString="; // const-string v2, "getMobileLowLatencyApps appString="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .i ( v2,v1 );
/* .line 1756 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 1757 */
/* .local v1, "appList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_0 */
/* .line 1758 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1759 */
/* .local v2, "packages":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1760 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_0 */
/* .line 1761 */
/* aget-object v4, v2, v3 */
/* .line 1760 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1765 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_0
} // .end method
private java.util.Set getNeedRestrictPowerSaveApps ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* ")", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1323 */
/* nop */
/* .line 1324 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1323 */
final String v1 = "cloud_block_scan_applist"; // const-string v1, "cloud_block_scan_applist"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1325 */
/* .local v0, "appString":Ljava/lang/String; */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 1326 */
/* .local v1, "appList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_0 */
/* .line 1327 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1328 */
/* .local v2, "packages":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1329 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_0 */
/* .line 1330 */
/* aget-object v4, v2, v3 */
/* .line 1329 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1334 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_0
} // .end method
private java.lang.String getNetworkPriorityCloudValue ( ) {
/* .locals 3 */
/* .line 754 */
/* const-string/jumbo v0, "vendor" */
miui.util.FeatureParser .getString ( v0 );
final String v1 = "mediatek"; // const-string v1, "mediatek"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = -2; // const/4 v1, -0x2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 755 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cloud_mtk_wifi_traffic_priority_mode"; // const-string v2, "cloud_mtk_wifi_traffic_priority_mode"
android.provider.Settings$System .getStringForUser ( v0,v2,v1 );
/* .local v0, "cvalue":Ljava/lang/String; */
/* .line 758 */
} // .end local v0 # "cvalue":Ljava/lang/String;
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "cloud_network_priority_enabled"; // const-string v2, "cloud_network_priority_enabled"
android.provider.Settings$System .getStringForUser ( v0,v2,v1 );
/* .line 761 */
/* .restart local v0 # "cvalue":Ljava/lang/String; */
} // :goto_0
/* if-nez v0, :cond_1 */
/* .line 762 */
final String v0 = ""; // const-string v0, ""
/* .line 764 */
} // :cond_1
} // .end method
private java.lang.String getRouterModel ( ) {
/* .locals 11 */
/* .line 1975 */
v0 = this.mContext;
/* const-string/jumbo v1, "wifi" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/wifi/WifiManager; */
/* .line 1976 */
/* .local v0, "wifiManager":Landroid/net/wifi/WifiManager; */
(( android.net.wifi.WifiManager ) v0 ).getConnectionInfo ( ); // invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;
/* .line 1977 */
/* .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo; */
(( android.net.wifi.WifiInfo ) v1 ).getInformationElements ( ); // invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getInformationElements()Ljava/util/List;
/* .line 1978 */
/* .local v2, "ies":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult$InformationElement;>;" */
final String v3 = ""; // const-string v3, ""
/* .line 1979 */
/* .local v3, "routerModel":Ljava/lang/String; */
final String v4 = "MiuiNetworkPolicy"; // const-string v4, "MiuiNetworkPolicy"
v5 = if ( v2 != null) { // if-eqz v2, :cond_3
/* if-nez v5, :cond_0 */
/* .line 1984 */
} // :cond_0
int v5 = 4; // const/4 v5, 0x4
try { // :try_start_0
/* new-array v6, v5, [B */
/* .line 1986 */
/* .local v6, "ouiInfo":[B */
v8 = } // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_2
/* check-cast v8, Landroid/net/wifi/ScanResult$InformationElement; */
/* .line 1987 */
/* .local v8, "ie":Landroid/net/wifi/ScanResult$InformationElement; */
if ( v8 != null) { // if-eqz v8, :cond_1
v9 = (( android.net.wifi.ScanResult$InformationElement ) v8 ).getId ( ); // invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getId()I
/* const/16 v10, 0xdd */
/* if-ne v9, v10, :cond_1 */
/* .line 1988 */
(( android.net.wifi.ScanResult$InformationElement ) v8 ).getBytes ( ); // invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;
int v10 = 0; // const/4 v10, 0x0
(( java.nio.ByteBuffer ) v9 ).get ( v6, v10, v5 ); // invoke-virtual {v9, v6, v10, v5}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
/* .line 1989 */
v9 = com.android.server.net.MiuiNetworkPolicyManagerService.WPA2_AKM_PSK_IE;
v9 = java.util.Arrays .equals ( v6,v9 );
if ( v9 != null) { // if-eqz v9, :cond_1
(( android.net.wifi.ScanResult$InformationElement ) v8 ).getBytes ( ); // invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;
v9 = (( java.nio.ByteBuffer ) v9 ).hasRemaining ( ); // invoke-virtual {v9}, Ljava/nio/ByteBuffer;->hasRemaining()Z
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 1990 */
(( android.net.wifi.ScanResult$InformationElement ) v8 ).getBytes ( ); // invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;
v5 = (( java.nio.ByteBuffer ) v5 ).remaining ( ); // invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I
/* new-array v5, v5, [B */
/* .line 1991 */
/* .local v5, "data":[B */
(( android.net.wifi.ScanResult$InformationElement ) v8 ).getBytes ( ); // invoke-virtual {v8}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;
(( java.nio.ByteBuffer ) v7 ).get ( v5 ); // invoke-virtual {v7, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 1992 */
/* invoke-direct {p0, v5}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->parseModelName([B)Ljava/lang/String; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v3, v7 */
/* .line 1993 */
/* .line 1996 */
} // .end local v5 # "data":[B
} // .end local v8 # "ie":Landroid/net/wifi/ScanResult$InformationElement;
} // :cond_1
/* .line 2000 */
} // .end local v6 # "ouiInfo":[B
} // :cond_2
} // :goto_1
/* nop */
/* .line 2001 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "getRouterModel = "; // const-string v6, "getRouterModel = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v4,v5 );
/* .line 2002 */
/* .line 1997 */
/* :catch_0 */
/* move-exception v5 */
/* .line 1998 */
/* .local v5, "e":Ljava/lang/Exception; */
final String v6 = "getRouterName Exception"; // const-string v6, "getRouterName Exception"
android.util.Log .e ( v4,v6 );
/* .line 1999 */
/* .line 1980 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_2
final String v5 = "getRouterModel InformationElement empty"; // const-string v5, "getRouterModel InformationElement empty"
android.util.Log .e ( v4,v5 );
/* .line 1981 */
} // .end method
private Integer getThermalWifiLimitMode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "wifi_limit" # I */
/* .line 2389 */
/* div-int/lit16 v0, p1, 0x2710 */
/* rem-int/lit8 v0, v0, 0xa */
} // .end method
private java.util.Set getUnRestrictedApps ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* ")", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1270 */
/* nop */
/* .line 1271 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1270 */
final String v1 = "cloud_network_priority_whitelist"; // const-string v1, "cloud_network_priority_whitelist"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 1272 */
/* .local v0, "whiteString":Ljava/lang/String; */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 1273 */
/* .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_1 */
/* .line 1274 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1275 */
/* .local v2, "packages":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1276 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* array-length v4, v2 */
/* if-ge v3, v4, :cond_0 */
/* .line 1277 */
/* aget-object v4, v2, v3 */
/* .line 1276 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1280 */
} // .end local v2 # "packages":[Ljava/lang/String;
} // .end local v3 # "i":I
} // :cond_0
/* .line 1281 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
v3 = com.android.server.net.MiuiNetworkPolicyManagerService.LOCAL_NETWORK_PRIORITY_WHITELIST;
/* array-length v4, v3 */
/* if-ge v2, v4, :cond_2 */
/* .line 1282 */
/* aget-object v3, v3, v2 */
/* .line 1281 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1285 */
} // .end local v2 # "i":I
} // :cond_2
} // :goto_2
} // .end method
private Integer getWmmForUidState ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 569 */
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isStateWmmed(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 570 */
v0 = this.mLowLatencyApps;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsCaptivePortal:Z */
/* if-nez v0, :cond_0 */
/* .line 571 */
int v0 = 2; // const/4 v0, 0x2
/* .line 573 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 576 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void happyEyeballsEnableCloudControl ( ) {
/* .locals 4 */
/* .line 973 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
v1 = this.mContext;
/* .line 975 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 974 */
final String v2 = "cloud_dns_happy_eyeballs_priority_enabled"; // const-string v2, "cloud_dns_happy_eyeballs_priority_enabled"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 980 */
/* .local v1, "enableValue":Ljava/lang/String; */
try { // :try_start_0
final String v2 = "persist.device_config.netd_native.happy_eyeballs_enable"; // const-string v2, "persist.device_config.netd_native.happy_eyeballs_enable"
final String v3 = "on"; // const-string v3, "on"
v3 = android.text.TextUtils .equals ( v3,v1 );
if ( v3 != null) { // if-eqz v3, :cond_0
final String v3 = "1"; // const-string v3, "1"
} // :cond_0
final String v3 = "0"; // const-string v3, "0"
} // :goto_0
android.os.SystemProperties .set ( v2,v3 );
/* .line 981 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "happy eyeballs enabled = "; // const-string v3, "happy eyeballs enabled = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 984 */
/* .line 982 */
/* :catch_0 */
/* move-exception v2 */
/* .line 983 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "Failed to set happy eyeballs cloud support config"; // const-string v3, "Failed to set happy eyeballs cloud support config"
android.util.Log .e ( v0,v3,v2 );
/* .line 985 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private void happyEyeballsMasterServerPriorityTimeCloudControl ( ) {
/* .locals 4 */
/* .line 988 */
v0 = this.mContext;
/* .line 990 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 989 */
final String v1 = "cloud_dns_happy_eyeballs_priority_time"; // const-string v1, "cloud_dns_happy_eyeballs_priority_time"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 995 */
/* .local v0, "timeValue":Ljava/lang/String; */
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
if ( v0 != null) { // if-eqz v0, :cond_0
try { // :try_start_0
final String v2 = ""; // const-string v2, ""
/* if-eq v0, v2, :cond_0 */
/* .line 996 */
final String v2 = "persist.device_config.netd_native.happy_eyeballs_master_server_priority_time"; // const-string v2, "persist.device_config.netd_native.happy_eyeballs_master_server_priority_time"
android.os.SystemProperties .set ( v2,v0 );
/* .line 998 */
/* :catch_0 */
/* move-exception v2 */
/* .line 997 */
} // :cond_0
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "happy eyeballs master server time = "; // const-string v3, "happy eyeballs master server time = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1000 */
/* .line 999 */
/* .local v2, "e":Ljava/lang/Exception; */
} // :goto_1
final String v3 = "Failed to set happy eyeballs master server time"; // const-string v3, "Failed to set happy eyeballs master server time"
android.util.Log .e ( v1,v3,v2 );
/* .line 1001 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
private Boolean isAutoForwardEnabled ( ) {
/* .locals 3 */
/* .line 895 */
v0 = this.mContext;
/* .line 896 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 895 */
final String v1 = "cloud_auto_forward_enabled"; // const-string v1, "cloud_auto_forward_enabled"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 897 */
/* .local v0, "cvalue":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v1 = "on"; // const-string v1, "on"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean isLimitterEnabled ( ) {
/* .locals 1 */
/* .line 806 */
/* iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled(I)Z */
} // .end method
private Boolean isLimitterEnabled ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 810 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p1, v0, :cond_1 */
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
} // .end method
private static Boolean isMobileLatencyAllowed ( ) {
/* .locals 1 */
/* .line 1850 */
/* nop */
/* .line 1851 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isMobileLatencyEnabledForUid ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 1791 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "isMLEnabled state:"; // const-string v1, "isMLEnabled state:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",uid:"; // const-string v1, ",uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",connect:"; // const-string v1, ",connect:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mConnectState;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .i ( v1,v0 );
/* .line 1792 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
v0 = this.mConnectState;
v1 = com.android.internal.telephony.PhoneConstants$DataState.CONNECTED;
/* if-ne v0, v1, :cond_0 */
v0 = this.mMobileLowLatencyApps;
/* .line 1794 */
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1792 */
} // :goto_0
} // .end method
public static Boolean isMobileTcFeatureAllowed ( ) {
/* .locals 1 */
/* .line 1878 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isNetworkPrioritySupport ( ) {
/* .locals 6 */
/* .line 726 */
int v0 = 0; // const/4 v0, 0x0
/* .line 728 */
/* .local v0, "support":Z */
try { // :try_start_0
final String v1 = "mediatek"; // const-string v1, "mediatek"
/* const-string/jumbo v2, "vendor" */
miui.util.FeatureParser .getString ( v2 );
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v2 = "android.miui"; // const-string v2, "android.miui"
final String v3 = "bool"; // const-string v3, "bool"
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 729 */
try { // :try_start_1
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 730 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v4 = this.mContext;
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v5 = "config_network_priority_mtk_global_enabled"; // const-string v5, "config_network_priority_mtk_global_enabled"
v2 = (( android.content.res.Resources ) v4 ).getIdentifier ( v5, v3, v2 ); // invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
v1 = (( android.content.res.Resources ) v1 ).getBoolean ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z
/* move v0, v1 */
/* .line 733 */
} // :cond_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v4 = this.mContext;
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v5 = "config_network_priority_mtk_enabled"; // const-string v5, "config_network_priority_mtk_enabled"
v2 = (( android.content.res.Resources ) v4 ).getIdentifier ( v5, v3, v2 ); // invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
v1 = (( android.content.res.Resources ) v1 ).getBoolean ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z
/* move v0, v1 */
/* .line 737 */
} // :cond_1
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 738 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v4 = this.mContext;
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v5 = "config_network_priority_global_enabled"; // const-string v5, "config_network_priority_global_enabled"
v2 = (( android.content.res.Resources ) v4 ).getIdentifier ( v5, v3, v2 ); // invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
v1 = (( android.content.res.Resources ) v1 ).getBoolean ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z
/* move v0, v1 */
/* .line 741 */
} // :cond_2
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v4 = this.mContext;
(( android.content.Context ) v4 ).getResources ( ); // invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v5 = "config_network_priority_enabled"; // const-string v5, "config_network_priority_enabled"
v2 = (( android.content.res.Resources ) v4 ).getIdentifier ( v5, v3, v2 ); // invoke-virtual {v4, v5, v3, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
v1 = (( android.content.res.Resources ) v1 ).getBoolean ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* move v0, v1 */
/* .line 748 */
} // :goto_0
/* .line 745 */
/* :catch_0 */
/* move-exception v1 */
/* .line 746 */
/* .local v1, "ex":Ljava/lang/Exception; */
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
final String v3 = "get network priority config fail"; // const-string v3, "get network priority config fail"
android.util.Log .e ( v2,v3 );
/* .line 747 */
int v0 = 0; // const/4 v0, 0x0
/* .line 749 */
} // .end local v1 # "ex":Ljava/lang/Exception;
} // :goto_1
} // .end method
private Boolean isPowerSaveRestrictedForUid ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 638 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiConnected:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mNeedRestrictPowerSaveApps;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isQosFeatureAllowed ( ) {
/* .locals 3 */
/* .line 1865 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
final String v0 = "andromeda"; // const-string v0, "andromeda"
v1 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1866 */
/* .local v0, "rst":Z */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isQosFeatureAllowed rst="; // const-string v2, "isQosFeatureAllowed rst="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .d ( v2,v1 );
/* .line 1867 */
} // .end method
private Boolean isStateUnRestrictedForUid ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 583 */
/* if-ltz p2, :cond_1 */
int v0 = 4; // const/4 v0, 0x4
/* if-le p2, v0, :cond_0 */
/* const/16 v0, 0x13 */
/* if-ge p2, v0, :cond_1 */
v0 = this.mUnRestrictApps;
/* .line 584 */
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 583 */
} // :goto_0
} // .end method
private Boolean isStateWmmed ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 579 */
/* if-ltz p1, :cond_0 */
int v0 = 2; // const/4 v0, 0x2
/* if-gt p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isUidValidForRules ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "uid" # I */
/* .line 589 */
/* const/16 v0, 0x3f5 */
/* if-eq p0, v0, :cond_1 */
/* const/16 v0, 0x3fb */
/* if-eq p0, v0, :cond_1 */
/* .line 590 */
v0 = android.os.UserHandle .isApp ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 594 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 591 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean isWmmerEnabled ( ) {
/* .locals 2 */
/* .line 815 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mCloudWmmerEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWifiConnected:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mIsMtkRouter:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mConnectedApModel;
/* .line 816 */
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_1 */
v0 = this.mWmmerRouterWhitelist;
v1 = this.mConnectedApModel;
/* .line 817 */
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 815 */
} // :goto_0
} // .end method
public static com.android.server.net.MiuiNetworkPolicyManagerService make ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 293 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;-><init>(Landroid/content/Context;)V */
/* .line 294 */
} // .end method
private void networkPriorityCloudControl ( ) {
/* .locals 4 */
/* .line 768 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getNetworkPriorityCloudValue()Ljava/lang/String; */
/* .line 769 */
/* .local v0, "cvalue":Ljava/lang/String; */
v1 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isNetworkPrioritySupport()Z */
/* if-nez v1, :cond_0 */
/* .line 770 */
final String v0 = "off"; // const-string v0, "off"
/* .line 773 */
} // :cond_0
try { // :try_start_0
/* const-string/jumbo v1, "sys.net.support.netprio" */
final String v2 = "off"; // const-string v2, "off"
v2 = android.text.TextUtils .equals ( v2,v0 );
if ( v2 != null) { // if-eqz v2, :cond_1
final String v2 = "false"; // const-string v2, "false"
} // :cond_1
/* const-string/jumbo v2, "true" */
} // :goto_0
android.os.SystemProperties .set ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 776 */
/* .line 774 */
/* :catch_0 */
/* move-exception v1 */
/* .line 775 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
final String v3 = "Failed to set network priority support config"; // const-string v3, "Failed to set network priority support config"
android.util.Log .e ( v2,v3,v1 );
/* .line 777 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private Integer networkPriorityMode ( ) {
/* .locals 4 */
/* .line 780 */
v0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isNetworkPrioritySupport()Z */
/* const/16 v1, 0xff */
/* if-nez v0, :cond_0 */
/* .line 781 */
/* .line 783 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getNetworkPriorityCloudValue()Ljava/lang/String; */
/* .line 784 */
/* .local v0, "cvalue":Ljava/lang/String; */
final String v2 = "off"; // const-string v2, "off"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/lit8 v2, v2, 0x1 */
/* .line 785 */
/* .local v2, "isCloudUnForceClosed":Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 786 */
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 788 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "force mThermalForceMode = "; // const-string v3, "force mThermalForceMode = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiNetworkPolicy"; // const-string v3, "MiuiNetworkPolicy"
android.util.Log .i ( v3,v1 );
/* .line 789 */
int v1 = 2; // const/4 v1, 0x2
/* .line 791 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 792 */
/* .line 794 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* .line 795 */
/* .local v1, "def":I */
final String v3 = "on"; // const-string v3, "on"
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 796 */
int v1 = 1; // const/4 v1, 0x1
/* .line 797 */
} // :cond_3
final String v3 = "fast"; // const-string v3, "fast"
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 798 */
int v1 = 2; // const/4 v1, 0x2
/* .line 800 */
} // :cond_4
} // :goto_0
/* .line 802 */
} // .end local v1 # "def":I
} // :cond_5
} // .end method
private void onVpnNetworkChanged ( android.net.Network p0, Boolean p1 ) {
/* .locals 11 */
/* .param p1, "network" # Landroid/net/Network; */
/* .param p2, "add" # Z */
/* .line 437 */
v0 = this.mConnectivityManager;
(( android.net.ConnectivityManager ) v0 ).getNetworkCapabilities ( p1 ); // invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;
/* .line 438 */
/* .local v0, "networkCapabilities":Landroid/net/NetworkCapabilities; */
/* if-nez v0, :cond_0 */
/* .line 439 */
return;
/* .line 441 */
} // :cond_0
(( android.net.NetworkCapabilities ) v0 ).getAdministratorUids ( ); // invoke-virtual {v0}, Landroid/net/NetworkCapabilities;->getAdministratorUids()[I
/* .line 442 */
/* .local v1, "uids":[I */
/* const/16 v2, 0x2710 */
int v3 = -1; // const/4 v3, -0x1
final String v4 = "MiuiNetworkPolicy"; // const-string v4, "MiuiNetworkPolicy"
int v5 = 0; // const/4 v5, 0x0
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 443 */
/* array-length v6, v1 */
} // :goto_0
/* if-ge v5, v6, :cond_4 */
/* aget v7, v1, v5 */
/* .line 444 */
/* .local v7, "uid":I */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "listened vpn network: "; // const-string v9, "listened vpn network: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v8 );
/* .line 445 */
/* if-eq v7, v3, :cond_1 */
/* if-le v7, v2, :cond_1 */
v8 = this.mUnRestrictApps;
v8 = java.lang.Integer .valueOf ( v7 );
/* if-nez v8, :cond_1 */
/* .line 446 */
v8 = this.mUnRestrictApps;
java.lang.Integer .valueOf ( v7 );
/* .line 447 */
v8 = this.mNetworkManager;
int v9 = 1; // const/4 v9, 0x1
(( com.android.server.net.MiuiNetworkManagementService ) v8 ).whiteListUid ( v7, v9 ); // invoke-virtual {v8, v7, v9}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z
/* .line 443 */
} // .end local v7 # "uid":I
} // :cond_1
/* add-int/lit8 v5, v5, 0x1 */
/* .line 451 */
} // :cond_2
/* array-length v6, v1 */
/* move v7, v5 */
} // :goto_1
/* if-ge v7, v6, :cond_4 */
/* aget v8, v1, v7 */
/* .line 452 */
/* .local v8, "uid":I */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "lost vpn network: "; // const-string v10, "lost vpn network: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v9 );
/* .line 453 */
/* if-eq v8, v3, :cond_3 */
/* if-le v8, v2, :cond_3 */
v9 = this.mUnRestrictApps;
v9 = java.lang.Integer .valueOf ( v8 );
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 454 */
v9 = this.mUnRestrictApps;
java.lang.Integer .valueOf ( v8 );
/* .line 455 */
v9 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v9 ).whiteListUid ( v8, v5 ); // invoke-virtual {v9, v8, v5}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z
/* .line 451 */
} // .end local v8 # "uid":I
} // :cond_3
/* add-int/lit8 v7, v7, 0x1 */
/* .line 459 */
} // :cond_4
return;
} // .end method
private java.lang.String parseModelName ( Object[] p0 ) {
/* .locals 8 */
/* .param p1, "bytes" # [B */
/* .line 2006 */
final String v0 = ""; // const-string v0, ""
/* .line 2008 */
/* .local v0, "modelName":Ljava/lang/String; */
try { // :try_start_0
java.nio.ByteBuffer .wrap ( p1 );
v2 = java.nio.ByteOrder.LITTLE_ENDIAN;
(( java.nio.ByteBuffer ) v1 ).order ( v2 ); // invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 2009 */
/* .local v1, "data":Ljava/nio/ByteBuffer; */
int v2 = 4; // const/4 v2, 0x4
/* new-array v3, v2, [B */
/* .line 2010 */
/* .local v3, "flag":[B */
(( java.nio.ByteBuffer ) v1 ).get ( v3 ); // invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 2011 */
int v4 = 2; // const/4 v4, 0x2
/* new-array v5, v4, [B */
/* .line 2012 */
/* .local v5, "id":[B */
/* new-array v4, v4, [B */
/* .line 2013 */
/* .local v4, "len":[B */
} // :goto_0
v6 = (( java.nio.ByteBuffer ) v1 ).remaining ( ); // invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I
/* if-le v6, v2, :cond_1 */
/* .line 2014 */
(( java.nio.ByteBuffer ) v1 ).get ( v5 ); // invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 2015 */
(( java.nio.ByteBuffer ) v1 ).get ( v4 ); // invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 2016 */
int v6 = 0; // const/4 v6, 0x0
/* aget-byte v6, v4, v6 */
/* and-int/lit16 v6, v6, 0xff */
/* shl-int/lit8 v6, v6, 0x8 */
int v7 = 1; // const/4 v7, 0x1
/* aget-byte v7, v4, v7 */
/* and-int/lit16 v7, v7, 0xff */
/* or-int/2addr v6, v7 */
/* new-array v6, v6, [B */
/* .line 2017 */
/* .local v6, "name":[B */
(( java.nio.ByteBuffer ) v1 ).get ( v6 ); // invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 2018 */
v7 = com.android.server.net.MiuiNetworkPolicyManagerService.MODEL_NAME;
v7 = java.util.Arrays .equals ( v5,v7 );
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 2019 */
/* invoke-direct {p0, v6}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->bytesToString([B)Ljava/lang/String; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v2 */
/* .line 2020 */
/* .line 2022 */
} // .end local v6 # "name":[B
} // :cond_0
/* .line 2026 */
} // .end local v1 # "data":Ljava/nio/ByteBuffer;
} // .end local v3 # "flag":[B
} // .end local v4 # "len":[B
} // .end local v5 # "id":[B
} // :cond_1
} // :goto_1
/* nop */
/* .line 2027 */
/* .line 2023 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2024 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
final String v3 = "parseModelName Exception"; // const-string v3, "parseModelName Exception"
android.util.Log .e ( v2,v3 );
/* .line 2025 */
} // .end method
private java.lang.String readLine ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 2318 */
final String v0 = "readLine:"; // const-string v0, "readLine:"
final String v1 = "close failed:"; // const-string v1, "close failed:"
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
int v3 = 0; // const/4 v3, 0x0
/* .line 2319 */
/* .local v3, "reader":Ljava/io/BufferedReader; */
int v4 = 0; // const/4 v4, 0x0
/* .line 2322 */
/* .local v4, "line":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v5, Ljava/io/BufferedReader; */
/* new-instance v6, Ljava/io/FileReader; */
/* invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* const/16 v7, 0x400 */
/* invoke-direct {v5, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V */
/* move-object v3, v5 */
/* .line 2323 */
(( java.io.BufferedReader ) v3 ).readLine ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v4, v0 */
/* .line 2332 */
/* nop */
/* .line 2334 */
try { // :try_start_1
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 2338 */
} // :goto_0
/* .line 2336 */
/* :catch_0 */
/* move-exception v0 */
/* .line 2337 */
/* .local v0, "ioe2":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_1
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
} // .end local v0 # "ioe2":Ljava/io/IOException;
/* .line 2332 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 2328 */
/* :catch_1 */
/* move-exception v5 */
/* .line 2329 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_2
android.util.Log .e ( v2,v0,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 2332 */
/* nop */
} // .end local v5 # "e":Ljava/lang/Exception;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 2334 */
try { // :try_start_3
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 2336 */
/* :catch_2 */
/* move-exception v0 */
/* .line 2337 */
/* .restart local v0 # "ioe2":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 2325 */
} // .end local v0 # "ioe2":Ljava/io/IOException;
/* :catch_3 */
/* move-exception v5 */
/* .line 2326 */
/* .local v5, "ioe":Ljava/io/IOException; */
try { // :try_start_4
android.util.Log .e ( v2,v0,v5 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 2332 */
/* nop */
} // .end local v5 # "ioe":Ljava/io/IOException;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 2334 */
try { // :try_start_5
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 2336 */
/* :catch_4 */
/* move-exception v0 */
/* .line 2337 */
/* .restart local v0 # "ioe2":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 2341 */
} // .end local v0 # "ioe2":Ljava/io/IOException;
} // :cond_0
} // :goto_2
/* .line 2332 */
} // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 2334 */
try { // :try_start_6
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_5 */
/* .line 2338 */
/* .line 2336 */
/* :catch_5 */
/* move-exception v5 */
/* .line 2337 */
/* .local v5, "ioe2":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v2,v1 );
/* .line 2340 */
} // .end local v5 # "ioe2":Ljava/io/IOException;
} // :cond_1
} // :goto_4
/* throw v0 */
} // .end method
private void registerDefaultInputMethodChangedObserver ( ) {
/* .locals 5 */
/* .line 1387 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$19;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1418 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1419 */
final String v2 = "default_input_method"; // const-string v2, "default_input_method"
android.provider.Settings$Secure .getUriFor ( v2 );
/* .line 1418 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1421 */
return;
} // .end method
private void registerHappyEyeballsChangeObserver ( ) {
/* .locals 5 */
/* .line 1004 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
final String v1 = "happy eyeballs observer register"; // const-string v1, "happy eyeballs observer register"
android.util.Log .d ( v0,v1 );
/* .line 1006 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$6; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$6;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1014 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1016 */
final String v2 = "cloud_dns_happy_eyeballs_priority_enabled"; // const-string v2, "cloud_dns_happy_eyeballs_priority_enabled"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1015 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1021 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1023 */
final String v2 = "cloud_dns_happy_eyeballs_priority_time"; // const-string v2, "cloud_dns_happy_eyeballs_priority_time"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1022 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1028 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$7; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$7;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1034 */
return;
} // .end method
private void registerLowLatencyAppsChangedObserver ( ) {
/* .locals 5 */
/* .line 1439 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$20; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$20;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1462 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1463 */
final String v2 = "cloud_lowlatency_whitelist"; // const-string v2, "cloud_lowlatency_whitelist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1462 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1465 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$21; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$21;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1471 */
return;
} // .end method
private void registerMiuiOptimizationChangedObserver ( ) {
/* .locals 5 */
/* .line 1474 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$22;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1495 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = android.provider.MiuiSettings$Secure.MIUI_OPTIMIZATION;
/* .line 1496 */
android.provider.Settings$Secure .getUriFor ( v2 );
/* .line 1495 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1497 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$23; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$23;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1503 */
return;
} // .end method
private void registerMobileLatencyAppsChangedObserver ( ) {
/* .locals 5 */
/* .line 1719 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$27; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$27;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1741 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1742 */
final String v2 = "cloud_block_scan_applist_for_mobile"; // const-string v2, "cloud_block_scan_applist_for_mobile"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1741 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1744 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$28; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$28;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1750 */
return;
} // .end method
private void registerNetworkProrityModeChangedObserver ( ) {
/* .locals 5 */
/* .line 1111 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$12; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$12;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1127 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1128 */
final String v2 = "cloud_network_priority_enabled"; // const-string v2, "cloud_network_priority_enabled"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1127 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1129 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1130 */
final String v2 = "cloud_mtk_wifi_traffic_priority_mode"; // const-string v2, "cloud_mtk_wifi_traffic_priority_mode"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1129 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1132 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1133 */
final String v2 = "cloud_auto_forward_enabled"; // const-string v2, "cloud_auto_forward_enabled"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1132 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1134 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$13; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$13;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1140 */
v1 = this.mConnectivityManager;
v2 = this.mVPNNetworkRequest;
v3 = this.mVPNNetworkCallback;
(( android.net.ConnectivityManager ) v1 ).registerNetworkCallback ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 1141 */
return;
} // .end method
private void registerP2PHCChangeObserver ( ) {
/* .locals 5 */
/* .line 1068 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
final String v1 = "p2phc observer register"; // const-string v1, "p2phc observer register"
android.util.Log .d ( v0,v1 );
/* .line 1070 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$10; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$10;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1095 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1097 */
final String v2 = "cloud_p2phc_enabled"; // const-string v2, "cloud_p2phc_enabled"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1096 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1102 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$11; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$11;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1108 */
return;
} // .end method
private void registerPowerKeeperSleep ( ) {
/* .locals 2 */
/* .line 2072 */
v0 = this.mContext;
/* const-string/jumbo v1, "user" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/UserManager; */
this.mUserManager = v0;
/* .line 2073 */
v0 = this.mContext;
/* const-class v1, Landroid/net/ConnectivityManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.cm = v0;
/* .line 2074 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mSleepModeWhitelistUids = v0;
/* .line 2075 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSleepModeEnableControlObserver()V */
/* .line 2076 */
return;
} // .end method
private void registerRestrictPowerSaveAppsChangedObserver ( ) {
/* .locals 5 */
/* .line 1289 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$15; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$15;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1311 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1312 */
final String v2 = "cloud_block_scan_applist"; // const-string v2, "cloud_block_scan_applist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1311 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1314 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$16; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$16;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1320 */
return;
} // .end method
private void registerSleepModeEnableControlObserver ( ) {
/* .locals 5 */
/* .line 2143 */
final String v0 = "MiuiNetworkPolicySleepMode"; // const-string v0, "MiuiNetworkPolicySleepMode"
final String v1 = "registerSleepModeEnableControlObserver"; // const-string v1, "registerSleepModeEnableControlObserver"
android.util.Log .d ( v0,v1 );
/* .line 2144 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 2164 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2166 */
final String v2 = "key_open_earthquake_warning"; // const-string v2, "key_open_earthquake_warning"
android.provider.Settings$Secure .getUriFor ( v2 );
/* .line 2165 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2170 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2172 */
final String v2 = "cloud_sleepmode_networkpolicy_enabled"; // const-string v2, "cloud_sleepmode_networkpolicy_enabled"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 2171 */
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2177 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$34; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$34;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2183 */
return;
} // .end method
private void registerSleepModeReceiver ( ) {
/* .locals 7 */
/* .line 2081 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 2082 */
/* .local v0, "sleepModeFilter":Landroid/content/IntentFilter; */
final String v1 = "com.miui.powerkeeper_sleep_changed"; // const-string v1, "com.miui.powerkeeper_sleep_changed"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2083 */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2084 */
v1 = this.mContext;
v2 = this.mSleepModeReceiver;
int v4 = 0; // const/4 v4, 0x0
v5 = this.mHandler;
int v6 = 2; // const/4 v6, 0x2
/* move-object v3, v0 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent; */
/* .line 2087 */
final String v1 = "MiuiNetworkPolicySleepMode"; // const-string v1, "MiuiNetworkPolicySleepMode"
final String v2 = "registerSleepModeReceiver"; // const-string v2, "registerSleepModeReceiver"
android.util.Log .d ( v1,v2 );
/* .line 2088 */
return;
} // .end method
private void registerSmartDnsChangeObserver ( ) {
/* .locals 5 */
/* .line 1037 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
final String v1 = "SmartDns observer register"; // const-string v1, "SmartDns observer register"
android.util.Log .d ( v0,v1 );
/* .line 1039 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$8; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$8;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1052 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1054 */
final String v2 = "cloud_smartdns_enabled"; // const-string v2, "cloud_smartdns_enabled"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1053 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1059 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$9; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$9;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1065 */
return;
} // .end method
private void registerTrustMiuiAddDnsObserver ( ) {
/* .locals 5 */
/* .line 945 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$5; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$5;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 965 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 966 */
final String v2 = "cloud_trust_miui_add_dns"; // const-string v2, "cloud_trust_miui_add_dns"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 965 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 970 */
return;
} // .end method
private void registerUnRestirctAppsChangedObserver ( ) {
/* .locals 5 */
/* .line 1338 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$17; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$17;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1362 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1363 */
final String v2 = "cloud_network_priority_whitelist"; // const-string v2, "cloud_network_priority_whitelist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1362 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1365 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$18; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$18;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1371 */
return;
} // .end method
private void registerWifiNetworkListener ( ) {
/* .locals 3 */
/* .line 931 */
v0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isAutoForwardEnabled()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z */
/* if-nez v0, :cond_0 */
/* .line 932 */
v0 = this.mConnectivityManager;
v1 = this.mWifiNetworkRequest;
v2 = this.mWifiNetworkCallback;
(( android.net.ConnectivityManager ) v0 ).registerNetworkCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 933 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z */
/* .line 934 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isAutoForwardEnabled()Z */
/* if-nez v0, :cond_2 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 935 */
v0 = this.mConnectivityManager;
v1 = this.mWifiNetworkCallback;
(( android.net.ConnectivityManager ) v0 ).unregisterNetworkCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V
/* .line 936 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasAutoForward:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 937 */
final String v0 = "MiuiNetworkPolicy"; // const-string v0, "MiuiNetworkPolicy"
final String v2 = "cloud control is disabled, remove autoforward rule"; // const-string v2, "cloud control is disabled, remove autoforward rule"
android.util.Log .d ( v0,v2 );
/* .line 938 */
/* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableAutoForward(Z)V */
/* .line 940 */
} // :cond_1
/* iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mHasListenWifiNetwork:Z */
/* .line 942 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void registerWmmerEnableChangedObserver ( ) {
/* .locals 5 */
/* .line 901 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$3; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$3;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 917 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 918 */
final String v2 = "cloud_wmmer_enabled"; // const-string v2, "cloud_wmmer_enabled"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 917 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 919 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 920 */
final String v2 = "cloud_mtk_wmmer_enabled"; // const-string v2, "cloud_mtk_wmmer_enabled"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 919 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 922 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyManagerService$4; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$4;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 928 */
return;
} // .end method
private void registerWmmerRouterWhitelistChangedObserver ( ) {
/* .locals 5 */
/* .line 1938 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$30; */
/* new-instance v1, Landroid/os/Handler; */
/* invoke-direct {v1}, Landroid/os/Handler;-><init>()V */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$30;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V */
/* .line 1952 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1953 */
final String v2 = "cloud_wmmer_router_whitelist"; // const-string v2, "cloud_wmmer_router_whitelist"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 1952 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1955 */
return;
} // .end method
private void removeUidState ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 1247 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "removeUidState uid = "; // const-string v1, "removeUidState uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .i ( v1,v0 );
/* .line 1248 */
v0 = this.mUidState;
v0 = (( android.util.SparseIntArray ) v0 ).indexOfKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I
/* .line 1249 */
/* .local v0, "index":I */
/* if-ltz v0, :cond_3 */
/* .line 1250 */
v1 = this.mUidState;
v1 = (( android.util.SparseIntArray ) v1 ).valueAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I
/* .line 1251 */
/* .local v1, "oldUidState":I */
v2 = this.mUidState;
(( android.util.SparseIntArray ) v2 ).removeAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->removeAt(I)V
/* .line 1252 */
/* const/16 v2, 0x13 */
/* if-eq v1, v2, :cond_3 */
/* .line 1253 */
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRulesForUidStateChange(III)V */
/* .line 1255 */
v3 = com.android.server.net.MiuiNetworkPolicyManagerService .isMobileLatencyAllowed ( );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1256 */
/* invoke-direct {p0, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatencyForUidStateChange(III)V */
/* .line 1259 */
} // :cond_0
v3 = this.mQosUtils;
if ( v3 != null) { // if-eqz v3, :cond_1
(( com.android.server.net.MiuiNetworkPolicyQosUtils ) v3 ).updateQosForUidStateChange ( p1, v1, v2 ); // invoke-virtual {v3, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosForUidStateChange(III)V
/* .line 1261 */
} // :cond_1
v3 = this.mAppBuckets;
if ( v3 != null) { // if-eqz v3, :cond_2
(( com.android.server.net.MiuiNetworkPolicyAppBuckets ) v3 ).updateAppBucketsForUidStateChange ( p1, v1, v2 ); // invoke-virtual {v3, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppBucketsForUidStateChange(III)V
/* .line 1263 */
} // :cond_2
v3 = this.mMobileTcUtils;
if ( v3 != null) { // if-eqz v3, :cond_3
(( com.android.server.net.MiuiNetworkPolicyTrafficLimit ) v3 ).updateRulesForUidStateChange ( p1, v1, v2 ); // invoke-virtual {v3, p1, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateRulesForUidStateChange(III)V
/* .line 1267 */
} // .end local v1 # "oldUidState":I
} // :cond_3
return;
} // .end method
private void setSleepModeWhitelistUidRules ( ) {
/* .locals 3 */
/* .line 2186 */
v0 = v0 = this.mSleepModeWhitelistUids;
/* if-nez v0, :cond_0 */
/* .line 2187 */
v0 = this.mSleepModeWhitelistUids;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 2188 */
/* .local v1, "uid":I */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateSleepModeWhitelistUidRules(IZ)V */
/* .line 2189 */
} // .end local v1 # "uid":I
/* .line 2191 */
} // :cond_0
return;
} // .end method
private Boolean setThermalPolicy ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "mode" # I */
/* .line 2350 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->validateThermalMode(I)Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2351 */
v0 = this.mHandler;
/* const/16 v2, 0xe */
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 2352 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v2, p1, v1 ); // invoke-virtual {v0, v2, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 2353 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2355 */
} // :cond_0
} // .end method
private Boolean sleepModeEnableControl ( ) {
/* .locals 5 */
/* .line 2127 */
v0 = this.mContext;
/* .line 2129 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2128 */
final String v1 = "key_open_earthquake_warning"; // const-string v1, "key_open_earthquake_warning"
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v1,v2,v3 );
/* .line 2133 */
/* .local v0, "earthQuack":I */
v1 = this.mContext;
/* .line 2135 */
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2134 */
final String v3 = "cloud_sleepmode_networkpolicy_enabled"; // const-string v3, "cloud_sleepmode_networkpolicy_enabled"
android.provider.Settings$System .getStringForUser ( v1,v3,v2 );
/* .line 2138 */
/* .local v1, "cloudEnable":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "sleepModeEnableControl earthQuack:" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " cloudEnable:"; // const-string v4, " cloudEnable:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiNetworkPolicySleepMode"; // const-string v4, "MiuiNetworkPolicySleepMode"
android.util.Log .d ( v4,v3 );
/* .line 2139 */
final String v3 = "on"; // const-string v3, "on"
v3 = android.text.TextUtils .equals ( v3,v1 );
if ( v3 != null) { // if-eqz v3, :cond_0
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v3, :cond_0 */
/* move v2, v3 */
} // :cond_0
} // .end method
private void unregisterSleepModeReceiver ( ) {
/* .locals 2 */
/* .line 2091 */
v0 = this.mSleepModeReceiver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2092 */
v1 = this.mContext;
(( android.content.Context ) v1 ).unregisterReceiver ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 2093 */
final String v0 = "MiuiNetworkPolicySleepMode"; // const-string v0, "MiuiNetworkPolicySleepMode"
/* const-string/jumbo v1, "unregisterSleepModeReceiver" */
android.util.Log .d ( v0,v1 );
/* .line 2095 */
} // :cond_0
return;
} // .end method
private void updateLimit ( Boolean p0 ) {
/* .locals 8 */
/* .param p1, "enabled" # Z */
/* .line 1506 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateLimit mLimitEnabled=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",enabled="; // const-string v1, ",enabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",mNetworkPriorityMode="; // const-string v1, ",mNetworkPriorityMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",mThermalForceMode="; // const-string v1, ",mThermalForceMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .d ( v1,v0 );
/* .line 1507 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z */
/* if-eq v0, p1, :cond_2 */
/* .line 1508 */
/* const-wide/16 v0, 0x0 */
/* .line 1509 */
/* .local v0, "bwBps":J */
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_1 */
/* .line 1510 */
/* const-wide/32 v0, 0x30d40 */
/* .line 1511 */
v2 = v2 = this.mHistoryBandWidth;
/* if-lez v2, :cond_0 */
/* .line 1512 */
v2 = this.mHistoryBandWidth;
java.util.Collections .max ( v2 );
/* check-cast v2, Ljava/lang/Long; */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* .line 1514 */
} // :cond_0
/* const-wide/16 v2, 0x50 */
/* mul-long/2addr v2, v0 */
/* const-wide/16 v4, 0x64 */
/* div-long/2addr v2, v4 */
/* const-wide/32 v4, 0xf4240 */
java.lang.Math .min ( v2,v3,v4,v5 );
/* move-result-wide v2 */
/* .line 1515 */
/* .local v2, "fgBw":J */
/* sub-long v4, v0, v2 */
/* const-wide/32 v6, 0x186a0 */
java.lang.Math .max ( v4,v5,v6,v7 );
/* move-result-wide v0 */
/* .line 1517 */
} // .end local v2 # "fgBw":J
} // :cond_1
v2 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v2 ).setLimit ( p1, v0, v1 ); // invoke-virtual {v2, p1, v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->setLimit(ZJ)Z
/* .line 1518 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mLimitEnabled:Z */
/* .line 1520 */
} // .end local v0 # "bwBps":J
} // :cond_2
return;
} // .end method
private void updateLimitterForUidState ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 618 */
v0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isLimitterEnabled()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 619 */
v0 = this.mNetworkManager;
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isStateUnRestrictedForUid(II)Z */
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).whiteListUid ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->whiteListUid(IZ)Z
/* .line 621 */
} // :cond_0
return;
} // .end method
private void updateMobileLatency ( ) {
/* .locals 6 */
/* .line 1820 */
v0 = this.mUidState;
v0 = (( android.util.SparseIntArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I
/* .line 1821 */
/* .local v0, "size":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 1822 */
v2 = this.mUidState;
v2 = (( android.util.SparseIntArray ) v2 ).keyAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->keyAt(I)I
/* .line 1823 */
/* .local v2, "uid":I */
v3 = this.mUidState;
/* const/16 v4, 0x13 */
v3 = (( android.util.SparseIntArray ) v3 ).get ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I
/* .line 1824 */
/* .local v3, "state":I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updateMobileLatency uid = " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", state = "; // const-string v5, ", state = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiNetworkPolicy"; // const-string v5, "MiuiNetworkPolicy"
android.util.Log .i ( v5,v4 );
/* .line 1825 */
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .isUidValidForRules ( v2 );
if ( v4 != null) { // if-eqz v4, :cond_0
int v4 = 2; // const/4 v4, 0x2
/* if-ne v3, v4, :cond_0 */
v4 = this.mMobileLowLatencyApps;
/* .line 1826 */
v4 = java.lang.Integer .valueOf ( v2 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1827 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatencyStateForUidState(II)V */
/* .line 1821 */
} // .end local v2 # "uid":I
} // .end local v3 # "state":I
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1830 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
private void updateMobileLatencyForUidStateChange ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "oldUidState" # I */
/* .param p3, "newUidState" # I */
/* .line 1833 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateMLUid uid:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",old:"; // const-string v1, ",old:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",new:"; // const-string v1, ",new:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .i ( v1,v0 );
/* .line 1834 */
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .isUidValidForRules ( p1 );
/* if-nez v0, :cond_0 */
return;
/* .line 1835 */
} // :cond_0
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyEnabledForUid(II)Z */
/* .line 1836 */
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyEnabledForUid(II)Z */
/* if-eq v0, v1, :cond_1 */
/* .line 1837 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatencyStateForUidState(II)V */
/* .line 1839 */
} // :cond_1
return;
} // .end method
private void updateMobileLatencyStateForUidState ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 1798 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isMobileLatencyEnabledForUid(II)Z */
/* .line 1799 */
/* .local v0, "enabled":Z */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateMobileLowLatencyStateForUidState enabled = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .i ( v2,v1 );
/* .line 1800 */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enableMobileLowLatency(Z)V */
/* .line 1801 */
return;
} // .end method
private void updatePowerSaveForUidDataActivityChanged ( Integer p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "active" # Z */
/* .line 648 */
v0 = this.mUidState;
/* const/16 v1, 0x13 */
v0 = (( android.util.SparseIntArray ) v0 ).get ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I
/* .line 649 */
/* .local v0, "state":I */
v1 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isPowerSaveRestrictedForUid(II)Z */
/* .line 650 */
/* .local v1, "restrict":Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "update ps for data activity, uid = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", state= "; // const-string v3, ", state= "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", restrict = "; // const-string v3, ", restrict = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", active = "; // const-string v3, ", active = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = ", mPS = "; // const-string v3, ", mPS = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiNetworkPolicy"; // const-string v3, "MiuiNetworkPolicy"
android.util.Log .i ( v3,v2 );
/* .line 652 */
if ( p2 != null) { // if-eqz p2, :cond_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 653 */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enablePowerSave(Z)V */
/* .line 654 */
} // :cond_0
/* if-nez p2, :cond_1 */
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mPowerSaveEnabled:Z */
/* if-nez v2, :cond_1 */
/* .line 655 */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enablePowerSave(Z)V */
/* .line 657 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void updatePowerSaveStateForUidState ( Integer p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 660 */
v6 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isPowerSaveRestrictedForUid(II)Z */
/* .line 664 */
/* .local v6, "restrict":Z */
v0 = this.mNetworkManager;
/* const/16 v3, 0x76 */
int v4 = 2; // const/4 v4, 0x2
/* move v2, p1 */
/* move v5, v6 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/net/MiuiNetworkManagementService;->listenUidDataActivity(IIIIZ)Z */
/* .line 666 */
/* xor-int/lit8 v0, v6, 0x1 */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->enablePowerSave(Z)V */
/* .line 667 */
return;
} // .end method
private void updateRuleForMobileTc ( ) {
/* .locals 7 */
/* .line 1884 */
v0 = this.mUidState;
v0 = (( android.util.SparseIntArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I
/* .line 1885 */
/* .local v0, "size":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 1886 */
v2 = this.mUidState;
v2 = (( android.util.SparseIntArray ) v2 ).keyAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->keyAt(I)I
/* .line 1887 */
/* .local v2, "uid":I */
v3 = this.mUidState;
/* const/16 v4, 0x13 */
v3 = (( android.util.SparseIntArray ) v3 ).get ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I
/* .line 1888 */
/* .local v3, "state":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "updateRuleForMobileTc uid = " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", state = "; // const-string v6, ", state = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "MiuiNetworkPolicy"; // const-string v6, "MiuiNetworkPolicy"
android.util.Log .i ( v6,v5 );
/* .line 1889 */
v5 = this.mMobileTcUtils;
if ( v5 != null) { // if-eqz v5, :cond_0
(( com.android.server.net.MiuiNetworkPolicyTrafficLimit ) v5 ).updateRulesForUidStateChange ( v2, v4, v3 ); // invoke-virtual {v5, v2, v4, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateRulesForUidStateChange(III)V
/* .line 1885 */
} // .end local v2 # "uid":I
} // .end local v3 # "state":I
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1892 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
private void updateRuleGlobal ( ) {
/* .locals 7 */
/* .line 679 */
v0 = this.mUidState;
v0 = (( android.util.SparseIntArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I
/* .line 680 */
/* .local v0, "size":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* .line 681 */
v2 = this.mUidState;
v2 = (( android.util.SparseIntArray ) v2 ).keyAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->keyAt(I)I
/* .line 682 */
/* .local v2, "uid":I */
v3 = this.mUidState;
/* const/16 v4, 0x13 */
v3 = (( android.util.SparseIntArray ) v3 ).get ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I
/* .line 683 */
/* .local v3, "state":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "updateRuleGlobal uid = " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", state = "; // const-string v6, ", state = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "MiuiNetworkPolicy"; // const-string v6, "MiuiNetworkPolicy"
android.util.Log .i ( v6,v5 );
/* .line 684 */
/* invoke-direct {p0, v2, v4, v3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRulesForUidStateChange(III)V */
/* .line 680 */
} // .end local v2 # "uid":I
} // .end local v3 # "state":I
/* add-int/lit8 v1, v1, 0x1 */
/* .line 686 */
} // .end local v1 # "i":I
} // :cond_0
return;
} // .end method
private void updateRulesForUidStateChange ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "oldUidState" # I */
/* .param p3, "newUidState" # I */
/* .line 624 */
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .isUidValidForRules ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 625 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I */
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I */
/* if-eq v0, v1, :cond_0 */
/* .line 626 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateWmmForUidState(II)V */
/* .line 628 */
} // :cond_0
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isStateUnRestrictedForUid(II)Z */
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isStateUnRestrictedForUid(II)Z */
/* if-eq v0, v1, :cond_1 */
/* .line 629 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateLimitterForUidState(II)V */
/* .line 631 */
} // :cond_1
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isPowerSaveRestrictedForUid(II)Z */
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->isPowerSaveRestrictedForUid(II)Z */
/* if-eq v0, v1, :cond_2 */
/* .line 632 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updatePowerSaveStateForUidState(II)V */
/* .line 635 */
} // :cond_2
return;
} // .end method
private void updateSleepModeWhitelistUidRules ( Integer p0, Boolean p1 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "added" # Z */
/* .line 2204 */
final String v0 = "MiuiNetworkPolicySleepMode"; // const-string v0, "MiuiNetworkPolicySleepMode"
try { // :try_start_0
v1 = this.cm;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2205 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateSleepModeWhitelistUidRules appId:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " added:"; // const-string v2, " added:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v1 );
/* .line 2206 */
final String v1 = "android.net.ConnectivityManager"; // const-string v1, "android.net.ConnectivityManager"
java.lang.Class .forName ( v1 );
/* .line 2207 */
/* .local v1, "clazz":Ljava/lang/Class; */
/* const-string/jumbo v2, "updateSleepModeUidRule" */
int v3 = 2; // const/4 v3, 0x2
/* new-array v4, v3, [Ljava/lang/Class; */
v5 = java.lang.Integer.TYPE;
int v6 = 0; // const/4 v6, 0x0
/* aput-object v5, v4, v6 */
v5 = java.lang.Boolean.TYPE;
int v7 = 1; // const/4 v7, 0x1
/* aput-object v5, v4, v7 */
(( java.lang.Class ) v1 ).getDeclaredMethod ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 2208 */
/* .local v2, "method":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v2 ).setAccessible ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 2209 */
v4 = this.cm;
/* new-array v3, v3, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
/* aput-object v5, v3, v6 */
java.lang.Boolean .valueOf ( p2 );
/* aput-object v5, v3, v7 */
(( java.lang.reflect.Method ) v2 ).invoke ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 2213 */
} // .end local v1 # "clazz":Ljava/lang/Class;
} // .end local v2 # "method":Ljava/lang/reflect/Method;
} // :cond_0
/* .line 2211 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2212 */
/* .local v1, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "updateSleepModeWhitelistUidRules error " */
android.util.Log .e ( v0,v2,v1 );
/* .line 2214 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void updateUidState ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "uidState" # I */
/* .line 1231 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateUidState uid = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", uidState = "; // const-string v1, ", uidState = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .i ( v1,v0 );
/* .line 1232 */
v0 = this.mUidState;
/* const/16 v1, 0x13 */
v0 = (( android.util.SparseIntArray ) v0 ).get ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I
/* .line 1233 */
/* .local v0, "oldUidState":I */
/* if-eq v0, p2, :cond_3 */
/* .line 1235 */
v1 = this.mUidState;
(( android.util.SparseIntArray ) v1 ).put ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Landroid/util/SparseIntArray;->put(II)V
/* .line 1236 */
/* invoke-direct {p0, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateRulesForUidStateChange(III)V */
/* .line 1237 */
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .isMobileLatencyAllowed ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1238 */
/* invoke-direct {p0, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->updateMobileLatencyForUidStateChange(III)V */
/* .line 1240 */
} // :cond_0
v1 = this.mQosUtils;
if ( v1 != null) { // if-eqz v1, :cond_1
(( com.android.server.net.MiuiNetworkPolicyQosUtils ) v1 ).updateQosForUidStateChange ( p1, v0, p2 ); // invoke-virtual {v1, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosForUidStateChange(III)V
/* .line 1241 */
} // :cond_1
v1 = this.mAppBuckets;
if ( v1 != null) { // if-eqz v1, :cond_2
(( com.android.server.net.MiuiNetworkPolicyAppBuckets ) v1 ).updateAppBucketsForUidStateChange ( p1, v0, p2 ); // invoke-virtual {v1, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->updateAppBucketsForUidStateChange(III)V
/* .line 1242 */
} // :cond_2
v1 = this.mMobileTcUtils;
if ( v1 != null) { // if-eqz v1, :cond_3
(( com.android.server.net.MiuiNetworkPolicyTrafficLimit ) v1 ).updateRulesForUidStateChange ( p1, v0, p2 ); // invoke-virtual {v1, p1, v0, p2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateRulesForUidStateChange(III)V
/* .line 1244 */
} // :cond_3
return;
} // .end method
private void updateWmmForUidState ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 598 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mWmmerEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 599 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateWmmForUidState uid: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " state: "; // const-string v1, " state: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " wmm: "; // const-string v1, " wmm: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .i ( v1,v0 );
/* .line 600 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "market_uid":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "download_uid":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 602 */
/* .local v2, "downloadui_uid":I */
try { // :try_start_0
v3 = this.mContext;
(( android.content.Context ) v3 ).getPackageManager ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 603 */
/* .local v3, "pm":Landroid/content/pm/PackageManager; */
final String v4 = "com.xiaomi.market"; // const-string v4, "com.xiaomi.market"
int v5 = 0; // const/4 v5, 0x0
(( android.content.pm.PackageManager ) v3 ).getApplicationInfo ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move v0, v4 */
/* .line 604 */
final String v4 = "com.android.providers.downloads"; // const-string v4, "com.android.providers.downloads"
(( android.content.pm.PackageManager ) v3 ).getApplicationInfo ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move v1, v4 */
/* .line 605 */
final String v4 = "com.android.providers.downloads.ui"; // const-string v4, "com.android.providers.downloads.ui"
(( android.content.pm.PackageManager ) v3 ).getApplicationInfo ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v2, v4 */
/* .line 608 */
} // .end local v3 # "pm":Landroid/content/pm/PackageManager;
/* .line 606 */
/* :catch_0 */
/* move-exception v3 */
/* .line 607 */
/* .local v3, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 609 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
v3 = this.mNetworkManager;
v4 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I */
(( com.android.server.net.MiuiNetworkManagementService ) v3 ).updateWmm ( p1, v4 ); // invoke-virtual {v3, p1, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->updateWmm(II)Z
/* .line 610 */
/* if-ne p1, v0, :cond_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 611 */
v3 = this.mNetworkManager;
v4 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I */
(( com.android.server.net.MiuiNetworkManagementService ) v3 ).updateWmm ( v1, v4 ); // invoke-virtual {v3, v1, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->updateWmm(II)Z
/* .line 612 */
v3 = this.mNetworkManager;
v4 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getWmmForUidState(II)I */
(( com.android.server.net.MiuiNetworkManagementService ) v3 ).updateWmm ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Lcom/android/server/net/MiuiNetworkManagementService;->updateWmm(II)Z
/* .line 615 */
} // .end local v0 # "market_uid":I
} // .end local v1 # "download_uid":I
} // .end local v2 # "downloadui_uid":I
} // :cond_0
return;
} // .end method
private Boolean validatePriorityMode ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 821 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
/* if-eq p1, v0, :cond_1 */
int v1 = 2; // const/4 v1, 0x2
/* if-eq p1, v1, :cond_1 */
/* const/16 v1, 0xff */
/* if-ne p1, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
} // .end method
private Boolean validateThermalMode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 2345 */
/* if-ltz p1, :cond_0 */
int v0 = 3; // const/4 v0, 0x3
/* if-gt p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
public Long getMiuiSlmVoipUdpAddress ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 1916 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1917 */
v0 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).getMiuiSlmVoipUdpAddress ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->getMiuiSlmVoipUdpAddress(I)J
/* move-result-wide v0 */
/* .line 1918 */
/* .local v0, "address":J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getMiuiSlmVoipUdpAddress uid = "; // const-string v3, "getMiuiSlmVoipUdpAddress uid = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " address = "; // const-string v3, " address = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiNetworkPolicy"; // const-string v3, "MiuiNetworkPolicy"
android.util.Log .d ( v3,v2 );
/* .line 1919 */
/* return-wide v0 */
/* .line 1921 */
} // .end local v0 # "address":J
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
public Integer getMiuiSlmVoipUdpPort ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1925 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1926 */
v0 = this.mNetworkManager;
v0 = (( com.android.server.net.MiuiNetworkManagementService ) v0 ).getMiuiSlmVoipUdpPort ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->getMiuiSlmVoipUdpPort(I)I
/* .line 1927 */
/* .local v0, "ports":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getMiuiSlmVoipUdpPort uid = "; // const-string v2, "getMiuiSlmVoipUdpPort uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " ports = "; // const-string v2, " ports = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .d ( v2,v1 );
/* .line 1928 */
/* .line 1930 */
} // .end local v0 # "ports":I
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long getShareStats ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .line 1934 */
v0 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).getShareStats ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->getShareStats(I)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Boolean onSleepModeWhitelistChange ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "appId" # I */
/* .param p2, "added" # Z */
/* .line 2056 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onSleepModeWhitelistChange appId:"; // const-string v1, "onSleepModeWhitelistChange appId:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicySleepMode"; // const-string v1, "MiuiNetworkPolicySleepMode"
android.util.Log .i ( v1,v0 );
/* .line 2057 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService$31;-><init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2068 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean setMiuiSlmBpfUid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1907 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1908 */
v0 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).setMiuiSlmBpfUid ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->setMiuiSlmBpfUid(I)V
/* .line 1909 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setMiuiSlmBpfUid uid = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .d ( v1,v0 );
/* .line 1910 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1912 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean setNetworkTrafficPolicy ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "mode" # I */
/* .line 828 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->validatePriorityMode(I)Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 829 */
v0 = this.mHandler;
int v2 = 7; // const/4 v2, 0x7
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 830 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( v2, p1, v1 ); // invoke-virtual {v0, v2, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 831 */
int v0 = 1; // const/4 v0, 0x1
/* .line 833 */
} // :cond_0
} // .end method
public Boolean setRpsStatus ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 837 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setRpsStatus/in [" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .d ( v1,v0 );
/* .line 838 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mRpsEnabled:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 839 */
v0 = this.mHandler;
/* const/16 v2, 0xa */
(( android.os.Handler ) v0 ).removeMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 840 */
v0 = this.mHandler;
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 841 */
/* const-string/jumbo v0, "setRpsStatus/out [ true ]" */
android.util.Log .d ( v1,v0 );
/* .line 842 */
int v0 = 1; // const/4 v0, 0x1
/* .line 844 */
} // :cond_0
/* const-string/jumbo v0, "setRpsStatus/out [ false]" */
android.util.Log .d ( v1,v0 );
/* .line 845 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer setTrafficControllerForMobile ( Boolean p0, Long p1 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .param p2, "rate" # J */
/* .line 1895 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setTrafficControllerForMobile enable=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = "rate = "; // const-string v1, "rate = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .d ( v1,v0 );
/* .line 1897 */
v0 = this.mMobileTcUtils;
/* if-nez v0, :cond_0 */
/* .line 1898 */
/* const-string/jumbo v0, "setTrafficControllerForMobile unsupport device!!!" */
android.util.Log .d ( v1,v0 );
/* .line 1899 */
int v0 = -1; // const/4 v0, -0x1
/* .line 1901 */
} // :cond_0
v0 = (( com.android.server.net.MiuiNetworkPolicyTrafficLimit ) v0 ).setMobileTrafficLimitForGame ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->setMobileTrafficLimitForGame(ZJ)I
/* .line 1902 */
/* .local v0, "rst":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setTrafficControllerForMobile rst = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v2 );
/* .line 1903 */
} // .end method
public void systemReady ( ) {
/* .locals 9 */
/* .line 480 */
/* const-string/jumbo v0, "wifi.interface" */
/* const-string/jumbo v1, "wlan0" */
android.os.SystemProperties .get ( v0,v1 );
this.mInterfaceName = v0;
/* .line 482 */
com.android.server.net.MiuiNetworkManagementService .getInstance ( );
this.mNetworkManager = v0;
/* .line 483 */
v1 = this.mUidDataActivityObserver;
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).setNetworkEventObserver ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->setNetworkEventObserver(Lcom/android/server/net/MiuiNetworkManagementService$NetworkEventObserver;)V
/* .line 484 */
v0 = this.mContext;
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.mConnectivityManager = v0;
/* .line 488 */
v0 = this.mNetworkManager;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).enableWmmer ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableWmmer(Z)Z
/* .line 489 */
v0 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).enableLimitter ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableLimitter(Z)Z
/* .line 491 */
v0 = this.mContext;
com.android.server.net.NetworkStatsReporter .make ( v0 );
/* .line 493 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v1 = "android.net.wifi.STATE_CHANGE"; // const-string v1, "android.net.wifi.STATE_CHANGE"
/* invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 495 */
/* .local v0, "wifiStateFilter":Landroid/content/IntentFilter; */
final String v1 = "android.net.conn.NETWORK_CONDITIONS_MEASURED"; // const-string v1, "android.net.conn.NETWORK_CONDITIONS_MEASURED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 496 */
v2 = this.mContext;
v3 = this.mWifiStateReceiver;
int v5 = 0; // const/4 v5, 0x0
v6 = this.mHandler;
int v7 = 2; // const/4 v7, 0x2
/* move-object v4, v0 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent; */
/* .line 498 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWmmerEnableChangedObserver()V */
/* .line 499 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerNetworkProrityModeChangedObserver()V */
/* .line 500 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->networkPriorityCloudControl()V */
/* .line 502 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->getDefaultInputMethod()Ljava/lang/String; */
this.mDefaultInputMethod = v1;
/* .line 504 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerUnRestirctAppsChangedObserver()V */
/* .line 506 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerLowLatencyAppsChangedObserver()V */
/* .line 508 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWmmerRouterWhitelistChangedObserver()V */
/* .line 510 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerDefaultInputMethodChangedObserver()V */
/* .line 513 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerRestrictPowerSaveAppsChangedObserver()V */
/* .line 516 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerTrustMiuiAddDnsObserver()V */
/* .line 519 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerHappyEyeballsChangeObserver()V */
/* .line 522 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerP2PHCChangeObserver()V */
/* .line 524 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSmartDnsChangeObserver()V */
/* .line 527 */
/* nop */
/* .line 532 */
/* new-instance v1, Landroid/content/IntentFilter; */
/* invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V */
/* .line 533 */
/* .local v1, "packageFilter":Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.PACKAGE_ADDED"; // const-string v2, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 534 */
final String v2 = "android.intent.action.PACKAGE_REMOVED"; // const-string v2, "android.intent.action.PACKAGE_REMOVED"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 535 */
final String v2 = "package"; // const-string v2, "package"
(( android.content.IntentFilter ) v1 ).addDataScheme ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 536 */
v2 = this.mContext;
v3 = this.mPackageReceiver;
int v5 = 0; // const/4 v5, 0x0
v6 = this.mHandler;
int v7 = 4; // const/4 v7, 0x4
/* move-object v4, v1 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent; */
/* .line 538 */
v2 = this.mSupport;
(( com.android.server.net.MiuiNetworkPolicyServiceSupport ) v2 ).registerUidObserver ( ); // invoke-virtual {v2}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->registerUidObserver()V
/* .line 540 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerWifiNetworkListener()V */
/* .line 542 */
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .isMobileLatencyAllowed ( );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 544 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerMobileLatencyAppsChangedObserver()V */
/* .line 545 */
/* new-instance v5, Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.ANY_DATA_STATE"; // const-string v2, "android.intent.action.ANY_DATA_STATE"
/* invoke-direct {v5, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 547 */
/* .local v5, "mobileNwFilter":Landroid/content/IntentFilter; */
v3 = this.mContext;
v4 = this.mMobileNwReceiver;
int v6 = 0; // const/4 v6, 0x0
v7 = this.mHandler;
int v8 = 4; // const/4 v8, 0x4
/* invoke-virtual/range {v3 ..v8}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent; */
/* .line 550 */
} // .end local v5 # "mobileNwFilter":Landroid/content/IntentFilter;
} // :cond_0
v2 = this.mQosUtils;
if ( v2 != null) { // if-eqz v2, :cond_1
v3 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkPolicyQosUtils ) v2 ).systemReady ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->systemReady(Lcom/android/server/net/MiuiNetworkManagementService;)V
/* .line 551 */
} // :cond_1
v2 = this.mAppBuckets;
if ( v2 != null) { // if-eqz v2, :cond_2
(( com.android.server.net.MiuiNetworkPolicyAppBuckets ) v2 ).systemReady ( ); // invoke-virtual {v2}, Lcom/android/server/net/MiuiNetworkPolicyAppBuckets;->systemReady()V
/* .line 552 */
} // :cond_2
v2 = this.mMobileTcUtils;
if ( v2 != null) { // if-eqz v2, :cond_3
v3 = this.mNetworkManager;
(( com.android.server.net.MiuiNetworkPolicyTrafficLimit ) v2 ).systemReady ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->systemReady(Lcom/android/server/net/MiuiNetworkManagementService;)V
/* .line 556 */
} // :cond_3
/* sget-boolean v2, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z */
/* if-nez v2, :cond_4 */
/* sget-boolean v2, Lmiui/util/DeviceLevel;->IS_MIUI_GO_VERSION:Z */
/* if-nez v2, :cond_4 */
/* .line 557 */
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
final String v3 = "is MIUI or is not apply getMiuiLiteVersion"; // const-string v3, "is MIUI or is not apply getMiuiLiteVersion"
android.util.Log .d ( v2,v3 );
/* .line 558 */
v2 = this.mContext;
com.xiaomi.NetworkBoost.NetworkBoostService .getInstance ( v2 );
this.mNetworkBoostService = v2;
/* .line 560 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 561 */
/* iget v3, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mNetworkPriorityMode:I */
/* iget v4, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mTrafficPolicyMode:I */
/* iget v5, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->mThermalForceMode:I */
(( com.xiaomi.NetworkBoost.NetworkBoostService ) v2 ).onNetworkPriorityChanged ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Lcom/xiaomi/NetworkBoost/NetworkBoostService;->onNetworkPriorityChanged(III)V
/* .line 565 */
} // :cond_4
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerPowerKeeperSleep()V */
/* .line 566 */
return;
} // .end method
void updateRouterWhitelist ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "whitelist" # Ljava/lang/String; */
/* .line 1959 */
try { // :try_start_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1960 */
try { // :try_start_1
final String v0 = "RT whitelist is empty."; // const-string v0, "RT whitelist is empty."
android.util.Log .e ( v1,v0 );
/* .line 1961 */
return;
/* .line 1963 */
} // :cond_0
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1964 */
/* .local v0, "temp":[Ljava/lang/String; */
v2 = this.mWmmerRouterWhitelist;
(( java.util.HashSet ) v2 ).clear ( ); // invoke-virtual {v2}, Ljava/util/HashSet;->clear()V
/* .line 1965 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v0 */
/* if-ge v2, v3, :cond_1 */
/* .line 1966 */
v3 = this.mWmmerRouterWhitelist;
/* aget-object v4, v0, v2 */
(( java.util.HashSet ) v3 ).add ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1967 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "update RT " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v4, v0, v2 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v3 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1965 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1971 */
} // .end local v0 # "temp":[Ljava/lang/String;
} // .end local v2 # "i":I
} // :cond_1
/* .line 1969 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1970 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1972 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
