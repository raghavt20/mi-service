public class com.android.server.net.MiuiNetworkPolicyTrafficLimit {
	 /* .source "MiuiNetworkPolicyTrafficLimit.java" */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final Boolean IS_QCOM;
	 private static final Integer MOBILE_TC_MODE_CLOSED;
	 private static final Integer MOBILE_TC_MODE_DROP;
	 public static final Integer RULE_RESULT_ERROR_EXIST;
	 public static final Integer RULE_RESULT_ERROR_IFACE;
	 public static final Integer RULE_RESULT_ERROR_OFF;
	 public static final Integer RULE_RESULT_ERROR_ON;
	 public static final Integer RULE_RESULT_ERROR_PARAMS;
	 public static final Integer RULE_RESULT_ERROR_PROCESSING_ENABLE_LIMIT;
	 public static final Integer RULE_RESULT_ERROR_PROCESSING_SET_LIMIT;
	 public static final Integer RULE_RESULT_INIT;
	 public static final Integer RULE_RESULT_SUCCESS;
	 private static final java.lang.String TAG;
	 private static final Integer TRAFFIC_LIMIT_INIT;
	 private static final Integer TRAFFIC_LIMIT_OFF;
	 private static final Integer TRAFFIC_LIMIT_ON;
	 private static final Integer TRAFFIC_LIMIT_RULE_OFF;
	 private static final Integer TRAFFIC_LIMIT_RULE_ON;
	 /* # instance fields */
	 private android.net.ConnectivityManager mCm;
	 private final android.content.Context mContext;
	 private java.lang.String mDefaultInputMethod;
	 private final android.os.Handler mHandler;
	 private java.lang.String mIface;
	 private Boolean mIsMobileNwOn;
	 private Integer mLastMobileTcMode;
	 private Boolean mMobileLimitEnabled;
	 private Boolean mMobileTcDropEnabled;
	 private com.android.server.net.MiuiNetworkManagementService mNetMgrService;
	 private Boolean mProcessEnableLimit;
	 private Boolean mProcessSetLimit;
	 final android.content.BroadcastReceiver mReceiver;
	 private Integer mTrafficLimitMode;
	 private java.util.Set mWhiteListAppsPN;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.Set mWhiteListAppsUid;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.lang.String -$$Nest$fgetmIface ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mIface;
} // .end method
static Boolean -$$Nest$fgetmIsMobileNwOn ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z */
} // .end method
static Integer -$$Nest$fgetmLastMobileTcMode ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I */
} // .end method
static Boolean -$$Nest$fgetmMobileTcDropEnabled ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileTcDropEnabled:Z */
} // .end method
static com.android.server.net.MiuiNetworkManagementService -$$Nest$fgetmNetMgrService ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetMgrService;
} // .end method
static void -$$Nest$fputmDefaultInputMethod ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mDefaultInputMethod = p1;
return;
} // .end method
static void -$$Nest$fputmProcessEnableLimit ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessEnableLimit:Z */
return;
} // .end method
static void -$$Nest$fputmProcessSetLimit ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessSetLimit:Z */
return;
} // .end method
static void -$$Nest$menableMobileTcMode ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTcMode(I)V */
return;
} // .end method
static java.lang.String -$$Nest$mgetDefaultInputMethod ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getDefaultInputMethod()Ljava/lang/String; */
} // .end method
static Integer -$$Nest$mgetUserMobileTcMode ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getUserMobileTcMode()I */
} // .end method
static void -$$Nest$misAllowedToEnableMobileTcMode ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isAllowedToEnableMobileTcMode()V */
return;
} // .end method
static void -$$Nest$mlog ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mupdateMobileTcWhiteList ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateMobileTcWhiteList()V */
return;
} // .end method
static void -$$Nest$mupdateTrafficLimitStatus ( com.android.server.net.MiuiNetworkPolicyTrafficLimit p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateTrafficLimitStatus(I)V */
return;
} // .end method
static com.android.server.net.MiuiNetworkPolicyTrafficLimit ( ) {
/* .locals 2 */
/* .line 89 */
/* const-string/jumbo v0, "vendor" */
miui.util.FeatureParser .getString ( v0 );
final String v1 = "qcom"; // const-string v1, "qcom"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.android.server.net.MiuiNetworkPolicyTrafficLimit.IS_QCOM = (v0!= 0);
return;
} // .end method
public com.android.server.net.MiuiNetworkPolicyTrafficLimit ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 99 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 88 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z */
/* .line 94 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileLimitEnabled:Z */
/* .line 97 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileTcDropEnabled:Z */
/* .line 423 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$9; */
/* invoke-direct {v0, p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$9;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)V */
this.mReceiver = v0;
/* .line 100 */
this.mContext = p1;
/* .line 101 */
this.mHandler = p2;
/* .line 102 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->initParams()V */
/* .line 103 */
return;
} // .end method
private void enableMobileTcMode ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "mode" # I */
/* .line 249 */
int v0 = 0; // const/4 v0, 0x0
/* .line 250 */
/* .local v0, "isNeedUpdate":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isLimitterEnabled()Z */
/* .line 251 */
/* .local v1, "oldLimitterEnabled":Z */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isLimitterEnabled(I)Z */
/* .line 252 */
/* .local v2, "newLimitterEnabled":Z */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "enableMobileTcMode oldLimitterEnabled="; // const-string v4, "enableMobileTcMode oldLimitterEnabled="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ",newLimitterEnabled="; // const-string v4, ",newLimitterEnabled="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 253 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* if-nez v2, :cond_0 */
/* .line 254 */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTrafficLimit(Z)V */
/* .line 255 */
/* iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I */
/* .line 256 */
} // :cond_0
/* if-nez v1, :cond_1 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget-boolean v3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 257 */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTrafficLimit(Z)V */
/* .line 258 */
/* iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I */
/* .line 259 */
int v0 = 1; // const/4 v0, 0x1
/* .line 262 */
} // :cond_1
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 263 */
v3 = this.mHandler;
/* const/16 v4, 0xc */
(( android.os.Handler ) v3 ).obtainMessage ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Handler ) v3 ).sendMessage ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 266 */
} // :cond_2
return;
} // .end method
private synchronized void enableMobileTrafficLimit ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* monitor-enter p0 */
/* .line 305 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enableMobileTrafficLimit enabled="; // const-string v1, "enableMobileTrafficLimit enabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",mIface="; // const-string v1, ",mIface="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mIface;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 306 */
v0 = this.mNetMgrService;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mIface;
v0 = android.text.TextUtils .isEmpty ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 310 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessEnableLimit:Z */
/* .line 311 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$6;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 326 */
/* monitor-exit p0 */
return;
/* .line 307 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
} // :cond_1
} // :goto_0
try { // :try_start_1
final String v0 = "enableMobileTrafficLimit return by invalid value!!!"; // const-string v0, "enableMobileTrafficLimit return by invalid value!!!"
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 308 */
/* monitor-exit p0 */
return;
/* .line 304 */
} // .end local p1 # "enabled":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private java.lang.String getDefaultInputMethod ( ) {
/* .locals 4 */
/* .line 149 */
final String v0 = ""; // const-string v0, ""
/* .line 150 */
/* .local v0, "defaultInputMethod":Ljava/lang/String; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "default_input_method"; // const-string v2, "default_input_method"
android.provider.Settings$Secure .getString ( v1,v2 );
/* .line 152 */
/* .local v1, "inputMethodId":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_0 */
/* .line 153 */
final String v2 = "/"; // const-string v2, "/"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 154 */
/* .local v2, "inputMethodIds":[Ljava/lang/String; */
/* array-length v3, v2 */
/* if-lez v3, :cond_0 */
/* .line 155 */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v0, v2, v3 */
/* .line 158 */
} // .end local v2 # "inputMethodIds":[Ljava/lang/String;
} // :cond_0
} // .end method
private java.lang.String getMobileLinkIface ( ) {
/* .locals 2 */
/* .line 438 */
v0 = this.mCm;
/* if-nez v0, :cond_0 */
/* .line 439 */
v0 = this.mContext;
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.mCm = v0;
/* .line 441 */
} // :cond_0
v0 = this.mCm;
int v1 = 0; // const/4 v1, 0x0
(( android.net.ConnectivityManager ) v0 ).getLinkProperties ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;
/* .line 442 */
/* .local v0, "prop":Landroid/net/LinkProperties; */
if ( v0 != null) { // if-eqz v0, :cond_2
(( android.net.LinkProperties ) v0 ).getInterfaceName ( ); // invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
v1 = android.text.TextUtils .isEmpty ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 445 */
} // :cond_1
(( android.net.LinkProperties ) v0 ).getInterfaceName ( ); // invoke-virtual {v0}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;
/* .line 443 */
} // :cond_2
} // :goto_0
final String v1 = ""; // const-string v1, ""
} // .end method
private Integer getUserMobileTcMode ( ) {
/* .locals 3 */
/* .line 299 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "mobile_tc_user_enable"; // const-string v1, "mobile_tc_user_enable"
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* .line 301 */
/* .local v0, "enable":I */
} // .end method
private void initParams ( ) {
/* .locals 2 */
/* .line 120 */
final String v0 = ""; // const-string v0, ""
this.mIface = v0;
/* .line 121 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I */
/* .line 122 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessEnableLimit:Z */
/* .line 123 */
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessSetLimit:Z */
/* .line 124 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
this.mWhiteListAppsUid = v1;
/* .line 125 */
/* iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I */
/* .line 126 */
return;
} // .end method
private void initReceiver ( ) {
/* .locals 3 */
/* .line 418 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 419 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 420 */
v1 = this.mContext;
v2 = this.mReceiver;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 421 */
return;
} // .end method
private Integer isAllowToSetTrafficRule ( Boolean p0, java.lang.String p1, Long p2 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .param p2, "iface" # Ljava/lang/String; */
/* .param p3, "rate" # J */
/* .line 369 */
int v0 = 0; // const/4 v0, 0x0
/* .line 370 */
/* .local v0, "rst":I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isAllowToSetTrafficRule enabled="; // const-string v2, "isAllowToSetTrafficRule enabled="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ",mode="; // const-string v2, ",mode="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 371 */
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessEnableLimit:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 7; // const/4 v1, 0x7
/* .line 372 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessSetLimit:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* const/16 v1, 0x8 */
/* .line 373 */
} // :cond_1
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 374 */
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I */
/* packed-switch v1, :pswitch_data_0 */
/* .line 386 */
/* .line 383 */
/* :pswitch_0 */
int v0 = 3; // const/4 v0, 0x3
/* .line 384 */
/* .line 380 */
/* :pswitch_1 */
int v0 = 1; // const/4 v0, 0x1
/* .line 381 */
/* .line 376 */
/* :pswitch_2 */
int v0 = 2; // const/4 v0, 0x2
/* .line 377 */
/* .line 389 */
} // :cond_2
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I */
/* packed-switch v1, :pswitch_data_1 */
/* .line 400 */
/* :pswitch_3 */
int v0 = 1; // const/4 v0, 0x1
/* .line 401 */
/* .line 397 */
/* :pswitch_4 */
int v0 = 3; // const/4 v0, 0x3
/* .line 398 */
/* .line 394 */
/* :pswitch_5 */
int v0 = 4; // const/4 v0, 0x4
/* .line 395 */
/* .line 391 */
/* :pswitch_6 */
int v0 = 2; // const/4 v0, 0x2
/* .line 392 */
/* nop */
/* .line 407 */
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_4 */
/* .line 408 */
v1 = /* invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidMobileIface(Ljava/lang/String;)Z */
/* if-nez v1, :cond_3 */
/* .line 409 */
int v0 = 6; // const/4 v0, 0x6
/* .line 410 */
} // :cond_3
v1 = /* invoke-direct {p0, p3, p4}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidBandWidth(J)Z */
/* if-nez v1, :cond_4 */
/* .line 411 */
int v0 = 5; // const/4 v0, 0x5
/* .line 414 */
} // :cond_4
} // :goto_1
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
} // .end packed-switch
} // .end method
private synchronized void isAllowedToEnableMobileTcMode ( ) {
/* .locals 5 */
/* monitor-enter p0 */
/* .line 489 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getMobileLinkIface()Ljava/lang/String; */
/* .line 490 */
/* .local v0, "iface":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "isAllowedToEnableMobileTcMode iface="; // const-string v2, "isAllowedToEnableMobileTcMode iface="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ",mIface="; // const-string v2, ",mIface="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mIface;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 491 */
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z */
/* .line 492 */
/* .local v1, "wasMobileNwOn":Z */
v2 = android.text.TextUtils .isEmpty ( v0 );
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* move v2, v3 */
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z */
/* .line 493 */
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.mIface;
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 494 */
this.mIface = v0;
/* .line 497 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
} // :cond_1
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z */
/* if-eq v2, v1, :cond_3 */
/* .line 498 */
v2 = /* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getUserMobileTcMode()I */
/* .line 499 */
/* .local v2, "tcMode":I */
v4 = /* invoke-direct {p0, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isLimitterEnabled(I)Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 500 */
/* iget-boolean v4, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mIsMobileNwOn:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* move v3, v2 */
} // :cond_2
/* invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->enableMobileTcMode(I)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 503 */
} // .end local v2 # "tcMode":I
} // :cond_3
/* monitor-exit p0 */
return;
/* .line 488 */
} // .end local v0 # "iface":Ljava/lang/String;
} // .end local v1 # "wasMobileNwOn":Z
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private Boolean isLimitterEnabled ( ) {
/* .locals 1 */
/* .line 241 */
/* iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mLastMobileTcMode:I */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isLimitterEnabled(I)Z */
} // .end method
private Boolean isLimitterEnabled ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 245 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isUidValidForMobileTrafficLimit ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "uid" # I */
/* .line 229 */
v0 = android.os.UserHandle .isApp ( p0 );
} // .end method
private Boolean isValidBandWidth ( Long p0 ) {
/* .locals 2 */
/* .param p1, "rate" # J */
/* .line 449 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p1, v0 */
/* if-ltz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isValidMobileIface ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "iface" # Ljava/lang/String; */
/* .line 453 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->IS_QCOM:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "rmnet"; // const-string v0, "rmnet"
v0 = (( java.lang.String ) p1 ).indexOf ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 456 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 454 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isValidStateForWhiteListApp ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 223 */
/* if-ltz p2, :cond_1 */
int v0 = 4; // const/4 v0, 0x4
/* if-le p2, v0, :cond_0 */
/* const/16 v0, 0x13 */
/* if-ge p2, v0, :cond_1 */
v0 = this.mWhiteListAppsUid;
/* .line 224 */
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 223 */
} // :goto_0
} // .end method
private void log ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "s" # Ljava/lang/String; */
/* .line 506 */
final String v0 = "MiuiNetworkPolicyTrafficLimit"; // const-string v0, "MiuiNetworkPolicyTrafficLimit"
android.util.Log .d ( v0,p1 );
/* .line 507 */
return;
} // .end method
private void registerDefaultInputMethodObserver ( ) {
/* .locals 5 */
/* .line 162 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$3; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$3;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/os/Handler;)V */
/* .line 170 */
/* .local v0, "dimObserver":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 171 */
final String v2 = "default_input_method"; // const-string v2, "default_input_method"
android.provider.Settings$Secure .getUriFor ( v2 );
/* .line 170 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 173 */
return;
} // .end method
private void registerMobileTrafficControllerChangedObserver ( ) {
/* .locals 5 */
/* .line 269 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$4;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/os/Handler;)V */
/* .line 280 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 281 */
final String v2 = "mobile_tc_user_enable"; // const-string v2, "mobile_tc_user_enable"
android.provider.Settings$Global .getUriFor ( v2 );
/* .line 280 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 284 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$5; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$5;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 290 */
return;
} // .end method
private void registerWhiteListAppsChangedObserver ( ) {
/* .locals 5 */
/* .line 129 */
/* new-instance v0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$1; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/os/Handler;)V */
/* .line 136 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 137 */
final String v2 = "mobile_tc_white_list_pkg_name"; // const-string v2, "mobile_tc_white_list_pkg_name"
android.provider.Settings$Global .getUriFor ( v2 );
/* .line 136 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 140 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$2; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$2;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Landroid/database/ContentObserver;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 146 */
return;
} // .end method
private synchronized void setMobileTrafficLimit ( Boolean p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .param p2, "rate" # J */
/* monitor-enter p0 */
/* .line 329 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setMobileTrafficLimit " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",rate="; // const-string v1, ",rate="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 330 */
v0 = this.mNetMgrService;
/* if-nez v0, :cond_0 */
/* .line 331 */
/* const-string/jumbo v0, "setMobileTrafficLimit return by invalid value!!!" */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 332 */
/* monitor-exit p0 */
return;
/* .line 334 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_1
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mProcessSetLimit:Z */
/* .line 335 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7; */
/* invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;ZJ)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 346 */
/* monitor-exit p0 */
return;
/* .line 328 */
} // .end local p1 # "enabled":Z
} // .end local p2 # "rate":J
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private synchronized void updateMobileTcWhiteList ( ) {
/* .locals 11 */
/* monitor-enter p0 */
/* .line 176 */
try { // :try_start_0
v0 = this.mContext;
/* const-string/jumbo v1, "user" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/UserManager; */
/* .line 177 */
/* .local v0, "um":Landroid/os/UserManager; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 178 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
(( android.os.UserManager ) v0 ).getUsers ( ); // invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;
/* .line 179 */
/* .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;" */
(( com.android.server.net.MiuiNetworkPolicyTrafficLimit ) p0 ).fetchMobileTcEnabledList ( ); // invoke-virtual {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->fetchMobileTcEnabledList()Ljava/util/Set;
this.mWhiteListAppsPN = v3;
/* .line 180 */
v3 = this.mDefaultInputMethod;
v3 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v3, :cond_0 */
v3 = this.mWhiteListAppsPN;
v3 = v4 = this.mDefaultInputMethod;
/* if-nez v3, :cond_0 */
/* .line 181 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updateMobileTcWhiteList add pkgNames=" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mDefaultInputMethod;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 182 */
v3 = this.mWhiteListAppsPN;
v4 = this.mDefaultInputMethod;
/* .line 184 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
} // :cond_0
v3 = this.mWhiteListAppsUid;
/* .line 185 */
v3 = v3 = this.mWhiteListAppsPN;
/* if-nez v3, :cond_3 */
/* .line 186 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Landroid/content/pm/UserInfo; */
/* .line 187 */
/* .local v4, "user":Landroid/content/pm/UserInfo; */
/* iget v5, v4, Landroid/content/pm/UserInfo;->id:I */
int v6 = 0; // const/4 v6, 0x0
(( android.content.pm.PackageManager ) v1 ).getInstalledPackagesAsUser ( v6, v5 ); // invoke-virtual {v1, v6, v5}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;
/* .line 188 */
/* .local v5, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_2
/* check-cast v7, Landroid/content/pm/PackageInfo; */
/* .line 189 */
/* .local v7, "app":Landroid/content/pm/PackageInfo; */
v8 = this.packageName;
if ( v8 != null) { // if-eqz v8, :cond_1
v8 = this.applicationInfo;
if ( v8 != null) { // if-eqz v8, :cond_1
v8 = this.mWhiteListAppsPN;
v9 = this.packageName;
v8 = /* .line 190 */
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 191 */
/* iget v8, v4, Landroid/content/pm/UserInfo;->id:I */
v9 = this.applicationInfo;
/* iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I */
v8 = android.os.UserHandle .getUid ( v8,v9 );
/* .line 192 */
/* .local v8, "uid":I */
v9 = this.mWhiteListAppsUid;
java.lang.Integer .valueOf ( v8 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 194 */
} // .end local v7 # "app":Landroid/content/pm/PackageInfo;
} // .end local v8 # "uid":I
} // :cond_1
/* .line 195 */
} // .end local v4 # "user":Landroid/content/pm/UserInfo;
} // .end local v5 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_2
/* .line 197 */
} // :cond_3
/* monitor-exit p0 */
return;
/* .line 175 */
} // .end local v0 # "um":Landroid/os/UserManager;
} // .end local v1 # "pm":Landroid/content/pm/PackageManager;
} // .end local v2 # "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private void updateTrafficLimitStatus ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 364 */
/* iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mTrafficLimitMode:I */
/* .line 365 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateTrafficLimitStatus mode=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 366 */
return;
} // .end method
private synchronized void updateWhiteListUidForMobileTraffic ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "add" # Z */
/* monitor-enter p0 */
/* .line 349 */
try { // :try_start_0
final String v0 = "MiuiNetworkPolicyTrafficLimit"; // const-string v0, "MiuiNetworkPolicyTrafficLimit"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateWhiteListUidForMobileTraffic uid=" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ",add="; // const-string v2, ",add="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v1 );
/* .line 350 */
v0 = this.mNetMgrService;
/* if-nez v0, :cond_0 */
/* .line 351 */
/* const-string/jumbo v0, "updateWhiteListUidForMobileTraffic return by invalid value!!!" */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 352 */
/* monitor-exit p0 */
return;
/* .line 354 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
} // :cond_0
try { // :try_start_1
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$8;-><init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;IZ)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 361 */
/* monitor-exit p0 */
return;
/* .line 348 */
} // .end local p1 # "uid":I
} // .end local p2 # "add":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
/* # virtual methods */
public java.util.Set fetchMobileTcEnabledList ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 200 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "mobile_tc_white_list_pkg_name"; // const-string v1, "mobile_tc_white_list_pkg_name"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 202 */
/* .local v0, "pkgNames":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "fetchMobileTcEnabledList pkgNames="; // const-string v2, "fetchMobileTcEnabledList pkgNames="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 203 */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 204 */
/* new-instance v1, Ljava/util/HashSet; */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 205 */
/* .local v1, "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .line 207 */
} // .end local v1 # "whiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :cond_0
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
} // .end method
public java.lang.String getMobileIface ( ) {
/* .locals 1 */
/* .line 471 */
v0 = this.mIface;
} // .end method
public Boolean getMobileLimitStatus ( ) {
/* .locals 1 */
/* .line 475 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileLimitEnabled:Z */
} // .end method
public synchronized Integer setMobileTrafficLimitForGame ( Boolean p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "enabled" # Z */
/* .param p2, "rate" # J */
/* monitor-enter p0 */
/* .line 460 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_0
/* const-wide/16 v1, 0x0 */
/* cmp-long v1, p2, v1 */
/* if-nez v1, :cond_0 */
/* move v1, v0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
try { // :try_start_0
/* iput-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileTcDropEnabled:Z */
/* .line 461 */
v1 = this.mIface;
v1 = /* invoke-direct {p0, p1, v1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isAllowToSetTrafficRule(ZLjava/lang/String;J)I */
/* .line 462 */
/* .local v1, "rst":I */
/* if-eq v1, v0, :cond_1 */
/* .line 463 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setMobileTrafficLimitForGame rst=" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ",mMobileTcDropEnabled="; // const-string v2, ",mMobileTcDropEnabled="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileTcDropEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 464 */
/* monitor-exit p0 */
/* .line 466 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
} // :cond_1
try { // :try_start_1
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->setMobileTrafficLimit(ZJ)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 467 */
/* monitor-exit p0 */
/* .line 459 */
} // .end local v1 # "rst":I
} // .end local p1 # "enabled":Z
} // .end local p2 # "rate":J
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void systemReady ( com.android.server.net.MiuiNetworkManagementService p0 ) {
/* .locals 2 */
/* .param p1, "networkMgr" # Lcom/android/server/net/MiuiNetworkManagementService; */
/* .line 106 */
this.mNetMgrService = p1;
/* .line 107 */
v0 = this.mContext;
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.mCm = v0;
/* .line 108 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->initReceiver()V */
/* .line 109 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->getDefaultInputMethod()Ljava/lang/String; */
this.mDefaultInputMethod = v0;
/* .line 110 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->registerDefaultInputMethodObserver()V */
/* .line 111 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->registerWhiteListAppsChangedObserver()V */
/* .line 112 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->registerMobileTrafficControllerChangedObserver()V */
/* .line 113 */
v0 = this.mNetMgrService;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.net.MiuiNetworkManagementService ) v0 ).enableLimitter ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableLimitter(Z)Z
/* .line 114 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateMobileTcWhiteList()V */
/* .line 115 */
/* const-string/jumbo v0, "systemReady" */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 116 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isAllowedToEnableMobileTcMode()V */
/* .line 117 */
return;
} // .end method
public synchronized void updateAppPN ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "installed" # Z */
/* monitor-enter p0 */
/* .line 212 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateAppPN packageName=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",uid="; // const-string v1, ",uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",installed="; // const-string v1, ",installed="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 213 */
v0 = this.mWhiteListAppsPN;
v0 = if ( v0 != null) { // if-eqz v0, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 214 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 215 */
v0 = this.mWhiteListAppsUid;
java.lang.Integer .valueOf ( p2 );
/* .line 217 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
} // :cond_0
v0 = this.mWhiteListAppsUid;
java.lang.Integer .valueOf ( p2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 220 */
} // :cond_1
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 211 */
} // .end local p1 # "packageName":Ljava/lang/String;
} // .end local p2 # "uid":I
} // .end local p3 # "installed":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void updateMobileLimit ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 479 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateMobileLimit enabled=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->log(Ljava/lang/String;)V */
/* .line 480 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileLimitEnabled:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 481 */
/* const-wide/16 v0, 0x0 */
/* .line 483 */
/* .local v0, "bwBps":J */
/* invoke-direct {p0, p1, v0, v1}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->setMobileTrafficLimit(ZJ)V */
/* .line 484 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->mMobileLimitEnabled:Z */
/* .line 486 */
} // .end local v0 # "bwBps":J
} // :cond_0
return;
} // .end method
public void updateRulesForUidStateChange ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "oldUidState" # I */
/* .param p3, "newUidState" # I */
/* .line 233 */
v0 = com.android.server.net.MiuiNetworkPolicyTrafficLimit .isUidValidForMobileTrafficLimit ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 234 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidStateForWhiteListApp(II)Z */
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidStateForWhiteListApp(II)Z */
/* if-eq v0, v1, :cond_0 */
/* .line 235 */
v0 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->isValidStateForWhiteListApp(II)Z */
/* invoke-direct {p0, p1, v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->updateWhiteListUidForMobileTraffic(IZ)V */
/* .line 238 */
} // :cond_0
return;
} // .end method
