.class Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
.super Ljava/lang/Object;
.source "MiuiNetworkManagementService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkManagementService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MiuiFireRule"
.end annotation


# instance fields
.field pkgName:Ljava/lang/String;

.field rule:I

.field setterPid:I

.field setterUid:I

.field uid:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "rule"    # I
    .param p4, "setterUid"    # I
    .param p5, "setterPid"    # I

    .line 587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 588
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->pkgName:Ljava/lang/String;

    .line 589
    iput p2, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->uid:I

    .line 590
    iput p3, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->rule:I

    .line 591
    iput p4, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->setterUid:I

    .line 592
    iput p5, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->setterPid:I

    .line 593
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .line 607
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    return v0

    .line 608
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 609
    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;

    .line 610
    .local v0, "fireRule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->pkgName:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->pkgName:Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    return v1

    .line 608
    .end local v0    # "fireRule":Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 615
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->pkgName:Ljava/lang/String;

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 597
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{pkgName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->uid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->rule:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", setterUid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->setterUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", setterPid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$MiuiFireRule;->setterPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
