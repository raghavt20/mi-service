.class Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;
.super Ljava/lang/Object;
.source "NetworkStatsReporter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/NetworkStatsReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackgroundNetworkStats"
.end annotation


# instance fields
.field private mMobileRxBytes:J

.field private mMobileTotalBytes:J

.field private mMobileTxBytes:J

.field private mPackageName:Ljava/lang/String;

.field private mTotalBytes:J

.field private mTotalRxBytes:J

.field private mTotalTxBytes:J

.field private mUid:I

.field private mWifiRxBytes:J

.field private mWifiTotalBytes:J

.field private mWifiTxBytes:J

.field final synthetic this$0:Lcom/android/server/net/NetworkStatsReporter;


# direct methods
.method static bridge synthetic -$$Nest$fgetmMobileRxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileRxBytes:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmMobileTotalBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTotalBytes:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmMobileTxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTxBytes:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageName(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTotalBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalBytes:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmTotalRxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalRxBytes:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmTotalTxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalTxBytes:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiRxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiRxBytes:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiTotalBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTotalBytes:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiTxBytes(Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTxBytes:J

    return-wide v0
.end method

.method public constructor <init>(Lcom/android/server/net/NetworkStatsReporter;ILjava/lang/String;)V
    .locals 0
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 266
    iput-object p1, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->this$0:Lcom/android/server/net/NetworkStatsReporter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    iput p2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mUid:I

    .line 268
    iput-object p3, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mPackageName:Ljava/lang/String;

    .line 269
    return-void
.end method


# virtual methods
.method public updateBytes(Landroid/net/NetworkTemplate;Landroid/net/NetworkStats$Entry;)V
    .locals 4
    .param p1, "networkTemplate"    # Landroid/net/NetworkTemplate;
    .param p2, "entry"    # Landroid/net/NetworkStats$Entry;

    .line 272
    invoke-virtual {p1}, Landroid/net/NetworkTemplate;->getMatchRule()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 274
    :sswitch_0
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTxBytes:J

    iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->txBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTxBytes:J

    .line 275
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiRxBytes:J

    iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->rxBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiRxBytes:J

    .line 276
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTxBytes:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mWifiTotalBytes:J

    .line 277
    goto :goto_0

    .line 279
    :sswitch_1
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTxBytes:J

    iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->txBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTxBytes:J

    .line 280
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileRxBytes:J

    iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->rxBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileRxBytes:J

    .line 281
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTxBytes:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mMobileTotalBytes:J

    .line 282
    nop

    .line 287
    :goto_0
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalTxBytes:J

    iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->txBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalTxBytes:J

    .line 288
    iget-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalRxBytes:J

    iget-wide v2, p2, Landroid/net/NetworkStats$Entry;->rxBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalRxBytes:J

    .line 289
    iget-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalTxBytes:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/server/net/NetworkStatsReporter$BackgroundNetworkStats;->mTotalBytes:J

    .line 290
    return-void

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x4 -> :sswitch_0
    .end sparse-switch
.end method
