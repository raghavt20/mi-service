public class com.android.server.net.MiuiNetworkPolicyQosUtils {
	 /* .source "MiuiNetworkPolicyQosUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;, */
	 /* Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTION_CLOUD_MEETING_LIST_DONE;
private static final java.lang.String ACTIVITY_STATUS;
private static final java.lang.String AN_WECHAT_SCAN_UI;
private static final Boolean DEBUG;
private static final Integer DSCP_DEFAULT_STATE;
private static final Integer DSCP_INIT_STATE;
private static final Integer DSCP_QOS_STATE;
private static final Integer DSCP_TOS_EF;
private static final java.lang.String EF_AN_TIKTOK;
private static final java.lang.String EF_AN_WECHAT;
private static final java.lang.String EF_MIMO_AN;
private static final java.lang.String EF_MIMO_PN;
private static final java.lang.String EF_PN_TIKTOK;
private static final java.lang.String EF_PN_WECHAT;
private static final java.lang.String GAME_ENABLED;
private static final java.lang.String GAME_SCENE;
private static final Integer IP;
public static final Boolean IS_QCOM;
private static final java.lang.String LATENCY_ACTION_CHANGE_LEVEL;
private static final java.lang.String LATENCY_KEY_SCENE;
private static final java.lang.String LOCAL_EF_APP_LIST;
private static final java.lang.String MEETING_OPTIMIZATION_WHITE_LIST_DEFAULT;
private static final java.lang.String MEETING_VOIP_ENABLED;
private static final java.lang.String MEETING_VOIP_SCENE;
private static final java.lang.String MIMO_SCENE_EXTRA;
private static final java.lang.String NOTIFACATION_RECEIVER_PACKAGE;
private static final Integer SCENE_DEFAULT;
private static final Integer SCENE_VOIP;
private static final Integer SCENE_WECHAT_SCAN;
private static final java.lang.String TAG;
private static final Integer UDP;
private static final java.lang.String WECHAT_VOIP_ENABLED;
private static final java.lang.String WECHAT_VOIP_SCENE;
private static final java.util.ArrayList mActivityStatusScene;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mMeetingAppsAN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mMeetingAppsPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private miui.process.IActivityChangeListener$Stub mActivityChangeListener;
private final android.content.Context mContext;
private Integer mDscpState;
private java.util.Set mEFAppsPN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.os.Handler mHandler;
private final android.content.BroadcastReceiver mIntentReceiver;
private Boolean mIsMobileDataOn;
private com.android.server.net.MiuiNetworkManagementService mNetMgrService;
private com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo mQosInfo;
private java.util.concurrent.ConcurrentHashMap mUidMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.net.MiuiNetworkPolicyQosUtils p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmIsMobileDataOn ( com.android.server.net.MiuiNetworkPolicyQosUtils p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z */
} // .end method
static com.android.server.net.MiuiNetworkManagementService -$$Nest$fgetmNetMgrService ( com.android.server.net.MiuiNetworkPolicyQosUtils p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNetMgrService;
} // .end method
static com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo -$$Nest$fgetmQosInfo ( com.android.server.net.MiuiNetworkPolicyQosUtils p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mQosInfo;
} // .end method
static void -$$Nest$fputmIsMobileDataOn ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z */
return;
} // .end method
static void -$$Nest$menableLLMMode ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, Boolean p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableLLMMode(ZI)V */
return;
} // .end method
static Integer -$$Nest$mgetUidFromMap ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->getUidFromMap(Ljava/lang/String;)I */
} // .end method
static void -$$Nest$minitMeetingWhiteList ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->initMeetingWhiteList(Landroid/content/Context;)V */
return;
} // .end method
static Boolean -$$Nest$misActivityNameContains ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, java.lang.String p1, java.lang.String[] p2 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isActivityNameContains(Ljava/lang/String;[Ljava/lang/String;)Z */
} // .end method
static void -$$Nest$mprocessMeetingListChanged ( com.android.server.net.MiuiNetworkPolicyQosUtils p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->processMeetingListChanged()V */
return;
} // .end method
static void -$$Nest$mregisterActivityChangeListener ( com.android.server.net.MiuiNetworkPolicyQosUtils p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->registerActivityChangeListener()V */
return;
} // .end method
static void -$$Nest$msetQos ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, Boolean p1, Integer p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setQos(ZII)V */
return;
} // .end method
static void -$$Nest$mupdateDscpStatus ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateDscpStatus(Z)V */
return;
} // .end method
static void -$$Nest$mupdateQosInfo ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, Boolean p1, Integer p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosInfo(ZII)V */
return;
} // .end method
static void -$$Nest$mupdateVoipStatus ( com.android.server.net.MiuiNetworkPolicyQosUtils p0, java.lang.String p1, Boolean p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateVoipStatus(Ljava/lang/String;ZI)V */
return;
} // .end method
static Integer -$$Nest$sfgetUDP ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static java.util.ArrayList -$$Nest$sfgetmActivityStatusScene ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mActivityStatusScene;
} // .end method
static java.util.List -$$Nest$sfgetmMeetingAppsAN ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsAN;
} // .end method
static java.util.List -$$Nest$sfgetmMeetingAppsPN ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsPN;
} // .end method
static com.android.server.net.MiuiNetworkPolicyQosUtils ( ) {
/* .locals 11 */
/* .line 75 */
final String v0 = "com.tencent.tmgp.pubgmhd"; // const-string v0, "com.tencent.tmgp.pubgmhd"
final String v1 = "com.tencent.lolm"; // const-string v1, "com.tencent.lolm"
final String v2 = "com.tencent.tmgp.sgame"; // const-string v2, "com.tencent.tmgp.sgame"
/* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
/* .line 86 */
final String v0 = "com.tencent.mm"; // const-string v0, "com.tencent.mm"
final String v1 = "com.ss.android.ugc.aweme"; // const-string v1, "com.ss.android.ugc.aweme"
/* filled-new-array {v0, v1}, [Ljava/lang/String; */
/* .line 89 */
final String v1 = "com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v1, "com.tencent.mm.plugin.voip.ui.VideoActivity"
final String v2 = "com.ss.android.ugc.aweme.main.MainActivity"; // const-string v2, "com.ss.android.ugc.aweme.main.MainActivity"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
/* .line 100 */
/* .line 101 */
/* .line 104 */
/* const-string/jumbo v2, "vendor" */
miui.util.FeatureParser .getString ( v2 );
final String v3 = "qcom"; // const-string v3, "qcom"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.android.server.net.MiuiNetworkPolicyQosUtils.IS_QCOM = (v2!= 0);
/* .line 118 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 120 */
/* new-instance v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene; */
final String v4 = "mimo_scene"; // const-string v4, "mimo_scene"
/* invoke-direct {v3, v0, v1, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;-><init>([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V */
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 138 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 139 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 141 */
final String v1 = "com.tencent.mm"; // const-string v1, "com.tencent.mm"
final String v2 = "com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v2, "com.tencent.mm.plugin.voip.ui.VideoActivity"
final String v3 = "com.tencent.wemeet.app"; // const-string v3, "com.tencent.wemeet.app"
final String v4 = "com.tencent.wemeet.sdk.meeting.inmeeting.InMeetingActivity"; // const-string v4, "com.tencent.wemeet.sdk.meeting.inmeeting.InMeetingActivity"
final String v5 = "com.ss.android.lark"; // const-string v5, "com.ss.android.lark"
final String v6 = "com.ss.android.vc.meeting.module.multi.ByteRTCMeetingActivityInstance"; // const-string v6, "com.ss.android.vc.meeting.module.multi.ByteRTCMeetingActivityInstance"
final String v7 = "com.alibaba.android.rimet"; // const-string v7, "com.alibaba.android.rimet"
final String v8 = "com.alibaba.android.teleconf.mozi.activity.TeleVideoConfActivity"; // const-string v8, "com.alibaba.android.teleconf.mozi.activity.TeleVideoConfActivity"
final String v9 = "com.ss.android.lark.kami"; // const-string v9, "com.ss.android.lark.kami"
final String v10 = "com.ss.android.vc.meeting.module.multi.ByteRTCMeetingActivity"; // const-string v10, "com.ss.android.vc.meeting.module.multi.ByteRTCMeetingActivity"
/* filled-new-array/range {v1 ..v10}, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.net.MiuiNetworkPolicyQosUtils ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 154 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 72 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mUidMap = v0;
/* .line 102 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z */
/* .line 160 */
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V */
this.mActivityChangeListener = v1;
/* .line 506 */
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4; */
/* invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$4;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V */
this.mIntentReceiver = v1;
/* .line 155 */
this.mContext = p1;
/* .line 156 */
this.mHandler = p2;
/* .line 157 */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V */
/* .line 158 */
return;
} // .end method
private void addUidToMap ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 398 */
v0 = this.mUidMap;
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 399 */
v0 = this.mUidMap;
java.lang.Integer .valueOf ( p2 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 401 */
} // :cond_0
return;
} // .end method
private void enableLLMMode ( Boolean p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .param p2, "scene" # I */
/* .line 617 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enableLLMMode enable="; // const-string v1, "enableLLMMode enable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ",scene="; // const-string v1, ",scene="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicyQosUtils"; // const-string v1, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v1,v0 );
/* .line 618 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 619 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.phone.intent.action.CHANGE_LEVEL"; // const-string v1, "com.android.phone.intent.action.CHANGE_LEVEL"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 620 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 621 */
final String v1 = "scene"; // const-string v1, "scene"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 622 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 623 */
return;
} // .end method
private synchronized void enableQos ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* monitor-enter p0 */
/* .line 308 */
try { // :try_start_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosReadyForEnable(Z)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mNetMgrService;
/* if-nez v0, :cond_0 */
/* .line 312 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 320 */
/* monitor-exit p0 */
return;
/* .line 309 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
} // :cond_1
} // :goto_0
try { // :try_start_1
final String v0 = "MiuiNetworkPolicyQosUtils"; // const-string v0, "MiuiNetworkPolicyQosUtils"
final String v1 = "enableQos return by invalid value!!!"; // const-string v1, "enableQos return by invalid value!!!"
android.util.Log .i ( v0,v1 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 310 */
/* monitor-exit p0 */
return;
/* .line 307 */
} // .end local p1 # "enable":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private void gameSceneNotification ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 638 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 639 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "gameSceneNotification enable="; // const-string v1, "gameSceneNotification enable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicyQosUtils"; // const-string v1, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v1,v0 );
/* .line 640 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 641 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.phone.intent.action.GAME_SCENE"; // const-string v1, "com.android.phone.intent.action.GAME_SCENE"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 642 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 643 */
final String v1 = "gameEnabled"; // const-string v1, "gameEnabled"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 644 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 645 */
return;
} // .end method
private java.util.Set getEFApps ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 370 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 371 */
/* .local v0, "appList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = com.android.server.net.MiuiNetworkPolicyQosUtils.LOCAL_EF_APP_LIST;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_0 */
/* .line 372 */
/* aget-object v2, v2, v1 */
/* .line 371 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 374 */
} // .end local v1 # "i":I
} // :cond_0
} // .end method
private Integer getUidFromMap ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 404 */
v0 = this.mUidMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* if-nez v0, :cond_0 */
/* .line 405 */
int v0 = -1; // const/4 v0, -0x1
/* .line 407 */
} // :cond_0
v0 = this.mUidMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // .end method
private Boolean hasUidFromMap ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 411 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = com.android.server.net.MiuiNetworkPolicyQosUtils.LOCAL_EF_APP_LIST;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_1 */
/* .line 412 */
/* aget-object v1, v1, v0 */
v1 = /* invoke-direct {p0, v1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->getUidFromMap(Ljava/lang/String;)I */
/* if-ne v1, p1, :cond_0 */
/* .line 413 */
int v1 = 1; // const/4 v1, 0x1
/* .line 411 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 416 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void initDefaultMeetingWhiteList ( ) {
/* .locals 6 */
/* .line 256 */
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.MEETING_OPTIMIZATION_WHITE_LIST_DEFAULT;
java.util.Arrays .asList ( v0 );
/* .line 257 */
/* .local v0, "defaultName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_2
/* if-lez v1, :cond_2 */
/* .line 258 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 259 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
int v2 = 0; // const/4 v2, 0x0
/* .line 260 */
/* .local v2, "flag":Z */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Ljava/lang/String; */
/* .line 261 */
/* .local v4, "str":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 262 */
final String v5 = ","; // const-string v5, ","
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 264 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* .line 266 */
} // :goto_1
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 267 */
} // .end local v4 # "str":Ljava/lang/String;
/* .line 268 */
} // :cond_1
v3 = this.mContext;
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 269 */
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 268 */
final String v5 = "meeting_optimization_white_list_pkg_name"; // const-string v5, "meeting_optimization_white_list_pkg_name"
android.provider.Settings$Global .putString ( v3,v5,v4 );
/* .line 271 */
} // .end local v1 # "sb":Ljava/lang/StringBuilder;
} // .end local v2 # "flag":Z
} // :cond_2
return;
} // .end method
private void initMeetingWhiteList ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 234 */
v0 = v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsPN;
/* if-gtz v0, :cond_0 */
v0 = v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsAN;
/* if-lez v0, :cond_1 */
/* .line 235 */
} // :cond_0
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsPN;
/* .line 236 */
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsAN;
/* .line 238 */
} // :cond_1
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "meeting_optimization_white_list_pkg_name"; // const-string v1, "meeting_optimization_white_list_pkg_name"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 239 */
/* .local v0, "pkgNames":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v0 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 240 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->initDefaultMeetingWhiteList()V */
/* .line 241 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .getString ( v2,v1 );
/* .line 243 */
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "initMeetingWhiteList pkgNames="; // const-string v2, "initMeetingWhiteList pkgNames="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicyQosUtils"; // const-string v2, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v2,v1 );
/* .line 244 */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 245 */
/* .local v1, "meetings":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, v1 */
/* add-int/lit8 v3, v3, -0x1 */
/* if-gt v2, v3, :cond_4 */
/* .line 246 */
/* rem-int/lit8 v3, v2, 0x2 */
/* if-nez v3, :cond_3 */
/* .line 247 */
v3 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsPN;
/* aget-object v4, v1, v2 */
/* .line 249 */
} // :cond_3
v3 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsAN;
/* aget-object v4, v1, v2 */
/* .line 245 */
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 252 */
} // .end local v2 # "i":I
} // :cond_4
return;
} // .end method
private Boolean isActivityNameContains ( java.lang.String p0, java.lang.String[] p1 ) {
/* .locals 2 */
/* .param p1, "anString" # Ljava/lang/String; */
/* .param p2, "sceneString" # [Ljava/lang/String; */
/* .line 211 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* array-length v1, p2 */
/* if-ge v0, v1, :cond_1 */
/* .line 212 */
/* aget-object v1, p2, v0 */
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 213 */
int v1 = 1; // const/4 v1, 0x1
/* .line 211 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 216 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isQosEnabledForUid ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 449 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->hasUidFromMap(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isQosFeatureEnabled ( ) {
/* .locals 4 */
/* .line 588 */
final String v0 = "cepheus"; // const-string v0, "cepheus"
v1 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_1 */
final String v0 = "raphael"; // const-string v0, "raphael"
v3 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = android.os.Build.DEVICE;
/* .line 589 */
final String v3 = "davinci"; // const-string v3, "davinci"
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "crux"; // const-string v0, "crux"
v3 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = android.os.Build.DEVICE;
/* .line 590 */
/* const-string/jumbo v3, "tucana" */
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "cmi"; // const-string v0, "cmi"
v3 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* const-string/jumbo v0, "umi" */
v3 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = android.os.Build.DEVICE;
/* .line 591 */
final String v3 = "picasso"; // const-string v3, "picasso"
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "phoenix"; // const-string v0, "phoenix"
v3 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "lmi"; // const-string v0, "lmi"
v3 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = android.os.Build.DEVICE;
/* .line 592 */
final String v3 = "lmipro"; // const-string v3, "lmipro"
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* const-string/jumbo v0, "vangogh" */
v3 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "cas"; // const-string v0, "cas"
v3 = android.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
v0 = android.os.Build.DEVICE;
/* .line 593 */
final String v3 = "apollo"; // const-string v3, "apollo"
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
/* move v0, v1 */
} // :cond_1
} // :goto_0
/* move v0, v2 */
/* .line 594 */
/* .local v0, "deviceRst":Z */
} // :goto_1
/* sget-boolean v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->IS_QCOM:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* move v3, v2 */
} // :cond_2
/* move v3, v1 */
/* .line 595 */
/* .local v3, "isQosAllowed":Z */
} // :goto_2
/* if-nez v0, :cond_3 */
if ( v3 != null) { // if-eqz v3, :cond_4
} // :cond_3
/* move v1, v2 */
} // :cond_4
} // .end method
private Boolean isQosReadyForEnable ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "action" # Z */
/* .line 350 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-nez v2, :cond_1 */
} // :cond_0
/* if-ne v2, v1, :cond_1 */
} // :goto_0
/* move v0, v1 */
} // :cond_1
} // .end method
private Boolean isQosReadyForSetValue ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "action" # Z */
/* .line 346 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
/* iget v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-ne v2, v1, :cond_1 */
} // :cond_0
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_1 */
} // :goto_0
/* move v0, v1 */
} // :cond_1
} // .end method
private static Boolean isUidValidForQos ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "uid" # I */
/* .line 304 */
v0 = android.os.UserHandle .isApp ( p0 );
} // .end method
private void meetingVoipNotification ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 578 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "meetingVoipNotification enable="; // const-string v1, "meetingVoipNotification enable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicyQosUtils"; // const-string v1, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v1,v0 );
/* .line 579 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 580 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.phone.intent.action.MEETING_VOIP_SCENE"; // const-string v1, "com.android.phone.intent.action.MEETING_VOIP_SCENE"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 581 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 582 */
final String v1 = "meetingVoipEnabled"; // const-string v1, "meetingVoipEnabled"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 583 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 584 */
return;
} // .end method
private void mobileTcEnabledStatusChanged ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 626 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mobileTcEnabledStatusChanged for wechat enable="; // const-string v1, "mobileTcEnabledStatusChanged for wechat enable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicyQosUtils"; // const-string v1, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v1,v0 );
/* .line 627 */
v0 = this.mHandler;
/* .line 629 */
/* nop */
/* .line 627 */
/* const/16 v1, 0xd */
int v2 = 0; // const/4 v2, 0x0
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1, v2 ); // invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 630 */
return;
} // .end method
private void processMeetingListChanged ( ) {
/* .locals 2 */
/* .line 533 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$5; */
/* invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$5;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 540 */
return;
} // .end method
private void registerActivityChangeListener ( ) {
/* .locals 6 */
/* .line 274 */
v0 = this.mActivityChangeListener;
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 275 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 276 */
/* .local v0, "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 277 */
/* .local v1, "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = com.android.server.net.MiuiNetworkPolicyQosUtils.mActivityStatusScene;
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene; */
/* .line 278 */
/* .local v3, "as":Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_1
v5 = this.EF_PN;
/* array-length v5, v5 */
/* if-ge v4, v5, :cond_1 */
/* .line 279 */
v5 = this.EF_PN;
v5 = /* aget-object v5, v5, v4 */
/* if-nez v5, :cond_0 */
/* .line 280 */
v5 = this.EF_PN;
/* aget-object v5, v5, v4 */
/* .line 278 */
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 283 */
} // .end local v4 # "i":I
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
} // :goto_2
v5 = this.EF_AN;
/* array-length v5, v5 */
/* if-ge v4, v5, :cond_3 */
/* .line 284 */
v5 = this.EF_AN;
v5 = /* aget-object v5, v5, v4 */
/* if-nez v5, :cond_2 */
/* .line 285 */
v5 = this.EF_AN;
/* aget-object v5, v5, v4 */
/* .line 283 */
} // :cond_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 288 */
} // .end local v3 # "as":Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;
} // .end local v4 # "i":I
} // :cond_3
/* .line 290 */
} // :cond_4
v2 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsPN;
v3 = } // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Ljava/lang/String; */
/* .line 291 */
v4 = /* .local v3, "meetingAppsPN":Ljava/lang/String; */
/* if-nez v4, :cond_5 */
/* .line 292 */
/* .line 294 */
} // .end local v3 # "meetingAppsPN":Ljava/lang/String;
} // :cond_5
/* .line 295 */
} // :cond_6
final String v2 = "com.tencent.mm.plugin.scanner.ui.BaseScanUI"; // const-string v2, "com.tencent.mm.plugin.scanner.ui.BaseScanUI"
/* .line 296 */
v2 = this.mActivityChangeListener;
miui.process.ProcessManager .unregisterActivityChanageListener ( v2 );
/* .line 297 */
v2 = this.mActivityChangeListener;
miui.process.ProcessManager .registerActivityChangeListener ( v1,v0,v2 );
/* .line 300 */
} // .end local v0 # "targetActivities":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v1 # "targetPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_7
return;
} // .end method
private void removeAll ( ) {
/* .locals 1 */
/* .line 424 */
v0 = this.mUidMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 425 */
return;
} // .end method
private void removeUidFromMap ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 420 */
v0 = this.mUidMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 421 */
return;
} // .end method
private void setDscpStatus ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "dscpStatus" # I */
/* .line 342 */
/* iput p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I */
/* .line 343 */
return;
} // .end method
private synchronized void setQos ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "add" # Z */
/* .param p2, "uid" # I */
/* .param p3, "protocol" # I */
/* monitor-enter p0 */
/* .line 323 */
try { // :try_start_0
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils .isQosFeatureEnabled ( );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v0, :cond_0 */
/* monitor-exit p0 */
return;
/* .line 324 */
} // :cond_0
try { // :try_start_1
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils .isUidValidForQos ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mNetMgrService;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosReadyForSetValue(Z)Z */
/* if-nez v0, :cond_1 */
/* .line 328 */
} // :cond_1
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3; */
/* invoke-direct {v1, p0, p3, p2, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$3;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;IIZ)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 339 */
/* monitor-exit p0 */
return;
/* .line 325 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
} // :cond_2
} // :goto_0
try { // :try_start_2
final String v0 = "MiuiNetworkPolicyQosUtils"; // const-string v0, "MiuiNetworkPolicyQosUtils"
/* const-string/jumbo v1, "setQos return by invalid value!!!" */
android.util.Log .i ( v0,v1 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 326 */
/* monitor-exit p0 */
return;
/* .line 322 */
} // .end local p1 # "add":Z
} // .end local p2 # "uid":I
} // .end local p3 # "protocol":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private void updateDscpStatus ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "action" # Z */
/* .line 354 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 355 */
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I */
/* if-nez v1, :cond_0 */
/* .line 356 */
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V */
/* .line 358 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V */
/* .line 361 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mDscpState:I */
/* if-ne v1, v0, :cond_2 */
/* .line 362 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V */
/* .line 364 */
} // :cond_2
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setDscpStatus(I)V */
/* .line 367 */
} // :goto_0
return;
} // .end method
private void updateQosForUidState ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "state" # I */
/* .line 453 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosEnabledForUid(II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 454 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updateQosForUidState mIsMobileDataOn" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mIsMobileDataOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicyQosUtils"; // const-string v1, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v1,v0 );
/* .line 455 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->gameSceneNotification(Z)V */
/* .line 456 */
/* invoke-direct {p0, v0, p1, v1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setQos(ZII)V */
/* .line 458 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->gameSceneNotification(Z)V */
/* .line 459 */
/* invoke-direct {p0, v0, p1, v1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->setQos(ZII)V */
/* .line 461 */
} // :goto_0
return;
} // .end method
private void updateQosInfo ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .param p2, "uid" # I */
/* .param p3, "protocol" # I */
/* .line 543 */
v0 = this.mQosInfo;
/* if-nez v0, :cond_0 */
return;
/* .line 544 */
} // :cond_0
final String v1 = "MiuiNetworkPolicyQosUtils"; // const-string v1, "MiuiNetworkPolicyQosUtils"
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 545 */
v0 = (( com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ) v0 ).getQosStatus ( ); // invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getQosStatus()Z
/* if-nez v0, :cond_1 */
/* .line 546 */
v0 = this.mQosInfo;
(( com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ) v0 ).updateAll ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->updateAll(ZII)V
/* .line 548 */
} // :cond_1
/* const-string/jumbo v0, "updateQosInfo enabled but already true" */
android.util.Log .i ( v1,v0 );
/* .line 551 */
} // :cond_2
v0 = (( com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ) v0 ).getQosStatus ( ); // invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->getQosStatus()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 552 */
v0 = this.mQosInfo;
(( com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ) v0 ).clear ( ); // invoke-virtual {v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->clear()V
/* .line 554 */
} // :cond_3
/* const-string/jumbo v0, "updateQosInfo disable but already false" */
android.util.Log .i ( v1,v0 );
/* .line 557 */
} // :goto_0
return;
} // .end method
private void updateQosUid ( ) {
/* .locals 10 */
/* .line 378 */
v0 = this.mContext;
/* const-string/jumbo v1, "user" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/UserManager; */
/* .line 379 */
/* .local v0, "um":Landroid/os/UserManager; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 380 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
(( android.os.UserManager ) v0 ).getUsers ( ); // invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;
/* .line 381 */
/* .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;" */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->getEFApps()Ljava/util/Set; */
this.mEFAppsPN = v3;
v3 = /* .line 382 */
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = v3 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsPN;
/* if-nez v3, :cond_4 */
/* .line 383 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->removeAll()V */
/* .line 384 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Landroid/content/pm/UserInfo; */
/* .line 385 */
/* .local v4, "user":Landroid/content/pm/UserInfo; */
int v5 = 0; // const/4 v5, 0x0
/* iget v6, v4, Landroid/content/pm/UserInfo;->id:I */
(( android.content.pm.PackageManager ) v1 ).getInstalledPackagesAsUser ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;
/* .line 386 */
/* .local v5, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_3
/* check-cast v7, Landroid/content/pm/PackageInfo; */
/* .line 387 */
/* .local v7, "app":Landroid/content/pm/PackageInfo; */
v8 = this.packageName;
if ( v8 != null) { // if-eqz v8, :cond_2
v8 = this.applicationInfo;
if ( v8 != null) { // if-eqz v8, :cond_2
v8 = this.mEFAppsPN;
v9 = this.packageName;
v8 = /* .line 388 */
/* if-nez v8, :cond_1 */
v8 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsPN;
v8 = v9 = this.packageName;
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 389 */
} // :cond_1
/* iget v8, v4, Landroid/content/pm/UserInfo;->id:I */
v9 = this.applicationInfo;
/* iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I */
v8 = android.os.UserHandle .getUid ( v8,v9 );
/* .line 390 */
/* .local v8, "uid":I */
v9 = this.packageName;
/* invoke-direct {p0, v9, v8}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->addUidToMap(Ljava/lang/String;I)V */
/* .line 392 */
} // .end local v7 # "app":Landroid/content/pm/PackageInfo;
} // .end local v8 # "uid":I
} // :cond_2
/* .line 393 */
} // .end local v4 # "user":Landroid/content/pm/UserInfo;
} // .end local v5 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_3
/* .line 395 */
} // :cond_4
return;
} // .end method
private synchronized void updateVoipStatus ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "enableStatus" # Z */
/* .param p3, "index" # I */
/* monitor-enter p0 */
/* .line 599 */
try { // :try_start_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_2 */
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsAN;
/* check-cast v0, Ljava/lang/CharSequence; */
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 600 */
v0 = com.android.server.net.MiuiNetworkPolicyManagerService .isMobileTcFeatureAllowed ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 601 */
/* invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->mobileTcEnabledStatusChanged(Z)V */
/* .line 603 */
} // .end local p0 # "this":Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
} // :cond_0
final String v0 = "com.tencent.mm.plugin.voip.ui.VideoActivity"; // const-string v0, "com.tencent.mm.plugin.voip.ui.VideoActivity"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 604 */
/* invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->weChatVoipNotification(Z)V */
/* .line 606 */
} // :cond_1
/* invoke-direct {p0, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->meetingVoipNotification(Z)V */
/* .line 609 */
} // :cond_2
} // :goto_0
miui.telephony.TelephonyManagerEx .getDefault ( );
v0 = (( miui.telephony.TelephonyManagerEx ) v0 ).isPlatform8550 ( ); // invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->isPlatform8550()Z
/* if-nez v0, :cond_3 */
/* .line 610 */
miui.telephony.TelephonyManagerEx .getDefault ( );
v0 = (( miui.telephony.TelephonyManagerEx ) v0 ).isPlatform8650 ( ); // invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->isPlatform8650()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 611 */
} // :cond_3
final String v0 = "MiuiNetworkPolicyQosUtils"; // const-string v0, "MiuiNetworkPolicyQosUtils"
final String v1 = "8550 platform DE3.0"; // const-string v1, "8550 platform DE3.0"
android.util.Log .d ( v0,v1 );
/* .line 612 */
if ( p2 != null) { // if-eqz p2, :cond_4
int v0 = 3; // const/4 v0, 0x3
} // :cond_4
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* invoke-direct {p0, p2, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableLLMMode(ZI)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 614 */
} // :cond_5
/* monitor-exit p0 */
return;
/* .line 598 */
} // .end local p1 # "name":Ljava/lang/String;
} // .end local p2 # "enableStatus":Z
} // .end local p3 # "index":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
private void weChatVoipNotification ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "enable" # Z */
/* .line 566 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "weChatVoipNotification enable=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicyQosUtils"; // const-string v1, "MiuiNetworkPolicyQosUtils"
android.util.Log .i ( v1,v0 );
/* .line 567 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 568 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.phone.intent.action.WECHAT_VOIP_SCENE"; // const-string v1, "com.android.phone.intent.action.WECHAT_VOIP_SCENE"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 569 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 570 */
/* const-string/jumbo v1, "wechatVoipEnabled" */
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 571 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 572 */
return;
} // .end method
/* # virtual methods */
public void systemReady ( com.android.server.net.MiuiNetworkManagementService p0 ) {
/* .locals 5 */
/* .param p1, "networkMgr" # Lcom/android/server/net/MiuiNetworkManagementService; */
/* .line 220 */
this.mNetMgrService = p1;
/* .line 221 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->initMeetingWhiteList(Landroid/content/Context;)V */
/* .line 222 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableQos(Z)V */
/* .line 223 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->registerActivityChangeListener()V */
/* .line 224 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableQos(Z)V */
/* .line 225 */
/* invoke-direct {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosUid()V */
/* .line 226 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 227 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 228 */
final String v1 = "com.android.phone.intent.action.CLOUD_MEETING_LIST_DONE"; // const-string v1, "com.android.phone.intent.action.CLOUD_MEETING_LIST_DONE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 229 */
v1 = this.mContext;
v2 = this.mIntentReceiver;
int v3 = 0; // const/4 v3, 0x0
v4 = this.mHandler;
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3, v4 ); // invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
/* .line 230 */
/* new-instance v1, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo; */
/* invoke-direct {v1, p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;-><init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V */
this.mQosInfo = v1;
/* .line 231 */
return;
} // .end method
public void updateAppPN ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "action" # Z */
/* .line 428 */
v0 = this.mEFAppsPN;
v0 = if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez v0, :cond_1 */
} // :cond_0
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils.mMeetingAppsPN;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = /* .line 429 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 430 */
} // :cond_1
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 431 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->addUidToMap(Ljava/lang/String;I)V */
/* .line 433 */
} // :cond_2
/* invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->removeUidFromMap(Ljava/lang/String;)V */
/* .line 436 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void updateQosForUidStateChange ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "oldUidState" # I */
/* .param p3, "newUidState" # I */
/* .line 439 */
v0 = com.android.server.net.MiuiNetworkPolicyQosUtils .isUidValidForQos ( p1 );
/* if-nez v0, :cond_0 */
/* .line 440 */
return;
/* .line 442 */
} // :cond_0
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosEnabledForUid(II)Z */
/* .line 443 */
v1 = /* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->isQosEnabledForUid(II)Z */
/* if-eq v0, v1, :cond_1 */
/* .line 444 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->updateQosForUidState(II)V */
/* .line 446 */
} // :cond_1
return;
} // .end method
