class com.android.server.net.MiuiNetworkPolicyManagerService$20 extends android.database.ContentObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerLowLatencyAppsChangedObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$20 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 1439 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 11 */
/* .param p1, "selfChange" # Z */
/* .line 1442 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v0 );
/* const-string/jumbo v1, "user" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/UserManager; */
/* .line 1443 */
/* .local v0, "um":Landroid/os/UserManager; */
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 1444 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
(( android.os.UserManager ) v0 ).getUsers ( ); // invoke-virtual {v0}, Landroid/os/UserManager;->getUsers()Ljava/util/List;
/* .line 1445 */
/* .local v2, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;" */
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmContext ( v3 );
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mgetLowLatencyApps ( v3,v4 );
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmLowLatencyAppsPN ( v3,v4 );
/* .line 1446 */
v3 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLowLatencyApps ( v3 );
/* .line 1447 */
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLowLatencyAppsPN ( v3 );
/* if-nez v3, :cond_2 */
/* .line 1448 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Landroid/content/pm/UserInfo; */
/* .line 1449 */
/* .local v4, "user":Landroid/content/pm/UserInfo; */
int v5 = 0; // const/4 v5, 0x0
/* iget v6, v4, Landroid/content/pm/UserInfo;->id:I */
(( android.content.pm.PackageManager ) v1 ).getInstalledPackagesAsUser ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;
/* .line 1450 */
/* .local v5, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_1
/* check-cast v7, Landroid/content/pm/PackageInfo; */
/* .line 1451 */
/* .local v7, "app":Landroid/content/pm/PackageInfo; */
v8 = this.packageName;
if ( v8 != null) { // if-eqz v8, :cond_0
	 v8 = this.applicationInfo;
	 if ( v8 != null) { // if-eqz v8, :cond_0
		 v8 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLowLatencyAppsPN ( v8 );
		 v9 = this.packageName;
		 v8 = 		 /* .line 1453 */
		 if ( v8 != null) { // if-eqz v8, :cond_0
			 /* .line 1454 */
			 /* iget v8, v4, Landroid/content/pm/UserInfo;->id:I */
			 v9 = this.applicationInfo;
			 /* iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I */
			 v8 = 			 android.os.UserHandle .getUid ( v8,v9 );
			 /* .line 1455 */
			 /* .local v8, "uid":I */
			 v9 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmLowLatencyApps ( v9 );
			 java.lang.Integer .valueOf ( v8 );
			 /* .line 1457 */
		 } // .end local v7 # "app":Landroid/content/pm/PackageInfo;
	 } // .end local v8 # "uid":I
} // :cond_0
/* .line 1458 */
} // .end local v4 # "user":Landroid/content/pm/UserInfo;
} // .end local v5 # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
} // :cond_1
/* .line 1460 */
} // :cond_2
return;
} // .end method
