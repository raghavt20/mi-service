class com.android.server.net.MiuiNetworkPolicyManagerService$25 implements com.android.server.net.MiuiNetworkManagementService$NetworkEventObserver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$25 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .line 1646 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void uidDataActivityChanged ( java.lang.String p0, Integer p1, Boolean p2, Long p3 ) {
/* .locals 3 */
/* .param p1, "label" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "active" # Z */
/* .param p4, "tsNanos" # J */
/* .line 1649 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "label "; // const-string v1, "label "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", uid "; // const-string v1, ", uid "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", active "; // const-string v1, ", active "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", tsNanos "; // const-string v1, ", tsNanos "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4, p5 ); // invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiNetworkPolicy"; // const-string v1, "MiuiNetworkPolicy"
android.util.Log .i ( v1,v0 );
/* .line 1650 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmHandler ( v1 );
/* .line 1651 */
/* nop */
/* .line 1650 */
int v2 = 3; // const/4 v2, 0x3
(( android.os.Handler ) v1 ).obtainMessage ( v2, p2, p3, p1 ); // invoke-virtual {v1, v2, p2, p3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1652 */
return;
} // .end method
