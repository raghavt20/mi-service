.class Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyQosUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->enableQos(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

.field final synthetic val$enable:Z


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 312
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->val$enable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 315
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmNetMgrService(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->val$enable:Z

    invoke-virtual {v0, v1}, Lcom/android/server/net/MiuiNetworkManagementService;->enableQos(Z)Z

    move-result v0

    .line 316
    .local v0, "rst":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableQos rst="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiNetworkPolicyQosUtils"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$2;->val$enable:Z

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$mupdateDscpStatus(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Z)V

    .line 318
    :cond_0
    return-void
.end method
