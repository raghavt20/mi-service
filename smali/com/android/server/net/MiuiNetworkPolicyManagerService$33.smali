.class Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;
.super Landroid/database/ContentObserver;
.source "MiuiNetworkPolicyManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyManagerService;->registerSleepModeEnableControlObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 2144
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 2147
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$msleepModeEnableControl(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v0

    const-string v1, "MiuiNetworkPolicySleepMode"

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2148
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mregisterSleepModeReceiver(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 2149
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmSleepModeEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 2150
    const-string v0, "Sleep mode receiver register"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2151
    :cond_0
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2152
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$munregisterSleepModeReceiver(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 2153
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmSleepModeEnable(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 2154
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2155
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fputmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 2156
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$mclearSleepModeWhitelistUidRules(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)V

    .line 2157
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyManagerService$33;->this$0:Lcom/android/server/net/MiuiNetworkPolicyManagerService;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$fgetmSleepModeEnter(Lcom/android/server/net/MiuiNetworkPolicyManagerService;)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/android/server/net/MiuiNetworkPolicyManagerService;->-$$Nest$menableSleepModeChain(Lcom/android/server/net/MiuiNetworkPolicyManagerService;Z)V

    .line 2159
    :cond_1
    const-string v0, "Sleep mode receiver unregister"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2161
    :cond_2
    :goto_0
    return-void
.end method
