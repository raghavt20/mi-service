.class Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;
.super Ljava/lang/Object;
.source "MiuiNetworkPolicyTrafficLimit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->setMobileTrafficLimit(ZJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

.field final synthetic val$enabled:Z

.field final synthetic val$rate:J


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;ZJ)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 335
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$enabled:Z

    iput-wide p3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$rate:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 338
    iget-object v0, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    invoke-static {v0}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fgetmNetMgrService(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;)Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$enabled:Z

    iget-wide v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$rate:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->setMobileTrafficLimit(ZJ)Z

    move-result v0

    .line 339
    .local v0, "rst":Z
    if-eqz v0, :cond_1

    .line 340
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$enabled:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    goto :goto_0

    :cond_0
    const/4 v2, 0x2

    :goto_0
    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mupdateTrafficLimitStatus(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;I)V

    .line 342
    :cond_1
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$fputmProcessSetLimit(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Z)V

    .line 343
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->this$0:Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setMobileTrafficLimit rst="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->-$$Nest$mlog(Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;Ljava/lang/String;)V

    .line 344
    return-void
.end method
