.class public Lcom/android/server/net/NetworkManagermentServiceImpl;
.super Ljava/lang/Object;
.source "NetworkManagermentServiceImpl.java"

# interfaces
.implements Lcom/android/server/net/NetworkManagementServiceStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addMiuiFirewallSharedUid(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 46
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManagementService;->getInstance()Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->addMiuiFirewallSharedUid(I)Z

    move-result v0

    return v0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 36
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManagementService;->getInstance()Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->dump(Ljava/io/PrintWriter;)V

    .line 37
    return-void
.end method

.method public getMiuiNetworkManager()Landroid/os/IBinder;
    .locals 1

    .line 16
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManager;->get()Lcom/android/server/net/MiuiNetworkManager;

    move-result-object v0

    return-object v0
.end method

.method public setCurrentNetworkState(I)Z
    .locals 1
    .param p1, "state"    # I

    .line 51
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManagementService;->getInstance()Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/net/MiuiNetworkManagementService;->setCurrentNetworkState(I)Z

    move-result v0

    return v0
.end method

.method public setMiuiFirewallRule(Ljava/lang/String;III)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "rule"    # I
    .param p4, "type"    # I

    .line 26
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManagementService;->getInstance()Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/net/MiuiNetworkManagementService;->setMiuiFirewallRule(Ljava/lang/String;III)Z

    move-result v0

    return v0
.end method

.method public setPidForPackage(Ljava/lang/String;II)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "pid"    # I
    .param p3, "uid"    # I

    .line 21
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManagementService;->getInstance()Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/net/MiuiNetworkManagementService;->setPidForPackage(Ljava/lang/String;II)V

    .line 22
    return-void
.end method

.method public showLogin(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "ssid"    # Ljava/lang/String;

    .line 32
    return-void
.end method

.method public updateAurogonUidRule(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "allow"    # Z

    .line 41
    invoke-static {}, Lcom/android/server/net/MiuiNetworkManagementService;->getInstance()Lcom/android/server/net/MiuiNetworkManagementService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/net/MiuiNetworkManagementService;->updateAurogonUidRule(IZ)V

    .line 42
    return-void
.end method
