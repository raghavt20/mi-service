.class Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;
.super Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;
.source "MiuiNetworkManagementService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkManagementService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OemNetdUnsolicitedEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkManagementService;


# direct methods
.method private constructor <init>(Lcom/android/server/net/MiuiNetworkManagementService;)V
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;->this$0:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-direct {p0}, Lcom/android/internal/net/IOemNetdUnsolicitedEventListener$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/net/MiuiNetworkManagementService;Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;-><init>(Lcom/android/server/net/MiuiNetworkManagementService;)V

    return-void
.end method


# virtual methods
.method public onFirewallBlocked(ILjava/lang/String;)V
    .locals 4
    .param p1, "code"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 365
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0, p2}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "code=%d, pkg=%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkManagement"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    const/16 v0, 0x2bb

    if-ne v0, p1, :cond_0

    if-eqz p2, :cond_0

    .line 367
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.FIREWALL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 368
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    const-string v1, "pkg"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkManagementService$OemNetdUnsolicitedEventListener;->this$0:Lcom/android/server/net/MiuiNetworkManagementService;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkManagementService;->-$$Nest$fgetmContext(Lcom/android/server/net/MiuiNetworkManagementService;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v3, "com.miui.permission.FIREWALL"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 372
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onRegistered()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 360
    const-string v0, "NetworkManagement"

    const-string v1, "onRegistered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    return-void
.end method

.method public onUnreachedPort(IILjava/lang/String;)V
    .locals 0
    .param p1, "port"    # I
    .param p2, "ip"    # I
    .param p3, "interfaceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 376
    return-void
.end method
