class com.android.server.net.MiuiNetworkPolicyManagerService$32 extends android.content.BroadcastReceiver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$32 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .line 2097 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 2100 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 2101 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "com.miui.powerkeeper_sleep_changed"; // const-string v1, "com.miui.powerkeeper_sleep_changed"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
final String v3 = "MiuiNetworkPolicySleepMode"; // const-string v3, "MiuiNetworkPolicySleepMode"
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 2102 */
	 /* const-string/jumbo v1, "state" */
	 int v4 = -1; // const/4 v4, -0x1
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v4 ); // invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 2103 */
	 /* .local v1, "state":I */
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "broadcastReceiver state is "; // const-string v5, "broadcastReceiver state is "
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Log .d ( v3,v4 );
	 /* .line 2104 */
	 int v3 = 1; // const/4 v3, 0x1
	 /* if-ne v1, v3, :cond_0 */
	 v4 = this.this$0;
	 v4 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnter ( v4 );
	 /* if-nez v4, :cond_0 */
	 v4 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeWhitelistUids ( v4 );
	 v4 = 	 /* .line 2105 */
	 /* if-nez v4, :cond_0 */
	 /* .line 2106 */
	 v2 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmSleepModeEnter ( v2,v3 );
	 /* .line 2107 */
	 v2 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$msetSleepModeWhitelistUidRules ( v2 );
	 /* .line 2108 */
	 v2 = this.this$0;
	 v3 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnter ( v2 );
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableSleepModeChain ( v2,v3 );
	 /* .line 2109 */
} // :cond_0
/* if-eq v1, v3, :cond_2 */
v3 = this.this$0;
v3 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnter ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
	 /* .line 2110 */
	 v3 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmSleepModeEnter ( v3,v2 );
	 /* .line 2111 */
	 v2 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mclearSleepModeWhitelistUidRules ( v2 );
	 /* .line 2112 */
	 v2 = this.this$0;
	 v3 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnter ( v2 );
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableSleepModeChain ( v2,v3 );
	 /* .line 2114 */
} // .end local v1 # "state":I
} // :cond_1
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 2115 */
final String v1 = "ACTION_SCREEN_ON"; // const-string v1, "ACTION_SCREEN_ON"
android.util.Log .d ( v3,v1 );
/* .line 2116 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnter ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_3
	 /* .line 2117 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmSleepModeEnter ( v1,v2 );
	 /* .line 2118 */
	 v1 = this.this$0;
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mclearSleepModeWhitelistUidRules ( v1 );
	 /* .line 2119 */
	 v1 = this.this$0;
	 v2 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSleepModeEnter ( v1 );
	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableSleepModeChain ( v1,v2 );
	 /* .line 2114 */
} // :cond_2
} // :goto_0
/* nop */
/* .line 2122 */
} // :cond_3
} // :goto_1
return;
} // .end method
