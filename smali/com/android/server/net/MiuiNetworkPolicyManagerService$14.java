class com.android.server.net.MiuiNetworkPolicyManagerService$14 extends android.content.BroadcastReceiver {
	 /* .source "MiuiNetworkPolicyManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyManagerService$14 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyManagerService; */
/* .line 1143 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1146 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1147 */
/* .local v0, "action":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mWifiStateReceiver onReceive: "; // const-string v2, "mWifiStateReceiver onReceive: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiNetworkPolicy"; // const-string v2, "MiuiNetworkPolicy"
android.util.Log .i ( v2,v1 );
/* .line 1148 */
final String v1 = "android.net.wifi.STATE_CHANGE"; // const-string v1, "android.net.wifi.STATE_CHANGE"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_3
	 /* .line 1149 */
	 final String v1 = "networkInfo"; // const-string v1, "networkInfo"
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v1, Landroid/net/NetworkInfo; */
	 /* .line 1150 */
	 /* .local v1, "netInfo":Landroid/net/NetworkInfo; */
	 v4 = this.this$0;
	 v4 = 	 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v4 );
	 /* .line 1151 */
	 /* .local v4, "wasConnected":Z */
	 v5 = this.this$0;
	 int v6 = 1; // const/4 v6, 0x1
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 v7 = 		 (( android.net.NetworkInfo ) v1 ).isConnected ( ); // invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z
		 if ( v7 != null) { // if-eqz v7, :cond_0
			 /* move v3, v6 */
		 } // :cond_0
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmWifiConnected ( v5,v3 );
		 /* .line 1152 */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v5, "wasConnected = " */
		 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v5 = " mWifiConnected = "; // const-string v5, " mWifiConnected = "
		 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v5 = this.this$0;
		 v5 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v5 );
		 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v5 = " mNetworkPriorityMode ="; // const-string v5, " mNetworkPriorityMode ="
		 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v5 = this.this$0;
		 v5 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmNetworkPriorityMode ( v5 );
		 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .i ( v2,v3 );
		 /* .line 1154 */
		 v3 = this.this$0;
		 v3 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v3 );
		 /* if-eq v3, v4, :cond_4 */
		 /* .line 1155 */
		 v3 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menablePowerSave ( v3,v6 );
		 /* .line 1156 */
		 v3 = this.this$0;
		 v3 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v3 );
		 if ( v3 != null) { // if-eqz v3, :cond_1
			 /* .line 1157 */
			 v3 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmSupport ( v3 );
			 v6 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmInterfaceName ( v6 );
			 (( com.android.server.net.MiuiNetworkPolicyServiceSupport ) v5 ).updateIface ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/server/net/MiuiNetworkPolicyServiceSupport;->updateIface(Ljava/lang/String;)Ljava/lang/String;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmInterfaceName ( v3,v5 );
			 /* .line 1158 */
			 v3 = this.this$0;
			 v5 = 			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mcheckRouterMTK ( v3 );
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmIsMtkRouter ( v3,v5 );
			 /* .line 1159 */
			 v3 = this.this$0;
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mgetRouterModel ( v3 );
			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmConnectedApModel ( v3,v5 );
			 /* .line 1160 */
			 /* new-instance v3, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v5 = "router is mtk: "; // const-string v5, "router is mtk: "
			 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v5 = this.this$0;
			 v5 = 			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmIsMtkRouter ( v5 );
			 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .d ( v2,v3 );
			 /* .line 1162 */
		 } // :cond_1
		 v2 = this.this$0;
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableWmmer ( v2 );
		 /* .line 1163 */
		 v2 = this.this$0;
		 v2 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mnetworkPriorityMode ( v2 );
		 /* .line 1164 */
		 /* .local v2, "networkPriority":I */
		 v3 = this.this$0;
		 v3 = 		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$misLimitterEnabled ( v3,v2 );
		 if ( v3 != null) { // if-eqz v3, :cond_4
			 /* .line 1165 */
			 v3 = this.this$0;
			 v5 = 			 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWifiConnected ( v3 );
			 if ( v5 != null) { // if-eqz v5, :cond_2
				 /* move v5, v2 */
			 } // :cond_2
			 /* const/16 v5, 0xff */
		 } // :goto_0
		 com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$menableNetworkPriority ( v3,v5 );
		 /* .line 1168 */
	 } // .end local v1 # "netInfo":Landroid/net/NetworkInfo;
} // .end local v2 # "networkPriority":I
} // .end local v4 # "wasConnected":Z
} // :cond_3
final String v1 = "android.net.conn.NETWORK_CONDITIONS_MEASURED"; // const-string v1, "android.net.conn.NETWORK_CONDITIONS_MEASURED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1169 */
v1 = this.this$0;
v1 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmIsCaptivePortal ( v1 );
/* .line 1170 */
/* .local v1, "wasCaptivePortal":Z */
v4 = this.this$0;
final String v5 = "extra_is_captive_portal"; // const-string v5, "extra_is_captive_portal"
v3 = (( android.content.Intent ) p2 ).getBooleanExtra ( v5, v3 ); // invoke-virtual {p2, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fputmIsCaptivePortal ( v4,v3 );
/* .line 1171 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "network was: "; // const-string v4, "network was: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " captive portal, and now is "; // const-string v4, " captive portal, and now is "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
v4 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmIsCaptivePortal ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v2,v3 );
/* .line 1172 */
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmIsCaptivePortal ( v2 );
/* if-eq v1, v2, :cond_5 */
v2 = this.this$0;
v2 = com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$fgetmWmmerEnable ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 1173 */
v2 = this.this$0;
com.android.server.net.MiuiNetworkPolicyManagerService .-$$Nest$mupdateRuleGlobal ( v2 );
/* .line 1168 */
} // .end local v1 # "wasCaptivePortal":Z
} // :cond_4
} // :goto_1
/* nop */
/* .line 1176 */
} // :cond_5
} // :goto_2
return;
} // .end method
