.class Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;
.super Lmiui/process/IActivityChangeListener$Stub;
.source "MiuiNetworkPolicyQosUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;


# direct methods
.method constructor <init>(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    .line 161
    iput-object p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-direct {p0}, Lmiui/process/IActivityChangeListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityChanged(Landroid/content/ComponentName;Landroid/content/ComponentName;)V
    .locals 11
    .param p1, "preName"    # Landroid/content/ComponentName;
    .param p2, "curName"    # Landroid/content/ComponentName;

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onActivityChanged pre:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",cur:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v1}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiNetworkPolicyQosUtils"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    if-eqz p1, :cond_a

    if-nez p2, :cond_0

    goto/16 :goto_5

    .line 166
    :cond_0
    invoke-virtual {p2}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "cur":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    .line 168
    .local v2, "pre":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$sfgetmMeetingAppsAN()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 169
    .local v3, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ge v4, v3, :cond_3

    .line 170
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$sfgetmMeetingAppsAN()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v7}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 171
    const-string v5, "onActivityChanged setQos true"

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v5, v0, v6, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$mupdateVoipStatus(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;ZI)V

    .line 173
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$sfgetmMeetingAppsPN()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v5, v7}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$mgetUidFromMap(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;)I

    move-result v7

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$sfgetUDP()I

    move-result v8

    invoke-static {v5, v6, v7, v8}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$msetQos(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZII)V

    goto :goto_1

    .line 174
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$sfgetmMeetingAppsAN()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 175
    const-string v6, "onActivityChanged setQos false"

    invoke-static {v1, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v6, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v6, v2, v5, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$mupdateVoipStatus(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;ZI)V

    .line 177
    iget-object v6, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$sfgetmMeetingAppsPN()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$mgetUidFromMap(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;)I

    move-result v7

    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$sfgetUDP()I

    move-result v8

    invoke-static {v6, v5, v7, v8}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$msetQos(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZII)V

    .line 169
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 180
    .end local v4    # "i":I
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const-string v7, "com.tencent.mm.plugin.scanner.ui.BaseScanUI"

    if-nez v4, :cond_4

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 182
    const-string v4, "onActivityChanged enter ScanQR"

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v1, v6, v6}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$menableLLMMode(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZI)V

    goto :goto_2

    .line 185
    :cond_4
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 186
    const-string v4, "onActivityChanged exit ScanQR"

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    const/4 v4, -0x1

    invoke-static {v1, v5, v4}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$menableLLMMode(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;ZI)V

    .line 191
    :cond_5
    :goto_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 192
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "com.android.phone.intent.action.ACTIVITY_STATUS"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    const-string v4, "com.android.phone"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const/4 v4, 0x0

    .line 195
    .local v4, "changed":Z
    invoke-static {}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$sfgetmActivityStatusScene()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;

    .line 196
    .local v8, "as":Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    iget-object v9, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    iget-object v10, v8, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->EF_AN:[Ljava/lang/String;

    invoke-static {v9, v0, v10}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$misActivityNameContains(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v9}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmIsMobileDataOn(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 197
    iget-object v9, v8, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->sceneStatusExtra:Ljava/lang/String;

    invoke-virtual {v1, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 198
    const/4 v4, 0x1

    goto :goto_4

    .line 199
    :cond_6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    iget-object v9, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    iget-object v10, v8, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->EF_AN:[Ljava/lang/String;

    invoke-static {v9, v2, v10}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$misActivityNameContains(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 200
    iget-object v9, v8, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;->sceneStatusExtra:Ljava/lang/String;

    invoke-virtual {v1, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 201
    const/4 v4, 0x1

    .line 203
    .end local v8    # "as":Lcom/android/server/net/MiuiNetworkPolicyQosUtils$ActivityStatusScene;
    :cond_7
    :goto_4
    goto :goto_3

    .line 204
    :cond_8
    if-eqz v4, :cond_9

    .line 205
    iget-object v5, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$1;->this$0:Lcom/android/server/net/MiuiNetworkPolicyQosUtils;

    invoke-static {v5}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils;->-$$Nest$fgetmContext(Lcom/android/server/net/MiuiNetworkPolicyQosUtils;)Landroid/content/Context;

    move-result-object v5

    sget-object v6, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v5, v1, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 207
    :cond_9
    return-void

    .line 165
    .end local v0    # "cur":Ljava/lang/String;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pre":Ljava/lang/String;
    .end local v3    # "size":I
    .end local v4    # "changed":Z
    :cond_a
    :goto_5
    return-void
.end method
