class com.android.server.net.MiuiNetworkPolicyTrafficLimit$7 implements java.lang.Runnable {
	 /* .source "MiuiNetworkPolicyTrafficLimit.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit;->setMobileTrafficLimit(ZJ)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.net.MiuiNetworkPolicyTrafficLimit this$0; //synthetic
final Boolean val$enabled; //synthetic
final Long val$rate; //synthetic
/* # direct methods */
 com.android.server.net.MiuiNetworkPolicyTrafficLimit$7 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 335 */
this.this$0 = p1;
/* iput-boolean p2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$enabled:Z */
/* iput-wide p3, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$rate:J */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 338 */
v0 = this.this$0;
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fgetmNetMgrService ( v0 );
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$enabled:Z */
/* iget-wide v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$rate:J */
v0 = (( com.android.server.net.MiuiNetworkManagementService ) v0 ).setMobileTrafficLimit ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/net/MiuiNetworkManagementService;->setMobileTrafficLimit(ZJ)Z
/* .line 339 */
/* .local v0, "rst":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 340 */
v1 = this.this$0;
/* iget-boolean v2, p0, Lcom/android/server/net/MiuiNetworkPolicyTrafficLimit$7;->val$enabled:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 int v2 = 3; // const/4 v2, 0x3
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
} // :goto_0
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mupdateTrafficLimitStatus ( v1,v2 );
/* .line 342 */
} // :cond_1
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$fputmProcessSetLimit ( v1,v2 );
/* .line 343 */
v1 = this.this$0;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setMobileTrafficLimit rst=" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.net.MiuiNetworkPolicyTrafficLimit .-$$Nest$mlog ( v1,v2 );
/* .line 344 */
return;
} // .end method
