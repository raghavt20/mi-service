class com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo {
	 /* .source "MiuiNetworkPolicyQosUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/net/MiuiNetworkPolicyQosUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "QosInfo" */
} // .end annotation
/* # instance fields */
private Boolean enabled;
private Integer protocol;
final com.android.server.net.MiuiNetworkPolicyQosUtils this$0; //synthetic
private Integer uid;
/* # direct methods */
public com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ( ) {
/* .locals 0 */
/* .line 469 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 470 */
(( com.android.server.net.MiuiNetworkPolicyQosUtils$QosInfo ) p0 ).clear ( ); // invoke-virtual {p0}, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->clear()V
/* .line 471 */
return;
} // .end method
/* # virtual methods */
public void clear ( ) {
/* .locals 1 */
/* .line 492 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->enabled:Z */
/* .line 493 */
/* iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->uid:I */
/* .line 494 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->protocol:I */
/* .line 495 */
return;
} // .end method
public Integer getProtocol ( ) {
/* .locals 1 */
/* .line 488 */
/* iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->protocol:I */
} // .end method
public Boolean getQosStatus ( ) {
/* .locals 1 */
/* .line 480 */
/* iget-boolean v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->enabled:Z */
} // .end method
public Integer getUid ( ) {
/* .locals 1 */
/* .line 484 */
/* iget v0, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->uid:I */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 499 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enabled="; // const-string v1, "enabled="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->enabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " uid="; // const-string v1, " uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->uid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " protocol="; // const-string v1, " protocol="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->protocol:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public void updateAll ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 0 */
/* .param p1, "enabled" # Z */
/* .param p2, "uid" # I */
/* .param p3, "protocol" # I */
/* .line 474 */
/* iput-boolean p1, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->enabled:Z */
/* .line 475 */
/* iput p2, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->uid:I */
/* .line 476 */
/* iput p3, p0, Lcom/android/server/net/MiuiNetworkPolicyQosUtils$QosInfo;->protocol:I */
/* .line 477 */
return;
} // .end method
