.class Lcom/android/server/DarkModeStatusTracker$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "DarkModeStatusTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DarkModeStatusTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/DarkModeStatusTracker;


# direct methods
.method public constructor <init>(Lcom/android/server/DarkModeStatusTracker;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/DarkModeStatusTracker;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 323
    iput-object p1, p0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;->this$0:Lcom/android/server/DarkModeStatusTracker;

    .line 324
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 325
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 329
    if-nez p2, :cond_0

    .line 330
    return-void

    .line 332
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "uri = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DarkModeStatusTracker"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    sparse-switch v1, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v1, "last_app_dark_mode_pkg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_1
    const-string v1, "dark_mode_time_type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_2
    const-string v1, "dark_mode_enable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_1

    :sswitch_3
    const-string v1, "dark_mode_contrast_enable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "dark_mode_time_enable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const-string v1, "577.2.0.1.23091"

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 354
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;->this$0:Lcom/android/server/DarkModeStatusTracker;

    const-string v1, "577.3.3.1.23094"

    invoke-static {v0, v1}, Lcom/android/server/DarkModeStatusTracker;->-$$Nest$muploadDarkModeStatusEvent(Lcom/android/server/DarkModeStatusTracker;Ljava/lang/String;)V

    goto :goto_2

    .line 351
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;->this$0:Lcom/android/server/DarkModeStatusTracker;

    const-string v1, "577.3.2.1.23093"

    invoke-static {v0, v1}, Lcom/android/server/DarkModeStatusTracker;->-$$Nest$muploadDarkModeStatusEvent(Lcom/android/server/DarkModeStatusTracker;Ljava/lang/String;)V

    .line 352
    goto :goto_2

    .line 348
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;->this$0:Lcom/android/server/DarkModeStatusTracker;

    invoke-static {v0, v1}, Lcom/android/server/DarkModeStatusTracker;->-$$Nest$muploadDarkModeStatusEvent(Lcom/android/server/DarkModeStatusTracker;Ljava/lang/String;)V

    .line 349
    goto :goto_2

    .line 345
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;->this$0:Lcom/android/server/DarkModeStatusTracker;

    invoke-static {v0, v1}, Lcom/android/server/DarkModeStatusTracker;->-$$Nest$muploadDarkModeStatusEvent(Lcom/android/server/DarkModeStatusTracker;Ljava/lang/String;)V

    .line 346
    goto :goto_2

    .line 337
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;->this$0:Lcom/android/server/DarkModeStatusTracker;

    invoke-static {v0}, Lcom/android/server/DarkModeStatusTracker;->-$$Nest$fgetmContext(Lcom/android/server/DarkModeStatusTracker;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dark_mode_enable_by_setting"

    invoke-static {v0, v1, v2, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_2

    .line 339
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;->this$0:Lcom/android/server/DarkModeStatusTracker;

    const-string v1, "577.5.0.1.23096"

    invoke-static {v0, v1}, Lcom/android/server/DarkModeStatusTracker;->-$$Nest$muploadDarkModeStatusEvent(Lcom/android/server/DarkModeStatusTracker;Ljava/lang/String;)V

    goto :goto_2

    .line 341
    :cond_2
    iget-object v0, p0, Lcom/android/server/DarkModeStatusTracker$SettingsObserver;->this$0:Lcom/android/server/DarkModeStatusTracker;

    const-string v1, "577.4.0.1.23106"

    invoke-static {v0, v1}, Lcom/android/server/DarkModeStatusTracker;->-$$Nest$muploadDarkModeStatusEvent(Lcom/android/server/DarkModeStatusTracker;Ljava/lang/String;)V

    .line 343
    nop

    .line 358
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x56ccb7be -> :sswitch_4
        -0x4b9a7c13 -> :sswitch_3
        -0x2a9ed7aa -> :sswitch_2
        0x5d2c55b9 -> :sswitch_1
        0x776698d2 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
