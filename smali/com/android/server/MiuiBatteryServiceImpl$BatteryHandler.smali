.class Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;
.super Landroid/os/Handler;
.source "MiuiBatteryServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BatteryHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;
    }
.end annotation


# static fields
.field private static final ACTION_HANDLE_BATTERY_STATE_CHANGED:Ljava/lang/String; = "miui.intent.action.ACTION_HANDLE_BATTERY_STATE_CHANGED"

.field private static final ACTION_HANDLE_STATE_CHANGED:Ljava/lang/String; = "miui.intent.action.ACTION_HANDLE_STATE_CHANGED"

.field private static final ACTION_MIUI_PC_BATTERY_CHANGED:Ljava/lang/String; = "miui.intent.action.MIUI_PC_BATTERY_CHANGED"

.field private static final ACTION_MOISTURE_DET:Ljava/lang/String; = "miui.intent.action.ACTION_MOISTURE_DET"

.field private static final ACTION_POGO_CONNECTED_STATE:Ljava/lang/String; = "miui.intent.action.ACTION_POGO_CONNECTED_STATE"

.field private static final ACTION_REVERSE_PEN_CHARGE_STATE:Ljava/lang/String; = "miui.intent.action.ACTION_PEN_REVERSE_CHARGE_STATE"

.field private static final ACTION_RX_OFFSET:Ljava/lang/String; = "miui.intent.action.ACTION_RX_OFFSET"

.field private static final ACTION_TYPE_C_HIGH_TEMP:Ljava/lang/String; = "miui.intent.action.ACTION_TYPE_C_HIGH_TEMP"

.field private static final ACTION_WIRELESS_CHARGING:Ljava/lang/String; = "miui.intent.action.ACTION_WIRELESS_CHARGING"

.field private static final ACTION_WIRELESS_CHG_WARNING_ACTIVITY:Ljava/lang/String; = "miui.intent.action.ACTIVITY_WIRELESS_CHG_WARNING"

.field private static final ACTION_WIRELESS_FW_UPDATE:Ljava/lang/String; = "miui.intent.action.ACTION_WIRELESS_FW_UPDATE"

.field private static final CAR_APP_STATE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_CAR_APP_STATE"

.field private static final CONNECTOR_TEMP_EVENT:Ljava/lang/String; = "POWER_SUPPLY_CONNECTOR_TEMP"

.field private static final EXTRA_CAR_CHG:Ljava/lang/String; = "miui.intent.extra.CAR_CHARGE"

.field private static final EXTRA_HANDLE_CONNECT_STATE:Ljava/lang/String; = "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"

.field private static final EXTRA_HANDLE_VERSION:Ljava/lang/String; = "miui.intent.extra.EXTRA_HANDLE_VERSION"

.field private static final EXTRA_LOW_TX_OFFESET_STATE:Ljava/lang/String; = "miui.intent.extra.EXTRA_LOW_TX_OFFSET_STATE"

.field private static final EXTRA_MOISTURE_DET:Ljava/lang/String; = "miui.intent.extra.EXTRA_MOISTURE_DET"

.field private static final EXTRA_NTC_ALARM:Ljava/lang/String; = "miui.intent.extra.EXTRA_NTC_ALARM"

.field private static final EXTRA_POGO_CONNECTED_STATE:Ljava/lang/String; = "miui.intent.extra.EXTRA_POGO_CONNECTED_STATE"

.field private static final EXTRA_POWER_MAX:Ljava/lang/String; = "miui.intent.extra.POWER_MAX"

.field private static final EXTRA_REVERSE_PEN_CHARGE_STATE:Ljava/lang/String; = "miui.intent.extra.ACTION_PEN_REVERSE_CHARGE_STATE"

.field private static final EXTRA_REVERSE_PEN_SOC:Ljava/lang/String; = "miui.intent.extra.REVERSE_PEN_SOC"

.field private static final EXTRA_RX_OFFSET:Ljava/lang/String; = "miui.intent.extra.EXTRA_RX_OFFSET"

.field private static final EXTRA_TYPE_C_HIGH_TEMP:Ljava/lang/String; = "miui.intent.extra.EXTRA_TYPE_C_HIGH_TEMP"

.field private static final EXTRA_WIRELESS_CHARGING:Ljava/lang/String; = "miui.intent.extra.WIRELESS_CHARGING"

.field private static final EXTRA_WIRELESS_FW_UPDATE:Ljava/lang/String; = "miui.intent.extra.EXTRA_WIRELESS_FW_UPDATE"

.field private static final HANDLE_BATTERY_CHANGED:Ljava/lang/String; = "POWER_SUPPLY_NAME"

.field private static final HANDLE_STATE_CONNECTED:I = 0x1

.field private static final HANDLE_STATE_DISCONNECTED:I = 0x0

.field private static final HAPTIC_STATE:Ljava/lang/String; = "haptic_feedback_disable"

.field private static final HVDCP3_TYPE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_HVDCP3_TYPE"

.field private static final LOW_INDUCTANCE_OFFSET_EVENT:Ljava/lang/String; = "POWER_SUPPLY_LOW_INDUCTANCE_OFFSET"

.field private static final MOISTURE_DET:Ljava/lang/String; = "POWER_SUPPLY_MOISTURE_DET_STS"

.field static final MSG_ADJUST_VOLTAGE:I = 0x14

.field static final MSG_BATTERY_CHANGED:I = 0x0

.field static final MSG_BLUETOOTH_CHANGED:I = 0xa

.field static final MSG_CAR_APP_STATE:I = 0x16

.field static final MSG_CHARGE_LIMIT:I = 0x1a

.field static final MSG_CHECK_TIME_REGION:I = 0xf

.field static final MSG_CONNECTOR_TEMP:I = 0x10

.field static final MSG_HANDLE_ATTACHED:I = 0x17

.field static final MSG_HANDLE_DETACHED:I = 0x18

.field static final MSG_HANDLE_LOW_TX_OFFSET:I = 0x1b

.field static final MSG_HANDLE_REBOOT:I = 0x19

.field static final MSG_HANDLE_TEMP_STATE:I = 0x1d

.field static final MSG_HVDCP3_DETECT:I = 0x2

.field static final MSG_MOISTURE_DET:I = 0x15

.field static final MSG_NFC_DISABLED:I = 0x9

.field static final MSG_NFC_ENABLED:I = 0x8

.field static final MSG_NTC_ALARM:I = 0x1c

.field static final MSG_POWER_OFF:I = 0xe

.field static final MSG_QUICKCHARGE_DETECT:I = 0x3

.field static final MSG_REVERSE_PEN_CHG_STATE:I = 0xc

.field static final MSG_RX_OFFSET:I = 0x11

.field static final MSG_SCREEN_OFF:I = 0x13

.field static final MSG_SCREEN_UNLOCK:I = 0x12

.field static final MSG_SHUTDOWN_DELAY:I = 0x7

.field static final MSG_SHUTDOWN_DELAY_WARNING:I = 0xd

.field static final MSG_SOC_DECIMAL:I = 0x4

.field static final MSG_WIRELESS_CHARGE_CLOSE:I = 0x5

.field static final MSG_WIRELESS_CHARGE_OPEN:I = 0x6

.field static final MSG_WIRELESS_FW_STATE:I = 0xb

.field static final MSG_WIRELESS_TX:I = 0x1

.field private static final NFC_CLOED:Ljava/lang/String; = "nfc_closd_from_wirelss"

.field private static final NTC_ALARM_EVENT:Ljava/lang/String; = "POWER_SUPPLY_NTC_ALARM"

.field private static final POWER_COMMON_RECEIVER_PERMISSION:Ljava/lang/String; = "com.miui.securitycenter.POWER_CENTER_COMMON_PERMISSION"

.field private static final POWER_SUPPLY_CAPACITY:Ljava/lang/String; = "POWER_SUPPLY_CAPACITY"

.field private static final POWER_SUPPLY_STATUS:Ljava/lang/String; = "POWER_SUPPLY_STATUS"

.field private static final QUICK_CHARGE_TYPE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_QUICK_CHARGE_TYPE"

.field private static final REDUCE_FULL_CHARGE_VBATT_10:I = 0xa

.field private static final REDUCE_FULL_CHARGE_VBATT_15:I = 0xf

.field private static final REDUCE_FULL_CHARGE_VBATT_20:I = 0x14

.field private static final RESET_FULL_CHARGE_VBATT:I = 0x0

.field private static final RETRY_UPDATE_DELAY:I = 0xea60

.field private static final REVERSE_CHG_MODE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_REVERSE_CHG_MODE"

.field private static final REVERSE_CHG_STATE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_REVERSE_CHG_STATE"

.field private static final REVERSE_PEN_CHG_STATE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_REVERSE_PEN_CHG_STATE"

.field private static final REVERSE_PEN_MAC_EVENT:Ljava/lang/String; = "POWER_SUPPLY_PEN_MAC"

.field private static final REVERSE_PEN_PLACE_ERR_EVENT:Ljava/lang/String; = "POWER_SUPPLY_PEN_PLACE_ERR"

.field private static final REVERSE_PEN_SOC_EVENT:Ljava/lang/String; = "POWER_SUPPLY_REVERSE_PEN_SOC"

.field private static final RX_OFFSET_EVENT:Ljava/lang/String; = "POWER_SUPPLY_RX_OFFSET"

.field private static final SHUTDOWN_DELAY_EVENT:Ljava/lang/String; = "POWER_SUPPLY_SHUTDOWN_DELAY"

.field private static final SOC_DECIMAL_EVENT:Ljava/lang/String; = "POWER_SUPPLY_SOC_DECIMAL"

.field private static final SOC_DECIMAL_RATE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_SOC_DECIMAL_RATE"

.field private static final UPDATE_DELAY:I = 0x3e8

.field private static final WIRELESS_AUTO_CLOSED_STATE:I = 0x1

.field private static final WIRELESS_CHG_ERROR_STATE:I = 0x2

.field private static final WIRELESS_LOW_BATTERY_LEVEL_STATE:I = 0x4

.field private static final WIRELESS_NO_ERROR_STATE:I = 0x0

.field private static final WIRELESS_OTHER_WIRELESS_CHG_STATE:I = 0x3

.field private static final WIRELESS_REVERSE_CHARGING:Ljava/lang/String; = "wireless_reverse_charging"

.field private static final WIRELESS_TX_TYPE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_TX_ADAPTER"

.field private static final WLS_FW_STATE_EVENT:Ljava/lang/String; = "POWER_SUPPLY_WLS_FW_STATE"


# instance fields
.field private mChargingNotificationId:I

.field private mClosedNfcFromCharging:Z

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private mCount:I

.field private mEndHighTempDate:Ljava/util/Date;

.field private mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

.field private mHapticState:Z

.field private mIsReverseWirelessCharge:Z

.field private mLastCloseReason:I

.field private mLastConnectorTemp:I

.field private mLastHvdcpType:I

.field private mLastMoistureDet:I

.field private mLastNtcAlarm:I

.field private mLastOffsetState:I

.field private mLastOpenStatus:I

.field private mLastPenReverseChargeState:I

.field private mLastPenReverseMac:Ljava/lang/String;

.field private mLastPenReversePLaceErr:Ljava/lang/String;

.field private mLastPenReverseSoc:I

.field private mLastPogoConnectedState:I

.field private mLastQuickChargeType:I

.field private mLastRxOffset:I

.field private mLastShutdownDelay:I

.field private mLastWirelessFwState:I

.field private mLastWirelessTxType:I

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mRetryAfterOneMin:Z

.field private mShowDisableNfc:Z

.field private mShowEnableNfc:Z

.field private mStartHithTempDate:Ljava/util/Date;

.field private final mUEventObserver:Landroid/os/UEventObserver;

.field private mUpdateSocDecimal:Z

.field final synthetic this$0:Lcom/android/server/MiuiBatteryServiceImpl;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandleInfo(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastCloseReason(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastCloseReason:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastConnectorTemp(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastConnectorTemp:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastHvdcpType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastHvdcpType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastMoistureDet(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastMoistureDet:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastNtcAlarm(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastNtcAlarm:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastOffsetState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOffsetState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastOpenStatus(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOpenStatus:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPenReverseChargeState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseChargeState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPenReverseMac(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseMac:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPenReversePLaceErr(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReversePLaceErr:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPenReverseSoc(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseSoc:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPogoConnectedState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPogoConnectedState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastQuickChargeType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastQuickChargeType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastRxOffset(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastRxOffset:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastShutdownDelay(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastShutdownDelay:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastWirelessFwState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessFwState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastWirelessTxType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessTxType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUpdateSocDecimal(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mUpdateSocDecimal:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmLastCloseReason(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastCloseReason:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastConnectorTemp(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastConnectorTemp:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastHvdcpType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastHvdcpType:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastMoistureDet(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastMoistureDet:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastNtcAlarm(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastNtcAlarm:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastOffsetState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOffsetState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastOpenStatus(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOpenStatus:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastPenReverseChargeState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseChargeState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastPenReverseMac(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseMac:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastPenReversePLaceErr(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReversePLaceErr:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastPenReverseSoc(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPenReverseSoc:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastPogoConnectedState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastPogoConnectedState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastQuickChargeType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastQuickChargeType:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastRxOffset(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastRxOffset:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastShutdownDelay(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastShutdownDelay:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastWirelessFwState(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessFwState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastWirelessTxType(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessTxType:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUpdateSocDecimal(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mUpdateSocDecimal:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$msendHandleBatteryStatsChangeBroadcast(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendHandleBatteryStatsChangeBroadcast()V

    return-void
.end method

.method public constructor <init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/os/Looper;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryServiceImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 667
    iput-object p1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    .line 668
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 649
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOffsetState:I

    .line 650
    iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastNtcAlarm:I

    .line 653
    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z

    .line 654
    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z

    .line 656
    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z

    .line 657
    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mRetryAfterOneMin:Z

    .line 659
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    .line 664
    iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I

    .line 669
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->initChargeStatus()V

    .line 670
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->initBatteryAuthentic()V

    .line 671
    invoke-static {p1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    .line 672
    new-instance v0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;

    invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver;-><init>(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$BatteryUEventObserver-IA;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mUEventObserver:Landroid/os/UEventObserver;

    .line 673
    const-string v1, "POWER_SUPPLY_TX_ADAPTER"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 674
    const-string v1, "POWER_SUPPLY_HVDCP3_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 675
    const-string v1, "POWER_SUPPLY_QUICK_CHARGE_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 676
    const-string v1, "POWER_SUPPLY_SOC_DECIMAL"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 677
    const-string v1, "POWER_SUPPLY_REVERSE_CHG_STATE"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 678
    const-string v1, "POWER_SUPPLY_REVERSE_CHG_MODE"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 679
    const-string v1, "POWER_SUPPLY_SHUTDOWN_DELAY"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 680
    const-string v1, "POWER_SUPPLY_WLS_FW_STATE"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 681
    const-string v1, "POWER_SUPPLY_REVERSE_PEN_CHG_STATE"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 683
    const-string v1, "POWER_SUPPLY_REVERSE_PEN_SOC"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 684
    const-string v1, "POWER_SUPPLY_PEN_MAC"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 685
    const-string v1, "POWER_SUPPLY_PEN_PLACE_ERR"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 687
    const-string v1, "POWER_SUPPLY_CONNECTOR_TEMP"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 688
    const-string v1, "POWER_SUPPLY_RX_OFFSET"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 689
    const-string v1, "POWER_SUPPLY_MOISTURE_DET_STS"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 690
    const-string v1, "POWER_SUPPLY_CAR_APP_STATE"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 691
    const-string v1, "POWER_SUPPLY_NAME"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 692
    const-string v1, "POWER_SUPPLY_LOW_INDUCTANCE_OFFSET"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 693
    const-string v1, "POWER_SUPPLY_NTC_ALARM"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 694
    return-void
.end method

.method private adjustVoltageFromStatsBroadcast(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1433
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryServiceImpl;)Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;

    move-result-object v0

    invoke-direct {v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getSBState()I

    move-result v0

    .line 1434
    .local v0, "smartBatt":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const-string v2, "miui.intent.extra.ADJUST_VOLTAGE_TS"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmIsSatisfyTempSocCondition(Lcom/android/server/MiuiBatteryServiceImpl;Z)V

    .line 1435
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const-string v2, "miui.intent.extra.ADJUST_VOLTAGE_TL"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmIsSatisfyTempLevelCondition(Lcom/android/server/MiuiBatteryServiceImpl;Z)V

    .line 1436
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmIsSatisfyTempLevelCondition(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1437
    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    .line 1438
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmiui/util/IMiCharge;->setSBState(I)Z

    goto :goto_0

    .line 1439
    :cond_0
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmIsSatisfyTempSocCondition(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1440
    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    .line 1441
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmiui/util/IMiCharge;->setSBState(I)Z

    goto :goto_0

    .line 1442
    :cond_1
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmIsSatisfyTimeRegionCondition(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1443
    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    .line 1444
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmiui/util/IMiCharge;->setSBState(I)Z

    goto :goto_0

    .line 1446
    :cond_2
    if-eqz v0, :cond_3

    .line 1447
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1, v3}, Lmiui/util/IMiCharge;->setSBState(I)Z

    .line 1449
    :cond_3
    :goto_0
    return-void
.end method

.method private adjustVoltageFromTimeRegion()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 1421
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getSBState()I

    move-result v0

    .line 1422
    .local v0, "smartBatt":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "smartBatt = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1423
    :cond_0
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isDateOfHighTemp()Z

    move-result v1

    const/16 v2, 0xa

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isInTragetCountry()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 1424
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmIsSatisfyTimeRegionCondition(Lcom/android/server/MiuiBatteryServiceImpl;Z)V

    .line 1425
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1, v2}, Lmiui/util/IMiCharge;->setSBState(I)Z

    goto :goto_0

    .line 1426
    :cond_1
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isDateOfHighTemp()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isInTragetCountry()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    if-ne v0, v2, :cond_3

    .line 1427
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmIsSatisfyTimeRegionCondition(Lcom/android/server/MiuiBatteryServiceImpl;Z)V

    .line 1428
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1, v2}, Lmiui/util/IMiCharge;->setSBState(I)Z

    .line 1430
    :cond_3
    :goto_0
    return-void
.end method

.method private checkSpecialHandle(Landroid/hardware/usb/UsbDevice;)V
    .locals 12
    .param p1, "usbDevice"    # Landroid/hardware/usb/UsbDevice;

    .line 1486
    if-nez p1, :cond_0

    .line 1487
    return-void

    .line 1489
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/hardware/usb/UsbDevice;->getConfiguration(I)Landroid/hardware/usb/UsbConfiguration;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/hardware/usb/UsbConfiguration;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I

    move-result v0

    .line 1490
    .local v0, "num":I
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getDecrpytedAddress(I)Ljava/lang/String;

    move-result-object v1

    .line 1492
    .local v1, "address":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v2

    .line 1493
    .local v2, "encryptedText":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v4, "MiuiBatteryServiceImpl"

    if-eqz v3, :cond_1

    .line 1494
    const-string v3, "getSerialNumber Failed"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1495
    return-void

    .line 1499
    :cond_1
    :try_start_0
    const-string v3, "123456789abcdefa"

    const-string v5, "US-ASCII"

    invoke-virtual {v3, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    .line 1501
    .local v3, "key":[B
    const-string v5, "AES/ECB/NoPadding"

    invoke-static {v5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v5

    .line 1502
    .local v5, "ciper":Ljavax/crypto/Cipher;
    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    const-string v7, "AES"

    invoke-direct {v6, v3, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 1503
    .local v6, "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    const/4 v7, 0x2

    invoke-virtual {v5, v7, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 1506
    invoke-virtual {p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v7

    .line 1507
    .local v7, "encrypted":[B
    invoke-virtual {v5, v7}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v8

    .line 1509
    .local v8, "decrypted":[B
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "0123456789abcd"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/String;

    sget-object v11, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v10, v8, v11}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1510
    new-instance v9, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget-object v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-direct {v9, v10, p1}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;-><init>(Lcom/android/server/MiuiBatteryServiceImpl;Landroid/hardware/usb/UsbDevice;)V

    iput-object v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    .line 1511
    const-string v9, "Decryption successful"

    invoke-static {v4, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1512
    iget-object v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget-object v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v10}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v10

    const-string v11, "getHandleColor"

    invoke-virtual {v10, v11}, Lmiui/util/IMiCharge;->getTypeCCommonInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->setColorNumber(Ljava/lang/String;)V

    .line 1513
    iget-object v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->setConnectState(I)V

    .line 1514
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendHandleStateChangeBroadcast()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1518
    .end local v3    # "key":[B
    .end local v5    # "ciper":Ljavax/crypto/Cipher;
    .end local v6    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .end local v7    # "encrypted":[B
    .end local v8    # "decrypted":[B
    :cond_2
    goto :goto_0

    .line 1516
    :catch_0
    move-exception v3

    .line 1517
    .local v3, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "decrypted exception = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private getCarChargingType()I
    .locals 2

    .line 1267
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getCarChargingType()Ljava/lang/String;

    move-result-object v0

    .line 1268
    .local v0, "carCharging":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1269
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 1271
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method private getChargingPowerMax()I
    .locals 2

    .line 1259
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getChargingPowerMax()Ljava/lang/String;

    move-result-object v0

    .line 1260
    .local v0, "powerMax":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1261
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 1263
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method private getCurrentDate()Ljava/lang/String;
    .locals 4

    .line 806
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 807
    .local v0, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 808
    .local v1, "date":Ljava/util/Date;
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 809
    .local v2, "today":Ljava/lang/String;
    return-object v2
.end method

.method private getDecrpytedAddress(I)Ljava/lang/String;
    .locals 3
    .param p1, "num"    # I

    .line 1583
    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    .line 1584
    mul-int/2addr p1, v0

    .line 1585
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1587
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->reverse()Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1588
    .local v0, "address":Ljava/lang/String;
    return-object v0
.end method

.method private getOneHundredThousandDigits()I
    .locals 4

    .line 1597
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, -0x2

    const-string/jumbo v3, "thermal_temp_state_value"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 1599
    .local v0, "tempState":I
    const v1, 0x186a0

    div-int v1, v0, v1

    rem-int/lit8 v1, v1, 0xa

    return v1
.end method

.method private getPSValue()I
    .locals 2

    .line 1275
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getPSValue()Ljava/lang/String;

    move-result-object v0

    .line 1276
    .local v0, "penSoc":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1277
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 1279
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method private getSBState()I
    .locals 2

    .line 1283
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getSBState()Ljava/lang/String;

    move-result-object v0

    .line 1284
    .local v0, "smartBatt":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1285
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 1287
    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method private handleConnectReboot()V
    .locals 5

    .line 1522
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "usb"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    .line 1523
    .local v0, "usbManager":Landroid/hardware/usb/UsbManager;
    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1524
    return-void

    .line 1526
    :cond_0
    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbDevice;

    .line 1527
    .local v2, "device":Landroid/hardware/usb/UsbDevice;
    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v3

    const/16 v4, 0x5083

    if-ne v3, v4, :cond_1

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v3

    const/16 v4, 0x2717

    if-ne v3, v4, :cond_1

    .line 1528
    const-string v1, "MiuiBatteryServiceImpl"

    const-string v3, "miHandle Attached when reboot"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    invoke-direct {p0, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->checkSpecialHandle(Landroid/hardware/usb/UsbDevice;)V

    .line 1530
    return-void

    .line 1532
    .end local v2    # "device":Landroid/hardware/usb/UsbDevice;
    :cond_1
    goto :goto_0

    .line 1533
    :cond_2
    return-void
.end method

.method private handleTempState()V
    .locals 5

    .line 1608
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getOneHundredThousandDigits()I

    move-result v0

    .line 1609
    .local v0, "tempStateValue":I
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isBatteryHighTemp()Z

    move-result v1

    .line 1610
    .local v1, "isBatteryHighTemp":Z
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isBatteryHighTemp()Z

    move-result v2

    const-string/jumbo v3, "set_rx_sleep"

    const/4 v4, 0x5

    if-eqz v2, :cond_1

    if-ne v0, v4, :cond_1

    .line 1611
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmIsStopCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1613
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v2

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1614
    :cond_0
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmIsStopCharge(Lcom/android/server/MiuiBatteryServiceImpl;Z)V

    goto :goto_0

    .line 1615
    :cond_1
    if-eq v0, v4, :cond_3

    .line 1616
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmIsStopCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1618
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v2

    const-string v4, "0"

    invoke-virtual {v2, v3, v4}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1619
    :cond_2
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmIsStopCharge(Lcom/android/server/MiuiBatteryServiceImpl;Z)V

    .line 1621
    :cond_3
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tempStateValue = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isBatteryHighTemp = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiBatteryServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1622
    return-void
.end method

.method private initBatteryAuthentic()V
    .locals 5

    .line 734
    const-string v0, "ro.product.name"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nabu"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 735
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "pipa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 736
    :cond_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getBatteryAuthentic()Ljava/lang/String;

    move-result-object v0

    .line 737
    .local v0, "batteryAuthentic":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 738
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 739
    .local v1, "batteryAuthenticValue":I
    if-nez v1, :cond_1

    .line 740
    const/16 v2, 0xd

    const-wide/16 v3, 0x7530

    invoke-virtual {p0, v2, v3, v4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V

    .line 744
    .end local v0    # "batteryAuthentic":Ljava/lang/String;
    .end local v1    # "batteryAuthenticValue":I
    :cond_1
    return-void
.end method

.method private initChargeStatus()V
    .locals 4

    .line 708
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getQuickChargeType()Ljava/lang/String;

    move-result-object v0

    .line 709
    .local v0, "quickChargeType":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/util/IMiCharge;->getTxAdapt()Ljava/lang/String;

    move-result-object v1

    .line 711
    .local v1, "txAdapter":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "quickChargeType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " txAdapter = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiBatteryServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 714
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 715
    .local v2, "quickChargeValue":I
    if-lez v2, :cond_0

    .line 716
    iput v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastQuickChargeType:I

    .line 717
    const/4 v3, 0x3

    invoke-virtual {p0, v3, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 718
    if-lt v2, v3, :cond_0

    .line 719
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendSocDecimaBroadcast()V

    .line 724
    .end local v2    # "quickChargeValue":I
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    .line 725
    invoke-virtual {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 726
    .local v2, "txAdapterValue":I
    if-lez v2, :cond_1

    .line 727
    iput v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastWirelessTxType:I

    .line 728
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 731
    .end local v2    # "txAdapterValue":I
    :cond_1
    return-void
.end method

.method private isBatteryHighTemp()Z
    .locals 3

    .line 1604
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "allowed_kill_battery_temp_threshhold"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method private isDateOfHighTemp()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 1452
    const-string v0, "06-15"

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mStartHithTempDate:Ljava/util/Date;

    .line 1453
    const-string v0, "09-15"

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mEndHighTempDate:Ljava/util/Date;

    .line 1454
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getCurrentDate()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 1455
    .local v0, "currentDate":Ljava/util/Date;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "currentDate = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1456
    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mStartHithTempDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mEndHighTempDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gtz v1, :cond_1

    .line 1457
    const/4 v1, 0x1

    return v1

    .line 1459
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method private isInChina()Z
    .locals 4

    .line 1464
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1465
    .local v0, "tel":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    .line 1466
    .local v1, "networkOperator":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1467
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1468
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "networkOperator = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiBatteryServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1469
    :cond_0
    const-string v2, "460"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    return v2

    .line 1471
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method private isInTragetCountry()Z
    .locals 3

    .line 1475
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isInChina()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    .line 1477
    :cond_0
    const-string v0, "ro.product.mod_device"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "taoyao"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1478
    const-string v0, "persist.vendor.domain.charge"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 1482
    :cond_1
    return v2

    .line 1479
    :cond_2
    :goto_0
    const-string v0, "ro.miui.region"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1480
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryServiceImpl;->SUPPORT_COUNTRY:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private isSupportControlHaptic()Z
    .locals 2

    .line 747
    const-string v0, "ro.product.device"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mayfly"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 748
    const-string v0, "persist.vendor.revchg.shutmotor"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 751
    :cond_0
    return v1

    .line 749
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private parseDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .param p1, "date"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 812
    const-string v0, "MM-dd"

    .line 813
    .local v0, "dateFormat":Ljava/lang/String;
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 814
    .local v1, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    return-object v2
.end method

.method private sendBroadcast(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1298
    const/high16 v0, 0x31000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1301
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1302
    return-void
.end method

.method private sendHandleBatteryStatsChangeBroadcast()V
    .locals 3

    .line 1553
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    const-string v1, "MiuiBatteryServiceImpl"

    if-nez v0, :cond_0

    .line 1554
    const-string v0, "faild to get handleInfo"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1555
    return-void

    .line 1557
    :cond_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1558
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "batteryInfo changed info = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1560
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_HANDLE_BATTERY_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1561
    .local v0, "handleBatteryChangeBroadcast":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I

    const-string v2, "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1562
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryLevel:I

    const-string v2, "batteryLevel"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1563
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mBatteryStats:Ljava/lang/String;

    const-string v2, "batteryStats"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1564
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1565
    return-void
.end method

.method private sendHandleStateChangeBroadcast()V
    .locals 3

    .line 1536
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    const-string v1, "MiuiBatteryServiceImpl"

    if-nez v0, :cond_0

    .line 1537
    const-string v0, "faild to get handleInfo"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1538
    return-void

    .line 1540
    :cond_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetDEBUG(Lcom/android/server/MiuiBatteryServiceImpl;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1541
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "connect changed info = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1543
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_HANDLE_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1544
    .local v0, "handleBroadcast":Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I

    const-string v2, "miui.intent.extra.EXTRA_HANDLE_CONNECT_STATE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1545
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mFwVersion:Ljava/lang/String;

    const-string v2, "miui.intent.extra.EXTRA_HANDLE_VERSION"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1546
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mVid:I

    const-string/jumbo v2, "vid"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1547
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mPid:I

    const-string v2, "pid"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1548
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    iget-object v1, v1, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mColorNumber:Ljava/lang/String;

    const-string v2, "ColorNumber"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1549
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendBroadcast(Landroid/content/Intent;)V

    .line 1550
    return-void
.end method

.method private sendSocDecimaBroadcast()V
    .locals 5

    .line 697
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/IMiCharge;->getSocDecimal()Ljava/lang/String;

    move-result-object v0

    .line 698
    .local v0, "socDecimal":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/util/IMiCharge;->getSocDecimalRate()Ljava/lang/String;

    move-result-object v1

    .line 699
    .local v1, "socDecimalRate":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 700
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 701
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 702
    .local v2, "socDecimalValue":I
    invoke-virtual {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 703
    .local v3, "socDecimalRateVaule":I
    const/4 v4, 0x4

    invoke-virtual {p0, v4, v2, v3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(III)V

    .line 705
    .end local v2    # "socDecimalValue":I
    .end local v3    # "socDecimalRateVaule":I
    :cond_0
    return-void
.end method

.method private sendStickyBroadcast(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1291
    const/high16 v0, 0x31000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1294
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1295
    return-void
.end method

.method private sendUpdateStatusBroadCast(I)V
    .locals 3
    .param p1, "status"    # I

    .line 1327
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_WIRELESS_CHARGING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1328
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1331
    const-string v1, "miui.intent.extra.WIRELESS_CHARGING"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1332
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1333
    return-void
.end method

.method private shouldCloseWirelessReverseCharging(I)V
    .locals 3
    .param p1, "batteryLevel"    # I

    .line 1398
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "wireless_reverse_charging"

    const/16 v2, 0x1e

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1399
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateWirelessReverseChargingNotification(I)V

    .line 1401
    :cond_0
    return-void
.end method

.method private showPowerOffWarningDialog()V
    .locals 6

    .line 1404
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemUiContext()Landroid/app/ContextImpl;

    move-result-object v0

    .line 1405
    .local v0, "systemuiContext":Landroid/content/Context;
    new-instance v1, Lmiuix/appcompat/app/AlertDialog$Builder;

    const v2, 0x66110006

    invoke-direct {v1, v0, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1406
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    .line 1407
    const v2, 0x110f0070

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v2

    .line 1408
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1409
    const/16 v3, 0x1e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    const/high16 v5, 0x110d0000

    invoke-virtual {v2, v5, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1408
    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$1;

    invoke-direct {v2, p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler$1;-><init>(Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;)V

    .line 1410
    const v3, 0x110f006f

    invoke-virtual {v1, v3, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    .line 1415
    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v1

    .line 1416
    .local v1, "powerOffDialog":Lmiuix/appcompat/app/AlertDialog;
    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7da

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 1417
    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog;->show()V

    .line 1418
    return-void
.end method

.method private showWirelessCharingWarningDialog()V
    .locals 3

    .line 1316
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTIVITY_WIRELESS_CHG_WARNING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1317
    .local v0, "dialogIntent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1318
    const-string v1, "plugstatus"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1319
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1320
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendUpdateStatusBroadCast(I)V

    .line 1321
    return-void
.end method

.method private updateMiuiPCBatteryChanged()V
    .locals 4

    .line 1305
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.MIUI_PC_BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1306
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.EXTRA_LOW_TX_OFFSET_STATE"

    iget v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastOffsetState:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1307
    const-string v1, "miui.intent.extra.EXTRA_NTC_ALARM"

    iget v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mLastNtcAlarm:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1309
    const/high16 v1, 0x31000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1312
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v3, "com.miui.securitycenter.POWER_CENTER_COMMON_PERMISSION"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 1313
    return-void
.end method

.method private updateWirelessReverseChargingNotification(I)V
    .locals 14
    .param p1, "closedReason"    # I

    .line 1336
    const/4 v0, 0x0

    .line 1337
    .local v0, "messageRes":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1338
    .local v1, "r":Landroid/content/res/Resources;
    const v2, 0x110f0418

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1340
    .local v2, "title":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v3}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v3

    .line 1341
    const-string v4, "notification"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 1342
    .local v3, "notificationManager":Landroid/app/NotificationManager;
    const-string v4, "MiuiBatteryServiceImpl"

    if-nez v3, :cond_0

    .line 1343
    const-string v5, "get notification service failed"

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1344
    return-void

    .line 1347
    :cond_0
    const/4 v5, 0x4

    const/4 v6, 0x1

    if-ne p1, v6, :cond_1

    .line 1348
    const v0, 0x110f0415

    goto :goto_0

    .line 1349
    :cond_1
    if-ne p1, v5, :cond_2

    .line 1350
    const v0, 0x110f0416

    goto :goto_0

    .line 1351
    :cond_2
    const/4 v7, 0x2

    if-ne p1, v7, :cond_3

    .line 1352
    const v0, 0x110f0417

    .line 1356
    :cond_3
    :goto_0
    iget v7, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mChargingNotificationId:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    if-eqz v7, :cond_4

    .line 1357
    sget-object v10, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v3, v8, v7, v10}, Landroid/app/NotificationManager;->cancelAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)V

    .line 1358
    const-string v7, "Clear notification"

    invoke-static {v4, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1359
    iput v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mChargingNotificationId:I

    .line 1362
    :cond_4
    if-eqz v0, :cond_6

    .line 1363
    new-instance v7, Landroid/app/Notification$Builder;

    iget-object v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v10}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v10

    sget-object v11, Lcom/android/internal/notification/SystemNotificationChannels;->USB:Ljava/lang/String;

    invoke-direct {v7, v10, v11}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1364
    const v10, 0x108089a

    invoke-virtual {v7, v10}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 1365
    const-wide/16 v10, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 1366
    invoke-virtual {v7, v9}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 1367
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 1368
    invoke-virtual {v7, v9}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v10}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v10

    .line 1369
    const v11, 0x106001c

    invoke-virtual {v10, v11}, Landroid/content/Context;->getColor(I)I

    move-result v10

    invoke-virtual {v7, v10}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 1372
    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 1373
    invoke-virtual {v7, v6}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 1375
    .local v7, "builder":Landroid/app/Notification$Builder;
    if-ne p1, v5, :cond_5

    .line 1376
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v10

    iget-object v11, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    .line 1377
    const-string/jumbo v12, "wireless_reverse_charging"

    const/16 v13, 0x1e

    invoke-static {v11, v12, v13}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v11

    int-to-float v11, v11

    const/high16 v12, 0x42c80000    # 100.0f

    div-float/2addr v11, v12

    float-to-double v11, v11

    invoke-virtual {v10, v11, v12}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v10

    filled-new-array {v10}, [Ljava/lang/Object;

    move-result-object v10

    .line 1376
    invoke-virtual {v1, v0, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1378
    .local v10, "messageString":Ljava/lang/String;
    invoke-virtual {v7, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1379
    .end local v10    # "messageString":Ljava/lang/String;
    goto :goto_1

    .line 1380
    :cond_5
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    .line 1381
    .local v10, "message":Ljava/lang/CharSequence;
    invoke-virtual {v7, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1383
    .end local v10    # "message":Ljava/lang/CharSequence;
    :goto_1
    invoke-virtual {v7}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v10

    .line 1384
    .local v10, "notification":Landroid/app/Notification;
    sget-object v11, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v3, v8, v0, v10, v11}, Landroid/app/NotificationManager;->notifyAsUser(Ljava/lang/String;ILandroid/app/Notification;Landroid/os/UserHandle;)V

    .line 1385
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "push notification:"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1386
    iput v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mChargingNotificationId:I

    .line 1389
    .end local v7    # "builder":Landroid/app/Notification$Builder;
    .end local v10    # "notification":Landroid/app/Notification;
    :cond_6
    if-ne p1, v5, :cond_7

    .line 1390
    iget-object v4, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v4}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v4

    invoke-virtual {v4, v9}, Lmiui/util/IMiCharge;->setWirelessChargingEnabled(Z)I

    goto :goto_2

    .line 1391
    :cond_7
    if-nez p1, :cond_8

    .line 1392
    return-void

    .line 1394
    :cond_8
    :goto_2
    invoke-direct {p0, v6}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendUpdateStatusBroadCast(I)V

    .line 1395
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .line 1018
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "Get NFC failed"

    const-string v2, "0"

    const-string v3, "1"

    const-string/jumbo v4, "screen_unlock"

    const/16 v5, 0x8

    const-string v6, "nfc_closd_from_wirelss"

    const/4 v7, 0x3

    const-string v8, "MiuiBatteryServiceImpl"

    const/4 v9, 0x1

    const/4 v10, 0x0

    packed-switch v0, :pswitch_data_0

    .line 1254
    const-string v0, "NO Message"

    invoke-static {v8, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 1251
    :pswitch_0
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->handleTempState()V

    .line 1252
    goto/16 :goto_6

    .line 1248
    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateMiuiPCBatteryChanged()V

    .line 1249
    goto/16 :goto_6

    .line 1239
    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1240
    .local v0, "batteryLevel":I
    const/16 v1, 0x4b

    if-lt v0, v1, :cond_0

    .line 1241
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1, v3}, Lmiui/util/IMiCharge;->setInputSuspendState(Ljava/lang/String;)Z

    goto/16 :goto_6

    .line 1242
    :cond_0
    const/16 v1, 0x28

    if-gt v0, v1, :cond_12

    .line 1243
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1, v2}, Lmiui/util/IMiCharge;->setInputSuspendState(Ljava/lang/String;)Z

    goto/16 :goto_6

    .line 1236
    .end local v0    # "batteryLevel":I
    :pswitch_3
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->handleConnectReboot()V

    .line 1237
    goto/16 :goto_6

    .line 1222
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    if-eqz v0, :cond_1

    .line 1223
    invoke-virtual {v0, v10}, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->setConnectState(I)V

    .line 1224
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendHandleStateChangeBroadcast()V

    .line 1225
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendHandleBatteryStatsChangeBroadcast()V

    .line 1227
    :cond_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fputmLastPhoneBatteryLevel(Lcom/android/server/MiuiBatteryServiceImpl;I)V

    .line 1228
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    .line 1229
    goto/16 :goto_6

    .line 1218
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/hardware/usb/UsbDevice;

    .line 1219
    .local v0, "au":Landroid/hardware/usb/UsbDevice;
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->checkSpecialHandle(Landroid/hardware/usb/UsbDevice;)V

    .line 1220
    goto/16 :goto_6

    .line 1231
    .end local v0    # "au":Landroid/hardware/usb/UsbDevice;
    :pswitch_6
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_POGO_CONNECTED_STATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1232
    .local v0, "pogoConnectedIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.EXTRA_POGO_CONNECTED_STATE"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1233
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1234
    goto/16 :goto_6

    .line 1212
    .end local v0    # "pogoConnectedIntent":Landroid/content/Intent;
    :pswitch_7
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_MOISTURE_DET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1213
    .local v0, "moistureDetIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.EXTRA_MOISTURE_DET"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1214
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1215
    goto/16 :goto_6

    .line 1208
    .end local v0    # "moistureDetIntent":Landroid/content/Intent;
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 1209
    .local v0, "j":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->adjustVoltageFromStatsBroadcast(Landroid/content/Intent;)V

    .line 1210
    goto/16 :goto_6

    .line 1026
    .end local v0    # "j":Landroid/content/Intent;
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0, v4, v2}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1027
    goto/16 :goto_6

    .line 1023
    :pswitch_a
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v0

    invoke-virtual {v0, v4, v3}, Lmiui/util/IMiCharge;->setMiChargePath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1024
    goto/16 :goto_6

    .line 1203
    :pswitch_b
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_RX_OFFSET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1204
    .local v0, "rxOffsetIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.EXTRA_RX_OFFSET"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1205
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1206
    goto/16 :goto_6

    .line 1198
    .end local v0    # "rxOffsetIntent":Landroid/content/Intent;
    :pswitch_c
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_TYPE_C_HIGH_TEMP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1199
    .local v0, "connectorTempIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.EXTRA_TYPE_C_HIGH_TEMP"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1200
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1201
    goto/16 :goto_6

    .line 1192
    .end local v0    # "connectorTempIntent":Landroid/content/Intent;
    :pswitch_d
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->adjustVoltageFromTimeRegion()V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1195
    goto/16 :goto_6

    .line 1193
    :catch_0
    move-exception v0

    .line 1194
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    .line 1196
    .end local v0    # "e":Ljava/text/ParseException;
    goto/16 :goto_6

    .line 1185
    :pswitch_e
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.internal.intent.action.REQUEST_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1186
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.KEY_CONFIRM"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1187
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1188
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1189
    goto/16 :goto_6

    .line 1181
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_f
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->showPowerOffWarningDialog()V

    .line 1182
    const/16 v0, 0xe

    const-wide/16 v1, 0x7530

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V

    .line 1183
    goto/16 :goto_6

    .line 1175
    :pswitch_10
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_PEN_REVERSE_CHARGE_STATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1176
    .local v0, "penReverseChgStateIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.ACTION_PEN_REVERSE_CHARGE_STATE"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1177
    const-string v1, "miui.intent.extra.REVERSE_PEN_SOC"

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getPSValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1178
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1179
    goto/16 :goto_6

    .line 1170
    .end local v0    # "penReverseChgStateIntent":Landroid/content/Intent;
    :pswitch_11
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_WIRELESS_FW_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1171
    .local v0, "wirelessFwIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.EXTRA_WIRELESS_FW_UPDATE"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1172
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1173
    goto/16 :goto_6

    .line 1166
    .end local v0    # "wirelessFwIntent":Landroid/content/Intent;
    :pswitch_12
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1167
    .local v0, "bluetoothState":I
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryServiceImpl;)Lmiui/util/IMiCharge;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/util/IMiCharge;->setBtTransferStartState(I)Z

    .line 1168
    goto/16 :goto_6

    .line 1136
    .end local v0    # "bluetoothState":I
    :pswitch_13
    :try_start_1
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getNfcAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mNfcAdapter:Landroid/nfc/NfcAdapter;
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1139
    goto :goto_0

    .line 1137
    :catch_1
    move-exception v0

    .line 1138
    .local v0, "e":Ljava/lang/UnsupportedOperationException;
    invoke-static {v8, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1140
    .end local v0    # "e":Ljava/lang/UnsupportedOperationException;
    :goto_0
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1141
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->disable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1142
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v0, v6, v9}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1143
    iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mClosedNfcFromCharging:Z

    .line 1144
    iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z

    goto :goto_1

    .line 1146
    :cond_2
    const-string v0, "close NFC failed"

    invoke-static {v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    :cond_3
    :goto_1
    iget-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z

    if-nez v0, :cond_4

    .line 1150
    iget-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z

    if-eqz v0, :cond_12

    .line 1151
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x110f01cf

    invoke-static {v0, v1, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1152
    iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z

    goto/16 :goto_6

    .line 1155
    :cond_4
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isSupportControlHaptic()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1156
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x110f01d0

    invoke-static {v0, v1, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_6

    .line 1158
    :cond_5
    iget-boolean v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z

    if-eqz v0, :cond_12

    .line 1159
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v0}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x110f01d1

    invoke-static {v0, v1, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1160
    iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowDisableNfc:Z

    goto/16 :goto_6

    .line 1084
    :pswitch_14
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1085
    .local v0, "txType":I
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v2, v6, v10}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_6

    move v2, v9

    goto :goto_2

    :cond_6
    move v2, v10

    :goto_2
    iput-boolean v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mClosedNfcFromCharging:Z

    .line 1087
    :try_start_2
    iget-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v2}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/nfc/NfcAdapter;->getNfcAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mNfcAdapter:Landroid/nfc/NfcAdapter;
    :try_end_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1090
    goto :goto_3

    .line 1088
    :catch_2
    move-exception v2

    .line 1089
    .local v2, "e":Ljava/lang/UnsupportedOperationException;
    invoke-static {v8, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1091
    .end local v2    # "e":Ljava/lang/UnsupportedOperationException;
    :goto_3
    if-nez v0, :cond_9

    iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mClosedNfcFromCharging:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_9

    .line 1092
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v1}, Landroid/nfc/NfcAdapter;->enable()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1093
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "try to open NFC "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " times success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1094
    iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mClosedNfcFromCharging:Z

    .line 1095
    iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z

    .line 1096
    iput v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I

    .line 1097
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v6, v10}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_4

    .line 1099
    :cond_7
    iget v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I

    if-ge v1, v7, :cond_8

    .line 1100
    add-int/2addr v1, v9

    iput v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I

    .line 1101
    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v5, v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V

    goto :goto_4

    .line 1103
    :cond_8
    const-string v1, "open NFC failed"

    invoke-static {v8, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    iput v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mCount:I

    .line 1105
    iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mRetryAfterOneMin:Z

    if-nez v1, :cond_9

    .line 1106
    const-wide/32 v1, 0xea60

    invoke-virtual {p0, v5, v1, v2}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(IJ)V

    .line 1107
    iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mRetryAfterOneMin:Z

    .line 1112
    :cond_9
    :goto_4
    iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z

    if-nez v1, :cond_a

    .line 1113
    iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z

    if-eqz v1, :cond_12

    .line 1114
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x110f01e1

    invoke-static {v1, v2, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1115
    iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z

    goto/16 :goto_6

    .line 1118
    :cond_a
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->isSupportControlHaptic()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1119
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "haptic_feedback_disable"

    invoke-static {v1, v2, v10, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-lez v1, :cond_b

    goto :goto_5

    :cond_b
    move v9, v10

    :goto_5
    iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHapticState:Z

    .line 1120
    if-eqz v9, :cond_c

    .line 1122
    const-string v1, "open haptic when wireless reverse charge"

    invoke-static {v8, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v1, v2, v10, v10}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 1125
    :cond_c
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x110f01e2

    invoke-static {v1, v2, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_6

    .line 1127
    :cond_d
    iget-boolean v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z

    if-eqz v1, :cond_12

    .line 1128
    iget-object v1, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->this$0:Lcom/android/server/MiuiBatteryServiceImpl;

    invoke-static {v1}, Lcom/android/server/MiuiBatteryServiceImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiBatteryServiceImpl;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x110f01e3

    invoke-static {v1, v2, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1129
    iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mShowEnableNfc:Z

    goto/16 :goto_6

    .line 1079
    .end local v0    # "txType":I
    :pswitch_15
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_SHUTDOWN_DELAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1080
    .local v0, "shutdownDelayIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.shutdown_delay"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1081
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1082
    goto/16 :goto_6

    .line 1070
    .end local v0    # "shutdownDelayIntent":Landroid/content/Intent;
    :pswitch_16
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1071
    .local v0, "openStatus":I
    if-lez v0, :cond_e

    .line 1073
    invoke-direct {p0, v10}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateWirelessReverseChargingNotification(I)V

    .line 1075
    :cond_e
    iput-boolean v9, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z

    .line 1076
    if-lez v0, :cond_f

    const/16 v5, 0x9

    :cond_f
    invoke-virtual {p0, v5, v10}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(II)V

    .line 1077
    goto/16 :goto_6

    .line 1060
    .end local v0    # "openStatus":I
    :pswitch_17
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1061
    .local v0, "closeReason":I
    if-ne v0, v9, :cond_10

    .line 1062
    invoke-direct {p0, v9}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateWirelessReverseChargingNotification(I)V

    goto/16 :goto_6

    .line 1063
    :cond_10
    const/4 v1, 0x2

    if-ne v0, v1, :cond_11

    .line 1064
    invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->updateWirelessReverseChargingNotification(I)V

    goto/16 :goto_6

    .line 1065
    :cond_11
    if-ne v0, v7, :cond_12

    .line 1066
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->showWirelessCharingWarningDialog()V

    goto :goto_6

    .line 1054
    .end local v0    # "closeReason":I
    :pswitch_18
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_SOC_DECIMAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1055
    .local v0, "socDecimalIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.soc_decimal"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1056
    const-string v1, "miui.intent.extra.soc_decimal_rate"

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1057
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1058
    goto :goto_6

    .line 1044
    .end local v0    # "socDecimalIntent":Landroid/content/Intent;
    :pswitch_19
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_QUICK_CHARGE_TYPE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1045
    .local v0, "quickChargeTypeIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.quick_charge_type"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1046
    const-string v1, "miui.intent.extra.POWER_MAX"

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getChargingPowerMax()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1047
    const-string v1, "miui.intent.extra.CAR_CHARGE"

    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->getCarChargingType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1048
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1049
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-lt v1, v7, :cond_12

    .line 1050
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendSocDecimaBroadcast()V

    goto :goto_6

    .line 1039
    .end local v0    # "quickChargeTypeIntent":Landroid/content/Intent;
    :pswitch_1a
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.ACTION_HVDCP_TYPE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1040
    .local v0, "hvdcpTypeIntent":Landroid/content/Intent;
    const-string v1, "miui.intent.extra.hvdcp_type"

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1041
    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1042
    goto :goto_6

    .line 1029
    .end local v0    # "hvdcpTypeIntent":Landroid/content/Intent;
    :pswitch_1b
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 1030
    .local v0, "wirelessTxType":I
    new-instance v1, Landroid/content/Intent;

    const-string v2, "miui.intent.action.ACTION_WIRELESS_TX_TYPE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1031
    .local v1, "wirelessTxTypeIntent":Landroid/content/Intent;
    const-string v2, "miui.intent.extra.wireless_tx_type"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1032
    invoke-direct {p0, v1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 1033
    iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mIsReverseWirelessCharge:Z

    .line 1036
    iput-boolean v10, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mRetryAfterOneMin:Z

    .line 1037
    goto :goto_6

    .line 1020
    .end local v0    # "wirelessTxType":I
    .end local v1    # "wirelessTxTypeIntent":Landroid/content/Intent;
    :pswitch_1c
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->shouldCloseWirelessReverseCharging(I)V

    .line 1021
    nop

    .line 1256
    :cond_12
    :goto_6
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public hexStringToByteArray(Ljava/lang/String;)[B
    .locals 7
    .param p1, "s"    # Ljava/lang/String;

    .line 1568
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 1569
    .local v0, "len":I
    div-int/lit8 v1, v0, 0x2

    new-array v1, v1, [B

    .line 1570
    .local v1, "data":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 1572
    :try_start_0
    div-int/lit8 v3, v2, 0x2

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v6, v2, 0x1

    .line 1573
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6, v5}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v1, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1577
    nop

    .line 1570
    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    .line 1574
    :catch_0
    move-exception v3

    .line 1575
    .local v3, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid hex string: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1576
    const/4 v4, 0x0

    return-object v4

    .line 1579
    .end local v2    # "i":I
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    return-object v1
.end method

.method isHandleConnect()Z
    .locals 2

    .line 1592
    iget-object v0, p0, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->mHandleInfo:Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/android/server/MiuiBatteryServiceImpl$HandleInfo;->mConnectState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public parseInt(Ljava/lang/String;)I
    .locals 3
    .param p1, "argument"    # Ljava/lang/String;

    .line 798
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 799
    :catch_0
    move-exception v0

    .line 800
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid integer argument "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBatteryServiceImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    const/4 v1, -0x1

    return v1
.end method

.method public sendMessage(II)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg1"    # I

    .line 762
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V

    .line 763
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 764
    .local v0, "m":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 765
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(Landroid/os/Message;)Z

    .line 766
    return-void
.end method

.method public sendMessage(III)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .line 769
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V

    .line 770
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 771
    .local v0, "m":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 772
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 773
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(Landroid/os/Message;)Z

    .line 774
    return-void
.end method

.method public sendMessage(IZ)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg"    # Z

    .line 755
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V

    .line 756
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 757
    .local v0, "m":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 758
    invoke-virtual {p0, v0}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessage(Landroid/os/Message;)Z

    .line 759
    return-void
.end method

.method public sendMessageDelayed(IJ)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "delayMillis"    # J

    .line 777
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V

    .line 778
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 779
    .local v0, "m":Landroid/os/Message;
    invoke-virtual {p0, v0, p2, p3}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 780
    return-void
.end method

.method public sendMessageDelayed(ILjava/lang/Object;J)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg"    # Ljava/lang/Object;
    .param p3, "delayMillis"    # J

    .line 790
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V

    .line 791
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 792
    .local v0, "m":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 793
    invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 794
    return-void
.end method

.method public sendMessageDelayed(IZJ)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg"    # Z
    .param p3, "delayMillis"    # J

    .line 783
    invoke-virtual {p0, p1}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->removeMessages(I)V

    .line 784
    invoke-static {p0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 785
    .local v0, "m":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 786
    invoke-virtual {p0, v0, p3, p4}, Lcom/android/server/MiuiBatteryServiceImpl$BatteryHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 787
    return-void
.end method
