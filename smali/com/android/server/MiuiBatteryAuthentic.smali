.class public Lcom/android/server/MiuiBatteryAuthentic;
.super Ljava/lang/Object;
.source "MiuiBatteryAuthentic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;,
        Lcom/android/server/MiuiBatteryAuthentic$IMTService;
    }
.end annotation


# static fields
.field private static final BATTERY_AUTHENTIC:Ljava/lang/String; = "battery_authentic_certificate"

.field private static final DEBUG:Z = false

.field private static final DEFAULT_BATTERT_SN:Ljava/lang/String; = "111111111111111111111111"

.field private static final DEFAULT_IMEI:Ljava/lang/String; = "1234567890"

.field private static final DELAY_TIME:I = 0xea60

.field private static volatile INSTANCE:Lcom/android/server/MiuiBatteryAuthentic; = null

.field private static final PROVISION_COMPLETE_BROADCAST:Ljava/lang/String; = "android.provision.action.PROVISION_COMPLETE"

.field private static final PUBLIC_KEY:Ljava/lang/String; = "3059301306072a8648ce3d020106082a8648ce3d030107034200045846fce7eaab1053c62f76cd7c61ae09a8411a5c106cad7a95c11c26dd25e507e963e2ae8f2c9672db92fe9834584dc41996454c8c929fc26e9d512e4096f450"

.field private static final TAG:Ljava/lang/String; = "MiuiBatteryAuthentic"


# instance fields
.field public mBatterySn:Ljava/lang/String;

.field public mCloudSign:Ljava/lang/String;

.field public final mContentResolver:Landroid/content/ContentResolver;

.field public mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

.field public mIMTService:Lcom/android/server/MiuiBatteryAuthentic$IMTService;

.field public mImei:Ljava/lang/String;

.field public mIsVerified:Z

.field private mMiCharge:Lmiui/util/IMiCharge;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/MiuiBatteryAuthentic;)Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mHandler:Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiCharge(Lcom/android/server/MiuiBatteryAuthentic;)Lmiui/util/IMiCharge;
    .locals 0

    iget-object p0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mMiCharge:Lmiui/util/IMiCharge;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 80
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/MiuiBatteryAuthentic;->INSTANCE:Lcom/android/server/MiuiBatteryAuthentic;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Lmiui/util/IMiCharge;->getInstance()Lmiui/util/IMiCharge;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mMiCharge:Lmiui/util/IMiCharge;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mImei:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z

    .line 94
    iput-object p1, p0, Lcom/android/server/MiuiBatteryAuthentic;->mContext:Landroid/content/Context;

    .line 95
    new-instance v0, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    invoke-static {}, Lcom/android/server/MiuiFgThread;->get()Lcom/android/server/MiuiFgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiFgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;-><init>(Lcom/android/server/MiuiBatteryAuthentic;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mHandler:Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mContentResolver:Landroid/content/ContentResolver;

    .line 97
    new-instance v0, Lcom/android/server/MiuiBatteryAuthentic$IMTService;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryAuthentic$IMTService;-><init>(Lcom/android/server/MiuiBatteryAuthentic;)V

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mIMTService:Lcom/android/server/MiuiBatteryAuthentic$IMTService;

    .line 98
    invoke-direct {p0}, Lcom/android/server/MiuiBatteryAuthentic;->initBatteryAuthenticCertificate()V

    .line 100
    new-instance v0, Lcom/android/server/MiuiBatteryAuthentic$1;

    invoke-direct {v0, p0}, Lcom/android/server/MiuiBatteryAuthentic$1;-><init>(Lcom/android/server/MiuiBatteryAuthentic;)V

    .line 112
    .local v0, "receiver":Landroid/content/BroadcastReceiver;
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 113
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.provision.action.PROVISION_COMPLETE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 114
    const/4 v2, 0x2

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 115
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/server/MiuiBatteryAuthentic;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 83
    sget-object v0, Lcom/android/server/MiuiBatteryAuthentic;->INSTANCE:Lcom/android/server/MiuiBatteryAuthentic;

    if-nez v0, :cond_1

    .line 84
    const-class v0, Lcom/android/server/MiuiBatteryAuthentic;

    monitor-enter v0

    .line 85
    :try_start_0
    sget-object v1, Lcom/android/server/MiuiBatteryAuthentic;->INSTANCE:Lcom/android/server/MiuiBatteryAuthentic;

    if-nez v1, :cond_0

    .line 86
    new-instance v1, Lcom/android/server/MiuiBatteryAuthentic;

    invoke-direct {v1, p0}, Lcom/android/server/MiuiBatteryAuthentic;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/MiuiBatteryAuthentic;->INSTANCE:Lcom/android/server/MiuiBatteryAuthentic;

    .line 88
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 90
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/MiuiBatteryAuthentic;->INSTANCE:Lcom/android/server/MiuiBatteryAuthentic;

    return-object v0
.end method

.method private initBatteryAuthenticCertificate()V
    .locals 4

    .line 118
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "battery_authentic_certificate"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mCloudSign:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mHandler:Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    invoke-virtual {v0}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->getBatterySn()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mBatterySn:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mCloudSign:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mHandler:Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    goto :goto_0

    .line 123
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/MiuiBatteryAuthentic;->mIsVerified:Z

    .line 124
    iget-object v3, p0, Lcom/android/server/MiuiBatteryAuthentic;->mHandler:Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;

    invoke-virtual {v3, v0, v1, v2}, Lcom/android/server/MiuiBatteryAuthentic$BatteryAuthenticHandler;->sendMessageDelayed(IJ)V

    .line 126
    :goto_0
    return-void
.end method
