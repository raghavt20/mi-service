.class Lcom/android/server/MiuiBatteryStatsService$TrackBatteryUsbInfo;
.super Ljava/lang/Object;
.source "MiuiBatteryStatsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiBatteryStatsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TrackBatteryUsbInfo"
.end annotation


# static fields
.field public static final ACTION_TRACK_EVENT:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field public static final ANALYTICS_PACKAGE:Ljava/lang/String; = "com.miui.analytics"

.field public static final APP_TIME:Ljava/lang/String; = "top3_app"

.field public static final AVETEMP:Ljava/lang/String; = "ave_temp"

.field public static final BATTERY_APP_ID:Ljava/lang/String; = "31000000094"

.field public static final BATTERY_CHARGE_ACTION_EVENT:Ljava/lang/String; = "charge_action"

.field public static final BATTERY_CHARGE_EVENT:Ljava/lang/String; = "charge"

.field public static final BATTERY_HEALTH_EVENT:Ljava/lang/String; = "battery_health"

.field public static final BATTERY_HIGH_TEMP_VOLTAGE:Ljava/lang/String; = "battery_high_temp_voltage"

.field public static final BATTERY_LPD_COUNT_EVENT:Ljava/lang/String; = "lpd_count"

.field public static final BATTERY_LPD_INFOMATION:Ljava/lang/String; = "battery_lpd_infomation"

.field public static final BATTERY_POWER_OFF_EVENT:Ljava/lang/String; = "power_off"

.field public static final BATTERY_TEMP_EVENT:Ljava/lang/String; = "battery_temp"

.field public static final BATT_AUTH:Ljava/lang/String; = "battery_authentic"

.field public static final BATT_RESISTANCE:Ljava/lang/String; = "resistance"

.field public static final BATT_THERMAL_LEVEL:Ljava/lang/String; = "thermal_level"

.field public static final CAPACITY_CHANGE_VALUE:Ljava/lang/String; = "capacity_change_value"

.field public static final CAPACITY_NONLINEAR_CHANGE_EVENT:Ljava/lang/String; = "nonlinear_change_of_capacity"

.field public static final CAPCAITY:Ljava/lang/String; = "capacity"

.field public static final CC_SHORT_VBUS:Ljava/lang/String; = "cc_short_vbus"

.field public static final CHARGE_BAT_MAX_TEMP:Ljava/lang/String; = "charge_battery_max_temp"

.field public static final CHARGE_BAT_MIN_TEMP:Ljava/lang/String; = "charge_battery_min_temp"

.field public static final CHARGE_END_CAPACITY:Ljava/lang/String; = "charge_end_capacity"

.field public static final CHARGE_END_TIME:Ljava/lang/String; = "charge_end_time"

.field public static final CHARGE_FULL:Ljava/lang/String; = "charger_full"

.field public static final CHARGE_POWER:Ljava/lang/String; = "charge_power"

.field public static final CHARGE_START_CAPACITY:Ljava/lang/String; = "charge_start_capacity"

.field public static final CHARGE_START_TIME:Ljava/lang/String; = "charge_start_time"

.field public static final CHARGE_STATUS:Ljava/lang/String; = "charge_status"

.field public static final CHARGE_TOTAL_TIME:Ljava/lang/String; = "charge_total_time"

.field public static final CHARGE_TYPE:Ljava/lang/String; = "charger_type"

.field public static final CYCLE_COUNT:Ljava/lang/String; = "cyclecount"

.field public static final DATA_UNLOCK:Ljava/lang/String; = "data_unlock"

.field public static final FLAG_NON_ANONYMOUS:I = 0x2

.field public static final FULL_CHARGE_END_TIME:Ljava/lang/String; = "full_charge_end_time"

.field public static final FULL_CHARGE_START_TIME:Ljava/lang/String; = "full_charge_start_time"

.field public static final FULL_CHARGE_TOTAL_TIME:Ljava/lang/String; = "full_charge_total_time"

.field public static final HIGH_TEMP_TIME:Ljava/lang/String; = "high_temp_time"

.field public static final HIGH_TEMP_VOLTAGE_TIME:Ljava/lang/String; = "high_temp_voltage_time"

.field public static final HIGH_VOLTAGE_TIME:Ljava/lang/String; = "high_voltage_time"

.field public static final IBAT:Ljava/lang/String; = "ibat"

.field public static final INTERMITTENT_CHARGE:Ljava/lang/String; = "intermittent_charge"

.field public static final INTERMITTENT_CHARGE_EVENT:Ljava/lang/String; = "intermittent_charge"

.field public static final LPD_COUNT:Ljava/lang/String; = "lpd_count"

.field public static final LPD_DM_RES:Ljava/lang/String; = "lpd_dm_res"

.field public static final LPD_DP_RES:Ljava/lang/String; = "lpd_dp_res"

.field public static final LPD_SBU1_RES:Ljava/lang/String; = "lpd_sbu1_res"

.field public static final LPD_SBU2_RES:Ljava/lang/String; = "lpd_sbu2_res"

.field public static final MAXTEMP:Ljava/lang/String; = "max_temp"

.field public static final MINTEMP:Ljava/lang/String; = "min_temp"

.field public static final NOT_FULLY_CHARGED:Ljava/lang/String; = "not_fully_charged"

.field public static final NOT_FULLY_CHARGED_EVENT:Ljava/lang/String; = "not_fully_charged"

.field public static final PARAM_APP_ID:Ljava/lang/String; = "APP_ID"

.field public static final PARAM_EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field public static final PARAM_PACKAGE:Ljava/lang/String; = "PACKAGE"

.field public static final PD_APDO_MAX:Ljava/lang/String; = "pd_apdoMax"

.field public static final PD_AUTHENTICATION:Ljava/lang/String; = "pd_authentication"

.field public static final SCENE:Ljava/lang/String; = "battery_charge_discharge_state"

.field public static final SCREEN_OFF_TIME:Ljava/lang/String; = "screen_off_time"

.field public static final SCREEN_ON_TIME:Ljava/lang/String; = "screen_on_time"

.field public static final SERIES_DELTA_VOLTAGE:Ljava/lang/String; = "series_delta_voltage"

.field public static final SERIES_DELTA_VOLTAGE_EVENT:Ljava/lang/String; = "series_delta_voltage"

.field public static final SHUTDOWN_DELAY:Ljava/lang/String; = "shutdown_delay"

.field public static final SLOW_CHARGE_EVENT:Ljava/lang/String; = "slow_charge"

.field public static final SLOW_CHARGE_TYPE:Ljava/lang/String; = "slow_charge_type"

.field public static final SOH:Ljava/lang/String; = "soh"

.field public static final TBAT:Ljava/lang/String; = "Tbat"

.field public static final TX_ADAPTER:Ljava/lang/String; = "tx_adapter"

.field public static final TX_UUID:Ljava/lang/String; = "tx_uuid"

.field public static final USB32:Ljava/lang/String; = "USB32"

.field public static final USB_APP_ID:Ljava/lang/String; = "31000000092"

.field public static final USB_CURRENT:Ljava/lang/String; = "usb_current"

.field public static final USB_DEVICE_EVENT:Ljava/lang/String; = "usb_deivce"

.field public static final USB_FUNCTION:Ljava/lang/String; = "usb_function"

.field public static final USB_VOLTAGE:Ljava/lang/String; = "usb_voltage"

.field public static final VBAT:Ljava/lang/String; = "vbat"

.field public static final VBUS_DISABLE:Ljava/lang/String; = "vbus_disable"

.field public static final VBUS_DISABLE_EVENT:Ljava/lang/String; = "vbus_disable"

.field public static final WIRELESS_COMPOSITE:Ljava/lang/String; = "wireless_composite"

.field public static final WIRELESS_COMPOSITE_EVENT:Ljava/lang/String; = "wireless_composite"

.field public static final WIRELESS_REVERSE_CHARGE:Ljava/lang/String; = "wireless_reverse_enable"

.field public static final WIRELESS_REVERSE_CHARGE_EVENT:Ljava/lang/String; = "wireless_reverse_charge"


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiBatteryStatsService;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiBatteryStatsService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiBatteryStatsService;

    .line 1835
    iput-object p1, p0, Lcom/android/server/MiuiBatteryStatsService$TrackBatteryUsbInfo;->this$0:Lcom/android/server/MiuiBatteryStatsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
