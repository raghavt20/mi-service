.class public abstract Lcom/android/server/FixedFileObserver;
.super Ljava/lang/Object;
.source "FixedFileObserver.java"


# static fields
.field private static final sObserverLists:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/io/File;",
            "Ljava/util/Set<",
            "Lcom/android/server/FixedFileObserver;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mMask:I

.field private mObserver:Landroid/os/FileObserver;

.field private final mRootPath:Ljava/io/File;


# direct methods
.method static bridge synthetic -$$Nest$fgetmMask(Lcom/android/server/FixedFileObserver;)I
    .locals 0

    iget p0, p0, Lcom/android/server/FixedFileObserver;->mMask:I

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/server/FixedFileObserver;->sObserverLists:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .line 19
    const/16 v0, 0xfff

    invoke-direct {p0, p1, v0}, Lcom/android/server/FixedFileObserver;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mask"    # I

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/FixedFileObserver;->mRootPath:Ljava/io/File;

    .line 22
    iput p2, p0, Lcom/android/server/FixedFileObserver;->mMask:I

    .line 23
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 0

    .line 55
    invoke-virtual {p0}, Lcom/android/server/FixedFileObserver;->stopWatching()V

    return-void
.end method

.method public abstract onEvent(ILjava/lang/String;)V
.end method

.method public startWatching()V
    .locals 4

    .line 28
    sget-object v0, Lcom/android/server/FixedFileObserver;->sObserverLists:Ljava/util/HashMap;

    monitor-enter v0

    .line 29
    :try_start_0
    iget-object v1, p0, Lcom/android/server/FixedFileObserver;->mRootPath:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/FixedFileObserver;->mRootPath:Ljava/io/File;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    :cond_0
    iget-object v1, p0, Lcom/android/server/FixedFileObserver;->mRootPath:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 33
    .local v1, "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/FixedFileObserver;

    iget-object v2, v2, Lcom/android/server/FixedFileObserver;->mObserver:Landroid/os/FileObserver;

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/android/server/FixedFileObserver$1;

    iget-object v3, p0, Lcom/android/server/FixedFileObserver;->mRootPath:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, v1}, Lcom/android/server/FixedFileObserver$1;-><init>(Lcom/android/server/FixedFileObserver;Ljava/lang/String;Ljava/util/Set;)V

    :goto_0
    iput-object v2, p0, Lcom/android/server/FixedFileObserver;->mObserver:Landroid/os/FileObserver;

    .line 38
    invoke-virtual {v2}, Landroid/os/FileObserver;->startWatching()V

    .line 39
    invoke-interface {v1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    nop

    .end local v1    # "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
    monitor-exit v0

    .line 41
    return-void

    .line 40
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public stopWatching()V
    .locals 3

    .line 44
    sget-object v0, Lcom/android/server/FixedFileObserver;->sObserverLists:Ljava/util/HashMap;

    monitor-enter v0

    .line 45
    :try_start_0
    iget-object v1, p0, Lcom/android/server/FixedFileObserver;->mRootPath:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 46
    .local v1, "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/server/FixedFileObserver;->mObserver:Landroid/os/FileObserver;

    if-nez v2, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 49
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/FixedFileObserver;->mObserver:Landroid/os/FileObserver;

    invoke-virtual {v2}, Landroid/os/FileObserver;->stopWatching()V

    .line 51
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/FixedFileObserver;->mObserver:Landroid/os/FileObserver;

    .line 52
    .end local v1    # "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
    monitor-exit v0

    .line 53
    return-void

    .line 46
    .restart local v1    # "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
    :cond_2
    :goto_0
    monitor-exit v0

    return-void

    .line 52
    .end local v1    # "fixedObservers":Ljava/util/Set;, "Ljava/util/Set<Lcom/android/server/FixedFileObserver;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
