class com.android.server.MiuiUiModeManagerServiceStubImpl$2 extends android.database.ContentObserver {
	 /* .source "MiuiUiModeManagerServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->registUIModeScaleChangeObserver(Lcom/android/server/UiModeManagerService;Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiUiModeManagerServiceStubImpl this$0; //synthetic
final android.content.Context val$context; //synthetic
final com.android.server.UiModeManagerService val$service; //synthetic
/* # direct methods */
 com.android.server.MiuiUiModeManagerServiceStubImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiUiModeManagerServiceStubImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 151 */
this.this$0 = p1;
this.val$context = p3;
this.val$service = p4;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 154 */
v0 = this.val$context;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "ui_mode_scale" */
int v2 = 1; // const/4 v2, 0x1
v0 = android.provider.Settings$System .getInt ( v0,v1,v2 );
/* .line 156 */
/* .local v0, "uiModeType":I */
v1 = com.android.server.UiModeManagerServiceProxy.mDefaultUiModeType;
v2 = this.val$service;
(( com.xiaomi.reflect.RefInt ) v1 ).set ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/xiaomi/reflect/RefInt;->set(Ljava/lang/Object;I)V
/* .line 157 */
v1 = this.this$0;
com.android.server.MiuiUiModeManagerServiceStubImpl .-$$Nest$fgetmUiModeManagerService ( v1 );
(( com.android.server.UiModeManagerService ) v1 ).getInnerLock ( ); // invoke-virtual {v1}, Lcom/android/server/UiModeManagerService;->getInnerLock()Ljava/lang/Object;
/* monitor-enter v1 */
/* .line 158 */
try { // :try_start_0
	 v2 = this.val$service;
	 /* iget-boolean v2, v2, Lcom/android/server/UiModeManagerService;->mSystemReady:Z */
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 159 */
		 v2 = this.val$service;
		 int v3 = 0; // const/4 v3, 0x0
		 (( com.android.server.UiModeManagerService ) v2 ).updateLocked ( v3, v3 ); // invoke-virtual {v2, v3, v3}, Lcom/android/server/UiModeManagerService;->updateLocked(II)V
		 /* .line 161 */
	 } // :cond_0
	 /* monitor-exit v1 */
	 /* .line 162 */
	 return;
	 /* .line 161 */
	 /* :catchall_0 */
	 /* move-exception v2 */
	 /* monitor-exit v1 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v2 */
} // .end method
