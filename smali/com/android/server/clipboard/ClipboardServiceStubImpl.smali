.class public Lcom/android/server/clipboard/ClipboardServiceStubImpl;
.super Lcom/android/server/clipboard/ClipboardServiceStub;
.source "ClipboardServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.clipboard.ClipboardServiceStub$$"
.end annotation


# static fields
.field private static final IS_SUPPORT_SUPER_CLIPBOARD:Z = true

.field private static final MIUI_INPUT_NO_NEED_SHOW_POP:Ljava/lang/String; = "miui_input_no_need_show_pop"

.field private static final TAG:Ljava/lang/String; = "ClipboardServiceI"

.field private static final sSuperClipboardPkgList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sWebViewWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAppOps:Landroid/app/AppOpsManager;

.field private mContext:Landroid/content/Context;

.field private mIGreezeManager:Lmiui/greeze/IGreezeManager;

.field private mService:Lcom/android/server/clipboard/ClipboardService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->sSuperClipboardPkgList:Ljava/util/List;

    .line 59
    const-string v1, "com.android.mms"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    const-string v1, "com.miui.notes"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    const-string v1, "com.miui.screenshot"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    const-string v1, "com.android.browser"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    const-string v1, "com.android.fileexplorer"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    const-string v1, "com.miui.gallery"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    const-string v1, "com.android.email"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->sWebViewWhiteList:Ljava/util/List;

    .line 72
    const-string v1, "com.yinxiang"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    const-string v1, "com.evernote"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    const-string v1, "com.youdao.note"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    const-string v1, "com.flomo.app"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    const-string v1, "com.yuque.mobile.android.app"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    const-string v1, "com.denglin.moji"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    const-string v1, "com.zhihu.android"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    const-string v1, "cn.wps.moffice_eng"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 45
    invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStub;-><init>()V

    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    return-void
.end method

.method private checkPermissionPkg()V
    .locals 4

    .line 239
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerServiceStub;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "callingPackageName":Ljava/lang/String;
    const-string v1, "com.lbe.security.miui"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    return-void

    .line 241
    :cond_0
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Permission Denial: attempt to assess internal clipboard from pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 242
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pkg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private firstClipData(Landroid/content/ClipData;)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "clipData"    # Landroid/content/ClipData;

    .line 252
    const/4 v0, 0x0

    .line 253
    .local v0, "paste":Ljava/lang/CharSequence;
    if-eqz p1, :cond_2

    .line 254
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    .line 256
    .local v1, "identity":J
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 257
    invoke-virtual {p1, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v4

    .line 258
    if-eqz v0, :cond_0

    .line 259
    goto :goto_1

    .line 256
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 263
    .end local v3    # "i":I
    :cond_1
    :goto_1
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 264
    goto :goto_2

    .line 263
    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 264
    throw v3

    .line 266
    .end local v1    # "identity":J
    :cond_2
    :goto_2
    return-object v0
.end method

.method private getGreeze()Lmiui/greeze/IGreezeManager;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    if-nez v0, :cond_0

    .line 205
    nop

    .line 206
    const-string v0, "greezer"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 205
    invoke-static {v0}, Lmiui/greeze/IGreezeManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/greeze/IGreezeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mIGreezeManager:Lmiui/greeze/IGreezeManager;

    return-object v0
.end method

.method private modifyClipDataForWebView(Landroid/content/ClipData;)Landroid/content/ClipData;
    .locals 4
    .param p1, "clipData"    # Landroid/content/ClipData;

    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 384
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 385
    invoke-virtual {p1, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 386
    .local v2, "text":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, ""

    goto :goto_1

    :cond_0
    move-object v3, v2

    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 384
    .end local v2    # "text":Ljava/lang/CharSequence;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 388
    .end local v1    # "i":I
    :cond_1
    new-instance v1, Landroid/content/ClipData$Item;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    .line 389
    .local v1, "item":Landroid/content/ClipData$Item;
    new-instance v2, Landroid/content/ClipData;

    invoke-virtual {p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/content/ClipData;-><init>(Landroid/content/ClipDescription;Landroid/content/ClipData$Item;)V

    return-object v2
.end method

.method private needModifyClipDataForWebView(Ljava/lang/String;Landroid/content/ClipData;)Z
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "clipData"    # Landroid/content/ClipData;

    .line 370
    sget-object v0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->sWebViewWhiteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 371
    return v1

    .line 373
    :cond_0
    invoke-virtual {p2}, Landroid/content/ClipData;->getItemCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    move v1, v2

    :cond_1
    return v1
.end method

.method private notifyGreeze(I)V
    .locals 4
    .param p1, "uid"    # I

    .line 211
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 212
    .local v0, "uids":[I
    const/4 v1, 0x0

    aput p1, v0, v1

    .line 214
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->getGreeze()Lmiui/greeze/IGreezeManager;

    move-result-object v1

    invoke-interface {v1, p1}, Lmiui/greeze/IGreezeManager;->isUidFrozen(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->getGreeze()Lmiui/greeze/IGreezeManager;

    move-result-object v1

    const-string v2, "clip"

    const/16 v3, 0x3e8

    invoke-interface {v1, v0, v3, v2}, Lmiui/greeze/IGreezeManager;->thawUids([IILjava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :cond_0
    goto :goto_0

    .line 216
    :catch_0
    move-exception v1

    .line 217
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "ClipboardServiceI"

    const-string v3, "ClipGreez err:"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private removeActivePermissionOwnerForPackage(Ljava/lang/String;I)V
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 348
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mService:Lcom/android/server/clipboard/ClipboardService;

    iget-object v1, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getDeviceId()I

    move-result v1

    invoke-virtual {v0, p2, v1}, Lcom/android/server/clipboard/ClipboardService;->getClipboardLocked(II)Lcom/android/server/clipboard/ClipboardService$Clipboard;

    move-result-object v0

    .line 349
    .local v0, "clipboard":Lcom/android/server/clipboard/ClipboardService$Clipboard;
    iget-object v1, v0, Lcom/android/server/clipboard/ClipboardService$Clipboard;->mPrimaryClipPackage:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351
    iget-object v1, v0, Lcom/android/server/clipboard/ClipboardService$Clipboard;->activePermissionOwners:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    goto :goto_0

    .line 354
    :cond_0
    iget-object v1, v0, Lcom/android/server/clipboard/ClipboardService$Clipboard;->activePermissionOwners:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 356
    :goto_0
    return-void
.end method


# virtual methods
.method public checkProviderWakePathForClipboard(Ljava/lang/String;ILandroid/content/pm/ProviderInfo;I)Z
    .locals 15
    .param p1, "callerPkg"    # Ljava/lang/String;
    .param p2, "callingUid"    # I
    .param p3, "providerInfo"    # Landroid/content/pm/ProviderInfo;
    .param p4, "userId"    # I

    .line 272
    move-object v0, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    .line 273
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_6

    if-eqz v9, :cond_6

    iget-object v1, v9, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    .line 275
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move/from16 v10, p4

    goto/16 :goto_1

    .line 279
    :cond_0
    iget-object v1, v0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mService:Lcom/android/server/clipboard/ClipboardService;

    iget-object v3, v0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getDeviceId()I

    move-result v3

    move/from16 v10, p4

    invoke-virtual {v1, v10, v3}, Lcom/android/server/clipboard/ClipboardService;->getClipboardLocked(II)Lcom/android/server/clipboard/ClipboardService$Clipboard;

    move-result-object v11

    .line 280
    .local v11, "clipboard":Lcom/android/server/clipboard/ClipboardService$Clipboard;
    iget-object v1, v11, Lcom/android/server/clipboard/ClipboardService$Clipboard;->primaryClip:Landroid/content/ClipData;

    const-string v3, "ClipboardServiceI"

    if-nez v1, :cond_1

    .line 281
    const-string v1, "checkProviderWakePathForClipboard: primaryClip is null"

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    return v2

    .line 285
    :cond_1
    iget-object v1, v11, Lcom/android/server/clipboard/ClipboardService$Clipboard;->activePermissionOwners:Ljava/util/HashSet;

    invoke-virtual {v1, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkProviderWakePathForClipboard: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "is not a activePermissionOwner"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    return v2

    .line 291
    :cond_2
    sget-object v1, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->sSuperClipboardPkgList:Ljava/util/List;

    iget-object v4, v9, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    const-string v4, "checkProviderWakePathForClipboard: Package "

    if-nez v1, :cond_3

    .line 292
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, v9, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is not in the white list"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    return v2

    .line 297
    :cond_3
    iget-object v1, v11, Lcom/android/server/clipboard/ClipboardService$Clipboard;->primaryClip:Landroid/content/ClipData;

    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v12

    .line 299
    .local v12, "count":I
    const/4 v1, 0x0

    move v13, v1

    .local v13, "i":I
    :goto_0
    if-ge v13, v12, :cond_5

    .line 300
    iget-object v1, v11, Lcom/android/server/clipboard/ClipboardService$Clipboard;->primaryClip:Landroid/content/ClipData;

    invoke-virtual {v1, v13}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v14

    .line 301
    .local v14, "uri":Landroid/net/Uri;
    if-eqz v14, :cond_4

    iget-object v1, v9, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    invoke-virtual {v14}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 302
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can to waked by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v1

    iget-object v3, v9, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    const/4 v4, 0x4

    .line 306
    invoke-static/range {p2 .. p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    const/4 v7, 0x1

    .line 304
    move-object/from16 v2, p1

    move/from16 v6, p4

    invoke-virtual/range {v1 .. v7}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 307
    const/4 v1, 0x1

    return v1

    .line 299
    :cond_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 310
    .end local v13    # "i":I
    .end local v14    # "uri":Landroid/net/Uri;
    :cond_5
    return v2

    .line 273
    .end local v11    # "clipboard":Lcom/android/server/clipboard/ClipboardService$Clipboard;
    .end local v12    # "count":I
    :cond_6
    move/from16 v10, p4

    .line 276
    :goto_1
    return v2
.end method

.method public clipboardAccessResult(Landroid/content/ClipData;Ljava/lang/String;IIIZ)I
    .locals 20
    .param p1, "clip"    # Landroid/content/ClipData;
    .param p2, "callerPkg"    # Ljava/lang/String;
    .param p3, "callerUid"    # I
    .param p4, "callerUserId"    # I
    .param p5, "primaryClipUid"    # I
    .param p6, "userCall"    # Z

    .line 92
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v8, p2

    move/from16 v9, p3

    move/from16 v10, p5

    invoke-direct {v0, v10}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->notifyGreeze(I)V

    .line 93
    invoke-direct/range {p0 .. p1}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->firstClipData(Landroid/content/ClipData;)Ljava/lang/CharSequence;

    move-result-object v11

    .line 94
    .local v11, "firstClipData":Ljava/lang/CharSequence;
    const/4 v12, 0x0

    const/4 v13, 0x1

    if-eqz v11, :cond_1

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v12

    goto :goto_1

    :cond_1
    :goto_0
    move v2, v13

    :goto_1
    move v14, v2

    .line 95
    .local v14, "firstClipDataEmpty":Z
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_13

    if-nez v14, :cond_13

    if-nez p6, :cond_13

    if-eq v10, v9, :cond_13

    .line 96
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v2

    iget-object v3, v0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/server/clipboard/ClipboardChecker;->isAiClipboardEnable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    move/from16 v15, p4

    goto/16 :goto_8

    .line 104
    :cond_2
    iget-object v2, v0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "default_input_method"

    move/from16 v15, p4

    invoke-static {v2, v3, v15}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v16

    .line 106
    .local v16, "defaultIme":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 107
    invoke-static/range {v16 .. v16}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "imePkg":Ljava/lang/String;
    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 109
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v3

    invoke-virtual {v3, v13, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 110
    return v12

    .line 113
    .end local v2    # "imePkg":Ljava/lang/String;
    :cond_3
    iget-object v2, v0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mAppOps:Landroid/app/AppOpsManager;

    const/16 v3, 0x1d

    invoke-virtual {v2, v3, v9, v8}, Landroid/app/AppOpsManager;->checkOp(IILjava/lang/String;)I

    move-result v7

    .line 114
    .local v7, "resultMode":I
    invoke-static/range {p3 .. p3}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v2

    const/16 v3, 0x2710

    if-ge v2, v3, :cond_4

    move v2, v13

    goto :goto_2

    :cond_4
    move v2, v12

    :goto_2
    move/from16 v17, v2

    .line 115
    .local v17, "isSystem":Z
    if-nez v17, :cond_6

    .line 116
    const-class v2, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v18, v2

    check-cast v18, Landroid/content/pm/PackageManagerInternal;

    .line 117
    .local v18, "pmi":Landroid/content/pm/PackageManagerInternal;
    const-wide/16 v4, 0x0

    const/16 v6, 0x3e8

    .line 118
    invoke-static/range {p3 .. p3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v19

    .line 117
    move-object/from16 v2, v18

    move-object/from16 v3, p2

    move v12, v7

    .end local v7    # "resultMode":I
    .local v12, "resultMode":I
    move/from16 v7, v19

    invoke-virtual/range {v2 .. v7}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 119
    .local v2, "appInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v2, :cond_5

    iget v3, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v3, v13

    if-eqz v3, :cond_5

    move v3, v13

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    :goto_3
    move/from16 v17, v3

    goto :goto_4

    .line 115
    .end local v2    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v12    # "resultMode":I
    .end local v18    # "pmi":Landroid/content/pm/PackageManagerInternal;
    .restart local v7    # "resultMode":I
    :cond_6
    move v12, v7

    .line 123
    .end local v7    # "resultMode":I
    .restart local v12    # "resultMode":I
    :goto_4
    const/4 v7, 0x5

    if-ne v12, v7, :cond_9

    if-nez v17, :cond_9

    .line 124
    invoke-virtual/range {p1 .. p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v2

    .line 125
    .local v2, "description":Landroid/content/ClipDescription;
    if-eqz v2, :cond_7

    .line 126
    const-string v3, "image/*"

    invoke-virtual {v2, v3}, Landroid/content/ClipDescription;->filterMimeTypes(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 127
    .local v3, "imageMimeTypes":[Ljava/lang/String;
    if-eqz v3, :cond_7

    .line 128
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v9, v8, v7}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 130
    return v13

    .line 134
    .end local v3    # "imageMimeTypes":[Ljava/lang/String;
    :cond_7
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    if-ge v3, v4, :cond_9

    .line 135
    invoke-virtual {v1, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    .line 136
    .local v4, "item":Landroid/content/ClipData$Item;
    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 137
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v9, v8, v7}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 139
    return v13

    .line 134
    .end local v4    # "item":Landroid/content/ClipData$Item;
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 144
    .end local v2    # "description":Landroid/content/ClipDescription;
    .end local v3    # "i":I
    :cond_9
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->hashCode()I

    move-result v6

    move-object/from16 v3, p2

    move/from16 v4, p3

    move-object v5, v11

    move v13, v7

    move/from16 v7, v17

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/clipboard/ClipboardChecker;->matchClipboardRule(Ljava/lang/String;ILjava/lang/CharSequence;IZ)I

    move-result v2

    .line 145
    .local v2, "matchResult":I
    const/4 v3, 0x3

    const/16 v4, 0x3e8

    const-string v5, "ClipboardServiceI"

    if-eqz v12, :cond_e

    const/4 v6, 0x4

    if-ne v12, v6, :cond_a

    goto :goto_6

    .line 164
    :cond_a
    if-ne v10, v4, :cond_b

    .line 165
    invoke-virtual/range {p1 .. p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 166
    invoke-virtual/range {p1 .. p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    const-string v6, "miui_input_no_need_show_pop"

    invoke-static {v6, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 167
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 168
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MIUILOG- Permission Denied when read clipboard [CloudSet], caller "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    const/4 v3, 0x1

    return v3

    .line 172
    :cond_b
    if-nez v2, :cond_c

    .line 174
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/android/server/clipboard/ClipboardChecker;->updateClipItemData(Landroid/content/ClipData;)V

    .line 175
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 176
    const/4 v6, 0x0

    return v6

    .line 177
    :cond_c
    const/4 v4, 0x1

    const/4 v6, 0x0

    if-ne v2, v4, :cond_d

    .line 179
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v3

    invoke-virtual {v3, v6, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 180
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MIUILOG- Permission Denied when read clipboard [Mismatch], caller "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v3, 0x1

    return v3

    .line 183
    :cond_d
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v4

    invoke-virtual {v4, v9, v1}, Lcom/android/server/clipboard/ClipboardChecker;->stashClip(ILandroid/content/ClipData;)V

    .line 184
    return v3

    .line 146
    :cond_e
    :goto_6
    if-eq v9, v4, :cond_12

    if-eqz v17, :cond_12

    .line 148
    if-ne v2, v3, :cond_f

    .line 149
    const/4 v3, 0x0

    return v3

    .line 151
    :cond_f
    const/4 v3, 0x0

    sget-boolean v4, Lmiui/os/Build;->IS_DEVELOPMENT_VERSION:Z

    if-eqz v4, :cond_11

    if-nez v2, :cond_10

    const/4 v3, 0x1

    goto :goto_7

    .line 155
    :cond_10
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v4

    invoke-virtual {v4, v3, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MIUILOG- Permission Denied when system app read clipboard, caller "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const/4 v3, 0x1

    return v3

    .line 151
    :cond_11
    const/4 v3, 0x1

    .line 152
    :goto_7
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v4

    invoke-virtual {v4, v3, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 153
    const/4 v4, 0x0

    return v4

    .line 146
    :cond_12
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 160
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v5

    invoke-virtual {v5, v3, v9, v8, v3}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 161
    return v4

    .line 95
    .end local v2    # "matchResult":I
    .end local v12    # "resultMode":I
    .end local v16    # "defaultIme":Ljava/lang/String;
    .end local v17    # "isSystem":Z
    :cond_13
    move/from16 v15, p4

    .line 97
    :goto_8
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_14

    if-nez v14, :cond_16

    if-eqz p6, :cond_16

    .line 98
    :cond_14
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v2

    .line 99
    if-eqz p6, :cond_15

    const/4 v3, 0x6

    goto :goto_9

    :cond_15
    const/4 v3, 0x1

    .line 98
    :goto_9
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v9, v8, v3}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V

    .line 101
    :cond_16
    const/4 v2, 0x0

    return v2
.end method

.method public getClipboardClickRuleData()Ljava/util/Map;
    .locals 1

    .line 248
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/clipboard/ClipboardChecker;->getClipboardClickTrack()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getStashClipboardData()Landroid/content/ClipData;
    .locals 1

    .line 228
    invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->checkPermissionPkg()V

    .line 229
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/clipboard/ClipboardChecker;->getClipItemData()Landroid/content/ClipData;

    move-result-object v0

    return-object v0
.end method

.method public init(Lcom/android/server/clipboard/ClipboardService;Landroid/app/AppOpsManager;)V
    .locals 2
    .param p1, "clipboardService"    # Lcom/android/server/clipboard/ClipboardService;
    .param p2, "appOps"    # Landroid/app/AppOpsManager;

    .line 84
    iput-object p1, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mService:Lcom/android/server/clipboard/ClipboardService;

    .line 85
    invoke-virtual {p1}, Lcom/android/server/clipboard/ClipboardService;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mContext:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mAppOps:Landroid/app/AppOpsManager;

    .line 87
    const-string v0, "persist.sys.support_super_clipboard"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method public isUidFocused(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 223
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/clipboard/ClipboardChecker;->hasStash(I)Z

    move-result v0

    return v0
.end method

.method public modifyClipDataIfNeed(Ljava/lang/String;Landroid/content/ClipData;)Landroid/content/ClipData;
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "clipData"    # Landroid/content/ClipData;

    .line 360
    invoke-direct {p0, p1, p2}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->needModifyClipDataForWebView(Ljava/lang/String;Landroid/content/ClipData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-direct {p0, p2}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->modifyClipDataForWebView(Landroid/content/ClipData;)Landroid/content/ClipData;

    move-result-object v0

    return-object v0

    .line 363
    :cond_0
    return-object p2
.end method

.method public onPackageUriPermissionRemoved(Ljava/lang/String;I)V
    .locals 6
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 316
    const-string v0, "ClipboardServiceI"

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 317
    const-string v1, "onPackageUriPermissionRemoved: package is empty"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    return-void

    .line 321
    :cond_0
    const/16 v1, -0x2710

    if-ne p2, v1, :cond_1

    .line 322
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPackageUriPermissionRemoved: Attempt to remove active permission owner for invalid user: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    return-void

    .line 327
    :cond_1
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    .line 328
    .local v1, "am":Landroid/app/IActivityManager;
    const/4 v2, -0x1

    if-ne p2, v2, :cond_3

    .line 329
    invoke-interface {v1}, Landroid/app/IActivityManager;->getRunningUserIds()[I

    move-result-object v2

    .line 330
    .local v2, "runningUserIds":[I
    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_2

    aget v5, v2, v4

    .line 331
    .local v5, "id":I
    invoke-direct {p0, p1, v5}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->removeActivePermissionOwnerForPackage(Ljava/lang/String;I)V

    .line 330
    .end local v5    # "id":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 333
    :cond_2
    return-void

    .line 336
    .end local v2    # "runningUserIds":[I
    :cond_3
    const/4 v2, -0x2

    if-eq p2, v2, :cond_5

    const/4 v2, -0x3

    if-ne p2, v2, :cond_4

    goto :goto_1

    .line 341
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->removeActivePermissionOwnerForPackage(Ljava/lang/String;I)V

    .line 344
    .end local v1    # "am":Landroid/app/IActivityManager;
    goto :goto_2

    .line 337
    .restart local v1    # "am":Landroid/app/IActivityManager;
    :cond_5
    :goto_1
    invoke-interface {v1}, Landroid/app/IActivityManager;->getCurrentUserId()I

    move-result v2

    invoke-direct {p0, p1, v2}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->removeActivePermissionOwnerForPackage(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    return-void

    .line 342
    .end local v1    # "am":Landroid/app/IActivityManager;
    :catch_0
    move-exception v1

    .line 343
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPackageUriPermissionRemoved: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method

.method public updateClipboardRuleData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/ClipboardRuleInfo;",
            ">;)V"
        }
    .end annotation

    .line 234
    .local p1, "ruleInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ClipboardRuleInfo;>;"
    invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->checkPermissionPkg()V

    .line 235
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/clipboard/ClipboardChecker;->updateClipboardPatterns(Ljava/util/List;)V

    .line 236
    return-void
.end method

.method public waitUserChoice(Ljava/lang/String;I)Landroid/content/ClipData;
    .locals 2
    .param p1, "callerPkg"    # Ljava/lang/String;
    .param p2, "callerUid"    # I

    .line 189
    const-string v0, "ai_read_clipboard"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    .line 191
    :try_start_0
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->mAppOps:Landroid/app/AppOpsManager;

    const/16 v1, 0x1d

    invoke-virtual {v0, v1, p2, p1}, Landroid/app/AppOpsManager;->noteOp(IILjava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 192
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/clipboard/ClipboardChecker;->getStashClip(I)Landroid/content/ClipData;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 196
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/server/clipboard/ClipboardChecker;->removeStashClipLater(I)V

    .line 192
    return-object v0

    .line 195
    :cond_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 196
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/clipboard/ClipboardChecker;->removeStashClipLater(I)V

    .line 197
    nop

    .line 198
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->EMPTY_CLIP:Landroid/content/ClipData;

    return-object v0

    .line 195
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 196
    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->getInstance()Lcom/android/server/clipboard/ClipboardChecker;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/server/clipboard/ClipboardChecker;->removeStashClipLater(I)V

    .line 197
    throw v0
.end method
