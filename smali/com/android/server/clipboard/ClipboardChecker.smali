.class public Lcom/android/server/clipboard/ClipboardChecker;
.super Ljava/lang/Object;
.source "ClipboardChecker.java"


# static fields
.field private static final DEFAULT_FUNCTION:I

.field public static final MATCH_APP_AND_RULE:I = 0x0

.field public static final MATCH_APP_MISMATCH_RULE:I = 0x1

.field private static final MIUI12_5_PRIVACY_ENABLE:Z

.field private static final MI_LAB_AI_CLIPBOARD_ENABLE:Ljava/lang/String; = "mi_lab_ai_clipboard_enable"

.field public static final SYSTEM_SPECIAL_ALLOW:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ClipboardServiceI"

.field public static final UNKNOWN:I = 0x2

.field private static final sAllowClipboardSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sInstance:Lcom/android/server/clipboard/ClipboardChecker;


# instance fields
.field private mAiClipboardEnable:Z

.field private mAppOpsService:Lcom/android/server/appop/AppOpsService;

.field private mClipItemData:Landroid/content/ClipData;

.field private mFirstObserve:Z

.field private mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;

.field private final mMatchHistoryCaller:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mMatchHistoryClipData:I

.field private final mPatternMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/util/regex/Pattern;",
            ">;>;"
        }
    .end annotation
.end field

.field private mStashUidClip:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/content/ClipData;",
            ">;"
        }
    .end annotation
.end field

.field private final mTraceClickInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmLock(Lcom/android/server/clipboard/ClipboardChecker;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mLock:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStashUidClip(Lcom/android/server/clipboard/ClipboardChecker;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mStashUidClip:Landroid/util/SparseArray;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAiClipboardEnable(Lcom/android/server/clipboard/ClipboardChecker;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAiClipboardEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetDEFAULT_FUNCTION()I
    .locals 1

    sget v0, Lcom/android/server/clipboard/ClipboardChecker;->DEFAULT_FUNCTION:I

    return v0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 39
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 41
    const-string v0, "ro.miui.ui.version.code"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    nop

    :goto_0
    sput-boolean v2, Lcom/android/server/clipboard/ClipboardChecker;->MIUI12_5_PRIVACY_ENABLE:Z

    .line 42
    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    xor-int/2addr v0, v1

    sput v0, Lcom/android/server/clipboard/ClipboardChecker;->DEFAULT_FUNCTION:I

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/clipboard/ClipboardChecker;->sAllowClipboardSet:Ljava/util/Set;

    .line 64
    const-string v1, "com.android.browser"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 65
    const-string v1, "com.milink.service"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryClipData:I

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryCaller:Ljava/util/List;

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mFirstObserve:Z

    .line 82
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mLock:Ljava/lang/Object;

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mPatternMap:Ljava/util/Map;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mTraceClickInfoMap:Ljava/util/Map;

    .line 87
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mStashUidClip:Landroid/util/SparseArray;

    .line 88
    new-instance v0, Lcom/android/server/clipboard/ClipboardChecker$1;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->get()Lcom/android/internal/os/BackgroundThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/clipboard/ClipboardChecker$1;-><init>(Lcom/android/server/clipboard/ClipboardChecker;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mHandler:Landroid/os/Handler;

    .line 96
    return-void
.end method

.method private getAppOpsService()Lcom/android/server/appop/AppOpsService;
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    if-nez v0, :cond_0

    .line 183
    const-string v0, "appops"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 184
    .local v0, "b":Landroid/os/IBinder;
    invoke-static {v0}, Lcom/android/internal/app/IAppOpsService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IAppOpsService;

    move-result-object v1

    check-cast v1, Lcom/android/server/appop/AppOpsService;

    iput-object v1, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    .line 186
    .end local v0    # "b":Landroid/os/IBinder;
    :cond_0
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    return-object v0
.end method

.method public static getInstance()Lcom/android/server/clipboard/ClipboardChecker;
    .locals 2

    .line 69
    sget-object v0, Lcom/android/server/clipboard/ClipboardChecker;->sInstance:Lcom/android/server/clipboard/ClipboardChecker;

    if-nez v0, :cond_1

    .line 70
    const-class v0, Lcom/android/server/clipboard/ClipboardChecker;

    monitor-enter v0

    .line 71
    :try_start_0
    sget-object v1, Lcom/android/server/clipboard/ClipboardChecker;->sInstance:Lcom/android/server/clipboard/ClipboardChecker;

    if-nez v1, :cond_0

    .line 72
    new-instance v1, Lcom/android/server/clipboard/ClipboardChecker;

    invoke-direct {v1}, Lcom/android/server/clipboard/ClipboardChecker;-><init>()V

    sput-object v1, Lcom/android/server/clipboard/ClipboardChecker;->sInstance:Lcom/android/server/clipboard/ClipboardChecker;

    .line 74
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 76
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/clipboard/ClipboardChecker;->sInstance:Lcom/android/server/clipboard/ClipboardChecker;

    return-object v0
.end method


# virtual methods
.method public applyReadClipboardOperation(ZILjava/lang/String;I)V
    .locals 9
    .param p1, "allow"    # Z
    .param p2, "uid"    # I
    .param p3, "pkgName"    # Ljava/lang/String;
    .param p4, "type"    # I

    .line 176
    invoke-static {}, Lcom/android/server/appop/AppOpsServiceStub;->getInstance()Lcom/android/server/appop/AppOpsServiceStub;

    move-result-object v0

    const/16 v3, 0x1d

    .line 177
    xor-int/lit8 v4, p1, 0x1

    const/16 v6, 0xc8

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 176
    move v1, p2

    move-object v2, p3

    move v5, p4

    invoke-virtual/range {v0 .. v8}, Lcom/android/server/appop/AppOpsServiceStub;->onAppApplyOperation(ILjava/lang/String;IIIIIZ)V

    .line 179
    return-void
.end method

.method public getClipItemData()Landroid/content/ClipData;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mClipItemData:Landroid/content/ClipData;

    return-object v0
.end method

.method public getClipboardClickTrack()Ljava/util/Map;
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mTraceClickInfoMap:Ljava/util/Map;

    return-object v0
.end method

.method public getStashClip(I)Landroid/content/ClipData;
    .locals 2
    .param p1, "uid"    # I

    .line 194
    monitor-enter p0

    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mStashUidClip:Landroid/util/SparseArray;

    invoke-static {}, Lcom/android/server/clipboard/ClipboardServiceStub;->get()Lcom/android/server/clipboard/ClipboardServiceStub;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/clipboard/ClipboardServiceStub;->EMPTY_CLIP:Landroid/content/ClipData;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipData;

    monitor-exit p0

    return-object v0

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hasStash(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 215
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mStashUidClip:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAiClipboardEnable(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .line 227
    const-string v0, "mi_lab_ai_clipboard_enable"

    sget-boolean v1, Lcom/android/server/clipboard/ClipboardChecker;->MIUI12_5_PRIVACY_ENABLE:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    iget-boolean v4, p0, Lcom/android/server/clipboard/ClipboardChecker;->mFirstObserve:Z

    if-eqz v4, :cond_1

    .line 228
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 230
    .local v4, "identity":J
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 231
    .local v6, "resolver":Landroid/content/ContentResolver;
    sget v7, Lcom/android/server/clipboard/ClipboardChecker;->DEFAULT_FUNCTION:I

    const/4 v8, -0x2

    invoke-static {v6, v0, v7, v8}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v7

    if-ne v7, v2, :cond_0

    move v7, v2

    goto :goto_0

    :cond_0
    move v7, v3

    :goto_0
    iput-boolean v7, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAiClipboardEnable:Z

    .line 233
    nop

    .line 234
    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v7, Lcom/android/server/clipboard/ClipboardChecker$2;

    .line 235
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v8

    invoke-direct {v7, p0, v8, v6}, Lcom/android/server/clipboard/ClipboardChecker$2;-><init>(Lcom/android/server/clipboard/ClipboardChecker;Landroid/os/Handler;Landroid/content/ContentResolver;)V

    .line 233
    const/4 v8, -0x1

    invoke-virtual {v6, v0, v3, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    .end local v6    # "resolver":Landroid/content/ContentResolver;
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 245
    nop

    .line 246
    iput-boolean v3, p0, Lcom/android/server/clipboard/ClipboardChecker;->mFirstObserve:Z

    goto :goto_1

    .line 244
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 245
    throw v0

    .line 248
    .end local v4    # "identity":J
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    iget-boolean v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAiClipboardEnable:Z

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v2, v3

    :goto_2
    return v2
.end method

.method public matchClipboardRule(Ljava/lang/String;ILjava/lang/CharSequence;IZ)I
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "callerUid"    # I
    .param p3, "content"    # Ljava/lang/CharSequence;
    .param p4, "clipCode"    # I
    .param p5, "isSystem"    # Z

    .line 102
    const/4 v0, 0x2

    if-nez p3, :cond_0

    .line 103
    return v0

    .line 105
    :cond_0
    if-eqz p5, :cond_1

    sget-object v1, Lcom/android/server/clipboard/ClipboardChecker;->sAllowClipboardSet:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    const/4 v0, 0x3

    return v0

    .line 108
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/server/clipboard/ClipboardChecker;->mPatternMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 110
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mPatternMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/regex/Pattern;

    .line 111
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 113
    if-eqz p5, :cond_2

    const-string v0, "com.android.quicksearchbox"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryClipData:I

    if-ne v0, p4, :cond_2

    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryCaller:Ljava/util/List;

    .line 114
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MIUILOG- Permission Denied when read clipboard repeatedly, caller "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ClipboardServiceI"

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return v2

    .line 118
    :cond_2
    iget v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryClipData:I

    if-eq v0, p4, :cond_3

    .line 119
    iput p4, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryClipData:I

    .line 120
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryCaller:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 122
    :cond_3
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryCaller:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    const/4 v0, 0x0

    return v0

    .line 125
    .end local v1    # "pattern":Ljava/util/regex/Pattern;
    :cond_4
    goto :goto_0

    .line 126
    :cond_5
    return v2

    .line 128
    :cond_6
    return v0
.end method

.method public removeStashClipLater(I)V
    .locals 3
    .param p1, "uid"    # I

    .line 208
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/server/clipboard/ClipboardChecker;->hasStash(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 212
    return-void

    .line 209
    :cond_1
    :goto_0
    return-void
.end method

.method public stashClip(ILandroid/content/ClipData;)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "content"    # Landroid/content/ClipData;

    .line 200
    iput-object p2, p0, Lcom/android/server/clipboard/ClipboardChecker;->mClipItemData:Landroid/content/ClipData;

    .line 201
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 202
    :try_start_0
    iget-object v1, p0, Lcom/android/server/clipboard/ClipboardChecker;->mStashUidClip:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 203
    iget-object v1, p0, Lcom/android/server/clipboard/ClipboardChecker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 204
    monitor-exit v0

    .line 205
    return-void

    .line 204
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateClipItemData(Landroid/content/ClipData;)V
    .locals 0
    .param p1, "content"    # Landroid/content/ClipData;

    .line 219
    iput-object p1, p0, Lcom/android/server/clipboard/ClipboardChecker;->mClipItemData:Landroid/content/ClipData;

    .line 220
    return-void
.end method

.method public updateClipboardPatterns(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/ClipboardRuleInfo;",
            ">;)V"
        }
    .end annotation

    .line 135
    .local p1, "ruleInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ClipboardRuleInfo;>;"
    monitor-enter p0

    .line 136
    if-eqz p1, :cond_7

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 137
    const/4 v0, 0x0

    .line 138
    .local v0, "patternMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/regex/Pattern;>;>;"
    const/4 v1, 0x0

    .line 139
    .local v1, "traceClickInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ClipboardRuleInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    .local v3, "item":Landroid/content/ClipboardRuleInfo;
    :try_start_1
    invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getType()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 142
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v4, "patternList":Ljava/util/List;, "Ljava/util/List<Ljava/util/regex/Pattern;>;"
    invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getRuleInfo()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 144
    .local v6, "ruleItem":Ljava/lang/String;
    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    nop

    .end local v6    # "ruleItem":Ljava/lang/String;
    goto :goto_1

    .line 146
    :cond_0
    if-nez v0, :cond_1

    .line 147
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move-object v0, v5

    .line 149
    :cond_1
    invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getPkgName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    nop

    .end local v4    # "patternList":Ljava/util/List;, "Ljava/util/List<Ljava/util/regex/Pattern;>;"
    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 151
    invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getRuleInfo()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 152
    if-nez v1, :cond_3

    .line 153
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    move-object v1, v4

    .line 155
    :cond_3
    invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getPkgName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getRuleInfo()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    :cond_4
    :goto_2
    nop

    .line 162
    .end local v3    # "item":Landroid/content/ClipboardRuleInfo;
    goto :goto_0

    .line 158
    .restart local v3    # "item":Landroid/content/ClipboardRuleInfo;
    :catch_0
    move-exception v2

    .line 159
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "ClipboardServiceI"

    const-string/jumbo v5, "updateClipboardPatterns error"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 160
    monitor-exit p0

    return-void

    .line 163
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "item":Landroid/content/ClipboardRuleInfo;
    :cond_5
    if-eqz v0, :cond_6

    .line 164
    iget-object v2, p0, Lcom/android/server/clipboard/ClipboardChecker;->mPatternMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 165
    iget-object v2, p0, Lcom/android/server/clipboard/ClipboardChecker;->mPatternMap:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 167
    :cond_6
    if-eqz v1, :cond_7

    .line 168
    iget-object v2, p0, Lcom/android/server/clipboard/ClipboardChecker;->mTraceClickInfoMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 169
    iget-object v2, p0, Lcom/android/server/clipboard/ClipboardChecker;->mTraceClickInfoMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 172
    .end local v0    # "patternMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/regex/Pattern;>;>;"
    .end local v1    # "traceClickInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_7
    monitor-exit p0

    .line 173
    return-void

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
