public class com.android.server.clipboard.ClipboardServiceStubImpl extends com.android.server.clipboard.ClipboardServiceStub {
	 /* .source "ClipboardServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.clipboard.ClipboardServiceStub$$" */
} // .end annotation
/* # static fields */
private static final Boolean IS_SUPPORT_SUPER_CLIPBOARD;
private static final java.lang.String MIUI_INPUT_NO_NEED_SHOW_POP;
private static final java.lang.String TAG;
private static final java.util.List sSuperClipboardPkgList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.List sWebViewWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.app.AppOpsManager mAppOps;
private android.content.Context mContext;
private miui.greeze.IGreezeManager mIGreezeManager;
private com.android.server.clipboard.ClipboardService mService;
/* # direct methods */
static com.android.server.clipboard.ClipboardServiceStubImpl ( ) {
/* .locals 2 */
/* .line 58 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 59 */
final String v1 = "com.android.mms"; // const-string v1, "com.android.mms"
/* .line 60 */
final String v1 = "com.miui.notes"; // const-string v1, "com.miui.notes"
/* .line 61 */
final String v1 = "com.miui.screenshot"; // const-string v1, "com.miui.screenshot"
/* .line 62 */
final String v1 = "com.android.browser"; // const-string v1, "com.android.browser"
/* .line 63 */
final String v1 = "com.android.fileexplorer"; // const-string v1, "com.android.fileexplorer"
/* .line 64 */
final String v1 = "com.miui.gallery"; // const-string v1, "com.miui.gallery"
/* .line 65 */
final String v1 = "com.android.email"; // const-string v1, "com.android.email"
/* .line 71 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 72 */
final String v1 = "com.yinxiang"; // const-string v1, "com.yinxiang"
/* .line 73 */
final String v1 = "com.evernote"; // const-string v1, "com.evernote"
/* .line 74 */
final String v1 = "com.youdao.note"; // const-string v1, "com.youdao.note"
/* .line 75 */
final String v1 = "com.flomo.app"; // const-string v1, "com.flomo.app"
/* .line 76 */
final String v1 = "com.yuque.mobile.android.app"; // const-string v1, "com.yuque.mobile.android.app"
/* .line 77 */
final String v1 = "com.denglin.moji"; // const-string v1, "com.denglin.moji"
/* .line 78 */
final String v1 = "com.zhihu.android"; // const-string v1, "com.zhihu.android"
/* .line 79 */
final String v1 = "cn.wps.moffice_eng"; // const-string v1, "cn.wps.moffice_eng"
/* .line 80 */
return;
} // .end method
public com.android.server.clipboard.ClipboardServiceStubImpl ( ) {
/* .locals 1 */
/* .line 45 */
/* invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStub;-><init>()V */
/* .line 201 */
int v0 = 0; // const/4 v0, 0x0
this.mIGreezeManager = v0;
return;
} // .end method
private void checkPermissionPkg ( ) {
/* .locals 4 */
/* .line 239 */
com.android.server.am.ActivityManagerServiceStub .get ( );
v1 = android.os.Binder .getCallingPid ( );
(( com.android.server.am.ActivityManagerServiceStub ) v0 ).getPackageNameByPid ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerServiceStub;->getPackageNameByPid(I)Ljava/lang/String;
/* .line 240 */
/* .local v0, "callingPackageName":Ljava/lang/String; */
final String v1 = "com.lbe.security.miui"; // const-string v1, "com.lbe.security.miui"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 244 */
return;
/* .line 241 */
} // :cond_0
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Permission Denial: attempt to assess internal clipboard from pid="; // const-string v3, "Permission Denial: attempt to assess internal clipboard from pid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 242 */
v3 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", uid="; // const-string v3, ", uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", pkg="; // const-string v3, ", pkg="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private java.lang.CharSequence firstClipData ( android.content.ClipData p0 ) {
/* .locals 5 */
/* .param p1, "clipData" # Landroid/content/ClipData; */
/* .line 252 */
int v0 = 0; // const/4 v0, 0x0
/* .line 253 */
/* .local v0, "paste":Ljava/lang/CharSequence; */
if ( p1 != null) { // if-eqz p1, :cond_2
/* .line 254 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v1 */
/* .line 256 */
/* .local v1, "identity":J */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
try { // :try_start_0
v4 = (( android.content.ClipData ) p1 ).getItemCount ( ); // invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I
/* if-ge v3, v4, :cond_1 */
/* .line 257 */
(( android.content.ClipData ) p1 ).getItemAt ( v3 ); // invoke-virtual {p1, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
(( android.content.ClipData$Item ) v4 ).getText ( ); // invoke-virtual {v4}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v0, v4 */
/* .line 258 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 259 */
/* .line 256 */
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 263 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 264 */
/* .line 263 */
/* :catchall_0 */
/* move-exception v3 */
android.os.Binder .restoreCallingIdentity ( v1,v2 );
/* .line 264 */
/* throw v3 */
/* .line 266 */
} // .end local v1 # "identity":J
} // :cond_2
} // :goto_2
} // .end method
private miui.greeze.IGreezeManager getGreeze ( ) {
/* .locals 1 */
/* .line 204 */
v0 = this.mIGreezeManager;
/* if-nez v0, :cond_0 */
/* .line 205 */
/* nop */
/* .line 206 */
final String v0 = "greezer"; // const-string v0, "greezer"
android.os.ServiceManager .getService ( v0 );
/* .line 205 */
miui.greeze.IGreezeManager$Stub .asInterface ( v0 );
this.mIGreezeManager = v0;
/* .line 207 */
} // :cond_0
v0 = this.mIGreezeManager;
} // .end method
private android.content.ClipData modifyClipDataForWebView ( android.content.ClipData p0 ) {
/* .locals 4 */
/* .param p1, "clipData" # Landroid/content/ClipData; */
/* .line 383 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 384 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( android.content.ClipData ) p1 ).getItemCount ( ); // invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I
/* if-ge v1, v2, :cond_1 */
/* .line 385 */
(( android.content.ClipData ) p1 ).getItemAt ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
(( android.content.ClipData$Item ) v2 ).getText ( ); // invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;
/* .line 386 */
/* .local v2, "text":Ljava/lang/CharSequence; */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_0
final String v3 = ""; // const-string v3, ""
} // :cond_0
/* move-object v3, v2 */
} // :goto_1
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
/* .line 384 */
} // .end local v2 # "text":Ljava/lang/CharSequence;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 388 */
} // .end local v1 # "i":I
} // :cond_1
/* new-instance v1, Landroid/content/ClipData$Item; */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V */
/* .line 389 */
/* .local v1, "item":Landroid/content/ClipData$Item; */
/* new-instance v2, Landroid/content/ClipData; */
(( android.content.ClipData ) p1 ).getDescription ( ); // invoke-virtual {p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;
/* invoke-direct {v2, v3, v1}, Landroid/content/ClipData;-><init>(Landroid/content/ClipDescription;Landroid/content/ClipData$Item;)V */
} // .end method
private Boolean needModifyClipDataForWebView ( java.lang.String p0, android.content.ClipData p1 ) {
/* .locals 3 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "clipData" # Landroid/content/ClipData; */
/* .line 370 */
v0 = v0 = com.android.server.clipboard.ClipboardServiceStubImpl.sWebViewWhiteList;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 371 */
/* .line 373 */
} // :cond_0
v0 = (( android.content.ClipData ) p2 ).getItemCount ( ); // invoke-virtual {p2}, Landroid/content/ClipData;->getItemCount()I
int v2 = 1; // const/4 v2, 0x1
/* if-le v0, v2, :cond_1 */
/* move v1, v2 */
} // :cond_1
} // .end method
private void notifyGreeze ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 211 */
int v0 = 1; // const/4 v0, 0x1
/* new-array v0, v0, [I */
/* .line 212 */
/* .local v0, "uids":[I */
int v1 = 0; // const/4 v1, 0x0
/* aput p1, v0, v1 */
/* .line 214 */
try { // :try_start_0
v1 = /* invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->getGreeze()Lmiui/greeze/IGreezeManager; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 215 */
/* invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->getGreeze()Lmiui/greeze/IGreezeManager; */
final String v2 = "clip"; // const-string v2, "clip"
/* const/16 v3, 0x3e8 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 218 */
} // :cond_0
/* .line 216 */
/* :catch_0 */
/* move-exception v1 */
/* .line 217 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "ClipboardServiceI"; // const-string v2, "ClipboardServiceI"
final String v3 = "ClipGreez err:"; // const-string v3, "ClipGreez err:"
android.util.Log .d ( v2,v3 );
/* .line 219 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void removeActivePermissionOwnerForPackage ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 348 */
v0 = this.mService;
v1 = this.mContext;
v1 = (( android.content.Context ) v1 ).getDeviceId ( ); // invoke-virtual {v1}, Landroid/content/Context;->getDeviceId()I
(( com.android.server.clipboard.ClipboardService ) v0 ).getClipboardLocked ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/android/server/clipboard/ClipboardService;->getClipboardLocked(II)Lcom/android/server/clipboard/ClipboardService$Clipboard;
/* .line 349 */
/* .local v0, "clipboard":Lcom/android/server/clipboard/ClipboardService$Clipboard; */
v1 = this.mPrimaryClipPackage;
v1 = android.text.TextUtils .equals ( p1,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 351 */
v1 = this.activePermissionOwners;
(( java.util.HashSet ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/HashSet;->clear()V
/* .line 354 */
} // :cond_0
v1 = this.activePermissionOwners;
(( java.util.HashSet ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 356 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean checkProviderWakePathForClipboard ( java.lang.String p0, Integer p1, android.content.pm.ProviderInfo p2, Integer p3 ) {
/* .locals 15 */
/* .param p1, "callerPkg" # Ljava/lang/String; */
/* .param p2, "callingUid" # I */
/* .param p3, "providerInfo" # Landroid/content/pm/ProviderInfo; */
/* .param p4, "userId" # I */
/* .line 272 */
/* move-object v0, p0 */
/* move-object/from16 v8, p1 */
/* move-object/from16 v9, p3 */
/* .line 273 */
v1 = /* invoke-static/range {p1 ..p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_6 */
if ( v9 != null) { // if-eqz v9, :cond_6
v1 = this.authority;
/* .line 275 */
v1 = android.text.TextUtils .isEmpty ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* move/from16 v10, p4 */
/* goto/16 :goto_1 */
/* .line 279 */
} // :cond_0
v1 = this.mService;
v3 = this.mContext;
v3 = (( android.content.Context ) v3 ).getDeviceId ( ); // invoke-virtual {v3}, Landroid/content/Context;->getDeviceId()I
/* move/from16 v10, p4 */
(( com.android.server.clipboard.ClipboardService ) v1 ).getClipboardLocked ( v10, v3 ); // invoke-virtual {v1, v10, v3}, Lcom/android/server/clipboard/ClipboardService;->getClipboardLocked(II)Lcom/android/server/clipboard/ClipboardService$Clipboard;
/* .line 280 */
/* .local v11, "clipboard":Lcom/android/server/clipboard/ClipboardService$Clipboard; */
v1 = this.primaryClip;
final String v3 = "ClipboardServiceI"; // const-string v3, "ClipboardServiceI"
/* if-nez v1, :cond_1 */
/* .line 281 */
final String v1 = "checkProviderWakePathForClipboard: primaryClip is null"; // const-string v1, "checkProviderWakePathForClipboard: primaryClip is null"
android.util.Log .i ( v3,v1 );
/* .line 282 */
/* .line 285 */
} // :cond_1
v1 = this.activePermissionOwners;
v1 = (( java.util.HashSet ) v1 ).contains ( v8 ); // invoke-virtual {v1, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_2 */
/* .line 286 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "checkProviderWakePathForClipboard: "; // const-string v4, "checkProviderWakePathForClipboard: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "is not a activePermissionOwner"; // const-string v4, "is not a activePermissionOwner"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v1 );
/* .line 288 */
/* .line 291 */
} // :cond_2
v1 = com.android.server.clipboard.ClipboardServiceStubImpl.sSuperClipboardPkgList;
v1 = v4 = this.packageName;
final String v4 = "checkProviderWakePathForClipboard: Package "; // const-string v4, "checkProviderWakePathForClipboard: Package "
/* if-nez v1, :cond_3 */
/* .line 292 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is not in the white list"; // const-string v4, " is not in the white list"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v1 );
/* .line 294 */
/* .line 297 */
} // :cond_3
v1 = this.primaryClip;
v12 = (( android.content.ClipData ) v1 ).getItemCount ( ); // invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I
/* .line 299 */
/* .local v12, "count":I */
int v1 = 0; // const/4 v1, 0x0
/* move v13, v1 */
/* .local v13, "i":I */
} // :goto_0
/* if-ge v13, v12, :cond_5 */
/* .line 300 */
v1 = this.primaryClip;
(( android.content.ClipData ) v1 ).getItemAt ( v13 ); // invoke-virtual {v1, v13}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
(( android.content.ClipData$Item ) v1 ).getUri ( ); // invoke-virtual {v1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;
/* .line 301 */
/* .local v14, "uri":Landroid/net/Uri; */
if ( v14 != null) { // if-eqz v14, :cond_4
v1 = this.authority;
(( android.net.Uri ) v14 ).getAuthority ( ); // invoke-virtual {v14}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;
v1 = android.text.TextUtils .equals ( v1,v5 );
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 302 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " can to waked by "; // const-string v2, " can to waked by "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v3,v1 );
/* .line 304 */
miui.security.WakePathChecker .getInstance ( );
v3 = this.packageName;
int v4 = 4; // const/4 v4, 0x4
/* .line 306 */
v5 = /* invoke-static/range {p2 ..p2}, Landroid/os/UserHandle;->getUserId(I)I */
int v7 = 1; // const/4 v7, 0x1
/* .line 304 */
/* move-object/from16 v2, p1 */
/* move/from16 v6, p4 */
/* invoke-virtual/range {v1 ..v7}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 307 */
int v1 = 1; // const/4 v1, 0x1
/* .line 299 */
} // :cond_4
/* add-int/lit8 v13, v13, 0x1 */
/* .line 310 */
} // .end local v13 # "i":I
} // .end local v14 # "uri":Landroid/net/Uri;
} // :cond_5
/* .line 273 */
} // .end local v11 # "clipboard":Lcom/android/server/clipboard/ClipboardService$Clipboard;
} // .end local v12 # "count":I
} // :cond_6
/* move/from16 v10, p4 */
/* .line 276 */
} // :goto_1
} // .end method
public Integer clipboardAccessResult ( android.content.ClipData p0, java.lang.String p1, Integer p2, Integer p3, Integer p4, Boolean p5 ) {
/* .locals 20 */
/* .param p1, "clip" # Landroid/content/ClipData; */
/* .param p2, "callerPkg" # Ljava/lang/String; */
/* .param p3, "callerUid" # I */
/* .param p4, "callerUserId" # I */
/* .param p5, "primaryClipUid" # I */
/* .param p6, "userCall" # Z */
/* .line 92 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v8, p2 */
/* move/from16 v9, p3 */
/* move/from16 v10, p5 */
/* invoke-direct {v0, v10}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->notifyGreeze(I)V */
/* .line 93 */
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->firstClipData(Landroid/content/ClipData;)Ljava/lang/CharSequence; */
/* .line 94 */
/* .local v11, "firstClipData":Ljava/lang/CharSequence; */
int v12 = 0; // const/4 v12, 0x0
int v13 = 1; // const/4 v13, 0x1
if ( v11 != null) { // if-eqz v11, :cond_1
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* if-nez v2, :cond_0 */
} // :cond_0
/* move v2, v12 */
} // :cond_1
} // :goto_0
/* move v2, v13 */
} // :goto_1
/* move v14, v2 */
/* .line 95 */
/* .local v14, "firstClipDataEmpty":Z */
/* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v2, :cond_13 */
/* if-nez v14, :cond_13 */
/* if-nez p6, :cond_13 */
/* if-eq v10, v9, :cond_13 */
/* .line 96 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
v3 = this.mContext;
v2 = (( com.android.server.clipboard.ClipboardChecker ) v2 ).isAiClipboardEnable ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/clipboard/ClipboardChecker;->isAiClipboardEnable(Landroid/content/Context;)Z
/* if-nez v2, :cond_2 */
/* move/from16 v15, p4 */
/* goto/16 :goto_8 */
/* .line 104 */
} // :cond_2
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "default_input_method"; // const-string v3, "default_input_method"
/* move/from16 v15, p4 */
android.provider.Settings$Secure .getStringForUser ( v2,v3,v15 );
/* .line 106 */
/* .local v16, "defaultIme":Ljava/lang/String; */
v2 = /* invoke-static/range {v16 ..v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
/* if-nez v2, :cond_3 */
/* .line 107 */
/* invoke-static/range {v16 ..v16}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName; */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 108 */
/* .local v2, "imePkg":Ljava/lang/String; */
v3 = (( java.lang.String ) v2 ).equals ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 109 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v3 ).applyReadClipboardOperation ( v13, v9, v8, v13 ); // invoke-virtual {v3, v13, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 110 */
/* .line 113 */
} // .end local v2 # "imePkg":Ljava/lang/String;
} // :cond_3
v2 = this.mAppOps;
/* const/16 v3, 0x1d */
v7 = (( android.app.AppOpsManager ) v2 ).checkOp ( v3, v9, v8 ); // invoke-virtual {v2, v3, v9, v8}, Landroid/app/AppOpsManager;->checkOp(IILjava/lang/String;)I
/* .line 114 */
/* .local v7, "resultMode":I */
v2 = /* invoke-static/range {p3 ..p3}, Landroid/os/UserHandle;->getAppId(I)I */
/* const/16 v3, 0x2710 */
/* if-ge v2, v3, :cond_4 */
/* move v2, v13 */
} // :cond_4
/* move v2, v12 */
} // :goto_2
/* move/from16 v17, v2 */
/* .line 115 */
/* .local v17, "isSystem":Z */
/* if-nez v17, :cond_6 */
/* .line 116 */
/* const-class v2, Landroid/content/pm/PackageManagerInternal; */
com.android.server.LocalServices .getService ( v2 );
/* move-object/from16 v18, v2 */
/* check-cast v18, Landroid/content/pm/PackageManagerInternal; */
/* .line 117 */
/* .local v18, "pmi":Landroid/content/pm/PackageManagerInternal; */
/* const-wide/16 v4, 0x0 */
/* const/16 v6, 0x3e8 */
/* .line 118 */
v19 = /* invoke-static/range {p3 ..p3}, Landroid/os/UserHandle;->getUserId(I)I */
/* .line 117 */
/* move-object/from16 v2, v18 */
/* move-object/from16 v3, p2 */
/* move v12, v7 */
} // .end local v7 # "resultMode":I
/* .local v12, "resultMode":I */
/* move/from16 v7, v19 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
/* .line 119 */
/* .local v2, "appInfo":Landroid/content/pm/ApplicationInfo; */
if ( v2 != null) { // if-eqz v2, :cond_5
/* iget v3, v2, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/2addr v3, v13 */
if ( v3 != null) { // if-eqz v3, :cond_5
/* move v3, v13 */
} // :cond_5
int v3 = 0; // const/4 v3, 0x0
} // :goto_3
/* move/from16 v17, v3 */
/* .line 115 */
} // .end local v2 # "appInfo":Landroid/content/pm/ApplicationInfo;
} // .end local v12 # "resultMode":I
} // .end local v18 # "pmi":Landroid/content/pm/PackageManagerInternal;
/* .restart local v7 # "resultMode":I */
} // :cond_6
/* move v12, v7 */
/* .line 123 */
} // .end local v7 # "resultMode":I
/* .restart local v12 # "resultMode":I */
} // :goto_4
int v7 = 5; // const/4 v7, 0x5
/* if-ne v12, v7, :cond_9 */
/* if-nez v17, :cond_9 */
/* .line 124 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription; */
/* .line 125 */
/* .local v2, "description":Landroid/content/ClipDescription; */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 126 */
final String v3 = "image/*"; // const-string v3, "image/*"
(( android.content.ClipDescription ) v2 ).filterMimeTypes ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/ClipDescription;->filterMimeTypes(Ljava/lang/String;)[Ljava/lang/String;
/* .line 127 */
/* .local v3, "imageMimeTypes":[Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 128 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.clipboard.ClipboardChecker ) v4 ).applyReadClipboardOperation ( v5, v9, v8, v7 ); // invoke-virtual {v4, v5, v9, v8, v7}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 130 */
/* .line 134 */
} // .end local v3 # "imageMimeTypes":[Ljava/lang/String;
} // :cond_7
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_5
v4 = /* invoke-virtual/range {p1 ..p1}, Landroid/content/ClipData;->getItemCount()I */
/* if-ge v3, v4, :cond_9 */
/* .line 135 */
(( android.content.ClipData ) v1 ).getItemAt ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
/* .line 136 */
/* .local v4, "item":Landroid/content/ClipData$Item; */
(( android.content.ClipData$Item ) v4 ).getUri ( ); // invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 137 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.clipboard.ClipboardChecker ) v5 ).applyReadClipboardOperation ( v6, v9, v8, v7 ); // invoke-virtual {v5, v6, v9, v8, v7}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 139 */
/* .line 134 */
} // .end local v4 # "item":Landroid/content/ClipData$Item;
} // :cond_8
/* add-int/lit8 v3, v3, 0x1 */
/* .line 144 */
} // .end local v2 # "description":Landroid/content/ClipDescription;
} // .end local v3 # "i":I
} // :cond_9
com.android.server.clipboard.ClipboardChecker .getInstance ( );
v6 = /* invoke-virtual/range {p1 ..p1}, Ljava/lang/Object;->hashCode()I */
/* move-object/from16 v3, p2 */
/* move/from16 v4, p3 */
/* move-object v5, v11 */
/* move v13, v7 */
/* move/from16 v7, v17 */
v2 = /* invoke-virtual/range {v2 ..v7}, Lcom/android/server/clipboard/ClipboardChecker;->matchClipboardRule(Ljava/lang/String;ILjava/lang/CharSequence;IZ)I */
/* .line 145 */
/* .local v2, "matchResult":I */
int v3 = 3; // const/4 v3, 0x3
/* const/16 v4, 0x3e8 */
final String v5 = "ClipboardServiceI"; // const-string v5, "ClipboardServiceI"
if ( v12 != null) { // if-eqz v12, :cond_e
int v6 = 4; // const/4 v6, 0x4
/* if-ne v12, v6, :cond_a */
/* .line 164 */
} // :cond_a
/* if-ne v10, v4, :cond_b */
/* .line 165 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription; */
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 166 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription; */
(( android.content.ClipDescription ) v4 ).getLabel ( ); // invoke-virtual {v4}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;
final String v6 = "miui_input_no_need_show_pop"; // const-string v6, "miui_input_no_need_show_pop"
v4 = android.text.TextUtils .equals ( v6,v4 );
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 167 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.clipboard.ClipboardChecker ) v3 ).applyReadClipboardOperation ( v4, v9, v8, v13 ); // invoke-virtual {v3, v4, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 168 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MIUILOG- Permission Denied when read clipboard [CloudSet], caller "; // const-string v4, "MIUILOG- Permission Denied when read clipboard [CloudSet], caller "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v3 );
/* .line 169 */
int v3 = 1; // const/4 v3, 0x1
/* .line 172 */
} // :cond_b
/* if-nez v2, :cond_c */
/* .line 174 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v3 ).updateClipItemData ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/clipboard/ClipboardChecker;->updateClipItemData(Landroid/content/ClipData;)V
/* .line 175 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.clipboard.ClipboardChecker ) v3 ).applyReadClipboardOperation ( v4, v9, v8, v13 ); // invoke-virtual {v3, v4, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 176 */
int v6 = 0; // const/4 v6, 0x0
/* .line 177 */
} // :cond_c
int v4 = 1; // const/4 v4, 0x1
int v6 = 0; // const/4 v6, 0x0
/* if-ne v2, v4, :cond_d */
/* .line 179 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v3 ).applyReadClipboardOperation ( v6, v9, v8, v13 ); // invoke-virtual {v3, v6, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 180 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MIUILOG- Permission Denied when read clipboard [Mismatch], caller "; // const-string v4, "MIUILOG- Permission Denied when read clipboard [Mismatch], caller "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v3 );
/* .line 181 */
int v3 = 1; // const/4 v3, 0x1
/* .line 183 */
} // :cond_d
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v4 ).stashClip ( v9, v1 ); // invoke-virtual {v4, v9, v1}, Lcom/android/server/clipboard/ClipboardChecker;->stashClip(ILandroid/content/ClipData;)V
/* .line 184 */
/* .line 146 */
} // :cond_e
} // :goto_6
/* if-eq v9, v4, :cond_12 */
if ( v17 != null) { // if-eqz v17, :cond_12
/* .line 148 */
/* if-ne v2, v3, :cond_f */
/* .line 149 */
int v3 = 0; // const/4 v3, 0x0
/* .line 151 */
} // :cond_f
int v3 = 0; // const/4 v3, 0x0
/* sget-boolean v4, Lmiui/os/Build;->IS_DEVELOPMENT_VERSION:Z */
if ( v4 != null) { // if-eqz v4, :cond_11
/* if-nez v2, :cond_10 */
int v3 = 1; // const/4 v3, 0x1
/* .line 155 */
} // :cond_10
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v4 ).applyReadClipboardOperation ( v3, v9, v8, v13 ); // invoke-virtual {v4, v3, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 156 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MIUILOG- Permission Denied when system app read clipboard, caller "; // const-string v4, "MIUILOG- Permission Denied when system app read clipboard, caller "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v3 );
/* .line 157 */
int v3 = 1; // const/4 v3, 0x1
/* .line 151 */
} // :cond_11
int v3 = 1; // const/4 v3, 0x1
/* .line 152 */
} // :goto_7
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v4 ).applyReadClipboardOperation ( v3, v9, v8, v13 ); // invoke-virtual {v4, v3, v9, v8, v13}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 153 */
int v4 = 0; // const/4 v4, 0x0
/* .line 146 */
} // :cond_12
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* .line 160 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v5 ).applyReadClipboardOperation ( v3, v9, v8, v3 ); // invoke-virtual {v5, v3, v9, v8, v3}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 161 */
/* .line 95 */
} // .end local v2 # "matchResult":I
} // .end local v12 # "resultMode":I
} // .end local v16 # "defaultIme":Ljava/lang/String;
} // .end local v17 # "isSystem":Z
} // :cond_13
/* move/from16 v15, p4 */
/* .line 97 */
} // :goto_8
/* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v2, :cond_14 */
/* if-nez v14, :cond_16 */
if ( p6 != null) { // if-eqz p6, :cond_16
/* .line 98 */
} // :cond_14
com.android.server.clipboard.ClipboardChecker .getInstance ( );
/* .line 99 */
if ( p6 != null) { // if-eqz p6, :cond_15
int v3 = 6; // const/4 v3, 0x6
} // :cond_15
int v3 = 1; // const/4 v3, 0x1
/* .line 98 */
} // :goto_9
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.clipboard.ClipboardChecker ) v2 ).applyReadClipboardOperation ( v4, v9, v8, v3 ); // invoke-virtual {v2, v4, v9, v8, v3}, Lcom/android/server/clipboard/ClipboardChecker;->applyReadClipboardOperation(ZILjava/lang/String;I)V
/* .line 101 */
} // :cond_16
int v2 = 0; // const/4 v2, 0x0
} // .end method
public java.util.Map getClipboardClickRuleData ( ) {
/* .locals 1 */
/* .line 248 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v0 ).getClipboardClickTrack ( ); // invoke-virtual {v0}, Lcom/android/server/clipboard/ClipboardChecker;->getClipboardClickTrack()Ljava/util/Map;
} // .end method
public android.content.ClipData getStashClipboardData ( ) {
/* .locals 1 */
/* .line 228 */
/* invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->checkPermissionPkg()V */
/* .line 229 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v0 ).getClipItemData ( ); // invoke-virtual {v0}, Lcom/android/server/clipboard/ClipboardChecker;->getClipItemData()Landroid/content/ClipData;
} // .end method
public void init ( com.android.server.clipboard.ClipboardService p0, android.app.AppOpsManager p1 ) {
/* .locals 2 */
/* .param p1, "clipboardService" # Lcom/android/server/clipboard/ClipboardService; */
/* .param p2, "appOps" # Landroid/app/AppOpsManager; */
/* .line 84 */
this.mService = p1;
/* .line 85 */
(( com.android.server.clipboard.ClipboardService ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/clipboard/ClipboardService;->getContext()Landroid/content/Context;
this.mContext = v0;
/* .line 86 */
this.mAppOps = p2;
/* .line 87 */
final String v0 = "persist.sys.support_super_clipboard"; // const-string v0, "persist.sys.support_super_clipboard"
final String v1 = "1"; // const-string v1, "1"
android.os.SystemProperties .set ( v0,v1 );
/* .line 88 */
return;
} // .end method
public Boolean isUidFocused ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 223 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
v0 = (( com.android.server.clipboard.ClipboardChecker ) v0 ).hasStash ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/clipboard/ClipboardChecker;->hasStash(I)Z
} // .end method
public android.content.ClipData modifyClipDataIfNeed ( java.lang.String p0, android.content.ClipData p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "clipData" # Landroid/content/ClipData; */
/* .line 360 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->needModifyClipDataForWebView(Ljava/lang/String;Landroid/content/ClipData;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 361 */
/* invoke-direct {p0, p2}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->modifyClipDataForWebView(Landroid/content/ClipData;)Landroid/content/ClipData; */
/* .line 363 */
} // :cond_0
} // .end method
public void onPackageUriPermissionRemoved ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 316 */
final String v0 = "ClipboardServiceI"; // const-string v0, "ClipboardServiceI"
try { // :try_start_0
v1 = android.text.TextUtils .isEmpty ( p1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 317 */
final String v1 = "onPackageUriPermissionRemoved: package is empty"; // const-string v1, "onPackageUriPermissionRemoved: package is empty"
android.util.Log .i ( v0,v1 );
/* .line 318 */
return;
/* .line 321 */
} // :cond_0
/* const/16 v1, -0x2710 */
/* if-ne p2, v1, :cond_1 */
/* .line 322 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onPackageUriPermissionRemoved: Attempt to remove active permission owner for invalid user: "; // const-string v2, "onPackageUriPermissionRemoved: Attempt to remove active permission owner for invalid user: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v0,v1 );
/* .line 324 */
return;
/* .line 327 */
} // :cond_1
android.app.ActivityManager .getService ( );
/* .line 328 */
/* .local v1, "am":Landroid/app/IActivityManager; */
int v2 = -1; // const/4 v2, -0x1
/* if-ne p2, v2, :cond_3 */
/* .line 329 */
/* .line 330 */
/* .local v2, "runningUserIds":[I */
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_2 */
/* aget v5, v2, v4 */
/* .line 331 */
/* .local v5, "id":I */
/* invoke-direct {p0, p1, v5}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->removeActivePermissionOwnerForPackage(Ljava/lang/String;I)V */
/* .line 330 */
} // .end local v5 # "id":I
/* add-int/lit8 v4, v4, 0x1 */
/* .line 333 */
} // :cond_2
return;
/* .line 336 */
} // .end local v2 # "runningUserIds":[I
} // :cond_3
int v2 = -2; // const/4 v2, -0x2
/* if-eq p2, v2, :cond_5 */
int v2 = -3; // const/4 v2, -0x3
/* if-ne p2, v2, :cond_4 */
/* .line 341 */
} // :cond_4
/* invoke-direct {p0, p1, p2}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->removeActivePermissionOwnerForPackage(Ljava/lang/String;I)V */
/* .line 344 */
} // .end local v1 # "am":Landroid/app/IActivityManager;
/* .line 337 */
/* .restart local v1 # "am":Landroid/app/IActivityManager; */
} // :cond_5
v2 = } // :goto_1
/* invoke-direct {p0, p1, v2}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->removeActivePermissionOwnerForPackage(Ljava/lang/String;I)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 338 */
return;
/* .line 342 */
} // .end local v1 # "am":Landroid/app/IActivityManager;
/* :catch_0 */
/* move-exception v1 */
/* .line 343 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "onPackageUriPermissionRemoved: "; // const-string v3, "onPackageUriPermissionRemoved: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v2 );
/* .line 345 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
public void updateClipboardRuleData ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/content/ClipboardRuleInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 234 */
/* .local p1, "ruleInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ClipboardRuleInfo;>;" */
/* invoke-direct {p0}, Lcom/android/server/clipboard/ClipboardServiceStubImpl;->checkPermissionPkg()V */
/* .line 235 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v0 ).updateClipboardPatterns ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/clipboard/ClipboardChecker;->updateClipboardPatterns(Ljava/util/List;)V
/* .line 236 */
return;
} // .end method
public android.content.ClipData waitUserChoice ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "callerPkg" # Ljava/lang/String; */
/* .param p2, "callerUid" # I */
/* .line 189 */
final String v0 = "ai_read_clipboard"; // const-string v0, "ai_read_clipboard"
android.os.Trace .beginSection ( v0 );
/* .line 191 */
try { // :try_start_0
v0 = this.mAppOps;
/* const/16 v1, 0x1d */
v0 = (( android.app.AppOpsManager ) v0 ).noteOp ( v1, p2, p1 ); // invoke-virtual {v0, v1, p2, p1}, Landroid/app/AppOpsManager;->noteOp(IILjava/lang/String;)I
/* if-nez v0, :cond_0 */
/* .line 192 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v0 ).getStashClip ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/clipboard/ClipboardChecker;->getStashClip(I)Landroid/content/ClipData;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 195 */
android.os.Trace .endSection ( );
/* .line 196 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v1 ).removeStashClipLater ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/clipboard/ClipboardChecker;->removeStashClipLater(I)V
/* .line 192 */
/* .line 195 */
} // :cond_0
android.os.Trace .endSection ( );
/* .line 196 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v0 ).removeStashClipLater ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/clipboard/ClipboardChecker;->removeStashClipLater(I)V
/* .line 197 */
/* nop */
/* .line 198 */
v0 = this.EMPTY_CLIP;
/* .line 195 */
/* :catchall_0 */
/* move-exception v0 */
android.os.Trace .endSection ( );
/* .line 196 */
com.android.server.clipboard.ClipboardChecker .getInstance ( );
(( com.android.server.clipboard.ClipboardChecker ) v1 ).removeStashClipLater ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/clipboard/ClipboardChecker;->removeStashClipLater(I)V
/* .line 197 */
/* throw v0 */
} // .end method
