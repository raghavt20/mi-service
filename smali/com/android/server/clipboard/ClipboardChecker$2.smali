.class Lcom/android/server/clipboard/ClipboardChecker$2;
.super Landroid/database/ContentObserver;
.source "ClipboardChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/clipboard/ClipboardChecker;->isAiClipboardEnable(Landroid/content/Context;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/clipboard/ClipboardChecker;

.field final synthetic val$resolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Lcom/android/server/clipboard/ClipboardChecker;Landroid/os/Handler;Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/clipboard/ClipboardChecker;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 235
    iput-object p1, p0, Lcom/android/server/clipboard/ClipboardChecker$2;->this$0:Lcom/android/server/clipboard/ClipboardChecker;

    iput-object p3, p0, Lcom/android/server/clipboard/ClipboardChecker$2;->val$resolver:Landroid/content/ContentResolver;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 238
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 239
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker$2;->this$0:Lcom/android/server/clipboard/ClipboardChecker;

    iget-object v1, p0, Lcom/android/server/clipboard/ClipboardChecker$2;->val$resolver:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/android/server/clipboard/ClipboardChecker;->-$$Nest$sfgetDEFAULT_FUNCTION()I

    move-result v2

    const/4 v3, -0x2

    const-string v4, "mi_lab_ai_clipboard_enable"

    invoke-static {v1, v4, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v2}, Lcom/android/server/clipboard/ClipboardChecker;->-$$Nest$fputmAiClipboardEnable(Lcom/android/server/clipboard/ClipboardChecker;Z)V

    .line 241
    return-void
.end method
