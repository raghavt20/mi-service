.class Lcom/android/server/clipboard/ClipboardChecker$1;
.super Landroid/os/Handler;
.source "ClipboardChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/clipboard/ClipboardChecker;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/clipboard/ClipboardChecker;


# direct methods
.method constructor <init>(Lcom/android/server/clipboard/ClipboardChecker;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/clipboard/ClipboardChecker;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 88
    iput-object p1, p0, Lcom/android/server/clipboard/ClipboardChecker$1;->this$0:Lcom/android/server/clipboard/ClipboardChecker;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 91
    iget-object v0, p0, Lcom/android/server/clipboard/ClipboardChecker$1;->this$0:Lcom/android/server/clipboard/ClipboardChecker;

    invoke-static {v0}, Lcom/android/server/clipboard/ClipboardChecker;->-$$Nest$fgetmLock(Lcom/android/server/clipboard/ClipboardChecker;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 92
    :try_start_0
    iget-object v1, p0, Lcom/android/server/clipboard/ClipboardChecker$1;->this$0:Lcom/android/server/clipboard/ClipboardChecker;

    invoke-static {v1}, Lcom/android/server/clipboard/ClipboardChecker;->-$$Nest$fgetmStashUidClip(Lcom/android/server/clipboard/ClipboardChecker;)Landroid/util/SparseArray;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 93
    monitor-exit v0

    .line 94
    return-void

    .line 93
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
