public class com.android.server.clipboard.ClipboardChecker {
	 /* .source "ClipboardChecker.java" */
	 /* # static fields */
	 private static final Integer DEFAULT_FUNCTION;
	 public static final Integer MATCH_APP_AND_RULE;
	 public static final Integer MATCH_APP_MISMATCH_RULE;
	 private static final Boolean MIUI12_5_PRIVACY_ENABLE;
	 private static final java.lang.String MI_LAB_AI_CLIPBOARD_ENABLE;
	 public static final Integer SYSTEM_SPECIAL_ALLOW;
	 private static final java.lang.String TAG;
	 public static final Integer UNKNOWN;
	 private static final java.util.Set sAllowClipboardSet;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static volatile com.android.server.clipboard.ClipboardChecker sInstance;
/* # instance fields */
private Boolean mAiClipboardEnable;
private com.android.server.appop.AppOpsService mAppOpsService;
private android.content.ClipData mClipItemData;
private Boolean mFirstObserve;
private android.os.Handler mHandler;
private final java.lang.Object mLock;
private final java.util.List mMatchHistoryCaller;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mMatchHistoryClipData;
private final java.util.Map mPatternMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/util/regex/Pattern;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private android.util.SparseArray mStashUidClip;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Landroid/content/ClipData;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Map mTraceClickInfoMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.lang.Object -$$Nest$fgetmLock ( com.android.server.clipboard.ClipboardChecker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLock;
} // .end method
static android.util.SparseArray -$$Nest$fgetmStashUidClip ( com.android.server.clipboard.ClipboardChecker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mStashUidClip;
} // .end method
static void -$$Nest$fputmAiClipboardEnable ( com.android.server.clipboard.ClipboardChecker p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAiClipboardEnable:Z */
return;
} // .end method
static Integer -$$Nest$sfgetDEFAULT_FUNCTION ( ) { //bridge//synthethic
/* .locals 1 */
} // .end method
static com.android.server.clipboard.ClipboardChecker ( ) {
/* .locals 4 */
/* .line 39 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* .line 41 */
final String v0 = "ro.miui.ui.version.code"; // const-string v0, "ro.miui.ui.version.code"
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* const/16 v3, 0xb */
/* if-lt v0, v3, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* nop */
} // :goto_0
com.android.server.clipboard.ClipboardChecker.MIUI12_5_PRIVACY_ENABLE = (v2!= 0);
/* .line 42 */
/* sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z */
/* xor-int/2addr v0, v1 */
/* .line 61 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 64 */
final String v1 = "com.android.browser"; // const-string v1, "com.android.browser"
/* .line 65 */
final String v1 = "com.milink.service"; // const-string v1, "com.milink.service"
/* .line 66 */
return;
} // .end method
private com.android.server.clipboard.ClipboardChecker ( ) {
/* .locals 2 */
/* .line 84 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryClipData:I */
/* .line 59 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mMatchHistoryCaller = v0;
/* .line 80 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mFirstObserve:Z */
/* .line 82 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 85 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mPatternMap = v0;
/* .line 86 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mTraceClickInfoMap = v0;
/* .line 87 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mStashUidClip = v0;
/* .line 88 */
/* new-instance v0, Lcom/android/server/clipboard/ClipboardChecker$1; */
com.android.internal.os.BackgroundThread .get ( );
(( com.android.internal.os.BackgroundThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/internal/os/BackgroundThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/clipboard/ClipboardChecker$1;-><init>(Lcom/android/server/clipboard/ClipboardChecker;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 96 */
return;
} // .end method
private com.android.server.appop.AppOpsService getAppOpsService ( ) {
/* .locals 2 */
/* .line 182 */
v0 = this.mAppOpsService;
/* if-nez v0, :cond_0 */
/* .line 183 */
final String v0 = "appops"; // const-string v0, "appops"
android.os.ServiceManager .getService ( v0 );
/* .line 184 */
/* .local v0, "b":Landroid/os/IBinder; */
com.android.internal.app.IAppOpsService$Stub .asInterface ( v0 );
/* check-cast v1, Lcom/android/server/appop/AppOpsService; */
this.mAppOpsService = v1;
/* .line 186 */
} // .end local v0 # "b":Landroid/os/IBinder;
} // :cond_0
v0 = this.mAppOpsService;
} // .end method
public static com.android.server.clipboard.ClipboardChecker getInstance ( ) {
/* .locals 2 */
/* .line 69 */
v0 = com.android.server.clipboard.ClipboardChecker.sInstance;
/* if-nez v0, :cond_1 */
/* .line 70 */
/* const-class v0, Lcom/android/server/clipboard/ClipboardChecker; */
/* monitor-enter v0 */
/* .line 71 */
try { // :try_start_0
v1 = com.android.server.clipboard.ClipboardChecker.sInstance;
/* if-nez v1, :cond_0 */
/* .line 72 */
/* new-instance v1, Lcom/android/server/clipboard/ClipboardChecker; */
/* invoke-direct {v1}, Lcom/android/server/clipboard/ClipboardChecker;-><init>()V */
/* .line 74 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 76 */
} // :cond_1
} // :goto_0
v0 = com.android.server.clipboard.ClipboardChecker.sInstance;
} // .end method
/* # virtual methods */
public void applyReadClipboardOperation ( Boolean p0, Integer p1, java.lang.String p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "allow" # Z */
/* .param p2, "uid" # I */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .param p4, "type" # I */
/* .line 176 */
com.android.server.appop.AppOpsServiceStub .getInstance ( );
/* const/16 v3, 0x1d */
/* .line 177 */
/* xor-int/lit8 v4, p1, 0x1 */
/* const/16 v6, 0xc8 */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* .line 176 */
/* move v1, p2 */
/* move-object v2, p3 */
/* move v5, p4 */
/* invoke-virtual/range {v0 ..v8}, Lcom/android/server/appop/AppOpsServiceStub;->onAppApplyOperation(ILjava/lang/String;IIIIIZ)V */
/* .line 179 */
return;
} // .end method
public android.content.ClipData getClipItemData ( ) {
/* .locals 1 */
/* .line 190 */
v0 = this.mClipItemData;
} // .end method
public java.util.Map getClipboardClickTrack ( ) {
/* .locals 1 */
/* .line 223 */
v0 = this.mTraceClickInfoMap;
} // .end method
public android.content.ClipData getStashClip ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 194 */
/* monitor-enter p0 */
/* .line 195 */
try { // :try_start_0
v0 = this.mStashUidClip;
com.android.server.clipboard.ClipboardServiceStub .get ( );
v1 = this.EMPTY_CLIP;
(( android.util.SparseArray ) v0 ).get ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Landroid/content/ClipData; */
/* monitor-exit p0 */
/* .line 196 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public Boolean hasStash ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 215 */
v0 = this.mStashUidClip;
v0 = (( android.util.SparseArray ) v0 ).indexOfKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I
/* if-ltz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isAiClipboardEnable ( android.content.Context p0 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 227 */
final String v0 = "mi_lab_ai_clipboard_enable"; // const-string v0, "mi_lab_ai_clipboard_enable"
/* sget-boolean v1, Lcom/android/server/clipboard/ClipboardChecker;->MIUI12_5_PRIVACY_ENABLE:Z */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v4, p0, Lcom/android/server/clipboard/ClipboardChecker;->mFirstObserve:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 228 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v4 */
/* .line 230 */
/* .local v4, "identity":J */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 231 */
/* .local v6, "resolver":Landroid/content/ContentResolver; */
int v8 = -2; // const/4 v8, -0x2
v7 = android.provider.Settings$Secure .getIntForUser ( v6,v0,v7,v8 );
/* if-ne v7, v2, :cond_0 */
/* move v7, v2 */
} // :cond_0
/* move v7, v3 */
} // :goto_0
/* iput-boolean v7, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAiClipboardEnable:Z */
/* .line 233 */
/* nop */
/* .line 234 */
android.provider.Settings$Secure .getUriFor ( v0 );
/* new-instance v7, Lcom/android/server/clipboard/ClipboardChecker$2; */
/* .line 235 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v7, p0, v8, v6}, Lcom/android/server/clipboard/ClipboardChecker$2;-><init>(Lcom/android/server/clipboard/ClipboardChecker;Landroid/os/Handler;Landroid/content/ContentResolver;)V */
/* .line 233 */
int v8 = -1; // const/4 v8, -0x1
(( android.content.ContentResolver ) v6 ).registerContentObserver ( v0, v3, v7, v8 ); // invoke-virtual {v6, v0, v3, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 244 */
} // .end local v6 # "resolver":Landroid/content/ContentResolver;
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 245 */
/* nop */
/* .line 246 */
/* iput-boolean v3, p0, Lcom/android/server/clipboard/ClipboardChecker;->mFirstObserve:Z */
/* .line 244 */
/* :catchall_0 */
/* move-exception v0 */
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 245 */
/* throw v0 */
/* .line 248 */
} // .end local v4 # "identity":J
} // :cond_1
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mAiClipboardEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_2
/* move v2, v3 */
} // :goto_2
} // .end method
public Integer matchClipboardRule ( java.lang.String p0, Integer p1, java.lang.CharSequence p2, Integer p3, Boolean p4 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "callerUid" # I */
/* .param p3, "content" # Ljava/lang/CharSequence; */
/* .param p4, "clipCode" # I */
/* .param p5, "isSystem" # Z */
/* .line 102 */
int v0 = 2; // const/4 v0, 0x2
/* if-nez p3, :cond_0 */
/* .line 103 */
/* .line 105 */
} // :cond_0
if ( p5 != null) { // if-eqz p5, :cond_1
v1 = v1 = com.android.server.clipboard.ClipboardChecker.sAllowClipboardSet;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 106 */
int v0 = 3; // const/4 v0, 0x3
/* .line 108 */
} // :cond_1
v1 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v1, :cond_6 */
v1 = v1 = this.mPatternMap;
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 110 */
v0 = this.mPatternMap;
/* check-cast v0, Ljava/util/List; */
v1 = } // :goto_0
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_5
/* check-cast v1, Ljava/util/regex/Pattern; */
/* .line 111 */
/* .local v1, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v1 ).matcher ( p3 ); // invoke-virtual {v1, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
v3 = (( java.util.regex.Matcher ) v3 ).find ( ); // invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 113 */
if ( p5 != null) { // if-eqz p5, :cond_2
final String v0 = "com.android.quicksearchbox"; // const-string v0, "com.android.quicksearchbox"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryClipData:I */
/* if-ne v0, p4, :cond_2 */
v0 = this.mMatchHistoryCaller;
/* .line 114 */
v0 = java.lang.Integer .valueOf ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 115 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MIUILOG- Permission Denied when read clipboard repeatedly, caller "; // const-string v3, "MIUILOG- Permission Denied when read clipboard repeatedly, caller "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ClipboardServiceI"; // const-string v3, "ClipboardServiceI"
android.util.Log .i ( v3,v0 );
/* .line 116 */
/* .line 118 */
} // :cond_2
/* iget v0, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryClipData:I */
/* if-eq v0, p4, :cond_3 */
/* .line 119 */
/* iput p4, p0, Lcom/android/server/clipboard/ClipboardChecker;->mMatchHistoryClipData:I */
/* .line 120 */
v0 = this.mMatchHistoryCaller;
/* .line 122 */
} // :cond_3
v0 = this.mMatchHistoryCaller;
java.lang.Integer .valueOf ( p2 );
/* .line 123 */
int v0 = 0; // const/4 v0, 0x0
/* .line 125 */
} // .end local v1 # "pattern":Ljava/util/regex/Pattern;
} // :cond_4
/* .line 126 */
} // :cond_5
/* .line 128 */
} // :cond_6
} // .end method
public void removeStashClipLater ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 208 */
v0 = this.mHandler;
v0 = (( android.os.Handler ) v0 ).hasMessages ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v0, :cond_1 */
v0 = (( com.android.server.clipboard.ClipboardChecker ) p0 ).hasStash ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/clipboard/ClipboardChecker;->hasStash(I)Z
/* if-nez v0, :cond_0 */
/* .line 211 */
} // :cond_0
v0 = this.mHandler;
/* const-wide/16 v1, 0x3e8 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 212 */
return;
/* .line 209 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void stashClip ( Integer p0, android.content.ClipData p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "content" # Landroid/content/ClipData; */
/* .line 200 */
this.mClipItemData = p2;
/* .line 201 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 202 */
try { // :try_start_0
v1 = this.mStashUidClip;
(( android.util.SparseArray ) v1 ).put ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 203 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).removeMessages ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 204 */
/* monitor-exit v0 */
/* .line 205 */
return;
/* .line 204 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateClipItemData ( android.content.ClipData p0 ) {
/* .locals 0 */
/* .param p1, "content" # Landroid/content/ClipData; */
/* .line 219 */
this.mClipItemData = p1;
/* .line 220 */
return;
} // .end method
public void updateClipboardPatterns ( java.util.List p0 ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/content/ClipboardRuleInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 135 */
/* .local p1, "ruleInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ClipboardRuleInfo;>;" */
/* monitor-enter p0 */
/* .line 136 */
if ( p1 != null) { // if-eqz p1, :cond_7
v0 = try { // :try_start_0
/* if-lez v0, :cond_7 */
/* .line 137 */
int v0 = 0; // const/4 v0, 0x0
/* .line 138 */
/* .local v0, "patternMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/regex/Pattern;>;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 139 */
/* .local v1, "traceClickInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Landroid/content/ClipboardRuleInfo; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 141 */
/* .local v3, "item":Landroid/content/ClipboardRuleInfo; */
try { // :try_start_1
v4 = (( android.content.ClipboardRuleInfo ) v3 ).getType ( ); // invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getType()I
int v5 = 1; // const/4 v5, 0x1
/* if-ne v4, v5, :cond_2 */
/* .line 142 */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 143 */
/* .local v4, "patternList":Ljava/util/List;, "Ljava/util/List<Ljava/util/regex/Pattern;>;" */
(( android.content.ClipboardRuleInfo ) v3 ).getRuleInfo ( ); // invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getRuleInfo()Ljava/util/List;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_0
/* check-cast v6, Ljava/lang/String; */
/* .line 144 */
/* .local v6, "ruleItem":Ljava/lang/String; */
java.util.regex.Pattern .compile ( v6 );
/* .line 145 */
/* nop */
} // .end local v6 # "ruleItem":Ljava/lang/String;
/* .line 146 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 147 */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* move-object v0, v5 */
/* .line 149 */
} // :cond_1
(( android.content.ClipboardRuleInfo ) v3 ).getPkgName ( ); // invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getPkgName()Ljava/lang/String;
/* .line 150 */
/* nop */
} // .end local v4 # "patternList":Ljava/util/List;, "Ljava/util/List<Ljava/util/regex/Pattern;>;"
} // :cond_2
v4 = (( android.content.ClipboardRuleInfo ) v3 ).getType ( ); // invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getType()I
int v5 = 2; // const/4 v5, 0x2
/* if-ne v4, v5, :cond_4 */
/* .line 151 */
v4 = (( android.content.ClipboardRuleInfo ) v3 ).getRuleInfo ( ); // invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getRuleInfo()Ljava/util/List;
/* if-lez v4, :cond_4 */
/* .line 152 */
/* if-nez v1, :cond_3 */
/* .line 153 */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
/* move-object v1, v4 */
/* .line 155 */
} // :cond_3
(( android.content.ClipboardRuleInfo ) v3 ).getPkgName ( ); // invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getPkgName()Ljava/lang/String;
(( android.content.ClipboardRuleInfo ) v3 ).getRuleInfo ( ); // invoke-virtual {v3}, Landroid/content/ClipboardRuleInfo;->getRuleInfo()Ljava/util/List;
int v6 = 0; // const/4 v6, 0x0
/* check-cast v5, Ljava/lang/String; */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 161 */
} // :cond_4
} // :goto_2
/* nop */
/* .line 162 */
} // .end local v3 # "item":Landroid/content/ClipboardRuleInfo;
/* .line 158 */
/* .restart local v3 # "item":Landroid/content/ClipboardRuleInfo; */
/* :catch_0 */
/* move-exception v2 */
/* .line 159 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v4 = "ClipboardServiceI"; // const-string v4, "ClipboardServiceI"
/* const-string/jumbo v5, "updateClipboardPatterns error" */
android.util.Log .e ( v4,v5,v2 );
/* .line 160 */
/* monitor-exit p0 */
return;
/* .line 163 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // .end local v3 # "item":Landroid/content/ClipboardRuleInfo;
} // :cond_5
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 164 */
v2 = this.mPatternMap;
/* .line 165 */
v2 = this.mPatternMap;
/* .line 167 */
} // :cond_6
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 168 */
v2 = this.mTraceClickInfoMap;
/* .line 169 */
v2 = this.mTraceClickInfoMap;
/* .line 172 */
} // .end local v0 # "patternMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/util/regex/Pattern;>;>;"
} // .end local v1 # "traceClickInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
} // :cond_7
/* monitor-exit p0 */
/* .line 173 */
return;
/* .line 172 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
} // .end method
