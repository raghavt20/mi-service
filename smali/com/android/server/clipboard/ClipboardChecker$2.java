class com.android.server.clipboard.ClipboardChecker$2 extends android.database.ContentObserver {
	 /* .source "ClipboardChecker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/clipboard/ClipboardChecker;->isAiClipboardEnable(Landroid/content/Context;)Z */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.clipboard.ClipboardChecker this$0; //synthetic
final android.content.ContentResolver val$resolver; //synthetic
/* # direct methods */
 com.android.server.clipboard.ClipboardChecker$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/clipboard/ClipboardChecker; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 235 */
this.this$0 = p1;
this.val$resolver = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 238 */
/* invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V */
/* .line 239 */
v0 = this.this$0;
v1 = this.val$resolver;
v2 = com.android.server.clipboard.ClipboardChecker .-$$Nest$sfgetDEFAULT_FUNCTION ( );
int v3 = -2; // const/4 v3, -0x2
final String v4 = "mi_lab_ai_clipboard_enable"; // const-string v4, "mi_lab_ai_clipboard_enable"
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v4,v2,v3 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
com.android.server.clipboard.ClipboardChecker .-$$Nest$fputmAiClipboardEnable ( v0,v2 );
/* .line 241 */
return;
} // .end method
