public class com.android.server.ForceDarkAppConfigProvider {
	 /* .source "ForceDarkAppConfigProvider.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CONFIG_VERSION;
private static final Boolean DEBUG;
private static final java.lang.String FORCE_DARK_APP_AMEND_NAME;
private static final java.lang.String FORCE_DARK_APP_CONFIG_FILE_PATH;
private static final java.lang.String FORCE_DARK_APP_CONFIG_FILE_VERSION_PATH;
private static final java.lang.String FORCE_DARK_APP_MODULE_NAME;
private static final java.lang.String TAG;
private static volatile com.android.server.ForceDarkAppConfigProvider sCloudDataHelper;
/* # instance fields */
private Boolean debugDisableCloud;
private java.util.HashMap mAmendForceDarkAppConfig;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener mAppConfigChangeListener;
private Integer mCloudConfigVersion;
private java.util.HashMap mCloudForceDarkAppConfig;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mLocalConfigVersion;
private java.util.HashMap mLocalForceDarkAppConfig;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.ForceDarkAppConfigProvider.TAG;
} // .end method
static com.android.server.ForceDarkAppConfigProvider ( ) {
/* .locals 1 */
/* .line 33 */
/* const-class v0, Lcom/android/server/ForceDarkAppConfigProvider; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 45 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
private com.android.server.ForceDarkAppConfigProvider ( ) {
/* .locals 3 */
/* .line 57 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 42 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudConfigVersion:I */
/* .line 43 */
/* iput v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalConfigVersion:I */
/* .line 48 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mLocalForceDarkAppConfig = v0;
/* .line 50 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCloudForceDarkAppConfig = v0;
/* .line 52 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAmendForceDarkAppConfig = v0;
/* .line 58 */
/* const-string/jumbo v0, "sys.forcedark.disable_cloud" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
/* move v1, v2 */
} // :cond_0
/* iput-boolean v1, p0, Lcom/android/server/ForceDarkAppConfigProvider;->debugDisableCloud:Z */
/* .line 59 */
return;
} // .end method
private java.lang.String getAmendAppConfig ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 186 */
v0 = this.mAmendForceDarkAppConfig;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 187 */
v0 = this.mAmendForceDarkAppConfig;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* .line 189 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private java.lang.String getCloudAppConfig ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 179 */
v0 = this.mCloudForceDarkAppConfig;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 180 */
v0 = this.mCloudForceDarkAppConfig;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* .line 182 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static com.android.server.ForceDarkAppConfigProvider getInstance ( ) {
/* .locals 2 */
/* .line 62 */
v0 = com.android.server.ForceDarkAppConfigProvider.sCloudDataHelper;
/* if-nez v0, :cond_1 */
/* .line 63 */
/* const-class v0, Lcom/android/server/ForceDarkAppConfigProvider; */
/* monitor-enter v0 */
/* .line 64 */
try { // :try_start_0
v1 = com.android.server.ForceDarkAppConfigProvider.sCloudDataHelper;
/* if-nez v1, :cond_0 */
/* .line 65 */
/* new-instance v1, Lcom/android/server/ForceDarkAppConfigProvider; */
/* invoke-direct {v1}, Lcom/android/server/ForceDarkAppConfigProvider;-><init>()V */
/* .line 67 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 69 */
} // :cond_1
} // :goto_0
v0 = com.android.server.ForceDarkAppConfigProvider.sCloudDataHelper;
} // .end method
private java.lang.String getLocalAppConfig ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 193 */
final String v0 = "../"; // const-string v0, "../"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 194 */
/* .line 196 */
} // :cond_0
v0 = this.mLocalForceDarkAppConfig;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 197 */
v0 = this.mLocalForceDarkAppConfig;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* .line 199 */
} // :cond_1
/* new-instance v0, Ljava/io/File; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "system_ext/etc/forcedarkconfig/" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ".json"; // const-string v3, ".json"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 200 */
/* .local v0, "localFile":Ljava/io/File; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 201 */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileInputStream; */
/* invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 202 */
/* .local v2, "inputStream":Ljava/io/FileInputStream; */
try { // :try_start_1
/* invoke-direct {p0, v2}, Lcom/android/server/ForceDarkAppConfigProvider;->readFully(Ljava/io/InputStream;)Ljava/lang/String; */
/* .line 203 */
/* .local v3, "jsonString":Ljava/lang/String; */
v4 = this.mLocalForceDarkAppConfig;
(( java.util.HashMap ) v4 ).put ( p1, v3 ); // invoke-virtual {v4, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 204 */
/* nop */
/* .line 205 */
try { // :try_start_2
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 204 */
/* .line 201 */
} // .end local v3 # "jsonString":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "localFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/ForceDarkAppConfigProvider;
} // .end local p1 # "packageName":Ljava/lang/String;
} // :goto_0
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 205 */
} // .end local v2 # "inputStream":Ljava/io/FileInputStream;
/* .restart local v0 # "localFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/ForceDarkAppConfigProvider; */
/* .restart local p1 # "packageName":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 206 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 209 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_2
} // .end method
private void getLocalAppConfigVersion ( ) {
/* .locals 6 */
/* .line 214 */
final String v0 = "configVersion"; // const-string v0, "configVersion"
/* new-instance v1, Ljava/io/File; */
/* const-string/jumbo v2, "system_ext/etc/forcedarkconfig/ForceDarkAppConfigVersion.json" */
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 215 */
/* .local v1, "versionFile":Ljava/io/File; */
v3 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 216 */
try { // :try_start_0
/* new-instance v3, Ljava/io/FileInputStream; */
/* invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v2, v3 */
/* .line 217 */
/* .local v2, "inputStream":Ljava/io/FileInputStream; */
try { // :try_start_1
/* invoke-direct {p0, v2}, Lcom/android/server/ForceDarkAppConfigProvider;->readFully(Ljava/io/InputStream;)Ljava/lang/String; */
/* .line 218 */
/* .local v3, "jsonString":Ljava/lang/String; */
/* new-instance v4, Lorg/json/JSONObject; */
/* invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 219 */
/* .local v4, "configVersionJson":Lorg/json/JSONObject; */
v5 = (( org.json.JSONObject ) v4 ).has ( v0 ); // invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 220 */
(( org.json.JSONObject ) v4 ).getString ( v0 ); // invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v0 = java.lang.Integer .parseInt ( v0 );
/* iput v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalConfigVersion:I */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 222 */
} // .end local v3 # "jsonString":Ljava/lang/String;
} // .end local v4 # "configVersionJson":Lorg/json/JSONObject;
} // :cond_0
try { // :try_start_2
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catch Lorg/json/JSONException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 224 */
} // .end local v2 # "inputStream":Ljava/io/FileInputStream;
/* .line 216 */
/* .restart local v2 # "inputStream":Ljava/io/FileInputStream; */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_3
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v0 ).addSuppressed ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "versionFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/ForceDarkAppConfigProvider;
} // :goto_0
/* throw v0 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catch Lorg/json/JSONException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 222 */
} // .end local v2 # "inputStream":Ljava/io/FileInputStream;
/* .restart local v1 # "versionFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/ForceDarkAppConfigProvider; */
/* :catch_0 */
/* move-exception v0 */
/* .line 223 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 226 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
return;
} // .end method
private java.lang.String readFully ( java.io.InputStream p0 ) {
/* .locals 4 */
/* .param p1, "inputStream" # Ljava/io/InputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 230 */
/* new-instance v0, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 231 */
/* .local v0, "baos":Ljava/io/OutputStream; */
/* const/16 v1, 0x400 */
/* new-array v1, v1, [B */
/* .line 232 */
/* .local v1, "buffer":[B */
v2 = (( java.io.InputStream ) p1 ).read ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I
/* .line 233 */
/* .local v2, "read":I */
} // :goto_0
/* if-ltz v2, :cond_0 */
/* .line 234 */
int v3 = 0; // const/4 v3, 0x0
(( java.io.OutputStream ) v0 ).write ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V
/* .line 235 */
v2 = (( java.io.InputStream ) p1 ).read ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I
/* .line 237 */
} // :cond_0
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
} // .end method
private void registerDataObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 141 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 142 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/ForceDarkAppConfigProvider$1; */
/* .line 143 */
com.android.server.MiuiBgThread .getHandler ( );
/* invoke-direct {v2, p0, v3, p1}, Lcom/android/server/ForceDarkAppConfigProvider$1;-><init>(Lcom/android/server/ForceDarkAppConfigProvider;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 141 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 150 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getForceDarkAppConfig ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 157 */
int v0 = 0; // const/4 v0, 0x0
/* .line 158 */
/* .local v0, "configJson":Ljava/lang/String; */
/* iget v1, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudConfigVersion:I */
/* iget v2, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalConfigVersion:I */
/* if-lt v1, v2, :cond_0 */
/* .line 159 */
/* invoke-direct {p0, p1}, Lcom/android/server/ForceDarkAppConfigProvider;->getCloudAppConfig(Ljava/lang/String;)Ljava/lang/String; */
/* .line 161 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 162 */
/* invoke-direct {p0, p1}, Lcom/android/server/ForceDarkAppConfigProvider;->getLocalAppConfig(Ljava/lang/String;)Ljava/lang/String; */
/* .line 164 */
} // :cond_1
v1 = com.android.server.ForceDarkAppConfigProvider.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "cloudConfigVersion: "; // const-string v3, "cloudConfigVersion: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudConfigVersion:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " localConfigVersion:"; // const-string v3, " localConfigVersion:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mLocalConfigVersion:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 165 */
/* invoke-direct {p0, p1}, Lcom/android/server/ForceDarkAppConfigProvider;->getAmendAppConfig(Ljava/lang/String;)Ljava/lang/String; */
/* .line 166 */
/* .local v1, "amendJson":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 168 */
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 169 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
final String v3 = "amend"; // const-string v3, "amend"
(( org.json.JSONObject ) v2 ).put ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 170 */
(( org.json.JSONObject ) v2 ).toString ( ); // invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 171 */
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
/* :catch_0 */
/* move-exception v2 */
/* .line 172 */
/* .local v2, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
/* .line 175 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :cond_2
} // .end method
public void onBootPhase ( Integer p0, android.content.Context p1 ) {
/* .locals 0 */
/* .param p1, "phase" # I */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 82 */
(( com.android.server.ForceDarkAppConfigProvider ) p0 ).updateLocalForceDarkAppConfig ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/ForceDarkAppConfigProvider;->updateLocalForceDarkAppConfig(Landroid/content/Context;)V
/* .line 83 */
/* invoke-direct {p0, p2}, Lcom/android/server/ForceDarkAppConfigProvider;->registerDataObserver(Landroid/content/Context;)V */
/* .line 84 */
/* invoke-direct {p0}, Lcom/android/server/ForceDarkAppConfigProvider;->getLocalAppConfigVersion()V */
/* .line 85 */
return;
} // .end method
public void setAppConfigChangeListener ( com.android.server.ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener p0 ) {
/* .locals 0 */
/* .param p1, "listChangeListener" # Lcom/android/server/ForceDarkAppConfigProvider$ForceDarkAppConfigChangeListener; */
/* .line 78 */
this.mAppConfigChangeListener = p1;
/* .line 79 */
return;
} // .end method
public void updateLocalForceDarkAppConfig ( android.content.Context p0 ) {
/* .locals 12 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 89 */
final String v0 = "configVersion"; // const-string v0, "configVersion"
final String v1 = "force_dark_app_config_amend"; // const-string v1, "force_dark_app_config_amend"
final String v2 = "force_dark_app_config"; // const-string v2, "force_dark_app_config"
/* iget-boolean v3, p0, Lcom/android/server/ForceDarkAppConfigProvider;->debugDisableCloud:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 90 */
v0 = this.mCloudForceDarkAppConfig;
(( java.util.HashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
/* .line 91 */
v0 = com.android.server.ForceDarkAppConfigProvider.TAG;
final String v1 = "getCloudForceDarkConfig empty for disableCloud"; // const-string v1, "getCloudForceDarkConfig empty for disableCloud"
android.util.Slog .d ( v0,v1 );
/* .line 92 */
return;
/* .line 96 */
} // :cond_0
/* nop */
/* .line 97 */
try { // :try_start_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 96 */
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v3,v2,v2,v5,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 99 */
/* .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v6 = "packageName"; // const-string v6, "packageName"
if ( v3 != null) { // if-eqz v3, :cond_2
try { // :try_start_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).json ( ); // invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 100 */
v7 = com.android.server.ForceDarkAppConfigProvider.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "getCloudConfig, force_dark_app_config: "; // const-string v9, "getCloudConfig, force_dark_app_config: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).toString ( ); // invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v8 );
/* .line 101 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).json ( ); // invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 102 */
/* .local v7, "jsonAll":Lorg/json/JSONObject; */
v8 = (( org.json.JSONObject ) v7 ).has ( v2 ); // invoke-virtual {v7, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 103 */
v8 = this.mCloudForceDarkAppConfig;
(( java.util.HashMap ) v8 ).clear ( ); // invoke-virtual {v8}, Ljava/util/HashMap;->clear()V
/* .line 104 */
v8 = (( org.json.JSONObject ) v7 ).has ( v0 ); // invoke-virtual {v7, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 105 */
(( org.json.JSONObject ) v7 ).getString ( v0 ); // invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v0 = java.lang.Integer .parseInt ( v0 );
/* iput v0, p0, Lcom/android/server/ForceDarkAppConfigProvider;->mCloudConfigVersion:I */
/* .line 107 */
} // :cond_1
(( org.json.JSONObject ) v7 ).getJSONArray ( v2 ); // invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 108 */
/* .local v0, "appConfigArray":Lorg/json/JSONArray; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v8 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v2, v8, :cond_2 */
/* .line 109 */
(( org.json.JSONArray ) v0 ).getJSONObject ( v2 ); // invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 110 */
/* .local v8, "appConfig":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v8 ).getString ( v6 ); // invoke-virtual {v8, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 111 */
/* .local v9, "packageName":Ljava/lang/String; */
v10 = this.mCloudForceDarkAppConfig;
(( org.json.JSONObject ) v8 ).toString ( ); // invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.util.HashMap ) v10 ).put ( v9, v11 ); // invoke-virtual {v10, v9, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 108 */
/* nop */
} // .end local v8 # "appConfig":Lorg/json/JSONObject;
} // .end local v9 # "packageName":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 115 */
} // .end local v0 # "appConfigArray":Lorg/json/JSONArray;
} // .end local v2 # "i":I
} // .end local v7 # "jsonAll":Lorg/json/JSONObject;
} // :cond_2
/* nop */
/* .line 116 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 115 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v1,v5,v4 );
/* .line 118 */
} // .end local v3 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v0 != null) { // if-eqz v0, :cond_3
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 119 */
v2 = com.android.server.ForceDarkAppConfigProvider.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getCloudConfig, force_dark_app_config_amend: "; // const-string v4, "getCloudConfig, force_dark_app_config_amend: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).toString ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 120 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* .line 121 */
/* .local v2, "jsonAll":Lorg/json/JSONObject; */
v3 = (( org.json.JSONObject ) v2 ).has ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 122 */
v3 = this.mAmendForceDarkAppConfig;
(( java.util.HashMap ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->clear()V
/* .line 123 */
(( org.json.JSONObject ) v2 ).getJSONArray ( v1 ); // invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 124 */
/* .local v1, "appConfigArray":Lorg/json/JSONArray; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_1
v4 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v4, :cond_3 */
/* .line 125 */
(( org.json.JSONArray ) v1 ).getJSONObject ( v3 ); // invoke-virtual {v1, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 126 */
/* .local v4, "appConfig":Lorg/json/JSONObject; */
(( org.json.JSONObject ) v4 ).getString ( v6 ); // invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 127 */
/* .local v5, "packageName":Ljava/lang/String; */
v7 = this.mAmendForceDarkAppConfig;
(( org.json.JSONObject ) v4 ).toString ( ); // invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.util.HashMap ) v7 ).put ( v5, v8 ); // invoke-virtual {v7, v5, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 124 */
/* nop */
} // .end local v4 # "appConfig":Lorg/json/JSONObject;
} // .end local v5 # "packageName":Ljava/lang/String;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 132 */
} // .end local v1 # "appConfigArray":Lorg/json/JSONArray;
} // .end local v2 # "jsonAll":Lorg/json/JSONObject;
} // .end local v3 # "i":I
} // :cond_3
v1 = this.mAppConfigChangeListener;
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 133 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 137 */
} // .end local v0 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // :cond_4
/* .line 135 */
/* :catch_0 */
/* move-exception v0 */
/* .line 136 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.ForceDarkAppConfigProvider.TAG;
final String v2 = "exception when getCloudForceDarkConfig: "; // const-string v2, "exception when getCloudForceDarkConfig: "
android.util.Slog .e ( v1,v2,v0 );
/* .line 138 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
