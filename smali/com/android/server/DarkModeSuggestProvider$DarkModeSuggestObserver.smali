.class Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;
.super Landroid/database/ContentObserver;
.source "DarkModeSuggestProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/DarkModeSuggestProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DarkModeSuggestObserver"
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/android/server/DarkModeSuggestProvider;


# direct methods
.method public constructor <init>(Lcom/android/server/DarkModeSuggestProvider;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .line 105
    iput-object p1, p0, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;->this$0:Lcom/android/server/DarkModeSuggestProvider;

    .line 106
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 101
    const-class p1, Lcom/android/server/DarkModeSuggestProvider;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;->TAG:Ljava/lang/String;

    .line 107
    iput-object p3, p0, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;->mContext:Landroid/content/Context;

    .line 108
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .line 112
    iget-object v0, p0, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;->TAG:Ljava/lang/String;

    const-string v1, "onChange"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget-object v0, p0, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;->this$0:Lcom/android/server/DarkModeSuggestProvider;

    iget-object v1, p0, Lcom/android/server/DarkModeSuggestProvider$DarkModeSuggestObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeSuggestProvider;->updateCloudDataForDarkModeSuggest(Landroid/content/Context;)V

    .line 114
    return-void
.end method
