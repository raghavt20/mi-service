.class Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;
.super Ljava/lang/Object;
.source "MiuiRestoreManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiRestoreManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackupDataTask"
.end annotation


# instance fields
.field private callback:Lmiui/app/backup/ITaskCommonCallback;

.field private domains:[Ljava/lang/String;

.field private excludePaths:[Ljava/lang/String;

.field private outFd:Landroid/os/ParcelFileDescriptor;

.field private packageName:Ljava/lang/String;

.field private parentPaths:[Ljava/lang/String;

.field private paths:[Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/MiuiRestoreManagerService;

.field private type:I


# direct methods
.method public constructor <init>(Lcom/android/server/MiuiRestoreManagerService;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Landroid/os/ParcelFileDescriptor;ILmiui/app/backup/ITaskCommonCallback;)V
    .locals 0
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "paths"    # [Ljava/lang/String;
    .param p4, "domains"    # [Ljava/lang/String;
    .param p5, "parentPaths"    # [Ljava/lang/String;
    .param p6, "excludePaths"    # [Ljava/lang/String;
    .param p7, "outFd"    # Landroid/os/ParcelFileDescriptor;
    .param p8, "type"    # I
    .param p9, "callback"    # Lmiui/app/backup/ITaskCommonCallback;

    .line 683
    iput-object p1, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 684
    iput-object p2, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->packageName:Ljava/lang/String;

    .line 685
    iput-object p3, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->paths:[Ljava/lang/String;

    .line 686
    iput-object p4, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->domains:[Ljava/lang/String;

    .line 687
    iput-object p5, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->parentPaths:[Ljava/lang/String;

    .line 688
    iput-object p6, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->excludePaths:[Ljava/lang/String;

    .line 689
    iput-object p7, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->outFd:Landroid/os/ParcelFileDescriptor;

    .line 690
    iput p8, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->type:I

    .line 691
    iput-object p9, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->callback:Lmiui/app/backup/ITaskCommonCallback;

    .line 692
    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .line 696
    const-string v0, "error when write EOF "

    const-string v1, "error when notify onTaskEnd "

    const-string v2, "MiuiRestoreManagerService"

    const/4 v3, 0x0

    .line 697
    .local v3, "errorCode":I
    const/4 v4, 0x0

    .line 699
    .local v4, "exception":Ljava/lang/String;
    const/4 v5, 0x4

    const/4 v6, 0x0

    :try_start_0
    iget-object v7, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->this$0:Lcom/android/server/MiuiRestoreManagerService;

    invoke-static {v7}, Lcom/android/server/MiuiRestoreManagerService;->-$$Nest$fgetmInstaller(Lcom/android/server/MiuiRestoreManagerService;)Lcom/android/server/pm/Installer;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->packageName:Ljava/lang/String;

    iget-object v10, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->paths:[Ljava/lang/String;

    iget-object v11, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->domains:[Ljava/lang/String;

    iget-object v12, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->parentPaths:[Ljava/lang/String;

    iget-object v13, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->excludePaths:[Ljava/lang/String;

    iget-object v14, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->outFd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual/range {v8 .. v14}, Lcom/android/server/pm/Installer;->backupAppData(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)I

    move-result v7
    :try_end_0
    .catch Lcom/android/server/pm/Installer$InstallerException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v3, v7

    .line 706
    :try_start_1
    iget-object v7, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->callback:Lmiui/app/backup/ITaskCommonCallback;

    invoke-interface {v7, v3, v4, v6}, Lmiui/app/backup/ITaskCommonCallback;->onTaskEnd(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 709
    goto :goto_0

    .line 707
    :catch_0
    move-exception v6

    .line 708
    .local v6, "e":Ljava/lang/Exception;
    invoke-static {v2, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 710
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->outFd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 711
    .local v1, "out":Ljava/io/FileOutputStream;
    new-array v5, v5, [B

    .line 713
    .local v5, "buf":[B
    :try_start_2
    invoke-virtual {v1, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 714
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 715
    :catch_1
    move-exception v6

    goto :goto_3

    .line 705
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .end local v5    # "buf":[B
    :catchall_0
    move-exception v7

    goto :goto_5

    .line 700
    :catch_2
    move-exception v7

    .line 701
    .local v7, "e":Lcom/android/server/pm/Installer$InstallerException;
    const/4 v3, 0x1

    .line 702
    :try_start_3
    invoke-virtual {v7}, Lcom/android/server/pm/Installer$InstallerException;->getMessage()Ljava/lang/String;

    move-result-object v8

    move-object v4, v8

    .line 703
    const-string v8, "backupData "

    invoke-static {v2, v8, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 706
    .end local v7    # "e":Lcom/android/server/pm/Installer$InstallerException;
    :try_start_4
    iget-object v7, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->callback:Lmiui/app/backup/ITaskCommonCallback;

    invoke-interface {v7, v3, v4, v6}, Lmiui/app/backup/ITaskCommonCallback;->onTaskEnd(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 709
    goto :goto_1

    .line 707
    :catch_3
    move-exception v6

    .line 708
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-static {v2, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 710
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_1
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->outFd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 711
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    new-array v5, v5, [B

    .line 713
    .restart local v5    # "buf":[B
    :try_start_5
    invoke-virtual {v1, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 714
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 717
    :goto_2
    goto :goto_4

    .line 715
    :catch_4
    move-exception v6

    .line 716
    .local v6, "e":Ljava/io/IOException;
    :goto_3
    invoke-static {v2, v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 719
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .end local v5    # "buf":[B
    .end local v6    # "e":Ljava/io/IOException;
    :goto_4
    nop

    .line 720
    return-void

    .line 706
    :goto_5
    :try_start_6
    iget-object v8, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->callback:Lmiui/app/backup/ITaskCommonCallback;

    invoke-interface {v8, v3, v4, v6}, Lmiui/app/backup/ITaskCommonCallback;->onTaskEnd(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 709
    goto :goto_6

    .line 707
    :catch_5
    move-exception v6

    .line 708
    .local v6, "e":Ljava/lang/Exception;
    invoke-static {v2, v1, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 710
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_6
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/android/server/MiuiRestoreManagerService$BackupDataTask;->outFd:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v6}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 711
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    new-array v5, v5, [B

    .line 713
    .restart local v5    # "buf":[B
    :try_start_7
    invoke-virtual {v1, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 714
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 717
    goto :goto_7

    .line 715
    :catch_6
    move-exception v6

    .line 716
    .local v6, "e":Ljava/io/IOException;
    invoke-static {v2, v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 719
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .end local v5    # "buf":[B
    .end local v6    # "e":Ljava/io/IOException;
    :goto_7
    throw v7
.end method
