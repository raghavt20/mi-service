public class com.android.server.print.MiuiPrintManager extends com.android.server.print.MiuiPrintManagerStub {
	 /* .source "MiuiPrintManager.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.print.UserState$$" */
} // .end annotation
/* # static fields */
public static final java.lang.String MIUI_ACTION_PRINT_DIALOG;
public static final java.lang.String MIUI_PREFIX;
public static final java.lang.String TAG;
/* # direct methods */
public com.android.server.print.MiuiPrintManager ( ) {
	 /* .locals 0 */
	 /* .line 12 */
	 /* invoke-direct {p0}, Lcom/android/server/print/MiuiPrintManagerStub;-><init>()V */
	 return;
} // .end method
/* # virtual methods */
public Boolean ensureInjected ( android.print.PrintJobInfo p0 ) {
	 /* .locals 3 */
	 /* .param p1, "info" # Landroid/print/PrintJobInfo; */
	 /* .line 31 */
	 (( android.print.PrintJobInfo ) p1 ).getLabel ( ); // invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getLabel()Ljava/lang/String;
	 /* .line 32 */
	 /* .local v0, "jobName":Ljava/lang/String; */
	 v1 = 	 android.text.TextUtils .isEmpty ( v0 );
	 /* if-nez v1, :cond_0 */
	 final String v1 = "MIUI:"; // const-string v1, "MIUI:"
	 v2 = 	 (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 34 */
		 v1 = 		 (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
		 (( java.lang.String ) v0 ).substring ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
		 (( android.print.PrintJobInfo ) p1 ).setLabel ( v1 ); // invoke-virtual {p1, v1}, Landroid/print/PrintJobInfo;->setLabel(Ljava/lang/String;)V
		 /* .line 35 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* .line 37 */
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
} // .end method
public void printInject ( Boolean p0, android.content.Intent p1 ) {
	 /* .locals 2 */
	 /* .param p1, "injected" # Z */
	 /* .param p2, "intent" # Landroid/content/Intent; */
	 /* .line 48 */
	 if ( p1 != null) { // if-eqz p1, :cond_0
		 /* .line 52 */
		 final String v0 = "miui.print.PRINT_DIALOG"; // const-string v0, "miui.print.PRINT_DIALOG"
		 (( android.content.Intent ) p2 ).setAction ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 54 */
		 final String v0 = "MiuiPrintManager"; // const-string v0, "MiuiPrintManager"
		 final String v1 = "printInject....will handle for MIUI target."; // const-string v1, "printInject....will handle for MIUI target."
		 android.util.Log .d ( v0,v1 );
		 /* .line 56 */
	 } // :cond_0
	 return;
} // .end method
