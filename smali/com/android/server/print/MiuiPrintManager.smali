.class public Lcom/android/server/print/MiuiPrintManager;
.super Lcom/android/server/print/MiuiPrintManagerStub;
.source "MiuiPrintManager.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.print.UserState$$"
.end annotation


# static fields
.field public static final MIUI_ACTION_PRINT_DIALOG:Ljava/lang/String; = "miui.print.PRINT_DIALOG"

.field public static final MIUI_PREFIX:Ljava/lang/String; = "MIUI:"

.field public static final TAG:Ljava/lang/String; = "MiuiPrintManager"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/android/server/print/MiuiPrintManagerStub;-><init>()V

    return-void
.end method


# virtual methods
.method public ensureInjected(Landroid/print/PrintJobInfo;)Z
    .locals 3
    .param p1, "info"    # Landroid/print/PrintJobInfo;

    .line 31
    invoke-virtual {p1}, Landroid/print/PrintJobInfo;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "jobName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "MIUI:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/print/PrintJobInfo;->setLabel(Ljava/lang/String;)V

    .line 35
    const/4 v1, 0x1

    return v1

    .line 37
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public printInject(ZLandroid/content/Intent;)V
    .locals 2
    .param p1, "injected"    # Z
    .param p2, "intent"    # Landroid/content/Intent;

    .line 48
    if-eqz p1, :cond_0

    .line 52
    const-string v0, "miui.print.PRINT_DIALOG"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const-string v0, "MiuiPrintManager"

    const-string v1, "printInject....will handle for MIUI target."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_0
    return-void
.end method
