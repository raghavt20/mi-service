.class Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;
.super Ljava/lang/Object;
.source "MiSyncStrategyImpl.java"

# interfaces
.implements Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/content/MiSyncStrategyImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CleverMJStrategy"
.end annotation


# static fields
.field private static final ALLOW_FIRST_SYNC_THRESHOLD:I = 0x3

.field private static final ALLOW_FIRST_SYNC_THRESHOLD_FOR_BROWSER:I = 0x8

.field private static final AUTHORITY_BROWSER:Ljava/lang/String; = "com.miui.browser"

.field private static final AUTHORITY_CALENDAR:Ljava/lang/String; = "com.android.calendar"

.field private static final AUTHORITY_CONTACTS:Ljava/lang/String; = "com.android.contacts"

.field private static final AUTHORITY_GALLERY:Ljava/lang/String; = "com.miui.gallery.cloud.provider"

.field private static final AUTHORITY_NOTES:Ljava/lang/String; = "notes"

.field private static final REAL_TIME_STRATEGY_AUTHORITY_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 195
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;->REAL_TIME_STRATEGY_AUTHORITY_SET:Ljava/util/Set;

    .line 198
    const-string v1, "com.android.calendar"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 199
    const-string v1, "notes"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 200
    const-string v1, "com.android.contacts"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 201
    const-string v1, "com.miui.gallery.cloud.provider"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 202
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;-><init>()V

    return-void
.end method

.method private isFirstTimes(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "authority"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 298
    const-string v0, "key_num_syncs"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 300
    .local v0, "num":I
    const-string v2, "com.miui.browser"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 301
    if-ltz v0, :cond_1

    const/16 v2, 0x8

    if-ge v0, v2, :cond_1

    .line 302
    return v3

    .line 305
    :cond_0
    if-ltz v0, :cond_1

    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    .line 306
    return v3

    .line 309
    :cond_1
    return v1
.end method


# virtual methods
.method public apply(Lcom/android/server/content/SyncOperation;Landroid/os/Bundle;Landroid/app/job/JobInfo$Builder;)V
    .locals 5
    .param p1, "syncOperation"    # Lcom/android/server/content/SyncOperation;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "builder"    # Landroid/app/job/JobInfo$Builder;

    .line 210
    const-string v0, "injector: apply: null parameter, return"

    const/4 v1, 0x3

    const-string v2, "Sync"

    if-eqz p1, :cond_7

    iget-object v3, p1, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    if-nez v3, :cond_0

    goto :goto_0

    .line 218
    :cond_0
    iget-object v3, p1, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    iget-object v3, v3, Lcom/android/server/content/SyncStorageEngine$EndPoint;->provider:Ljava/lang/String;

    .line 219
    .local v3, "authority":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 220
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 221
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :cond_1
    return-void

    .line 227
    :cond_2
    sget-object v0, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;->REAL_TIME_STRATEGY_AUTHORITY_SET:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 228
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    const-string v0, "injector: apply: authority is not affected by strategy, return"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_3
    return-void

    .line 235
    :cond_4
    invoke-direct {p0, v3, p2}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;->isFirstTimes(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 236
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 237
    const-string v0, "injector: apply: first full sync, return"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_5
    return-void

    .line 241
    :cond_6
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/app/job/JobInfo$Builder;->setRequiresCharging(Z)Landroid/app/job/JobInfo$Builder;

    .line 242
    return-void

    .line 211
    .end local v3    # "authority":Ljava/lang/String;
    :cond_7
    :goto_0
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 212
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :cond_8
    return-void
.end method

.method public isAllowedToRun(Lcom/android/server/content/SyncOperation;Landroid/os/Bundle;)Z
    .locals 17
    .param p1, "syncOperation"    # Lcom/android/server/content/SyncOperation;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 247
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "injector: isAllowedToRun: null parameter, return true"

    const/4 v3, 0x1

    const/4 v4, 0x3

    const-string v5, "Sync"

    if-eqz v0, :cond_9

    iget-object v6, v0, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    if-nez v6, :cond_0

    move-object/from16 v7, p0

    goto :goto_0

    .line 255
    :cond_0
    iget-object v6, v0, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    iget-object v6, v6, Lcom/android/server/content/SyncStorageEngine$EndPoint;->provider:Ljava/lang/String;

    .line 256
    .local v6, "authority":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 257
    invoke-static {v5, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 258
    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :cond_1
    return v3

    .line 264
    :cond_2
    sget-object v2, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;->REAL_TIME_STRATEGY_AUTHORITY_SET:Ljava/util/Set;

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 265
    invoke-static {v5, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 266
    const-string v2, "injector: isAllowedToRun: authority is not affected by strategy, return true"

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_3
    return v3

    .line 272
    :cond_4
    move-object/from16 v7, p0

    invoke-direct {v7, v6, v1}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;->isFirstTimes(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 273
    invoke-static {v5, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 274
    const-string v2, "injector: isAllowedToRun: first full sync, return true"

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :cond_5
    return v3

    .line 279
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 280
    .local v8, "currentTimeMills":J
    const-string v2, "key_interactive"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 282
    .local v2, "isInteractive":Z
    const-string v10, "key_last_screen_off_time"

    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 284
    .local v10, "lastScreenOffTime":J
    const-string v12, "key_battery_charging"

    invoke-virtual {v1, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 286
    .local v12, "isBatteryCharging":Z
    if-eqz v12, :cond_8

    if-nez v2, :cond_8

    sub-long v13, v8, v10

    const-wide/32 v15, 0x1d4c0

    cmp-long v13, v13, v15

    if-lez v13, :cond_8

    .line 288
    invoke-static {v5, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 289
    const-string v4, "injector: isAllowedToRun: condition is satisfied, return true"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    :cond_7
    return v3

    .line 294
    :cond_8
    const/4 v3, 0x0

    return v3

    .line 247
    .end local v2    # "isInteractive":Z
    .end local v6    # "authority":Ljava/lang/String;
    .end local v8    # "currentTimeMills":J
    .end local v10    # "lastScreenOffTime":J
    .end local v12    # "isBatteryCharging":Z
    :cond_9
    move-object/from16 v7, p0

    .line 248
    :goto_0
    invoke-static {v5, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 249
    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_a
    return v3
.end method
