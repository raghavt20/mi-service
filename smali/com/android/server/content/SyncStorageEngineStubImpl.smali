.class public Lcom/android/server/content/SyncStorageEngineStubImpl;
.super Ljava/lang/Object;
.source "SyncStorageEngineStubImpl.java"

# interfaces
.implements Lcom/android/server/content/SyncStorageEngineStub;


# static fields
.field private static final MI_PAUSE_FILE_NAME:Ljava/lang/String; = "mi_pause.xml"

.field private static final MI_STRATEGY_FILE_NAME:Ljava/lang/String; = "mi_strategy.xml"

.field private static final TAG:Ljava/lang/String; = "SyncManager"

.field private static final TAG_FILE:Ljava/lang/String; = "SyncManagerFile"

.field private static mMiPauseFile:Landroid/util/AtomicFile;

.field private static mMiStrategyFile:Landroid/util/AtomicFile;

.field private static mMiSyncPause:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/content/MiSyncPauseImpl;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mMiSyncStrategy:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/content/MiSyncStrategyImpl;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncPause:Landroid/util/SparseArray;

    .line 43
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncStrategy:Landroid/util/SparseArray;

    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiPauseFile:Landroid/util/AtomicFile;

    .line 49
    sput-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiStrategyFile:Landroid/util/AtomicFile;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static clear()V
    .locals 1

    .line 77
    sget-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncPause:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 78
    sget-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncStrategy:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 79
    return-void
.end method

.method private static containsXiaomiAccountName([Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 5
    .param p0, "accounts"    # [Landroid/accounts/Account;
    .param p1, "accountName"    # Ljava/lang/String;

    .line 373
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 374
    return v0

    .line 376
    :cond_0
    array-length v1, p0

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, p0, v2

    .line 377
    .local v3, "account":Landroid/accounts/Account;
    invoke-static {v3}, Lcom/android/server/content/MiSyncUtils;->checkAccount(Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 378
    iget-object v4, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 379
    const/4 v0, 0x1

    return v0

    .line 376
    .end local v3    # "account":Landroid/accounts/Account;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 383
    :cond_2
    return v0
.end method

.method private static doMiPauseCleanUpLocked([Landroid/accounts/Account;I)V
    .locals 4
    .param p0, "runningAccounts"    # [Landroid/accounts/Account;
    .param p1, "uid"    # I

    .line 323
    sget-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncPause:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 324
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncPauseImpl;>;"
    if-nez v0, :cond_1

    .line 325
    const/4 v1, 0x2

    const-string v2, "SyncManager"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    const-string v1, "doMiPauseCleanUpLocked: map is null"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :cond_0
    return-void

    .line 330
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getRemovingAccounts([Landroid/accounts/Account;Ljava/util/Set;)Ljava/util/List;

    move-result-object v1

    .line 331
    .local v1, "removing":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 332
    .local v3, "accountName":Ljava/lang/String;
    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    .end local v3    # "accountName":Ljava/lang/String;
    goto :goto_0

    .line 334
    :cond_2
    return-void
.end method

.method private static doMiStrategyCleanUpLocked([Landroid/accounts/Account;I)V
    .locals 4
    .param p0, "runningAccounts"    # [Landroid/accounts/Account;
    .param p1, "uid"    # I

    .line 337
    sget-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncStrategy:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 338
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncStrategyImpl;>;"
    if-nez v0, :cond_1

    .line 339
    const/4 v1, 0x2

    const-string v2, "SyncManager"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 340
    const-string v1, "doMiStrategyCleanUpLocked: map is null"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :cond_0
    return-void

    .line 344
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getRemovingAccounts([Landroid/accounts/Account;Ljava/util/Set;)Ljava/util/List;

    move-result-object v1

    .line 345
    .local v1, "removing":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 346
    .local v3, "accountName":Ljava/lang/String;
    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    .end local v3    # "accountName":Ljava/lang/String;
    goto :goto_0

    .line 348
    :cond_2
    return-void
.end method

.method private static getOrCreateMiSyncPauseLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncPauseImpl;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "uid"    # I

    .line 467
    sget-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncPause:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 468
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncPauseImpl;>;"
    if-nez v0, :cond_0

    .line 469
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v0, v1

    .line 470
    sget-object v1, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncPause:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472
    :cond_0
    const/4 v1, 0x0

    .line 473
    .local v1, "item":Lcom/android/server/content/MiSyncPauseImpl;
    if-nez p0, :cond_1

    .line 474
    const-string p0, ""

    .line 476
    :cond_1
    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 477
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    check-cast v1, Lcom/android/server/content/MiSyncPauseImpl;

    .line 479
    :cond_2
    if-nez v1, :cond_3

    .line 480
    new-instance v2, Lcom/android/server/content/MiSyncPauseImpl;

    invoke-direct {v2, p1, p0}, Lcom/android/server/content/MiSyncPauseImpl;-><init>(ILjava/lang/String;)V

    move-object v1, v2

    .line 481
    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    :cond_3
    return-object v1
.end method

.method private static getOrCreateMiSyncStrategyLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncStrategyImpl;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "uid"    # I

    .line 487
    sget-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncStrategy:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 488
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncStrategyImpl;>;"
    if-nez v0, :cond_0

    .line 489
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v0, v1

    .line 490
    sget-object v1, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncStrategy:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 492
    :cond_0
    const/4 v1, 0x0

    .line 493
    .local v1, "item":Lcom/android/server/content/MiSyncStrategyImpl;
    if-nez p0, :cond_1

    .line 494
    const-string p0, ""

    .line 496
    :cond_1
    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 497
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    check-cast v1, Lcom/android/server/content/MiSyncStrategyImpl;

    .line 499
    :cond_2
    if-nez v1, :cond_3

    .line 500
    new-instance v2, Lcom/android/server/content/MiSyncStrategyImpl;

    invoke-direct {v2, p1, p0}, Lcom/android/server/content/MiSyncStrategyImpl;-><init>(ILjava/lang/String;)V

    move-object v1, v2

    .line 501
    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    :cond_3
    return-object v1
.end method

.method private static getRemovingAccounts([Landroid/accounts/Account;Ljava/util/Set;)Ljava/util/List;
    .locals 4
    .param p0, "runningAccounts"    # [Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/accounts/Account;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 351
    .local p1, "currentAccountNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 353
    .local v0, "removing":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_1

    .line 354
    const/4 v1, 0x2

    const-string v2, "SyncManager"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 355
    const-string v1, "getRemovingAccounts: Argument is null"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    :cond_0
    return-object v0

    .line 360
    :cond_1
    if-nez p0, :cond_2

    .line 361
    const/4 v1, 0x0

    new-array p0, v1, [Landroid/accounts/Account;

    .line 364
    :cond_2
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 365
    .local v2, "accountName":Ljava/lang/String;
    invoke-static {p0, v2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->containsXiaomiAccountName([Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 366
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    .end local v2    # "accountName":Ljava/lang/String;
    :cond_3
    goto :goto_0

    .line 369
    :cond_4
    return-object v0
.end method

.method private static readAndWriteLocked()V
    .locals 0

    .line 62
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->readLocked()V

    .line 63
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->writeLocked()V

    .line 64
    return-void
.end method

.method private static readLocked()V
    .locals 0

    .line 67
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->readMiPauseLocked()V

    .line 68
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->readMiStrategyLocked()V

    .line 69
    return-void
.end method

.method private static readMiPauseLocked()V
    .locals 12

    .line 95
    const-string v0, "No initial mi pause"

    const-string v1, "Error reading mi pause"

    const-string v2, "SyncManagerFile"

    const-string v3, "SyncManager"

    const/4 v4, 0x0

    .line 97
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_0
    sget-object v5, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiPauseFile:Landroid/util/AtomicFile;

    invoke-virtual {v5}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v5

    move-object v4, v5

    .line 98
    const/4 v5, 0x2

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 99
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Reading "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiPauseFile:Landroid/util/AtomicFile;

    invoke-virtual {v7}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 102
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-object v6, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v4, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 103
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    .line 104
    .local v6, "eventType":I
    :goto_0
    const/4 v7, 0x1

    if-eq v6, v5, :cond_1

    if-eq v6, v7, :cond_1

    .line 106
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v6, v7

    goto :goto_0

    .line 108
    :cond_1
    if-ne v6, v7, :cond_3

    .line 109
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    if-eqz v4, :cond_2

    .line 143
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 145
    goto :goto_1

    .line 144
    :catch_0
    move-exception v0

    .line 110
    :cond_2
    :goto_1
    return-void

    .line 113
    :cond_3
    :try_start_2
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v8

    .line 114
    .local v8, "tagName":Ljava/lang/String;
    const-string v9, "mi_pause"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 115
    const-string/jumbo v9, "version"

    const/4 v10, 0x0

    invoke-interface {v2, v10, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 118
    .local v9, "versionString":Ljava/lang/String;
    if-nez v9, :cond_4

    const/4 v10, 0x0

    goto :goto_2

    :cond_4
    :try_start_3
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    .local v10, "version":I
    :goto_2
    goto :goto_3

    .line 119
    .end local v10    # "version":I
    :catch_1
    move-exception v10

    .line 120
    .local v10, "e":Ljava/lang/NumberFormatException;
    const/4 v11, 0x0

    move v10, v11

    .line 122
    .local v10, "version":I
    :goto_3
    if-lt v10, v7, :cond_7

    .line 123
    :try_start_4
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v11

    move v6, v11

    .line 125
    :cond_5
    if-ne v6, v5, :cond_6

    .line 126
    invoke-static {v2}, Lcom/android/server/content/MiSyncPauseImpl;->readFromXML(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/content/MiSyncPauseImpl;

    move-result-object v11

    .line 127
    .local v11, "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl;
    invoke-static {v11}, Lcom/android/server/content/SyncStorageEngineStubImpl;->setMiPauseInternalLocked(Lcom/android/server/content/MiSyncPauseImpl;)V

    .line 129
    .end local v11    # "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl;
    :cond_6
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v11
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v6, v11

    .line 130
    if-ne v6, v7, :cond_5

    .line 141
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v6    # "eventType":I
    .end local v8    # "tagName":Ljava/lang/String;
    .end local v9    # "versionString":Ljava/lang/String;
    .end local v10    # "version":I
    :cond_7
    if-eqz v4, :cond_8

    .line 143
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 145
    :goto_4
    goto :goto_5

    .line 144
    :catch_2
    move-exception v0

    goto :goto_4

    .line 148
    :cond_8
    :goto_5
    return-void

    .line 141
    :catchall_0
    move-exception v0

    goto :goto_9

    .line 136
    :catch_3
    move-exception v2

    .line 137
    .local v2, "e":Ljava/io/IOException;
    if-nez v4, :cond_9

    :try_start_6
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 138
    :cond_9
    invoke-static {v3, v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 141
    :goto_6
    if-eqz v4, :cond_a

    .line 143
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 145
    goto :goto_7

    .line 144
    :catch_4
    move-exception v0

    .line 139
    :cond_a
    :goto_7
    return-void

    .line 133
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 134
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_8
    invoke-static {v3, v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 141
    if-eqz v4, :cond_b

    .line 143
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 145
    goto :goto_8

    .line 144
    :catch_6
    move-exception v1

    .line 135
    :cond_b
    :goto_8
    return-void

    .line 141
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_9
    if-eqz v4, :cond_c

    .line 143
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 145
    goto :goto_a

    .line 144
    :catch_7
    move-exception v1

    .line 147
    :cond_c
    :goto_a
    throw v0
.end method

.method private static readMiStrategyLocked()V
    .locals 12

    .line 151
    const-string v0, "No initial mi strategy"

    const-string v1, "Error reading mi strategy"

    const-string v2, "SyncManagerFile"

    const-string v3, "SyncManager"

    const/4 v4, 0x0

    .line 153
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_0
    sget-object v5, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiStrategyFile:Landroid/util/AtomicFile;

    invoke-virtual {v5}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v5

    move-object v4, v5

    .line 154
    const/4 v5, 0x2

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 155
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Reading "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiStrategyFile:Landroid/util/AtomicFile;

    invoke-virtual {v7}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 158
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-object v6, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v4, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 159
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    .line 160
    .local v6, "eventType":I
    :goto_0
    const/4 v7, 0x1

    if-eq v6, v5, :cond_1

    if-eq v6, v7, :cond_1

    .line 162
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    move v6, v7

    goto :goto_0

    .line 164
    :cond_1
    if-ne v6, v7, :cond_3

    .line 165
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    if-eqz v4, :cond_2

    .line 199
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 201
    goto :goto_1

    .line 200
    :catch_0
    move-exception v0

    .line 166
    :cond_2
    :goto_1
    return-void

    .line 169
    :cond_3
    :try_start_2
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v8

    .line 170
    .local v8, "tagName":Ljava/lang/String;
    const-string v9, "mi_strategy"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 171
    const-string/jumbo v9, "version"

    const/4 v10, 0x0

    invoke-interface {v2, v10, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 174
    .local v9, "versionString":Ljava/lang/String;
    if-nez v9, :cond_4

    const/4 v10, 0x0

    goto :goto_2

    :cond_4
    :try_start_3
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    .local v10, "version":I
    :goto_2
    goto :goto_3

    .line 175
    .end local v10    # "version":I
    :catch_1
    move-exception v10

    .line 176
    .local v10, "e":Ljava/lang/NumberFormatException;
    const/4 v11, 0x0

    move v10, v11

    .line 178
    .local v10, "version":I
    :goto_3
    if-lt v10, v7, :cond_7

    .line 179
    :try_start_4
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v11

    move v6, v11

    .line 181
    :cond_5
    if-ne v6, v5, :cond_6

    .line 182
    invoke-static {v2}, Lcom/android/server/content/MiSyncStrategyImpl;->readFromXML(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/content/MiSyncStrategyImpl;

    move-result-object v11

    .line 183
    .local v11, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl;
    invoke-static {v11}, Lcom/android/server/content/SyncStorageEngineStubImpl;->setMiStrategyInternalLocked(Lcom/android/server/content/MiSyncStrategyImpl;)V

    .line 185
    .end local v11    # "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl;
    :cond_6
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v11
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v6, v11

    .line 186
    if-ne v6, v7, :cond_5

    .line 197
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v6    # "eventType":I
    .end local v8    # "tagName":Ljava/lang/String;
    .end local v9    # "versionString":Ljava/lang/String;
    .end local v10    # "version":I
    :cond_7
    if-eqz v4, :cond_8

    .line 199
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 201
    :goto_4
    goto :goto_5

    .line 200
    :catch_2
    move-exception v0

    goto :goto_4

    .line 204
    :cond_8
    :goto_5
    return-void

    .line 197
    :catchall_0
    move-exception v0

    goto :goto_9

    .line 192
    :catch_3
    move-exception v2

    .line 193
    .local v2, "e":Ljava/io/IOException;
    if-nez v4, :cond_9

    :try_start_6
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 194
    :cond_9
    invoke-static {v3, v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 197
    :goto_6
    if-eqz v4, :cond_a

    .line 199
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 201
    goto :goto_7

    .line 200
    :catch_4
    move-exception v0

    .line 195
    :cond_a
    :goto_7
    return-void

    .line 189
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 190
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_8
    invoke-static {v3, v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 197
    if-eqz v4, :cond_b

    .line 199
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 201
    goto :goto_8

    .line 200
    :catch_6
    move-exception v1

    .line 191
    :cond_b
    :goto_8
    return-void

    .line 197
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_9
    if-eqz v4, :cond_c

    .line 199
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 201
    goto :goto_a

    .line 200
    :catch_7
    move-exception v1

    .line 203
    :cond_c
    :goto_a
    throw v0
.end method

.method private static setMiPauseInternalLocked(Lcom/android/server/content/MiSyncPauseImpl;)V
    .locals 4
    .param p0, "miSyncPause"    # Lcom/android/server/content/MiSyncPauseImpl;

    .line 277
    if-nez p0, :cond_1

    .line 278
    const/4 v0, 0x2

    const-string v1, "SyncManager"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    const-string/jumbo v0, "setMiPauseInternalLocked: miSyncPause is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_0
    return-void

    .line 283
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/content/MiSyncPauseImpl;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 284
    invoke-virtual {p0}, Lcom/android/server/content/MiSyncPauseImpl;->getPauseEndTime()J

    move-result-wide v1

    .line 285
    invoke-virtual {p0}, Lcom/android/server/content/MiSyncPauseImpl;->getUid()I

    move-result v3

    .line 283
    invoke-static {v0, v1, v2, v3}, Lcom/android/server/content/SyncStorageEngineStubImpl;->setMiPauseInternalLocked(Ljava/lang/String;JI)V

    .line 286
    return-void
.end method

.method private static setMiPauseInternalLocked(Ljava/lang/String;JI)V
    .locals 2
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "pauseTimeMillis"    # J
    .param p3, "uid"    # I

    .line 289
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    const/4 v0, 0x2

    const-string v1, "SyncManager"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    const-string/jumbo v0, "setMiPauseInternalLocked: accountName is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_0
    return-void

    .line 295
    :cond_1
    invoke-static {p0, p3}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getOrCreateMiSyncPauseLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncPauseImpl;

    move-result-object v0

    .line 296
    .local v0, "item":Lcom/android/server/content/MiSyncPauseImpl;
    invoke-virtual {v0, p1, p2}, Lcom/android/server/content/MiSyncPauseImpl;->setPauseToTime(J)Z

    .line 297
    return-void
.end method

.method private static setMiStrategyInternalLocked(Lcom/android/server/content/MiSyncStrategyImpl;)V
    .locals 3
    .param p0, "miSyncStrategy"    # Lcom/android/server/content/MiSyncStrategyImpl;

    .line 300
    if-nez p0, :cond_1

    .line 301
    const/4 v0, 0x2

    const-string v1, "SyncManager"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    const-string/jumbo v0, "setMiStrategyInternalLocked: miSyncStrategy is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_0
    return-void

    .line 306
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/content/MiSyncStrategyImpl;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 307
    invoke-virtual {p0}, Lcom/android/server/content/MiSyncStrategyImpl;->getStrategy()I

    move-result v1

    .line 308
    invoke-virtual {p0}, Lcom/android/server/content/MiSyncStrategyImpl;->getUid()I

    move-result v2

    .line 306
    invoke-static {v0, v1, v2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->setMiStrategyInternalLocked(Ljava/lang/String;II)V

    .line 309
    return-void
.end method

.method private static setMiStrategyInternalLocked(Ljava/lang/String;II)V
    .locals 2
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "strategy"    # I
    .param p2, "uid"    # I

    .line 312
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    const/4 v0, 0x2

    const-string v1, "SyncManager"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    const-string/jumbo v0, "setMiStrategyInternalLocked: accountName is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_0
    return-void

    .line 318
    :cond_1
    invoke-static {p0, p2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getOrCreateMiSyncStrategyLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncStrategyImpl;

    move-result-object v0

    .line 319
    .local v0, "item":Lcom/android/server/content/MiSyncStrategyImpl;
    invoke-virtual {v0, p1}, Lcom/android/server/content/MiSyncStrategyImpl;->setStrategy(I)Z

    .line 320
    return-void
.end method

.method private static writeLocked()V
    .locals 0

    .line 72
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->writeMiPauseLocked()V

    .line 73
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->writeMiStrategyLocked()V

    .line 74
    return-void
.end method

.method private static writeMiPauseLocked()V
    .locals 10

    .line 207
    const-string v0, "mi_pause"

    const/4 v1, 0x2

    const-string v2, "SyncManagerFile"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Writing new "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiPauseFile:Landroid/util/AtomicFile;

    invoke-virtual {v3}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :cond_0
    const/4 v1, 0x0

    .line 213
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    sget-object v2, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiPauseFile:Landroid/util/AtomicFile;

    invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v2

    move-object v1, v2

    .line 214
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 215
    .local v2, "out":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 216
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 217
    const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v2, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 219
    invoke-interface {v2, v5, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 220
    const-string/jumbo v4, "version"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v5, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 222
    sget-object v3, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncPause:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 223
    .local v3, "m":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_2

    .line 224
    sget-object v6, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncPause:Landroid/util/SparseArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 225
    .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncPauseImpl;>;"
    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    .line 226
    .local v7, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/server/content/MiSyncPauseImpl;>;"
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/content/MiSyncPauseImpl;

    .line 227
    .local v9, "item":Lcom/android/server/content/MiSyncPauseImpl;
    invoke-virtual {v9, v2}, Lcom/android/server/content/MiSyncPauseImpl;->writeToXML(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 228
    .end local v9    # "item":Lcom/android/server/content/MiSyncPauseImpl;
    goto :goto_1

    .line 223
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncPauseImpl;>;"
    .end local v7    # "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/server/content/MiSyncPauseImpl;>;"
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 230
    .end local v4    # "i":I
    :cond_2
    invoke-interface {v2, v5, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 231
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 232
    sget-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiPauseFile:Landroid/util/AtomicFile;

    invoke-virtual {v0, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    .end local v2    # "out":Lorg/xmlpull/v1/XmlSerializer;
    .end local v3    # "m":I
    goto :goto_2

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e1":Ljava/io/IOException;
    const-string v2, "SyncManager"

    const-string v3, "Error writing mi pause"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 235
    if-eqz v1, :cond_3

    .line 236
    sget-object v2, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiPauseFile:Landroid/util/AtomicFile;

    invoke-virtual {v2, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 239
    .end local v0    # "e1":Ljava/io/IOException;
    :cond_3
    :goto_2
    return-void
.end method

.method private static writeMiStrategyLocked()V
    .locals 10

    .line 242
    const-string v0, "mi_strategy"

    const/4 v1, 0x2

    const-string v2, "SyncManagerFile"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Writing new "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiStrategyFile:Landroid/util/AtomicFile;

    invoke-virtual {v3}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :cond_0
    const/4 v1, 0x0

    .line 248
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    sget-object v2, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiStrategyFile:Landroid/util/AtomicFile;

    invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v2

    move-object v1, v2

    .line 249
    new-instance v2, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 250
    .local v2, "out":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 251
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 252
    const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v2, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 254
    invoke-interface {v2, v5, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 255
    const-string/jumbo v4, "version"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v5, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 257
    sget-object v3, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncStrategy:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 258
    .local v3, "m":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v3, :cond_2

    .line 259
    sget-object v6, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiSyncStrategy:Landroid/util/SparseArray;

    invoke-virtual {v6, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 260
    .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncStrategyImpl;>;"
    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    .line 261
    .local v7, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/server/content/MiSyncStrategyImpl;>;"
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/content/MiSyncStrategyImpl;

    .line 262
    .local v9, "item":Lcom/android/server/content/MiSyncStrategyImpl;
    invoke-virtual {v9, v2}, Lcom/android/server/content/MiSyncStrategyImpl;->writeToXML(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 263
    .end local v9    # "item":Lcom/android/server/content/MiSyncStrategyImpl;
    goto :goto_1

    .line 258
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncStrategyImpl;>;"
    .end local v7    # "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/server/content/MiSyncStrategyImpl;>;"
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 265
    .end local v4    # "i":I
    :cond_2
    invoke-interface {v2, v5, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 266
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 267
    sget-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiStrategyFile:Landroid/util/AtomicFile;

    invoke-virtual {v0, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    .end local v2    # "out":Lorg/xmlpull/v1/XmlSerializer;
    .end local v3    # "m":I
    goto :goto_2

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e1":Ljava/io/IOException;
    const-string v2, "SyncManager"

    const-string v3, "Error writing mi strategy"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 270
    if-eqz v1, :cond_3

    .line 271
    sget-object v2, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiStrategyFile:Landroid/util/AtomicFile;

    invoke-virtual {v2, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 274
    .end local v0    # "e1":Ljava/io/IOException;
    :cond_3
    :goto_2
    return-void
.end method


# virtual methods
.method public clearAndReadAndWriteLocked()V
    .locals 0

    .line 83
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->clear()V

    .line 84
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->readAndWriteLocked()V

    .line 85
    return-void
.end method

.method public doDatabaseCleanupLocked([Landroid/accounts/Account;I)V
    .locals 0
    .param p1, "accounts"    # [Landroid/accounts/Account;
    .param p2, "uid"    # I

    .line 89
    invoke-static {p1, p2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->doMiPauseCleanUpLocked([Landroid/accounts/Account;I)V

    .line 90
    invoke-static {p1, p2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->doMiStrategyCleanUpLocked([Landroid/accounts/Account;I)V

    .line 91
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->writeLocked()V

    .line 92
    return-void
.end method

.method public getMiSyncPauseLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncPause;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 458
    invoke-static {p1, p2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getOrCreateMiSyncPauseLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncPauseImpl;

    move-result-object v0

    return-object v0
.end method

.method public getMiSyncPauseToTimeLocked(Landroid/accounts/Account;I)J
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "uid"    # I

    .line 433
    invoke-static {p1}, Lcom/android/server/content/MiSyncUtils;->checkAccount(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 434
    const/4 v0, 0x2

    const-string v1, "SyncManager"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    const-string v0, "getMiSyncPauseToTimeLocked: not xiaomi account"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0

    .line 439
    :cond_1
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getOrCreateMiSyncPauseLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncPauseImpl;

    move-result-object v0

    .line 440
    .local v0, "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl;
    invoke-virtual {v0}, Lcom/android/server/content/MiSyncPauseImpl;->getPauseEndTime()J

    move-result-wide v1

    return-wide v1
.end method

.method public getMiSyncStrategyLocked(Landroid/accounts/Account;I)I
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "uid"    # I

    .line 446
    invoke-static {p1}, Lcom/android/server/content/MiSyncUtils;->checkAccount(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 447
    const/4 v0, 0x2

    const-string v1, "SyncManager"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448
    const-string v0, "getMiSyncStrategyLocked: not xiaomi account"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 452
    :cond_1
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getOrCreateMiSyncStrategyLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncStrategyImpl;

    move-result-object v0

    .line 453
    .local v0, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl;
    invoke-virtual {v0}, Lcom/android/server/content/MiSyncStrategyImpl;->getStrategy()I

    move-result v1

    return v1
.end method

.method public getMiSyncStrategyLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncStrategy;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 463
    invoke-static {p1, p2}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getOrCreateMiSyncStrategyLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncStrategyImpl;

    move-result-object v0

    return-object v0
.end method

.method public initAndReadAndWriteLocked(Ljava/io/File;)V
    .locals 3
    .param p1, "syncDir"    # Ljava/io/File;

    .line 55
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    const-string v2, "mi_pause.xml"

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    sput-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiPauseFile:Landroid/util/AtomicFile;

    .line 56
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    const-string v2, "mi_strategy.xml"

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    sput-object v0, Lcom/android/server/content/SyncStorageEngineStubImpl;->mMiStrategyFile:Landroid/util/AtomicFile;

    .line 58
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->readAndWriteLocked()V

    .line 59
    return-void
.end method

.method public setMiSyncPauseToTimeLocked(Landroid/accounts/Account;JI)V
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "pauseTimeMillis"    # J
    .param p4, "uid"    # I

    .line 389
    invoke-static {p1}, Lcom/android/server/content/MiSyncUtils;->checkAccount(Landroid/accounts/Account;)Z

    move-result v0

    const/4 v1, 0x2

    const-string v2, "SyncManager"

    if-nez v0, :cond_1

    .line 390
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    const-string/jumbo v0, "setMiSyncPauseToTimeLocked: account is null"

    invoke-static {v2, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_0
    return-void

    .line 396
    :cond_1
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p4}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getOrCreateMiSyncPauseLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncPauseImpl;

    move-result-object v0

    .line 397
    .local v0, "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl;
    invoke-virtual {v0}, Lcom/android/server/content/MiSyncPauseImpl;->getPauseEndTime()J

    move-result-wide v3

    cmp-long v3, v3, p2

    if-nez v3, :cond_3

    .line 398
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 399
    const-string/jumbo v1, "setMiSyncPauseTimeLocked: pause time is not changed"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_2
    return-void

    .line 403
    :cond_3
    invoke-virtual {v0, p2, p3}, Lcom/android/server/content/MiSyncPauseImpl;->setPauseToTime(J)Z

    .line 405
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->writeMiPauseLocked()V

    .line 406
    return-void
.end method

.method public setMiSyncStrategyLocked(Landroid/accounts/Account;II)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "strategy"    # I
    .param p3, "uid"    # I

    .line 411
    invoke-static {p1}, Lcom/android/server/content/MiSyncUtils;->checkAccount(Landroid/accounts/Account;)Z

    move-result v0

    const/4 v1, 0x2

    const-string v2, "SyncManager"

    if-nez v0, :cond_1

    .line 412
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    const-string/jumbo v0, "setMiSyncStrategyLocked: account is null"

    invoke-static {v2, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_0
    return-void

    .line 418
    :cond_1
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p3}, Lcom/android/server/content/SyncStorageEngineStubImpl;->getOrCreateMiSyncStrategyLocked(Ljava/lang/String;I)Lcom/android/server/content/MiSyncStrategyImpl;

    move-result-object v0

    .line 419
    .local v0, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl;
    invoke-virtual {v0}, Lcom/android/server/content/MiSyncStrategyImpl;->getStrategy()I

    move-result v3

    if-ne v3, p2, :cond_3

    .line 420
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 421
    const-string/jumbo v1, "setMiSyncPauseTimeLocked: strategy is not changed"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :cond_2
    return-void

    .line 425
    :cond_3
    invoke-virtual {v0, p2}, Lcom/android/server/content/MiSyncStrategyImpl;->setStrategy(I)Z

    .line 427
    invoke-static {}, Lcom/android/server/content/SyncStorageEngineStubImpl;->writeMiStrategyLocked()V

    .line 428
    return-void
.end method

.method public updateResultStatusLocked(Landroid/content/SyncStatusInfo;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 0
    .param p1, "syncStatusInfo"    # Landroid/content/SyncStatusInfo;
    .param p2, "lastSyncMessage"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;

    .line 509
    invoke-static {p1, p2, p3}, Lcom/android/server/content/MiSyncResultStatusAdapter;->updateResultStatus(Landroid/content/SyncStatusInfo;Ljava/lang/String;Landroid/content/SyncResult;)V

    .line 511
    return-void
.end method
