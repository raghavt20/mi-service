.class public Lcom/android/server/content/MiSyncPauseImpl;
.super Ljava/lang/Object;
.source "MiSyncPauseImpl.java"

# interfaces
.implements Lcom/android/server/content/MiSyncPause;


# static fields
.field private static final PAUSE_TIME_MAX_INTERVAL:J = 0x5265c00L

.field private static final PAUSE_TIME_START_DELTA:J = 0xea60L

.field private static final TAG:Ljava/lang/String; = "Sync"

.field private static final VERSION:I = 0x1

.field private static final XML_ATTR_ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field private static final XML_ATTR_PAUSE_END_TIME:Ljava/lang/String; = "end_time"

.field private static final XML_ATTR_UID:Ljava/lang/String; = "uid"

.field private static final XML_ATTR_VERSION:Ljava/lang/String; = "version"

.field public static final XML_FILE_NAME:Ljava/lang/String; = "mi_pause"

.field public static final XML_FILE_VERSION:I = 0x1

.field private static final XML_TAG_ITEM:Ljava/lang/String; = "sync_pause_item"


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mPauseEndTimeMills:J

.field private mPauseStartTimeMills:J

.field private mUid:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "accountName"    # Ljava/lang/String;

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseStartTimeMills:J

    .line 44
    iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J

    .line 47
    iput p1, p0, Lcom/android/server/content/MiSyncPauseImpl;->mUid:I

    .line 48
    iput-object p2, p0, Lcom/android/server/content/MiSyncPauseImpl;->mAccountName:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static readFromXML(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/content/MiSyncPauseImpl;
    .locals 13
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;

    .line 115
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "tagName":Ljava/lang/String;
    const-string/jumbo v1, "sync_pause_item"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 117
    return-object v2

    .line 119
    :cond_0
    const-string/jumbo v1, "version"

    invoke-interface {p0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "itemVersionString":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v4, "Sync"

    if-eqz v3, :cond_1

    .line 121
    const-string/jumbo v3, "the version in mi pause is null"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    return-object v2

    .line 124
    :cond_1
    const/4 v3, 0x0

    .line 126
    .local v3, "itemVersion":I
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move v3, v5

    .line 130
    nop

    .line 131
    const/4 v5, 0x1

    if-lt v3, v5, :cond_4

    .line 132
    const-string/jumbo v5, "uid"

    invoke-interface {p0, v2, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 133
    .local v5, "uidString":Ljava/lang/String;
    const-string v6, "account_name"

    invoke-interface {p0, v2, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 134
    .local v6, "accountName":Ljava/lang/String;
    const-string v7, "end_time"

    invoke-interface {p0, v2, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 136
    .local v7, "pauseEndTimeMillsString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 137
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 138
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_0

    .line 143
    :cond_2
    const/4 v8, 0x0

    .line 144
    .local v8, "uid":I
    const-wide/16 v9, 0x0

    .line 146
    .local v9, "pauseEndTimeMills":J
    :try_start_1
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    move v8, v11

    .line 147
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-wide v9, v11

    .line 151
    nop

    .line 152
    new-instance v2, Lcom/android/server/content/MiSyncPauseImpl;

    invoke-direct {v2, v8, v6}, Lcom/android/server/content/MiSyncPauseImpl;-><init>(ILjava/lang/String;)V

    .line 153
    .local v2, "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl;
    invoke-virtual {v2, v9, v10}, Lcom/android/server/content/MiSyncPauseImpl;->setPauseToTime(J)Z

    .line 154
    return-object v2

    .line 148
    .end local v2    # "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl;
    :catch_0
    move-exception v11

    .line 149
    .local v11, "e":Ljava/lang/NumberFormatException;
    const-string v12, "error parsing item for mi pause"

    invoke-static {v4, v12, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 150
    return-object v2

    .line 139
    .end local v8    # "uid":I
    .end local v9    # "pauseEndTimeMills":J
    .end local v11    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    :goto_0
    const-string/jumbo v8, "the item in mi pause is null"

    invoke-static {v4, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return-object v2

    .line 156
    .end local v5    # "uidString":Ljava/lang/String;
    .end local v6    # "accountName":Ljava/lang/String;
    .end local v7    # "pauseEndTimeMillsString":Ljava/lang/String;
    :cond_4
    return-object v2

    .line 127
    :catch_1
    move-exception v5

    .line 128
    .local v5, "e":Ljava/lang/NumberFormatException;
    const-string v6, "error parsing version for mi pause"

    invoke-static {v4, v6, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 129
    return-object v2
.end method


# virtual methods
.method public getAccountName()Ljava/lang/String;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getPauseEndTime()J
    .locals 2

    .line 63
    iget-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J

    return-wide v0
.end method

.method public getResumeTimeLeft()J
    .locals 7

    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 69
    .local v0, "currentTimeMills":J
    iget-wide v2, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseStartTimeMills:J

    const-wide/32 v4, 0xea60

    sub-long/2addr v2, v4

    cmp-long v2, v0, v2

    const-wide/16 v3, 0x0

    if-gtz v2, :cond_0

    .line 70
    return-wide v3

    .line 72
    :cond_0
    iget-wide v5, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J

    cmp-long v2, v5, v0

    if-gtz v2, :cond_1

    .line 73
    return-wide v3

    .line 75
    :cond_1
    sub-long/2addr v5, v0

    return-wide v5
.end method

.method public getUid()I
    .locals 1

    .line 53
    iget v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mUid:I

    return v0
.end method

.method public setPauseToTime(J)Z
    .locals 10
    .param p1, "pauseTimeMillis"    # J

    .line 82
    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    const/4 v3, 0x1

    const/4 v4, 0x3

    const-string v5, "Sync"

    if-nez v2, :cond_1

    .line 83
    invoke-static {v5, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    const-string v2, "Resume syncs"

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_0
    iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseStartTimeMills:J

    .line 87
    iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J

    .line 88
    return v3

    .line 91
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 92
    .local v0, "currentTimeMillis":J
    cmp-long v2, p1, v0

    if-ltz v2, :cond_3

    sub-long v6, p1, v0

    const-wide/32 v8, 0x5265c00

    cmp-long v2, v6, v8

    if-lez v2, :cond_2

    goto :goto_0

    .line 99
    :cond_2
    iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseStartTimeMills:J

    .line 100
    iput-wide p1, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J

    .line 101
    return v3

    .line 94
    :cond_3
    :goto_0
    invoke-static {v5, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 95
    const-string v2, "Illegal time"

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_4
    const/4 v2, 0x0

    return v2
.end method

.method public writeToXML(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 4
    .param p1, "out"    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 106
    const/4 v0, 0x0

    const-string/jumbo v1, "sync_pause_item"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 107
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "version"

    invoke-interface {p1, v0, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 108
    iget v2, p0, Lcom/android/server/content/MiSyncPauseImpl;->mUid:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "uid"

    invoke-interface {p1, v0, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 109
    const-string v2, "account_name"

    iget-object v3, p0, Lcom/android/server/content/MiSyncPauseImpl;->mAccountName:Ljava/lang/String;

    invoke-interface {p1, v0, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 110
    iget-wide v2, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "end_time"

    invoke-interface {p1, v0, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 111
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 112
    return-void
.end method
