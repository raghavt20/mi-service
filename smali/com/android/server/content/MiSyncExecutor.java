class com.android.server.content.MiSyncExecutor {
	 /* .source "MiSyncExecutor.java" */
	 /* # direct methods */
	 private com.android.server.content.MiSyncExecutor ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void sync ( com.android.server.content.SyncManager p0, Integer p1, android.accounts.Account p2 ) {
		 /* .locals 2 */
		 /* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .param p1, "sendingUserId" # I */
		 /* .param p2, "account" # Landroid/accounts/Account; */
		 /* .line 11 */
		 /* new-instance v0, Ljava/lang/UnsupportedOperationException; */
		 final String v1 = "Android sdk >= Q, unsupport call MiSyncExecutor sync method"; // const-string v1, "Android sdk >= Q, unsupport call MiSyncExecutor sync method"
		 /* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
	 } // .end method
