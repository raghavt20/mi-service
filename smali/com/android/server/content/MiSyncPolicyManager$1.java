class com.android.server.content.MiSyncPolicyManager$1 extends android.database.ContentObserver {
	 /* .source "MiSyncPolicyManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/content/MiSyncPolicyManager;->registerSyncSettingsObserver(Landroid/content/Context;Lcom/android/server/content/SyncManager;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.content.SyncManager val$syncManager; //synthetic
/* # direct methods */
 com.android.server.content.MiSyncPolicyManager$1 ( ) {
/* .locals 0 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .line 25 */
this.val$syncManager = p2;
/* invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .line 28 */
v0 = this.val$syncManager;
com.android.server.content.MiSyncPolicyManager .handleMasterWifiOnlyChanged ( v0 );
/* .line 29 */
return;
} // .end method
