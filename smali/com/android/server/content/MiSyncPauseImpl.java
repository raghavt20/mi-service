public class com.android.server.content.MiSyncPauseImpl implements com.android.server.content.MiSyncPause {
	 /* .source "MiSyncPauseImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Long PAUSE_TIME_MAX_INTERVAL;
	 private static final Long PAUSE_TIME_START_DELTA;
	 private static final java.lang.String TAG;
	 private static final Integer VERSION;
	 private static final java.lang.String XML_ATTR_ACCOUNT_NAME;
	 private static final java.lang.String XML_ATTR_PAUSE_END_TIME;
	 private static final java.lang.String XML_ATTR_UID;
	 private static final java.lang.String XML_ATTR_VERSION;
	 public static final java.lang.String XML_FILE_NAME;
	 public static final Integer XML_FILE_VERSION;
	 private static final java.lang.String XML_TAG_ITEM;
	 /* # instance fields */
	 private java.lang.String mAccountName;
	 private Long mPauseEndTimeMills;
	 private Long mPauseStartTimeMills;
	 private Integer mUid;
	 /* # direct methods */
	 public com.android.server.content.MiSyncPauseImpl ( ) {
		 /* .locals 2 */
		 /* .param p1, "uid" # I */
		 /* .param p2, "accountName" # Ljava/lang/String; */
		 /* .line 46 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 43 */
		 /* const-wide/16 v0, 0x0 */
		 /* iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseStartTimeMills:J */
		 /* .line 44 */
		 /* iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J */
		 /* .line 47 */
		 /* iput p1, p0, Lcom/android/server/content/MiSyncPauseImpl;->mUid:I */
		 /* .line 48 */
		 this.mAccountName = p2;
		 /* .line 49 */
		 return;
	 } // .end method
	 public static com.android.server.content.MiSyncPauseImpl readFromXML ( org.xmlpull.v1.XmlPullParser p0 ) {
		 /* .locals 13 */
		 /* .param p0, "parser" # Lorg/xmlpull/v1/XmlPullParser; */
		 /* .line 115 */
		 /* .line 116 */
		 /* .local v0, "tagName":Ljava/lang/String; */
		 /* const-string/jumbo v1, "sync_pause_item" */
		 v1 = 		 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 int v2 = 0; // const/4 v2, 0x0
		 /* if-nez v1, :cond_0 */
		 /* .line 117 */
		 /* .line 119 */
	 } // :cond_0
	 /* const-string/jumbo v1, "version" */
	 /* .line 120 */
	 /* .local v1, "itemVersionString":Ljava/lang/String; */
	 v3 = 	 android.text.TextUtils .isEmpty ( v1 );
	 final String v4 = "Sync"; // const-string v4, "Sync"
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 121 */
		 /* const-string/jumbo v3, "the version in mi pause is null" */
		 android.util.Slog .e ( v4,v3 );
		 /* .line 122 */
		 /* .line 124 */
	 } // :cond_1
	 int v3 = 0; // const/4 v3, 0x0
	 /* .line 126 */
	 /* .local v3, "itemVersion":I */
	 try { // :try_start_0
		 v5 = 		 java.lang.Integer .parseInt ( v1 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_1 */
		 /* move v3, v5 */
		 /* .line 130 */
		 /* nop */
		 /* .line 131 */
		 int v5 = 1; // const/4 v5, 0x1
		 /* if-lt v3, v5, :cond_4 */
		 /* .line 132 */
		 /* const-string/jumbo v5, "uid" */
		 /* .line 133 */
		 /* .local v5, "uidString":Ljava/lang/String; */
		 final String v6 = "account_name"; // const-string v6, "account_name"
		 /* .line 134 */
		 /* .local v6, "accountName":Ljava/lang/String; */
		 final String v7 = "end_time"; // const-string v7, "end_time"
		 /* .line 136 */
		 /* .local v7, "pauseEndTimeMillsString":Ljava/lang/String; */
		 v8 = 		 android.text.TextUtils .isEmpty ( v5 );
		 /* if-nez v8, :cond_3 */
		 /* .line 137 */
		 v8 = 		 android.text.TextUtils .isEmpty ( v6 );
		 /* if-nez v8, :cond_3 */
		 /* .line 138 */
		 v8 = 		 android.text.TextUtils .isEmpty ( v7 );
		 if ( v8 != null) { // if-eqz v8, :cond_2
			 /* .line 143 */
		 } // :cond_2
		 int v8 = 0; // const/4 v8, 0x0
		 /* .line 144 */
		 /* .local v8, "uid":I */
		 /* const-wide/16 v9, 0x0 */
		 /* .line 146 */
		 /* .local v9, "pauseEndTimeMills":J */
		 try { // :try_start_1
			 v11 = 			 java.lang.Integer .parseInt ( v5 );
			 /* move v8, v11 */
			 /* .line 147 */
			 java.lang.Long .parseLong ( v7 );
			 /* move-result-wide v11 */
			 /* :try_end_1 */
			 /* .catch Ljava/lang/NumberFormatException; {:try_start_1 ..:try_end_1} :catch_0 */
			 /* move-wide v9, v11 */
			 /* .line 151 */
			 /* nop */
			 /* .line 152 */
			 /* new-instance v2, Lcom/android/server/content/MiSyncPauseImpl; */
			 /* invoke-direct {v2, v8, v6}, Lcom/android/server/content/MiSyncPauseImpl;-><init>(ILjava/lang/String;)V */
			 /* .line 153 */
			 /* .local v2, "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl; */
			 (( com.android.server.content.MiSyncPauseImpl ) v2 ).setPauseToTime ( v9, v10 ); // invoke-virtual {v2, v9, v10}, Lcom/android/server/content/MiSyncPauseImpl;->setPauseToTime(J)Z
			 /* .line 154 */
			 /* .line 148 */
		 } // .end local v2 # "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl;
		 /* :catch_0 */
		 /* move-exception v11 */
		 /* .line 149 */
		 /* .local v11, "e":Ljava/lang/NumberFormatException; */
		 final String v12 = "error parsing item for mi pause"; // const-string v12, "error parsing item for mi pause"
		 android.util.Slog .e ( v4,v12,v11 );
		 /* .line 150 */
		 /* .line 139 */
	 } // .end local v8 # "uid":I
} // .end local v9 # "pauseEndTimeMills":J
} // .end local v11 # "e":Ljava/lang/NumberFormatException;
} // :cond_3
} // :goto_0
/* const-string/jumbo v8, "the item in mi pause is null" */
android.util.Slog .e ( v4,v8 );
/* .line 140 */
/* .line 156 */
} // .end local v5 # "uidString":Ljava/lang/String;
} // .end local v6 # "accountName":Ljava/lang/String;
} // .end local v7 # "pauseEndTimeMillsString":Ljava/lang/String;
} // :cond_4
/* .line 127 */
/* :catch_1 */
/* move-exception v5 */
/* .line 128 */
/* .local v5, "e":Ljava/lang/NumberFormatException; */
final String v6 = "error parsing version for mi pause"; // const-string v6, "error parsing version for mi pause"
android.util.Slog .e ( v4,v6,v5 );
/* .line 129 */
} // .end method
/* # virtual methods */
public java.lang.String getAccountName ( ) {
/* .locals 1 */
/* .line 58 */
v0 = this.mAccountName;
} // .end method
public Long getPauseEndTime ( ) {
/* .locals 2 */
/* .line 63 */
/* iget-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J */
/* return-wide v0 */
} // .end method
public Long getResumeTimeLeft ( ) {
/* .locals 7 */
/* .line 68 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 69 */
/* .local v0, "currentTimeMills":J */
/* iget-wide v2, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseStartTimeMills:J */
/* const-wide/32 v4, 0xea60 */
/* sub-long/2addr v2, v4 */
/* cmp-long v2, v0, v2 */
/* const-wide/16 v3, 0x0 */
/* if-gtz v2, :cond_0 */
/* .line 70 */
/* return-wide v3 */
/* .line 72 */
} // :cond_0
/* iget-wide v5, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J */
/* cmp-long v2, v5, v0 */
/* if-gtz v2, :cond_1 */
/* .line 73 */
/* return-wide v3 */
/* .line 75 */
} // :cond_1
/* sub-long/2addr v5, v0 */
/* return-wide v5 */
} // .end method
public Integer getUid ( ) {
/* .locals 1 */
/* .line 53 */
/* iget v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mUid:I */
} // .end method
public Boolean setPauseToTime ( Long p0 ) {
/* .locals 10 */
/* .param p1, "pauseTimeMillis" # J */
/* .line 82 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v2, p1, v0 */
int v3 = 1; // const/4 v3, 0x1
int v4 = 3; // const/4 v4, 0x3
final String v5 = "Sync"; // const-string v5, "Sync"
/* if-nez v2, :cond_1 */
/* .line 83 */
v2 = android.util.Log .isLoggable ( v5,v4 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 84 */
final String v2 = "Resume syncs"; // const-string v2, "Resume syncs"
android.util.Log .d ( v5,v2 );
/* .line 86 */
} // :cond_0
/* iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseStartTimeMills:J */
/* .line 87 */
/* iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J */
/* .line 88 */
/* .line 91 */
} // :cond_1
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 92 */
/* .local v0, "currentTimeMillis":J */
/* cmp-long v2, p1, v0 */
/* if-ltz v2, :cond_3 */
/* sub-long v6, p1, v0 */
/* const-wide/32 v8, 0x5265c00 */
/* cmp-long v2, v6, v8 */
/* if-lez v2, :cond_2 */
/* .line 99 */
} // :cond_2
/* iput-wide v0, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseStartTimeMills:J */
/* .line 100 */
/* iput-wide p1, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J */
/* .line 101 */
/* .line 94 */
} // :cond_3
} // :goto_0
v2 = android.util.Log .isLoggable ( v5,v4 );
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 95 */
final String v2 = "Illegal time"; // const-string v2, "Illegal time"
android.util.Log .d ( v5,v2 );
/* .line 97 */
} // :cond_4
int v2 = 0; // const/4 v2, 0x0
} // .end method
public void writeToXML ( org.xmlpull.v1.XmlSerializer p0 ) {
/* .locals 4 */
/* .param p1, "out" # Lorg/xmlpull/v1/XmlSerializer; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 106 */
int v0 = 0; // const/4 v0, 0x0
/* const-string/jumbo v1, "sync_pause_item" */
/* .line 107 */
int v2 = 1; // const/4 v2, 0x1
java.lang.Integer .toString ( v2 );
/* const-string/jumbo v3, "version" */
/* .line 108 */
/* iget v2, p0, Lcom/android/server/content/MiSyncPauseImpl;->mUid:I */
java.lang.Integer .toString ( v2 );
/* const-string/jumbo v3, "uid" */
/* .line 109 */
final String v2 = "account_name"; // const-string v2, "account_name"
v3 = this.mAccountName;
/* .line 110 */
/* iget-wide v2, p0, Lcom/android/server/content/MiSyncPauseImpl;->mPauseEndTimeMills:J */
java.lang.Long .toString ( v2,v3 );
final String v3 = "end_time"; // const-string v3, "end_time"
/* .line 111 */
/* .line 112 */
return;
} // .end method
