.class public Lcom/android/server/content/MiSyncUtils;
.super Ljava/lang/Object;
.source "MiSyncUtils.java"


# static fields
.field private static final HIGH_PARALLEL_SYNC_NUM:I = 0x7fffffff

.field private static final LOW_PARALLEL_SYNC_DEVICES:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOW_PARALLEL_SYNC_NUM:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MiSyncUtils"

.field private static final XIAOMI_MAX_PARALLEL_SYNC_NUM:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 21
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/content/MiSyncUtils;->LOW_PARALLEL_SYNC_DEVICES:Ljava/util/HashSet;

    .line 24
    const-string v1, "onc"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 25
    const-string v1, "pine"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 26
    const-string/jumbo v1, "ugg"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 27
    const-string v1, "cactus"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 28
    const-string v1, "cereus"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 29
    const-string v1, "santoni"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 30
    const-string v1, "riva"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 31
    const-string v1, "rosy"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 32
    const-string v1, "rolex"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 34
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 35
    .local v1, "device":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x1

    sput v0, Lcom/android/server/content/MiSyncUtils;->XIAOMI_MAX_PARALLEL_SYNC_NUM:I

    goto :goto_0

    .line 38
    :cond_0
    const v0, 0x7fffffff

    sput v0, Lcom/android/server/content/MiSyncUtils;->XIAOMI_MAX_PARALLEL_SYNC_NUM:I

    .line 40
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Max parallel sync number is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/android/server/content/MiSyncUtils;->XIAOMI_MAX_PARALLEL_SYNC_NUM:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "MiSyncUtils"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    .end local v1    # "device":Ljava/lang/String;
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkAccount(Landroid/accounts/Account;)Z
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;

    .line 135
    const/4 v0, 0x3

    const-string v1, "MiSyncUtils"

    if-nez p0, :cond_1

    .line 136
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "injector: checkAccount: false"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 141
    :cond_1
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "injector: checkAccount: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_2
    const-string v0, "com.xiaomi"

    iget-object v1, p0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static checkSyncOperationAccount(Lcom/android/server/content/SyncOperation;)Z
    .locals 4
    .param p0, "syncOperation"    # Lcom/android/server/content/SyncOperation;

    .line 84
    const/4 v0, 0x3

    const-string v1, "MiSyncUtils"

    if-eqz p0, :cond_2

    iget-object v2, p0, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    iget-object v2, v2, Lcom/android/server/content/SyncStorageEngine$EndPoint;->account:Landroid/accounts/Account;

    if-nez v2, :cond_0

    goto :goto_0

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    iget-object v2, v2, Lcom/android/server/content/SyncStorageEngine$EndPoint;->account:Landroid/accounts/Account;

    .line 93
    .local v2, "account":Landroid/accounts/Account;
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "injector: checkSyncOperationAccount: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_1
    const-string v0, "com.xiaomi"

    iget-object v1, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 86
    .end local v2    # "account":Landroid/accounts/Account;
    :cond_2
    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 87
    const-string v0, "injector: checkSyncOperationAccount: false"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method static checkSyncOperationPass(Lcom/android/server/content/SyncOperation;)Z
    .locals 6
    .param p0, "syncOperation"    # Lcom/android/server/content/SyncOperation;

    .line 101
    const/4 v0, 0x0

    const/4 v1, 0x3

    const-string v2, "MiSyncUtils"

    if-nez p0, :cond_1

    .line 102
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    const-string v1, "injector: checkSyncOperationPass: null parameter, fail"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    return v0

    .line 109
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/content/SyncOperation;->isInitialization()Z

    move-result v3

    const/4 v4, 0x1

    if-nez v3, :cond_6

    .line 110
    invoke-virtual {p0}, Lcom/android/server/content/SyncOperation;->isManual()Z

    move-result v3

    if-nez v3, :cond_6

    .line 111
    invoke-virtual {p0}, Lcom/android/server/content/SyncOperation;->isIgnoreSettings()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 119
    :cond_2
    iget v3, p0, Lcom/android/server/content/SyncOperation;->reason:I

    const/4 v5, -0x6

    if-ne v3, v5, :cond_4

    .line 120
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    const-string v0, "injector: checkSyncOperationPass: sync for auto, pass"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_3
    return v4

    .line 127
    :cond_4
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 128
    const-string v1, "injector: checkSyncOperationPass: fail"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_5
    return v0

    .line 112
    :cond_6
    :goto_0
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 113
    const-string v0, "injector: checkSyncOperationPass: init or ignore settings, pass"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_7
    return v4
.end method

.method static isSyncRoomForbiddenH(Lcom/android/server/content/SyncOperation;Lcom/android/server/content/SyncManager;)Z
    .locals 6
    .param p0, "op"    # Lcom/android/server/content/SyncOperation;
    .param p1, "syncManager"    # Lcom/android/server/content/SyncManager;

    .line 45
    const/4 v0, 0x3

    const/4 v1, 0x0

    const-string v2, "MiSyncUtils"

    if-eqz p0, :cond_9

    if-nez p1, :cond_0

    goto :goto_1

    .line 53
    :cond_0
    invoke-static {p0}, Lcom/android/server/content/MiSyncUtils;->checkSyncOperationAccount(Lcom/android/server/content/SyncOperation;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 54
    invoke-static {v2, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const-string v0, "injector: isSyncRoomAvailable: not xiaomi account, false"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_1
    return v1

    .line 61
    :cond_2
    invoke-static {p0}, Lcom/android/server/content/MiSyncUtils;->checkSyncOperationPass(Lcom/android/server/content/SyncOperation;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 62
    invoke-static {v2, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63
    const-string v0, "injector: isSyncRoomAvailable: sync operation pass, false"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_3
    return v1

    .line 68
    :cond_4
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "device":Ljava/lang/String;
    const-string v2, "dipper"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 70
    return v1

    .line 73
    :cond_5
    const/4 v2, 0x0

    .line 74
    .local v2, "count":I
    iget-object v3, p1, Lcom/android/server/content/SyncManager;->mActiveSyncContexts:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/content/SyncManager$ActiveSyncContext;

    .line 75
    .local v4, "activeSyncContext":Lcom/android/server/content/SyncManager$ActiveSyncContext;
    iget-object v5, v4, Lcom/android/server/content/SyncManager$ActiveSyncContext;->mSyncOperation:Lcom/android/server/content/SyncOperation;

    invoke-static {v5}, Lcom/android/server/content/MiSyncUtils;->checkSyncOperationAccount(Lcom/android/server/content/SyncOperation;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 76
    add-int/lit8 v2, v2, 0x1

    .line 78
    .end local v4    # "activeSyncContext":Lcom/android/server/content/SyncManager$ActiveSyncContext;
    :cond_6
    goto :goto_0

    .line 79
    :cond_7
    sget v3, Lcom/android/server/content/MiSyncUtils;->XIAOMI_MAX_PARALLEL_SYNC_NUM:I

    if-lt v2, v3, :cond_8

    const/4 v1, 0x1

    :cond_8
    return v1

    .line 46
    .end local v0    # "device":Ljava/lang/String;
    .end local v2    # "count":I
    :cond_9
    :goto_1
    invoke-static {v2, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 47
    const-string v0, "injector: isSyncRoomAvailable: null parameter, false"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    :cond_a
    return v1
.end method
