class com.android.server.content.MiSyncStrategyImpl$CleverMJStrategy implements com.android.server.content.MiSyncStrategyImpl$ISyncStrategy {
	 /* .source "MiSyncStrategyImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/content/MiSyncStrategyImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "CleverMJStrategy" */
} // .end annotation
/* # static fields */
private static final Integer ALLOW_FIRST_SYNC_THRESHOLD;
private static final Integer ALLOW_FIRST_SYNC_THRESHOLD_FOR_BROWSER;
private static final java.lang.String AUTHORITY_BROWSER;
private static final java.lang.String AUTHORITY_CALENDAR;
private static final java.lang.String AUTHORITY_CONTACTS;
private static final java.lang.String AUTHORITY_GALLERY;
private static final java.lang.String AUTHORITY_NOTES;
private static final java.util.Set REAL_TIME_STRATEGY_AUTHORITY_SET;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.content.MiSyncStrategyImpl$CleverMJStrategy ( ) {
/* .locals 2 */
/* .line 195 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 198 */
final String v1 = "com.android.calendar"; // const-string v1, "com.android.calendar"
/* .line 199 */
final String v1 = "notes"; // const-string v1, "notes"
/* .line 200 */
final String v1 = "com.android.contacts"; // const-string v1, "com.android.contacts"
/* .line 201 */
final String v1 = "com.miui.gallery.cloud.provider"; // const-string v1, "com.miui.gallery.cloud.provider"
/* .line 202 */
return;
} // .end method
private com.android.server.content.MiSyncStrategyImpl$CleverMJStrategy ( ) {
/* .locals 0 */
/* .line 188 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.content.MiSyncStrategyImpl$CleverMJStrategy ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;-><init>()V */
return;
} // .end method
private Boolean isFirstTimes ( java.lang.String p0, android.os.Bundle p1 ) {
/* .locals 4 */
/* .param p1, "authority" # Ljava/lang/String; */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .line 298 */
final String v0 = "key_num_syncs"; // const-string v0, "key_num_syncs"
int v1 = 0; // const/4 v1, 0x0
v0 = (( android.os.Bundle ) p2 ).getInt ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 300 */
/* .local v0, "num":I */
final String v2 = "com.miui.browser"; // const-string v2, "com.miui.browser"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 301 */
/* if-ltz v0, :cond_1 */
/* const/16 v2, 0x8 */
/* if-ge v0, v2, :cond_1 */
/* .line 302 */
/* .line 305 */
} // :cond_0
/* if-ltz v0, :cond_1 */
int v2 = 3; // const/4 v2, 0x3
/* if-ge v0, v2, :cond_1 */
/* .line 306 */
/* .line 309 */
} // :cond_1
} // .end method
/* # virtual methods */
public void apply ( com.android.server.content.SyncOperation p0, android.os.Bundle p1, android.app.job.JobInfo$Builder p2 ) {
/* .locals 5 */
/* .param p1, "syncOperation" # Lcom/android/server/content/SyncOperation; */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .param p3, "builder" # Landroid/app/job/JobInfo$Builder; */
/* .line 210 */
final String v0 = "injector: apply: null parameter, return"; // const-string v0, "injector: apply: null parameter, return"
int v1 = 3; // const/4 v1, 0x3
final String v2 = "Sync"; // const-string v2, "Sync"
if ( p1 != null) { // if-eqz p1, :cond_7
v3 = this.target;
/* if-nez v3, :cond_0 */
/* .line 218 */
} // :cond_0
v3 = this.target;
v3 = this.provider;
/* .line 219 */
/* .local v3, "authority":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 220 */
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 221 */
android.util.Log .d ( v2,v0 );
/* .line 223 */
} // :cond_1
return;
/* .line 227 */
} // :cond_2
v0 = v0 = com.android.server.content.MiSyncStrategyImpl$CleverMJStrategy.REAL_TIME_STRATEGY_AUTHORITY_SET;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 228 */
v0 = android.util.Log .isLoggable ( v2,v1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 229 */
final String v0 = "injector: apply: authority is not affected by strategy, return"; // const-string v0, "injector: apply: authority is not affected by strategy, return"
android.util.Log .d ( v2,v0 );
/* .line 231 */
} // :cond_3
return;
/* .line 235 */
} // :cond_4
v0 = /* invoke-direct {p0, v3, p2}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;->isFirstTimes(Ljava/lang/String;Landroid/os/Bundle;)Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 236 */
v0 = android.util.Log .isLoggable ( v2,v1 );
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 237 */
final String v0 = "injector: apply: first full sync, return"; // const-string v0, "injector: apply: first full sync, return"
android.util.Log .d ( v2,v0 );
/* .line 239 */
} // :cond_5
return;
/* .line 241 */
} // :cond_6
int v0 = 1; // const/4 v0, 0x1
(( android.app.job.JobInfo$Builder ) p3 ).setRequiresCharging ( v0 ); // invoke-virtual {p3, v0}, Landroid/app/job/JobInfo$Builder;->setRequiresCharging(Z)Landroid/app/job/JobInfo$Builder;
/* .line 242 */
return;
/* .line 211 */
} // .end local v3 # "authority":Ljava/lang/String;
} // :cond_7
} // :goto_0
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 212 */
android.util.Log .d ( v2,v0 );
/* .line 214 */
} // :cond_8
return;
} // .end method
public Boolean isAllowedToRun ( com.android.server.content.SyncOperation p0, android.os.Bundle p1 ) {
/* .locals 17 */
/* .param p1, "syncOperation" # Lcom/android/server/content/SyncOperation; */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .line 247 */
/* move-object/from16 v0, p1 */
/* move-object/from16 v1, p2 */
final String v2 = "injector: isAllowedToRun: null parameter, return true"; // const-string v2, "injector: isAllowedToRun: null parameter, return true"
int v3 = 1; // const/4 v3, 0x1
int v4 = 3; // const/4 v4, 0x3
final String v5 = "Sync"; // const-string v5, "Sync"
if ( v0 != null) { // if-eqz v0, :cond_9
v6 = this.target;
/* if-nez v6, :cond_0 */
/* move-object/from16 v7, p0 */
/* .line 255 */
} // :cond_0
v6 = this.target;
v6 = this.provider;
/* .line 256 */
/* .local v6, "authority":Ljava/lang/String; */
v7 = android.text.TextUtils .isEmpty ( v6 );
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 257 */
v4 = android.util.Log .isLoggable ( v5,v4 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 258 */
android.util.Log .d ( v5,v2 );
/* .line 260 */
} // :cond_1
/* .line 264 */
} // :cond_2
v2 = v2 = com.android.server.content.MiSyncStrategyImpl$CleverMJStrategy.REAL_TIME_STRATEGY_AUTHORITY_SET;
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 265 */
v2 = android.util.Log .isLoggable ( v5,v4 );
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 266 */
final String v2 = "injector: isAllowedToRun: authority is not affected by strategy, return true"; // const-string v2, "injector: isAllowedToRun: authority is not affected by strategy, return true"
android.util.Log .d ( v5,v2 );
/* .line 268 */
} // :cond_3
/* .line 272 */
} // :cond_4
/* move-object/from16 v7, p0 */
v2 = /* invoke-direct {v7, v6, v1}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;->isFirstTimes(Ljava/lang/String;Landroid/os/Bundle;)Z */
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 273 */
v2 = android.util.Log .isLoggable ( v5,v4 );
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 274 */
final String v2 = "injector: isAllowedToRun: first full sync, return true"; // const-string v2, "injector: isAllowedToRun: first full sync, return true"
android.util.Log .d ( v5,v2 );
/* .line 276 */
} // :cond_5
/* .line 279 */
} // :cond_6
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
/* .line 280 */
/* .local v8, "currentTimeMills":J */
final String v2 = "key_interactive"; // const-string v2, "key_interactive"
v2 = (( android.os.Bundle ) v1 ).getBoolean ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
/* .line 282 */
/* .local v2, "isInteractive":Z */
final String v10 = "key_last_screen_off_time"; // const-string v10, "key_last_screen_off_time"
(( android.os.Bundle ) v1 ).getLong ( v10 ); // invoke-virtual {v1, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J
/* move-result-wide v10 */
/* .line 284 */
/* .local v10, "lastScreenOffTime":J */
final String v12 = "key_battery_charging"; // const-string v12, "key_battery_charging"
v12 = (( android.os.Bundle ) v1 ).getBoolean ( v12 ); // invoke-virtual {v1, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
/* .line 286 */
/* .local v12, "isBatteryCharging":Z */
if ( v12 != null) { // if-eqz v12, :cond_8
/* if-nez v2, :cond_8 */
/* sub-long v13, v8, v10 */
/* const-wide/32 v15, 0x1d4c0 */
/* cmp-long v13, v13, v15 */
/* if-lez v13, :cond_8 */
/* .line 288 */
v4 = android.util.Log .isLoggable ( v5,v4 );
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 289 */
final String v4 = "injector: isAllowedToRun: condition is satisfied, return true"; // const-string v4, "injector: isAllowedToRun: condition is satisfied, return true"
android.util.Log .d ( v5,v4 );
/* .line 291 */
} // :cond_7
/* .line 294 */
} // :cond_8
int v3 = 0; // const/4 v3, 0x0
/* .line 247 */
} // .end local v2 # "isInteractive":Z
} // .end local v6 # "authority":Ljava/lang/String;
} // .end local v8 # "currentTimeMills":J
} // .end local v10 # "lastScreenOffTime":J
} // .end local v12 # "isBatteryCharging":Z
} // :cond_9
/* move-object/from16 v7, p0 */
/* .line 248 */
} // :goto_0
v4 = android.util.Log .isLoggable ( v5,v4 );
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 249 */
android.util.Log .d ( v5,v2 );
/* .line 251 */
} // :cond_a
} // .end method
