public class com.android.server.content.SyncManagerStubImpl extends com.android.server.content.SyncManagerAccountChangePolicy implements com.android.server.content.SyncManagerStub {
	 /* .source "SyncManagerStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final Long SYNC_DELAY_ON_DISALLOW_METERED;
	 private static final java.lang.String TAG;
	 public static final android.net.Uri uri;
	 /* # instance fields */
	 com.miui.server.process.ProcessManagerInternal mPmi;
	 /* # direct methods */
	 static com.android.server.content.SyncManagerStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 35 */
		 /* const-string/jumbo v0, "sync_on_wifi_only" */
		 android.provider.Settings$Secure .getUriFor ( v0 );
		 return;
	 } // .end method
	 public com.android.server.content.SyncManagerStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Lcom/android/server/content/SyncManagerAccountChangePolicy;-><init>()V */
		 /* .line 22 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mPmi = v0;
		 return;
	 } // .end method
	 public static void handleMasterWifiOnlyChanged ( com.android.server.content.SyncManager p0 ) {
		 /* .locals 0 */
		 /* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 86 */
		 com.android.server.content.MiSyncPolicyManager .handleMasterWifiOnlyChanged ( p0 );
		 /* .line 87 */
		 return;
	 } // .end method
	 public static void handleSyncPauseChanged ( android.content.Context p0, com.android.server.content.SyncManager p1, Long p2 ) {
		 /* .locals 0 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .param p2, "pauseTimeMills" # J */
		 /* .line 91 */
		 com.android.server.content.MiSyncPolicyManager .handleSyncPauseChanged ( p0,p1,p2,p3 );
		 /* .line 92 */
		 return;
	 } // .end method
	 public static void handleSyncPauseChanged ( com.android.server.content.SyncManager p0 ) {
		 /* .locals 0 */
		 /* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 95 */
		 com.android.server.content.MiSyncPolicyManager .handleSyncPauseChanged ( p0 );
		 /* .line 96 */
		 return;
	 } // .end method
	 public static void handleSyncStrategyChanged ( android.content.Context p0, com.android.server.content.SyncManager p1 ) {
		 /* .locals 0 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 99 */
		 com.android.server.content.MiSyncPolicyManager .handleSyncStrategyChanged ( p0,p1 );
		 /* .line 100 */
		 return;
	 } // .end method
	 public static void handleSyncStrategyChanged ( com.android.server.content.SyncManager p0 ) {
		 /* .locals 0 */
		 /* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 103 */
		 com.android.server.content.MiSyncPolicyManager .handleSyncStrategyChanged ( p0 );
		 /* .line 104 */
		 return;
	 } // .end method
	 public static Boolean isDisallowMeteredBySettings ( android.content.Context p0 ) {
		 /* .locals 3 */
		 /* .param p0, "ctx" # Landroid/content/Context; */
		 /* .line 32 */
		 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* const-string/jumbo v1, "sync_on_wifi_only" */
		 int v2 = 0; // const/4 v2, 0x0
		 v0 = 		 android.provider.Settings$Secure .getInt ( v0,v1,v2 );
		 int v1 = 1; // const/4 v1, 0x1
		 /* if-ne v0, v1, :cond_0 */
		 /* move v2, v1 */
	 } // :cond_0
} // .end method
/* # virtual methods */
public Boolean canBindService ( android.content.Context p0, android.content.Intent p1, Integer p2 ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "service" # Landroid/content/Intent; */
	 /* .param p3, "userId" # I */
	 /* .line 28 */
	 v0 = 	 com.android.server.am.AutoStartManagerServiceStub .getInstance ( );
} // .end method
public Boolean getMasterSyncAutomatically ( android.accounts.Account p0, Integer p1, com.android.server.content.SyncStorageEngine p2 ) {
	 /* .locals 3 */
	 /* .param p1, "account" # Landroid/accounts/Account; */
	 /* .param p2, "userId" # I */
	 /* .param p3, "syncStorageEngine" # Lcom/android/server/content/SyncStorageEngine; */
	 /* .line 65 */
	 final String v0 = "com.xiaomi"; // const-string v0, "com.xiaomi"
	 /* .line 66 */
	 /* .local v0, "XIAOMI_ACCOUNT_TYPE":Ljava/lang/String; */
	 if ( p1 != null) { // if-eqz p1, :cond_0
		 final String v1 = "com.xiaomi"; // const-string v1, "com.xiaomi"
		 v2 = this.type;
		 v1 = 		 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 67 */
			 int v1 = 1; // const/4 v1, 0x1
			 /* .line 69 */
		 } // :cond_0
		 v1 = 		 (( com.android.server.content.SyncStorageEngine ) p3 ).getMasterSyncAutomatically ( p2 ); // invoke-virtual {p3, p2}, Lcom/android/server/content/SyncStorageEngine;->getMasterSyncAutomatically(I)Z
	 } // .end method
	 public Long getSyncDelayedH ( com.android.server.content.SyncOperation p0, com.android.server.content.SyncManager p1 ) {
		 /* .locals 2 */
		 /* .param p1, "op" # Lcom/android/server/content/SyncOperation; */
		 /* .param p2, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 44 */
		 com.android.server.content.MiSyncPolicyManager .getSyncDelayedH ( p1,p2 );
		 /* move-result-wide v0 */
		 /* return-wide v0 */
	 } // .end method
	 public Boolean isRestrictSync ( java.lang.String p0, Integer p1, java.lang.Object[] p2 ) {
		 /* .locals 3 */
		 /* .param p1, "pkgName" # Ljava/lang/String; */
		 /* .param p2, "uid" # I */
		 /* .param p3, "reserved" # [Ljava/lang/Object; */
		 /* .line 73 */
		 v0 = this.mPmi;
		 int v1 = 0; // const/4 v1, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 74 */
		 /* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
		 this.mPmi = v0;
		 /* .line 75 */
		 /* if-nez v0, :cond_0 */
		 /* .line 76 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "isRestrictSync false for service unready uid="; // const-string v2, "isRestrictSync false for service unready uid="
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v2 = "SyncManager"; // const-string v2, "SyncManager"
		 android.util.Log .d ( v2,v0 );
		 /* .line 77 */
		 /* .line 80 */
	 } // :cond_0
	 v0 = this.mPmi;
	 (( com.miui.server.process.ProcessManagerInternal ) v0 ).isForegroundApp ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/process/ProcessManagerInternal;->isForegroundApp(Ljava/lang/String;I)Z
} // .end method
public void registerSyncSettingsObserver ( android.content.Context p0, com.android.server.content.SyncManager p1 ) {
	 /* .locals 0 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "syncManager" # Lcom/android/server/content/SyncManager; */
	 /* .line 39 */
	 com.android.server.content.MiSyncPolicyManager .registerSyncSettingsObserver ( p1,p2 );
	 /* .line 40 */
	 return;
} // .end method
public void wrapSyncJobInfo ( android.content.Context p0, com.android.server.content.SyncOperation p1, com.android.server.content.SyncStorageEngine p2, android.app.job.JobInfo$Builder p3, Long p4 ) {
	 /* .locals 0 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "op" # Lcom/android/server/content/SyncOperation; */
	 /* .param p3, "syncStorageEngine" # Lcom/android/server/content/SyncStorageEngine; */
	 /* .param p4, "builder" # Landroid/app/job/JobInfo$Builder; */
	 /* .param p5, "minDelay" # J */
	 /* .line 59 */
	 /* invoke-static/range {p1 ..p6}, Lcom/android/server/content/MiSyncPolicyManager;->wrapSyncJobInfo(Landroid/content/Context;Lcom/android/server/content/SyncOperation;Lcom/android/server/content/SyncStorageEngine;Landroid/app/job/JobInfo$Builder;J)V */
	 /* .line 60 */
	 return;
} // .end method
