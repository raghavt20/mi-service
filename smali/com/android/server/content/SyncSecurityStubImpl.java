public class com.android.server.content.SyncSecurityStubImpl implements com.android.server.content.SyncSecurityStub {
	 /* .source "SyncSecurityStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String CLOUD_MANAGER_PERMISSION;
	 private static final java.lang.String TAG;
	 private static final java.lang.String XIAOMI_ACCOUNT_TYPE;
	 /* # direct methods */
	 public com.android.server.content.SyncSecurityStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static java.lang.String getAccountType ( android.accounts.Account p0 ) {
		 /* .locals 2 */
		 /* .param p0, "account" # Landroid/accounts/Account; */
		 /* .line 136 */
		 /* if-nez p0, :cond_0 */
		 /* .line 137 */
		 final String v0 = "[NULL]"; // const-string v0, "[NULL]"
		 /* .line 140 */
	 } // :cond_0
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "["; // const-string v1, "["
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.type;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v1 = "]"; // const-string v1, "]"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
/* # virtual methods */
public android.accounts.AccountAndUser filterOutXiaomiAccount ( android.accounts.AccountAndUser[] p0, Integer p1 ) {
	 /* .locals 8 */
	 /* .param p1, "accountAndUsers" # [Landroid/accounts/AccountAndUser; */
	 /* .param p2, "reason" # I */
	 /* .line 42 */
	 int v0 = 2; // const/4 v0, 0x2
	 final String v1 = "SyncSecurityImpl"; // const-string v1, "SyncSecurityImpl"
	 /* if-nez p1, :cond_1 */
	 /* .line 43 */
	 v0 = 	 android.util.Log .isLoggable ( v1,v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 44 */
		 final String v0 = "filterOutXiaomiAccount: null accountAndUsers, abort."; // const-string v0, "filterOutXiaomiAccount: null accountAndUsers, abort."
		 android.util.Slog .i ( v1,v0 );
		 /* .line 46 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 49 */
} // :cond_1
/* if-gez p2, :cond_3 */
/* .line 50 */
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 51 */
	 final String v0 = "filterOutXiaomiAccount: internal request, abort."; // const-string v0, "filterOutXiaomiAccount: internal request, abort."
	 android.util.Slog .i ( v1,v0 );
	 /* .line 53 */
} // :cond_2
/* .line 57 */
} // :cond_3
v2 = android.os.UserHandle .getAppId ( p2 );
/* .line 59 */
/* .local v2, "appId":I */
/* const/16 v3, 0x2710 */
/* if-ge v2, v3, :cond_5 */
/* .line 60 */
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 61 */
final String v0 = "filterOutXiaomiAccount: system request, abort."; // const-string v0, "filterOutXiaomiAccount: system request, abort."
android.util.Slog .i ( v1,v0 );
/* .line 63 */
} // :cond_4
/* .line 67 */
} // :cond_5
/* nop */
/* .line 68 */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
v3 = final String v4 = "com.xiaomi.permission.CLOUD_MANAGER"; // const-string v4, "com.xiaomi.permission.CLOUD_MANAGER"
/* if-nez v3, :cond_7 */
/* .line 69 */
v3 = android.util.Log .isLoggable ( v1,v0 );
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 70 */
final String v3 = "filterOutXiaomiAccount: CLOUD MANAGER, abort."; // const-string v3, "filterOutXiaomiAccount: CLOUD MANAGER, abort."
android.util.Slog .i ( v1,v3 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 72 */
} // :cond_6
/* .line 76 */
} // :cond_7
/* .line 74 */
/* :catch_0 */
/* move-exception v3 */
/* .line 78 */
} // :goto_0
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 79 */
final String v0 = "filterOutXiaomiAccount: go."; // const-string v0, "filterOutXiaomiAccount: go."
android.util.Slog .i ( v1,v0 );
/* .line 82 */
} // :cond_8
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 83 */
/* .local v0, "filtered":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/AccountAndUser;>;" */
/* array-length v1, p1 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_1
/* if-ge v4, v1, :cond_a */
/* aget-object v5, p1, v4 */
/* .line 84 */
/* .local v5, "au":Landroid/accounts/AccountAndUser; */
if ( v5 != null) { // if-eqz v5, :cond_9
v6 = this.account;
if ( v6 != null) { // if-eqz v6, :cond_9
v6 = this.account;
v6 = this.type;
final String v7 = "com.xiaomi"; // const-string v7, "com.xiaomi"
v6 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 85 */
/* .line 87 */
} // :cond_9
(( java.util.ArrayList ) v0 ).add ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 83 */
} // .end local v5 # "au":Landroid/accounts/AccountAndUser;
} // :goto_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 89 */
} // :cond_a
/* new-array v1, v3, [Landroid/accounts/AccountAndUser; */
(( java.util.ArrayList ) v0 ).toArray ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
/* check-cast v1, [Landroid/accounts/AccountAndUser; */
} // .end method
public Boolean permitControlSyncForAccount ( android.content.Context p0, android.accounts.Account p1 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "account" # Landroid/accounts/Account; */
/* .line 95 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 96 */
/* .local v0, "pid":I */
v1 = android.os.Binder .getCallingUid ( );
/* .line 98 */
/* .local v1, "uid":I */
final String v2 = "disallow_auto_sync"; // const-string v2, "disallow_auto_sync"
v3 = com.miui.enterprise.RestrictionsHelper .hasRestriction ( p1,v2 );
int v4 = 0; // const/4 v4, 0x0
final String v5 = "SyncSecurityImpl"; // const-string v5, "SyncSecurityImpl"
/* if-nez v3, :cond_9 */
/* .line 100 */
v2 = miui.enterprise.RestrictionsHelperStub .getInstance ( );
if ( v2 != null) { // if-eqz v2, :cond_0
/* goto/16 :goto_1 */
/* .line 107 */
} // :cond_0
/* const/16 v2, 0x2710 */
final String v3 = "Permit sync control for account "; // const-string v3, "Permit sync control for account "
int v6 = 1; // const/4 v6, 0x1
final String v7 = " by pid "; // const-string v7, " by pid "
int v8 = 2; // const/4 v8, 0x2
/* if-ge v1, v2, :cond_2 */
/* .line 108 */
v2 = android.util.Log .isLoggable ( v5,v8 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 109 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.content.SyncSecurityStubImpl .getAccountType ( p2 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ".SYSTEM UID."; // const-string v3, ".SYSTEM UID."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v2 );
/* .line 111 */
} // :cond_1
/* .line 113 */
} // :cond_2
if ( p2 != null) { // if-eqz p2, :cond_7
final String v2 = "com.xiaomi"; // const-string v2, "com.xiaomi"
v9 = this.type;
v2 = (( java.lang.String ) v2 ).equals ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_3 */
/* .line 119 */
} // :cond_3
final String v2 = "com.xiaomi.permission.CLOUD_MANAGER"; // const-string v2, "com.xiaomi.permission.CLOUD_MANAGER"
v2 = (( android.content.Context ) p1 ).checkCallingOrSelfPermission ( v2 ); // invoke-virtual {p1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
/* if-nez v2, :cond_5 */
/* .line 121 */
v2 = android.util.Log .isLoggable ( v5,v8 );
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 122 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.content.SyncSecurityStubImpl .getAccountType ( p2 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ".CLOUD MANAGER."; // const-string v3, ".CLOUD MANAGER."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v2 );
/* .line 124 */
} // :cond_4
/* .line 127 */
} // :cond_5
v2 = android.util.Log .isLoggable ( v5,v8 );
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 128 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Deny sync control for account "; // const-string v3, "Deny sync control for account "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.content.SyncSecurityStubImpl .getAccountType ( p2 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "."; // const-string v3, "."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v2 );
/* .line 130 */
} // :cond_6
/* .line 114 */
} // :cond_7
} // :goto_0
v2 = android.util.Log .isLoggable ( v5,v8 );
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 115 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.content.SyncSecurityStubImpl .getAccountType ( p2 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ".OTHER ACCOUNT."; // const-string v3, ".OTHER ACCOUNT."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v2 );
/* .line 117 */
} // :cond_8
/* .line 102 */
} // :cond_9
} // :goto_1
final String v2 = "Device is in enterprise mode, sync is restricted by enterprise!"; // const-string v2, "Device is in enterprise mode, sync is restricted by enterprise!"
android.util.Slog .d ( v5,v2 );
/* .line 103 */
} // .end method
