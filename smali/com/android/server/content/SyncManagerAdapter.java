class com.android.server.content.SyncManagerAdapter {
	 /* .source "SyncManagerAdapter.java" */
	 /* # direct methods */
	 private com.android.server.content.SyncManagerAdapter ( ) {
		 /* .locals 0 */
		 /* .line 4 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void postScheduleSyncMessage ( com.android.server.content.SyncManager p0, com.android.server.content.SyncOperation p1, Long p2 ) {
		 /* .locals 0 */
		 /* .param p0, "manager" # Lcom/android/server/content/SyncManager; */
		 /* .param p1, "op" # Lcom/android/server/content/SyncOperation; */
		 /* .param p2, "delay" # J */
		 /* .line 7 */
		 (( com.android.server.content.SyncManager ) p0 ).postScheduleSyncMessage ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/content/SyncManager;->postScheduleSyncMessage(Lcom/android/server/content/SyncOperation;J)V
		 /* .line 8 */
		 return;
	 } // .end method
