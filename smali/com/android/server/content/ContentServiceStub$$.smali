.class public final Lcom/android/server/content/ContentServiceStub$$;
.super Ljava/lang/Object;
.source "ContentServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 18
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/content/ContentServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/content/ContentServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.content.ContentServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    new-instance v0, Lcom/android/server/content/SyncSecurityStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/content/SyncSecurityStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.content.SyncSecurityStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v0, Lcom/android/server/content/SyncManagerStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/content/SyncManagerStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.content.SyncManagerStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/content/SyncStorageEngineStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/content/SyncStorageEngineStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.content.SyncStorageEngineStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    return-void
.end method
