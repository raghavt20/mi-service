class com.android.server.content.SyncJobInfoProcessor {
	 /* .source "SyncJobInfoProcessor.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 com.android.server.content.SyncJobInfoProcessor ( ) {
		 /* .locals 0 */
		 /* .line 21 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static void buildSyncJobInfo ( android.content.Context p0, com.android.server.content.SyncOperation p1, com.android.server.content.SyncStorageEngine p2, android.app.job.JobInfo$Builder p3, Long p4 ) {
		 /* .locals 9 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "op" # Lcom/android/server/content/SyncOperation; */
		 /* .param p2, "syncStorageEngine" # Lcom/android/server/content/SyncStorageEngine; */
		 /* .param p3, "builder" # Landroid/app/job/JobInfo$Builder; */
		 /* .param p4, "minDelay" # J */
		 /* .line 39 */
		 int v0 = 3; // const/4 v0, 0x3
		 final String v1 = "SyncJobInfoProcessor"; // const-string v1, "SyncJobInfoProcessor"
		 /* if-nez p1, :cond_1 */
		 /* .line 40 */
		 v0 = 		 android.util.Log .isLoggable ( v1,v0 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 41 */
			 final String v0 = "injector: wrapSyncJobInfo: null parameter, return"; // const-string v0, "injector: wrapSyncJobInfo: null parameter, return"
			 android.util.Log .d ( v1,v0 );
			 /* .line 43 */
		 } // :cond_0
		 return;
		 /* .line 46 */
	 } // :cond_1
	 v2 = 	 (( com.android.server.content.SyncOperation ) p1 ).isScheduledAsExpeditedJob ( ); // invoke-virtual {p1}, Lcom/android/server/content/SyncOperation;->isScheduledAsExpeditedJob()Z
	 int v3 = 1; // const/4 v3, 0x1
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 /* iget-boolean v2, p1, Lcom/android/server/content/SyncOperation;->scheduleEjAsRegularJob:Z */
		 /* if-nez v2, :cond_2 */
		 /* move v2, v3 */
	 } // :cond_2
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 47 */
	 /* .local v2, "isScheduledAsEj":Z */
} // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_4
	 /* .line 48 */
	 v0 = 	 android.util.Log .isLoggable ( v1,v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_3
		 /* .line 49 */
		 final String v0 = "injector: wrapSyncJobInfo: is scheduled as expedited job, no more constraints."; // const-string v0, "injector: wrapSyncJobInfo: is scheduled as expedited job, no more constraints."
		 android.util.Log .d ( v1,v0 );
		 /* .line 51 */
	 } // :cond_3
	 return;
	 /* .line 55 */
} // :cond_4
v4 = com.android.server.content.MiSyncUtils .checkSyncOperationPass ( p1 );
/* .line 58 */
/* .local v4, "isSyncOperationPass":Z */
/* if-nez v4, :cond_6 */
/* .line 59 */
v5 = android.util.Log .isLoggable ( v1,v0 );
if ( v5 != null) { // if-eqz v5, :cond_5
	 /* .line 60 */
	 final String v5 = "injector: wrapSyncJobInfo: setRequiresBatteryNotLow"; // const-string v5, "injector: wrapSyncJobInfo: setRequiresBatteryNotLow"
	 android.util.Log .d ( v1,v5 );
	 /* .line 63 */
} // :cond_5
(( android.app.job.JobInfo$Builder ) p3 ).setRequiresBatteryNotLow ( v3 ); // invoke-virtual {p3, v3}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;
/* .line 67 */
} // :cond_6
/* if-nez v4, :cond_8 */
/* .line 68 */
v3 = com.android.server.content.SyncJobInfoProcessor .isMasterSyncOnWifiOnly ( p0 );
if ( v3 != null) { // if-eqz v3, :cond_8
v3 = (( com.android.server.content.SyncOperation ) p1 ).isNotAllowedOnMetered ( ); // invoke-virtual {p1}, Lcom/android/server/content/SyncOperation;->isNotAllowedOnMetered()Z
/* if-nez v3, :cond_8 */
/* .line 69 */
v3 = android.util.Log .isLoggable ( v1,v0 );
if ( v3 != null) { // if-eqz v3, :cond_7
	 /* .line 70 */
	 final String v3 = "injector: wrapSyncJobInfo: setRequiredNetworkType: Unmetered"; // const-string v3, "injector: wrapSyncJobInfo: setRequiredNetworkType: Unmetered"
	 android.util.Log .d ( v1,v3 );
	 /* .line 72 */
} // :cond_7
int v3 = 2; // const/4 v3, 0x2
(( android.app.job.JobInfo$Builder ) p3 ).setRequiredNetworkType ( v3 ); // invoke-virtual {p3, v3}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;
/* .line 77 */
} // :cond_8
v3 = com.android.server.content.MiSyncUtils .checkSyncOperationAccount ( p1 );
/* if-nez v3, :cond_a */
/* .line 78 */
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 79 */
final String v0 = "injector: wrapSyncJobInfo: not xiaomi account, return"; // const-string v0, "injector: wrapSyncJobInfo: not xiaomi account, return"
android.util.Log .v ( v1,v0 );
/* .line 81 */
} // :cond_9
return;
/* .line 85 */
} // :cond_a
v3 = this.target;
v3 = this.account;
v3 = this.name;
v5 = this.target;
/* iget v5, v5, Lcom/android/server/content/SyncStorageEngine$EndPoint;->userId:I */
/* .line 86 */
(( com.android.server.content.SyncStorageEngine ) p2 ).getMiSyncPause ( v3, v5 ); // invoke-virtual {p2, v3, v5}, Lcom/android/server/content/SyncStorageEngine;->getMiSyncPause(Ljava/lang/String;I)Lcom/android/server/content/MiSyncPause;
/* .line 87 */
/* .local v3, "miSyncPause":Lcom/android/server/content/MiSyncPause; */
/* if-nez v3, :cond_c */
/* .line 88 */
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 89 */
final String v0 = "injector: wrapSyncJobInfo: mi sync pause is null"; // const-string v0, "injector: wrapSyncJobInfo: mi sync pause is null"
android.util.Log .d ( v1,v0 );
/* .line 91 */
} // :cond_b
return;
/* .line 93 */
} // :cond_c
/* move-result-wide v5 */
/* .line 94 */
/* .local v5, "syncResumeTimeLeft":J */
/* cmp-long v7, v5, p4 */
/* if-lez v7, :cond_e */
/* iget-boolean v7, p1, Lcom/android/server/content/SyncOperation;->isPeriodic:Z */
/* if-nez v7, :cond_e */
/* .line 95 */
v7 = android.util.Log .isLoggable ( v1,v0 );
if ( v7 != null) { // if-eqz v7, :cond_d
/* .line 96 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "injector: wrapSyncJobInfo: setMinimumLatency: "; // const-string v8, "injector: wrapSyncJobInfo: setMinimumLatency: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5, v6 ); // invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v7 );
/* .line 98 */
} // :cond_d
(( android.app.job.JobInfo$Builder ) p3 ).setMinimumLatency ( v5, v6 ); // invoke-virtual {p3, v5, v6}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;
/* .line 102 */
} // :cond_e
/* if-nez v4, :cond_12 */
/* .line 103 */
v7 = this.target;
v7 = this.account;
v7 = this.name;
v8 = this.target;
/* iget v8, v8, Lcom/android/server/content/SyncStorageEngine$EndPoint;->userId:I */
/* .line 104 */
(( com.android.server.content.SyncStorageEngine ) p2 ).getMiSyncStrategy ( v7, v8 ); // invoke-virtual {p2, v7, v8}, Lcom/android/server/content/SyncStorageEngine;->getMiSyncStrategy(Ljava/lang/String;I)Lcom/android/server/content/MiSyncStrategy;
/* .line 105 */
/* .local v7, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategy; */
/* if-nez v7, :cond_10 */
/* .line 106 */
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 107 */
final String v0 = "injector: wrapSyncJobInfo: mi sync strategy is null"; // const-string v0, "injector: wrapSyncJobInfo: mi sync strategy is null"
android.util.Log .d ( v1,v0 );
/* .line 109 */
} // :cond_f
return;
/* .line 112 */
} // :cond_10
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_11
/* .line 113 */
final String v0 = "injector: wrapSyncJobInfo: apply mi sync strategy"; // const-string v0, "injector: wrapSyncJobInfo: apply mi sync strategy"
android.util.Log .d ( v1,v0 );
/* .line 115 */
} // :cond_11
com.android.server.content.SyncJobInfoProcessor .getExtrasForStrategy ( p2,p1 );
/* .line 117 */
} // .end local v7 # "miSyncStrategy":Lcom/android/server/content/MiSyncStrategy;
} // :cond_12
return;
} // .end method
private static android.os.Bundle getExtrasForStrategy ( com.android.server.content.SyncStorageEngine p0, com.android.server.content.SyncOperation p1 ) {
/* .locals 4 */
/* .param p0, "syncStorageEngine" # Lcom/android/server/content/SyncStorageEngine; */
/* .param p1, "op" # Lcom/android/server/content/SyncOperation; */
/* .line 126 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 127 */
/* .local v0, "bundle":Landroid/os/Bundle; */
if ( p0 != null) { // if-eqz p0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 128 */
v1 = this.target;
/* .line 129 */
(( com.android.server.content.SyncStorageEngine ) p0 ).getStatusByAuthority ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/content/SyncStorageEngine;->getStatusByAuthority(Lcom/android/server/content/SyncStorageEngine$EndPoint;)Landroid/content/SyncStatusInfo;
/* .line 130 */
/* .local v1, "syncStatusInfo":Landroid/content/SyncStatusInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 131 */
/* nop */
/* .line 132 */
v2 = android.content.SyncStatusInfoAdapter .getNumSyncs ( v1 );
/* .line 131 */
final String v3 = "key_num_syncs"; // const-string v3, "key_num_syncs"
(( android.os.Bundle ) v0 ).putInt ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 135 */
} // .end local v1 # "syncStatusInfo":Landroid/content/SyncStatusInfo;
} // :cond_0
} // .end method
private static Boolean isMasterSyncOnWifiOnly ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 120 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "sync_on_wifi_only" */
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
