.class public Lcom/android/server/content/ContentServiceStubImpl;
.super Lcom/android/server/content/ContentServiceStub;
.source "ContentServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.content.ContentServiceStub$$"
.end annotation


# static fields
.field private static final MIUI_OBSERVERS_THRESHOLD:I = 0x4e20

.field private static final sObserverHistogram:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 101
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Lcom/android/server/content/ContentServiceStubImpl;->sObserverHistogram:Landroid/util/ArrayMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/android/server/content/ContentServiceStub;-><init>()V

    return-void
.end method


# virtual methods
.method public decreasePidObserverCount(II)V
    .locals 6
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 121
    sget-object v0, Lcom/android/server/content/ContentServiceStubImpl;->sObserverHistogram:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 122
    int-to-long v1, p1

    const/16 v3, 0x20

    shl-long/2addr v1, v3

    int-to-long v3, p2

    or-long/2addr v1, v3

    .line 123
    .local v1, "uidPid":J
    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 124
    .local v3, "prevCount":I
    if-ne v3, v4, :cond_0

    monitor-exit v0

    return-void

    .line 125
    :cond_0
    if-lez v3, :cond_1

    .line 126
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    add-int/lit8 v5, v3, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 128
    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    .end local v1    # "uidPid":J
    .end local v3    # "prevCount":I
    :goto_0
    monitor-exit v0

    .line 131
    return-void

    .line 130
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dumpObserverHistogram(Ljava/io/PrintWriter;)V
    .locals 10
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 135
    const-string v0, "Observer histogram:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 136
    sget-object v0, Lcom/android/server/content/ContentServiceStubImpl;->sObserverHistogram:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 137
    const/4 v1, 0x0

    .local v1, "i":I
    :try_start_0
    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 138
    sget-object v3, Lcom/android/server/content/ContentServiceStubImpl;->sObserverHistogram:Landroid/util/ArrayMap;

    invoke-virtual {v3, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 139
    .local v4, "key":J
    invoke-virtual {v3, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 140
    .local v3, "count":I
    const/16 v6, 0x20

    shr-long v6, v4, v6

    long-to-int v6, v6

    .line 141
    .local v6, "uid":I
    long-to-int v7, v4

    .line 142
    .local v7, "pid":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  uid: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " pid: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " observers"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 137
    .end local v3    # "count":I
    .end local v4    # "key":J
    .end local v6    # "uid":I
    .end local v7    # "pid":I
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    .end local v1    # "i":I
    .end local v2    # "size":I
    :cond_0
    monitor-exit v0

    .line 145
    return-void

    .line 144
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getMiSyncPauseToTime(Landroid/content/Context;Lcom/android/server/content/ContentService;Landroid/accounts/Account;I)J
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentService"    # Lcom/android/server/content/ContentService;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "uid"    # I

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "no permission to read the sync settings for user "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Lcom/android/server/content/ContentService;->enforceCrossUserPermissionForInjector(ILjava/lang/String;)V

    .line 45
    const-string v0, "android.permission.READ_SYNC_SETTINGS"

    const-string v1, "no permission to read the sync settings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-static {}, Lcom/android/server/content/ContentService;->clearCallingIdentity()J

    move-result-wide v0

    .line 50
    .local v0, "identityToken":J
    :try_start_0
    invoke-virtual {p2}, Lcom/android/server/content/ContentService;->getSyncManagerForInjector()Lcom/android/server/content/SyncManager;

    move-result-object v2

    .line 51
    .local v2, "syncManager":Lcom/android/server/content/SyncManager;
    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {v2}, Lcom/android/server/content/SyncManager;->getSyncStorageEngine()Lcom/android/server/content/SyncStorageEngine;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Lcom/android/server/content/SyncStorageEngine;->getMiSyncPauseToTime(Landroid/accounts/Account;I)J

    move-result-wide v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 52
    return-wide v3

    .line 55
    .end local v2    # "syncManager":Lcom/android/server/content/SyncManager;
    :cond_0
    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 56
    nop

    .line 57
    const-wide/16 v2, 0x0

    return-wide v2

    .line 55
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 56
    throw v2
.end method

.method public getMiSyncStrategy(Landroid/content/Context;Lcom/android/server/content/ContentService;Landroid/accounts/Account;I)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentService"    # Lcom/android/server/content/ContentService;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "uid"    # I

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "no permission to read the sync settings for user "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Lcom/android/server/content/ContentService;->enforceCrossUserPermissionForInjector(ILjava/lang/String;)V

    .line 85
    const-string v0, "android.permission.READ_SYNC_SETTINGS"

    const-string v1, "no permission to read the sync settings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-static {}, Lcom/android/server/content/ContentService;->clearCallingIdentity()J

    move-result-wide v0

    .line 90
    .local v0, "identityToken":J
    :try_start_0
    invoke-virtual {p2}, Lcom/android/server/content/ContentService;->getSyncManagerForInjector()Lcom/android/server/content/SyncManager;

    move-result-object v2

    .line 91
    .local v2, "syncManager":Lcom/android/server/content/SyncManager;
    if-eqz v2, :cond_0

    .line 92
    invoke-virtual {v2}, Lcom/android/server/content/SyncManager;->getSyncStorageEngine()Lcom/android/server/content/SyncStorageEngine;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Lcom/android/server/content/SyncStorageEngine;->getMiSyncStrategy(Landroid/accounts/Account;I)I

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 92
    return v3

    .line 95
    .end local v2    # "syncManager":Lcom/android/server/content/SyncManager;
    :cond_0
    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 96
    nop

    .line 97
    const/4 v2, 0x0

    return v2

    .line 95
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 96
    throw v2
.end method

.method public increasePidObserverCount(II)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 106
    sget-object v0, Lcom/android/server/content/ContentServiceStubImpl;->sObserverHistogram:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 107
    int-to-long v1, p1

    const/16 v3, 0x20

    shl-long/2addr v1, v3

    int-to-long v3, p2

    or-long/2addr v1, v3

    .line 108
    .local v1, "uidPid":J
    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 109
    .local v3, "count":I
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    const/16 v4, 0x4e20

    if-ne v3, v4, :cond_1

    const/16 v4, 0x3e8

    if-eq p1, v4, :cond_1

    .line 112
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    if-ne v4, v5, :cond_0

    goto :goto_0

    .line 113
    :cond_0
    new-instance v4, Ljava/lang/SecurityException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "uid "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pid "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " registered too many content observers"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/android/server/content/ContentServiceStubImpl;
    .end local p1    # "uid":I
    .end local p2    # "pid":I
    throw v4

    .line 116
    .end local v1    # "uidPid":J
    .end local v3    # "count":I
    .restart local p0    # "this":Lcom/android/server/content/ContentServiceStubImpl;
    .restart local p1    # "uid":I
    .restart local p2    # "pid":I
    :cond_1
    :goto_0
    monitor-exit v0

    .line 117
    return-void

    .line 116
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setMiSyncPauseToTime(Landroid/content/Context;Lcom/android/server/content/ContentService;Landroid/accounts/Account;JI)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentService"    # Lcom/android/server/content/ContentService;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "pauseTimeMillis"    # J
    .param p6, "uid"    # I

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "no permission to set the sync status for user "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p6, v0}, Lcom/android/server/content/ContentService;->enforceCrossUserPermissionForInjector(ILjava/lang/String;)V

    .line 25
    const-string v0, "android.permission.WRITE_SYNC_SETTINGS"

    const-string v1, "no permission to write the sync settings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-static {}, Lcom/android/server/content/ContentService;->clearCallingIdentity()J

    move-result-wide v0

    .line 30
    .local v0, "identityToken":J
    :try_start_0
    invoke-virtual {p2}, Lcom/android/server/content/ContentService;->getSyncManagerForInjector()Lcom/android/server/content/SyncManager;

    move-result-object v2

    .line 31
    .local v2, "syncManager":Lcom/android/server/content/SyncManager;
    if-eqz v2, :cond_0

    .line 32
    invoke-virtual {v2}, Lcom/android/server/content/SyncManager;->getSyncStorageEngine()Lcom/android/server/content/SyncStorageEngine;

    move-result-object v3

    invoke-virtual {v3, p3, p4, p5, p6}, Lcom/android/server/content/SyncStorageEngine;->setMiSyncPauseToTime(Landroid/accounts/Account;JI)V

    .line 33
    invoke-static {p1, v2, p4, p5}, Lcom/android/server/content/SyncManagerStubImpl;->handleSyncPauseChanged(Landroid/content/Context;Lcom/android/server/content/SyncManager;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    .end local v2    # "syncManager":Lcom/android/server/content/SyncManager;
    :cond_0
    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 37
    nop

    .line 38
    return-void

    .line 36
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 37
    throw v2
.end method

.method public setMiSyncStrategy(Landroid/content/Context;Lcom/android/server/content/ContentService;Landroid/accounts/Account;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentService"    # Lcom/android/server/content/ContentService;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "strategy"    # I
    .param p5, "uid"    # I

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "no permission to set the sync status for user "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p5, v0}, Lcom/android/server/content/ContentService;->enforceCrossUserPermissionForInjector(ILjava/lang/String;)V

    .line 65
    const-string v0, "android.permission.WRITE_SYNC_SETTINGS"

    const-string v1, "no permission to write the sync settings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/android/server/content/ContentService;->clearCallingIdentity()J

    move-result-wide v0

    .line 70
    .local v0, "identityToken":J
    :try_start_0
    invoke-virtual {p2}, Lcom/android/server/content/ContentService;->getSyncManagerForInjector()Lcom/android/server/content/SyncManager;

    move-result-object v2

    .line 71
    .local v2, "syncManager":Lcom/android/server/content/SyncManager;
    if-eqz v2, :cond_0

    .line 72
    invoke-virtual {v2}, Lcom/android/server/content/SyncManager;->getSyncStorageEngine()Lcom/android/server/content/SyncStorageEngine;

    move-result-object v3

    invoke-virtual {v3, p3, p4, p5}, Lcom/android/server/content/SyncStorageEngine;->setMiSyncStrategy(Landroid/accounts/Account;II)V

    .line 73
    invoke-static {p1, v2}, Lcom/android/server/content/SyncManagerStubImpl;->handleSyncStrategyChanged(Landroid/content/Context;Lcom/android/server/content/SyncManager;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    .end local v2    # "syncManager":Lcom/android/server/content/SyncManager;
    :cond_0
    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 77
    nop

    .line 78
    return-void

    .line 76
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Lcom/android/server/content/ContentService;->restoreCallingIdentity(J)V

    .line 77
    throw v2
.end method
