.class Lcom/android/server/content/SyncJobInfoProcessor;
.super Ljava/lang/Object;
.source "SyncJobInfoProcessor.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SyncJobInfoProcessor"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static buildSyncJobInfo(Landroid/content/Context;Lcom/android/server/content/SyncOperation;Lcom/android/server/content/SyncStorageEngine;Landroid/app/job/JobInfo$Builder;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "op"    # Lcom/android/server/content/SyncOperation;
    .param p2, "syncStorageEngine"    # Lcom/android/server/content/SyncStorageEngine;
    .param p3, "builder"    # Landroid/app/job/JobInfo$Builder;
    .param p4, "minDelay"    # J

    .line 39
    const/4 v0, 0x3

    const-string v1, "SyncJobInfoProcessor"

    if-nez p1, :cond_1

    .line 40
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const-string v0, "injector: wrapSyncJobInfo: null parameter, return"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_0
    return-void

    .line 46
    :cond_1
    invoke-virtual {p1}, Lcom/android/server/content/SyncOperation;->isScheduledAsExpeditedJob()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    iget-boolean v2, p1, Lcom/android/server/content/SyncOperation;->scheduleEjAsRegularJob:Z

    if-nez v2, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 47
    .local v2, "isScheduledAsEj":Z
    :goto_0
    if-eqz v2, :cond_4

    .line 48
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 49
    const-string v0, "injector: wrapSyncJobInfo: is scheduled as expedited job, no more constraints."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_3
    return-void

    .line 55
    :cond_4
    invoke-static {p1}, Lcom/android/server/content/MiSyncUtils;->checkSyncOperationPass(Lcom/android/server/content/SyncOperation;)Z

    move-result v4

    .line 58
    .local v4, "isSyncOperationPass":Z
    if-nez v4, :cond_6

    .line 59
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 60
    const-string v5, "injector: wrapSyncJobInfo: setRequiresBatteryNotLow"

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_5
    invoke-virtual {p3, v3}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;

    .line 67
    :cond_6
    if-nez v4, :cond_8

    .line 68
    invoke-static {p0}, Lcom/android/server/content/SyncJobInfoProcessor;->isMasterSyncOnWifiOnly(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p1}, Lcom/android/server/content/SyncOperation;->isNotAllowedOnMetered()Z

    move-result v3

    if-nez v3, :cond_8

    .line 69
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 70
    const-string v3, "injector: wrapSyncJobInfo: setRequiredNetworkType: Unmetered"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_7
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    .line 77
    :cond_8
    invoke-static {p1}, Lcom/android/server/content/MiSyncUtils;->checkSyncOperationAccount(Lcom/android/server/content/SyncOperation;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 78
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 79
    const-string v0, "injector: wrapSyncJobInfo: not xiaomi account, return"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_9
    return-void

    .line 85
    :cond_a
    iget-object v3, p1, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    iget-object v3, v3, Lcom/android/server/content/SyncStorageEngine$EndPoint;->account:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    iget v5, v5, Lcom/android/server/content/SyncStorageEngine$EndPoint;->userId:I

    .line 86
    invoke-virtual {p2, v3, v5}, Lcom/android/server/content/SyncStorageEngine;->getMiSyncPause(Ljava/lang/String;I)Lcom/android/server/content/MiSyncPause;

    move-result-object v3

    .line 87
    .local v3, "miSyncPause":Lcom/android/server/content/MiSyncPause;
    if-nez v3, :cond_c

    .line 88
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 89
    const-string v0, "injector: wrapSyncJobInfo: mi sync pause is null"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_b
    return-void

    .line 93
    :cond_c
    invoke-interface {v3}, Lcom/android/server/content/MiSyncPause;->getResumeTimeLeft()J

    move-result-wide v5

    .line 94
    .local v5, "syncResumeTimeLeft":J
    cmp-long v7, v5, p4

    if-lez v7, :cond_e

    iget-boolean v7, p1, Lcom/android/server/content/SyncOperation;->isPeriodic:Z

    if-nez v7, :cond_e

    .line 95
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 96
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "injector: wrapSyncJobInfo: setMinimumLatency: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_d
    invoke-virtual {p3, v5, v6}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    .line 102
    :cond_e
    if-nez v4, :cond_12

    .line 103
    iget-object v7, p1, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    iget-object v7, v7, Lcom/android/server/content/SyncStorageEngine$EndPoint;->account:Landroid/accounts/Account;

    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v8, p1, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    iget v8, v8, Lcom/android/server/content/SyncStorageEngine$EndPoint;->userId:I

    .line 104
    invoke-virtual {p2, v7, v8}, Lcom/android/server/content/SyncStorageEngine;->getMiSyncStrategy(Ljava/lang/String;I)Lcom/android/server/content/MiSyncStrategy;

    move-result-object v7

    .line 105
    .local v7, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategy;
    if-nez v7, :cond_10

    .line 106
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 107
    const-string v0, "injector: wrapSyncJobInfo: mi sync strategy is null"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_f
    return-void

    .line 112
    :cond_10
    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 113
    const-string v0, "injector: wrapSyncJobInfo: apply mi sync strategy"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_11
    invoke-static {p2, p1}, Lcom/android/server/content/SyncJobInfoProcessor;->getExtrasForStrategy(Lcom/android/server/content/SyncStorageEngine;Lcom/android/server/content/SyncOperation;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {v7, p1, v0, p3}, Lcom/android/server/content/MiSyncStrategy;->apply(Lcom/android/server/content/SyncOperation;Landroid/os/Bundle;Landroid/app/job/JobInfo$Builder;)V

    .line 117
    .end local v7    # "miSyncStrategy":Lcom/android/server/content/MiSyncStrategy;
    :cond_12
    return-void
.end method

.method private static getExtrasForStrategy(Lcom/android/server/content/SyncStorageEngine;Lcom/android/server/content/SyncOperation;)Landroid/os/Bundle;
    .locals 4
    .param p0, "syncStorageEngine"    # Lcom/android/server/content/SyncStorageEngine;
    .param p1, "op"    # Lcom/android/server/content/SyncOperation;

    .line 126
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 127
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 128
    iget-object v1, p1, Lcom/android/server/content/SyncOperation;->target:Lcom/android/server/content/SyncStorageEngine$EndPoint;

    .line 129
    invoke-virtual {p0, v1}, Lcom/android/server/content/SyncStorageEngine;->getStatusByAuthority(Lcom/android/server/content/SyncStorageEngine$EndPoint;)Landroid/content/SyncStatusInfo;

    move-result-object v1

    .line 130
    .local v1, "syncStatusInfo":Landroid/content/SyncStatusInfo;
    if-eqz v1, :cond_0

    .line 131
    nop

    .line 132
    invoke-static {v1}, Landroid/content/SyncStatusInfoAdapter;->getNumSyncs(Landroid/content/SyncStatusInfo;)I

    move-result v2

    .line 131
    const-string v3, "key_num_syncs"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 135
    .end local v1    # "syncStatusInfo":Landroid/content/SyncStatusInfo;
    :cond_0
    return-object v0
.end method

.method private static isMasterSyncOnWifiOnly(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 120
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sync_on_wifi_only"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method
