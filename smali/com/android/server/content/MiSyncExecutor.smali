.class Lcom/android/server/content/MiSyncExecutor;
.super Ljava/lang/Object;
.source "MiSyncExecutor.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static sync(Lcom/android/server/content/SyncManager;ILandroid/accounts/Account;)V
    .locals 2
    .param p0, "syncManager"    # Lcom/android/server/content/SyncManager;
    .param p1, "sendingUserId"    # I
    .param p2, "account"    # Landroid/accounts/Account;

    .line 11
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Android sdk >= Q, unsupport call MiSyncExecutor sync method"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
