public class com.android.server.content.SyncManagerAccountChangePolicy {
	 /* .source "SyncManagerAccountChangePolicy.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/content/SyncManagerAccountChangePolicy$RealTimeStrategy;, */
	 /* Lcom/android/server/content/SyncManagerAccountChangePolicy$DefaultSyncDataStrategy;, */
	 /* Lcom/android/server/content/SyncManagerAccountChangePolicy$SyncForbiddenStrategy; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ALLOW_FIRST_NUM_SYNCS;
private static final Integer ALLOW_FIRST_NUM_SYNCS_FOR_BROWSER;
private static final java.lang.String AUTHORITY_BROWSER;
private static final java.lang.String AUTHORITY_CALENDAR;
private static final java.lang.String AUTHORITY_CONTACTS;
protected static final java.lang.String AUTHORITY_GALLERY;
private static final java.lang.String AUTHORITY_NOTES;
private static final Long DEFAULT_SCREEN_OFF_PENDING_TIME;
static final java.lang.String EXTRA_KEY_BATTERY_CHARGING;
static final java.lang.String EXTRA_KEY_BATTERY_LOW;
static final java.lang.String EXTRA_KEY_INTERACTIVE;
static final java.lang.String EXTRA_KEY_LAST_SCREEN_OFF_TIME;
static final java.lang.String EXTRA_KEY_NUM_SYNCS;
private static final Integer LOW_BATTERY_LEVEL_LIMIT;
protected static final java.lang.String PACKAGE_NAME_GALLERY;
private static final java.util.Set REAL_TIME_STRATEGY_AUTHORITY_SET;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String TAG;
/* # direct methods */
static com.android.server.content.SyncManagerAccountChangePolicy ( ) {
/* .locals 2 */
/* .line 73 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 75 */
final String v1 = "com.android.calendar"; // const-string v1, "com.android.calendar"
/* .line 76 */
final String v1 = "notes"; // const-string v1, "notes"
/* .line 77 */
final String v1 = "com.android.contacts"; // const-string v1, "com.android.contacts"
/* .line 78 */
return;
} // .end method
public com.android.server.content.SyncManagerAccountChangePolicy ( ) {
/* .locals 0 */
/* .line 21 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static com.android.server.content.SyncManagerAccountChangePolicy$SyncForbiddenStrategy getSyncForbiddenStrategy ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "authority" # Ljava/lang/String; */
/* .line 91 */
v0 = v0 = com.android.server.content.SyncManagerAccountChangePolicy.REAL_TIME_STRATEGY_AUTHORITY_SET;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 92 */
/* new-instance v0, Lcom/android/server/content/SyncManagerAccountChangePolicy$RealTimeStrategy; */
/* invoke-direct {v0, v1}, Lcom/android/server/content/SyncManagerAccountChangePolicy$RealTimeStrategy;-><init>(Lcom/android/server/content/SyncManagerAccountChangePolicy$RealTimeStrategy-IA;)V */
/* .line 94 */
} // :cond_0
/* new-instance v0, Lcom/android/server/content/SyncManagerAccountChangePolicy$DefaultSyncDataStrategy; */
/* invoke-direct {v0, v1}, Lcom/android/server/content/SyncManagerAccountChangePolicy$DefaultSyncDataStrategy;-><init>(Lcom/android/server/content/SyncManagerAccountChangePolicy$DefaultSyncDataStrategy-IA;)V */
} // .end method
public static Boolean isBatteryCharging ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "status" # I */
/* .line 41 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p0, v0, :cond_1 */
int v0 = 5; // const/4 v0, 0x5
/* if-ne p0, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public static Boolean isBatteryCharging ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 45 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 46 */
/* .local v0, "ifilter":Landroid/content/IntentFilter; */
int v1 = 0; // const/4 v1, 0x0
(( android.content.Context ) p0 ).registerReceiver ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 48 */
/* .local v1, "batteryStatus":Landroid/content/Intent; */
/* const-string/jumbo v2, "status" */
int v3 = -1; // const/4 v3, -0x1
v2 = (( android.content.Intent ) v1 ).getIntExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 49 */
/* .local v2, "status":I */
v3 = com.android.server.content.SyncManagerAccountChangePolicy .isBatteryCharging ( v2 );
} // .end method
public static Boolean isBatteryLow ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "status" # I */
/* .param p1, "level" # I */
/* .line 53 */
int v0 = 2; // const/4 v0, 0x2
/* if-eq p0, v0, :cond_0 */
/* const/16 v0, 0x14 */
/* if-gt p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean isBatteryLow ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 57 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
/* invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 58 */
/* .local v0, "ifilter":Landroid/content/IntentFilter; */
int v1 = 0; // const/4 v1, 0x0
(( android.content.Context ) p0 ).registerReceiver ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 60 */
/* .local v1, "batteryStatus":Landroid/content/Intent; */
/* const-string/jumbo v2, "status" */
int v3 = -1; // const/4 v3, -0x1
v2 = (( android.content.Intent ) v1 ).getIntExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 61 */
/* .local v2, "status":I */
final String v3 = "level"; // const-string v3, "level"
int v4 = 0; // const/4 v4, 0x0
v3 = (( android.content.Intent ) v1 ).getIntExtra ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 62 */
/* .local v3, "level":I */
v4 = com.android.server.content.SyncManagerAccountChangePolicy .isBatteryLow ( v2,v3 );
} // .end method
protected static Boolean isPackageNameForeground ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 26 */
final String v0 = "activity"; // const-string v0, "activity"
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 27 */
/* .local v0, "am":Landroid/app/ActivityManager; */
int v1 = 1; // const/4 v1, 0x1
(( android.app.ActivityManager ) v0 ).getRunningTasks ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;
/* .line 28 */
/* .local v1, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;" */
int v2 = 0; // const/4 v2, 0x0
v3 = if ( v1 != null) { // if-eqz v1, :cond_2
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 31 */
} // :cond_0
/* check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo; */
v3 = this.topActivity;
/* .line 32 */
/* .local v3, "topActivity":Landroid/content/ComponentName; */
/* if-nez v3, :cond_1 */
/* .line 33 */
/* .line 35 */
} // :cond_1
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 29 */
} // .end local v3 # "topActivity":Landroid/content/ComponentName;
} // :cond_2
} // :goto_0
} // .end method
