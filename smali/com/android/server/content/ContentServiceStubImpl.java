public class com.android.server.content.ContentServiceStubImpl extends com.android.server.content.ContentServiceStub {
	 /* .source "ContentServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.content.ContentServiceStub$$" */
} // .end annotation
/* # static fields */
private static final Integer MIUI_OBSERVERS_THRESHOLD;
private static final android.util.ArrayMap sObserverHistogram;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/Long;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.content.ContentServiceStubImpl ( ) {
/* .locals 1 */
/* .line 101 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
return;
} // .end method
public com.android.server.content.ContentServiceStubImpl ( ) {
/* .locals 0 */
/* .line 18 */
/* invoke-direct {p0}, Lcom/android/server/content/ContentServiceStub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void decreasePidObserverCount ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 121 */
v0 = com.android.server.content.ContentServiceStubImpl.sObserverHistogram;
/* monitor-enter v0 */
/* .line 122 */
/* int-to-long v1, p1 */
/* const/16 v3, 0x20 */
/* shl-long/2addr v1, v3 */
/* int-to-long v3, p2 */
/* or-long/2addr v1, v3 */
/* .line 123 */
/* .local v1, "uidPid":J */
try { // :try_start_0
java.lang.Long .valueOf ( v1,v2 );
int v4 = -1; // const/4 v4, -0x1
java.lang.Integer .valueOf ( v4 );
(( android.util.ArrayMap ) v0 ).getOrDefault ( v3, v5 ); // invoke-virtual {v0, v3, v5}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 124 */
/* .local v3, "prevCount":I */
/* if-ne v3, v4, :cond_0 */
/* monitor-exit v0 */
return;
/* .line 125 */
} // :cond_0
/* if-lez v3, :cond_1 */
/* .line 126 */
java.lang.Long .valueOf ( v1,v2 );
/* add-int/lit8 v5, v3, -0x1 */
java.lang.Integer .valueOf ( v5 );
(( android.util.ArrayMap ) v0 ).put ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 128 */
} // :cond_1
java.lang.Long .valueOf ( v1,v2 );
(( android.util.ArrayMap ) v0 ).remove ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 130 */
} // .end local v1 # "uidPid":J
} // .end local v3 # "prevCount":I
} // :goto_0
/* monitor-exit v0 */
/* .line 131 */
return;
/* .line 130 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void dumpObserverHistogram ( java.io.PrintWriter p0 ) {
/* .locals 10 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 135 */
final String v0 = "Observer histogram:"; // const-string v0, "Observer histogram:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 136 */
v0 = com.android.server.content.ContentServiceStubImpl.sObserverHistogram;
/* monitor-enter v0 */
/* .line 137 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
try { // :try_start_0
v2 = (( android.util.ArrayMap ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I
/* .local v2, "size":I */
} // :goto_0
/* if-ge v1, v2, :cond_0 */
/* .line 138 */
v3 = com.android.server.content.ContentServiceStubImpl.sObserverHistogram;
(( android.util.ArrayMap ) v3 ).keyAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Long; */
(( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* .line 139 */
/* .local v4, "key":J */
(( android.util.ArrayMap ) v3 ).valueAt ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 140 */
/* .local v3, "count":I */
/* const/16 v6, 0x20 */
/* shr-long v6, v4, v6 */
/* long-to-int v6, v6 */
/* .line 141 */
/* .local v6, "uid":I */
/* long-to-int v7, v4 */
/* .line 142 */
/* .local v7, "pid":I */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = " uid: "; // const-string v9, " uid: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " pid: "; // const-string v9, " pid: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " "; // const-string v9, " "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " observers"; // const-string v9, " observers"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v8 ); // invoke-virtual {p1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 137 */
} // .end local v3 # "count":I
} // .end local v4 # "key":J
} // .end local v6 # "uid":I
} // .end local v7 # "pid":I
/* add-int/lit8 v1, v1, 0x1 */
/* .line 144 */
} // .end local v1 # "i":I
} // .end local v2 # "size":I
} // :cond_0
/* monitor-exit v0 */
/* .line 145 */
return;
/* .line 144 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Long getMiSyncPauseToTime ( android.content.Context p0, com.android.server.content.ContentService p1, android.accounts.Account p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "contentService" # Lcom/android/server/content/ContentService; */
/* .param p3, "account" # Landroid/accounts/Account; */
/* .param p4, "uid" # I */
/* .line 43 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "no permission to read the sync settings for user "; // const-string v1, "no permission to read the sync settings for user "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.content.ContentService ) p2 ).enforceCrossUserPermissionForInjector ( p4, v0 ); // invoke-virtual {p2, p4, v0}, Lcom/android/server/content/ContentService;->enforceCrossUserPermissionForInjector(ILjava/lang/String;)V
/* .line 45 */
final String v0 = "android.permission.READ_SYNC_SETTINGS"; // const-string v0, "android.permission.READ_SYNC_SETTINGS"
final String v1 = "no permission to read the sync settings"; // const-string v1, "no permission to read the sync settings"
(( android.content.Context ) p1 ).enforceCallingOrSelfPermission ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 48 */
com.android.server.content.ContentService .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 50 */
/* .local v0, "identityToken":J */
try { // :try_start_0
(( com.android.server.content.ContentService ) p2 ).getSyncManagerForInjector ( ); // invoke-virtual {p2}, Lcom/android/server/content/ContentService;->getSyncManagerForInjector()Lcom/android/server/content/SyncManager;
/* .line 51 */
/* .local v2, "syncManager":Lcom/android/server/content/SyncManager; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 52 */
(( com.android.server.content.SyncManager ) v2 ).getSyncStorageEngine ( ); // invoke-virtual {v2}, Lcom/android/server/content/SyncManager;->getSyncStorageEngine()Lcom/android/server/content/SyncStorageEngine;
(( com.android.server.content.SyncStorageEngine ) v3 ).getMiSyncPauseToTime ( p3, p4 ); // invoke-virtual {v3, p3, p4}, Lcom/android/server/content/SyncStorageEngine;->getMiSyncPauseToTime(Landroid/accounts/Account;I)J
/* move-result-wide v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 55 */
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 52 */
/* return-wide v3 */
/* .line 55 */
} // .end local v2 # "syncManager":Lcom/android/server/content/SyncManager;
} // :cond_0
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 56 */
/* nop */
/* .line 57 */
/* const-wide/16 v2, 0x0 */
/* return-wide v2 */
/* .line 55 */
/* :catchall_0 */
/* move-exception v2 */
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 56 */
/* throw v2 */
} // .end method
public Integer getMiSyncStrategy ( android.content.Context p0, com.android.server.content.ContentService p1, android.accounts.Account p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "contentService" # Lcom/android/server/content/ContentService; */
/* .param p3, "account" # Landroid/accounts/Account; */
/* .param p4, "uid" # I */
/* .line 83 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "no permission to read the sync settings for user "; // const-string v1, "no permission to read the sync settings for user "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.content.ContentService ) p2 ).enforceCrossUserPermissionForInjector ( p4, v0 ); // invoke-virtual {p2, p4, v0}, Lcom/android/server/content/ContentService;->enforceCrossUserPermissionForInjector(ILjava/lang/String;)V
/* .line 85 */
final String v0 = "android.permission.READ_SYNC_SETTINGS"; // const-string v0, "android.permission.READ_SYNC_SETTINGS"
final String v1 = "no permission to read the sync settings"; // const-string v1, "no permission to read the sync settings"
(( android.content.Context ) p1 ).enforceCallingOrSelfPermission ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 88 */
com.android.server.content.ContentService .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 90 */
/* .local v0, "identityToken":J */
try { // :try_start_0
(( com.android.server.content.ContentService ) p2 ).getSyncManagerForInjector ( ); // invoke-virtual {p2}, Lcom/android/server/content/ContentService;->getSyncManagerForInjector()Lcom/android/server/content/SyncManager;
/* .line 91 */
/* .local v2, "syncManager":Lcom/android/server/content/SyncManager; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 92 */
(( com.android.server.content.SyncManager ) v2 ).getSyncStorageEngine ( ); // invoke-virtual {v2}, Lcom/android/server/content/SyncManager;->getSyncStorageEngine()Lcom/android/server/content/SyncStorageEngine;
v3 = (( com.android.server.content.SyncStorageEngine ) v3 ).getMiSyncStrategy ( p3, p4 ); // invoke-virtual {v3, p3, p4}, Lcom/android/server/content/SyncStorageEngine;->getMiSyncStrategy(Landroid/accounts/Account;I)I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 95 */
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 92 */
/* .line 95 */
} // .end local v2 # "syncManager":Lcom/android/server/content/SyncManager;
} // :cond_0
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 96 */
/* nop */
/* .line 97 */
int v2 = 0; // const/4 v2, 0x0
/* .line 95 */
/* :catchall_0 */
/* move-exception v2 */
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 96 */
/* throw v2 */
} // .end method
public void increasePidObserverCount ( Integer p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 106 */
v0 = com.android.server.content.ContentServiceStubImpl.sObserverHistogram;
/* monitor-enter v0 */
/* .line 107 */
/* int-to-long v1, p1 */
/* const/16 v3, 0x20 */
/* shl-long/2addr v1, v3 */
/* int-to-long v3, p2 */
/* or-long/2addr v1, v3 */
/* .line 108 */
/* .local v1, "uidPid":J */
try { // :try_start_0
java.lang.Long .valueOf ( v1,v2 );
int v4 = 0; // const/4 v4, 0x0
java.lang.Integer .valueOf ( v4 );
(( android.util.ArrayMap ) v0 ).getOrDefault ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* add-int/lit8 v3, v3, 0x1 */
/* .line 109 */
/* .local v3, "count":I */
java.lang.Long .valueOf ( v1,v2 );
java.lang.Integer .valueOf ( v3 );
(( android.util.ArrayMap ) v0 ).put ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 110 */
/* const/16 v4, 0x4e20 */
/* if-ne v3, v4, :cond_1 */
/* const/16 v4, 0x3e8 */
/* if-eq p1, v4, :cond_1 */
/* .line 112 */
v4 = android.os.Binder .getCallingPid ( );
v5 = android.os.Process .myPid ( );
/* if-ne v4, v5, :cond_0 */
/* .line 113 */
} // :cond_0
/* new-instance v4, Ljava/lang/SecurityException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "uid " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " pid "; // const-string v6, " pid "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " registered too many content observers"; // const-string v6, " registered too many content observers"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/content/ContentServiceStubImpl;
} // .end local p1 # "uid":I
} // .end local p2 # "pid":I
/* throw v4 */
/* .line 116 */
} // .end local v1 # "uidPid":J
} // .end local v3 # "count":I
/* .restart local p0 # "this":Lcom/android/server/content/ContentServiceStubImpl; */
/* .restart local p1 # "uid":I */
/* .restart local p2 # "pid":I */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
/* .line 117 */
return;
/* .line 116 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setMiSyncPauseToTime ( android.content.Context p0, com.android.server.content.ContentService p1, android.accounts.Account p2, Long p3, Integer p4 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "contentService" # Lcom/android/server/content/ContentService; */
/* .param p3, "account" # Landroid/accounts/Account; */
/* .param p4, "pauseTimeMillis" # J */
/* .param p6, "uid" # I */
/* .line 23 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "no permission to set the sync status for user "; // const-string v1, "no permission to set the sync status for user "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p6 ); // invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.content.ContentService ) p2 ).enforceCrossUserPermissionForInjector ( p6, v0 ); // invoke-virtual {p2, p6, v0}, Lcom/android/server/content/ContentService;->enforceCrossUserPermissionForInjector(ILjava/lang/String;)V
/* .line 25 */
final String v0 = "android.permission.WRITE_SYNC_SETTINGS"; // const-string v0, "android.permission.WRITE_SYNC_SETTINGS"
final String v1 = "no permission to write the sync settings"; // const-string v1, "no permission to write the sync settings"
(( android.content.Context ) p1 ).enforceCallingOrSelfPermission ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 28 */
com.android.server.content.ContentService .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 30 */
/* .local v0, "identityToken":J */
try { // :try_start_0
(( com.android.server.content.ContentService ) p2 ).getSyncManagerForInjector ( ); // invoke-virtual {p2}, Lcom/android/server/content/ContentService;->getSyncManagerForInjector()Lcom/android/server/content/SyncManager;
/* .line 31 */
/* .local v2, "syncManager":Lcom/android/server/content/SyncManager; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 32 */
(( com.android.server.content.SyncManager ) v2 ).getSyncStorageEngine ( ); // invoke-virtual {v2}, Lcom/android/server/content/SyncManager;->getSyncStorageEngine()Lcom/android/server/content/SyncStorageEngine;
(( com.android.server.content.SyncStorageEngine ) v3 ).setMiSyncPauseToTime ( p3, p4, p5, p6 ); // invoke-virtual {v3, p3, p4, p5, p6}, Lcom/android/server/content/SyncStorageEngine;->setMiSyncPauseToTime(Landroid/accounts/Account;JI)V
/* .line 33 */
com.android.server.content.SyncManagerStubImpl .handleSyncPauseChanged ( p1,v2,p4,p5 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 36 */
} // .end local v2 # "syncManager":Lcom/android/server/content/SyncManager;
} // :cond_0
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 37 */
/* nop */
/* .line 38 */
return;
/* .line 36 */
/* :catchall_0 */
/* move-exception v2 */
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 37 */
/* throw v2 */
} // .end method
public void setMiSyncStrategy ( android.content.Context p0, com.android.server.content.ContentService p1, android.accounts.Account p2, Integer p3, Integer p4 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "contentService" # Lcom/android/server/content/ContentService; */
/* .param p3, "account" # Landroid/accounts/Account; */
/* .param p4, "strategy" # I */
/* .param p5, "uid" # I */
/* .line 63 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "no permission to set the sync status for user "; // const-string v1, "no permission to set the sync status for user "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.content.ContentService ) p2 ).enforceCrossUserPermissionForInjector ( p5, v0 ); // invoke-virtual {p2, p5, v0}, Lcom/android/server/content/ContentService;->enforceCrossUserPermissionForInjector(ILjava/lang/String;)V
/* .line 65 */
final String v0 = "android.permission.WRITE_SYNC_SETTINGS"; // const-string v0, "android.permission.WRITE_SYNC_SETTINGS"
final String v1 = "no permission to write the sync settings"; // const-string v1, "no permission to write the sync settings"
(( android.content.Context ) p1 ).enforceCallingOrSelfPermission ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 68 */
com.android.server.content.ContentService .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 70 */
/* .local v0, "identityToken":J */
try { // :try_start_0
(( com.android.server.content.ContentService ) p2 ).getSyncManagerForInjector ( ); // invoke-virtual {p2}, Lcom/android/server/content/ContentService;->getSyncManagerForInjector()Lcom/android/server/content/SyncManager;
/* .line 71 */
/* .local v2, "syncManager":Lcom/android/server/content/SyncManager; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 72 */
(( com.android.server.content.SyncManager ) v2 ).getSyncStorageEngine ( ); // invoke-virtual {v2}, Lcom/android/server/content/SyncManager;->getSyncStorageEngine()Lcom/android/server/content/SyncStorageEngine;
(( com.android.server.content.SyncStorageEngine ) v3 ).setMiSyncStrategy ( p3, p4, p5 ); // invoke-virtual {v3, p3, p4, p5}, Lcom/android/server/content/SyncStorageEngine;->setMiSyncStrategy(Landroid/accounts/Account;II)V
/* .line 73 */
com.android.server.content.SyncManagerStubImpl .handleSyncStrategyChanged ( p1,v2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 76 */
} // .end local v2 # "syncManager":Lcom/android/server/content/SyncManager;
} // :cond_0
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 77 */
/* nop */
/* .line 78 */
return;
/* .line 76 */
/* :catchall_0 */
/* move-exception v2 */
com.android.server.content.ContentService .restoreCallingIdentity ( v0,v1 );
/* .line 77 */
/* throw v2 */
} // .end method
