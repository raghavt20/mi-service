.class public Lcom/android/server/content/MiSyncStrategyImpl;
.super Ljava/lang/Object;
.source "MiSyncStrategyImpl.java"

# interfaces
.implements Lcom/android/server/content/MiSyncStrategy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;,
        Lcom/android/server/content/MiSyncStrategyImpl$OfficialStrategy;,
        Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;
    }
.end annotation


# static fields
.field public static final CLEVER_MJ_STRATEGY:I = 0x1

.field public static final DEFAULT_STRATEGY:I = 0x1

.field public static final OFFICIAL_STRATEGY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Sync"

.field private static final VERSION:I = 0x1

.field private static final XML_ATTR_ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field private static final XML_ATTR_STRATEGY:Ljava/lang/String; = "strategy"

.field private static final XML_ATTR_UID:Ljava/lang/String; = "uid"

.field private static final XML_ATTR_VERSION:Ljava/lang/String; = "version"

.field public static final XML_FILE_NAME:Ljava/lang/String; = "mi_strategy"

.field public static final XML_FILE_VERSION:I = 0x1

.field private static final XML_TAG_ITEM:Ljava/lang/String; = "sync_strategy_item"


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private mStrategy:I

.field private mUid:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "accountName"    # Ljava/lang/String;

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I

    .line 145
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mCache:Landroid/util/SparseArray;

    .line 49
    iput p1, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mUid:I

    .line 50
    iput-object p2, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mAccountName:Ljava/lang/String;

    .line 51
    return-void
.end method

.method private getSyncStrategyInternal(I)Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;
    .locals 3
    .param p1, "strategy"    # I

    .line 148
    iget-object v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mCache:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mCache:Landroid/util/SparseArray;

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mCache:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;

    .line 152
    .local v0, "syncStrategy":Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;
    if-eqz v0, :cond_1

    .line 153
    return-object v0

    .line 155
    :cond_1
    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    .line 163
    new-instance v2, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;

    invoke-direct {v2, v1}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;-><init>(Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy-IA;)V

    move-object v0, v2

    goto :goto_0

    .line 160
    :pswitch_0
    new-instance v2, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;

    invoke-direct {v2, v1}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;-><init>(Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy-IA;)V

    move-object v0, v2

    .line 161
    goto :goto_0

    .line 157
    :pswitch_1
    new-instance v2, Lcom/android/server/content/MiSyncStrategyImpl$OfficialStrategy;

    invoke-direct {v2, v1}, Lcom/android/server/content/MiSyncStrategyImpl$OfficialStrategy;-><init>(Lcom/android/server/content/MiSyncStrategyImpl$OfficialStrategy-IA;)V

    move-object v0, v2

    .line 158
    nop

    .line 166
    :goto_0
    iget-object v1, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mCache:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 167
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static readFromXML(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/server/content/MiSyncStrategyImpl;
    .locals 12
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;

    .line 92
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "tagName":Ljava/lang/String;
    const-string/jumbo v1, "sync_strategy_item"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 94
    return-object v2

    .line 97
    :cond_0
    const-string/jumbo v1, "version"

    invoke-interface {p0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "itemVersionString":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v4, "Sync"

    if-eqz v3, :cond_1

    .line 99
    const-string/jumbo v3, "the version in mi strategy is null"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    return-object v2

    .line 102
    :cond_1
    const/4 v3, 0x0

    .line 104
    .local v3, "itemVersion":I
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move v3, v5

    .line 108
    nop

    .line 109
    const/4 v5, 0x1

    if-lt v3, v5, :cond_4

    .line 110
    const-string/jumbo v5, "uid"

    invoke-interface {p0, v2, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 111
    .local v5, "uidString":Ljava/lang/String;
    const-string v6, "account_name"

    invoke-interface {p0, v2, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 112
    .local v6, "accountName":Ljava/lang/String;
    const-string/jumbo v7, "strategy"

    invoke-interface {p0, v2, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 114
    .local v7, "strategyString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 115
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 116
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_0

    .line 119
    :cond_2
    const/4 v8, 0x0

    .line 120
    .local v8, "uid":I
    const/4 v9, 0x1

    .line 122
    .local v9, "strategy":I
    :try_start_1
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    move v8, v10

    .line 123
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 127
    .end local v9    # "strategy":I
    .local v2, "strategy":I
    nop

    .line 128
    new-instance v4, Lcom/android/server/content/MiSyncStrategyImpl;

    invoke-direct {v4, v8, v6}, Lcom/android/server/content/MiSyncStrategyImpl;-><init>(ILjava/lang/String;)V

    .line 129
    .local v4, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl;
    invoke-virtual {v4, v2}, Lcom/android/server/content/MiSyncStrategyImpl;->setStrategy(I)Z

    .line 130
    return-object v4

    .line 124
    .end local v2    # "strategy":I
    .end local v4    # "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl;
    .restart local v9    # "strategy":I
    :catch_0
    move-exception v10

    .line 125
    .local v10, "e":Ljava/lang/NumberFormatException;
    const-string v11, "error parsing item for mi strategy"

    invoke-static {v4, v11, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 126
    return-object v2

    .line 117
    .end local v8    # "uid":I
    .end local v9    # "strategy":I
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    :goto_0
    return-object v2

    .line 132
    .end local v5    # "uidString":Ljava/lang/String;
    .end local v6    # "accountName":Ljava/lang/String;
    .end local v7    # "strategyString":Ljava/lang/String;
    :cond_4
    return-object v2

    .line 105
    :catch_1
    move-exception v5

    .line 106
    .local v5, "e":Ljava/lang/NumberFormatException;
    const-string v6, "error parsing version for mi strategy"

    invoke-static {v4, v6, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 107
    return-object v2
.end method


# virtual methods
.method public apply(Lcom/android/server/content/SyncOperation;Landroid/os/Bundle;Landroid/app/job/JobInfo$Builder;)V
    .locals 1
    .param p1, "syncOperation"    # Lcom/android/server/content/SyncOperation;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "builder"    # Landroid/app/job/JobInfo$Builder;

    .line 137
    iget v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I

    invoke-direct {p0, v0}, Lcom/android/server/content/MiSyncStrategyImpl;->getSyncStrategyInternal(I)Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;->apply(Lcom/android/server/content/SyncOperation;Landroid/os/Bundle;Landroid/app/job/JobInfo$Builder;)V

    .line 138
    return-void
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getStrategy()I
    .locals 1

    .line 78
    iget v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I

    return v0
.end method

.method public getUid()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mUid:I

    return v0
.end method

.method public isAllowedToRun(Lcom/android/server/content/SyncOperation;Landroid/os/Bundle;)Z
    .locals 1
    .param p1, "syncOperation"    # Lcom/android/server/content/SyncOperation;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 142
    iget v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I

    invoke-direct {p0, v0}, Lcom/android/server/content/MiSyncStrategyImpl;->getSyncStrategyInternal(I)Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;->isAllowedToRun(Lcom/android/server/content/SyncOperation;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public setStrategy(I)Z
    .locals 2
    .param p1, "strategy"    # I

    .line 65
    const/4 v0, 0x1

    if-eqz p1, :cond_2

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 70
    :cond_0
    const/4 v0, 0x3

    const-string v1, "Sync"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    const-string v0, "Illegal strategy"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_1
    const/4 v0, 0x0

    return v0

    .line 67
    :cond_2
    :goto_0
    iput p1, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I

    .line 68
    return v0
.end method

.method public writeToXML(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 4
    .param p1, "out"    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 83
    const/4 v0, 0x0

    const-string/jumbo v1, "sync_strategy_item"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 84
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "version"

    invoke-interface {p1, v0, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 85
    iget v2, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mUid:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "uid"

    invoke-interface {p1, v0, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 86
    const-string v2, "account_name"

    iget-object v3, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mAccountName:Ljava/lang/String;

    invoke-interface {p1, v0, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 87
    iget v2, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "strategy"

    invoke-interface {p1, v0, v3, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 88
    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 89
    return-void
.end method
