class com.android.server.content.SyncManagerAccountChangePolicy$DefaultSyncDataStrategy implements com.android.server.content.SyncManagerAccountChangePolicy$SyncForbiddenStrategy {
	 /* .source "SyncManagerAccountChangePolicy.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/content/SyncManagerAccountChangePolicy; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "DefaultSyncDataStrategy" */
} // .end annotation
/* # direct methods */
private com.android.server.content.SyncManagerAccountChangePolicy$DefaultSyncDataStrategy ( ) {
/* .locals 0 */
/* .line 109 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.content.SyncManagerAccountChangePolicy$DefaultSyncDataStrategy ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/content/SyncManagerAccountChangePolicy$DefaultSyncDataStrategy;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean isSyncForbidden ( android.content.Context p0, java.lang.String p1, android.os.Bundle p2 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "authority" # Ljava/lang/String; */
/* .param p3, "extras" # Landroid/os/Bundle; */
/* .line 112 */
final String v0 = "num_syncs"; // const-string v0, "num_syncs"
int v1 = -1; // const/4 v1, -0x1
v0 = (( android.os.Bundle ) p3 ).getInt ( v0, v1 ); // invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
/* .line 114 */
/* .local v0, "num":I */
final String v1 = "com.miui.browser"; // const-string v1, "com.miui.browser"
v1 = (( java.lang.String ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 115 */
	 /* if-ltz v0, :cond_1 */
	 /* const/16 v1, 0x8 */
	 /* if-ge v0, v1, :cond_1 */
	 /* .line 116 */
	 /* .line 119 */
} // :cond_0
/* if-ltz v0, :cond_1 */
int v1 = 3; // const/4 v1, 0x3
/* if-ge v0, v1, :cond_1 */
/* .line 120 */
/* .line 124 */
} // :cond_1
final String v1 = "interactive"; // const-string v1, "interactive"
v1 = (( android.os.Bundle ) p3 ).getBoolean ( v1, v2 ); // invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
/* .line 125 */
/* .local v1, "isInteractive":Z */
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 126 */
/* .line 128 */
} // :cond_2
final String v4 = "last_screen_off_time"; // const-string v4, "last_screen_off_time"
/* const-wide/16 v5, 0x0 */
(( android.os.Bundle ) p3 ).getLong ( v4, v5, v6 ); // invoke-virtual {p3, v4, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J
/* move-result-wide v4 */
/* .line 129 */
/* .local v4, "lastScreenOffTime":J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
/* sub-long/2addr v6, v4 */
/* const-wide/32 v8, 0x1d4c0 */
/* cmp-long v6, v6, v8 */
/* if-gez v6, :cond_3 */
/* .line 130 */
/* .line 132 */
} // :cond_3
final String v6 = "battery_charging"; // const-string v6, "battery_charging"
v6 = (( android.os.Bundle ) p3 ).getBoolean ( v6, v2 ); // invoke-virtual {p3, v6, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
/* .line 133 */
/* .local v6, "isBatteryCharging":Z */
/* if-nez v6, :cond_4 */
/* .line 134 */
/* .line 136 */
} // :cond_4
} // .end method
