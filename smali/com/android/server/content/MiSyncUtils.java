public class com.android.server.content.MiSyncUtils {
	 /* .source "MiSyncUtils.java" */
	 /* # static fields */
	 private static final Integer HIGH_PARALLEL_SYNC_NUM;
	 private static final java.util.HashSet LOW_PARALLEL_SYNC_DEVICES;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashSet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final Integer LOW_PARALLEL_SYNC_NUM;
private static final java.lang.String TAG;
private static final Integer XIAOMI_MAX_PARALLEL_SYNC_NUM;
/* # direct methods */
static com.android.server.content.MiSyncUtils ( ) {
/* .locals 3 */
/* .line 21 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 24 */
final String v1 = "onc"; // const-string v1, "onc"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 25 */
final String v1 = "pine"; // const-string v1, "pine"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 26 */
/* const-string/jumbo v1, "ugg" */
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 27 */
final String v1 = "cactus"; // const-string v1, "cactus"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 28 */
final String v1 = "cereus"; // const-string v1, "cereus"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 29 */
final String v1 = "santoni"; // const-string v1, "santoni"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 30 */
final String v1 = "riva"; // const-string v1, "riva"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 31 */
final String v1 = "rosy"; // const-string v1, "rosy"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 32 */
final String v1 = "rolex"; // const-string v1, "rolex"
(( java.util.HashSet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 34 */
v1 = android.os.Build.DEVICE;
(( java.lang.String ) v1 ).toLowerCase ( ); // invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
/* .line 35 */
/* .local v1, "device":Ljava/lang/String; */
v0 = (( java.util.HashSet ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 36 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 38 */
} // :cond_0
/* const v0, 0x7fffffff */
/* .line 40 */
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Max parallel sync number is "; // const-string v2, "Max parallel sync number is "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiSyncUtils"; // const-string v2, "MiSyncUtils"
android.util.Log .i ( v2,v0 );
/* .line 41 */
} // .end local v1 # "device":Ljava/lang/String;
return;
} // .end method
public com.android.server.content.MiSyncUtils ( ) {
/* .locals 0 */
/* .line 14 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static Boolean checkAccount ( android.accounts.Account p0 ) {
/* .locals 3 */
/* .param p0, "account" # Landroid/accounts/Account; */
/* .line 135 */
int v0 = 3; // const/4 v0, 0x3
final String v1 = "MiSyncUtils"; // const-string v1, "MiSyncUtils"
/* if-nez p0, :cond_1 */
/* .line 136 */
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 137 */
final String v0 = "injector: checkAccount: false"; // const-string v0, "injector: checkAccount: false"
android.util.Log .d ( v1,v0 );
/* .line 139 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 141 */
} // :cond_1
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 142 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "injector: checkAccount: "; // const-string v2, "injector: checkAccount: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.type;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 144 */
} // :cond_2
final String v0 = "com.xiaomi"; // const-string v0, "com.xiaomi"
v1 = this.type;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
static Boolean checkSyncOperationAccount ( com.android.server.content.SyncOperation p0 ) {
/* .locals 4 */
/* .param p0, "syncOperation" # Lcom/android/server/content/SyncOperation; */
/* .line 84 */
int v0 = 3; // const/4 v0, 0x3
final String v1 = "MiSyncUtils"; // const-string v1, "MiSyncUtils"
if ( p0 != null) { // if-eqz p0, :cond_2
v2 = this.target;
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.target;
v2 = this.account;
/* if-nez v2, :cond_0 */
/* .line 92 */
} // :cond_0
v2 = this.target;
v2 = this.account;
/* .line 93 */
/* .local v2, "account":Landroid/accounts/Account; */
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 94 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "injector: checkSyncOperationAccount: "; // const-string v3, "injector: checkSyncOperationAccount: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.type;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v0 );
/* .line 96 */
} // :cond_1
final String v0 = "com.xiaomi"; // const-string v0, "com.xiaomi"
v1 = this.type;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 86 */
} // .end local v2 # "account":Landroid/accounts/Account;
} // :cond_2
} // :goto_0
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 87 */
final String v0 = "injector: checkSyncOperationAccount: false"; // const-string v0, "injector: checkSyncOperationAccount: false"
android.util.Log .d ( v1,v0 );
/* .line 89 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // .end method
static Boolean checkSyncOperationPass ( com.android.server.content.SyncOperation p0 ) {
/* .locals 6 */
/* .param p0, "syncOperation" # Lcom/android/server/content/SyncOperation; */
/* .line 101 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 3; // const/4 v1, 0x3
final String v2 = "MiSyncUtils"; // const-string v2, "MiSyncUtils"
/* if-nez p0, :cond_1 */
/* .line 102 */
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 103 */
final String v1 = "injector: checkSyncOperationPass: null parameter, fail"; // const-string v1, "injector: checkSyncOperationPass: null parameter, fail"
android.util.Log .d ( v2,v1 );
/* .line 105 */
} // :cond_0
/* .line 109 */
} // :cond_1
v3 = (( com.android.server.content.SyncOperation ) p0 ).isInitialization ( ); // invoke-virtual {p0}, Lcom/android/server/content/SyncOperation;->isInitialization()Z
int v4 = 1; // const/4 v4, 0x1
/* if-nez v3, :cond_6 */
/* .line 110 */
v3 = (( com.android.server.content.SyncOperation ) p0 ).isManual ( ); // invoke-virtual {p0}, Lcom/android/server/content/SyncOperation;->isManual()Z
/* if-nez v3, :cond_6 */
/* .line 111 */
v3 = (( com.android.server.content.SyncOperation ) p0 ).isIgnoreSettings ( ); // invoke-virtual {p0}, Lcom/android/server/content/SyncOperation;->isIgnoreSettings()Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 119 */
} // :cond_2
/* iget v3, p0, Lcom/android/server/content/SyncOperation;->reason:I */
int v5 = -6; // const/4 v5, -0x6
/* if-ne v3, v5, :cond_4 */
/* .line 120 */
v0 = android.util.Log .isLoggable ( v2,v1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 121 */
final String v0 = "injector: checkSyncOperationPass: sync for auto, pass"; // const-string v0, "injector: checkSyncOperationPass: sync for auto, pass"
android.util.Log .d ( v2,v0 );
/* .line 123 */
} // :cond_3
/* .line 127 */
} // :cond_4
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 128 */
final String v1 = "injector: checkSyncOperationPass: fail"; // const-string v1, "injector: checkSyncOperationPass: fail"
android.util.Log .d ( v2,v1 );
/* .line 130 */
} // :cond_5
/* .line 112 */
} // :cond_6
} // :goto_0
v0 = android.util.Log .isLoggable ( v2,v1 );
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 113 */
final String v0 = "injector: checkSyncOperationPass: init or ignore settings, pass"; // const-string v0, "injector: checkSyncOperationPass: init or ignore settings, pass"
android.util.Log .d ( v2,v0 );
/* .line 115 */
} // :cond_7
} // .end method
static Boolean isSyncRoomForbiddenH ( com.android.server.content.SyncOperation p0, com.android.server.content.SyncManager p1 ) {
/* .locals 6 */
/* .param p0, "op" # Lcom/android/server/content/SyncOperation; */
/* .param p1, "syncManager" # Lcom/android/server/content/SyncManager; */
/* .line 45 */
int v0 = 3; // const/4 v0, 0x3
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MiSyncUtils"; // const-string v2, "MiSyncUtils"
if ( p0 != null) { // if-eqz p0, :cond_9
/* if-nez p1, :cond_0 */
/* .line 53 */
} // :cond_0
v3 = com.android.server.content.MiSyncUtils .checkSyncOperationAccount ( p0 );
/* if-nez v3, :cond_2 */
/* .line 54 */
v0 = android.util.Log .isLoggable ( v2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 55 */
final String v0 = "injector: isSyncRoomAvailable: not xiaomi account, false"; // const-string v0, "injector: isSyncRoomAvailable: not xiaomi account, false"
android.util.Log .d ( v2,v0 );
/* .line 57 */
} // :cond_1
/* .line 61 */
} // :cond_2
v3 = com.android.server.content.MiSyncUtils .checkSyncOperationPass ( p0 );
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 62 */
v0 = android.util.Log .isLoggable ( v2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 63 */
final String v0 = "injector: isSyncRoomAvailable: sync operation pass, false"; // const-string v0, "injector: isSyncRoomAvailable: sync operation pass, false"
android.util.Log .d ( v2,v0 );
/* .line 65 */
} // :cond_3
/* .line 68 */
} // :cond_4
v0 = android.os.Build.DEVICE;
(( java.lang.String ) v0 ).toLowerCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
/* .line 69 */
/* .local v0, "device":Ljava/lang/String; */
final String v2 = "dipper"; // const-string v2, "dipper"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 70 */
/* .line 73 */
} // :cond_5
int v2 = 0; // const/4 v2, 0x0
/* .line 74 */
/* .local v2, "count":I */
v3 = this.mActiveSyncContexts;
(( java.util.concurrent.CopyOnWriteArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_7
/* check-cast v4, Lcom/android/server/content/SyncManager$ActiveSyncContext; */
/* .line 75 */
/* .local v4, "activeSyncContext":Lcom/android/server/content/SyncManager$ActiveSyncContext; */
v5 = this.mSyncOperation;
v5 = com.android.server.content.MiSyncUtils .checkSyncOperationAccount ( v5 );
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 76 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 78 */
} // .end local v4 # "activeSyncContext":Lcom/android/server/content/SyncManager$ActiveSyncContext;
} // :cond_6
/* .line 79 */
} // :cond_7
/* if-lt v2, v3, :cond_8 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_8
/* .line 46 */
} // .end local v0 # "device":Ljava/lang/String;
} // .end local v2 # "count":I
} // :cond_9
} // :goto_1
v0 = android.util.Log .isLoggable ( v2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 47 */
final String v0 = "injector: isSyncRoomAvailable: null parameter, false"; // const-string v0, "injector: isSyncRoomAvailable: null parameter, false"
android.util.Log .d ( v2,v0 );
/* .line 49 */
} // :cond_a
} // .end method
