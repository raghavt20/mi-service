public class com.android.server.content.MiSyncStrategyImpl implements com.android.server.content.MiSyncStrategy {
	 /* .source "MiSyncStrategyImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;, */
	 /* Lcom/android/server/content/MiSyncStrategyImpl$OfficialStrategy;, */
	 /* Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer CLEVER_MJ_STRATEGY;
public static final Integer DEFAULT_STRATEGY;
public static final Integer OFFICIAL_STRATEGY;
private static final java.lang.String TAG;
private static final Integer VERSION;
private static final java.lang.String XML_ATTR_ACCOUNT_NAME;
private static final java.lang.String XML_ATTR_STRATEGY;
private static final java.lang.String XML_ATTR_UID;
private static final java.lang.String XML_ATTR_VERSION;
public static final java.lang.String XML_FILE_NAME;
public static final Integer XML_FILE_VERSION;
private static final java.lang.String XML_TAG_ITEM;
/* # instance fields */
private java.lang.String mAccountName;
private android.util.SparseArray mCache;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mStrategy;
private Integer mUid;
/* # direct methods */
public com.android.server.content.MiSyncStrategyImpl ( ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "accountName" # Ljava/lang/String; */
/* .line 48 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 46 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I */
/* .line 145 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mCache = v0;
/* .line 49 */
/* iput p1, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mUid:I */
/* .line 50 */
this.mAccountName = p2;
/* .line 51 */
return;
} // .end method
private com.android.server.content.MiSyncStrategyImpl$ISyncStrategy getSyncStrategyInternal ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "strategy" # I */
/* .line 148 */
v0 = this.mCache;
/* if-nez v0, :cond_0 */
/* .line 149 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mCache = v0;
/* .line 151 */
} // :cond_0
v0 = this.mCache;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy; */
/* .line 152 */
/* .local v0, "syncStrategy":Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 153 */
/* .line 155 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* packed-switch p1, :pswitch_data_0 */
/* .line 163 */
/* new-instance v2, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy; */
/* invoke-direct {v2, v1}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;-><init>(Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy-IA;)V */
/* move-object v0, v2 */
/* .line 160 */
/* :pswitch_0 */
/* new-instance v2, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy; */
/* invoke-direct {v2, v1}, Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy;-><init>(Lcom/android/server/content/MiSyncStrategyImpl$CleverMJStrategy-IA;)V */
/* move-object v0, v2 */
/* .line 161 */
/* .line 157 */
/* :pswitch_1 */
/* new-instance v2, Lcom/android/server/content/MiSyncStrategyImpl$OfficialStrategy; */
/* invoke-direct {v2, v1}, Lcom/android/server/content/MiSyncStrategyImpl$OfficialStrategy;-><init>(Lcom/android/server/content/MiSyncStrategyImpl$OfficialStrategy-IA;)V */
/* move-object v0, v2 */
/* .line 158 */
/* nop */
/* .line 166 */
} // :goto_0
v1 = this.mCache;
(( android.util.SparseArray ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 167 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static com.android.server.content.MiSyncStrategyImpl readFromXML ( org.xmlpull.v1.XmlPullParser p0 ) {
/* .locals 12 */
/* .param p0, "parser" # Lorg/xmlpull/v1/XmlPullParser; */
/* .line 92 */
/* .line 93 */
/* .local v0, "tagName":Ljava/lang/String; */
/* const-string/jumbo v1, "sync_strategy_item" */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 94 */
/* .line 97 */
} // :cond_0
/* const-string/jumbo v1, "version" */
/* .line 98 */
/* .local v1, "itemVersionString":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v1 );
final String v4 = "Sync"; // const-string v4, "Sync"
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 99 */
/* const-string/jumbo v3, "the version in mi strategy is null" */
android.util.Slog .e ( v4,v3 );
/* .line 100 */
/* .line 102 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .line 104 */
/* .local v3, "itemVersion":I */
try { // :try_start_0
v5 = java.lang.Integer .parseInt ( v1 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_1 */
/* move v3, v5 */
/* .line 108 */
/* nop */
/* .line 109 */
int v5 = 1; // const/4 v5, 0x1
/* if-lt v3, v5, :cond_4 */
/* .line 110 */
/* const-string/jumbo v5, "uid" */
/* .line 111 */
/* .local v5, "uidString":Ljava/lang/String; */
final String v6 = "account_name"; // const-string v6, "account_name"
/* .line 112 */
/* .local v6, "accountName":Ljava/lang/String; */
/* const-string/jumbo v7, "strategy" */
/* .line 114 */
/* .local v7, "strategyString":Ljava/lang/String; */
v8 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v8, :cond_3 */
/* .line 115 */
v8 = android.text.TextUtils .isEmpty ( v6 );
/* if-nez v8, :cond_3 */
/* .line 116 */
v8 = android.text.TextUtils .isEmpty ( v7 );
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 119 */
} // :cond_2
int v8 = 0; // const/4 v8, 0x0
/* .line 120 */
/* .local v8, "uid":I */
int v9 = 1; // const/4 v9, 0x1
/* .line 122 */
/* .local v9, "strategy":I */
try { // :try_start_1
v10 = java.lang.Integer .parseInt ( v5 );
/* move v8, v10 */
/* .line 123 */
v2 = java.lang.Integer .parseInt ( v7 );
/* :try_end_1 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 127 */
} // .end local v9 # "strategy":I
/* .local v2, "strategy":I */
/* nop */
/* .line 128 */
/* new-instance v4, Lcom/android/server/content/MiSyncStrategyImpl; */
/* invoke-direct {v4, v8, v6}, Lcom/android/server/content/MiSyncStrategyImpl;-><init>(ILjava/lang/String;)V */
/* .line 129 */
/* .local v4, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl; */
(( com.android.server.content.MiSyncStrategyImpl ) v4 ).setStrategy ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/content/MiSyncStrategyImpl;->setStrategy(I)Z
/* .line 130 */
/* .line 124 */
} // .end local v2 # "strategy":I
} // .end local v4 # "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl;
/* .restart local v9 # "strategy":I */
/* :catch_0 */
/* move-exception v10 */
/* .line 125 */
/* .local v10, "e":Ljava/lang/NumberFormatException; */
final String v11 = "error parsing item for mi strategy"; // const-string v11, "error parsing item for mi strategy"
android.util.Slog .e ( v4,v11,v10 );
/* .line 126 */
/* .line 117 */
} // .end local v8 # "uid":I
} // .end local v9 # "strategy":I
} // .end local v10 # "e":Ljava/lang/NumberFormatException;
} // :cond_3
} // :goto_0
/* .line 132 */
} // .end local v5 # "uidString":Ljava/lang/String;
} // .end local v6 # "accountName":Ljava/lang/String;
} // .end local v7 # "strategyString":Ljava/lang/String;
} // :cond_4
/* .line 105 */
/* :catch_1 */
/* move-exception v5 */
/* .line 106 */
/* .local v5, "e":Ljava/lang/NumberFormatException; */
final String v6 = "error parsing version for mi strategy"; // const-string v6, "error parsing version for mi strategy"
android.util.Slog .e ( v4,v6,v5 );
/* .line 107 */
} // .end method
/* # virtual methods */
public void apply ( com.android.server.content.SyncOperation p0, android.os.Bundle p1, android.app.job.JobInfo$Builder p2 ) {
/* .locals 1 */
/* .param p1, "syncOperation" # Lcom/android/server/content/SyncOperation; */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .param p3, "builder" # Landroid/app/job/JobInfo$Builder; */
/* .line 137 */
/* iget v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I */
/* invoke-direct {p0, v0}, Lcom/android/server/content/MiSyncStrategyImpl;->getSyncStrategyInternal(I)Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy; */
/* .line 138 */
return;
} // .end method
public java.lang.String getAccountName ( ) {
/* .locals 1 */
/* .line 60 */
v0 = this.mAccountName;
} // .end method
public Integer getStrategy ( ) {
/* .locals 1 */
/* .line 78 */
/* iget v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I */
} // .end method
public Integer getUid ( ) {
/* .locals 1 */
/* .line 55 */
/* iget v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mUid:I */
} // .end method
public Boolean isAllowedToRun ( com.android.server.content.SyncOperation p0, android.os.Bundle p1 ) {
/* .locals 1 */
/* .param p1, "syncOperation" # Lcom/android/server/content/SyncOperation; */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .line 142 */
/* iget v0, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/content/MiSyncStrategyImpl;->getSyncStrategyInternal(I)Lcom/android/server/content/MiSyncStrategyImpl$ISyncStrategy; */
} // .end method
public Boolean setStrategy ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "strategy" # I */
/* .line 65 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_2
/* if-ne p1, v0, :cond_0 */
/* .line 70 */
} // :cond_0
int v0 = 3; // const/4 v0, 0x3
final String v1 = "Sync"; // const-string v1, "Sync"
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 71 */
final String v0 = "Illegal strategy"; // const-string v0, "Illegal strategy"
android.util.Log .d ( v1,v0 );
/* .line 73 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 67 */
} // :cond_2
} // :goto_0
/* iput p1, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I */
/* .line 68 */
} // .end method
public void writeToXML ( org.xmlpull.v1.XmlSerializer p0 ) {
/* .locals 4 */
/* .param p1, "out" # Lorg/xmlpull/v1/XmlSerializer; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 83 */
int v0 = 0; // const/4 v0, 0x0
/* const-string/jumbo v1, "sync_strategy_item" */
/* .line 84 */
int v2 = 1; // const/4 v2, 0x1
java.lang.Integer .toString ( v2 );
/* const-string/jumbo v3, "version" */
/* .line 85 */
/* iget v2, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mUid:I */
java.lang.Integer .toString ( v2 );
/* const-string/jumbo v3, "uid" */
/* .line 86 */
final String v2 = "account_name"; // const-string v2, "account_name"
v3 = this.mAccountName;
/* .line 87 */
/* iget v2, p0, Lcom/android/server/content/MiSyncStrategyImpl;->mStrategy:I */
java.lang.Integer .toString ( v2 );
/* const-string/jumbo v3, "strategy" */
/* .line 88 */
/* .line 89 */
return;
} // .end method
