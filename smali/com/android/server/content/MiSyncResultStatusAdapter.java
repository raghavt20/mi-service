public class com.android.server.content.MiSyncResultStatusAdapter {
	 /* .source "MiSyncResultStatusAdapter.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.content.MiSyncResultStatusAdapter ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void updateResultStatus ( android.content.SyncStatusInfo p0, java.lang.String p1, android.content.SyncResult p2 ) {
		 /* .locals 3 */
		 /* .param p0, "syncStatusInfo" # Landroid/content/SyncStatusInfo; */
		 /* .param p1, "lastSyncMessage" # Ljava/lang/String; */
		 /* .param p2, "syncResult" # Landroid/content/SyncResult; */
		 /* .line 19 */
		 final String v0 = "canceled"; // const-string v0, "canceled"
		 v0 = 		 android.text.TextUtils .equals ( p1,v0 );
		 int v1 = 3; // const/4 v1, 0x3
		 final String v2 = "SyncManager"; // const-string v2, "SyncManager"
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 20 */
			 v0 = 			 android.util.Log .isLoggable ( v2,v1 );
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 21 */
				 /* const-string/jumbo v0, "updateResultStatus: lastSyncMessage is canceled, do nothing" */
				 android.util.Log .d ( v2,v0 );
				 /* .line 23 */
			 } // :cond_0
			 return;
			 /* .line 26 */
		 } // :cond_1
		 v0 = this.miSyncStatusInfo;
		 this.lastResultMessage = p1;
		 /* .line 27 */
		 if ( p2 != null) { // if-eqz p2, :cond_3
			 v0 = this.miSyncResult;
			 if ( v0 != null) { // if-eqz v0, :cond_3
				 v0 = this.miSyncResult;
				 v0 = this.resultMessage;
				 /* .line 28 */
				 v0 = 				 android.text.TextUtils .isEmpty ( v0 );
				 if ( v0 != null) { // if-eqz v0, :cond_2
					 /* .line 35 */
				 } // :cond_2
				 v0 = this.miSyncStatusInfo;
				 v1 = this.miSyncResult;
				 v1 = this.resultMessage;
				 this.lastResultMessage = v1;
				 /* .line 36 */
				 return;
				 /* .line 29 */
			 } // :cond_3
		 } // :goto_0
		 v0 = 		 android.util.Log .isLoggable ( v2,v1 );
		 if ( v0 != null) { // if-eqz v0, :cond_4
			 /* .line 30 */
			 /* const-string/jumbo v0, "updateResultStatus: sync result message is null" */
			 android.util.Log .d ( v2,v0 );
			 /* .line 32 */
		 } // :cond_4
		 return;
	 } // .end method
