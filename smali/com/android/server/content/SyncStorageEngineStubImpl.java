public class com.android.server.content.SyncStorageEngineStubImpl implements com.android.server.content.SyncStorageEngineStub {
	 /* .source "SyncStorageEngineStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String MI_PAUSE_FILE_NAME;
	 private static final java.lang.String MI_STRATEGY_FILE_NAME;
	 private static final java.lang.String TAG;
	 private static final java.lang.String TAG_FILE;
	 private static android.util.AtomicFile mMiPauseFile;
	 private static android.util.AtomicFile mMiStrategyFile;
	 private static android.util.SparseArray mMiSyncPause;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/SparseArray<", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Lcom/android/server/content/MiSyncPauseImpl;", */
	 /* ">;>;" */
	 /* } */
} // .end annotation
} // .end field
private static android.util.SparseArray mMiSyncStrategy;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/content/MiSyncStrategyImpl;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.content.SyncStorageEngineStubImpl ( ) {
/* .locals 1 */
/* .line 39 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 43 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 46 */
int v0 = 0; // const/4 v0, 0x0
/* .line 49 */
return;
} // .end method
public com.android.server.content.SyncStorageEngineStubImpl ( ) {
/* .locals 0 */
/* .line 33 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private static void clear ( ) {
/* .locals 1 */
/* .line 77 */
v0 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncPause;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 78 */
v0 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncStrategy;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 79 */
return;
} // .end method
private static Boolean containsXiaomiAccountName ( android.accounts.Account[] p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p0, "accounts" # [Landroid/accounts/Account; */
/* .param p1, "accountName" # Ljava/lang/String; */
/* .line 373 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 374 */
/* .line 376 */
} // :cond_0
/* array-length v1, p0 */
/* move v2, v0 */
} // :goto_0
/* if-ge v2, v1, :cond_2 */
/* aget-object v3, p0, v2 */
/* .line 377 */
/* .local v3, "account":Landroid/accounts/Account; */
v4 = com.android.server.content.MiSyncUtils .checkAccount ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 378 */
v4 = this.name;
v4 = android.text.TextUtils .equals ( v4,p1 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 379 */
int v0 = 1; // const/4 v0, 0x1
/* .line 376 */
} // .end local v3 # "account":Landroid/accounts/Account;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 383 */
} // :cond_2
} // .end method
private static void doMiPauseCleanUpLocked ( android.accounts.Account[] p0, Integer p1 ) {
/* .locals 4 */
/* .param p0, "runningAccounts" # [Landroid/accounts/Account; */
/* .param p1, "uid" # I */
/* .line 323 */
v0 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncPause;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/Map; */
/* .line 324 */
/* .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncPauseImpl;>;" */
/* if-nez v0, :cond_1 */
/* .line 325 */
int v1 = 2; // const/4 v1, 0x2
final String v2 = "SyncManager"; // const-string v2, "SyncManager"
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 326 */
final String v1 = "doMiPauseCleanUpLocked: map is null"; // const-string v1, "doMiPauseCleanUpLocked: map is null"
android.util.Slog .v ( v2,v1 );
/* .line 328 */
} // :cond_0
return;
/* .line 330 */
} // :cond_1
com.android.server.content.SyncStorageEngineStubImpl .getRemovingAccounts ( p0,v1 );
/* .line 331 */
/* .local v1, "removing":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/String; */
/* .line 332 */
/* .local v3, "accountName":Ljava/lang/String; */
/* .line 333 */
} // .end local v3 # "accountName":Ljava/lang/String;
/* .line 334 */
} // :cond_2
return;
} // .end method
private static void doMiStrategyCleanUpLocked ( android.accounts.Account[] p0, Integer p1 ) {
/* .locals 4 */
/* .param p0, "runningAccounts" # [Landroid/accounts/Account; */
/* .param p1, "uid" # I */
/* .line 337 */
v0 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncStrategy;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/Map; */
/* .line 338 */
/* .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncStrategyImpl;>;" */
/* if-nez v0, :cond_1 */
/* .line 339 */
int v1 = 2; // const/4 v1, 0x2
final String v2 = "SyncManager"; // const-string v2, "SyncManager"
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 340 */
final String v1 = "doMiStrategyCleanUpLocked: map is null"; // const-string v1, "doMiStrategyCleanUpLocked: map is null"
android.util.Slog .v ( v2,v1 );
/* .line 342 */
} // :cond_0
return;
/* .line 344 */
} // :cond_1
com.android.server.content.SyncStorageEngineStubImpl .getRemovingAccounts ( p0,v1 );
/* .line 345 */
/* .local v1, "removing":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/String; */
/* .line 346 */
/* .local v3, "accountName":Ljava/lang/String; */
/* .line 347 */
} // .end local v3 # "accountName":Ljava/lang/String;
/* .line 348 */
} // :cond_2
return;
} // .end method
private static com.android.server.content.MiSyncPauseImpl getOrCreateMiSyncPauseLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "accountName" # Ljava/lang/String; */
/* .param p1, "uid" # I */
/* .line 467 */
v0 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncPause;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/Map; */
/* .line 468 */
/* .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncPauseImpl;>;" */
/* if-nez v0, :cond_0 */
/* .line 469 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* move-object v0, v1 */
/* .line 470 */
v1 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncPause;
(( android.util.SparseArray ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 472 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 473 */
/* .local v1, "item":Lcom/android/server/content/MiSyncPauseImpl; */
/* if-nez p0, :cond_1 */
/* .line 474 */
final String p0 = ""; // const-string p0, ""
/* .line 476 */
v2 = } // :cond_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 477 */
/* move-object v1, v2 */
/* check-cast v1, Lcom/android/server/content/MiSyncPauseImpl; */
/* .line 479 */
} // :cond_2
/* if-nez v1, :cond_3 */
/* .line 480 */
/* new-instance v2, Lcom/android/server/content/MiSyncPauseImpl; */
/* invoke-direct {v2, p1, p0}, Lcom/android/server/content/MiSyncPauseImpl;-><init>(ILjava/lang/String;)V */
/* move-object v1, v2 */
/* .line 481 */
/* .line 483 */
} // :cond_3
} // .end method
private static com.android.server.content.MiSyncStrategyImpl getOrCreateMiSyncStrategyLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p0, "accountName" # Ljava/lang/String; */
/* .param p1, "uid" # I */
/* .line 487 */
v0 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncStrategy;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/Map; */
/* .line 488 */
/* .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncStrategyImpl;>;" */
/* if-nez v0, :cond_0 */
/* .line 489 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* move-object v0, v1 */
/* .line 490 */
v1 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncStrategy;
(( android.util.SparseArray ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 492 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 493 */
/* .local v1, "item":Lcom/android/server/content/MiSyncStrategyImpl; */
/* if-nez p0, :cond_1 */
/* .line 494 */
final String p0 = ""; // const-string p0, ""
/* .line 496 */
v2 = } // :cond_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 497 */
/* move-object v1, v2 */
/* check-cast v1, Lcom/android/server/content/MiSyncStrategyImpl; */
/* .line 499 */
} // :cond_2
/* if-nez v1, :cond_3 */
/* .line 500 */
/* new-instance v2, Lcom/android/server/content/MiSyncStrategyImpl; */
/* invoke-direct {v2, p1, p0}, Lcom/android/server/content/MiSyncStrategyImpl;-><init>(ILjava/lang/String;)V */
/* move-object v1, v2 */
/* .line 501 */
/* .line 503 */
} // :cond_3
} // .end method
private static java.util.List getRemovingAccounts ( android.accounts.Account[] p0, java.util.Set p1 ) {
/* .locals 4 */
/* .param p0, "runningAccounts" # [Landroid/accounts/Account; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([", */
/* "Landroid/accounts/Account;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 351 */
/* .local p1, "currentAccountNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 353 */
/* .local v0, "removing":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez p1, :cond_1 */
/* .line 354 */
int v1 = 2; // const/4 v1, 0x2
final String v2 = "SyncManager"; // const-string v2, "SyncManager"
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 355 */
final String v1 = "getRemovingAccounts: Argument is null"; // const-string v1, "getRemovingAccounts: Argument is null"
android.util.Slog .v ( v2,v1 );
/* .line 357 */
} // :cond_0
/* .line 360 */
} // :cond_1
/* if-nez p0, :cond_2 */
/* .line 361 */
int v1 = 0; // const/4 v1, 0x0
/* new-array p0, v1, [Landroid/accounts/Account; */
/* .line 364 */
} // :cond_2
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_4
/* check-cast v2, Ljava/lang/String; */
/* .line 365 */
/* .local v2, "accountName":Ljava/lang/String; */
v3 = com.android.server.content.SyncStorageEngineStubImpl .containsXiaomiAccountName ( p0,v2 );
/* if-nez v3, :cond_3 */
/* .line 366 */
/* .line 368 */
} // .end local v2 # "accountName":Ljava/lang/String;
} // :cond_3
/* .line 369 */
} // :cond_4
} // .end method
private static void readAndWriteLocked ( ) {
/* .locals 0 */
/* .line 62 */
com.android.server.content.SyncStorageEngineStubImpl .readLocked ( );
/* .line 63 */
com.android.server.content.SyncStorageEngineStubImpl .writeLocked ( );
/* .line 64 */
return;
} // .end method
private static void readLocked ( ) {
/* .locals 0 */
/* .line 67 */
com.android.server.content.SyncStorageEngineStubImpl .readMiPauseLocked ( );
/* .line 68 */
com.android.server.content.SyncStorageEngineStubImpl .readMiStrategyLocked ( );
/* .line 69 */
return;
} // .end method
private static void readMiPauseLocked ( ) {
/* .locals 12 */
/* .line 95 */
final String v0 = "No initial mi pause"; // const-string v0, "No initial mi pause"
final String v1 = "Error reading mi pause"; // const-string v1, "Error reading mi pause"
final String v2 = "SyncManagerFile"; // const-string v2, "SyncManagerFile"
final String v3 = "SyncManager"; // const-string v3, "SyncManager"
int v4 = 0; // const/4 v4, 0x0
/* .line 97 */
/* .local v4, "fis":Ljava/io/FileInputStream; */
try { // :try_start_0
v5 = com.android.server.content.SyncStorageEngineStubImpl.mMiPauseFile;
(( android.util.AtomicFile ) v5 ).openRead ( ); // invoke-virtual {v5}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* move-object v4, v5 */
/* .line 98 */
int v5 = 2; // const/4 v5, 0x2
v6 = android.util.Log .isLoggable ( v2,v5 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 99 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Reading "; // const-string v7, "Reading "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = com.android.server.content.SyncStorageEngineStubImpl.mMiPauseFile;
(( android.util.AtomicFile ) v7 ).getBaseFile ( ); // invoke-virtual {v7}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v6 );
/* .line 101 */
} // :cond_0
android.util.Xml .newPullParser ( );
/* .line 102 */
/* .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser; */
v6 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v6 ).name ( ); // invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
v6 = /* .line 103 */
/* .line 104 */
/* .local v6, "eventType":I */
} // :goto_0
int v7 = 1; // const/4 v7, 0x1
/* if-eq v6, v5, :cond_1 */
/* if-eq v6, v7, :cond_1 */
v7 = /* .line 106 */
/* move v6, v7 */
/* .line 108 */
} // :cond_1
/* if-ne v6, v7, :cond_3 */
/* .line 109 */
android.util.Slog .i ( v3,v0 );
/* :try_end_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 141 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 143 */
try { // :try_start_1
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 145 */
/* .line 144 */
/* :catch_0 */
/* move-exception v0 */
/* .line 110 */
} // :cond_2
} // :goto_1
return;
/* .line 113 */
} // :cond_3
try { // :try_start_2
/* .line 114 */
/* .local v8, "tagName":Ljava/lang/String; */
final String v9 = "mi_pause"; // const-string v9, "mi_pause"
v9 = (( java.lang.String ) v9 ).equalsIgnoreCase ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 115 */
/* const-string/jumbo v9, "version" */
int v10 = 0; // const/4 v10, 0x0
/* :try_end_2 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 ..:try_end_2} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_3 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 118 */
/* .local v9, "versionString":Ljava/lang/String; */
/* if-nez v9, :cond_4 */
int v10 = 0; // const/4 v10, 0x0
} // :cond_4
try { // :try_start_3
v10 = java.lang.Integer .parseInt ( v9 );
/* :try_end_3 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 ..:try_end_3} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 121 */
/* .local v10, "version":I */
} // :goto_2
/* .line 119 */
} // .end local v10 # "version":I
/* :catch_1 */
/* move-exception v10 */
/* .line 120 */
/* .local v10, "e":Ljava/lang/NumberFormatException; */
int v11 = 0; // const/4 v11, 0x0
/* move v10, v11 */
/* .line 122 */
/* .local v10, "version":I */
} // :goto_3
/* if-lt v10, v7, :cond_7 */
/* .line 123 */
v11 = try { // :try_start_4
/* move v6, v11 */
/* .line 125 */
} // :cond_5
/* if-ne v6, v5, :cond_6 */
/* .line 126 */
com.android.server.content.MiSyncPauseImpl .readFromXML ( v2 );
/* .line 127 */
/* .local v11, "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl; */
com.android.server.content.SyncStorageEngineStubImpl .setMiPauseInternalLocked ( v11 );
/* .line 129 */
} // .end local v11 # "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl;
v11 = } // :cond_6
/* :try_end_4 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 ..:try_end_4} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* move v6, v11 */
/* .line 130 */
/* if-ne v6, v7, :cond_5 */
/* .line 141 */
} // .end local v2 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v6 # "eventType":I
} // .end local v8 # "tagName":Ljava/lang/String;
} // .end local v9 # "versionString":Ljava/lang/String;
} // .end local v10 # "version":I
} // :cond_7
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 143 */
try { // :try_start_5
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_2 */
/* .line 145 */
} // :goto_4
/* .line 144 */
/* :catch_2 */
/* move-exception v0 */
/* .line 148 */
} // :cond_8
} // :goto_5
return;
/* .line 141 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 136 */
/* :catch_3 */
/* move-exception v2 */
/* .line 137 */
/* .local v2, "e":Ljava/io/IOException; */
/* if-nez v4, :cond_9 */
try { // :try_start_6
android.util.Slog .i ( v3,v0 );
/* .line 138 */
} // :cond_9
android.util.Slog .w ( v3,v1,v2 );
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 141 */
} // :goto_6
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 143 */
try { // :try_start_7
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_4 */
/* .line 145 */
/* .line 144 */
/* :catch_4 */
/* move-exception v0 */
/* .line 139 */
} // :cond_a
} // :goto_7
return;
/* .line 133 */
} // .end local v2 # "e":Ljava/io/IOException;
/* :catch_5 */
/* move-exception v0 */
/* .line 134 */
/* .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException; */
try { // :try_start_8
android.util.Slog .w ( v3,v1,v0 );
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_0 */
/* .line 141 */
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 143 */
try { // :try_start_9
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_9 */
/* .catch Ljava/io/IOException; {:try_start_9 ..:try_end_9} :catch_6 */
/* .line 145 */
/* .line 144 */
/* :catch_6 */
/* move-exception v1 */
/* .line 135 */
} // :cond_b
} // :goto_8
return;
/* .line 141 */
} // .end local v0 # "e":Lorg/xmlpull/v1/XmlPullParserException;
} // :goto_9
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 143 */
try { // :try_start_a
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_a */
/* .catch Ljava/io/IOException; {:try_start_a ..:try_end_a} :catch_7 */
/* .line 145 */
/* .line 144 */
/* :catch_7 */
/* move-exception v1 */
/* .line 147 */
} // :cond_c
} // :goto_a
/* throw v0 */
} // .end method
private static void readMiStrategyLocked ( ) {
/* .locals 12 */
/* .line 151 */
final String v0 = "No initial mi strategy"; // const-string v0, "No initial mi strategy"
final String v1 = "Error reading mi strategy"; // const-string v1, "Error reading mi strategy"
final String v2 = "SyncManagerFile"; // const-string v2, "SyncManagerFile"
final String v3 = "SyncManager"; // const-string v3, "SyncManager"
int v4 = 0; // const/4 v4, 0x0
/* .line 153 */
/* .local v4, "fis":Ljava/io/FileInputStream; */
try { // :try_start_0
v5 = com.android.server.content.SyncStorageEngineStubImpl.mMiStrategyFile;
(( android.util.AtomicFile ) v5 ).openRead ( ); // invoke-virtual {v5}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;
/* move-object v4, v5 */
/* .line 154 */
int v5 = 2; // const/4 v5, 0x2
v6 = android.util.Log .isLoggable ( v2,v5 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 155 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Reading "; // const-string v7, "Reading "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = com.android.server.content.SyncStorageEngineStubImpl.mMiStrategyFile;
(( android.util.AtomicFile ) v7 ).getBaseFile ( ); // invoke-virtual {v7}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v6 );
/* .line 157 */
} // :cond_0
android.util.Xml .newPullParser ( );
/* .line 158 */
/* .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser; */
v6 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v6 ).name ( ); // invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
v6 = /* .line 159 */
/* .line 160 */
/* .local v6, "eventType":I */
} // :goto_0
int v7 = 1; // const/4 v7, 0x1
/* if-eq v6, v5, :cond_1 */
/* if-eq v6, v7, :cond_1 */
v7 = /* .line 162 */
/* move v6, v7 */
/* .line 164 */
} // :cond_1
/* if-ne v6, v7, :cond_3 */
/* .line 165 */
android.util.Slog .i ( v3,v0 );
/* :try_end_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 197 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 199 */
try { // :try_start_1
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 201 */
/* .line 200 */
/* :catch_0 */
/* move-exception v0 */
/* .line 166 */
} // :cond_2
} // :goto_1
return;
/* .line 169 */
} // :cond_3
try { // :try_start_2
/* .line 170 */
/* .local v8, "tagName":Ljava/lang/String; */
final String v9 = "mi_strategy"; // const-string v9, "mi_strategy"
v9 = (( java.lang.String ) v9 ).equalsIgnoreCase ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 171 */
/* const-string/jumbo v9, "version" */
int v10 = 0; // const/4 v10, 0x0
/* :try_end_2 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 ..:try_end_2} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_3 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 174 */
/* .local v9, "versionString":Ljava/lang/String; */
/* if-nez v9, :cond_4 */
int v10 = 0; // const/4 v10, 0x0
} // :cond_4
try { // :try_start_3
v10 = java.lang.Integer .parseInt ( v9 );
/* :try_end_3 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 ..:try_end_3} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 177 */
/* .local v10, "version":I */
} // :goto_2
/* .line 175 */
} // .end local v10 # "version":I
/* :catch_1 */
/* move-exception v10 */
/* .line 176 */
/* .local v10, "e":Ljava/lang/NumberFormatException; */
int v11 = 0; // const/4 v11, 0x0
/* move v10, v11 */
/* .line 178 */
/* .local v10, "version":I */
} // :goto_3
/* if-lt v10, v7, :cond_7 */
/* .line 179 */
v11 = try { // :try_start_4
/* move v6, v11 */
/* .line 181 */
} // :cond_5
/* if-ne v6, v5, :cond_6 */
/* .line 182 */
com.android.server.content.MiSyncStrategyImpl .readFromXML ( v2 );
/* .line 183 */
/* .local v11, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl; */
com.android.server.content.SyncStorageEngineStubImpl .setMiStrategyInternalLocked ( v11 );
/* .line 185 */
} // .end local v11 # "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl;
v11 = } // :cond_6
/* :try_end_4 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 ..:try_end_4} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* move v6, v11 */
/* .line 186 */
/* if-ne v6, v7, :cond_5 */
/* .line 197 */
} // .end local v2 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v6 # "eventType":I
} // .end local v8 # "tagName":Ljava/lang/String;
} // .end local v9 # "versionString":Ljava/lang/String;
} // .end local v10 # "version":I
} // :cond_7
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 199 */
try { // :try_start_5
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_2 */
/* .line 201 */
} // :goto_4
/* .line 200 */
/* :catch_2 */
/* move-exception v0 */
/* .line 204 */
} // :cond_8
} // :goto_5
return;
/* .line 197 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 192 */
/* :catch_3 */
/* move-exception v2 */
/* .line 193 */
/* .local v2, "e":Ljava/io/IOException; */
/* if-nez v4, :cond_9 */
try { // :try_start_6
android.util.Slog .i ( v3,v0 );
/* .line 194 */
} // :cond_9
android.util.Slog .w ( v3,v1,v2 );
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 197 */
} // :goto_6
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 199 */
try { // :try_start_7
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_4 */
/* .line 201 */
/* .line 200 */
/* :catch_4 */
/* move-exception v0 */
/* .line 195 */
} // :cond_a
} // :goto_7
return;
/* .line 189 */
} // .end local v2 # "e":Ljava/io/IOException;
/* :catch_5 */
/* move-exception v0 */
/* .line 190 */
/* .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException; */
try { // :try_start_8
android.util.Slog .w ( v3,v1,v0 );
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_0 */
/* .line 197 */
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 199 */
try { // :try_start_9
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_9 */
/* .catch Ljava/io/IOException; {:try_start_9 ..:try_end_9} :catch_6 */
/* .line 201 */
/* .line 200 */
/* :catch_6 */
/* move-exception v1 */
/* .line 191 */
} // :cond_b
} // :goto_8
return;
/* .line 197 */
} // .end local v0 # "e":Lorg/xmlpull/v1/XmlPullParserException;
} // :goto_9
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 199 */
try { // :try_start_a
(( java.io.FileInputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
/* :try_end_a */
/* .catch Ljava/io/IOException; {:try_start_a ..:try_end_a} :catch_7 */
/* .line 201 */
/* .line 200 */
/* :catch_7 */
/* move-exception v1 */
/* .line 203 */
} // :cond_c
} // :goto_a
/* throw v0 */
} // .end method
private static void setMiPauseInternalLocked ( com.android.server.content.MiSyncPauseImpl p0 ) {
/* .locals 4 */
/* .param p0, "miSyncPause" # Lcom/android/server/content/MiSyncPauseImpl; */
/* .line 277 */
/* if-nez p0, :cond_1 */
/* .line 278 */
int v0 = 2; // const/4 v0, 0x2
final String v1 = "SyncManager"; // const-string v1, "SyncManager"
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 279 */
/* const-string/jumbo v0, "setMiPauseInternalLocked: miSyncPause is null" */
android.util.Slog .v ( v1,v0 );
/* .line 281 */
} // :cond_0
return;
/* .line 283 */
} // :cond_1
(( com.android.server.content.MiSyncPauseImpl ) p0 ).getAccountName ( ); // invoke-virtual {p0}, Lcom/android/server/content/MiSyncPauseImpl;->getAccountName()Ljava/lang/String;
/* .line 284 */
(( com.android.server.content.MiSyncPauseImpl ) p0 ).getPauseEndTime ( ); // invoke-virtual {p0}, Lcom/android/server/content/MiSyncPauseImpl;->getPauseEndTime()J
/* move-result-wide v1 */
/* .line 285 */
v3 = (( com.android.server.content.MiSyncPauseImpl ) p0 ).getUid ( ); // invoke-virtual {p0}, Lcom/android/server/content/MiSyncPauseImpl;->getUid()I
/* .line 283 */
com.android.server.content.SyncStorageEngineStubImpl .setMiPauseInternalLocked ( v0,v1,v2,v3 );
/* .line 286 */
return;
} // .end method
private static void setMiPauseInternalLocked ( java.lang.String p0, Long p1, Integer p2 ) {
/* .locals 2 */
/* .param p0, "accountName" # Ljava/lang/String; */
/* .param p1, "pauseTimeMillis" # J */
/* .param p3, "uid" # I */
/* .line 289 */
v0 = android.text.TextUtils .isEmpty ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 290 */
int v0 = 2; // const/4 v0, 0x2
final String v1 = "SyncManager"; // const-string v1, "SyncManager"
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 291 */
/* const-string/jumbo v0, "setMiPauseInternalLocked: accountName is null" */
android.util.Slog .v ( v1,v0 );
/* .line 293 */
} // :cond_0
return;
/* .line 295 */
} // :cond_1
com.android.server.content.SyncStorageEngineStubImpl .getOrCreateMiSyncPauseLocked ( p0,p3 );
/* .line 296 */
/* .local v0, "item":Lcom/android/server/content/MiSyncPauseImpl; */
(( com.android.server.content.MiSyncPauseImpl ) v0 ).setPauseToTime ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/content/MiSyncPauseImpl;->setPauseToTime(J)Z
/* .line 297 */
return;
} // .end method
private static void setMiStrategyInternalLocked ( com.android.server.content.MiSyncStrategyImpl p0 ) {
/* .locals 3 */
/* .param p0, "miSyncStrategy" # Lcom/android/server/content/MiSyncStrategyImpl; */
/* .line 300 */
/* if-nez p0, :cond_1 */
/* .line 301 */
int v0 = 2; // const/4 v0, 0x2
final String v1 = "SyncManager"; // const-string v1, "SyncManager"
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 302 */
/* const-string/jumbo v0, "setMiStrategyInternalLocked: miSyncStrategy is null" */
android.util.Slog .v ( v1,v0 );
/* .line 304 */
} // :cond_0
return;
/* .line 306 */
} // :cond_1
(( com.android.server.content.MiSyncStrategyImpl ) p0 ).getAccountName ( ); // invoke-virtual {p0}, Lcom/android/server/content/MiSyncStrategyImpl;->getAccountName()Ljava/lang/String;
/* .line 307 */
v1 = (( com.android.server.content.MiSyncStrategyImpl ) p0 ).getStrategy ( ); // invoke-virtual {p0}, Lcom/android/server/content/MiSyncStrategyImpl;->getStrategy()I
/* .line 308 */
v2 = (( com.android.server.content.MiSyncStrategyImpl ) p0 ).getUid ( ); // invoke-virtual {p0}, Lcom/android/server/content/MiSyncStrategyImpl;->getUid()I
/* .line 306 */
com.android.server.content.SyncStorageEngineStubImpl .setMiStrategyInternalLocked ( v0,v1,v2 );
/* .line 309 */
return;
} // .end method
private static void setMiStrategyInternalLocked ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p0, "accountName" # Ljava/lang/String; */
/* .param p1, "strategy" # I */
/* .param p2, "uid" # I */
/* .line 312 */
v0 = android.text.TextUtils .isEmpty ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 313 */
int v0 = 2; // const/4 v0, 0x2
final String v1 = "SyncManager"; // const-string v1, "SyncManager"
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 314 */
/* const-string/jumbo v0, "setMiStrategyInternalLocked: accountName is null" */
android.util.Slog .v ( v1,v0 );
/* .line 316 */
} // :cond_0
return;
/* .line 318 */
} // :cond_1
com.android.server.content.SyncStorageEngineStubImpl .getOrCreateMiSyncStrategyLocked ( p0,p2 );
/* .line 319 */
/* .local v0, "item":Lcom/android/server/content/MiSyncStrategyImpl; */
(( com.android.server.content.MiSyncStrategyImpl ) v0 ).setStrategy ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/content/MiSyncStrategyImpl;->setStrategy(I)Z
/* .line 320 */
return;
} // .end method
private static void writeLocked ( ) {
/* .locals 0 */
/* .line 72 */
com.android.server.content.SyncStorageEngineStubImpl .writeMiPauseLocked ( );
/* .line 73 */
com.android.server.content.SyncStorageEngineStubImpl .writeMiStrategyLocked ( );
/* .line 74 */
return;
} // .end method
private static void writeMiPauseLocked ( ) {
/* .locals 10 */
/* .line 207 */
final String v0 = "mi_pause"; // const-string v0, "mi_pause"
int v1 = 2; // const/4 v1, 0x2
final String v2 = "SyncManagerFile"; // const-string v2, "SyncManagerFile"
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 208 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Writing new "; // const-string v3, "Writing new "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.android.server.content.SyncStorageEngineStubImpl.mMiPauseFile;
(( android.util.AtomicFile ) v3 ).getBaseFile ( ); // invoke-virtual {v3}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v1 );
/* .line 210 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 213 */
/* .local v1, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_0
v2 = com.android.server.content.SyncStorageEngineStubImpl.mMiPauseFile;
(( android.util.AtomicFile ) v2 ).startWrite ( ); // invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v1, v2 */
/* .line 214 */
/* new-instance v2, Lcom/android/internal/util/FastXmlSerializer; */
/* invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V */
/* .line 215 */
/* .local v2, "out":Lorg/xmlpull/v1/XmlSerializer; */
v3 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v3 ).name ( ); // invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 216 */
int v3 = 1; // const/4 v3, 0x1
java.lang.Boolean .valueOf ( v3 );
int v5 = 0; // const/4 v5, 0x0
/* .line 217 */
final String v4 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 219 */
/* .line 220 */
/* const-string/jumbo v4, "version" */
java.lang.Integer .toString ( v3 );
/* .line 222 */
v3 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncPause;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* .line 223 */
/* .local v3, "m":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v3, :cond_2 */
/* .line 224 */
v6 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncPause;
(( android.util.SparseArray ) v6 ).valueAt ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Ljava/util/Map; */
/* .line 225 */
/* .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncPauseImpl;>;" */
/* .line 226 */
/* .local v7, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/server/content/MiSyncPauseImpl;>;" */
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_1
/* check-cast v9, Lcom/android/server/content/MiSyncPauseImpl; */
/* .line 227 */
/* .local v9, "item":Lcom/android/server/content/MiSyncPauseImpl; */
(( com.android.server.content.MiSyncPauseImpl ) v9 ).writeToXML ( v2 ); // invoke-virtual {v9, v2}, Lcom/android/server/content/MiSyncPauseImpl;->writeToXML(Lorg/xmlpull/v1/XmlSerializer;)V
/* .line 228 */
} // .end local v9 # "item":Lcom/android/server/content/MiSyncPauseImpl;
/* .line 223 */
} // .end local v6 # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncPauseImpl;>;"
} // .end local v7 # "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/server/content/MiSyncPauseImpl;>;"
} // :cond_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 230 */
} // .end local v4 # "i":I
} // :cond_2
/* .line 231 */
/* .line 232 */
v0 = com.android.server.content.SyncStorageEngineStubImpl.mMiPauseFile;
(( android.util.AtomicFile ) v0 ).finishWrite ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 238 */
} // .end local v2 # "out":Lorg/xmlpull/v1/XmlSerializer;
} // .end local v3 # "m":I
/* .line 233 */
/* :catch_0 */
/* move-exception v0 */
/* .line 234 */
/* .local v0, "e1":Ljava/io/IOException; */
final String v2 = "SyncManager"; // const-string v2, "SyncManager"
final String v3 = "Error writing mi pause"; // const-string v3, "Error writing mi pause"
android.util.Slog .w ( v2,v3,v0 );
/* .line 235 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 236 */
v2 = com.android.server.content.SyncStorageEngineStubImpl.mMiPauseFile;
(( android.util.AtomicFile ) v2 ).failWrite ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 239 */
} // .end local v0 # "e1":Ljava/io/IOException;
} // :cond_3
} // :goto_2
return;
} // .end method
private static void writeMiStrategyLocked ( ) {
/* .locals 10 */
/* .line 242 */
final String v0 = "mi_strategy"; // const-string v0, "mi_strategy"
int v1 = 2; // const/4 v1, 0x2
final String v2 = "SyncManagerFile"; // const-string v2, "SyncManagerFile"
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 243 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Writing new "; // const-string v3, "Writing new "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.android.server.content.SyncStorageEngineStubImpl.mMiStrategyFile;
(( android.util.AtomicFile ) v3 ).getBaseFile ( ); // invoke-virtual {v3}, Landroid/util/AtomicFile;->getBaseFile()Ljava/io/File;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v2,v1 );
/* .line 245 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 248 */
/* .local v1, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_0
v2 = com.android.server.content.SyncStorageEngineStubImpl.mMiStrategyFile;
(( android.util.AtomicFile ) v2 ).startWrite ( ); // invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v1, v2 */
/* .line 249 */
/* new-instance v2, Lcom/android/internal/util/FastXmlSerializer; */
/* invoke-direct {v2}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V */
/* .line 250 */
/* .local v2, "out":Lorg/xmlpull/v1/XmlSerializer; */
v3 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v3 ).name ( ); // invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 251 */
int v3 = 1; // const/4 v3, 0x1
java.lang.Boolean .valueOf ( v3 );
int v5 = 0; // const/4 v5, 0x0
/* .line 252 */
final String v4 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v4, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 254 */
/* .line 255 */
/* const-string/jumbo v4, "version" */
java.lang.Integer .toString ( v3 );
/* .line 257 */
v3 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncStrategy;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* .line 258 */
/* .local v3, "m":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v3, :cond_2 */
/* .line 259 */
v6 = com.android.server.content.SyncStorageEngineStubImpl.mMiSyncStrategy;
(( android.util.SparseArray ) v6 ).valueAt ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Ljava/util/Map; */
/* .line 260 */
/* .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncStrategyImpl;>;" */
/* .line 261 */
/* .local v7, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/server/content/MiSyncStrategyImpl;>;" */
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_1
/* check-cast v9, Lcom/android/server/content/MiSyncStrategyImpl; */
/* .line 262 */
/* .local v9, "item":Lcom/android/server/content/MiSyncStrategyImpl; */
(( com.android.server.content.MiSyncStrategyImpl ) v9 ).writeToXML ( v2 ); // invoke-virtual {v9, v2}, Lcom/android/server/content/MiSyncStrategyImpl;->writeToXML(Lorg/xmlpull/v1/XmlSerializer;)V
/* .line 263 */
} // .end local v9 # "item":Lcom/android/server/content/MiSyncStrategyImpl;
/* .line 258 */
} // .end local v6 # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/content/MiSyncStrategyImpl;>;"
} // .end local v7 # "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/android/server/content/MiSyncStrategyImpl;>;"
} // :cond_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 265 */
} // .end local v4 # "i":I
} // :cond_2
/* .line 266 */
/* .line 267 */
v0 = com.android.server.content.SyncStorageEngineStubImpl.mMiStrategyFile;
(( android.util.AtomicFile ) v0 ).finishWrite ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 273 */
} // .end local v2 # "out":Lorg/xmlpull/v1/XmlSerializer;
} // .end local v3 # "m":I
/* .line 268 */
/* :catch_0 */
/* move-exception v0 */
/* .line 269 */
/* .local v0, "e1":Ljava/io/IOException; */
final String v2 = "SyncManager"; // const-string v2, "SyncManager"
final String v3 = "Error writing mi strategy"; // const-string v3, "Error writing mi strategy"
android.util.Slog .w ( v2,v3,v0 );
/* .line 270 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 271 */
v2 = com.android.server.content.SyncStorageEngineStubImpl.mMiStrategyFile;
(( android.util.AtomicFile ) v2 ).failWrite ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 274 */
} // .end local v0 # "e1":Ljava/io/IOException;
} // :cond_3
} // :goto_2
return;
} // .end method
/* # virtual methods */
public void clearAndReadAndWriteLocked ( ) {
/* .locals 0 */
/* .line 83 */
com.android.server.content.SyncStorageEngineStubImpl .clear ( );
/* .line 84 */
com.android.server.content.SyncStorageEngineStubImpl .readAndWriteLocked ( );
/* .line 85 */
return;
} // .end method
public void doDatabaseCleanupLocked ( android.accounts.Account[] p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "accounts" # [Landroid/accounts/Account; */
/* .param p2, "uid" # I */
/* .line 89 */
com.android.server.content.SyncStorageEngineStubImpl .doMiPauseCleanUpLocked ( p1,p2 );
/* .line 90 */
com.android.server.content.SyncStorageEngineStubImpl .doMiStrategyCleanUpLocked ( p1,p2 );
/* .line 91 */
com.android.server.content.SyncStorageEngineStubImpl .writeLocked ( );
/* .line 92 */
return;
} // .end method
public com.android.server.content.MiSyncPause getMiSyncPauseLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "accountName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 458 */
com.android.server.content.SyncStorageEngineStubImpl .getOrCreateMiSyncPauseLocked ( p1,p2 );
} // .end method
public Long getMiSyncPauseToTimeLocked ( android.accounts.Account p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "account" # Landroid/accounts/Account; */
/* .param p2, "uid" # I */
/* .line 433 */
v0 = com.android.server.content.MiSyncUtils .checkAccount ( p1 );
/* if-nez v0, :cond_1 */
/* .line 434 */
int v0 = 2; // const/4 v0, 0x2
final String v1 = "SyncManager"; // const-string v1, "SyncManager"
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 435 */
final String v0 = "getMiSyncPauseToTimeLocked: not xiaomi account"; // const-string v0, "getMiSyncPauseToTimeLocked: not xiaomi account"
android.util.Slog .v ( v1,v0 );
/* .line 437 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
/* .line 439 */
} // :cond_1
v0 = this.name;
com.android.server.content.SyncStorageEngineStubImpl .getOrCreateMiSyncPauseLocked ( v0,p2 );
/* .line 440 */
/* .local v0, "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl; */
(( com.android.server.content.MiSyncPauseImpl ) v0 ).getPauseEndTime ( ); // invoke-virtual {v0}, Lcom/android/server/content/MiSyncPauseImpl;->getPauseEndTime()J
/* move-result-wide v1 */
/* return-wide v1 */
} // .end method
public Integer getMiSyncStrategyLocked ( android.accounts.Account p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "account" # Landroid/accounts/Account; */
/* .param p2, "uid" # I */
/* .line 446 */
v0 = com.android.server.content.MiSyncUtils .checkAccount ( p1 );
/* if-nez v0, :cond_1 */
/* .line 447 */
int v0 = 2; // const/4 v0, 0x2
final String v1 = "SyncManager"; // const-string v1, "SyncManager"
v0 = android.util.Log .isLoggable ( v1,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 448 */
final String v0 = "getMiSyncStrategyLocked: not xiaomi account"; // const-string v0, "getMiSyncStrategyLocked: not xiaomi account"
android.util.Slog .v ( v1,v0 );
/* .line 450 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 452 */
} // :cond_1
v0 = this.name;
com.android.server.content.SyncStorageEngineStubImpl .getOrCreateMiSyncStrategyLocked ( v0,p2 );
/* .line 453 */
/* .local v0, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl; */
v1 = (( com.android.server.content.MiSyncStrategyImpl ) v0 ).getStrategy ( ); // invoke-virtual {v0}, Lcom/android/server/content/MiSyncStrategyImpl;->getStrategy()I
} // .end method
public com.android.server.content.MiSyncStrategy getMiSyncStrategyLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "accountName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 463 */
com.android.server.content.SyncStorageEngineStubImpl .getOrCreateMiSyncStrategyLocked ( p1,p2 );
} // .end method
public void initAndReadAndWriteLocked ( java.io.File p0 ) {
/* .locals 3 */
/* .param p1, "syncDir" # Ljava/io/File; */
/* .line 55 */
/* new-instance v0, Landroid/util/AtomicFile; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "mi_pause.xml"; // const-string v2, "mi_pause.xml"
/* invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
/* .line 56 */
/* new-instance v0, Landroid/util/AtomicFile; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "mi_strategy.xml"; // const-string v2, "mi_strategy.xml"
/* invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
/* .line 58 */
com.android.server.content.SyncStorageEngineStubImpl .readAndWriteLocked ( );
/* .line 59 */
return;
} // .end method
public void setMiSyncPauseToTimeLocked ( android.accounts.Account p0, Long p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "account" # Landroid/accounts/Account; */
/* .param p2, "pauseTimeMillis" # J */
/* .param p4, "uid" # I */
/* .line 389 */
v0 = com.android.server.content.MiSyncUtils .checkAccount ( p1 );
int v1 = 2; // const/4 v1, 0x2
final String v2 = "SyncManager"; // const-string v2, "SyncManager"
/* if-nez v0, :cond_1 */
/* .line 390 */
v0 = android.util.Log .isLoggable ( v2,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 391 */
/* const-string/jumbo v0, "setMiSyncPauseToTimeLocked: account is null" */
android.util.Slog .v ( v2,v0 );
/* .line 393 */
} // :cond_0
return;
/* .line 396 */
} // :cond_1
v0 = this.name;
com.android.server.content.SyncStorageEngineStubImpl .getOrCreateMiSyncPauseLocked ( v0,p4 );
/* .line 397 */
/* .local v0, "miSyncPause":Lcom/android/server/content/MiSyncPauseImpl; */
(( com.android.server.content.MiSyncPauseImpl ) v0 ).getPauseEndTime ( ); // invoke-virtual {v0}, Lcom/android/server/content/MiSyncPauseImpl;->getPauseEndTime()J
/* move-result-wide v3 */
/* cmp-long v3, v3, p2 */
/* if-nez v3, :cond_3 */
/* .line 398 */
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 399 */
/* const-string/jumbo v1, "setMiSyncPauseTimeLocked: pause time is not changed" */
android.util.Slog .v ( v2,v1 );
/* .line 401 */
} // :cond_2
return;
/* .line 403 */
} // :cond_3
(( com.android.server.content.MiSyncPauseImpl ) v0 ).setPauseToTime ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/android/server/content/MiSyncPauseImpl;->setPauseToTime(J)Z
/* .line 405 */
com.android.server.content.SyncStorageEngineStubImpl .writeMiPauseLocked ( );
/* .line 406 */
return;
} // .end method
public void setMiSyncStrategyLocked ( android.accounts.Account p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "account" # Landroid/accounts/Account; */
/* .param p2, "strategy" # I */
/* .param p3, "uid" # I */
/* .line 411 */
v0 = com.android.server.content.MiSyncUtils .checkAccount ( p1 );
int v1 = 2; // const/4 v1, 0x2
final String v2 = "SyncManager"; // const-string v2, "SyncManager"
/* if-nez v0, :cond_1 */
/* .line 412 */
v0 = android.util.Log .isLoggable ( v2,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 413 */
/* const-string/jumbo v0, "setMiSyncStrategyLocked: account is null" */
android.util.Slog .v ( v2,v0 );
/* .line 415 */
} // :cond_0
return;
/* .line 418 */
} // :cond_1
v0 = this.name;
com.android.server.content.SyncStorageEngineStubImpl .getOrCreateMiSyncStrategyLocked ( v0,p3 );
/* .line 419 */
/* .local v0, "miSyncStrategy":Lcom/android/server/content/MiSyncStrategyImpl; */
v3 = (( com.android.server.content.MiSyncStrategyImpl ) v0 ).getStrategy ( ); // invoke-virtual {v0}, Lcom/android/server/content/MiSyncStrategyImpl;->getStrategy()I
/* if-ne v3, p2, :cond_3 */
/* .line 420 */
v1 = android.util.Log .isLoggable ( v2,v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 421 */
/* const-string/jumbo v1, "setMiSyncPauseTimeLocked: strategy is not changed" */
android.util.Slog .v ( v2,v1 );
/* .line 423 */
} // :cond_2
return;
/* .line 425 */
} // :cond_3
(( com.android.server.content.MiSyncStrategyImpl ) v0 ).setStrategy ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/content/MiSyncStrategyImpl;->setStrategy(I)Z
/* .line 427 */
com.android.server.content.SyncStorageEngineStubImpl .writeMiStrategyLocked ( );
/* .line 428 */
return;
} // .end method
public void updateResultStatusLocked ( android.content.SyncStatusInfo p0, java.lang.String p1, android.content.SyncResult p2 ) {
/* .locals 0 */
/* .param p1, "syncStatusInfo" # Landroid/content/SyncStatusInfo; */
/* .param p2, "lastSyncMessage" # Ljava/lang/String; */
/* .param p3, "syncResult" # Landroid/content/SyncResult; */
/* .line 509 */
com.android.server.content.MiSyncResultStatusAdapter .updateResultStatus ( p1,p2,p3 );
/* .line 511 */
return;
} // .end method
