public class com.android.server.content.MiSyncPolicyManager extends com.android.server.content.MiSyncPolicyManagerBase {
	 /* .source "MiSyncPolicyManager.java" */
	 /* # static fields */
	 private static final android.net.Uri SYNC_ON_WIFI_ONLY_URI;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static void -$$Nest$smrescheduleAllSyncsH ( com.android.server.content.SyncManager p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 com.android.server.content.MiSyncPolicyManager .rescheduleAllSyncsH ( p0 );
		 return;
	 } // .end method
	 static void -$$Nest$smrescheduleXiaomiSyncsH ( com.android.server.content.SyncManager p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 com.android.server.content.MiSyncPolicyManager .rescheduleXiaomiSyncsH ( p0 );
		 return;
	 } // .end method
	 static com.android.server.content.MiSyncPolicyManager ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 /* nop */
		 /* .line 22 */
		 /* const-string/jumbo v0, "sync_on_wifi_only" */
		 android.provider.Settings$Secure .getUriFor ( v0 );
		 /* .line 21 */
		 return;
	 } // .end method
	 public com.android.server.content.MiSyncPolicyManager ( ) {
		 /* .locals 0 */
		 /* .line 18 */
		 /* invoke-direct {p0}, Lcom/android/server/content/MiSyncPolicyManagerBase;-><init>()V */
		 return;
	 } // .end method
	 public static Long getSyncDelayedH ( com.android.server.content.SyncOperation p0, com.android.server.content.SyncManager p1 ) {
		 /* .locals 2 */
		 /* .param p0, "op" # Lcom/android/server/content/SyncOperation; */
		 /* .param p1, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 36 */
		 v0 = 		 com.android.server.content.MiSyncPolicyManager .isSyncRoomForbiddenH ( p0,p1 );
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 37 */
			 int v0 = 3; // const/4 v0, 0x3
			 final String v1 = "SyncManager"; // const-string v1, "SyncManager"
			 v0 = 			 android.util.Log .isLoggable ( v1,v0 );
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 38 */
				 final String v0 = "injector: sync is forbidden for no room!"; // const-string v0, "injector: sync is forbidden for no room!"
				 android.util.Log .d ( v1,v0 );
				 /* .line 40 */
			 } // :cond_0
			 /* const-wide/16 v0, 0x7530 */
			 /* return-wide v0 */
			 /* .line 42 */
		 } // :cond_1
		 /* const-wide/16 v0, 0x0 */
		 /* return-wide v0 */
	 } // .end method
	 public static void handleMasterWifiOnlyChanged ( com.android.server.content.SyncManager p0 ) {
		 /* .locals 2 */
		 /* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 64 */
		 v0 = this.mSyncHandler;
		 /* new-instance v1, Lcom/android/server/content/MiSyncPolicyManager$2; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/content/MiSyncPolicyManager$2;-><init>(Lcom/android/server/content/SyncManager;)V */
		 (( com.android.server.content.SyncManager$SyncHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/content/SyncManager$SyncHandler;->post(Ljava/lang/Runnable;)Z
		 /* .line 70 */
		 return;
	 } // .end method
	 public static void handleSyncPauseChanged ( android.content.Context p0, com.android.server.content.SyncManager p1, Long p2 ) {
		 /* .locals 0 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .param p2, "pauseTimeMills" # J */
		 /* .line 74 */
		 com.android.server.content.MiSyncPolicyManager .handleSyncPauseChanged ( p1 );
		 /* .line 75 */
		 return;
	 } // .end method
	 public static void handleSyncPauseChanged ( com.android.server.content.SyncManager p0 ) {
		 /* .locals 2 */
		 /* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 78 */
		 v0 = this.mSyncHandler;
		 /* new-instance v1, Lcom/android/server/content/MiSyncPolicyManager$3; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/content/MiSyncPolicyManager$3;-><init>(Lcom/android/server/content/SyncManager;)V */
		 (( com.android.server.content.SyncManager$SyncHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/content/SyncManager$SyncHandler;->post(Ljava/lang/Runnable;)Z
		 /* .line 84 */
		 return;
	 } // .end method
	 public static void handleSyncStrategyChanged ( android.content.Context p0, com.android.server.content.SyncManager p1 ) {
		 /* .locals 0 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 87 */
		 com.android.server.content.MiSyncPolicyManager .handleSyncStrategyChanged ( p1 );
		 /* .line 88 */
		 return;
	 } // .end method
	 public static void handleSyncStrategyChanged ( com.android.server.content.SyncManager p0 ) {
		 /* .locals 2 */
		 /* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 91 */
		 v0 = this.mSyncHandler;
		 /* new-instance v1, Lcom/android/server/content/MiSyncPolicyManager$4; */
		 /* invoke-direct {v1, p0}, Lcom/android/server/content/MiSyncPolicyManager$4;-><init>(Lcom/android/server/content/SyncManager;)V */
		 (( com.android.server.content.SyncManager$SyncHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/content/SyncManager$SyncHandler;->post(Ljava/lang/Runnable;)Z
		 /* .line 97 */
		 return;
	 } // .end method
	 private static Boolean isSyncRoomForbiddenH ( com.android.server.content.SyncOperation p0, com.android.server.content.SyncManager p1 ) {
		 /* .locals 1 */
		 /* .param p0, "op" # Lcom/android/server/content/SyncOperation; */
		 /* .param p1, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 46 */
		 v0 = 		 com.android.server.content.MiSyncUtils .isSyncRoomForbiddenH ( p0,p1 );
	 } // .end method
	 public static void registerSyncSettingsObserver ( android.content.Context p0, com.android.server.content.SyncManager p1 ) {
		 /* .locals 4 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 25 */
		 /* new-instance v0, Lcom/android/server/content/MiSyncPolicyManager$1; */
		 v1 = this.mSyncHandler;
		 /* invoke-direct {v0, v1, p1}, Lcom/android/server/content/MiSyncPolicyManager$1;-><init>(Landroid/os/Handler;Lcom/android/server/content/SyncManager;)V */
		 /* .line 31 */
		 /* .local v0, "syncSettingsObserver":Landroid/database/ContentObserver; */
		 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 v2 = com.android.server.content.MiSyncPolicyManager.SYNC_ON_WIFI_ONLY_URI;
		 int v3 = 0; // const/4 v3, 0x0
		 (( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
		 /* .line 33 */
		 return;
	 } // .end method
	 private static void rescheduleAllSyncsH ( com.android.server.content.SyncManager p0 ) {
		 /* .locals 7 */
		 /* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
		 /* .line 101 */
		 v0 = this.mActiveSyncContexts;
		 (( java.util.concurrent.CopyOnWriteArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;
	 v1 = 	 } // :goto_0
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* check-cast v1, Lcom/android/server/content/SyncManager$ActiveSyncContext; */
		 /* .line 102 */
		 /* .local v1, "asc":Lcom/android/server/content/SyncManager$ActiveSyncContext; */
		 v2 = this.mSyncHandler;
		 final String v3 = "reschedule-all"; // const-string v3, "reschedule-all"
		 (( com.android.server.content.SyncManager$SyncHandler ) v2 ).deferActiveSyncH ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/content/SyncManager$SyncHandler;->deferActiveSyncH(Lcom/android/server/content/SyncManager$ActiveSyncContext;Ljava/lang/String;)V
		 /* .line 103 */
	 } // .end local v1 # "asc":Lcom/android/server/content/SyncManager$ActiveSyncContext;
	 /* .line 106 */
} // :cond_0
(( com.android.server.content.SyncManager ) p0 ).getJobScheduler ( ); // invoke-virtual {p0}, Lcom/android/server/content/SyncManager;->getJobScheduler()Landroid/app/job/JobScheduler;
/* .line 107 */
/* .local v0, "jobScheduler":Landroid/app/job/JobScheduler; */
(( com.android.server.content.SyncManager ) p0 ).getAllPendingSyncs ( ); // invoke-virtual {p0}, Lcom/android/server/content/SyncManager;->getAllPendingSyncs()Ljava/util/List;
/* .line 108 */
/* .local v1, "ops":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/content/SyncOperation;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 109 */
/* .local v2, "count":I */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Lcom/android/server/content/SyncOperation; */
/* .line 110 */
/* .local v4, "op":Lcom/android/server/content/SyncOperation; */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 111 */
/* iget v5, v4, Lcom/android/server/content/SyncOperation;->jobId:I */
(( android.app.job.JobScheduler ) v0 ).cancel ( v5 ); // invoke-virtual {v0, v5}, Landroid/app/job/JobScheduler;->cancel(I)V
/* .line 112 */
/* const-wide/16 v5, 0x0 */
com.android.server.content.SyncManagerAdapter .postScheduleSyncMessage ( p0,v4,v5,v6 );
/* .line 113 */
} // .end local v4 # "op":Lcom/android/server/content/SyncOperation;
/* .line 114 */
} // :cond_1
int v3 = 2; // const/4 v3, 0x2
final String v4 = "SyncManager"; // const-string v4, "SyncManager"
v3 = android.util.Log .isLoggable ( v4,v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 115 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Rescheduled "; // const-string v5, "Rescheduled "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " syncs"; // const-string v5, " syncs"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v4,v3 );
/* .line 117 */
} // :cond_2
return;
} // .end method
private static void rescheduleXiaomiSyncsH ( com.android.server.content.SyncManager p0 ) {
/* .locals 7 */
/* .param p0, "syncManager" # Lcom/android/server/content/SyncManager; */
/* .line 121 */
v0 = this.mActiveSyncContexts;
(( java.util.concurrent.CopyOnWriteArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/content/SyncManager$ActiveSyncContext; */
/* .line 122 */
/* .local v1, "asc":Lcom/android/server/content/SyncManager$ActiveSyncContext; */
v2 = this.mSyncOperation;
v2 = com.android.server.content.MiSyncUtils .checkSyncOperationAccount ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 123 */
v2 = this.mSyncHandler;
final String v3 = "reschedule-xiaomi"; // const-string v3, "reschedule-xiaomi"
(( com.android.server.content.SyncManager$SyncHandler ) v2 ).deferActiveSyncH ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/content/SyncManager$SyncHandler;->deferActiveSyncH(Lcom/android/server/content/SyncManager$ActiveSyncContext;Ljava/lang/String;)V
/* .line 125 */
} // .end local v1 # "asc":Lcom/android/server/content/SyncManager$ActiveSyncContext;
} // :cond_0
/* .line 128 */
} // :cond_1
(( com.android.server.content.SyncManager ) p0 ).getJobScheduler ( ); // invoke-virtual {p0}, Lcom/android/server/content/SyncManager;->getJobScheduler()Landroid/app/job/JobScheduler;
/* .line 129 */
/* .local v0, "jobScheduler":Landroid/app/job/JobScheduler; */
(( com.android.server.content.SyncManager ) p0 ).getAllPendingSyncs ( ); // invoke-virtual {p0}, Lcom/android/server/content/SyncManager;->getAllPendingSyncs()Ljava/util/List;
/* .line 130 */
/* .local v1, "ops":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/content/SyncOperation;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 131 */
/* .local v2, "count":I */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Lcom/android/server/content/SyncOperation; */
/* .line 132 */
/* .local v4, "op":Lcom/android/server/content/SyncOperation; */
v5 = com.android.server.content.MiSyncUtils .checkSyncOperationAccount ( v4 );
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 133 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 134 */
/* iget v5, v4, Lcom/android/server/content/SyncOperation;->jobId:I */
(( android.app.job.JobScheduler ) v0 ).cancel ( v5 ); // invoke-virtual {v0, v5}, Landroid/app/job/JobScheduler;->cancel(I)V
/* .line 135 */
/* const-wide/16 v5, 0x0 */
com.android.server.content.SyncManagerAdapter .postScheduleSyncMessage ( p0,v4,v5,v6 );
/* .line 137 */
} // .end local v4 # "op":Lcom/android/server/content/SyncOperation;
} // :cond_2
/* .line 138 */
} // :cond_3
int v3 = 2; // const/4 v3, 0x2
final String v4 = "SyncManager"; // const-string v4, "SyncManager"
v3 = android.util.Log .isLoggable ( v4,v3 );
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 139 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Rescheduled "; // const-string v5, "Rescheduled "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " syncs"; // const-string v5, " syncs"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .v ( v4,v3 );
/* .line 141 */
} // :cond_4
return;
} // .end method
public static void wrapSyncJobInfo ( android.content.Context p0, com.android.server.content.SyncOperation p1, com.android.server.content.SyncStorageEngine p2, android.app.job.JobInfo$Builder p3, Long p4 ) {
/* .locals 0 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "op" # Lcom/android/server/content/SyncOperation; */
/* .param p2, "syncStorageEngine" # Lcom/android/server/content/SyncStorageEngine; */
/* .param p3, "builder" # Landroid/app/job/JobInfo$Builder; */
/* .param p4, "minDelay" # J */
/* .line 60 */
/* invoke-static/range {p0 ..p5}, Lcom/android/server/content/SyncJobInfoProcessor;->buildSyncJobInfo(Landroid/content/Context;Lcom/android/server/content/SyncOperation;Lcom/android/server/content/SyncStorageEngine;Landroid/app/job/JobInfo$Builder;J)V */
/* .line 61 */
return;
} // .end method
