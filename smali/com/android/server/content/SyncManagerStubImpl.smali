.class public Lcom/android/server/content/SyncManagerStubImpl;
.super Lcom/android/server/content/SyncManagerAccountChangePolicy;
.source "SyncManagerStubImpl.java"

# interfaces
.implements Lcom/android/server/content/SyncManagerStub;


# static fields
.field public static final SYNC_DELAY_ON_DISALLOW_METERED:J = 0x36ee80L

.field private static final TAG:Ljava/lang/String; = "SyncManager"

.field public static final uri:Landroid/net/Uri;


# instance fields
.field mPmi:Lcom/miui/server/process/ProcessManagerInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    const-string/jumbo v0, "sync_on_wifi_only"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/server/content/SyncManagerStubImpl;->uri:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Lcom/android/server/content/SyncManagerAccountChangePolicy;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/content/SyncManagerStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    return-void
.end method

.method public static handleMasterWifiOnlyChanged(Lcom/android/server/content/SyncManager;)V
    .locals 0
    .param p0, "syncManager"    # Lcom/android/server/content/SyncManager;

    .line 86
    invoke-static {p0}, Lcom/android/server/content/MiSyncPolicyManager;->handleMasterWifiOnlyChanged(Lcom/android/server/content/SyncManager;)V

    .line 87
    return-void
.end method

.method public static handleSyncPauseChanged(Landroid/content/Context;Lcom/android/server/content/SyncManager;J)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "syncManager"    # Lcom/android/server/content/SyncManager;
    .param p2, "pauseTimeMills"    # J

    .line 91
    invoke-static {p0, p1, p2, p3}, Lcom/android/server/content/MiSyncPolicyManager;->handleSyncPauseChanged(Landroid/content/Context;Lcom/android/server/content/SyncManager;J)V

    .line 92
    return-void
.end method

.method public static handleSyncPauseChanged(Lcom/android/server/content/SyncManager;)V
    .locals 0
    .param p0, "syncManager"    # Lcom/android/server/content/SyncManager;

    .line 95
    invoke-static {p0}, Lcom/android/server/content/MiSyncPolicyManager;->handleSyncPauseChanged(Lcom/android/server/content/SyncManager;)V

    .line 96
    return-void
.end method

.method public static handleSyncStrategyChanged(Landroid/content/Context;Lcom/android/server/content/SyncManager;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "syncManager"    # Lcom/android/server/content/SyncManager;

    .line 99
    invoke-static {p0, p1}, Lcom/android/server/content/MiSyncPolicyManager;->handleSyncStrategyChanged(Landroid/content/Context;Lcom/android/server/content/SyncManager;)V

    .line 100
    return-void
.end method

.method public static handleSyncStrategyChanged(Lcom/android/server/content/SyncManager;)V
    .locals 0
    .param p0, "syncManager"    # Lcom/android/server/content/SyncManager;

    .line 103
    invoke-static {p0}, Lcom/android/server/content/MiSyncPolicyManager;->handleSyncStrategyChanged(Lcom/android/server/content/SyncManager;)V

    .line 104
    return-void
.end method

.method public static isDisallowMeteredBySettings(Landroid/content/Context;)Z
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .line 32
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sync_on_wifi_only"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method


# virtual methods
.method public canBindService(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Landroid/content/Intent;
    .param p3, "userId"    # I

    .line 28
    invoke-static {}, Lcom/android/server/am/AutoStartManagerServiceStub;->getInstance()Lcom/android/server/am/AutoStartManagerServiceStub;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/android/server/am/AutoStartManagerServiceStub;->isAllowStartService(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    return v0
.end method

.method public getMasterSyncAutomatically(Landroid/accounts/Account;ILcom/android/server/content/SyncStorageEngine;)Z
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "userId"    # I
    .param p3, "syncStorageEngine"    # Lcom/android/server/content/SyncStorageEngine;

    .line 65
    const-string v0, "com.xiaomi"

    .line 66
    .local v0, "XIAOMI_ACCOUNT_TYPE":Ljava/lang/String;
    if-eqz p1, :cond_0

    const-string v1, "com.xiaomi"

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    const/4 v1, 0x1

    return v1

    .line 69
    :cond_0
    invoke-virtual {p3, p2}, Lcom/android/server/content/SyncStorageEngine;->getMasterSyncAutomatically(I)Z

    move-result v1

    return v1
.end method

.method public getSyncDelayedH(Lcom/android/server/content/SyncOperation;Lcom/android/server/content/SyncManager;)J
    .locals 2
    .param p1, "op"    # Lcom/android/server/content/SyncOperation;
    .param p2, "syncManager"    # Lcom/android/server/content/SyncManager;

    .line 44
    invoke-static {p1, p2}, Lcom/android/server/content/MiSyncPolicyManager;->getSyncDelayedH(Lcom/android/server/content/SyncOperation;Lcom/android/server/content/SyncManager;)J

    move-result-wide v0

    return-wide v0
.end method

.method public isRestrictSync(Ljava/lang/String;I[Ljava/lang/Object;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "reserved"    # [Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lcom/android/server/content/SyncManagerStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 74
    const-class v0, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v0, p0, Lcom/android/server/content/SyncManagerStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    .line 75
    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRestrictSync false for service unready uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SyncManager"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return v1

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/android/server/content/SyncManagerStubImpl;->mPmi:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/process/ProcessManagerInternal;->isForegroundApp(Ljava/lang/String;I)Z

    return v1
.end method

.method public registerSyncSettingsObserver(Landroid/content/Context;Lcom/android/server/content/SyncManager;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "syncManager"    # Lcom/android/server/content/SyncManager;

    .line 39
    invoke-static {p1, p2}, Lcom/android/server/content/MiSyncPolicyManager;->registerSyncSettingsObserver(Landroid/content/Context;Lcom/android/server/content/SyncManager;)V

    .line 40
    return-void
.end method

.method public wrapSyncJobInfo(Landroid/content/Context;Lcom/android/server/content/SyncOperation;Lcom/android/server/content/SyncStorageEngine;Landroid/app/job/JobInfo$Builder;J)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "op"    # Lcom/android/server/content/SyncOperation;
    .param p3, "syncStorageEngine"    # Lcom/android/server/content/SyncStorageEngine;
    .param p4, "builder"    # Landroid/app/job/JobInfo$Builder;
    .param p5, "minDelay"    # J

    .line 59
    invoke-static/range {p1 .. p6}, Lcom/android/server/content/MiSyncPolicyManager;->wrapSyncJobInfo(Landroid/content/Context;Lcom/android/server/content/SyncOperation;Lcom/android/server/content/SyncStorageEngine;Landroid/app/job/JobInfo$Builder;J)V

    .line 60
    return-void
.end method
