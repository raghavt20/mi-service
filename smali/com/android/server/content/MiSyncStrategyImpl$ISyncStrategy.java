abstract class com.android.server.content.MiSyncStrategyImpl$ISyncStrategy {
	 /* .source "MiSyncStrategyImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/content/MiSyncStrategyImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x60a */
/* name = "ISyncStrategy" */
} // .end annotation
/* # virtual methods */
public abstract void apply ( com.android.server.content.SyncOperation p0, android.os.Bundle p1, android.app.job.JobInfo$Builder p2 ) {
} // .end method
public abstract Boolean isAllowedToRun ( com.android.server.content.SyncOperation p0, android.os.Bundle p1 ) {
} // .end method
