class com.android.server.FixedFileObserver$1 extends android.os.FileObserver {
	 /* .source "FixedFileObserver.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/FixedFileObserver;->startWatching()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.FixedFileObserver this$0; //synthetic
final java.util.Set val$fixedObservers; //synthetic
/* # direct methods */
 com.android.server.FixedFileObserver$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/FixedFileObserver; */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 33 */
this.this$0 = p1;
this.val$fixedObservers = p3;
/* invoke-direct {p0, p2}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public void onEvent ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "event" # I */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 35 */
v0 = this.val$fixedObservers;
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/android/server/FixedFileObserver; */
/* .line 36 */
/* .local v1, "fixedObserver":Lcom/android/server/FixedFileObserver; */
v2 = com.android.server.FixedFileObserver .-$$Nest$fgetmMask ( v1 );
/* and-int/2addr v2, p1 */
if ( v2 != null) { // if-eqz v2, :cond_0
(( com.android.server.FixedFileObserver ) v1 ).onEvent ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lcom/android/server/FixedFileObserver;->onEvent(ILjava/lang/String;)V
/* .line 37 */
} // .end local v1 # "fixedObserver":Lcom/android/server/FixedFileObserver;
} // :cond_1
return;
} // .end method
