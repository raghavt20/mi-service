public class com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl implements com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStub {
	 /* .source "HeartRateFingerprintServiceStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final java.lang.String TAG;
	 private static android.os.IBinder mHeartRateBinder;
	 /* # instance fields */
	 private android.os.Handler mHandler;
	 private com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController mMiuiFingerprintCloudController;
	 private com.android.server.biometrics.sensors.fingerprint.FingerprintService mService;
	 /* # direct methods */
	 static com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 106 */
		 int v0 = 0; // const/4 v0, 0x0
		 return;
	 } // .end method
	 public com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 101 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 107 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mMiuiFingerprintCloudController = v0;
		 return;
	 } // .end method
	 public static Boolean heartRateDataCallback ( Integer p0, Integer p1, Object[] p2 ) {
		 /* .locals 5 */
		 /* .param p0, "msgId" # I */
		 /* .param p1, "cmdId" # I */
		 /* .param p2, "msg_data" # [B */
		 /* .line 200 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "heartRateDataCallback: msgId: "; // const-string v1, "heartRateDataCallback: msgId: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = " cmdId: "; // const-string v1, " cmdId: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = " msg_data: "; // const-string v1, " msg_data: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 final String v1 = " mHeartRateBinder: "; // const-string v1, " mHeartRateBinder: "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl.mHeartRateBinder;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v1 = "HeartRateFingerprintServiceStubImpl"; // const-string v1, "HeartRateFingerprintServiceStubImpl"
		 android.util.Log .d ( v1,v0 );
		 /* .line 202 */
		 v0 = com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl.mHeartRateBinder;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 203 */
			 android.os.Parcel .obtain ( );
			 /* .line 205 */
			 /* .local v0, "request":Landroid/os/Parcel; */
			 int v1 = 0; // const/4 v1, 0x0
			 try { // :try_start_0
				 final String v2 = "com.android.app.HeartRate"; // const-string v2, "com.android.app.HeartRate"
				 (( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
				 /* .line 206 */
				 (( android.os.Parcel ) v0 ).writeInt ( p0 ); // invoke-virtual {v0, p0}, Landroid/os/Parcel;->writeInt(I)V
				 /* .line 207 */
				 (( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
				 /* .line 208 */
				 (( android.os.Parcel ) v0 ).writeByteArray ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V
				 /* .line 209 */
				 v2 = com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl.mHeartRateBinder;
				 /* const v3, 0xfffffc */
				 int v4 = 1; // const/4 v4, 0x1
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* .line 211 */
				 /* nop */
				 /* .line 216 */
				 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
				 /* .line 211 */
				 /* .line 216 */
				 /* :catchall_0 */
				 /* move-exception v1 */
				 /* .line 212 */
				 /* :catch_0 */
				 /* move-exception v2 */
				 /* .line 213 */
				 /* .local v2, "e":Ljava/lang/Exception; */
				 try { // :try_start_1
					 /* .line 214 */
					 (( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
					 /* :try_end_1 */
					 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
					 /* .line 216 */
				 } // .end local v2 # "e":Ljava/lang/Exception;
				 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
				 /* .line 217 */
				 /* .line 216 */
			 } // :goto_0
			 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
			 /* .line 217 */
			 /* throw v1 */
			 /* .line 219 */
		 } // .end local v0 # "request":Landroid/os/Parcel;
	 } // :cond_0
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void registerCallback ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 186 */
final String v0 = "reg callback"; // const-string v0, "reg callback"
final String v1 = "HeartRateFingerprintServiceStubImpl"; // const-string v1, "HeartRateFingerprintServiceStubImpl"
android.util.Slog .d ( v1,v0 );
/* .line 187 */
android.hardware.fingerprint.MiFxTunnelAidl .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 188 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).getSupportInterfaceVersion ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getSupportInterfaceVersion()I
int v2 = 2; // const/4 v2, 0x2
/* if-ne v0, v2, :cond_0 */
/* .line 190 */
android.hardware.fingerprint.MiFxTunnelAidl .getInstance ( );
v1 = this.mHandler;
(( android.hardware.fingerprint.MiFxTunnelAidl ) v0 ).registerCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->registerCallback(Landroid/os/Handler;)V
/* .line 191 */
} // :cond_0
android.hardware.fingerprint.MiFxTunnelHidl .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 192 */
final String v0 = "get reg"; // const-string v0, "get reg"
android.util.Slog .d ( v1,v0 );
/* .line 193 */
android.hardware.fingerprint.MiFxTunnelHidl .getInstance ( );
v1 = this.mHandler;
(( android.hardware.fingerprint.MiFxTunnelHidl ) v0 ).registerCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->registerCallback(Landroid/os/Handler;)V
/* .line 195 */
} // :cond_1
final String v0 = "get null"; // const-string v0, "get null"
android.util.Slog .d ( v1,v0 );
/* .line 197 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Integer cloudCmd ( android.os.Looper p0, android.content.Context p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "looper" # Landroid/os/Looper; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p3, "cmd" # I */
/* .param p4, "param" # I */
/* .line 158 */
/* packed-switch p3, :pswitch_data_0 */
/* .line 160 */
/* :pswitch_0 */
v0 = this.mMiuiFingerprintCloudController;
/* if-nez v0, :cond_0 */
/* .line 161 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;-><init>(Landroid/os/Looper;Landroid/content/Context;)V */
this.mMiuiFingerprintCloudController = v0;
/* .line 166 */
/* :pswitch_1 */
int v0 = 0; // const/4 v0, 0x0
this.mMiuiFingerprintCloudController = v0;
/* .line 169 */
} // :cond_0
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public android.hardware.fingerprint.HeartRateCmdResult doSendCommand ( Integer p0, Object[] p1 ) {
/* .locals 2 */
/* .param p1, "cmdId" # I */
/* .param p2, "params" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 173 */
android.hardware.fingerprint.MiFxTunnelAidl .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 174 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).getSupportInterfaceVersion ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getSupportInterfaceVersion()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
/* .line 176 */
android.hardware.fingerprint.MiFxTunnelAidl .getInstance ( );
(( android.hardware.fingerprint.MiFxTunnelAidl ) v0 ).sendCommand ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->sendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;
/* .line 177 */
} // :cond_0
android.hardware.fingerprint.MiFxTunnelHidl .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 178 */
android.hardware.fingerprint.MiFxTunnelHidl .getInstance ( );
(( android.hardware.fingerprint.MiFxTunnelHidl ) v0 ).sendCommand ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->sendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;
/* .line 180 */
} // :cond_1
final String v0 = "HeartRateFingerprintServiceStubImpl"; // const-string v0, "HeartRateFingerprintServiceStubImpl"
final String v1 = "get null"; // const-string v1, "get null"
android.util.Slog .d ( v0,v1 );
/* .line 181 */
/* new-instance v0, Landroid/hardware/fingerprint/HeartRateCmdResult; */
/* invoke-direct {v0}, Landroid/hardware/fingerprint/HeartRateCmdResult;-><init>()V */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .line 114 */
final String v0 = "com.android.app.HeartRate"; // const-string v0, "com.android.app.HeartRate"
int v1 = 1; // const/4 v1, 0x1
/* packed-switch p1, :pswitch_data_0 */
/* .line 148 */
/* .line 120 */
/* :pswitch_0 */
try { // :try_start_0
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 121 */
v0 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 122 */
/* .local v0, "cmd":I */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 123 */
/* .local v2, "size":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 124 */
/* .local v3, "val":[B */
/* if-ltz v2, :cond_0 */
/* .line 125 */
/* new-array v4, v2, [B */
/* move-object v3, v4 */
/* .line 126 */
(( android.os.Parcel ) p2 ).readByteArray ( v3 ); // invoke-virtual {p2, v3}, Landroid/os/Parcel;->readByteArray([B)V
/* .line 128 */
} // :cond_0
(( com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl ) p0 ).doSendCommand ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->doSendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;
/* .line 129 */
/* .local v4, "result":Landroid/hardware/fingerprint/HeartRateCmdResult; */
(( android.os.Parcel ) p3 ).writeParcelable ( v4, v1 ); // invoke-virtual {p3, v4, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
/* .line 130 */
/* .line 135 */
} // .end local v0 # "cmd":I
} // .end local v2 # "size":I
} // .end local v3 # "val":[B
} // .end local v4 # "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
/* :pswitch_1 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 136 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
/* .line 137 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->registerCallback()V */
/* .line 138 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 139 */
/* .line 144 */
/* :pswitch_2 */
int v0 = 0; // const/4 v0, 0x0
/* .line 145 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 146 */
/* .line 150 */
/* :catch_0 */
/* move-exception v0 */
/* .line 151 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v1 = "HeartRateFingerprintServiceStubImpl"; // const-string v1, "HeartRateFingerprintServiceStubImpl"
final String v2 = "onTra : "; // const-string v2, "onTra : "
android.util.Slog .d ( v1,v2,v0 );
/* .line 152 */
int v1 = 0; // const/4 v1, 0x0
/* :pswitch_data_0 */
/* .packed-switch 0xfffffd */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
