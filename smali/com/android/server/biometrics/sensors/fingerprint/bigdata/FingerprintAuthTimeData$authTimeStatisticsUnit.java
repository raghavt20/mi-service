class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeStatisticsUnit {
	 /* .source "FingerprintAuthTimeData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "authTimeStatisticsUnit" */
} // .end annotation
/* # instance fields */
public Long authFullTime;
public Long authResultTimeStamp;
public Integer authStep;
public Long captureToAuthResultTime;
public Long fingerDownTimeStamp;
public Long fingerDownToCaptureTime;
public Long startCaptureTimeStamp;
/* # direct methods */
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeStatisticsUnit ( ) {
/* .locals 2 */
/* .line 62 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 63 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I */
/* .line 64 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownTimeStamp:J */
/* .line 65 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->startCaptureTimeStamp:J */
/* .line 66 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authResultTimeStamp:J */
/* .line 68 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authFullTime:J */
/* .line 69 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownToCaptureTime:J */
/* .line 70 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->captureToAuthResultTime:J */
return;
} // .end method
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeStatisticsUnit ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer calculateTime ( ) {
/* .locals 11 */
/* .line 73 */
/* iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authResultTimeStamp:J */
/* iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownTimeStamp:J */
/* sub-long v4, v0, v2 */
/* iput-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authFullTime:J */
/* .line 74 */
/* iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->startCaptureTimeStamp:J */
/* sub-long v2, v4, v2 */
/* iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownToCaptureTime:J */
/* .line 75 */
/* sub-long/2addr v0, v4 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->captureToAuthResultTime:J */
/* .line 77 */
/* const-wide/16 v4, 0x2710 */
/* cmp-long v6, v2, v4 */
int v7 = -1; // const/4 v7, -0x1
final String v8 = "FingerprintAuthTimeData"; // const-string v8, "FingerprintAuthTimeData"
/* if-gtz v6, :cond_3 */
/* const-wide/16 v9, 0x0 */
/* cmp-long v2, v2, v9 */
/* if-gez v2, :cond_0 */
/* .line 81 */
} // :cond_0
/* cmp-long v2, v0, v4 */
/* if-gtz v2, :cond_2 */
/* cmp-long v0, v0, v9 */
/* if-gez v0, :cond_1 */
/* .line 91 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 82 */
} // :cond_2
} // :goto_0
final String v0 = "captureToAuthResultTime time abnormal!!!!!"; // const-string v0, "captureToAuthResultTime time abnormal!!!!!"
android.util.Slog .e ( v8,v0 );
/* .line 83 */
/* .line 78 */
} // :cond_3
} // :goto_1
final String v0 = "fingerDownToCaptureTime time abnormal!!!!!"; // const-string v0, "fingerDownToCaptureTime time abnormal!!!!!"
android.util.Slog .e ( v8,v0 );
/* .line 79 */
} // .end method
public void reset ( ) {
/* .locals 2 */
/* .line 95 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I */
/* .line 96 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownTimeStamp:J */
/* .line 97 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->startCaptureTimeStamp:J */
/* .line 98 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authResultTimeStamp:J */
/* .line 100 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authFullTime:J */
/* .line 101 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownToCaptureTime:J */
/* .line 102 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->captureToAuthResultTime:J */
/* .line 103 */
return;
} // .end method
