.class Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
.super Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
.source "FingerprintUnlockRateData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SystemUIUnlockRateStatistics"
.end annotation


# instance fields
.field public highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

.field public lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

.field public screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

.field public screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

.field public screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

.field public screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

.field public screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

.field public screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 232
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>()V

    .line 223
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 224
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 225
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 226
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 227
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 228
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 229
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 230
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 233
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->name:Ljava/lang/String;

    .line 234
    return-void
.end method


# virtual methods
.method public reset()V
    .locals 1

    .line 237
    invoke-super {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->reset()V

    .line 238
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 239
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 240
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 241
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 242
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 243
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 244
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 245
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 246
    return-void
.end method
