.class Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;
.super Ljava/lang/Object;
.source "FingerprintAuthTimeData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "authTimeStatisticsUnit"
.end annotation


# instance fields
.field public authFullTime:J

.field public authResultTimeStamp:J

.field public authStep:I

.field public captureToAuthResultTime:J

.field public fingerDownTimeStamp:J

.field public fingerDownToCaptureTime:J

.field public startCaptureTimeStamp:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I

    .line 64
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownTimeStamp:J

    .line 65
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->startCaptureTimeStamp:J

    .line 66
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authResultTimeStamp:J

    .line 68
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authFullTime:J

    .line 69
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownToCaptureTime:J

    .line 70
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->captureToAuthResultTime:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;-><init>()V

    return-void
.end method


# virtual methods
.method public calculateTime()I
    .locals 11

    .line 73
    iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authResultTimeStamp:J

    iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownTimeStamp:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authFullTime:J

    .line 74
    iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->startCaptureTimeStamp:J

    sub-long v2, v4, v2

    iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownToCaptureTime:J

    .line 75
    sub-long/2addr v0, v4

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->captureToAuthResultTime:J

    .line 77
    const-wide/16 v4, 0x2710

    cmp-long v6, v2, v4

    const/4 v7, -0x1

    const-string v8, "FingerprintAuthTimeData"

    if-gtz v6, :cond_3

    const-wide/16 v9, 0x0

    cmp-long v2, v2, v9

    if-gez v2, :cond_0

    goto :goto_1

    .line 81
    :cond_0
    cmp-long v2, v0, v4

    if-gtz v2, :cond_2

    cmp-long v0, v0, v9

    if-gez v0, :cond_1

    goto :goto_0

    .line 91
    :cond_1
    const/4 v0, 0x0

    return v0

    .line 82
    :cond_2
    :goto_0
    const-string v0, "captureToAuthResultTime time abnormal!!!!!"

    invoke-static {v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    return v7

    .line 78
    :cond_3
    :goto_1
    const-string v0, "fingerDownToCaptureTime time abnormal!!!!!"

    invoke-static {v8, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return v7
.end method

.method public reset()V
    .locals 2

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I

    .line 96
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownTimeStamp:J

    .line 97
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->startCaptureTimeStamp:J

    .line 98
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authResultTimeStamp:J

    .line 100
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authFullTime:J

    .line 101
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownToCaptureTime:J

    .line 102
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->captureToAuthResultTime:J

    .line 103
    return-void
.end method
