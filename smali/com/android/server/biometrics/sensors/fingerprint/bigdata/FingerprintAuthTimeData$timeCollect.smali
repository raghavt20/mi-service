.class Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
.super Ljava/lang/Object;
.source "FingerprintAuthTimeData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "timeCollect"
.end annotation


# instance fields
.field public avgAuthTime:J

.field public avgCaptureToAuthResultTime:J

.field public avgFingerDownToCaptureTime:J

.field public name:Ljava/lang/String;

.field public totalAuthCount:I

.field public totalAuthTime:J

.field public totalCaptureToAuthResultTime:J

.field public totalFingerDownToCaptureTime:J


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I

    .line 111
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J

    .line 112
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J

    .line 113
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalCaptureToAuthResultTime:J

    .line 115
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgAuthTime:J

    .line 116
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgFingerDownToCaptureTime:J

    .line 117
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgCaptureToAuthResultTime:J

    .line 120
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->name:Ljava/lang/String;

    .line 121
    return-void
.end method


# virtual methods
.method public debug()V
    .locals 0

    .line 151
    return-void
.end method

.method public reset()V
    .locals 2

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I

    .line 136
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J

    .line 137
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J

    .line 138
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalCaptureToAuthResultTime:J

    .line 139
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgAuthTime:J

    .line 140
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgFingerDownToCaptureTime:J

    .line 141
    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgCaptureToAuthResultTime:J

    .line 142
    return-void
.end method

.method public timeProcess()V
    .locals 7

    .line 124
    iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J

    sget-object v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    iget-wide v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authFullTime:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J

    .line 125
    iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J

    sget-object v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    iget-wide v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownToCaptureTime:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J

    .line 126
    iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalCaptureToAuthResultTime:J

    sget-object v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    iget-wide v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->captureToAuthResultTime:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalCaptureToAuthResultTime:J

    .line 128
    iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J

    iget v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I

    int-to-long v5, v4

    div-long/2addr v2, v5

    iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgAuthTime:J

    .line 129
    iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J

    int-to-long v5, v4

    div-long/2addr v2, v5

    iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgFingerDownToCaptureTime:J

    .line 130
    int-to-long v2, v4

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgCaptureToAuthResultTime:J

    .line 131
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->debug()V

    .line 132
    return-void
.end method
