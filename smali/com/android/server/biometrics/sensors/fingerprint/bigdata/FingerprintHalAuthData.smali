.class public Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;
.super Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
.source "FingerprintHalAuthData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FingerprintHalAuthData"

.field private static volatile sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;


# instance fields
.field private UnlockHalInfoKeyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private UnlockHalInfoValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[I>;"
        }
    .end annotation
.end field

.field private consecutiveFailJsonArray:Lorg/json/JSONArray;

.field private consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

.field private halAuthData:Landroid/hardware/fingerprint/HalDataCmdResult;

.field private qualityScoreFailCountArray:[I

.field private qualityScoreSuccCountArray:[I

.field private retryCountArray:[I

.field private unlockHalInfoJson:Lorg/json/JSONObject;

.field private weightScoreFailCountArray:[I

.field private weightScoreSuccCountArray:[I


# direct methods
.method private constructor <init>()V
    .locals 6

    .line 59
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V

    .line 34
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->unlockHalInfoJson:Lorg/json/JSONObject;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    .line 38
    const/16 v0, 0xb

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreSuccCountArray:[I

    .line 39
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreSuccCountArray:[I

    .line 40
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreFailCountArray:[I

    .line 41
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreFailCountArray:[I

    .line 43
    const-string/jumbo v0, "weight_score_fail_count"

    const-string v1, "retry_count_arr"

    const-string v2, "quality_score_succ_count"

    const-string v3, "quality_score_fail_count"

    const-string/jumbo v4, "weight_score_succ_count"

    filled-new-array {v2, v3, v4, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->UnlockHalInfoKeyList:Ljava/util/List;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreSuccCountArray:[I

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreFailCountArray:[I

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreSuccCountArray:[I

    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreFailCountArray:[I

    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    filled-new-array {v1, v2, v3, v4, v5}, [[I

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->UnlockHalInfoValueList:Ljava/util/List;

    .line 439
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    .line 440
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveFailJsonArray:Lorg/json/JSONArray;

    .line 60
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreSuccCountArray:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 61
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreSuccCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 62
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreFailCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 63
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreFailCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 64
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 65
    return-void
.end method

.method private consecutiveUnlockFailDistribution()V
    .locals 12

    .line 515
    const-string v0, "auth_result"

    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "FingerprintHalAuthData"

    if-eqz v1, :cond_8

    const-string v1, "quality_score"

    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 516
    const-string v3, "match_score"

    invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_8

    const-string v4, "fail_reason_retry0"

    invoke-virtual {p0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 517
    const-string v5, "fail_reason_retry1"

    invoke-virtual {p0, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_0

    goto/16 :goto_2

    .line 523
    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 524
    .local v0, "auth_result":Ljava/lang/Integer;
    invoke-virtual {p0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 525
    .local v4, "fail_reason_retry0":Ljava/lang/Integer;
    invoke-virtual {p0, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    .line 526
    .local v5, "fail_reason_retry1":Ljava/lang/Integer;
    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 527
    .local v1, "quality_score":Ljava/lang/Integer;
    invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 528
    .local v3, "match_score":Ljava/lang/Integer;
    iget-object v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget v6, v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    .line 530
    .local v6, "consecutiveFaileCount":I
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x5

    const/4 v9, 0x1

    if-eq v7, v9, :cond_2

    if-ge v6, v8, :cond_2

    .line 531
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget v7, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    if-nez v7, :cond_1

    .line 532
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->currentTime()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->startFailTime:Ljava/lang/String;

    .line 535
    :cond_1
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget-object v7, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry0Reason:[I

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v7, v6

    .line 536
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget-object v7, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry1Reason:[I

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v7, v6

    .line 538
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget-object v7, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedQualityScore:[I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v7, v6

    .line 539
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget-object v7, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedMatchScore:[I

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v7, v6

    .line 540
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    add-int/2addr v8, v9

    iput v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    .line 541
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "consecutiveUnlockFailDistribution, consecutiveFaileCount++: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget v8, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 542
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-eq v7, v9, :cond_3

    if-ge v6, v8, :cond_4

    .line 543
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v7, v9, :cond_7

    const/4 v7, 0x3

    if-lt v6, v7, :cond_7

    .line 544
    :cond_4
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->currentTime()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->endFailTime:Ljava/lang/String;

    .line 545
    iget-object v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    add-int/2addr v8, v9

    iput v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    .line 548
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "times_unlock_fail"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget v8, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 549
    .local v7, "failStr":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    invoke-virtual {v8}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailToJson()Lorg/json/JSONObject;

    move-result-object v8

    .line 550
    .local v8, "temp":Lorg/json/JSONObject;
    iget-object v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveFailJsonArray:Lorg/json/JSONArray;

    if-nez v10, :cond_5

    .line 551
    const-string v10, "consecutiveUnlockFailDistribution: consecutiveFailJsonArray == null "

    invoke-static {v2, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    iput-object v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveFailJsonArray:Lorg/json/JSONArray;

    .line 555
    :cond_5
    if-eqz v8, :cond_6

    iget-object v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget v10, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    if-lez v10, :cond_6

    .line 556
    iget-object v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveFailJsonArray:Lorg/json/JSONArray;

    iget-object v11, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iget v11, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    sub-int/2addr v11, v9

    invoke-virtual {v10, v11, v8}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 560
    .end local v7    # "failStr":Ljava/lang/String;
    .end local v8    # "temp":Lorg/json/JSONObject;
    :cond_6
    goto :goto_0

    .line 558
    :catch_0
    move-exception v7

    .line 559
    .local v7, "e":Lorg/json/JSONException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "consecutiveUnlockFailDistribution :JSONException "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    .end local v7    # "e":Lorg/json/JSONException;
    :goto_0
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    invoke-virtual {v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->reset()V

    goto :goto_1

    .line 563
    :cond_7
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    invoke-virtual {v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->reset()V

    .line 570
    :goto_1
    return-void

    .line 519
    .end local v0    # "auth_result":Ljava/lang/Integer;
    .end local v1    # "quality_score":Ljava/lang/Integer;
    .end local v3    # "match_score":Ljava/lang/Integer;
    .end local v4    # "fail_reason_retry0":Ljava/lang/Integer;
    .end local v5    # "fail_reason_retry1":Ljava/lang/Integer;
    .end local v6    # "consecutiveFaileCount":I
    :cond_8
    :goto_2
    const-string v0, "auth_result or quality_score or match_score or fail_reason_retry0 is null"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    return-void
.end method

.method private getAuthDataFromHal()Z
    .locals 5

    .line 91
    const-string v0, "FingerprintHalAuthData"

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "getAuthDataFromHal"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelAidl;

    move-result-object v2

    const v3, 0x7a121

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getHalData(I[B)Landroid/hardware/fingerprint/HalDataCmdResult;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->halAuthData:Landroid/hardware/fingerprint/HalDataCmdResult;

    .line 93
    iget v2, v2, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultCode:I

    if-eqz v2, :cond_0

    .line 94
    const-string v2, "Get halAuthData null"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    return v1

    .line 97
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 98
    :catch_0
    move-exception v2

    .line 99
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "Get halAuthData error."

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 100
    return v1
.end method

.method public static getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;
    .locals 3

    .line 76
    const-string v0, "FingerprintHalAuthData"

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    if-nez v0, :cond_1

    .line 78
    const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    monitor-enter v0

    .line 79
    :try_start_0
    const-string v1, "FingerprintHalAuthData"

    const-string v2, "getInstance class"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    if-nez v1, :cond_0

    .line 81
    const-string v1, "FingerprintHalAuthData"

    const-string v2, "getInstance new"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;-><init>()V

    sput-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    .line 84
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 86
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    return-object v0
.end method

.method private imageQualityDistribution()V
    .locals 6

    .line 349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "auth_result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "auth_result"

    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ,quality_score: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "quality_score"

    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "FingerprintHalAuthData"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 355
    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 356
    .local v0, "auth_result":Ljava/lang/Integer;
    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 357
    .local v1, "quality_score":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v4, 0x6e

    const/4 v5, 0x1

    if-ne v2, v5, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v4, :cond_1

    .line 358
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreSuccCountArray:[I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    div-int/lit8 v3, v3, 0xa

    aget v4, v2, v3

    add-int/2addr v4, v5

    aput v4, v2, v3

    goto :goto_0

    .line 366
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v4, :cond_2

    .line 367
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreFailCountArray:[I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    div-int/lit8 v3, v3, 0xa

    aget v4, v2, v3

    add-int/2addr v4, v5

    aput v4, v2, v3

    goto :goto_0

    .line 374
    :cond_2
    const-string v2, "quality_score is illegal"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :goto_0
    return-void

    .line 351
    .end local v0    # "auth_result":Ljava/lang/Integer;
    .end local v1    # "quality_score":Ljava/lang/Integer;
    :cond_3
    :goto_1
    const-string v0, "auth_result or quality_score is null"

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    return-void
.end method

.method private parseAndInsertHalInfo(Landroid/hardware/fingerprint/HalDataCmdResult;)Z
    .locals 7
    .param p1, "halData"    # Landroid/hardware/fingerprint/HalDataCmdResult;

    .line 106
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 107
    return v0

    .line 109
    :cond_0
    const-string v1, "persist.vendor.sys.fp.vendor"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "fpType":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseAndInsertHalInfo, fpType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "FingerprintHalAuthData"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    if-eqz v1, :cond_a

    const-string v2, ""

    if-ne v1, v2, :cond_1

    goto/16 :goto_3

    .line 116
    :cond_1
    iget-object v4, p1, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultData:[B

    invoke-virtual {p0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setHalData([B)V

    .line 118
    const-string v4, "goodix_fod"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "goodix_fod6"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "goodix_fod7"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "goodix_fod7s"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0

    .line 127
    :cond_2
    const-string v2, "goodix"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 128
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V

    .line 129
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseGoodixInfo()V

    goto :goto_1

    .line 130
    :cond_3
    const-string v2, "fpc_fod"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 131
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V

    .line 132
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseFpcFodInfo()V

    goto :goto_1

    .line 133
    :cond_4
    const-string v2, "fpc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 134
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V

    .line 135
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseFpcInfo()V

    goto :goto_1

    .line 136
    :cond_5
    const-string v2, "jiiov"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 137
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V

    .line 138
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseJiiovInfo()V

    goto :goto_1

    .line 140
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unknown sensor type: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    return v0

    .line 119
    :cond_7
    :goto_0
    const-string v4, "persist.vendor.sys.fp.bigdata.type"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 120
    .local v4, "bigdataType":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parseAndInsertHalInfo, bigdataType:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    if-eqz v4, :cond_9

    if-ne v4, v2, :cond_8

    goto :goto_2

    .line 125
    :cond_8
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V

    .line 126
    invoke-direct {p0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseGoodixFodInfo(Ljava/lang/String;)V

    .line 127
    .end local v4    # "bigdataType":Ljava/lang/String;
    nop

    .line 143
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 122
    .restart local v4    # "bigdataType":Ljava/lang/String;
    :cond_9
    :goto_2
    return v0

    .line 113
    .end local v4    # "bigdataType":Ljava/lang/String;
    :cond_a
    :goto_3
    return v0
.end method

.method private parseFpcFodInfo()V
    .locals 0

    .line 211
    return-void
.end method

.method private parseFpcInfo()V
    .locals 0

    .line 215
    return-void
.end method

.method private parseGoodixFodInfo(Ljava/lang/String;)V
    .locals 17
    .param p1, "bigdataType"    # Ljava/lang/String;

    .line 148
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "goodix_fod"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "fail_reason_retry2"

    const-string v4, "fail_reason_retry1"

    const-string v5, "fail_reason_retry0"

    const-string v6, "retry_count"

    const-string v7, "img_area"

    const-string v8, "match_score"

    const-string v9, "quality_score"

    const/4 v10, 0x3

    const-string v11, "auth_result"

    const-string v12, "FingerprintHalAuthData"

    const/4 v13, 0x2

    const/4 v14, 0x1

    const/4 v15, 0x0

    if-eqz v2, :cond_0

    .line 149
    const-string v2, "G3S"

    invoke-static {v12, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v11, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 151
    new-array v2, v10, [Ljava/lang/Integer;

    .line 152
    .local v2, "fail_reason_retry":[Ljava/lang/Integer;
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v2, v15

    .line 153
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v2, v14

    .line 154
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v2, v13

    .line 155
    aget-object v11, v2, v15

    invoke-virtual {v0, v5, v11}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 156
    aget-object v5, v2, v14

    invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 157
    aget-object v4, v2, v13

    invoke-virtual {v0, v3, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v9, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 159
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v8, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v7, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string/jumbo v4, "touch_diff"

    invoke-virtual {v0, v4, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 163
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string/jumbo v4, "screen_protector_type"

    invoke-virtual {v0, v4, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 164
    new-array v3, v10, [Ljava/lang/Integer;

    .line 165
    .local v3, "kpi_time_all":[Ljava/lang/Integer;
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v15

    .line 166
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v14

    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v13

    .line 168
    const-string v4, "kpi_time_all0"

    aget-object v5, v3, v15

    invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 169
    const-string v4, "kpi_time_all1"

    aget-object v5, v3, v14

    invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 170
    const-string v4, "kpi_time_all2"

    aget-object v5, v3, v13

    invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 171
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "highlight_flag"

    invoke-virtual {v0, v5, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 173
    .end local v2    # "fail_reason_retry":[Ljava/lang/Integer;
    .end local v3    # "kpi_time_all":[Ljava/lang/Integer;
    goto/16 :goto_0

    :cond_0
    const-string v2, "goodix_fod3u"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    const-string v2, "G3U"

    invoke-static {v12, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v11, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 176
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v9, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v8, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 178
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v6, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 181
    :cond_1
    const-string v2, "goodix_fod7"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    const-string v13, "goodix_fod7s"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 183
    :cond_2
    invoke-static {v12, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v11, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 185
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string/jumbo v11, "weight_score"

    invoke-virtual {v0, v11, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v9, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 187
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v8, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 188
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 190
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v6, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 191
    new-array v2, v10, [Ljava/lang/Integer;

    .line 192
    .restart local v2    # "fail_reason_retry":[Ljava/lang/Integer;
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v15

    .line 193
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v14

    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x2

    aput-object v6, v2, v7

    .line 195
    aget-object v6, v2, v15

    invoke-virtual {v0, v5, v6}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 196
    aget-object v5, v2, v14

    invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 197
    aget-object v4, v2, v7

    invoke-virtual {v0, v3, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 203
    .end local v2    # "fail_reason_retry":[Ljava/lang/Integer;
    :cond_3
    :goto_0
    return-void
.end method

.method private parseGoodixInfo()V
    .locals 0

    .line 207
    return-void
.end method

.method private parseJiiovInfo()V
    .locals 3

    .line 218
    const-string v0, "FingerprintHalAuthData"

    const-string v1, "parseJiiovInfo"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/16 v0, 0xff

    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "image_name"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 222
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 223
    .local v0, "auth_result":Ljava/lang/Integer;
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    if-nez v1, :cond_0

    .line 224
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 226
    :cond_0
    const-string v1, "auth_result"

    invoke-virtual {p0, v1, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 227
    const-string v1, "algo_version"

    const/16 v2, 0x20

    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 228
    const-string v1, "project_version"

    const/16 v2, 0x20

    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 229
    const-string v1, "chip_id"

    const/16 v2, 0x20

    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 231
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "finger_quality_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 232
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compare_cache_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 233
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "seg_feat_mean"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 235
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    div-int/lit8 v1, v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "quality_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 236
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fp_direction"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 238
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "retry_count"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 239
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "finger_light_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 240
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_studied"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "enrolled_template_count"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 242
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fp_protector_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 244
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fp_temperature_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 245
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "finger_live_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 246
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "matched_template_idx"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 248
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    div-int/lit8 v1, v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "match_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 249
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "img_variance"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 251
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "img_contrast"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 252
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "finger_strange_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 253
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "debase_quality_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 254
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "debase_has_pattern"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 255
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "matched_image_sum"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 257
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "matched_user_id"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 258
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "matched_finger_id"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 259
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "mat_s"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 260
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "mat_d"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 261
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "mat_c"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 262
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "mat_cs"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 264
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_cls_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 265
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_tnum_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 266
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_area_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 267
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_island_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 268
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_image_pixel_1"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 269
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_image_pixel_2"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 270
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "verify_index"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 271
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "env_light"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 273
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_abnormal_expo"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 274
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fusion_cnt"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 275
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "expo_time"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 276
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ghost_cache_behavior"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 277
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "dynamic_liveness_th"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 278
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "hbm_time"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 279
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "keyghost_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 280
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "ctnghost_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 283
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "small_cls_fast_reject_count"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 284
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "small_cls_fast_accept_count"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 285
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "tnum_fast_accept_count"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 286
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "glb_fast_reject_count"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 287
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "lf_fast_reject_count"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 288
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "total_cmp_cls_times"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 289
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "total_cache_count"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 290
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_seg_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 292
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "algo_internal_failed_reason"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 293
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_double_ghost_score"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 294
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "ta_time"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "study_time"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 297
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "signal"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 298
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "noise"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 299
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "hi_freq"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 300
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "screen_leak_ratio"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 302
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fov"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 303
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "shading"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 304
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "cal_expo_time"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 305
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "magnification"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 309
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "algo_status0"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 310
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "raw_img_sum0"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 311
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_image_pixel0_3"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 312
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "capture_time0"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 313
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "extract_time0"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 314
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "verify_time0"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 315
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "kpi_time0"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 319
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "algo_status1"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 320
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "raw_img_sum1"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 321
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_image_pixel1_3"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 322
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "capture_time1"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 323
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "extract_time1"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 324
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "verify_time1"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 325
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "kpi_time1"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 327
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "algo_status2"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 328
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "raw_img_sum2"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 329
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "compress_image_pixel2_3"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 330
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "capture_time2"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 331
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "extract_time2"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 332
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "verify_time2"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 333
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "kpi_time2"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 335
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "finger_up_down_time"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 336
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "screen_off"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 337
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "finger_status"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 338
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "image_special_data"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 339
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "area_ratio"

    invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 344
    return-void
.end method

.method private ratioOfRetry()V
    .locals 7

    .line 408
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ratioOfRetry: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "retry_count"

    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "FingerprintHalAuthData"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    const-string v0, "auth_result"

    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    goto :goto_1

    .line 414
    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 415
    .local v0, "auth_result":Ljava/lang/Integer;
    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 417
    .local v1, "retry_count":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_2

    .line 418
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_1

    .line 419
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    aget v5, v2, v4

    add-int/2addr v5, v4

    aput v5, v2, v4

    .line 421
    :cond_1
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    aget v5, v2, v3

    add-int/2addr v5, v4

    aput v5, v2, v3

    goto :goto_0

    .line 422
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_4

    .line 423
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 424
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    const/4 v5, 0x3

    aget v6, v2, v5

    add-int/2addr v6, v4

    aput v6, v2, v5

    .line 426
    :cond_3
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    aget v5, v2, v3

    add-int/2addr v5, v4

    aput v5, v2, v3

    .line 427
    const/4 v3, 0x2

    aget v5, v2, v3

    add-int/2addr v5, v4

    aput v5, v2, v3

    .line 436
    :cond_4
    :goto_0
    return-void

    .line 410
    .end local v0    # "auth_result":Ljava/lang/Integer;
    .end local v1    # "retry_count":Ljava/lang/Integer;
    :cond_5
    :goto_1
    const-string v0, "auth_result or retry_count is null"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    return-void
.end method

.method private weightScoreDistribution()V
    .locals 6

    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "auth_result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "auth_result"

    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ,weight_score: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "weight_score"

    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "FingerprintHalAuthData"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 385
    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 386
    .local v0, "auth_result":Ljava/lang/Integer;
    invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 387
    .local v1, "weight_score":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v4, 0x6e

    const/4 v5, 0x1

    if-ne v2, v5, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v4, :cond_1

    .line 388
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreSuccCountArray:[I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    div-int/lit8 v3, v3, 0xa

    aget v4, v2, v3

    add-int/2addr v4, v5

    aput v4, v2, v3

    goto :goto_0

    .line 394
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v4, :cond_2

    .line 395
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreFailCountArray:[I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    div-int/lit8 v3, v3, 0xa

    aget v4, v2, v3

    add-int/2addr v4, v5

    aput v4, v2, v3

    goto :goto_0

    .line 402
    :cond_2
    const-string/jumbo v2, "weight_score is illegal"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :goto_0
    return-void

    .line 381
    .end local v0    # "auth_result":Ljava/lang/Integer;
    .end local v1    # "weight_score":Ljava/lang/Integer;
    :cond_3
    :goto_1
    const-string v0, "auth_result or weight_score is null"

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    return-void
.end method


# virtual methods
.method public calculateHalAuthInfo()Z
    .locals 2

    .line 575
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getAuthDataFromHal()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 576
    return v1

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->halAuthData:Landroid/hardware/fingerprint/HalDataCmdResult;

    invoke-direct {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseAndInsertHalInfo(Landroid/hardware/fingerprint/HalDataCmdResult;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 580
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->clearLocalInfo()V

    .line 581
    return v1

    .line 584
    :cond_1
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->imageQualityDistribution()V

    .line 585
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreDistribution()V

    .line 586
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->ratioOfRetry()V

    .line 587
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFailDistribution()V

    .line 588
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->clearLocalInfo()V

    .line 589
    const/4 v0, 0x1

    return v0
.end method

.method public resetLocalFpInfo()V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreSuccCountArray:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 69
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreSuccCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 70
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreFailCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 71
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreFailCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 72
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 73
    return-void
.end method

.method public resetLocalInfo()V
    .locals 2

    .line 657
    const-string v0, "FingerprintHalAuthData"

    const-string v1, "resetLocalInfo"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreSuccCountArray:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 659
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreSuccCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 660
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreFailCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 661
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreFailCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 662
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 663
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    .line 664
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFail:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->reset()V

    .line 665
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveFailJsonArray:Lorg/json/JSONArray;

    .line 666
    return-void
.end method

.method public updateDataToJson(Lorg/json/JSONObject;)Z
    .locals 3
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 630
    const-string/jumbo v0, "updateDataToJson"

    const-string v1, "FingerprintHalAuthData"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    if-nez v0, :cond_0

    .line 632
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    move-result-object v0

    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    .line 635
    :cond_0
    if-nez p1, :cond_1

    .line 636
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    move-object p1, v0

    .line 640
    :cond_1
    :try_start_0
    const-string v0, "quality_score_succ_count"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreSuccCountArray:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 641
    const-string v0, "quality_score_fail_count"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->qualityScoreFailCountArray:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 642
    const-string/jumbo v0, "weight_score_succ_count"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreSuccCountArray:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 643
    const-string/jumbo v0, "weight_score_fail_count"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreFailCountArray:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 644
    const-string v0, "retry_count_arr"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->retryCountArray:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 645
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveFailJsonArray:Lorg/json/JSONArray;

    if-eqz v0, :cond_2

    .line 646
    const-string v2, "consecutive_fail_arr"

    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 652
    :cond_2
    nop

    .line 653
    const/4 v0, 0x1

    return v0

    .line 649
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Lorg/json/JSONException;
    const-string/jumbo v2, "updateDataToJson JSONException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 651
    const/4 v1, 0x0

    return v1
.end method

.method public updateJsonToData(Lorg/json/JSONObject;)Z
    .locals 13
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 595
    const-string v0, ""

    const-string v1, "FingerprintHalAuthData"

    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->UnlockHalInfoKeyList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 596
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->UnlockHalInfoKeyList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 597
    .local v4, "tempString":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateJsonToData: tempString: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 599
    const-string v5, "["

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 600
    const-string v5, "]"

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 601
    const-string v5, " "

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 602
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "updateJsonToData: new tempString: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    const/4 v5, 0x0

    .line 604
    .local v5, "secondTempIndex":I
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v8, v3

    :goto_1
    if-ge v8, v7, :cond_1

    aget-object v9, v6, v8

    .line 605
    .local v9, "splitstring":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    iget-object v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->UnlockHalInfoValueList:Ljava/util/List;

    invoke-interface {v10, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [I

    array-length v10, v10

    if-ge v5, v10, :cond_0

    .line 606
    iget-object v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->UnlockHalInfoValueList:Ljava/util/List;

    invoke-interface {v10, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [I

    add-int/lit8 v11, v5, 0x1

    .end local v5    # "secondTempIndex":I
    .local v11, "secondTempIndex":I
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    aput v12, v10, v5

    move v5, v11

    .line 604
    .end local v9    # "splitstring":Ljava/lang/String;
    .end local v11    # "secondTempIndex":I
    .restart local v5    # "secondTempIndex":I
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 595
    .end local v5    # "secondTempIndex":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 612
    .end local v2    # "index":I
    .end local v4    # "tempString":Ljava/lang/String;
    :cond_2
    const-string v0, "consecutive_fail_arr"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveFailJsonArray:Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 625
    nop

    .line 626
    const/4 v0, 0x1

    return v0

    .line 622
    :catch_0
    move-exception v0

    .line 623
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "updateJsonToData Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 624
    return v3

    .line 618
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 619
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string/jumbo v2, "updateJsonToData IndexOutOfBoundsException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 620
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->resetLocalInfo()V

    .line 621
    return v3

    .line 614
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_2
    move-exception v0

    .line 615
    .local v0, "e":Lorg/json/JSONException;
    const-string/jumbo v2, "updateJsonToData JSONException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 616
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->resetLocalInfo()V

    .line 617
    return v3
.end method
