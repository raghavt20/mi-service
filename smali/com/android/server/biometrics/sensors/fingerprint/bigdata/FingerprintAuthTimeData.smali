.class public Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;
.super Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
.source "FingerprintAuthTimeData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;,
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;,
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;,
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
    }
.end annotation


# static fields
.field private static final AUTH_FAIL:I = 0x0

.field private static final AUTH_STEP_FINGERDOWN:I = 0x1

.field private static final AUTH_STEP_NONE:I = 0x0

.field private static final AUTH_STEP_RESULT:I = 0x3

.field private static final AUTH_STEP_UI_READY:I = 0x2

.field private static final AUTH_SUCCESS:I = 0x1

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_FINGER_DOWN:I = 0x16

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_FINGER_UP:I = 0x17

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_HIGHLIGHT_CAPTURE_START:I = 0x14

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_LOWLIGHT_CAPTURE_START:I = 0x32

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_UNLOCK_SHOW_WINDOW:I = 0x13

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_WAIT_FINGER_INPUT:I = 0x15

.field private static final HBM:I = 0x0

.field private static final LOW_BRIGHT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "FingerprintAuthTimeData"

.field public static sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

.field private static volatile sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;


# instance fields
.field private authFailTime:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

.field private authFailTimeJson:Lorg/json/JSONObject;

.field private authResultTimeJsonKeyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private authResultTimeJsonList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private authResultTimeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;",
            ">;"
        }
    .end annotation
.end field

.field private authSuccessTime:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

.field private authSuccessTimeJson:Lorg/json/JSONObject;

.field private authTimeJson:Lorg/json/JSONObject;

.field private mFingerUnlockBright:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 106
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit-IA;)V

    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 20
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mFingerUnlockBright:I

    .line 227
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    const-string v1, "authFailTime"

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authFailTime:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    .line 228
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    const-string v1, "authSuccessTime"

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authSuccessTime:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    .line 230
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authTimeJson:Lorg/json/JSONObject;

    .line 231
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authSuccessTimeJson:Lorg/json/JSONObject;

    .line 232
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authFailTimeJson:Lorg/json/JSONObject;

    .line 234
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authFailTime:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authSuccessTime:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    filled-new-array {v1, v2}, [Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeList:Ljava/util/List;

    .line 239
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authFailTimeJson:Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authSuccessTimeJson:Lorg/json/JSONObject;

    filled-new-array {v1, v2}, [Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeJsonList:Ljava/util/List;

    .line 244
    const-string v0, "auth_fail_time"

    const-string v1, "auth_success_time"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeJsonKeyList:Ljava/util/List;

    return-void
.end method

.method public static getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;
    .locals 3

    .line 45
    const-string v0, "FingerprintAuthTimeData"

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    if-nez v0, :cond_1

    .line 47
    const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    monitor-enter v0

    .line 48
    :try_start_0
    const-string v1, "FingerprintAuthTimeData"

    const-string v2, "getInstance class"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    if-nez v1, :cond_0

    .line 50
    const-string v1, "FingerprintAuthTimeData"

    const-string v2, "getInstance new"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;-><init>()V

    sput-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    .line 53
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 55
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    return-object v0
.end method


# virtual methods
.method public calculateAuthTime(II)I
    .locals 6
    .param p1, "authen"    # I
    .param p2, "screenState"    # I

    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calculateAuthTime, authen: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", screenState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintAuthTimeData"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iput p2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mScreenStatus:I

    .line 289
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    iget v0, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I

    const/4 v2, 0x2

    const/4 v3, 0x3

    if-ne v0, v2, :cond_0

    .line 290
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    iput v3, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I

    .line 291
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authResultTimeStamp:J

    .line 292
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->calculateTime()I

    move-result v0

    if-gez v0, :cond_0

    .line 293
    const-string v0, "calculate auth time abnormal, drop it!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v0, -0x1

    return v0

    .line 298
    :cond_0
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    iget v0, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I

    if-ne v0, v3, :cond_1

    .line 303
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    iget-object v0, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusList:Ljava/util/List;

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mScreenStatus:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    .line 304
    .local v0, "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
    iget-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusList:Ljava/util/List;

    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mFingerUnlockBright:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;

    iget v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I

    .line 305
    iget-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusList:Ljava/util/List;

    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mFingerUnlockBright:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;

    invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->timeProcess()V

    .line 307
    .end local v0    # "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
    :cond_1
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->reset()V

    .line 308
    const/4 v0, 0x0

    return v0
.end method

.method public handleAcquiredInfo(II)V
    .locals 4
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I

    .line 256
    const-string v0, "FingerprintAuthTimeData"

    const-string v1, "handleAcquiredInfo"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    if-nez p1, :cond_0

    .line 258
    return-void

    .line 261
    :cond_0
    const/4 v0, 0x6

    if-ne p1, v0, :cond_3

    .line 262
    const/4 v0, 0x1

    sparse-switch p2, :sswitch_data_0

    goto :goto_1

    .line 265
    :sswitch_0
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->reset()V

    .line 266
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    iput v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I

    .line 267
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownTimeStamp:J

    .line 268
    goto :goto_1

    .line 271
    :sswitch_1
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    iget v1, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I

    if-ne v1, v0, :cond_1

    .line 272
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    const/4 v2, 0x2

    iput v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I

    .line 273
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->startCaptureTimeStamp:J

    goto :goto_0

    .line 275
    :cond_1
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->sAuthTimeUnit:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;

    invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->reset()V

    .line 277
    :goto_0
    const/16 v1, 0x14

    if-ne p2, v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mFingerUnlockBright:I

    .line 278
    nop

    .line 283
    :cond_3
    :goto_1
    return-void

    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_1
        0x16 -> :sswitch_0
        0x32 -> :sswitch_1
    .end sparse-switch
.end method

.method public resetLocalInfo()V
    .locals 2

    .line 387
    const-string v0, "FingerprintAuthTimeData"

    const-string v1, "resetLocalInfo"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    .line 389
    .local v1, "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->reset()V

    .line 390
    .end local v1    # "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    goto :goto_0

    .line 391
    :cond_0
    return-void
.end method

.method public updateDataToJson(Lorg/json/JSONObject;)Z
    .locals 16
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 312
    move-object/from16 v1, p0

    const-string/jumbo v0, "updateAuthTimeJson"

    const-string v2, "FingerprintAuthTimeData"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    const/4 v0, 0x0

    .local v0, "tempIndex":I
    :goto_0
    :try_start_0
    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 316
    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    .line 317
    .local v3, "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    iget-object v4, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeJsonList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    .line 318
    .local v4, "tempAuthResultTimeJson":Lorg/json/JSONObject;
    iget-object v5, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeJsonKeyList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 319
    .local v5, "tempAuthResultTimeJsonKey":Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "secondTempIndex":I
    :goto_1
    iget-object v7, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3

    if-ge v6, v7, :cond_1

    .line 320
    :try_start_1
    iget-object v7, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusList:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    .line 321
    .local v7, "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
    iget-object v8, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusJsonList:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    .line 322
    .local v8, "tempScreenStatusJson":Lorg/json/JSONObject;
    invoke-static {v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->-$$Nest$fgetscreenStatusJsonKeyList(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 323
    .local v9, "tempScreenStatusJsonKey":Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "thirdTempIndex":I
    :goto_2
    iget-object v11, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v10, v11, :cond_0

    .line 324
    iget-object v11, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusList:Ljava/util/List;

    invoke-interface {v11, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;

    .line 325
    .local v11, "tempTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
    iget-object v12, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusJsonList:Ljava/util/List;

    invoke-interface {v12, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/json/JSONObject;

    .line 326
    .local v12, "tempLightStatusJson":Lorg/json/JSONObject;
    iget-object v13, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusJsonKeyList:Ljava/util/List;

    invoke-interface {v13, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 328
    .local v13, "tempLightStatusJsonKey":Ljava/lang/String;
    const-string/jumbo v14, "total_auth_count"

    iget v15, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I

    invoke-virtual {v12, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 329
    const-string/jumbo v14, "total_auth_time"
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v15, v2

    :try_start_2
    iget-wide v1, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J

    invoke-virtual {v12, v14, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 330
    const-string/jumbo v1, "total_down_to_capture_time"

    move-object v14, v3

    .end local v3    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    .local v14, "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J

    invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 331
    const-string/jumbo v1, "total_capture_to_result_time"

    iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalCaptureToAuthResultTime:J

    invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 332
    const-string v1, "avg_auth_time"

    iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgAuthTime:J

    invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 333
    const-string v1, "avg_down_to_capture_time"

    iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgFingerDownToCaptureTime:J

    invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 334
    const-string v1, "avg_capture_to_result_time"

    iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgCaptureToAuthResultTime:J

    invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 336
    invoke-virtual {v8, v13, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 323
    nop

    .end local v11    # "tempTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
    .end local v12    # "tempLightStatusJson":Lorg/json/JSONObject;
    .end local v13    # "tempLightStatusJsonKey":Ljava/lang/String;
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v1, p0

    move-object v3, v14

    move-object v2, v15

    goto :goto_2

    .end local v14    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    .restart local v3    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    :cond_0
    move-object v15, v2

    move-object v14, v3

    .line 338
    .end local v3    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    .end local v10    # "thirdTempIndex":I
    .restart local v14    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    invoke-virtual {v4, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 319
    nop

    .end local v7    # "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
    .end local v8    # "tempScreenStatusJson":Lorg/json/JSONObject;
    .end local v9    # "tempScreenStatusJsonKey":Ljava/lang/String;
    add-int/lit8 v6, v6, 0x1

    move-object/from16 v1, p0

    move-object v3, v14

    move-object v2, v15

    goto/16 :goto_1

    .line 342
    .end local v0    # "tempIndex":I
    .end local v4    # "tempAuthResultTimeJson":Lorg/json/JSONObject;
    .end local v5    # "tempAuthResultTimeJsonKey":Ljava/lang/String;
    .end local v6    # "secondTempIndex":I
    .end local v14    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v15, v2

    :goto_3
    move-object/from16 v1, p1

    goto :goto_4

    .line 319
    .restart local v0    # "tempIndex":I
    .restart local v3    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    .restart local v4    # "tempAuthResultTimeJson":Lorg/json/JSONObject;
    .restart local v5    # "tempAuthResultTimeJsonKey":Ljava/lang/String;
    .restart local v6    # "secondTempIndex":I
    :cond_1
    move-object v15, v2

    move-object v14, v3

    .line 340
    .end local v3    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    .end local v6    # "secondTempIndex":I
    .restart local v14    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    move-object/from16 v1, p1

    :try_start_3
    invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    .line 314
    nop

    .end local v4    # "tempAuthResultTimeJson":Lorg/json/JSONObject;
    .end local v5    # "tempAuthResultTimeJsonKey":Ljava/lang/String;
    .end local v14    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    add-int/lit8 v0, v0, 0x1

    move-object/from16 v1, p0

    move-object v2, v15

    goto/16 :goto_0

    .line 342
    .end local v0    # "tempIndex":I
    :catch_2
    move-exception v0

    goto :goto_4

    .line 314
    .restart local v0    # "tempIndex":I
    :cond_2
    move-object/from16 v1, p1

    .line 345
    .end local v0    # "tempIndex":I
    nop

    .line 346
    const/4 v0, 0x1

    return v0

    .line 342
    :catch_3
    move-exception v0

    move-object/from16 v1, p1

    move-object v15, v2

    .line 343
    .local v0, "e":Lorg/json/JSONException;
    :goto_4
    const-string/jumbo v2, "updateAuthTimeJson exception"

    move-object v3, v15

    invoke-static {v3, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 344
    const/4 v2, 0x0

    return v2
.end method

.method public updateJsonToData(Lorg/json/JSONObject;)Z
    .locals 13
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 350
    const-string/jumbo v0, "updateJsonToData"

    const-string v1, "FingerprintAuthTimeData"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    const/4 v0, 0x0

    .local v0, "tempIndex":I
    :goto_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 354
    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;

    .line 355
    .local v3, "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->authResultTimeJsonKeyList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 357
    .local v4, "tempAuthResultTimeJson":Lorg/json/JSONObject;
    const/4 v5, 0x0

    .local v5, "secondTempIndex":I
    :goto_1
    invoke-static {v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->-$$Nest$fgetscreenStatusJsonKeyList(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 358
    iget-object v6, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusList:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    .line 359
    .local v6, "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
    invoke-static {v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->-$$Nest$fgetscreenStatusJsonKeyList(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 361
    .local v7, "tempScreenStatusTimeJson":Lorg/json/JSONObject;
    const/4 v8, 0x0

    .local v8, "thirdTempIndex":I
    :goto_2
    iget-object v9, v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_0

    .line 362
    iget-object v9, v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusList:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;

    .line 363
    .local v9, "tempTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
    iget-object v10, v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->lightStatusJsonKeyList:Ljava/util/List;

    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 364
    .local v10, "tempLightStatusJson":Lorg/json/JSONObject;
    const-string/jumbo v11, "total_auth_count"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    iput v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I

    .line 365
    const-string/jumbo v11, "total_auth_time"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    int-to-long v11, v11

    iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J

    .line 366
    const-string/jumbo v11, "total_down_to_capture_time"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    int-to-long v11, v11

    iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J

    .line 367
    const-string/jumbo v11, "total_capture_to_result_time"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    int-to-long v11, v11

    iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalCaptureToAuthResultTime:J

    .line 368
    const-string v11, "avg_auth_time"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    int-to-long v11, v11

    iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgAuthTime:J

    .line 369
    const-string v11, "avg_down_to_capture_time"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    int-to-long v11, v11

    iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgFingerDownToCaptureTime:J

    .line 370
    const-string v11, "avg_capture_to_result_time"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    int-to-long v11, v11

    iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgCaptureToAuthResultTime:J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    .end local v9    # "tempTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
    .end local v10    # "tempLightStatusJson":Lorg/json/JSONObject;
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 357
    .end local v6    # "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
    .end local v7    # "tempScreenStatusTimeJson":Lorg/json/JSONObject;
    .end local v8    # "thirdTempIndex":I
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 353
    .end local v3    # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
    .end local v4    # "tempAuthResultTimeJson":Lorg/json/JSONObject;
    .end local v5    # "secondTempIndex":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 382
    :cond_2
    nop

    .line 383
    const/4 v1, 0x1

    return v1

    .line 378
    .end local v0    # "tempIndex":I
    :catch_0
    move-exception v0

    .line 379
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string/jumbo v3, "updateJsonToData IndexOutOfBoundsException"

    invoke-static {v1, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 380
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->resetLocalInfo()V

    .line 381
    return v2

    .line 374
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 375
    .local v0, "e":Lorg/json/JSONException;
    const-string/jumbo v3, "updateJsonToData exception"

    invoke-static {v1, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 376
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->resetLocalInfo()V

    .line 377
    return v2
.end method
