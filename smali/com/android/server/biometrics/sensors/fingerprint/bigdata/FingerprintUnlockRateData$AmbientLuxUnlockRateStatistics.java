class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics extends com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AppUnlockRateStatistic {
	 /* .source "FingerprintUnlockRateData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "AmbientLuxUnlockRateStatistics" */
} // .end annotation
/* # instance fields */
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock0;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock1;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock2;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock3;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock4;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock5;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock6;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock7;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit luxLightUnlock8;
public org.json.JSONArray luxLightUnlockJsonArray;
public java.util.List luxLightUnlockList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics ( ) {
/* .locals 10 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 198 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>()V */
/* .line 162 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.luxLightUnlockJsonArray = v0;
/* .line 163 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock0 = v0;
/* .line 164 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock1 = v0;
/* .line 165 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock2 = v0;
/* .line 166 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock3 = v0;
/* .line 167 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock4 = v0;
/* .line 168 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock5 = v0;
/* .line 169 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock6 = v0;
/* .line 170 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock7 = v0;
/* .line 171 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.luxLightUnlock8 = v0;
/* .line 173 */
/* new-instance v0, Ljava/util/ArrayList; */
v1 = this.luxLightUnlock0;
v2 = this.luxLightUnlock1;
v3 = this.luxLightUnlock2;
v4 = this.luxLightUnlock3;
v5 = this.luxLightUnlock4;
v6 = this.luxLightUnlock5;
v7 = this.luxLightUnlock6;
v8 = this.luxLightUnlock7;
v9 = this.luxLightUnlock8;
/* filled-new-array/range {v1 ..v9}, [Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.luxLightUnlockList = v0;
/* .line 199 */
this.name = p1;
/* .line 200 */
return;
} // .end method
/* # virtual methods */
public void addLuxAuthCount ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "luxLevel" # I */
/* .param p2, "authen" # I */
/* .line 186 */
v0 = v0 = this.luxLightUnlockList;
/* if-ge p1, v0, :cond_2 */
/* if-gez p1, :cond_0 */
/* .line 189 */
} // :cond_0
v0 = this.luxLightUnlockList;
/* check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* iget v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
int v2 = 1; // const/4 v2, 0x1
/* add-int/2addr v1, v2 */
/* iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 190 */
/* if-ne p2, v2, :cond_1 */
/* .line 191 */
v0 = this.luxLightUnlockList;
/* check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* iget v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v1, v2 */
/* iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 196 */
} // :cond_1
return;
/* .line 187 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void printAmbientLuxUnlock ( ) {
/* .locals 5 */
/* .line 203 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "index":I */
} // :goto_0
v1 = v1 = this.luxLightUnlockList;
/* if-ge v0, v1, :cond_0 */
/* .line 204 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "printAmbientLuxUnlock,authCount index: "; // const-string v2, "printAmbientLuxUnlock,authCount index: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.luxLightUnlockList;
/* check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "FingerprintUnlockRateData"; // const-string v3, "FingerprintUnlockRateData"
android.util.Slog .d ( v3,v1 );
/* .line 205 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "printAmbientLuxUnlock,authSuccessCount: "; // const-string v4, "printAmbientLuxUnlock,authSuccessCount: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.luxLightUnlockList;
/* check-cast v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* iget v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 203 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 207 */
} // .end local v0 # "index":I
} // :cond_0
return;
} // .end method
public void reset ( ) {
/* .locals 2 */
/* .line 210 */
/* invoke-super {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->reset()V */
/* .line 211 */
v0 = this.luxLightUnlockList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* .line 212 */
/* .local v1, "luxLightUnlock":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v1 ).reset ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 213 */
} // .end local v1 # "luxLightUnlock":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;
/* .line 214 */
} // :cond_0
return;
} // .end method
