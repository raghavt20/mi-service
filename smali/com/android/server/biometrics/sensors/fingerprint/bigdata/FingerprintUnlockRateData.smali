.class public Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;
.super Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
.source "FingerprintUnlockRateData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;,
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;,
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;,
        Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    }
.end annotation


# static fields
.field protected static final HBM:I = 0x0

.field private static final SYSTEMUI_NAME:Ljava/lang/String; = "com.android.systemui"

.field private static final TAG:Ljava/lang/String; = "FingerprintUnlockRateData"

.field private static volatile sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;


# instance fields
.field private final LUX_LEVEL_UNDEFINED:I

.field private final LUX_LIGHT_LEVEL0:I

.field private final LUX_LIGHT_LEVEL1:I

.field private final LUX_LIGHT_LEVEL2:I

.field private final LUX_LIGHT_LEVEL3:I

.field private final LUX_LIGHT_LEVEL4:I

.field private final LUX_LIGHT_LEVEL5:I

.field private final LUX_LIGHT_LEVEL6:I

.field private final LUX_LIGHT_LEVEL7:I

.field private final LUX_LIGHT_LEVEL8:I

.field private appAuthMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;",
            ">;"
        }
    .end annotation
.end field

.field private appUnlockRateArray:Lorg/json/JSONArray;

.field luxLightOtherAuthSuccCountArray:Lorg/json/JSONArray;

.field luxLightOtherSyAuthCountArray:Lorg/json/JSONArray;

.field luxLightSystemuiAuthSuccCountArray:Lorg/json/JSONArray;

.field luxLightSystemuiSyAuthCountArray:Lorg/json/JSONArray;

.field private mALSProbe:Lcom/android/server/biometrics/log/ALSProbe;

.field private mAmbientLux:I

.field private mContext:Landroid/content/Context;

.field private mFingerUnlockBright:I

.field private mProbeIsEnable:I

.field private mScreenStatus:I

.field private otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

.field private sAppStatisticList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;",
            ">;"
        }
    .end annotation
.end field

.field private sPackageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private sensorManager:Landroid/hardware/SensorManager;

.field private systemuiLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

.field private systemuiUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

.field private totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;


# direct methods
.method private constructor <init>()V
    .locals 6

    .line 120
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mScreenStatus:I

    .line 27
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I

    .line 29
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LEVEL_UNDEFINED:I

    .line 30
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL0:I

    .line 31
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL1:I

    .line 32
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL2:I

    .line 33
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL3:I

    .line 34
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL4:I

    .line 35
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL5:I

    .line 36
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL6:I

    .line 37
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL7:I

    .line 38
    const/16 v0, 0x8

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL8:I

    .line 41
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I

    .line 217
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    const-string v1, "com.android.systemui"

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    .line 218
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    const-string v2, "other App"

    invoke-direct {v0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    .line 250
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    .line 256
    const-string v0, "com.miui.securitycenter"

    const-string v2, "com.tencent.mm"

    const-string v3, "com.eg.android.AlipayGphone"

    filled-new-array {v1, v0, v2, v3}, [Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sPackageList:Ljava/util/List;

    .line 263
    new-instance v4, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;

    invoke-direct {v4, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    .line 264
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    new-instance v5, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    invoke-direct {v5, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    invoke-direct {v0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    invoke-direct {v2, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>(Ljava/lang/String;)V

    filled-new-array {v4, v5, v0, v2}, [Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sAppStatisticList:Ljava/util/List;

    .line 270
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appAuthMap:Ljava/util/Map;

    .line 274
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    .line 366
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiSyAuthCountArray:Lorg/json/JSONArray;

    .line 367
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiAuthSuccCountArray:Lorg/json/JSONArray;

    .line 368
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherSyAuthCountArray:Lorg/json/JSONArray;

    .line 369
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherAuthSuccCountArray:Lorg/json/JSONArray;

    .line 122
    const/4 v0, 0x0

    .local v0, "tempIndex":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sPackageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appAuthMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sPackageList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sAppStatisticList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    .end local v0    # "tempIndex":I
    :cond_0
    return-void
.end method

.method private ambientLuxUnlockRate(Ljava/lang/String;I)V
    .locals 3
    .param p1, "packName"    # Ljava/lang/String;
    .param p2, "authen"    # I

    .line 279
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->getAmbientLuxLevel()I

    move-result v0

    .line 280
    .local v0, "luxLevel":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 281
    return-void

    .line 283
    :cond_0
    const-string v1, "com.android.systemui"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "FingerprintUnlockRateData"

    if-eqz v1, :cond_1

    .line 284
    const-string v1, "ambientLuxUnlockRate: SystemUi "

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    invoke-virtual {v1, v0, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->addLuxAuthCount(II)V

    goto :goto_0

    .line 287
    :cond_1
    const-string v1, "ambientLuxUnlockRate: OtherApp "

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    invoke-virtual {v1, v0, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->addLuxAuthCount(II)V

    .line 290
    :goto_0
    return-void
.end method

.method private getAmbientLuxLevel()I
    .locals 3

    .line 83
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mAmbientLux:I

    const/16 v1, 0x1f4

    if-ltz v0, :cond_0

    if-ge v0, v1, :cond_0

    .line 84
    const/4 v0, 0x0

    return v0

    .line 85
    :cond_0
    const/16 v2, 0x3e8

    if-lt v0, v1, :cond_1

    if-ge v0, v2, :cond_1

    .line 86
    const/4 v0, 0x1

    return v0

    .line 87
    :cond_1
    const/16 v1, 0x7d0

    if-lt v0, v2, :cond_2

    if-ge v0, v1, :cond_2

    .line 88
    const/4 v0, 0x2

    return v0

    .line 89
    :cond_2
    const/16 v2, 0x1388

    if-lt v0, v1, :cond_3

    if-ge v0, v2, :cond_3

    .line 90
    const/4 v0, 0x3

    return v0

    .line 91
    :cond_3
    const/16 v1, 0x1b58

    if-lt v0, v2, :cond_4

    if-ge v0, v1, :cond_4

    .line 92
    const/4 v0, 0x4

    return v0

    .line 93
    :cond_4
    const/16 v2, 0x2710

    if-lt v0, v1, :cond_5

    if-ge v0, v2, :cond_5

    .line 94
    const/4 v0, 0x5

    return v0

    .line 95
    :cond_5
    const/16 v1, 0x3a98

    if-lt v0, v2, :cond_6

    if-ge v0, v1, :cond_6

    .line 96
    const/4 v0, 0x6

    return v0

    .line 97
    :cond_6
    const/16 v2, 0x4e20

    if-lt v0, v1, :cond_7

    if-ge v0, v2, :cond_7

    .line 98
    const/4 v0, 0x7

    return v0

    .line 99
    :cond_7
    if-lt v0, v2, :cond_8

    .line 100
    const/16 v0, 0x8

    return v0

    .line 102
    :cond_8
    const/4 v0, -0x1

    return v0
.end method

.method public static getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;
    .locals 3

    .line 107
    const-string v0, "FingerprintUnlockRateData"

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    if-nez v0, :cond_1

    .line 109
    const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    monitor-enter v0

    .line 110
    :try_start_0
    const-string v1, "FingerprintUnlockRateData"

    const-string v2, "getInstance class"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    if-nez v1, :cond_0

    .line 112
    const-string v1, "FingerprintUnlockRateData"

    const-string v2, "getInstance new"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;-><init>()V

    sput-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    .line 115
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 117
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    return-object v0
.end method


# virtual methods
.method public appUnlockRate(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packName"    # Ljava/lang/String;
    .param p2, "authen"    # I

    .line 293
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appAuthMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appAuthMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    iget-object v0, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v2, v1

    iput v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 295
    if-ne p2, v1, :cond_1

    .line 296
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appAuthMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    iget-object v0, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v2, v1

    iput v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    goto :goto_0

    .line 299
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calculateUnlockCnt: appAuthMap doesn\'t contains:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "FingerprintUnlockRateData"

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    :cond_1
    :goto_0
    const-string v0, "com.android.systemui"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 303
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;

    .line 304
    .local v0, "systemuiStatistics":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mScreenStatus:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 305
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 306
    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I

    if-ne v2, v1, :cond_2

    .line 307
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    goto :goto_1

    .line 309
    :cond_2
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 312
    :goto_1
    if-ne p2, v1, :cond_7

    .line 313
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 314
    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I

    if-ne v2, v1, :cond_3

    .line 315
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    goto :goto_3

    .line 317
    :cond_3
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    goto :goto_3

    .line 321
    :cond_4
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 323
    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I

    if-ne v2, v1, :cond_5

    .line 324
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    goto :goto_2

    .line 326
    :cond_5
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 329
    :goto_2
    if-ne p2, v1, :cond_7

    .line 330
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 331
    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I

    if-ne v2, v1, :cond_6

    .line 332
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    goto :goto_3

    .line 334
    :cond_6
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 339
    :cond_7
    :goto_3
    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I

    if-ne v2, v1, :cond_8

    .line 340
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 341
    if-ne p2, v1, :cond_9

    .line 342
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    goto :goto_4

    .line 345
    :cond_8
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 347
    if-ne p2, v1, :cond_9

    .line 348
    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 352
    .end local v0    # "systemuiStatistics":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
    :cond_9
    :goto_4
    return-void
.end method

.method public calculateUnlockCnt(Ljava/lang/String;II)V
    .locals 3
    .param p1, "packName"    # Ljava/lang/String;
    .param p2, "authen"    # I
    .param p3, "screenState"    # I

    .line 355
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calculateUnlockCnt, packName:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",  authen: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",screenState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintUnlockRateData"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iput p3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mScreenStatus:I

    .line 357
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 358
    if-ne p2, v2, :cond_0

    .line 359
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 361
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRate(Ljava/lang/String;I)V

    .line 362
    invoke-direct {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->ambientLuxUnlockRate(Ljava/lang/String;I)V

    .line 363
    return-void
.end method

.method public handleAcquiredInfo(II)V
    .locals 4
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I

    .line 56
    sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->IS_FOD:Z

    const-string v1, "FingerprintUnlockRateData"

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 57
    const-string v0, "is not fod\uff01"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    return-void

    .line 61
    :cond_0
    const/16 v0, 0x16

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I

    if-nez v0, :cond_1

    .line 62
    const-string v0, "getMostRecentLux: finger down"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mALSProbe:Lcom/android/server/biometrics/log/ALSProbe;

    if-eqz v0, :cond_1

    .line 64
    invoke-virtual {v0}, Lcom/android/server/biometrics/log/ALSProbe;->enable()V

    .line 65
    iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I

    .line 69
    :cond_1
    const/16 v0, 0x17

    if-ne p2, v0, :cond_2

    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I

    if-ne v0, v2, :cond_2

    .line 70
    const-string v0, "getMostRecentLux:finger up "

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mALSProbe:Lcom/android/server/biometrics/log/ALSProbe;

    if-eqz v0, :cond_2

    .line 72
    invoke-virtual {v0}, Lcom/android/server/biometrics/log/ALSProbe;->getMostRecentLux()F

    move-result v0

    .line 73
    .local v0, "ambientLux":F
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMostRecentLux: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    float-to-int v1, v0

    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mAmbientLux:I

    .line 75
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mALSProbe:Lcom/android/server/biometrics/log/ALSProbe;

    invoke-virtual {v1}, Lcom/android/server/biometrics/log/ALSProbe;->disable()V

    .line 76
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I

    .line 80
    .end local v0    # "ambientLux":F
    :cond_2
    return-void
.end method

.method public resetLocalInfo()V
    .locals 2

    .line 534
    const-string v0, "FingerprintUnlockRateData"

    const-string v1, "resetLocalInfo "

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V

    .line 536
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sAppStatisticList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    .line 537
    .local v1, "appunlock":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->reset()V

    .line 538
    .end local v1    # "appunlock":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    goto :goto_0

    .line 539
    :cond_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->reset()V

    .line 540
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->reset()V

    .line 541
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 47
    const-string v0, "FingerprintUnlockRateData"

    const-string/jumbo v1, "setContext"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mContext:Landroid/content/Context;

    .line 49
    const-class v0, Landroid/hardware/SensorManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sensorManager:Landroid/hardware/SensorManager;

    .line 50
    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Lcom/android/server/biometrics/log/ALSProbe;

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sensorManager:Landroid/hardware/SensorManager;

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/log/ALSProbe;-><init>(Landroid/hardware/SensorManager;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mALSProbe:Lcom/android/server/biometrics/log/ALSProbe;

    .line 53
    :cond_0
    return-void
.end method

.method public updateDataToJson(Lorg/json/JSONObject;)Z
    .locals 19
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 376
    move-object/from16 v1, p0

    const-string v0, "low_light_status"

    const-string v2, "high_light_status"

    const-string v3, "auth_success_count"

    const-string v4, "auth_count"

    const-string/jumbo v5, "updateDataToJson"

    const-string v6, "FingerprintUnlockRateData"

    invoke-static {v6, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    if-nez p1, :cond_0

    .line 379
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local p1    # "dataInfoJson":Lorg/json/JSONObject;
    .local v5, "dataInfoJson":Lorg/json/JSONObject;
    goto :goto_0

    .line 449
    .end local v5    # "dataInfoJson":Lorg/json/JSONObject;
    .restart local p1    # "dataInfoJson":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    move-object/from16 v5, p1

    goto/16 :goto_4

    .line 378
    :cond_0
    move-object/from16 v5, p1

    .line 382
    .end local p1    # "dataInfoJson":Lorg/json/JSONObject;
    .restart local v5    # "dataInfoJson":Lorg/json/JSONObject;
    :goto_0
    :try_start_1
    iget-object v7, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    if-nez v7, :cond_1

    .line 383
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    iput-object v7, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    .line 386
    :cond_1
    const-string/jumbo v7, "total_auth_cnt"

    iget-object v8, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v8, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v5, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 387
    const-string/jumbo v7, "total_auth_succ_cnt"

    iget-object v8, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v8, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v5, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 389
    const/4 v7, 0x0

    .local v7, "index":I
    :goto_1
    iget-object v8, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sPackageList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v7, v8, :cond_3

    .line 390
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 391
    .local v8, "appUnlockRateUnit":Lorg/json/JSONObject;
    iget-object v9, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appAuthMap:Ljava/util/Map;

    iget-object v10, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sPackageList:Ljava/util/List;

    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    .line 392
    .local v9, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    const-string v10, "app_name"

    iget-object v11, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sPackageList:Ljava/util/List;

    invoke-interface {v11, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v8, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 393
    iget-object v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v10, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v8, v4, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 394
    iget-object v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v10, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v8, v3, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 395
    iget-object v10, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sPackageList:Ljava/util/List;

    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v11, "com.android.systemui"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 396
    iget-object v10, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    check-cast v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;

    .line 397
    .local v10, "tempSystemUIUnlockRate":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 398
    .local v11, "screenOn":Lorg/json/JSONObject;
    iget-object v12, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v12, v12, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v11, v4, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 399
    iget-object v12, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v12, v12, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v11, v3, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 400
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 401
    .local v12, "screenOnHighLight":Lorg/json/JSONObject;
    iget-object v13, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v13, v13, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v12, v4, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 402
    iget-object v13, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v13, v13, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v12, v3, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 403
    invoke-virtual {v11, v2, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 404
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V

    .line 405
    .local v13, "screenOnLowLight":Lorg/json/JSONObject;
    iget-object v14, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v14, v14, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v13, v4, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 406
    iget-object v14, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v14, v14, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v13, v3, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 407
    invoke-virtual {v11, v0, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 408
    const-string v14, "screen_on_status"

    invoke-virtual {v8, v14, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 410
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 411
    .local v14, "screenOff":Lorg/json/JSONObject;
    iget-object v15, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v15, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v14, v4, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 412
    iget-object v15, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v15, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v14, v3, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 413
    new-instance v15, Lorg/json/JSONObject;

    invoke-direct {v15}, Lorg/json/JSONObject;-><init>()V

    .line 414
    .local v15, "screenOffHighLight":Lorg/json/JSONObject;
    move-object/from16 p1, v9

    .end local v9    # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    .local p1, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    iget-object v9, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v15, v4, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 415
    iget-object v9, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v15, v3, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 416
    invoke-virtual {v14, v2, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 417
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 418
    .local v9, "screenOffLowLight":Lorg/json/JSONObject;
    move-object/from16 v16, v11

    .end local v11    # "screenOn":Lorg/json/JSONObject;
    .local v16, "screenOn":Lorg/json/JSONObject;
    iget-object v11, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v11, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v9, v4, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 419
    iget-object v11, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v11, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v9, v3, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 420
    invoke-virtual {v14, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 421
    const-string v11, "screen_off_status"

    invoke-virtual {v8, v11, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 423
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 424
    .local v11, "highLight":Lorg/json/JSONObject;
    move-object/from16 v17, v9

    .end local v9    # "screenOffLowLight":Lorg/json/JSONObject;
    .local v17, "screenOffLowLight":Lorg/json/JSONObject;
    iget-object v9, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v11, v4, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 425
    iget-object v9, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v11, v3, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 426
    invoke-virtual {v8, v2, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 428
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 429
    .local v9, "lowLight":Lorg/json/JSONObject;
    move-object/from16 v18, v2

    iget-object v2, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v9, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 430
    iget-object v2, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v9, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 431
    invoke-virtual {v8, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 395
    .end local v10    # "tempSystemUIUnlockRate":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
    .end local v11    # "highLight":Lorg/json/JSONObject;
    .end local v12    # "screenOnHighLight":Lorg/json/JSONObject;
    .end local v13    # "screenOnLowLight":Lorg/json/JSONObject;
    .end local v14    # "screenOff":Lorg/json/JSONObject;
    .end local v15    # "screenOffHighLight":Lorg/json/JSONObject;
    .end local v16    # "screenOn":Lorg/json/JSONObject;
    .end local v17    # "screenOffLowLight":Lorg/json/JSONObject;
    .end local p1    # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    .local v9, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    :cond_2
    move-object/from16 v18, v2

    move-object/from16 p1, v9

    .line 433
    .end local v9    # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    .restart local p1    # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    :goto_2
    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 389
    nop

    .end local v8    # "appUnlockRateUnit":Lorg/json/JSONObject;
    .end local p1    # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    add-int/lit8 v7, v7, 0x1

    move-object/from16 v2, v18

    goto/16 :goto_1

    .line 435
    .end local v7    # "index":I
    :cond_3
    const-string v0, "app_unlock_rate"

    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 438
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_3
    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 439
    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiSyAuthCountArray:Lorg/json/JSONArray;

    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;

    .line 440
    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiAuthSuccCountArray:Lorg/json/JSONArray;

    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;

    .line 441
    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherSyAuthCountArray:Lorg/json/JSONArray;

    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;

    .line 442
    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherAuthSuccCountArray:Lorg/json/JSONArray;

    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;

    .line 438
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 444
    .end local v0    # "index":I
    :cond_4
    const-string v0, "lux_systemui_count_array"

    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiSyAuthCountArray:Lorg/json/JSONArray;

    invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 445
    const-string v0, "lux_systemui_succ_count_array"

    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiAuthSuccCountArray:Lorg/json/JSONArray;

    invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 446
    const-string v0, "lux_otherapp_count_array"

    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherSyAuthCountArray:Lorg/json/JSONArray;

    invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 447
    const-string v0, "lux_otherapp_succ_count_array"

    iget-object v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherAuthSuccCountArray:Lorg/json/JSONArray;

    invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 452
    nop

    .line 453
    const/4 v0, 0x1

    return v0

    .line 449
    :catch_1
    move-exception v0

    .line 450
    .local v0, "e":Lorg/json/JSONException;
    :goto_4
    const-string/jumbo v2, "updateDataToJson exception"

    invoke-static {v6, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 451
    const/4 v2, 0x0

    return v2
.end method

.method public updateJsonToData(Lorg/json/JSONObject;)Z
    .locals 21
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 459
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v0, "low_light_status"

    const-string v3, "high_light_status"

    const-string v4, "FingerprintUnlockRateData"

    const-string v5, "auth_success_count"

    const-string v6, "auth_count"

    :try_start_0
    iget-object v8, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    const-string/jumbo v9, "total_auth_cnt"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 460
    iget-object v8, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    const-string/jumbo v9, "total_auth_succ_cnt"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 462
    const-string v8, "app_unlock_rate"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    iput-object v8, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    .line 463
    const/4 v8, 0x0

    .local v8, "tempIndex":I
    :goto_0
    iget-object v9, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v8, v9, :cond_1

    .line 464
    iget-object v9, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    invoke-virtual {v9, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 465
    .local v9, "appUnlockRateUnit":Lorg/json/JSONObject;
    iget-object v10, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->sAppStatisticList:Ljava/util/List;

    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    .line 466
    .local v10, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    iget-object v11, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v9, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    iput v12, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 467
    iget-object v11, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->totalAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v9, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    iput v12, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 468
    const-string v11, "app_name"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    const-string v12, "com.android.systemui"

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 469
    iget-object v11, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;

    check-cast v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;

    .line 470
    .local v11, "tempSystemUIUnlockRate":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
    const-string v12, "screen_on_status"

    invoke-virtual {v9, v12}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 471
    .local v12, "screenOn":Lorg/json/JSONObject;
    iget-object v13, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v12, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    iput v14, v13, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 472
    iget-object v13, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOnAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v12, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    iput v14, v13, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 473
    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 474
    .local v13, "screenOnHighLight":Lorg/json/JSONObject;
    iget-object v14, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v13, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v14, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 475
    iget-object v14, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v13, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v14, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 476
    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 477
    .local v14, "screenOnLowLight":Lorg/json/JSONObject;
    iget-object v15, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v14, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 478
    iget-object v7, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOn_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v14, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 479
    const-string v7, "screen_off_status"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 480
    .local v7, "screenOff":Lorg/json/JSONObject;
    iget-object v15, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    move-object/from16 v16, v10

    .end local v10    # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    .local v16, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    invoke-virtual {v7, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    iput v10, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 481
    iget-object v10, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOffAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v7, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 482
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 483
    .local v10, "screenOffHighLight":Lorg/json/JSONObject;
    iget-object v15, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    move-object/from16 v17, v12

    .end local v12    # "screenOn":Lorg/json/JSONObject;
    .local v17, "screenOn":Lorg/json/JSONObject;
    invoke-virtual {v10, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    iput v12, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 484
    iget-object v12, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_highLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v10, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v12, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 485
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 486
    .local v12, "screenOffLowLight":Lorg/json/JSONObject;
    iget-object v15, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    move-object/from16 v18, v7

    .end local v7    # "screenOff":Lorg/json/JSONObject;
    .local v18, "screenOff":Lorg/json/JSONObject;
    invoke-virtual {v12, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 487
    iget-object v7, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->screenOff_lowLight_Auth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v12, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 489
    invoke-virtual {v9, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 490
    .local v7, "highLight":Lorg/json/JSONObject;
    iget-object v15, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    move-object/from16 v19, v3

    invoke-virtual {v7, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 491
    iget-object v3, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->highLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v7, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 492
    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 493
    .local v3, "lowLight":Lorg/json/JSONObject;
    iget-object v15, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    move-object/from16 v20, v0

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 494
    iget-object v0, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;->lowLightAuth:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    goto :goto_1

    .line 468
    .end local v3    # "lowLight":Lorg/json/JSONObject;
    .end local v7    # "highLight":Lorg/json/JSONObject;
    .end local v11    # "tempSystemUIUnlockRate":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
    .end local v12    # "screenOffLowLight":Lorg/json/JSONObject;
    .end local v13    # "screenOnHighLight":Lorg/json/JSONObject;
    .end local v14    # "screenOnLowLight":Lorg/json/JSONObject;
    .end local v16    # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    .end local v17    # "screenOn":Lorg/json/JSONObject;
    .end local v18    # "screenOff":Lorg/json/JSONObject;
    .local v10, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    :cond_0
    move-object/from16 v20, v0

    move-object/from16 v19, v3

    move-object/from16 v16, v10

    .line 463
    .end local v9    # "appUnlockRateUnit":Lorg/json/JSONObject;
    .end local v10    # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
    :goto_1
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v3, v19

    move-object/from16 v0, v20

    goto/16 :goto_0

    .line 499
    :cond_1
    const-string v0, "lux_systemui_count_array"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiSyAuthCountArray:Lorg/json/JSONArray;

    .line 500
    const-string v0, "lux_systemui_succ_count_array"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiAuthSuccCountArray:Lorg/json/JSONArray;

    .line 501
    const-string v0, "lux_otherapp_count_array"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherSyAuthCountArray:Lorg/json/JSONArray;

    .line 502
    const-string v0, "lux_otherapp_succ_count_array"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherAuthSuccCountArray:Lorg/json/JSONArray;

    .line 504
    iget-object v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiSyAuthCountArray:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 505
    .local v0, "arraylength":I
    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiSyAuthCountArray:Lorg/json/JSONArray;

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ne v3, v0, :cond_4

    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiAuthSuccCountArray:Lorg/json/JSONArray;

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ne v3, v0, :cond_4

    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherSyAuthCountArray:Lorg/json/JSONArray;

    .line 506
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ne v3, v0, :cond_4

    iget-object v3, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherAuthSuccCountArray:Lorg/json/JSONArray;

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-eq v3, v0, :cond_2

    goto/16 :goto_3

    .line 510
    :cond_2
    const/4 v3, 0x0

    .end local v8    # "tempIndex":I
    .local v3, "tempIndex":I
    :goto_2
    iget-object v5, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRateArray:Lorg/json/JSONArray;

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 511
    iget-object v5, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiSyAuthCountArray:Lorg/json/JSONArray;

    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 512
    .local v5, "luxSystemuicount":Ljava/lang/Integer;
    iget-object v6, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightSystemuiAuthSuccCountArray:Lorg/json/JSONArray;

    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 513
    .local v6, "luxSystemuiSuccCount":Ljava/lang/Integer;
    iget-object v7, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherSyAuthCountArray:Lorg/json/JSONArray;

    invoke-virtual {v7, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 514
    .local v7, "luxOtherAppcount":Ljava/lang/Integer;
    iget-object v8, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->luxLightOtherAuthSuccCountArray:Lorg/json/JSONArray;

    invoke-virtual {v8, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 515
    .local v8, "luxOtherAppSuccCount":Ljava/lang/Integer;
    iget-object v9, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iput v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 516
    iget-object v9, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->systemuiLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iput v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 517
    iget-object v9, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iput v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I

    .line 518
    iget-object v9, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->otherAppLuxUnlockRate:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;

    iget-object v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->luxLightUnlockList:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iput v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I

    .line 510
    .end local v5    # "luxSystemuicount":Ljava/lang/Integer;
    .end local v6    # "luxSystemuiSuccCount":Ljava/lang/Integer;
    .end local v7    # "luxOtherAppcount":Ljava/lang/Integer;
    .end local v8    # "luxOtherAppSuccCount":Ljava/lang/Integer;
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 529
    .end local v0    # "arraylength":I
    .end local v3    # "tempIndex":I
    :cond_3
    nop

    .line 530
    const/4 v0, 0x1

    return v0

    .line 507
    .restart local v0    # "arraylength":I
    .local v8, "tempIndex":I
    :cond_4
    :goto_3
    const-string v3, "bad luxLight JSONArray"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    const/4 v3, 0x0

    return v3

    .line 525
    .end local v0    # "arraylength":I
    .end local v8    # "tempIndex":I
    :catch_0
    move-exception v0

    .line 526
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string/jumbo v3, "updateJsonToData IndexOutOfBoundsException"

    invoke-static {v4, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 527
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->resetLocalInfo()V

    .line 528
    const/4 v3, 0x0

    return v3

    .line 521
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v0

    const/4 v3, 0x0

    .line 522
    .local v0, "e":Lorg/json/JSONException;
    const-string/jumbo v5, "updateJsonToData JSONException"

    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 523
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->resetLocalInfo()V

    .line 524
    return v3
.end method
