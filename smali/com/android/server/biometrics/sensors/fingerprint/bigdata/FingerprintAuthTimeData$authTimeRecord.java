class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord {
	 /* .source "FingerprintAuthTimeData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "authTimeRecord" */
} // .end annotation
/* # instance fields */
public java.lang.String name;
private org.json.JSONObject screenDozeAuthTimeJson;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$screenStatusTimeCollect screenDozeTimeCollect;
private org.json.JSONObject screenOffAuthTimeJson;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$screenStatusTimeCollect screenOffTimeCollect;
private org.json.JSONObject screenOnAuthTimeJson;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$screenStatusTimeCollect screenOnTimeCollect;
private java.util.List screenStatusJsonKeyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List screenStatusJsonList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lorg/json/JSONObject;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List screenStatusList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.util.List -$$Nest$fgetscreenStatusJsonKeyList ( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.screenStatusJsonKeyList;
} // .end method
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord ( ) {
/* .locals 4 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 190 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 194 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
final String v1 = "screenOff"; // const-string v1, "screenOff"
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;-><init>(Ljava/lang/String;)V */
this.screenOffTimeCollect = v0;
/* .line 195 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
final String v1 = "screenOn"; // const-string v1, "screenOn"
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;-><init>(Ljava/lang/String;)V */
this.screenOnTimeCollect = v0;
/* .line 196 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
final String v1 = "screenDoze"; // const-string v1, "screenDoze"
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;-><init>(Ljava/lang/String;)V */
this.screenDozeTimeCollect = v0;
/* .line 198 */
/* new-instance v0, Ljava/util/ArrayList; */
v1 = this.screenOffTimeCollect;
v2 = this.screenOnTimeCollect;
v3 = this.screenDozeTimeCollect;
/* filled-new-array {v1, v2, v3}, [Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.screenStatusList = v0;
/* .line 204 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.screenOffAuthTimeJson = v0;
/* .line 205 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.screenOnAuthTimeJson = v0;
/* .line 206 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.screenDozeAuthTimeJson = v0;
/* .line 208 */
/* new-instance v0, Ljava/util/ArrayList; */
v1 = this.screenOffAuthTimeJson;
v2 = this.screenOnAuthTimeJson;
v3 = this.screenDozeAuthTimeJson;
/* filled-new-array {v1, v2, v3}, [Lorg/json/JSONObject; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.screenStatusJsonList = v0;
/* .line 214 */
final String v0 = "screen_on_auth_time"; // const-string v0, "screen_on_auth_time"
final String v1 = "screen_doze_auth_time"; // const-string v1, "screen_doze_auth_time"
final String v2 = "screen_off_auth_time"; // const-string v2, "screen_off_auth_time"
/* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
this.screenStatusJsonKeyList = v0;
/* .line 191 */
this.name = p1;
/* .line 192 */
return;
} // .end method
/* # virtual methods */
public void reset ( ) {
/* .locals 2 */
/* .line 221 */
v0 = this.screenStatusList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
/* .line 222 */
/* .local v1, "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$screenStatusTimeCollect ) v1 ).reset ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->reset()V
/* .line 223 */
} // .end local v1 # "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
/* .line 224 */
} // :cond_0
return;
} // .end method
