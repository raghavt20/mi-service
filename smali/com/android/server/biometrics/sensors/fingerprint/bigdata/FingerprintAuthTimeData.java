public class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData extends com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData {
	 /* .source "FingerprintAuthTimeData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer AUTH_FAIL;
private static final Integer AUTH_STEP_FINGERDOWN;
private static final Integer AUTH_STEP_NONE;
private static final Integer AUTH_STEP_RESULT;
private static final Integer AUTH_STEP_UI_READY;
private static final Integer AUTH_SUCCESS;
private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_FINGER_DOWN;
private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_FINGER_UP;
private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_HIGHLIGHT_CAPTURE_START;
private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_LOWLIGHT_CAPTURE_START;
private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_UNLOCK_SHOW_WINDOW;
private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_WAIT_FINGER_INPUT;
private static final Integer HBM;
private static final Integer LOW_BRIGHT;
private static final java.lang.String TAG;
public static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeStatisticsUnit sAuthTimeUnit;
private static volatile com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData sInstance;
/* # instance fields */
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord authFailTime;
private org.json.JSONObject authFailTimeJson;
private java.util.List authResultTimeJsonKeyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List authResultTimeJsonList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lorg/json/JSONObject;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List authResultTimeList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord authSuccessTime;
private org.json.JSONObject authSuccessTimeJson;
private org.json.JSONObject authTimeJson;
private Integer mFingerUnlockBright;
/* # direct methods */
static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData ( ) {
/* .locals 2 */
/* .line 106 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit-IA;)V */
return;
} // .end method
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData ( ) {
/* .locals 3 */
/* .line 20 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V */
/* .line 29 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mFingerUnlockBright:I */
/* .line 227 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
final String v1 = "authFailTime"; // const-string v1, "authFailTime"
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;-><init>(Ljava/lang/String;)V */
this.authFailTime = v0;
/* .line 228 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
final String v1 = "authSuccessTime"; // const-string v1, "authSuccessTime"
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;-><init>(Ljava/lang/String;)V */
this.authSuccessTime = v0;
/* .line 230 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.authTimeJson = v0;
/* .line 231 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.authSuccessTimeJson = v0;
/* .line 232 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.authFailTimeJson = v0;
/* .line 234 */
/* new-instance v0, Ljava/util/ArrayList; */
v1 = this.authFailTime;
v2 = this.authSuccessTime;
/* filled-new-array {v1, v2}, [Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.authResultTimeList = v0;
/* .line 239 */
/* new-instance v0, Ljava/util/ArrayList; */
v1 = this.authFailTimeJson;
v2 = this.authSuccessTimeJson;
/* filled-new-array {v1, v2}, [Lorg/json/JSONObject; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.authResultTimeJsonList = v0;
/* .line 244 */
final String v0 = "auth_fail_time"; // const-string v0, "auth_fail_time"
final String v1 = "auth_success_time"; // const-string v1, "auth_success_time"
/* filled-new-array {v0, v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
this.authResultTimeJsonKeyList = v0;
return;
} // .end method
public static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData getInstance ( ) {
/* .locals 3 */
/* .line 45 */
final String v0 = "FingerprintAuthTimeData"; // const-string v0, "FingerprintAuthTimeData"
final String v1 = "getInstance"; // const-string v1, "getInstance"
android.util.Slog .d ( v0,v1 );
/* .line 46 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sInstance;
/* if-nez v0, :cond_1 */
/* .line 47 */
/* const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData; */
/* monitor-enter v0 */
/* .line 48 */
try { // :try_start_0
final String v1 = "FingerprintAuthTimeData"; // const-string v1, "FingerprintAuthTimeData"
final String v2 = "getInstance class"; // const-string v2, "getInstance class"
android.util.Slog .d ( v1,v2 );
/* .line 49 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sInstance;
/* if-nez v1, :cond_0 */
/* .line 50 */
final String v1 = "FingerprintAuthTimeData"; // const-string v1, "FingerprintAuthTimeData"
final String v2 = "getInstance new"; // const-string v2, "getInstance new"
android.util.Slog .d ( v1,v2 );
/* .line 51 */
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData; */
/* invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;-><init>()V */
/* .line 53 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 55 */
} // :cond_1
} // :goto_0
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sInstance;
} // .end method
/* # virtual methods */
public Integer calculateAuthTime ( Integer p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "authen" # I */
/* .param p2, "screenState" # I */
/* .line 287 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "calculateAuthTime, authen: "; // const-string v1, "calculateAuthTime, authen: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", screenState: "; // const-string v1, ", screenState: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintAuthTimeData"; // const-string v1, "FingerprintAuthTimeData"
android.util.Slog .w ( v1,v0 );
/* .line 288 */
/* iput p2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mScreenStatus:I */
/* .line 289 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
/* iget v0, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I */
int v2 = 2; // const/4 v2, 0x2
int v3 = 3; // const/4 v3, 0x3
/* if-ne v0, v2, :cond_0 */
/* .line 290 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
/* iput v3, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I */
/* .line 291 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
java.util.Calendar .getInstance ( );
(( java.util.Calendar ) v2 ).getTimeInMillis ( ); // invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v4 */
/* iput-wide v4, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authResultTimeStamp:J */
/* .line 292 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
v0 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeStatisticsUnit ) v0 ).calculateTime ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->calculateTime()I
/* if-gez v0, :cond_0 */
/* .line 293 */
final String v0 = "calculate auth time abnormal, drop it!"; // const-string v0, "calculate auth time abnormal, drop it!"
android.util.Slog .e ( v1,v0 );
/* .line 294 */
int v0 = -1; // const/4 v0, -0x1
/* .line 298 */
} // :cond_0
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
/* iget v0, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I */
/* if-ne v0, v3, :cond_1 */
/* .line 303 */
v0 = this.authResultTimeList;
/* check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
v0 = this.screenStatusList;
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mScreenStatus:I */
/* check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
/* .line 304 */
/* .local v0, "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
v1 = this.lightStatusList;
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mFingerUnlockBright:I */
/* check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
/* iget v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I */
/* .line 305 */
v1 = this.lightStatusList;
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mFingerUnlockBright:I */
/* check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$timeCollect ) v1 ).timeProcess ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->timeProcess()V
/* .line 307 */
} // .end local v0 # "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
} // :cond_1
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeStatisticsUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->reset()V
/* .line 308 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void handleAcquiredInfo ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .line 256 */
final String v0 = "FingerprintAuthTimeData"; // const-string v0, "FingerprintAuthTimeData"
final String v1 = "handleAcquiredInfo"; // const-string v1, "handleAcquiredInfo"
android.util.Slog .w ( v0,v1 );
/* .line 257 */
/* if-nez p1, :cond_0 */
/* .line 258 */
return;
/* .line 261 */
} // :cond_0
int v0 = 6; // const/4 v0, 0x6
/* if-ne p1, v0, :cond_3 */
/* .line 262 */
int v0 = 1; // const/4 v0, 0x1
/* sparse-switch p2, :sswitch_data_0 */
/* .line 265 */
/* :sswitch_0 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeStatisticsUnit ) v1 ).reset ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->reset()V
/* .line 266 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
/* iput v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I */
/* .line 267 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
java.util.Calendar .getInstance ( );
(( java.util.Calendar ) v1 ).getTimeInMillis ( ); // invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->fingerDownTimeStamp:J */
/* .line 268 */
/* .line 271 */
/* :sswitch_1 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
/* iget v1, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I */
/* if-ne v1, v0, :cond_1 */
/* .line 272 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
int v2 = 2; // const/4 v2, 0x2
/* iput v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->authStep:I */
/* .line 273 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
java.util.Calendar .getInstance ( );
(( java.util.Calendar ) v2 ).getTimeInMillis ( ); // invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v2 */
/* iput-wide v2, v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->startCaptureTimeStamp:J */
/* .line 275 */
} // :cond_1
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData.sAuthTimeUnit;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeStatisticsUnit ) v1 ).reset ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeStatisticsUnit;->reset()V
/* .line 277 */
} // :goto_0
/* const/16 v1, 0x14 */
/* if-ne p2, v1, :cond_2 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_2
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->mFingerUnlockBright:I */
/* .line 278 */
/* nop */
/* .line 283 */
} // :cond_3
} // :goto_1
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x14 -> :sswitch_1 */
/* 0x16 -> :sswitch_0 */
/* 0x32 -> :sswitch_1 */
} // .end sparse-switch
} // .end method
public void resetLocalInfo ( ) {
/* .locals 2 */
/* .line 387 */
final String v0 = "FingerprintAuthTimeData"; // const-string v0, "FingerprintAuthTimeData"
final String v1 = "resetLocalInfo"; // const-string v1, "resetLocalInfo"
android.util.Slog .w ( v0,v1 );
/* .line 388 */
v0 = this.authResultTimeList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
/* .line 389 */
/* .local v1, "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord ) v1 ).reset ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->reset()V
/* .line 390 */
} // .end local v1 # "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
/* .line 391 */
} // :cond_0
return;
} // .end method
public Boolean updateDataToJson ( org.json.JSONObject p0 ) {
/* .locals 16 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 312 */
/* move-object/from16 v1, p0 */
/* const-string/jumbo v0, "updateAuthTimeJson" */
final String v2 = "FingerprintAuthTimeData"; // const-string v2, "FingerprintAuthTimeData"
android.util.Slog .w ( v2,v0 );
/* .line 314 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "tempIndex":I */
} // :goto_0
try { // :try_start_0
v3 = v3 = this.authResultTimeList;
/* if-ge v0, v3, :cond_2 */
/* .line 316 */
v3 = this.authResultTimeList;
/* check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
/* .line 317 */
/* .local v3, "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
v4 = this.authResultTimeJsonList;
/* check-cast v4, Lorg/json/JSONObject; */
/* .line 318 */
/* .local v4, "tempAuthResultTimeJson":Lorg/json/JSONObject; */
v5 = this.authResultTimeJsonKeyList;
/* check-cast v5, Ljava/lang/String; */
/* .line 319 */
/* .local v5, "tempAuthResultTimeJsonKey":Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "secondTempIndex":I */
} // :goto_1
v7 = v7 = this.screenStatusList;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_3 */
/* if-ge v6, v7, :cond_1 */
/* .line 320 */
try { // :try_start_1
v7 = this.screenStatusList;
/* check-cast v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
/* .line 321 */
/* .local v7, "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
v8 = this.screenStatusJsonList;
/* check-cast v8, Lorg/json/JSONObject; */
/* .line 322 */
/* .local v8, "tempScreenStatusJson":Lorg/json/JSONObject; */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord .-$$Nest$fgetscreenStatusJsonKeyList ( v3 );
/* check-cast v9, Ljava/lang/String; */
/* .line 323 */
/* .local v9, "tempScreenStatusJsonKey":Ljava/lang/String; */
int v10 = 0; // const/4 v10, 0x0
/* .local v10, "thirdTempIndex":I */
} // :goto_2
v11 = v11 = this.lightStatusList;
/* if-ge v10, v11, :cond_0 */
/* .line 324 */
v11 = this.lightStatusList;
/* check-cast v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
/* .line 325 */
/* .local v11, "tempTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
v12 = this.lightStatusJsonList;
/* check-cast v12, Lorg/json/JSONObject; */
/* .line 326 */
/* .local v12, "tempLightStatusJson":Lorg/json/JSONObject; */
v13 = this.lightStatusJsonKeyList;
/* check-cast v13, Ljava/lang/String; */
/* .line 328 */
/* .local v13, "tempLightStatusJsonKey":Ljava/lang/String; */
/* const-string/jumbo v14, "total_auth_count" */
/* iget v15, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I */
(( org.json.JSONObject ) v12 ).put ( v14, v15 ); // invoke-virtual {v12, v14, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 329 */
/* const-string/jumbo v14, "total_auth_time" */
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object v15, v2 */
try { // :try_start_2
/* iget-wide v1, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J */
(( org.json.JSONObject ) v12 ).put ( v14, v1, v2 ); // invoke-virtual {v12, v14, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 330 */
/* const-string/jumbo v1, "total_down_to_capture_time" */
/* move-object v14, v3 */
} // .end local v3 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
/* .local v14, "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
/* iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J */
(( org.json.JSONObject ) v12 ).put ( v1, v2, v3 ); // invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 331 */
/* const-string/jumbo v1, "total_capture_to_result_time" */
/* iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalCaptureToAuthResultTime:J */
(( org.json.JSONObject ) v12 ).put ( v1, v2, v3 ); // invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 332 */
final String v1 = "avg_auth_time"; // const-string v1, "avg_auth_time"
/* iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgAuthTime:J */
(( org.json.JSONObject ) v12 ).put ( v1, v2, v3 ); // invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 333 */
final String v1 = "avg_down_to_capture_time"; // const-string v1, "avg_down_to_capture_time"
/* iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgFingerDownToCaptureTime:J */
(( org.json.JSONObject ) v12 ).put ( v1, v2, v3 ); // invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 334 */
final String v1 = "avg_capture_to_result_time"; // const-string v1, "avg_capture_to_result_time"
/* iget-wide v2, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgCaptureToAuthResultTime:J */
(( org.json.JSONObject ) v12 ).put ( v1, v2, v3 ); // invoke-virtual {v12, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* .line 336 */
(( org.json.JSONObject ) v8 ).put ( v13, v12 ); // invoke-virtual {v8, v13, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 323 */
/* nop */
} // .end local v11 # "tempTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
} // .end local v12 # "tempLightStatusJson":Lorg/json/JSONObject;
} // .end local v13 # "tempLightStatusJsonKey":Ljava/lang/String;
/* add-int/lit8 v10, v10, 0x1 */
/* move-object/from16 v1, p0 */
/* move-object v3, v14 */
/* move-object v2, v15 */
} // .end local v14 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
/* .restart local v3 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
} // :cond_0
/* move-object v15, v2 */
/* move-object v14, v3 */
/* .line 338 */
} // .end local v3 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
} // .end local v10 # "thirdTempIndex":I
/* .restart local v14 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
(( org.json.JSONObject ) v4 ).put ( v9, v8 ); // invoke-virtual {v4, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_2 */
/* .catch Lorg/json/JSONException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 319 */
/* nop */
} // .end local v7 # "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
} // .end local v8 # "tempScreenStatusJson":Lorg/json/JSONObject;
} // .end local v9 # "tempScreenStatusJsonKey":Ljava/lang/String;
/* add-int/lit8 v6, v6, 0x1 */
/* move-object/from16 v1, p0 */
/* move-object v3, v14 */
/* move-object v2, v15 */
/* goto/16 :goto_1 */
/* .line 342 */
} // .end local v0 # "tempIndex":I
} // .end local v4 # "tempAuthResultTimeJson":Lorg/json/JSONObject;
} // .end local v5 # "tempAuthResultTimeJsonKey":Ljava/lang/String;
} // .end local v6 # "secondTempIndex":I
} // .end local v14 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* move-object v15, v2 */
} // :goto_3
/* move-object/from16 v1, p1 */
/* .line 319 */
/* .restart local v0 # "tempIndex":I */
/* .restart local v3 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
/* .restart local v4 # "tempAuthResultTimeJson":Lorg/json/JSONObject; */
/* .restart local v5 # "tempAuthResultTimeJsonKey":Ljava/lang/String; */
/* .restart local v6 # "secondTempIndex":I */
} // :cond_1
/* move-object v15, v2 */
/* move-object v14, v3 */
/* .line 340 */
} // .end local v3 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
} // .end local v6 # "secondTempIndex":I
/* .restart local v14 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
/* move-object/from16 v1, p1 */
try { // :try_start_3
(( org.json.JSONObject ) v1 ).put ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_3 */
/* .catch Lorg/json/JSONException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 314 */
/* nop */
} // .end local v4 # "tempAuthResultTimeJson":Lorg/json/JSONObject;
} // .end local v5 # "tempAuthResultTimeJsonKey":Ljava/lang/String;
} // .end local v14 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
/* add-int/lit8 v0, v0, 0x1 */
/* move-object/from16 v1, p0 */
/* move-object v2, v15 */
/* goto/16 :goto_0 */
/* .line 342 */
} // .end local v0 # "tempIndex":I
/* :catch_2 */
/* move-exception v0 */
/* .line 314 */
/* .restart local v0 # "tempIndex":I */
} // :cond_2
/* move-object/from16 v1, p1 */
/* .line 345 */
} // .end local v0 # "tempIndex":I
/* nop */
/* .line 346 */
int v0 = 1; // const/4 v0, 0x1
/* .line 342 */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v1, p1 */
/* move-object v15, v2 */
/* .line 343 */
/* .local v0, "e":Lorg/json/JSONException; */
} // :goto_4
/* const-string/jumbo v2, "updateAuthTimeJson exception" */
/* move-object v3, v15 */
android.util.Slog .e ( v3,v2,v0 );
/* .line 344 */
int v2 = 0; // const/4 v2, 0x0
} // .end method
public Boolean updateJsonToData ( org.json.JSONObject p0 ) {
/* .locals 13 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 350 */
/* const-string/jumbo v0, "updateJsonToData" */
final String v1 = "FingerprintAuthTimeData"; // const-string v1, "FingerprintAuthTimeData"
android.util.Slog .i ( v1,v0 );
/* .line 353 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "tempIndex":I */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
v3 = v3 = this.authResultTimeList;
/* if-ge v0, v3, :cond_2 */
/* .line 354 */
v3 = this.authResultTimeList;
/* check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
/* .line 355 */
/* .local v3, "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord; */
v4 = this.authResultTimeJsonKeyList;
/* check-cast v4, Ljava/lang/String; */
(( org.json.JSONObject ) p1 ).getJSONObject ( v4 ); // invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 357 */
/* .local v4, "tempAuthResultTimeJson":Lorg/json/JSONObject; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "secondTempIndex":I */
} // :goto_1
v6 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord .-$$Nest$fgetscreenStatusJsonKeyList ( v3 );
/* if-ge v5, v6, :cond_1 */
/* .line 358 */
v6 = this.screenStatusList;
/* check-cast v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
/* .line 359 */
/* .local v6, "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect; */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$authTimeRecord .-$$Nest$fgetscreenStatusJsonKeyList ( v3 );
/* check-cast v7, Ljava/lang/String; */
(( org.json.JSONObject ) v4 ).getJSONObject ( v7 ); // invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 361 */
/* .local v7, "tempScreenStatusTimeJson":Lorg/json/JSONObject; */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "thirdTempIndex":I */
} // :goto_2
v9 = v9 = this.lightStatusList;
/* if-ge v8, v9, :cond_0 */
/* .line 362 */
v9 = this.lightStatusList;
/* check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
/* .line 363 */
/* .local v9, "tempTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
v10 = this.lightStatusJsonKeyList;
/* check-cast v10, Ljava/lang/String; */
(( org.json.JSONObject ) v7 ).getJSONObject ( v10 ); // invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 364 */
/* .local v10, "tempLightStatusJson":Lorg/json/JSONObject; */
/* const-string/jumbo v11, "total_auth_count" */
v11 = (( org.json.JSONObject ) v10 ).getInt ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthCount:I */
/* .line 365 */
/* const-string/jumbo v11, "total_auth_time" */
v11 = (( org.json.JSONObject ) v10 ).getInt ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* int-to-long v11, v11 */
/* iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalAuthTime:J */
/* .line 366 */
/* const-string/jumbo v11, "total_down_to_capture_time" */
v11 = (( org.json.JSONObject ) v10 ).getInt ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* int-to-long v11, v11 */
/* iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalFingerDownToCaptureTime:J */
/* .line 367 */
/* const-string/jumbo v11, "total_capture_to_result_time" */
v11 = (( org.json.JSONObject ) v10 ).getInt ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* int-to-long v11, v11 */
/* iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->totalCaptureToAuthResultTime:J */
/* .line 368 */
final String v11 = "avg_auth_time"; // const-string v11, "avg_auth_time"
v11 = (( org.json.JSONObject ) v10 ).getInt ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* int-to-long v11, v11 */
/* iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgAuthTime:J */
/* .line 369 */
final String v11 = "avg_down_to_capture_time"; // const-string v11, "avg_down_to_capture_time"
v11 = (( org.json.JSONObject ) v10 ).getInt ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* int-to-long v11, v11 */
/* iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgFingerDownToCaptureTime:J */
/* .line 370 */
final String v11 = "avg_capture_to_result_time"; // const-string v11, "avg_capture_to_result_time"
v11 = (( org.json.JSONObject ) v10 ).getInt ( v11 ); // invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* int-to-long v11, v11 */
/* iput-wide v11, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->avgCaptureToAuthResultTime:J */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 361 */
} // .end local v9 # "tempTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
} // .end local v10 # "tempLightStatusJson":Lorg/json/JSONObject;
/* add-int/lit8 v8, v8, 0x1 */
/* .line 357 */
} // .end local v6 # "tempScreenTimeCollect":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
} // .end local v7 # "tempScreenStatusTimeJson":Lorg/json/JSONObject;
} // .end local v8 # "thirdTempIndex":I
} // :cond_0
/* add-int/lit8 v5, v5, 0x1 */
/* goto/16 :goto_1 */
/* .line 353 */
} // .end local v3 # "tempAuthResultTimeRecord":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
} // .end local v4 # "tempAuthResultTimeJson":Lorg/json/JSONObject;
} // .end local v5 # "secondTempIndex":I
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* goto/16 :goto_0 */
/* .line 382 */
} // :cond_2
/* nop */
/* .line 383 */
int v1 = 1; // const/4 v1, 0x1
/* .line 378 */
} // .end local v0 # "tempIndex":I
/* :catch_0 */
/* move-exception v0 */
/* .line 379 */
/* .local v0, "e":Ljava/lang/IndexOutOfBoundsException; */
/* const-string/jumbo v3, "updateJsonToData IndexOutOfBoundsException" */
android.util.Slog .e ( v1,v3,v0 );
/* .line 380 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData ) p0 ).resetLocalInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->resetLocalInfo()V
/* .line 381 */
/* .line 374 */
} // .end local v0 # "e":Ljava/lang/IndexOutOfBoundsException;
/* :catch_1 */
/* move-exception v0 */
/* .line 375 */
/* .local v0, "e":Lorg/json/JSONException; */
/* const-string/jumbo v3, "updateJsonToData exception" */
android.util.Slog .e ( v1,v3,v0 );
/* .line 376 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData ) p0 ).resetLocalInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->resetLocalInfo()V
/* .line 377 */
} // .end method
