public class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData {
	 /* .source "FingerprintLocalStatisticsData.java" */
	 /* # static fields */
	 private static final java.lang.String APP_ID;
	 private static final java.lang.String EVENT_NAME;
	 private static final Integer FLAG_NON_ANONYMOUS;
	 private static final Boolean FP_LOCAL_STATISTICS_DEBUG;
	 private static final java.lang.String FP_LOCAL_STATISTICS_DIR;
	 private static final Boolean FP_LOCAL_STATISTICS_ENABLED;
	 private static final java.lang.String FP_LOCAL_STATISTICS_PREFS_FILE;
	 private static final Long FP_LOCAL_STATISTICS_UPLOAD_PERIOD;
	 private static java.lang.String FP_VENDOR;
	 private static Boolean IS_FOD;
	 private static Boolean IS_POWERFP;
	 private static final java.lang.String KEY_FP_ENROLLED_COUNT;
	 private static final java.lang.String KEY_FP_OPTION;
	 private static final java.lang.String KEY_FP_TOUCH_FILM_MODE;
	 private static final java.lang.String KEY_FP_TYPE;
	 private static final java.lang.String KEY_FP_VENDOR;
	 private static final java.lang.String ONETRACK_ACTION;
	 private static final java.lang.String ONETRACK_PACKAGE_NAME;
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData sInstance;
	 /* # instance fields */
	 private java.util.Map InstanceMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.Map JsonMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lorg/json/JSONObject;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List LocalStatisticsKeyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData authTimeInstance;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData failReasonInstance;
private Integer filmMode;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData halAuthInstance;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData halEnrollInstance;
private android.content.Context mContext;
private Integer mEnrolledCount;
private android.content.SharedPreferences mFpLocalSharedPreferences;
private java.io.File mFpPrefsFile;
private android.content.SharedPreferences$Editor mFpSharedPreferencesEditor;
private Boolean mLocalStatisticsInit;
private Long mLocalStatisticsUploadTime;
private Integer mScreenStatus;
private Integer mUnlockOption;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData unlockRateInstance;
/* # direct methods */
static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ( ) {
/* .locals 3 */
/* .line 70 */
final String v0 = "ro.hardware.fp.fod"; // const-string v0, "ro.hardware.fp.fod"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData.IS_FOD = (v0!= 0);
/* .line 71 */
final String v0 = "ro.hardware.fp.sideCap"; // const-string v0, "ro.hardware.fp.sideCap"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData.IS_POWERFP = (v0!= 0);
/* .line 72 */
final String v0 = "persist.vendor.sys.fp.vendor"; // const-string v0, "persist.vendor.sys.fp.vendor"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 82 */
final String v0 = "persist.vendor.sys.fp.onetrack.enable"; // const-string v0, "persist.vendor.sys.fp.onetrack.enable"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData.FP_LOCAL_STATISTICS_ENABLED = (v0!= 0);
/* .line 85 */
final String v0 = "ro.hardware.fp.onetrack.period"; // const-string v0, "ro.hardware.fp.onetrack.period"
/* const-wide/32 v1, 0x5265c00 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* sput-wide v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->FP_LOCAL_STATISTICS_UPLOAD_PERIOD:J */
return;
} // .end method
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ( ) {
/* .locals 4 */
/* .line 37 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 48 */
final String v0 = "auth_time_info"; // const-string v0, "auth_time_info"
/* const-string/jumbo v1, "unlock_hal_info" */
/* const-string/jumbo v2, "unlock_rate_info" */
final String v3 = "fail_reason_info"; // const-string v3, "fail_reason_info"
/* filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
this.LocalStatisticsKeyList = v0;
/* .line 56 */
/* new-instance v0, Ljava/util/LinkedHashMap; */
/* invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V */
this.JsonMap = v0;
/* .line 57 */
/* new-instance v0, Ljava/util/LinkedHashMap; */
/* invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V */
this.InstanceMap = v0;
/* .line 75 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mUnlockOption:I */
/* .line 76 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mEnrolledCount:I */
/* .line 77 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mScreenStatus:I */
/* .line 78 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I */
/* .line 90 */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J */
/* .line 91 */
/* iput-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsInit:Z */
return;
} // .end method
public static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData getInstance ( ) {
/* .locals 3 */
/* .line 95 */
final String v0 = "FingerprintLocalStatisticsData"; // const-string v0, "FingerprintLocalStatisticsData"
final String v1 = "getInstance"; // const-string v1, "getInstance"
android.util.Slog .d ( v0,v1 );
/* .line 96 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData.sInstance;
/* if-nez v0, :cond_1 */
/* .line 97 */
/* const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData; */
/* monitor-enter v0 */
/* .line 98 */
try { // :try_start_0
final String v1 = "FingerprintLocalStatisticsData"; // const-string v1, "FingerprintLocalStatisticsData"
final String v2 = "getInstance class"; // const-string v2, "getInstance class"
android.util.Slog .d ( v1,v2 );
/* .line 99 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData.sInstance;
/* if-nez v1, :cond_0 */
/* .line 100 */
final String v1 = "FingerprintLocalStatisticsData"; // const-string v1, "FingerprintLocalStatisticsData"
final String v2 = "getInstance new"; // const-string v2, "getInstance new"
android.util.Slog .d ( v1,v2 );
/* .line 101 */
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData; */
/* invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;-><init>()V */
/* .line 103 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 105 */
} // :cond_1
} // :goto_0
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData.sInstance;
} // .end method
private void initInstanceMap ( ) {
/* .locals 3 */
/* .line 109 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData .getInstance ( );
this.unlockRateInstance = v0;
/* .line 110 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData .getInstance ( );
this.failReasonInstance = v0;
/* .line 111 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData .getInstance ( );
this.authTimeInstance = v0;
/* .line 112 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData .getInstance ( );
this.halAuthInstance = v0;
/* .line 113 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData .getInstance ( );
this.halEnrollInstance = v0;
/* .line 114 */
v0 = this.InstanceMap;
/* const-string/jumbo v1, "unlock_rate_info" */
v2 = this.unlockRateInstance;
/* .line 115 */
v0 = this.InstanceMap;
final String v1 = "fail_reason_info"; // const-string v1, "fail_reason_info"
v2 = this.failReasonInstance;
/* .line 116 */
v0 = this.InstanceMap;
final String v1 = "auth_time_info"; // const-string v1, "auth_time_info"
v2 = this.authTimeInstance;
/* .line 117 */
v0 = this.InstanceMap;
/* const-string/jumbo v1, "unlock_hal_info" */
v2 = this.halAuthInstance;
/* .line 118 */
v0 = this.InstanceMap;
final String v1 = "enroll_hal_info"; // const-string v1, "enroll_hal_info"
v2 = this.halEnrollInstance;
/* .line 119 */
return;
} // .end method
/* # virtual methods */
public Boolean getModeCurValue ( ) {
/* .locals 7 */
/* .line 288 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
miui.util.ITouchFeature .getInstance ( );
/* .line 289 */
/* .local v1, "touchFeature":Lmiui/util/ITouchFeature; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 290 */
/* const/16 v2, 0x3f7 */
(( miui.util.ITouchFeature ) v1 ).getModeCurValueString ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lmiui/util/ITouchFeature;->getModeCurValueString(II)Ljava/lang/String;
/* .line 292 */
/* .local v2, "touchMode":Ljava/lang/String; */
v3 = (( java.lang.String ) v2 ).charAt ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C
int v4 = 1; // const/4 v4, 0x1
/* packed-switch v3, :pswitch_data_0 */
/* .line 300 */
/* :pswitch_0 */
int v3 = 2; // const/4 v3, 0x2
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I */
/* .line 301 */
/* .line 297 */
/* :pswitch_1 */
/* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I */
/* .line 298 */
/* .line 294 */
/* :pswitch_2 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I */
/* .line 295 */
/* nop */
/* .line 305 */
} // :goto_0
final String v3 = "FingerprintLocalStatisticsData"; // const-string v3, "FingerprintLocalStatisticsData"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "getModeCurValue, touchMode: "; // const-string v6, "getModeCurValue, touchMode: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ", filmMode: "; // const-string v6, ", filmMode: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 306 */
/* .line 311 */
} // .end local v1 # "touchFeature":Lmiui/util/ITouchFeature;
} // .end local v2 # "touchMode":Ljava/lang/String;
} // :cond_0
/* nop */
/* .line 312 */
/* .line 308 */
/* :catch_0 */
/* move-exception v1 */
/* .line 309 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 310 */
/* :pswitch_data_0 */
/* .packed-switch 0x30 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public synchronized void initLocalStatistics ( android.content.Context p0 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* monitor-enter p0 */
/* .line 123 */
int v0 = 0; // const/4 v0, 0x0
/* .line 124 */
/* .local v0, "tempIndex":I */
try { // :try_start_0
this.mContext = p1;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 125 */
/* if-nez p1, :cond_0 */
/* .line 126 */
/* monitor-exit p0 */
return;
/* .line 129 */
} // :cond_0
try { // :try_start_1
/* iget-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsInit:Z */
/* if-nez v1, :cond_6 */
/* .line 130 */
final String v1 = "FingerprintLocalStatisticsData"; // const-string v1, "FingerprintLocalStatisticsData"
final String v2 = "initLocalStatistics"; // const-string v2, "initLocalStatistics"
android.util.Slog .d ( v1,v2 );
/* .line 131 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initInstanceMap()V */
/* .line 132 */
/* new-instance v1, Ljava/io/File; */
int v2 = 0; // const/4 v2, 0x0
android.os.Environment .getDataSystemCeDirectory ( v2 );
final String v3 = "fingerprint"; // const-string v3, "fingerprint"
/* invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 133 */
/* .local v1, "prefsDir":Ljava/io/File; */
/* new-instance v2, Ljava/io/File; */
final String v3 = "fingerprint_track"; // const-string v3, "fingerprint_track"
/* invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mFpPrefsFile = v2;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 136 */
try { // :try_start_2
v3 = this.mContext;
/* const v4, 0x8000 */
(( android.content.Context ) v3 ).getSharedPreferences ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
this.mFpLocalSharedPreferences = v2;
/* .line 137 */
this.mFpSharedPreferencesEditor = v2;
/* .line 138 */
v2 = this.mFpLocalSharedPreferences;
final String v3 = "reporttimestamp"; // const-string v3, "reporttimestamp"
/* const-wide/16 v4, 0x0 */
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J */
/* :try_end_2 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 140 */
try { // :try_start_3
v2 = this.LocalStatisticsKeyList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Ljava/lang/String; */
/* .line 142 */
/* .local v3, "key":Ljava/lang/String; */
v4 = this.mFpLocalSharedPreferences;
final String v5 = ""; // const-string v5, ""
/* .line 143 */
/* .local v4, "sharedStr":Ljava/lang/String; */
v5 = (( java.lang.String ) v4 ).isEmpty ( ); // invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z
/* if-nez v5, :cond_3 */
/* .line 144 */
final String v5 = "FingerprintLocalStatisticsData"; // const-string v5, "FingerprintLocalStatisticsData"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "mLocalStatisticsInit: get: "; // const-string v7, "mLocalStatisticsInit: get: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", from SP: "; // const-string v7, ", from SP: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.String ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v6 );
/* .line 145 */
v5 = this.JsonMap;
/* new-instance v6, Lorg/json/JSONObject; */
/* invoke-direct {v6, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 146 */
v5 = this.JsonMap;
/* check-cast v5, Lorg/json/JSONObject; */
/* .line 148 */
/* .local v5, "targetJson":Lorg/json/JSONObject; */
v6 = this.InstanceMap;
/* if-nez v6, :cond_1 */
/* .line 149 */
final String v6 = "FingerprintLocalStatisticsData"; // const-string v6, "FingerprintLocalStatisticsData"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "get key of instance fail: "; // const-string v8, "get key of instance fail: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v6,v7 );
/* .line 150 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).removeTargetLocalStatistics ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->removeTargetLocalStatistics(Ljava/lang/String;)V
/* .line 151 */
/* .line 154 */
} // .end local p0 # "this":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;
} // :cond_1
v6 = this.InstanceMap;
/* check-cast v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData; */
v6 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData ) v6 ).updateJsonToData ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->updateJsonToData(Lorg/json/JSONObject;)Z
/* .line 155 */
/* .local v6, "result":Z */
/* if-nez v6, :cond_2 */
/* .line 156 */
final String v7 = "FingerprintLocalStatisticsData"; // const-string v7, "FingerprintLocalStatisticsData"
final String v8 = "initLocalStatistics unlockRateInfo exception"; // const-string v8, "initLocalStatistics unlockRateInfo exception"
android.util.Slog .e ( v7,v8 );
/* .line 157 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).removeTargetLocalStatistics ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->removeTargetLocalStatistics(Ljava/lang/String;)V
/* .line 160 */
} // .end local v5 # "targetJson":Lorg/json/JSONObject;
} // .end local v6 # "result":Z
} // :cond_2
/* .line 161 */
} // :cond_3
v5 = this.JsonMap;
/* new-instance v6, Lorg/json/JSONObject; */
/* invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V */
/* .line 162 */
v5 = this.InstanceMap;
/* check-cast v5, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData; */
/* .line 163 */
/* .local v5, "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData; */
/* if-nez v5, :cond_4 */
/* .line 164 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initInstanceMap()V */
/* .line 166 */
} // :cond_4
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).resetTargetInstanceData ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetTargetInstanceData(Ljava/lang/String;)V
/* :try_end_3 */
/* .catch Lorg/json/JSONException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 168 */
} // .end local v3 # "key":Ljava/lang/String;
} // .end local v4 # "sharedStr":Ljava/lang/String;
} // .end local v5 # "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
} // :goto_1
/* goto/16 :goto_0 */
/* .line 172 */
} // :cond_5
/* nop */
/* .line 187 */
/* nop */
/* .line 188 */
int v2 = 1; // const/4 v2, 0x1
try { // :try_start_4
/* iput-boolean v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsInit:Z */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 169 */
/* :catch_0 */
/* move-exception v2 */
/* .line 170 */
/* .local v2, "e":Lorg/json/JSONException; */
try { // :try_start_5
final String v3 = "FingerprintLocalStatisticsData"; // const-string v3, "FingerprintLocalStatisticsData"
final String v4 = "initLocalStatistics exception"; // const-string v4, "initLocalStatistics exception"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_5 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_5 ..:try_end_5} :catch_1 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 171 */
/* monitor-exit p0 */
return;
/* .line 183 */
} // .end local v2 # "e":Lorg/json/JSONException;
/* :catch_1 */
/* move-exception v2 */
/* .line 184 */
/* .local v2, "e":Ljava/lang/IllegalStateException; */
try { // :try_start_6
final String v3 = "FingerprintLocalStatisticsData"; // const-string v3, "FingerprintLocalStatisticsData"
final String v4 = "initLocalStatistics IllegalStateException"; // const-string v4, "initLocalStatistics IllegalStateException"
android.util.Slog .e ( v3,v4,v2 );
/* .line 185 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).resetLocalStatisticsData ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetLocalStatisticsData()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 186 */
/* monitor-exit p0 */
return;
/* .line 190 */
} // .end local v1 # "prefsDir":Ljava/io/File;
} // .end local v2 # "e":Ljava/lang/IllegalStateException;
} // :cond_6
} // :goto_2
/* monitor-exit p0 */
return;
/* .line 122 */
} // .end local v0 # "tempIndex":I
} // .end local p1 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void recordFpTypeAndEnrolledCount ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "unlockOption" # I */
/* .param p2, "enrolledCount" # I */
/* .line 281 */
/* move v0, p1 */
/* .line 282 */
/* .local v0, "mUnlockOption":I */
/* move v1, p2 */
/* .line 283 */
/* .local v1, "mEnrolledCount":I */
return;
} // .end method
public void removeTargetLocalStatistics ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "targetStr" # Ljava/lang/String; */
/* .line 249 */
v0 = this.mFpSharedPreferencesEditor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 250 */
/* .line 251 */
v0 = this.mFpSharedPreferencesEditor;
/* .line 252 */
v0 = this.mFpSharedPreferencesEditor;
final String v1 = "reporttimestamp"; // const-string v1, "reporttimestamp"
/* iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J */
/* .line 253 */
v0 = this.mFpSharedPreferencesEditor;
/* .line 255 */
} // :cond_0
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).resetTargetInstanceData ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetTargetInstanceData(Ljava/lang/String;)V
/* .line 256 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).updataLocalStatistics ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V
/* .line 257 */
return;
} // .end method
public void resetLocalStatisticsData ( ) {
/* .locals 4 */
/* .line 261 */
java.util.Calendar .getInstance ( );
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J */
/* .line 262 */
v0 = this.mFpSharedPreferencesEditor;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 263 */
/* .line 264 */
v0 = this.mFpSharedPreferencesEditor;
/* .line 265 */
v0 = this.mFpSharedPreferencesEditor;
final String v1 = "reporttimestamp"; // const-string v1, "reporttimestamp"
/* iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J */
/* .line 266 */
v0 = this.mFpSharedPreferencesEditor;
/* .line 269 */
} // :cond_0
v0 = this.LocalStatisticsKeyList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 270 */
/* .local v1, "targetStr":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "resetLocalStatisticsData, target: "; // const-string v3, "resetLocalStatisticsData, target: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "FingerprintLocalStatisticsData"; // const-string v3, "FingerprintLocalStatisticsData"
android.util.Slog .w ( v3,v2 );
/* .line 271 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).resetTargetInstanceData ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetTargetInstanceData(Ljava/lang/String;)V
/* .line 272 */
} // .end local v1 # "targetStr":Ljava/lang/String;
/* .line 273 */
} // :cond_1
return;
} // .end method
public void resetTargetInstanceData ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "targetStr" # Ljava/lang/String; */
/* .line 239 */
final String v0 = "FingerprintLocalStatisticsData"; // const-string v0, "FingerprintLocalStatisticsData"
final String v1 = "resetTargetInstanceData"; // const-string v1, "resetTargetInstanceData"
android.util.Slog .i ( v0,v1 );
/* .line 240 */
v0 = this.InstanceMap;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 241 */
v0 = this.InstanceMap;
/* check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData; */
/* .line 242 */
/* .local v0, "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData ) v0 ).resetLocalInfo ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->resetLocalInfo()V
/* .line 244 */
} // .end local v0 # "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
} // :cond_0
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).updataLocalStatistics ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V
/* .line 245 */
return;
} // .end method
public void startLocalStatisticsOneTrackUpload ( ) {
/* .locals 10 */
/* .line 318 */
/* const-string/jumbo v0, "startLocalStatisticsOneTrackUpload" */
final String v1 = "FingerprintLocalStatisticsData"; // const-string v1, "FingerprintLocalStatisticsData"
android.util.Slog .i ( v1,v0 );
/* .line 319 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsInit:Z */
/* if-nez v0, :cond_0 */
/* .line 320 */
final String v0 = "mLocalStatisticsInit failed, skip OneTrackUpload"; // const-string v0, "mLocalStatisticsInit failed, skip OneTrackUpload"
android.util.Slog .e ( v1,v0 );
/* .line 321 */
return;
/* .line 323 */
} // :cond_0
java.util.Calendar .getInstance ( );
(( java.util.Calendar ) v0 ).getTimeInMillis ( ); // invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
/* move-result-wide v2 */
/* .line 325 */
/* .local v2, "nowTime":J */
/* iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J */
/* const-wide/16 v6, 0x0 */
/* cmp-long v0, v4, v6 */
/* if-nez v0, :cond_1 */
/* .line 326 */
/* iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J */
/* .line 329 */
} // :cond_1
/* iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J */
/* sub-long v4, v2, v4 */
/* sget-wide v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->FP_LOCAL_STATISTICS_UPLOAD_PERIOD:J */
/* cmp-long v0, v4, v6 */
/* if-gez v0, :cond_2 */
/* .line 334 */
return;
/* .line 336 */
} // :cond_2
/* new-instance v0, Landroid/content/Intent; */
final String v4 = "onetrack.action.TRACK_EVENT"; // const-string v4, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 337 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v4 = "com.miui.analytics"; // const-string v4, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v4 ); // invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 338 */
final String v4 = "APP_ID"; // const-string v4, "APP_ID"
final String v5 = "31000000080"; // const-string v5, "31000000080"
(( android.content.Intent ) v0 ).putExtra ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 339 */
final String v4 = "EVENT_NAME"; // const-string v4, "EVENT_NAME"
final String v5 = "FingerprintUnlockInfo"; // const-string v5, "FingerprintUnlockInfo"
(( android.content.Intent ) v0 ).putExtra ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 340 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
final String v5 = "PACKAGE"; // const-string v5, "PACKAGE"
(( android.content.Intent ) v0 ).putExtra ( v5, v4 ); // invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 342 */
/* sget-boolean v4, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->IS_POWERFP:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
final String v4 = "powerFP"; // const-string v4, "powerFP"
} // :cond_3
final String v4 = "backFP"; // const-string v4, "backFP"
/* .line 343 */
/* .local v4, "fp_type":Ljava/lang/String; */
} // :goto_0
/* sget-boolean v5, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->IS_FOD:Z */
if ( v5 != null) { // if-eqz v5, :cond_4
final String v5 = "fod"; // const-string v5, "fod"
} // :cond_4
/* move-object v5, v4 */
} // :goto_1
final String v6 = "key_fp_type"; // const-string v6, "key_fp_type"
(( android.content.Intent ) v0 ).putExtra ( v6, v5 ); // invoke-virtual {v0, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
v6 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData.FP_VENDOR;
/* .line 344 */
final String v7 = "key_fp_vendor"; // const-string v7, "key_fp_vendor"
(( android.content.Intent ) v5 ).putExtra ( v7, v6 ); // invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 345 */
/* sget-boolean v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->IS_POWERFP:Z */
if ( v6 != null) { // if-eqz v6, :cond_5
/* iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mUnlockOption:I */
java.lang.String .valueOf ( v6 );
} // :cond_5
final String v6 = "null"; // const-string v6, "null"
} // :goto_2
final String v7 = "key_fp_option"; // const-string v7, "key_fp_option"
(( android.content.Intent ) v5 ).putExtra ( v7, v6 ); // invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mEnrolledCount:I */
/* .line 346 */
final String v7 = "key_fp_enrolled_count"; // const-string v7, "key_fp_enrolled_count"
(( android.content.Intent ) v5 ).putExtra ( v7, v6 ); // invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 348 */
v5 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).getModeCurValue ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getModeCurValue()Z
int v6 = 1; // const/4 v6, 0x1
/* if-ne v5, v6, :cond_6 */
/* .line 349 */
final String v5 = "key_fp_touch_film_mode"; // const-string v5, "key_fp_touch_film_mode"
/* iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I */
(( android.content.Intent ) v0 ).putExtra ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 353 */
} // :cond_6
v5 = this.mFpLocalSharedPreferences;
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 354 */
/* .line 362 */
/* .local v5, "fpLocalSaveKeyValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;" */
try { // :try_start_0
v7 = } // :goto_3
if ( v7 != null) { // if-eqz v7, :cond_8
/* check-cast v7, Ljava/lang/String; */
/* .line 363 */
/* .local v7, "key":Ljava/lang/String; */
final String v8 = "reporttimestamp"; // const-string v8, "reporttimestamp"
v8 = (( java.lang.String ) v7 ).equals ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 364 */
/* .line 366 */
} // :cond_7
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ": "; // const-string v9, ": "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v9 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v8 );
/* .line 367 */
java.lang.String .valueOf ( v8 );
(( android.content.Intent ) v0 ).putExtra ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 368 */
/* nop */
} // .end local v7 # "key":Ljava/lang/String;
/* .line 369 */
} // :cond_8
/* sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v6, :cond_9 */
/* .line 370 */
int v6 = 2; // const/4 v6, 0x2
(( android.content.Intent ) v0 ).setFlags ( v6 ); // invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 372 */
} // :cond_9
v6 = this.mContext;
v7 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v6 ).startServiceAsUser ( v0, v7 ); // invoke-virtual {v6, v0, v7}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 377 */
/* :catch_0 */
/* move-exception v6 */
/* .line 378 */
/* .local v6, "e":Ljava/lang/Exception; */
final String v7 = "initLocalStatistics unlockRateInfo exception"; // const-string v7, "initLocalStatistics unlockRateInfo exception"
android.util.Slog .e ( v1,v7,v6 );
/* .line 379 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).resetLocalStatisticsData ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetLocalStatisticsData()V
/* .line 380 */
return;
/* .line 375 */
} // .end local v6 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v6 */
/* .line 376 */
/* .local v6, "e":Ljava/lang/SecurityException; */
final String v7 = "Unable to start service."; // const-string v7, "Unable to start service."
android.util.Slog .e ( v1,v7 );
} // .end local v6 # "e":Ljava/lang/SecurityException;
/* .line 373 */
/* :catch_2 */
/* move-exception v6 */
/* .line 374 */
/* .local v6, "e":Ljava/lang/IllegalStateException; */
final String v7 = "Failed to upload FingerprintService event."; // const-string v7, "Failed to upload FingerprintService event."
android.util.Slog .e ( v1,v7 );
/* .line 381 */
} // .end local v6 # "e":Ljava/lang/IllegalStateException;
} // :goto_4
/* nop */
/* .line 382 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) p0 ).resetLocalStatisticsData ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetLocalStatisticsData()V
/* .line 384 */
} // .end local v5 # "fpLocalSaveKeyValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
} // :cond_a
return;
} // .end method
public void updataLocalStatistics ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "targetStr" # Ljava/lang/String; */
/* .line 198 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "updataLocalStatistics, target: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintLocalStatisticsData"; // const-string v1, "FingerprintLocalStatisticsData"
android.util.Slog .w ( v1,v0 );
/* .line 199 */
int v0 = 0; // const/4 v0, 0x0
/* .line 200 */
/* .local v0, "targetJson":Lorg/json/JSONObject; */
v2 = this.mFpSharedPreferencesEditor;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 202 */
try { // :try_start_0
v2 = this.InstanceMap;
/* if-nez v2, :cond_0 */
/* .line 203 */
/* const-string/jumbo v2, "updataLocalStatistics, target instance is null !" */
android.util.Slog .w ( v1,v2 );
/* .line 204 */
return;
/* .line 206 */
} // :cond_0
v2 = this.JsonMap;
/* if-nez v2, :cond_1 */
/* .line 207 */
v2 = this.JsonMap;
/* new-instance v3, Lorg/json/JSONObject; */
/* invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V */
/* .line 210 */
} // :cond_1
v2 = this.InstanceMap;
/* check-cast v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData; */
/* .line 211 */
/* .local v2, "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData; */
v3 = this.JsonMap;
/* check-cast v3, Lorg/json/JSONObject; */
/* move-object v0, v3 */
/* .line 212 */
v3 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData ) v2 ).updateDataToJson ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->updateDataToJson(Lorg/json/JSONObject;)Z
/* .line 213 */
/* .local v3, "result":Z */
/* if-nez v3, :cond_2 */
/* .line 214 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "updataLocalStatistics, update: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " fail!"; // const-string v5, " fail!"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v4 );
/* .line 215 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData ) v2 ).resetLocalInfo ( ); // invoke-virtual {v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->resetLocalInfo()V
/* .line 216 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData ) v2 ).updateDataToJson ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->updateDataToJson(Lorg/json/JSONObject;)Z
/* .line 219 */
} // :cond_2
v4 = this.mFpSharedPreferencesEditor;
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* .line 220 */
v4 = this.mFpSharedPreferencesEditor;
/* :try_end_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 224 */
} // .end local v2 # "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
} // .end local v3 # "result":Z
/* .line 221 */
/* :catch_0 */
/* move-exception v2 */
/* .line 222 */
/* .local v2, "e":Ljava/lang/IllegalStateException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "updataLocalStatistics, updata " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " IllegalStateException!"; // const-string v4, " IllegalStateException!"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3,v2 );
/* .line 223 */
return;
/* .line 229 */
} // .end local v2 # "e":Ljava/lang/IllegalStateException;
} // :cond_3
} // :goto_0
return;
} // .end method
