class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$SystemUIUnlockRateStatistics extends com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AppUnlockRateStatistic {
	 /* .source "FingerprintUnlockRateData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "SystemUIUnlockRateStatistics" */
} // .end annotation
/* # instance fields */
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit highLightAuth;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit lowLightAuth;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit screenOffAuth;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit screenOff_highLight_Auth;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit screenOff_lowLight_Auth;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit screenOnAuth;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit screenOn_highLight_Auth;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit screenOn_lowLight_Auth;
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$SystemUIUnlockRateStatistics ( ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 232 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>()V */
/* .line 223 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.screenOnAuth = v0;
/* .line 224 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.screenOffAuth = v0;
/* .line 225 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.lowLightAuth = v0;
/* .line 226 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.highLightAuth = v0;
/* .line 227 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.screenOn_lowLight_Auth = v0;
/* .line 228 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.screenOn_highLight_Auth = v0;
/* .line 229 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.screenOff_lowLight_Auth = v0;
/* .line 230 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.screenOff_highLight_Auth = v0;
/* .line 233 */
this.name = p1;
/* .line 234 */
return;
} // .end method
/* # virtual methods */
public void reset ( ) {
/* .locals 1 */
/* .line 237 */
/* invoke-super {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->reset()V */
/* .line 238 */
v0 = this.screenOnAuth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 239 */
v0 = this.screenOffAuth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 240 */
v0 = this.lowLightAuth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 241 */
v0 = this.highLightAuth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 242 */
v0 = this.screenOn_lowLight_Auth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 243 */
v0 = this.screenOn_highLight_Auth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 244 */
v0 = this.screenOff_lowLight_Auth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 245 */
v0 = this.screenOff_highLight_Auth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 246 */
return;
} // .end method
