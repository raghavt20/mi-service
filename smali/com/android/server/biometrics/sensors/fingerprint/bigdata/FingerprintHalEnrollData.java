public class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData extends com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData {
	 /* .source "FingerprintHalEnrollData.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static java.util.Map goodix_optical_map;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static volatile com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData sInstance;
/* # direct methods */
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData ( ) {
/* .locals 0 */
/* .line 24 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V */
/* .line 25 */
return;
} // .end method
public static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData getInstance ( ) {
/* .locals 3 */
/* .line 28 */
final String v0 = "FingerprintHalEnrollData"; // const-string v0, "FingerprintHalEnrollData"
final String v1 = "getInstance"; // const-string v1, "getInstance"
android.util.Slog .d ( v0,v1 );
/* .line 29 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData.sInstance;
/* if-nez v0, :cond_1 */
/* .line 30 */
/* const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData; */
/* monitor-enter v0 */
/* .line 31 */
try { // :try_start_0
	 final String v1 = "FingerprintHalEnrollData"; // const-string v1, "FingerprintHalEnrollData"
	 final String v2 = "getInstance class"; // const-string v2, "getInstance class"
	 android.util.Slog .d ( v1,v2 );
	 /* .line 32 */
	 v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData.sInstance;
	 /* if-nez v1, :cond_0 */
	 /* .line 33 */
	 final String v1 = "FingerprintHalEnrollData"; // const-string v1, "FingerprintHalEnrollData"
	 final String v2 = "getInstance new"; // const-string v2, "getInstance new"
	 android.util.Slog .d ( v1,v2 );
	 /* .line 34 */
	 /* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData; */
	 /* invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;-><init>()V */
	 /* .line 36 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 38 */
} // :cond_1
} // :goto_0
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalEnrollData.sInstance;
} // .end method
/* # virtual methods */
public void getEnrollDataFromHal ( java.lang.String p0, java.lang.Boolean p1 ) {
/* .locals 3 */
/* .param p1, "vendorName" # Ljava/lang/String; */
/* .param p2, "is_fod" # Ljava/lang/Boolean; */
/* .line 43 */
try { // :try_start_0
android.hardware.fingerprint.MiFxTunnelAidl .getInstance ( );
/* const v1, 0x7a122 */
int v2 = 0; // const/4 v2, 0x0
(( android.hardware.fingerprint.MiFxTunnelAidl ) v0 ).getHalData ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getHalData(I[B)Landroid/hardware/fingerprint/HalDataCmdResult;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 47 */
/* .line 45 */
/* :catch_0 */
/* move-exception v0 */
/* .line 46 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "FingerprintHalEnrollData"; // const-string v1, "FingerprintHalEnrollData"
final String v2 = "Get halEnrollData error."; // const-string v2, "Get halEnrollData error."
android.util.Slog .e ( v1,v2,v0 );
/* .line 48 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void parseFpcFodInfo ( ) {
/* .locals 0 */
/* .line 61 */
return;
} // .end method
public void parseFpcInfo ( ) {
/* .locals 0 */
/* .line 65 */
return;
} // .end method
public void parseGoodixFodInfo ( ) {
/* .locals 2 */
/* .line 52 */
final String v0 = "FingerprintHalEnrollData"; // const-string v0, "FingerprintHalEnrollData"
final String v1 = "parseGoodixFodInfo."; // const-string v1, "parseGoodixFodInfo."
android.util.Slog .i ( v0,v1 );
/* .line 53 */
return;
} // .end method
public void parseGoodixInfo ( ) {
/* .locals 0 */
/* .line 57 */
return;
} // .end method
public void resetLocalInfo ( ) {
/* .locals 0 */
/* .line 78 */
return;
} // .end method
public Boolean updateDataToJson ( org.json.JSONObject p0 ) {
/* .locals 1 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 73 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean updateJsonToData ( org.json.JSONObject p0 ) {
/* .locals 1 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 69 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
