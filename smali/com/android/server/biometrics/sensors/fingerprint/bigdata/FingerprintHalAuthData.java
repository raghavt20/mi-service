public class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData extends com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData {
	 /* .source "FingerprintHalAuthData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static volatile com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData sInstance;
/* # instance fields */
private java.util.List UnlockHalInfoKeyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List UnlockHalInfoValueList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "[I>;" */
/* } */
} // .end annotation
} // .end field
private org.json.JSONArray consecutiveFailJsonArray;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit consecutiveUnlockFail;
private android.hardware.fingerprint.HalDataCmdResult halAuthData;
private qualityScoreFailCountArray;
private qualityScoreSuccCountArray;
private retryCountArray;
private org.json.JSONObject unlockHalInfoJson;
private weightScoreFailCountArray;
private weightScoreSuccCountArray;
/* # direct methods */
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ( ) {
/* .locals 6 */
/* .line 59 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V */
/* .line 34 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.unlockHalInfoJson = v0;
/* .line 36 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v0, v0, [I */
this.retryCountArray = v0;
/* .line 38 */
/* const/16 v0, 0xb */
/* new-array v1, v0, [I */
this.qualityScoreSuccCountArray = v1;
/* .line 39 */
/* new-array v1, v0, [I */
this.weightScoreSuccCountArray = v1;
/* .line 40 */
/* new-array v1, v0, [I */
this.qualityScoreFailCountArray = v1;
/* .line 41 */
/* new-array v0, v0, [I */
this.weightScoreFailCountArray = v0;
/* .line 43 */
/* const-string/jumbo v0, "weight_score_fail_count" */
final String v1 = "retry_count_arr"; // const-string v1, "retry_count_arr"
final String v2 = "quality_score_succ_count"; // const-string v2, "quality_score_succ_count"
final String v3 = "quality_score_fail_count"; // const-string v3, "quality_score_fail_count"
/* const-string/jumbo v4, "weight_score_succ_count" */
/* filled-new-array {v2, v3, v4, v0, v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
this.UnlockHalInfoKeyList = v0;
/* .line 51 */
/* new-instance v0, Ljava/util/ArrayList; */
v1 = this.qualityScoreSuccCountArray;
v2 = this.qualityScoreFailCountArray;
v3 = this.weightScoreSuccCountArray;
v4 = this.weightScoreFailCountArray;
v5 = this.retryCountArray;
/* filled-new-array {v1, v2, v3, v4, v5}, [[I */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.UnlockHalInfoValueList = v0;
/* .line 439 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit-IA;)V */
this.consecutiveUnlockFail = v0;
/* .line 440 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.consecutiveFailJsonArray = v0;
/* .line 60 */
v0 = this.qualityScoreSuccCountArray;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 61 */
v0 = this.weightScoreSuccCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 62 */
v0 = this.qualityScoreFailCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 63 */
v0 = this.weightScoreFailCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 64 */
v0 = this.retryCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 65 */
return;
} // .end method
private void consecutiveUnlockFailDistribution ( ) {
/* .locals 12 */
/* .line 515 */
final String v0 = "auth_result"; // const-string v0, "auth_result"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
final String v2 = "FingerprintHalAuthData"; // const-string v2, "FingerprintHalAuthData"
if ( v1 != null) { // if-eqz v1, :cond_8
final String v1 = "quality_score"; // const-string v1, "quality_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 516 */
final String v3 = "match_score"; // const-string v3, "match_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
if ( v4 != null) { // if-eqz v4, :cond_8
final String v4 = "fail_reason_retry0"; // const-string v4, "fail_reason_retry0"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
if ( v5 != null) { // if-eqz v5, :cond_8
	 /* .line 517 */
	 final String v5 = "fail_reason_retry1"; // const-string v5, "fail_reason_retry1"
	 (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
	 /* if-nez v6, :cond_0 */
	 /* goto/16 :goto_2 */
	 /* .line 523 */
} // :cond_0
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v0 );
/* .line 524 */
/* .local v0, "auth_result":Ljava/lang/Integer; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v4 );
/* .line 525 */
/* .local v4, "fail_reason_retry0":Ljava/lang/Integer; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v5 );
/* .line 526 */
/* .local v5, "fail_reason_retry1":Ljava/lang/Integer; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v1 );
/* .line 527 */
/* .local v1, "quality_score":Ljava/lang/Integer; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v3 );
/* .line 528 */
/* .local v3, "match_score":Ljava/lang/Integer; */
v6 = this.consecutiveUnlockFail;
/* iget v6, v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
/* .line 530 */
/* .local v6, "consecutiveFaileCount":I */
v7 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
int v8 = 5; // const/4 v8, 0x5
int v9 = 1; // const/4 v9, 0x1
/* if-eq v7, v9, :cond_2 */
/* if-ge v6, v8, :cond_2 */
/* .line 531 */
v7 = this.consecutiveUnlockFail;
/* iget v7, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
/* if-nez v7, :cond_1 */
/* .line 532 */
v7 = this.consecutiveUnlockFail;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).currentTime ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->currentTime()Ljava/lang/String;
this.startFailTime = v8;
/* .line 535 */
} // :cond_1
v7 = this.consecutiveUnlockFail;
v7 = this.retry0Reason;
v8 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* aput v8, v7, v6 */
/* .line 536 */
v7 = this.consecutiveUnlockFail;
v7 = this.retry1Reason;
v8 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* aput v8, v7, v6 */
/* .line 538 */
v7 = this.consecutiveUnlockFail;
v7 = this.failedQualityScore;
v8 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* aput v8, v7, v6 */
/* .line 539 */
v7 = this.consecutiveUnlockFail;
v7 = this.failedMatchScore;
v8 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* aput v8, v7, v6 */
/* .line 540 */
v7 = this.consecutiveUnlockFail;
/* iget v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
/* add-int/2addr v8, v9 */
/* iput v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
/* .line 541 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "consecutiveUnlockFailDistribution, consecutiveFaileCount++: "; // const-string v8, "consecutiveUnlockFailDistribution, consecutiveFaileCount++: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.consecutiveUnlockFail;
/* iget v8, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v7 );
/* goto/16 :goto_1 */
/* .line 542 */
} // :cond_2
v7 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* if-eq v7, v9, :cond_3 */
/* if-ge v6, v8, :cond_4 */
/* .line 543 */
} // :cond_3
v7 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* if-ne v7, v9, :cond_7 */
int v7 = 3; // const/4 v7, 0x3
/* if-lt v6, v7, :cond_7 */
/* .line 544 */
} // :cond_4
v7 = this.consecutiveUnlockFail;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).currentTime ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->currentTime()Ljava/lang/String;
this.endFailTime = v8;
/* .line 545 */
v7 = this.consecutiveUnlockFail;
/* iget v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
/* add-int/2addr v8, v9 */
/* iput v8, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
/* .line 548 */
try { // :try_start_0
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "times_unlock_fail" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.consecutiveUnlockFail;
/* iget v8, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
java.lang.String .valueOf ( v8 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 549 */
/* .local v7, "failStr":Ljava/lang/String; */
v8 = this.consecutiveUnlockFail;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit ) v8 ).consecutiveFailToJson ( ); // invoke-virtual {v8}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailToJson()Lorg/json/JSONObject;
/* .line 550 */
/* .local v8, "temp":Lorg/json/JSONObject; */
v10 = this.consecutiveFailJsonArray;
/* if-nez v10, :cond_5 */
/* .line 551 */
final String v10 = "consecutiveUnlockFailDistribution: consecutiveFailJsonArray == null "; // const-string v10, "consecutiveUnlockFailDistribution: consecutiveFailJsonArray == null "
android.util.Slog .i ( v2,v10 );
/* .line 552 */
/* new-instance v10, Lorg/json/JSONArray; */
/* invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V */
this.consecutiveFailJsonArray = v10;
/* .line 555 */
} // :cond_5
if ( v8 != null) { // if-eqz v8, :cond_6
v10 = this.consecutiveUnlockFail;
/* iget v10, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
/* if-lez v10, :cond_6 */
/* .line 556 */
v10 = this.consecutiveFailJsonArray;
v11 = this.consecutiveUnlockFail;
/* iget v11, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
/* sub-int/2addr v11, v9 */
(( org.json.JSONArray ) v10 ).put ( v11, v8 ); // invoke-virtual {v10, v11, v8}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 560 */
} // .end local v7 # "failStr":Ljava/lang/String;
} // .end local v8 # "temp":Lorg/json/JSONObject;
} // :cond_6
/* .line 558 */
/* :catch_0 */
/* move-exception v7 */
/* .line 559 */
/* .local v7, "e":Lorg/json/JSONException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "consecutiveUnlockFailDistribution :JSONException "; // const-string v9, "consecutiveUnlockFailDistribution :JSONException "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v8 );
/* .line 561 */
} // .end local v7 # "e":Lorg/json/JSONException;
} // :goto_0
v2 = this.consecutiveUnlockFail;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit ) v2 ).reset ( ); // invoke-virtual {v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->reset()V
/* .line 563 */
} // :cond_7
v2 = this.consecutiveUnlockFail;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit ) v2 ).reset ( ); // invoke-virtual {v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->reset()V
/* .line 570 */
} // :goto_1
return;
/* .line 519 */
} // .end local v0 # "auth_result":Ljava/lang/Integer;
} // .end local v1 # "quality_score":Ljava/lang/Integer;
} // .end local v3 # "match_score":Ljava/lang/Integer;
} // .end local v4 # "fail_reason_retry0":Ljava/lang/Integer;
} // .end local v5 # "fail_reason_retry1":Ljava/lang/Integer;
} // .end local v6 # "consecutiveFaileCount":I
} // :cond_8
} // :goto_2
final String v0 = "auth_result or quality_score or match_score or fail_reason_retry0 is null"; // const-string v0, "auth_result or quality_score or match_score or fail_reason_retry0 is null"
android.util.Slog .d ( v2,v0 );
/* .line 520 */
return;
} // .end method
private Boolean getAuthDataFromHal ( ) {
/* .locals 5 */
/* .line 91 */
final String v0 = "FingerprintHalAuthData"; // const-string v0, "FingerprintHalAuthData"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
final String v2 = "getAuthDataFromHal"; // const-string v2, "getAuthDataFromHal"
android.util.Slog .d ( v0,v2 );
/* .line 92 */
android.hardware.fingerprint.MiFxTunnelAidl .getInstance ( );
/* const v3, 0x7a121 */
int v4 = 0; // const/4 v4, 0x0
(( android.hardware.fingerprint.MiFxTunnelAidl ) v2 ).getHalData ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getHalData(I[B)Landroid/hardware/fingerprint/HalDataCmdResult;
this.halAuthData = v2;
/* .line 93 */
/* iget v2, v2, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultCode:I */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 94 */
final String v2 = "Get halAuthData null"; // const-string v2, "Get halAuthData null"
android.util.Slog .e ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 95 */
/* .line 97 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 98 */
/* :catch_0 */
/* move-exception v2 */
/* .line 99 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "Get halAuthData error."; // const-string v3, "Get halAuthData error."
android.util.Slog .e ( v0,v3,v2 );
/* .line 100 */
} // .end method
public static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData getInstance ( ) {
/* .locals 3 */
/* .line 76 */
final String v0 = "FingerprintHalAuthData"; // const-string v0, "FingerprintHalAuthData"
final String v1 = "getInstance"; // const-string v1, "getInstance"
android.util.Slog .d ( v0,v1 );
/* .line 77 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData.sInstance;
/* if-nez v0, :cond_1 */
/* .line 78 */
/* const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData; */
/* monitor-enter v0 */
/* .line 79 */
try { // :try_start_0
final String v1 = "FingerprintHalAuthData"; // const-string v1, "FingerprintHalAuthData"
final String v2 = "getInstance class"; // const-string v2, "getInstance class"
android.util.Slog .d ( v1,v2 );
/* .line 80 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData.sInstance;
/* if-nez v1, :cond_0 */
/* .line 81 */
final String v1 = "FingerprintHalAuthData"; // const-string v1, "FingerprintHalAuthData"
final String v2 = "getInstance new"; // const-string v2, "getInstance new"
android.util.Slog .d ( v1,v2 );
/* .line 82 */
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData; */
/* invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;-><init>()V */
/* .line 84 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 86 */
} // :cond_1
} // :goto_0
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData.sInstance;
} // .end method
private void imageQualityDistribution ( ) {
/* .locals 6 */
/* .line 349 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "auth_result:"; // const-string v1, "auth_result:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "auth_result"; // const-string v1, "auth_result"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " ,quality_score: "; // const-string v2, " ,quality_score: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "quality_score"; // const-string v2, "quality_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "FingerprintHalAuthData"; // const-string v3, "FingerprintHalAuthData"
android.util.Slog .d ( v3,v0 );
/* .line 350 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_3
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
/* if-nez v0, :cond_0 */
/* .line 355 */
} // :cond_0
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v0 );
/* .line 356 */
/* .local v0, "auth_result":Ljava/lang/Integer; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v1 );
/* .line 357 */
/* .local v1, "quality_score":Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* const/16 v4, 0x6e */
int v5 = 1; // const/4 v5, 0x1
/* if-ne v2, v5, :cond_1 */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ltz v2, :cond_1 */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ge v2, v4, :cond_1 */
/* .line 358 */
v2 = this.qualityScoreSuccCountArray;
v3 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* div-int/lit8 v3, v3, 0xa */
/* aget v4, v2, v3 */
/* add-int/2addr v4, v5 */
/* aput v4, v2, v3 */
/* .line 366 */
} // :cond_1
v2 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* if-nez v2, :cond_2 */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ltz v2, :cond_2 */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ge v2, v4, :cond_2 */
/* .line 367 */
v2 = this.qualityScoreFailCountArray;
v3 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* div-int/lit8 v3, v3, 0xa */
/* aget v4, v2, v3 */
/* add-int/2addr v4, v5 */
/* aput v4, v2, v3 */
/* .line 374 */
} // :cond_2
final String v2 = "quality_score is illegal"; // const-string v2, "quality_score is illegal"
android.util.Slog .d ( v3,v2 );
/* .line 376 */
} // :goto_0
return;
/* .line 351 */
} // .end local v0 # "auth_result":Ljava/lang/Integer;
} // .end local v1 # "quality_score":Ljava/lang/Integer;
} // :cond_3
} // :goto_1
final String v0 = "auth_result or quality_score is null"; // const-string v0, "auth_result or quality_score is null"
android.util.Slog .i ( v3,v0 );
/* .line 352 */
return;
} // .end method
private Boolean parseAndInsertHalInfo ( android.hardware.fingerprint.HalDataCmdResult p0 ) {
/* .locals 7 */
/* .param p1, "halData" # Landroid/hardware/fingerprint/HalDataCmdResult; */
/* .line 106 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 107 */
/* .line 109 */
} // :cond_0
final String v1 = "persist.vendor.sys.fp.vendor"; // const-string v1, "persist.vendor.sys.fp.vendor"
android.os.SystemProperties .get ( v1 );
/* .line 111 */
/* .local v1, "fpType":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "parseAndInsertHalInfo, fpType:"; // const-string v3, "parseAndInsertHalInfo, fpType:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "FingerprintHalAuthData"; // const-string v3, "FingerprintHalAuthData"
android.util.Slog .d ( v3,v2 );
/* .line 112 */
if ( v1 != null) { // if-eqz v1, :cond_a
final String v2 = ""; // const-string v2, ""
/* if-ne v1, v2, :cond_1 */
/* goto/16 :goto_3 */
/* .line 116 */
} // :cond_1
v4 = this.mResultData;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).setHalData ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setHalData([B)V
/* .line 118 */
final String v4 = "goodix_fod"; // const-string v4, "goodix_fod"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_7 */
final String v4 = "goodix_fod6"; // const-string v4, "goodix_fod6"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_7 */
final String v4 = "goodix_fod7"; // const-string v4, "goodix_fod7"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_7 */
final String v4 = "goodix_fod7s"; // const-string v4, "goodix_fod7s"
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 127 */
} // :cond_2
final String v2 = "goodix"; // const-string v2, "goodix"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 128 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).setParseIndex ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V
/* .line 129 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseGoodixInfo()V */
/* .line 130 */
} // :cond_3
final String v2 = "fpc_fod"; // const-string v2, "fpc_fod"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 131 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).setParseIndex ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V
/* .line 132 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseFpcFodInfo()V */
/* .line 133 */
} // :cond_4
final String v2 = "fpc"; // const-string v2, "fpc"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 134 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).setParseIndex ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V
/* .line 135 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseFpcInfo()V */
/* .line 136 */
} // :cond_5
final String v2 = "jiiov"; // const-string v2, "jiiov"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 137 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).setParseIndex ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V
/* .line 138 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseJiiovInfo()V */
/* .line 140 */
} // :cond_6
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "unknown sensor type: " */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 141 */
/* .line 119 */
} // :cond_7
} // :goto_0
final String v4 = "persist.vendor.sys.fp.bigdata.type"; // const-string v4, "persist.vendor.sys.fp.bigdata.type"
android.os.SystemProperties .get ( v4 );
/* .line 120 */
/* .local v4, "bigdataType":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "parseAndInsertHalInfo, bigdataType:"; // const-string v6, "parseAndInsertHalInfo, bigdataType:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v5 );
/* .line 121 */
if ( v4 != null) { // if-eqz v4, :cond_9
/* if-ne v4, v2, :cond_8 */
/* .line 125 */
} // :cond_8
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).setParseIndex ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->setParseIndex(I)V
/* .line 126 */
/* invoke-direct {p0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseGoodixFodInfo(Ljava/lang/String;)V */
/* .line 127 */
} // .end local v4 # "bigdataType":Ljava/lang/String;
/* nop */
/* .line 143 */
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
/* .line 122 */
/* .restart local v4 # "bigdataType":Ljava/lang/String; */
} // :cond_9
} // :goto_2
/* .line 113 */
} // .end local v4 # "bigdataType":Ljava/lang/String;
} // :cond_a
} // :goto_3
} // .end method
private void parseFpcFodInfo ( ) {
/* .locals 0 */
/* .line 211 */
return;
} // .end method
private void parseFpcInfo ( ) {
/* .locals 0 */
/* .line 215 */
return;
} // .end method
private void parseGoodixFodInfo ( java.lang.String p0 ) {
/* .locals 17 */
/* .param p1, "bigdataType" # Ljava/lang/String; */
/* .line 148 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
final String v2 = "goodix_fod"; // const-string v2, "goodix_fod"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v3 = "fail_reason_retry2"; // const-string v3, "fail_reason_retry2"
final String v4 = "fail_reason_retry1"; // const-string v4, "fail_reason_retry1"
final String v5 = "fail_reason_retry0"; // const-string v5, "fail_reason_retry0"
final String v6 = "retry_count"; // const-string v6, "retry_count"
final String v7 = "img_area"; // const-string v7, "img_area"
final String v8 = "match_score"; // const-string v8, "match_score"
final String v9 = "quality_score"; // const-string v9, "quality_score"
int v10 = 3; // const/4 v10, 0x3
final String v11 = "auth_result"; // const-string v11, "auth_result"
final String v12 = "FingerprintHalAuthData"; // const-string v12, "FingerprintHalAuthData"
int v13 = 2; // const/4 v13, 0x2
int v14 = 1; // const/4 v14, 0x1
int v15 = 0; // const/4 v15, 0x0
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 149 */
final String v2 = "G3S"; // const-string v2, "G3S"
android.util.Slog .d ( v12,v2 );
/* .line 150 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v11, v2 ); // invoke-virtual {v0, v11, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 151 */
/* new-array v2, v10, [Ljava/lang/Integer; */
/* .line 152 */
/* .local v2, "fail_reason_retry":[Ljava/lang/Integer; */
v11 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v11 );
/* aput-object v11, v2, v15 */
/* .line 153 */
v11 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v11 );
/* aput-object v11, v2, v14 */
/* .line 154 */
v11 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v11 );
/* aput-object v11, v2, v13 */
/* .line 155 */
/* aget-object v11, v2, v15 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v5, v11 ); // invoke-virtual {v0, v5, v11}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 156 */
/* aget-object v5, v2, v14 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 157 */
/* aget-object v4, v2, v13 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 158 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v3 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v9, v3 ); // invoke-virtual {v0, v9, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 159 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v3 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v8, v3 ); // invoke-virtual {v0, v8, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 160 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v3 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v7, v3 ); // invoke-virtual {v0, v7, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 161 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v3 );
/* const-string/jumbo v4, "touch_diff" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 162 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v3 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v6, v3 ); // invoke-virtual {v0, v6, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 163 */
v3 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v3 );
/* const-string/jumbo v4, "screen_protector_type" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 164 */
/* new-array v3, v10, [Ljava/lang/Integer; */
/* .line 165 */
/* .local v3, "kpi_time_all":[Ljava/lang/Integer; */
v4 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v4 );
/* aput-object v4, v3, v15 */
/* .line 166 */
v4 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v4 );
/* aput-object v4, v3, v14 */
/* .line 167 */
v4 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v4 );
/* aput-object v4, v3, v13 */
/* .line 168 */
final String v4 = "kpi_time_all0"; // const-string v4, "kpi_time_all0"
/* aget-object v5, v3, v15 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 169 */
final String v4 = "kpi_time_all1"; // const-string v4, "kpi_time_all1"
/* aget-object v5, v3, v14 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 170 */
final String v4 = "kpi_time_all2"; // const-string v4, "kpi_time_all2"
/* aget-object v5, v3, v13 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 171 */
v4 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v4 );
final String v5 = "highlight_flag"; // const-string v5, "highlight_flag"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v5, v4 ); // invoke-virtual {v0, v5, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 173 */
} // .end local v2 # "fail_reason_retry":[Ljava/lang/Integer;
} // .end local v3 # "kpi_time_all":[Ljava/lang/Integer;
/* goto/16 :goto_0 */
} // :cond_0
final String v2 = "goodix_fod3u"; // const-string v2, "goodix_fod3u"
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 174 */
final String v2 = "G3U"; // const-string v2, "G3U"
android.util.Slog .d ( v12,v2 );
/* .line 175 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v11, v2 ); // invoke-virtual {v0, v11, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 176 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v9, v2 ); // invoke-virtual {v0, v9, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 177 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v8, v2 ); // invoke-virtual {v0, v8, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 178 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v7, v2 ); // invoke-virtual {v0, v7, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 179 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v6, v2 ); // invoke-virtual {v0, v6, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* goto/16 :goto_0 */
/* .line 181 */
} // :cond_1
final String v2 = "goodix_fod7"; // const-string v2, "goodix_fod7"
v16 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v16, :cond_2 */
final String v13 = "goodix_fod7s"; // const-string v13, "goodix_fod7s"
v13 = (( java.lang.String ) v1 ).equals ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_3
/* .line 183 */
} // :cond_2
android.util.Slog .d ( v12,v2 );
/* .line 184 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v11, v2 ); // invoke-virtual {v0, v11, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 185 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
/* const-string/jumbo v11, "weight_score" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v11, v2 ); // invoke-virtual {v0, v11, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 186 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v9, v2 ); // invoke-virtual {v0, v9, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 187 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v8, v2 ); // invoke-virtual {v0, v8, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 188 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v7, v2 ); // invoke-virtual {v0, v7, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 190 */
v2 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v2 );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v6, v2 ); // invoke-virtual {v0, v6, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 191 */
/* new-array v2, v10, [Ljava/lang/Integer; */
/* .line 192 */
/* .restart local v2 # "fail_reason_retry":[Ljava/lang/Integer; */
v6 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v6 );
/* aput-object v6, v2, v15 */
/* .line 193 */
v6 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v6 );
/* aput-object v6, v2, v14 */
/* .line 194 */
v6 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I */
java.lang.Integer .valueOf ( v6 );
int v7 = 2; // const/4 v7, 0x2
/* aput-object v6, v2, v7 */
/* .line 195 */
/* aget-object v6, v2, v15 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 196 */
/* aget-object v5, v2, v14 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 197 */
/* aget-object v4, v2, v7 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).addLocalInfo ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 203 */
} // .end local v2 # "fail_reason_retry":[Ljava/lang/Integer;
} // :cond_3
} // :goto_0
return;
} // .end method
private void parseGoodixInfo ( ) {
/* .locals 0 */
/* .line 207 */
return;
} // .end method
private void parseJiiovInfo ( ) {
/* .locals 3 */
/* .line 218 */
final String v0 = "FingerprintHalAuthData"; // const-string v0, "FingerprintHalAuthData"
final String v1 = "parseJiiovInfo"; // const-string v1, "parseJiiovInfo"
android.util.Slog .i ( v0,v1 );
/* .line 220 */
/* const/16 v0, 0xff */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataString ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataString(I)Ljava/lang/String;
final String v1 = "image_name"; // const-string v1, "image_name"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 222 */
int v0 = 0; // const/4 v0, 0x0
java.lang.Integer .valueOf ( v0 );
/* .line 223 */
/* .local v0, "auth_result":Ljava/lang/Integer; */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
/* if-nez v1, :cond_0 */
/* .line 224 */
int v1 = 1; // const/4 v1, 0x1
java.lang.Integer .valueOf ( v1 );
/* .line 226 */
} // :cond_0
final String v1 = "auth_result"; // const-string v1, "auth_result"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 227 */
final String v1 = "algo_version"; // const-string v1, "algo_version"
/* const/16 v2, 0x20 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataString ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataString(I)Ljava/lang/String;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 228 */
final String v1 = "project_version"; // const-string v1, "project_version"
/* const/16 v2, 0x20 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataString ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataString(I)Ljava/lang/String;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 229 */
final String v1 = "chip_id"; // const-string v1, "chip_id"
/* const/16 v2, 0x20 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataString ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataString(I)Ljava/lang/String;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 231 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "finger_quality_score"; // const-string v2, "finger_quality_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 232 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compare_cache_score"; // const-string v2, "compare_cache_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 233 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "seg_feat_mean" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 235 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
/* div-int/lit8 v1, v1, 0xa */
java.lang.Integer .valueOf ( v1 );
final String v2 = "quality_score"; // const-string v2, "quality_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 236 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "fp_direction"; // const-string v2, "fp_direction"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 238 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "retry_count"; // const-string v2, "retry_count"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 239 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "finger_light_score"; // const-string v2, "finger_light_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 240 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "is_studied"; // const-string v2, "is_studied"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 241 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "enrolled_template_count"; // const-string v2, "enrolled_template_count"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 242 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "fp_protector_score"; // const-string v2, "fp_protector_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 244 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "fp_temperature_score"; // const-string v2, "fp_temperature_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 245 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "finger_live_score"; // const-string v2, "finger_live_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 246 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "matched_template_idx"; // const-string v2, "matched_template_idx"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 248 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
/* div-int/lit8 v1, v1, 0xa */
java.lang.Integer .valueOf ( v1 );
final String v2 = "match_score"; // const-string v2, "match_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 249 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "img_variance"; // const-string v2, "img_variance"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 251 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "img_contrast"; // const-string v2, "img_contrast"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 252 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "finger_strange_score"; // const-string v2, "finger_strange_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 253 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "debase_quality_score"; // const-string v2, "debase_quality_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 254 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "debase_has_pattern"; // const-string v2, "debase_has_pattern"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 255 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "matched_image_sum"; // const-string v2, "matched_image_sum"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 257 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "matched_user_id"; // const-string v2, "matched_user_id"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 258 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "matched_finger_id"; // const-string v2, "matched_finger_id"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 259 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "mat_s"; // const-string v2, "mat_s"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 260 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "mat_d"; // const-string v2, "mat_d"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 261 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "mat_c"; // const-string v2, "mat_c"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 262 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "mat_cs"; // const-string v2, "mat_cs"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 264 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_cls_score"; // const-string v2, "compress_cls_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 265 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_tnum_score"; // const-string v2, "compress_tnum_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 266 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_area_score"; // const-string v2, "compress_area_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 267 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_island_score"; // const-string v2, "compress_island_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 268 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_image_pixel_1"; // const-string v2, "compress_image_pixel_1"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 269 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_image_pixel_2"; // const-string v2, "compress_image_pixel_2"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 270 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "verify_index" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 271 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "env_light"; // const-string v2, "env_light"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 273 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "is_abnormal_expo"; // const-string v2, "is_abnormal_expo"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 274 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "fusion_cnt"; // const-string v2, "fusion_cnt"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 275 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "expo_time"; // const-string v2, "expo_time"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 276 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "ghost_cache_behavior"; // const-string v2, "ghost_cache_behavior"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 277 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "dynamic_liveness_th"; // const-string v2, "dynamic_liveness_th"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 278 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "hbm_time"; // const-string v2, "hbm_time"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 279 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "keyghost_score"; // const-string v2, "keyghost_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 280 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "ctnghost_score"; // const-string v2, "ctnghost_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 283 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "small_cls_fast_reject_count" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 284 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "small_cls_fast_accept_count" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 285 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "tnum_fast_accept_count" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 286 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "glb_fast_reject_count"; // const-string v2, "glb_fast_reject_count"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 287 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "lf_fast_reject_count"; // const-string v2, "lf_fast_reject_count"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 288 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "total_cmp_cls_times" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 289 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "total_cache_count" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 290 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_seg_score"; // const-string v2, "compress_seg_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 292 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "algo_internal_failed_reason"; // const-string v2, "algo_internal_failed_reason"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 293 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_double_ghost_score"; // const-string v2, "compress_double_ghost_score"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 294 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "ta_time" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 295 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "study_time" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 297 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "signal" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 298 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "noise"; // const-string v2, "noise"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 299 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "hi_freq"; // const-string v2, "hi_freq"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 300 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "screen_leak_ratio"; // const-string v2, "screen_leak_ratio"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 302 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "fov"; // const-string v2, "fov"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 303 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "shading" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 304 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "cal_expo_time"; // const-string v2, "cal_expo_time"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 305 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "magnification"; // const-string v2, "magnification"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 309 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "algo_status0"; // const-string v2, "algo_status0"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 310 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "raw_img_sum0"; // const-string v2, "raw_img_sum0"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 311 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_image_pixel0_3"; // const-string v2, "compress_image_pixel0_3"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 312 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "capture_time0"; // const-string v2, "capture_time0"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 313 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "extract_time0"; // const-string v2, "extract_time0"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 314 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "verify_time0" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 315 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "kpi_time0"; // const-string v2, "kpi_time0"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 319 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "algo_status1"; // const-string v2, "algo_status1"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 320 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "raw_img_sum1"; // const-string v2, "raw_img_sum1"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 321 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_image_pixel1_3"; // const-string v2, "compress_image_pixel1_3"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 322 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "capture_time1"; // const-string v2, "capture_time1"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 323 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "extract_time1"; // const-string v2, "extract_time1"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 324 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "verify_time1" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 325 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "kpi_time1"; // const-string v2, "kpi_time1"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 327 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "algo_status2"; // const-string v2, "algo_status2"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 328 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "raw_img_sum2"; // const-string v2, "raw_img_sum2"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 329 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "compress_image_pixel2_3"; // const-string v2, "compress_image_pixel2_3"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 330 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "capture_time2"; // const-string v2, "capture_time2"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 331 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "extract_time2"; // const-string v2, "extract_time2"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 332 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
/* const-string/jumbo v2, "verify_time2" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 333 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "kpi_time2"; // const-string v2, "kpi_time2"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 335 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "finger_up_down_time"; // const-string v2, "finger_up_down_time"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 336 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "screen_off"; // const-string v2, "screen_off"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 337 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "finger_status"; // const-string v2, "finger_status"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 338 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "image_special_data"; // const-string v2, "image_special_data"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 339 */
v1 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getHalDataInt ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getHalDataInt()I
java.lang.Integer .valueOf ( v1 );
final String v2 = "area_ratio"; // const-string v2, "area_ratio"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).addLocalInfo ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 344 */
return;
} // .end method
private void ratioOfRetry ( ) {
/* .locals 7 */
/* .line 408 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ratioOfRetry: "; // const-string v1, "ratioOfRetry: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "retry_count"; // const-string v1, "retry_count"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "FingerprintHalAuthData"; // const-string v2, "FingerprintHalAuthData"
android.util.Slog .d ( v2,v0 );
/* .line 409 */
final String v0 = "auth_result"; // const-string v0, "auth_result"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
if ( v3 != null) { // if-eqz v3, :cond_5
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
/* if-nez v3, :cond_0 */
/* .line 414 */
} // :cond_0
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v0 );
/* .line 415 */
/* .local v0, "auth_result":Ljava/lang/Integer; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v1 );
/* .line 417 */
/* .local v1, "retry_count":Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
/* if-nez v2, :cond_2 */
/* .line 418 */
v2 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* if-ne v2, v4, :cond_1 */
/* .line 419 */
v2 = this.retryCountArray;
/* aget v5, v2, v4 */
/* add-int/2addr v5, v4 */
/* aput v5, v2, v4 */
/* .line 421 */
} // :cond_1
v2 = this.retryCountArray;
/* aget v5, v2, v3 */
/* add-int/2addr v5, v4 */
/* aput v5, v2, v3 */
/* .line 422 */
} // :cond_2
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ne v2, v4, :cond_4 */
/* .line 423 */
v2 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* if-ne v2, v4, :cond_3 */
/* .line 424 */
v2 = this.retryCountArray;
int v5 = 3; // const/4 v5, 0x3
/* aget v6, v2, v5 */
/* add-int/2addr v6, v4 */
/* aput v6, v2, v5 */
/* .line 426 */
} // :cond_3
v2 = this.retryCountArray;
/* aget v5, v2, v3 */
/* add-int/2addr v5, v4 */
/* aput v5, v2, v3 */
/* .line 427 */
int v3 = 2; // const/4 v3, 0x2
/* aget v5, v2, v3 */
/* add-int/2addr v5, v4 */
/* aput v5, v2, v3 */
/* .line 436 */
} // :cond_4
} // :goto_0
return;
/* .line 410 */
} // .end local v0 # "auth_result":Ljava/lang/Integer;
} // .end local v1 # "retry_count":Ljava/lang/Integer;
} // :cond_5
} // :goto_1
final String v0 = "auth_result or retry_count is null"; // const-string v0, "auth_result or retry_count is null"
android.util.Slog .i ( v2,v0 );
/* .line 411 */
return;
} // .end method
private void weightScoreDistribution ( ) {
/* .locals 6 */
/* .line 379 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "auth_result:"; // const-string v1, "auth_result:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "auth_result"; // const-string v1, "auth_result"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " ,weight_score: "; // const-string v2, " ,weight_score: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "weight_score" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "FingerprintHalAuthData"; // const-string v3, "FingerprintHalAuthData"
android.util.Slog .d ( v3,v0 );
/* .line 380 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_3
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
/* if-nez v0, :cond_0 */
/* .line 385 */
} // :cond_0
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v0 );
/* .line 386 */
/* .local v0, "auth_result":Ljava/lang/Integer; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).getLocalInfo ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v1 );
/* .line 387 */
/* .local v1, "weight_score":Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* const/16 v4, 0x6e */
int v5 = 1; // const/4 v5, 0x1
/* if-ne v2, v5, :cond_1 */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ltz v2, :cond_1 */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ge v2, v4, :cond_1 */
/* .line 388 */
v2 = this.weightScoreSuccCountArray;
v3 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* div-int/lit8 v3, v3, 0xa */
/* aget v4, v2, v3 */
/* add-int/2addr v4, v5 */
/* aput v4, v2, v3 */
/* .line 394 */
} // :cond_1
v2 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* if-nez v2, :cond_2 */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ltz v2, :cond_2 */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* if-ge v2, v4, :cond_2 */
/* .line 395 */
v2 = this.weightScoreFailCountArray;
v3 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* div-int/lit8 v3, v3, 0xa */
/* aget v4, v2, v3 */
/* add-int/2addr v4, v5 */
/* aput v4, v2, v3 */
/* .line 402 */
} // :cond_2
/* const-string/jumbo v2, "weight_score is illegal" */
android.util.Slog .d ( v3,v2 );
/* .line 405 */
} // :goto_0
return;
/* .line 381 */
} // .end local v0 # "auth_result":Ljava/lang/Integer;
} // .end local v1 # "weight_score":Ljava/lang/Integer;
} // :cond_3
} // :goto_1
final String v0 = "auth_result or weight_score is null"; // const-string v0, "auth_result or weight_score is null"
android.util.Slog .d ( v3,v0 );
/* .line 382 */
return;
} // .end method
/* # virtual methods */
public Boolean calculateHalAuthInfo ( ) {
/* .locals 2 */
/* .line 575 */
v0 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getAuthDataFromHal()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 576 */
/* .line 579 */
} // :cond_0
v0 = this.halAuthData;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->parseAndInsertHalInfo(Landroid/hardware/fingerprint/HalDataCmdResult;)Z */
/* if-nez v0, :cond_1 */
/* .line 580 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).clearLocalInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->clearLocalInfo()V
/* .line 581 */
/* .line 584 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->imageQualityDistribution()V */
/* .line 585 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->weightScoreDistribution()V */
/* .line 586 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->ratioOfRetry()V */
/* .line 587 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->consecutiveUnlockFailDistribution()V */
/* .line 588 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).clearLocalInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->clearLocalInfo()V
/* .line 589 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void resetLocalFpInfo ( ) {
/* .locals 2 */
/* .line 68 */
v0 = this.qualityScoreSuccCountArray;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 69 */
v0 = this.weightScoreSuccCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 70 */
v0 = this.qualityScoreFailCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 71 */
v0 = this.weightScoreFailCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 72 */
v0 = this.retryCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 73 */
return;
} // .end method
public void resetLocalInfo ( ) {
/* .locals 2 */
/* .line 657 */
final String v0 = "FingerprintHalAuthData"; // const-string v0, "FingerprintHalAuthData"
final String v1 = "resetLocalInfo"; // const-string v1, "resetLocalInfo"
android.util.Slog .d ( v0,v1 );
/* .line 658 */
v0 = this.qualityScoreSuccCountArray;
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 659 */
v0 = this.weightScoreSuccCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 660 */
v0 = this.qualityScoreFailCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 661 */
v0 = this.weightScoreFailCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 662 */
v0 = this.retryCountArray;
java.util.Arrays .fill ( v0,v1 );
/* .line 663 */
v0 = this.consecutiveUnlockFail;
/* iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
/* .line 664 */
v0 = this.consecutiveUnlockFail;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->reset()V
/* .line 665 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.consecutiveFailJsonArray = v0;
/* .line 666 */
return;
} // .end method
public Boolean updateDataToJson ( org.json.JSONObject p0 ) {
/* .locals 3 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 630 */
/* const-string/jumbo v0, "updateDataToJson" */
final String v1 = "FingerprintHalAuthData"; // const-string v1, "FingerprintHalAuthData"
android.util.Slog .d ( v1,v0 );
/* .line 631 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData.sInstance;
/* if-nez v0, :cond_0 */
/* .line 632 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData .getInstance ( );
/* .line 635 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 636 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* move-object p1, v0 */
/* .line 640 */
} // :cond_1
try { // :try_start_0
final String v0 = "quality_score_succ_count"; // const-string v0, "quality_score_succ_count"
v2 = this.qualityScoreSuccCountArray;
java.util.Arrays .toString ( v2 );
(( org.json.JSONObject ) p1 ).put ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 641 */
final String v0 = "quality_score_fail_count"; // const-string v0, "quality_score_fail_count"
v2 = this.qualityScoreFailCountArray;
java.util.Arrays .toString ( v2 );
(( org.json.JSONObject ) p1 ).put ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 642 */
/* const-string/jumbo v0, "weight_score_succ_count" */
v2 = this.weightScoreSuccCountArray;
java.util.Arrays .toString ( v2 );
(( org.json.JSONObject ) p1 ).put ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 643 */
/* const-string/jumbo v0, "weight_score_fail_count" */
v2 = this.weightScoreFailCountArray;
java.util.Arrays .toString ( v2 );
(( org.json.JSONObject ) p1 ).put ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 644 */
final String v0 = "retry_count_arr"; // const-string v0, "retry_count_arr"
v2 = this.retryCountArray;
java.util.Arrays .toString ( v2 );
(( org.json.JSONObject ) p1 ).put ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 645 */
v0 = this.consecutiveFailJsonArray;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 646 */
final String v2 = "consecutive_fail_arr"; // const-string v2, "consecutive_fail_arr"
(( org.json.JSONObject ) p1 ).put ( v2, v0 ); // invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 652 */
} // :cond_2
/* nop */
/* .line 653 */
int v0 = 1; // const/4 v0, 0x1
/* .line 649 */
/* :catch_0 */
/* move-exception v0 */
/* .line 650 */
/* .local v0, "e":Lorg/json/JSONException; */
/* const-string/jumbo v2, "updateDataToJson JSONException" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 651 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean updateJsonToData ( org.json.JSONObject p0 ) {
/* .locals 13 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 595 */
final String v0 = ""; // const-string v0, ""
final String v1 = "FingerprintHalAuthData"; // const-string v1, "FingerprintHalAuthData"
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "index":I */
} // :goto_0
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
v4 = v4 = this.UnlockHalInfoKeyList;
/* if-ge v2, v4, :cond_2 */
/* .line 596 */
v4 = this.UnlockHalInfoKeyList;
/* check-cast v4, Ljava/lang/String; */
(( org.json.JSONObject ) p1 ).getString ( v4 ); // invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 597 */
/* .local v4, "tempString":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "updateJsonToData: tempString: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v5 );
/* .line 598 */
v5 = (( java.lang.String ) v4 ).isEmpty ( ); // invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z
/* if-nez v5, :cond_1 */
/* .line 599 */
final String v5 = "["; // const-string v5, "["
(( java.lang.String ) v4 ).replace ( v5, v0 ); // invoke-virtual {v4, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v4, v5 */
/* .line 600 */
final String v5 = "]"; // const-string v5, "]"
(( java.lang.String ) v4 ).replace ( v5, v0 ); // invoke-virtual {v4, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v4, v5 */
/* .line 601 */
final String v5 = " "; // const-string v5, " "
(( java.lang.String ) v4 ).replace ( v5, v0 ); // invoke-virtual {v4, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v4, v5 */
/* .line 602 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "updateJsonToData: new tempString: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v5 );
/* .line 603 */
int v5 = 0; // const/4 v5, 0x0
/* .line 604 */
/* .local v5, "secondTempIndex":I */
final String v6 = ","; // const-string v6, ","
(( java.lang.String ) v4 ).split ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* array-length v7, v6 */
/* move v8, v3 */
} // :goto_1
/* if-ge v8, v7, :cond_1 */
/* aget-object v9, v6, v8 */
/* .line 605 */
/* .local v9, "splitstring":Ljava/lang/String; */
v10 = (( java.lang.String ) v9 ).isEmpty ( ); // invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z
/* if-nez v10, :cond_0 */
v10 = this.UnlockHalInfoValueList;
/* check-cast v10, [I */
/* array-length v10, v10 */
/* if-ge v5, v10, :cond_0 */
/* .line 606 */
v10 = this.UnlockHalInfoValueList;
/* check-cast v10, [I */
/* add-int/lit8 v11, v5, 0x1 */
} // .end local v5 # "secondTempIndex":I
/* .local v11, "secondTempIndex":I */
v12 = java.lang.Integer .parseInt ( v9 );
/* aput v12, v10, v5 */
/* move v5, v11 */
/* .line 604 */
} // .end local v9 # "splitstring":Ljava/lang/String;
} // .end local v11 # "secondTempIndex":I
/* .restart local v5 # "secondTempIndex":I */
} // :cond_0
/* add-int/lit8 v8, v8, 0x1 */
/* .line 595 */
} // .end local v5 # "secondTempIndex":I
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* goto/16 :goto_0 */
/* .line 612 */
} // .end local v2 # "index":I
} // .end local v4 # "tempString":Ljava/lang/String;
} // :cond_2
final String v0 = "consecutive_fail_arr"; // const-string v0, "consecutive_fail_arr"
(( org.json.JSONObject ) p1 ).getJSONArray ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
this.consecutiveFailJsonArray = v0;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 625 */
/* nop */
/* .line 626 */
int v0 = 1; // const/4 v0, 0x1
/* .line 622 */
/* :catch_0 */
/* move-exception v0 */
/* .line 623 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-string/jumbo v2, "updateJsonToData Exception" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 624 */
/* .line 618 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* .line 619 */
/* .local v0, "e":Ljava/lang/IndexOutOfBoundsException; */
/* const-string/jumbo v2, "updateJsonToData IndexOutOfBoundsException" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 620 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).resetLocalInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->resetLocalInfo()V
/* .line 621 */
/* .line 614 */
} // .end local v0 # "e":Ljava/lang/IndexOutOfBoundsException;
/* :catch_2 */
/* move-exception v0 */
/* .line 615 */
/* .local v0, "e":Lorg/json/JSONException; */
/* const-string/jumbo v2, "updateJsonToData JSONException" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 616 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) p0 ).resetLocalInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->resetLocalInfo()V
/* .line 617 */
} // .end method
