.class Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;
.super Ljava/lang/Object;
.source "FingerprintHalAuthData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConsecutiveUnlockFailUnit"
.end annotation


# instance fields
.field public consecutiveFailUnitJson:Lorg/json/JSONObject;

.field public consecutiveFaileCount:I

.field public dayCount:I

.field public endFailTime:Ljava/lang/String;

.field public failedMatchScore:[I

.field public failedQualityScore:[I

.field public retry0Reason:[I

.field public retry1Reason:[I

.field public startFailTime:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    .line 445
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    .line 446
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->startFailTime:Ljava/lang/String;

    .line 447
    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->endFailTime:Ljava/lang/String;

    .line 448
    const/4 v0, 0x6

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry0Reason:[I

    .line 449
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry1Reason:[I

    .line 450
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedQualityScore:[I

    .line 451
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedMatchScore:[I

    .line 453
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;-><init>()V

    return-void
.end method


# virtual methods
.method public consecutiveFailToJson()Lorg/json/JSONObject;
    .locals 4

    .line 456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dayCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,consecutiveFaileCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintHalAuthData"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :try_start_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    const-string v2, "day_count"

    iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 459
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    const-string v2, "consecutive_faile_count"

    iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 460
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    const-string/jumbo v2, "start_fail_time"

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->startFailTime:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 461
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    const-string v2, "end_fail_time"

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->endFailTime:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 462
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    const-string v2, "retry0_reason_array"

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry0Reason:[I

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 463
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    const-string v2, "retry1_reason_array"

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry1Reason:[I

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 464
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    const-string v2, "faile_quality_array"

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedQualityScore:[I

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 465
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    const-string v2, "faile_match_array"

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedMatchScore:[I

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    nop

    .line 474
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    return-object v0

    .line 466
    :catch_0
    move-exception v0

    .line 467
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "consecutiveFailToJson :JSONException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->reset()V

    .line 469
    const/4 v1, 0x0

    return-object v1
.end method

.method public printConsecutiveUnlockFailUnit()V
    .locals 5

    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "printConsecutive, dayCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,consecutiveFaileCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintHalAuthData"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry0Reason:[I

    array-length v2, v2

    const-string v3, " : "

    if-ge v0, v2, :cond_0

    .line 483
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "printConsecutive, retry0Reason: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry0Reason:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 486
    .end local v0    # "i":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry1Reason:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 487
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "printConsecutive, retry1Reason: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry1Reason:[I

    aget v4, v4, v0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 490
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedQualityScore:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 491
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "printConsecutive, failedQualityScore: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedQualityScore:[I

    aget v4, v4, v0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 494
    .end local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedMatchScore:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 495
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "printConsecutive, failedMatchScore: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedMatchScore:[I

    aget v4, v4, v0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 498
    .end local v0    # "i":I
    :cond_3
    return-void
.end method

.method public reset()V
    .locals 2

    .line 501
    const-string v0, "FingerprintHalAuthData"

    const-string v1, "reset"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I

    .line 503
    const-string v1, ""

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->startFailTime:Ljava/lang/String;

    .line 504
    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->endFailTime:Ljava/lang/String;

    .line 505
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry0Reason:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 506
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->retry1Reason:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 507
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedQualityScore:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 508
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->failedMatchScore:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->fill([II)V

    .line 509
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFailUnitJson:Lorg/json/JSONObject;

    .line 510
    return-void
.end method
