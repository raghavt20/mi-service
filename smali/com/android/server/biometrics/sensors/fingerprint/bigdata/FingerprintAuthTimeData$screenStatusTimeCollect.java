public class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$screenStatusTimeCollect {
	 /* .source "FingerprintAuthTimeData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "screenStatusTimeCollect" */
} // .end annotation
/* # instance fields */
public org.json.JSONObject highLightJSONObject;
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$timeCollect highLightTimeCollect;
public java.util.List lightStatusJsonKeyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List lightStatusJsonList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lorg/json/JSONObject;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List lightStatusList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$timeCollect lowLightCollect;
public org.json.JSONObject lowLightJSONObject;
public java.lang.String name;
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$screenStatusTimeCollect ( ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 157 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 161 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
final String v1 = "highlight"; // const-string v1, "highlight"
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;-><init>(Ljava/lang/String;)V */
this.highLightTimeCollect = v0;
/* .line 162 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
final String v1 = "lowlight"; // const-string v1, "lowlight"
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;-><init>(Ljava/lang/String;)V */
this.lowLightCollect = v0;
/* .line 163 */
/* new-instance v0, Ljava/util/ArrayList; */
v1 = this.highLightTimeCollect;
v2 = this.lowLightCollect;
/* filled-new-array {v1, v2}, [Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.lightStatusList = v0;
/* .line 168 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.highLightJSONObject = v0;
/* .line 169 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.lowLightJSONObject = v0;
/* .line 170 */
/* new-instance v0, Ljava/util/ArrayList; */
v1 = this.highLightJSONObject;
v2 = this.lowLightJSONObject;
/* filled-new-array {v1, v2}, [Lorg/json/JSONObject; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.lightStatusJsonList = v0;
/* .line 175 */
final String v0 = "hight_light_auth_time"; // const-string v0, "hight_light_auth_time"
final String v1 = "low_light_auth_time"; // const-string v1, "low_light_auth_time"
/* filled-new-array {v0, v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
this.lightStatusJsonKeyList = v0;
/* .line 158 */
this.name = p1;
/* .line 159 */
return;
} // .end method
/* # virtual methods */
public void reset ( ) {
/* .locals 2 */
/* .line 181 */
v0 = this.lightStatusList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
/* .line 182 */
/* .local v1, "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData$timeCollect ) v1 ).reset ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;->reset()V
/* .line 183 */
} // .end local v1 # "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$timeCollect;
/* .line 184 */
} // :cond_0
return;
} // .end method
