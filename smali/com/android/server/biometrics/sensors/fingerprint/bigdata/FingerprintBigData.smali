.class public abstract Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
.super Ljava/lang/Object;
.source "FingerprintBigData.java"


# static fields
.field protected static final ACQUIRED_FINGER_DOWN:I = 0x16

.field protected static final ACQUIRED_FINGER_UP:I = 0x17

.field protected static final ACQUIRED_WAIT_FINGER_INPUT:I = 0x16

.field protected static final AUTH_FAIL:I = 0x0

.field protected static final AUTH_SUCCESS:I = 0x1

.field protected static final FP_LOCAL_STATISTICS_DEBUG:Z = false

.field public static final HAL_DATA_CMD_GET_INFO_AUTH:I = 0x7a121

.field public static final HAL_DATA_CMD_GET_INFO_ENROLL:I = 0x7a122

.field public static final HAL_DATA_CMD_GET_INFO_INIT:I = 0x7a120

.field public static final HAL_DATA_PARSE_START_INDEX_FPC:I = 0x0

.field public static final HAL_DATA_PARSE_START_INDEX_FPC_FOD:I = 0x0

.field public static final HAL_DATA_PARSE_START_INDEX_GOODIX:I = 0x0

.field public static final HAL_DATA_PARSE_START_INDEX_GOODIX_FOD:I = 0x0

.field public static final HAL_DATA_PARSE_START_INDEX_JIIOV:I = 0x0

.field protected static final HBM:I = 0x0

.field protected static IS_FOD:Z = false

.field protected static final LOW_BRIGHT:I = 0x1

.field protected static final SCREEN_STATUS_DOZE:I = 0x3

.field protected static final SCREEN_STATUS_OFF:I = 0x1

.field protected static final SCREEN_STATUS_ON:I = 0x2

.field private static final TAG:Ljava/lang/String; = "FingerprintHalData"


# instance fields
.field private bigData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private currentIndex:I

.field private data:[B

.field protected mFingerUnlockBright:I

.field protected mScreenStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 35
    const-string v0, "ro.hardware.fp.fod"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->IS_FOD:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->mScreenStatus:I

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->mFingerUnlockBright:I

    .line 58
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->bigData:Ljava/util/Map;

    .line 63
    return-void
.end method


# virtual methods
.method public addLocalInfo(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->bigData:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-void
.end method

.method public clearLocalInfo()V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->bigData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 71
    return-void
.end method

.method public currentTime()Ljava/lang/String;
    .locals 10

    .line 122
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 123
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 124
    .local v2, "year":I
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v3, v1

    .line 125
    .local v3, "month":I
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 126
    .local v1, "day":I
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 127
    .local v4, "hour":I
    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 128
    .local v5, "minute":I
    const/16 v6, 0xd

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 129
    .local v6, "second":I
    const/16 v7, 0xe

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 130
    .local v7, "millisecond":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 131
    .local v8, "dayTime":Ljava/lang/String;
    return-object v8
.end method

.method public getBigData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->bigData:Ljava/util/Map;

    return-object v0
.end method

.method public getHalDataInt()I
    .locals 5

    .line 87
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->data:[B

    array-length v1, v0

    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    add-int/lit8 v3, v2, 0x4

    if-ge v1, v3, :cond_0

    .line 88
    const/4 v0, 0x0

    return v0

    .line 90
    :cond_0
    add-int/lit8 v1, v2, 0x1

    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    aget-byte v1, v0, v1

    shl-int/lit8 v1, v1, 0x8

    const v4, 0xff00

    and-int/2addr v1, v4

    or-int/2addr v1, v2

    add-int/lit8 v2, v3, 0x1

    iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    aget-byte v3, v0, v3

    shl-int/lit8 v3, v3, 0x10

    const/high16 v4, 0xff0000

    and-int/2addr v3, v4

    or-int/2addr v1, v3

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    aget-byte v0, v0, v2

    shl-int/lit8 v0, v0, 0x18

    const/high16 v2, -0x1000000

    and-int/2addr v0, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public getHalDataString(I)Ljava/lang/String;
    .locals 6
    .param p1, "len"    # I

    .line 97
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->data:[B

    array-length v0, v0

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    add-int v2, v1, p1

    if-ge v0, v2, :cond_0

    .line 98
    const-string v0, ""

    return-object v0

    .line 101
    :cond_0
    add-int/2addr v1, p1

    .line 102
    .local v1, "newcurrent":I
    new-array v0, p1, [C

    .line 103
    .local v0, "charArray":[C
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 105
    .local v3, "i":I
    :goto_0
    if-ge v3, p1, :cond_2

    .line 106
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->data:[B

    iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    aget-byte v4, v4, v5

    if-nez v4, :cond_1

    .line 107
    goto :goto_1

    .line 109
    :cond_1
    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    int-to-char v4, v4

    aput-char v4, v0, v3

    .line 110
    aget-char v4, v0, v3

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 113
    :cond_2
    :goto_1
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    .line 114
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getLocalInfo(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->bigData:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract resetLocalInfo()V
.end method

.method public setHalData([B)V
    .locals 1
    .param p1, "halData"    # [B

    .line 78
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->data:[B

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    .line 80
    return-void
.end method

.method public setParseIndex(I)V
    .locals 0
    .param p1, "start"    # I

    .line 83
    iput p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I

    .line 84
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->bigData:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 138
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->isArray()Z

    move-result v3

    const-string v4, "; "

    const-string v5, "="

    if-eqz v3, :cond_1

    .line 139
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v3

    .line 140
    .local v3, "len":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v3, :cond_0

    .line 141
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 142
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8, v6}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 144
    .end local v3    # "len":I
    .end local v6    # "i":I
    :cond_0
    goto :goto_2

    .line 145
    :cond_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_2
    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public abstract updateDataToJson(Lorg/json/JSONObject;)Z
.end method

.method public abstract updateJsonToData(Lorg/json/JSONObject;)Z
.end method
