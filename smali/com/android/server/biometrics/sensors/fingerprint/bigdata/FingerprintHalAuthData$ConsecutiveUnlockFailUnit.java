class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit {
	 /* .source "FingerprintHalAuthData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ConsecutiveUnlockFailUnit" */
} // .end annotation
/* # instance fields */
public org.json.JSONObject consecutiveFailUnitJson;
public Integer consecutiveFaileCount;
public Integer dayCount;
public java.lang.String endFailTime;
public failedMatchScore;
public failedQualityScore;
public retry0Reason;
public retry1Reason;
public java.lang.String startFailTime;
/* # direct methods */
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit ( ) {
/* .locals 2 */
/* .line 443 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 444 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
/* .line 445 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
/* .line 446 */
final String v0 = ""; // const-string v0, ""
this.startFailTime = v0;
/* .line 447 */
this.endFailTime = v0;
/* .line 448 */
int v0 = 6; // const/4 v0, 0x6
/* new-array v1, v0, [I */
this.retry0Reason = v1;
/* .line 449 */
/* new-array v1, v0, [I */
this.retry1Reason = v1;
/* .line 450 */
/* new-array v1, v0, [I */
this.failedQualityScore = v1;
/* .line 451 */
/* new-array v0, v0, [I */
this.failedMatchScore = v0;
/* .line 453 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.consecutiveFailUnitJson = v0;
return;
} // .end method
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;-><init>()V */
return;
} // .end method
/* # virtual methods */
public org.json.JSONObject consecutiveFailToJson ( ) {
/* .locals 4 */
/* .line 456 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "dayCount: "; // const-string v1, "dayCount: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " ,consecutiveFaileCount: "; // const-string v1, " ,consecutiveFaileCount: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintHalAuthData"; // const-string v1, "FingerprintHalAuthData"
android.util.Slog .d ( v1,v0 );
/* .line 458 */
try { // :try_start_0
	 v0 = this.consecutiveFailUnitJson;
	 final String v2 = "day_count"; // const-string v2, "day_count"
	 /* iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
	 java.lang.String .valueOf ( v3 );
	 (( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 459 */
	 v0 = this.consecutiveFailUnitJson;
	 final String v2 = "consecutive_faile_count"; // const-string v2, "consecutive_faile_count"
	 /* iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
	 java.lang.String .valueOf ( v3 );
	 (( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 460 */
	 v0 = this.consecutiveFailUnitJson;
	 /* const-string/jumbo v2, "start_fail_time" */
	 v3 = this.startFailTime;
	 (( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 461 */
	 v0 = this.consecutiveFailUnitJson;
	 final String v2 = "end_fail_time"; // const-string v2, "end_fail_time"
	 v3 = this.endFailTime;
	 (( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 462 */
	 v0 = this.consecutiveFailUnitJson;
	 final String v2 = "retry0_reason_array"; // const-string v2, "retry0_reason_array"
	 v3 = this.retry0Reason;
	 java.util.Arrays .toString ( v3 );
	 (( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 463 */
	 v0 = this.consecutiveFailUnitJson;
	 final String v2 = "retry1_reason_array"; // const-string v2, "retry1_reason_array"
	 v3 = this.retry1Reason;
	 java.util.Arrays .toString ( v3 );
	 (( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 464 */
	 v0 = this.consecutiveFailUnitJson;
	 final String v2 = "faile_quality_array"; // const-string v2, "faile_quality_array"
	 v3 = this.failedQualityScore;
	 java.util.Arrays .toString ( v3 );
	 (( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 465 */
	 v0 = this.consecutiveFailUnitJson;
	 final String v2 = "faile_match_array"; // const-string v2, "faile_match_array"
	 v3 = this.failedMatchScore;
	 java.util.Arrays .toString ( v3 );
	 (( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* :try_end_0 */
	 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 470 */
	 /* nop */
	 /* .line 474 */
	 v0 = this.consecutiveFailUnitJson;
	 /* .line 466 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 467 */
	 /* .local v0, "e":Lorg/json/JSONException; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "consecutiveFailToJson :JSONException "; // const-string v3, "consecutiveFailToJson :JSONException "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v2 );
	 /* .line 468 */
	 (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData$ConsecutiveUnlockFailUnit ) p0 ).reset ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->reset()V
	 /* .line 469 */
	 int v1 = 0; // const/4 v1, 0x0
} // .end method
public void printConsecutiveUnlockFailUnit ( ) {
	 /* .locals 5 */
	 /* .line 479 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "printConsecutive, dayCount: "; // const-string v1, "printConsecutive, dayCount: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->dayCount:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = " ,consecutiveFaileCount: "; // const-string v1, " ,consecutiveFaileCount: "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "FingerprintHalAuthData"; // const-string v1, "FingerprintHalAuthData"
	 android.util.Slog .d ( v1,v0 );
	 /* .line 482 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .local v0, "i":I */
} // :goto_0
v2 = this.retry0Reason;
/* array-length v2, v2 */
final String v3 = " : "; // const-string v3, " : "
/* if-ge v0, v2, :cond_0 */
/* .line 483 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "printConsecutive, retry0Reason: "; // const-string v4, "printConsecutive, retry0Reason: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.retry0Reason;
/* aget v3, v3, v0 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 482 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 486 */
} // .end local v0 # "i":I
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_1
v2 = this.retry1Reason;
/* array-length v2, v2 */
/* if-ge v0, v2, :cond_1 */
/* .line 487 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "printConsecutive, retry1Reason: "; // const-string v4, "printConsecutive, retry1Reason: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.retry1Reason;
/* aget v4, v4, v0 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 486 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 490 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_2
v2 = this.failedQualityScore;
/* array-length v2, v2 */
/* if-ge v0, v2, :cond_2 */
/* .line 491 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "printConsecutive, failedQualityScore: "; // const-string v4, "printConsecutive, failedQualityScore: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.failedQualityScore;
/* aget v4, v4, v0 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 490 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 494 */
} // .end local v0 # "i":I
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .restart local v0 # "i":I */
} // :goto_3
v2 = this.failedMatchScore;
/* array-length v2, v2 */
/* if-ge v0, v2, :cond_3 */
/* .line 495 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "printConsecutive, failedMatchScore: "; // const-string v4, "printConsecutive, failedMatchScore: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.failedMatchScore;
/* aget v4, v4, v0 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 494 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 498 */
} // .end local v0 # "i":I
} // :cond_3
return;
} // .end method
public void reset ( ) {
/* .locals 2 */
/* .line 501 */
final String v0 = "FingerprintHalAuthData"; // const-string v0, "FingerprintHalAuthData"
final String v1 = "reset"; // const-string v1, "reset"
android.util.Slog .d ( v0,v1 );
/* .line 502 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData$ConsecutiveUnlockFailUnit;->consecutiveFaileCount:I */
/* .line 503 */
final String v1 = ""; // const-string v1, ""
this.startFailTime = v1;
/* .line 504 */
this.endFailTime = v1;
/* .line 505 */
v1 = this.retry0Reason;
java.util.Arrays .fill ( v1,v0 );
/* .line 506 */
v1 = this.retry1Reason;
java.util.Arrays .fill ( v1,v0 );
/* .line 507 */
v1 = this.failedQualityScore;
java.util.Arrays .fill ( v1,v0 );
/* .line 508 */
v1 = this.failedMatchScore;
java.util.Arrays .fill ( v1,v0 );
/* .line 509 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.consecutiveFailUnitJson = v0;
/* .line 510 */
return;
} // .end method
