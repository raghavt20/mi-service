class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit {
	 /* .source "FingerprintUnlockRateData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "UnlockRateUnit" */
} // .end annotation
/* # instance fields */
public Integer authCount;
public Integer authSuccessCount;
/* # direct methods */
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ( ) {
/* .locals 1 */
/* .line 132 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 134 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 135 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
return;
} // .end method
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void reset ( ) {
/* .locals 1 */
/* .line 137 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 138 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 139 */
return;
} // .end method
