.class Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;
.super Ljava/lang/Object;
.source "FingerprintAuthTimeData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "authTimeRecord"
.end annotation


# instance fields
.field public name:Ljava/lang/String;

.field private screenDozeAuthTimeJson:Lorg/json/JSONObject;

.field public screenDozeTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

.field private screenOffAuthTimeJson:Lorg/json/JSONObject;

.field public screenOffTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

.field private screenOnAuthTimeJson:Lorg/json/JSONObject;

.field public screenOnTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

.field private screenStatusJsonKeyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public screenStatusJsonList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field public screenStatusList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetscreenStatusJsonKeyList(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusJsonKeyList:Ljava/util/List;

    return-object p0
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    const-string v1, "screenOff"

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenOffTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    .line 195
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    const-string v1, "screenOn"

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenOnTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    .line 196
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    const-string v1, "screenDoze"

    invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenDozeTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    .line 198
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenOffTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenOnTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenDozeTimeCollect:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    filled-new-array {v1, v2, v3}, [Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusList:Ljava/util/List;

    .line 204
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenOffAuthTimeJson:Lorg/json/JSONObject;

    .line 205
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenOnAuthTimeJson:Lorg/json/JSONObject;

    .line 206
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenDozeAuthTimeJson:Lorg/json/JSONObject;

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenOffAuthTimeJson:Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenOnAuthTimeJson:Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenDozeAuthTimeJson:Lorg/json/JSONObject;

    filled-new-array {v1, v2, v3}, [Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusJsonList:Ljava/util/List;

    .line 214
    const-string v0, "screen_on_auth_time"

    const-string v1, "screen_doze_auth_time"

    const-string v2, "screen_off_auth_time"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusJsonKeyList:Ljava/util/List;

    .line 191
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->name:Ljava/lang/String;

    .line 192
    return-void
.end method


# virtual methods
.method public reset()V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$authTimeRecord;->screenStatusList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;

    .line 222
    .local v1, "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
    invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;->reset()V

    .line 223
    .end local v1    # "object":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData$screenStatusTimeCollect;
    goto :goto_0

    .line 224
    :cond_0
    return-void
.end method
