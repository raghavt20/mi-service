class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AppUnlockRateStatistic {
	 /* .source "FingerprintUnlockRateData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "AppUnlockRateStatistic" */
} // .end annotation
/* # instance fields */
public java.lang.String name;
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit totalAuth;
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AppUnlockRateStatistic ( ) {
/* .locals 2 */
/* .line 148 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 146 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.totalAuth = v0;
/* .line 148 */
return;
} // .end method
 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AppUnlockRateStatistic ( ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 150 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 146 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.totalAuth = v0;
/* .line 151 */
this.name = p1;
/* .line 152 */
return;
} // .end method
/* # virtual methods */
public void reset ( ) {
/* .locals 1 */
/* .line 155 */
v0 = this.totalAuth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 156 */
return;
} // .end method
