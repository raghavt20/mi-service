public abstract class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData {
	 /* .source "FingerprintBigData.java" */
	 /* # static fields */
	 protected static final Integer ACQUIRED_FINGER_DOWN;
	 protected static final Integer ACQUIRED_FINGER_UP;
	 protected static final Integer ACQUIRED_WAIT_FINGER_INPUT;
	 protected static final Integer AUTH_FAIL;
	 protected static final Integer AUTH_SUCCESS;
	 protected static final Boolean FP_LOCAL_STATISTICS_DEBUG;
	 public static final Integer HAL_DATA_CMD_GET_INFO_AUTH;
	 public static final Integer HAL_DATA_CMD_GET_INFO_ENROLL;
	 public static final Integer HAL_DATA_CMD_GET_INFO_INIT;
	 public static final Integer HAL_DATA_PARSE_START_INDEX_FPC;
	 public static final Integer HAL_DATA_PARSE_START_INDEX_FPC_FOD;
	 public static final Integer HAL_DATA_PARSE_START_INDEX_GOODIX;
	 public static final Integer HAL_DATA_PARSE_START_INDEX_GOODIX_FOD;
	 public static final Integer HAL_DATA_PARSE_START_INDEX_JIIOV;
	 protected static final Integer HBM;
	 protected static Boolean IS_FOD;
	 protected static final Integer LOW_BRIGHT;
	 protected static final Integer SCREEN_STATUS_DOZE;
	 protected static final Integer SCREEN_STATUS_OFF;
	 protected static final Integer SCREEN_STATUS_ON;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private java.util.Map bigData;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Object;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Integer currentIndex;
private data;
protected Integer mFingerUnlockBright;
protected Integer mScreenStatus;
/* # direct methods */
static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData ( ) {
/* .locals 2 */
/* .line 35 */
final String v0 = "ro.hardware.fp.fod"; // const-string v0, "ro.hardware.fp.fod"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData.IS_FOD = (v0!= 0);
return;
} // .end method
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData ( ) {
/* .locals 1 */
/* .line 61 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 36 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->mScreenStatus:I */
/* .line 41 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->mFingerUnlockBright:I */
/* .line 58 */
/* new-instance v0, Ljava/util/LinkedHashMap; */
/* invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V */
this.bigData = v0;
/* .line 63 */
return;
} // .end method
/* # virtual methods */
public void addLocalInfo ( java.lang.String p0, java.lang.Object p1 ) {
/* .locals 1 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .line 66 */
v0 = this.bigData;
/* .line 67 */
return;
} // .end method
public void clearLocalInfo ( ) {
/* .locals 1 */
/* .line 70 */
v0 = this.bigData;
/* .line 71 */
return;
} // .end method
public java.lang.String currentTime ( ) {
/* .locals 10 */
/* .line 122 */
java.util.Calendar .getInstance ( );
/* .line 123 */
/* .local v0, "calendar":Ljava/util/Calendar; */
int v1 = 1; // const/4 v1, 0x1
v2 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
/* .line 124 */
/* .local v2, "year":I */
int v3 = 2; // const/4 v3, 0x2
v3 = (( java.util.Calendar ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I
/* add-int/2addr v3, v1 */
/* .line 125 */
/* .local v3, "month":I */
int v1 = 5; // const/4 v1, 0x5
v1 = (( java.util.Calendar ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
/* .line 126 */
/* .local v1, "day":I */
/* const/16 v4, 0xb */
v4 = (( java.util.Calendar ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I
/* .line 127 */
/* .local v4, "hour":I */
/* const/16 v5, 0xc */
v5 = (( java.util.Calendar ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I
/* .line 128 */
/* .local v5, "minute":I */
/* const/16 v6, 0xd */
v6 = (( java.util.Calendar ) v0 ).get ( v6 ); // invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I
/* .line 129 */
/* .local v6, "second":I */
/* const/16 v7, 0xe */
v7 = (( java.util.Calendar ) v0 ).get ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I
/* .line 130 */
/* .local v7, "millisecond":I */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = "-"; // const-string v9, "-"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " "; // const-string v9, " "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ":"; // const-string v9, ":"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 131 */
/* .local v8, "dayTime":Ljava/lang/String; */
} // .end method
public java.util.Map getBigData ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 118 */
v0 = this.bigData;
} // .end method
public Integer getHalDataInt ( ) {
/* .locals 5 */
/* .line 87 */
v0 = this.data;
/* array-length v1, v0 */
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* add-int/lit8 v3, v2, 0x4 */
/* if-ge v1, v3, :cond_0 */
/* .line 88 */
int v0 = 0; // const/4 v0, 0x0
/* .line 90 */
} // :cond_0
/* add-int/lit8 v1, v2, 0x1 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* aget-byte v2, v0, v2 */
/* and-int/lit16 v2, v2, 0xff */
/* add-int/lit8 v3, v1, 0x1 */
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* aget-byte v1, v0, v1 */
/* shl-int/lit8 v1, v1, 0x8 */
/* const v4, 0xff00 */
/* and-int/2addr v1, v4 */
/* or-int/2addr v1, v2 */
/* add-int/lit8 v2, v3, 0x1 */
/* iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* aget-byte v3, v0, v3 */
/* shl-int/lit8 v3, v3, 0x10 */
/* const/high16 v4, 0xff0000 */
/* and-int/2addr v3, v4 */
/* or-int/2addr v1, v3 */
/* add-int/lit8 v3, v2, 0x1 */
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* aget-byte v0, v0, v2 */
/* shl-int/lit8 v0, v0, 0x18 */
/* const/high16 v2, -0x1000000 */
/* and-int/2addr v0, v2 */
/* or-int/2addr v0, v1 */
} // .end method
public java.lang.String getHalDataString ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "len" # I */
/* .line 97 */
v0 = this.data;
/* array-length v0, v0 */
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* add-int v2, v1, p1 */
/* if-ge v0, v2, :cond_0 */
/* .line 98 */
final String v0 = ""; // const-string v0, ""
/* .line 101 */
} // :cond_0
/* add-int/2addr v1, p1 */
/* .line 102 */
/* .local v1, "newcurrent":I */
/* new-array v0, p1, [C */
/* .line 103 */
/* .local v0, "charArray":[C */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 104 */
/* .local v2, "sb":Ljava/lang/StringBuilder; */
int v3 = 0; // const/4 v3, 0x0
/* .line 105 */
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, p1, :cond_2 */
/* .line 106 */
v4 = this.data;
/* iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* aget-byte v4, v4, v5 */
/* if-nez v4, :cond_1 */
/* .line 107 */
/* .line 109 */
} // :cond_1
/* add-int/lit8 v5, v5, 0x1 */
/* iput v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* int-to-char v4, v4 */
/* aput-char v4, v0, v3 */
/* .line 110 */
/* aget-char v4, v0, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 113 */
} // :cond_2
} // :goto_1
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* .line 114 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public java.lang.Object getLocalInfo ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "key" # Ljava/lang/String; */
/* .line 74 */
v0 = this.bigData;
} // .end method
public abstract void resetLocalInfo ( ) {
} // .end method
public void setHalData ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "halData" # [B */
/* .line 78 */
this.data = p1;
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* .line 80 */
return;
} // .end method
public void setParseIndex ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "start" # I */
/* .line 83 */
/* iput p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->currentIndex:I */
/* .line 84 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 9 */
/* .line 136 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 137 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
v1 = this.bigData;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 138 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;" */
(( java.lang.Object ) v3 ).getClass ( ); // invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
v3 = (( java.lang.Class ) v3 ).isArray ( ); // invoke-virtual {v3}, Ljava/lang/Class;->isArray()Z
final String v4 = "; "; // const-string v4, "; "
final String v5 = "="; // const-string v5, "="
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 139 */
v3 = java.lang.reflect.Array .getLength ( v3 );
/* .line 140 */
/* .local v3, "len":I */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_1
/* if-ge v6, v3, :cond_0 */
/* .line 141 */
/* check-cast v7, Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "["; // const-string v8, "["
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = "]"; // const-string v8, "]"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 142 */
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.reflect.Array .get ( v8,v6 );
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 140 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 144 */
} // .end local v3 # "len":I
} // .end local v6 # "i":I
} // :cond_0
/* .line 145 */
} // :cond_1
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 147 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
} // :goto_2
/* .line 148 */
} // :cond_2
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public abstract Boolean updateDataToJson ( org.json.JSONObject p0 ) {
} // .end method
public abstract Boolean updateJsonToData ( org.json.JSONObject p0 ) {
} // .end method
