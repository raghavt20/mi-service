.class public Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;
.super Ljava/lang/Object;
.source "FingerprintLocalStatisticsData.java"


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000080"

.field private static final EVENT_NAME:Ljava/lang/String; = "FingerprintUnlockInfo"

.field private static final FLAG_NON_ANONYMOUS:I = 0x2

.field private static final FP_LOCAL_STATISTICS_DEBUG:Z = false

.field private static final FP_LOCAL_STATISTICS_DIR:Ljava/lang/String; = "fingerprint"

.field private static final FP_LOCAL_STATISTICS_ENABLED:Z

.field private static final FP_LOCAL_STATISTICS_PREFS_FILE:Ljava/lang/String; = "fingerprint_track"

.field private static final FP_LOCAL_STATISTICS_UPLOAD_PERIOD:J

.field private static FP_VENDOR:Ljava/lang/String; = null

.field private static IS_FOD:Z = false

.field private static IS_POWERFP:Z = false

.field private static final KEY_FP_ENROLLED_COUNT:Ljava/lang/String; = "key_fp_enrolled_count"

.field private static final KEY_FP_OPTION:Ljava/lang/String; = "key_fp_option"

.field private static final KEY_FP_TOUCH_FILM_MODE:Ljava/lang/String; = "key_fp_touch_film_mode"

.field private static final KEY_FP_TYPE:Ljava/lang/String; = "key_fp_type"

.field private static final KEY_FP_VENDOR:Ljava/lang/String; = "key_fp_vendor"

.field private static final ONETRACK_ACTION:Ljava/lang/String; = "onetrack.action.TRACK_EVENT"

.field private static final ONETRACK_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static final TAG:Ljava/lang/String; = "FingerprintLocalStatisticsData"

.field private static volatile sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;


# instance fields
.field private InstanceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;",
            ">;"
        }
    .end annotation
.end field

.field private JsonMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private LocalStatisticsKeyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private authTimeInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

.field private failReasonInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

.field private filmMode:I

.field private halAuthInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

.field private halEnrollInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

.field private mContext:Landroid/content/Context;

.field private mEnrolledCount:I

.field private mFpLocalSharedPreferences:Landroid/content/SharedPreferences;

.field private mFpPrefsFile:Ljava/io/File;

.field private mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

.field private mLocalStatisticsInit:Z

.field private mLocalStatisticsUploadTime:J

.field private mScreenStatus:I

.field private mUnlockOption:I

.field private unlockRateInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 70
    const-string v0, "ro.hardware.fp.fod"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->IS_FOD:Z

    .line 71
    const-string v0, "ro.hardware.fp.sideCap"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->IS_POWERFP:Z

    .line 72
    const-string v0, "persist.vendor.sys.fp.vendor"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->FP_VENDOR:Ljava/lang/String;

    .line 82
    const-string v0, "persist.vendor.sys.fp.onetrack.enable"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->FP_LOCAL_STATISTICS_ENABLED:Z

    .line 85
    const-string v0, "ro.hardware.fp.onetrack.period"

    const-wide/32 v1, 0x5265c00

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->FP_LOCAL_STATISTICS_UPLOAD_PERIOD:J

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "auth_time_info"

    const-string/jumbo v1, "unlock_hal_info"

    const-string/jumbo v2, "unlock_rate_info"

    const-string v3, "fail_reason_info"

    filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->LocalStatisticsKeyList:Ljava/util/List;

    .line 56
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->JsonMap:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mUnlockOption:I

    .line 76
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mEnrolledCount:I

    .line 77
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mScreenStatus:I

    .line 78
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I

    .line 90
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J

    .line 91
    iput-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsInit:Z

    return-void
.end method

.method public static getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;
    .locals 3

    .line 95
    const-string v0, "FingerprintLocalStatisticsData"

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    if-nez v0, :cond_1

    .line 97
    const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    monitor-enter v0

    .line 98
    :try_start_0
    const-string v1, "FingerprintLocalStatisticsData"

    const-string v2, "getInstance class"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    if-nez v1, :cond_0

    .line 100
    const-string v1, "FingerprintLocalStatisticsData"

    const-string v2, "getInstance new"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;-><init>()V

    sput-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    .line 103
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 105
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    return-object v0
.end method

.method private initInstanceMap()V
    .locals 3

    .line 109
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->unlockRateInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    .line 110
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->failReasonInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    .line 111
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->authTimeInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    .line 112
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->halAuthInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    .line 113
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->halEnrollInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    .line 114
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    const-string/jumbo v1, "unlock_rate_info"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->unlockRateInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    const-string v1, "fail_reason_info"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->failReasonInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    const-string v1, "auth_time_info"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->authTimeInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    const-string/jumbo v1, "unlock_hal_info"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->halAuthInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    const-string v1, "enroll_hal_info"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->halEnrollInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    return-void
.end method


# virtual methods
.method public getModeCurValue()Z
    .locals 7

    .line 288
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v1

    .line 289
    .local v1, "touchFeature":Lmiui/util/ITouchFeature;
    if-eqz v1, :cond_0

    .line 290
    const/16 v2, 0x3f7

    invoke-virtual {v1, v0, v2}, Lmiui/util/ITouchFeature;->getModeCurValueString(II)Ljava/lang/String;

    move-result-object v2

    .line 292
    .local v2, "touchMode":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/4 v4, 0x1

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 300
    :pswitch_0
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I

    .line 301
    goto :goto_0

    .line 297
    :pswitch_1
    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I

    .line 298
    goto :goto_0

    .line 294
    :pswitch_2
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I

    .line 295
    nop

    .line 305
    :goto_0
    const-string v3, "FingerprintLocalStatisticsData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getModeCurValue, touchMode:  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", filmMode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    return v4

    .line 311
    .end local v1    # "touchFeature":Lmiui/util/ITouchFeature;
    .end local v2    # "touchMode":Ljava/lang/String;
    :cond_0
    nop

    .line 312
    return v0

    .line 308
    :catch_0
    move-exception v1

    .line 309
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 310
    return v0

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized initLocalStatistics(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    monitor-enter p0

    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "tempIndex":I
    :try_start_0
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mContext:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    if-nez p1, :cond_0

    .line 126
    monitor-exit p0

    return-void

    .line 129
    :cond_0
    :try_start_1
    iget-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsInit:Z

    if-nez v1, :cond_6

    .line 130
    const-string v1, "FingerprintLocalStatisticsData"

    const-string v2, "initLocalStatistics"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initInstanceMap()V

    .line 132
    new-instance v1, Ljava/io/File;

    const/4 v2, 0x0

    invoke-static {v2}, Landroid/os/Environment;->getDataSystemCeDirectory(I)Ljava/io/File;

    move-result-object v2

    const-string v3, "fingerprint"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 133
    .local v1, "prefsDir":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v3, "fingerprint_track"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpPrefsFile:Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :try_start_2
    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mContext:Landroid/content/Context;

    const v4, 0x8000

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpLocalSharedPreferences:Landroid/content/SharedPreferences;

    .line 137
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    .line 138
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpLocalSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "reporttimestamp"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 140
    :try_start_3
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->LocalStatisticsKeyList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 142
    .local v3, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpLocalSharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, ""

    invoke-interface {v4, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 143
    .local v4, "sharedStr":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 144
    const-string v5, "FingerprintLocalStatisticsData"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mLocalStatisticsInit: get: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", from SP: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->JsonMap:Ljava/util/Map;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->JsonMap:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/json/JSONObject;

    .line 148
    .local v5, "targetJson":Lorg/json/JSONObject;
    iget-object v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 149
    const-string v6, "FingerprintLocalStatisticsData"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get key of instance fail: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->removeTargetLocalStatistics(Ljava/lang/String;)V

    .line 151
    goto :goto_0

    .line 154
    .end local p0    # "this":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;
    :cond_1
    iget-object v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;

    invoke-virtual {v6, v5}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->updateJsonToData(Lorg/json/JSONObject;)Z

    move-result v6

    .line 155
    .local v6, "result":Z
    if-nez v6, :cond_2

    .line 156
    const-string v7, "FingerprintLocalStatisticsData"

    const-string v8, "initLocalStatistics unlockRateInfo exception"

    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->removeTargetLocalStatistics(Ljava/lang/String;)V

    .line 160
    .end local v5    # "targetJson":Lorg/json/JSONObject;
    .end local v6    # "result":Z
    :cond_2
    goto :goto_1

    .line 161
    :cond_3
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->JsonMap:Ljava/util/Map;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;

    .line 163
    .local v5, "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
    if-nez v5, :cond_4

    .line 164
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initInstanceMap()V

    .line 166
    :cond_4
    invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetTargetInstanceData(Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 168
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "sharedStr":Ljava/lang/String;
    .end local v5    # "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
    :goto_1
    goto/16 :goto_0

    .line 172
    :cond_5
    nop

    .line 187
    nop

    .line 188
    const/4 v2, 0x1

    :try_start_4
    iput-boolean v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsInit:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 169
    :catch_0
    move-exception v2

    .line 170
    .local v2, "e":Lorg/json/JSONException;
    :try_start_5
    const-string v3, "FingerprintLocalStatisticsData"

    const-string v4, "initLocalStatistics exception"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 171
    monitor-exit p0

    return-void

    .line 183
    .end local v2    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v2

    .line 184
    .local v2, "e":Ljava/lang/IllegalStateException;
    :try_start_6
    const-string v3, "FingerprintLocalStatisticsData"

    const-string v4, "initLocalStatistics IllegalStateException"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 185
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetLocalStatisticsData()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 186
    monitor-exit p0

    return-void

    .line 190
    .end local v1    # "prefsDir":Ljava/io/File;
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :cond_6
    :goto_2
    monitor-exit p0

    return-void

    .line 122
    .end local v0    # "tempIndex":I
    .end local p1    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public recordFpTypeAndEnrolledCount(II)V
    .locals 2
    .param p1, "unlockOption"    # I
    .param p2, "enrolledCount"    # I

    .line 281
    move v0, p1

    .line 282
    .local v0, "mUnlockOption":I
    move v1, p2

    .line 283
    .local v1, "mEnrolledCount":I
    return-void
.end method

.method public removeTargetLocalStatistics(Ljava/lang/String;)V
    .locals 4
    .param p1, "targetStr"    # Ljava/lang/String;

    .line 249
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    if-eqz v0, :cond_0

    .line 250
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 251
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 252
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "reporttimestamp"

    iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 253
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 255
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetTargetInstanceData(Ljava/lang/String;)V

    .line 256
    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V

    .line 257
    return-void
.end method

.method public resetLocalStatisticsData()V
    .locals 4

    .line 261
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J

    .line 262
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    if-eqz v0, :cond_0

    .line 263
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 264
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 265
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "reporttimestamp"

    iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 266
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->LocalStatisticsKeyList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 270
    .local v1, "targetStr":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetLocalStatisticsData, target: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "FingerprintLocalStatisticsData"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-virtual {p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetTargetInstanceData(Ljava/lang/String;)V

    .line 272
    .end local v1    # "targetStr":Ljava/lang/String;
    goto :goto_0

    .line 273
    :cond_1
    return-void
.end method

.method public resetTargetInstanceData(Ljava/lang/String;)V
    .locals 2
    .param p1, "targetStr"    # Ljava/lang/String;

    .line 239
    const-string v0, "FingerprintLocalStatisticsData"

    const-string v1, "resetTargetInstanceData"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;

    .line 242
    .local v0, "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->resetLocalInfo()V

    .line 244
    .end local v0    # "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V

    .line 245
    return-void
.end method

.method public startLocalStatisticsOneTrackUpload()V
    .locals 10

    .line 318
    const-string/jumbo v0, "startLocalStatisticsOneTrackUpload"

    const-string v1, "FingerprintLocalStatisticsData"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsInit:Z

    if-nez v0, :cond_0

    .line 320
    const-string v0, "mLocalStatisticsInit failed, skip OneTrackUpload"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    return-void

    .line 323
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 325
    .local v2, "nowTime":J
    iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 326
    iput-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J

    .line 329
    :cond_1
    iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mLocalStatisticsUploadTime:J

    sub-long v4, v2, v4

    sget-wide v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->FP_LOCAL_STATISTICS_UPLOAD_PERIOD:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_2

    .line 334
    return-void

    .line 336
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v4, "onetrack.action.TRACK_EVENT"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 337
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "com.miui.analytics"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 338
    const-string v4, "APP_ID"

    const-string v5, "31000000080"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 339
    const-string v4, "EVENT_NAME"

    const-string v5, "FingerprintUnlockInfo"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 340
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PACKAGE"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    sget-boolean v4, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->IS_POWERFP:Z

    if-eqz v4, :cond_3

    const-string v4, "powerFP"

    goto :goto_0

    :cond_3
    const-string v4, "backFP"

    .line 343
    .local v4, "fp_type":Ljava/lang/String;
    :goto_0
    sget-boolean v5, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->IS_FOD:Z

    if-eqz v5, :cond_4

    const-string v5, "fod"

    goto :goto_1

    :cond_4
    move-object v5, v4

    :goto_1
    const-string v6, "key_fp_type"

    invoke-virtual {v0, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    sget-object v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->FP_VENDOR:Ljava/lang/String;

    .line 344
    const-string v7, "key_fp_vendor"

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 345
    sget-boolean v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->IS_POWERFP:Z

    if-eqz v6, :cond_5

    iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mUnlockOption:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_5
    const-string v6, "null"

    :goto_2
    const-string v7, "key_fp_option"

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mEnrolledCount:I

    .line 346
    const-string v7, "key_fp_enrolled_count"

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 348
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getModeCurValue()Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_6

    .line 349
    const-string v5, "key_fp_touch_film_mode"

    iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->filmMode:I

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 353
    :cond_6
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpLocalSharedPreferences:Landroid/content/SharedPreferences;

    if-eqz v5, :cond_a

    .line 354
    invoke-interface {v5}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v5

    .line 362
    .local v5, "fpLocalSaveKeyValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    :try_start_0
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 363
    .local v7, "key":Ljava/lang/String;
    const-string v8, "reporttimestamp"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 364
    goto :goto_3

    .line 366
    :cond_7
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 368
    nop

    .end local v7    # "key":Ljava/lang/String;
    goto :goto_3

    .line 369
    :cond_8
    sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v6, :cond_9

    .line 370
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 372
    :cond_9
    iget-object v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mContext:Landroid/content/Context;

    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v6, v0, v7}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 377
    :catch_0
    move-exception v6

    .line 378
    .local v6, "e":Ljava/lang/Exception;
    const-string v7, "initLocalStatistics unlockRateInfo exception"

    invoke-static {v1, v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 379
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetLocalStatisticsData()V

    .line 380
    return-void

    .line 375
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 376
    .local v6, "e":Ljava/lang/SecurityException;
    const-string v7, "Unable to start service."

    invoke-static {v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v6    # "e":Ljava/lang/SecurityException;
    goto :goto_4

    .line 373
    :catch_2
    move-exception v6

    .line 374
    .local v6, "e":Ljava/lang/IllegalStateException;
    const-string v7, "Failed to upload FingerprintService event."

    invoke-static {v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    .end local v6    # "e":Ljava/lang/IllegalStateException;
    :goto_4
    nop

    .line 382
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->resetLocalStatisticsData()V

    .line 384
    .end local v5    # "fpLocalSaveKeyValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    :cond_a
    return-void
.end method

.method public updataLocalStatistics(Ljava/lang/String;)V
    .locals 6
    .param p1, "targetStr"    # Ljava/lang/String;

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updataLocalStatistics, target:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintLocalStatisticsData"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    const/4 v0, 0x0

    .line 200
    .local v0, "targetJson":Lorg/json/JSONObject;
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    if-eqz v2, :cond_3

    .line 202
    :try_start_0
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 203
    const-string/jumbo v2, "updataLocalStatistics, target instance is null !"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    return-void

    .line 206
    :cond_0
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->JsonMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 207
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->JsonMap:Ljava/util/Map;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    :cond_1
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->InstanceMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;

    .line 211
    .local v2, "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->JsonMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    move-object v0, v3

    .line 212
    invoke-virtual {v2, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->updateDataToJson(Lorg/json/JSONObject;)Z

    move-result v3

    .line 213
    .local v3, "result":Z
    if-nez v3, :cond_2

    .line 214
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updataLocalStatistics, update: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " fail!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-virtual {v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->resetLocalInfo()V

    .line 216
    invoke-virtual {v2, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;->updateDataToJson(Lorg/json/JSONObject;)Z

    .line 219
    :cond_2
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 220
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->mFpSharedPreferencesEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    .end local v2    # "bigdata":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
    .end local v3    # "result":Z
    goto :goto_0

    .line 221
    :catch_0
    move-exception v2

    .line 222
    .local v2, "e":Ljava/lang/IllegalStateException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updataLocalStatistics, updata "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " IllegalStateException!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 223
    return-void

    .line 229
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :cond_3
    :goto_0
    return-void
.end method
