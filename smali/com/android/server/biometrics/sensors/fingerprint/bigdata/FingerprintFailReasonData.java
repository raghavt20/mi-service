public class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData extends com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData {
	 /* .source "FingerprintFailReasonData.java" */
	 /* # static fields */
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_ANTI_SPOOF;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_DRY_FINGER;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_DUPLICATE_AREA;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_DUPLICATE_FINGER;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_ENROLL_ERROR;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_FIXED_PATTERN;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_HBM_TOO_SLOW;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_INPUT_TOO_LONG;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_INVALID_DATA;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_LATENT_CLASSIFIER;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_LOGO;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_BRIGHTNESS;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_CONTRAST;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_MOBILITY;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_OVERLAP;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_SIMILARITY;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_NOT_MATCH;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_OVER_EXPOSURE;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_QUICK_CLICK;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_SHOW_CIRCLE;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_SIMULATED_FINGER;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_SPI_COMMUNICATION;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_SUSPICIOUS_BAD_QUALITY;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_SUSPICIOUS_LOW_QUALITY;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_TOO_DIM;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_TOUCH_BY_MISTAKE;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_UI_DISAPPEAR;
	 private static final Integer CUSTOMIZED_FINGERPRINT_ACQUIRED_WET_FINGER;
	 private static final java.lang.String TAG;
	 private static volatile com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData sInstance;
	 /* # instance fields */
	 private org.json.JSONArray failReasonArray;
	 private org.json.JSONObject failReasonJson;
	 private java.util.List failReasonJsonArrayList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lorg/json/JSONArray;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.List failReasonJsonKeyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List failReasonRecordList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.util.List failReasonStatisticList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private org.json.JSONArray hbmFailReasonArray;
private java.util.List hbmFailReasonStatisticList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private org.json.JSONArray lowlightFailReasonArray;
private java.util.List lowlightFailReasonStatisticList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mFailReasonCodeList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mFailReasonInfoList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private org.json.JSONArray screenOffFailReasonArray;
private java.util.List screenOffFailReasonStatisticList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private org.json.JSONArray screenOnFailReasonArray;
private java.util.List screenOnFailReasonStatisticList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData ( ) {
/* .locals 35 */
/* .line 15 */
/* move-object/from16 v0, p0 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V */
/* .line 69 */
final String v1 = "1-0"; // const-string v1, "1-0"
final String v2 = "2-0"; // const-string v2, "2-0"
final String v3 = "3-0"; // const-string v3, "3-0"
final String v4 = "4-0"; // const-string v4, "4-0"
final String v5 = "5-0"; // const-string v5, "5-0"
final String v6 = "6-24"; // const-string v6, "6-24"
final String v7 = "6-25"; // const-string v7, "6-25"
final String v8 = "6-26"; // const-string v8, "6-26"
final String v9 = "6-27"; // const-string v9, "6-27"
final String v10 = "6-28"; // const-string v10, "6-28"
final String v11 = "6-29"; // const-string v11, "6-29"
final String v12 = "6-30"; // const-string v12, "6-30"
final String v13 = "6-31"; // const-string v13, "6-31"
final String v14 = "6-32"; // const-string v14, "6-32"
final String v15 = "6-33"; // const-string v15, "6-33"
final String v16 = "6-34"; // const-string v16, "6-34"
final String v17 = "6-40"; // const-string v17, "6-40"
final String v18 = "6-41"; // const-string v18, "6-41"
final String v19 = "6-42"; // const-string v19, "6-42"
final String v20 = "6-43"; // const-string v20, "6-43"
final String v21 = "6-44"; // const-string v21, "6-44"
final String v22 = "6-45"; // const-string v22, "6-45"
final String v23 = "6-46"; // const-string v23, "6-46"
final String v24 = "6-47"; // const-string v24, "6-47"
final String v25 = "6-48"; // const-string v25, "6-48"
final String v26 = "6-49"; // const-string v26, "6-49"
final String v27 = "6-51"; // const-string v27, "6-51"
final String v28 = "6-52"; // const-string v28, "6-52"
final String v29 = "6-53"; // const-string v29, "6-53"
final String v30 = "6-54"; // const-string v30, "6-54"
final String v31 = "6-55"; // const-string v31, "6-55"
final String v32 = "6-56"; // const-string v32, "6-56"
final String v33 = "6-57"; // const-string v33, "6-57"
/* filled-new-array/range {v1 ..v33}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
this.mFailReasonCodeList = v1;
/* .line 105 */
final String v2 = "fail_reason_partial_cnt"; // const-string v2, "fail_reason_partial_cnt"
final String v3 = "fail_reason_insufficient_cnt"; // const-string v3, "fail_reason_insufficient_cnt"
final String v4 = "fail_reason_img_dirty_cnt"; // const-string v4, "fail_reason_img_dirty_cnt"
final String v5 = "fail_reason_too_slow_cnt"; // const-string v5, "fail_reason_too_slow_cnt"
final String v6 = "fail_reason_finger_leave_too_fast_cnt"; // const-string v6, "fail_reason_finger_leave_too_fast_cnt"
final String v7 = "fail_reason_input_too_long_cnt"; // const-string v7, "fail_reason_input_too_long_cnt"
final String v8 = "fail_reason_duplicate_area_cnt"; // const-string v8, "fail_reason_duplicate_area_cnt"
final String v9 = "fail_reason_duplicate_finger_cnt"; // const-string v9, "fail_reason_duplicate_finger_cnt"
final String v10 = "fail_reason_simulated_finger_cnt"; // const-string v10, "fail_reason_simulated_finger_cnt"
final String v11 = "fail_reason_touch_by_mistake_cnt"; // const-string v11, "fail_reason_touch_by_mistake_cnt"
final String v12 = "fail_reason_suspicious_cmn_cnt"; // const-string v12, "fail_reason_suspicious_cmn_cnt"
final String v13 = "fail_reason_latent_classifier_cnt"; // const-string v13, "fail_reason_latent_classifier_cnt"
final String v14 = "fail_reason_anti_spoof_cnt"; // const-string v14, "fail_reason_anti_spoof_cnt"
final String v15 = "fail_reason_dry_finger_cnt"; // const-string v15, "fail_reason_dry_finger_cnt"
final String v16 = "fail_reason_low_brightness_cnt"; // const-string v16, "fail_reason_low_brightness_cnt"
final String v17 = "fail_reason_logo_cnt"; // const-string v17, "fail_reason_logo_cnt"
final String v18 = "fail_reason_HBM_too_slow_cnt"; // const-string v18, "fail_reason_HBM_too_slow_cnt"
final String v19 = "fail_reason_SPI_communication_cnt"; // const-string v19, "fail_reason_SPI_communication_cnt"
final String v20 = "fail_reason_wet_finger_cnt"; // const-string v20, "fail_reason_wet_finger_cnt"
final String v21 = "fail_reason_too_dim_cnt"; // const-string v21, "fail_reason_too_dim_cnt"
final String v22 = "fail_reason_not_match_cnt"; // const-string v22, "fail_reason_not_match_cnt"
final String v23 = "fail_reason_UI_disappear_cnt"; // const-string v23, "fail_reason_UI_disappear_cnt"
final String v24 = "fail_reason_show_circle_cnt"; // const-string v24, "fail_reason_show_circle_cnt"
final String v25 = "fail_reason_enroll_error_cnt"; // const-string v25, "fail_reason_enroll_error_cnt"
final String v26 = "fail_reason_invalid_data_cnt"; // const-string v26, "fail_reason_invalid_data_cnt"
final String v27 = "fail_reason_low_mobility_cnt"; // const-string v27, "fail_reason_low_mobility_cnt"
final String v28 = "fail_reason_suspicious_low_quality_cnt"; // const-string v28, "fail_reason_suspicious_low_quality_cnt"
final String v29 = "fail_reason_suspicious_bad_quality_cnt"; // const-string v29, "fail_reason_suspicious_bad_quality_cnt"
final String v30 = "fail_reason_over_exposure_cnt"; // const-string v30, "fail_reason_over_exposure_cnt"
final String v31 = "fail_reason_low_overlap_cnt"; // const-string v31, "fail_reason_low_overlap_cnt"
final String v32 = "fail_reason_low_similarity_cnt"; // const-string v32, "fail_reason_low_similarity_cnt"
final String v33 = "fail_reason_low_contrast_cnt"; // const-string v33, "fail_reason_low_contrast_cnt"
final String v34 = "fail_reason_fixed_pattern_cnt"; // const-string v34, "fail_reason_fixed_pattern_cnt"
/* filled-new-array/range {v2 ..v34}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
this.mFailReasonInfoList = v1;
/* .line 142 */
/* new-instance v1, Ljava/util/ArrayList; */
v2 = v2 = this.mFailReasonInfoList;
int v3 = 0; // const/4 v3, 0x0
java.lang.Integer .valueOf ( v3 );
java.util.Collections .nCopies ( v2,v3 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.failReasonStatisticList = v1;
/* .line 143 */
/* new-instance v1, Ljava/util/ArrayList; */
v2 = v2 = this.mFailReasonInfoList;
java.util.Collections .nCopies ( v2,v3 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.screenOnFailReasonStatisticList = v1;
/* .line 144 */
/* new-instance v1, Ljava/util/ArrayList; */
v2 = v2 = this.mFailReasonInfoList;
java.util.Collections .nCopies ( v2,v3 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.screenOffFailReasonStatisticList = v1;
/* .line 145 */
/* new-instance v1, Ljava/util/ArrayList; */
v2 = v2 = this.mFailReasonInfoList;
java.util.Collections .nCopies ( v2,v3 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.hbmFailReasonStatisticList = v1;
/* .line 146 */
/* new-instance v1, Ljava/util/ArrayList; */
v2 = v2 = this.mFailReasonInfoList;
java.util.Collections .nCopies ( v2,v3 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.lowlightFailReasonStatisticList = v1;
/* .line 149 */
/* new-instance v1, Lorg/json/JSONObject; */
/* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
this.failReasonJson = v1;
/* .line 150 */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V */
this.failReasonArray = v1;
/* .line 151 */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V */
this.screenOnFailReasonArray = v1;
/* .line 152 */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V */
this.screenOffFailReasonArray = v1;
/* .line 153 */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V */
this.hbmFailReasonArray = v1;
/* .line 154 */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V */
this.lowlightFailReasonArray = v1;
/* .line 156 */
/* new-instance v1, Ljava/util/ArrayList; */
v2 = this.failReasonStatisticList;
v3 = this.screenOnFailReasonStatisticList;
v4 = this.screenOffFailReasonStatisticList;
v5 = this.hbmFailReasonStatisticList;
v6 = this.lowlightFailReasonStatisticList;
/* filled-new-array {v2, v3, v4, v5, v6}, [Ljava/util/List; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.failReasonRecordList = v1;
/* .line 164 */
/* new-instance v1, Ljava/util/ArrayList; */
v2 = this.failReasonArray;
v3 = this.screenOnFailReasonArray;
v4 = this.screenOffFailReasonArray;
v5 = this.hbmFailReasonArray;
v6 = this.lowlightFailReasonArray;
/* filled-new-array {v2, v3, v4, v5, v6}, [Lorg/json/JSONArray; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.failReasonJsonArrayList = v1;
/* .line 172 */
final String v1 = "hbm_fail_reason"; // const-string v1, "hbm_fail_reason"
final String v2 = "lowlight_fail_reason"; // const-string v2, "lowlight_fail_reason"
final String v3 = "fail_reason"; // const-string v3, "fail_reason"
final String v4 = "screen_on_fail_reason"; // const-string v4, "screen_on_fail_reason"
final String v5 = "screen_off_fail_reason"; // const-string v5, "screen_off_fail_reason"
/* filled-new-array {v3, v4, v5, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
this.failReasonJsonKeyList = v1;
return;
} // .end method
public static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData getInstance ( ) {
/* .locals 3 */
/* .line 21 */
final String v0 = "FingerprintFailReasonData"; // const-string v0, "FingerprintFailReasonData"
final String v1 = "getInstance"; // const-string v1, "getInstance"
android.util.Slog .d ( v0,v1 );
/* .line 22 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData.sInstance;
/* if-nez v0, :cond_1 */
/* .line 23 */
/* const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData; */
/* monitor-enter v0 */
/* .line 24 */
try { // :try_start_0
final String v1 = "FingerprintFailReasonData"; // const-string v1, "FingerprintFailReasonData"
final String v2 = "getInstance class"; // const-string v2, "getInstance class"
android.util.Slog .d ( v1,v2 );
/* .line 25 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData.sInstance;
/* if-nez v1, :cond_0 */
/* .line 26 */
final String v1 = "FingerprintFailReasonData"; // const-string v1, "FingerprintFailReasonData"
final String v2 = "getInstance new"; // const-string v2, "getInstance new"
android.util.Slog .d ( v1,v2 );
/* .line 27 */
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData; */
/* invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;-><init>()V */
/* .line 29 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 31 */
} // :cond_1
} // :goto_0
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData.sInstance;
} // .end method
static java.lang.Integer lambda$resetLocalInfo$0 ( java.lang.Integer p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "i" # Ljava/lang/Integer; */
/* .line 284 */
int v0 = 0; // const/4 v0, 0x0
java.lang.Integer .valueOf ( v0 );
} // .end method
/* # virtual methods */
public Boolean calculateFailReasonCnt ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .param p3, "state" # I */
/* .line 182 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "calculateFailReasonCnt, acquiredInfo: "; // const-string v1, "calculateFailReasonCnt, acquiredInfo: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " ,vendorCode : "; // const-string v1, " ,vendorCode : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",screenStatus: "; // const-string v1, ",screenStatus: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintFailReasonData"; // const-string v1, "FingerprintFailReasonData"
android.util.Slog .w ( v1,v0 );
/* .line 183 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.String .valueOf ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v2, 0x2d */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 184 */
/* .local v0, "failReasonCode":Ljava/lang/String; */
v2 = v2 = this.mFailReasonCodeList;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 185 */
/* const-string/jumbo v2, "start calculateFailReasonCnt" */
android.util.Slog .w ( v1,v2 );
/* .line 186 */
v1 = this.failReasonStatisticList;
v2 = v2 = this.mFailReasonCodeList;
v3 = this.failReasonStatisticList;
v4 = this.mFailReasonCodeList;
v4 = /* .line 187 */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
int v4 = 1; // const/4 v4, 0x1
/* add-int/2addr v3, v4 */
java.lang.Integer .valueOf ( v3 );
/* .line 186 */
/* .line 188 */
/* iput p3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mScreenStatus:I */
/* .line 189 */
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mScreenStatus:I */
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_0 */
/* .line 190 */
v1 = this.screenOnFailReasonStatisticList;
v2 = v2 = this.mFailReasonCodeList;
v3 = this.screenOnFailReasonStatisticList;
v5 = this.mFailReasonCodeList;
v5 = /* .line 191 */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* add-int/2addr v3, v4 */
java.lang.Integer .valueOf ( v3 );
/* .line 190 */
/* .line 193 */
} // :cond_0
v1 = this.screenOffFailReasonStatisticList;
v2 = v2 = this.mFailReasonCodeList;
v3 = this.screenOffFailReasonStatisticList;
v5 = this.mFailReasonCodeList;
v5 = /* .line 194 */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* add-int/2addr v3, v4 */
java.lang.Integer .valueOf ( v3 );
/* .line 193 */
/* .line 197 */
} // :goto_0
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFingerUnlockBright:I */
/* if-ne v1, v4, :cond_1 */
/* .line 198 */
v1 = this.lowlightFailReasonStatisticList;
v2 = v2 = this.mFailReasonCodeList;
v3 = this.lowlightFailReasonStatisticList;
v5 = this.mFailReasonCodeList;
v5 = /* .line 199 */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* add-int/2addr v3, v4 */
java.lang.Integer .valueOf ( v3 );
/* .line 198 */
/* .line 201 */
} // :cond_1
v1 = this.hbmFailReasonStatisticList;
v2 = v2 = this.mFailReasonCodeList;
v3 = this.hbmFailReasonStatisticList;
v5 = this.mFailReasonCodeList;
v5 = /* .line 202 */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* add-int/2addr v3, v4 */
java.lang.Integer .valueOf ( v3 );
/* .line 201 */
/* .line 209 */
} // :goto_1
/* .line 211 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void resetLocalInfo ( ) {
/* .locals 3 */
/* .line 282 */
final String v0 = "FingerprintFailReasonData"; // const-string v0, "FingerprintFailReasonData"
final String v1 = "resetLocalInfo"; // const-string v1, "resetLocalInfo"
android.util.Slog .i ( v0,v1 );
/* .line 283 */
v0 = this.failReasonRecordList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/List; */
/* .line 284 */
/* .local v1, "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData$$ExternalSyntheticLambda1; */
/* invoke-direct {v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData$$ExternalSyntheticLambda1;-><init>()V */
/* .line 285 */
} // .end local v1 # "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .line 286 */
} // :cond_0
return;
} // .end method
public Boolean updateDataToJson ( org.json.JSONObject p0 ) {
/* .locals 7 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 248 */
/* const-string/jumbo v0, "updateDataToJson" */
final String v1 = "FingerprintFailReasonData"; // const-string v1, "FingerprintFailReasonData"
android.util.Slog .i ( v1,v0 );
/* .line 250 */
/* if-nez p1, :cond_0 */
/* .line 251 */
try { // :try_start_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* move-object p1, v0 */
/* .line 254 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 255 */
/* .local v0, "failReasonStringList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v2 = this.failReasonRecordList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/util/List; */
/* .line 256 */
/* .local v3, "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "["; // const-string v5, "["
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData$$ExternalSyntheticLambda0; */
/* invoke-direct {v6}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData$$ExternalSyntheticLambda0;-><init>()V */
final String v6 = ","; // const-string v6, ","
java.util.stream.Collectors .joining ( v6 );
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "]"; // const-string v5, "]"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 257 */
/* nop */
} // .end local v3 # "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .line 259 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .line 260 */
/* .local v2, "tempIndex":I */
/* new-instance v3, Lorg/json/JSONArray; */
/* add-int/lit8 v4, v2, 0x1 */
} // .end local v2 # "tempIndex":I
/* .local v4, "tempIndex":I */
/* check-cast v2, Ljava/lang/String; */
/* invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
this.failReasonArray = v3;
/* .line 261 */
final String v2 = "fail_reason"; // const-string v2, "fail_reason"
(( org.json.JSONObject ) p1 ).put ( v2, v3 ); // invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 263 */
/* new-instance v2, Lorg/json/JSONArray; */
/* add-int/lit8 v3, v4, 0x1 */
} // .end local v4 # "tempIndex":I
/* .local v3, "tempIndex":I */
/* check-cast v4, Ljava/lang/String; */
/* invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
this.screenOnFailReasonArray = v2;
/* .line 264 */
final String v4 = "screen_on_fail_reason"; // const-string v4, "screen_on_fail_reason"
(( org.json.JSONObject ) p1 ).put ( v4, v2 ); // invoke-virtual {p1, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 266 */
/* new-instance v2, Lorg/json/JSONArray; */
/* add-int/lit8 v4, v3, 0x1 */
} // .end local v3 # "tempIndex":I
/* .restart local v4 # "tempIndex":I */
/* check-cast v3, Ljava/lang/String; */
/* invoke-direct {v2, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
this.screenOffFailReasonArray = v2;
/* .line 267 */
final String v3 = "screen_off_fail_reason"; // const-string v3, "screen_off_fail_reason"
(( org.json.JSONObject ) p1 ).put ( v3, v2 ); // invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 269 */
/* new-instance v2, Lorg/json/JSONArray; */
/* add-int/lit8 v3, v4, 0x1 */
} // .end local v4 # "tempIndex":I
/* .restart local v3 # "tempIndex":I */
/* check-cast v4, Ljava/lang/String; */
/* invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
this.hbmFailReasonArray = v2;
/* .line 270 */
final String v4 = "hbm_fail_reason"; // const-string v4, "hbm_fail_reason"
(( org.json.JSONObject ) p1 ).put ( v4, v2 ); // invoke-virtual {p1, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 272 */
/* new-instance v2, Lorg/json/JSONArray; */
/* add-int/lit8 v4, v3, 0x1 */
} // .end local v3 # "tempIndex":I
/* .restart local v4 # "tempIndex":I */
/* check-cast v3, Ljava/lang/String; */
/* invoke-direct {v2, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V */
this.lowlightFailReasonArray = v2;
/* .line 273 */
final String v3 = "lowlight_fail_reason"; // const-string v3, "lowlight_fail_reason"
(( org.json.JSONObject ) p1 ).put ( v3, v2 ); // invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 277 */
/* nop */
/* .line 278 */
} // .end local v0 # "failReasonStringList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v4 # "tempIndex":I
int v0 = 1; // const/4 v0, 0x1
/* .line 274 */
/* :catch_0 */
/* move-exception v0 */
/* .line 275 */
/* .local v0, "e":Lorg/json/JSONException; */
/* const-string/jumbo v2, "updateDataToJson JSONException" */
android.util.Slog .e ( v1,v2,v0 );
/* .line 276 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean updateJsonToData ( org.json.JSONObject p0 ) {
/* .locals 14 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 217 */
final String v0 = ""; // const-string v0, ""
/* const-string/jumbo v1, "updateJsonToData" */
final String v2 = "FingerprintFailReasonData"; // const-string v2, "FingerprintFailReasonData"
android.util.Slog .i ( v2,v1 );
/* .line 220 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "tempIndex":I */
} // :goto_0
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
v4 = v4 = this.failReasonJsonArrayList;
/* if-ge v1, v4, :cond_2 */
/* .line 221 */
v4 = this.failReasonJsonArrayList;
/* check-cast v4, Lorg/json/JSONArray; */
/* .line 222 */
/* .local v4, "tempJSONArray":Lorg/json/JSONArray; */
v5 = this.failReasonJsonKeyList;
/* check-cast v5, Ljava/lang/String; */
(( org.json.JSONObject ) p1 ).getJSONArray ( v5 ); // invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* move-object v4, v5 */
/* .line 223 */
(( org.json.JSONArray ) v4 ).toString ( ); // invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
/* .line 224 */
/* .local v5, "tempString":Ljava/lang/String; */
v6 = (( java.lang.String ) v5 ).isEmpty ( ); // invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z
/* if-nez v6, :cond_1 */
/* .line 225 */
final String v6 = "["; // const-string v6, "["
(( java.lang.String ) v5 ).replace ( v6, v0 ); // invoke-virtual {v5, v6, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v5, v6 */
/* .line 226 */
final String v6 = "]"; // const-string v6, "]"
(( java.lang.String ) v5 ).replace ( v6, v0 ); // invoke-virtual {v5, v6, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v5, v6 */
/* .line 227 */
int v6 = 0; // const/4 v6, 0x0
/* .line 228 */
/* .local v6, "secondTempIndex":I */
final String v7 = ","; // const-string v7, ","
(( java.lang.String ) v5 ).split ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* array-length v8, v7 */
/* move v9, v3 */
} // :goto_1
/* if-ge v9, v8, :cond_1 */
/* aget-object v10, v7, v9 */
/* .line 229 */
/* .local v10, "splitstring":Ljava/lang/String; */
v11 = (( java.lang.String ) v10 ).isEmpty ( ); // invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z
/* if-nez v11, :cond_0 */
v11 = v11 = this.failReasonRecordList;
/* if-ge v6, v11, :cond_0 */
/* .line 230 */
v11 = this.failReasonRecordList;
/* check-cast v11, Ljava/util/List; */
/* add-int/lit8 v12, v6, 0x1 */
} // .end local v6 # "secondTempIndex":I
/* .local v12, "secondTempIndex":I */
v13 = java.lang.Integer .parseInt ( v10 );
java.lang.Integer .valueOf ( v13 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v6, v12 */
/* .line 228 */
} // .end local v10 # "splitstring":Ljava/lang/String;
} // .end local v12 # "secondTempIndex":I
/* .restart local v6 # "secondTempIndex":I */
} // :cond_0
/* add-int/lit8 v9, v9, 0x1 */
/* .line 220 */
} // .end local v4 # "tempJSONArray":Lorg/json/JSONArray;
} // .end local v5 # "tempString":Ljava/lang/String;
} // .end local v6 # "secondTempIndex":I
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 243 */
} // :cond_2
/* nop */
/* .line 244 */
int v0 = 1; // const/4 v0, 0x1
/* .line 239 */
} // .end local v1 # "tempIndex":I
/* :catch_0 */
/* move-exception v0 */
/* .line 240 */
/* .local v0, "e":Ljava/lang/IndexOutOfBoundsException; */
/* const-string/jumbo v1, "updateJsonToData IndexOutOfBoundsException" */
android.util.Slog .e ( v2,v1,v0 );
/* .line 241 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData ) p0 ).resetLocalInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->resetLocalInfo()V
/* .line 242 */
/* .line 235 */
} // .end local v0 # "e":Ljava/lang/IndexOutOfBoundsException;
/* :catch_1 */
/* move-exception v0 */
/* .line 236 */
/* .local v0, "e":Lorg/json/JSONException; */
/* const-string/jumbo v1, "updateJsonToData JSONException" */
android.util.Slog .e ( v2,v1,v0 );
/* .line 237 */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData ) p0 ).resetLocalInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->resetLocalInfo()V
/* .line 238 */
} // .end method
