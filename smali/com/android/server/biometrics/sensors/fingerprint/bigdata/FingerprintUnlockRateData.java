public class com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData extends com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintBigData {
	 /* .source "FingerprintUnlockRateData.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
	 /* } */
} // .end annotation
/* # static fields */
protected static final Integer HBM;
private static final java.lang.String SYSTEMUI_NAME;
private static final java.lang.String TAG;
private static volatile com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData sInstance;
/* # instance fields */
private final Integer LUX_LEVEL_UNDEFINED;
private final Integer LUX_LIGHT_LEVEL0;
private final Integer LUX_LIGHT_LEVEL1;
private final Integer LUX_LIGHT_LEVEL2;
private final Integer LUX_LIGHT_LEVEL3;
private final Integer LUX_LIGHT_LEVEL4;
private final Integer LUX_LIGHT_LEVEL5;
private final Integer LUX_LIGHT_LEVEL6;
private final Integer LUX_LIGHT_LEVEL7;
private final Integer LUX_LIGHT_LEVEL8;
private java.util.Map appAuthMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private org.json.JSONArray appUnlockRateArray;
org.json.JSONArray luxLightOtherAuthSuccCountArray;
org.json.JSONArray luxLightOtherSyAuthCountArray;
org.json.JSONArray luxLightSystemuiAuthSuccCountArray;
org.json.JSONArray luxLightSystemuiSyAuthCountArray;
private com.android.server.biometrics.log.ALSProbe mALSProbe;
private Integer mAmbientLux;
private android.content.Context mContext;
private Integer mFingerUnlockBright;
private Integer mProbeIsEnable;
private Integer mScreenStatus;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics otherAppLuxUnlockRate;
private java.util.List sAppStatisticList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List sPackageList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.hardware.SensorManager sensorManager;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics systemuiLuxUnlockRate;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AppUnlockRateStatistic systemuiUnlockRate;
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit totalAuth;
/* # direct methods */
private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData ( ) {
/* .locals 6 */
/* .line 120 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V */
/* .line 26 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mScreenStatus:I */
/* .line 27 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I */
/* .line 29 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LEVEL_UNDEFINED:I */
/* .line 30 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL0:I */
/* .line 31 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL1:I */
/* .line 32 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL2:I */
/* .line 33 */
int v0 = 3; // const/4 v0, 0x3
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL3:I */
/* .line 34 */
int v0 = 4; // const/4 v0, 0x4
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL4:I */
/* .line 35 */
int v0 = 5; // const/4 v0, 0x5
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL5:I */
/* .line 36 */
int v0 = 6; // const/4 v0, 0x6
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL6:I */
/* .line 37 */
int v0 = 7; // const/4 v0, 0x7
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL7:I */
/* .line 38 */
/* const/16 v0, 0x8 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->LUX_LIGHT_LEVEL8:I */
/* .line 41 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I */
/* .line 217 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics; */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;-><init>(Ljava/lang/String;)V */
this.systemuiLuxUnlockRate = v0;
/* .line 218 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics; */
final String v2 = "other App"; // const-string v2, "other App"
/* invoke-direct {v0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;-><init>(Ljava/lang/String;)V */
this.otherAppLuxUnlockRate = v0;
/* .line 250 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit-IA;)V */
this.totalAuth = v0;
/* .line 256 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
final String v2 = "com.tencent.mm"; // const-string v2, "com.tencent.mm"
final String v3 = "com.eg.android.AlipayGphone"; // const-string v3, "com.eg.android.AlipayGphone"
/* filled-new-array {v1, v0, v2, v3}, [Ljava/lang/String; */
java.util.Arrays .asList ( v4 );
this.sPackageList = v4;
/* .line 263 */
/* new-instance v4, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics; */
/* invoke-direct {v4, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;-><init>(Ljava/lang/String;)V */
this.systemuiUnlockRate = v4;
/* .line 264 */
/* new-instance v1, Ljava/util/ArrayList; */
v4 = this.systemuiUnlockRate;
/* new-instance v5, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
/* invoke-direct {v5, v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>(Ljava/lang/String;)V */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
/* invoke-direct {v0, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>(Ljava/lang/String;)V */
/* new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
/* invoke-direct {v2, v3}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;-><init>(Ljava/lang/String;)V */
/* filled-new-array {v4, v5, v0, v2}, [Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
java.util.Arrays .asList ( v0 );
/* invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.sAppStatisticList = v1;
/* .line 270 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.appAuthMap = v0;
/* .line 274 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.appUnlockRateArray = v0;
/* .line 366 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.luxLightSystemuiSyAuthCountArray = v0;
/* .line 367 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.luxLightSystemuiAuthSuccCountArray = v0;
/* .line 368 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.luxLightOtherSyAuthCountArray = v0;
/* .line 369 */
/* new-instance v0, Lorg/json/JSONArray; */
/* invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V */
this.luxLightOtherAuthSuccCountArray = v0;
/* .line 122 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "tempIndex":I */
} // :goto_0
v1 = v1 = this.sPackageList;
/* if-ge v0, v1, :cond_0 */
/* .line 123 */
v1 = this.appAuthMap;
v2 = this.sPackageList;
/* check-cast v2, Ljava/lang/String; */
v3 = this.sAppStatisticList;
/* check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
/* .line 122 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 126 */
} // .end local v0 # "tempIndex":I
} // :cond_0
return;
} // .end method
private void ambientLuxUnlockRate ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packName" # Ljava/lang/String; */
/* .param p2, "authen" # I */
/* .line 279 */
v0 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->getAmbientLuxLevel()I */
/* .line 280 */
/* .local v0, "luxLevel":I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 281 */
return;
/* .line 283 */
} // :cond_0
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v2 = "FingerprintUnlockRateData"; // const-string v2, "FingerprintUnlockRateData"
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 284 */
final String v1 = "ambientLuxUnlockRate: SystemUi "; // const-string v1, "ambientLuxUnlockRate: SystemUi "
android.util.Slog .d ( v2,v1 );
/* .line 285 */
v1 = this.systemuiLuxUnlockRate;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics ) v1 ).addLuxAuthCount ( v0, p2 ); // invoke-virtual {v1, v0, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->addLuxAuthCount(II)V
/* .line 287 */
} // :cond_1
final String v1 = "ambientLuxUnlockRate: OtherApp "; // const-string v1, "ambientLuxUnlockRate: OtherApp "
android.util.Slog .d ( v2,v1 );
/* .line 288 */
v1 = this.otherAppLuxUnlockRate;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics ) v1 ).addLuxAuthCount ( v0, p2 ); // invoke-virtual {v1, v0, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->addLuxAuthCount(II)V
/* .line 290 */
} // :goto_0
return;
} // .end method
private Integer getAmbientLuxLevel ( ) {
/* .locals 3 */
/* .line 83 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mAmbientLux:I */
/* const/16 v1, 0x1f4 */
/* if-ltz v0, :cond_0 */
/* if-ge v0, v1, :cond_0 */
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* .line 85 */
} // :cond_0
/* const/16 v2, 0x3e8 */
/* if-lt v0, v1, :cond_1 */
/* if-ge v0, v2, :cond_1 */
/* .line 86 */
int v0 = 1; // const/4 v0, 0x1
/* .line 87 */
} // :cond_1
/* const/16 v1, 0x7d0 */
/* if-lt v0, v2, :cond_2 */
/* if-ge v0, v1, :cond_2 */
/* .line 88 */
int v0 = 2; // const/4 v0, 0x2
/* .line 89 */
} // :cond_2
/* const/16 v2, 0x1388 */
/* if-lt v0, v1, :cond_3 */
/* if-ge v0, v2, :cond_3 */
/* .line 90 */
int v0 = 3; // const/4 v0, 0x3
/* .line 91 */
} // :cond_3
/* const/16 v1, 0x1b58 */
/* if-lt v0, v2, :cond_4 */
/* if-ge v0, v1, :cond_4 */
/* .line 92 */
int v0 = 4; // const/4 v0, 0x4
/* .line 93 */
} // :cond_4
/* const/16 v2, 0x2710 */
/* if-lt v0, v1, :cond_5 */
/* if-ge v0, v2, :cond_5 */
/* .line 94 */
int v0 = 5; // const/4 v0, 0x5
/* .line 95 */
} // :cond_5
/* const/16 v1, 0x3a98 */
/* if-lt v0, v2, :cond_6 */
/* if-ge v0, v1, :cond_6 */
/* .line 96 */
int v0 = 6; // const/4 v0, 0x6
/* .line 97 */
} // :cond_6
/* const/16 v2, 0x4e20 */
/* if-lt v0, v1, :cond_7 */
/* if-ge v0, v2, :cond_7 */
/* .line 98 */
int v0 = 7; // const/4 v0, 0x7
/* .line 99 */
} // :cond_7
/* if-lt v0, v2, :cond_8 */
/* .line 100 */
/* const/16 v0, 0x8 */
/* .line 102 */
} // :cond_8
int v0 = -1; // const/4 v0, -0x1
} // .end method
public static com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData getInstance ( ) {
/* .locals 3 */
/* .line 107 */
final String v0 = "FingerprintUnlockRateData"; // const-string v0, "FingerprintUnlockRateData"
final String v1 = "getInstance"; // const-string v1, "getInstance"
android.util.Slog .d ( v0,v1 );
/* .line 108 */
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData.sInstance;
/* if-nez v0, :cond_1 */
/* .line 109 */
/* const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData; */
/* monitor-enter v0 */
/* .line 110 */
try { // :try_start_0
final String v1 = "FingerprintUnlockRateData"; // const-string v1, "FingerprintUnlockRateData"
final String v2 = "getInstance class"; // const-string v2, "getInstance class"
android.util.Slog .d ( v1,v2 );
/* .line 111 */
v1 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData.sInstance;
/* if-nez v1, :cond_0 */
/* .line 112 */
final String v1 = "FingerprintUnlockRateData"; // const-string v1, "FingerprintUnlockRateData"
final String v2 = "getInstance new"; // const-string v2, "getInstance new"
android.util.Slog .d ( v1,v2 );
/* .line 113 */
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData; */
/* invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;-><init>()V */
/* .line 115 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 117 */
} // :cond_1
} // :goto_0
v0 = com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData.sInstance;
} // .end method
/* # virtual methods */
public void appUnlockRate ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packName" # Ljava/lang/String; */
/* .param p2, "authen" # I */
/* .line 293 */
v0 = v0 = this.appAuthMap;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 294 */
v0 = this.appAuthMap;
/* check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
v0 = this.totalAuth;
/* iget v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v2, v1 */
/* iput v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 295 */
/* if-ne p2, v1, :cond_1 */
/* .line 296 */
v0 = this.appAuthMap;
/* check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
v0 = this.totalAuth;
/* iget v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v2, v1 */
/* iput v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 299 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "calculateUnlockCnt: appAuthMap doesn\'t contains:"; // const-string v2, "calculateUnlockCnt: appAuthMap doesn\'t contains:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "FingerprintUnlockRateData"; // const-string v2, "FingerprintUnlockRateData"
android.util.Slog .e ( v2,v0 );
/* .line 302 */
} // :cond_1
} // :goto_0
final String v0 = "com.android.systemui"; // const-string v0, "com.android.systemui"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 303 */
v0 = this.systemuiUnlockRate;
/* check-cast v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics; */
/* .line 304 */
/* .local v0, "systemuiStatistics":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics; */
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mScreenStatus:I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_4 */
/* .line 305 */
v2 = this.screenOnAuth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 306 */
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I */
/* if-ne v2, v1, :cond_2 */
/* .line 307 */
v2 = this.screenOn_lowLight_Auth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 309 */
} // :cond_2
v2 = this.screenOn_highLight_Auth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 312 */
} // :goto_1
/* if-ne p2, v1, :cond_7 */
/* .line 313 */
v2 = this.screenOnAuth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 314 */
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I */
/* if-ne v2, v1, :cond_3 */
/* .line 315 */
v2 = this.screenOn_lowLight_Auth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 317 */
} // :cond_3
v2 = this.screenOn_highLight_Auth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 321 */
} // :cond_4
v2 = this.screenOffAuth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 323 */
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I */
/* if-ne v2, v1, :cond_5 */
/* .line 324 */
v2 = this.screenOff_lowLight_Auth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 326 */
} // :cond_5
v2 = this.screenOff_highLight_Auth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 329 */
} // :goto_2
/* if-ne p2, v1, :cond_7 */
/* .line 330 */
v2 = this.screenOffAuth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 331 */
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I */
/* if-ne v2, v1, :cond_6 */
/* .line 332 */
v2 = this.screenOff_lowLight_Auth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 334 */
} // :cond_6
v2 = this.screenOff_highLight_Auth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 339 */
} // :cond_7
} // :goto_3
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mFingerUnlockBright:I */
/* if-ne v2, v1, :cond_8 */
/* .line 340 */
v2 = this.lowLightAuth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 341 */
/* if-ne p2, v1, :cond_9 */
/* .line 342 */
v2 = this.lowLightAuth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 345 */
} // :cond_8
v2 = this.highLightAuth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 347 */
/* if-ne p2, v1, :cond_9 */
/* .line 348 */
v2 = this.highLightAuth;
/* iget v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v3, v1 */
/* iput v3, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 352 */
} // .end local v0 # "systemuiStatistics":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
} // :cond_9
} // :goto_4
return;
} // .end method
public void calculateUnlockCnt ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "packName" # Ljava/lang/String; */
/* .param p2, "authen" # I */
/* .param p3, "screenState" # I */
/* .line 355 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "calculateUnlockCnt, packName: "; // const-string v1, "calculateUnlockCnt, packName: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", authen: "; // const-string v1, ", authen: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",screenState: "; // const-string v1, ",screenState: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintUnlockRateData"; // const-string v1, "FingerprintUnlockRateData"
android.util.Slog .d ( v1,v0 );
/* .line 356 */
/* iput p3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mScreenStatus:I */
/* .line 357 */
v0 = this.totalAuth;
/* iget v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
int v2 = 1; // const/4 v2, 0x1
/* add-int/2addr v1, v2 */
/* iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 358 */
/* if-ne p2, v2, :cond_0 */
/* .line 359 */
v0 = this.totalAuth;
/* iget v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* add-int/2addr v1, v2 */
/* iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 361 */
} // :cond_0
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData ) p0 ).appUnlockRate ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->appUnlockRate(Ljava/lang/String;I)V
/* .line 362 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->ambientLuxUnlockRate(Ljava/lang/String;I)V */
/* .line 363 */
return;
} // .end method
public void handleAcquiredInfo ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .line 56 */
/* sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->IS_FOD:Z */
final String v1 = "FingerprintUnlockRateData"; // const-string v1, "FingerprintUnlockRateData"
int v2 = 1; // const/4 v2, 0x1
/* if-eq v0, v2, :cond_0 */
/* .line 57 */
final String v0 = "is not fod\uff01"; // const-string v0, "is not fod\uff01"
android.util.Slog .d ( v1,v0 );
/* .line 58 */
return;
/* .line 61 */
} // :cond_0
/* const/16 v0, 0x16 */
/* if-ne p2, v0, :cond_1 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I */
/* if-nez v0, :cond_1 */
/* .line 62 */
final String v0 = "getMostRecentLux: finger down"; // const-string v0, "getMostRecentLux: finger down"
android.util.Slog .d ( v1,v0 );
/* .line 63 */
v0 = this.mALSProbe;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 64 */
(( com.android.server.biometrics.log.ALSProbe ) v0 ).enable ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/log/ALSProbe;->enable()V
/* .line 65 */
/* iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I */
/* .line 69 */
} // :cond_1
/* const/16 v0, 0x17 */
/* if-ne p2, v0, :cond_2 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I */
/* if-ne v0, v2, :cond_2 */
/* .line 70 */
final String v0 = "getMostRecentLux:finger up "; // const-string v0, "getMostRecentLux:finger up "
android.util.Slog .d ( v1,v0 );
/* .line 71 */
v0 = this.mALSProbe;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 72 */
v0 = (( com.android.server.biometrics.log.ALSProbe ) v0 ).getMostRecentLux ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/log/ALSProbe;->getMostRecentLux()F
/* .line 73 */
/* .local v0, "ambientLux":F */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getMostRecentLux: "; // const-string v3, "getMostRecentLux: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 74 */
/* float-to-int v1, v0 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mAmbientLux:I */
/* .line 75 */
v1 = this.mALSProbe;
(( com.android.server.biometrics.log.ALSProbe ) v1 ).disable ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/log/ALSProbe;->disable()V
/* .line 76 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->mProbeIsEnable:I */
/* .line 80 */
} // .end local v0 # "ambientLux":F
} // :cond_2
return;
} // .end method
public void resetLocalInfo ( ) {
/* .locals 2 */
/* .line 534 */
final String v0 = "FingerprintUnlockRateData"; // const-string v0, "FingerprintUnlockRateData"
final String v1 = "resetLocalInfo "; // const-string v1, "resetLocalInfo "
android.util.Slog .d ( v0,v1 );
/* .line 535 */
v0 = this.totalAuth;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$UnlockRateUnit ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->reset()V
/* .line 536 */
v0 = this.sAppStatisticList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
/* .line 537 */
/* .local v1, "appunlock":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AppUnlockRateStatistic ) v1 ).reset ( ); // invoke-virtual {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;->reset()V
/* .line 538 */
} // .end local v1 # "appunlock":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
/* .line 539 */
} // :cond_0
v0 = this.systemuiLuxUnlockRate;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->reset()V
/* .line 540 */
v0 = this.otherAppLuxUnlockRate;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AmbientLuxUnlockRateStatistics;->reset()V
/* .line 541 */
return;
} // .end method
public void setContext ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 47 */
final String v0 = "FingerprintUnlockRateData"; // const-string v0, "FingerprintUnlockRateData"
/* const-string/jumbo v1, "setContext" */
android.util.Slog .i ( v0,v1 );
/* .line 48 */
this.mContext = p1;
/* .line 49 */
/* const-class v0, Landroid/hardware/SensorManager; */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
this.sensorManager = v0;
/* .line 50 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 51 */
/* new-instance v0, Lcom/android/server/biometrics/log/ALSProbe; */
v1 = this.sensorManager;
/* invoke-direct {v0, v1}, Lcom/android/server/biometrics/log/ALSProbe;-><init>(Landroid/hardware/SensorManager;)V */
this.mALSProbe = v0;
/* .line 53 */
} // :cond_0
return;
} // .end method
public Boolean updateDataToJson ( org.json.JSONObject p0 ) {
/* .locals 19 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 376 */
/* move-object/from16 v1, p0 */
final String v0 = "low_light_status"; // const-string v0, "low_light_status"
final String v2 = "high_light_status"; // const-string v2, "high_light_status"
final String v3 = "auth_success_count"; // const-string v3, "auth_success_count"
final String v4 = "auth_count"; // const-string v4, "auth_count"
/* const-string/jumbo v5, "updateDataToJson" */
final String v6 = "FingerprintUnlockRateData"; // const-string v6, "FingerprintUnlockRateData"
android.util.Slog .i ( v6,v5 );
/* .line 378 */
/* if-nez p1, :cond_0 */
/* .line 379 */
try { // :try_start_0
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
} // .end local p1 # "dataInfoJson":Lorg/json/JSONObject;
/* .local v5, "dataInfoJson":Lorg/json/JSONObject; */
/* .line 449 */
} // .end local v5 # "dataInfoJson":Lorg/json/JSONObject;
/* .restart local p1 # "dataInfoJson":Lorg/json/JSONObject; */
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v5, p1 */
/* goto/16 :goto_4 */
/* .line 378 */
} // :cond_0
/* move-object/from16 v5, p1 */
/* .line 382 */
} // .end local p1 # "dataInfoJson":Lorg/json/JSONObject;
/* .restart local v5 # "dataInfoJson":Lorg/json/JSONObject; */
} // :goto_0
try { // :try_start_1
v7 = this.appUnlockRateArray;
/* if-nez v7, :cond_1 */
/* .line 383 */
/* new-instance v7, Lorg/json/JSONArray; */
/* invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V */
this.appUnlockRateArray = v7;
/* .line 386 */
} // :cond_1
/* const-string/jumbo v7, "total_auth_cnt" */
v8 = this.totalAuth;
/* iget v8, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v5 ).put ( v7, v8 ); // invoke-virtual {v5, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 387 */
/* const-string/jumbo v7, "total_auth_succ_cnt" */
v8 = this.totalAuth;
/* iget v8, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v5 ).put ( v7, v8 ); // invoke-virtual {v5, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 389 */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "index":I */
} // :goto_1
v8 = v8 = this.sPackageList;
/* if-ge v7, v8, :cond_3 */
/* .line 390 */
/* new-instance v8, Lorg/json/JSONObject; */
/* invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V */
/* .line 391 */
/* .local v8, "appUnlockRateUnit":Lorg/json/JSONObject; */
v9 = this.appAuthMap;
v10 = this.sPackageList;
/* check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
/* .line 392 */
/* .local v9, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
final String v10 = "app_name"; // const-string v10, "app_name"
v11 = this.sPackageList;
(( org.json.JSONObject ) v8 ).put ( v10, v11 ); // invoke-virtual {v8, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 393 */
v10 = this.totalAuth;
/* iget v10, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v8 ).put ( v4, v10 ); // invoke-virtual {v8, v4, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 394 */
v10 = this.totalAuth;
/* iget v10, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v8 ).put ( v3, v10 ); // invoke-virtual {v8, v3, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 395 */
v10 = this.sPackageList;
/* check-cast v10, Ljava/lang/String; */
final String v11 = "com.android.systemui"; // const-string v11, "com.android.systemui"
v10 = (( java.lang.String ) v10 ).equals ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 396 */
v10 = this.systemuiUnlockRate;
/* check-cast v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics; */
/* .line 397 */
/* .local v10, "tempSystemUIUnlockRate":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics; */
/* new-instance v11, Lorg/json/JSONObject; */
/* invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V */
/* .line 398 */
/* .local v11, "screenOn":Lorg/json/JSONObject; */
v12 = this.screenOnAuth;
/* iget v12, v12, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v11 ).put ( v4, v12 ); // invoke-virtual {v11, v4, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 399 */
v12 = this.screenOnAuth;
/* iget v12, v12, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v11 ).put ( v3, v12 ); // invoke-virtual {v11, v3, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 400 */
/* new-instance v12, Lorg/json/JSONObject; */
/* invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V */
/* .line 401 */
/* .local v12, "screenOnHighLight":Lorg/json/JSONObject; */
v13 = this.screenOn_highLight_Auth;
/* iget v13, v13, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v12 ).put ( v4, v13 ); // invoke-virtual {v12, v4, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 402 */
v13 = this.screenOn_highLight_Auth;
/* iget v13, v13, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v12 ).put ( v3, v13 ); // invoke-virtual {v12, v3, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 403 */
(( org.json.JSONObject ) v11 ).put ( v2, v12 ); // invoke-virtual {v11, v2, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 404 */
/* new-instance v13, Lorg/json/JSONObject; */
/* invoke-direct {v13}, Lorg/json/JSONObject;-><init>()V */
/* .line 405 */
/* .local v13, "screenOnLowLight":Lorg/json/JSONObject; */
v14 = this.screenOn_lowLight_Auth;
/* iget v14, v14, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v13 ).put ( v4, v14 ); // invoke-virtual {v13, v4, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 406 */
v14 = this.screenOn_lowLight_Auth;
/* iget v14, v14, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v13 ).put ( v3, v14 ); // invoke-virtual {v13, v3, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 407 */
(( org.json.JSONObject ) v11 ).put ( v0, v13 ); // invoke-virtual {v11, v0, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 408 */
final String v14 = "screen_on_status"; // const-string v14, "screen_on_status"
(( org.json.JSONObject ) v8 ).put ( v14, v11 ); // invoke-virtual {v8, v14, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 410 */
/* new-instance v14, Lorg/json/JSONObject; */
/* invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V */
/* .line 411 */
/* .local v14, "screenOff":Lorg/json/JSONObject; */
v15 = this.screenOffAuth;
/* iget v15, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v14 ).put ( v4, v15 ); // invoke-virtual {v14, v4, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 412 */
v15 = this.screenOffAuth;
/* iget v15, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v14 ).put ( v3, v15 ); // invoke-virtual {v14, v3, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 413 */
/* new-instance v15, Lorg/json/JSONObject; */
/* invoke-direct {v15}, Lorg/json/JSONObject;-><init>()V */
/* .line 414 */
/* .local v15, "screenOffHighLight":Lorg/json/JSONObject; */
/* move-object/from16 p1, v9 */
} // .end local v9 # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
/* .local p1, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
v9 = this.screenOff_highLight_Auth;
/* iget v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v15 ).put ( v4, v9 ); // invoke-virtual {v15, v4, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 415 */
v9 = this.screenOff_highLight_Auth;
/* iget v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v15 ).put ( v3, v9 ); // invoke-virtual {v15, v3, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 416 */
(( org.json.JSONObject ) v14 ).put ( v2, v15 ); // invoke-virtual {v14, v2, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 417 */
/* new-instance v9, Lorg/json/JSONObject; */
/* invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V */
/* .line 418 */
/* .local v9, "screenOffLowLight":Lorg/json/JSONObject; */
/* move-object/from16 v16, v11 */
} // .end local v11 # "screenOn":Lorg/json/JSONObject;
/* .local v16, "screenOn":Lorg/json/JSONObject; */
v11 = this.screenOff_lowLight_Auth;
/* iget v11, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v9 ).put ( v4, v11 ); // invoke-virtual {v9, v4, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 419 */
v11 = this.screenOff_lowLight_Auth;
/* iget v11, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v9 ).put ( v3, v11 ); // invoke-virtual {v9, v3, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 420 */
(( org.json.JSONObject ) v14 ).put ( v0, v9 ); // invoke-virtual {v14, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 421 */
final String v11 = "screen_off_status"; // const-string v11, "screen_off_status"
(( org.json.JSONObject ) v8 ).put ( v11, v14 ); // invoke-virtual {v8, v11, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 423 */
/* new-instance v11, Lorg/json/JSONObject; */
/* invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V */
/* .line 424 */
/* .local v11, "highLight":Lorg/json/JSONObject; */
/* move-object/from16 v17, v9 */
} // .end local v9 # "screenOffLowLight":Lorg/json/JSONObject;
/* .local v17, "screenOffLowLight":Lorg/json/JSONObject; */
v9 = this.highLightAuth;
/* iget v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v11 ).put ( v4, v9 ); // invoke-virtual {v11, v4, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 425 */
v9 = this.highLightAuth;
/* iget v9, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v11 ).put ( v3, v9 ); // invoke-virtual {v11, v3, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 426 */
(( org.json.JSONObject ) v8 ).put ( v2, v11 ); // invoke-virtual {v8, v2, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 428 */
/* new-instance v9, Lorg/json/JSONObject; */
/* invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V */
/* .line 429 */
/* .local v9, "lowLight":Lorg/json/JSONObject; */
/* move-object/from16 v18, v2 */
v2 = this.lowLightAuth;
/* iget v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONObject ) v9 ).put ( v4, v2 ); // invoke-virtual {v9, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 430 */
v2 = this.lowLightAuth;
/* iget v2, v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONObject ) v9 ).put ( v3, v2 ); // invoke-virtual {v9, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 431 */
(( org.json.JSONObject ) v8 ).put ( v0, v9 ); // invoke-virtual {v8, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 395 */
} // .end local v10 # "tempSystemUIUnlockRate":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
} // .end local v11 # "highLight":Lorg/json/JSONObject;
} // .end local v12 # "screenOnHighLight":Lorg/json/JSONObject;
} // .end local v13 # "screenOnLowLight":Lorg/json/JSONObject;
} // .end local v14 # "screenOff":Lorg/json/JSONObject;
} // .end local v15 # "screenOffHighLight":Lorg/json/JSONObject;
} // .end local v16 # "screenOn":Lorg/json/JSONObject;
} // .end local v17 # "screenOffLowLight":Lorg/json/JSONObject;
} // .end local p1 # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
/* .local v9, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
} // :cond_2
/* move-object/from16 v18, v2 */
/* move-object/from16 p1, v9 */
/* .line 433 */
} // .end local v9 # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
/* .restart local p1 # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
} // :goto_2
v2 = this.appUnlockRateArray;
(( org.json.JSONArray ) v2 ).put ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;
/* .line 389 */
/* nop */
} // .end local v8 # "appUnlockRateUnit":Lorg/json/JSONObject;
} // .end local p1 # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
/* add-int/lit8 v7, v7, 0x1 */
/* move-object/from16 v2, v18 */
/* goto/16 :goto_1 */
/* .line 435 */
} // .end local v7 # "index":I
} // :cond_3
final String v0 = "app_unlock_rate"; // const-string v0, "app_unlock_rate"
v2 = this.appUnlockRateArray;
(( org.json.JSONObject ) v5 ).put ( v0, v2 ); // invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 438 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "index":I */
} // :goto_3
v2 = this.otherAppLuxUnlockRate;
v2 = v2 = this.luxLightUnlockList;
/* if-ge v0, v2, :cond_4 */
/* .line 439 */
v2 = this.luxLightSystemuiSyAuthCountArray;
v3 = this.systemuiLuxUnlockRate;
v3 = this.luxLightUnlockList;
/* check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONArray ) v2 ).put ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;
/* .line 440 */
v2 = this.luxLightSystemuiAuthSuccCountArray;
v3 = this.systemuiLuxUnlockRate;
v3 = this.luxLightUnlockList;
/* check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONArray ) v2 ).put ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;
/* .line 441 */
v2 = this.luxLightOtherSyAuthCountArray;
v3 = this.otherAppLuxUnlockRate;
v3 = this.luxLightUnlockList;
/* check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
(( org.json.JSONArray ) v2 ).put ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;
/* .line 442 */
v2 = this.luxLightOtherAuthSuccCountArray;
v3 = this.otherAppLuxUnlockRate;
v3 = this.luxLightUnlockList;
/* check-cast v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
/* iget v3, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
(( org.json.JSONArray ) v2 ).put ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(II)Lorg/json/JSONArray;
/* .line 438 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 444 */
} // .end local v0 # "index":I
} // :cond_4
final String v0 = "lux_systemui_count_array"; // const-string v0, "lux_systemui_count_array"
v2 = this.luxLightSystemuiSyAuthCountArray;
(( org.json.JSONObject ) v5 ).put ( v0, v2 ); // invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 445 */
final String v0 = "lux_systemui_succ_count_array"; // const-string v0, "lux_systemui_succ_count_array"
v2 = this.luxLightSystemuiAuthSuccCountArray;
(( org.json.JSONObject ) v5 ).put ( v0, v2 ); // invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 446 */
final String v0 = "lux_otherapp_count_array"; // const-string v0, "lux_otherapp_count_array"
v2 = this.luxLightOtherSyAuthCountArray;
(( org.json.JSONObject ) v5 ).put ( v0, v2 ); // invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 447 */
final String v0 = "lux_otherapp_succ_count_array"; // const-string v0, "lux_otherapp_succ_count_array"
v2 = this.luxLightOtherAuthSuccCountArray;
(( org.json.JSONObject ) v5 ).put ( v0, v2 ); // invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 452 */
/* nop */
/* .line 453 */
int v0 = 1; // const/4 v0, 0x1
/* .line 449 */
/* :catch_1 */
/* move-exception v0 */
/* .line 450 */
/* .local v0, "e":Lorg/json/JSONException; */
} // :goto_4
/* const-string/jumbo v2, "updateDataToJson exception" */
android.util.Slog .e ( v6,v2,v0 );
/* .line 451 */
int v2 = 0; // const/4 v2, 0x0
} // .end method
public Boolean updateJsonToData ( org.json.JSONObject p0 ) {
/* .locals 21 */
/* .param p1, "dataInfoJson" # Lorg/json/JSONObject; */
/* .line 459 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
final String v0 = "low_light_status"; // const-string v0, "low_light_status"
final String v3 = "high_light_status"; // const-string v3, "high_light_status"
final String v4 = "FingerprintUnlockRateData"; // const-string v4, "FingerprintUnlockRateData"
final String v5 = "auth_success_count"; // const-string v5, "auth_success_count"
final String v6 = "auth_count"; // const-string v6, "auth_count"
try { // :try_start_0
v8 = this.totalAuth;
/* const-string/jumbo v9, "total_auth_cnt" */
v9 = (( org.json.JSONObject ) v2 ).getInt ( v9 ); // invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v9, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 460 */
v8 = this.totalAuth;
/* const-string/jumbo v9, "total_auth_succ_cnt" */
v9 = (( org.json.JSONObject ) v2 ).getInt ( v9 ); // invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v9, v8, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 462 */
final String v8 = "app_unlock_rate"; // const-string v8, "app_unlock_rate"
(( org.json.JSONObject ) v2 ).getJSONArray ( v8 ); // invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
this.appUnlockRateArray = v8;
/* .line 463 */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "tempIndex":I */
} // :goto_0
v9 = this.appUnlockRateArray;
v9 = (( org.json.JSONArray ) v9 ).length ( ); // invoke-virtual {v9}, Lorg/json/JSONArray;->length()I
/* if-ge v8, v9, :cond_1 */
/* .line 464 */
v9 = this.appUnlockRateArray;
(( org.json.JSONArray ) v9 ).getJSONObject ( v8 ); // invoke-virtual {v9, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 465 */
/* .local v9, "appUnlockRateUnit":Lorg/json/JSONObject; */
v10 = this.sAppStatisticList;
/* check-cast v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
/* .line 466 */
/* .local v10, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
v11 = this.totalAuth;
v12 = (( org.json.JSONObject ) v9 ).getInt ( v6 ); // invoke-virtual {v9, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v12, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 467 */
v11 = this.totalAuth;
v12 = (( org.json.JSONObject ) v9 ).getInt ( v5 ); // invoke-virtual {v9, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v12, v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 468 */
final String v11 = "app_name"; // const-string v11, "app_name"
(( org.json.JSONObject ) v9 ).get ( v11 ); // invoke-virtual {v9, v11}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
final String v12 = "com.android.systemui"; // const-string v12, "com.android.systemui"
v11 = (( java.lang.Object ) v11 ).equals ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v11 != null) { // if-eqz v11, :cond_0
/* .line 469 */
v11 = this.systemuiUnlockRate;
/* check-cast v11, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics; */
/* .line 470 */
/* .local v11, "tempSystemUIUnlockRate":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics; */
final String v12 = "screen_on_status"; // const-string v12, "screen_on_status"
(( org.json.JSONObject ) v9 ).getJSONObject ( v12 ); // invoke-virtual {v9, v12}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 471 */
/* .local v12, "screenOn":Lorg/json/JSONObject; */
v13 = this.screenOnAuth;
v14 = (( org.json.JSONObject ) v12 ).getInt ( v6 ); // invoke-virtual {v12, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v14, v13, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 472 */
v13 = this.screenOnAuth;
v14 = (( org.json.JSONObject ) v12 ).getInt ( v5 ); // invoke-virtual {v12, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v14, v13, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 473 */
(( org.json.JSONObject ) v12 ).getJSONObject ( v3 ); // invoke-virtual {v12, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 474 */
/* .local v13, "screenOnHighLight":Lorg/json/JSONObject; */
v14 = this.screenOn_highLight_Auth;
v15 = (( org.json.JSONObject ) v13 ).getInt ( v6 ); // invoke-virtual {v13, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v15, v14, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 475 */
v14 = this.screenOn_highLight_Auth;
v15 = (( org.json.JSONObject ) v13 ).getInt ( v5 ); // invoke-virtual {v13, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v15, v14, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 476 */
(( org.json.JSONObject ) v12 ).getJSONObject ( v0 ); // invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 477 */
/* .local v14, "screenOnLowLight":Lorg/json/JSONObject; */
v15 = this.screenOn_lowLight_Auth;
v7 = (( org.json.JSONObject ) v14 ).getInt ( v6 ); // invoke-virtual {v14, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v7, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 478 */
v7 = this.screenOn_lowLight_Auth;
v15 = (( org.json.JSONObject ) v14 ).getInt ( v5 ); // invoke-virtual {v14, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v15, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 479 */
final String v7 = "screen_off_status"; // const-string v7, "screen_off_status"
(( org.json.JSONObject ) v9 ).getJSONObject ( v7 ); // invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 480 */
/* .local v7, "screenOff":Lorg/json/JSONObject; */
v15 = this.screenOffAuth;
/* move-object/from16 v16, v10 */
} // .end local v10 # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
/* .local v16, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
v10 = (( org.json.JSONObject ) v7 ).getInt ( v6 ); // invoke-virtual {v7, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v10, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 481 */
v10 = this.screenOffAuth;
v15 = (( org.json.JSONObject ) v7 ).getInt ( v5 ); // invoke-virtual {v7, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v15, v10, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 482 */
(( org.json.JSONObject ) v7 ).getJSONObject ( v3 ); // invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 483 */
/* .local v10, "screenOffHighLight":Lorg/json/JSONObject; */
v15 = this.screenOff_highLight_Auth;
/* move-object/from16 v17, v12 */
} // .end local v12 # "screenOn":Lorg/json/JSONObject;
/* .local v17, "screenOn":Lorg/json/JSONObject; */
v12 = (( org.json.JSONObject ) v10 ).getInt ( v6 ); // invoke-virtual {v10, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v12, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 484 */
v12 = this.screenOff_highLight_Auth;
v15 = (( org.json.JSONObject ) v10 ).getInt ( v5 ); // invoke-virtual {v10, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v15, v12, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 485 */
(( org.json.JSONObject ) v7 ).getJSONObject ( v0 ); // invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 486 */
/* .local v12, "screenOffLowLight":Lorg/json/JSONObject; */
v15 = this.screenOff_lowLight_Auth;
/* move-object/from16 v18, v7 */
} // .end local v7 # "screenOff":Lorg/json/JSONObject;
/* .local v18, "screenOff":Lorg/json/JSONObject; */
v7 = (( org.json.JSONObject ) v12 ).getInt ( v6 ); // invoke-virtual {v12, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v7, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 487 */
v7 = this.screenOff_lowLight_Auth;
v15 = (( org.json.JSONObject ) v12 ).getInt ( v5 ); // invoke-virtual {v12, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v15, v7, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 489 */
(( org.json.JSONObject ) v9 ).getJSONObject ( v3 ); // invoke-virtual {v9, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 490 */
/* .local v7, "highLight":Lorg/json/JSONObject; */
v15 = this.highLightAuth;
/* move-object/from16 v19, v3 */
v3 = (( org.json.JSONObject ) v7 ).getInt ( v6 ); // invoke-virtual {v7, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v3, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 491 */
v3 = this.highLightAuth;
v15 = (( org.json.JSONObject ) v7 ).getInt ( v5 ); // invoke-virtual {v7, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v15, v3, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 492 */
(( org.json.JSONObject ) v9 ).getJSONObject ( v0 ); // invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 493 */
/* .local v3, "lowLight":Lorg/json/JSONObject; */
v15 = this.lowLightAuth;
/* move-object/from16 v20, v0 */
v0 = (( org.json.JSONObject ) v3 ).getInt ( v6 ); // invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v0, v15, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 494 */
v0 = this.lowLightAuth;
v15 = (( org.json.JSONObject ) v3 ).getInt ( v5 ); // invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v15, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 468 */
} // .end local v3 # "lowLight":Lorg/json/JSONObject;
} // .end local v7 # "highLight":Lorg/json/JSONObject;
} // .end local v11 # "tempSystemUIUnlockRate":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$SystemUIUnlockRateStatistics;
} // .end local v12 # "screenOffLowLight":Lorg/json/JSONObject;
} // .end local v13 # "screenOnHighLight":Lorg/json/JSONObject;
} // .end local v14 # "screenOnLowLight":Lorg/json/JSONObject;
} // .end local v16 # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
} // .end local v17 # "screenOn":Lorg/json/JSONObject;
} // .end local v18 # "screenOff":Lorg/json/JSONObject;
/* .local v10, "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic; */
} // :cond_0
/* move-object/from16 v20, v0 */
/* move-object/from16 v19, v3 */
/* move-object/from16 v16, v10 */
/* .line 463 */
} // .end local v9 # "appUnlockRateUnit":Lorg/json/JSONObject;
} // .end local v10 # "tempUnit":Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$AppUnlockRateStatistic;
} // :goto_1
/* add-int/lit8 v8, v8, 0x1 */
/* move-object/from16 v3, v19 */
/* move-object/from16 v0, v20 */
/* goto/16 :goto_0 */
/* .line 499 */
} // :cond_1
final String v0 = "lux_systemui_count_array"; // const-string v0, "lux_systemui_count_array"
(( org.json.JSONObject ) v2 ).getJSONArray ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
this.luxLightSystemuiSyAuthCountArray = v0;
/* .line 500 */
final String v0 = "lux_systemui_succ_count_array"; // const-string v0, "lux_systemui_succ_count_array"
(( org.json.JSONObject ) v2 ).getJSONArray ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
this.luxLightSystemuiAuthSuccCountArray = v0;
/* .line 501 */
final String v0 = "lux_otherapp_count_array"; // const-string v0, "lux_otherapp_count_array"
(( org.json.JSONObject ) v2 ).getJSONArray ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
this.luxLightOtherSyAuthCountArray = v0;
/* .line 502 */
final String v0 = "lux_otherapp_succ_count_array"; // const-string v0, "lux_otherapp_succ_count_array"
(( org.json.JSONObject ) v2 ).getJSONArray ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
this.luxLightOtherAuthSuccCountArray = v0;
/* .line 504 */
v0 = this.luxLightSystemuiSyAuthCountArray;
v0 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* .line 505 */
/* .local v0, "arraylength":I */
v3 = this.luxLightSystemuiSyAuthCountArray;
v3 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-ne v3, v0, :cond_4 */
v3 = this.luxLightSystemuiAuthSuccCountArray;
v3 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-ne v3, v0, :cond_4 */
v3 = this.luxLightOtherSyAuthCountArray;
/* .line 506 */
v3 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-ne v3, v0, :cond_4 */
v3 = this.luxLightOtherAuthSuccCountArray;
v3 = (( org.json.JSONArray ) v3 ).length ( ); // invoke-virtual {v3}, Lorg/json/JSONArray;->length()I
/* if-eq v3, v0, :cond_2 */
/* goto/16 :goto_3 */
/* .line 510 */
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
} // .end local v8 # "tempIndex":I
/* .local v3, "tempIndex":I */
} // :goto_2
v5 = this.appUnlockRateArray;
v5 = (( org.json.JSONArray ) v5 ).length ( ); // invoke-virtual {v5}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v5, :cond_3 */
/* .line 511 */
v5 = this.luxLightSystemuiSyAuthCountArray;
(( org.json.JSONArray ) v5 ).get ( v3 ); // invoke-virtual {v5, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
(( java.lang.Object ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
java.lang.Integer .valueOf ( v5 );
/* .line 512 */
/* .local v5, "luxSystemuicount":Ljava/lang/Integer; */
v6 = this.luxLightSystemuiAuthSuccCountArray;
(( org.json.JSONArray ) v6 ).get ( v3 ); // invoke-virtual {v6, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
(( java.lang.Object ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;
v6 = java.lang.Integer .parseInt ( v6 );
java.lang.Integer .valueOf ( v6 );
/* .line 513 */
/* .local v6, "luxSystemuiSuccCount":Ljava/lang/Integer; */
v7 = this.luxLightOtherSyAuthCountArray;
(( org.json.JSONArray ) v7 ).get ( v3 ); // invoke-virtual {v7, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
(( java.lang.Object ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;
v7 = java.lang.Integer .parseInt ( v7 );
java.lang.Integer .valueOf ( v7 );
/* .line 514 */
/* .local v7, "luxOtherAppcount":Ljava/lang/Integer; */
v8 = this.luxLightOtherAuthSuccCountArray;
(( org.json.JSONArray ) v8 ).get ( v3 ); // invoke-virtual {v8, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
(( java.lang.Object ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;
v8 = java.lang.Integer .parseInt ( v8 );
java.lang.Integer .valueOf ( v8 );
/* .line 515 */
/* .local v8, "luxOtherAppSuccCount":Ljava/lang/Integer; */
v9 = this.systemuiLuxUnlockRate;
v9 = this.luxLightUnlockList;
/* check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
v10 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* iput v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 516 */
v9 = this.systemuiLuxUnlockRate;
v9 = this.luxLightUnlockList;
/* check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
v10 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* iput v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 517 */
v9 = this.otherAppLuxUnlockRate;
v9 = this.luxLightUnlockList;
/* check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
v10 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* iput v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authCount:I */
/* .line 518 */
v9 = this.otherAppLuxUnlockRate;
v9 = this.luxLightUnlockList;
/* check-cast v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit; */
v10 = (( java.lang.Integer ) v8 ).intValue ( ); // invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I
/* iput v10, v9, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData$UnlockRateUnit;->authSuccessCount:I */
/* .line 510 */
} // .end local v5 # "luxSystemuicount":Ljava/lang/Integer;
} // .end local v6 # "luxSystemuiSuccCount":Ljava/lang/Integer;
} // .end local v7 # "luxOtherAppcount":Ljava/lang/Integer;
} // .end local v8 # "luxOtherAppSuccCount":Ljava/lang/Integer;
/* add-int/lit8 v3, v3, 0x1 */
/* goto/16 :goto_2 */
/* .line 529 */
} // .end local v0 # "arraylength":I
} // .end local v3 # "tempIndex":I
} // :cond_3
/* nop */
/* .line 530 */
int v0 = 1; // const/4 v0, 0x1
/* .line 507 */
/* .restart local v0 # "arraylength":I */
/* .local v8, "tempIndex":I */
} // :cond_4
} // :goto_3
final String v3 = "bad luxLight JSONArray"; // const-string v3, "bad luxLight JSONArray"
android.util.Slog .e ( v4,v3 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 508 */
int v3 = 0; // const/4 v3, 0x0
/* .line 525 */
} // .end local v0 # "arraylength":I
} // .end local v8 # "tempIndex":I
/* :catch_0 */
/* move-exception v0 */
/* .line 526 */
/* .local v0, "e":Ljava/lang/IndexOutOfBoundsException; */
/* const-string/jumbo v3, "updateJsonToData IndexOutOfBoundsException" */
android.util.Slog .e ( v4,v3,v0 );
/* .line 527 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->resetLocalInfo()V */
/* .line 528 */
int v3 = 0; // const/4 v3, 0x0
/* .line 521 */
} // .end local v0 # "e":Ljava/lang/IndexOutOfBoundsException;
/* :catch_1 */
/* move-exception v0 */
int v3 = 0; // const/4 v3, 0x0
/* .line 522 */
/* .local v0, "e":Lorg/json/JSONException; */
/* const-string/jumbo v5, "updateJsonToData JSONException" */
android.util.Slog .e ( v4,v5,v0 );
/* .line 523 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->resetLocalInfo()V */
/* .line 524 */
} // .end method
