.class public Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;
.super Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
.source "FingerprintFailReasonData.java"


# static fields
.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_ANTI_SPOOF:I = 0x1f

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_DRY_FINGER:I = 0x20

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_DUPLICATE_AREA:I = 0x19

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_DUPLICATE_FINGER:I = 0x1a

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_ENROLL_ERROR:I = 0x2f

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_FIXED_PATTERN:I = 0x39

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_HBM_TOO_SLOW:I = 0x28

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_INPUT_TOO_LONG:I = 0x18

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_INVALID_DATA:I = 0x30

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_LATENT_CLASSIFIER:I = 0x1e

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_LOGO:I = 0x22

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_BRIGHTNESS:I = 0x21

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_CONTRAST:I = 0x38

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_MOBILITY:I = 0x31

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_OVERLAP:I = 0x36

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_LOW_SIMILARITY:I = 0x37

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_NOT_MATCH:I = 0x2c

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_OVER_EXPOSURE:I = 0x35

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_QUICK_CLICK:I = 0x1d

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_SHOW_CIRCLE:I = 0x2e

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_SIMULATED_FINGER:I = 0x1b

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_SPI_COMMUNICATION:I = 0x29

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_SUSPICIOUS_BAD_QUALITY:I = 0x34

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_SUSPICIOUS_LOW_QUALITY:I = 0x33

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_TOO_DIM:I = 0x2b

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_TOUCH_BY_MISTAKE:I = 0x1c

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_UI_DISAPPEAR:I = 0x2d

.field private static final CUSTOMIZED_FINGERPRINT_ACQUIRED_WET_FINGER:I = 0x2a

.field private static final TAG:Ljava/lang/String; = "FingerprintFailReasonData"

.field private static volatile sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;


# instance fields
.field private failReasonArray:Lorg/json/JSONArray;

.field private failReasonJson:Lorg/json/JSONObject;

.field private failReasonJsonArrayList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/json/JSONArray;",
            ">;"
        }
    .end annotation
.end field

.field private failReasonJsonKeyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private failReasonRecordList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private failReasonStatisticList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private hbmFailReasonArray:Lorg/json/JSONArray;

.field private hbmFailReasonStatisticList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private lowlightFailReasonArray:Lorg/json/JSONArray;

.field private lowlightFailReasonStatisticList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFailReasonCodeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFailReasonInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private screenOffFailReasonArray:Lorg/json/JSONArray;

.field private screenOffFailReasonStatisticList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private screenOnFailReasonArray:Lorg/json/JSONArray;

.field private screenOnFailReasonStatisticList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 35

    .line 15
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V

    .line 69
    const-string v1, "1-0"

    const-string v2, "2-0"

    const-string v3, "3-0"

    const-string v4, "4-0"

    const-string v5, "5-0"

    const-string v6, "6-24"

    const-string v7, "6-25"

    const-string v8, "6-26"

    const-string v9, "6-27"

    const-string v10, "6-28"

    const-string v11, "6-29"

    const-string v12, "6-30"

    const-string v13, "6-31"

    const-string v14, "6-32"

    const-string v15, "6-33"

    const-string v16, "6-34"

    const-string v17, "6-40"

    const-string v18, "6-41"

    const-string v19, "6-42"

    const-string v20, "6-43"

    const-string v21, "6-44"

    const-string v22, "6-45"

    const-string v23, "6-46"

    const-string v24, "6-47"

    const-string v25, "6-48"

    const-string v26, "6-49"

    const-string v27, "6-51"

    const-string v28, "6-52"

    const-string v29, "6-53"

    const-string v30, "6-54"

    const-string v31, "6-55"

    const-string v32, "6-56"

    const-string v33, "6-57"

    filled-new-array/range {v1 .. v33}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    .line 105
    const-string v2, "fail_reason_partial_cnt"

    const-string v3, "fail_reason_insufficient_cnt"

    const-string v4, "fail_reason_img_dirty_cnt"

    const-string v5, "fail_reason_too_slow_cnt"

    const-string v6, "fail_reason_finger_leave_too_fast_cnt"

    const-string v7, "fail_reason_input_too_long_cnt"

    const-string v8, "fail_reason_duplicate_area_cnt"

    const-string v9, "fail_reason_duplicate_finger_cnt"

    const-string v10, "fail_reason_simulated_finger_cnt"

    const-string v11, "fail_reason_touch_by_mistake_cnt"

    const-string v12, "fail_reason_suspicious_cmn_cnt"

    const-string v13, "fail_reason_latent_classifier_cnt"

    const-string v14, "fail_reason_anti_spoof_cnt"

    const-string v15, "fail_reason_dry_finger_cnt"

    const-string v16, "fail_reason_low_brightness_cnt"

    const-string v17, "fail_reason_logo_cnt"

    const-string v18, "fail_reason_HBM_too_slow_cnt"

    const-string v19, "fail_reason_SPI_communication_cnt"

    const-string v20, "fail_reason_wet_finger_cnt"

    const-string v21, "fail_reason_too_dim_cnt"

    const-string v22, "fail_reason_not_match_cnt"

    const-string v23, "fail_reason_UI_disappear_cnt"

    const-string v24, "fail_reason_show_circle_cnt"

    const-string v25, "fail_reason_enroll_error_cnt"

    const-string v26, "fail_reason_invalid_data_cnt"

    const-string v27, "fail_reason_low_mobility_cnt"

    const-string v28, "fail_reason_suspicious_low_quality_cnt"

    const-string v29, "fail_reason_suspicious_bad_quality_cnt"

    const-string v30, "fail_reason_over_exposure_cnt"

    const-string v31, "fail_reason_low_overlap_cnt"

    const-string v32, "fail_reason_low_similarity_cnt"

    const-string v33, "fail_reason_low_contrast_cnt"

    const-string v34, "fail_reason_fixed_pattern_cnt"

    filled-new-array/range {v2 .. v34}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonInfoList:Ljava/util/List;

    .line 142
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonStatisticList:Ljava/util/List;

    .line 143
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2, v3}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOnFailReasonStatisticList:Ljava/util/List;

    .line 144
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2, v3}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOffFailReasonStatisticList:Ljava/util/List;

    .line 145
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2, v3}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->hbmFailReasonStatisticList:Ljava/util/List;

    .line 146
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2, v3}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->lowlightFailReasonStatisticList:Ljava/util/List;

    .line 149
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonJson:Lorg/json/JSONObject;

    .line 150
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonArray:Lorg/json/JSONArray;

    .line 151
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOnFailReasonArray:Lorg/json/JSONArray;

    .line 152
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOffFailReasonArray:Lorg/json/JSONArray;

    .line 153
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->hbmFailReasonArray:Lorg/json/JSONArray;

    .line 154
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->lowlightFailReasonArray:Lorg/json/JSONArray;

    .line 156
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonStatisticList:Ljava/util/List;

    iget-object v3, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOnFailReasonStatisticList:Ljava/util/List;

    iget-object v4, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOffFailReasonStatisticList:Ljava/util/List;

    iget-object v5, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->hbmFailReasonStatisticList:Ljava/util/List;

    iget-object v6, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->lowlightFailReasonStatisticList:Ljava/util/List;

    filled-new-array {v2, v3, v4, v5, v6}, [Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonRecordList:Ljava/util/List;

    .line 164
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonArray:Lorg/json/JSONArray;

    iget-object v3, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOnFailReasonArray:Lorg/json/JSONArray;

    iget-object v4, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOffFailReasonArray:Lorg/json/JSONArray;

    iget-object v5, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->hbmFailReasonArray:Lorg/json/JSONArray;

    iget-object v6, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->lowlightFailReasonArray:Lorg/json/JSONArray;

    filled-new-array {v2, v3, v4, v5, v6}, [Lorg/json/JSONArray;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonJsonArrayList:Ljava/util/List;

    .line 172
    const-string v1, "hbm_fail_reason"

    const-string v2, "lowlight_fail_reason"

    const-string v3, "fail_reason"

    const-string v4, "screen_on_fail_reason"

    const-string v5, "screen_off_fail_reason"

    filled-new-array {v3, v4, v5, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonJsonKeyList:Ljava/util/List;

    return-void
.end method

.method public static getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;
    .locals 3

    .line 21
    const-string v0, "FingerprintFailReasonData"

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    if-nez v0, :cond_1

    .line 23
    const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    monitor-enter v0

    .line 24
    :try_start_0
    const-string v1, "FingerprintFailReasonData"

    const-string v2, "getInstance class"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    if-nez v1, :cond_0

    .line 26
    const-string v1, "FingerprintFailReasonData"

    const-string v2, "getInstance new"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;-><init>()V

    sput-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    .line 29
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 31
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    return-object v0
.end method

.method static synthetic lambda$resetLocalInfo$0(Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0, "i"    # Ljava/lang/Integer;

    .line 284
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public calculateFailReasonCnt(III)Z
    .locals 6
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I
    .param p3, "state"    # I

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calculateFailReasonCnt, acquiredInfo: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,vendorCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",screenStatus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintFailReasonData"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, "failReasonCode":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 185
    const-string/jumbo v2, "start calculateFailReasonCnt"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonStatisticList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonStatisticList:Ljava/util/List;

    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    .line 187
    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 186
    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 188
    iput p3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mScreenStatus:I

    .line 189
    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mScreenStatus:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 190
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOnFailReasonStatisticList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOnFailReasonStatisticList:Ljava/util/List;

    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    .line 191
    invoke-interface {v5, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 190
    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOffFailReasonStatisticList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOffFailReasonStatisticList:Ljava/util/List;

    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    .line 194
    invoke-interface {v5, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 193
    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 197
    :goto_0
    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFingerUnlockBright:I

    if-ne v1, v4, :cond_1

    .line 198
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->lowlightFailReasonStatisticList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->lowlightFailReasonStatisticList:Ljava/util/List;

    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    .line 199
    invoke-interface {v5, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 198
    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 201
    :cond_1
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->hbmFailReasonStatisticList:Ljava/util/List;

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->hbmFailReasonStatisticList:Ljava/util/List;

    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->mFailReasonCodeList:Ljava/util/List;

    .line 202
    invoke-interface {v5, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 201
    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 209
    :goto_1
    return v4

    .line 211
    :cond_2
    const/4 v1, 0x0

    return v1
.end method

.method public resetLocalInfo()V
    .locals 3

    .line 282
    const-string v0, "FingerprintFailReasonData"

    const-string v1, "resetLocalInfo"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonRecordList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 284
    .local v1, "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData$$ExternalSyntheticLambda1;

    invoke-direct {v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->replaceAll(Ljava/util/function/UnaryOperator;)V

    .line 285
    .end local v1    # "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_0

    .line 286
    :cond_0
    return-void
.end method

.method public updateDataToJson(Lorg/json/JSONObject;)Z
    .locals 7
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 248
    const-string/jumbo v0, "updateDataToJson"

    const-string v1, "FingerprintFailReasonData"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    if-nez p1, :cond_0

    .line 251
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    move-object p1, v0

    .line 254
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v0, "failReasonStringList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonRecordList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 256
    .local v3, "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v5

    new-instance v6, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData$$ExternalSyntheticLambda0;

    invoke-direct {v6}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v5, v6}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object v5

    const-string v6, ","

    invoke-static {v6}, Ljava/util/stream/Collectors;->joining(Ljava/lang/CharSequence;)Ljava/util/stream/Collector;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    nop

    .end local v3    # "object":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_0

    .line 259
    :cond_1
    const/4 v2, 0x0

    .line 260
    .local v2, "tempIndex":I
    new-instance v3, Lorg/json/JSONArray;

    add-int/lit8 v4, v2, 0x1

    .end local v2    # "tempIndex":I
    .local v4, "tempIndex":I
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonArray:Lorg/json/JSONArray;

    .line 261
    const-string v2, "fail_reason"

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 263
    new-instance v2, Lorg/json/JSONArray;

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "tempIndex":I
    .local v3, "tempIndex":I
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOnFailReasonArray:Lorg/json/JSONArray;

    .line 264
    const-string v4, "screen_on_fail_reason"

    invoke-virtual {p1, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 266
    new-instance v2, Lorg/json/JSONArray;

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "tempIndex":I
    .restart local v4    # "tempIndex":I
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->screenOffFailReasonArray:Lorg/json/JSONArray;

    .line 267
    const-string v3, "screen_off_fail_reason"

    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 269
    new-instance v2, Lorg/json/JSONArray;

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "tempIndex":I
    .restart local v3    # "tempIndex":I
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->hbmFailReasonArray:Lorg/json/JSONArray;

    .line 270
    const-string v4, "hbm_fail_reason"

    invoke-virtual {p1, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 272
    new-instance v2, Lorg/json/JSONArray;

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "tempIndex":I
    .restart local v4    # "tempIndex":I
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->lowlightFailReasonArray:Lorg/json/JSONArray;

    .line 273
    const-string v3, "lowlight_fail_reason"

    invoke-virtual {p1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    nop

    .line 278
    .end local v0    # "failReasonStringList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "tempIndex":I
    const/4 v0, 0x1

    return v0

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Lorg/json/JSONException;
    const-string/jumbo v2, "updateDataToJson JSONException"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 276
    const/4 v1, 0x0

    return v1
.end method

.method public updateJsonToData(Lorg/json/JSONObject;)Z
    .locals 14
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 217
    const-string v0, ""

    const-string/jumbo v1, "updateJsonToData"

    const-string v2, "FingerprintFailReasonData"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v1, 0x0

    .local v1, "tempIndex":I
    :goto_0
    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonJsonArrayList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 221
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonJsonArrayList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONArray;

    .line 222
    .local v4, "tempJSONArray":Lorg/json/JSONArray;
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonJsonKeyList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    move-object v4, v5

    .line 223
    invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v5

    .line 224
    .local v5, "tempString":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 225
    const-string v6, "["

    invoke-virtual {v5, v6, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 226
    const-string v6, "]"

    invoke-virtual {v5, v6, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 227
    const/4 v6, 0x0

    .line 228
    .local v6, "secondTempIndex":I
    const-string v7, ","

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    move v9, v3

    :goto_1
    if-ge v9, v8, :cond_1

    aget-object v10, v7, v9

    .line 229
    .local v10, "splitstring":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_0

    iget-object v11, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonRecordList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v6, v11, :cond_0

    .line 230
    iget-object v11, p0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->failReasonRecordList:Ljava/util/List;

    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    add-int/lit8 v12, v6, 0x1

    .end local v6    # "secondTempIndex":I
    .local v12, "secondTempIndex":I
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v11, v6, v13}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move v6, v12

    .line 228
    .end local v10    # "splitstring":Ljava/lang/String;
    .end local v12    # "secondTempIndex":I
    .restart local v6    # "secondTempIndex":I
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 220
    .end local v4    # "tempJSONArray":Lorg/json/JSONArray;
    .end local v5    # "tempString":Ljava/lang/String;
    .end local v6    # "secondTempIndex":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 243
    :cond_2
    nop

    .line 244
    const/4 v0, 0x1

    return v0

    .line 239
    .end local v1    # "tempIndex":I
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string/jumbo v1, "updateJsonToData IndexOutOfBoundsException"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 241
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->resetLocalInfo()V

    .line 242
    return v3

    .line 235
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v0

    .line 236
    .local v0, "e":Lorg/json/JSONException;
    const-string/jumbo v1, "updateJsonToData JSONException"

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 237
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->resetLocalInfo()V

    .line 238
    return v3
.end method
