.class public Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;
.super Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;
.source "FingerprintHalEnrollData.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FingerprintHalEnrollData"

.field private static goodix_optical_map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintBigData;-><init>()V

    .line 25
    return-void
.end method

.method public static getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;
    .locals 3

    .line 28
    const-string v0, "FingerprintHalEnrollData"

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    if-nez v0, :cond_1

    .line 30
    const-class v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    monitor-enter v0

    .line 31
    :try_start_0
    const-string v1, "FingerprintHalEnrollData"

    const-string v2, "getInstance class"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    if-nez v1, :cond_0

    .line 33
    const-string v1, "FingerprintHalEnrollData"

    const-string v2, "getInstance new"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    invoke-direct {v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;-><init>()V

    sput-object v1, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    .line 36
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 38
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;->sInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalEnrollData;

    return-object v0
.end method


# virtual methods
.method public getEnrollDataFromHal(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "vendorName"    # Ljava/lang/String;
    .param p2, "is_fod"    # Ljava/lang/Boolean;

    .line 43
    :try_start_0
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelAidl;

    move-result-object v0

    const v1, 0x7a122

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getHalData(I[B)Landroid/hardware/fingerprint/HalDataCmdResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FingerprintHalEnrollData"

    const-string v2, "Get halEnrollData error."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 48
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public parseFpcFodInfo()V
    .locals 0

    .line 61
    return-void
.end method

.method public parseFpcInfo()V
    .locals 0

    .line 65
    return-void
.end method

.method public parseGoodixFodInfo()V
    .locals 2

    .line 52
    const-string v0, "FingerprintHalEnrollData"

    const-string v1, "parseGoodixFodInfo."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    return-void
.end method

.method public parseGoodixInfo()V
    .locals 0

    .line 57
    return-void
.end method

.method public resetLocalInfo()V
    .locals 0

    .line 78
    return-void
.end method

.method public updateDataToJson(Lorg/json/JSONObject;)Z
    .locals 1
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public updateJsonToData(Lorg/json/JSONObject;)Z
    .locals 1
    .param p1, "dataInfoJson"    # Lorg/json/JSONObject;

    .line 69
    const/4 v0, 0x1

    return v0
.end method
