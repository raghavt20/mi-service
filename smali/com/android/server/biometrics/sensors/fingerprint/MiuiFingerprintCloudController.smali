.class public Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;
.super Ljava/lang/Object;
.source "MiuiFingerprintCloudController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch;,
        Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;,
        Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;,
        Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect;
    }
.end annotation


# static fields
.field private static final ANTI_MISTOUCH_ENABLE:Ljava/lang/String; = "enable"

.field private static final ANTI_MISTOUCH_VALID_TIMES:Ljava/lang/String; = "valid_times_per_screenlock"

.field private static final CLOUD_BACKUP_FILE_NAME:Ljava/lang/String; = "fingerprint_cloud_backup.xml"

.field public static final CLOUD_EVENTS_ANTI_MISTOUCH_CONTROL:J = 0x1L

.field public static final CLOUD_EVENTS_LOWBRIGHTNESS_AUTH_CONTROL:J = 0x2L

.field public static final CLOUD_EVENTS_OCP_DETECT_CONTROL:J = 0x4L

.field private static final COUND_FP_FEATURE_ANTI_MISTOUCH:Ljava/lang/String; = "anti_mistouch"

.field private static final COUND_FP_FEATURE_LOWBRIGHTNESS_AUTH:Ljava/lang/String; = "lowbrightness_auth"

.field private static final COUND_FP_FEATURE_OCP_DETECT:Ljava/lang/String; = "ocp_detect"

.field private static final COUND_FP_SUBMODULE_NAME:Ljava/lang/String; = "fingerprint_feature_control"

.field public static final FP_CLOUD_ANTI_MISTOUCH_CMD:I = 0xe

.field public static final FP_CLOUD_ANTI_MISTOUCH_DISABLE_PARAM:I = 0x0

.field public static final FP_CLOUD_ANTI_MISTOUCH_ENABLE_PARAM:I = 0x10

.field public static FP_CLOUD_ANTI_MISTOUCH_VALID_TIMES_PARAM:I = 0x0

.field public static final FP_CLOUD_LOWBRIGHTNESS_AUTH_CMD:I = 0xf

.field public static final FP_CLOUD_LOWBRIGHTNESS_AUTH_DISABLE_PARAM:I = 0x0

.field public static final FP_CLOUD_LOWBRIGHTNESS_AUTH_ENABLE_PARAM:I = 0x10

.field public static final FP_CLOUD_LOWBRIGHTNESS_AUTH_SETTING_DISABLE_DEFAULT_PARAM:I = 0x1

.field public static final FP_CLOUD_LOWBRIGHTNESS_AUTH_SETTING_ENABLE_DEFAULT_PARAM:I = 0x11

.field public static final FP_CLOUD_OCP_DETECT_CMD:I = 0xb

.field public static final FP_CLOUD_OCP_DETECT_DISABLE_PARAM:I = 0x0

.field public static final FP_CLOUD_OCP_DETECT_ENABLE_PARAM:I = 0x10

.field public static final FP_CLOUD_OCP_DETECT_SETTING_DISABLE_DEFAULT_PARAM:I = 0x1

.field public static final FP_CLOUD_OCP_DETECT_SETTING_ENABLE_DEFAULT_PARAM:I = 0x11

.field private static final LOWBRIGHTNESS_AUTH_ENABLE:Ljava/lang/String; = "enable"

.field private static final LOWBRIGHTNESS_AUTH_SETTING_ENABLE_DEFAULT:Ljava/lang/String; = "setting_enable_default"

.field private static final OCP_DETECT_ENABLE:Ljava/lang/String; = "enable"

.field private static final OCP_DETECT_SETTING_ENABLE_DEFAULT:Ljava/lang/String; = "setting_enable_default"

.field private static final TAG:Ljava/lang/String; = "MiuiFingerprintCloudController"

.field private static mFingerprintService:Landroid/hardware/fingerprint/IFingerprintService;

.field private static mInstance:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;


# instance fields
.field private mAntiMistouchEnable:Z

.field private mAntiMistouchValidTimes:I

.field private mCloudBackupXmlFile:Landroid/util/AtomicFile;

.field private mCloudEventsData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mCloudEventsSummary:J

.field private mCloudListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mFingerprintAntiMistouch:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch;

.field private mFingerprintLowBrightnessAuth:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;

.field private mFingerprintOcpDetect:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect;

.field private mHandler:Landroid/os/Handler;

.field private mLowBrightnessAuthEnable:Z

.field private mLowBrightnessAuthSettingEnableDefault:Z

.field private mOcpDetectEnable:Z

.field private mOcpDetectSettingEnableDefault:Z


# direct methods
.method public static synthetic $r8$lambda$NqGkiZJ8Sm2K6_ZMNdmCXg2ueTA(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->lambda$updateDataFromCloudControl$0(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAntiMistouchEnable(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmAntiMistouchValidTimes(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)I
    .locals 0

    iget p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLowBrightnessAuthEnable(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLowBrightnessAuthSettingEnableDefault(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmOcpDetectEnable(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmOcpDetectSettingEnableDefault(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$msyncLocalBackupFromCloud(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->syncLocalBackupFromCloud()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDataFromCloudControl(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->updateDataFromCloudControl()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetmFingerprintService()Landroid/hardware/fingerprint/IFingerprintService;
    .locals 1

    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mFingerprintService:Landroid/hardware/fingerprint/IFingerprintService;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 95
    const/4 v0, 0x1

    sput v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->FP_CLOUD_ANTI_MISTOUCH_VALID_TIMES_PARAM:I

    .line 107
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mFingerprintService:Landroid/hardware/fingerprint/IFingerprintService;

    .line 108
    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mInstance:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/content/Context;)V
    .locals 2
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "context"    # Landroid/content/Context;

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z

    .line 60
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I

    .line 64
    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z

    .line 66
    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z

    .line 70
    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z

    .line 72
    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudListeners:Ljava/util/List;

    .line 111
    iput-object p2, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mContext:Landroid/content/Context;

    .line 112
    const-string v0, "MiuiFingerprintCloudController"

    const-string v1, "construct."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    .line 114
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mFingerprintAntiMistouch:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch;

    .line 115
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->addCloudListener(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V

    .line 116
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;

    invoke-direct {v0, p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mFingerprintLowBrightnessAuth:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;

    .line 117
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->addCloudListener(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V

    .line 118
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect;

    invoke-direct {v0, p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect-IA;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mFingerprintOcpDetect:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect;

    .line 119
    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->addCloudListener(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V

    .line 120
    const-string v0, "fingerprint_cloud_backup.xml"

    invoke-direct {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->getFile(Ljava/lang/String;)Landroid/util/AtomicFile;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    .line 121
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mHandler:Landroid/os/Handler;

    .line 122
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->registerCloudDataObserver()V

    .line 123
    return-void
.end method

.method private getFile(Ljava/lang/String;)Landroid/util/AtomicFile;
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .line 173
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataSystemDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "fingerprintconfig"

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/os/Environment;->buildPath(Ljava/io/File;[Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method private synthetic lambda$updateDataFromCloudControl$0(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V
    .locals 3
    .param p1, "l"    # Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;

    .line 167
    iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    invoke-interface {p1, v0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;->onCloudUpdated(JLjava/util/Map;)V

    .line 168
    return-void
.end method

.method private registerCloudDataObserver()V
    .locals 4

    .line 137
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 138
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;

    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Landroid/os/Handler;)V

    .line 137
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 152
    return-void
.end method

.method private syncLocalBackupFromCloud()V
    .locals 14

    .line 177
    const-string v0, "fingerprint_feature_control"

    const-string v1, "MiuiFingerprintCloudController"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    if-nez v2, :cond_0

    .line 178
    return-void

    .line 180
    :cond_0
    const/4 v2, 0x0

    .line 182
    .local v2, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v3, "Start syncing local backup from cloud."

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    invoke-virtual {v3}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v3

    move-object v2, v3

    .line 184
    invoke-static {v2}, Landroid/util/Xml;->resolveSerializer(Ljava/io/OutputStream;)Lcom/android/modules/utils/TypedXmlSerializer;

    move-result-object v3

    .line 185
    .local v3, "out":Lcom/android/modules/utils/TypedXmlSerializer;
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v11, 0x0

    invoke-interface {v3, v11, v5}, Lcom/android/modules/utils/TypedXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 186
    const-string v5, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v3, v5, v4}, Lcom/android/modules/utils/TypedXmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 187
    invoke-interface {v3, v11, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 189
    iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    const-wide/16 v6, 0x1

    and-long/2addr v4, v6

    const-wide/16 v12, 0x0

    cmp-long v4, v4, v12

    if-eqz v4, :cond_1

    .line 190
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    const-string v8, "enable"

    const-string v9, "anti_mistouch"

    iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z

    move-object v4, p0

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 192
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    const-string/jumbo v8, "valid_times_per_screenlock"

    const-string v9, "anti_mistouch"

    iget v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I

    move-object v4, p0

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureIntToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;I)V

    .line 196
    :cond_1
    iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    const-wide/16 v6, 0x2

    and-long/2addr v4, v6

    cmp-long v4, v4, v12

    if-eqz v4, :cond_2

    .line 197
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    const-string v8, "enable"

    const-string v9, "lowbrightness_auth"

    iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z

    move-object v4, p0

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 199
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    const-string/jumbo v8, "setting_enable_default"

    const-string v9, "lowbrightness_auth"

    iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z

    move-object v4, p0

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 203
    :cond_2
    iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    const-wide/16 v6, 0x4

    and-long/2addr v4, v6

    cmp-long v4, v4, v12

    if-eqz v4, :cond_3

    .line 204
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    const-string v8, "enable"

    const-string v9, "ocp_detect"

    iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z

    move-object v4, p0

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 206
    iget-object v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    const-string/jumbo v8, "setting_enable_default"

    const-string v9, "ocp_detect"

    iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z

    move-object v4, p0

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 209
    :cond_3
    invoke-interface {v3, v11, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 210
    invoke-interface {v3}, Lcom/android/modules/utils/TypedXmlSerializer;->endDocument()V

    .line 211
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 212
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    invoke-virtual {v0, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    .end local v3    # "out":Lcom/android/modules/utils/TypedXmlSerializer;
    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/io/IOException;
    iget-object v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudBackupXmlFile:Landroid/util/AtomicFile;

    invoke-virtual {v3, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 215
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to write local backup"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private updateAntiLowBrightnessAuthCloudState()Z
    .locals 10

    .line 274
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mContext:Landroid/content/Context;

    .line 275
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "fingerprint_feature_control"

    const-string v2, "lowbrightness_auth"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 277
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string/jumbo v1, "updateAntiLowBrightnessAuthCloudState."

    const-string v3, "MiuiFingerprintCloudController"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "data.json() lowbrightness_auth: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 281
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v2, "enable"

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z

    .line 282
    if-eqz v1, :cond_0

    .line 283
    const-string/jumbo v5, "setting_enable_default"

    invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z

    .line 284
    iget-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    const-wide/16 v8, 0x2

    or-long/2addr v6, v8

    iput-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    .line 285
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    iget-boolean v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v4, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update lowbrightness_auth, enable: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :cond_0
    const/4 v2, 0x1

    return v2

    .line 291
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    return v4
.end method

.method private updateAntiMistouchCloudState()Z
    .locals 10

    .line 252
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mContext:Landroid/content/Context;

    .line 253
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "fingerprint_feature_control"

    const-string v2, "anti_mistouch"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 255
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string/jumbo v1, "updateAntiMistouchCloudState."

    const-string v3, "MiuiFingerprintCloudController"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 257
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "data.json() anti_mistouch: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 259
    .local v1, "jsonObject":Lorg/json/JSONObject;
    if-eqz v1, :cond_0

    .line 260
    const-string v2, "enable"

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z

    .line 261
    const-string/jumbo v5, "valid_times_per_screenlock"

    invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I

    .line 262
    iget-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    const-wide/16 v8, 0x1

    or-long/2addr v6, v8

    iput-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    .line 263
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    iget-boolean v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v4, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    iget v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update anti_mistouch, enable: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :cond_0
    const/4 v2, 0x1

    return v2

    .line 269
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    return v4
.end method

.method private updateDataFromCloudControl()Z
    .locals 3

    .line 157
    const-string v0, "MiuiFingerprintCloudController"

    const-string/jumbo v1, "updateDataFromCloudControl."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mFingerprintService:Landroid/hardware/fingerprint/IFingerprintService;

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->getFingerprintService(Landroid/content/Context;)Landroid/hardware/fingerprint/IFingerprintService;

    move-result-object v0

    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mFingerprintService:Landroid/hardware/fingerprint/IFingerprintService;

    .line 161
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    .line 162
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 163
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->updateAntiMistouchCloudState()Z

    move-result v0

    .line 164
    .local v0, "updated":Z
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->updateAntiLowBrightnessAuthCloudState()Z

    move-result v1

    or-int/2addr v0, v1

    .line 165
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->updateOcpDetectCloudState()Z

    move-result v1

    or-int/2addr v0, v1

    .line 166
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudListeners:Ljava/util/List;

    new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)V

    invoke-interface {v1, v2}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 169
    return v0
.end method

.method private updateOcpDetectCloudState()Z
    .locals 10

    .line 296
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mContext:Landroid/content/Context;

    .line 297
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "fingerprint_feature_control"

    const-string v2, "ocp_detect"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 299
    .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string/jumbo v1, "updateOcpDetectCloudState."

    const-string v3, "MiuiFingerprintCloudController"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 301
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "data.json() ocp_detect: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 303
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v2, "enable"

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z

    .line 304
    if-eqz v1, :cond_0

    .line 305
    const-string/jumbo v5, "setting_enable_default"

    invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z

    .line 306
    iget-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    const-wide/16 v8, 0x4

    or-long/2addr v6, v8

    iput-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    .line 307
    iget-object v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    iget-boolean v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v4, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update ocp_detect, enable: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_0
    const/4 v2, 0x1

    return v2

    .line 313
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    return v4
.end method

.method private writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "writeFile"    # Landroid/util/AtomicFile;
    .param p2, "outStream"    # Ljava/io/FileOutputStream;
    .param p3, "out"    # Lcom/android/modules/utils/TypedXmlSerializer;
    .param p4, "attribute"    # Ljava/lang/String;
    .param p5, "tag"    # Ljava/lang/String;
    .param p6, "enable"    # Z

    .line 223
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p3, v0, p5}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 224
    invoke-interface {p3, v0, p4, p6}, Lcom/android/modules/utils/TypedXmlSerializer;->attributeBoolean(Ljava/lang/String;Ljava/lang/String;Z)Lorg/xmlpull/v1/XmlSerializer;

    .line 225
    invoke-interface {p3, v0, p5}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 228
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to write local backup of value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFingerprintCloudController"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private writeFeatureIntToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "writeFile"    # Landroid/util/AtomicFile;
    .param p2, "outStream"    # Ljava/io/FileOutputStream;
    .param p3, "out"    # Lcom/android/modules/utils/TypedXmlSerializer;
    .param p4, "attribute"    # Ljava/lang/String;
    .param p5, "tag"    # Ljava/lang/String;
    .param p6, "value"    # I

    .line 236
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p3, v0, p5}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 237
    invoke-interface {p3, v0, p4, p6}, Lcom/android/modules/utils/TypedXmlSerializer;->attributeInt(Ljava/lang/String;Ljava/lang/String;I)Lorg/xmlpull/v1/XmlSerializer;

    .line 238
    invoke-interface {p3, v0, p5}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    goto :goto_0

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 241
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to write local backup of value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiFingerprintCloudController"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method private writeTagToXml(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "out"    # Lorg/xmlpull/v1/XmlSerializer;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 246
    const/4 v0, 0x0

    invoke-interface {p1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 247
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 248
    invoke-interface {p1, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 249
    return-void
.end method


# virtual methods
.method protected addCloudListener(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;

    .line 318
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsData:Ljava/util/Map;

    invoke-interface {p1, v0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;->onCloudUpdated(JLjava/util/Map;)V

    .line 322
    :cond_0
    return-void
.end method

.method public getFingerprintService(Landroid/content/Context;)Landroid/hardware/fingerprint/IFingerprintService;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 134
    const-string v0, "fingerprint"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/hardware/fingerprint/IFingerprintService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/fingerprint/IFingerprintService;

    move-result-object v0

    return-object v0
.end method
