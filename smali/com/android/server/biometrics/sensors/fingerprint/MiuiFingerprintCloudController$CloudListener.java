public abstract class com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$CloudListener {
	 /* .source "MiuiFingerprintCloudController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "CloudListener" */
} // .end annotation
/* # virtual methods */
public abstract void onCloudUpdated ( Long p0, java.util.Map p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;)V" */
/* } */
} // .end annotation
} // .end method
