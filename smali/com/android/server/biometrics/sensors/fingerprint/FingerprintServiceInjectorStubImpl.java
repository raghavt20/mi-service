public class com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl implements com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStub {
	 /* .source "FingerprintServiceInjectorStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer FINGERPRINT_ACQUIRED_AUTH_BIGDATA;
	 private static final Integer FINGERPRINT_ACQUIRED_ENROLL_BIGDATA;
	 private static final Integer FINGERPRINT_ACQUIRED_FINGER_DOWN;
	 private static final Integer FINGERPRINT_ACQUIRED_INFORE6;
	 private static final Integer FINGERPRINT_ACQUIRED_INFORE7;
	 private static final Integer FINGERPRINT_ACQUIRED_INIT_BIGDATA;
	 private static final Boolean FP_LOCAL_STATISTICS_ENABLED;
	 private static final Integer MISIGHT_FINGERPRINT_TEMPLATELOST_EVENT;
	 private static final Integer MISIGHT_FINGERPRINT_TEMPLATELOST_ID;
	 private static final Integer SCREEN_STATUS_DOZE;
	 private static final Integer SCREEN_STATUS_OFF;
	 private static final Integer SCREEN_STATUS_ON;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData localStatisticsInstance;
	 private Integer mBiometricState;
	 private android.content.Context mContext;
	 private android.hardware.display.DisplayManager mDisplayManager;
	 private android.hardware.fingerprint.FingerprintManager mFingerprintManager;
	 private Integer mScreenStatus;
	 /* # direct methods */
	 public static void $r8$lambda$1TH3fXCZY-nZOsDJy6owVVXMl8s ( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl p0, java.lang.String p1, Integer p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->lambda$recordAuthResult$0(Ljava/lang/String;I)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$fYigozsH_tExrUHktTbK5uKl7BI ( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl p0, Integer p1, Integer p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->lambda$recordAcquiredInfo$1(II)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$iGhhCNVm3xQPNoocZTYlbpQm3ug ( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl p0 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->lambda$initAuthStatistics$2()V */
		 return;
	 } // .end method
	 static android.hardware.fingerprint.FingerprintManager -$$Nest$fgetmFingerprintManager ( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mFingerprintManager;
	 } // .end method
	 static void -$$Nest$fputmBiometricState ( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl p0, Integer p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iput p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mBiometricState:I */
		 return;
	 } // .end method
	 static com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl ( ) {
		 /* .locals 2 */
		 /* .line 54 */
		 final String v0 = "persist.vendor.sys.fp.onetrack.enable"; // const-string v0, "persist.vendor.sys.fp.onetrack.enable"
		 int v1 = 1; // const/4 v1, 0x1
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl.FP_LOCAL_STATISTICS_ENABLED = (v0!= 0);
		 return;
	 } // .end method
	 public com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 50 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 62 */
		 int v0 = -1; // const/4 v0, -0x1
		 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I */
		 /* .line 71 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mBiometricState:I */
		 return;
	 } // .end method
	 private void lambda$initAuthStatistics$2 ( ) { //synthethic
		 /* .locals 2 */
		 /* .line 173 */
		 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
		 v1 = this.mContext;
		 (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v0 ).initLocalStatistics ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initLocalStatistics(Landroid/content/Context;)V
		 /* .line 174 */
		 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData .getInstance ( );
		 v1 = this.mContext;
		 (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData ) v0 ).setContext ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->setContext(Landroid/content/Context;)V
		 /* .line 175 */
		 return;
	 } // .end method
	 private void lambda$recordAcquiredInfo$1 ( Integer p0, Integer p1 ) { //synthethic
		 /* .locals 3 */
		 /* .param p1, "acquiredInfo" # I */
		 /* .param p2, "vendorCode" # I */
		 /* .line 132 */
		 /* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mBiometricState:I */
		 int v1 = 2; // const/4 v1, 0x2
		 /* if-eq v0, v1, :cond_0 */
		 int v1 = 3; // const/4 v1, 0x3
		 /* if-eq v0, v1, :cond_0 */
		 int v1 = 4; // const/4 v1, 0x4
		 /* if-ne v0, v1, :cond_1 */
		 /* .line 133 */
	 } // :cond_0
	 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData .getInstance ( );
	 /* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I */
	 v0 = 	 (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintFailReasonData ) v0 ).calculateFailReasonCnt ( p1, p2, v1 ); // invoke-virtual {v0, p1, p2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->calculateFailReasonCnt(III)Z
	 /* .line 134 */
	 /* .local v0, "result":Z */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 135 */
		 com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
		 final String v2 = "fail_reason_info"; // const-string v2, "fail_reason_info"
		 (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v1 ).updataLocalStatistics ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V
		 /* .line 139 */
	 } // .end local v0 # "result":Z
} // :cond_1
int v0 = 6; // const/4 v0, 0x6
/* if-eq p1, v0, :cond_2 */
int v0 = 7; // const/4 v0, 0x7
/* if-ne p1, v0, :cond_3 */
/* .line 140 */
} // :cond_2
final String v0 = "FingerprintServiceInjectorStubImpl"; // const-string v0, "FingerprintServiceInjectorStubImpl"
/* packed-switch p2, :pswitch_data_0 */
/* .line 152 */
/* :pswitch_0 */
final String v1 = "calculateHalEnrollInfo"; // const-string v1, "calculateHalEnrollInfo"
android.util.Slog .d ( v0,v1 );
/* .line 153 */
/* .line 145 */
/* :pswitch_1 */
final String v1 = "calculateHalAuthInfo"; // const-string v1, "calculateHalAuthInfo"
android.util.Slog .d ( v0,v1 );
/* .line 146 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintHalAuthData ) v0 ).calculateHalAuthInfo ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->calculateHalAuthInfo()Z
/* .line 147 */
/* .restart local v0 # "result":Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 148 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
/* const-string/jumbo v2, "unlock_hal_info" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v1 ).updataLocalStatistics ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V
/* .line 142 */
} // .end local v0 # "result":Z
/* :pswitch_2 */
final String v1 = "calculateHalInitInfo"; // const-string v1, "calculateHalInitInfo"
android.util.Slog .d ( v0,v1 );
/* .line 143 */
/* nop */
/* .line 161 */
} // :cond_3
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0xc8 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void lambda$recordAuthResult$0 ( java.lang.String p0, Integer p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "packName" # Ljava/lang/String; */
/* .param p2, "authen" # I */
/* .line 110 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData .getInstance ( );
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData ) v0 ).calculateUnlockCnt ( p1, p2, v1 ); // invoke-virtual {v0, p1, p2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->calculateUnlockCnt(Ljava/lang/String;II)V
/* .line 111 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
/* const-string/jumbo v1, "unlock_rate_info" */
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v0 ).updataLocalStatistics ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V
/* .line 112 */
return;
} // .end method
static void lambda$recordFpTypeAndEnrolledCount$3 ( Integer p0, Integer p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "unlockType" # I */
/* .param p1, "count" # I */
/* .line 222 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v0 ).recordFpTypeAndEnrolledCount ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->recordFpTypeAndEnrolledCount(II)V
/* .line 223 */
return;
} // .end method
private void miSightEventReport ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "event" # I */
/* .param p2, "param" # I */
/* .line 231 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 238 */
final String v0 = "FingerprintServiceInjectorStubImpl"; // const-string v0, "FingerprintServiceInjectorStubImpl"
/* const-string/jumbo v1, "unknow mi sight event" */
android.util.Slog .w ( v0,v1 );
/* .line 239 */
return;
/* .line 233 */
/* :pswitch_0 */
/* new-instance v0, Lcom/miui/misight/MiEvent; */
/* const v1, 0x367a8c6b */
/* invoke-direct {v0, v1}, Lcom/miui/misight/MiEvent;-><init>(I)V */
/* .line 234 */
/* .local v0, "miEvent":Lcom/miui/misight/MiEvent; */
final String v1 = "FingerprintTemplateLost"; // const-string v1, "FingerprintTemplateLost"
(( com.miui.misight.MiEvent ) v0 ).addInt ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lcom/miui/misight/MiEvent;->addInt(Ljava/lang/String;I)Lcom/miui/misight/MiEvent;
/* .line 235 */
com.miui.misight.MiSight .sendEvent ( v0 );
/* .line 236 */
/* nop */
/* .line 241 */
} // .end local v0 # "miEvent":Lcom/miui/misight/MiEvent;
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void updataScreenStatus ( ) {
/* .locals 5 */
/* .line 78 */
v0 = this.mDisplayManager;
int v1 = 0; // const/4 v1, 0x0
(( android.hardware.display.DisplayManager ) v0 ).getDisplay ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getState ( ); // invoke-virtual {v0}, Landroid/view/Display;->getState()I
/* .line 79 */
/* .local v0, "state":I */
int v2 = 1; // const/4 v2, 0x1
final String v3 = "FingerprintServiceInjectorStubImpl"; // const-string v3, "FingerprintServiceInjectorStubImpl"
int v4 = 2; // const/4 v4, 0x2
/* if-ne v0, v4, :cond_0 */
/* .line 80 */
/* iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I */
/* .line 81 */
final String v1 = "screen on when finger down"; // const-string v1, "screen on when finger down"
android.util.Slog .d ( v3,v1 );
/* .line 82 */
} // :cond_0
/* if-ne v0, v2, :cond_1 */
/* .line 83 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I */
/* .line 84 */
final String v1 = "screen off when finger down"; // const-string v1, "screen off when finger down"
android.util.Slog .d ( v3,v1 );
/* .line 85 */
} // :cond_1
int v1 = 3; // const/4 v1, 0x3
/* if-eq v0, v1, :cond_2 */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v0, v1, :cond_3 */
/* .line 86 */
} // :cond_2
/* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I */
/* .line 87 */
final String v1 = "screen doze when finger down"; // const-string v1, "screen doze when finger down"
android.util.Slog .d ( v3,v1 );
/* .line 89 */
} // :cond_3
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updataScreenStatus, state: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", mScreenStatus: "; // const-string v2, ", mScreenStatus: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v1 );
/* .line 90 */
return;
} // .end method
/* # virtual methods */
public void handleUnknowTemplateCleanup ( com.android.server.biometrics.sensors.BaseClientMonitor p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .param p2, "unknownTemplateCount" # I */
/* .line 246 */
v0 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p1 ).statsModality ( ); // invoke-virtual {p1}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->statsModality()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* if-lez p2, :cond_0 */
/* .line 247 */
/* invoke-direct {p0, v1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->miSightEventReport(II)V */
/* .line 248 */
} // :cond_0
return;
} // .end method
public void initAuthStatistics ( ) {
/* .locals 2 */
/* .line 167 */
final String v0 = "FingerprintServiceInjectorStubImpl"; // const-string v0, "FingerprintServiceInjectorStubImpl"
final String v1 = "initAuthStatistics"; // const-string v1, "initAuthStatistics"
android.util.Slog .w ( v0,v1 );
/* .line 168 */
/* sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z */
/* if-nez v0, :cond_0 */
/* .line 169 */
return;
/* .line 171 */
} // :cond_0
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 177 */
return;
} // .end method
public void initRecordFeature ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 182 */
/* sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z */
/* if-nez v0, :cond_0 */
/* .line 183 */
return;
/* .line 185 */
} // :cond_0
final String v0 = "FingerprintServiceInjectorStubImpl"; // const-string v0, "FingerprintServiceInjectorStubImpl"
final String v1 = "initRecordFeature"; // const-string v1, "initRecordFeature"
android.util.Slog .w ( v0,v1 );
/* .line 186 */
this.mContext = p1;
/* .line 187 */
v0 = this.mDisplayManager;
/* if-nez v0, :cond_1 */
/* .line 188 */
/* const-class v0, Landroid/hardware/display/DisplayManager; */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/display/DisplayManager; */
this.mDisplayManager = v0;
/* .line 191 */
} // :cond_1
v0 = this.mContext;
/* const-class v1, Landroid/hardware/fingerprint/FingerprintManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/fingerprint/FingerprintManager; */
this.mFingerprintManager = v0;
/* .line 192 */
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;)V */
(( android.hardware.fingerprint.FingerprintManager ) v0 ).addAuthenticatorsRegisteredCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->addAuthenticatorsRegisteredCallback(Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback;)V
/* .line 210 */
return;
} // .end method
public void recordAcquiredInfo ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .line 118 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "recordAcquiredInfo, acquiredInfo:"; // const-string v1, "recordAcquiredInfo, acquiredInfo:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", vendorCode:"; // const-string v1, ", vendorCode:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintServiceInjectorStubImpl"; // const-string v1, "FingerprintServiceInjectorStubImpl"
android.util.Slog .w ( v1,v0 );
/* .line 119 */
/* sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z */
/* if-nez v0, :cond_0 */
/* .line 120 */
return;
/* .line 123 */
} // :cond_0
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
v1 = this.mContext;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v0 ).initLocalStatistics ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initLocalStatistics(Landroid/content/Context;)V
/* .line 125 */
/* const/16 v0, 0x16 */
/* if-ne p2, v0, :cond_1 */
/* .line 126 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->updataScreenStatus()V */
/* .line 129 */
} // :cond_1
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData ) v0 ).handleAcquiredInfo ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->handleAcquiredInfo(II)V
/* .line 130 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintUnlockRateData ) v0 ).handleAcquiredInfo ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->handleAcquiredInfo(II)V
/* .line 131 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;II)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 162 */
return;
} // .end method
public void recordActivityVisible ( ) {
/* .locals 0 */
/* .line 228 */
return;
} // .end method
public void recordAuthResult ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "packName" # Ljava/lang/String; */
/* .param p2, "authen" # I */
/* .line 96 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "recordAuthResult, packName: "; // const-string v1, "recordAuthResult, packName: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", authen"; // const-string v1, ", authen"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintServiceInjectorStubImpl"; // const-string v1, "FingerprintServiceInjectorStubImpl"
android.util.Slog .w ( v1,v0 );
/* .line 97 */
/* sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z */
/* if-nez v0, :cond_0 */
/* .line 98 */
return;
/* .line 101 */
} // :cond_0
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
v1 = this.mContext;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v0 ).initLocalStatistics ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initLocalStatistics(Landroid/content/Context;)V
/* .line 102 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v0 ).startLocalStatisticsOneTrackUpload ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->startLocalStatisticsOneTrackUpload()V
/* .line 104 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData .getInstance ( );
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I */
v0 = (( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintAuthTimeData ) v0 ).calculateAuthTime ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->calculateAuthTime(II)I
/* .line 105 */
/* .local v0, "result":I */
/* if-nez v0, :cond_1 */
/* .line 106 */
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
final String v2 = "auth_time_info"; // const-string v2, "auth_time_info"
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v1 ).updataLocalStatistics ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V
/* .line 109 */
} // :cond_1
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;Ljava/lang/String;I)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 114 */
return;
} // .end method
public void recordFpTypeAndEnrolledCount ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "unlockType" # I */
/* .param p2, "count" # I */
/* .line 214 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "recordFpTypeAndEnrolledCount\uff0cunlockType: "; // const-string v1, "recordFpTypeAndEnrolledCount\uff0cunlockType: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", count: "; // const-string v1, ", count: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintServiceInjectorStubImpl"; // const-string v1, "FingerprintServiceInjectorStubImpl"
android.util.Slog .w ( v1,v0 );
/* .line 215 */
/* sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z */
/* if-nez v0, :cond_0 */
/* .line 216 */
return;
/* .line 219 */
} // :cond_0
com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData .getInstance ( );
v1 = this.mContext;
(( com.android.server.biometrics.sensors.fingerprint.bigdata.FingerprintLocalStatisticsData ) v0 ).initLocalStatistics ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initLocalStatistics(Landroid/content/Context;)V
/* .line 221 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda1;-><init>(II)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 225 */
return;
} // .end method
