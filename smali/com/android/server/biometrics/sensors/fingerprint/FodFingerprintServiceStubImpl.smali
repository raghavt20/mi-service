.class public Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;
.super Ljava/lang/Object;
.source "FodFingerprintServiceStubImpl.java"

# interfaces
.implements Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl$AuthenticationFidoCallback;
    }
.end annotation


# static fields
.field protected static final DEBUG:Z = true

.field protected static final TAG:Ljava/lang/String; = "[FingerprintService]FodFingerprintServiceStubImpl"


# instance fields
.field private final CMD_NOTIFY_FOD_LOWBRIGHTNESS_ALLOW_STATE:I

.field private final CMD_NOTIFY_LOCK_OUT_TO_FOD_ENGINE:I

.field private final CMD_NOTIFY_MONITOR_STATE_TO_FOD_ENGINE:I

.field private final CMD_NOTIFY_TO_SURFACEFLINGER:I

.field private final CODE_PROCESS_CMD:I

.field private final DEFAULT_DISPLAY:I

.field private final DEFAULT_PACKNAME:Ljava/lang/String;

.field private final FOD_LOCATION_PROPERTY:Ljava/lang/String;

.field private final FOD_LOCATION_WH:Ljava/lang/String;

.field private final FOD_LOCATION_WH_PROPERTY:Ljava/lang/String;

.field private final FOD_LOCATION_XY:Ljava/lang/String;

.field private final FOD_LOCATION_XY_PROPERTY:Ljava/lang/String;

.field private final FOD_SERVICE_NAME:Ljava/lang/String;

.field private GXZW_HEIGHT_PRCENT:F

.field private GXZW_ICON_HEIGHT:I

.field private GXZW_ICON_WIDTH:I

.field private GXZW_ICON_X:I

.field private GXZW_ICON_Y:I

.field private GXZW_WIDTH_PRCENT:F

.field private GXZW_X_PRCENT:F

.field private GXZW_Y_PRCENT:F

.field private final HEART_RATE_PACKAGE:Ljava/lang/String;

.field private final INTERFACE_DESCRIPTOR:Ljava/lang/String;

.field protected IS_FOD:Z

.field private IS_FODENGINE_ENABLED:Z

.field protected IS_FOD_LHBM:Z

.field private final IS_FOD_SENSOR_LOCATION_LOW:Z

.field private final KEYGUARD_PACKAGE:Ljava/lang/String;

.field private final MIUI_DEFAULT_RESOLUTION:Ljava/lang/String;

.field private final MIUI_DEFAULT_RESOLUTION_PROPERTY:Ljava/lang/String;

.field private SCREEN_HEIGHT_PHYSICAL:I

.field private SCREEN_HEIGHT_PX:I

.field private SCREEN_WIDTH_PHYSICAL:I

.field private SCREEN_WIDTH_PX:I

.field private final SETTING_PACKAGE:Ljava/lang/String;

.field private final SETTING_VALUE_OFF:I

.field private final SETTING_VALUE_ON:I

.field private final SF_AUTH_START:I

.field private final SF_AUTH_STOP:I

.field private final SF_ENROLL_START:I

.field private final SF_ENROLL_STOP:I

.field private final SF_FINGERPRINT_NONE:I

.field private final SF_HEART_RATE_START:I

.field private final SF_HEART_RATE_STOP:I

.field private final SF_KEYGUARD_DETECT_START:I

.field private final SF_KEYGUARD_DETECT_STOP:I

.field private final TOUCH_AUTHEN:I

.field private final TOUCH_CANCEL:I

.field private final TOUCH_MODE:I

.field private final mFodLock:Ljava/lang/Object;

.field private mFodService:Landroid/os/IBinder;

.field protected mRootUserId:I

.field private mSettingPointerLocationState:I

.field private mSettingShowTapsState:I

.field private mSfPackageType:I

.field private mSfValue:I

.field private mTouchStatus:I


# direct methods
.method public static synthetic $r8$lambda$znTbKHJw79KlSUpwHmPyTIUgWyA(Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->lambda$getFodServ$0()V

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mFodLock:Ljava/lang/Object;

    .line 52
    const-string v0, "android.app.fod.ICallback"

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_SERVICE_NAME:Ljava/lang/String;

    .line 53
    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    .line 54
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CODE_PROCESS_CMD:I

    .line 55
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mRootUserId:I

    .line 57
    const-string v2, "com.mi.health"

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->HEART_RATE_PACKAGE:Ljava/lang/String;

    .line 58
    const-string v2, "com.android.systemui"

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->KEYGUARD_PACKAGE:Ljava/lang/String;

    .line 59
    const-string v2, "com.android.settings"

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SETTING_PACKAGE:Ljava/lang/String;

    .line 61
    const-string v2, ""

    iput-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->DEFAULT_PACKNAME:Ljava/lang/String;

    .line 62
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->TOUCH_AUTHEN:I

    .line 63
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->TOUCH_CANCEL:I

    .line 64
    const/16 v3, 0xa

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->TOUCH_MODE:I

    .line 65
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mTouchStatus:I

    .line 66
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I

    .line 67
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfPackageType:I

    .line 69
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->DEFAULT_DISPLAY:I

    .line 72
    const/16 v4, 0x7987

    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CMD_NOTIFY_TO_SURFACEFLINGER:I

    .line 73
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_FINGERPRINT_NONE:I

    .line 74
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_ENROLL_START:I

    .line 75
    const/4 v4, 0x2

    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_ENROLL_STOP:I

    .line 76
    const/4 v4, 0x3

    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_AUTH_START:I

    .line 77
    const/4 v4, 0x4

    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_AUTH_STOP:I

    .line 78
    const/4 v5, 0x5

    iput v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_HEART_RATE_START:I

    .line 79
    const/4 v6, 0x6

    iput v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_HEART_RATE_STOP:I

    .line 80
    const/4 v6, 0x7

    iput v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_KEYGUARD_DETECT_START:I

    .line 81
    const/16 v7, 0x8

    iput v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_KEYGUARD_DETECT_STOP:I

    .line 83
    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CMD_NOTIFY_MONITOR_STATE_TO_FOD_ENGINE:I

    .line 84
    iput v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CMD_NOTIFY_LOCK_OUT_TO_FOD_ENGINE:I

    .line 85
    iput v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CMD_NOTIFY_FOD_LOWBRIGHTNESS_ALLOW_STATE:I

    .line 87
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SETTING_VALUE_ON:I

    .line 88
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SETTING_VALUE_OFF:I

    .line 89
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingShowTapsState:I

    .line 90
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingPointerLocationState:I

    .line 92
    const-string v0, "ro.hardware.fp.fod"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD:Z

    .line 94
    const-string v0, "ro.hardware.fp.fod.location"

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_PROPERTY:Ljava/lang/String;

    .line 95
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "low"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD_SENSOR_LOCATION_LOW:Z

    .line 97
    const-string v0, "persist.vendor.sys.fp.fod.location.X_Y"

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_XY_PROPERTY:Ljava/lang/String;

    .line 98
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_XY:Ljava/lang/String;

    .line 100
    const-string v0, "persist.vendor.sys.fp.fod.size.width_height"

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_WH_PROPERTY:Ljava/lang/String;

    .line 101
    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_WH:Ljava/lang/String;

    .line 103
    const-string v0, "ro.vendor.localhbm.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD_LHBM:Z

    .line 104
    const-string v0, "ro.hardware.fp.fod.touch.ctl.version"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "2.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FODENGINE_ENABLED:Z

    .line 106
    const-string v0, "persist.sys.miui_default_resolution"

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->MIUI_DEFAULT_RESOLUTION_PROPERTY:Ljava/lang/String;

    .line 107
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->MIUI_DEFAULT_RESOLUTION:Ljava/lang/String;

    .line 110
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I

    .line 111
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I

    .line 114
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_X_PRCENT:F

    .line 115
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_Y_PRCENT:F

    .line 116
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_WIDTH_PRCENT:F

    .line 117
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_HEIGHT_PRCENT:F

    .line 118
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PX:I

    .line 119
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PX:I

    .line 122
    const/16 v0, 0x1c5

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I

    .line 123
    const/16 v0, 0x668

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I

    .line 124
    const/16 v0, 0xad

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I

    .line 125
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I

    return-void
.end method

.method private cmdSendToKeyguard(ILjava/lang/String;)I
    .locals 3
    .param p1, "cmd"    # I
    .param p2, "packName"    # Ljava/lang/String;

    .line 136
    const/4 v0, 0x2

    const/4 v1, 0x1

    const-string v2, "com.android.settings"

    packed-switch p1, :pswitch_data_0

    .line 152
    :pswitch_0
    return p1

    .line 140
    :pswitch_1
    return v0

    .line 138
    :pswitch_2
    return v1

    .line 147
    :pswitch_3
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    return v0

    .line 150
    :cond_0
    return p1

    .line 142
    :pswitch_4
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    return v1

    .line 145
    :cond_1
    return p1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getCmd4SurfaceFlinger(IILcom/android/server/biometrics/sensors/BaseClientMonitor;)I
    .locals 3
    .param p1, "cmd"    # I
    .param p2, "param"    # I
    .param p3, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    .line 390
    const/4 v0, -0x1

    .line 391
    .local v0, "res":I
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 437
    :sswitch_0
    const/4 v0, 0x6

    .line 438
    goto :goto_0

    .line 433
    :sswitch_1
    const/4 v0, 0x5

    .line 434
    goto :goto_0

    .line 443
    :sswitch_2
    const/16 v0, 0x8

    .line 444
    goto :goto_0

    .line 440
    :sswitch_3
    const/4 v0, 0x7

    .line 441
    goto :goto_0

    .line 396
    :sswitch_4
    if-nez p2, :cond_0

    .line 397
    const/4 v0, 0x2

    goto :goto_0

    .line 399
    :cond_0
    const/4 v0, 0x1

    .line 401
    goto :goto_0

    .line 427
    :sswitch_5
    const/4 v0, 0x2

    .line 428
    goto :goto_0

    .line 393
    :sswitch_6
    const/4 v0, 0x1

    .line 394
    goto :goto_0

    .line 430
    :sswitch_7
    const/4 v0, 0x0

    .line 431
    goto :goto_0

    .line 403
    :sswitch_8
    if-eqz p3, :cond_3

    .line 404
    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getStatsAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 405
    const/4 v0, 0x2

    .line 407
    :cond_1
    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getStatsAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 408
    const/4 v0, 0x4

    goto :goto_0

    .line 416
    :sswitch_9
    if-nez p2, :cond_2

    .line 417
    const/4 v0, 0x3

    goto :goto_0

    .line 419
    :cond_2
    const/4 v0, 0x4

    .line 421
    goto :goto_0

    .line 424
    :sswitch_a
    const/4 v0, 0x4

    .line 425
    goto :goto_0

    .line 413
    :sswitch_b
    const/4 v0, 0x3

    .line 414
    nop

    .line 448
    :cond_3
    :goto_0
    return v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_b
        0x2 -> :sswitch_a
        0x3 -> :sswitch_9
        0x4 -> :sswitch_8
        0x5 -> :sswitch_7
        0x6 -> :sswitch_a
        0x7 -> :sswitch_6
        0x8 -> :sswitch_5
        0x9 -> :sswitch_4
        0xc -> :sswitch_3
        0xd -> :sswitch_2
        0x61a81 -> :sswitch_1
        0x61a84 -> :sswitch_0
        0x61a86 -> :sswitch_0
    .end sparse-switch
.end method

.method private isFodMonitorState(I)Z
    .locals 1
    .param p1, "sf_status"    # I

    .line 375
    const/4 v0, 0x0

    .line 377
    .local v0, "result":Z
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 381
    :pswitch_1
    const/4 v0, 0x1

    .line 382
    nop

    .line 386
    :goto_0
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private synthetic lambda$getFodServ$0()V
    .locals 3

    .line 502
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mFodLock:Ljava/lang/Object;

    monitor-enter v0

    .line 503
    :try_start_0
    const-string v1, "[FingerprintService]FodFingerprintServiceStubImpl"

    const-string v2, "fodCallBack Service Died."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mFodService:Landroid/os/IBinder;

    .line 505
    monitor-exit v0

    .line 506
    return-void

    .line 505
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private notifySurfaceFlinger(Landroid/content/Context;ILjava/lang/String;II)I
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "msg"    # I
    .param p3, "packName"    # Ljava/lang/String;
    .param p4, "value"    # I
    .param p5, "cmd"    # I

    .line 452
    const/4 v0, -0x1

    .line 453
    .local v0, "resBack":I
    const/4 v1, 0x0

    .line 455
    .local v1, "packageType":I
    iget-boolean v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD:Z

    if-nez v2, :cond_0

    .line 456
    return v0

    .line 459
    :cond_0
    invoke-virtual {p0, p3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isKeyguard(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    if-ne p5, v2, :cond_1

    .line 460
    const/4 v1, 0x1

    .line 463
    :cond_1
    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I

    const-string v3, ", packageType: "

    const-string v4, ", value: "

    const-string v5, "[FingerprintService]FodFingerprintServiceStubImpl"

    if-ne v2, p4, :cond_2

    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfPackageType:I

    if-ne v2, v1, :cond_2

    .line 465
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "duplicate notifySurfaceFlinger msg: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    return v0

    .line 469
    :cond_2
    const-string v2, "SurfaceFlinger"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    .line 470
    .local v2, "flinger":Landroid/os/IBinder;
    if-eqz v2, :cond_3

    .line 471
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    .line 472
    .local v6, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    .line 473
    .local v7, "reply":Landroid/os/Parcel;
    const-string v8, "android.ui.ISurfaceComposer"

    invoke-virtual {v6, v8}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 474
    invoke-virtual {v6, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 475
    invoke-virtual {v6, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 477
    const/4 v8, 0x0

    :try_start_0
    invoke-interface {v2, p2, v6, v7, v8}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 478
    invoke-virtual {v7}, Landroid/os/Parcel;->readException()V

    .line 479
    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v8
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v8

    .line 483
    nop

    :goto_0
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    .line 484
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    .line 485
    goto :goto_2

    .line 483
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 480
    :catch_0
    move-exception v8

    .line 481
    .local v8, "ex":Landroid/os/RemoteException;
    :try_start_1
    const-string v9, "Failed to notifySurfaceFlinger"

    invoke-static {v5, v9, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483
    nop

    .end local v8    # "ex":Landroid/os/RemoteException;
    goto :goto_0

    :goto_1
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    .line 484
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    .line 485
    throw v3

    .line 488
    .end local v6    # "data":Landroid/os/Parcel;
    .end local v7    # "reply":Landroid/os/Parcel;
    :cond_3
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notifySurfaceFlinger msg: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    iput p4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I

    .line 491
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfPackageType:I

    .line 493
    return v0
.end method

.method private resetDefaultValue()V
    .locals 1

    .line 695
    const/16 v0, 0x1c5

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I

    .line 696
    const/16 v0, 0x668

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I

    .line 697
    const/16 v0, 0xad

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I

    .line 698
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I

    .line 699
    return-void
.end method

.method private screenWhPx(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 686
    const-string v0, "display"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 687
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 688
    .local v1, "dm":Landroid/util/DisplayMetrics;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 689
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    move v2, v4

    .line 690
    .local v2, "orientation":Z
    :cond_0
    if-eqz v2, :cond_1

    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0

    :cond_1
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_0
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PX:I

    .line 691
    if-eqz v2, :cond_2

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_1

    :cond_2
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_1
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PX:I

    .line 692
    return-void
.end method


# virtual methods
.method public calculateFodSensorLocation(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .line 609
    const-string v0, ","

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 610
    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->phySicalScreenPx(Landroid/content/Context;)V

    .line 612
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->screenWhPx(Landroid/content/Context;)V

    .line 614
    const-string v1, "persist.vendor.sys.fp.fod.location.X_Y"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 615
    .local v1, "xyString":Ljava/lang/String;
    const-string v3, "persist.vendor.sys.fp.fod.size.width_height"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 624
    .local v2, "whString":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    goto/16 :goto_1

    .line 630
    :cond_1
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I

    .line 631
    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    aget-object v3, v3, v5

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I

    .line 632
    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I

    .line 633
    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I

    .line 641
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I

    iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I

    invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getPrcent(II)F

    move-result v0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_X_PRCENT:F

    .line 642
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I

    iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I

    invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getPrcent(II)F

    move-result v0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_Y_PRCENT:F

    .line 643
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I

    iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I

    invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getPrcent(II)F

    move-result v0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_WIDTH_PRCENT:F

    .line 644
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I

    iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I

    invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getPrcent(II)F

    move-result v0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_HEIGHT_PRCENT:F

    .line 646
    iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PX:I

    int-to-float v4, v3

    iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_X_PRCENT:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I

    .line 647
    iget v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PX:I

    int-to-float v5, v4

    iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_Y_PRCENT:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I

    .line 648
    int-to-float v3, v3

    iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_WIDTH_PRCENT:F

    mul-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I

    .line 649
    int-to-float v3, v4

    mul-float/2addr v3, v0

    float-to-int v0, v3

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 653
    goto :goto_0

    .line 650
    :catch_0
    move-exception v0

    .line 651
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 652
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->resetDefaultValue()V

    .line 654
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 625
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->resetDefaultValue()V

    .line 626
    return-void
.end method

.method public fodCallBack(Landroid/content/Context;IILcom/android/server/biometrics/sensors/BaseClientMonitor;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cmd"    # I
    .param p3, "param"    # I
    .param p4, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    .line 158
    if-eqz p4, :cond_0

    .line 159
    invoke-virtual {p4}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getOwnerString()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I

    move-result v0

    return v0

    .line 162
    :cond_0
    const-string v5, ""

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I

    move-result v0

    return v0
.end method

.method public declared-synchronized fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cmd"    # I
    .param p3, "param"    # I
    .param p4, "packName"    # Ljava/lang/String;
    .param p5, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    move-object/from16 v7, p0

    move/from16 v8, p2

    move/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    monitor-enter p0

    .line 167
    const/4 v12, -0x1

    .line 169
    .local v12, "resBack":I
    :try_start_0
    const-string v0, "com.mi.health"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD:Z

    if-eqz v0, :cond_0

    .line 170
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerprintClient(Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    .line 171
    .end local p0    # "this":Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;
    :cond_0
    monitor-exit p0

    return v12

    .line 174
    :cond_1
    :try_start_1
    invoke-virtual {v7, v8, v9}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getCmd4Touch(II)I

    move-result v0

    move v13, v0

    .line 175
    .local v13, "cmdSendToTouch":I
    invoke-direct {v7, v8, v9, v11}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getCmd4SurfaceFlinger(IILcom/android/server/biometrics/sensors/BaseClientMonitor;)I

    move-result v0

    move v14, v0

    .line 176
    .local v14, "cmdSendToSf":I
    const/4 v0, 0x0

    .line 177
    .local v0, "touchFod":Z
    const/4 v15, 0x0

    .line 181
    .local v15, "alreadySetTouch":Z
    iget-boolean v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FODENGINE_ENABLED:Z

    const/4 v2, 0x4

    const/4 v6, 0x5

    if-nez v1, :cond_2

    if-ne v8, v2, :cond_2

    if-ne v9, v6, :cond_2

    const-string v1, "com.android.settings"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v1, :cond_2

    .line 182
    monitor-exit p0

    return v12

    .line 185
    :cond_2
    :try_start_2
    iget-boolean v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FODENGINE_ENABLED:Z

    const/4 v5, 0x6

    const/4 v4, 0x3

    const/4 v3, -0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_a

    .line 186
    if-eq v14, v3, :cond_9

    .line 187
    iget v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I

    if-ne v1, v4, :cond_4

    if-eq v14, v6, :cond_3

    if-ne v14, v5, :cond_4

    .line 188
    :cond_3
    const-string v1, "[FingerprintService]FodFingerprintServiceStubImpl"

    const-string/jumbo v2, "skip notify heart rate as fingerprint auth start already"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 189
    monitor-exit p0

    return v12

    .line 192
    :cond_4
    const/16 v1, 0xc

    if-ne v8, v1, :cond_5

    .line 193
    :try_start_3
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    const/4 v1, 0x2

    invoke-static {v6, v1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->startExtCmd(II)I

    goto :goto_0

    .line 194
    :cond_5
    const/16 v1, 0xd

    if-ne v8, v1, :cond_6

    .line 195
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    invoke-static {v6, v2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->startExtCmd(II)I

    .line 198
    :cond_6
    :goto_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    const/4 v1, 0x4

    invoke-static {v1, v14}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->startExtCmd(II)I

    .line 199
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    invoke-virtual {v7, v10}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isKeyguard(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v3, 0x1

    if-ne v8, v3, :cond_8

    move v1, v3

    goto :goto_1

    :cond_7
    const/4 v3, 0x1

    :cond_8
    move v1, v2

    :goto_1
    const/4 v5, 0x7

    invoke-static {v5, v1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->startExtCmd(II)I

    .line 200
    const/16 v5, 0x7987

    move-object/from16 v1, p0

    move v6, v2

    move-object/from16 v2, p1

    move/from16 v16, v0

    move v0, v3

    .end local v0    # "touchFod":Z
    .local v16, "touchFod":Z
    move v3, v5

    move v5, v4

    move-object/from16 v4, p4

    move v0, v5

    move v5, v14

    move v0, v6

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->notifySurfaceFlinger(Landroid/content/Context;ILjava/lang/String;II)I

    goto :goto_2

    .line 186
    .end local v16    # "touchFod":Z
    .restart local v0    # "touchFod":Z
    :cond_9
    move/from16 v16, v0

    move v0, v2

    .line 224
    .end local v0    # "touchFod":Z
    .restart local v16    # "touchFod":Z
    :goto_2
    move/from16 v1, v16

    goto/16 :goto_8

    .line 204
    .end local v16    # "touchFod":Z
    .restart local v0    # "touchFod":Z
    :cond_a
    move/from16 v16, v0

    move v0, v2

    .end local v0    # "touchFod":Z
    .restart local v16    # "touchFod":Z
    const-string v1, ""

    invoke-virtual {v1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const/16 v4, 0xa

    if-nez v1, :cond_c

    invoke-virtual {v7, v10}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isKeyguard(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    goto :goto_3

    :cond_b
    move v0, v6

    goto :goto_6

    .line 205
    :cond_c
    :goto_3
    if-eq v13, v3, :cond_d

    invoke-virtual {v7, v0, v4, v13}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->setTouchMode(III)Z

    move-result v2

    goto :goto_4

    :cond_d
    move v2, v0

    :goto_4
    move/from16 v16, v2

    .line 206
    const/4 v15, 0x1

    .line 207
    if-eqz v16, :cond_e

    move v1, v13

    goto :goto_5

    :cond_e
    iget v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mTouchStatus:I

    :goto_5
    iput v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mTouchStatus:I

    .line 208
    if-eq v14, v3, :cond_f

    .line 209
    const/16 v17, 0x7987

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move v0, v3

    move/from16 v3, v17

    move-object/from16 v4, p4

    move v0, v5

    move v5, v14

    move v0, v6

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->notifySurfaceFlinger(Landroid/content/Context;ILjava/lang/String;II)I

    goto :goto_6

    .line 208
    :cond_f
    move v0, v6

    .line 212
    :goto_6
    iget v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_11

    if-eq v14, v0, :cond_10

    const/4 v0, 0x6

    if-ne v14, v0, :cond_11

    .line 213
    :cond_10
    const-string v0, "[FingerprintService]FodFingerprintServiceStubImpl"

    const-string/jumbo v1, "skip notify heart rate as fingerprint auth start already"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 214
    monitor-exit p0

    return v12

    .line 216
    :cond_11
    if-nez v15, :cond_14

    .line 217
    const/4 v0, -0x1

    if-eq v13, v0, :cond_12

    const/16 v0, 0xa

    const/4 v1, 0x0

    :try_start_4
    invoke-virtual {v7, v1, v0, v13}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->setTouchMode(III)Z

    move-result v2

    goto :goto_7

    :cond_12
    const/4 v2, 0x0

    :goto_7
    move v0, v2

    .line 218
    .end local v16    # "touchFod":Z
    .restart local v0    # "touchFod":Z
    const/4 v1, -0x1

    if-eq v14, v1, :cond_13

    .line 219
    const/16 v3, 0x7987

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p4

    move v5, v14

    move/from16 v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->notifySurfaceFlinger(Landroid/content/Context;ILjava/lang/String;II)I

    .line 224
    :cond_13
    move v1, v0

    goto :goto_8

    .line 216
    .end local v0    # "touchFod":Z
    .restart local v16    # "touchFod":Z
    :cond_14
    move/from16 v1, v16

    .line 224
    .end local v16    # "touchFod":Z
    .local v1, "touchFod":Z
    :goto_8
    iget-boolean v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD_LHBM:Z

    if-nez v0, :cond_18

    .line 225
    invoke-direct {v7, v14}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isFodMonitorState(I)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x3

    if-eq v8, v0, :cond_16

    const/16 v0, 0x9

    if-eq v8, v0, :cond_16

    .line 226
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "show_touches"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingShowTapsState:I

    .line 227
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "pointer_location"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingPointerLocationState:I

    .line 229
    iget v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingShowTapsState:I

    if-eqz v0, :cond_15

    .line 230
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "show_touches"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 231
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "show_touches_preventrecord"

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 234
    :cond_15
    iget v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingPointerLocationState:I

    if-eqz v0, :cond_18

    .line 235
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "pointer_location"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_9

    .line 236
    :cond_16
    invoke-direct {v7, v14}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isFodMonitorState(I)Z

    move-result v0

    if-nez v0, :cond_18

    .line 237
    iget v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingShowTapsState:I

    if-eqz v0, :cond_17

    .line 238
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "show_touches"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 239
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "show_touches_preventrecord"

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 242
    :cond_17
    iget v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingPointerLocationState:I

    if-eqz v0, :cond_18

    .line 243
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "pointer_location"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 247
    :cond_18
    :goto_9
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    move-object v2, v0

    .line 248
    .local v2, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v3, v0

    .line 250
    .local v3, "reply":Landroid/os/Parcel;
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getFodServ()Landroid/os/IBinder;

    move-result-object v0

    .line 251
    .local v0, "remote":Landroid/os/IBinder;
    if-nez v0, :cond_19

    .line 252
    const-string v4, "[FingerprintService]FodFingerprintServiceStubImpl"

    const-string v5, "fodCallBack, service not found"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 254
    :cond_19
    const-string v4, "android.app.fod.ICallback"

    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 255
    invoke-direct {v7, v8, v10}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->cmdSendToKeyguard(ILjava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 256
    invoke-virtual {v2, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 257
    invoke-virtual {v2, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 258
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v0, v4, v2, v3, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 259
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V

    .line 260
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v4
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v12, v4

    .line 265
    .end local v0    # "remote":Landroid/os/IBinder;
    :goto_a
    :try_start_6
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 266
    :goto_b
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 267
    goto :goto_c

    .line 265
    :catchall_0
    move-exception v0

    goto :goto_d

    .line 262
    :catch_0
    move-exception v0

    .line 263
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_7
    const-string v4, "[FingerprintService]FodFingerprintServiceStubImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fodCallBack failed, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 265
    .end local v0    # "e":Landroid/os/RemoteException;
    :try_start_8
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_b

    .line 268
    :goto_c
    const-string v0, "[FingerprintService]FodFingerprintServiceStubImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fodCallBack cmd: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v7, v8}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getCmdStr(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " backRes:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cmdSendToTouch:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cmdSendToSf:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " writeRes:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 271
    monitor-exit p0

    return v12

    .line 265
    :goto_d
    :try_start_9
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 266
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 267
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 166
    .end local v1    # "touchFod":Z
    .end local v2    # "data":Landroid/os/Parcel;
    .end local v3    # "reply":Landroid/os/Parcel;
    .end local v12    # "resBack":I
    .end local v13    # "cmdSendToTouch":I
    .end local v14    # "cmdSendToSf":I
    .end local v15    # "alreadySetTouch":Z
    .end local p1    # "context":Landroid/content/Context;
    .end local p2    # "cmd":I
    .end local p3    # "param":I
    .end local p4    # "packName":Ljava/lang/String;
    .end local p5    # "client":Lcom/android/server/biometrics/sensors/BaseClientMonitor;
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCmd4Touch(II)I
    .locals 1
    .param p1, "cmd"    # I
    .param p2, "param"    # I

    .line 334
    const/4 v0, -0x1

    .line 335
    .local v0, "res":I
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 365
    :sswitch_0
    const/4 v0, 0x0

    .line 366
    goto :goto_0

    .line 361
    :sswitch_1
    const/4 v0, 0x1

    .line 362
    goto :goto_0

    .line 342
    :sswitch_2
    if-nez p2, :cond_0

    .line 343
    const/4 v0, 0x0

    goto :goto_0

    .line 347
    :sswitch_3
    if-eqz p2, :cond_0

    .line 348
    const/4 v0, 0x0

    goto :goto_0

    .line 358
    :sswitch_4
    const/4 v0, 0x0

    .line 359
    goto :goto_0

    .line 339
    :sswitch_5
    const/4 v0, 0x1

    .line 340
    nop

    .line 370
    :cond_0
    :goto_0
    return v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_4
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_4
        0x6 -> :sswitch_4
        0x7 -> :sswitch_5
        0x8 -> :sswitch_4
        0x9 -> :sswitch_2
        0xb -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_4
        0x61a81 -> :sswitch_1
        0x61a84 -> :sswitch_0
        0x61a86 -> :sswitch_0
    .end sparse-switch
.end method

.method public getCmdStr(I)Ljava/lang/String;
    .locals 1
    .param p1, "cmd"    # I

    .line 514
    const-string/jumbo v0, "unknown"

    .line 515
    .local v0, "res":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 550
    :pswitch_1
    const-string v0, "CMD_KEYGUARD_CANCEL_DETECT"

    .line 551
    goto :goto_0

    .line 547
    :pswitch_2
    const-string v0, "CMD_KEYGUARD_DETECT"

    .line 548
    goto :goto_0

    .line 544
    :pswitch_3
    const-string v0, "CMD_VENDOR_REMOVED"

    .line 545
    goto :goto_0

    .line 541
    :pswitch_4
    const-string v0, "CMD_VENDOR_ENROLL_RES"

    .line 542
    goto :goto_0

    .line 538
    :pswitch_5
    const-string v0, "CMD_APP_CANCEL_ENROLL"

    .line 539
    goto :goto_0

    .line 535
    :pswitch_6
    const-string v0, "CMD_APP_ENROLL"

    .line 536
    goto :goto_0

    .line 532
    :pswitch_7
    const-string v0, "CMD_FW_TOP_APP_CANCEL"

    .line 533
    goto :goto_0

    .line 529
    :pswitch_8
    const-string v0, "CMD_FW_LOCK_CANCEL"

    .line 530
    goto :goto_0

    .line 526
    :pswitch_9
    const-string v0, "CMD_VENDOR_ERROR"

    .line 527
    goto :goto_0

    .line 523
    :pswitch_a
    const-string v0, "CMD_VENDOR_AUTHENTICATED"

    .line 524
    goto :goto_0

    .line 520
    :pswitch_b
    const-string v0, "CMD_APP_CANCEL_AUTHEN"

    .line 521
    goto :goto_0

    .line 517
    :pswitch_c
    const-string v0, "CMD_APP_AUTHEN"

    .line 518
    nop

    .line 555
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public getFodCoordinate([I)Z
    .locals 5
    .param p1, "udfpsProps"    # [I

    .line 287
    array-length v0, p1

    const/4 v1, 0x3

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    .line 288
    const-string v0, "[FingerprintService]FodFingerprintServiceStubImpl"

    const-string/jumbo v1, "wrong udfpsProps length!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    return v2

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_XY:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_XY:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 293
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_XY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    aput v0, p1, v2

    .line 294
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_XY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    aput v0, p1, v3

    .line 296
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_WH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_WH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 297
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_WH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 298
    .local v0, "width":I
    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->FOD_LOCATION_WH:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 300
    .local v1, "height":I
    if-ne v0, v1, :cond_1

    .line 301
    div-int/lit8 v2, v0, 0x2

    const/4 v4, 0x2

    aput v2, p1, v4

    .line 303
    .end local v0    # "width":I
    .end local v1    # "height":I
    :cond_1
    nop

    .line 310
    return v3

    .line 304
    :cond_2
    return v2

    .line 307
    :cond_3
    return v2
.end method

.method public getFodServ()Landroid/os/IBinder;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 497
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mFodLock:Ljava/lang/Object;

    monitor-enter v0

    .line 498
    :try_start_0
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mFodService:Landroid/os/IBinder;

    if-nez v1, :cond_0

    .line 499
    const-string v1, "android.app.fod.ICallback"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mFodService:Landroid/os/IBinder;

    .line 500
    if-eqz v1, :cond_0

    .line 501
    new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;)V

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 509
    :cond_0
    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mFodService:Landroid/os/IBinder;

    monitor-exit v0

    return-object v1

    .line 510
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getIsFod()Z
    .locals 1

    .line 277
    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD:Z

    return v0
.end method

.method public getIsFodLocationLow()Z
    .locals 1

    .line 282
    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD_SENSOR_LOCATION_LOW:Z

    return v0
.end method

.method public getPrcent(II)F
    .locals 4
    .param p1, "xory"    # I
    .param p2, "widthorheight"    # I

    .line 657
    if-eqz p2, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 659
    :cond_0
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p2}, Ljava/math/BigDecimal;-><init>(I)V

    const/16 v2, 0xa

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;II)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F

    move-result v0

    return v0

    .line 658
    :cond_1
    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getSensorLocation(Landroid/content/Context;)[I
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 594
    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->calculateFodSensorLocation(Landroid/content/Context;)V

    .line 596
    const-string v0, "display"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 597
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v2

    .line 598
    .local v2, "display":Landroid/view/Display;
    invoke-virtual {v2}, Landroid/view/Display;->getSupportedModes()[Landroid/view/Display$Mode;

    move-result-object v3

    .line 600
    .local v3, "resolutiones":[Landroid/view/Display$Mode;
    const/4 v4, 0x4

    new-array v4, v4, [I

    .line 601
    .local v4, "mFodLocation":[I
    iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I

    aput v5, v4, v1

    .line 602
    const/4 v1, 0x1

    iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I

    aput v5, v4, v1

    .line 603
    const/4 v1, 0x2

    iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I

    aput v5, v4, v1

    .line 604
    const/4 v1, 0x3

    iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I

    aput v5, v4, v1

    .line 605
    return-object v4
.end method

.method public handleAcquiredInfo(IILcom/android/server/biometrics/sensors/AcquisitionClient;)V
    .locals 3
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I
    .param p3, "client"    # Lcom/android/server/biometrics/sensors/AcquisitionClient;

    .line 580
    :try_start_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerprintClient(Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getIsFod()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 581
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerDownAcquireCode(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AcquisitionClient;->getListener()Lcom/android/server/biometrics/sensors/ClientMonitorCallbackConverter;

    move-result-object v0

    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AcquisitionClient;->getSensorId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/ClientMonitorCallbackConverter;->onUdfpsPointerDown(I)V

    goto :goto_0

    .line 583
    :cond_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerUpAcquireCode(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 584
    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AcquisitionClient;->getListener()Lcom/android/server/biometrics/sensors/ClientMonitorCallbackConverter;

    move-result-object v0

    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AcquisitionClient;->getSensorId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/ClientMonitorCallbackConverter;->onUdfpsPointerUp(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 589
    :cond_1
    :goto_0
    goto :goto_1

    .line 587
    :catch_0
    move-exception v0

    .line 588
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p3, Lcom/android/server/biometrics/sensors/AcquisitionClient;->mCallback:Lcom/android/server/biometrics/sensors/ClientMonitorCallback;

    const/4 v2, 0x0

    invoke-interface {v1, p3, v2}, Lcom/android/server/biometrics/sensors/ClientMonitorCallback;->onClientFinished(Lcom/android/server/biometrics/sensors/BaseClientMonitor;Z)V

    .line 590
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method protected isKeyguard(Ljava/lang/String;)Z
    .locals 1
    .param p1, "clientPackage"    # Ljava/lang/String;

    .line 574
    const-string v0, "com.android.systemui"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public phySicalScreenPx(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 668
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->MIUI_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 669
    const-string v0, "display"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 670
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v1

    .line 671
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getMode()Landroid/view/Display$Mode;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display$Mode;->getPhysicalWidth()I

    move-result v2

    iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I

    .line 672
    invoke-virtual {v1}, Landroid/view/Display;->getMode()Landroid/view/Display$Mode;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display$Mode;->getPhysicalHeight()I

    move-result v2

    iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I

    .line 673
    .end local v0    # "displayManager":Landroid/hardware/display/DisplayManager;
    .end local v1    # "display":Landroid/view/Display;
    goto :goto_0

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->MIUI_DEFAULT_RESOLUTION:Ljava/lang/String;

    const-string/jumbo v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I

    .line 676
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->MIUI_DEFAULT_RESOLUTION:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I

    .line 678
    :goto_0
    return-void
.end method

.method public setCurrentLockoutMode(I)V
    .locals 1
    .param p1, "lockoutMode"    # I

    .line 315
    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FODENGINE_ENABLED:Z

    if-eqz v0, :cond_0

    .line 316
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    const/4 v0, 0x5

    invoke-static {v0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->startExtCmd(II)I

    .line 318
    :cond_0
    return-void
.end method

.method public setTouchMode(III)Z
    .locals 2
    .param p1, "touchId"    # I
    .param p2, "mode"    # I
    .param p3, "value"    # I

    .line 322
    :try_start_0
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v0

    .line 323
    .local v0, "touchFeature":Lmiui/util/ITouchFeature;
    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {v0, p1, p2, p3}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 328
    .end local v0    # "touchFeature":Lmiui/util/ITouchFeature;
    :cond_0
    goto :goto_0

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 329
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v0, 0x0

    return v0
.end method
