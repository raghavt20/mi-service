public class com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStubImpl extends com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub {
	 /* .source "FingerprintServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final Integer CODE_EXT_CMD;
private final java.lang.String EXT_DESCRIPTOR;
private final java.lang.String NAME_EXT_DAEMON;
private Integer lockFlag;
private android.os.IHwBinder mExtDaemon;
android.hardware.fingerprint.Fingerprint mFingerIdentifer;
java.util.ArrayList mFingerToken;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
protected Integer mLockoutMode;
public Long mOpId;
public java.lang.String mOpPackage;
/* # direct methods */
public com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStubImpl ( ) {
/* .locals 2 */
/* .line 41 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;-><init>()V */
/* .line 43 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpId:J */
/* .line 44 */
final String v0 = ""; // const-string v0, ""
this.mOpPackage = v0;
/* .line 49 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mLockoutMode:I */
/* .line 53 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I */
/* .line 56 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.fingerprintextension@1.0::IXiaomiFingerprint" */
this.NAME_EXT_DAEMON = v0;
/* .line 57 */
this.EXT_DESCRIPTOR = v0;
/* .line 58 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->CODE_EXT_CMD:I */
return;
} // .end method
/* # virtual methods */
public void clearSavedAuthenResult ( ) {
/* .locals 1 */
/* .line 110 */
v0 = com.android.server.biometrics.sensors.fingerprint.PowerFingerprintServiceStub .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 111 */
int v0 = 0; // const/4 v0, 0x0
this.mFingerIdentifer = v0;
/* .line 112 */
this.mFingerToken = v0;
/* .line 114 */
} // :cond_0
return;
} // .end method
public Integer getCurrentLockoutMode ( ) {
/* .locals 1 */
/* .line 181 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mLockoutMode:I */
} // .end method
public android.hardware.fingerprint.Fingerprint getIdentifier ( ) {
/* .locals 1 */
/* .line 101 */
v0 = this.mFingerIdentifer;
} // .end method
public Integer getMiuiGroupId ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .line 164 */
/* move v0, p1 */
/* .line 166 */
/* .local v0, "groupId":I */
if ( p1 != null) { // if-eqz p1, :cond_0
v1 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStubImpl ) p0 ).isFingerDataSharer ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->isFingerDataSharer(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 167 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 170 */
} // :cond_0
} // .end method
public Long getOpId ( ) {
/* .locals 2 */
/* .line 83 */
/* iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpId:J */
/* .line 84 */
/* .local v0, "opId":J */
/* return-wide v0 */
} // .end method
public java.lang.String getOpPackageName ( ) {
/* .locals 1 */
/* .line 89 */
v0 = this.mOpPackage;
/* .line 90 */
/* .local v0, "opPackage":Ljava/lang/String; */
} // .end method
public java.util.ArrayList getToken ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 105 */
v0 = this.mFingerToken;
} // .end method
public Integer getcurrentUserId ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "groupId" # I */
/* .line 160 */
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStubImpl ) p0 ).getMiuiGroupId ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->getMiuiGroupId(I)I
} // .end method
public void handleAcquiredInfo ( Integer p0, Integer p1, com.android.server.biometrics.sensors.AuthenticationClient p2 ) {
/* .locals 4 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .param p3, "client" # Lcom/android/server/biometrics/sensors/AuthenticationClient; */
/* .line 231 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStubImpl ) p0 ).shouldHandleFailedAttempt ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->shouldHandleFailedAttempt(II)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 232 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I */
/* .line 235 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I */
/* if-ne v0, v1, :cond_3 */
int v0 = 5; // const/4 v0, 0x5
/* if-ne p1, v0, :cond_3 */
/* .line 236 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I */
/* .line 237 */
v2 = (( com.android.server.biometrics.sensors.AuthenticationClient ) p3 ).getTargetUserId ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->getTargetUserId()I
v2 = (( com.android.server.biometrics.sensors.AuthenticationClient ) p3 ).handleFailedAttempt ( v2 ); // invoke-virtual {p3, v2}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->handleFailedAttempt(I)I
/* .line 239 */
/* .local v2, "lockoutMode":I */
(( com.android.server.biometrics.sensors.AuthenticationClient ) p3 ).getOwnerString ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->getOwnerString()Ljava/lang/String;
v3 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStubImpl ) p0 ).isSecurityCenterClient ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->isSecurityCenterClient(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 240 */
/* const/16 v3, 0x13 */
(( com.android.server.biometrics.sensors.AuthenticationClient ) p3 ).onError ( v3, v0 ); // invoke-virtual {p3, v3, v0}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->onError(II)V
/* .line 242 */
} // :cond_1
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 243 */
/* if-ne v2, v1, :cond_2 */
/* .line 244 */
int v1 = 7; // const/4 v1, 0x7
/* .line 245 */
} // :cond_2
/* const/16 v1, 0x9 */
} // :goto_0
/* nop */
/* .line 246 */
/* .local v1, "errorCode":I */
(( com.android.server.biometrics.sensors.AuthenticationClient ) p3 ).onError ( v1, v0 ); // invoke-virtual {p3, v1, v0}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->onError(II)V
/* .line 249 */
} // .end local v1 # "errorCode":I
} // .end local v2 # "lockoutMode":I
} // :cond_3
return;
} // .end method
public Boolean interruptsPrecedingClients ( com.android.server.biometrics.sensors.BaseClientMonitor p0 ) {
/* .locals 2 */
/* .param p1, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .line 62 */
(( com.android.server.biometrics.sensors.BaseClientMonitor ) p1 ).getContext ( ); // invoke-virtual {p1}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getContext()Landroid/content/Context;
(( com.android.server.biometrics.sensors.BaseClientMonitor ) p1 ).getOwnerString ( ); // invoke-virtual {p1}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getOwnerString()Ljava/lang/String;
v0 = com.android.server.biometrics.Utils .isKeyguard ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 63 */
int v0 = 1; // const/4 v0, 0x1
/* .line 65 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isFingerDataSharer ( Integer p0 ) {
/* .locals 10 */
/* .param p1, "userId" # I */
/* .line 117 */
final String v0 = "FingerprintServiceStubImpl"; // const-string v0, "FingerprintServiceStubImpl"
int v1 = 0; // const/4 v1, 0x0
/* .line 118 */
/* .local v1, "uinfo":Landroid/content/pm/UserInfo; */
android.app.ActivityThread .currentApplication ( );
/* .line 119 */
/* .local v2, "app":Landroid/app/Application; */
/* const-class v3, Landroid/os/UserManager; */
(( android.app.Application ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/app/Application;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v3, Landroid/os/UserManager; */
/* .line 121 */
/* .local v3, "manager":Landroid/os/UserManager; */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v4 */
/* .line 123 */
/* .local v4, "token":J */
try { // :try_start_0
(( android.os.UserManager ) v3 ).getUserInfo ( p1 ); // invoke-virtual {v3, p1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v1, v6 */
/* .line 127 */
/* nop */
} // :goto_0
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 128 */
/* .line 127 */
/* :catchall_0 */
/* move-exception v0 */
/* goto/16 :goto_5 */
/* .line 124 */
/* :catch_0 */
/* move-exception v6 */
/* .line 125 */
/* .local v6, "ex":Ljava/lang/Exception; */
try { // :try_start_1
(( java.lang.Exception ) v6 ).getMessage ( ); // invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Log .d ( v0,v7 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 127 */
/* nop */
} // .end local v6 # "ex":Ljava/lang/Exception;
/* .line 130 */
} // :goto_1
int v6 = 0; // const/4 v6, 0x0
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 132 */
v7 = this.name;
int v8 = 1; // const/4 v8, 0x1
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 133 */
v7 = this.name;
v9 = (( java.lang.String ) v7 ).hashCode ( ); // invoke-virtual {v7}, Ljava/lang/String;->hashCode()I
/* sparse-switch v9, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
/* const-string/jumbo v9, "security space" */
v7 = (( java.lang.String ) v7 ).equals ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
/* move v7, v6 */
/* :sswitch_1 */
final String v9 = "child_model"; // const-string v9, "child_model"
v7 = (( java.lang.String ) v7 ).equals ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
/* move v7, v8 */
/* :sswitch_2 */
final String v9 = "XSpace"; // const-string v9, "XSpace"
v7 = (( java.lang.String ) v7 ).equals ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
int v7 = 2; // const/4 v7, 0x2
} // :goto_2
int v7 = -1; // const/4 v7, -0x1
} // :goto_3
/* packed-switch v7, :pswitch_data_0 */
/* .line 141 */
/* .line 137 */
/* :pswitch_0 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "calling from "; // const-string v7, "calling from "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.name;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", id = "; // const-string v7, ", id = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v6 );
/* .line 138 */
/* .line 143 */
} // :cond_1
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "calling from anonymous user, id = "; // const-string v9, "calling from anonymous user, id = "
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v7 );
/* .line 147 */
} // :goto_4
v7 = (( android.content.pm.UserInfo ) v1 ).isManagedProfile ( ); // invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 148 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "calling from managed-profile, id = "; // const-string v7, "calling from managed-profile, id = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", owned by "; // const-string v7, ", owned by "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v1, Landroid/content/pm/UserInfo;->profileGroupId:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v6 );
/* .line 149 */
/* .line 152 */
} // :cond_2
(( android.content.pm.UserInfo ) v1 ).toFullString ( ); // invoke-virtual {v1}, Landroid/content/pm/UserInfo;->toFullString()Ljava/lang/String;
android.util.Log .d ( v0,v7 );
/* .line 155 */
} // :cond_3
/* .line 127 */
} // :goto_5
android.os.Binder .restoreCallingIdentity ( v4,v5 );
/* .line 128 */
/* throw v0 */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x650f8872 -> :sswitch_2 */
/* -0x3868f1ba -> :sswitch_1 */
/* 0x7f500c26 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean isFingerDownAcquireCode ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .line 194 */
int v0 = 6; // const/4 v0, 0x6
/* if-ne p1, v0, :cond_0 */
/* const/16 v0, 0x16 */
/* if-ne p2, v0, :cond_0 */
/* .line 195 */
int v0 = 1; // const/4 v0, 0x1
/* .line 197 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isFingerUpAcquireCode ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .line 202 */
int v0 = 6; // const/4 v0, 0x6
/* if-ne p1, v0, :cond_0 */
/* const/16 v0, 0x17 */
/* if-ne p2, v0, :cond_0 */
/* .line 203 */
int v0 = 1; // const/4 v0, 0x1
/* .line 205 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isFingerprintClient ( com.android.server.biometrics.sensors.BaseClientMonitor p0 ) {
/* .locals 2 */
/* .param p1, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .line 186 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p1 ).statsModality ( ); // invoke-virtual {p1}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->statsModality()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 187 */
/* .line 189 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isFpHardwareDetected ( ) {
/* .locals 4 */
/* .line 253 */
final String v0 = "persist.vendor.sys.fp.vendor"; // const-string v0, "persist.vendor.sys.fp.vendor"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 254 */
/* .local v0, "fpVendor":Ljava/lang/String; */
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
final String v3 = "none"; // const-string v3, "none"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
(( java.lang.String ) v0 ).trim ( ); // invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 255 */
int v1 = 1; // const/4 v1, 0x1
/* .line 257 */
} // :cond_0
final String v1 = "FingerprintServiceStubImpl"; // const-string v1, "FingerprintServiceStubImpl"
final String v2 = "Fingerprint isHardwareDetected is failed."; // const-string v2, "Fingerprint isHardwareDetected is failed."
android.util.Log .e ( v1,v2 );
/* .line 258 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isScreenOn ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 71 */
/* const-class v0, Landroid/os/PowerManager; */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
/* .line 72 */
/* .local v0, "powerManager":Landroid/os/PowerManager; */
v1 = (( android.os.PowerManager ) v0 ).isInteractive ( ); // invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z
} // .end method
public Boolean isSecurityCenterClient ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "clientName" # Ljava/lang/String; */
/* .line 210 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public void resetLockFlag ( ) {
/* .locals 1 */
/* .line 225 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I */
/* .line 226 */
return;
} // .end method
public void saveAuthenResultLocal ( android.hardware.fingerprint.Fingerprint p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "identifier" # Landroid/hardware/fingerprint/Fingerprint; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/hardware/fingerprint/Fingerprint;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 95 */
/* .local p2, "token":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
this.mFingerIdentifer = p1;
/* .line 96 */
this.mFingerToken = p2;
/* .line 97 */
return;
} // .end method
public void saveAuthenticateConfig ( Long p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "opId" # J */
/* .param p3, "opPackageName" # Ljava/lang/String; */
/* .line 77 */
/* iput-wide p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpId:J */
/* .line 78 */
this.mOpPackage = p3;
/* .line 79 */
return;
} // .end method
public void setCurrentLockoutMode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "lockoutMode" # I */
/* .line 175 */
/* iput p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mLockoutMode:I */
/* .line 176 */
com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStub .getInstance ( );
/* .line 177 */
return;
} // .end method
public Boolean shouldHandleFailedAttempt ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .line 215 */
int v0 = 6; // const/4 v0, 0x6
/* if-ne p1, v0, :cond_0 */
/* const/16 v0, 0x13 */
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0x14 */
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0x15 */
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0x16 */
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0x17 */
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0x1c */
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0x32 */
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0x34 */
/* if-eq p2, v0, :cond_0 */
/* .line 218 */
int v0 = 1; // const/4 v0, 0x1
/* .line 220 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
