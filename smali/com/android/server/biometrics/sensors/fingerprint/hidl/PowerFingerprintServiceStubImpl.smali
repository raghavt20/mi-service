.class public Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;
.super Ljava/lang/Object;
.source "PowerFingerprintServiceStubImpl.java"

# interfaces
.implements Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub;


# static fields
.field private static final INTERCEPT_POWERKEY_THRESHOLD:J = 0xc8L

.field private static final TAG:Ljava/lang/String; = "PowerFingerprintServiceStubImpl"


# instance fields
.field private final DOUBLE_TAP_SIDE_FP:Ljava/lang/String;

.field private final FINGERPRINT_CMD_LOCKOUT_MODE:I

.field private final IS_FOLD:Z

.field private IS_POWERFP:Z

.field private IS_SUPPORT_FINGERPRINT_TAP:Z

.field protected final POWERFP_DISABLE_NAVIGATION:I

.field protected final POWERFP_ENABLE_LOCK_KEY:I

.field protected final POWERFP_ENABLE_NAVIGATION:I

.field private final PRODUCT_NAME:Ljava/lang/String;

.field private RO_BOOT_HWC:Ljava/lang/String;

.field private dealOnChange:Z

.field private listener:Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener;

.field private mBiometricState:I

.field private mDoubleTapSideFp:Ljava/lang/String;

.field private mFingerprintAuthFailTime:J

.field private mFingerprintAuthState:I

.field private mFingerprintAuthSuccessTime:J

.field private mFingerprintManager:Landroid/hardware/fingerprint/FingerprintManager;

.field private mIsInterceptPowerkeyAuthOrEnroll:Z

.field private mIsScreenOnWhenFingerdown:Z

.field protected mSideFpUnlockType:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmDoubleTapSideFp(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mDoubleTapSideFp:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFingerprintManager(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;)Landroid/hardware/fingerprint/FingerprintManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintManager:Landroid/hardware/fingerprint/FingerprintManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmBiometricState(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mBiometricState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDoubleTapSideFp(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mDoubleTapSideFp:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetDealOnChange(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDealOnChange()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetDoubleTapSideFpOption(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misDefaultPressUnlock(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->isDefaultPressUnlock()I

    move-result p0

    return p0
.end method

.method public constructor <init>()V
    .locals 6

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "ro.hardware.fp.sideCap"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_POWERFP:Z

    .line 46
    const-string v0, "is_support_fingerprint_tap"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_SUPPORT_FINGERPRINT_TAP:Z

    .line 47
    const-string v0, "persist.sys.muiltdisplay_type"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_FOLD:Z

    .line 48
    const-string v0, "ro.product.device"

    const-string/jumbo v4, "unknow"

    invoke-static {v0, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->PRODUCT_NAME:Ljava/lang/String;

    .line 49
    const-string v0, "ro.boot.hwc"

    const-string v4, ""

    invoke-static {v0, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->RO_BOOT_HWC:Ljava/lang/String;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mSideFpUnlockType:I

    .line 52
    iput-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->dealOnChange:Z

    .line 55
    const-string v0, "fingerprint_double_tap"

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->DOUBLE_TAP_SIDE_FP:Ljava/lang/String;

    .line 58
    iput-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z

    .line 60
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthState:I

    .line 61
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthSuccessTime:J

    .line 62
    iput-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthFailTime:J

    .line 64
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mBiometricState:I

    .line 65
    iput-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsInterceptPowerkeyAuthOrEnroll:Z

    .line 69
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->POWERFP_DISABLE_NAVIGATION:I

    .line 71
    iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->POWERFP_ENABLE_LOCK_KEY:I

    .line 73
    iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->POWERFP_ENABLE_NAVIGATION:I

    .line 75
    const/16 v0, 0xc

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->FINGERPRINT_CMD_LOCKOUT_MODE:I

    return-void
.end method

.method private getDealOnChange()Z
    .locals 1

    .line 151
    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->dealOnChange:Z

    return v0
.end method

.method private getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 208
    const/4 v0, 0x0

    .line 209
    .local v0, "dobuleTap":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_SUPPORT_FINGERPRINT_TAP:Z

    if-nez v1, :cond_0

    goto :goto_0

    .line 212
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "fingerprint_double_tap"

    const/4 v3, -0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 214
    return-object v0

    .line 210
    :cond_1
    :goto_0
    return-object v0
.end method

.method private isDefaultPressUnlock()I
    .locals 2

    .line 249
    const-string v0, "INDIA"

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->RO_BOOT_HWC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "IN"

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->RO_BOOT_HWC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_FOLD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->PRODUCT_NAME:Ljava/lang/String;

    .line 250
    const-string v1, "cetus"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 249
    :goto_1
    return v0
.end method

.method private isPad()Z
    .locals 1

    .line 155
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    return v0
.end method

.method private registerSideFpUnlockTypeChangedObserver(Landroid/os/Handler;Landroid/content/Context;)V
    .locals 4
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "context"    # Landroid/content/Context;

    .line 269
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$2;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;Landroid/os/Handler;Landroid/content/Context;)V

    .line 276
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MiuiSettings$Secure;->FINGERPRINT_UNLOCK_TYPE:Ljava/lang/String;

    .line 277
    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 276
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 278
    return-void
.end method

.method private setChangeListener(Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener;

    .line 140
    const-string v0, "PowerFingerprintServiceStubImpl"

    const-string/jumbo v1, "setChangeListener"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->listener:Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener;

    .line 142
    return-void
.end method

.method private shouldRestartClient(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    .line 316
    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getFingerprintUnlockType(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/android/server/biometrics/sensors/AuthenticationClient;

    if-eqz v0, :cond_0

    .line 317
    return v1

    .line 320
    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public getFingerprintUnlockType(Landroid/content/Context;)I
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 255
    const/4 v0, -0x1

    .line 256
    .local v0, "unlockType":I
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z

    move-result v1

    if-nez v1, :cond_0

    .line 257
    return v0

    .line 259
    :cond_0
    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mSideFpUnlockType:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 260
    return v1

    .line 263
    :cond_1
    nop

    .line 262
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MiuiSettings$Secure;->FINGERPRINT_UNLOCK_TYPE:Ljava/lang/String;

    .line 263
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->isDefaultPressUnlock()I

    move-result v3

    .line 262
    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 263
    move v4, v2

    goto :goto_0

    :cond_2
    nop

    :goto_0
    move v0, v4

    .line 264
    return v0
.end method

.method public getIsPowerfp()Z
    .locals 1

    .line 79
    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_POWERFP:Z

    return v0
.end method

.method public getIsSupportFpTap()Z
    .locals 1

    .line 180
    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_SUPPORT_FINGERPRINT_TAP:Z

    return v0
.end method

.method public handleAcquiredInfo(IILandroid/content/Context;Lcom/android/server/biometrics/sensors/BiometricScheduler;ILcom/android/server/biometrics/sensors/fingerprint/hidl/Fingerprint21$HalResultController;)V
    .locals 16
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "scheduler"    # Lcom/android/server/biometrics/sensors/BiometricScheduler;
    .param p5, "sensorId"    # I
    .param p6, "halResultController"    # Lcom/android/server/biometrics/sensors/fingerprint/hidl/Fingerprint21$HalResultController;

    .line 326
    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    const-string v5, "PowerFingerprintServiceStubImpl"

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    return-void

    .line 330
    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/biometrics/sensors/BiometricScheduler;->getCurrentClient()Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    move-result-object v6

    .line 331
    .local v6, "client":Lcom/android/server/biometrics/sensors/BaseClientMonitor;
    instance-of v0, v6, Lcom/android/server/biometrics/sensors/AuthenticationClient;

    const/4 v7, 0x1

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerDownAcquireCode(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isScreenOn(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z

    .line 333
    invoke-virtual {v1, v7}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->setDealOnChange(Z)V

    move/from16 v11, p5

    move-object/from16 v15, p6

    goto/16 :goto_3

    .line 334
    :cond_1
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerUpAcquireCode(II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 335
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getOwnerString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/android/server/biometrics/Utils;->isKeyguard(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 336
    invoke-direct {v1, v4, v6}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->shouldRestartClient(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 338
    :try_start_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getOpId()J

    move-result-wide v8

    .line 339
    .local v8, "opId":J
    const/4 v0, -0x1

    .line 341
    .local v0, "result":I
    move-object v10, v6

    check-cast v10, Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;

    .line 342
    .local v10, "authClient":Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;
    invoke-virtual {v10}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;->getFreshDaemon()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/hardware/biometrics/fingerprint/V2_1/IBiometricsFingerprint;

    invoke-virtual {v10}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;->getTargetUserId()I

    move-result v12

    invoke-interface {v11, v8, v9, v12}, Landroid/hardware/biometrics/fingerprint/V2_1/IBiometricsFingerprint;->authenticate(JI)I

    move-result v11

    move v0, v11

    .line 344
    if-eqz v0, :cond_2

    .line 345
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "startAuthentication failed, result="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 346
    move/from16 v11, p5

    int-to-long v12, v11

    const/4 v14, 0x0

    move-object/from16 v15, p6

    :try_start_1
    invoke-virtual {v15, v12, v13, v7, v14}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/Fingerprint21$HalResultController;->onError(JII)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 348
    .end local v0    # "result":I
    .end local v8    # "opId":J
    .end local v10    # "authClient":Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;
    :catch_0
    move-exception v0

    goto :goto_1

    .line 344
    .restart local v0    # "result":I
    .restart local v8    # "opId":J
    .restart local v10    # "authClient":Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;
    :cond_2
    move/from16 v11, p5

    move-object/from16 v15, p6

    .line 350
    .end local v0    # "result":I
    .end local v8    # "opId":J
    .end local v10    # "authClient":Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;
    :goto_0
    goto :goto_2

    .line 348
    :catch_1
    move-exception v0

    move/from16 v11, p5

    move-object/from16 v15, p6

    .line 349
    .local v0, "e":Landroid/os/RemoteException;
    :goto_1
    const-string/jumbo v7, "startAuthentication failed"

    invoke-static {v5, v7, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 336
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    move/from16 v11, p5

    move-object/from16 v15, p6

    goto :goto_2

    .line 335
    :cond_4
    move/from16 v11, p5

    move-object/from16 v15, p6

    .line 352
    :goto_2
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->clearSavedAuthenResult()V

    goto :goto_3

    .line 334
    :cond_5
    move/from16 v11, p5

    move-object/from16 v15, p6

    .line 354
    :goto_3
    return-void
.end method

.method public init(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BiometricScheduler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "scheduler"    # Lcom/android/server/biometrics/sensors/BiometricScheduler;

    .line 358
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    return-void

    .line 362
    :cond_0
    invoke-direct {p0, p2, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->registerSideFpUnlockTypeChangedObserver(Landroid/os/Handler;Landroid/content/Context;)V

    .line 363
    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/BiometricScheduler;->getCurrentClient()Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->registerDoubleTapSideFpOptionObserver(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)V

    .line 364
    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/BiometricScheduler;->getCurrentClient()Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V

    .line 365
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$3;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BiometricScheduler;)V

    invoke-direct {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->setChangeListener(Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener;)V

    .line 398
    const-class v0, Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/fingerprint/FingerprintManager;

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintManager:Landroid/hardware/fingerprint/FingerprintManager;

    .line 399
    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;

    invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;)V

    invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->addAuthenticatorsRegisteredCallback(Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback;)V

    .line 416
    return-void
.end method

.method public notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;
    .param p3, "lockoutMode"    # I
    .param p4, "param"    # I

    .line 219
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    return-void

    .line 223
    :cond_0
    if-eqz p2, :cond_1

    .line 224
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerprintClient(Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->isAlreadyDone()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p3, :cond_1

    .line 227
    invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getStatsAction()I

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mCurrentClient.statsAction() "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->statsModality()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "mCurrentClient.isAlreadyDone() "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 229
    invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->isAlreadyDone()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 230
    invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getStatsAction()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "lockoutMode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    const-string v1, "PowerFingerprintServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    return-void

    .line 235
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/16 v2, 0xc

    if-ne p4, v1, :cond_2

    iget-boolean v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_SUPPORT_FINGERPRINT_TAP:Z

    if-nez v3, :cond_2

    .line 236
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    invoke-static {v2, v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->startExtCmd(II)I

    goto :goto_0

    .line 237
    :cond_2
    if-ne p4, v1, :cond_4

    .line 238
    invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 239
    invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "none"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 240
    :cond_3
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    invoke-static {v2, v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->startExtCmd(II)I

    goto :goto_0

    .line 242
    :cond_4
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    invoke-static {v2, p4}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->startExtCmd(II)I

    .line 244
    :goto_0
    return-void
.end method

.method public notifyPowerPressed()V
    .locals 2

    .line 160
    const-string v0, "PowerFingerprintServiceStubImpl"

    const-string v1, "notifyPowerPressed"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mBiometricState:I

    packed-switch v0, :pswitch_data_0

    .line 170
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsInterceptPowerkeyAuthOrEnroll:Z

    goto :goto_0

    .line 165
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsInterceptPowerkeyAuthOrEnroll:Z

    .line 166
    nop

    .line 173
    :goto_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->listener:Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener;

    if-eqz v0, :cond_0

    .line 174
    iget-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->dealOnChange:Z

    invoke-interface {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener;->onChange(Z)V

    .line 176
    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public registerDoubleTapSideFpOptionObserver(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    .line 185
    invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mDoubleTapSideFp:Ljava/lang/String;

    .line 186
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "none"

    iget-object v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mDoubleTapSideFp:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 189
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, p3, v1, v0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V

    goto :goto_1

    .line 187
    :cond_1
    :goto_0
    invoke-virtual {p0, p1, p3, v1, v1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V

    .line 191
    :goto_1
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$1;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;Landroid/os/Handler;Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)V

    .line 203
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "fingerprint_double_tap"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 205
    return-void
.end method

.method public setDealOnChange(Z)V
    .locals 2
    .param p1, "value"    # Z

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setDealOnChange:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string/jumbo v1, "true"

    goto :goto_0

    :cond_0
    const-string v1, "false"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerFingerprintServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iput-boolean p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->dealOnChange:Z

    .line 148
    return-void
.end method

.method public setFingerprintAuthState(I)V
    .locals 4
    .param p1, "state"    # I

    .line 84
    const-string v0, "PowerFingerprintServiceStubImpl"

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 90
    :pswitch_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthFailTime:J

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fingerprint Auth failed time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthFailTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    goto :goto_0

    .line 86
    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthSuccessTime:J

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fingerprint Auth success time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthSuccessTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    nop

    .line 97
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public shouldConsumeSinglePress(JI)Z
    .locals 10
    .param p1, "eventTime"    # J
    .param p3, "biometricState"    # I

    .line 102
    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_POWERFP:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 103
    return v1

    .line 105
    :cond_0
    const-string v0, "fingerprint intercept power key press"

    const-wide/16 v2, 0xc8

    const-string v4, "intervalFpPower gap:"

    const/4 v5, 0x1

    const-string v6, "PowerFingerprintServiceStubImpl"

    packed-switch p3, :pswitch_data_0

    .line 135
    return v1

    .line 113
    :pswitch_0
    const-string v0, "intercept power press due to STATE_AUTH_OTHER"

    invoke-static {v6, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    return v5

    .line 110
    :pswitch_1
    const-string v0, "intercept power press due to STATE_BP_AUTH"

    invoke-static {v6, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return v5

    .line 116
    :pswitch_2
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->isPad()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 117
    iget-wide v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthFailTime:J

    sub-long v7, p1, v7

    .line 118
    .local v7, "intervalFpPower":J
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z

    if-nez v4, :cond_1

    cmp-long v2, v7, v2

    if-gez v2, :cond_1

    .line 120
    invoke-static {v6, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    return v5

    .line 124
    .end local v7    # "intervalFpPower":J
    :cond_1
    return v1

    .line 107
    :pswitch_3
    const-string v0, "intercept power press due to STATE_ENROLLING"

    invoke-static {v6, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    return v5

    .line 126
    :pswitch_4
    iget-wide v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthSuccessTime:J

    sub-long v7, p1, v7

    .line 127
    .restart local v7    # "intervalFpPower":J
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    cmp-long v2, v7, v2

    if-ltz v2, :cond_3

    iget-boolean v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsInterceptPowerkeyAuthOrEnroll:Z

    if-eqz v2, :cond_2

    goto :goto_0

    .line 132
    :cond_2
    return v1

    .line 129
    :cond_3
    :goto_0
    invoke-static {v6, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    return v5

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public shouldDropFailAuthenResult(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;Z)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;
    .param p3, "authenticated"    # Z

    .line 282
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->isPad()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 286
    :cond_0
    invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getOwnerString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/server/biometrics/Utils;->isKeyguard(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    const-string v2, "PowerFingerprintServiceStubImpl"

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    .line 287
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDealOnChange()Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isScreenOn(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 289
    const-string/jumbo v0, "shouldDropFailAuthenResult ture"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const/4 v0, 0x1

    return v0

    .line 292
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "shouldDropFailAuthenResult false mIsScreenOnWhenFingerdown:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " authenticated:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " getDealOnChange:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 293
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDealOnChange()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " isScreenOn:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 294
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isScreenOn(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 292
    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    return v1

    .line 283
    :cond_2
    :goto_0
    return v1
.end method

.method public shouldSavaAuthenResult(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    .line 302
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 303
    return v1

    .line 306
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getFingerprintUnlockType(Landroid/content/Context;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDealOnChange()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isScreenOn(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p2, Lcom/android/server/biometrics/sensors/AuthenticationClient;

    if-eqz v0, :cond_1

    .line 309
    return v2

    .line 312
    :cond_1
    return v1
.end method
