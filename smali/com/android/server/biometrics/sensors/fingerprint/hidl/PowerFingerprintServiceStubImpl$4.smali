.class Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;
.super Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback$Stub;
.source "PowerFingerprintServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->init(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BiometricScheduler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;

    .line 400
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;

    invoke-direct {p0}, Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onAllAuthenticatorsRegistered(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/fingerprint/FingerprintSensorPropertiesInternal;",
            ">;)V"
        }
    .end annotation

    .line 404
    .local p1, "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/fingerprint/FingerprintSensorPropertiesInternal;>;"
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->-$$Nest$fgetmFingerprintManager(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4$1;

    invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;)V

    invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->registerBiometricStateListener(Landroid/hardware/biometrics/BiometricStateListener;)V

    .line 413
    return-void
.end method
