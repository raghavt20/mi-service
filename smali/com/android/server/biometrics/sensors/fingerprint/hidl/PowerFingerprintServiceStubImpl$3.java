class com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl$3 implements com.android.server.biometrics.sensors.fingerprint.PowerFingerprintServiceStub$ChangeListener {
	 /* .source "PowerFingerprintServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->init(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BiometricScheduler;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl this$0; //synthetic
final android.content.Context val$context; //synthetic
final android.os.Handler val$handler; //synthetic
final com.android.server.biometrics.sensors.BiometricScheduler val$scheduler; //synthetic
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 365 */
this.this$0 = p1;
this.val$context = p2;
this.val$handler = p3;
this.val$scheduler = p4;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static void lambda$onChange$0 ( com.android.server.biometrics.sensors.BiometricScheduler p0 ) { //synthethic
/* .locals 5 */
/* .param p0, "scheduler" # Lcom/android/server/biometrics/sensors/BiometricScheduler; */
/* .line 371 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).getIdentifier ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getIdentifier()Landroid/hardware/fingerprint/Fingerprint;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 372 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).getIdentifier ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getIdentifier()Landroid/hardware/fingerprint/Fingerprint;
/* .line 373 */
/* .local v0, "fp":Landroid/hardware/fingerprint/Fingerprint; */
v1 = (( android.hardware.fingerprint.Fingerprint ) v0 ).getBiometricId ( ); // invoke-virtual {v0}, Landroid/hardware/fingerprint/Fingerprint;->getBiometricId()I
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* move v1, v2 */
/* .line 374 */
/* .local v1, "authenticated":Z */
} // :goto_0
/* if-nez v1, :cond_1 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v3 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v3 ).getSupportInterfaceVersion ( ); // invoke-virtual {v3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getSupportInterfaceVersion()I
int v4 = 2; // const/4 v4, 0x2
/* if-ne v3, v4, :cond_1 */
/* .line 375 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
/* const/16 v3, 0x11 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v3,v2 );
/* .line 377 */
} // :cond_1
(( com.android.server.biometrics.sensors.BiometricScheduler ) p0 ).getCurrentClient ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/BiometricScheduler;->getCurrentClient()Lcom/android/server/biometrics/sensors/BaseClientMonitor;
/* .line 378 */
/* .local v2, "client":Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* instance-of v3, v2, Lcom/android/server/biometrics/sensors/AuthenticationConsumer; */
/* if-nez v3, :cond_2 */
/* .line 379 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "onAuthenticated for non-authentication consumer: "; // const-string v4, "onAuthenticated for non-authentication consumer: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 380 */
com.android.server.biometrics.Utils .getClientName ( v2 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 379 */
final String v4 = "PowerFingerprintServiceStubImpl"; // const-string v4, "PowerFingerprintServiceStubImpl"
android.util.Slog .e ( v4,v3 );
/* .line 381 */
return;
/* .line 384 */
} // :cond_2
/* move-object v3, v2 */
/* check-cast v3, Lcom/android/server/biometrics/sensors/AuthenticationConsumer; */
/* .line 387 */
/* .local v3, "authenticationConsumer":Lcom/android/server/biometrics/sensors/AuthenticationConsumer; */
/* nop */
/* .line 388 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v4 ).getToken ( ); // invoke-virtual {v4}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getToken()Ljava/util/ArrayList;
/* .line 387 */
/* .line 390 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v4 ).clearSavedAuthenResult ( ); // invoke-virtual {v4}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->clearSavedAuthenResult()V
/* .line 392 */
} // .end local v0 # "fp":Landroid/hardware/fingerprint/Fingerprint;
} // .end local v1 # "authenticated":Z
} // .end local v2 # "client":Lcom/android/server/biometrics/sensors/BaseClientMonitor;
} // .end local v3 # "authenticationConsumer":Lcom/android/server/biometrics/sensors/AuthenticationConsumer;
} // :cond_3
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "value" # Z */
/* .line 368 */
v0 = this.this$0;
v0 = com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl .-$$Nest$mgetDealOnChange ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.this$0;
v1 = this.val$context;
v0 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) v0 ).getFingerprintUnlockType ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getFingerprintUnlockType(Landroid/content/Context;)I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* .line 369 */
final String v0 = "PowerFingerprintServiceStubImpl"; // const-string v0, "PowerFingerprintServiceStubImpl"
final String v1 = "onChange enter"; // const-string v1, "onChange enter"
android.util.Slog .i ( v0,v1 );
/* .line 370 */
v0 = this.val$handler;
v1 = this.val$scheduler;
/* new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$3$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$3$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/BiometricScheduler;)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 394 */
} // :cond_0
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) v0 ).setDealOnChange ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->setDealOnChange(Z)V
/* .line 395 */
return;
} // .end method
