.class Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4$1;
.super Landroid/hardware/biometrics/BiometricStateListener;
.source "PowerFingerprintServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;->onAllAuthenticatorsRegistered(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;


# direct methods
.method constructor <init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;

    .line 405
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4$1;->this$1:Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;

    invoke-direct {p0}, Landroid/hardware/biometrics/BiometricStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChanged(I)V
    .locals 2
    .param p1, "newState"    # I

    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onStateChanged : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerFingerprintServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4$1;->this$1:Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;

    iget-object v0, v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;

    invoke-static {v0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->-$$Nest$fputmBiometricState(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;I)V

    .line 411
    return-void
.end method
