public class com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl implements com.android.server.biometrics.sensors.fingerprint.PowerFingerprintServiceStub {
	 /* .source "PowerFingerprintServiceStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Long INTERCEPT_POWERKEY_THRESHOLD;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.lang.String DOUBLE_TAP_SIDE_FP;
	 private final Integer FINGERPRINT_CMD_LOCKOUT_MODE;
	 private final Boolean IS_FOLD;
	 private Boolean IS_POWERFP;
	 private Boolean IS_SUPPORT_FINGERPRINT_TAP;
	 protected final Integer POWERFP_DISABLE_NAVIGATION;
	 protected final Integer POWERFP_ENABLE_LOCK_KEY;
	 protected final Integer POWERFP_ENABLE_NAVIGATION;
	 private final java.lang.String PRODUCT_NAME;
	 private java.lang.String RO_BOOT_HWC;
	 private Boolean dealOnChange;
	 private com.android.server.biometrics.sensors.fingerprint.PowerFingerprintServiceStub$ChangeListener listener;
	 private Integer mBiometricState;
	 private java.lang.String mDoubleTapSideFp;
	 private Long mFingerprintAuthFailTime;
	 private Integer mFingerprintAuthState;
	 private Long mFingerprintAuthSuccessTime;
	 private android.hardware.fingerprint.FingerprintManager mFingerprintManager;
	 private Boolean mIsInterceptPowerkeyAuthOrEnroll;
	 private Boolean mIsScreenOnWhenFingerdown;
	 protected Integer mSideFpUnlockType;
	 /* # direct methods */
	 static java.lang.String -$$Nest$fgetmDoubleTapSideFp ( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mDoubleTapSideFp;
	 } // .end method
	 static android.hardware.fingerprint.FingerprintManager -$$Nest$fgetmFingerprintManager ( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mFingerprintManager;
	 } // .end method
	 static void -$$Nest$fputmBiometricState ( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl p0, Integer p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* iput p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mBiometricState:I */
		 return;
	 } // .end method
	 static void -$$Nest$fputmDoubleTapSideFp ( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl p0, java.lang.String p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mDoubleTapSideFp = p1;
		 return;
	 } // .end method
	 static Boolean -$$Nest$mgetDealOnChange ( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = 		 /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDealOnChange()Z */
	 } // .end method
	 static java.lang.String -$$Nest$mgetDoubleTapSideFpOption ( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl p0, android.content.Context p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String; */
	 } // .end method
	 static Integer -$$Nest$misDefaultPressUnlock ( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = 		 /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->isDefaultPressUnlock()I */
	 } // .end method
	 public com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ( ) {
		 /* .locals 6 */
		 /* .line 41 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 45 */
		 final String v0 = "ro.hardware.fp.sideCap"; // const-string v0, "ro.hardware.fp.sideCap"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 /* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_POWERFP:Z */
		 /* .line 46 */
		 final String v0 = "is_support_fingerprint_tap"; // const-string v0, "is_support_fingerprint_tap"
		 v0 = 		 miui.util.FeatureParser .getBoolean ( v0,v1 );
		 /* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_SUPPORT_FINGERPRINT_TAP:Z */
		 /* .line 47 */
		 final String v0 = "persist.sys.muiltdisplay_type"; // const-string v0, "persist.sys.muiltdisplay_type"
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 int v2 = 1; // const/4 v2, 0x1
		 int v3 = 2; // const/4 v3, 0x2
		 /* if-ne v0, v3, :cond_0 */
		 /* move v0, v2 */
	 } // :cond_0
	 /* move v0, v1 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_FOLD:Z */
/* .line 48 */
final String v0 = "ro.product.device"; // const-string v0, "ro.product.device"
/* const-string/jumbo v4, "unknow" */
android.os.SystemProperties .get ( v0,v4 );
this.PRODUCT_NAME = v0;
/* .line 49 */
final String v0 = "ro.boot.hwc"; // const-string v0, "ro.boot.hwc"
final String v4 = ""; // const-string v4, ""
android.os.SystemProperties .get ( v0,v4 );
this.RO_BOOT_HWC = v0;
/* .line 51 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mSideFpUnlockType:I */
/* .line 52 */
/* iput-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->dealOnChange:Z */
/* .line 55 */
final String v0 = "fingerprint_double_tap"; // const-string v0, "fingerprint_double_tap"
this.DOUBLE_TAP_SIDE_FP = v0;
/* .line 58 */
/* iput-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z */
/* .line 60 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthState:I */
/* .line 61 */
/* const-wide/16 v4, 0x0 */
/* iput-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthSuccessTime:J */
/* .line 62 */
/* iput-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthFailTime:J */
/* .line 64 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mBiometricState:I */
/* .line 65 */
/* iput-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsInterceptPowerkeyAuthOrEnroll:Z */
/* .line 69 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->POWERFP_DISABLE_NAVIGATION:I */
/* .line 71 */
/* iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->POWERFP_ENABLE_LOCK_KEY:I */
/* .line 73 */
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->POWERFP_ENABLE_NAVIGATION:I */
/* .line 75 */
/* const/16 v0, 0xc */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->FINGERPRINT_CMD_LOCKOUT_MODE:I */
return;
} // .end method
private Boolean getDealOnChange ( ) {
/* .locals 1 */
/* .line 151 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->dealOnChange:Z */
} // .end method
private java.lang.String getDoubleTapSideFpOption ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 208 */
int v0 = 0; // const/4 v0, 0x0
/* .line 209 */
/* .local v0, "dobuleTap":Ljava/lang/String; */
v1 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).getIsPowerfp ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* iget-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_SUPPORT_FINGERPRINT_TAP:Z */
	 /* if-nez v1, :cond_0 */
	 /* .line 212 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "fingerprint_double_tap"; // const-string v2, "fingerprint_double_tap"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
/* .line 214 */
/* .line 210 */
} // :cond_1
} // :goto_0
} // .end method
private Integer isDefaultPressUnlock ( ) {
/* .locals 2 */
/* .line 249 */
final String v0 = "INDIA"; // const-string v0, "INDIA"
v1 = this.RO_BOOT_HWC;
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
final String v0 = "IN"; // const-string v0, "IN"
v1 = this.RO_BOOT_HWC;
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_FOLD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.PRODUCT_NAME;
/* .line 250 */
final String v1 = "cetus"; // const-string v1, "cetus"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 249 */
} // :goto_1
} // .end method
private Boolean isPad ( ) {
/* .locals 1 */
/* .line 155 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
} // .end method
private void registerSideFpUnlockTypeChangedObserver ( android.os.Handler p0, android.content.Context p1 ) {
/* .locals 4 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 269 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$2; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$2;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 276 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p2 ).getContentResolver ( ); // invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = android.provider.MiuiSettings$Secure.FINGERPRINT_UNLOCK_TYPE;
/* .line 277 */
android.provider.Settings$Secure .getUriFor ( v2 );
/* .line 276 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 278 */
return;
} // .end method
private void setChangeListener ( com.android.server.biometrics.sensors.fingerprint.PowerFingerprintServiceStub$ChangeListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener; */
/* .line 140 */
final String v0 = "PowerFingerprintServiceStubImpl"; // const-string v0, "PowerFingerprintServiceStubImpl"
/* const-string/jumbo v1, "setChangeListener" */
android.util.Slog .d ( v0,v1 );
/* .line 141 */
this.listener = p1;
/* .line 142 */
return;
} // .end method
private Boolean shouldRestartClient ( android.content.Context p0, com.android.server.biometrics.sensors.BaseClientMonitor p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .line 316 */
v0 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).getFingerprintUnlockType ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getFingerprintUnlockType(Landroid/content/Context;)I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z */
/* if-nez v0, :cond_0 */
/* instance-of v0, p2, Lcom/android/server/biometrics/sensors/AuthenticationClient; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 317 */
/* .line 320 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public Integer getFingerprintUnlockType ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 255 */
int v0 = -1; // const/4 v0, -0x1
/* .line 256 */
/* .local v0, "unlockType":I */
v1 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).getIsPowerfp ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z
/* if-nez v1, :cond_0 */
/* .line 257 */
/* .line 259 */
} // :cond_0
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mSideFpUnlockType:I */
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_1 */
/* .line 260 */
/* .line 263 */
} // :cond_1
/* nop */
/* .line 262 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = android.provider.MiuiSettings$Secure.FINGERPRINT_UNLOCK_TYPE;
/* .line 263 */
v3 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->isDefaultPressUnlock()I */
/* .line 262 */
int v4 = 0; // const/4 v4, 0x0
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v2,v3,v4 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_2 */
/* .line 263 */
/* move v4, v2 */
} // :cond_2
/* nop */
} // :goto_0
/* move v0, v4 */
/* .line 264 */
} // .end method
public Boolean getIsPowerfp ( ) {
/* .locals 1 */
/* .line 79 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_POWERFP:Z */
} // .end method
public Boolean getIsSupportFpTap ( ) {
/* .locals 1 */
/* .line 180 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_SUPPORT_FINGERPRINT_TAP:Z */
} // .end method
public void handleAcquiredInfo ( Integer p0, Integer p1, android.content.Context p2, com.android.server.biometrics.sensors.BiometricScheduler p3, Integer p4, com.android.server.biometrics.sensors.fingerprint.hidl.Fingerprint21$HalResultController p5 ) {
/* .locals 16 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .param p3, "context" # Landroid/content/Context; */
/* .param p4, "scheduler" # Lcom/android/server/biometrics/sensors/BiometricScheduler; */
/* .param p5, "sensorId" # I */
/* .param p6, "halResultController" # Lcom/android/server/biometrics/sensors/fingerprint/hidl/Fingerprint21$HalResultController; */
/* .line 326 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move/from16 v3, p2 */
/* move-object/from16 v4, p3 */
final String v5 = "PowerFingerprintServiceStubImpl"; // const-string v5, "PowerFingerprintServiceStubImpl"
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z */
/* if-nez v0, :cond_0 */
/* .line 327 */
return;
/* .line 330 */
} // :cond_0
/* invoke-virtual/range {p4 ..p4}, Lcom/android/server/biometrics/sensors/BiometricScheduler;->getCurrentClient()Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .line 331 */
/* .local v6, "client":Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* instance-of v0, v6, Lcom/android/server/biometrics/sensors/AuthenticationClient; */
int v7 = 1; // const/4 v7, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isFingerDownAcquireCode ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerDownAcquireCode(II)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 332 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isScreenOn ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isScreenOn(Landroid/content/Context;)Z
/* iput-boolean v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z */
/* .line 333 */
(( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) v1 ).setDealOnChange ( v7 ); // invoke-virtual {v1, v7}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->setDealOnChange(Z)V
/* move/from16 v11, p5 */
/* move-object/from16 v15, p6 */
/* goto/16 :goto_3 */
/* .line 334 */
} // :cond_1
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isFingerUpAcquireCode ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerUpAcquireCode(II)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 335 */
if ( v6 != null) { // if-eqz v6, :cond_4
(( com.android.server.biometrics.sensors.BaseClientMonitor ) v6 ).getOwnerString ( ); // invoke-virtual {v6}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getOwnerString()Ljava/lang/String;
v0 = com.android.server.biometrics.Utils .isKeyguard ( v4,v0 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 336 */
v0 = /* invoke-direct {v1, v4, v6}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->shouldRestartClient(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 338 */
try { // :try_start_0
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).getOpId ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getOpId()J
/* move-result-wide v8 */
/* .line 339 */
/* .local v8, "opId":J */
int v0 = -1; // const/4 v0, -0x1
/* .line 341 */
/* .local v0, "result":I */
/* move-object v10, v6 */
/* check-cast v10, Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient; */
/* .line 342 */
/* .local v10, "authClient":Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient; */
(( com.android.server.biometrics.sensors.fingerprint.hidl.FingerprintAuthenticationClient ) v10 ).getFreshDaemon ( ); // invoke-virtual {v10}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;->getFreshDaemon()Ljava/lang/Object;
/* check-cast v11, Landroid/hardware/biometrics/fingerprint/V2_1/IBiometricsFingerprint; */
v11 = v12 = (( com.android.server.biometrics.sensors.fingerprint.hidl.FingerprintAuthenticationClient ) v10 ).getTargetUserId ( ); // invoke-virtual {v10}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;->getTargetUserId()I
/* move v0, v11 */
/* .line 344 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 345 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v12, "startAuthentication failed, result=" */
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v0 ); // invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v11 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 346 */
/* move/from16 v11, p5 */
/* int-to-long v12, v11 */
int v14 = 0; // const/4 v14, 0x0
/* move-object/from16 v15, p6 */
try { // :try_start_1
(( com.android.server.biometrics.sensors.fingerprint.hidl.Fingerprint21$HalResultController ) v15 ).onError ( v12, v13, v7, v14 ); // invoke-virtual {v15, v12, v13, v7, v14}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/Fingerprint21$HalResultController;->onError(JII)V
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 348 */
} // .end local v0 # "result":I
} // .end local v8 # "opId":J
} // .end local v10 # "authClient":Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;
/* :catch_0 */
/* move-exception v0 */
/* .line 344 */
/* .restart local v0 # "result":I */
/* .restart local v8 # "opId":J */
/* .restart local v10 # "authClient":Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient; */
} // :cond_2
/* move/from16 v11, p5 */
/* move-object/from16 v15, p6 */
/* .line 350 */
} // .end local v0 # "result":I
} // .end local v8 # "opId":J
} // .end local v10 # "authClient":Lcom/android/server/biometrics/sensors/fingerprint/hidl/FingerprintAuthenticationClient;
} // :goto_0
/* .line 348 */
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v11, p5 */
/* move-object/from16 v15, p6 */
/* .line 349 */
/* .local v0, "e":Landroid/os/RemoteException; */
} // :goto_1
/* const-string/jumbo v7, "startAuthentication failed" */
android.util.Slog .e ( v5,v7,v0 );
/* .line 336 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_3
/* move/from16 v11, p5 */
/* move-object/from16 v15, p6 */
/* .line 335 */
} // :cond_4
/* move/from16 v11, p5 */
/* move-object/from16 v15, p6 */
/* .line 352 */
} // :goto_2
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
(( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).clearSavedAuthenResult ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->clearSavedAuthenResult()V
/* .line 334 */
} // :cond_5
/* move/from16 v11, p5 */
/* move-object/from16 v15, p6 */
/* .line 354 */
} // :goto_3
return;
} // .end method
public void init ( android.content.Context p0, android.os.Handler p1, com.android.server.biometrics.sensors.BiometricScheduler p2 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "scheduler" # Lcom/android/server/biometrics/sensors/BiometricScheduler; */
/* .line 358 */
v0 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).getIsPowerfp ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z
/* if-nez v0, :cond_0 */
/* .line 359 */
return;
/* .line 362 */
} // :cond_0
/* invoke-direct {p0, p2, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->registerSideFpUnlockTypeChangedObserver(Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 363 */
(( com.android.server.biometrics.sensors.BiometricScheduler ) p3 ).getCurrentClient ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/BiometricScheduler;->getCurrentClient()Lcom/android/server/biometrics/sensors/BaseClientMonitor;
(( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).registerDoubleTapSideFpOptionObserver ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->registerDoubleTapSideFpOptionObserver(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)V
/* .line 364 */
(( com.android.server.biometrics.sensors.BiometricScheduler ) p3 ).getCurrentClient ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/BiometricScheduler;->getCurrentClient()Lcom/android/server/biometrics/sensors/BaseClientMonitor;
int v1 = 0; // const/4 v1, 0x0
int v2 = 2; // const/4 v2, 0x2
(( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).notifyLockOutState ( p1, v0, v1, v2 ); // invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V
/* .line 365 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$3; */
/* invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$3;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BiometricScheduler;)V */
/* invoke-direct {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->setChangeListener(Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub$ChangeListener;)V */
/* .line 398 */
/* const-class v0, Landroid/hardware/fingerprint/FingerprintManager; */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/fingerprint/FingerprintManager; */
this.mFingerprintManager = v0;
/* .line 399 */
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4; */
/* invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;)V */
(( android.hardware.fingerprint.FingerprintManager ) v0 ).addAuthenticatorsRegisteredCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->addAuthenticatorsRegisteredCallback(Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback;)V
/* .line 416 */
return;
} // .end method
public void notifyLockOutState ( android.content.Context p0, com.android.server.biometrics.sensors.BaseClientMonitor p1, Integer p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .param p3, "lockoutMode" # I */
/* .param p4, "param" # I */
/* .line 219 */
v0 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).getIsPowerfp ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z
/* if-nez v0, :cond_0 */
/* .line 220 */
return;
/* .line 223 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 224 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isFingerprintClient ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerprintClient(Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 225 */
v0 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p2 ).isAlreadyDone ( ); // invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->isAlreadyDone()Z
/* if-nez v0, :cond_1 */
/* if-nez p3, :cond_1 */
/* .line 227 */
v0 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p2 ).getStatsAction ( ); // invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getStatsAction()I
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 228 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mCurrentClient.statsAction() "; // const-string v1, "mCurrentClient.statsAction() "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p2 ).statsModality ( ); // invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->statsModality()I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "mCurrentClient.isAlreadyDone() "; // const-string v2, "mCurrentClient.isAlreadyDone() "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 229 */
v2 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p2 ).isAlreadyDone ( ); // invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->isAlreadyDone()Z
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 230 */
v1 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p2 ).getStatsAction ( ); // invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getStatsAction()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "lockoutMode "; // const-string v1, "lockoutMode "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 228 */
final String v1 = "PowerFingerprintServiceStubImpl"; // const-string v1, "PowerFingerprintServiceStubImpl"
android.util.Slog .w ( v1,v0 );
/* .line 232 */
return;
/* .line 235 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
int v1 = 2; // const/4 v1, 0x2
/* const/16 v2, 0xc */
/* if-ne p4, v1, :cond_2 */
/* iget-boolean v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_SUPPORT_FINGERPRINT_TAP:Z */
/* if-nez v3, :cond_2 */
/* .line 236 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v2,v0 );
/* .line 237 */
} // :cond_2
/* if-ne p4, v1, :cond_4 */
/* .line 238 */
/* invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_3 */
/* .line 239 */
/* invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String; */
final String v3 = "none"; // const-string v3, "none"
v1 = (( java.lang.String ) v3 ).equalsIgnoreCase ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 240 */
} // :cond_3
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v2,v0 );
/* .line 242 */
} // :cond_4
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v2,p4 );
/* .line 244 */
} // :goto_0
return;
} // .end method
public void notifyPowerPressed ( ) {
/* .locals 2 */
/* .line 160 */
final String v0 = "PowerFingerprintServiceStubImpl"; // const-string v0, "PowerFingerprintServiceStubImpl"
final String v1 = "notifyPowerPressed"; // const-string v1, "notifyPowerPressed"
android.util.Slog .d ( v0,v1 );
/* .line 161 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mBiometricState:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 170 */
/* :pswitch_0 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsInterceptPowerkeyAuthOrEnroll:Z */
/* .line 165 */
/* :pswitch_1 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsInterceptPowerkeyAuthOrEnroll:Z */
/* .line 166 */
/* nop */
/* .line 173 */
} // :goto_0
v0 = this.listener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 174 */
/* iget-boolean v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->dealOnChange:Z */
/* .line 176 */
} // :cond_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void registerDoubleTapSideFpOptionObserver ( android.content.Context p0, android.os.Handler p1, com.android.server.biometrics.sensors.BaseClientMonitor p2 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .line 185 */
/* invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDoubleTapSideFpOption(Landroid/content/Context;)Ljava/lang/String; */
this.mDoubleTapSideFp = v0;
/* .line 186 */
v0 = android.text.TextUtils .isEmpty ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_1 */
final String v0 = "none"; // const-string v0, "none"
v2 = this.mDoubleTapSideFp;
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 189 */
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
(( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).notifyLockOutState ( p1, p3, v1, v0 ); // invoke-virtual {p0, p1, p3, v1, v0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V
/* .line 187 */
} // :cond_1
} // :goto_0
(( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).notifyLockOutState ( p1, p3, v1, v1 ); // invoke-virtual {p0, p1, p3, v1, v1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V
/* .line 191 */
} // :goto_1
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$1; */
/* invoke-direct {v0, p0, p2, p1, p3}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;Landroid/os/Handler;Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)V */
/* .line 203 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "fingerprint_double_tap"; // const-string v3, "fingerprint_double_tap"
android.provider.Settings$System .getUriFor ( v3 );
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v1, v0, v4 ); // invoke-virtual {v2, v3, v1, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 205 */
return;
} // .end method
public void setDealOnChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "value" # Z */
/* .line 146 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setDealOnChange:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( p1 != null) { // if-eqz p1, :cond_0
/* const-string/jumbo v1, "true" */
} // :cond_0
final String v1 = "false"; // const-string v1, "false"
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PowerFingerprintServiceStubImpl"; // const-string v1, "PowerFingerprintServiceStubImpl"
android.util.Slog .d ( v1,v0 );
/* .line 147 */
/* iput-boolean p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->dealOnChange:Z */
/* .line 148 */
return;
} // .end method
public void setFingerprintAuthState ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "state" # I */
/* .line 84 */
final String v0 = "PowerFingerprintServiceStubImpl"; // const-string v0, "PowerFingerprintServiceStubImpl"
/* packed-switch p1, :pswitch_data_0 */
/* .line 90 */
/* :pswitch_0 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthFailTime:J */
/* .line 91 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Fingerprint Auth failed time:"; // const-string v2, "Fingerprint Auth failed time:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthFailTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 92 */
/* .line 86 */
/* :pswitch_1 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthSuccessTime:J */
/* .line 87 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Fingerprint Auth success time:"; // const-string v2, "Fingerprint Auth success time:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthSuccessTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 88 */
/* nop */
/* .line 97 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean shouldConsumeSinglePress ( Long p0, Integer p1 ) {
/* .locals 10 */
/* .param p1, "eventTime" # J */
/* .param p3, "biometricState" # I */
/* .line 102 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->IS_POWERFP:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 103 */
/* .line 105 */
} // :cond_0
final String v0 = "fingerprint intercept power key press"; // const-string v0, "fingerprint intercept power key press"
/* const-wide/16 v2, 0xc8 */
final String v4 = "intervalFpPower gap:"; // const-string v4, "intervalFpPower gap:"
int v5 = 1; // const/4 v5, 0x1
final String v6 = "PowerFingerprintServiceStubImpl"; // const-string v6, "PowerFingerprintServiceStubImpl"
/* packed-switch p3, :pswitch_data_0 */
/* .line 135 */
/* .line 113 */
/* :pswitch_0 */
final String v0 = "intercept power press due to STATE_AUTH_OTHER"; // const-string v0, "intercept power press due to STATE_AUTH_OTHER"
android.util.Slog .d ( v6,v0 );
/* .line 114 */
/* .line 110 */
/* :pswitch_1 */
final String v0 = "intercept power press due to STATE_BP_AUTH"; // const-string v0, "intercept power press due to STATE_BP_AUTH"
android.util.Slog .d ( v6,v0 );
/* .line 111 */
/* .line 116 */
/* :pswitch_2 */
v7 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->isPad()Z */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 117 */
/* iget-wide v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthFailTime:J */
/* sub-long v7, p1, v7 */
/* .line 118 */
/* .local v7, "intervalFpPower":J */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v4 );
/* .line 119 */
/* iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z */
/* if-nez v4, :cond_1 */
/* cmp-long v2, v7, v2 */
/* if-gez v2, :cond_1 */
/* .line 120 */
android.util.Slog .d ( v6,v0 );
/* .line 121 */
/* .line 124 */
} // .end local v7 # "intervalFpPower":J
} // :cond_1
/* .line 107 */
/* :pswitch_3 */
final String v0 = "intercept power press due to STATE_ENROLLING"; // const-string v0, "intercept power press due to STATE_ENROLLING"
android.util.Slog .d ( v6,v0 );
/* .line 108 */
/* .line 126 */
/* :pswitch_4 */
/* iget-wide v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mFingerprintAuthSuccessTime:J */
/* sub-long v7, p1, v7 */
/* .line 127 */
/* .restart local v7 # "intervalFpPower":J */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v4 );
/* .line 128 */
/* cmp-long v2, v7, v2 */
/* if-ltz v2, :cond_3 */
/* iget-boolean v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsInterceptPowerkeyAuthOrEnroll:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 132 */
} // :cond_2
/* .line 129 */
} // :cond_3
} // :goto_0
android.util.Slog .d ( v6,v0 );
/* .line 130 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean shouldDropFailAuthenResult ( android.content.Context p0, com.android.server.biometrics.sensors.BaseClientMonitor p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .param p3, "authenticated" # Z */
/* .line 282 */
v0 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).getIsPowerfp ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->isPad()Z */
/* if-nez v0, :cond_0 */
/* .line 286 */
} // :cond_0
(( com.android.server.biometrics.sensors.BaseClientMonitor ) p2 ).getOwnerString ( ); // invoke-virtual {p2}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getOwnerString()Ljava/lang/String;
v0 = com.android.server.biometrics.Utils .isKeyguard ( p1,v0 );
final String v2 = "PowerFingerprintServiceStubImpl"; // const-string v2, "PowerFingerprintServiceStubImpl"
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez p3, :cond_1 */
/* .line 287 */
v0 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDealOnChange()Z */
/* if-nez v0, :cond_1 */
/* .line 288 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isScreenOn ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isScreenOn(Landroid/content/Context;)Z
/* if-nez v0, :cond_1 */
/* .line 289 */
/* const-string/jumbo v0, "shouldDropFailAuthenResult ture" */
android.util.Slog .d ( v2,v0 );
/* .line 290 */
int v0 = 1; // const/4 v0, 0x1
/* .line 292 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "shouldDropFailAuthenResult false mIsScreenOnWhenFingerdown:" */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mIsScreenOnWhenFingerdown:Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " authenticated:"; // const-string v3, " authenticated:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " getDealOnChange:"; // const-string v3, " getDealOnChange:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 293 */
v3 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDealOnChange()Z */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " isScreenOn:"; // const-string v3, " isScreenOn:"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 294 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v3 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v3 ).isScreenOn ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isScreenOn(Landroid/content/Context;)Z
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 292 */
android.util.Slog .d ( v2,v0 );
/* .line 297 */
/* .line 283 */
} // :cond_2
} // :goto_0
} // .end method
public Boolean shouldSavaAuthenResult ( android.content.Context p0, com.android.server.biometrics.sensors.BaseClientMonitor p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .line 302 */
v0 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).getIsPowerfp ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getIsPowerfp()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 303 */
/* .line 306 */
} // :cond_0
v0 = (( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) p0 ).getFingerprintUnlockType ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getFingerprintUnlockType(Landroid/content/Context;)I
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
v0 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->getDealOnChange()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 307 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isScreenOn ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isScreenOn(Landroid/content/Context;)Z
/* if-nez v0, :cond_1 */
/* instance-of v0, p2, Lcom/android/server/biometrics/sensors/AuthenticationClient; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 309 */
/* .line 312 */
} // :cond_1
} // .end method
