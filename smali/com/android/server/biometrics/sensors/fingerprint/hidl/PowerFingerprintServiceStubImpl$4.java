class com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl$4 extends android.hardware.fingerprint.IFingerprintAuthenticatorsRegisteredCallback$Stub {
	 /* .source "PowerFingerprintServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->init(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BiometricScheduler;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl this$0; //synthetic
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl; */
/* .line 400 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAllAuthenticatorsRegistered ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/hardware/fingerprint/FingerprintSensorPropertiesInternal;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 404 */
/* .local p1, "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/fingerprint/FingerprintSensorPropertiesInternal;>;" */
v0 = this.this$0;
com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl .-$$Nest$fgetmFingerprintManager ( v0 );
/* new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$4;)V */
(( android.hardware.fingerprint.FingerprintManager ) v0 ).registerBiometricStateListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->registerBiometricStateListener(Landroid/hardware/biometrics/BiometricStateListener;)V
/* .line 413 */
return;
} // .end method
