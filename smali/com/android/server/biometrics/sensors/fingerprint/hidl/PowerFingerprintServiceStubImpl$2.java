class com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl$2 extends android.database.ContentObserver {
	 /* .source "PowerFingerprintServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->registerSideFpUnlockTypeChangedObserver(Landroid/os/Handler;Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 269 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .line 272 */
v0 = this.this$0;
v1 = this.val$context;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v2 = android.provider.MiuiSettings$Secure.FINGERPRINT_UNLOCK_TYPE;
v3 = this.this$0;
/* .line 273 */
v3 = com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl .-$$Nest$misDefaultPressUnlock ( v3 );
/* .line 272 */
int v4 = 0; // const/4 v4, 0x0
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v2,v3,v4 );
/* iput v1, v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->mSideFpUnlockType:I */
/* .line 274 */
return;
} // .end method
