class com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl$1 extends android.database.ContentObserver {
	 /* .source "PowerFingerprintServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->registerDoubleTapSideFpOptionObserver(Landroid/content/Context;Landroid/os/Handler;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl this$0; //synthetic
final com.android.server.biometrics.sensors.BaseClientMonitor val$client; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 191 */
this.this$0 = p1;
this.val$context = p3;
this.val$client = p4;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .line 194 */
v0 = this.this$0;
v1 = this.val$context;
com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl .-$$Nest$mgetDoubleTapSideFpOption ( v0,v1 );
com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl .-$$Nest$fputmDoubleTapSideFp ( v0,v1 );
/* .line 195 */
v0 = this.this$0;
com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl .-$$Nest$fgetmDoubleTapSideFp ( v0 );
final String v1 = "none"; // const-string v1, "none"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 196 */
v0 = this.this$0;
v2 = this.val$context;
v3 = this.val$client;
int v4 = 2; // const/4 v4, 0x2
(( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) v0 ).notifyLockOutState ( v2, v3, v1, v4 ); // invoke-virtual {v0, v2, v3, v1, v4}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V
/* .line 198 */
} // :cond_0
v0 = this.this$0;
v2 = this.val$context;
v3 = this.val$client;
(( com.android.server.biometrics.sensors.fingerprint.hidl.PowerFingerprintServiceStubImpl ) v0 ).notifyLockOutState ( v2, v3, v1, v1 ); // invoke-virtual {v0, v2, v3, v1, v1}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl;->notifyLockOutState(Landroid/content/Context;Lcom/android/server/biometrics/sensors/BaseClientMonitor;II)V
/* .line 200 */
} // :goto_0
return;
} // .end method
