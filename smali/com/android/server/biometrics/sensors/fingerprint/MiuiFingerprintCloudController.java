public class com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController {
	 /* .source "MiuiFingerprintCloudController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;, */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ANTI_MISTOUCH_ENABLE;
private static final java.lang.String ANTI_MISTOUCH_VALID_TIMES;
private static final java.lang.String CLOUD_BACKUP_FILE_NAME;
public static final Long CLOUD_EVENTS_ANTI_MISTOUCH_CONTROL;
public static final Long CLOUD_EVENTS_LOWBRIGHTNESS_AUTH_CONTROL;
public static final Long CLOUD_EVENTS_OCP_DETECT_CONTROL;
private static final java.lang.String COUND_FP_FEATURE_ANTI_MISTOUCH;
private static final java.lang.String COUND_FP_FEATURE_LOWBRIGHTNESS_AUTH;
private static final java.lang.String COUND_FP_FEATURE_OCP_DETECT;
private static final java.lang.String COUND_FP_SUBMODULE_NAME;
public static final Integer FP_CLOUD_ANTI_MISTOUCH_CMD;
public static final Integer FP_CLOUD_ANTI_MISTOUCH_DISABLE_PARAM;
public static final Integer FP_CLOUD_ANTI_MISTOUCH_ENABLE_PARAM;
public static Integer FP_CLOUD_ANTI_MISTOUCH_VALID_TIMES_PARAM;
public static final Integer FP_CLOUD_LOWBRIGHTNESS_AUTH_CMD;
public static final Integer FP_CLOUD_LOWBRIGHTNESS_AUTH_DISABLE_PARAM;
public static final Integer FP_CLOUD_LOWBRIGHTNESS_AUTH_ENABLE_PARAM;
public static final Integer FP_CLOUD_LOWBRIGHTNESS_AUTH_SETTING_DISABLE_DEFAULT_PARAM;
public static final Integer FP_CLOUD_LOWBRIGHTNESS_AUTH_SETTING_ENABLE_DEFAULT_PARAM;
public static final Integer FP_CLOUD_OCP_DETECT_CMD;
public static final Integer FP_CLOUD_OCP_DETECT_DISABLE_PARAM;
public static final Integer FP_CLOUD_OCP_DETECT_ENABLE_PARAM;
public static final Integer FP_CLOUD_OCP_DETECT_SETTING_DISABLE_DEFAULT_PARAM;
public static final Integer FP_CLOUD_OCP_DETECT_SETTING_ENABLE_DEFAULT_PARAM;
private static final java.lang.String LOWBRIGHTNESS_AUTH_ENABLE;
private static final java.lang.String LOWBRIGHTNESS_AUTH_SETTING_ENABLE_DEFAULT;
private static final java.lang.String OCP_DETECT_ENABLE;
private static final java.lang.String OCP_DETECT_SETTING_ENABLE_DEFAULT;
private static final java.lang.String TAG;
private static android.hardware.fingerprint.IFingerprintService mFingerprintService;
private static com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController mInstance;
/* # instance fields */
private Boolean mAntiMistouchEnable;
private Integer mAntiMistouchValidTimes;
private android.util.AtomicFile mCloudBackupXmlFile;
private java.util.Map mCloudEventsData;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mCloudEventsSummary;
private java.util.List mCloudListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$FingerprintAntiMistouch mFingerprintAntiMistouch;
private com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$FingerprintLowBrightnessAuth mFingerprintLowBrightnessAuth;
private com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$FingerprintOcpDetect mFingerprintOcpDetect;
private android.os.Handler mHandler;
private Boolean mLowBrightnessAuthEnable;
private Boolean mLowBrightnessAuthSettingEnableDefault;
private Boolean mOcpDetectEnable;
private Boolean mOcpDetectSettingEnableDefault;
/* # direct methods */
public static void $r8$lambda$NqGkiZJ8Sm2K6_ZMNdmCXg2ueTA ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0, com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$CloudListener p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->lambda$updateDataFromCloudControl$0(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V */
return;
} // .end method
static Boolean -$$Nest$fgetmAntiMistouchEnable ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z */
} // .end method
static Integer -$$Nest$fgetmAntiMistouchValidTimes ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I */
} // .end method
static Boolean -$$Nest$fgetmLowBrightnessAuthEnable ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z */
} // .end method
static Boolean -$$Nest$fgetmLowBrightnessAuthSettingEnableDefault ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z */
} // .end method
static Boolean -$$Nest$fgetmOcpDetectEnable ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z */
} // .end method
static Boolean -$$Nest$fgetmOcpDetectSettingEnableDefault ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z */
} // .end method
static void -$$Nest$msyncLocalBackupFromCloud ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->syncLocalBackupFromCloud()V */
return;
} // .end method
static Boolean -$$Nest$mupdateDataFromCloudControl ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->updateDataFromCloudControl()Z */
} // .end method
static android.hardware.fingerprint.IFingerprintService -$$Nest$sfgetmFingerprintService ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController.mFingerprintService;
} // .end method
static com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController ( ) {
/* .locals 1 */
/* .line 95 */
int v0 = 1; // const/4 v0, 0x1
/* .line 107 */
int v0 = 0; // const/4 v0, 0x0
/* .line 108 */
return;
} // .end method
public com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController ( ) {
/* .locals 2 */
/* .param p1, "looper" # Landroid/os/Looper; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 110 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z */
/* .line 60 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I */
/* .line 64 */
/* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z */
/* .line 66 */
/* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z */
/* .line 70 */
/* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z */
/* .line 72 */
/* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z */
/* .line 76 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCloudListeners = v0;
/* .line 111 */
this.mContext = p2;
/* .line 112 */
final String v0 = "MiuiFingerprintCloudController"; // const-string v0, "MiuiFingerprintCloudController"
final String v1 = "construct."; // const-string v1, "construct."
android.util.Slog .i ( v0,v1 );
/* .line 113 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCloudEventsData = v0;
/* .line 114 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintAntiMistouch-IA;)V */
this.mFingerprintAntiMistouch = v0;
/* .line 115 */
(( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController ) p0 ).addCloudListener ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->addCloudListener(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V
/* .line 116 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth-IA;)V */
this.mFingerprintLowBrightnessAuth = v0;
/* .line 117 */
(( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController ) p0 ).addCloudListener ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->addCloudListener(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V
/* .line 118 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintOcpDetect-IA;)V */
this.mFingerprintOcpDetect = v0;
/* .line 119 */
(( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController ) p0 ).addCloudListener ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->addCloudListener(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;)V
/* .line 120 */
final String v0 = "fingerprint_cloud_backup.xml"; // const-string v0, "fingerprint_cloud_backup.xml"
/* invoke-direct {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->getFile(Ljava/lang/String;)Landroid/util/AtomicFile; */
this.mCloudBackupXmlFile = v0;
/* .line 121 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 122 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->registerCloudDataObserver()V */
/* .line 123 */
return;
} // .end method
private android.util.AtomicFile getFile ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "fileName" # Ljava/lang/String; */
/* .line 173 */
/* new-instance v0, Landroid/util/AtomicFile; */
/* new-instance v1, Ljava/io/File; */
android.os.Environment .getDataSystemDirectory ( );
final String v3 = "fingerprintconfig"; // const-string v3, "fingerprintconfig"
/* filled-new-array {v3}, [Ljava/lang/String; */
android.os.Environment .buildPath ( v2,v3 );
/* invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V */
} // .end method
private void lambda$updateDataFromCloudControl$0 ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$CloudListener p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "l" # Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener; */
/* .line 167 */
/* iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
v2 = this.mCloudEventsData;
/* .line 168 */
return;
} // .end method
private void registerCloudDataObserver ( ) {
/* .locals 4 */
/* .line 137 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 138 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Landroid/os/Handler;)V */
/* .line 137 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 152 */
return;
} // .end method
private void syncLocalBackupFromCloud ( ) {
/* .locals 14 */
/* .line 177 */
final String v0 = "fingerprint_feature_control"; // const-string v0, "fingerprint_feature_control"
final String v1 = "MiuiFingerprintCloudController"; // const-string v1, "MiuiFingerprintCloudController"
v2 = this.mCloudBackupXmlFile;
/* if-nez v2, :cond_0 */
/* .line 178 */
return;
/* .line 180 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 182 */
/* .local v2, "outputStream":Ljava/io/FileOutputStream; */
try { // :try_start_0
final String v3 = "Start syncing local backup from cloud."; // const-string v3, "Start syncing local backup from cloud."
android.util.Slog .i ( v1,v3 );
/* .line 183 */
v3 = this.mCloudBackupXmlFile;
(( android.util.AtomicFile ) v3 ).startWrite ( ); // invoke-virtual {v3}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v2, v3 */
/* .line 184 */
android.util.Xml .resolveSerializer ( v2 );
/* .line 185 */
/* .local v3, "out":Lcom/android/modules/utils/TypedXmlSerializer; */
int v4 = 1; // const/4 v4, 0x1
java.lang.Boolean .valueOf ( v4 );
int v11 = 0; // const/4 v11, 0x0
/* .line 186 */
final String v5 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v5, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 187 */
/* .line 189 */
/* iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v6, 0x1 */
/* and-long/2addr v4, v6 */
/* const-wide/16 v12, 0x0 */
/* cmp-long v4, v4, v12 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 190 */
v5 = this.mCloudBackupXmlFile;
final String v8 = "enable"; // const-string v8, "enable"
final String v9 = "anti_mistouch"; // const-string v9, "anti_mistouch"
/* iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z */
/* move-object v4, p0 */
/* move-object v6, v2 */
/* move-object v7, v3 */
/* invoke-direct/range {v4 ..v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 192 */
v5 = this.mCloudBackupXmlFile;
/* const-string/jumbo v8, "valid_times_per_screenlock" */
final String v9 = "anti_mistouch"; // const-string v9, "anti_mistouch"
/* iget v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I */
/* move-object v4, p0 */
/* move-object v6, v2 */
/* move-object v7, v3 */
/* invoke-direct/range {v4 ..v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureIntToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 196 */
} // :cond_1
/* iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v6, 0x2 */
/* and-long/2addr v4, v6 */
/* cmp-long v4, v4, v12 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 197 */
v5 = this.mCloudBackupXmlFile;
final String v8 = "enable"; // const-string v8, "enable"
final String v9 = "lowbrightness_auth"; // const-string v9, "lowbrightness_auth"
/* iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z */
/* move-object v4, p0 */
/* move-object v6, v2 */
/* move-object v7, v3 */
/* invoke-direct/range {v4 ..v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 199 */
v5 = this.mCloudBackupXmlFile;
/* const-string/jumbo v8, "setting_enable_default" */
final String v9 = "lowbrightness_auth"; // const-string v9, "lowbrightness_auth"
/* iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z */
/* move-object v4, p0 */
/* move-object v6, v2 */
/* move-object v7, v3 */
/* invoke-direct/range {v4 ..v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 203 */
} // :cond_2
/* iget-wide v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v6, 0x4 */
/* and-long/2addr v4, v6 */
/* cmp-long v4, v4, v12 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 204 */
v5 = this.mCloudBackupXmlFile;
final String v8 = "enable"; // const-string v8, "enable"
final String v9 = "ocp_detect"; // const-string v9, "ocp_detect"
/* iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z */
/* move-object v4, p0 */
/* move-object v6, v2 */
/* move-object v7, v3 */
/* invoke-direct/range {v4 ..v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 206 */
v5 = this.mCloudBackupXmlFile;
/* const-string/jumbo v8, "setting_enable_default" */
final String v9 = "ocp_detect"; // const-string v9, "ocp_detect"
/* iget-boolean v10, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z */
/* move-object v4, p0 */
/* move-object v6, v2 */
/* move-object v7, v3 */
/* invoke-direct/range {v4 ..v10}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->writeFeatureBooleanToXml(Landroid/util/AtomicFile;Ljava/io/FileOutputStream;Lcom/android/modules/utils/TypedXmlSerializer;Ljava/lang/String;Ljava/lang/String;Z)V */
/* .line 209 */
} // :cond_3
/* .line 210 */
/* .line 211 */
(( java.io.FileOutputStream ) v2 ).flush ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
/* .line 212 */
v0 = this.mCloudBackupXmlFile;
(( android.util.AtomicFile ) v0 ).finishWrite ( v2 ); // invoke-virtual {v0, v2}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 216 */
} // .end local v3 # "out":Lcom/android/modules/utils/TypedXmlSerializer;
/* .line 213 */
/* :catch_0 */
/* move-exception v0 */
/* .line 214 */
/* .local v0, "e":Ljava/io/IOException; */
v3 = this.mCloudBackupXmlFile;
(( android.util.AtomicFile ) v3 ).failWrite ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 215 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to write local backup"; // const-string v4, "Failed to write local backup"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 217 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private Boolean updateAntiLowBrightnessAuthCloudState ( ) {
/* .locals 10 */
/* .line 274 */
v0 = this.mContext;
/* .line 275 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "fingerprint_feature_control"; // const-string v1, "fingerprint_feature_control"
final String v2 = "lowbrightness_auth"; // const-string v2, "lowbrightness_auth"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 277 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* const-string/jumbo v1, "updateAntiLowBrightnessAuthCloudState." */
final String v3 = "MiuiFingerprintCloudController"; // const-string v3, "MiuiFingerprintCloudController"
android.util.Slog .d ( v3,v1 );
/* .line 278 */
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 279 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "data.json() lowbrightness_auth: "; // const-string v5, "data.json() lowbrightness_auth: "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
final String v6 = ""; // const-string v6, ""
(( org.json.JSONObject ) v5 ).optString ( v2, v6 ); // invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v1 );
/* .line 280 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v1 ).optJSONObject ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 281 */
/* .local v1, "jsonObject":Lorg/json/JSONObject; */
final String v2 = "enable"; // const-string v2, "enable"
v5 = (( org.json.JSONObject ) v1 ).optBoolean ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z */
/* .line 282 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 283 */
/* const-string/jumbo v5, "setting_enable_default" */
v4 = (( org.json.JSONObject ) v1 ).optBoolean ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z */
/* .line 284 */
/* iget-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v8, 0x2 */
/* or-long/2addr v6, v8 */
/* iput-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* .line 285 */
v4 = this.mCloudEventsData;
/* iget-boolean v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z */
java.lang.Boolean .valueOf ( v6 );
/* .line 286 */
v2 = this.mCloudEventsData;
/* iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z */
java.lang.Boolean .valueOf ( v4 );
/* .line 287 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Update lowbrightness_auth, enable: "; // const-string v4, "Update lowbrightness_auth, enable: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", "; // const-string v4, ", "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ": "; // const-string v4, ": "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mLowBrightnessAuthSettingEnableDefault:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 289 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* .line 291 */
} // .end local v1 # "jsonObject":Lorg/json/JSONObject;
} // :cond_1
} // .end method
private Boolean updateAntiMistouchCloudState ( ) {
/* .locals 10 */
/* .line 252 */
v0 = this.mContext;
/* .line 253 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "fingerprint_feature_control"; // const-string v1, "fingerprint_feature_control"
final String v2 = "anti_mistouch"; // const-string v2, "anti_mistouch"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 255 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* const-string/jumbo v1, "updateAntiMistouchCloudState." */
final String v3 = "MiuiFingerprintCloudController"; // const-string v3, "MiuiFingerprintCloudController"
android.util.Slog .d ( v3,v1 );
/* .line 256 */
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 257 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "data.json() anti_mistouch: "; // const-string v5, "data.json() anti_mistouch: "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
final String v6 = ""; // const-string v6, ""
(( org.json.JSONObject ) v5 ).optString ( v2, v6 ); // invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v1 );
/* .line 258 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v1 ).optJSONObject ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 259 */
/* .local v1, "jsonObject":Lorg/json/JSONObject; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 260 */
final String v2 = "enable"; // const-string v2, "enable"
v5 = (( org.json.JSONObject ) v1 ).optBoolean ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z */
/* .line 261 */
/* const-string/jumbo v5, "valid_times_per_screenlock" */
v4 = (( org.json.JSONObject ) v1 ).optInt ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I */
/* .line 262 */
/* iget-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v8, 0x1 */
/* or-long/2addr v6, v8 */
/* iput-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* .line 263 */
v4 = this.mCloudEventsData;
/* iget-boolean v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z */
java.lang.Boolean .valueOf ( v6 );
/* .line 264 */
v2 = this.mCloudEventsData;
/* iget v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I */
java.lang.Integer .valueOf ( v4 );
/* .line 265 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Update anti_mistouch, enable: "; // const-string v4, "Update anti_mistouch, enable: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", "; // const-string v4, ", "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ": "; // const-string v4, ": "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mAntiMistouchValidTimes:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 267 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* .line 269 */
} // .end local v1 # "jsonObject":Lorg/json/JSONObject;
} // :cond_1
} // .end method
private Boolean updateDataFromCloudControl ( ) {
/* .locals 3 */
/* .line 157 */
final String v0 = "MiuiFingerprintCloudController"; // const-string v0, "MiuiFingerprintCloudController"
/* const-string/jumbo v1, "updateDataFromCloudControl." */
android.util.Slog .i ( v0,v1 );
/* .line 158 */
v0 = com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController.mFingerprintService;
/* if-nez v0, :cond_0 */
/* .line 159 */
v0 = this.mContext;
(( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController ) p0 ).getFingerprintService ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->getFingerprintService(Landroid/content/Context;)Landroid/hardware/fingerprint/IFingerprintService;
/* .line 161 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* .line 162 */
v0 = this.mCloudEventsData;
/* .line 163 */
v0 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->updateAntiMistouchCloudState()Z */
/* .line 164 */
/* .local v0, "updated":Z */
v1 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->updateAntiLowBrightnessAuthCloudState()Z */
/* or-int/2addr v0, v1 */
/* .line 165 */
v1 = /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->updateOcpDetectCloudState()Z */
/* or-int/2addr v0, v1 */
/* .line 166 */
v1 = this.mCloudListeners;
/* new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)V */
/* .line 169 */
} // .end method
private Boolean updateOcpDetectCloudState ( ) {
/* .locals 10 */
/* .line 296 */
v0 = this.mContext;
/* .line 297 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "fingerprint_feature_control"; // const-string v1, "fingerprint_feature_control"
final String v2 = "ocp_detect"; // const-string v2, "ocp_detect"
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v1,v2,v3,v4 );
/* .line 299 */
/* .local v0, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* const-string/jumbo v1, "updateOcpDetectCloudState." */
final String v3 = "MiuiFingerprintCloudController"; // const-string v3, "MiuiFingerprintCloudController"
android.util.Slog .d ( v3,v1 );
/* .line 300 */
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 301 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "data.json() ocp_detect: "; // const-string v5, "data.json() ocp_detect: "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
final String v6 = ""; // const-string v6, ""
(( org.json.JSONObject ) v5 ).optString ( v2, v6 ); // invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v1 );
/* .line 302 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
(( org.json.JSONObject ) v1 ).optJSONObject ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 303 */
/* .local v1, "jsonObject":Lorg/json/JSONObject; */
final String v2 = "enable"; // const-string v2, "enable"
v5 = (( org.json.JSONObject ) v1 ).optBoolean ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z */
/* .line 304 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 305 */
/* const-string/jumbo v5, "setting_enable_default" */
v4 = (( org.json.JSONObject ) v1 ).optBoolean ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z */
/* .line 306 */
/* iget-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* const-wide/16 v8, 0x4 */
/* or-long/2addr v6, v8 */
/* iput-wide v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
/* .line 307 */
v4 = this.mCloudEventsData;
/* iget-boolean v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z */
java.lang.Boolean .valueOf ( v6 );
/* .line 308 */
v2 = this.mCloudEventsData;
/* iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z */
java.lang.Boolean .valueOf ( v4 );
/* .line 309 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Update ocp_detect, enable: "; // const-string v4, "Update ocp_detect, enable: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = ", "; // const-string v4, ", "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ": "; // const-string v4, ": "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mOcpDetectSettingEnableDefault:Z */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 311 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* .line 313 */
} // .end local v1 # "jsonObject":Lorg/json/JSONObject;
} // :cond_1
} // .end method
private void writeFeatureBooleanToXml ( android.util.AtomicFile p0, java.io.FileOutputStream p1, com.android.modules.utils.TypedXmlSerializer p2, java.lang.String p3, java.lang.String p4, Boolean p5 ) {
/* .locals 3 */
/* .param p1, "writeFile" # Landroid/util/AtomicFile; */
/* .param p2, "outStream" # Ljava/io/FileOutputStream; */
/* .param p3, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .param p4, "attribute" # Ljava/lang/String; */
/* .param p5, "tag" # Ljava/lang/String; */
/* .param p6, "enable" # Z */
/* .line 223 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* .line 224 */
/* .line 225 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 229 */
/* .line 226 */
/* :catch_0 */
/* move-exception v0 */
/* .line 227 */
/* .local v0, "e":Ljava/io/IOException; */
(( android.util.AtomicFile ) p1 ).failWrite ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 228 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write local backup of value"; // const-string v2, "Failed to write local backup of value"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFingerprintCloudController"; // const-string v2, "MiuiFingerprintCloudController"
android.util.Slog .e ( v2,v1 );
/* .line 230 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void writeFeatureIntToXml ( android.util.AtomicFile p0, java.io.FileOutputStream p1, com.android.modules.utils.TypedXmlSerializer p2, java.lang.String p3, java.lang.String p4, Integer p5 ) {
/* .locals 3 */
/* .param p1, "writeFile" # Landroid/util/AtomicFile; */
/* .param p2, "outStream" # Ljava/io/FileOutputStream; */
/* .param p3, "out" # Lcom/android/modules/utils/TypedXmlSerializer; */
/* .param p4, "attribute" # Ljava/lang/String; */
/* .param p5, "tag" # Ljava/lang/String; */
/* .param p6, "value" # I */
/* .line 236 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* .line 237 */
/* .line 238 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 242 */
/* .line 239 */
/* :catch_0 */
/* move-exception v0 */
/* .line 240 */
/* .local v0, "e":Ljava/io/IOException; */
(( android.util.AtomicFile ) p1 ).failWrite ( p2 ); // invoke-virtual {p1, p2}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* .line 241 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to write local backup of value"; // const-string v2, "Failed to write local backup of value"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFingerprintCloudController"; // const-string v2, "MiuiFingerprintCloudController"
android.util.Slog .e ( v2,v1 );
/* .line 243 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
private void writeTagToXml ( org.xmlpull.v1.XmlSerializer p0, java.lang.String p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "out" # Lorg/xmlpull/v1/XmlSerializer; */
/* .param p2, "tag" # Ljava/lang/String; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 246 */
int v0 = 0; // const/4 v0, 0x0
/* .line 247 */
java.lang.String .valueOf ( p3 );
/* .line 248 */
/* .line 249 */
return;
} // .end method
/* # virtual methods */
protected void addCloudListener ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$CloudListener p0 ) {
/* .locals 3 */
/* .param p1, "listener" # Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener; */
/* .line 318 */
v0 = v0 = this.mCloudListeners;
/* if-nez v0, :cond_0 */
/* .line 319 */
v0 = this.mCloudListeners;
/* .line 320 */
/* iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->mCloudEventsSummary:J */
v2 = this.mCloudEventsData;
/* .line 322 */
} // :cond_0
return;
} // .end method
public android.hardware.fingerprint.IFingerprintService getFingerprintService ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 134 */
final String v0 = "fingerprint"; // const-string v0, "fingerprint"
android.os.ServiceManager .getService ( v0 );
android.hardware.fingerprint.IFingerprintService$Stub .asInterface ( v0 );
} // .end method
