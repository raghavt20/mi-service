.class public final Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub$$;
.super Ljava/lang/Object;
.source "FingerprintServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 20
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/biometrics/sensors/fingerprint/hidl/PowerFingerprintServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.biometrics.sensors.fingerprint.PowerFingerprintServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v0, Lcom/android/server/biometrics/sensors/face/FaceServiceStubImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/biometrics/sensors/face/FaceServiceStubImpl$Provider;-><init>()V

    const-string v1, "com.android.server.biometrics.sensors.face.FaceServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method
