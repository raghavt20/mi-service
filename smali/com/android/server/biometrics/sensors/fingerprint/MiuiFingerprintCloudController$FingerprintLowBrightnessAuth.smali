.class final Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;
.super Ljava/lang/Object;
.source "MiuiFingerprintCloudController.java"

# interfaces
.implements Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$CloudListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FingerprintLowBrightnessAuth"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;


# direct methods
.method private constructor <init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)V
    .locals 0

    .line 345
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)V

    return-void
.end method


# virtual methods
.method public onCloudUpdated(JLjava/util/Map;)V
    .locals 15
    .param p1, "summary"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 348
    .local p3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object v1, p0

    const-wide/16 v2, 0x2

    and-long v2, p1, v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 349
    const-string v0, "FingerprintLowBrightnessAuth onCloudUpdated"

    const-string v2, "MiuiFingerprintCloudController"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->-$$Nest$sfgetmFingerprintService()Landroid/hardware/fingerprint/IFingerprintService;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 352
    :try_start_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->-$$Nest$sfgetmFingerprintService()Landroid/hardware/fingerprint/IFingerprintService;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    iget-object v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->-$$Nest$fgetmLowBrightnessAuthEnable(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move v7, v0

    const/4 v8, 0x0

    invoke-interface/range {v3 .. v8}, Landroid/hardware/fingerprint/IFingerprintService;->extCmd(Landroid/os/IBinder;IIILjava/lang/String;)I

    .line 353
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->-$$Nest$sfgetmFingerprintService()Landroid/hardware/fingerprint/IFingerprintService;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xf

    iget-object v0, v1, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$FingerprintLowBrightnessAuth;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->-$$Nest$fgetmLowBrightnessAuthSettingEnableDefault(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x11

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v13, v0

    const/4 v14, 0x0

    invoke-interface/range {v9 .. v14}, Landroid/hardware/fingerprint/IFingerprintService;->extCmd(Landroid/os/IBinder;IIILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    goto :goto_2

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "RemoteException when FingerprintService extCmd"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 359
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :goto_2
    return-void
.end method
