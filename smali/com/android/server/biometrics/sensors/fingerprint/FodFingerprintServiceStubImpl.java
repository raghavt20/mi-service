public class com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl implements com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStub {
	 /* .source "FodFingerprintServiceStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl$AuthenticationFidoCallback; */
	 /* } */
} // .end annotation
/* # static fields */
protected static final Boolean DEBUG;
protected static final java.lang.String TAG;
/* # instance fields */
private final Integer CMD_NOTIFY_FOD_LOWBRIGHTNESS_ALLOW_STATE;
private final Integer CMD_NOTIFY_LOCK_OUT_TO_FOD_ENGINE;
private final Integer CMD_NOTIFY_MONITOR_STATE_TO_FOD_ENGINE;
private final Integer CMD_NOTIFY_TO_SURFACEFLINGER;
private final Integer CODE_PROCESS_CMD;
private final Integer DEFAULT_DISPLAY;
private final java.lang.String DEFAULT_PACKNAME;
private final java.lang.String FOD_LOCATION_PROPERTY;
private final java.lang.String FOD_LOCATION_WH;
private final java.lang.String FOD_LOCATION_WH_PROPERTY;
private final java.lang.String FOD_LOCATION_XY;
private final java.lang.String FOD_LOCATION_XY_PROPERTY;
private final java.lang.String FOD_SERVICE_NAME;
private Float GXZW_HEIGHT_PRCENT;
private Integer GXZW_ICON_HEIGHT;
private Integer GXZW_ICON_WIDTH;
private Integer GXZW_ICON_X;
private Integer GXZW_ICON_Y;
private Float GXZW_WIDTH_PRCENT;
private Float GXZW_X_PRCENT;
private Float GXZW_Y_PRCENT;
private final java.lang.String HEART_RATE_PACKAGE;
private final java.lang.String INTERFACE_DESCRIPTOR;
protected Boolean IS_FOD;
private Boolean IS_FODENGINE_ENABLED;
protected Boolean IS_FOD_LHBM;
private final Boolean IS_FOD_SENSOR_LOCATION_LOW;
private final java.lang.String KEYGUARD_PACKAGE;
private final java.lang.String MIUI_DEFAULT_RESOLUTION;
private final java.lang.String MIUI_DEFAULT_RESOLUTION_PROPERTY;
private Integer SCREEN_HEIGHT_PHYSICAL;
private Integer SCREEN_HEIGHT_PX;
private Integer SCREEN_WIDTH_PHYSICAL;
private Integer SCREEN_WIDTH_PX;
private final java.lang.String SETTING_PACKAGE;
private final Integer SETTING_VALUE_OFF;
private final Integer SETTING_VALUE_ON;
private final Integer SF_AUTH_START;
private final Integer SF_AUTH_STOP;
private final Integer SF_ENROLL_START;
private final Integer SF_ENROLL_STOP;
private final Integer SF_FINGERPRINT_NONE;
private final Integer SF_HEART_RATE_START;
private final Integer SF_HEART_RATE_STOP;
private final Integer SF_KEYGUARD_DETECT_START;
private final Integer SF_KEYGUARD_DETECT_STOP;
private final Integer TOUCH_AUTHEN;
private final Integer TOUCH_CANCEL;
private final Integer TOUCH_MODE;
private final java.lang.Object mFodLock;
private android.os.IBinder mFodService;
protected Integer mRootUserId;
private Integer mSettingPointerLocationState;
private Integer mSettingShowTapsState;
private Integer mSfPackageType;
private Integer mSfValue;
private Integer mTouchStatus;
/* # direct methods */
public static void $r8$lambda$znTbKHJw79KlSUpwHmPyTIUgWyA ( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->lambda$getFodServ$0()V */
	 return;
} // .end method
public com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ( ) {
	 /* .locals 8 */
	 /* .line 47 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 51 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mFodLock = v0;
	 /* .line 52 */
	 final String v0 = "android.app.fod.ICallback"; // const-string v0, "android.app.fod.ICallback"
	 this.FOD_SERVICE_NAME = v0;
	 /* .line 53 */
	 this.INTERFACE_DESCRIPTOR = v0;
	 /* .line 54 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CODE_PROCESS_CMD:I */
	 /* .line 55 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mRootUserId:I */
	 /* .line 57 */
	 final String v2 = "com.mi.health"; // const-string v2, "com.mi.health"
	 this.HEART_RATE_PACKAGE = v2;
	 /* .line 58 */
	 final String v2 = "com.android.systemui"; // const-string v2, "com.android.systemui"
	 this.KEYGUARD_PACKAGE = v2;
	 /* .line 59 */
	 final String v2 = "com.android.settings"; // const-string v2, "com.android.settings"
	 this.SETTING_PACKAGE = v2;
	 /* .line 61 */
	 final String v2 = ""; // const-string v2, ""
	 this.DEFAULT_PACKNAME = v2;
	 /* .line 62 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->TOUCH_AUTHEN:I */
	 /* .line 63 */
	 /* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->TOUCH_CANCEL:I */
	 /* .line 64 */
	 /* const/16 v3, 0xa */
	 /* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->TOUCH_MODE:I */
	 /* .line 65 */
	 int v3 = -1; // const/4 v3, -0x1
	 /* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mTouchStatus:I */
	 /* .line 66 */
	 /* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I */
	 /* .line 67 */
	 /* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfPackageType:I */
	 /* .line 69 */
	 /* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->DEFAULT_DISPLAY:I */
	 /* .line 72 */
	 /* const/16 v4, 0x7987 */
	 /* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CMD_NOTIFY_TO_SURFACEFLINGER:I */
	 /* .line 73 */
	 /* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_FINGERPRINT_NONE:I */
	 /* .line 74 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_ENROLL_START:I */
	 /* .line 75 */
	 int v4 = 2; // const/4 v4, 0x2
	 /* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_ENROLL_STOP:I */
	 /* .line 76 */
	 int v4 = 3; // const/4 v4, 0x3
	 /* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_AUTH_START:I */
	 /* .line 77 */
	 int v4 = 4; // const/4 v4, 0x4
	 /* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_AUTH_STOP:I */
	 /* .line 78 */
	 int v5 = 5; // const/4 v5, 0x5
	 /* iput v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_HEART_RATE_START:I */
	 /* .line 79 */
	 int v6 = 6; // const/4 v6, 0x6
	 /* iput v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_HEART_RATE_STOP:I */
	 /* .line 80 */
	 int v6 = 7; // const/4 v6, 0x7
	 /* iput v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_KEYGUARD_DETECT_START:I */
	 /* .line 81 */
	 /* const/16 v7, 0x8 */
	 /* iput v7, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SF_KEYGUARD_DETECT_STOP:I */
	 /* .line 83 */
	 /* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CMD_NOTIFY_MONITOR_STATE_TO_FOD_ENGINE:I */
	 /* .line 84 */
	 /* iput v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CMD_NOTIFY_LOCK_OUT_TO_FOD_ENGINE:I */
	 /* .line 85 */
	 /* iput v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->CMD_NOTIFY_FOD_LOWBRIGHTNESS_ALLOW_STATE:I */
	 /* .line 87 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SETTING_VALUE_ON:I */
	 /* .line 88 */
	 /* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SETTING_VALUE_OFF:I */
	 /* .line 89 */
	 /* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingShowTapsState:I */
	 /* .line 90 */
	 /* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingPointerLocationState:I */
	 /* .line 92 */
	 final String v0 = "ro.hardware.fp.fod"; // const-string v0, "ro.hardware.fp.fod"
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 /* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD:Z */
	 /* .line 94 */
	 final String v0 = "ro.hardware.fp.fod.location"; // const-string v0, "ro.hardware.fp.fod.location"
	 this.FOD_LOCATION_PROPERTY = v0;
	 /* .line 95 */
	 android.os.SystemProperties .get ( v0,v2 );
	 final String v4 = "low"; // const-string v4, "low"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD_SENSOR_LOCATION_LOW:Z */
	 /* .line 97 */
	 final String v0 = "persist.vendor.sys.fp.fod.location.X_Y"; // const-string v0, "persist.vendor.sys.fp.fod.location.X_Y"
	 this.FOD_LOCATION_XY_PROPERTY = v0;
	 /* .line 98 */
	 android.os.SystemProperties .get ( v0,v2 );
	 this.FOD_LOCATION_XY = v0;
	 /* .line 100 */
	 final String v0 = "persist.vendor.sys.fp.fod.size.width_height"; // const-string v0, "persist.vendor.sys.fp.fod.size.width_height"
	 this.FOD_LOCATION_WH_PROPERTY = v0;
	 /* .line 101 */
	 android.os.SystemProperties .get ( v0,v2 );
	 this.FOD_LOCATION_WH = v0;
	 /* .line 103 */
	 final String v0 = "ro.vendor.localhbm.enable"; // const-string v0, "ro.vendor.localhbm.enable"
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 /* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD_LHBM:Z */
	 /* .line 104 */
	 final String v0 = "ro.hardware.fp.fod.touch.ctl.version"; // const-string v0, "ro.hardware.fp.fod.touch.ctl.version"
	 android.os.SystemProperties .get ( v0,v2 );
	 final String v1 = "2.0"; // const-string v1, "2.0"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* iput-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FODENGINE_ENABLED:Z */
	 /* .line 106 */
	 final String v0 = "persist.sys.miui_default_resolution"; // const-string v0, "persist.sys.miui_default_resolution"
	 this.MIUI_DEFAULT_RESOLUTION_PROPERTY = v0;
	 /* .line 107 */
	 android.os.SystemProperties .get ( v0 );
	 this.MIUI_DEFAULT_RESOLUTION = v0;
	 /* .line 110 */
	 /* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I */
	 /* .line 111 */
	 /* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I */
	 /* .line 114 */
	 /* const/high16 v0, -0x40800000 # -1.0f */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_X_PRCENT:F */
	 /* .line 115 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_Y_PRCENT:F */
	 /* .line 116 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_WIDTH_PRCENT:F */
	 /* .line 117 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_HEIGHT_PRCENT:F */
	 /* .line 118 */
	 /* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PX:I */
	 /* .line 119 */
	 /* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PX:I */
	 /* .line 122 */
	 /* const/16 v0, 0x1c5 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I */
	 /* .line 123 */
	 /* const/16 v0, 0x668 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I */
	 /* .line 124 */
	 /* const/16 v0, 0xad */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I */
	 /* .line 125 */
	 /* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I */
	 return;
} // .end method
private Integer cmdSendToKeyguard ( Integer p0, java.lang.String p1 ) {
	 /* .locals 3 */
	 /* .param p1, "cmd" # I */
	 /* .param p2, "packName" # Ljava/lang/String; */
	 /* .line 136 */
	 int v0 = 2; // const/4 v0, 0x2
	 int v1 = 1; // const/4 v1, 0x1
	 final String v2 = "com.android.settings"; // const-string v2, "com.android.settings"
	 /* packed-switch p1, :pswitch_data_0 */
	 /* .line 152 */
	 /* :pswitch_0 */
	 /* .line 140 */
	 /* :pswitch_1 */
	 /* .line 138 */
	 /* :pswitch_2 */
	 /* .line 147 */
	 /* :pswitch_3 */
	 v1 = 	 (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v1, :cond_0 */
	 /* .line 148 */
	 /* .line 150 */
} // :cond_0
/* .line 142 */
/* :pswitch_4 */
v0 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 143 */
/* .line 145 */
} // :cond_1
/* :pswitch_data_0 */
/* .packed-switch 0x7 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private Integer getCmd4SurfaceFlinger ( Integer p0, Integer p1, com.android.server.biometrics.sensors.BaseClientMonitor p2 ) {
/* .locals 3 */
/* .param p1, "cmd" # I */
/* .param p2, "param" # I */
/* .param p3, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .line 390 */
int v0 = -1; // const/4 v0, -0x1
/* .line 391 */
/* .local v0, "res":I */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 437 */
/* :sswitch_0 */
int v0 = 6; // const/4 v0, 0x6
/* .line 438 */
/* .line 433 */
/* :sswitch_1 */
int v0 = 5; // const/4 v0, 0x5
/* .line 434 */
/* .line 443 */
/* :sswitch_2 */
/* const/16 v0, 0x8 */
/* .line 444 */
/* .line 440 */
/* :sswitch_3 */
int v0 = 7; // const/4 v0, 0x7
/* .line 441 */
/* .line 396 */
/* :sswitch_4 */
/* if-nez p2, :cond_0 */
/* .line 397 */
int v0 = 2; // const/4 v0, 0x2
/* .line 399 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 401 */
/* .line 427 */
/* :sswitch_5 */
int v0 = 2; // const/4 v0, 0x2
/* .line 428 */
/* .line 393 */
/* :sswitch_6 */
int v0 = 1; // const/4 v0, 0x1
/* .line 394 */
/* .line 430 */
/* :sswitch_7 */
int v0 = 0; // const/4 v0, 0x0
/* .line 431 */
/* .line 403 */
/* :sswitch_8 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 404 */
v1 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p3 ).getStatsAction ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getStatsAction()I
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_1 */
/* .line 405 */
int v0 = 2; // const/4 v0, 0x2
/* .line 407 */
} // :cond_1
v1 = (( com.android.server.biometrics.sensors.BaseClientMonitor ) p3 ).getStatsAction ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getStatsAction()I
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_3 */
/* .line 408 */
int v0 = 4; // const/4 v0, 0x4
/* .line 416 */
/* :sswitch_9 */
/* if-nez p2, :cond_2 */
/* .line 417 */
int v0 = 3; // const/4 v0, 0x3
/* .line 419 */
} // :cond_2
int v0 = 4; // const/4 v0, 0x4
/* .line 421 */
/* .line 424 */
/* :sswitch_a */
int v0 = 4; // const/4 v0, 0x4
/* .line 425 */
/* .line 413 */
/* :sswitch_b */
int v0 = 3; // const/4 v0, 0x3
/* .line 414 */
/* nop */
/* .line 448 */
} // :cond_3
} // :goto_0
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_b */
/* 0x2 -> :sswitch_a */
/* 0x3 -> :sswitch_9 */
/* 0x4 -> :sswitch_8 */
/* 0x5 -> :sswitch_7 */
/* 0x6 -> :sswitch_a */
/* 0x7 -> :sswitch_6 */
/* 0x8 -> :sswitch_5 */
/* 0x9 -> :sswitch_4 */
/* 0xc -> :sswitch_3 */
/* 0xd -> :sswitch_2 */
/* 0x61a81 -> :sswitch_1 */
/* 0x61a84 -> :sswitch_0 */
/* 0x61a86 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private Boolean isFodMonitorState ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "sf_status" # I */
/* .line 375 */
int v0 = 0; // const/4 v0, 0x0
/* .line 377 */
/* .local v0, "result":Z */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 381 */
/* :pswitch_1 */
int v0 = 1; // const/4 v0, 0x1
/* .line 382 */
/* nop */
/* .line 386 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void lambda$getFodServ$0 ( ) { //synthethic
/* .locals 3 */
/* .line 502 */
v0 = this.mFodLock;
/* monitor-enter v0 */
/* .line 503 */
try { // :try_start_0
final String v1 = "[FingerprintService]FodFingerprintServiceStubImpl"; // const-string v1, "[FingerprintService]FodFingerprintServiceStubImpl"
final String v2 = "fodCallBack Service Died."; // const-string v2, "fodCallBack Service Died."
android.util.Slog .e ( v1,v2 );
/* .line 504 */
int v1 = 0; // const/4 v1, 0x0
this.mFodService = v1;
/* .line 505 */
/* monitor-exit v0 */
/* .line 506 */
return;
/* .line 505 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Integer notifySurfaceFlinger ( android.content.Context p0, Integer p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "msg" # I */
/* .param p3, "packName" # Ljava/lang/String; */
/* .param p4, "value" # I */
/* .param p5, "cmd" # I */
/* .line 452 */
int v0 = -1; // const/4 v0, -0x1
/* .line 453 */
/* .local v0, "resBack":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 455 */
/* .local v1, "packageType":I */
/* iget-boolean v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD:Z */
/* if-nez v2, :cond_0 */
/* .line 456 */
/* .line 459 */
} // :cond_0
v2 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) p0 ).isKeyguard ( p3 ); // invoke-virtual {p0, p3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isKeyguard(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 1; // const/4 v2, 0x1
/* if-ne p5, v2, :cond_1 */
/* .line 460 */
int v1 = 1; // const/4 v1, 0x1
/* .line 463 */
} // :cond_1
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I */
final String v3 = ", packageType: "; // const-string v3, ", packageType: "
final String v4 = ", value: "; // const-string v4, ", value: "
final String v5 = "[FingerprintService]FodFingerprintServiceStubImpl"; // const-string v5, "[FingerprintService]FodFingerprintServiceStubImpl"
/* if-ne v2, p4, :cond_2 */
/* iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfPackageType:I */
/* if-ne v2, v1, :cond_2 */
/* .line 465 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "duplicate notifySurfaceFlinger msg: "; // const-string v6, "duplicate notifySurfaceFlinger msg: "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v2 );
/* .line 466 */
/* .line 469 */
} // :cond_2
final String v2 = "SurfaceFlinger"; // const-string v2, "SurfaceFlinger"
android.os.ServiceManager .getService ( v2 );
/* .line 470 */
/* .local v2, "flinger":Landroid/os/IBinder; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 471 */
android.os.Parcel .obtain ( );
/* .line 472 */
/* .local v6, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 473 */
/* .local v7, "reply":Landroid/os/Parcel; */
final String v8 = "android.ui.ISurfaceComposer"; // const-string v8, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v6 ).writeInterfaceToken ( v8 ); // invoke-virtual {v6, v8}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 474 */
(( android.os.Parcel ) v6 ).writeInt ( p4 ); // invoke-virtual {v6, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 475 */
(( android.os.Parcel ) v6 ).writeInt ( v1 ); // invoke-virtual {v6, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 477 */
int v8 = 0; // const/4 v8, 0x0
try { // :try_start_0
/* .line 478 */
(( android.os.Parcel ) v7 ).readException ( ); // invoke-virtual {v7}, Landroid/os/Parcel;->readException()V
/* .line 479 */
v8 = (( android.os.Parcel ) v7 ).readInt ( ); // invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v0, v8 */
/* .line 483 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v6 ).recycle ( ); // invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V
/* .line 484 */
(( android.os.Parcel ) v7 ).recycle ( ); // invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V
/* .line 485 */
/* .line 483 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 480 */
/* :catch_0 */
/* move-exception v8 */
/* .line 481 */
/* .local v8, "ex":Landroid/os/RemoteException; */
try { // :try_start_1
final String v9 = "Failed to notifySurfaceFlinger"; // const-string v9, "Failed to notifySurfaceFlinger"
android.util.Slog .e ( v5,v9,v8 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 483 */
/* nop */
} // .end local v8 # "ex":Landroid/os/RemoteException;
} // :goto_1
(( android.os.Parcel ) v6 ).recycle ( ); // invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V
/* .line 484 */
(( android.os.Parcel ) v7 ).recycle ( ); // invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V
/* .line 485 */
/* throw v3 */
/* .line 488 */
} // .end local v6 # "data":Landroid/os/Parcel;
} // .end local v7 # "reply":Landroid/os/Parcel;
} // :cond_3
} // :goto_2
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "notifySurfaceFlinger msg: "; // const-string v7, "notifySurfaceFlinger msg: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p4 ); // invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v3 );
/* .line 490 */
/* iput p4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I */
/* .line 491 */
/* iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfPackageType:I */
/* .line 493 */
} // .end method
private void resetDefaultValue ( ) {
/* .locals 1 */
/* .line 695 */
/* const/16 v0, 0x1c5 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I */
/* .line 696 */
/* const/16 v0, 0x668 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I */
/* .line 697 */
/* const/16 v0, 0xad */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I */
/* .line 698 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I */
/* .line 699 */
return;
} // .end method
private void screenWhPx ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 686 */
final String v0 = "display"; // const-string v0, "display"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/display/DisplayManager; */
/* .line 687 */
/* .local v0, "displayManager":Landroid/hardware/display/DisplayManager; */
/* new-instance v1, Landroid/util/DisplayMetrics; */
/* invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V */
/* .line 688 */
/* .local v1, "dm":Landroid/util/DisplayMetrics; */
int v2 = 0; // const/4 v2, 0x0
(( android.hardware.display.DisplayManager ) v0 ).getDisplay ( v2 ); // invoke-virtual {v0, v2}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
(( android.view.Display ) v3 ).getRealMetrics ( v1 ); // invoke-virtual {v3, v1}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V
/* .line 689 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v3 ).getConfiguration ( ); // invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* iget v3, v3, Landroid/content/res/Configuration;->orientation:I */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_0 */
/* move v2, v4 */
/* .line 690 */
/* .local v2, "orientation":Z */
} // :cond_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I */
} // :cond_1
/* iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I */
} // :goto_0
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PX:I */
/* .line 691 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I */
} // :cond_2
/* iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I */
} // :goto_1
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PX:I */
/* .line 692 */
return;
} // .end method
/* # virtual methods */
public void calculateFodSensorLocation ( android.content.Context p0 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 609 */
final String v0 = ","; // const-string v0, ","
/* iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I */
int v2 = -1; // const/4 v2, -0x1
/* if-ne v1, v2, :cond_0 */
/* .line 610 */
(( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) p0 ).phySicalScreenPx ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->phySicalScreenPx(Landroid/content/Context;)V
/* .line 612 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->screenWhPx(Landroid/content/Context;)V */
/* .line 614 */
final String v1 = "persist.vendor.sys.fp.fod.location.X_Y"; // const-string v1, "persist.vendor.sys.fp.fod.location.X_Y"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
/* .line 615 */
/* .local v1, "xyString":Ljava/lang/String; */
final String v3 = "persist.vendor.sys.fp.fod.size.width_height"; // const-string v3, "persist.vendor.sys.fp.fod.size.width_height"
android.os.SystemProperties .get ( v3,v2 );
/* .line 624 */
/* .local v2, "whString":Ljava/lang/String; */
v3 = (( java.lang.String ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_2 */
v3 = (( java.lang.String ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* goto/16 :goto_1 */
/* .line 630 */
} // :cond_1
try { // :try_start_0
(( java.lang.String ) v1 ).split ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v4 = 0; // const/4 v4, 0x0
/* aget-object v3, v3, v4 */
v3 = java.lang.Integer .parseInt ( v3 );
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I */
/* .line 631 */
(( java.lang.String ) v1 ).split ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v5 = 1; // const/4 v5, 0x1
/* aget-object v3, v3, v5 */
v3 = java.lang.Integer .parseInt ( v3 );
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I */
/* .line 632 */
(( java.lang.String ) v2 ).split ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v3, v3, v4 */
v3 = java.lang.Integer .parseInt ( v3 );
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I */
/* .line 633 */
(( java.lang.String ) v2 ).split ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v0, v0, v5 */
v0 = java.lang.Integer .parseInt ( v0 );
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I */
/* .line 641 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I */
/* iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I */
v0 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) p0 ).getPrcent ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getPrcent(II)F
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_X_PRCENT:F */
/* .line 642 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I */
/* iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I */
v0 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) p0 ).getPrcent ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getPrcent(II)F
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_Y_PRCENT:F */
/* .line 643 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I */
/* iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I */
v0 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) p0 ).getPrcent ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getPrcent(II)F
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_WIDTH_PRCENT:F */
/* .line 644 */
/* iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I */
/* iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I */
v0 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) p0 ).getPrcent ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getPrcent(II)F
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_HEIGHT_PRCENT:F */
/* .line 646 */
/* iget v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PX:I */
/* int-to-float v4, v3 */
/* iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_X_PRCENT:F */
/* mul-float/2addr v4, v5 */
/* float-to-int v4, v4 */
/* iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I */
/* .line 647 */
/* iget v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PX:I */
/* int-to-float v5, v4 */
/* iget v6, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_Y_PRCENT:F */
/* mul-float/2addr v5, v6 */
/* float-to-int v5, v5 */
/* iput v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I */
/* .line 648 */
/* int-to-float v3, v3 */
/* iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_WIDTH_PRCENT:F */
/* mul-float/2addr v3, v5 */
/* float-to-int v3, v3 */
/* iput v3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I */
/* .line 649 */
/* int-to-float v3, v4 */
/* mul-float/2addr v3, v0 */
/* float-to-int v0, v3 */
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 653 */
/* .line 650 */
/* :catch_0 */
/* move-exception v0 */
/* .line 651 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 652 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->resetDefaultValue()V */
/* .line 654 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 625 */
} // :cond_2
} // :goto_1
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->resetDefaultValue()V */
/* .line 626 */
return;
} // .end method
public Integer fodCallBack ( android.content.Context p0, Integer p1, Integer p2, com.android.server.biometrics.sensors.BaseClientMonitor p3 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "cmd" # I */
/* .param p3, "param" # I */
/* .param p4, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* .line 158 */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 159 */
(( com.android.server.biometrics.sensors.BaseClientMonitor ) p4 ).getOwnerString ( ); // invoke-virtual {p4}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getOwnerString()Ljava/lang/String;
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move v2, p2 */
/* move v3, p3 */
/* move-object v5, p4 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I */
/* .line 162 */
} // :cond_0
final String v5 = ""; // const-string v5, ""
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move-object v6, p4 */
v0 = /* invoke-virtual/range {v1 ..v6}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I */
} // .end method
public synchronized Integer fodCallBack ( android.content.Context p0, Integer p1, Integer p2, java.lang.String p3, com.android.server.biometrics.sensors.BaseClientMonitor p4 ) {
/* .locals 18 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "cmd" # I */
/* .param p3, "param" # I */
/* .param p4, "packName" # Ljava/lang/String; */
/* .param p5, "client" # Lcom/android/server/biometrics/sensors/BaseClientMonitor; */
/* move-object/from16 v7, p0 */
/* move/from16 v8, p2 */
/* move/from16 v9, p3 */
/* move-object/from16 v10, p4 */
/* move-object/from16 v11, p5 */
/* monitor-enter p0 */
/* .line 167 */
int v12 = -1; // const/4 v12, -0x1
/* .line 169 */
/* .local v12, "resBack":I */
try { // :try_start_0
final String v0 = "com.mi.health"; // const-string v0, "com.mi.health"
v0 = (( java.lang.String ) v0 ).equals ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* iget-boolean v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 170 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isFingerprintClient ( v11 ); // invoke-virtual {v0, v11}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerprintClient(Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* if-nez v0, :cond_1 */
/* .line 171 */
} // .end local p0 # "this":Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;
} // :cond_0
/* monitor-exit p0 */
/* .line 174 */
} // :cond_1
try { // :try_start_1
v0 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) v7 ).getCmd4Touch ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getCmd4Touch(II)I
/* move v13, v0 */
/* .line 175 */
/* .local v13, "cmdSendToTouch":I */
v0 = /* invoke-direct {v7, v8, v9, v11}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getCmd4SurfaceFlinger(IILcom/android/server/biometrics/sensors/BaseClientMonitor;)I */
/* move v14, v0 */
/* .line 176 */
/* .local v14, "cmdSendToSf":I */
int v0 = 0; // const/4 v0, 0x0
/* .line 177 */
/* .local v0, "touchFod":Z */
int v15 = 0; // const/4 v15, 0x0
/* .line 181 */
/* .local v15, "alreadySetTouch":Z */
/* iget-boolean v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FODENGINE_ENABLED:Z */
int v2 = 4; // const/4 v2, 0x4
int v6 = 5; // const/4 v6, 0x5
/* if-nez v1, :cond_2 */
/* if-ne v8, v2, :cond_2 */
/* if-ne v9, v6, :cond_2 */
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
v1 = (( java.lang.String ) v1 ).equals ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* if-nez v1, :cond_2 */
/* .line 182 */
/* monitor-exit p0 */
/* .line 185 */
} // :cond_2
try { // :try_start_2
/* iget-boolean v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FODENGINE_ENABLED:Z */
int v5 = 6; // const/4 v5, 0x6
int v4 = 3; // const/4 v4, 0x3
int v3 = -1; // const/4 v3, -0x1
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 186 */
/* if-eq v14, v3, :cond_9 */
/* .line 187 */
/* iget v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I */
/* if-ne v1, v4, :cond_4 */
/* if-eq v14, v6, :cond_3 */
/* if-ne v14, v5, :cond_4 */
/* .line 188 */
} // :cond_3
final String v1 = "[FingerprintService]FodFingerprintServiceStubImpl"; // const-string v1, "[FingerprintService]FodFingerprintServiceStubImpl"
/* const-string/jumbo v2, "skip notify heart rate as fingerprint auth start already" */
android.util.Slog .d ( v1,v2 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 189 */
/* monitor-exit p0 */
/* .line 192 */
} // :cond_4
/* const/16 v1, 0xc */
/* if-ne v8, v1, :cond_5 */
/* .line 193 */
try { // :try_start_3
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
int v1 = 2; // const/4 v1, 0x2
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v6,v1 );
/* .line 194 */
} // :cond_5
/* const/16 v1, 0xd */
/* if-ne v8, v1, :cond_6 */
/* .line 195 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v6,v2 );
/* .line 198 */
} // :cond_6
} // :goto_0
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
int v1 = 4; // const/4 v1, 0x4
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v1,v14 );
/* .line 199 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v1 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) v7 ).isKeyguard ( v10 ); // invoke-virtual {v7, v10}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isKeyguard(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_7
int v3 = 1; // const/4 v3, 0x1
/* if-ne v8, v3, :cond_8 */
/* move v1, v3 */
} // :cond_7
int v3 = 1; // const/4 v3, 0x1
} // :cond_8
/* move v1, v2 */
} // :goto_1
int v5 = 7; // const/4 v5, 0x7
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v5,v1 );
/* .line 200 */
/* const/16 v5, 0x7987 */
/* move-object/from16 v1, p0 */
/* move v6, v2 */
/* move-object/from16 v2, p1 */
/* move/from16 v16, v0 */
/* move v0, v3 */
} // .end local v0 # "touchFod":Z
/* .local v16, "touchFod":Z */
/* move v3, v5 */
/* move v5, v4 */
/* move-object/from16 v4, p4 */
/* move v0, v5 */
/* move v5, v14 */
/* move v0, v6 */
/* move/from16 v6, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->notifySurfaceFlinger(Landroid/content/Context;ILjava/lang/String;II)I */
/* .line 186 */
} // .end local v16 # "touchFod":Z
/* .restart local v0 # "touchFod":Z */
} // :cond_9
/* move/from16 v16, v0 */
/* move v0, v2 */
/* .line 224 */
} // .end local v0 # "touchFod":Z
/* .restart local v16 # "touchFod":Z */
} // :goto_2
/* move/from16 v1, v16 */
/* goto/16 :goto_8 */
/* .line 204 */
} // .end local v16 # "touchFod":Z
/* .restart local v0 # "touchFod":Z */
} // :cond_a
/* move/from16 v16, v0 */
/* move v0, v2 */
} // .end local v0 # "touchFod":Z
/* .restart local v16 # "touchFod":Z */
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
/* const/16 v4, 0xa */
/* if-nez v1, :cond_c */
v1 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) v7 ).isKeyguard ( v10 ); // invoke-virtual {v7, v10}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isKeyguard(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_b
} // :cond_b
/* move v0, v6 */
/* .line 205 */
} // :cond_c
} // :goto_3
/* if-eq v13, v3, :cond_d */
v2 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) v7 ).setTouchMode ( v0, v4, v13 ); // invoke-virtual {v7, v0, v4, v13}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->setTouchMode(III)Z
} // :cond_d
/* move v2, v0 */
} // :goto_4
/* move/from16 v16, v2 */
/* .line 206 */
int v15 = 1; // const/4 v15, 0x1
/* .line 207 */
if ( v16 != null) { // if-eqz v16, :cond_e
/* move v1, v13 */
} // :cond_e
/* iget v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mTouchStatus:I */
} // :goto_5
/* iput v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mTouchStatus:I */
/* .line 208 */
/* if-eq v14, v3, :cond_f */
/* .line 209 */
/* const/16 v17, 0x7987 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move v0, v3 */
/* move/from16 v3, v17 */
/* move-object/from16 v4, p4 */
/* move v0, v5 */
/* move v5, v14 */
/* move v0, v6 */
/* move/from16 v6, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->notifySurfaceFlinger(Landroid/content/Context;ILjava/lang/String;II)I */
/* .line 208 */
} // :cond_f
/* move v0, v6 */
/* .line 212 */
} // :goto_6
/* iget v1, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSfValue:I */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_11 */
/* if-eq v14, v0, :cond_10 */
int v0 = 6; // const/4 v0, 0x6
/* if-ne v14, v0, :cond_11 */
/* .line 213 */
} // :cond_10
final String v0 = "[FingerprintService]FodFingerprintServiceStubImpl"; // const-string v0, "[FingerprintService]FodFingerprintServiceStubImpl"
/* const-string/jumbo v1, "skip notify heart rate as fingerprint auth start already" */
android.util.Slog .d ( v0,v1 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 214 */
/* monitor-exit p0 */
/* .line 216 */
} // :cond_11
/* if-nez v15, :cond_14 */
/* .line 217 */
int v0 = -1; // const/4 v0, -0x1
/* if-eq v13, v0, :cond_12 */
/* const/16 v0, 0xa */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_4
v2 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) v7 ).setTouchMode ( v1, v0, v13 ); // invoke-virtual {v7, v1, v0, v13}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->setTouchMode(III)Z
} // :cond_12
int v2 = 0; // const/4 v2, 0x0
} // :goto_7
/* move v0, v2 */
/* .line 218 */
} // .end local v16 # "touchFod":Z
/* .restart local v0 # "touchFod":Z */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v14, v1, :cond_13 */
/* .line 219 */
/* const/16 v3, 0x7987 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v4, p4 */
/* move v5, v14 */
/* move/from16 v6, p2 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->notifySurfaceFlinger(Landroid/content/Context;ILjava/lang/String;II)I */
/* .line 224 */
} // :cond_13
/* move v1, v0 */
/* .line 216 */
} // .end local v0 # "touchFod":Z
/* .restart local v16 # "touchFod":Z */
} // :cond_14
/* move/from16 v1, v16 */
/* .line 224 */
} // .end local v16 # "touchFod":Z
/* .local v1, "touchFod":Z */
} // :goto_8
/* iget-boolean v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD_LHBM:Z */
/* if-nez v0, :cond_18 */
/* .line 225 */
v0 = /* invoke-direct {v7, v14}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isFodMonitorState(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_16
int v0 = 3; // const/4 v0, 0x3
/* if-eq v8, v0, :cond_16 */
/* const/16 v0, 0x9 */
/* if-eq v8, v0, :cond_16 */
/* .line 226 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
/* const-string/jumbo v2, "show_touches" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v2,v3 );
/* iput v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingShowTapsState:I */
/* .line 227 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
final String v2 = "pointer_location"; // const-string v2, "pointer_location"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getInt ( v0,v2,v3 );
/* iput v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingPointerLocationState:I */
/* .line 229 */
/* iget v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingShowTapsState:I */
if ( v0 != null) { // if-eqz v0, :cond_15
/* .line 230 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
/* const-string/jumbo v2, "show_touches" */
int v3 = 0; // const/4 v3, 0x0
android.provider.Settings$System .putInt ( v0,v2,v3 );
/* .line 231 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
/* const-string/jumbo v2, "show_touches_preventrecord" */
android.provider.Settings$System .putInt ( v0,v2,v3 );
/* .line 234 */
} // :cond_15
/* iget v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingPointerLocationState:I */
if ( v0 != null) { // if-eqz v0, :cond_18
/* .line 235 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
final String v2 = "pointer_location"; // const-string v2, "pointer_location"
int v3 = 0; // const/4 v3, 0x0
android.provider.Settings$System .putInt ( v0,v2,v3 );
/* .line 236 */
} // :cond_16
v0 = /* invoke-direct {v7, v14}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->isFodMonitorState(I)Z */
/* if-nez v0, :cond_18 */
/* .line 237 */
/* iget v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingShowTapsState:I */
if ( v0 != null) { // if-eqz v0, :cond_17
/* .line 238 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
/* const-string/jumbo v2, "show_touches" */
int v3 = 1; // const/4 v3, 0x1
android.provider.Settings$System .putInt ( v0,v2,v3 );
/* .line 239 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
/* const-string/jumbo v2, "show_touches_preventrecord" */
android.provider.Settings$System .putInt ( v0,v2,v3 );
/* .line 242 */
} // :cond_17
/* iget v0, v7, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->mSettingPointerLocationState:I */
if ( v0 != null) { // if-eqz v0, :cond_18
/* .line 243 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
final String v2 = "pointer_location"; // const-string v2, "pointer_location"
int v3 = 1; // const/4 v3, 0x1
android.provider.Settings$System .putInt ( v0,v2,v3 );
/* .line 247 */
} // :cond_18
} // :goto_9
android.os.Parcel .obtain ( );
/* move-object v2, v0 */
/* .line 248 */
/* .local v2, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* move-object v3, v0 */
/* .line 250 */
/* .local v3, "reply":Landroid/os/Parcel; */
try { // :try_start_5
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getFodServ()Landroid/os/IBinder; */
/* .line 251 */
/* .local v0, "remote":Landroid/os/IBinder; */
/* if-nez v0, :cond_19 */
/* .line 252 */
final String v4 = "[FingerprintService]FodFingerprintServiceStubImpl"; // const-string v4, "[FingerprintService]FodFingerprintServiceStubImpl"
final String v5 = "fodCallBack, service not found"; // const-string v5, "fodCallBack, service not found"
android.util.Slog .e ( v4,v5 );
/* .line 254 */
} // :cond_19
final String v4 = "android.app.fod.ICallback"; // const-string v4, "android.app.fod.ICallback"
(( android.os.Parcel ) v2 ).writeInterfaceToken ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 255 */
v4 = /* invoke-direct {v7, v8, v10}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->cmdSendToKeyguard(ILjava/lang/String;)I */
(( android.os.Parcel ) v2 ).writeInt ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 256 */
(( android.os.Parcel ) v2 ).writeInt ( v9 ); // invoke-virtual {v2, v9}, Landroid/os/Parcel;->writeInt(I)V
/* .line 257 */
(( android.os.Parcel ) v2 ).writeString ( v10 ); // invoke-virtual {v2, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 258 */
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
/* .line 259 */
(( android.os.Parcel ) v3 ).readException ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
/* .line 260 */
v4 = (( android.os.Parcel ) v3 ).readInt ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I
/* :try_end_5 */
/* .catch Landroid/os/RemoteException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* move v12, v4 */
/* .line 265 */
} // .end local v0 # "remote":Landroid/os/IBinder;
} // :goto_a
try { // :try_start_6
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 266 */
} // :goto_b
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 267 */
/* .line 265 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 262 */
/* :catch_0 */
/* move-exception v0 */
/* .line 263 */
/* .local v0, "e":Landroid/os/RemoteException; */
try { // :try_start_7
final String v4 = "[FingerprintService]FodFingerprintServiceStubImpl"; // const-string v4, "[FingerprintService]FodFingerprintServiceStubImpl"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "fodCallBack failed, "; // const-string v6, "fodCallBack failed, "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_0 */
/* .line 265 */
} // .end local v0 # "e":Landroid/os/RemoteException;
try { // :try_start_8
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 268 */
} // :goto_c
final String v0 = "[FingerprintService]FodFingerprintServiceStubImpl"; // const-string v0, "[FingerprintService]FodFingerprintServiceStubImpl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "fodCallBack cmd: "; // const-string v5, "fodCallBack cmd: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) v7 ).getCmdStr ( v8 ); // invoke-virtual {v7, v8}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getCmdStr(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " , "; // const-string v5, " , "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " , "; // const-string v5, " , "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v10 ); // invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " backRes:"; // const-string v5, " backRes:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " cmdSendToTouch:"; // const-string v5, " cmdSendToTouch:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v13 ); // invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " cmdSendToSf:"; // const-string v5, " cmdSendToSf:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v14 ); // invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " writeRes:"; // const-string v5, " writeRes:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v4 );
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_1 */
/* .line 271 */
/* monitor-exit p0 */
/* .line 265 */
} // :goto_d
try { // :try_start_9
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 266 */
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 267 */
/* throw v0 */
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_1 */
/* .line 166 */
} // .end local v1 # "touchFod":Z
} // .end local v2 # "data":Landroid/os/Parcel;
} // .end local v3 # "reply":Landroid/os/Parcel;
} // .end local v12 # "resBack":I
} // .end local v13 # "cmdSendToTouch":I
} // .end local v14 # "cmdSendToSf":I
} // .end local v15 # "alreadySetTouch":Z
} // .end local p1 # "context":Landroid/content/Context;
} // .end local p2 # "cmd":I
} // .end local p3 # "param":I
} // .end local p4 # "packName":Ljava/lang/String;
} // .end local p5 # "client":Lcom/android/server/biometrics/sensors/BaseClientMonitor;
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getCmd4Touch ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "cmd" # I */
/* .param p2, "param" # I */
/* .line 334 */
int v0 = -1; // const/4 v0, -0x1
/* .line 335 */
/* .local v0, "res":I */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 365 */
/* :sswitch_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 366 */
/* .line 361 */
/* :sswitch_1 */
int v0 = 1; // const/4 v0, 0x1
/* .line 362 */
/* .line 342 */
/* :sswitch_2 */
/* if-nez p2, :cond_0 */
/* .line 343 */
int v0 = 0; // const/4 v0, 0x0
/* .line 347 */
/* :sswitch_3 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 348 */
int v0 = 0; // const/4 v0, 0x0
/* .line 358 */
/* :sswitch_4 */
int v0 = 0; // const/4 v0, 0x0
/* .line 359 */
/* .line 339 */
/* :sswitch_5 */
int v0 = 1; // const/4 v0, 0x1
/* .line 340 */
/* nop */
/* .line 370 */
} // :cond_0
} // :goto_0
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_5 */
/* 0x2 -> :sswitch_4 */
/* 0x3 -> :sswitch_3 */
/* 0x4 -> :sswitch_4 */
/* 0x5 -> :sswitch_4 */
/* 0x6 -> :sswitch_4 */
/* 0x7 -> :sswitch_5 */
/* 0x8 -> :sswitch_4 */
/* 0x9 -> :sswitch_2 */
/* 0xb -> :sswitch_4 */
/* 0xc -> :sswitch_5 */
/* 0xd -> :sswitch_4 */
/* 0x61a81 -> :sswitch_1 */
/* 0x61a84 -> :sswitch_0 */
/* 0x61a86 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public java.lang.String getCmdStr ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "cmd" # I */
/* .line 514 */
/* const-string/jumbo v0, "unknown" */
/* .line 515 */
/* .local v0, "res":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 550 */
/* :pswitch_1 */
final String v0 = "CMD_KEYGUARD_CANCEL_DETECT"; // const-string v0, "CMD_KEYGUARD_CANCEL_DETECT"
/* .line 551 */
/* .line 547 */
/* :pswitch_2 */
final String v0 = "CMD_KEYGUARD_DETECT"; // const-string v0, "CMD_KEYGUARD_DETECT"
/* .line 548 */
/* .line 544 */
/* :pswitch_3 */
final String v0 = "CMD_VENDOR_REMOVED"; // const-string v0, "CMD_VENDOR_REMOVED"
/* .line 545 */
/* .line 541 */
/* :pswitch_4 */
final String v0 = "CMD_VENDOR_ENROLL_RES"; // const-string v0, "CMD_VENDOR_ENROLL_RES"
/* .line 542 */
/* .line 538 */
/* :pswitch_5 */
final String v0 = "CMD_APP_CANCEL_ENROLL"; // const-string v0, "CMD_APP_CANCEL_ENROLL"
/* .line 539 */
/* .line 535 */
/* :pswitch_6 */
final String v0 = "CMD_APP_ENROLL"; // const-string v0, "CMD_APP_ENROLL"
/* .line 536 */
/* .line 532 */
/* :pswitch_7 */
final String v0 = "CMD_FW_TOP_APP_CANCEL"; // const-string v0, "CMD_FW_TOP_APP_CANCEL"
/* .line 533 */
/* .line 529 */
/* :pswitch_8 */
final String v0 = "CMD_FW_LOCK_CANCEL"; // const-string v0, "CMD_FW_LOCK_CANCEL"
/* .line 530 */
/* .line 526 */
/* :pswitch_9 */
final String v0 = "CMD_VENDOR_ERROR"; // const-string v0, "CMD_VENDOR_ERROR"
/* .line 527 */
/* .line 523 */
/* :pswitch_a */
final String v0 = "CMD_VENDOR_AUTHENTICATED"; // const-string v0, "CMD_VENDOR_AUTHENTICATED"
/* .line 524 */
/* .line 520 */
/* :pswitch_b */
final String v0 = "CMD_APP_CANCEL_AUTHEN"; // const-string v0, "CMD_APP_CANCEL_AUTHEN"
/* .line 521 */
/* .line 517 */
/* :pswitch_c */
final String v0 = "CMD_APP_AUTHEN"; // const-string v0, "CMD_APP_AUTHEN"
/* .line 518 */
/* nop */
/* .line 555 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public Boolean getFodCoordinate ( Integer[] p0 ) {
/* .locals 5 */
/* .param p1, "udfpsProps" # [I */
/* .line 287 */
/* array-length v0, p1 */
int v1 = 3; // const/4 v1, 0x3
int v2 = 0; // const/4 v2, 0x0
/* if-ge v0, v1, :cond_0 */
/* .line 288 */
final String v0 = "[FingerprintService]FodFingerprintServiceStubImpl"; // const-string v0, "[FingerprintService]FodFingerprintServiceStubImpl"
/* const-string/jumbo v1, "wrong udfpsProps length!" */
android.util.Slog .i ( v0,v1 );
/* .line 289 */
/* .line 292 */
} // :cond_0
v0 = this.FOD_LOCATION_XY;
v0 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v0, :cond_3 */
v0 = this.FOD_LOCATION_XY;
final String v1 = ","; // const-string v1, ","
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 293 */
v0 = this.FOD_LOCATION_XY;
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v0, v0, v2 */
v0 = java.lang.Integer .parseInt ( v0 );
/* aput v0, p1, v2 */
/* .line 294 */
v0 = this.FOD_LOCATION_XY;
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v3 = 1; // const/4 v3, 0x1
/* aget-object v0, v0, v3 */
v0 = java.lang.Integer .parseInt ( v0 );
/* aput v0, p1, v3 */
/* .line 296 */
v0 = this.FOD_LOCATION_WH;
v0 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
/* if-nez v0, :cond_2 */
v0 = this.FOD_LOCATION_WH;
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 297 */
v0 = this.FOD_LOCATION_WH;
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v0, v0, v2 */
v0 = java.lang.Integer .parseInt ( v0 );
/* .line 298 */
/* .local v0, "width":I */
v2 = this.FOD_LOCATION_WH;
(( java.lang.String ) v2 ).split ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v1, v1, v3 */
v1 = java.lang.Integer .parseInt ( v1 );
/* .line 300 */
/* .local v1, "height":I */
/* if-ne v0, v1, :cond_1 */
/* .line 301 */
/* div-int/lit8 v2, v0, 0x2 */
int v4 = 2; // const/4 v4, 0x2
/* aput v2, p1, v4 */
/* .line 303 */
} // .end local v0 # "width":I
} // .end local v1 # "height":I
} // :cond_1
/* nop */
/* .line 310 */
/* .line 304 */
} // :cond_2
/* .line 307 */
} // :cond_3
} // .end method
public android.os.IBinder getFodServ ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 497 */
v0 = this.mFodLock;
/* monitor-enter v0 */
/* .line 498 */
try { // :try_start_0
v1 = this.mFodService;
/* if-nez v1, :cond_0 */
/* .line 499 */
final String v1 = "android.app.fod.ICallback"; // const-string v1, "android.app.fod.ICallback"
android.os.ServiceManager .getService ( v1 );
this.mFodService = v1;
/* .line 500 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 501 */
/* new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;)V */
int v3 = 0; // const/4 v3, 0x0
/* .line 509 */
} // :cond_0
v1 = this.mFodService;
/* monitor-exit v0 */
/* .line 510 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean getIsFod ( ) {
/* .locals 1 */
/* .line 277 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD:Z */
} // .end method
public Boolean getIsFodLocationLow ( ) {
/* .locals 1 */
/* .line 282 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FOD_SENSOR_LOCATION_LOW:Z */
} // .end method
public Float getPrcent ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "xory" # I */
/* .param p2, "widthorheight" # I */
/* .line 657 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* if-nez p1, :cond_0 */
/* .line 659 */
} // :cond_0
/* new-instance v0, Ljava/math/BigDecimal; */
/* invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(I)V */
/* new-instance v1, Ljava/math/BigDecimal; */
/* invoke-direct {v1, p2}, Ljava/math/BigDecimal;-><init>(I)V */
/* const/16 v2, 0xa */
int v3 = 5; // const/4 v3, 0x5
(( java.math.BigDecimal ) v0 ).divide ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;II)Ljava/math/BigDecimal;
v0 = (( java.math.BigDecimal ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F
/* .line 658 */
} // :cond_1
} // :goto_0
/* const/high16 v0, 0x3f800000 # 1.0f */
} // .end method
public getSensorLocation ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 594 */
(( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) p0 ).calculateFodSensorLocation ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->calculateFodSensorLocation(Landroid/content/Context;)V
/* .line 596 */
final String v0 = "display"; // const-string v0, "display"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/display/DisplayManager; */
/* .line 597 */
/* .local v0, "displayManager":Landroid/hardware/display/DisplayManager; */
int v1 = 0; // const/4 v1, 0x0
(( android.hardware.display.DisplayManager ) v0 ).getDisplay ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
/* .line 598 */
/* .local v2, "display":Landroid/view/Display; */
(( android.view.Display ) v2 ).getSupportedModes ( ); // invoke-virtual {v2}, Landroid/view/Display;->getSupportedModes()[Landroid/view/Display$Mode;
/* .line 600 */
/* .local v3, "resolutiones":[Landroid/view/Display$Mode; */
int v4 = 4; // const/4 v4, 0x4
/* new-array v4, v4, [I */
/* .line 601 */
/* .local v4, "mFodLocation":[I */
/* iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_X:I */
/* aput v5, v4, v1 */
/* .line 602 */
int v1 = 1; // const/4 v1, 0x1
/* iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_Y:I */
/* aput v5, v4, v1 */
/* .line 603 */
int v1 = 2; // const/4 v1, 0x2
/* iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_WIDTH:I */
/* aput v5, v4, v1 */
/* .line 604 */
int v1 = 3; // const/4 v1, 0x3
/* iget v5, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->GXZW_ICON_HEIGHT:I */
/* aput v5, v4, v1 */
/* .line 605 */
} // .end method
public void handleAcquiredInfo ( Integer p0, Integer p1, com.android.server.biometrics.sensors.AcquisitionClient p2 ) {
/* .locals 3 */
/* .param p1, "acquiredInfo" # I */
/* .param p2, "vendorCode" # I */
/* .param p3, "client" # Lcom/android/server/biometrics/sensors/AcquisitionClient; */
/* .line 580 */
try { // :try_start_0
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isFingerprintClient ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerprintClient(Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStubImpl ) p0 ).getIsFod ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->getIsFod()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 581 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isFingerDownAcquireCode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerDownAcquireCode(II)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 582 */
(( com.android.server.biometrics.sensors.AcquisitionClient ) p3 ).getListener ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AcquisitionClient;->getListener()Lcom/android/server/biometrics/sensors/ClientMonitorCallbackConverter;
v1 = (( com.android.server.biometrics.sensors.AcquisitionClient ) p3 ).getSensorId ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AcquisitionClient;->getSensorId()I
(( com.android.server.biometrics.sensors.ClientMonitorCallbackConverter ) v0 ).onUdfpsPointerDown ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/ClientMonitorCallbackConverter;->onUdfpsPointerDown(I)V
/* .line 583 */
} // :cond_0
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub ) v0 ).isFingerUpAcquireCode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->isFingerUpAcquireCode(II)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 584 */
(( com.android.server.biometrics.sensors.AcquisitionClient ) p3 ).getListener ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AcquisitionClient;->getListener()Lcom/android/server/biometrics/sensors/ClientMonitorCallbackConverter;
v1 = (( com.android.server.biometrics.sensors.AcquisitionClient ) p3 ).getSensorId ( ); // invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AcquisitionClient;->getSensorId()I
(( com.android.server.biometrics.sensors.ClientMonitorCallbackConverter ) v0 ).onUdfpsPointerUp ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/ClientMonitorCallbackConverter;->onUdfpsPointerUp(I)V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 589 */
} // :cond_1
} // :goto_0
/* .line 587 */
/* :catch_0 */
/* move-exception v0 */
/* .line 588 */
/* .local v0, "e":Landroid/os/RemoteException; */
v1 = this.mCallback;
int v2 = 0; // const/4 v2, 0x0
/* .line 590 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
return;
} // .end method
protected Boolean isKeyguard ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "clientPackage" # Ljava/lang/String; */
/* .line 574 */
final String v0 = "com.android.systemui"; // const-string v0, "com.android.systemui"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public void phySicalScreenPx ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 668 */
v0 = this.MIUI_DEFAULT_RESOLUTION;
v0 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 669 */
final String v0 = "display"; // const-string v0, "display"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/display/DisplayManager; */
/* .line 670 */
/* .local v0, "displayManager":Landroid/hardware/display/DisplayManager; */
(( android.hardware.display.DisplayManager ) v0 ).getDisplay ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
/* .line 671 */
/* .local v1, "display":Landroid/view/Display; */
(( android.view.Display ) v1 ).getMode ( ); // invoke-virtual {v1}, Landroid/view/Display;->getMode()Landroid/view/Display$Mode;
v2 = (( android.view.Display$Mode ) v2 ).getPhysicalWidth ( ); // invoke-virtual {v2}, Landroid/view/Display$Mode;->getPhysicalWidth()I
/* iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I */
/* .line 672 */
(( android.view.Display ) v1 ).getMode ( ); // invoke-virtual {v1}, Landroid/view/Display;->getMode()Landroid/view/Display$Mode;
v2 = (( android.view.Display$Mode ) v2 ).getPhysicalHeight ( ); // invoke-virtual {v2}, Landroid/view/Display$Mode;->getPhysicalHeight()I
/* iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I */
/* .line 673 */
} // .end local v0 # "displayManager":Landroid/hardware/display/DisplayManager;
} // .end local v1 # "display":Landroid/view/Display;
/* .line 675 */
} // :cond_0
v0 = this.MIUI_DEFAULT_RESOLUTION;
/* const-string/jumbo v2, "x" */
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v0, v0, v1 */
v0 = java.lang.Integer .parseInt ( v0 );
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_WIDTH_PHYSICAL:I */
/* .line 676 */
v0 = this.MIUI_DEFAULT_RESOLUTION;
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v1 = 1; // const/4 v1, 0x1
/* aget-object v0, v0, v1 */
v0 = java.lang.Integer .parseInt ( v0 );
/* iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->SCREEN_HEIGHT_PHYSICAL:I */
/* .line 678 */
} // :goto_0
return;
} // .end method
public void setCurrentLockoutMode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "lockoutMode" # I */
/* .line 315 */
/* iget-boolean v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStubImpl;->IS_FODENGINE_ENABLED:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 316 */
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .getInstance ( );
int v0 = 5; // const/4 v0, 0x5
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub .startExtCmd ( v0,p1 );
/* .line 318 */
} // :cond_0
return;
} // .end method
public Boolean setTouchMode ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "touchId" # I */
/* .param p2, "mode" # I */
/* .param p3, "value" # I */
/* .line 322 */
try { // :try_start_0
miui.util.ITouchFeature .getInstance ( );
/* .line 323 */
/* .local v0, "touchFeature":Lmiui/util/ITouchFeature; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 324 */
v1 = (( miui.util.ITouchFeature ) v0 ).setTouchMode ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 328 */
} // .end local v0 # "touchFeature":Lmiui/util/ITouchFeature;
} // :cond_0
/* .line 326 */
/* :catch_0 */
/* move-exception v0 */
/* .line 327 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 329 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
