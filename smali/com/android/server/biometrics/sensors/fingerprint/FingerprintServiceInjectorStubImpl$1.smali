.class Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1;
.super Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback$Stub;
.source "FingerprintServiceInjectorStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->initRecordFeature(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;

    .line 193
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;

    invoke-direct {p0}, Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onAllAuthenticatorsRegistered(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/fingerprint/FingerprintSensorPropertiesInternal;",
            ">;)V"
        }
    .end annotation

    .line 197
    .local p1, "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/fingerprint/FingerprintSensorPropertiesInternal;>;"
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->-$$Nest$fgetmFingerprintManager(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1$1;

    invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1;)V

    invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->registerBiometricStateListener(Landroid/hardware/biometrics/BiometricStateListener;)V

    .line 206
    return-void
.end method
