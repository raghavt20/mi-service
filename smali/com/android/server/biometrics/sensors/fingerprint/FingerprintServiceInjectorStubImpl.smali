.class public Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;
.super Ljava/lang/Object;
.source "FingerprintServiceInjectorStubImpl.java"

# interfaces
.implements Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStub;


# static fields
.field private static final FINGERPRINT_ACQUIRED_AUTH_BIGDATA:I = 0xc9

.field private static final FINGERPRINT_ACQUIRED_ENROLL_BIGDATA:I = 0xca

.field private static final FINGERPRINT_ACQUIRED_FINGER_DOWN:I = 0x16

.field private static final FINGERPRINT_ACQUIRED_INFORE6:I = 0x6

.field private static final FINGERPRINT_ACQUIRED_INFORE7:I = 0x7

.field private static final FINGERPRINT_ACQUIRED_INIT_BIGDATA:I = 0xc8

.field private static final FP_LOCAL_STATISTICS_ENABLED:Z

.field private static final MISIGHT_FINGERPRINT_TEMPLATELOST_EVENT:I = 0x1

.field private static final MISIGHT_FINGERPRINT_TEMPLATELOST_ID:I = 0x367a8c6b

.field private static final SCREEN_STATUS_DOZE:I = 0x2

.field private static final SCREEN_STATUS_OFF:I = 0x0

.field private static final SCREEN_STATUS_ON:I = 0x1

.field private static final TAG:Ljava/lang/String; = "FingerprintServiceInjectorStubImpl"


# instance fields
.field private localStatisticsInstance:Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

.field private mBiometricState:I

.field private mContext:Landroid/content/Context;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mFingerprintManager:Landroid/hardware/fingerprint/FingerprintManager;

.field private mScreenStatus:I


# direct methods
.method public static synthetic $r8$lambda$1TH3fXCZY-nZOsDJy6owVVXMl8s(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->lambda$recordAuthResult$0(Ljava/lang/String;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$fYigozsH_tExrUHktTbK5uKl7BI(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->lambda$recordAcquiredInfo$1(II)V

    return-void
.end method

.method public static synthetic $r8$lambda$iGhhCNVm3xQPNoocZTYlbpQm3ug(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->lambda$initAuthStatistics$2()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmFingerprintManager(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;)Landroid/hardware/fingerprint/FingerprintManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mFingerprintManager:Landroid/hardware/fingerprint/FingerprintManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmBiometricState(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mBiometricState:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 54
    const-string v0, "persist.vendor.sys.fp.onetrack.enable"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mBiometricState:I

    return-void
.end method

.method private synthetic lambda$initAuthStatistics$2()V
    .locals 2

    .line 173
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initLocalStatistics(Landroid/content/Context;)V

    .line 174
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->setContext(Landroid/content/Context;)V

    .line 175
    return-void
.end method

.method private synthetic lambda$recordAcquiredInfo$1(II)V
    .locals 3
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I

    .line 132
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mBiometricState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 133
    :cond_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;

    move-result-object v0

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintFailReasonData;->calculateFailReasonCnt(III)Z

    move-result v0

    .line 134
    .local v0, "result":Z
    if-eqz v0, :cond_1

    .line 135
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v1

    const-string v2, "fail_reason_info"

    invoke-virtual {v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V

    .line 139
    .end local v0    # "result":Z
    :cond_1
    const/4 v0, 0x6

    if-eq p1, v0, :cond_2

    const/4 v0, 0x7

    if-ne p1, v0, :cond_3

    .line 140
    :cond_2
    const-string v0, "FingerprintServiceInjectorStubImpl"

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 152
    :pswitch_0
    const-string v1, "calculateHalEnrollInfo"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    goto :goto_0

    .line 145
    :pswitch_1
    const-string v1, "calculateHalAuthInfo"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintHalAuthData;->calculateHalAuthInfo()Z

    move-result v0

    .line 147
    .restart local v0    # "result":Z
    if-eqz v0, :cond_3

    .line 148
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v1

    const-string/jumbo v2, "unlock_hal_info"

    invoke-virtual {v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V

    goto :goto_0

    .line 142
    .end local v0    # "result":Z
    :pswitch_2
    const-string v1, "calculateHalInitInfo"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    nop

    .line 161
    :cond_3
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private synthetic lambda$recordAuthResult$0(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packName"    # Ljava/lang/String;
    .param p2, "authen"    # I

    .line 110
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    move-result-object v0

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->calculateUnlockCnt(Ljava/lang/String;II)V

    .line 111
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v0

    const-string/jumbo v1, "unlock_rate_info"

    invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method static synthetic lambda$recordFpTypeAndEnrolledCount$3(II)V
    .locals 1
    .param p0, "unlockType"    # I
    .param p1, "count"    # I

    .line 222
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->recordFpTypeAndEnrolledCount(II)V

    .line 223
    return-void
.end method

.method private miSightEventReport(II)V
    .locals 2
    .param p1, "event"    # I
    .param p2, "param"    # I

    .line 231
    packed-switch p1, :pswitch_data_0

    .line 238
    const-string v0, "FingerprintServiceInjectorStubImpl"

    const-string/jumbo v1, "unknow mi sight event"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    return-void

    .line 233
    :pswitch_0
    new-instance v0, Lcom/miui/misight/MiEvent;

    const v1, 0x367a8c6b

    invoke-direct {v0, v1}, Lcom/miui/misight/MiEvent;-><init>(I)V

    .line 234
    .local v0, "miEvent":Lcom/miui/misight/MiEvent;
    const-string v1, "FingerprintTemplateLost"

    invoke-virtual {v0, v1, p2}, Lcom/miui/misight/MiEvent;->addInt(Ljava/lang/String;I)Lcom/miui/misight/MiEvent;

    .line 235
    invoke-static {v0}, Lcom/miui/misight/MiSight;->sendEvent(Lcom/miui/misight/MiEvent;)V

    .line 236
    nop

    .line 241
    .end local v0    # "miEvent":Lcom/miui/misight/MiEvent;
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private updataScreenStatus()V
    .locals 5

    .line 78
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getState()I

    move-result v0

    .line 79
    .local v0, "state":I
    const/4 v2, 0x1

    const-string v3, "FingerprintServiceInjectorStubImpl"

    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    .line 80
    iput v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I

    .line 81
    const-string v1, "screen on when finger down"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 82
    :cond_0
    if-ne v0, v2, :cond_1

    .line 83
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I

    .line 84
    const-string v1, "screen off when finger down"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :cond_1
    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 86
    :cond_2
    iput v4, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I

    .line 87
    const-string v1, "screen doze when finger down"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_3
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updataScreenStatus, state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mScreenStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    return-void
.end method


# virtual methods
.method public handleUnknowTemplateCleanup(Lcom/android/server/biometrics/sensors/BaseClientMonitor;I)V
    .locals 2
    .param p1, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;
    .param p2, "unknownTemplateCount"    # I

    .line 246
    invoke-virtual {p1}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->statsModality()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-lez p2, :cond_0

    .line 247
    invoke-direct {p0, v1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->miSightEventReport(II)V

    .line 248
    :cond_0
    return-void
.end method

.method public initAuthStatistics()V
    .locals 2

    .line 167
    const-string v0, "FingerprintServiceInjectorStubImpl"

    const-string v1, "initAuthStatistics"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z

    if-nez v0, :cond_0

    .line 169
    return-void

    .line 171
    :cond_0
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 177
    return-void
.end method

.method public initRecordFeature(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 182
    sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z

    if-nez v0, :cond_0

    .line 183
    return-void

    .line 185
    :cond_0
    const-string v0, "FingerprintServiceInjectorStubImpl"

    const-string v1, "initRecordFeature"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mContext:Landroid/content/Context;

    .line 187
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-nez v0, :cond_1

    .line 188
    const-class v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mContext:Landroid/content/Context;

    const-class v1, Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/fingerprint/FingerprintManager;

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mFingerprintManager:Landroid/hardware/fingerprint/FingerprintManager;

    .line 192
    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1;

    invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;)V

    invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->addAuthenticatorsRegisteredCallback(Landroid/hardware/fingerprint/IFingerprintAuthenticatorsRegisteredCallback;)V

    .line 210
    return-void
.end method

.method public recordAcquiredInfo(II)V
    .locals 2
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recordAcquiredInfo, acquiredInfo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", vendorCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintServiceInjectorStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z

    if-nez v0, :cond_0

    .line 120
    return-void

    .line 123
    :cond_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initLocalStatistics(Landroid/content/Context;)V

    .line 125
    const/16 v0, 0x16

    if-ne p2, v0, :cond_1

    .line 126
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->updataScreenStatus()V

    .line 129
    :cond_1
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->handleAcquiredInfo(II)V

    .line 130
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintUnlockRateData;->handleAcquiredInfo(II)V

    .line 131
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 162
    return-void
.end method

.method public recordActivityVisible()V
    .locals 0

    .line 228
    return-void
.end method

.method public recordAuthResult(Ljava/lang/String;I)V
    .locals 3
    .param p1, "packName"    # Ljava/lang/String;
    .param p2, "authen"    # I

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recordAuthResult, packName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", authen"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintServiceInjectorStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z

    if-nez v0, :cond_0

    .line 98
    return-void

    .line 101
    :cond_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initLocalStatistics(Landroid/content/Context;)V

    .line 102
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->startLocalStatisticsOneTrackUpload()V

    .line 104
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;

    move-result-object v0

    iget v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mScreenStatus:I

    invoke-virtual {v0, p2, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintAuthTimeData;->calculateAuthTime(II)I

    move-result v0

    .line 105
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 106
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v1

    const-string v2, "auth_time_info"

    invoke-virtual {v1, v2}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->updataLocalStatistics(Ljava/lang/String;)V

    .line 109
    :cond_1
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 114
    return-void
.end method

.method public recordFpTypeAndEnrolledCount(II)V
    .locals 2
    .param p1, "unlockType"    # I
    .param p2, "count"    # I

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recordFpTypeAndEnrolledCount\uff0cunlockType:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FingerprintServiceInjectorStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    sget-boolean v0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->FP_LOCAL_STATISTICS_ENABLED:Z

    if-nez v0, :cond_0

    .line 216
    return-void

    .line 219
    :cond_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/biometrics/sensors/fingerprint/bigdata/FingerprintLocalStatisticsData;->initLocalStatistics(Landroid/content/Context;)V

    .line 221
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$$ExternalSyntheticLambda1;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 225
    return-void
.end method
