class com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$1 extends android.database.ContentObserver {
	 /* .source "MiuiFingerprintCloudController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->registerCloudDataObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$R5PhNh1UxmgdPkk9pdlg_fzyD6E ( com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$1 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;->lambda$onChange$0()V */
return;
} // .end method
 com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 139 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
private void lambda$onChange$0 ( ) { //synthethic
/* .locals 1 */
/* .line 146 */
v0 = this.this$0;
com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController .-$$Nest$msyncLocalBackupFromCloud ( v0 );
/* .line 147 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .line 142 */
final String v0 = "MiuiFingerprintCloudController"; // const-string v0, "MiuiFingerprintCloudController"
final String v1 = "onChange."; // const-string v1, "onChange."
android.util.Slog .i ( v0,v1 );
/* .line 143 */
v0 = this.this$0;
v0 = com.android.server.biometrics.sensors.fingerprint.MiuiFingerprintCloudController .-$$Nest$mupdateDataFromCloudControl ( v0 );
/* .line 144 */
/* .local v0, "changed":Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 145 */
	 com.android.internal.os.BackgroundThread .getHandler ( );
	 /* new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;)V */
	 (( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
	 /* .line 150 */
} // :cond_0
return;
} // .end method
