.class Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;
.super Landroid/database/ContentObserver;
.source "MiuiFingerprintCloudController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->registerCloudDataObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;


# direct methods
.method public static synthetic $r8$lambda$R5PhNh1UxmgdPkk9pdlg_fzyD6E(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;->lambda$onChange$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 139
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method private synthetic lambda$onChange$0()V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->-$$Nest$msyncLocalBackupFromCloud(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)V

    .line 147
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 142
    const-string v0, "MiuiFingerprintCloudController"

    const-string v1, "onChange."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;->this$0:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;->-$$Nest$mupdateDataFromCloudControl(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;)Z

    move-result v0

    .line 144
    .local v0, "changed":Z
    if-eqz v0, :cond_0

    .line 145
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 150
    :cond_0
    return-void
.end method
