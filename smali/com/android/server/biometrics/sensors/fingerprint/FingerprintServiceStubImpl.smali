.class public Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;
.super Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;
.source "FingerprintServiceStubImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.biometrics.sensors.fingerprint.FingerprintServiceStub$$"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FingerprintServiceStubImpl"


# instance fields
.field private final CODE_EXT_CMD:I

.field private final EXT_DESCRIPTOR:Ljava/lang/String;

.field private final NAME_EXT_DAEMON:Ljava/lang/String;

.field private lockFlag:I

.field private mExtDaemon:Landroid/os/IHwBinder;

.field mFingerIdentifer:Landroid/hardware/fingerprint/Fingerprint;

.field mFingerToken:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field protected mLockoutMode:I

.field public mOpId:J

.field public mOpPackage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 41
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;-><init>()V

    .line 43
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpId:J

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpPackage:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mLockoutMode:I

    .line 53
    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I

    .line 56
    const-string/jumbo v0, "vendor.xiaomi.hardware.fingerprintextension@1.0::IXiaomiFingerprint"

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->NAME_EXT_DAEMON:Ljava/lang/String;

    .line 57
    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->EXT_DESCRIPTOR:Ljava/lang/String;

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->CODE_EXT_CMD:I

    return-void
.end method


# virtual methods
.method public clearSavedAuthenResult()V
    .locals 1

    .line 110
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/biometrics/sensors/fingerprint/PowerFingerprintServiceStub;->getIsPowerfp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mFingerIdentifer:Landroid/hardware/fingerprint/Fingerprint;

    .line 112
    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mFingerToken:Ljava/util/ArrayList;

    .line 114
    :cond_0
    return-void
.end method

.method public getCurrentLockoutMode()I
    .locals 1

    .line 181
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mLockoutMode:I

    return v0
.end method

.method public getIdentifier()Landroid/hardware/fingerprint/Fingerprint;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mFingerIdentifer:Landroid/hardware/fingerprint/Fingerprint;

    return-object v0
.end method

.method public getMiuiGroupId(I)I
    .locals 2
    .param p1, "userId"    # I

    .line 164
    move v0, p1

    .line 166
    .local v0, "groupId":I
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->isFingerDataSharer(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const/4 v0, 0x0

    .line 170
    :cond_0
    return v0
.end method

.method public getOpId()J
    .locals 2

    .line 83
    iget-wide v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpId:J

    .line 84
    .local v0, "opId":J
    return-wide v0
.end method

.method public getOpPackageName()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpPackage:Ljava/lang/String;

    .line 90
    .local v0, "opPackage":Ljava/lang/String;
    return-object v0
.end method

.method public getToken()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mFingerToken:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getcurrentUserId(I)I
    .locals 1
    .param p1, "groupId"    # I

    .line 160
    invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->getMiuiGroupId(I)I

    move-result v0

    return v0
.end method

.method public handleAcquiredInfo(IILcom/android/server/biometrics/sensors/AuthenticationClient;)V
    .locals 4
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I
    .param p3, "client"    # Lcom/android/server/biometrics/sensors/AuthenticationClient;

    .line 231
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->shouldHandleFailedAttempt(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iput v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I

    .line 235
    :cond_0
    iget v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I

    if-ne v0, v1, :cond_3

    const/4 v0, 0x5

    if-ne p1, v0, :cond_3

    .line 236
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I

    .line 237
    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->getTargetUserId()I

    move-result v2

    invoke-virtual {p3, v2}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->handleFailedAttempt(I)I

    move-result v2

    .line 239
    .local v2, "lockoutMode":I
    invoke-virtual {p3}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->getOwnerString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->isSecurityCenterClient(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 240
    const/16 v3, 0x13

    invoke-virtual {p3, v3, v0}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->onError(II)V

    .line 242
    :cond_1
    if-eqz v2, :cond_3

    .line 243
    if-ne v2, v1, :cond_2

    .line 244
    const/4 v1, 0x7

    goto :goto_0

    .line 245
    :cond_2
    const/16 v1, 0x9

    :goto_0
    nop

    .line 246
    .local v1, "errorCode":I
    invoke-virtual {p3, v1, v0}, Lcom/android/server/biometrics/sensors/AuthenticationClient;->onError(II)V

    .line 249
    .end local v1    # "errorCode":I
    .end local v2    # "lockoutMode":I
    :cond_3
    return-void
.end method

.method public interruptsPrecedingClients(Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z
    .locals 2
    .param p1, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    .line 62
    invoke-virtual {p1}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->getOwnerString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/biometrics/Utils;->isKeyguard(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x1

    return v0

    .line 65
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isFingerDataSharer(I)Z
    .locals 10
    .param p1, "userId"    # I

    .line 117
    const-string v0, "FingerprintServiceStubImpl"

    const/4 v1, 0x0

    .line 118
    .local v1, "uinfo":Landroid/content/pm/UserInfo;
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v2

    .line 119
    .local v2, "app":Landroid/app/Application;
    const-class v3, Landroid/os/UserManager;

    invoke-virtual {v2, v3}, Landroid/app/Application;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 121
    .local v3, "manager":Landroid/os/UserManager;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 123
    .local v4, "token":J
    :try_start_0
    invoke-virtual {v3, p1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v6

    .line 127
    nop

    :goto_0
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 128
    goto :goto_1

    .line 127
    :catchall_0
    move-exception v0

    goto/16 :goto_5

    .line 124
    :catch_0
    move-exception v6

    .line 125
    .local v6, "ex":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    nop

    .end local v6    # "ex":Ljava/lang/Exception;
    goto :goto_0

    .line 130
    :goto_1
    const/4 v6, 0x0

    if-eqz v1, :cond_3

    .line 132
    iget-object v7, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    const/4 v8, 0x1

    if-eqz v7, :cond_1

    .line 133
    iget-object v7, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_0
    goto :goto_2

    :sswitch_0
    const-string/jumbo v9, "security space"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v7, v6

    goto :goto_3

    :sswitch_1
    const-string v9, "child_model"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v7, v8

    goto :goto_3

    :sswitch_2
    const-string v9, "XSpace"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x2

    goto :goto_3

    :goto_2
    const/4 v7, -0x1

    :goto_3
    packed-switch v7, :pswitch_data_0

    .line 141
    goto :goto_4

    .line 137
    :pswitch_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "calling from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return v8

    .line 143
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "calling from anonymous user, id = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :goto_4
    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 148
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "calling from managed-profile, id =  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", owned by "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v1, Landroid/content/pm/UserInfo;->profileGroupId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    return v8

    .line 152
    :cond_2
    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->toFullString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_3
    return v6

    .line 127
    :goto_5
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 128
    throw v0

    :sswitch_data_0
    .sparse-switch
        -0x650f8872 -> :sswitch_2
        -0x3868f1ba -> :sswitch_1
        0x7f500c26 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isFingerDownAcquireCode(II)Z
    .locals 1
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I

    .line 194
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    const/16 v0, 0x16

    if-ne p2, v0, :cond_0

    .line 195
    const/4 v0, 0x1

    return v0

    .line 197
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isFingerUpAcquireCode(II)Z
    .locals 1
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I

    .line 202
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    const/16 v0, 0x17

    if-ne p2, v0, :cond_0

    .line 203
    const/4 v0, 0x1

    return v0

    .line 205
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isFingerprintClient(Lcom/android/server/biometrics/sensors/BaseClientMonitor;)Z
    .locals 2
    .param p1, "client"    # Lcom/android/server/biometrics/sensors/BaseClientMonitor;

    .line 186
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/biometrics/sensors/BaseClientMonitor;->statsModality()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 187
    return v1

    .line 189
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isFpHardwareDetected()Z
    .locals 4

    .line 253
    const-string v0, "persist.vendor.sys.fp.vendor"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "fpVendor":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "none"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 255
    const/4 v1, 0x1

    return v1

    .line 257
    :cond_0
    const-string v1, "FingerprintServiceStubImpl"

    const-string v2, "Fingerprint isHardwareDetected is failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    const/4 v1, 0x0

    return v1
.end method

.method public isScreenOn(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 71
    const-class v0, Landroid/os/PowerManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 72
    .local v0, "powerManager":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v1

    return v1
.end method

.method public isSecurityCenterClient(Ljava/lang/String;)Z
    .locals 1
    .param p1, "clientName"    # Ljava/lang/String;

    .line 210
    const-string v0, "com.miui.securitycenter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public resetLockFlag()V
    .locals 1

    .line 225
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->lockFlag:I

    .line 226
    return-void
.end method

.method public saveAuthenResultLocal(Landroid/hardware/fingerprint/Fingerprint;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "identifier"    # Landroid/hardware/fingerprint/Fingerprint;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/hardware/fingerprint/Fingerprint;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .line 95
    .local p2, "token":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mFingerIdentifer:Landroid/hardware/fingerprint/Fingerprint;

    .line 96
    iput-object p2, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mFingerToken:Ljava/util/ArrayList;

    .line 97
    return-void
.end method

.method public saveAuthenticateConfig(JLjava/lang/String;)V
    .locals 0
    .param p1, "opId"    # J
    .param p3, "opPackageName"    # Ljava/lang/String;

    .line 77
    iput-wide p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpId:J

    .line 78
    iput-object p3, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mOpPackage:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setCurrentLockoutMode(I)V
    .locals 1
    .param p1, "lockoutMode"    # I

    .line 175
    iput p1, p0, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStubImpl;->mLockoutMode:I

    .line 176
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;->setCurrentLockoutMode(I)V

    .line 177
    return-void
.end method

.method public shouldHandleFailedAttempt(II)Z
    .locals 1
    .param p1, "acquiredInfo"    # I
    .param p2, "vendorCode"    # I

    .line 215
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    const/16 v0, 0x13

    if-eq p2, v0, :cond_0

    const/16 v0, 0x14

    if-eq p2, v0, :cond_0

    const/16 v0, 0x15

    if-eq p2, v0, :cond_0

    const/16 v0, 0x16

    if-eq p2, v0, :cond_0

    const/16 v0, 0x17

    if-eq p2, v0, :cond_0

    const/16 v0, 0x1c

    if-eq p2, v0, :cond_0

    const/16 v0, 0x32

    if-eq p2, v0, :cond_0

    const/16 v0, 0x34

    if-eq p2, v0, :cond_0

    .line 218
    const/4 v0, 0x1

    return v0

    .line 220
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
