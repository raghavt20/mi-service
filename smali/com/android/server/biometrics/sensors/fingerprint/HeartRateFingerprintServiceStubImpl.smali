.class public Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;
.super Ljava/lang/Object;
.source "HeartRateFingerprintServiceStubImpl.java"

# interfaces
.implements Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStub;


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "HeartRateFingerprintServiceStubImpl"

.field private static mHeartRateBinder:Landroid/os/IBinder;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mMiuiFingerprintCloudController:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

.field private mService:Lcom/android/server/biometrics/sensors/fingerprint/FingerprintService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 106
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHeartRateBinder:Landroid/os/IBinder;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mMiuiFingerprintCloudController:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    return-void
.end method

.method public static heartRateDataCallback(II[B)Z
    .locals 5
    .param p0, "msgId"    # I
    .param p1, "cmdId"    # I
    .param p2, "msg_data"    # [B

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "heartRateDataCallback: msgId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cmdId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " msg_data: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mHeartRateBinder: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHeartRateBinder:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HeartRateFingerprintServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    sget-object v0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHeartRateBinder:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 203
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 205
    .local v0, "request":Landroid/os/Parcel;
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "com.android.app.HeartRate"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 206
    invoke-virtual {v0, p0}, Landroid/os/Parcel;->writeInt(I)V

    .line 207
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 208
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 209
    sget-object v2, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHeartRateBinder:Landroid/os/IBinder;

    const v3, 0xfffffc

    const/4 v4, 0x1

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    nop

    .line 216
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 211
    return v4

    .line 216
    :catchall_0
    move-exception v1

    goto :goto_0

    .line 212
    :catch_0
    move-exception v2

    .line 213
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sput-object v1, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHeartRateBinder:Landroid/os/IBinder;

    .line 214
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    .end local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 217
    goto :goto_1

    .line 216
    :goto_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 217
    throw v1

    .line 219
    .end local v0    # "request":Landroid/os/Parcel;
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return v0
.end method

.method private registerCallback()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 186
    const-string v0, "reg callback"

    const-string v1, "HeartRateFingerprintServiceStubImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelAidl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getSupportInterfaceVersion()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 190
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelAidl;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->registerCallback(Landroid/os/Handler;)V

    goto :goto_0

    .line 191
    :cond_0
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelHidl;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 192
    const-string v0, "get reg"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelHidl;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->registerCallback(Landroid/os/Handler;)V

    goto :goto_0

    .line 195
    :cond_1
    const-string v0, "get null"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :goto_0
    return-void
.end method


# virtual methods
.method public cloudCmd(Landroid/os/Looper;Landroid/content/Context;II)I
    .locals 1
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cmd"    # I
    .param p4, "param"    # I

    .line 158
    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 160
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mMiuiFingerprintCloudController:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    if-nez v0, :cond_0

    .line 161
    new-instance v0, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    invoke-direct {v0, p1, p2}, Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;-><init>(Landroid/os/Looper;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mMiuiFingerprintCloudController:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    goto :goto_0

    .line 166
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mMiuiFingerprintCloudController:Lcom/android/server/biometrics/sensors/fingerprint/MiuiFingerprintCloudController;

    .line 169
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public doSendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;
    .locals 2
    .param p1, "cmdId"    # I
    .param p2, "params"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 173
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelAidl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceStub;->getSupportInterfaceVersion()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 176
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelAidl;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->sendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;

    move-result-object v0

    return-object v0

    .line 177
    :cond_0
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelHidl;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 178
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getInstance()Landroid/hardware/fingerprint/MiFxTunnelHidl;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->sendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;

    move-result-object v0

    return-object v0

    .line 180
    :cond_1
    const-string v0, "HeartRateFingerprintServiceStubImpl"

    const-string v1, "get null"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    new-instance v0, Landroid/hardware/fingerprint/HeartRateCmdResult;

    invoke-direct {v0}, Landroid/hardware/fingerprint/HeartRateCmdResult;-><init>()V

    return-object v0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I

    .line 114
    const-string v0, "com.android.app.HeartRate"

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    .line 148
    return v1

    .line 120
    :pswitch_0
    :try_start_0
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 122
    .local v0, "cmd":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 123
    .local v2, "size":I
    const/4 v3, 0x0

    .line 124
    .local v3, "val":[B
    if-ltz v2, :cond_0

    .line 125
    new-array v4, v2, [B

    move-object v3, v4

    .line 126
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->readByteArray([B)V

    .line 128
    :cond_0
    invoke-virtual {p0, v0, v3}, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->doSendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;

    move-result-object v4

    .line 129
    .local v4, "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
    invoke-virtual {p3, v4, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 130
    return v1

    .line 135
    .end local v0    # "cmd":I
    .end local v2    # "size":I
    .end local v3    # "val":[B
    .end local v4    # "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
    :pswitch_1
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHeartRateBinder:Landroid/os/IBinder;

    .line 137
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->registerCallback()V

    .line 138
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 139
    return v1

    .line 144
    :pswitch_2
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->mHeartRateBinder:Landroid/os/IBinder;

    .line 145
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    return v1

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "HeartRateFingerprintServiceStubImpl"

    const-string v2, "onTra : "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 152
    const/4 v1, 0x0

    return v1

    :pswitch_data_0
    .packed-switch 0xfffffd
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
