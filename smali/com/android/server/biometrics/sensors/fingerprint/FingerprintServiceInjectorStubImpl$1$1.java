class com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl$1$1 extends android.hardware.biometrics.BiometricStateListener {
	 /* .source "FingerprintServiceInjectorStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1;->onAllAuthenticatorsRegistered(Ljava/util/List;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl$1 this$1; //synthetic
/* # direct methods */
 com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl$1$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/android/server/biometrics/sensors/fingerprint/FingerprintServiceInjectorStubImpl$1; */
/* .line 198 */
this.this$1 = p1;
/* invoke-direct {p0}, Landroid/hardware/biometrics/BiometricStateListener;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onStateChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "newState" # I */
/* .line 202 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onStateChanged : "; // const-string v1, "onStateChanged : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FingerprintServiceInjectorStubImpl"; // const-string v1, "FingerprintServiceInjectorStubImpl"
android.util.Slog .d ( v1,v0 );
/* .line 203 */
v0 = this.this$1;
v0 = this.this$0;
com.android.server.biometrics.sensors.fingerprint.FingerprintServiceInjectorStubImpl .-$$Nest$fputmBiometricState ( v0,p1 );
/* .line 204 */
return;
} // .end method
