public abstract class com.android.server.biometrics.sensors.face.IMiFaceSessionCallback implements miui.android.services.internal.hidl.base.V1_0.IBase {
	 /* .source "IMiFaceSessionCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Proxy;, */
	 /* Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String kInterfaceName;
/* # direct methods */
public static com.android.server.biometrics.sensors.face.IMiFaceSessionCallback asInterface ( android.os.IHwBinder p0 ) {
	 /* .locals 7 */
	 /* .param p0, "binder" # Landroid/os/IHwBinder; */
	 /* .line 16 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* if-nez p0, :cond_0 */
	 /* .line 17 */
	 /* .line 20 */
} // :cond_0
/* nop */
/* .line 21 */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSessionCallback" */
/* .line 23 */
/* .local v2, "iface":Landroid/os/IHwInterface; */
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* instance-of v3, v2, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback; */
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 24 */
		 /* move-object v0, v2 */
		 /* check-cast v0, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback; */
		 /* .line 27 */
	 } // :cond_1
	 /* new-instance v3, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Proxy; */
	 /* invoke-direct {v3, p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Proxy;-><init>(Landroid/os/IHwBinder;)V */
	 /* .line 30 */
	 /* .local v3, "proxy":Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback; */
	 try { // :try_start_0
		 (( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
	 v5 = 	 } // :goto_0
	 if ( v5 != null) { // if-eqz v5, :cond_3
		 /* check-cast v5, Ljava/lang/String; */
		 /* .line 31 */
		 /* .local v5, "descriptor":Ljava/lang/String; */
		 v6 = 		 (( java.lang.String ) v5 ).equals ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 if ( v6 != null) { // if-eqz v6, :cond_2
			 /* .line 32 */
			 /* .line 34 */
		 } // .end local v5 # "descriptor":Ljava/lang/String;
	 } // :cond_2
	 /* .line 36 */
} // :cond_3
/* .line 35 */
/* :catch_0 */
/* move-exception v1 */
/* .line 38 */
} // :goto_1
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSessionCallback castFrom ( android.os.IHwInterface p0 ) {
/* .locals 1 */
/* .param p0, "iface" # Landroid/os/IHwInterface; */
/* .line 45 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
com.android.server.biometrics.sensors.face.IMiFaceSessionCallback .asInterface ( v0 );
} // :goto_0
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSessionCallback getService ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 87 */
final String v0 = "default"; // const-string v0, "default"
com.android.server.biometrics.sensors.face.IMiFaceSessionCallback .getService ( v0 );
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSessionCallback getService ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 77 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSessionCallback" */
android.os.HwBinder .getService ( v0,p0 );
com.android.server.biometrics.sensors.face.IMiFaceSessionCallback .asInterface ( v0 );
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSessionCallback getService ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p0, "serviceName" # Ljava/lang/String; */
/* .param p1, "retry" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 60 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSessionCallback" */
android.os.HwBinder .getService ( v0,p0,p1 );
com.android.server.biometrics.sensors.face.IMiFaceSessionCallback .asInterface ( v0 );
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSessionCallback getService ( Boolean p0 ) {
/* .locals 1 */
/* .param p0, "retry" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 67 */
final String v0 = "default"; // const-string v0, "default"
com.android.server.biometrics.sensors.face.IMiFaceSessionCallback .getService ( v0,p0 );
} // .end method
/* # virtual methods */
public abstract android.os.IHwBinder asBinder ( ) {
} // .end method
public abstract void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList getHashChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList interfaceChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.lang.String interfaceDescriptor ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void notifySyspropsChanged ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onAcquired ( Integer p0, Integer p1, Boolean p2 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onAuthenticationFailed ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onAuthenticationSucceeded ( Integer p0, java.util.ArrayList p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onAuthenticatorIdInvalidated ( Long p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onAuthenticatorIdRetrieved ( Long p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onChallengeGenerated ( Long p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onChallengeRevoked ( Long p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onEnrollmentProgress ( Integer p0, Integer p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onEnrollmentsEnumerated ( java.util.ArrayList p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onEnrollmentsRemoved ( java.util.ArrayList p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onError ( Integer p0, Integer p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onFeatureSet ( Integer p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onFeaturesRetrieved ( java.util.ArrayList p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onInteractionDetected ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onLockoutCleared ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onLockoutPermanent ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onLockoutTimed ( Long p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onSessionClosed ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void ping ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setHALInstrumentation ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
