public class com.android.server.biometrics.sensors.face.FaceServiceStubImpl implements com.android.server.biometrics.sensors.face.FaceServiceStub {
	 /* .source "FaceServiceStubImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.biometrics.sensors.face.FaceServiceStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void setDetectArea ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
		 /* .locals 1 */
		 /* .param p1, "left" # I */
		 /* .param p2, "top" # I */
		 /* .param p3, "right" # I */
		 /* .param p4, "bottom" # I */
		 /* .line 41 */
		 try { // :try_start_0
			 com.android.server.biometrics.sensors.face.MiuiFaceHidl .getInstance ( );
			 (( com.android.server.biometrics.sensors.face.MiuiFaceHidl ) v0 ).setDetectArea ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setDetectArea(IIII)Z
			 /* :try_end_0 */
			 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 44 */
			 /* .line 42 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 43 */
			 /* .local v0, "e":Landroid/os/RemoteException; */
			 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
			 /* .line 45 */
		 } // .end local v0 # "e":Landroid/os/RemoteException;
	 } // :goto_0
	 return;
} // .end method
public void setEnrollArea ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
	 /* .locals 1 */
	 /* .param p1, "left" # I */
	 /* .param p2, "top" # I */
	 /* .param p3, "right" # I */
	 /* .param p4, "bottom" # I */
	 /* .line 32 */
	 try { // :try_start_0
		 com.android.server.biometrics.sensors.face.MiuiFaceHidl .getInstance ( );
		 (( com.android.server.biometrics.sensors.face.MiuiFaceHidl ) v0 ).setEnrollArea ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setEnrollArea(IIII)Z
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 35 */
		 /* .line 33 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 34 */
		 /* .local v0, "e":Landroid/os/RemoteException; */
		 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
		 /* .line 36 */
	 } // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void setEnrollStep ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "steps" # I */
/* .line 50 */
try { // :try_start_0
	 com.android.server.biometrics.sensors.face.MiuiFaceHidl .getInstance ( );
	 (( com.android.server.biometrics.sensors.face.MiuiFaceHidl ) v0 ).setEnrollStep ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setEnrollStep(I)Z
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 53 */
	 /* .line 51 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 52 */
	 /* .local v0, "e":Landroid/os/RemoteException; */
	 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
	 /* .line 54 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void setEnrollSurface ( android.view.Surface p0 ) {
/* .locals 1 */
/* .param p1, "surface" # Landroid/view/Surface; */
/* .line 58 */
com.android.server.biometrics.sensors.face.MiuiFaceHidl .getInstance ( );
(( com.android.server.biometrics.sensors.face.MiuiFaceHidl ) v0 ).setEnrollSurface ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setEnrollSurface(Landroid/view/Surface;)V
/* .line 59 */
return;
} // .end method
public Boolean setPreviewNotifyCallback ( ) {
/* .locals 1 */
/* .line 20 */
try { // :try_start_0
com.android.server.biometrics.sensors.face.MiuiFaceHidl .getInstance ( );
v0 = (( com.android.server.biometrics.sensors.face.MiuiFaceHidl ) v0 ).setPreviewNotifyCallback ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setPreviewNotifyCallback()Z
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 23 */
/* :catch_0 */
/* move-exception v0 */
/* .line 24 */
/* .local v0, "e":Landroid/system/ErrnoException; */
(( android.system.ErrnoException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/system/ErrnoException;->printStackTrace()V
/* .line 21 */
} // .end local v0 # "e":Landroid/system/ErrnoException;
/* :catch_1 */
/* move-exception v0 */
/* .line 22 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 25 */
} // .end local v0 # "e":Landroid/os/RemoteException;
/* nop */
/* .line 26 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void stopGetFrame ( ) {
/* .locals 1 */
/* .line 14 */
com.android.server.biometrics.sensors.face.MiuiFaceHidl .getInstance ( );
(( com.android.server.biometrics.sensors.face.MiuiFaceHidl ) v0 ).stopGetFrame ( ); // invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->stopGetFrame()V
/* .line 15 */
return;
} // .end method
