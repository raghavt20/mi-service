.class public Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;
.super Ljava/lang/Object;
.source "MiuiFaceHidl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$MyHandler;
    }
.end annotation


# static fields
.field private static final ENROLL_HEIGHT:I

.field private static final ENROLL_WIDTH:I = 0x244

.field private static final FRAME_HEIGHT:I = 0x1e0

.field private static final FRAME_SIZE:I = 0x75800

.field private static final FRAME_WIDTH:I = 0x280

.field public static final MSG_GET_IMG_FRAME:I = 0x2711

.field private static final TAG:Ljava/lang/String; = "MiuiFaceHidl"

.field private static mCanvas:Landroid/graphics/Canvas;

.field private static mDetectRect:Landroid/graphics/RectF;

.field private static mEnrollRect:Landroid/graphics/RectF;

.field private static mEnrollSurface:Landroid/view/Surface;

.field private static volatile mFrameList:Ljava/util/ArrayList;

.field private static mGetFrame:Ljava/lang/Boolean;

.field private static volatile sInstance:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

.field static scale:D


# instance fields
.field private callback:Lcom/android/server/biometrics/sensors/face/IMiFaceProprietaryClientCallback;

.field private mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

.field mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private mHandler:Landroid/os/Handler;

.field private mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDeathRecipient(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Landroid/os/IHwBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Lcom/android/server/biometrics/sensors/face/IMiFace;
    .locals 0

    iget-object p0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmMiuiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;Lcom/android/server/biometrics/sensors/face/IMiFace;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetMiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Lcom/android/server/biometrics/sensors/face/IMiFace;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getMiFace()Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetmGetFrame()Ljava/lang/Boolean;
    .locals 1

    sget-object v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mGetFrame:Ljava/lang/Boolean;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputmGetFrame(Ljava/lang/Boolean;)V
    .locals 0

    sput-object p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mGetFrame:Ljava/lang/Boolean;

    return-void
.end method

.method static bridge synthetic -$$Nest$smnv12ToBitmap([BII)Landroid/graphics/Bitmap;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->nv12ToBitmap([BII)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 50
    const-wide v0, 0x3ff3555555555555L    # 1.2083333333333333

    sput-wide v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->scale:D

    .line 52
    const-wide/high16 v2, 0x4084000000000000L    # 640.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    sput v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->ENROLL_HEIGHT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    .line 69
    new-instance v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;

    invoke-direct {v0, p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;-><init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    .line 157
    new-instance v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$2;

    invoke-direct {v0, p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$2;-><init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->callback:Lcom/android/server/biometrics/sensors/face/IMiFaceProprietaryClientCallback;

    return-void
.end method

.method private MiuiFaceHidl()V
    .locals 0

    .line 58
    return-void
.end method

.method public static byteArr2Sm([B)Landroid/os/SharedMemory;
    .locals 3
    .param p0, "msg"    # [B

    .line 321
    :try_start_0
    const-string v0, "miuifacehidl"

    const/high16 v1, 0x80000

    invoke-static {v0, v1}, Landroid/os/SharedMemory;->create(Ljava/lang/String;I)Landroid/os/SharedMemory;

    move-result-object v0

    .line 322
    .local v0, "sharedMemory":Landroid/os/SharedMemory;
    invoke-virtual {v0}, Landroid/os/SharedMemory;->mapReadWrite()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 323
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    array-length v2, p0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 324
    invoke-virtual {v1, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    nop

    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    goto :goto_0

    .line 326
    .end local v0    # "sharedMemory":Landroid/os/SharedMemory;
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiFaceHidl"

    const-string/jumbo v2, "str2Sm error ! e = "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 328
    const/4 v1, 0x0

    move-object v0, v1

    .line 330
    .local v0, "sharedMemory":Landroid/os/SharedMemory;
    :goto_0
    return-object v0
.end method

.method public static byteToHex([B)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # [B

    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 383
    .local v0, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    .line 384
    aget-byte v2, p0, v1

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    .line 385
    .local v2, "v2":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_0
    move-object v3, v2

    .line 386
    .local v3, "v3":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    .end local v2    # "v2":Ljava/lang/String;
    .end local v3    # "v3":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 388
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getInstance()Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;
    .locals 2

    .line 60
    sget-object v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->sInstance:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    if-nez v0, :cond_1

    .line 61
    const-class v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    monitor-enter v0

    .line 62
    :try_start_0
    sget-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->sInstance:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    if-nez v1, :cond_0

    .line 63
    new-instance v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-direct {v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;-><init>()V

    sput-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->sInstance:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    .line 65
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 67
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->sInstance:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    return-object v0
.end method

.method private getMiFace()Lcom/android/server/biometrics/sensors/face/IMiFace;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    if-nez v0, :cond_0

    .line 108
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/IMiFace;->getService()Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    .line 109
    new-instance v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$MyHandler;

    invoke-static {}, Lcom/android/server/MiuiFgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$MyHandler;-><init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mHandler:Landroid/os/Handler;

    .line 110
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v3, 0x3

    const v4, 0x7fffffff

    const-wide/16 v5, 0x1f4

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v8}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mFrameList:Ljava/util/ArrayList;

    .line 112
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/android/server/biometrics/sensors/face/IMiFace;->linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    return-object v0
.end method

.method public static intToByte(I)[B
    .locals 6
    .param p0, "value"    # I

    .line 286
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 287
    .local v0, "b":[B
    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    .line 288
    .local v1, "v2":B
    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 289
    shr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    .line 290
    .local v2, "v2_2":B
    const/4 v3, 0x1

    aput-byte v2, v0, v3

    .line 291
    shr-int/lit8 v3, p0, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    .line 292
    .local v3, "v2_3":B
    const/4 v4, 0x2

    aput-byte v3, v0, v4

    .line 293
    shr-int/lit8 v4, p0, 0x18

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    const/4 v5, 0x3

    aput-byte v4, v0, v5

    .line 294
    return-object v0
.end method

.method public static intToBytesBig(I)[B
    .locals 6
    .param p0, "value"    # I

    .line 297
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 298
    .local v0, "src":[B
    shr-int/lit8 v1, p0, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    .line 299
    .local v1, "v2":B
    const/4 v2, 0x0

    aput-byte v1, v0, v2

    .line 300
    shr-int/lit8 v2, p0, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    .line 301
    .local v2, "v2_2":B
    const/4 v3, 0x1

    aput-byte v2, v0, v3

    .line 302
    shr-int/lit8 v3, p0, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    .line 303
    .local v3, "v2_3":B
    const/4 v4, 0x2

    aput-byte v3, v0, v4

    .line 304
    and-int/lit16 v4, p0, 0xff

    int-to-byte v4, v4

    const/4 v5, 0x3

    aput-byte v4, v0, v5

    .line 305
    return-object v0
.end method

.method public static intToBytesLittle(I)[B
    .locals 6
    .param p0, "value"    # I

    .line 308
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 309
    .local v0, "src":[B
    shr-int/lit8 v1, p0, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    .line 310
    .local v1, "v2":B
    const/4 v2, 0x3

    aput-byte v1, v0, v2

    .line 311
    shr-int/lit8 v2, p0, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    .line 312
    .local v2, "v2_2":B
    const/4 v3, 0x2

    aput-byte v2, v0, v3

    .line 313
    shr-int/lit8 v3, p0, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    .line 314
    .local v3, "v2_3":B
    const/4 v4, 0x1

    aput-byte v3, v0, v4

    .line 315
    and-int/lit16 v4, p0, 0xff

    int-to-byte v4, v4

    const/4 v5, 0x0

    aput-byte v4, v0, v5

    .line 316
    return-object v0
.end method

.method private static nv12ToBitmap([BII)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "data"    # [B
    .param p1, "w"    # I
    .param p2, "h"    # I

    .line 345
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->spToBitmap([BIIII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static nv21ToBitmap([BII)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "data"    # [B
    .param p1, "w"    # I
    .param p2, "h"    # I

    .line 348
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->spToBitmap([BIIII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static rotateBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "angle"    # I
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .line 273
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 274
    .local v0, "matrix":Landroid/graphics/Matrix;
    int-to-float v1, p0

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 275
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 276
    sget-wide v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->scale:D

    double-to-float v3, v1

    double-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 277
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 278
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/4 v7, 0x1

    .line 277
    move-object v1, p1

    move-object v6, v0

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 279
    .local v1, "resizedBitmap":Landroid/graphics/Bitmap;
    if-eq v1, p1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 280
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 281
    const/4 p1, 0x0

    .line 283
    :cond_0
    return-object v1
.end method

.method public static saveBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "name"    # Ljava/lang/String;

    .line 236
    if-nez p0, :cond_0

    return-void

    .line 237
    :cond_0
    const-string v0, "/data/system/files/"

    .line 238
    .local v0, "mediaStorageDir":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".png"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 240
    .local v1, "bitmapFileName":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 242
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 244
    :cond_1
    const-string v3, "MiuiFaceHidl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 246
    .local v3, "fos":Ljava/io/FileOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 247
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 248
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 249
    :catch_0
    move-exception v2

    .line 250
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 252
    .end local v2    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method public static saveFile([BLjava/lang/String;)V
    .locals 6
    .param p0, "bitmap"    # [B
    .param p1, "name"    # Ljava/lang/String;

    .line 254
    if-nez p0, :cond_0

    return-void

    .line 255
    :cond_0
    const-string v0, "/data/system/files/"

    .line 256
    .local v0, "mediaStorageDir":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".yuv"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, "fileName":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 260
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 262
    :cond_1
    const-string v3, "MiuiFaceHidl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 264
    .local v3, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {v3, p0}, Ljava/io/FileOutputStream;->write([B)V

    .line 266
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 267
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 268
    :catch_0
    move-exception v2

    .line 269
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 271
    .end local v2    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method public static showImage(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .line 218
    sget-object v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mFrameList:Ljava/util/ArrayList;

    monitor-enter v0

    .line 219
    :try_start_0
    sget-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mGetFrame:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 220
    monitor-exit v0

    return-void

    .line 222
    :cond_0
    sget-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mDetectRect:Landroid/graphics/RectF;

    if-nez v1, :cond_1

    .line 223
    new-instance v1, Landroid/graphics/RectF;

    const/high16 v2, 0x43c80000    # 400.0f

    const/high16 v3, 0x43fa0000    # 500.0f

    const/high16 v4, 0x428c0000    # 70.0f

    const/high16 v5, 0x431c0000    # 156.0f

    invoke-direct {v1, v4, v5, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mDetectRect:Landroid/graphics/RectF;

    .line 225
    :cond_1
    sget-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mEnrollSurface:Landroid/view/Surface;

    new-instance v2, Landroid/graphics/Rect;

    const/16 v3, 0x1f4

    const/16 v4, 0x244

    const/4 v5, 0x0

    invoke-direct {v2, v5, v5, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v2}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v1

    sput-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mCanvas:Landroid/graphics/Canvas;

    .line 226
    const/16 v2, 0x1b8

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setDensity(I)V

    .line 227
    sget-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mCanvas:Landroid/graphics/Canvas;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v5, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 228
    sget-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mCanvas:Landroid/graphics/Canvas;

    new-instance v2, Landroid/graphics/Rect;

    .line 230
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v2, v5, v5, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    sget v6, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->ENROLL_HEIGHT:I

    int-to-float v6, v6

    const v7, 0x3f19999a    # 0.6f

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-direct {v3, v5, v5, v4, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 228
    invoke-virtual {v1, p0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 232
    sget-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mEnrollSurface:Landroid/view/Surface;

    sget-object v2, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v2}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 233
    monitor-exit v0

    .line 234
    return-void

    .line 233
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static sm2Byte(Landroid/os/SharedMemory;I)[B
    .locals 4
    .param p0, "sm"    # Landroid/os/SharedMemory;
    .param p1, "len"    # I

    .line 333
    const/4 v0, 0x0

    .line 335
    .local v0, "buffArr":[B
    :try_start_0
    invoke-virtual {p0}, Landroid/os/SharedMemory;->mapReadWrite()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 336
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    new-array v2, p1, [B

    move-object v0, v2

    .line 337
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    nop

    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    goto :goto_0

    .line 339
    :catch_0
    move-exception v1

    .line 340
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sm2Byte error ! e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFaceHidl"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v0
.end method

.method private static spToBitmap([BIIII)Landroid/graphics/Bitmap;
    .locals 18
    .param p0, "data"    # [B
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "uOff"    # I
    .param p4, "vOff"    # I

    .line 351
    move/from16 v0, p1

    move/from16 v1, p2

    mul-int v2, v0, v1

    .line 352
    .local v2, "plane":I
    new-array v3, v2, [I

    .line 353
    .local v3, "colors":[I
    const/4 v4, 0x0

    .local v4, "yPos":I
    move v5, v2

    .line 354
    .local v5, "uvPos":I
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_0
    if-ge v6, v1, :cond_9

    .line 355
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v0, :cond_7

    .line 357
    aget-byte v8, p0, v4

    and-int/lit16 v8, v8, 0xff

    .line 358
    .local v8, "y1":I
    add-int v9, v5, p3

    aget-byte v9, p0, v9

    and-int/lit16 v9, v9, 0xff

    add-int/lit8 v9, v9, -0x80

    .line 359
    .local v9, "u":I
    add-int v10, v5, p4

    aget-byte v10, p0, v10

    and-int/lit16 v10, v10, 0xff

    add-int/lit8 v10, v10, -0x80

    .line 360
    .local v10, "v":I
    mul-int/lit16 v11, v8, 0x4a8

    .line 361
    .local v11, "y1192":I
    mul-int/lit16 v12, v10, 0x662

    add-int/2addr v12, v11

    .line 362
    .local v12, "r":I
    mul-int/lit16 v13, v10, 0x341

    sub-int v13, v11, v13

    mul-int/lit16 v14, v9, 0x190

    sub-int/2addr v13, v14

    .line 363
    .local v13, "g":I
    mul-int/lit16 v14, v9, 0x812

    add-int/2addr v14, v11

    .line 364
    .local v14, "b":I
    const v15, 0x3ffff

    if-gez v12, :cond_0

    const/16 v17, 0x0

    goto :goto_2

    :cond_0
    if-le v12, v15, :cond_1

    move/from16 v17, v15

    goto :goto_2

    :cond_1
    move/from16 v17, v12

    :goto_2
    move/from16 v12, v17

    .line 365
    if-gez v13, :cond_2

    const/16 v17, 0x0

    goto :goto_3

    :cond_2
    if-le v13, v15, :cond_3

    move/from16 v17, v15

    goto :goto_3

    :cond_3
    move/from16 v17, v13

    :goto_3
    move/from16 v13, v17

    .line 366
    if-gez v14, :cond_4

    const/4 v15, 0x0

    goto :goto_4

    :cond_4
    if-le v14, v15, :cond_5

    goto :goto_4

    :cond_5
    move v15, v14

    :goto_4
    move v14, v15

    .line 367
    shl-int/lit8 v15, v12, 0x6

    const/high16 v16, 0xff0000

    and-int v15, v15, v16

    shr-int/lit8 v16, v13, 0x2

    const v17, 0xff00

    and-int v16, v16, v17

    or-int v15, v15, v16

    move/from16 v16, v2

    .end local v2    # "plane":I
    .local v16, "plane":I
    shr-int/lit8 v2, v14, 0xa

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v15

    aput v2, v3, v4

    .line 370
    add-int/lit8 v2, v4, 0x1

    .end local v4    # "yPos":I
    .local v2, "yPos":I
    and-int/lit8 v4, v4, 0x1

    const/4 v15, 0x1

    if-ne v4, v15, :cond_6

    .line 371
    add-int/lit8 v5, v5, 0x2

    .line 355
    .end local v8    # "y1":I
    .end local v9    # "u":I
    .end local v10    # "v":I
    .end local v11    # "y1192":I
    .end local v12    # "r":I
    .end local v13    # "g":I
    .end local v14    # "b":I
    :cond_6
    add-int/lit8 v7, v7, 0x1

    move v4, v2

    move/from16 v2, v16

    goto :goto_1

    .end local v16    # "plane":I
    .local v2, "plane":I
    .restart local v4    # "yPos":I
    :cond_7
    move/from16 v16, v2

    .line 374
    .end local v2    # "plane":I
    .end local v7    # "i":I
    .restart local v16    # "plane":I
    and-int/lit8 v2, v6, 0x1

    if-nez v2, :cond_8

    .line 375
    sub-int/2addr v5, v0

    .line 354
    :cond_8
    add-int/lit8 v6, v6, 0x1

    move/from16 v2, v16

    goto/16 :goto_0

    .end local v16    # "plane":I
    .restart local v2    # "plane":I
    :cond_9
    move/from16 v16, v2

    .line 378
    .end local v2    # "plane":I
    .end local v6    # "j":I
    .restart local v16    # "plane":I
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getPreviewBuffer()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 117
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mGetFrame:Ljava/lang/Boolean;

    .line 118
    invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->startPreview()V

    .line 119
    return v0
.end method

.method public setDetectArea(IIII)Z
    .locals 9
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 122
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/IMiFace;->getService()Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v0

    .line 124
    .local v0, "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace;
    sub-int v1, p3, p1

    add-int/lit16 v1, v1, -0x122

    .line 125
    .local v1, "teeLeft":I
    if-lez v1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    move v1, v2

    .line 126
    sub-int v2, p3, p1

    add-int/lit16 v2, v2, 0x122

    .line 127
    .local v2, "teeRight":I
    invoke-interface {v0, v1, p2, v2, p4}, Lcom/android/server/biometrics/sensors/face/IMiFace;->setDetectArea(IIII)Z

    move-result v3

    .line 128
    .local v3, "res":Z
    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, p1

    int-to-float v6, p2

    int-to-float v7, p3

    int-to-float v8, p4

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v4, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mDetectRect:Landroid/graphics/RectF;

    .line 129
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setDetectArea left: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", top: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", right: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bottom: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", res: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MiuiFaceHidl"

    invoke-static {v5, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    return v3
.end method

.method public setEnrollArea(IIII)Z
    .locals 7
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 133
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/IMiFace;->getService()Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v0

    .line 134
    .local v0, "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace;
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/server/biometrics/sensors/face/IMiFace;->setEnrollArea(IIII)Z

    move-result v1

    .line 135
    .local v1, "res":Z
    new-instance v2, Landroid/graphics/RectF;

    int-to-float v3, p1

    int-to-float v4, p2

    int-to-float v5, p3

    int-to-float v6, p4

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v2, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mEnrollRect:Landroid/graphics/RectF;

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setEnrollArea left: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", top: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", right: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", bottom: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", res: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFaceHidl"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    return v1
.end method

.method public setEnrollStep(I)Z
    .locals 4
    .param p1, "steps"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 140
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/IMiFace;->getService()Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v0

    .line 141
    .local v0, "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace;
    invoke-interface {v0, p1}, Lcom/android/server/biometrics/sensors/face/IMiFace;->setEnrollStep(I)Z

    move-result v1

    .line 142
    .local v1, "res":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setEnrollStep steps: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", res: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiFaceHidl"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return v1
.end method

.method public setEnrollSurface(Landroid/view/Surface;)V
    .locals 0
    .param p1, "surface"    # Landroid/view/Surface;

    .line 154
    sput-object p1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mEnrollSurface:Landroid/view/Surface;

    .line 155
    return-void
.end method

.method public setPreviewNotifyCallback()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 147
    invoke-direct {p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getMiFace()Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    .line 148
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mGetFrame:Ljava/lang/Boolean;

    .line 149
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mMiuiFace:Lcom/android/server/biometrics/sensors/face/IMiFace;

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->callback:Lcom/android/server/biometrics/sensors/face/IMiFaceProprietaryClientCallback;

    invoke-interface {v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFace;->setPreviewNotifyCallback(Lcom/android/server/biometrics/sensors/face/IMiFaceProprietaryClientCallback;)Z

    move-result v0

    .line 150
    .local v0, "res":Z
    return v0
.end method

.method public startPreview()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 174
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3;

    invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3;-><init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 216
    return-void
.end method

.method public stopGetFrame()V
    .locals 1

    .line 170
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mGetFrame:Ljava/lang/Boolean;

    .line 171
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->mCanvas:Landroid/graphics/Canvas;

    .line 172
    return-void
.end method
