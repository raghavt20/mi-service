.class public Lcom/android/server/biometrics/sensors/face/FaceServiceStubImpl;
.super Ljava/lang/Object;
.source "FaceServiceStubImpl.java"

# interfaces
.implements Lcom/android/server/biometrics/sensors/face/FaceServiceStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setDetectArea(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 41
    :try_start_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getInstance()Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setDetectArea(IIII)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 45
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public setEnrollArea(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 32
    :try_start_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getInstance()Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setEnrollArea(IIII)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    goto :goto_0

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 36
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public setEnrollStep(I)V
    .locals 1
    .param p1, "steps"    # I

    .line 50
    :try_start_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getInstance()Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setEnrollStep(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 54
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public setEnrollSurface(Landroid/view/Surface;)V
    .locals 1
    .param p1, "surface"    # Landroid/view/Surface;

    .line 58
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getInstance()Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setEnrollSurface(Landroid/view/Surface;)V

    .line 59
    return-void
.end method

.method public setPreviewNotifyCallback()Z
    .locals 1

    .line 20
    :try_start_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getInstance()Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->setPreviewNotifyCallback()Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Landroid/system/ErrnoException;
    invoke-virtual {v0}, Landroid/system/ErrnoException;->printStackTrace()V

    goto :goto_0

    .line 21
    .end local v0    # "e":Landroid/system/ErrnoException;
    :catch_1
    move-exception v0

    .line 22
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 25
    .end local v0    # "e":Landroid/os/RemoteException;
    nop

    .line 26
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public stopGetFrame()V
    .locals 1

    .line 14
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getInstance()Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->stopGetFrame()V

    .line 15
    return-void
.end method
