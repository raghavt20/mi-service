.class Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;
.super Ljava/lang/Object;
.source "MiuiFaceHidl.java"

# interfaces
.implements Landroid/os/IHwBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;


# direct methods
.method constructor <init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    .line 69
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serviceDied(J)V
    .locals 2
    .param p1, "cookie"    # J

    .line 72
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$fgetmMiuiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v0

    if-nez v0, :cond_0

    .line 73
    return-void

    .line 76
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$fgetmMiuiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-static {v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$fgetmDeathRecipient(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Landroid/os/IHwBinder$DeathRecipient;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFace;->unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z

    .line 77
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$fputmMiuiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;Lcom/android/server/biometrics/sensors/face/IMiFace;)V

    .line 78
    iget-object v0, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$mgetMiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$fputmMiuiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;Lcom/android/server/biometrics/sensors/face/IMiFace;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Landroid/system/ErrnoException;
    invoke-virtual {v0}, Landroid/system/ErrnoException;->printStackTrace()V

    goto :goto_1

    .line 79
    .end local v0    # "e":Landroid/system/ErrnoException;
    :catch_1
    move-exception v0

    .line 80
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 83
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    nop

    .line 84
    :goto_1
    return-void
.end method
