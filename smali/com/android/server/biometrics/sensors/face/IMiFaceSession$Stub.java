public abstract class com.android.server.biometrics.sensors.face.IMiFaceSession$Stub extends android.os.HwBinder implements com.android.server.biometrics.sensors.face.IMiFaceSession {
	 /* .source "IMiFaceSession.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/face/IMiFaceSession; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ( ) {
/* .locals 0 */
/* .line 691 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 694 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 707 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 744 */
/* new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 745 */
/* .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 746 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 747 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 748 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 719 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_1 */
/* filled-new-array {v2, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 699 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 713 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .line 732 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 754 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 756 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "_hidl_request" # Landroid/os/HwParcel; */
/* .param p3, "_hidl_reply" # Landroid/os/HwParcel; */
/* .param p4, "_hidl_flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 784 */
final String v0 = "android.hidl.base@1.0::IBase"; // const-string v0, "android.hidl.base@1.0::IBase"
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
int v2 = 0; // const/4 v2, 0x0
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 1042 */
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1044 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->notifySyspropsChanged()V
/* .line 1045 */
/* goto/16 :goto_1 */
/* .line 1031 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1033 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* .line 1034 */
/* .local v0, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1035 */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( p3 ); // invoke-virtual {v0, p3}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 1036 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1037 */
/* goto/16 :goto_1 */
/* .line 1021 */
} // .end local v0 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1023 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->ping()V
/* .line 1024 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1025 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1026 */
/* goto/16 :goto_1 */
/* .line 1016 */
/* :sswitch_3 */
/* goto/16 :goto_1 */
/* .line 1008 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1010 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->setHALInstrumentation()V
/* .line 1011 */
/* goto/16 :goto_1 */
/* .line 974 */
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 976 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 977 */
/* .local v0, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 979 */
/* new-instance v1, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v1, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 981 */
/* .local v1, "_hidl_blob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 982 */
/* .local v3, "_hidl_vec_size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v1 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v1, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 983 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v1 ).putBool ( v4, v5, v2 ); // invoke-virtual {v1, v4, v5, v2}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 984 */
/* new-instance v2, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v2, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 985 */
/* .local v2, "childBlob":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "_hidl_index_0":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 987 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 988 */
/* .local v5, "_hidl_array_offset_1":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 990 */
/* .local v7, "_hidl_array_item_1":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 994 */
(( android.os.HwBlob ) v2 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v2, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 995 */
/* nop */
/* .line 985 */
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 991 */
/* .restart local v5 # "_hidl_array_offset_1":J */
/* .restart local v7 # "_hidl_array_item_1":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 998 */
} // .end local v4 # "_hidl_index_0":I
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v1 ).putBlob ( v4, v5, v2 ); // invoke-virtual {v1, v4, v5, v2}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 1000 */
} // .end local v2 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_vec_size":I
(( android.os.HwParcel ) p3 ).writeBuffer ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 1002 */
} // .end local v1 # "_hidl_blob":Landroid/os/HwBlob;
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1003 */
/* goto/16 :goto_1 */
/* .line 963 */
} // .end local v0 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 965 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 966 */
/* .local v0, "_hidl_out_descriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 967 */
(( android.os.HwParcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 968 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 969 */
/* goto/16 :goto_1 */
/* .line 951 */
} // .end local v0 # "_hidl_out_descriptor":Ljava/lang/String;
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 953 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 954 */
/* .local v0, "fd":Landroid/os/NativeHandle; */
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* .line 955 */
/* .local v1, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).debug ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 956 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 957 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 958 */
/* goto/16 :goto_1 */
/* .line 940 */
} // .end local v0 # "fd":Landroid/os/NativeHandle;
} // .end local v1 # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 942 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 943 */
/* .local v0, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 944 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 945 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 946 */
/* goto/16 :goto_1 */
/* .line 929 */
} // .end local v0 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 931 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).cancel ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->cancel()I
/* .line 932 */
/* .local v0, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 933 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 934 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 935 */
/* goto/16 :goto_1 */
/* .line 918 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 920 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).close ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->close()I
/* .line 921 */
/* .restart local v0 # "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 922 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 923 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 924 */
/* goto/16 :goto_1 */
/* .line 906 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 908 */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 909 */
/* .local v0, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v1 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).resetLockout ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->resetLockout(Ljava/util/ArrayList;)I
/* .line 910 */
/* .local v1, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 911 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 912 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 913 */
/* goto/16 :goto_1 */
/* .line 895 */
} // .end local v0 # "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v1 # "_hidl_out_status":I
/* :sswitch_c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 897 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).invalidateAuthenticatorId ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->invalidateAuthenticatorId()I
/* .line 898 */
/* .local v0, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 899 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 900 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 901 */
/* goto/16 :goto_1 */
/* .line 884 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 886 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).getAuthenticatorId ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->getAuthenticatorId()J
/* move-result-wide v0 */
/* .line 887 */
/* .local v0, "_hidl_out_result":J */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 888 */
(( android.os.HwParcel ) p3 ).writeInt64 ( v0, v1 ); // invoke-virtual {p3, v0, v1}, Landroid/os/HwParcel;->writeInt64(J)V
/* .line 889 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 890 */
/* goto/16 :goto_1 */
/* .line 870 */
} // .end local v0 # "_hidl_out_result":J
/* :sswitch_e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 872 */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 873 */
/* .local v0, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 874 */
/* .local v1, "feature":I */
v3 = (( android.os.HwParcel ) p2 ).readBool ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readBool()Z
/* .line 875 */
/* .local v3, "enabled":Z */
v4 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).setFeature ( v0, v1, v3 ); // invoke-virtual {p0, v0, v1, v3}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->setFeature(Ljava/util/ArrayList;IZ)I
/* .line 876 */
/* .local v4, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 877 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v4 ); // invoke-virtual {p3, v4}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 878 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 879 */
/* goto/16 :goto_1 */
/* .line 859 */
} // .end local v0 # "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v1 # "feature":I
} // .end local v3 # "enabled":Z
} // .end local v4 # "_hidl_out_status":I
/* :sswitch_f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 861 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).getFeatures ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->getFeatures()I
/* .line 862 */
/* .local v0, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 863 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 864 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 865 */
/* goto/16 :goto_1 */
/* .line 847 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_10 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 849 */
(( android.os.HwParcel ) p2 ).readInt32Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;
/* .line 850 */
/* .local v0, "enrollmentIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v1 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).removeEnrollments ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->removeEnrollments(Ljava/util/ArrayList;)I
/* .line 851 */
/* .local v1, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 852 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 853 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 854 */
/* .line 836 */
} // .end local v0 # "enrollmentIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v1 # "_hidl_out_status":I
/* :sswitch_11 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 838 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).enumerateEnrollments ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->enumerateEnrollments()I
/* .line 839 */
/* .local v0, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 840 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 841 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 842 */
/* .line 824 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_12 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 826 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 827 */
/* .local v0, "operationId":J */
v3 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).authenticate ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->authenticate(J)I
/* .line 828 */
/* .local v3, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 829 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 830 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 831 */
/* .line 810 */
} // .end local v0 # "operationId":J
} // .end local v3 # "_hidl_out_status":I
/* :sswitch_13 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 812 */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 813 */
/* .local v0, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
(( android.os.HwParcel ) p2 ).readInt32Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;
/* .line 814 */
/* .local v1, "features":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 815 */
/* .local v3, "previewSurface":Landroid/os/NativeHandle; */
v4 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).enroll ( v0, v1, v3 ); // invoke-virtual {p0, v0, v1, v3}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->enroll(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/os/NativeHandle;)I
/* .line 816 */
/* .restart local v4 # "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 817 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v4 ); // invoke-virtual {p3, v4}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 818 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 819 */
/* .line 798 */
} // .end local v0 # "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v1 # "features":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v3 # "previewSurface":Landroid/os/NativeHandle;
} // .end local v4 # "_hidl_out_status":I
/* :sswitch_14 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 800 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 801 */
/* .local v0, "challenge":J */
v3 = (( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).revokeChallenge ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->revokeChallenge(J)I
/* .line 802 */
/* .local v3, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 803 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 804 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 805 */
/* .line 787 */
} // .end local v0 # "challenge":J
} // .end local v3 # "_hidl_out_status":I
/* :sswitch_15 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 789 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).generateChallenge ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->generateChallenge()J
/* move-result-wide v0 */
/* .line 790 */
/* .local v0, "_hidl_out_status":J */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 791 */
(( android.os.HwParcel ) p3 ).writeInt64 ( v0, v1 ); // invoke-virtual {p3, v0, v1}, Landroid/os/HwParcel;->writeInt64(J)V
/* .line 792 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 793 */
/* nop */
/* .line 1054 */
} // .end local v0 # "_hidl_out_status":J
} // :goto_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_15 */
/* 0x2 -> :sswitch_14 */
/* 0x3 -> :sswitch_13 */
/* 0x4 -> :sswitch_12 */
/* 0x5 -> :sswitch_11 */
/* 0x6 -> :sswitch_10 */
/* 0x7 -> :sswitch_f */
/* 0x8 -> :sswitch_e */
/* 0x9 -> :sswitch_d */
/* 0xa -> :sswitch_c */
/* 0xb -> :sswitch_b */
/* 0xc -> :sswitch_a */
/* 0xd -> :sswitch_9 */
/* 0xf43484e -> :sswitch_8 */
/* 0xf444247 -> :sswitch_7 */
/* 0xf445343 -> :sswitch_6 */
/* 0xf485348 -> :sswitch_5 */
/* 0xf494e54 -> :sswitch_4 */
/* 0xf4c5444 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 738 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "descriptor" # Ljava/lang/String; */
/* .line 766 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 767 */
/* .line 769 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 773 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->registerService(Ljava/lang/String;)V
/* .line 774 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 728 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 778 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 760 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
