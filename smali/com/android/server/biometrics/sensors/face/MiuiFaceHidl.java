public class com.android.server.biometrics.sensors.face.MiuiFaceHidl {
	 /* .source "MiuiFaceHidl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$MyHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ENROLL_HEIGHT;
private static final Integer ENROLL_WIDTH;
private static final Integer FRAME_HEIGHT;
private static final Integer FRAME_SIZE;
private static final Integer FRAME_WIDTH;
public static final Integer MSG_GET_IMG_FRAME;
private static final java.lang.String TAG;
private static android.graphics.Canvas mCanvas;
private static android.graphics.RectF mDetectRect;
private static android.graphics.RectF mEnrollRect;
private static android.view.Surface mEnrollSurface;
private static volatile java.util.ArrayList mFrameList;
private static java.lang.Boolean mGetFrame;
private static volatile com.android.server.biometrics.sensors.face.MiuiFaceHidl sInstance;
static Double scale;
/* # instance fields */
private com.android.server.biometrics.sensors.face.IMiFaceProprietaryClientCallback callback;
private android.os.IHwBinder$DeathRecipient mDeathRecipient;
java.util.concurrent.ThreadPoolExecutor mExecutor;
private android.os.Handler mHandler;
private com.android.server.biometrics.sensors.face.IMiFace mMiuiFace;
/* # direct methods */
static android.os.IHwBinder$DeathRecipient -$$Nest$fgetmDeathRecipient ( com.android.server.biometrics.sensors.face.MiuiFaceHidl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDeathRecipient;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.android.server.biometrics.sensors.face.MiuiFaceHidl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static com.android.server.biometrics.sensors.face.IMiFace -$$Nest$fgetmMiuiFace ( com.android.server.biometrics.sensors.face.MiuiFaceHidl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mMiuiFace;
} // .end method
static void -$$Nest$fputmMiuiFace ( com.android.server.biometrics.sensors.face.MiuiFaceHidl p0, com.android.server.biometrics.sensors.face.IMiFace p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mMiuiFace = p1;
	 return;
} // .end method
static com.android.server.biometrics.sensors.face.IMiFace -$$Nest$mgetMiFace ( com.android.server.biometrics.sensors.face.MiuiFaceHidl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getMiFace()Lcom/android/server/biometrics/sensors/face/IMiFace; */
} // .end method
static java.lang.Boolean -$$Nest$sfgetmGetFrame ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mGetFrame;
} // .end method
static void -$$Nest$sfputmGetFrame ( java.lang.Boolean p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 return;
} // .end method
static android.graphics.Bitmap -$$Nest$smnv12ToBitmap ( Object[] p0, Integer p1, Integer p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 com.android.server.biometrics.sensors.face.MiuiFaceHidl .nv12ToBitmap ( p0,p1,p2 );
} // .end method
static com.android.server.biometrics.sensors.face.MiuiFaceHidl ( ) {
	 /* .locals 4 */
	 /* .line 50 */
	 /* const-wide v0, 0x3ff3555555555555L # 1.2083333333333333 */
	 /* sput-wide v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->scale:D */
	 /* .line 52 */
	 /* const-wide/high16 v2, 0x4084000000000000L # 640.0 */
	 /* mul-double/2addr v0, v2 */
	 /* double-to-int v0, v0 */
	 return;
} // .end method
public com.android.server.biometrics.sensors.face.MiuiFaceHidl ( ) {
	 /* .locals 1 */
	 /* .line 35 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 38 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mMiuiFace = v0;
	 /* .line 69 */
	 /* new-instance v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$1;-><init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)V */
	 this.mDeathRecipient = v0;
	 /* .line 157 */
	 /* new-instance v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$2;-><init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)V */
	 this.callback = v0;
	 return;
} // .end method
private void MiuiFaceHidl ( ) {
	 /* .locals 0 */
	 /* .line 58 */
	 return;
} // .end method
public static android.os.SharedMemory byteArr2Sm ( Object[] p0 ) {
	 /* .locals 3 */
	 /* .param p0, "msg" # [B */
	 /* .line 321 */
	 try { // :try_start_0
		 final String v0 = "miuifacehidl"; // const-string v0, "miuifacehidl"
		 /* const/high16 v1, 0x80000 */
		 android.os.SharedMemory .create ( v0,v1 );
		 /* .line 322 */
		 /* .local v0, "sharedMemory":Landroid/os/SharedMemory; */
		 (( android.os.SharedMemory ) v0 ).mapReadWrite ( ); // invoke-virtual {v0}, Landroid/os/SharedMemory;->mapReadWrite()Ljava/nio/ByteBuffer;
		 /* .line 323 */
		 /* .local v1, "buffer":Ljava/nio/ByteBuffer; */
		 /* array-length v2, p0 */
		 (( java.nio.ByteBuffer ) v1 ).putInt ( v2 ); // invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
		 /* .line 324 */
		 (( java.nio.ByteBuffer ) v1 ).put ( p0 ); // invoke-virtual {v1, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 329 */
		 /* nop */
	 } // .end local v1 # "buffer":Ljava/nio/ByteBuffer;
	 /* .line 326 */
} // .end local v0 # "sharedMemory":Landroid/os/SharedMemory;
/* :catch_0 */
/* move-exception v0 */
/* .line 327 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MiuiFaceHidl"; // const-string v1, "MiuiFaceHidl"
/* const-string/jumbo v2, "str2Sm error ! e = " */
android.util.Slog .e ( v1,v2,v0 );
/* .line 328 */
int v1 = 0; // const/4 v1, 0x0
/* move-object v0, v1 */
/* .line 330 */
/* .local v0, "sharedMemory":Landroid/os/SharedMemory; */
} // :goto_0
} // .end method
public static java.lang.String byteToHex ( Object[] p0 ) {
/* .locals 5 */
/* .param p0, "bytes" # [B */
/* .line 381 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = ""; // const-string v1, ""
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 383 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "n":I */
} // :goto_0
/* array-length v2, p0 */
/* if-ge v1, v2, :cond_1 */
/* .line 384 */
/* aget-byte v2, p0, v1 */
/* and-int/lit16 v2, v2, 0xff */
java.lang.Integer .toHexString ( v2 );
/* .line 385 */
/* .local v2, "v2":Ljava/lang/String; */
v3 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_0 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "0"; // const-string v4, "0"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :cond_0
/* move-object v3, v2 */
/* .line 386 */
/* .local v3, "v3":Ljava/lang/String; */
} // :goto_1
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 383 */
} // .end local v2 # "v2":Ljava/lang/String;
} // .end local v3 # "v3":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 388 */
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) v2 ).toUpperCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
} // .end method
public static com.android.server.biometrics.sensors.face.MiuiFaceHidl getInstance ( ) {
/* .locals 2 */
/* .line 60 */
v0 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.sInstance;
/* if-nez v0, :cond_1 */
/* .line 61 */
/* const-class v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl; */
/* monitor-enter v0 */
/* .line 62 */
try { // :try_start_0
v1 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.sInstance;
/* if-nez v1, :cond_0 */
/* .line 63 */
/* new-instance v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl; */
/* invoke-direct {v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;-><init>()V */
/* .line 65 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 67 */
} // :cond_1
} // :goto_0
v0 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.sInstance;
} // .end method
private com.android.server.biometrics.sensors.face.IMiFace getMiFace ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException;, */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 107 */
v0 = this.mMiuiFace;
/* if-nez v0, :cond_0 */
/* .line 108 */
com.android.server.biometrics.sensors.face.IMiFace .getService ( );
this.mMiuiFace = v0;
/* .line 109 */
/* new-instance v0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$MyHandler; */
com.android.server.MiuiFgThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$MyHandler;-><init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 110 */
/* new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor; */
int v3 = 3; // const/4 v3, 0x3
/* const v4, 0x7fffffff */
/* const-wide/16 v5, 0x1f4 */
v7 = java.util.concurrent.TimeUnit.MILLISECONDS;
/* new-instance v8, Ljava/util/concurrent/SynchronousQueue; */
/* invoke-direct {v8}, Ljava/util/concurrent/SynchronousQueue;-><init>()V */
/* move-object v2, v0 */
/* invoke-direct/range {v2 ..v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V */
this.mExecutor = v0;
/* .line 111 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 112 */
v0 = this.mMiuiFace;
v1 = this.mDeathRecipient;
/* const-wide/16 v2, 0x0 */
/* .line 114 */
} // :cond_0
v0 = this.mMiuiFace;
} // .end method
public static intToByte ( Integer p0 ) {
/* .locals 6 */
/* .param p0, "value" # I */
/* .line 286 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v0, v0, [B */
/* .line 287 */
/* .local v0, "b":[B */
/* and-int/lit16 v1, p0, 0xff */
/* int-to-byte v1, v1 */
/* .line 288 */
/* .local v1, "v2":B */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 289 */
/* shr-int/lit8 v2, p0, 0x8 */
/* and-int/lit16 v2, v2, 0xff */
/* int-to-byte v2, v2 */
/* .line 290 */
/* .local v2, "v2_2":B */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v2, v0, v3 */
/* .line 291 */
/* shr-int/lit8 v3, p0, 0x10 */
/* and-int/lit16 v3, v3, 0xff */
/* int-to-byte v3, v3 */
/* .line 292 */
/* .local v3, "v2_3":B */
int v4 = 2; // const/4 v4, 0x2
/* aput-byte v3, v0, v4 */
/* .line 293 */
/* shr-int/lit8 v4, p0, 0x18 */
/* and-int/lit16 v4, v4, 0xff */
/* int-to-byte v4, v4 */
int v5 = 3; // const/4 v5, 0x3
/* aput-byte v4, v0, v5 */
/* .line 294 */
} // .end method
public static intToBytesBig ( Integer p0 ) {
/* .locals 6 */
/* .param p0, "value" # I */
/* .line 297 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v0, v0, [B */
/* .line 298 */
/* .local v0, "src":[B */
/* shr-int/lit8 v1, p0, 0x18 */
/* and-int/lit16 v1, v1, 0xff */
/* int-to-byte v1, v1 */
/* .line 299 */
/* .local v1, "v2":B */
int v2 = 0; // const/4 v2, 0x0
/* aput-byte v1, v0, v2 */
/* .line 300 */
/* shr-int/lit8 v2, p0, 0x10 */
/* and-int/lit16 v2, v2, 0xff */
/* int-to-byte v2, v2 */
/* .line 301 */
/* .local v2, "v2_2":B */
int v3 = 1; // const/4 v3, 0x1
/* aput-byte v2, v0, v3 */
/* .line 302 */
/* shr-int/lit8 v3, p0, 0x8 */
/* and-int/lit16 v3, v3, 0xff */
/* int-to-byte v3, v3 */
/* .line 303 */
/* .local v3, "v2_3":B */
int v4 = 2; // const/4 v4, 0x2
/* aput-byte v3, v0, v4 */
/* .line 304 */
/* and-int/lit16 v4, p0, 0xff */
/* int-to-byte v4, v4 */
int v5 = 3; // const/4 v5, 0x3
/* aput-byte v4, v0, v5 */
/* .line 305 */
} // .end method
public static intToBytesLittle ( Integer p0 ) {
/* .locals 6 */
/* .param p0, "value" # I */
/* .line 308 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v0, v0, [B */
/* .line 309 */
/* .local v0, "src":[B */
/* shr-int/lit8 v1, p0, 0x18 */
/* and-int/lit16 v1, v1, 0xff */
/* int-to-byte v1, v1 */
/* .line 310 */
/* .local v1, "v2":B */
int v2 = 3; // const/4 v2, 0x3
/* aput-byte v1, v0, v2 */
/* .line 311 */
/* shr-int/lit8 v2, p0, 0x10 */
/* and-int/lit16 v2, v2, 0xff */
/* int-to-byte v2, v2 */
/* .line 312 */
/* .local v2, "v2_2":B */
int v3 = 2; // const/4 v3, 0x2
/* aput-byte v2, v0, v3 */
/* .line 313 */
/* shr-int/lit8 v3, p0, 0x8 */
/* and-int/lit16 v3, v3, 0xff */
/* int-to-byte v3, v3 */
/* .line 314 */
/* .local v3, "v2_3":B */
int v4 = 1; // const/4 v4, 0x1
/* aput-byte v3, v0, v4 */
/* .line 315 */
/* and-int/lit16 v4, p0, 0xff */
/* int-to-byte v4, v4 */
int v5 = 0; // const/4 v5, 0x0
/* aput-byte v4, v0, v5 */
/* .line 316 */
} // .end method
private static android.graphics.Bitmap nv12ToBitmap ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p0, "data" # [B */
/* .param p1, "w" # I */
/* .param p2, "h" # I */
/* .line 345 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
com.android.server.biometrics.sensors.face.MiuiFaceHidl .spToBitmap ( p0,p1,p2,v0,v1 );
} // .end method
public static android.graphics.Bitmap nv21ToBitmap ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p0, "data" # [B */
/* .param p1, "w" # I */
/* .param p2, "h" # I */
/* .line 348 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
com.android.server.biometrics.sensors.face.MiuiFaceHidl .spToBitmap ( p0,p1,p2,v0,v1 );
} // .end method
public static android.graphics.Bitmap rotateBitmap ( Integer p0, android.graphics.Bitmap p1 ) {
/* .locals 8 */
/* .param p0, "angle" # I */
/* .param p1, "bitmap" # Landroid/graphics/Bitmap; */
/* .line 273 */
/* new-instance v0, Landroid/graphics/Matrix; */
/* invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V */
/* .line 274 */
/* .local v0, "matrix":Landroid/graphics/Matrix; */
/* int-to-float v1, p0 */
(( android.graphics.Matrix ) v0 ).postRotate ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z
/* .line 275 */
/* const/high16 v1, -0x40800000 # -1.0f */
/* const/high16 v2, 0x3f800000 # 1.0f */
(( android.graphics.Matrix ) v0 ).postScale ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z
/* .line 276 */
/* sget-wide v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->scale:D */
/* double-to-float v3, v1 */
/* double-to-float v1, v1 */
(( android.graphics.Matrix ) v0 ).postScale ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/graphics/Matrix;->postScale(FF)Z
/* .line 277 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* .line 278 */
v4 = (( android.graphics.Bitmap ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I
v5 = (( android.graphics.Bitmap ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I
int v7 = 1; // const/4 v7, 0x1
/* .line 277 */
/* move-object v1, p1 */
/* move-object v6, v0 */
/* invoke-static/range {v1 ..v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap; */
/* .line 279 */
/* .local v1, "resizedBitmap":Landroid/graphics/Bitmap; */
/* if-eq v1, p1, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
v2 = (( android.graphics.Bitmap ) p1 ).isRecycled ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z
/* if-nez v2, :cond_0 */
/* .line 280 */
(( android.graphics.Bitmap ) p1 ).recycle ( ); // invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
/* .line 281 */
int p1 = 0; // const/4 p1, 0x0
/* .line 283 */
} // :cond_0
} // .end method
public static void saveBitmap ( android.graphics.Bitmap p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p0, "bitmap" # Landroid/graphics/Bitmap; */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 236 */
/* if-nez p0, :cond_0 */
return;
/* .line 237 */
} // :cond_0
final String v0 = "/data/system/files/"; // const-string v0, "/data/system/files/"
/* .line 238 */
/* .local v0, "mediaStorageDir":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ".png"; // const-string v2, ".png"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 240 */
/* .local v1, "bitmapFileName":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 241 */
/* .local v2, "file":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_1 */
/* .line 242 */
(( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
/* .line 244 */
} // :cond_1
final String v3 = "MiuiFaceHidl"; // const-string v3, "MiuiFaceHidl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "path: "; // const-string v5, "path: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 245 */
/* new-instance v3, Ljava/io/FileOutputStream; */
/* invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* .line 246 */
/* .local v3, "fos":Ljava/io/FileOutputStream; */
v4 = android.graphics.Bitmap$CompressFormat.PNG;
/* const/16 v5, 0x64 */
(( android.graphics.Bitmap ) p0 ).compress ( v4, v5, v3 ); // invoke-virtual {p0, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
/* .line 247 */
(( java.io.FileOutputStream ) v3 ).flush ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
/* .line 248 */
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 251 */
} // .end local v2 # "file":Ljava/io/File;
} // .end local v3 # "fos":Ljava/io/FileOutputStream;
/* .line 249 */
/* :catch_0 */
/* move-exception v2 */
/* .line 250 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 252 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
public static void saveFile ( Object[] p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p0, "bitmap" # [B */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 254 */
/* if-nez p0, :cond_0 */
return;
/* .line 255 */
} // :cond_0
final String v0 = "/data/system/files/"; // const-string v0, "/data/system/files/"
/* .line 256 */
/* .local v0, "mediaStorageDir":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "_"; // const-string v2, "_"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".yuv"; // const-string v2, ".yuv"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 258 */
/* .local v1, "fileName":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 259 */
/* .local v2, "file":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_1 */
/* .line 260 */
(( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
/* .line 262 */
} // :cond_1
final String v3 = "MiuiFaceHidl"; // const-string v3, "MiuiFaceHidl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "path: "; // const-string v5, "path: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 263 */
/* new-instance v3, Ljava/io/FileOutputStream; */
/* invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* .line 264 */
/* .local v3, "fos":Ljava/io/FileOutputStream; */
(( java.io.FileOutputStream ) v3 ).write ( p0 ); // invoke-virtual {v3, p0}, Ljava/io/FileOutputStream;->write([B)V
/* .line 266 */
(( java.io.FileOutputStream ) v3 ).flush ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
/* .line 267 */
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 270 */
} // .end local v2 # "file":Ljava/io/File;
} // .end local v3 # "fos":Ljava/io/FileOutputStream;
/* .line 268 */
/* :catch_0 */
/* move-exception v2 */
/* .line 269 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 271 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
public static void showImage ( android.graphics.Bitmap p0 ) {
/* .locals 8 */
/* .param p0, "bitmap" # Landroid/graphics/Bitmap; */
/* .line 218 */
v0 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mFrameList;
/* monitor-enter v0 */
/* .line 219 */
try { // :try_start_0
v1 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mGetFrame;
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
/* if-nez v1, :cond_0 */
/* .line 220 */
/* monitor-exit v0 */
return;
/* .line 222 */
} // :cond_0
v1 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mDetectRect;
/* if-nez v1, :cond_1 */
/* .line 223 */
/* new-instance v1, Landroid/graphics/RectF; */
/* const/high16 v2, 0x43c80000 # 400.0f */
/* const/high16 v3, 0x43fa0000 # 500.0f */
/* const/high16 v4, 0x428c0000 # 70.0f */
/* const/high16 v5, 0x431c0000 # 156.0f */
/* invoke-direct {v1, v4, v5, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V */
/* .line 225 */
} // :cond_1
v1 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mEnrollSurface;
/* new-instance v2, Landroid/graphics/Rect; */
/* const/16 v3, 0x1f4 */
/* const/16 v4, 0x244 */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v2, v5, v5, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V */
(( android.view.Surface ) v1 ).lockCanvas ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
/* .line 226 */
/* const/16 v2, 0x1b8 */
(( android.graphics.Canvas ) v1 ).setDensity ( v2 ); // invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setDensity(I)V
/* .line 227 */
v1 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mCanvas;
v2 = android.graphics.PorterDuff$Mode.CLEAR;
(( android.graphics.Canvas ) v1 ).drawColor ( v5, v2 ); // invoke-virtual {v1, v5, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V
/* .line 228 */
v1 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mCanvas;
/* new-instance v2, Landroid/graphics/Rect; */
/* .line 230 */
v3 = (( android.graphics.Bitmap ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I
v6 = (( android.graphics.Bitmap ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I
/* invoke-direct {v2, v5, v5, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V */
/* new-instance v3, Landroid/graphics/Rect; */
/* int-to-float v6, v6 */
/* const v7, 0x3f19999a # 0.6f */
/* mul-float/2addr v6, v7 */
/* float-to-int v6, v6 */
/* invoke-direct {v3, v5, v5, v4, v6}, Landroid/graphics/Rect;-><init>(IIII)V */
/* new-instance v4, Landroid/graphics/Paint; */
/* invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V */
/* .line 228 */
(( android.graphics.Canvas ) v1 ).drawBitmap ( p0, v2, v3, v4 ); // invoke-virtual {v1, p0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
/* .line 232 */
v1 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mEnrollSurface;
v2 = com.android.server.biometrics.sensors.face.MiuiFaceHidl.mCanvas;
(( android.view.Surface ) v1 ).unlockCanvasAndPost ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
/* .line 233 */
/* monitor-exit v0 */
/* .line 234 */
return;
/* .line 233 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static sm2Byte ( android.os.SharedMemory p0, Integer p1 ) {
/* .locals 4 */
/* .param p0, "sm" # Landroid/os/SharedMemory; */
/* .param p1, "len" # I */
/* .line 333 */
int v0 = 0; // const/4 v0, 0x0
/* .line 335 */
/* .local v0, "buffArr":[B */
try { // :try_start_0
(( android.os.SharedMemory ) p0 ).mapReadWrite ( ); // invoke-virtual {p0}, Landroid/os/SharedMemory;->mapReadWrite()Ljava/nio/ByteBuffer;
/* .line 336 */
/* .local v1, "buffer":Ljava/nio/ByteBuffer; */
/* new-array v2, p1, [B */
/* move-object v0, v2 */
/* .line 337 */
int v2 = 0; // const/4 v2, 0x0
(( java.nio.ByteBuffer ) v1 ).get ( v0, v2, p1 ); // invoke-virtual {v1, v0, v2, p1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 341 */
/* nop */
} // .end local v1 # "buffer":Ljava/nio/ByteBuffer;
/* .line 339 */
/* :catch_0 */
/* move-exception v1 */
/* .line 340 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "sm2Byte error ! e = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFaceHidl"; // const-string v3, "MiuiFaceHidl"
android.util.Slog .e ( v3,v2 );
/* .line 342 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
private static android.graphics.Bitmap spToBitmap ( Object[] p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 18 */
/* .param p0, "data" # [B */
/* .param p1, "w" # I */
/* .param p2, "h" # I */
/* .param p3, "uOff" # I */
/* .param p4, "vOff" # I */
/* .line 351 */
/* move/from16 v0, p1 */
/* move/from16 v1, p2 */
/* mul-int v2, v0, v1 */
/* .line 352 */
/* .local v2, "plane":I */
/* new-array v3, v2, [I */
/* .line 353 */
/* .local v3, "colors":[I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "yPos":I */
/* move v5, v2 */
/* .line 354 */
/* .local v5, "uvPos":I */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "j":I */
} // :goto_0
/* if-ge v6, v1, :cond_9 */
/* .line 355 */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_1
/* if-ge v7, v0, :cond_7 */
/* .line 357 */
/* aget-byte v8, p0, v4 */
/* and-int/lit16 v8, v8, 0xff */
/* .line 358 */
/* .local v8, "y1":I */
/* add-int v9, v5, p3 */
/* aget-byte v9, p0, v9 */
/* and-int/lit16 v9, v9, 0xff */
/* add-int/lit8 v9, v9, -0x80 */
/* .line 359 */
/* .local v9, "u":I */
/* add-int v10, v5, p4 */
/* aget-byte v10, p0, v10 */
/* and-int/lit16 v10, v10, 0xff */
/* add-int/lit8 v10, v10, -0x80 */
/* .line 360 */
/* .local v10, "v":I */
/* mul-int/lit16 v11, v8, 0x4a8 */
/* .line 361 */
/* .local v11, "y1192":I */
/* mul-int/lit16 v12, v10, 0x662 */
/* add-int/2addr v12, v11 */
/* .line 362 */
/* .local v12, "r":I */
/* mul-int/lit16 v13, v10, 0x341 */
/* sub-int v13, v11, v13 */
/* mul-int/lit16 v14, v9, 0x190 */
/* sub-int/2addr v13, v14 */
/* .line 363 */
/* .local v13, "g":I */
/* mul-int/lit16 v14, v9, 0x812 */
/* add-int/2addr v14, v11 */
/* .line 364 */
/* .local v14, "b":I */
/* const v15, 0x3ffff */
/* if-gez v12, :cond_0 */
/* const/16 v17, 0x0 */
} // :cond_0
/* if-le v12, v15, :cond_1 */
/* move/from16 v17, v15 */
} // :cond_1
/* move/from16 v17, v12 */
} // :goto_2
/* move/from16 v12, v17 */
/* .line 365 */
/* if-gez v13, :cond_2 */
/* const/16 v17, 0x0 */
} // :cond_2
/* if-le v13, v15, :cond_3 */
/* move/from16 v17, v15 */
} // :cond_3
/* move/from16 v17, v13 */
} // :goto_3
/* move/from16 v13, v17 */
/* .line 366 */
/* if-gez v14, :cond_4 */
int v15 = 0; // const/4 v15, 0x0
} // :cond_4
/* if-le v14, v15, :cond_5 */
} // :cond_5
/* move v15, v14 */
} // :goto_4
/* move v14, v15 */
/* .line 367 */
/* shl-int/lit8 v15, v12, 0x6 */
/* const/high16 v16, 0xff0000 */
/* and-int v15, v15, v16 */
/* shr-int/lit8 v16, v13, 0x2 */
/* const v17, 0xff00 */
/* and-int v16, v16, v17 */
/* or-int v15, v15, v16 */
/* move/from16 v16, v2 */
} // .end local v2 # "plane":I
/* .local v16, "plane":I */
/* shr-int/lit8 v2, v14, 0xa */
/* and-int/lit16 v2, v2, 0xff */
/* or-int/2addr v2, v15 */
/* aput v2, v3, v4 */
/* .line 370 */
/* add-int/lit8 v2, v4, 0x1 */
} // .end local v4 # "yPos":I
/* .local v2, "yPos":I */
/* and-int/lit8 v4, v4, 0x1 */
int v15 = 1; // const/4 v15, 0x1
/* if-ne v4, v15, :cond_6 */
/* .line 371 */
/* add-int/lit8 v5, v5, 0x2 */
/* .line 355 */
} // .end local v8 # "y1":I
} // .end local v9 # "u":I
} // .end local v10 # "v":I
} // .end local v11 # "y1192":I
} // .end local v12 # "r":I
} // .end local v13 # "g":I
} // .end local v14 # "b":I
} // :cond_6
/* add-int/lit8 v7, v7, 0x1 */
/* move v4, v2 */
/* move/from16 v2, v16 */
} // .end local v16 # "plane":I
/* .local v2, "plane":I */
/* .restart local v4 # "yPos":I */
} // :cond_7
/* move/from16 v16, v2 */
/* .line 374 */
} // .end local v2 # "plane":I
} // .end local v7 # "i":I
/* .restart local v16 # "plane":I */
/* and-int/lit8 v2, v6, 0x1 */
/* if-nez v2, :cond_8 */
/* .line 375 */
/* sub-int/2addr v5, v0 */
/* .line 354 */
} // :cond_8
/* add-int/lit8 v6, v6, 0x1 */
/* move/from16 v2, v16 */
/* goto/16 :goto_0 */
} // .end local v16 # "plane":I
/* .restart local v2 # "plane":I */
} // :cond_9
/* move/from16 v16, v2 */
/* .line 378 */
} // .end local v2 # "plane":I
} // .end local v6 # "j":I
/* .restart local v16 # "plane":I */
v2 = android.graphics.Bitmap$Config.RGB_565;
android.graphics.Bitmap .createBitmap ( v3,v0,v1,v2 );
} // .end method
/* # virtual methods */
public Boolean getPreviewBuffer ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 117 */
int v0 = 1; // const/4 v0, 0x1
java.lang.Boolean .valueOf ( v0 );
/* .line 118 */
(( com.android.server.biometrics.sensors.face.MiuiFaceHidl ) p0 ).startPreview ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->startPreview()V
/* .line 119 */
} // .end method
public Boolean setDetectArea ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "left" # I */
/* .param p2, "top" # I */
/* .param p3, "right" # I */
/* .param p4, "bottom" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 122 */
com.android.server.biometrics.sensors.face.IMiFace .getService ( );
/* .line 124 */
/* .local v0, "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace; */
/* sub-int v1, p3, p1 */
/* add-int/lit16 v1, v1, -0x122 */
/* .line 125 */
/* .local v1, "teeLeft":I */
/* if-lez v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* move v2, p1 */
} // :goto_0
/* move v1, v2 */
/* .line 126 */
/* sub-int v2, p3, p1 */
/* add-int/lit16 v2, v2, 0x122 */
/* .line 127 */
v3 = /* .local v2, "teeRight":I */
/* .line 128 */
/* .local v3, "res":Z */
/* new-instance v4, Landroid/graphics/RectF; */
/* int-to-float v5, p1 */
/* int-to-float v6, p2 */
/* int-to-float v7, p3 */
/* int-to-float v8, p4 */
/* invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V */
/* .line 129 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "setDetectArea left: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", top: "; // const-string v5, ", top: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", right: "; // const-string v5, ", right: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p3 ); // invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", bottom: "; // const-string v5, ", bottom: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p4 ); // invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", res: "; // const-string v5, ", res: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiFaceHidl"; // const-string v5, "MiuiFaceHidl"
android.util.Slog .e ( v5,v4 );
/* .line 130 */
} // .end method
public Boolean setEnrollArea ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 7 */
/* .param p1, "left" # I */
/* .param p2, "top" # I */
/* .param p3, "right" # I */
/* .param p4, "bottom" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 133 */
com.android.server.biometrics.sensors.face.IMiFace .getService ( );
/* .line 134 */
v1 = /* .local v0, "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace; */
/* .line 135 */
/* .local v1, "res":Z */
/* new-instance v2, Landroid/graphics/RectF; */
/* int-to-float v3, p1 */
/* int-to-float v4, p2 */
/* int-to-float v5, p3 */
/* int-to-float v6, p4 */
/* invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V */
/* .line 136 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setEnrollArea left: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", top: "; // const-string v3, ", top: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", right: "; // const-string v3, ", right: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", bottom: "; // const-string v3, ", bottom: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", res: "; // const-string v3, ", res: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFaceHidl"; // const-string v3, "MiuiFaceHidl"
android.util.Slog .e ( v3,v2 );
/* .line 137 */
} // .end method
public Boolean setEnrollStep ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "steps" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 140 */
com.android.server.biometrics.sensors.face.IMiFace .getService ( );
/* .line 141 */
v1 = /* .local v0, "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace; */
/* .line 142 */
/* .local v1, "res":Z */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "setEnrollStep steps: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", res: "; // const-string v3, ", res: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiFaceHidl"; // const-string v3, "MiuiFaceHidl"
android.util.Slog .e ( v3,v2 );
/* .line 143 */
} // .end method
public void setEnrollSurface ( android.view.Surface p0 ) {
/* .locals 0 */
/* .param p1, "surface" # Landroid/view/Surface; */
/* .line 154 */
/* .line 155 */
return;
} // .end method
public Boolean setPreviewNotifyCallback ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException;, */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 147 */
/* invoke-direct {p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->getMiFace()Lcom/android/server/biometrics/sensors/face/IMiFace; */
this.mMiuiFace = v0;
/* .line 148 */
int v0 = 1; // const/4 v0, 0x1
java.lang.Boolean .valueOf ( v0 );
/* .line 149 */
v0 = this.mMiuiFace;
v0 = v1 = this.callback;
/* .line 150 */
/* .local v0, "res":Z */
} // .end method
public void startPreview ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 174 */
v0 = this.mExecutor;
/* new-instance v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3; */
/* invoke-direct {v1, p0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3;-><init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)V */
(( java.util.concurrent.ThreadPoolExecutor ) v0 ).execute ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
/* .line 216 */
return;
} // .end method
public void stopGetFrame ( ) {
/* .locals 1 */
/* .line 170 */
int v0 = 0; // const/4 v0, 0x0
java.lang.Boolean .valueOf ( v0 );
/* .line 171 */
int v0 = 0; // const/4 v0, 0x0
/* .line 172 */
return;
} // .end method
