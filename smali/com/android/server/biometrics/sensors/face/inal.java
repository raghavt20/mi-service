public class inal implements com.android.server.biometrics.sensors.face.IMiFaceSession {
	 /* .source "IMiFaceSession.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/face/IMiFaceSession; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x19 */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IHwBinder mRemote;
/* # direct methods */
public inal ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IHwBinder; */
/* .line 230 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 231 */
java.util.Objects .requireNonNull ( p1 );
/* check-cast v0, Landroid/os/IHwBinder; */
this.mRemote = v0;
/* .line 232 */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 1 */
/* .line 236 */
v0 = this.mRemote;
} // .end method
public Integer authenticate ( Long p0 ) {
/* .locals 5 */
/* .param p1, "operationId" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 324 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 325 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 326 */
(( android.os.HwParcel ) v0 ).writeInt64 ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/HwParcel;->writeInt64(J)V
/* .line 328 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 330 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
int v4 = 0; // const/4 v4, 0x0
/* .line 331 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 332 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 334 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 335 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 337 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 335 */
/* .line 337 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 338 */
/* throw v2 */
} // .end method
public Integer cancel ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 501 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 502 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 504 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 506 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xd */
int v4 = 0; // const/4 v4, 0x0
/* .line 507 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 508 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 510 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 511 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 513 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 511 */
/* .line 513 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 514 */
/* throw v2 */
} // .end method
public Integer close ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 482 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 483 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 485 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 487 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xc */
int v4 = 0; // const/4 v4, 0x0
/* .line 488 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 489 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 491 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 492 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 494 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 492 */
/* .line 494 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 495 */
/* throw v2 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 5 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 540 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 541 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 542 */
(( android.os.HwParcel ) v0 ).writeNativeHandle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeNativeHandle(Landroid/os/NativeHandle;)V
/* .line 543 */
(( android.os.HwParcel ) v0 ).writeStringVector ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 545 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 547 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf444247 */
int v4 = 0; // const/4 v4, 0x0
/* .line 548 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 549 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 551 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 552 */
/* nop */
/* .line 553 */
return;
/* .line 551 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 552 */
/* throw v2 */
} // .end method
public Integer enroll ( java.util.ArrayList p0, java.util.ArrayList p1, android.os.NativeHandle p2 ) {
/* .locals 5 */
/* .param p3, "previewSurface" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Landroid/os/NativeHandle;", */
/* ")I" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 302 */
/* .local p1, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* .local p2, "features":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 303 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 304 */
(( android.os.HwParcel ) v0 ).writeInt8Vector ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 305 */
(( android.os.HwParcel ) v0 ).writeInt32Vector ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeInt32Vector(Ljava/util/ArrayList;)V
/* .line 306 */
(( android.os.HwParcel ) v0 ).writeNativeHandle ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/HwParcel;->writeNativeHandle(Landroid/os/NativeHandle;)V
/* .line 308 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 310 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
int v4 = 0; // const/4 v4, 0x0
/* .line 311 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 312 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 314 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 315 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 317 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 315 */
/* .line 317 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 318 */
/* throw v2 */
} // .end method
public Integer enumerateEnrollments ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 344 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 345 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 347 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 349 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
int v4 = 0; // const/4 v4, 0x0
/* .line 350 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 351 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 353 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 354 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 356 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 354 */
/* .line 356 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 357 */
/* throw v2 */
} // .end method
public final Boolean equals ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "other" # Ljava/lang/Object; */
/* .line 251 */
v0 = android.os.HidlSupport .interfacesEqual ( p0,p1 );
} // .end method
public Long generateChallenge ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 263 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 264 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 266 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 268 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* .line 269 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 270 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 272 */
(( android.os.HwParcel ) v1 ).readInt64 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 273 */
/* .local v2, "_hidl_out_status":J */
/* nop */
/* .line 275 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 273 */
/* return-wide v2 */
/* .line 275 */
} // .end local v2 # "_hidl_out_status":J
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 276 */
/* throw v2 */
} // .end method
public Long getAuthenticatorId ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 424 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 425 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 427 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 429 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x9 */
int v4 = 0; // const/4 v4, 0x0
/* .line 430 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 431 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 433 */
(( android.os.HwParcel ) v1 ).readInt64 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 434 */
/* .local v2, "_hidl_out_result":J */
/* nop */
/* .line 436 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 434 */
/* return-wide v2 */
/* .line 436 */
} // .end local v2 # "_hidl_out_result":J
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 437 */
/* throw v2 */
} // .end method
public miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 652 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 653 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 655 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 657 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf524546 */
int v4 = 0; // const/4 v4, 0x0
/* .line 658 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 659 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 661 */
/* new-instance v2, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v2}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 662 */
/* .local v2, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v2 ).readFromParcel ( v1 ); // invoke-virtual {v2, v1}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->readFromParcel(Landroid/os/HwParcel;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 663 */
/* nop */
/* .line 665 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 663 */
/* .line 665 */
} // .end local v2 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 666 */
/* throw v2 */
} // .end method
public Integer getFeatures ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 383 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 384 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 386 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 388 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 7; // const/4 v3, 0x7
int v4 = 0; // const/4 v4, 0x0
/* .line 389 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 390 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 392 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 393 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 395 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 393 */
/* .line 395 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 396 */
/* throw v2 */
} // .end method
public java.util.ArrayList getHashChain ( ) {
/* .locals 13 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 577 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 578 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 580 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 582 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf485348 */
int v4 = 0; // const/4 v4, 0x0
/* .line 583 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 584 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 586 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v10, v2 */
/* .line 588 */
/* .local v10, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
/* const-wide/16 v2, 0x10 */
(( android.os.HwParcel ) v1 ).readBuffer ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/HwParcel;->readBuffer(J)Landroid/os/HwBlob;
/* move-object v11, v2 */
/* .line 590 */
/* .local v11, "_hidl_blob":Landroid/os/HwBlob; */
/* const-wide/16 v2, 0x8 */
v2 = (( android.os.HwBlob ) v11 ).getInt32 ( v2, v3 ); // invoke-virtual {v11, v2, v3}, Landroid/os/HwBlob;->getInt32(J)I
/* move v12, v2 */
/* .line 591 */
/* .local v12, "_hidl_vec_size":I */
/* mul-int/lit8 v2, v12, 0x20 */
/* int-to-long v3, v2 */
/* .line 592 */
(( android.os.HwBlob ) v11 ).handle ( ); // invoke-virtual {v11}, Landroid/os/HwBlob;->handle()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x0 */
int v9 = 1; // const/4 v9, 0x1
/* .line 591 */
/* move-object v2, v1 */
/* invoke-virtual/range {v2 ..v9}, Landroid/os/HwParcel;->readEmbeddedBuffer(JJJZ)Landroid/os/HwBlob; */
/* .line 595 */
/* .local v2, "childBlob":Landroid/os/HwBlob; */
(( java.util.ArrayList ) v10 ).clear ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V
/* .line 596 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "_hidl_index_0":I */
} // :goto_0
/* if-ge v3, v12, :cond_0 */
/* .line 597 */
/* const/16 v4, 0x20 */
/* new-array v5, v4, [B */
/* .line 599 */
/* .local v5, "_hidl_vec_element":[B */
/* mul-int/lit8 v6, v3, 0x20 */
/* int-to-long v6, v6 */
/* .line 600 */
/* .local v6, "_hidl_array_offset_1":J */
(( android.os.HwBlob ) v2 ).copyToInt8Array ( v6, v7, v5, v4 ); // invoke-virtual {v2, v6, v7, v5, v4}, Landroid/os/HwBlob;->copyToInt8Array(J[BI)V
/* .line 601 */
/* nop */
/* .line 603 */
} // .end local v6 # "_hidl_array_offset_1":J
(( java.util.ArrayList ) v10 ).add ( v5 ); // invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 596 */
/* nop */
} // .end local v5 # "_hidl_vec_element":[B
/* add-int/lit8 v3, v3, 0x1 */
/* .line 607 */
} // .end local v2 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_index_0":I
} // .end local v11 # "_hidl_blob":Landroid/os/HwBlob;
} // .end local v12 # "_hidl_vec_size":I
} // :cond_0
/* nop */
/* .line 609 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 607 */
/* .line 609 */
} // .end local v10 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 610 */
/* throw v2 */
} // .end method
public final Integer hashCode ( ) {
/* .locals 1 */
/* .line 256 */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Proxy;->asBinder()Landroid/os/IHwBinder;
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
} // .end method
public java.util.ArrayList interfaceChain ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 521 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 522 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 524 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 526 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf43484e */
int v4 = 0; // const/4 v4, 0x0
/* .line 527 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 528 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 530 */
(( android.os.HwParcel ) v1 ).readStringVector ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 531 */
/* .local v2, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* nop */
/* .line 533 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 531 */
/* .line 533 */
} // .end local v2 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 534 */
/* throw v2 */
} // .end method
public java.lang.String interfaceDescriptor ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 558 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 559 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 561 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 563 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf445343 */
int v4 = 0; // const/4 v4, 0x0
/* .line 564 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 565 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 567 */
(( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 568 */
/* .local v2, "_hidl_out_descriptor":Ljava/lang/String; */
/* nop */
/* .line 570 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 568 */
/* .line 570 */
} // .end local v2 # "_hidl_out_descriptor":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 571 */
/* throw v2 */
} // .end method
public Integer invalidateAuthenticatorId ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 443 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 444 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 446 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 448 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xa */
int v4 = 0; // const/4 v4, 0x0
/* .line 449 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 450 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 452 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 453 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 455 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 453 */
/* .line 455 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 456 */
/* throw v2 */
} // .end method
public Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 631 */
v0 = v0 = this.mRemote;
} // .end method
public void notifySyspropsChanged ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 672 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 673 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 675 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 677 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf535953 */
int v4 = 1; // const/4 v4, 0x1
/* .line 678 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 680 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 681 */
/* nop */
/* .line 682 */
return;
/* .line 680 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 681 */
/* throw v2 */
} // .end method
public void ping ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 636 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 637 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 639 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 641 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf504e47 */
int v4 = 0; // const/4 v4, 0x0
/* .line 642 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 643 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 645 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 646 */
/* nop */
/* .line 647 */
return;
/* .line 645 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 646 */
/* throw v2 */
} // .end method
public Integer removeEnrollments ( java.util.ArrayList p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;)I" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 363 */
/* .local p1, "enrollmentIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 364 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 365 */
(( android.os.HwParcel ) v0 ).writeInt32Vector ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt32Vector(Ljava/util/ArrayList;)V
/* .line 367 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 369 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 6; // const/4 v3, 0x6
int v4 = 0; // const/4 v4, 0x0
/* .line 370 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 371 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 373 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 374 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 376 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 374 */
/* .line 376 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 377 */
/* throw v2 */
} // .end method
public Integer resetLockout ( java.util.ArrayList p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)I" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 462 */
/* .local p1, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 463 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 464 */
(( android.os.HwParcel ) v0 ).writeInt8Vector ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 466 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 468 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0xb */
int v4 = 0; // const/4 v4, 0x0
/* .line 469 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 470 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 472 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 473 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 475 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 473 */
/* .line 475 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 476 */
/* throw v2 */
} // .end method
public Integer revokeChallenge ( Long p0 ) {
/* .locals 5 */
/* .param p1, "challenge" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 282 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 283 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 284 */
(( android.os.HwParcel ) v0 ).writeInt64 ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/HwParcel;->writeInt64(J)V
/* .line 286 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 288 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
/* .line 289 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 290 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 292 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 293 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 295 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 293 */
/* .line 295 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 296 */
/* throw v2 */
} // .end method
public Integer setFeature ( java.util.ArrayList p0, Integer p1, Boolean p2 ) {
/* .locals 5 */
/* .param p2, "feature" # I */
/* .param p3, "enabled" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;IZ)I" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 402 */
/* .local p1, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 403 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSession" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 404 */
(( android.os.HwParcel ) v0 ).writeInt8Vector ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 405 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 406 */
(( android.os.HwParcel ) v0 ).writeBool ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 408 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 410 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const/16 v3, 0x8 */
int v4 = 0; // const/4 v4, 0x0
/* .line 411 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 412 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 414 */
v2 = (( android.os.HwParcel ) v1 ).readInt32 ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 415 */
/* .local v2, "_hidl_out_status":I */
/* nop */
/* .line 417 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 415 */
/* .line 417 */
} // .end local v2 # "_hidl_out_status":I
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 418 */
/* throw v2 */
} // .end method
public void setHALInstrumentation ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 616 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 617 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 619 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 621 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf494e54 */
int v4 = 1; // const/4 v4, 0x1
/* .line 622 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 624 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 625 */
/* nop */
/* .line 626 */
return;
/* .line 624 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 625 */
/* throw v2 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 242 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.biometrics.sensors.face.IMiFaceSession$Proxy ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSession$Proxy;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Proxy"; // const-string v1, "@Proxy"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 243 */
/* :catch_0 */
/* move-exception v0 */
/* .line 246 */
final String v0 = "[class or subclass of vendor.xiaomi.hardware.miface@1.0::IMiFaceSession]@Proxy"; // const-string v0, "[class or subclass of vendor.xiaomi.hardware.miface@1.0::IMiFaceSession]@Proxy"
} // .end method
public Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 687 */
v0 = v0 = this.mRemote;
} // .end method
