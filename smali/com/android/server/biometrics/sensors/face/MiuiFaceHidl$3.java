class com.android.server.biometrics.sensors.face.MiuiFaceHidl$3 implements java.lang.Runnable {
	 /* .source "MiuiFaceHidl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->startPreview()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.biometrics.sensors.face.MiuiFaceHidl this$0; //synthetic
/* # direct methods */
 com.android.server.biometrics.sensors.face.MiuiFaceHidl$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl; */
/* .line 174 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 18 */
/* .line 177 */
/* move-object/from16 v1, p0 */
final String v2 = "MiuiFaceHidl"; // const-string v2, "MiuiFaceHidl"
int v3 = 0; // const/4 v3, 0x0
/* .line 179 */
/* .local v3, "i":I */
try { // :try_start_0
	 com.android.server.biometrics.sensors.face.MiuiFaceHidl .-$$Nest$sfgetmGetFrame ( );
	 v0 = 	 (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 180 */
		 /* const v0, 0x75800 */
		 /* new-array v4, v0, [B */
		 /* .line 181 */
		 /* .local v4, "value":[B */
		 int v5 = 1; // const/4 v5, 0x1
		 com.android.server.biometrics.sensors.face.MiuiFaceHidl .intToBytesLittle ( v5 );
		 /* .line 182 */
		 /* .local v6, "saved":[B */
		 /* array-length v7, v6 */
		 int v8 = 0; // const/4 v8, 0x0
		 int v9 = 4; // const/4 v9, 0x4
		 java.lang.System .arraycopy ( v6,v8,v4,v9,v7 );
		 /* .line 183 */
		 /* array-length v7, v6 */
		 /* .line 184 */
		 /* .local v7, "length":I */
		 com.android.server.biometrics.sensors.face.MiuiFaceHidl .intToBytesLittle ( v5 );
		 /* .line 185 */
		 /* .local v5, "signed":[B */
		 /* array-length v9, v5 */
		 /* const/16 v10, 0x8 */
		 java.lang.System .arraycopy ( v5,v8,v4,v10,v9 );
		 /* .line 186 */
		 /* array-length v9, v5 */
		 /* add-int/2addr v9, v7 */
		 com.android.server.biometrics.sensors.face.MiuiFaceHidl .intToBytesLittle ( v9 );
		 /* .line 187 */
		 /* .local v9, "pos":[B */
		 /* array-length v10, v9 */
		 java.lang.System .arraycopy ( v9,v8,v4,v8,v10 );
		 /* .line 188 */
		 com.android.server.biometrics.sensors.face.MiuiFaceHidl .byteArr2Sm ( v4 );
		 /* .line 189 */
		 /* .local v10, "sm":Landroid/os/SharedMemory; */
		 (( android.os.SharedMemory ) v10 ).getFileDescriptor ( ); // invoke-virtual {v10}, Landroid/os/SharedMemory;->getFileDescriptor()Ljava/io/FileDescriptor;
		 /* .line 190 */
		 /* .local v11, "fd":Ljava/io/FileDescriptor; */
		 /* new-instance v12, Landroid/os/NativeHandle; */
		 /* invoke-direct {v12, v11, v8}, Landroid/os/NativeHandle;-><init>(Ljava/io/FileDescriptor;Z)V */
		 /* .line 191 */
		 /* .local v12, "handle":Landroid/os/NativeHandle; */
		 v13 = this.this$0;
		 com.android.server.biometrics.sensors.face.MiuiFaceHidl .-$$Nest$mgetMiFace ( v13 );
		 /* .line 192 */
		 v14 = 		 /* .local v13, "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace; */
		 /* .line 193 */
		 /* .local v14, "res":Z */
		 /* const-string/jumbo v15, "startPreview\uff01" */
		 android.util.Slog .e ( v2,v15 );
		 /* .line 194 */
		 /* if-nez v14, :cond_0 */
		 /* .line 195 */
		 return;
		 /* .line 197 */
	 } // :cond_0
	 com.android.server.biometrics.sensors.face.MiuiFaceHidl .sm2Byte ( v10,v0 );
	 /* .line 199 */
	 /* .local v0, "img":[B */
	 /* const/16 v15, 0x280 */
	 /* const/16 v8, 0x1e0 */
	 com.android.server.biometrics.sensors.face.MiuiFaceHidl .-$$Nest$smnv12ToBitmap ( v0,v15,v8 );
	 /* const/16 v15, -0x5a */
	 com.android.server.biometrics.sensors.face.MiuiFaceHidl .rotateBitmap ( v15,v8 );
	 /* .line 201 */
	 /* .local v8, "rotateBitMap":Landroid/graphics/Bitmap; */
	 com.android.server.biometrics.sensors.face.MiuiFaceHidl .showImage ( v8 );
	 /* .line 202 */
	 android.os.Message .obtain ( );
	 /* .line 203 */
	 /* .local v15, "msg":Landroid/os/Message; */
	 /* move-object/from16 v16, v0 */
} // .end local v0 # "img":[B
/* .local v16, "img":[B */
/* const/16 v0, 0x2711 */
/* iput v0, v15, Landroid/os/Message;->what:I */
/* .line 204 */
this.obj = v8;
/* .line 205 */
v0 = this.this$0;
com.android.server.biometrics.sensors.face.MiuiFaceHidl .-$$Nest$fgetmHandler ( v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* move/from16 v17, v3 */
/* const/16 v3, 0x2711 */
} // .end local v3 # "i":I
/* .local v17, "i":I */
try { // :try_start_1
v0 = (( android.os.Handler ) v0 ).hasMessages ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v0, :cond_1 */
/* .line 206 */
v0 = this.this$0;
com.android.server.biometrics.sensors.face.MiuiFaceHidl .-$$Nest$fgetmHandler ( v0 );
(( android.os.Handler ) v0 ).sendMessage ( v15 ); // invoke-virtual {v0, v15}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 208 */
} // :cond_1
(( android.os.SharedMemory ) v10 ).close ( ); // invoke-virtual {v10}, Landroid/os/SharedMemory;->close()V
/* .line 209 */
int v0 = 0; // const/4 v0, 0x0
java.lang.Boolean .valueOf ( v0 );
com.android.server.biometrics.sensors.face.MiuiFaceHidl .-$$Nest$sfputmGetFrame ( v0 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 211 */
} // .end local v4 # "value":[B
} // .end local v5 # "signed":[B
} // .end local v6 # "saved":[B
} // .end local v7 # "length":I
} // .end local v8 # "rotateBitMap":Landroid/graphics/Bitmap;
} // .end local v9 # "pos":[B
} // .end local v10 # "sm":Landroid/os/SharedMemory;
} // .end local v11 # "fd":Ljava/io/FileDescriptor;
} // .end local v12 # "handle":Landroid/os/NativeHandle;
} // .end local v13 # "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace;
} // .end local v14 # "res":Z
} // .end local v15 # "msg":Landroid/os/Message;
} // .end local v16 # "img":[B
/* :catch_0 */
/* move-exception v0 */
/* .line 179 */
} // .end local v17 # "i":I
/* .restart local v3 # "i":I */
} // :cond_2
/* move/from16 v17, v3 */
/* .line 213 */
} // .end local v3 # "i":I
/* .restart local v17 # "i":I */
} // :goto_0
/* .line 211 */
} // .end local v17 # "i":I
/* .restart local v3 # "i":I */
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v17, v3 */
/* .line 212 */
} // .end local v3 # "i":I
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v17 # "i":I */
} // :goto_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "get image frame from sharedmemory failed! e = "; // const-string v4, "get image frame from sharedmemory failed! e = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 214 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
