public abstract class com.android.server.biometrics.sensors.face.IMiFace$Stub extends android.os.HwBinder implements com.android.server.biometrics.sensors.face.IMiFace {
	 /* .source "IMiFace.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/face/IMiFace; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public com.android.server.biometrics.sensors.face.IMiFace$Stub ( ) {
/* .locals 0 */
/* .line 811 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 814 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 827 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 864 */
/* new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 865 */
/* .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 866 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 867 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 868 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 839 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_1 */
/* filled-new-array {v2, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 819 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFace" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 833 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFace" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .line 852 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 874 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 876 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "_hidl_request" # Landroid/os/HwParcel; */
/* .param p3, "_hidl_reply" # Landroid/os/HwParcel; */
/* .param p4, "_hidl_flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 904 */
final String v0 = "android.hidl.base@1.0::IBase"; // const-string v0, "android.hidl.base@1.0::IBase"
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFace" */
int v2 = 0; // const/4 v2, 0x0
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_2 */
/* .line 1222 */
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1224 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->notifySyspropsChanged()V
/* .line 1225 */
/* goto/16 :goto_2 */
/* .line 1211 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1213 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* .line 1214 */
/* .local v0, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1215 */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( p3 ); // invoke-virtual {v0, p3}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 1216 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1217 */
/* goto/16 :goto_2 */
/* .line 1201 */
} // .end local v0 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1203 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->ping()V
/* .line 1204 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1205 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1206 */
/* goto/16 :goto_2 */
/* .line 1196 */
/* :sswitch_3 */
/* goto/16 :goto_2 */
/* .line 1188 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1190 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->setHALInstrumentation()V
/* .line 1191 */
/* goto/16 :goto_2 */
/* .line 1154 */
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1156 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 1157 */
/* .local v0, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1159 */
/* new-instance v1, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v1, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1161 */
/* .local v1, "_hidl_blob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 1162 */
/* .local v3, "_hidl_vec_size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v1 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v1, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 1163 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v1 ).putBool ( v4, v5, v2 ); // invoke-virtual {v1, v4, v5, v2}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 1164 */
/* new-instance v2, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v2, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1165 */
/* .local v2, "childBlob":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "_hidl_index_0":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 1167 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 1168 */
/* .local v5, "_hidl_array_offset_1":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 1170 */
/* .local v7, "_hidl_array_item_1":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 1174 */
(( android.os.HwBlob ) v2 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v2, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 1175 */
/* nop */
/* .line 1165 */
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1171 */
/* .restart local v5 # "_hidl_array_offset_1":J */
/* .restart local v7 # "_hidl_array_item_1":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 1178 */
} // .end local v4 # "_hidl_index_0":I
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v1 ).putBlob ( v4, v5, v2 ); // invoke-virtual {v1, v4, v5, v2}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 1180 */
} // .end local v2 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_vec_size":I
(( android.os.HwParcel ) p3 ).writeBuffer ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 1182 */
} // .end local v1 # "_hidl_blob":Landroid/os/HwBlob;
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1183 */
/* goto/16 :goto_2 */
/* .line 1143 */
} // .end local v0 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1145 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 1146 */
/* .local v0, "_hidl_out_descriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1147 */
(( android.os.HwParcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 1148 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1149 */
/* goto/16 :goto_2 */
/* .line 1131 */
} // .end local v0 # "_hidl_out_descriptor":Ljava/lang/String;
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1133 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 1134 */
/* .local v0, "fd":Landroid/os/NativeHandle; */
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* .line 1135 */
/* .local v1, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).debug ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 1136 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1137 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1138 */
/* goto/16 :goto_2 */
/* .line 1120 */
} // .end local v0 # "fd":Landroid/os/NativeHandle;
} // .end local v1 # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1122 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 1123 */
/* .local v0, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1124 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 1125 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1126 */
/* goto/16 :goto_2 */
/* .line 1106 */
} // .end local v0 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1108 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1109 */
/* .local v0, "sensorId":I */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1110 */
/* .local v1, "userId":I */
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.biometrics.sensors.face.IMiFaceSessionCallback .asInterface ( v3 );
/* .line 1111 */
/* .local v3, "callback":Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback; */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).createSession ( v0, v1, v3 ); // invoke-virtual {p0, v0, v1, v3}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->createSession(IILcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback;)Lcom/android/server/biometrics/sensors/face/IMiFaceSession;
/* .line 1112 */
/* .local v4, "_hidl_out_session":Lcom/android/server/biometrics/sensors/face/IMiFaceSession; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1113 */
/* if-nez v4, :cond_2 */
int v2 = 0; // const/4 v2, 0x0
} // :cond_2
} // :goto_1
(( android.os.HwParcel ) p3 ).writeStrongBinder ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 1114 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1115 */
/* goto/16 :goto_2 */
/* .line 1095 */
} // .end local v0 # "sensorId":I
} // .end local v1 # "userId":I
} // .end local v3 # "callback":Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback;
} // .end local v4 # "_hidl_out_session":Lcom/android/server/biometrics/sensors/face/IMiFaceSession;
/* :sswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1096 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1097 */
/* .local v0, "counter":I */
v1 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).setEnrollStep ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->setEnrollStep(I)Z
/* .line 1098 */
/* .local v1, "_hidl_out_result":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1099 */
(( android.os.HwParcel ) p3 ).writeBool ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1100 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1101 */
/* goto/16 :goto_2 */
/* .line 1082 */
} // .end local v0 # "counter":I
} // .end local v1 # "_hidl_out_result":Z
/* :sswitch_b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1083 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1084 */
/* .local v0, "left":I */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1085 */
/* .local v1, "top":I */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1086 */
/* .local v3, "right":I */
v4 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1087 */
/* .local v4, "bottom":I */
v5 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).setEnrollArea ( v0, v1, v3, v4 ); // invoke-virtual {p0, v0, v1, v3, v4}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->setEnrollArea(IIII)Z
/* .line 1088 */
/* .local v5, "_hidl_out_result":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1089 */
(( android.os.HwParcel ) p3 ).writeBool ( v5 ); // invoke-virtual {p3, v5}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1090 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1091 */
/* goto/16 :goto_2 */
/* .line 1069 */
} // .end local v0 # "left":I
} // .end local v1 # "top":I
} // .end local v3 # "right":I
} // .end local v4 # "bottom":I
} // .end local v5 # "_hidl_out_result":Z
/* :sswitch_c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1070 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1071 */
/* .restart local v0 # "left":I */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1072 */
/* .restart local v1 # "top":I */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1073 */
/* .restart local v3 # "right":I */
v4 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1074 */
/* .restart local v4 # "bottom":I */
v5 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).setDetectArea ( v0, v1, v3, v4 ); // invoke-virtual {p0, v0, v1, v3, v4}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->setDetectArea(IIII)Z
/* .line 1075 */
/* .restart local v5 # "_hidl_out_result":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1076 */
(( android.os.HwParcel ) p3 ).writeBool ( v5 ); // invoke-virtual {p3, v5}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1077 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1078 */
/* goto/16 :goto_2 */
/* .line 1059 */
} // .end local v0 # "left":I
} // .end local v1 # "top":I
} // .end local v3 # "right":I
} // .end local v4 # "bottom":I
} // .end local v5 # "_hidl_out_result":Z
/* :sswitch_d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1060 */
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.biometrics.sensors.face.IMiFaceProprietaryClientCallback .asInterface ( v0 );
/* .line 1061 */
/* .local v0, "callback":Lcom/android/server/biometrics/sensors/face/IMiFaceProprietaryClientCallback; */
v1 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).setPreviewNotifyCallback ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->setPreviewNotifyCallback(Lcom/android/server/biometrics/sensors/face/IMiFaceProprietaryClientCallback;)Z
/* .line 1062 */
/* .local v1, "_hidl_out_result":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1063 */
(( android.os.HwParcel ) p3 ).writeBool ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1064 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1065 */
/* goto/16 :goto_2 */
/* .line 1049 */
} // .end local v0 # "callback":Lcom/android/server/biometrics/sensors/face/IMiFaceProprietaryClientCallback;
} // .end local v1 # "_hidl_out_result":Z
/* :sswitch_e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1050 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 1051 */
/* .local v0, "image_sm":Landroid/os/NativeHandle; */
v1 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).getPreviewBuffer ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->getPreviewBuffer(Landroid/os/NativeHandle;)Z
/* .line 1052 */
/* .restart local v1 # "_hidl_out_result":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1053 */
(( android.os.HwParcel ) p3 ).writeBool ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1054 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1055 */
/* goto/16 :goto_2 */
/* .line 1039 */
} // .end local v0 # "image_sm":Landroid/os/NativeHandle;
} // .end local v1 # "_hidl_out_result":Z
/* :sswitch_f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1040 */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 1041 */
/* .local v0, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v1 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).resetLockout ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->resetLockout(Ljava/util/ArrayList;)I
/* .line 1042 */
/* .local v1, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1043 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1044 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1045 */
/* goto/16 :goto_2 */
/* .line 1030 */
} // .end local v0 # "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v1 # "_hidl_out_status":I
/* :sswitch_10 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1031 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).userActivity ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->userActivity()I
/* .line 1032 */
/* .local v0, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1033 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1034 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1035 */
/* goto/16 :goto_2 */
/* .line 1020 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_11 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1021 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 1022 */
/* .local v0, "operationId":J */
v3 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).authenticate ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->authenticate(J)I
/* .line 1023 */
/* .local v3, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1024 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1025 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1026 */
/* goto/16 :goto_2 */
/* .line 1010 */
} // .end local v0 # "operationId":J
} // .end local v3 # "_hidl_out_status":I
/* :sswitch_12 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1011 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 1012 */
/* .local v0, "faceId":I */
v1 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).remove ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->remove(I)I
/* .line 1013 */
/* .restart local v1 # "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1014 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1015 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1016 */
/* goto/16 :goto_2 */
/* .line 1001 */
} // .end local v0 # "faceId":I
} // .end local v1 # "_hidl_out_status":I
/* :sswitch_13 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1002 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).enumerate ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->enumerate()I
/* .line 1003 */
/* .local v0, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1004 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 1005 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1006 */
/* goto/16 :goto_2 */
/* .line 992 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_14 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 993 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).cancel ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->cancel()I
/* .line 994 */
/* .restart local v0 # "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 995 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 996 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 997 */
/* goto/16 :goto_2 */
/* .line 983 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_15 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 984 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).getAuthenticatorId ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->getAuthenticatorId()J
/* move-result-wide v0 */
/* .line 985 */
/* .local v0, "_hidl_out_result":J */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 986 */
(( android.os.HwParcel ) p3 ).writeInt64 ( v0, v1 ); // invoke-virtual {p3, v0, v1}, Landroid/os/HwParcel;->writeInt64(J)V
/* .line 987 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 988 */
/* goto/16 :goto_2 */
/* .line 972 */
} // .end local v0 # "_hidl_out_result":J
/* :sswitch_16 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 973 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 974 */
/* .local v0, "feature":I */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 975 */
/* .local v1, "faceId":I */
v3 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).getFeature ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->getFeature(II)I
/* .line 976 */
/* .local v3, "_hidl_out_result":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 977 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 978 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 979 */
/* goto/16 :goto_2 */
/* .line 959 */
} // .end local v0 # "feature":I
} // .end local v1 # "faceId":I
} // .end local v3 # "_hidl_out_result":I
/* :sswitch_17 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 960 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 961 */
/* .restart local v0 # "feature":I */
v1 = (( android.os.HwParcel ) p2 ).readBool ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readBool()Z
/* .line 962 */
/* .local v1, "enabled":Z */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 963 */
/* .local v3, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v4 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 964 */
/* .local v4, "faceId":I */
v5 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).setFeature ( v0, v1, v3, v4 ); // invoke-virtual {p0, v0, v1, v3, v4}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->setFeature(IZLjava/util/ArrayList;I)I
/* .line 965 */
/* .local v5, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 966 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v5 ); // invoke-virtual {p3, v5}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 967 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 968 */
/* goto/16 :goto_2 */
/* .line 950 */
} // .end local v0 # "feature":I
} // .end local v1 # "enabled":Z
} // .end local v3 # "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v4 # "faceId":I
} // .end local v5 # "_hidl_out_status":I
/* :sswitch_18 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 951 */
v0 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).revokeChallenge ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->revokeChallenge()I
/* .line 952 */
/* .local v0, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 953 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 954 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 955 */
/* .line 938 */
} // .end local v0 # "_hidl_out_status":I
/* :sswitch_19 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 939 */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 940 */
/* .local v0, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 941 */
/* .local v1, "timeoutSec":I */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 942 */
/* .local v3, "disabledFeatures":I */
v4 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).enroll ( v0, v1, v3 ); // invoke-virtual {p0, v0, v1, v3}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->enroll(Ljava/util/ArrayList;II)I
/* .line 943 */
/* .local v4, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 944 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v4 ); // invoke-virtual {p3, v4}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 945 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 946 */
/* .line 928 */
} // .end local v0 # "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v1 # "timeoutSec":I
} // .end local v3 # "disabledFeatures":I
} // .end local v4 # "_hidl_out_status":I
/* :sswitch_1a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 929 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 930 */
/* .local v0, "challengeTimeoutSec":I */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).generateChallenge ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->generateChallenge(I)J
/* move-result-wide v3 */
/* .line 931 */
/* .local v3, "_hidl_out_result":J */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 932 */
(( android.os.HwParcel ) p3 ).writeInt64 ( v3, v4 ); // invoke-virtual {p3, v3, v4}, Landroid/os/HwParcel;->writeInt64(J)V
/* .line 933 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 934 */
/* .line 917 */
} // .end local v0 # "challengeTimeoutSec":I
} // .end local v3 # "_hidl_out_result":J
/* :sswitch_1b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 918 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 919 */
/* .local v0, "userId":I */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 920 */
/* .local v1, "storePath":Ljava/lang/String; */
v3 = (( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).setActiveUser ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->setActiveUser(ILjava/lang/String;)I
/* .line 921 */
/* .local v3, "_hidl_out_status":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 922 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 923 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 924 */
/* .line 907 */
} // .end local v0 # "userId":I
} // .end local v1 # "storePath":Ljava/lang/String;
} // .end local v3 # "_hidl_out_status":I
/* :sswitch_1c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 908 */
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
com.android.server.biometrics.sensors.face.IMiFaceClientCallback .asInterface ( v0 );
/* .line 909 */
/* .local v0, "clientCallback":Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback; */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).setCallback ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->setCallback(Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback;)J
/* move-result-wide v3 */
/* .line 910 */
/* .local v3, "_hidl_out_result":J */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 911 */
(( android.os.HwParcel ) p3 ).writeInt64 ( v3, v4 ); // invoke-virtual {p3, v3, v4}, Landroid/os/HwParcel;->writeInt64(J)V
/* .line 912 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 913 */
/* nop */
/* .line 1234 */
} // .end local v0 # "clientCallback":Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback;
} // .end local v3 # "_hidl_out_result":J
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1c */
/* 0x2 -> :sswitch_1b */
/* 0x3 -> :sswitch_1a */
/* 0x4 -> :sswitch_19 */
/* 0x5 -> :sswitch_18 */
/* 0x6 -> :sswitch_17 */
/* 0x7 -> :sswitch_16 */
/* 0x8 -> :sswitch_15 */
/* 0x9 -> :sswitch_14 */
/* 0xa -> :sswitch_13 */
/* 0xb -> :sswitch_12 */
/* 0xc -> :sswitch_11 */
/* 0xd -> :sswitch_10 */
/* 0xe -> :sswitch_f */
/* 0xf -> :sswitch_e */
/* 0x10 -> :sswitch_d */
/* 0x11 -> :sswitch_c */
/* 0x12 -> :sswitch_b */
/* 0x13 -> :sswitch_a */
/* 0x14 -> :sswitch_9 */
/* 0xf43484e -> :sswitch_8 */
/* 0xf444247 -> :sswitch_7 */
/* 0xf445343 -> :sswitch_6 */
/* 0xf485348 -> :sswitch_5 */
/* 0xf494e54 -> :sswitch_4 */
/* 0xf4c5444 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 858 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "descriptor" # Ljava/lang/String; */
/* .line 886 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFace" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 887 */
/* .line 889 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 893 */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->registerService(Ljava/lang/String;)V
/* .line 894 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 848 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 898 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.biometrics.sensors.face.IMiFace$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFace$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 880 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
