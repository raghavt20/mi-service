class com.android.server.biometrics.sensors.face.MiuiFaceHidl$MyHandler extends android.os.Handler {
	 /* .source "MiuiFaceHidl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MyHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.biometrics.sensors.face.MiuiFaceHidl this$0; //synthetic
/* # direct methods */
 com.android.server.biometrics.sensors.face.MiuiFaceHidl$MyHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 87 */
this.this$0 = p1;
/* .line 88 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 89 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 92 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 94 */
try { // :try_start_0
	 /* iget v0, p1, Landroid/os/Message;->what:I */
	 /* packed-switch v0, :pswitch_data_0 */
	 /* .line 96 */
	 /* :pswitch_0 */
	 v0 = this.obj;
	 /* check-cast v0, Landroid/graphics/Bitmap; */
	 com.android.server.biometrics.sensors.face.MiuiFaceHidl .showImage ( v0 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 97 */
	 /* nop */
	 /* .line 103 */
} // :goto_0
/* .line 101 */
/* :catch_0 */
/* move-exception v0 */
/* .line 102 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "get Image Frame failed !"; // const-string v2, "get Image Frame failed !"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiFaceHidl"; // const-string v2, "MiuiFaceHidl"
android.util.Slog .e ( v2,v1 );
/* .line 104 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x2711 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
