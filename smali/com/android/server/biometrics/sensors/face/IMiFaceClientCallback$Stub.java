public abstract class com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub extends android.os.HwBinder implements com.android.server.biometrics.sensors.face.IMiFaceClientCallback {
	 /* .source "IMiFaceClientCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ( ) {
/* .locals 0 */
/* .line 540 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 543 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 556 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 593 */
/* new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 594 */
/* .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 595 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 596 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 597 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 568 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_1 */
/* filled-new-array {v2, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* -0x3et */
/* 0x3at */
/* 0x5et */
/* 0x70t */
/* 0x42t */
/* -0x3ft */
/* -0x48t */
/* -0x72t */
/* -0x65t */
/* -0x3dt */
/* 0x17t */
/* -0x5ct */
/* -0x2t */
/* -0x15t */
/* 0xat */
/* 0x73t */
/* -0x1et */
/* 0x7dt */
/* -0x51t */
/* -0x61t */
/* 0x5t */
/* -0x3bt */
/* 0x41t */
/* 0x40t */
/* -0x41t */
/* 0x64t */
/* 0x28t */
/* 0x52t */
/* 0x32t */
/* 0x19t */
/* 0x3bt */
/* -0x19t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 548 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceClientCallback" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 562 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceClientCallback" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .line 581 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 603 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 605 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 11 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "_hidl_request" # Landroid/os/HwParcel; */
/* .param p3, "_hidl_reply" # Landroid/os/HwParcel; */
/* .param p4, "_hidl_flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 633 */
int v0 = 0; // const/4 v0, 0x0
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceClientCallback" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 817 */
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 819 */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->notifySyspropsChanged()V
/* .line 820 */
/* goto/16 :goto_1 */
/* .line 806 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 808 */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* .line 809 */
/* .local v1, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 810 */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v1 ).writeToParcel ( p3 ); // invoke-virtual {v1, p3}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 811 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 812 */
/* goto/16 :goto_1 */
/* .line 796 */
} // .end local v1 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 798 */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->ping()V
/* .line 799 */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 800 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 801 */
/* goto/16 :goto_1 */
/* .line 791 */
/* :sswitch_3 */
/* goto/16 :goto_1 */
/* .line 783 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 785 */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->setHALInstrumentation()V
/* .line 786 */
/* goto/16 :goto_1 */
/* .line 749 */
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 751 */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 752 */
/* .local v1, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 754 */
/* new-instance v2, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 756 */
/* .local v2, "_hidl_blob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* .line 757 */
/* .local v3, "_hidl_vec_size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v2 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 758 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v2 ).putBool ( v4, v5, v0 ); // invoke-virtual {v2, v4, v5, v0}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 759 */
/* new-instance v0, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v0, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 760 */
/* .local v0, "childBlob":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "_hidl_index_0":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 762 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 763 */
/* .local v5, "_hidl_array_offset_1":J */
(( java.util.ArrayList ) v1 ).get ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 765 */
/* .local v7, "_hidl_array_item_1":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 769 */
(( android.os.HwBlob ) v0 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v0, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 770 */
/* nop */
/* .line 760 */
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 766 */
/* .restart local v5 # "_hidl_array_offset_1":J */
/* .restart local v7 # "_hidl_array_item_1":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 773 */
} // .end local v4 # "_hidl_index_0":I
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v2 ).putBlob ( v4, v5, v0 ); // invoke-virtual {v2, v4, v5, v0}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 775 */
} // .end local v0 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_vec_size":I
(( android.os.HwParcel ) p3 ).writeBuffer ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 777 */
} // .end local v2 # "_hidl_blob":Landroid/os/HwBlob;
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 778 */
/* goto/16 :goto_1 */
/* .line 738 */
} // .end local v1 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 740 */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 741 */
/* .local v1, "_hidl_out_descriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 742 */
(( android.os.HwParcel ) p3 ).writeString ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 743 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 744 */
/* goto/16 :goto_1 */
/* .line 726 */
} // .end local v1 # "_hidl_out_descriptor":Ljava/lang/String;
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 728 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 729 */
/* .local v1, "fd":Landroid/os/NativeHandle; */
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* .line 730 */
/* .local v2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).debug ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 731 */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 732 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 733 */
/* goto/16 :goto_1 */
/* .line 715 */
} // .end local v1 # "fd":Landroid/os/NativeHandle;
} // .end local v2 # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 717 */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 718 */
/* .local v1, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 719 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 720 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 721 */
/* goto/16 :goto_1 */
/* .line 706 */
} // .end local v1 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 708 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 709 */
/* .local v0, "duration":J */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).onLockoutChanged ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->onLockoutChanged(J)V
/* .line 710 */
/* goto/16 :goto_1 */
/* .line 695 */
} // .end local v0 # "duration":J
/* :sswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 697 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 698 */
/* .local v0, "deviceId":J */
(( android.os.HwParcel ) p2 ).readInt32Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;
/* .line 699 */
/* .local v2, "faceIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 700 */
/* .local v3, "userId":I */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).onEnumerate ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->onEnumerate(JLjava/util/ArrayList;I)V
/* .line 701 */
/* goto/16 :goto_1 */
/* .line 684 */
} // .end local v0 # "deviceId":J
} // .end local v2 # "faceIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v3 # "userId":I
/* :sswitch_b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 686 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 687 */
/* .restart local v0 # "deviceId":J */
(( android.os.HwParcel ) p2 ).readInt32Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;
/* .line 688 */
/* .local v2, "removed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 689 */
/* .restart local v3 # "userId":I */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).onRemoved ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->onRemoved(JLjava/util/ArrayList;I)V
/* .line 690 */
/* goto/16 :goto_1 */
/* .line 672 */
} // .end local v0 # "deviceId":J
} // .end local v2 # "removed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v3 # "userId":I
/* :sswitch_c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 674 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 675 */
/* .restart local v0 # "deviceId":J */
v2 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 676 */
/* .local v2, "userId":I */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 677 */
/* .local v3, "error":I */
v10 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 678 */
/* .local v10, "vendorCode":I */
/* move-object v4, p0 */
/* move-wide v5, v0 */
/* move v7, v2 */
/* move v8, v3 */
/* move v9, v10 */
/* invoke-virtual/range {v4 ..v9}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->onError(JIII)V */
/* .line 679 */
/* .line 660 */
} // .end local v0 # "deviceId":J
} // .end local v2 # "userId":I
} // .end local v3 # "error":I
} // .end local v10 # "vendorCode":I
/* :sswitch_d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 662 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 663 */
/* .restart local v0 # "deviceId":J */
v2 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 664 */
/* .restart local v2 # "userId":I */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 665 */
/* .local v3, "acquiredInfo":I */
v10 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 666 */
/* .restart local v10 # "vendorCode":I */
/* move-object v4, p0 */
/* move-wide v5, v0 */
/* move v7, v2 */
/* move v8, v3 */
/* move v9, v10 */
/* invoke-virtual/range {v4 ..v9}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->onAcquired(JIII)V */
/* .line 667 */
/* .line 648 */
} // .end local v0 # "deviceId":J
} // .end local v2 # "userId":I
} // .end local v3 # "acquiredInfo":I
} // .end local v10 # "vendorCode":I
/* :sswitch_e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 650 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 651 */
/* .restart local v0 # "deviceId":J */
v2 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 652 */
/* .local v2, "faceId":I */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 653 */
/* .local v3, "userId":I */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 654 */
/* .local v10, "token":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* move-object v4, p0 */
/* move-wide v5, v0 */
/* move v7, v2 */
/* move v8, v3 */
/* move-object v9, v10 */
/* invoke-virtual/range {v4 ..v9}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->onAuthenticated(JIILjava/util/ArrayList;)V */
/* .line 655 */
/* .line 636 */
} // .end local v0 # "deviceId":J
} // .end local v2 # "faceId":I
} // .end local v3 # "userId":I
} // .end local v10 # "token":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
/* :sswitch_f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 638 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 639 */
/* .restart local v0 # "deviceId":J */
v2 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 640 */
/* .restart local v2 # "faceId":I */
v3 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 641 */
/* .restart local v3 # "userId":I */
v10 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 642 */
/* .local v10, "remaining":I */
/* move-object v4, p0 */
/* move-wide v5, v0 */
/* move v7, v2 */
/* move v8, v3 */
/* move v9, v10 */
/* invoke-virtual/range {v4 ..v9}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->onEnrollResult(JIII)V */
/* .line 643 */
/* nop */
/* .line 829 */
} // .end local v0 # "deviceId":J
} // .end local v2 # "faceId":I
} // .end local v3 # "userId":I
} // .end local v10 # "remaining":I
} // :goto_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_f */
/* 0x2 -> :sswitch_e */
/* 0x3 -> :sswitch_d */
/* 0x4 -> :sswitch_c */
/* 0x5 -> :sswitch_b */
/* 0x6 -> :sswitch_a */
/* 0x7 -> :sswitch_9 */
/* 0xf43484e -> :sswitch_8 */
/* 0xf444247 -> :sswitch_7 */
/* 0xf445343 -> :sswitch_6 */
/* 0xf485348 -> :sswitch_5 */
/* 0xf494e54 -> :sswitch_4 */
/* 0xf4c5444 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 587 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "descriptor" # Ljava/lang/String; */
/* .line 615 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceClientCallback" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 616 */
/* .line 618 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 622 */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->registerService(Ljava/lang/String;)V
/* .line 623 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 577 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 627 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.biometrics.sensors.face.IMiFaceClientCallback$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceClientCallback$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 609 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
