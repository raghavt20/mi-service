public abstract class com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub extends android.os.HwBinder implements com.android.server.biometrics.sensors.face.IMiFaceSessionCallback {
	 /* .source "IMiFaceSessionCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ( ) {
/* .locals 0 */
/* .line 732 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 735 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 748 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 785 */
/* new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 786 */
/* .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 787 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 788 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 789 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 760 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_1 */
/* filled-new-array {v2, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
/* 0x0t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 740 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSessionCallback" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 754 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSessionCallback" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .line 773 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 795 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 797 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "_hidl_request" # Landroid/os/HwParcel; */
/* .param p3, "_hidl_reply" # Landroid/os/HwParcel; */
/* .param p4, "_hidl_flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 825 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
/* const-string/jumbo v2, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSessionCallback" */
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 1092 */
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1094 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->notifySyspropsChanged()V
/* .line 1095 */
/* goto/16 :goto_1 */
/* .line 1081 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1083 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* .line 1084 */
/* .local v1, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1085 */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v1 ).writeToParcel ( p3 ); // invoke-virtual {v1, p3}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 1086 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1087 */
/* goto/16 :goto_1 */
/* .line 1071 */
} // .end local v1 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1073 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->ping()V
/* .line 1074 */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1075 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1076 */
/* goto/16 :goto_1 */
/* .line 1066 */
/* :sswitch_3 */
/* goto/16 :goto_1 */
/* .line 1058 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1060 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->setHALInstrumentation()V
/* .line 1061 */
/* goto/16 :goto_1 */
/* .line 1024 */
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1026 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 1027 */
/* .local v1, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1029 */
/* new-instance v2, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1031 */
/* .local v2, "_hidl_blob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* .line 1032 */
/* .local v3, "_hidl_vec_size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v2 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 1033 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v2 ).putBool ( v4, v5, v0 ); // invoke-virtual {v2, v4, v5, v0}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 1034 */
/* new-instance v0, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v0, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1035 */
/* .local v0, "childBlob":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "_hidl_index_0":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 1037 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 1038 */
/* .local v5, "_hidl_array_offset_1":J */
(( java.util.ArrayList ) v1 ).get ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 1040 */
/* .local v7, "_hidl_array_item_1":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 1044 */
(( android.os.HwBlob ) v0 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v0, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 1045 */
/* nop */
/* .line 1035 */
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1041 */
/* .restart local v5 # "_hidl_array_offset_1":J */
/* .restart local v7 # "_hidl_array_item_1":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 1048 */
} // .end local v4 # "_hidl_index_0":I
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v2 ).putBlob ( v4, v5, v0 ); // invoke-virtual {v2, v4, v5, v0}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 1050 */
} // .end local v0 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_vec_size":I
(( android.os.HwParcel ) p3 ).writeBuffer ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 1052 */
} // .end local v2 # "_hidl_blob":Landroid/os/HwBlob;
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1053 */
/* goto/16 :goto_1 */
/* .line 1013 */
} // .end local v1 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1015 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 1016 */
/* .local v1, "_hidl_out_descriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1017 */
(( android.os.HwParcel ) p3 ).writeString ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 1018 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1019 */
/* goto/16 :goto_1 */
/* .line 1001 */
} // .end local v1 # "_hidl_out_descriptor":Ljava/lang/String;
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1003 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 1004 */
/* .local v1, "fd":Landroid/os/NativeHandle; */
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* .line 1005 */
/* .local v2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).debug ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 1006 */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1007 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1008 */
/* goto/16 :goto_1 */
/* .line 990 */
} // .end local v1 # "fd":Landroid/os/NativeHandle;
} // .end local v2 # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 992 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 993 */
/* .local v1, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 994 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 995 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 996 */
/* goto/16 :goto_1 */
/* .line 982 */
} // .end local v1 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 984 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onSessionClosed ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onSessionClosed()V
/* .line 985 */
/* goto/16 :goto_1 */
/* .line 973 */
/* :sswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 975 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 976 */
/* .local v0, "newAuthenticatorId":J */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onAuthenticatorIdInvalidated ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onAuthenticatorIdInvalidated(J)V
/* .line 977 */
/* goto/16 :goto_1 */
/* .line 964 */
} // .end local v0 # "newAuthenticatorId":J
/* :sswitch_b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 966 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 967 */
/* .local v0, "authenticatorId":J */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onAuthenticatorIdRetrieved ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onAuthenticatorIdRetrieved(J)V
/* .line 968 */
/* goto/16 :goto_1 */
/* .line 955 */
} // .end local v0 # "authenticatorId":J
/* :sswitch_c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 957 */
(( android.os.HwParcel ) p2 ).readInt32Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;
/* .line 958 */
/* .local v0, "enrollmentIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onEnrollmentsRemoved ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onEnrollmentsRemoved(Ljava/util/ArrayList;)V
/* .line 959 */
/* goto/16 :goto_1 */
/* .line 946 */
} // .end local v0 # "enrollmentIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* :sswitch_d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 948 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 949 */
/* .local v0, "feature":I */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onFeatureSet ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onFeatureSet(I)V
/* .line 950 */
/* goto/16 :goto_1 */
/* .line 937 */
} // .end local v0 # "feature":I
/* :sswitch_e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 939 */
(( android.os.HwParcel ) p2 ).readInt32Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;
/* .line 940 */
/* .local v0, "features":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onFeaturesRetrieved ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onFeaturesRetrieved(Ljava/util/ArrayList;)V
/* .line 941 */
/* goto/16 :goto_1 */
/* .line 928 */
} // .end local v0 # "features":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* :sswitch_f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 930 */
(( android.os.HwParcel ) p2 ).readInt32Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32Vector()Ljava/util/ArrayList;
/* .line 931 */
/* .local v0, "enrollmentIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onEnrollmentsEnumerated ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onEnrollmentsEnumerated(Ljava/util/ArrayList;)V
/* .line 932 */
/* goto/16 :goto_1 */
/* .line 920 */
} // .end local v0 # "enrollmentIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* :sswitch_10 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 922 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onInteractionDetected ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onInteractionDetected()V
/* .line 923 */
/* goto/16 :goto_1 */
/* .line 912 */
/* :sswitch_11 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 914 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onLockoutCleared ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onLockoutCleared()V
/* .line 915 */
/* goto/16 :goto_1 */
/* .line 904 */
/* :sswitch_12 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 906 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onLockoutPermanent ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onLockoutPermanent()V
/* .line 907 */
/* .line 895 */
/* :sswitch_13 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 897 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 898 */
/* .local v0, "durationMillis":J */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onLockoutTimed ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onLockoutTimed(J)V
/* .line 899 */
/* .line 887 */
} // .end local v0 # "durationMillis":J
/* :sswitch_14 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 889 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onAuthenticationFailed ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onAuthenticationFailed()V
/* .line 890 */
/* .line 877 */
/* :sswitch_15 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 879 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 880 */
/* .local v0, "enrollmentId":I */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 881 */
/* .local v1, "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onAuthenticationSucceeded ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onAuthenticationSucceeded(ILjava/util/ArrayList;)V
/* .line 882 */
/* .line 867 */
} // .end local v0 # "enrollmentId":I
} // .end local v1 # "hat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
/* :sswitch_16 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 869 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 870 */
/* .restart local v0 # "enrollmentId":I */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 871 */
/* .local v1, "remaining":I */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onEnrollmentProgress ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onEnrollmentProgress(II)V
/* .line 872 */
/* .line 857 */
} // .end local v0 # "enrollmentId":I
} // .end local v1 # "remaining":I
/* :sswitch_17 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 859 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 860 */
/* .local v0, "error":I */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 861 */
/* .local v1, "vendorCode":I */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onError ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onError(II)V
/* .line 862 */
/* .line 846 */
} // .end local v0 # "error":I
} // .end local v1 # "vendorCode":I
/* :sswitch_18 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 848 */
v0 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 849 */
/* .local v0, "acquiredInfo":I */
v1 = (( android.os.HwParcel ) p2 ).readInt32 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt32()I
/* .line 850 */
/* .restart local v1 # "vendorCode":I */
v2 = (( android.os.HwParcel ) p2 ).readBool ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readBool()Z
/* .line 851 */
/* .local v2, "enroll":Z */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onAcquired ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onAcquired(IIZ)V
/* .line 852 */
/* .line 837 */
} // .end local v0 # "acquiredInfo":I
} // .end local v1 # "vendorCode":I
} // .end local v2 # "enroll":Z
/* :sswitch_19 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 839 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 840 */
/* .local v0, "challenge":J */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onChallengeRevoked ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onChallengeRevoked(J)V
/* .line 841 */
/* .line 828 */
} // .end local v0 # "challenge":J
/* :sswitch_1a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 830 */
(( android.os.HwParcel ) p2 ).readInt64 ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt64()J
/* move-result-wide v0 */
/* .line 831 */
/* .restart local v0 # "challenge":J */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).onChallengeGenerated ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->onChallengeGenerated(J)V
/* .line 832 */
/* nop */
/* .line 1104 */
} // .end local v0 # "challenge":J
} // :goto_1
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_1a */
/* 0x2 -> :sswitch_19 */
/* 0x3 -> :sswitch_18 */
/* 0x4 -> :sswitch_17 */
/* 0x5 -> :sswitch_16 */
/* 0x6 -> :sswitch_15 */
/* 0x7 -> :sswitch_14 */
/* 0x8 -> :sswitch_13 */
/* 0x9 -> :sswitch_12 */
/* 0xa -> :sswitch_11 */
/* 0xb -> :sswitch_10 */
/* 0xc -> :sswitch_f */
/* 0xd -> :sswitch_e */
/* 0xe -> :sswitch_d */
/* 0xf -> :sswitch_c */
/* 0x10 -> :sswitch_b */
/* 0x11 -> :sswitch_a */
/* 0x12 -> :sswitch_9 */
/* 0xf43484e -> :sswitch_8 */
/* 0xf444247 -> :sswitch_7 */
/* 0xf445343 -> :sswitch_6 */
/* 0xf485348 -> :sswitch_5 */
/* 0xf494e54 -> :sswitch_4 */
/* 0xf4c5444 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 779 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "descriptor" # Ljava/lang/String; */
/* .line 807 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSessionCallback" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 808 */
/* .line 810 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 814 */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->registerService(Ljava/lang/String;)V
/* .line 815 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 769 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 819 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.biometrics.sensors.face.IMiFaceSessionCallback$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSessionCallback$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 801 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
