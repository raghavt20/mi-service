public abstract class com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback implements miui.android.services.internal.hidl.base.V1_0.IBase {
	 /* .source "IMiFaceSecCamCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/biometrics/sensors/face/IMiFaceSecCamCallback$Proxy;, */
	 /* Lcom/android/server/biometrics/sensors/face/IMiFaceSecCamCallback$Stub; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String kInterfaceName;
/* # direct methods */
public static com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback asInterface ( android.os.IHwBinder p0 ) {
	 /* .locals 7 */
	 /* .param p0, "binder" # Landroid/os/IHwBinder; */
	 /* .line 13 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* if-nez p0, :cond_0 */
	 /* .line 14 */
	 /* .line 17 */
} // :cond_0
/* nop */
/* .line 18 */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSecCamCallback" */
/* .line 20 */
/* .local v2, "iface":Landroid/os/IHwInterface; */
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* instance-of v3, v2, Lcom/android/server/biometrics/sensors/face/IMiFaceSecCamCallback; */
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 21 */
		 /* move-object v0, v2 */
		 /* check-cast v0, Lcom/android/server/biometrics/sensors/face/IMiFaceSecCamCallback; */
		 /* .line 24 */
	 } // :cond_1
	 /* new-instance v3, Lcom/android/server/biometrics/sensors/face/IMiFaceSecCamCallback$Proxy; */
	 /* invoke-direct {v3, p0}, Lcom/android/server/biometrics/sensors/face/IMiFaceSecCamCallback$Proxy;-><init>(Landroid/os/IHwBinder;)V */
	 /* .line 27 */
	 /* .local v3, "proxy":Lcom/android/server/biometrics/sensors/face/IMiFaceSecCamCallback; */
	 try { // :try_start_0
		 (( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
	 v5 = 	 } // :goto_0
	 if ( v5 != null) { // if-eqz v5, :cond_3
		 /* check-cast v5, Ljava/lang/String; */
		 /* .line 28 */
		 /* .local v5, "descriptor":Ljava/lang/String; */
		 v6 = 		 (( java.lang.String ) v5 ).equals ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 if ( v6 != null) { // if-eqz v6, :cond_2
			 /* .line 29 */
			 /* .line 31 */
		 } // .end local v5 # "descriptor":Ljava/lang/String;
	 } // :cond_2
	 /* .line 33 */
} // :cond_3
/* .line 32 */
/* :catch_0 */
/* move-exception v1 */
/* .line 35 */
} // :goto_1
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback castFrom ( android.os.IHwInterface p0 ) {
/* .locals 1 */
/* .param p0, "iface" # Landroid/os/IHwInterface; */
/* .line 42 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback .asInterface ( v0 );
} // :goto_0
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback getService ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 84 */
final String v0 = "default"; // const-string v0, "default"
com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback .getService ( v0 );
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback getService ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 74 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSecCamCallback" */
android.os.HwBinder .getService ( v0,p0 );
com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback .asInterface ( v0 );
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback getService ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p0, "serviceName" # Ljava/lang/String; */
/* .param p1, "retry" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 57 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.miface@1.0::IMiFaceSecCamCallback" */
android.os.HwBinder .getService ( v0,p0,p1 );
com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback .asInterface ( v0 );
} // .end method
public static com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback getService ( Boolean p0 ) {
/* .locals 1 */
/* .param p0, "retry" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 64 */
final String v0 = "default"; // const-string v0, "default"
com.android.server.biometrics.sensors.face.IMiFaceSecCamCallback .getService ( v0,p0 );
} // .end method
/* # virtual methods */
public abstract android.os.IHwBinder asBinder ( ) {
} // .end method
public abstract void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList getHashChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList interfaceChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.lang.String interfaceDescriptor ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void notifySyspropsChanged ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onFrameEvent ( Integer p0, Integer p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void ping ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setHALInstrumentation ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
