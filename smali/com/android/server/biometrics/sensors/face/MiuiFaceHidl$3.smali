.class Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3;
.super Ljava/lang/Object;
.source "MiuiFaceHidl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->startPreview()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;


# direct methods
.method constructor <init>(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    .line 174
    iput-object p1, p0, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .line 177
    move-object/from16 v1, p0

    const-string v2, "MiuiFaceHidl"

    const/4 v3, 0x0

    .line 179
    .local v3, "i":I
    :try_start_0
    invoke-static {}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$sfgetmGetFrame()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 180
    const v0, 0x75800

    new-array v4, v0, [B

    .line 181
    .local v4, "value":[B
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->intToBytesLittle(I)[B

    move-result-object v6

    .line 182
    .local v6, "saved":[B
    array-length v7, v6

    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-static {v6, v8, v4, v9, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 183
    array-length v7, v6

    .line 184
    .local v7, "length":I
    invoke-static {v5}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->intToBytesLittle(I)[B

    move-result-object v5

    .line 185
    .local v5, "signed":[B
    array-length v9, v5

    const/16 v10, 0x8

    invoke-static {v5, v8, v4, v10, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    array-length v9, v5

    add-int/2addr v9, v7

    invoke-static {v9}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->intToBytesLittle(I)[B

    move-result-object v9

    .line 187
    .local v9, "pos":[B
    array-length v10, v9

    invoke-static {v9, v8, v4, v8, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 188
    invoke-static {v4}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->byteArr2Sm([B)Landroid/os/SharedMemory;

    move-result-object v10

    .line 189
    .local v10, "sm":Landroid/os/SharedMemory;
    invoke-virtual {v10}, Landroid/os/SharedMemory;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v11

    .line 190
    .local v11, "fd":Ljava/io/FileDescriptor;
    new-instance v12, Landroid/os/NativeHandle;

    invoke-direct {v12, v11, v8}, Landroid/os/NativeHandle;-><init>(Ljava/io/FileDescriptor;Z)V

    .line 191
    .local v12, "handle":Landroid/os/NativeHandle;
    iget-object v13, v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-static {v13}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$mgetMiFace(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Lcom/android/server/biometrics/sensors/face/IMiFace;

    move-result-object v13

    .line 192
    .local v13, "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace;
    invoke-interface {v13, v12}, Lcom/android/server/biometrics/sensors/face/IMiFace;->getPreviewBuffer(Landroid/os/NativeHandle;)Z

    move-result v14

    .line 193
    .local v14, "res":Z
    const-string/jumbo v15, "startPreview\uff01"

    invoke-static {v2, v15}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    if-nez v14, :cond_0

    .line 195
    return-void

    .line 197
    :cond_0
    invoke-static {v10, v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->sm2Byte(Landroid/os/SharedMemory;I)[B

    move-result-object v0

    .line 199
    .local v0, "img":[B
    const/16 v15, 0x280

    const/16 v8, 0x1e0

    invoke-static {v0, v15, v8}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$smnv12ToBitmap([BII)Landroid/graphics/Bitmap;

    move-result-object v8

    const/16 v15, -0x5a

    invoke-static {v15, v8}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->rotateBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 201
    .local v8, "rotateBitMap":Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->showImage(Landroid/graphics/Bitmap;)V

    .line 202
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v15

    .line 203
    .local v15, "msg":Landroid/os/Message;
    move-object/from16 v16, v0

    .end local v0    # "img":[B
    .local v16, "img":[B
    const/16 v0, 0x2711

    iput v0, v15, Landroid/os/Message;->what:I

    .line 204
    iput-object v8, v15, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 205
    iget-object v0, v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$fgetmHandler(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Landroid/os/Handler;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v17, v3

    const/16 v3, 0x2711

    .end local v3    # "i":I
    .local v17, "i":I
    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    iget-object v0, v1, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl$3;->this$0:Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$fgetmHandler(Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v15}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 208
    :cond_1
    invoke-virtual {v10}, Landroid/os/SharedMemory;->close()V

    .line 209
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/biometrics/sensors/face/MiuiFaceHidl;->-$$Nest$sfputmGetFrame(Ljava/lang/Boolean;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 211
    .end local v4    # "value":[B
    .end local v5    # "signed":[B
    .end local v6    # "saved":[B
    .end local v7    # "length":I
    .end local v8    # "rotateBitMap":Landroid/graphics/Bitmap;
    .end local v9    # "pos":[B
    .end local v10    # "sm":Landroid/os/SharedMemory;
    .end local v11    # "fd":Ljava/io/FileDescriptor;
    .end local v12    # "handle":Landroid/os/NativeHandle;
    .end local v13    # "miFace":Lcom/android/server/biometrics/sensors/face/IMiFace;
    .end local v14    # "res":Z
    .end local v15    # "msg":Landroid/os/Message;
    .end local v16    # "img":[B
    :catch_0
    move-exception v0

    goto :goto_1

    .line 179
    .end local v17    # "i":I
    .restart local v3    # "i":I
    :cond_2
    move/from16 v17, v3

    .line 213
    .end local v3    # "i":I
    .restart local v17    # "i":I
    :goto_0
    goto :goto_2

    .line 211
    .end local v17    # "i":I
    .restart local v3    # "i":I
    :catch_1
    move-exception v0

    move/from16 v17, v3

    .line 212
    .end local v3    # "i":I
    .local v0, "e":Ljava/lang/Exception;
    .restart local v17    # "i":I
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get image frame from sharedmemory failed! e = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method
