class com.android.server.ExtendMImpl$MyHandler extends android.os.Handler {
	 /* .source "ExtendMImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ExtendMImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MyHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.ExtendMImpl this$0; //synthetic
/* # direct methods */
public com.android.server.ExtendMImpl$MyHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 1337 */
this.this$0 = p1;
/* .line 1338 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1339 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 1343 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 1357 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$mtryToMarkPages ( v0 );
/* .line 1358 */
/* .line 1354 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$mstartExtM ( v0 );
/* .line 1355 */
/* .line 1360 */
/* :pswitch_2 */
v0 = this.this$0;
v0 = com.android.server.ExtendMImpl .-$$Nest$fgetisFlushFinished ( v0 );
/* if-nez v0, :cond_0 */
/* .line 1361 */
final String v0 = "ExtM"; // const-string v0, "ExtM"
final String v1 = "flush time out , stop it"; // const-string v1, "flush time out , stop it"
android.util.Slog .d ( v0,v1 );
/* .line 1362 */
v0 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$mstopFlush ( v0 );
/* .line 1351 */
/* :pswitch_3 */
v0 = this.this$0;
v1 = com.android.server.ExtendMImpl .-$$Nest$sfgetFLUSH_HIGH_LEVEL ( );
com.android.server.ExtendMImpl .-$$Nest$mrunFlush ( v0,v1 );
/* .line 1352 */
/* .line 1348 */
/* :pswitch_4 */
v0 = this.this$0;
v1 = com.android.server.ExtendMImpl .-$$Nest$sfgetFLUSH_MEDIUM_LEVEL ( );
com.android.server.ExtendMImpl .-$$Nest$mrunFlush ( v0,v1 );
/* .line 1349 */
/* .line 1345 */
/* :pswitch_5 */
v0 = this.this$0;
v1 = com.android.server.ExtendMImpl .-$$Nest$sfgetFLUSH_LOW_LEVEL ( );
com.android.server.ExtendMImpl .-$$Nest$mrunFlush ( v0,v1 );
/* .line 1346 */
/* nop */
/* .line 1368 */
} // :cond_0
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
