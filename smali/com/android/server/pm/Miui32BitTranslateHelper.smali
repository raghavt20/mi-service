.class public Lcom/android/server/pm/Miui32BitTranslateHelper;
.super Lcom/android/server/pm/Miui32BitTranslateHelperStub;
.source "Miui32BitTranslateHelper.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static volatile s32BitAsyncPreTransHandler:Landroid/os/Handler;

.field private static s32BitAsyncPreTransLock:Ljava/lang/Object;

.field private static s32BitAsyncPreTransThread:Landroid/os/HandlerThread;


# direct methods
.method public static synthetic $r8$lambda$Aifxrp0iA8VLLsX3tQMokZU7w-s(Lcom/android/server/pm/Miui32BitTranslateHelper;Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/pm/Miui32BitTranslateHelper;->lambda$do32BitPretranslateOnInstall$0(Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 23
    const-class v0, Lcom/android/server/pm/Miui32BitTranslateHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/Miui32BitTranslateHelper;->TAG:Ljava/lang/String;

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/pm/Miui32BitTranslateHelper;->s32BitAsyncPreTransLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/android/server/pm/Miui32BitTranslateHelperStub;-><init>()V

    return-void
.end method

.method private do32BitPretranslateOnArtService(Ljava/lang/String;IZ)V
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isApk"    # Z

    .line 108
    :try_start_0
    invoke-static {}, Lcom/android/server/pm/DexOptHelper;->getArtManagerLocal()Lcom/android/server/art/ArtManagerLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 109
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v1, "do32BitPretranslate"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 113
    .local v1, "declaredMethod":Ljava/lang/reflect/Method;
    invoke-virtual {v1, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 114
    invoke-static {}, Lcom/android/server/pm/DexOptHelper;->getArtManagerLocal()Lcom/android/server/art/ArtManagerLocal;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v6

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v7

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    nop

    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v1    # "declaredMethod":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/android/server/pm/Miui32BitTranslateHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "do32BitPretranslate failed. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static get32BitTransHandler()Landroid/os/Handler;
    .locals 3

    .line 30
    sget-object v0, Lcom/android/server/pm/Miui32BitTranslateHelper;->s32BitAsyncPreTransHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 31
    sget-object v0, Lcom/android/server/pm/Miui32BitTranslateHelper;->s32BitAsyncPreTransLock:Ljava/lang/Object;

    monitor-enter v0

    .line 32
    :try_start_0
    sget-object v1, Lcom/android/server/pm/Miui32BitTranslateHelper;->s32BitAsyncPreTransHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 33
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "32bit_pretranslate_thread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/android/server/pm/Miui32BitTranslateHelper;->s32BitAsyncPreTransThread:Landroid/os/HandlerThread;

    .line 34
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 35
    new-instance v1, Landroid/os/Handler;

    sget-object v2, Lcom/android/server/pm/Miui32BitTranslateHelper;->s32BitAsyncPreTransThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/android/server/pm/Miui32BitTranslateHelper;->s32BitAsyncPreTransHandler:Landroid/os/Handler;

    .line 37
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 39
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/pm/Miui32BitTranslateHelper;->s32BitAsyncPreTransHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private synthetic lambda$do32BitPretranslateOnInstall$0(Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V
    .locals 0
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "pkgSetting"    # Lcom/android/server/pm/pkg/PackageStateInternal;

    .line 47
    invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/Miui32BitTranslateHelper;->do32BitPretranslate(Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V

    return-void
.end method


# virtual methods
.method public do32BitPretranslate(Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V
    .locals 12
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "pkgSetting"    # Lcom/android/server/pm/pkg/PackageStateInternal;

    .line 56
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getSharedAppGid(I)I

    move-result v0

    .line 59
    .local v0, "sharedGid":I
    const/4 v1, 0x0

    .line 60
    .local v1, "libDir":Ljava/lang/String;
    invoke-interface {p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPrimaryCpuAbi()Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "primaryCpuAbi":Ljava/lang/String;
    invoke-interface {p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getSecondaryCpuAbi()Ljava/lang/String;

    move-result-object v3

    .line 62
    .local v3, "secondaryCpuAbi":Ljava/lang/String;
    const/4 v4, 0x0

    .line 63
    .local v4, "is32BitApp":Z
    const-string v5, "arm"

    if-eqz v2, :cond_0

    .line 64
    invoke-static {v2}, Ldalvik/system/VMRuntime;->getInstructionSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 65
    invoke-static {v2}, Ldalvik/system/VMRuntime;->getInstructionSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 66
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getNativeLibraryDir()Ljava/lang/String;

    move-result-object v1

    .line 67
    const/4 v4, 0x1

    goto :goto_0

    .line 68
    :cond_0
    if-eqz v3, :cond_1

    .line 69
    invoke-static {v3}, Ldalvik/system/VMRuntime;->getInstructionSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 70
    invoke-static {v3}, Ldalvik/system/VMRuntime;->getInstructionSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 71
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getSecondaryNativeLibraryDir()Ljava/lang/String;

    move-result-object v1

    .line 72
    const/4 v4, 0x1

    .line 78
    :cond_1
    :goto_0
    const-string/jumbo v5, "tango.pretrans.apk"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz v4, :cond_3

    .line 79
    invoke-static {p1}, Lcom/android/server/pm/parsing/pkg/AndroidPackageUtils;->getAllCodePathsExcludingResourceOnly(Lcom/android/server/pm/pkg/AndroidPackage;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 80
    .local v7, "apk":Ljava/lang/String;
    const/4 v8, 0x1

    invoke-direct {p0, v7, v0, v8}, Lcom/android/server/pm/Miui32BitTranslateHelper;->do32BitPretranslateOnArtService(Ljava/lang/String;IZ)V

    .line 81
    .end local v7    # "apk":Ljava/lang/String;
    goto :goto_1

    :cond_2
    goto :goto_2

    .line 83
    :cond_3
    sget-object v5, Lcom/android/server/pm/Miui32BitTranslateHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "32BitPretranslate: skipping APK pre-translation for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :goto_2
    const-string/jumbo v5, "tango.pretrans.lib"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 88
    if-eqz v1, :cond_6

    .line 89
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 90
    .local v5, "f":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 91
    new-instance v7, Lcom/android/server/pm/Miui32BitTranslateHelper$1;

    invoke-direct {v7, p0}, Lcom/android/server/pm/Miui32BitTranslateHelper$1;-><init>(Lcom/android/server/pm/Miui32BitTranslateHelper;)V

    invoke-virtual {v5, v7}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v7

    array-length v8, v7

    move v9, v6

    :goto_3
    if-ge v9, v8, :cond_4

    aget-object v10, v7, v9

    .line 96
    .local v10, "lib":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11, v0, v6}, Lcom/android/server/pm/Miui32BitTranslateHelper;->do32BitPretranslateOnArtService(Ljava/lang/String;IZ)V

    .line 91
    .end local v10    # "lib":Ljava/io/File;
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 99
    .end local v5    # "f":Ljava/io/File;
    :cond_4
    goto :goto_4

    .line 101
    :cond_5
    sget-object v5, Lcom/android/server/pm/Miui32BitTranslateHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "32BitPretranslate: skipping shared library pre-translation for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_6
    :goto_4
    return-void
.end method

.method public do32BitPretranslateOnInstall(Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V
    .locals 2
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "pkgSetting"    # Lcom/android/server/pm/pkg/PackageStateInternal;

    .line 46
    const-string/jumbo v0, "tango.pretrans_on_install"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-static {}, Lcom/android/server/pm/Miui32BitTranslateHelper;->get32BitTransHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/pm/Miui32BitTranslateHelper$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/pm/Miui32BitTranslateHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/Miui32BitTranslateHelper;Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 49
    :cond_0
    return-void
.end method
