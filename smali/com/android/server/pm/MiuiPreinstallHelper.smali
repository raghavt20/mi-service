.class public Lcom/android/server/pm/MiuiPreinstallHelper;
.super Lcom/android/server/pm/MiuiPreinstallHelperStub;
.source "MiuiPreinstallHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    }
.end annotation


# static fields
.field private static final DATA_APP_DIR:Ljava/lang/String; = "/data/app/"

.field private static final ENCRYPTING_STATE:Ljava/lang/String; = "trigger_restart_min_framework"

.field private static final RANDOM_DIR_PREFIX:Ljava/lang/String; = "~~"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

.field private mIsDeviceUpgrading:Z

.field private mIsFirstBoot:Z

.field private mLegacyPreinstallAppPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLock:Ljava/lang/Object;

.field private mMiuiPreinstallApps:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/MiuiPreinstallApp;",
            ">;"
        }
    .end annotation
.end field

.field private mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

.field private mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

.field private mPms:Lcom/android/server/pm/PackageManagerService;

.field private mPreinstallDirs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviousSettingsFilename:Ljava/io/File;

.field private mSettingsFilename:Ljava/io/File;

.field private mSettingsReserveCopyFilename:Ljava/io/File;


# direct methods
.method public static synthetic $r8$lambda$0wP2YGMYBTMdPJxedgXQ90mAHGk(Lcom/android/server/pm/MiuiPreinstallHelper;Ljava/io/File;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->lambda$cleanUpResource$0(Ljava/io/File;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$JrB0RJqFiMgNTQ2A5Mwlb0EJ3-w(Lcom/android/server/pm/MiuiPreinstallHelper;Lcom/android/server/pm/BackgroundPreinstalloptService;Landroid/app/job/JobParameters;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/pm/MiuiPreinstallHelper;->lambda$onStartJob$1(Lcom/android/server/pm/BackgroundPreinstalloptService;Landroid/app/job/JobParameters;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 66
    const-class v0, Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 65
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelperStub;-><init>()V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPreinstallDirs:Ljava/util/List;

    .line 72
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    return-void
.end method

.method private addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V
    .locals 4
    .param p1, "miuiPreinstallApp"    # Lcom/android/server/pm/MiuiPreinstallApp;

    .line 998
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addMiuiPreinstallApp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/server/pm/MiuiPreinstallApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " versionCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 999
    invoke-virtual {p1}, Lcom/android/server/pm/MiuiPreinstallApp;->getVersionCode()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " apkPath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1000
    invoke-virtual {p1}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 998
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1002
    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {p1}, Lcom/android/server/pm/MiuiPreinstallApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1003
    monitor-exit v0

    .line 1004
    return-void

    .line 1003
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private batchInstallApps(Ljava/util/List;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/pm/parsing/PackageLite;",
            ">;)V"
        }
    .end annotation

    .line 345
    .local p1, "apkList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/parsing/PackageLite;>;"
    move-object/from16 v0, p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 346
    .local v1, "installedApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/parsing/PackageLite;>;"
    new-instance v2, Landroid/util/SparseArray;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 347
    .local v2, "activeSessions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/content/pm/parsing/PackageLite;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 348
    .local v3, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const-wide/16 v4, 0x0

    .line 349
    .local v4, "waitDuration":J
    new-instance v6, Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v6, v7}, Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;-><init>(I)V

    .line 350
    .local v6, "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    iget-object v7, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v7, v7, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    .line 351
    .local v7, "context":Landroid/content/Context;
    if-nez v7, :cond_0

    .line 352
    sget-object v8, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v9, "batchInstallApps context is null!"

    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/parsing/PackageLite;

    .line 355
    .local v9, "packageLite":Landroid/content/pm/parsing/PackageLite;
    invoke-virtual {v9}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 356
    sget-object v10, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Fail to installApp: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", duplicate package name, version: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 357
    invoke-virtual {v9}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 356
    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    goto :goto_0

    .line 360
    :cond_1
    invoke-virtual {v9}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 361
    invoke-direct {v0, v7, v6, v9}, Lcom/android/server/pm/MiuiPreinstallHelper;->installOne(Landroid/content/Context;Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;Landroid/content/pm/parsing/PackageLite;)I

    move-result v10

    .line 362
    .local v10, "sessionId":I
    if-lez v10, :cond_2

    .line 363
    invoke-virtual {v2, v10, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 364
    sget-object v11, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v12, 0x2

    invoke-virtual {v11, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v11

    add-long/2addr v4, v11

    .line 366
    .end local v9    # "packageLite":Landroid/content/pm/parsing/PackageLite;
    .end local v10    # "sessionId":I
    :cond_2
    goto :goto_0

    .line 367
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 368
    .local v8, "start":J
    :goto_1
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v10

    const-string v11, "Failed to install "

    const/4 v12, 0x0

    if-lez v10, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v13

    sub-long/2addr v13, v8

    cmp-long v10, v13, v4

    if-gez v10, :cond_7

    .line 369
    const-wide/16 v13, 0x3e8

    invoke-static {v13, v14}, Landroid/os/SystemClock;->sleep(J)V

    .line 370
    invoke-virtual {v6}, Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;->getResultsNoWait()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/Intent;

    .line 371
    .local v13, "intent":Landroid/content/Intent;
    nop

    .line 372
    const-string v14, "android.content.pm.extra.SESSION_ID"

    invoke-virtual {v13, v14, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    .line 373
    .local v14, "sessionId":I
    invoke-virtual {v2, v14}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v15

    if-gez v15, :cond_4

    .line 374
    goto :goto_2

    .line 376
    :cond_4
    invoke-virtual {v2, v14}, Landroid/util/SparseArray;->removeReturnOld(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/pm/parsing/PackageLite;

    .line 377
    .local v15, "pkgLite":Landroid/content/pm/parsing/PackageLite;
    const-string v12, "android.content.pm.extra.STATUS"

    move-object/from16 v16, v3

    .end local v3    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .local v16, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v3, 0x1

    invoke-virtual {v13, v12, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 379
    .local v3, "status":I
    if-nez v3, :cond_5

    .line 380
    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    move-object/from16 v3, v16

    const/4 v12, 0x0

    goto :goto_2

    .line 383
    :cond_5
    nop

    .line 384
    const-string v12, "android.content.pm.extra.STATUS_MESSAGE"

    invoke-virtual {v13, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 385
    .local v12, "errorMsg":Ljava/lang/String;
    move-wide/from16 v17, v4

    .end local v4    # "waitDuration":J
    .local v17, "waitDuration":J
    sget-object v4, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v19, v6

    .end local v6    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    .local v19, "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    invoke-virtual {v15}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": error code="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", msg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    .end local v3    # "status":I
    .end local v12    # "errorMsg":Ljava/lang/String;
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v14    # "sessionId":I
    .end local v15    # "pkgLite":Landroid/content/pm/parsing/PackageLite;
    move-object/from16 v3, v16

    move-wide/from16 v4, v17

    move-object/from16 v6, v19

    const/4 v12, 0x0

    goto :goto_2

    .end local v16    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v17    # "waitDuration":J
    .end local v19    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    .local v3, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v4    # "waitDuration":J
    .restart local v6    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    :cond_6
    move-object/from16 v16, v3

    move-wide/from16 v17, v4

    move-object/from16 v19, v6

    .end local v3    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v4    # "waitDuration":J
    .end local v6    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    .restart local v16    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v17    # "waitDuration":J
    .restart local v19    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    goto/16 :goto_1

    .line 368
    .end local v16    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v17    # "waitDuration":J
    .end local v19    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    .restart local v3    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v4    # "waitDuration":J
    .restart local v6    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    :cond_7
    move-object/from16 v16, v3

    move-wide/from16 v17, v4

    move-object/from16 v19, v6

    .line 390
    .end local v3    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v4    # "waitDuration":J
    .end local v6    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    .restart local v16    # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v17    # "waitDuration":J
    .restart local v19    # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_8

    .line 391
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 392
    .local v5, "sessionId":I
    invoke-virtual {v2, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/parsing/PackageLite;

    .line 393
    .local v6, "pkgLite":Landroid/content/pm/parsing/PackageLite;
    sget-object v10, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v6}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": timeout, sessionId="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    .end local v5    # "sessionId":I
    .end local v6    # "pkgLite":Landroid/content/pm/parsing/PackageLite;
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 397
    .end local v3    # "i":I
    :cond_8
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/parsing/PackageLite;

    .line 398
    .local v4, "pkgLite":Landroid/content/pm/parsing/PackageLite;
    new-instance v5, Lcom/android/server/pm/MiuiPreinstallApp;

    invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 399
    invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getLongVersionCode()J

    move-result-wide v10

    invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v5, v6, v10, v11, v12}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 398
    invoke-direct {v0, v5}, Lcom/android/server/pm/MiuiPreinstallHelper;->addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V

    .line 400
    .end local v4    # "pkgLite":Landroid/content/pm/parsing/PackageLite;
    goto :goto_4

    .line 401
    :cond_9
    return-void
.end method

.method private cleanUpResource(Ljava/io/File;)V
    .locals 1
    .param p1, "dstCodePath"    # Ljava/io/File;

    .line 785
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 786
    new-instance v0, Lcom/android/server/pm/MiuiPreinstallHelper$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/server/pm/MiuiPreinstallHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/MiuiPreinstallHelper;)V

    invoke-virtual {p1, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    .line 801
    :cond_0
    return-void
.end method

.method private copyLegacyPreisntallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Landroid/content/pm/parsing/PackageLite;)V
    .locals 16
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "ps"    # Lcom/android/server/pm/pkg/PackageStateInternal;
    .param p3, "packageLite"    # Landroid/content/pm/parsing/PackageLite;

    .line 696
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v3, v0

    .line 697
    .local v3, "targetAppDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 698
    .local v4, "files":[Ljava/io/File;
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->isEmpty([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "there is none apk file in path: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    return-void

    .line 702
    :cond_0
    const/4 v0, 0x0

    .line 703
    .local v0, "dstCodePath":Ljava/io/File;
    const-string v5, "/data/app/"

    if-eqz p2, :cond_1

    invoke-interface/range {p2 .. p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 704
    invoke-interface/range {p2 .. p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPath()Ljava/io/File;

    move-result-object v0

    .line 705
    invoke-direct {v1, v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->cleanUpResource(Ljava/io/File;)V

    .line 708
    :cond_1
    if-nez v0, :cond_2

    .line 709
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 710
    invoke-virtual/range {p3 .. p3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/android/server/pm/PackageManagerServiceUtils;->getNextCodePath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    move-object v5, v0

    goto :goto_0

    .line 708
    :cond_2
    move-object v5, v0

    .line 713
    .end local v0    # "dstCodePath":Ljava/io/File;
    .local v5, "dstCodePath":Ljava/io/File;
    :goto_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 714
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 717
    :cond_3
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v6, -0x1

    if-eqz v0, :cond_5

    .line 718
    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 719
    .local v0, "codePathParent":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "~~"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    const/16 v8, 0x1fd

    if-eqz v7, :cond_4

    .line 720
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v8, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 723
    :cond_4
    invoke-static {v5, v8, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 726
    .end local v0    # "codePathParent":Ljava/io/File;
    :cond_5
    const/4 v0, 0x0

    .line 727
    .local v0, "res":Z
    array-length v7, v4

    const/4 v8, 0x0

    move v9, v0

    move v10, v8

    .end local v0    # "res":Z
    .local v9, "res":Z
    :goto_1
    if-ge v10, v7, :cond_7

    aget-object v11, v4, v10

    .line 728
    .local v11, "file":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v0, v5, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v12, v0

    .line 731
    .local v12, "dstFile":Ljava/io/File;
    :try_start_0
    invoke-static {v11, v12}, Landroid/os/FileUtils;->copy(Ljava/io/File;Ljava/io/File;)J

    .line 732
    const/16 v0, 0x1a4

    invoke-static {v12, v0, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_2

    :cond_6
    move v0, v8

    .line 736
    .end local v9    # "res":Z
    .restart local v0    # "res":Z
    :goto_2
    move v9, v0

    goto :goto_3

    .line 733
    .end local v0    # "res":Z
    .restart local v9    # "res":Z
    :catch_0
    move-exception v0

    .line 734
    .local v0, "e":Ljava/io/IOException;
    sget-object v13, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Copy failed from: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " to "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 735
    invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 734
    invoke-static {v13, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    .end local v0    # "e":Ljava/io/IOException;
    .end local v11    # "file":Ljava/io/File;
    .end local v12    # "dstFile":Ljava/io/File;
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 738
    :cond_7
    if-eqz v9, :cond_8

    .line 739
    new-instance v0, Lcom/android/server/pm/MiuiPreinstallApp;

    invoke-virtual/range {p3 .. p3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 740
    invoke-virtual/range {p3 .. p3}, Landroid/content/pm/parsing/PackageLite;->getLongVersionCode()J

    move-result-wide v7

    invoke-virtual/range {p3 .. p3}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v6, v7, v8, v10}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 739
    invoke-direct {v1, v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V

    .line 742
    :cond_8
    return-void
.end method

.method private copyMiuiPreinstallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Lcom/android/server/pm/parsing/pkg/ParsedPackage;)V
    .locals 16
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "ps"    # Lcom/android/server/pm/pkg/PackageStateInternal;
    .param p3, "parsedPackage"    # Lcom/android/server/pm/parsing/pkg/ParsedPackage;

    .line 746
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v3, v0

    .line 747
    .local v3, "targetAppDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 748
    .local v4, "files":[Ljava/io/File;
    invoke-static {v4}, Lcom/android/internal/util/ArrayUtils;->isEmpty([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "there is none apk file in path: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    return-void

    .line 752
    :cond_0
    const/4 v0, 0x0

    .line 753
    .local v0, "dstCodePath":Ljava/io/File;
    if-eqz p2, :cond_1

    .line 754
    invoke-interface/range {p2 .. p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPath()Ljava/io/File;

    move-result-object v0

    .line 755
    invoke-direct {v1, v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->cleanUpResource(Ljava/io/File;)V

    move-object v5, v0

    goto :goto_0

    .line 753
    :cond_1
    move-object v5, v0

    .line 758
    .end local v0    # "dstCodePath":Ljava/io/File;
    .local v5, "dstCodePath":Ljava/io/File;
    :goto_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v6, -0x1

    if-eqz v0, :cond_3

    .line 759
    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 760
    .local v0, "codePathParent":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "~~"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    const/16 v8, 0x1fd

    if-eqz v7, :cond_2

    .line 761
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v8, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 763
    :cond_2
    invoke-static {v5, v8, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 766
    .end local v0    # "codePathParent":Ljava/io/File;
    :cond_3
    const/4 v0, 0x0

    .line 767
    .local v0, "res":Z
    array-length v7, v4

    const/4 v8, 0x0

    move v9, v0

    move v10, v8

    .end local v0    # "res":Z
    .local v9, "res":Z
    :goto_1
    if-ge v10, v7, :cond_5

    aget-object v11, v4, v10

    .line 768
    .local v11, "file":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v0, v5, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v12, v0

    .line 771
    .local v12, "dstFile":Ljava/io/File;
    :try_start_0
    invoke-static {v11, v12}, Landroid/os/FileUtils;->copy(Ljava/io/File;Ljava/io/File;)J

    .line 772
    const/16 v0, 0x1a4

    invoke-static {v12, v0, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    move v0, v8

    .line 776
    .end local v9    # "res":Z
    .restart local v0    # "res":Z
    :goto_2
    move v9, v0

    goto :goto_3

    .line 773
    .end local v0    # "res":Z
    .restart local v9    # "res":Z
    :catch_0
    move-exception v0

    .line 774
    .local v0, "e":Ljava/io/IOException;
    sget-object v13, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Copy failed from: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " to "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 775
    invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 774
    invoke-static {v13, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    .end local v0    # "e":Ljava/io/IOException;
    .end local v11    # "file":Ljava/io/File;
    .end local v12    # "dstFile":Ljava/io/File;
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 778
    :cond_5
    if-eqz v9, :cond_6

    .line 779
    new-instance v0, Lcom/android/server/pm/MiuiPreinstallApp;

    invoke-interface/range {p3 .. p3}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 780
    invoke-interface/range {p3 .. p3}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getLongVersionCode()J

    move-result-wide v7

    invoke-interface/range {p3 .. p3}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v6, v7, v8, v10}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 779
    invoke-direct {v1, v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V

    .line 782
    :cond_6
    return-void
.end method

.method private deleteContentsRecursive(Ljava/io/File;)Z
    .locals 8
    .param p1, "dir"    # Ljava/io/File;

    .line 804
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 805
    .local v0, "files":[Ljava/io/File;
    const/4 v1, 0x1

    .line 806
    .local v1, "success":Z
    if-eqz v0, :cond_2

    .line 807
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v0, v3

    .line 808
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 809
    invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->deleteContentsRecursive(Ljava/io/File;)Z

    move-result v5

    and-int/2addr v1, v5

    .line 811
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v5

    if-nez v5, :cond_1

    .line 812
    sget-object v5, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to delete "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    const/4 v1, 0x0

    .line 807
    .end local v4    # "file":Ljava/io/File;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 817
    :cond_2
    return v1
.end method

.method private deriveAppLib(Ljava/lang/String;)Ljava/io/File;
    .locals 6
    .param p1, "apkPath"    # Ljava/lang/String;

    .line 1076
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, p1

    .line 1077
    .local v1, "trimmedInput":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1078
    .local v0, "parts":[Ljava/lang/String;
    array-length v3, v0

    const/4 v4, 0x0

    if-lez v3, :cond_1

    array-length v3, v0

    sub-int/2addr v3, v2

    aget-object v2, v0, v3

    goto :goto_1

    :cond_1
    move-object v2, v4

    .line 1079
    .local v2, "apkName":Ljava/lang/String;
    :goto_1
    new-instance v3, Ljava/io/File;

    invoke-static {}, Lcom/android/server/pm/ScanPackageUtils;->getAppLib32InstallDir()Ljava/io/File;

    move-result-object v5

    invoke-direct {v3, v5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1080
    .local v3, "applib":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1081
    return-object v3

    .line 1083
    :cond_2
    return-object v4
.end method

.method private static doAandonSession(Landroid/content/Context;I)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sessionId"    # I

    .line 467
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    .line 468
    .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller;
    const/4 v1, 0x0

    .line 470
    .local v1, "session":Landroid/content/pm/PackageInstaller$Session;
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v2

    move-object v1, v2

    .line 471
    invoke-virtual {v1}, Landroid/content/pm/PackageInstaller$Session;->abandon()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    nop

    :goto_0
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 476
    goto :goto_1

    .line 475
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 472
    :catch_0
    move-exception v2

    .line 473
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v3, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v4, "doAandonSession failed: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 475
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 477
    :goto_1
    return-void

    .line 475
    :goto_2
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 476
    throw v2
.end method

.method private static doCommitSession(Landroid/content/Context;ILandroid/content/IntentSender;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sessionId"    # I
    .param p2, "target"    # Landroid/content/IntentSender;

    .line 497
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    .line 498
    .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller;
    const/4 v1, 0x0

    .line 500
    .local v1, "session":Landroid/content/pm/PackageInstaller$Session;
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v2

    move-object v1, v2

    .line 501
    invoke-virtual {v1, p2}, Landroid/content/pm/PackageInstaller$Session;->commit(Landroid/content/IntentSender;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    nop

    .line 506
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 502
    const/4 v2, 0x1

    return v2

    .line 506
    :catchall_0
    move-exception v2

    goto :goto_0

    .line 503
    :catch_0
    move-exception v2

    .line 504
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v4, "doCommitSession failed: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 506
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 507
    nop

    .line 508
    const/4 v2, 0x0

    return v2

    .line 506
    :goto_0
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 507
    throw v2
.end method

.method private static doCreateSession(Landroid/content/Context;Landroid/content/pm/PackageInstaller$SessionParams;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sessionParams"    # Landroid/content/pm/PackageInstaller$SessionParams;

    .line 455
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    .line 456
    .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller;
    const/4 v1, 0x0

    .line 458
    .local v1, "sessionId":I
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->createSession(Landroid/content/pm/PackageInstaller$SessionParams;)I

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 462
    goto :goto_0

    .line 460
    :catch_0
    move-exception v2

    .line 461
    .local v2, "e":Ljava/io/IOException;
    sget-object v3, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v4, "doCreateSession failed: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 463
    .end local v2    # "e":Ljava/io/IOException;
    :goto_0
    return v1
.end method

.method private doWriteSession(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;I)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "apkFile"    # Ljava/io/File;
    .param p4, "sessionId"    # I

    .line 480
    const/4 v0, 0x0

    .line 481
    .local v0, "session":Landroid/content/pm/PackageInstaller$Session;
    const/4 v1, 0x0

    .line 483
    .local v1, "pfd":Landroid/os/ParcelFileDescriptor;
    const/high16 v2, 0x10000000

    :try_start_0
    invoke-static {p3, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 484
    .end local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    .local v9, "pfd":Landroid/os/ParcelFileDescriptor;
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 485
    .end local v0    # "session":Landroid/content/pm/PackageInstaller$Session;
    .local v3, "session":Landroid/content/pm/PackageInstaller$Session;
    const-wide/16 v5, 0x0

    :try_start_2
    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v7

    move-object v4, p2

    invoke-virtual/range {v3 .. v9}, Landroid/content/pm/PackageInstaller$Session;->write(Ljava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 486
    nop

    .line 490
    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 491
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 486
    const/4 v0, 0x1

    return v0

    .line 490
    :catchall_0
    move-exception v0

    move-object v1, v9

    goto :goto_1

    .line 487
    :catch_0
    move-exception v0

    move-object v1, v9

    goto :goto_0

    .line 490
    .end local v3    # "session":Landroid/content/pm/PackageInstaller$Session;
    .restart local v0    # "session":Landroid/content/pm/PackageInstaller$Session;
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    move-object v1, v9

    goto :goto_1

    .line 487
    :catch_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    move-object v1, v9

    goto :goto_0

    .line 490
    .end local v9    # "pfd":Landroid/os/ParcelFileDescriptor;
    .restart local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    :catchall_2
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    goto :goto_1

    .line 487
    :catch_2
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    .line 488
    .local v0, "e":Ljava/lang/Exception;
    .restart local v3    # "session":Landroid/content/pm/PackageInstaller$Session;
    :goto_0
    :try_start_3
    sget-object v2, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v4, "doWriteSession failed: "

    invoke-static {v2, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 490
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 491
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 492
    nop

    .line 493
    const/4 v0, 0x0

    return v0

    .line 490
    :catchall_3
    move-exception v0

    :goto_1
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 491
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 492
    throw v0
.end method

.method private getCustAppList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 325
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 326
    .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustAppList()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustAppList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 329
    :cond_0
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->getCustAppList()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 330
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->getCustAppList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 332
    :cond_1
    return-object v0
.end method

.method public static getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;
    .locals 1

    .line 92
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelperStub;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelperStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/MiuiPreinstallHelper;

    return-object v0
.end method

.method private getSettingsFile()Lcom/android/server/pm/ResilientAtomicFile;
    .locals 8

    .line 1007
    new-instance v7, Lcom/android/server/pm/ResilientAtomicFile;

    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mSettingsFilename:Ljava/io/File;

    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPreviousSettingsFilename:Ljava/io/File;

    iget-object v3, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mSettingsReserveCopyFilename:Ljava/io/File;

    const/16 v4, 0x1b0

    const-string v5, "package manager preinstall settings"

    move-object v0, v7

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/server/pm/ResilientAtomicFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;ILjava/lang/String;Lcom/android/server/pm/ResilientAtomicFile$ReadEventLogger;)V

    return-object v7
.end method

.method private getVanwardAppList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 318
    .local v0, "vanwardAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getVanwardAppList()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 319
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getVanwardAppList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 321
    :cond_0
    return-object v0
.end method

.method private initLegacyPreinstallList()V
    .locals 4

    .line 584
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 585
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    iget-boolean v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z

    iget-boolean v3, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z

    .line 586
    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->getLegacyPreinstallList(ZZ)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 587
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    iget-boolean v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z

    iget-boolean v3, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z

    .line 588
    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getLegacyPreinstallList(ZZ)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 589
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    iget-boolean v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z

    iget-boolean v3, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z

    .line 590
    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->getLegacyPreinstallList(ZZ)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 591
    return-void
.end method

.method private initPreinstallDirs()V
    .locals 2

    .line 561
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->getPreinstallDirs()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPreinstallDirs:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->getPreinstallDirs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 566
    :cond_0
    const-string v0, "ro.miui.product_to_cust"

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 567
    invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustMiuiPreinstallDirs()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 568
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPreinstallDirs:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustMiuiPreinstallDirs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 571
    :cond_1
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getPreinstallDirs()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 577
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z

    move-result v0

    if-nez v0, :cond_3

    .line 578
    :cond_2
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPreinstallDirs:Ljava/util/List;

    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getPreinstallDirs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 581
    :cond_3
    return-void
.end method

.method private installOne(Landroid/content/Context;Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;Landroid/content/pm/parsing/PackageLite;)I
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiver"    # Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
    .param p3, "packageLite"    # Landroid/content/pm/parsing/PackageLite;

    .line 405
    nop

    .line 406
    const/4 v0, -0x1

    :try_start_0
    invoke-virtual {p3}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/server/pm/MiuiPreinstallHelper;->needLegacyBatchPreinstall(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 407
    .local v1, "useLegacyPreinstall":Z
    nop

    .line 408
    invoke-direct {p0, p3, v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->makeSessionParams(Landroid/content/pm/parsing/PackageLite;Z)Landroid/content/pm/PackageInstaller$SessionParams;

    move-result-object v2

    .line 409
    .local v2, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
    invoke-static {p1, v2}, Lcom/android/server/pm/MiuiPreinstallHelper;->doCreateSession(Landroid/content/Context;Landroid/content/pm/PackageInstaller$SessionParams;)I

    move-result v3

    .line 410
    .local v3, "sessionId":I
    if-gtz v3, :cond_0

    .line 411
    return v0

    .line 414
    :cond_0
    if-eqz v1, :cond_2

    .line 415
    invoke-virtual {p3}, Landroid/content/pm/parsing/PackageLite;->getAllApkPaths()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 416
    .local v5, "splitCodePath":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 417
    .local v6, "splitFile":Ljava/io/File;
    if-eqz v1, :cond_1

    .line 418
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, p1, v7, v6, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->doWriteSession(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 419
    invoke-static {p1, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->doAandonSession(Landroid/content/Context;I)V

    .line 420
    return v0

    .line 422
    .end local v5    # "splitCodePath":Ljava/lang/String;
    .end local v6    # "splitFile":Ljava/io/File;
    :cond_1
    goto :goto_0

    .line 425
    :cond_2
    invoke-virtual {p2}, Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->doCommitSession(Landroid/content/Context;ILandroid/content/IntentSender;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_3

    move v0, v3

    goto :goto_1

    .line 426
    :cond_3
    nop

    .line 425
    :goto_1
    return v0

    .line 427
    .end local v1    # "useLegacyPreinstall":Z
    .end local v2    # "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
    .end local v3    # "sessionId":I
    :catch_0
    move-exception v1

    .line 428
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to install "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 429
    return v0
.end method

.method private synthetic lambda$cleanUpResource$0(Ljava/io/File;)Z
    .locals 3
    .param p1, "f"    # Ljava/io/File;

    .line 788
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".apk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 789
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "list and delete "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 791
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->deleteContentsRecursive(Ljava/io/File;)Z

    .line 794
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 796
    :cond_1
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "list unknown file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private synthetic lambda$onStartJob$1(Lcom/android/server/pm/BackgroundPreinstalloptService;Landroid/app/job/JobParameters;)V
    .locals 7
    .param p1, "jobService"    # Lcom/android/server/pm/BackgroundPreinstalloptService;
    .param p2, "params"    # Landroid/app/job/JobParameters;

    .line 1041
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1042
    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1043
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/MiuiPreinstallApp;>;"
    invoke-direct {p0, v2}, Lcom/android/server/pm/MiuiPreinstallHelper;->shouldCleanUp(Ljava/util/Map$Entry;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1044
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/MiuiPreinstallApp;

    invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->deriveAppLib(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 1045
    .local v3, "appLib":Ljava/io/File;
    if-eqz v3, :cond_0

    .line 1046
    sget-object v4, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "will clean up "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1047
    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->removeNativeBinariesFromDir(Ljava/io/File;Z)V

    .line 1050
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/MiuiPreinstallApp;>;"
    .end local v3    # "appLib":Ljava/io/File;
    :cond_0
    goto :goto_0

    .line 1051
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1053
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lcom/android/server/pm/BackgroundPreinstalloptService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 1054
    return-void

    .line 1051
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private makeSessionParams(Landroid/content/pm/parsing/PackageLite;Z)Landroid/content/pm/PackageInstaller$SessionParams;
    .locals 4
    .param p1, "pkgLite"    # Landroid/content/pm/parsing/PackageLite;
    .param p2, "useLegacyPreinstall"    # Z

    .line 434
    new-instance v0, Landroid/content/pm/PackageInstaller$SessionParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;-><init>(I)V

    .line 436
    .local v0, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallAsInstantApp(Z)V

    .line 437
    const-string v1, "android"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallerPackageName(Ljava/lang/String;)V

    .line 438
    invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setAppPackageName(Ljava/lang/String;)V

    .line 439
    invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getInstallLocation()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallLocation(I)V

    .line 440
    if-nez p2, :cond_0

    .line 442
    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionParams;->setIsMiuiPreinstall()V

    .line 443
    invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setApkLocation(Ljava/lang/String;)V

    .line 446
    :cond_0
    nop

    .line 447
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p1, v1}, Lcom/android/internal/content/InstallLocationUtils;->calculateInstalledSize(Landroid/content/pm/parsing/PackageLite;Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    goto :goto_0

    .line 448
    :catch_0
    move-exception v1

    .line 449
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V

    .line 451
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    return-object v0
.end method

.method private needIgnore(Landroid/content/pm/parsing/PackageLite;)Z
    .locals 3
    .param p1, "packageLite"    # Landroid/content/pm/parsing/PackageLite;

    .line 550
    invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 551
    .local v0, "apkPath":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 552
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 553
    invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    .line 554
    invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 557
    :cond_0
    const/4 v2, 0x0

    return v2

    .line 555
    :cond_1
    :goto_0
    const/4 v2, 0x1

    return v2
.end method

.method private needLegacyBatchPreinstall(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 336
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->needLegacyPreinstall(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    .line 337
    invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->needLegacyPreinstall(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 340
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 338
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private parseApkLite(Ljava/io/File;)Landroid/content/pm/parsing/ApkLite;
    .locals 5
    .param p1, "apkFile"    # Ljava/io/File;

    .line 833
    invoke-static {}, Landroid/content/pm/parsing/result/ParseTypeImpl;->forDefaultParsing()Landroid/content/pm/parsing/result/ParseTypeImpl;

    move-result-object v0

    .line 834
    .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl;
    nop

    .line 835
    invoke-virtual {v0}, Landroid/content/pm/parsing/result/ParseTypeImpl;->reset()Landroid/content/pm/parsing/result/ParseInput;

    move-result-object v1

    .line 834
    const/16 v2, 0x20

    invoke-static {v1, p1, v2}, Landroid/content/pm/parsing/ApkLiteParseUtils;->parseApkLite(Landroid/content/pm/parsing/result/ParseInput;Ljava/io/File;I)Landroid/content/pm/parsing/result/ParseResult;

    move-result-object v1

    .line 836
    .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/ApkLite;>;"
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->isError()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 837
    sget-object v2, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parseApkLite: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 838
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    .line 837
    invoke-static {v2, v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 839
    const/4 v2, 0x0

    return-object v2

    .line 841
    :cond_0
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/parsing/ApkLite;

    return-object v2
.end method

.method private parsedPackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;
    .locals 5
    .param p1, "apkFile"    # Ljava/io/File;

    .line 821
    invoke-static {}, Landroid/content/pm/parsing/result/ParseTypeImpl;->forDefaultParsing()Landroid/content/pm/parsing/result/ParseTypeImpl;

    move-result-object v0

    .line 822
    .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl;
    nop

    .line 823
    invoke-virtual {v0}, Landroid/content/pm/parsing/result/ParseTypeImpl;->reset()Landroid/content/pm/parsing/result/ParseInput;

    move-result-object v1

    .line 822
    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Landroid/content/pm/parsing/ApkLiteParseUtils;->parsePackageLite(Landroid/content/pm/parsing/result/ParseInput;Ljava/io/File;I)Landroid/content/pm/parsing/result/ParseResult;

    move-result-object v1

    .line 824
    .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/PackageLite;>;"
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->isError()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 825
    sget-object v2, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parsePackageLite: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 826
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    .line 825
    invoke-static {v2, v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 827
    const/4 v2, 0x0

    return-object v2

    .line 829
    :cond_0
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/parsing/PackageLite;

    return-object v2
.end method

.method private readHistory()V
    .locals 12

    .line 845
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 846
    return-void

    .line 848
    :cond_0
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getSettingsFile()Lcom/android/server/pm/ResilientAtomicFile;

    move-result-object v0

    .line 849
    .local v0, "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    const/4 v1, 0x0

    .line 851
    .local v1, "str":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {v0}, Lcom/android/server/pm/ResilientAtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v2

    .line 852
    if-nez v1, :cond_2

    .line 881
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/server/pm/ResilientAtomicFile;->close()V

    .line 853
    :cond_1
    return-void

    .line 855
    :cond_2
    :try_start_1
    invoke-static {v1}, Landroid/util/Xml;->resolvePullParser(Ljava/io/InputStream;)Lcom/android/modules/utils/TypedXmlPullParser;

    move-result-object v2

    .line 857
    .local v2, "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    :goto_0
    invoke-interface {v2}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v3

    move v4, v3

    .local v4, "type":I
    const/4 v5, 0x1

    const/4 v6, 0x2

    if-eq v3, v6, :cond_3

    if-eq v4, v5, :cond_3

    goto :goto_0

    .line 861
    :cond_3
    if-eq v4, v6, :cond_4

    .line 862
    sget-object v3, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v6, "No start tag found in preinstall settings"

    invoke-static {v3, v6}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    :cond_4
    invoke-interface {v2}, Lcom/android/modules/utils/TypedXmlPullParser;->getDepth()I

    move-result v3

    .line 865
    .local v3, "outerDepth":I
    :cond_5
    :goto_1
    invoke-interface {v2}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v6

    move v4, v6

    if-eq v6, v5, :cond_9

    const/4 v6, 0x3

    if-ne v4, v6, :cond_6

    .line 866
    invoke-interface {v2}, Lcom/android/modules/utils/TypedXmlPullParser;->getDepth()I

    move-result v7

    if-le v7, v3, :cond_9

    .line 867
    :cond_6
    if-eq v4, v6, :cond_5

    const/4 v6, 0x4

    if-ne v4, v6, :cond_7

    .line 868
    goto :goto_1

    .line 870
    :cond_7
    invoke-interface {v2}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 871
    .local v6, "tagName":Ljava/lang/String;
    const-string v7, "preinstall_package"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 872
    const-string v7, "name"

    const/4 v8, 0x0

    invoke-interface {v2, v8, v7}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 873
    .local v7, "packageName":Ljava/lang/String;
    const-string/jumbo v9, "version"

    const-wide/16 v10, 0x0

    invoke-interface {v2, v8, v9, v10, v11}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeLong(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v9

    .line 874
    .local v9, "versionCode":J
    const-string v11, "path"

    invoke-interface {v2, v8, v11}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 875
    .local v8, "apkPath":Ljava/lang/String;
    new-instance v11, Lcom/android/server/pm/MiuiPreinstallApp;

    invoke-direct {v11, v7, v9, v10, v8}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-direct {p0, v11}, Lcom/android/server/pm/MiuiPreinstallHelper;->addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 877
    .end local v6    # "tagName":Ljava/lang/String;
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v8    # "apkPath":Ljava/lang/String;
    .end local v9    # "versionCode":J
    :cond_8
    goto :goto_1

    .line 880
    .end local v2    # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .end local v3    # "outerDepth":I
    .end local v4    # "type":I
    :cond_9
    goto :goto_3

    .line 848
    .end local v1    # "str":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_a

    :try_start_2
    invoke-virtual {v0}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v2

    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :cond_a
    :goto_2
    throw v1

    .line 878
    .restart local v1    # "str":Ljava/io/FileInputStream;
    :catch_0
    move-exception v2

    .line 881
    .end local v1    # "str":Ljava/io/FileInputStream;
    :goto_3
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/android/server/pm/ResilientAtomicFile;->close()V

    .line 882
    .end local v0    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    :cond_b
    return-void
.end method

.method private removeNativeBinariesFromDir(Ljava/io/File;Z)V
    .locals 5
    .param p1, "nativeLibraryRoot"    # Ljava/io/File;
    .param p2, "deleteRootDir"    # Z

    .line 1092
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deleting native binaries from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1100
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 1101
    .local v0, "files":[Ljava/io/File;
    if-eqz v0, :cond_2

    .line 1102
    const/4 v1, 0x0

    .local v1, "nn":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 1103
    sget-object v2, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    Deleting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1106
    aget-object v2, v0, v1

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->removeNativeBinariesFromDir(Ljava/io/File;Z)V

    goto :goto_1

    .line 1107
    :cond_0
    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1108
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not delete native binary: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1114
    .end local v1    # "nn":I
    :cond_2
    if-eqz p2, :cond_3

    .line 1115
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1116
    sget-object v1, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not delete native binary directory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1117
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1116
    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    .end local v0    # "files":[Ljava/io/File;
    :cond_3
    return-void
.end method

.method private shouldCleanUp(Ljava/util/Map$Entry;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/MiuiPreinstallApp;",
            ">;)Z"
        }
    .end annotation

    .line 1060
    .local p1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/MiuiPreinstallApp;>;"
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v0

    .line 1061
    .local v0, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/MiuiPreinstallApp;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;

    move-result-object v1

    .line 1062
    .local v1, "apkPath":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 1063
    return v3

    .line 1065
    :cond_0
    const/4 v2, 0x1

    if-nez v0, :cond_1

    .line 1066
    return v2

    .line 1068
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1069
    invoke-interface {v0}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/app"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1070
    return v2

    .line 1072
    :cond_2
    return v3
.end method


# virtual methods
.method public deleteArtifactsForPreinstall(Lcom/android/server/pm/PackageSetting;ZI)V
    .locals 5
    .param p1, "ps"    # Lcom/android/server/pm/PackageSetting;
    .param p2, "deleteCodeAndResources"    # Z
    .param p3, "userId"    # I

    .line 234
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 235
    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;->isMiuiPreinstall()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p2, :cond_0

    goto :goto_2

    .line 237
    :cond_0
    const/4 v0, -0x1

    if-eq p3, v0, :cond_3

    .line 238
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mInjector:Lcom/android/server/pm/PackageManagerServiceInjector;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceInjector;->getUserManagerInternal()Lcom/android/server/pm/UserManagerInternal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/UserManagerInternal;->getUserIds()[I

    move-result-object v0

    .line 239
    .local v0, "userIds":[I
    array-length v1, v0

    if-lez v1, :cond_3

    .line 240
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget v3, v0, v2

    .line 241
    .local v3, "nextUserId":I
    if-ne v3, p3, :cond_1

    goto :goto_1

    .line 242
    :cond_1
    invoke-virtual {p1, v3}, Lcom/android/server/pm/PackageSetting;->getUserStateOrDefault(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/server/pm/pkg/PackageUserStateInternal;->isInstalled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 243
    sget-object v1, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v2, "Still installed by other users, don\'t delete artifacts!"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    return-void

    .line 240
    .end local v3    # "nextUserId":I
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 249
    .end local v0    # "userIds":[I
    :cond_3
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not installed by other users, will delete oat artifacts and app-libs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getLegacyNativeLibraryPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 253
    .local v0, "file":Ljava/io/File;
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/internal/content/NativeLibraryHelper;->removeNativeBinariesFromDirLI(Ljava/io/File;Z)V

    .line 254
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PackageManagerService;->deleteOatArtifactsOfPackage(Lcom/android/server/pm/Computer;Ljava/lang/String;)J

    .line 255
    return-void

    .line 235
    .end local v0    # "file":Ljava/io/File;
    :cond_4
    :goto_2
    return-void
.end method

.method public getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    if-nez v0, :cond_0

    .line 301
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v1, "BusinessPreinstallConfig is not init!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    new-instance v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-direct {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    return-object v0
.end method

.method public getOperatorPreinstallConfig()Lcom/android/server/pm/MiuiOperatorPreinstallConfig;
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    if-nez v0, :cond_0

    .line 310
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v1, "OperatorPreinstallConfig is not init!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    new-instance v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    invoke-direct {v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    return-object v0
.end method

.method public getPlatformPreinstallConfig()Lcom/android/server/pm/MiuiPlatformPreinstallConfig;
    .locals 2

    .line 291
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    if-nez v0, :cond_0

    .line 292
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v1, "PlatformPreinstallConfig is not init!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    new-instance v0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    invoke-direct {v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    return-object v0
.end method

.method public getPreinstallAppVersion(Ljava/lang/String;)I
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 914
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 915
    return v1

    .line 919
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 920
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    .line 921
    invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 922
    invoke-virtual {v2, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isCloudOfflineApp(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 923
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/MiuiPreinstallApp;

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiPreinstallApp;->getVersionCode()J

    move-result-wide v1

    long-to-int v1, v1

    monitor-exit v0

    return v1

    .line 925
    :cond_1
    monitor-exit v0

    .line 926
    return v1

    .line 925
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected getPreinstallApps()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/server/pm/MiuiPreinstallApp;",
            ">;"
        }
    .end annotation

    .line 990
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 991
    .local v0, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/MiuiPreinstallApp;>;"
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 992
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 993
    monitor-exit v1

    .line 994
    return-object v0

    .line 993
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getPreinstallDirs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 153
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPreinstallDirs:Ljava/util/List;

    return-object v0
.end method

.method public init(Lcom/android/server/pm/PackageManagerService;)V
    .locals 4
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;

    .line 97
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "use Miui Preinstall Frame."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iput-object p1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 99
    invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z

    .line 100
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z

    .line 102
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "system"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 103
    .local v0, "systemDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 105
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1fd

    const/4 v3, -0x1

    invoke-static {v1, v2, v3, v3}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 110
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "preinstall_packages.xml"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mSettingsFilename:Ljava/io/File;

    .line 111
    new-instance v1, Ljava/io/File;

    const-string v2, "preinstall_packages.xml.reservecopy"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mSettingsReserveCopyFilename:Ljava/io/File;

    .line 112
    new-instance v1, Ljava/io/File;

    const-string v2, "preinstall_packages-backup.xml"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPreviousSettingsFilename:Ljava/io/File;

    .line 114
    new-instance v1, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    invoke-direct {v1}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    .line 115
    new-instance v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-direct {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 116
    new-instance v1, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    invoke-direct {v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    .line 118
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 119
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->clear()V

    .line 120
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->readHistory()V

    .line 121
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->initPreinstallDirs()V

    .line 123
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->initLegacyPreinstallList()V

    .line 124
    return-void

    .line 121
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public init(Lcom/android/server/pm/PackageManagerService;Ljava/util/List;Ljava/util/Map;Lcom/android/server/pm/MiuiBusinessPreinstallConfig;)V
    .locals 2
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;
    .param p4, "miuiBusinessPreinstallConfig"    # Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/pm/PackageManagerService;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/pm/MiuiPreinstallApp;",
            ">;",
            "Lcom/android/server/pm/MiuiBusinessPreinstallConfig;",
            ")V"
        }
    .end annotation

    .line 131
    .local p2, "legacyPreinstallAppPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "miuiPreinstallApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/pm/MiuiPreinstallApp;>;"
    iput-object p1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 132
    invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z

    .line 133
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 134
    iput-object p2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    .line 135
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 136
    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V

    .line 137
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v1, p3}, Landroid/util/ArrayMap;->putAll(Ljava/util/Map;)V

    .line 138
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    new-instance v0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    invoke-direct {v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    .line 140
    iput-object p4, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 141
    new-instance v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    invoke-direct {v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    .line 142
    return-void

    .line 138
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public insertPreinstallPackageSetting(Lcom/android/server/pm/PackageSetting;)V
    .locals 5
    .param p1, "ps"    # Lcom/android/server/pm/PackageSetting;

    .line 686
    new-instance v0, Lcom/android/server/pm/MiuiPreinstallApp;

    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 687
    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getVersionCode()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPath()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    .line 688
    .local v0, "miuiPreinstallApp":Lcom/android/server/pm/MiuiPreinstallApp;
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 689
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 690
    monitor-exit v1

    .line 691
    return-void

    .line 690
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public installCustApps()V
    .locals 10

    .line 512
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getCustAppList()Ljava/util/List;

    move-result-object v0

    .line 513
    .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 514
    return-void

    .line 516
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 517
    .local v1, "filterList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/parsing/PackageLite;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 518
    .local v3, "apkFile":Ljava/io/File;
    invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->parsedPackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;

    move-result-object v4

    .line 519
    .local v4, "packageLite":Landroid/content/pm/parsing/PackageLite;
    if-nez v4, :cond_1

    .line 520
    sget-object v5, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "installCustApps parsedPackageLite failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    goto :goto_0

    .line 523
    :cond_1
    iget-object v5, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 524
    invoke-virtual {v5}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v5

    invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v5

    .line 525
    .local v5, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 526
    :try_start_0
    iget-object v7, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    if-nez v5, :cond_2

    .line 527
    sget-object v7, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "installCustApps package had beed uninstalled: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 528
    invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " skip!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 527
    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    monitor-exit v6

    goto :goto_0

    .line 531
    :cond_2
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    if-eqz v5, :cond_3

    .line 533
    sget-object v6, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "installCustApps the app had been installed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 534
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " keep first: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v5}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 533
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    goto/16 :goto_0

    .line 537
    :cond_3
    invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->needIgnore(Landroid/content/pm/parsing/PackageLite;)Z

    move-result v6

    if-eqz v6, :cond_4

    goto/16 :goto_0

    .line 538
    :cond_4
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 539
    .end local v3    # "apkFile":Ljava/io/File;
    .end local v4    # "packageLite":Landroid/content/pm/parsing/PackageLite;
    .end local v5    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    goto/16 :goto_0

    .line 531
    .restart local v3    # "apkFile":Ljava/io/File;
    .restart local v4    # "packageLite":Landroid/content/pm/parsing/PackageLite;
    .restart local v5    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 541
    .end local v3    # "apkFile":Ljava/io/File;
    .end local v4    # "packageLite":Landroid/content/pm/parsing/PackageLite;
    .end local v5    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 542
    return-void

    .line 545
    :cond_6
    invoke-direct {p0, v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->batchInstallApps(Ljava/util/List;)V

    .line 546
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->writeHistory()V

    .line 547
    return-void
.end method

.method public installVanwardApps()V
    .locals 9

    .line 258
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getVanwardAppList()Ljava/util/List;

    move-result-object v0

    .line 259
    .local v0, "vanwardAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    return-void

    .line 262
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 263
    .local v1, "filterList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/parsing/PackageLite;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 264
    .local v3, "apkFile":Ljava/io/File;
    invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->parsedPackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;

    move-result-object v4

    .line 265
    .local v4, "packageLite":Landroid/content/pm/parsing/PackageLite;
    if-nez v4, :cond_1

    .line 266
    sget-object v5, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v6, "installVanwardApps parsedPackageLite failed:  apkFile"

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    goto :goto_0

    .line 269
    :cond_1
    iget-object v5, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v5}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v5

    .line 270
    invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v5

    .line 271
    .local v5, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    if-eqz v5, :cond_2

    .line 272
    sget-object v6, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "installVanwardApps the app had been installed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 273
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " keep first: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v5}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 272
    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    goto :goto_0

    .line 277
    :cond_2
    invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->needIgnore(Landroid/content/pm/parsing/PackageLite;)Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_0

    .line 278
    :cond_3
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    .end local v3    # "apkFile":Ljava/io/File;
    .end local v4    # "packageLite":Landroid/content/pm/parsing/PackageLite;
    .end local v5    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    goto :goto_0

    .line 281
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 282
    return-void

    .line 285
    :cond_5
    invoke-direct {p0, v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->batchInstallApps(Ljava/util/List;)V

    .line 286
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->writeHistory()V

    .line 287
    return-void
.end method

.method public isMiuiPreinstallApp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 886
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 887
    return v1

    .line 889
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 890
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 891
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 893
    :cond_1
    monitor-exit v0

    .line 894
    return v1

    .line 893
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isMiuiPreinstallAppPath(Ljava/lang/String;)Z
    .locals 2
    .param p1, "apkPath"    # Ljava/lang/String;

    .line 931
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 932
    return v1

    .line 934
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 935
    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isBusinessPreinstall(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 936
    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isCustMiuiPreinstall(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    .line 937
    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->isPlatformPreinstall(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    .line 938
    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->isOperatorPreinstall(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 939
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 941
    :cond_2
    return v1
.end method

.method public isPreinstalledPackage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 898
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 899
    return v1

    .line 903
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 904
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    .line 905
    invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 906
    invoke-virtual {v2, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isCloudOfflineApp(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 907
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 909
    :cond_1
    monitor-exit v0

    .line 910
    return v1

    .line 909
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isSupportNewFrame()Z
    .locals 2

    .line 147
    sget v0, Landroid/os/Build$VERSION;->DEVICE_INITIAL_SDK_INT:I

    const/16 v1, 0x21

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public logEvent(ILjava/lang/String;)V
    .locals 0
    .param p1, "priority"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .line 1126
    return-void
.end method

.method public onStartJob(Lcom/android/server/pm/BackgroundPreinstalloptService;Landroid/app/job/JobParameters;)Z
    .locals 4
    .param p1, "jobService"    # Lcom/android/server/pm/BackgroundPreinstalloptService;
    .param p2, "params"    # Landroid/app/job/JobParameters;

    .line 1029
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1030
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v2, "clean up faild, reason: pkms is null"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    return v1

    .line 1033
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1034
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1035
    sget-object v2, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v3, "clean up faild, reason: mMiuiPreinstallApps is empty"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    monitor-exit v0

    return v1

    .line 1038
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1040
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/pm/MiuiPreinstallHelper$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/server/pm/MiuiPreinstallHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/pm/MiuiPreinstallHelper;Lcom/android/server/pm/BackgroundPreinstalloptService;Landroid/app/job/JobParameters;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1056
    const/4 v0, 0x1

    return v0

    .line 1038
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public performLegacyCopyPreinstall()V
    .locals 19

    .line 595
    move-object/from16 v0, p0

    const-string/jumbo v1, "vold.decrypt"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 596
    .local v1, "cryptState":Ljava/lang/String;
    const-string/jumbo v2, "trigger_restart_min_framework"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 597
    sget-object v2, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v3, "Detected encryption in progress - can\'t copy preinstall apps now!"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    return-void

    .line 602
    :cond_0
    iget-boolean v2, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z

    if-nez v2, :cond_1

    iget-boolean v2, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z

    if-nez v2, :cond_1

    .line 603
    return-void

    .line 606
    :cond_1
    iget-object v2, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 607
    return-void

    .line 609
    :cond_2
    new-instance v3, Lcom/android/server/pm/DeletePackageHelper;

    iget-object v2, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-direct {v3, v2}, Lcom/android/server/pm/DeletePackageHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V

    .line 611
    .local v3, "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
    iget-object v2, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Ljava/lang/String;

    .line 612
    .local v10, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v11, v4

    .line 613
    .local v11, "targetAppDir":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v4

    const-string v5, "copyLegacyPreisntallApp: "

    if-nez v4, :cond_3

    .line 614
    sget-object v4, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not exists!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    goto :goto_0

    .line 617
    :cond_3
    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 618
    .local v12, "files":[Ljava/io/File;
    invoke-static {v12}, Lcom/android/internal/util/ArrayUtils;->isEmpty([Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 619
    sget-object v4, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "there is none apk file in path: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    goto :goto_0

    .line 622
    :cond_4
    invoke-direct {v0, v11}, Lcom/android/server/pm/MiuiPreinstallHelper;->parsedPackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;

    move-result-object v13

    .line 623
    .local v13, "packageLite":Landroid/content/pm/parsing/PackageLite;
    if-nez v13, :cond_5

    .line 624
    sget-object v4, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parsedPackageLite failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    goto :goto_0

    .line 627
    :cond_5
    invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v14

    .line 630
    .local v14, "pkgName":Ljava/lang/String;
    invoke-direct {v0, v13}, Lcom/android/server/pm/MiuiPreinstallHelper;->needIgnore(Landroid/content/pm/parsing/PackageLite;)Z

    move-result v4

    if-eqz v4, :cond_6

    goto/16 :goto_0

    .line 632
    :cond_6
    sget-object v15, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v15, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    iget-boolean v4, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z

    const/4 v5, 0x0

    if-eqz v4, :cond_7

    .line 634
    invoke-direct {v0, v10, v5, v13}, Lcom/android/server/pm/MiuiPreinstallHelper;->copyLegacyPreisntallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Landroid/content/pm/parsing/PackageLite;)V

    move-object/from16 v18, v1

    goto/16 :goto_2

    .line 635
    :cond_7
    iget-boolean v4, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z

    if-eqz v4, :cond_e

    .line 639
    iget-object v4, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v4, v14}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 640
    iget-object v4, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 641
    invoke-virtual {v4}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v4

    invoke-interface {v4, v14}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v9

    .line 642
    .local v9, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    if-nez v9, :cond_8

    .line 644
    goto/16 :goto_0

    .line 647
    :cond_8
    invoke-interface {v9}, Lcom/android/server/pm/pkg/PackageStateInternal;->getVersionCode()J

    move-result-wide v4

    invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getLongVersionCode()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-ltz v4, :cond_9

    .line 648
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not newer than "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 649
    invoke-interface {v9}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 650
    invoke-interface {v9}, Lcom/android/server/pm/pkg/PackageStateInternal;->getVersionCode()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "], skip coping"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 648
    invoke-static {v15, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    goto/16 :goto_0

    .line 654
    :cond_9
    new-instance v4, Ljava/io/File;

    invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->parseApkLite(Ljava/io/File;)Landroid/content/pm/parsing/ApkLite;

    move-result-object v16

    .line 655
    .local v16, "apkLite":Landroid/content/pm/parsing/ApkLite;
    if-nez v16, :cond_a

    .line 656
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseApkLite failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v15, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    goto/16 :goto_0

    .line 659
    :cond_a
    nop

    .line 660
    invoke-virtual/range {v16 .. v16}, Landroid/content/pm/parsing/ApkLite;->getSigningDetails()Landroid/content/pm/SigningDetails;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;

    move-result-object v4

    .line 661
    invoke-interface {v9}, Lcom/android/server/pm/pkg/PackageStateInternal;->getSigningDetails()Landroid/content/pm/SigningDetails;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;

    move-result-object v5

    .line 660
    invoke-static {v4, v5}, Lcom/android/server/pm/PackageManagerServiceUtils;->compareSignatures([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)I

    move-result v4

    if-eqz v4, :cond_b

    .line 663
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Landroid/content/pm/parsing/ApkLite;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mismatch signature with "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 664
    invoke-interface {v9}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", delete it\'s resources and data before coping"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 663
    invoke-static {v15, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    invoke-interface {v9}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 667
    invoke-interface {v9}, Lcom/android/server/pm/pkg/PackageStateInternal;->getVersionCode()J

    move-result-wide v5

    const/4 v7, 0x0

    const/4 v8, 0x2

    const/16 v17, 0x1

    .line 666
    move-object/from16 v18, v1

    move-object v1, v9

    .end local v9    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    .local v1, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    .local v18, "cryptState":Ljava/lang/String;
    move/from16 v9, v17

    invoke-virtual/range {v3 .. v9}, Lcom/android/server/pm/DeletePackageHelper;->deletePackageX(Ljava/lang/String;JIIZ)I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_c

    .line 670
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete mismatch signature app "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed, skip coping "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 671
    invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 670
    invoke-static {v15, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 660
    .end local v18    # "cryptState":Ljava/lang/String;
    .local v1, "cryptState":Ljava/lang/String;
    .restart local v9    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    :cond_b
    move-object/from16 v18, v1

    move-object v1, v9

    .line 675
    .end local v9    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    .local v1, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    .restart local v18    # "cryptState":Ljava/lang/String;
    :cond_c
    :goto_1
    invoke-direct {v0, v10, v1, v13}, Lcom/android/server/pm/MiuiPreinstallHelper;->copyLegacyPreisntallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Landroid/content/pm/parsing/PackageLite;)V

    .line 676
    .end local v1    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    .end local v16    # "apkLite":Landroid/content/pm/parsing/ApkLite;
    goto :goto_2

    .line 678
    .end local v18    # "cryptState":Ljava/lang/String;
    .local v1, "cryptState":Ljava/lang/String;
    :cond_d
    move-object/from16 v18, v1

    .end local v1    # "cryptState":Ljava/lang/String;
    .restart local v18    # "cryptState":Ljava/lang/String;
    invoke-direct {v0, v10, v5, v13}, Lcom/android/server/pm/MiuiPreinstallHelper;->copyLegacyPreisntallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Landroid/content/pm/parsing/PackageLite;)V

    goto :goto_2

    .line 635
    .end local v18    # "cryptState":Ljava/lang/String;
    .restart local v1    # "cryptState":Ljava/lang/String;
    :cond_e
    move-object/from16 v18, v1

    .line 681
    .end local v1    # "cryptState":Ljava/lang/String;
    .end local v10    # "path":Ljava/lang/String;
    .end local v11    # "targetAppDir":Ljava/io/File;
    .end local v12    # "files":[Ljava/io/File;
    .end local v13    # "packageLite":Landroid/content/pm/parsing/PackageLite;
    .end local v14    # "pkgName":Ljava/lang/String;
    .restart local v18    # "cryptState":Ljava/lang/String;
    :goto_2
    move-object/from16 v1, v18

    goto/16 :goto_0

    .line 682
    .end local v18    # "cryptState":Ljava/lang/String;
    .restart local v1    # "cryptState":Ljava/lang/String;
    :cond_f
    return-void
.end method

.method public scheduleJob()V
    .locals 4

    .line 1014
    new-instance v0, Landroid/app/job/JobInfo$Builder;

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/android/server/pm/BackgroundPreinstalloptService;

    .line 1016
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android"

    invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v2, 0x1ceea9

    invoke-direct {v0, v2, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 1017
    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    .line 1018
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/job/JobInfo$Builder;->setRequiresDeviceIdle(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    .line 1019
    invoke-virtual {v0, v1}, Landroid/app/job/JobInfo$Builder;->setRequiresCharging(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    .line 1020
    invoke-virtual {v0, v1}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    .line 1021
    invoke-virtual {v0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v0

    .line 1022
    .local v0, "jobInfo":Landroid/app/job/JobInfo;
    iget-object v1, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    .line 1023
    const-string v2, "jobscheduler"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/job/JobScheduler;

    .line 1024
    .local v1, "jobScheduler":Landroid/app/job/JobScheduler;
    invoke-virtual {v1, v0}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 1025
    return-void
.end method

.method public shouldIgnoreInstall(Lcom/android/server/pm/parsing/pkg/ParsedPackage;)Z
    .locals 13
    .param p1, "parsedPackage"    # Lcom/android/server/pm/parsing/pkg/ParsedPackage;

    .line 158
    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "pkgName":Ljava/lang/String;
    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getLongVersionCode()J

    move-result-wide v1

    .line 160
    .local v1, "versionCode":J
    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 163
    .local v3, "apkPath":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLegacyPreinstallAppPaths:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_0

    .line 164
    sget-object v4, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "package use legacy preinstall: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    return v5

    .line 167
    :cond_0
    iget-object v4, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 169
    :try_start_0
    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v6, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_a

    .line 170
    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v6}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v6

    invoke-interface {v6, v0}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v6

    .line 171
    .local v6, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    if-nez v6, :cond_1

    .line 173
    sget-object v7, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "package had beed uninstalled: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " skip!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    monitor-exit v4

    return v5

    .line 177
    :cond_1
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 178
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isBusinessPreinstall(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 179
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getVersionCode()J

    move-result-wide v8

    cmp-long v8, v8, v1

    if-nez v8, :cond_2

    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 180
    :cond_2
    sget-object v7, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "business preinstall app must not update: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " do not scan the path: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    monitor-exit v4

    return v5

    .line 186
    :cond_3
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 187
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 188
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/app"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 189
    iget-boolean v8, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z

    if-eqz v8, :cond_8

    iget-object v8, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 190
    invoke-virtual {v8, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isBusinessPreinstall(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 191
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getVersionCode()J

    move-result-wide v8

    cmp-long v8, v8, v1

    if-gez v8, :cond_8

    .line 192
    new-instance v8, Ljava/io/File;

    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getBaseApkPath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Lcom/android/server/pm/MiuiPreinstallHelper;->parseApkLite(Ljava/io/File;)Landroid/content/pm/parsing/ApkLite;

    move-result-object v8

    .line 193
    .local v8, "apkLite":Landroid/content/pm/parsing/ApkLite;
    if-nez v8, :cond_4

    .line 194
    sget-object v9, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parseApkLite failed: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getBaseApkPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_4
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Landroid/content/pm/parsing/ApkLite;->getSigningDetails()Landroid/content/pm/SigningDetails;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;

    move-result-object v9

    goto :goto_0

    :cond_5
    const/4 v9, 0x0

    .line 198
    :goto_0
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getSigningDetails()Landroid/content/pm/SigningDetails;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;

    move-result-object v10

    .line 196
    invoke-static {v9, v10}, Lcom/android/server/pm/PackageManagerServiceUtils;->compareSignatures([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)I

    move-result v9

    if-nez v9, :cond_6

    move v7, v5

    .line 200
    .local v7, "sameSignatures":Z
    :cond_6
    if-eqz v7, :cond_7

    .line 201
    sget-object v9, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "package had beed updated: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " versionCode: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 202
    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getVersionCode()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", but current version is bertter: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 201
    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-direct {p0, v3, v6, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->copyMiuiPreinstallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Lcom/android/server/pm/parsing/pkg/ParsedPackage;)V

    goto :goto_1

    .line 206
    :cond_7
    sget-object v9, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mismatch signature with "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v6}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", need skip copy!"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    .end local v7    # "sameSignatures":Z
    .end local v8    # "apkLite":Landroid/content/pm/parsing/ApkLite;
    :cond_8
    :goto_1
    sget-object v7, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "package had beed updated: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " skip!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    monitor-exit v4

    return v5

    .line 214
    :cond_9
    monitor-exit v4

    return v7

    .line 217
    .end local v6    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    :cond_a
    sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v6, :cond_d

    .line 218
    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v6, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isBusinessPreinstall(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_c

    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    .line 219
    invoke-virtual {v6, v3, v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_c

    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mOperatorPreinstallConfig:Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    .line 220
    invoke-virtual {v6, v3, v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    goto :goto_2

    :cond_b
    move v5, v7

    goto :goto_3

    :cond_c
    :goto_2
    nop

    :goto_3
    monitor-exit v4

    .line 218
    return v5

    .line 223
    :cond_d
    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mPlatformPreinstallConfig:Lcom/android/server/pm/MiuiPlatformPreinstallConfig;

    invoke-virtual {v6, v3, v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_f

    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mBusinessPreinstallConfig:Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    .line 224
    invoke-virtual {v6, v3, v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    goto :goto_4

    :cond_e
    move v5, v7

    goto :goto_5

    :cond_f
    :goto_4
    nop

    :goto_5
    monitor-exit v4

    .line 223
    return v5

    .line 227
    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public writeHistory()V
    .locals 12

    .line 946
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-nez v0, :cond_0

    .line 947
    return-void

    .line 949
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 950
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 951
    .local v1, "startTime":J
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getSettingsFile()Lcom/android/server/pm/ResilientAtomicFile;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 952
    .local v3, "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    const/4 v4, 0x0

    .line 954
    .local v4, "str":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v5

    move-object v4, v5

    .line 956
    invoke-static {v4}, Landroid/util/Xml;->resolveSerializer(Ljava/io/OutputStream;)Lcom/android/modules/utils/TypedXmlSerializer;

    move-result-object v5

    .line 957
    .local v5, "serializer":Lcom/android/modules/utils/TypedXmlSerializer;
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v5, v8, v7}, Lcom/android/modules/utils/TypedXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 958
    const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v5, v7, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 961
    const-string v6, "preinstall_packages"

    invoke-interface {v5, v8, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 963
    iget-object v6, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mMiuiPreinstallApps:Landroid/util/ArrayMap;

    invoke-virtual {v6}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/pm/MiuiPreinstallApp;

    .line 964
    .local v7, "miuiPreinstallApp":Lcom/android/server/pm/MiuiPreinstallApp;
    const-string v9, "preinstall_package"

    invoke-interface {v5, v8, v9}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 965
    const-string v9, "name"

    invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallApp;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v8, v9, v10}, Lcom/android/modules/utils/TypedXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 966
    const-string/jumbo v9, "version"

    invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallApp;->getVersionCode()J

    move-result-wide v10

    invoke-interface {v5, v8, v9, v10, v11}, Lcom/android/modules/utils/TypedXmlSerializer;->attributeLong(Ljava/lang/String;Ljava/lang/String;J)Lorg/xmlpull/v1/XmlSerializer;

    .line 967
    const-string v9, "path"

    invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v8, v9, v10}, Lcom/android/modules/utils/TypedXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 968
    const-string v9, "preinstall_package"

    invoke-interface {v5, v8, v9}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 969
    nop

    .end local v7    # "miuiPreinstallApp":Lcom/android/server/pm/MiuiPreinstallApp;
    goto :goto_0

    .line 971
    :cond_1
    const-string v6, "preinstall_packages"

    invoke-interface {v5, v8, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 973
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlSerializer;->endDocument()V

    .line 975
    invoke-virtual {v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    .line 976
    const-string v6, "preinstall_package"

    .line 977
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v1

    .line 976
    invoke-static {v6, v7, v8}, Lcom/android/internal/logging/EventLogTags;->writeCommitSysConfigFile(Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 984
    .end local v5    # "serializer":Lcom/android/modules/utils/TypedXmlSerializer;
    goto :goto_1

    .line 951
    .end local v4    # "str":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v4

    goto :goto_2

    .line 978
    .restart local v4    # "str":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v5

    .line 979
    .local v5, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v6, Lcom/android/server/pm/MiuiPreinstallHelper;->TAG:Ljava/lang/String;

    const-string v7, "Unable to write preinstall settings, current changes will be lost at reboot"

    invoke-static {v6, v7, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 981
    if-eqz v4, :cond_2

    .line 982
    invoke-virtual {v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 985
    .end local v4    # "str":Ljava/io/FileOutputStream;
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    :try_start_3
    invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->close()V

    .line 986
    .end local v1    # "startTime":J
    .end local v3    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    :cond_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 987
    return-void

    .line 951
    .restart local v1    # "startTime":J
    .restart local v3    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    :goto_2
    if-eqz v3, :cond_4

    :try_start_4
    invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v5

    :try_start_5
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/pm/MiuiPreinstallHelper;
    :cond_4
    :goto_3
    throw v4

    .line 986
    .end local v1    # "startTime":J
    .end local v3    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    .restart local p0    # "this":Lcom/android/server/pm/MiuiPreinstallHelper;
    :catchall_2
    move-exception v1

    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v1
.end method
