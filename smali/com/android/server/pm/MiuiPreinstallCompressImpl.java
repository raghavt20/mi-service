public class com.android.server.pm.MiuiPreinstallCompressImpl extends com.android.server.pm.MiuiPreinstallCompressStub {
	 /* .source "MiuiPreinstallCompressImpl.java" */
	 /* # static fields */
	 private static final Boolean F2FS_COMPR_SUPPORT;
	 private static final java.lang.String MIUI_APP_PREDIR;
	 private static final Boolean OTA_COMPR_ENABLE;
	 private static final java.lang.String PARTNER_DIR_PREFIX;
	 private static final java.lang.String RANDOM_DIR_PREFIX;
	 private static final Boolean REGION_IS_CN;
	 private static final java.lang.String TAG;
	 private static android.os.Handler mHandler;
	 private static Boolean mIsDeviceUpgrading;
	 private static Boolean mIsFirstBoot;
	 private static java.util.List mPackageList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.lang.String preinstallPackageList;
/* # instance fields */
private com.android.server.pm.MiuiPreinstallHelper mMiuiPreinstallHelper;
private com.android.server.pm.PackageManagerService mPms;
/* # direct methods */
static com.android.server.pm.MiuiPreinstallCompressImpl ( ) {
/* .locals 2 */
/* .line 33 */
/* nop */
/* .line 34 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
/* const-string/jumbo v1, "unknown" */
android.os.SystemProperties .get ( v0,v1 );
final String v1 = "cn"; // const-string v1, "cn"
v0 = (( java.lang.String ) v0 ).equalsIgnoreCase ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
com.android.server.pm.MiuiPreinstallCompressImpl.REGION_IS_CN = (v0!= 0);
/* .line 35 */
/* nop */
/* .line 36 */
final String v0 = "ro.miui.ota_compr_enable"; // const-string v0, "ro.miui.ota_compr_enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.pm.MiuiPreinstallCompressImpl.OTA_COMPR_ENABLE = (v0!= 0);
/* .line 37 */
v0 = com.android.internal.content.F2fsUtils .isCompressSupport ( );
com.android.server.pm.MiuiPreinstallCompressImpl.F2FS_COMPR_SUPPORT = (v0!= 0);
/* .line 38 */
com.android.server.pm.MiuiPreinstallCompressImpl .getPackageList ( );
return;
} // .end method
public com.android.server.pm.MiuiPreinstallCompressImpl ( ) {
/* .locals 0 */
/* .line 26 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallCompressStub;-><init>()V */
return;
} // .end method
private void compressLib ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "libDir" # Ljava/lang/String; */
/* .line 116 */
v0 = com.android.server.pm.MiuiPreinstallCompressImpl.mHandler;
/* new-instance v1, Lcom/android/server/pm/MiuiPreinstallCompressImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1}, Lcom/android/server/pm/MiuiPreinstallCompressImpl$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 119 */
return;
} // .end method
private static java.util.List getPackageList ( ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 122 */
/* sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->OTA_COMPR_ENABLE:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 123 */
/* .line 125 */
} // :cond_0
/* new-instance v0, Ljava/io/File; */
final String v2 = "/data/app/preinstall_package_path"; // const-string v2, "/data/app/preinstall_package_path"
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 126 */
/* .local v0, "file":Ljava/io/File; */
v3 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_1 */
/* .line 127 */
final String v2 = "MiuiPreinstallComprImpl"; // const-string v2, "MiuiPreinstallComprImpl"
final String v3 = "Preinstall file is not exist."; // const-string v3, "Preinstall file is not exist."
android.util.Slog .d ( v2,v3 );
/* .line 128 */
/* .line 130 */
} // :cond_1
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 133 */
/* .local v1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .line 134 */
/* .local v3, "read":Ljava/io/InputStreamReader; */
int v4 = 0; // const/4 v4, 0x0
/* .line 136 */
/* .local v4, "buffer":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v5, Ljava/io/FileInputStream; */
/* invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* move-object v2, v5 */
/* .line 137 */
/* .local v2, "inputStream":Ljava/io/InputStream; */
/* new-instance v5, Ljava/io/InputStreamReader; */
/* const-string/jumbo v6, "utf-8" */
/* invoke-direct {v5, v2, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V */
/* move-object v3, v5 */
/* .line 138 */
/* new-instance v5, Ljava/io/BufferedReader; */
/* invoke-direct {v5, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v4, v5 */
/* .line 139 */
} // :cond_2
} // :goto_0
(( java.io.BufferedReader ) v4 ).readLine ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v6, v5 */
/* .local v6, "line":Ljava/lang/String; */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 140 */
final String v5 = "/product/data-app"; // const-string v5, "/product/data-app"
v5 = (( java.lang.String ) v6 ).contains ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 141 */
final String v5 = ":"; // const-string v5, ":"
v5 = (( java.lang.String ) v6 ).indexOf ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
int v7 = 0; // const/4 v7, 0x0
(( java.lang.String ) v6 ).substring ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 142 */
/* .local v5, "name":Ljava/lang/String; */
/* :try_end_0 */
/* .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 ..:try_end_0} :catch_7 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_5 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 153 */
} // .end local v2 # "inputStream":Ljava/io/InputStream;
} // .end local v5 # "name":Ljava/lang/String;
} // :cond_3
/* nop */
/* .line 155 */
try { // :try_start_1
(( java.io.InputStreamReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 158 */
/* .line 156 */
/* :catch_0 */
/* move-exception v2 */
/* .line 157 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 160 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_1
/* nop */
/* .line 162 */
try { // :try_start_2
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 165 */
/* .line 163 */
/* :catch_1 */
/* move-exception v2 */
/* .line 164 */
/* .restart local v2 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 165 */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 153 */
} // .end local v6 # "line":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
/* .line 150 */
/* :catch_2 */
/* move-exception v2 */
/* .line 151 */
/* .restart local v2 # "e":Ljava/io/IOException; */
try { // :try_start_3
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 153 */
} // .end local v2 # "e":Ljava/io/IOException;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 155 */
try { // :try_start_4
(( java.io.InputStreamReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 158 */
/* .line 156 */
/* :catch_3 */
/* move-exception v2 */
/* .line 157 */
/* .restart local v2 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 160 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_4
} // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 162 */
try { // :try_start_5
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 165 */
} // :goto_3
/* .line 163 */
/* :catch_4 */
/* move-exception v2 */
/* .line 164 */
/* .restart local v2 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 165 */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 148 */
/* :catch_5 */
/* move-exception v2 */
/* .line 149 */
/* .local v2, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_6
(( java.io.FileNotFoundException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 153 */
} // .end local v2 # "e":Ljava/io/FileNotFoundException;
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 155 */
try { // :try_start_7
(( java.io.InputStreamReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_6 */
/* .line 158 */
/* .line 156 */
/* :catch_6 */
/* move-exception v2 */
/* .line 157 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 160 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_5
} // :goto_4
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 162 */
try { // :try_start_8
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_4 */
/* .line 146 */
/* :catch_7 */
/* move-exception v2 */
/* .line 147 */
/* .local v2, "e":Ljava/io/UnsupportedEncodingException; */
try { // :try_start_9
(( java.io.UnsupportedEncodingException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_0 */
/* .line 153 */
} // .end local v2 # "e":Ljava/io/UnsupportedEncodingException;
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 155 */
try { // :try_start_a
(( java.io.InputStreamReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
/* :try_end_a */
/* .catch Ljava/io/IOException; {:try_start_a ..:try_end_a} :catch_8 */
/* .line 158 */
/* .line 156 */
/* :catch_8 */
/* move-exception v2 */
/* .line 157 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 160 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_6
} // :goto_5
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 162 */
try { // :try_start_b
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_b */
/* .catch Ljava/io/IOException; {:try_start_b ..:try_end_b} :catch_4 */
/* .line 168 */
} // :cond_7
} // :goto_6
/* .line 153 */
} // :goto_7
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 155 */
try { // :try_start_c
(( java.io.InputStreamReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
/* :try_end_c */
/* .catch Ljava/io/IOException; {:try_start_c ..:try_end_c} :catch_9 */
/* .line 158 */
/* .line 156 */
/* :catch_9 */
/* move-exception v5 */
/* .line 157 */
/* .local v5, "e":Ljava/io/IOException; */
(( java.io.IOException ) v5 ).printStackTrace ( ); // invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
/* .line 160 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_8
} // :goto_8
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 162 */
try { // :try_start_d
(( java.io.BufferedReader ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
/* :try_end_d */
/* .catch Ljava/io/IOException; {:try_start_d ..:try_end_d} :catch_a */
/* .line 165 */
/* .line 163 */
/* :catch_a */
/* move-exception v5 */
/* .line 164 */
/* .restart local v5 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v5 ).printStackTrace ( ); // invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
/* .line 167 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_9
} // :goto_9
/* throw v2 */
} // .end method
private Boolean isCompressApkFirstBootOrUpgradeNewFrame ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "rootLibPath" # Ljava/lang/String; */
/* .param p2, "scanFlags" # I */
/* .line 68 */
/* const/high16 v0, 0x40000000 # 2.0f */
/* and-int/2addr v0, p2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->REGION_IS_CN:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->F2FS_COMPR_SUPPORT:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 70 */
/* sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mIsFirstBoot:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 71 */
int v0 = 1; // const/4 v0, 0x1
/* .line 74 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isCompressApkFirstBootOrUpgradeOldFrame ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "rootLibPath" # Ljava/lang/String; */
/* .line 78 */
/* sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->OTA_COMPR_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 79 */
v0 = com.android.server.pm.MiuiPreinstallCompressImpl.mPackageList;
v0 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 80 */
/* const-string/jumbo v0, "~~" */
v0 = (( java.lang.String ) p2 ).contains ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_0 */
/* .line 81 */
int v0 = 1; // const/4 v0, 0x1
/* .line 84 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isCompressApkInstallNewFrame ( com.android.server.pm.pkg.AndroidPackage p0 ) {
/* .locals 2 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .line 99 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isMiuiPreinstallApp ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->isMiuiPreinstallApp(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->REGION_IS_CN:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->F2FS_COMPR_SUPPORT:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 101 */
int v0 = 1; // const/4 v0, 0x1
/* .line 103 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isCompressApkInstallOldFrame ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 107 */
/* sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->OTA_COMPR_ENABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 108 */
v0 = com.android.server.pm.MiuiPreinstallCompressImpl.mPackageList;
v0 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 109 */
int v0 = 1; // const/4 v0, 0x1
/* .line 112 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static void lambda$compressLib$0 ( java.lang.String p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "libDir" # Ljava/lang/String; */
/* .line 117 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
com.android.internal.content.F2fsUtils .compressAndReleaseBlocks ( v0 );
return;
} // .end method
/* # virtual methods */
public void compressPreinstallAppFirstBootOrUpgrade ( com.android.server.pm.pkg.AndroidPackage p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "scanFlags" # I */
/* .line 58 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 59 */
v0 = /* invoke-direct {p0, v0, p2}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->isCompressApkFirstBootOrUpgradeNewFrame(Ljava/lang/String;I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 60 */
/* invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->compressLib(Ljava/lang/String;)V */
/* .line 62 */
} // :cond_0
/* .line 63 */
/* .line 62 */
v0 = /* invoke-direct {p0, v0, v1}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->isCompressApkFirstBootOrUpgradeOldFrame(Ljava/lang/String;Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 64 */
/* invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->compressLib(Ljava/lang/String;)V */
/* .line 66 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void compressPreinstallAppInstall ( com.android.server.pm.pkg.AndroidPackage p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .line 89 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->isCompressApkInstallNewFrame(Lcom/android/server/pm/pkg/AndroidPackage;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 90 */
/* invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->compressLib(Ljava/lang/String;)V */
/* .line 92 */
} // :cond_0
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->isCompressApkInstallOldFrame(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 93 */
/* invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->compressLib(Ljava/lang/String;)V */
/* .line 96 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void init ( com.android.server.pm.PackageManagerService p0 ) {
/* .locals 3 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .line 46 */
final String v0 = "MiuiPreinstallComprImpl"; // const-string v0, "MiuiPreinstallComprImpl"
final String v1 = "init Miui Preinstall Compress Stub."; // const-string v1, "init Miui Preinstall Compress Stub."
android.util.Slog .d ( v0,v1 );
/* .line 47 */
this.mPms = p1;
/* .line 48 */
v0 = (( com.android.server.pm.PackageManagerService ) p1 ).isFirstBoot ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
com.android.server.pm.MiuiPreinstallCompressImpl.mIsFirstBoot = (v0!= 0);
/* .line 49 */
v0 = this.mPms;
v0 = (( com.android.server.pm.PackageManagerService ) v0 ).isDeviceUpgrading ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z
com.android.server.pm.MiuiPreinstallCompressImpl.mIsDeviceUpgrading = (v0!= 0);
/* .line 50 */
com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
this.mMiuiPreinstallHelper = v0;
/* .line 51 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "Compress"; // const-string v1, "Compress"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 52 */
/* .local v0, "handlerThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 53 */
/* new-instance v1, Landroid/os/Handler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 54 */
return;
} // .end method
