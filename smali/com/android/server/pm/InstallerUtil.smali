.class Lcom/android/server/pm/InstallerUtil;
.super Ljava/lang/Object;
.source "InstallerUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;,
        Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "InstallerUtil"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static deleteApp(Landroid/content/pm/IPackageManager;Ljava/lang/String;Z)Z
    .locals 7
    .param p0, "pm"    # Landroid/content/pm/IPackageManager;
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "keepData"    # Z

    .line 69
    const/4 v0, 0x2

    .line 70
    .local v0, "flags":I
    if-eqz p2, :cond_0

    .line 71
    or-int/lit8 v0, v0, 0x1

    .line 73
    :cond_0
    new-instance v1, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;-><init>(Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver-IA;)V

    .line 75
    .local v1, "obs":Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;
    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Landroid/content/pm/VersionedPackage;

    const/4 v4, -0x1

    invoke-direct {v3, p1, v4}, Landroid/content/pm/VersionedPackage;-><init>(Ljava/lang/String;I)V

    invoke-interface {p0, v3, v1, v2, v0}, Landroid/content/pm/IPackageManager;->deletePackageVersioned(Landroid/content/pm/VersionedPackage;Landroid/content/pm/IPackageDeleteObserver2;II)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 80
    nop

    .line 81
    monitor-enter v1

    .line 82
    :goto_0
    :try_start_1
    iget-boolean v2, v1, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;->finished:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_1

    .line 84
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    :goto_1
    goto :goto_0

    .line 85
    :catch_0
    move-exception v2

    goto :goto_1

    .line 88
    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 89
    iget-boolean v2, v1, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;->result:Z

    return v2

    .line 88
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 77
    :catch_1
    move-exception v3

    .line 78
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "InstallerUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to delete "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 79
    return v2
.end method

.method private static doAandonSession(Landroid/content/Context;I)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sessionId"    # I

    .line 295
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    .line 296
    .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller;
    const/4 v1, 0x0

    .line 298
    .local v1, "session":Landroid/content/pm/PackageInstaller$Session;
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v2

    move-object v1, v2

    .line 299
    invoke-virtual {v1}, Landroid/content/pm/PackageInstaller$Session;->abandon()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    nop

    :goto_0
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 304
    goto :goto_1

    .line 303
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 300
    :catch_0
    move-exception v2

    .line 301
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    const-string v3, "InstallerUtil"

    const-string v4, "doAandonSession failed: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 303
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 305
    :goto_1
    return-void

    .line 303
    :goto_2
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 304
    throw v2
.end method

.method private static doCommitSession(Landroid/content/Context;ILandroid/content/IntentSender;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sessionId"    # I
    .param p2, "target"    # Landroid/content/IntentSender;

    .line 183
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    .line 184
    .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller;
    const/4 v1, 0x0

    .line 186
    .local v1, "session":Landroid/content/pm/PackageInstaller$Session;
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v2

    move-object v1, v2

    .line 187
    invoke-virtual {v1, p2}, Landroid/content/pm/PackageInstaller$Session;->commit(Landroid/content/IntentSender;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    nop

    .line 192
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 188
    const/4 v2, 0x1

    return v2

    .line 192
    :catchall_0
    move-exception v2

    goto :goto_0

    .line 189
    :catch_0
    move-exception v2

    .line 190
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "InstallerUtil"

    const-string v4, "doCommitSession failed: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 193
    nop

    .line 194
    const/4 v2, 0x0

    return v2

    .line 192
    :goto_0
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 193
    throw v2
.end method

.method private static doCommitSession(Landroid/content/Context;Ljava/io/File;ILandroid/content/IntentSender;)Z
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "apkFile"    # Ljava/io/File;
    .param p2, "sessionId"    # I
    .param p3, "intentSender"    # Landroid/content/IntentSender;

    .line 267
    move-object/from16 v1, p1

    const-string v2, "InstallerUtil"

    const/4 v3, 0x0

    .line 268
    .local v3, "session":Landroid/content/pm/PackageInstaller$Session;
    const/4 v4, 0x0

    .line 269
    .local v4, "pfd":Landroid/os/ParcelFileDescriptor;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v5

    .line 271
    .local v5, "iden":J
    const/high16 v0, 0x10000000

    const/4 v7, 0x0

    :try_start_0
    invoke-static {v1, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    move-object v4, v0

    .line 272
    if-nez v4, :cond_0

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "doWriteSession failed: can\'t open "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 274
    nop

    .line 287
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 288
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 289
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 274
    return v7

    .line 276
    :cond_0
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move/from16 v15, p2

    :try_start_2
    invoke-virtual {v0, v15}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v0

    move-object v3, v0

    .line 277
    const-string v9, "base.apk"

    const-wide/16 v10, 0x0

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v12

    move-object v8, v3

    move-object v14, v4

    invoke-virtual/range {v8 .. v14}, Landroid/content/pm/PackageInstaller$Session;->write(Ljava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V

    .line 278
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 279
    move-object/from16 v8, p3

    :try_start_3
    invoke-virtual {v3, v8}, Landroid/content/pm/PackageInstaller$Session;->commit(Landroid/content/IntentSender;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 280
    nop

    .line 287
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 288
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 289
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 280
    const/4 v0, 0x1

    return v0

    .line 281
    :catch_0
    move-exception v0

    goto :goto_2

    .line 287
    :catchall_0
    move-exception v0

    goto :goto_0

    .line 281
    :catch_1
    move-exception v0

    goto :goto_1

    .line 287
    :catchall_1
    move-exception v0

    move/from16 v15, p2

    :goto_0
    move-object/from16 v8, p3

    goto :goto_3

    .line 281
    :catch_2
    move-exception v0

    move/from16 v15, p2

    :goto_1
    move-object/from16 v8, p3

    .line 282
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    const-string v9, "doWriteSession failed"

    invoke-static {v2, v9, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 283
    if-eqz v3, :cond_1

    .line 284
    invoke-virtual {v3}, Landroid/content/pm/PackageInstaller$Session;->abandon()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 287
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 288
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 289
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 290
    nop

    .line 291
    return v7

    .line 287
    :catchall_2
    move-exception v0

    :goto_3
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 288
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 289
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 290
    throw v0
.end method

.method private static doCreateSession(Landroid/content/Context;Landroid/content/pm/PackageInstaller$SessionParams;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sessionParams"    # Landroid/content/pm/PackageInstaller$SessionParams;

    .line 254
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    .line 255
    .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller;
    const/4 v1, 0x0

    .line 257
    .local v1, "sessionId":I
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->createSession(Landroid/content/pm/PackageInstaller$SessionParams;)I

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 261
    goto :goto_0

    .line 259
    :catch_0
    move-exception v2

    .line 260
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "InstallerUtil"

    const-string v4, "doCreateSession failed: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 262
    .end local v2    # "e":Ljava/io/IOException;
    :goto_0
    return v1
.end method

.method private static doWriteSession(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;I)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "apkFile"    # Ljava/io/File;
    .param p3, "sessionId"    # I

    .line 166
    const/4 v0, 0x0

    .line 167
    .local v0, "session":Landroid/content/pm/PackageInstaller$Session;
    const/4 v1, 0x0

    .line 169
    .local v1, "pfd":Landroid/os/ParcelFileDescriptor;
    const/high16 v2, 0x10000000

    :try_start_0
    invoke-static {p2, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 170
    .end local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    .local v9, "pfd":Landroid/os/ParcelFileDescriptor;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 171
    .end local v0    # "session":Landroid/content/pm/PackageInstaller$Session;
    .local v3, "session":Landroid/content/pm/PackageInstaller$Session;
    const-wide/16 v5, 0x0

    :try_start_2
    invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v7

    move-object v4, p1

    invoke-virtual/range {v3 .. v9}, Landroid/content/pm/PackageInstaller$Session;->write(Ljava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 172
    nop

    .line 176
    invoke-static {v9}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 177
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 172
    const/4 v0, 0x1

    return v0

    .line 176
    :catchall_0
    move-exception v0

    move-object v1, v9

    goto :goto_1

    .line 173
    :catch_0
    move-exception v0

    move-object v1, v9

    goto :goto_0

    .line 176
    .end local v3    # "session":Landroid/content/pm/PackageInstaller$Session;
    .restart local v0    # "session":Landroid/content/pm/PackageInstaller$Session;
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    move-object v1, v9

    goto :goto_1

    .line 173
    :catch_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    move-object v1, v9

    goto :goto_0

    .line 176
    .end local v9    # "pfd":Landroid/os/ParcelFileDescriptor;
    .restart local v1    # "pfd":Landroid/os/ParcelFileDescriptor;
    :catchall_2
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    goto :goto_1

    .line 173
    :catch_2
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    .line 174
    .local v0, "e":Ljava/lang/Exception;
    .restart local v3    # "session":Landroid/content/pm/PackageInstaller$Session;
    :goto_0
    :try_start_3
    const-string v2, "InstallerUtil"

    const-string v4, "doWriteSession failed: "

    invoke-static {v2, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 176
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 177
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 178
    nop

    .line 179
    const/4 v0, 0x0

    return v0

    .line 176
    :catchall_3
    move-exception v0

    :goto_1
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 177
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 178
    throw v0
.end method

.method static installAppList(Landroid/content/Context;Ljava/util/Collection;)Ljava/util/Map;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Collection<",
            "Ljava/io/File;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/io/File;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 200
    .local p1, "apkFileList":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/io/File;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 201
    .local v0, "installedResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/io/File;Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    return-object v0

    .line 204
    :cond_0
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 205
    .local v1, "sessions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/io/File;>;"
    new-instance v2, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;-><init>(I)V

    .line 206
    .local v2, "receiver":Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 208
    .local v3, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const-string v6, "InstallerUtil"

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    .line 209
    .local v5, "apkFile":Ljava/io/File;
    invoke-static {v5}, Lcom/android/server/pm/InstallerUtil;->parsePackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;

    move-result-object v7

    .line 210
    .local v7, "apkLite":Landroid/content/pm/parsing/PackageLite;
    if-nez v7, :cond_1

    .line 211
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to installAppList: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    goto :goto_0

    .line 215
    :cond_1
    invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 216
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to installApp: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", duplicate package name, version: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 217
    invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 216
    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    goto :goto_0

    .line 220
    :cond_2
    invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 222
    invoke-static {v7}, Lcom/android/server/pm/InstallerUtil;->makeSessionParams(Landroid/content/pm/parsing/PackageLite;)Landroid/content/pm/PackageInstaller$SessionParams;

    move-result-object v6

    .line 223
    .local v6, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
    invoke-static {p0, v6}, Lcom/android/server/pm/InstallerUtil;->doCreateSession(Landroid/content/Context;Landroid/content/pm/PackageInstaller$SessionParams;)I

    move-result v8

    .line 224
    .local v8, "sessionId":I
    if-eqz v8, :cond_3

    .line 225
    invoke-virtual {v2}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v9

    invoke-static {p0, v5, v8, v9}, Lcom/android/server/pm/InstallerUtil;->doCommitSession(Landroid/content/Context;Ljava/io/File;ILandroid/content/IntentSender;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 226
    invoke-virtual {v1, v8, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 228
    .end local v5    # "apkFile":Ljava/io/File;
    .end local v6    # "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
    .end local v7    # "apkLite":Landroid/content/pm/parsing/PackageLite;
    .end local v8    # "sessionId":I
    :cond_3
    goto :goto_0

    .line 230
    :cond_4
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v4

    .line 231
    .local v4, "size":I
    :goto_1
    if-lez v4, :cond_7

    .line 232
    invoke-virtual {v2}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;->getResult()Landroid/content/Intent;

    move-result-object v5

    .line 233
    .local v5, "result":Landroid/content/Intent;
    const-string v7, "android.content.pm.extra.SESSION_ID"

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 234
    .local v7, "sessionId":I
    invoke-virtual {v1, v7}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v8

    if-gez v8, :cond_5

    .line 235
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "InstallApp received invalid sessionId:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    goto :goto_1

    .line 238
    :cond_5
    const-string v8, "android.content.pm.extra.STATUS"

    const/4 v9, 0x1

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 240
    .local v8, "status":I
    invoke-virtual {v1, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v0, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    if-eqz v8, :cond_6

    .line 242
    const-string v9, "android.content.pm.extra.PACKAGE_NAME"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 243
    .local v9, "packageName":Ljava/lang/String;
    const-string v10, "android.content.pm.extra.STATUS_MESSAGE"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 244
    .local v10, "errorMsg":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "InstallApp failed for id:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " pkg:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " status:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " msg:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "errorMsg":Ljava/lang/String;
    :cond_6
    nop

    .end local v5    # "result":Landroid/content/Intent;
    .end local v7    # "sessionId":I
    .end local v8    # "status":I
    add-int/lit8 v4, v4, -0x1

    .line 249
    goto/16 :goto_1

    .line 250
    :cond_7
    return-object v0
.end method

.method static installApps(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 94
    .local p1, "codePathList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v0, "installedApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    return-object v0

    .line 98
    :cond_0
    new-instance v1, Landroid/util/SparseArray;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V

    .line 99
    .local v1, "activeSessions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/io/File;>;"
    const-wide/16 v2, 0x0

    .line 100
    .local v2, "waitDuration":J
    new-instance v4, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;-><init>(I)V

    .line 101
    .local v4, "receiver":Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    .line 102
    .local v6, "codePath":Ljava/io/File;
    move-object/from16 v7, p0

    invoke-static {v7, v4, v6}, Lcom/android/server/pm/InstallerUtil;->installOne(Landroid/content/Context;Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;Ljava/io/File;)I

    move-result v8

    .line 103
    .local v8, "sessionId":I
    if-lez v8, :cond_1

    .line 104
    invoke-virtual {v1, v8, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 105
    sget-object v9, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0x2

    invoke-virtual {v9, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v9

    add-long/2addr v2, v9

    .line 107
    .end local v6    # "codePath":Ljava/io/File;
    .end local v8    # "sessionId":I
    :cond_1
    goto :goto_0

    .line 108
    :cond_2
    move-object/from16 v7, p0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    .line 109
    .local v5, "start":J
    :goto_1
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v8

    const-string v9, "Failed to install "

    const-string v10, "InstallerUtil"

    const/4 v11, 0x0

    if-lez v8, :cond_6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v5

    cmp-long v8, v12, v2

    if-gez v8, :cond_6

    .line 110
    const-wide/16 v12, 0x3e8

    invoke-static {v12, v13}, Landroid/os/SystemClock;->sleep(J)V

    .line 111
    invoke-virtual {v4}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;->getResultsNoWait()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Intent;

    .line 112
    .local v12, "intent":Landroid/content/Intent;
    const-string v13, "android.content.pm.extra.SESSION_ID"

    invoke-virtual {v12, v13, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 113
    .local v13, "sessionId":I
    invoke-virtual {v1, v13}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v14

    if-gez v14, :cond_3

    .line 114
    goto :goto_2

    .line 116
    :cond_3
    invoke-virtual {v1, v13}, Landroid/util/SparseArray;->removeReturnOld(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    .line 117
    .local v14, "file":Ljava/io/File;
    const-string v15, "android.content.pm.extra.STATUS"

    const/4 v11, 0x1

    invoke-virtual {v12, v15, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 119
    .local v11, "status":I
    if-nez v11, :cond_4

    .line 120
    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    const/4 v11, 0x0

    goto :goto_2

    .line 123
    :cond_4
    const-string v15, "android.content.pm.extra.STATUS_MESSAGE"

    invoke-virtual {v12, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 124
    .local v15, "errorMsg":Ljava/lang/String;
    move-wide/from16 v16, v2

    .end local v2    # "waitDuration":J
    .local v16, "waitDuration":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":  error code="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", msg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v10, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    .end local v11    # "status":I
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v13    # "sessionId":I
    .end local v14    # "file":Ljava/io/File;
    .end local v15    # "errorMsg":Ljava/lang/String;
    move-wide/from16 v2, v16

    const/4 v11, 0x0

    goto :goto_2

    .end local v16    # "waitDuration":J
    .restart local v2    # "waitDuration":J
    :cond_5
    move-wide/from16 v16, v2

    .end local v2    # "waitDuration":J
    .restart local v16    # "waitDuration":J
    goto/16 :goto_1

    .line 109
    .end local v16    # "waitDuration":J
    .restart local v2    # "waitDuration":J
    :cond_6
    move-wide/from16 v16, v2

    .line 128
    .end local v2    # "waitDuration":J
    .restart local v16    # "waitDuration":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 129
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    .line 130
    .restart local v8    # "sessionId":I
    invoke-virtual {v1, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/io/File;

    .line 131
    .local v11, "file":Ljava/io/File;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": timeout, sessionId="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    .end local v8    # "sessionId":I
    .end local v11    # "file":Ljava/io/File;
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 133
    .end local v2    # "i":I
    :cond_7
    return-object v0
.end method

.method private static installOne(Landroid/content/Context;Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;Ljava/io/File;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "receiver"    # Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;
    .param p2, "codePath"    # Ljava/io/File;

    .line 138
    const-string v0, "InstallerUtil"

    const/4 v1, -0x1

    :try_start_0
    invoke-static {p2}, Lcom/android/server/pm/InstallerUtil;->parsePackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;

    move-result-object v2

    .line 139
    .local v2, "pkgLite":Landroid/content/pm/parsing/PackageLite;
    if-nez v2, :cond_0

    .line 140
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to installOne: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const/4 v0, -0x4

    return v0

    .line 144
    :cond_0
    invoke-static {v2}, Lcom/android/server/pm/InstallerUtil;->makeSessionParams(Landroid/content/pm/parsing/PackageLite;)Landroid/content/pm/PackageInstaller$SessionParams;

    move-result-object v3

    .line 145
    .local v3, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
    invoke-static {p0, v3}, Lcom/android/server/pm/InstallerUtil;->doCreateSession(Landroid/content/Context;Landroid/content/pm/PackageInstaller$SessionParams;)I

    move-result v4

    .line 146
    .local v4, "sessionId":I
    if-gtz v4, :cond_1

    .line 147
    return v1

    .line 150
    :cond_1
    invoke-virtual {v2}, Landroid/content/pm/parsing/PackageLite;->getAllApkPaths()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 151
    .local v6, "splitCodePath":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 152
    .local v7, "splitFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8, v7, v4}, Lcom/android/server/pm/InstallerUtil;->doWriteSession(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;I)Z

    move-result v8

    if-nez v8, :cond_2

    .line 153
    invoke-static {p0, v4}, Lcom/android/server/pm/InstallerUtil;->doAandonSession(Landroid/content/Context;I)V

    .line 154
    return v1

    .line 156
    .end local v6    # "splitCodePath":Ljava/lang/String;
    .end local v7    # "splitFile":Ljava/io/File;
    :cond_2
    goto :goto_0

    .line 157
    :cond_3
    invoke-virtual {p1}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lcom/android/server/pm/InstallerUtil;->doCommitSession(Landroid/content/Context;ILandroid/content/IntentSender;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_4

    move v1, v4

    goto :goto_1

    .line 158
    :cond_4
    nop

    .line 157
    :goto_1
    return v1

    .line 159
    .end local v2    # "pkgLite":Landroid/content/pm/parsing/PackageLite;
    .end local v3    # "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
    .end local v4    # "sessionId":I
    :catch_0
    move-exception v2

    .line 160
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to install "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 161
    return v1
.end method

.method private static makeSessionParams(Landroid/content/pm/parsing/PackageLite;)Landroid/content/pm/PackageInstaller$SessionParams;
    .locals 4
    .param p0, "pkgLite"    # Landroid/content/pm/parsing/PackageLite;

    .line 320
    new-instance v0, Landroid/content/pm/PackageInstaller$SessionParams;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;-><init>(I)V

    .line 321
    .local v0, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallAsInstantApp(Z)V

    .line 322
    const-string v1, "android"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallerPackageName(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setAppPackageName(Ljava/lang/String;)V

    .line 324
    invoke-virtual {p0}, Landroid/content/pm/parsing/PackageLite;->getInstallLocation()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallLocation(I)V

    .line 326
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1}, Lcom/android/internal/content/InstallLocationUtils;->calculateInstalledSize(Landroid/content/pm/parsing/PackageLite;Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    goto :goto_0

    .line 327
    :catch_0
    move-exception v1

    .line 328
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V

    .line 330
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    return-object v0
.end method

.method private static parsePackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;
    .locals 5
    .param p0, "codePath"    # Ljava/io/File;

    .line 308
    invoke-static {}, Landroid/content/pm/parsing/result/ParseTypeImpl;->forDefaultParsing()Landroid/content/pm/parsing/result/ParseTypeImpl;

    move-result-object v0

    .line 309
    .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl;
    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Landroid/content/pm/parsing/ApkLiteParseUtils;->parsePackageLite(Landroid/content/pm/parsing/result/ParseInput;Ljava/io/File;I)Landroid/content/pm/parsing/result/ParseResult;

    move-result-object v1

    .line 311
    .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/PackageLite;>;"
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->isError()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 312
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parsePackageLite: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 313
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getException()Ljava/lang/Exception;

    move-result-object v3

    .line 312
    const-string v4, "InstallerUtil"

    invoke-static {v4, v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 314
    const/4 v2, 0x0

    return-object v2

    .line 316
    :cond_0
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/parsing/PackageLite;

    return-object v2
.end method
