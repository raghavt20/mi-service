.class public Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
.super Lcom/android/server/pm/MiuiPreinstallConfig;
.source "MiuiBusinessPreinstallConfig.java"


# static fields
.field private static final BUSINESS_PREINSTALL_IN_DATA_PATH:Ljava/lang/String; = "/data/miui/app/recommended"

.field private static final CLOUD_CONTROL_OFFLINE_PACKAGE_LIST:Ljava/lang/String; = "/data/system/cloud_control_offline_package.list"

.field private static final CUSTOMIZED_APP_DIR:Ljava/io/File;

.field private static final CUST_MIUI_PREINSTALL_DIR:Ljava/io/File;

.field private static final CUST_MIUI_PREINSTALL_PATH:Ljava/lang/String; = "/cust/data-app"

.field private static final MIUI_BUSINESS_PREINALL_DIR:Ljava/io/File;

.field private static final MIUI_BUSINESS_PREINALL_PATH:Ljava/lang/String; = "/cust/app/customized"

.field private static final OTA_SKIP_BUSINESS_APP_LIST_FILE:Ljava/io/File;

.field private static final RECOMMENDED_APP_DIR:Ljava/io/File;

.field private static final TAG:Ljava/lang/String;

.field public static sCloudControlUninstall:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final NOT_OTA_PACKAGE_NAMES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private deviceName:Ljava/lang/String;

.field private hasLoadGlobalLegacyPreinstall:Z

.field listLegacyApkPath:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCloudControlOfflinePackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 38
    const-class v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->sCloudControlUninstall:Ljava/util/ArrayList;

    .line 46
    invoke-static {}, Lmiui/util/CustomizeUtil;->getMiuiCustomizedAppDir()Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->CUSTOMIZED_APP_DIR:Ljava/io/File;

    .line 51
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/miui/app/recommended"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->RECOMMENDED_APP_DIR:Ljava/io/File;

    .line 53
    new-instance v0, Ljava/io/File;

    const-string v1, "/cust/app/customized"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->MIUI_BUSINESS_PREINALL_DIR:Ljava/io/File;

    .line 57
    new-instance v0, Ljava/io/File;

    const-string v1, "/cust/data-app"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->CUST_MIUI_PREINSTALL_DIR:Ljava/io/File;

    .line 66
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/etc/ota_skip_apps"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->OTA_SKIP_BUSINESS_APP_LIST_FILE:Ljava/io/File;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .line 72
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallConfig;-><init>()V

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->mCloudControlOfflinePackages:Ljava/util/Set;

    .line 70
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->NOT_OTA_PACKAGE_NAMES:Ljava/util/Set;

    .line 302
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->hasLoadGlobalLegacyPreinstall:Z

    .line 303
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->listLegacyApkPath:Ljava/util/List;

    .line 74
    invoke-direct {p0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readCloudControlOfflinePackage()V

    .line 76
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->init()V

    .line 77
    return-void
.end method

.method private addPreinstallAppPathToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V
    .locals 8
    .param p2, "appDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 252
    .local p1, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "filterSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 253
    .local v0, "apps":[Ljava/io/File;
    if-eqz v0, :cond_3

    .line 255
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, v0, v2

    .line 256
    .local v3, "app":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 258
    invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getBaseApkFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    .line 259
    .local v4, "apk":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 260
    goto :goto_2

    .line 265
    :cond_0
    if-eqz p3, :cond_2

    .line 266
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 267
    .local v6, "pkgName":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 268
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    .end local v6    # "pkgName":Ljava/lang/String;
    :cond_1
    goto :goto_1

    .line 255
    .end local v3    # "app":Ljava/io/File;
    .end local v4    # "apk":Ljava/io/File;
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 274
    :cond_3
    return-void
.end method

.method private addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V
    .locals 6
    .param p2, "appDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 277
    .local p1, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .local p3, "filterSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 278
    .local v0, "apps":[Ljava/io/File;
    if-eqz v0, :cond_3

    .line 280
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, v0, v2

    .line 281
    .local v3, "app":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 283
    invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getBaseApkFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    .line 284
    .local v4, "apk":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 285
    goto :goto_1

    .line 290
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 291
    goto :goto_1

    .line 293
    :cond_1
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    .end local v3    # "app":Ljava/io/File;
    .end local v4    # "apk":Ljava/io/File;
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 296
    :cond_3
    return-void
.end method

.method private addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p2, "channelDir"    # Ljava/io/File;
    .param p3, "channelListFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 336
    .local p1, "preinstallChannelList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/FileReader;

    invoke-direct {v1, p3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 338
    .local v0, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 339
    .local v1, "channelName":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    if-eqz v2, :cond_0

    .line 340
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 343
    :cond_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    .end local v0    # "reader":Ljava/io/BufferedReader;
    .end local v1    # "channelName":Ljava/lang/String;
    goto :goto_1

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error occurs while read preinstalled channels "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method private getBaseApkFile(Ljava/io/File;)Ljava/io/File;
    .locals 3
    .param p1, "dir"    # Ljava/io/File;

    .line 299
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".apk"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private isUninstallByMccOrMnc(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 403
    const/4 v0, 0x0

    .line 405
    .local v0, "isUninstalled":Z
    const-string v1, "com.yandex.searchapp"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 406
    const-string v1, "ro.miui.build.region"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 407
    .local v1, "sku":Ljava/lang/String;
    const-string v3, "eea"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "global"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 408
    :cond_0
    const-string v3, "persist.sys.carrier.subnetwork"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ru_operator"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    move v0, v2

    .line 412
    .end local v1    # "sku":Ljava/lang/String;
    :cond_1
    return v0
.end method

.method private isValidIme(Ljava/lang/String;Ljava/util/Locale;)Z
    .locals 5
    .param p1, "locale"    # Ljava/lang/String;
    .param p2, "curLocale"    # Ljava/util/Locale;

    .line 240
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "locales":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 242
    aget-object v2, v0, v1

    invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    aget-object v2, v0, v1

    .line 243
    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    aget-object v2, v0, v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    invoke-virtual {p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_*"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 241
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    :cond_1
    :goto_1
    const/4 v2, 0x1

    return v2

    .line 248
    .end local v1    # "i":I
    :cond_2
    const/4 v1, 0x0

    return v1
.end method

.method private readCloudControlOfflinePackage()V
    .locals 4

    .line 350
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/FileReader;

    const-string v2, "/data/system/cloud_control_offline_package.list"

    invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    .local v0, "reader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .local v2, "line":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 353
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354
    goto :goto_0

    .line 356
    :cond_0
    iget-object v1, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->mCloudControlOfflinePackages:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 358
    .end local v2    # "line":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 360
    .end local v0    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 350
    .restart local v0    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v2

    :try_start_4
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
    :goto_1
    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 358
    .end local v0    # "reader":Ljava/io/BufferedReader;
    .restart local p0    # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error occurs while offline preinstall packages "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    .end local v0    # "e":Ljava/io/IOException;
    :goto_2
    return-void
.end method

.method private signCheckFailed(Ljava/lang/String;)Z
    .locals 7
    .param p1, "apkPath"    # Ljava/lang/String;

    .line 129
    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    const/4 v1, 0x0

    if-nez v0, :cond_4

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 132
    :cond_0
    invoke-direct {p0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->supportSignVerifyInCust()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    const-string v0, "/cust/app/customized"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 133
    .local v0, "custAppSupportSign":Z
    :goto_0
    sget-object v3, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sign support is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    if-eqz v0, :cond_2

    invoke-static {}, Lmiui/os/CustVerifier;->getInstance()Lmiui/os/CustVerifier;

    move-result-object v4

    if-nez v4, :cond_2

    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CustVerifier init error !"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " will won\'t install."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    return v2

    .line 138
    :cond_2
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getBaseApkFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    .line 139
    .local v4, "baseApkFile":Ljava/io/File;
    if-eqz v0, :cond_3

    invoke-static {}, Lmiui/os/CustVerifier;->getInstance()Lmiui/os/CustVerifier;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lmiui/os/CustVerifier;->verifyApkSignatue(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 140
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " verify failed!"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    return v2

    .line 143
    :cond_3
    return v1

    .line 130
    .end local v0    # "custAppSupportSign":Z
    .end local v4    # "baseApkFile":Ljava/io/File;
    :cond_4
    :goto_1
    return v1
.end method

.method private skipOTA(Ljava/lang/String;)Z
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->deviceName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "android.os.SystemProperties"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v2, "get"

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Ljava/lang/String;

    aput-object v5, v4, v1

    const-class v5, Ljava/lang/String;

    const/4 v6, 0x1

    aput-object v5, v4, v6

    .line 181
    invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v2, v3, [Ljava/lang/Object;

    const-string v3, "ro.product.name"

    aput-object v3, v2, v1

    const-string v3, ""

    aput-object v3, v2, v6

    .line 182
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->deviceName:Ljava/lang/String;

    .line 184
    :cond_0
    const-string v0, "sagit"

    iget-object v2, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.xiaomi.youpin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    sget-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skipOTA, deviceName is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    return v1

    .line 190
    :cond_1
    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v2, "Get exception"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 191
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->NOT_OTA_PACKAGE_NAMES:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private supportSignVerifyInCust()Z
    .locals 2

    .line 147
    const-string/jumbo v0, "support_sign_verify_in_cust"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private writeOfflinePreinstallPackage(Ljava/lang/String;)V
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;

    .line 374
    sget-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v1, "Write offline preinstall package name into /data/system/cloud_control_offline_package.list"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    return-void

    .line 378
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/FileWriter;

    const-string v2, "/data/system/cloud_control_offline_package.list"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    .local v0, "bufferWriter":Ljava/io/BufferedWriter;
    :try_start_1
    iget-object v1, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->mCloudControlOfflinePackages:Ljava/util/Set;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 380
    :try_start_2
    iget-object v2, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->mCloudControlOfflinePackages:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 381
    invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 382
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 383
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 384
    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 386
    .end local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    goto :goto_1

    .line 383
    .restart local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .end local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    .end local p0    # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
    .end local p1    # "pkg":Ljava/lang/String;
    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 378
    .restart local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    .restart local p0    # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
    .restart local p1    # "pkg":Ljava/lang/String;
    :catchall_1
    move-exception v1

    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v2

    :try_start_7
    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
    .end local p1    # "pkg":Ljava/lang/String;
    :goto_0
    throw v1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    .line 384
    .end local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    .restart local p0    # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
    .restart local p1    # "pkg":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v2, "Error occurs when to write offline preinstall package name."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method


# virtual methods
.method public getAdvanceApps()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 390
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 391
    .local v0, "sAdvanceApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->getPreinstallApps()Ljava/util/List;

    move-result-object v1

    .line 392
    .local v1, "preinstallApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/MiuiPreinstallApp;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/MiuiPreinstallApp;

    .line 393
    .local v3, "app":Lcom/android/server/pm/MiuiPreinstallApp;
    invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    goto :goto_0

    .line 395
    :cond_0
    invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "miuiAdvancePreload"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "miuiAdvertisement"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 396
    :cond_1
    invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    .end local v3    # "app":Lcom/android/server/pm/MiuiPreinstallApp;
    :cond_2
    goto :goto_0

    .line 399
    :cond_3
    return-object v0
.end method

.method public getCustAppList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 221
    sget-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v1, "getCustAppList"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-static {}, Lmiui/util/CustomizeUtil;->getMiuiCustVariantDir()Ljava/io/File;

    move-result-object v1

    .line 224
    .local v1, "custVariantDir":Ljava/io/File;
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 225
    .local v2, "customizedAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 226
    .local v3, "recommendedAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 228
    new-instance v4, Ljava/io/File;

    const-string v5, "customized_applist"

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v4, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 229
    new-instance v4, Ljava/io/File;

    const-string v5, "ota_customized_applist"

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v4, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 230
    new-instance v4, Ljava/io/File;

    const-string v5, "recommended_applist"

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v4, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 231
    new-instance v4, Ljava/io/File;

    const-string v5, "ota_recommended_applist"

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v4, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 233
    :cond_0
    sget-object v4, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->CUSTOMIZED_APP_DIR:Ljava/io/File;

    invoke-direct {p0, v0, v4, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V

    .line 235
    sget-object v4, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->RECOMMENDED_APP_DIR:Ljava/io/File;

    invoke-direct {p0, v0, v4, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V

    .line 236
    return-object v0
.end method

.method protected getCustMiuiPreinstallDirs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v0, "custMiuiPreinstallDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    sget-object v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->CUST_MIUI_PREINSTALL_DIR:Ljava/io/File;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    return-object v0
.end method

.method protected getLegacyPreinstallList(ZZ)Ljava/util/List;
    .locals 5
    .param p1, "isFirstBoot"    # Z
    .param p2, "isDeviceUpgrading"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 162
    if-eqz p2, :cond_0

    .line 163
    sget-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->OTA_SKIP_BUSINESS_APP_LIST_FILE:Ljava/io/File;

    iget-object v1, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->NOT_OTA_PACKAGE_NAMES:Ljava/util/Set;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 166
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v0, "legacyPreinstallApkList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 169
    .local v1, "legacyPreinstallApkSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v2, Ljava/io/File;

    invoke-static {}, Lmiui/util/CustomizeUtil;->getMiuiCustomizedDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "app_legacy_install_list"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v2, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 170
    sget-object v2, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->CUSTOMIZED_APP_DIR:Ljava/io/File;

    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallAppPathToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V

    .line 171
    sget-object v2, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->RECOMMENDED_APP_DIR:Ljava/io/File;

    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallAppPathToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V

    .line 173
    return-object v0
.end method

.method public getPeinstalledChannelList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 318
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 319
    .local v0, "preinstalledChannelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lmiui/util/CustomizeUtil;->getMiuiCustVariantDir()Ljava/io/File;

    move-result-object v1

    .line 320
    .local v1, "custVariantDir":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 321
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 322
    .local v2, "custVariantPath":Ljava/lang/String;
    sget-object v3, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->CUSTOMIZED_APP_DIR:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/customized_channellist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v3, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V

    .line 324
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ota_customized_channellist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v3, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V

    .line 326
    sget-object v3, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->RECOMMENDED_APP_DIR:Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/recommended_channellist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v3, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V

    .line 328
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/ota_recommended_channellist"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v3, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V

    .line 331
    .end local v2    # "custVariantPath":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method protected getPreinstallDirs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v0, "preinstallDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    sget-object v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->MIUI_BUSINESS_PREINALL_DIR:Ljava/io/File;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    sget-object v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->RECOMMENDED_APP_DIR:Ljava/io/File;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    return-object v0
.end method

.method protected getVanwardAppList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 198
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustAppList()Ljava/util/List;

    move-result-object v0

    .line 199
    .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v1, "vanwardAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 201
    .local v2, "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v3, Ljava/io/File;

    invoke-static {}, Lmiui/util/CustomizeUtil;->getMiuiAppDir()Ljava/io/File;

    move-result-object v4

    const-string/jumbo v5, "vanward_applist"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v3, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 203
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    .line 207
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/File;

    .line 208
    .local v4, "appDir":Ljava/io/File;
    invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getBaseApkFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v5

    .line 209
    .local v5, "apkFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 210
    goto :goto_0

    .line 212
    :cond_1
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    .end local v4    # "appDir":Ljava/io/File;
    .end local v5    # "apkFile":Ljava/io/File;
    goto :goto_0

    .line 214
    :cond_2
    return-object v1

    .line 204
    :cond_3
    :goto_1
    sget-object v3, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v4, "No vanward cust app need to install"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    return-object v1
.end method

.method protected isBusinessPreinstall(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .line 151
    const-string v0, "/cust/app/customized"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "/data/miui/app/recommended"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method protected isCloudOfflineApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 364
    iget-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->mCloudControlOfflinePackages:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->mCloudControlOfflinePackages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected isCustMiuiPreinstall(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .line 156
    const-string v0, "/cust/data-app"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 96
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v2, "apkPath is null, won\'t install."

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return v1

    .line 100
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 101
    .local v0, "apk":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 102
    sget-object v2, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "apk is not exist, won\'t install, apkPath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    return v1

    .line 105
    :cond_1
    sget-object v2, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->sCloudControlUninstall:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 106
    sget-object v2, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CloudControlUninstall apk won\'t install, packageName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    return v1

    .line 109
    :cond_2
    invoke-direct {p0, p2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->skipOTA(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 110
    sget-object v2, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " not support ota, packageName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return v1

    .line 113
    :cond_3
    invoke-direct {p0, p2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isUninstallByMccOrMnc(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 114
    sget-object v2, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "region or mcc not support,won\'t install, packageName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    return v1

    .line 117
    :cond_4
    invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->signCheckFailed(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method protected needLegacyPreinstall(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 307
    iget-boolean v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->hasLoadGlobalLegacyPreinstall:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 308
    invoke-virtual {p0, v2, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getLegacyPreinstallList(ZZ)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->listLegacyApkPath:Ljava/util/List;

    .line 309
    iput-boolean v1, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->hasLoadGlobalLegacyPreinstall:Z

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->listLegacyApkPath:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    return v1

    .line 314
    :cond_1
    return v2
.end method

.method public removeFromPreinstallList(Ljava/lang/String;)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 368
    iget-object v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->mCloudControlOfflinePackages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 369
    invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->writeOfflinePreinstallPackage(Ljava/lang/String;)V

    .line 371
    :cond_0
    return-void
.end method
