.class Lcom/android/server/pm/Encoding;
.super Ljava/lang/Object;
.source "ProfileTranscoder.java"


# static fields
.field static final SIZEOF_BYTE:I = 0x8

.field static final UINT_16_SIZE:I = 0x2

.field static final UINT_32_SIZE:I = 0x4

.field static final UINT_8_SIZE:I = 0x1


# direct methods
.method constructor <init>()V
    .locals 0

    .line 882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method bitsToBytes(I)I
    .locals 1
    .param p1, "numberOfBits"    # I

    .line 908
    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, -0x1

    and-int/lit8 v0, v0, -0x8

    div-int/lit8 v0, v0, 0x8

    return v0
.end method

.method compress([B)[B
    .locals 5
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 995
    new-instance v0, Ljava/util/zip/Deflater;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/zip/Deflater;-><init>(I)V

    .line 996
    .local v0, "compressor":Ljava/util/zip/Deflater;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 997
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v2, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {v2, v1, v0}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 998
    .local v2, "deflater":Ljava/util/zip/DeflaterOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/util/zip/DeflaterOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 999
    :try_start_2
    invoke-virtual {v2}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1000
    .end local v2    # "deflater":Ljava/util/zip/DeflaterOutputStream;
    invoke-virtual {v0}, Ljava/util/zip/Deflater;->end()V

    .line 1001
    nop

    .line 1002
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2

    .line 997
    .restart local v2    # "deflater":Ljava/util/zip/DeflaterOutputStream;
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "compressor":Ljava/util/zip/Deflater;
    .end local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .end local p0    # "this":Lcom/android/server/pm/Encoding;
    .end local p1    # "data":[B
    :goto_0
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1000
    .end local v2    # "deflater":Ljava/util/zip/DeflaterOutputStream;
    .restart local v0    # "compressor":Ljava/util/zip/Deflater;
    .restart local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local p0    # "this":Lcom/android/server/pm/Encoding;
    .restart local p1    # "data":[B
    :catchall_2
    move-exception v2

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->end()V

    .line 1001
    throw v2
.end method

.method error(Ljava/lang/String;)Ljava/lang/Exception;
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .line 1012
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method read(Ljava/io/InputStream;I)[B
    .locals 5
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 911
    new-array v0, p2, [B

    .line 912
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .line 913
    .local v1, "offset":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 914
    sub-int v2, p2, v1

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 915
    .local v2, "result":I
    if-ltz v2, :cond_0

    .line 918
    add-int/2addr v1, v2

    .line 919
    .end local v2    # "result":I
    goto :goto_0

    .line 916
    .restart local v2    # "result":I
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not enough bytes to read: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v3

    throw v3

    .line 920
    .end local v2    # "result":I
    :cond_1
    return-object v0
.end method

.method readCompressed(Ljava/io/InputStream;II)[B
    .locals 8
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "compressedDataSize"    # I
    .param p3, "uncompressedDataSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 949
    new-instance v0, Ljava/util/zip/Inflater;

    invoke-direct {v0}, Ljava/util/zip/Inflater;-><init>()V

    .line 951
    .local v0, "inf":Ljava/util/zip/Inflater;
    :try_start_0
    new-array v1, p3, [B

    .line 952
    .local v1, "result":[B
    const/4 v2, 0x0

    .line 953
    .local v2, "totalBytesRead":I
    const/4 v3, 0x0

    .line 954
    .local v3, "totalBytesInflated":I
    const/16 v4, 0x800

    new-array v4, v4, [B

    .line 955
    .local v4, "input":[B
    :goto_0
    nop

    .line 956
    invoke-virtual {v0}, Ljava/util/zip/Inflater;->finished()Z

    move-result v5

    if-nez v5, :cond_1

    .line 957
    invoke-virtual {v0}, Ljava/util/zip/Inflater;->needsDictionary()Z

    move-result v5

    if-nez v5, :cond_1

    if-ge v2, p2, :cond_1

    .line 960
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 961
    .local v5, "bytesRead":I
    if-ltz v5, :cond_0

    .line 967
    const/4 v6, 0x0

    invoke-virtual {v0, v4, v6, v5}, Ljava/util/zip/Inflater;->setInput([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 969
    sub-int v6, p3, v3

    :try_start_1
    invoke-virtual {v0, v1, v3, v6}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/2addr v3, v6

    .line 976
    nop

    .line 977
    add-int/2addr v2, v5

    .line 978
    .end local v5    # "bytesRead":I
    goto :goto_0

    .line 974
    .restart local v5    # "bytesRead":I
    :catch_0
    move-exception v6

    .line 975
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v7

    .end local v0    # "inf":Ljava/util/zip/Inflater;
    .end local p0    # "this":Lcom/android/server/pm/Encoding;
    .end local p1    # "is":Ljava/io/InputStream;
    .end local p2    # "compressedDataSize":I
    .end local p3    # "uncompressedDataSize":I
    throw v7

    .line 962
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v0    # "inf":Ljava/util/zip/Inflater;
    .restart local p0    # "this":Lcom/android/server/pm/Encoding;
    .restart local p1    # "is":Ljava/io/InputStream;
    .restart local p2    # "compressedDataSize":I
    .restart local p3    # "uncompressedDataSize":I
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid zip data. Stream ended after $totalBytesRead bytes. Expected "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " bytes"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v6

    .end local v0    # "inf":Ljava/util/zip/Inflater;
    .end local p0    # "this":Lcom/android/server/pm/Encoding;
    .end local p1    # "is":Ljava/io/InputStream;
    .end local p2    # "compressedDataSize":I
    .end local p3    # "uncompressedDataSize":I
    throw v6

    .line 979
    .end local v5    # "bytesRead":I
    .restart local v0    # "inf":Ljava/util/zip/Inflater;
    .restart local p0    # "this":Lcom/android/server/pm/Encoding;
    .restart local p1    # "is":Ljava/io/InputStream;
    .restart local p2    # "compressedDataSize":I
    .restart local p3    # "uncompressedDataSize":I
    :cond_1
    if-ne v2, p2, :cond_3

    .line 986
    invoke-virtual {v0}, Ljava/util/zip/Inflater;->finished()Z

    move-result v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v5, :cond_2

    .line 989
    nop

    .line 991
    invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V

    .line 989
    return-object v1

    .line 987
    :cond_2
    :try_start_3
    const-string v5, "Inflater did not finish"

    invoke-virtual {p0, v5}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v5

    .end local v0    # "inf":Ljava/util/zip/Inflater;
    .end local p0    # "this":Lcom/android/server/pm/Encoding;
    .end local p1    # "is":Ljava/io/InputStream;
    .end local p2    # "compressedDataSize":I
    .end local p3    # "uncompressedDataSize":I
    throw v5

    .line 980
    .restart local v0    # "inf":Ljava/util/zip/Inflater;
    .restart local p0    # "this":Lcom/android/server/pm/Encoding;
    .restart local p1    # "is":Ljava/io/InputStream;
    .restart local p2    # "compressedDataSize":I
    .restart local p3    # "uncompressedDataSize":I
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Didn\'t read enough bytes during decompression. expected="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " actual="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v5

    .end local v0    # "inf":Ljava/util/zip/Inflater;
    .end local p0    # "this":Lcom/android/server/pm/Encoding;
    .end local p1    # "is":Ljava/io/InputStream;
    .end local p2    # "compressedDataSize":I
    .end local p3    # "uncompressedDataSize":I
    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 991
    .end local v1    # "result":[B
    .end local v2    # "totalBytesRead":I
    .end local v3    # "totalBytesInflated":I
    .end local v4    # "input":[B
    .restart local v0    # "inf":Ljava/util/zip/Inflater;
    .restart local p0    # "this":Lcom/android/server/pm/Encoding;
    .restart local p1    # "is":Ljava/io/InputStream;
    .restart local p2    # "compressedDataSize":I
    .restart local p3    # "uncompressedDataSize":I
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V

    .line 992
    throw v1
.end method

.method readString(Ljava/io/InputStream;I)Ljava/lang/String;
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 941
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B

    move-result-object v1

    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method readUInt(Ljava/io/InputStream;I)J
    .locals 8
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "numberOfBytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 923
    invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 924
    .local v0, "buffer":[B
    const-wide/16 v1, 0x0

    .line 925
    .local v1, "value":J
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, p2, :cond_0

    .line 926
    aget-byte v4, v0, v3

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    .line 927
    .local v4, "next":J
    mul-int/lit8 v6, v3, 0x8

    shl-long v6, v4, v6

    add-long/2addr v1, v6

    .line 925
    .end local v4    # "next":J
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 929
    .end local v3    # "i":I
    :cond_0
    return-wide v1
.end method

.method readUInt16(Ljava/io/InputStream;)I
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 935
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/android/server/pm/Encoding;->readUInt(Ljava/io/InputStream;I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method readUInt32(Ljava/io/InputStream;)J
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 938
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/server/pm/Encoding;->readUInt(Ljava/io/InputStream;I)J

    move-result-wide v0

    return-wide v0
.end method

.method readUInt8(Ljava/io/InputStream;)I
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 932
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/server/pm/Encoding;->readUInt(Ljava/io/InputStream;I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method utf8Length(Ljava/lang/String;)I
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .line 888
    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method writeAll(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1005
    const/16 v0, 0x200

    new-array v0, v0, [B

    .line 1007
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    move v2, v1

    .local v2, "length":I
    if-lez v1, :cond_0

    .line 1008
    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 1010
    :cond_0
    return-void
.end method

.method writeString(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 1
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 905
    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 906
    return-void
.end method

.method writeUInt(Ljava/io/OutputStream;JI)V
    .locals 6
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "value"    # J
    .param p4, "numberOfBytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 892
    new-array v0, p4, [B

    .line 893
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p4, :cond_0

    .line 894
    mul-int/lit8 v2, v1, 0x8

    shr-long v2, p2, v2

    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 893
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 896
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 897
    return-void
.end method

.method writeUInt16(Ljava/io/OutputStream;I)V
    .locals 3
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 899
    int-to-long v0, p2

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/server/pm/Encoding;->writeUInt(Ljava/io/OutputStream;JI)V

    .line 900
    return-void
.end method

.method writeUInt32(Ljava/io/OutputStream;J)V
    .locals 1
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 902
    const/4 v0, 0x4

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/server/pm/Encoding;->writeUInt(Ljava/io/OutputStream;JI)V

    .line 903
    return-void
.end method
