class com.android.server.pm.PreInstallServiceTrack$1 implements android.content.ServiceConnection {
	 /* .source "PreInstallServiceTrack.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/PreInstallServiceTrack; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.pm.PreInstallServiceTrack this$0; //synthetic
/* # direct methods */
 com.android.server.pm.PreInstallServiceTrack$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/PreInstallServiceTrack; */
/* .line 32 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 35 */
v0 = this.this$0;
com.miui.analytics.ITrackBinder$Stub .asInterface ( p2 );
com.android.server.pm.PreInstallServiceTrack .-$$Nest$fputmService ( v0,v1 );
/* .line 36 */
com.android.server.pm.PreInstallServiceTrack .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "onServiceConnected: "; // const-string v2, "onServiceConnected: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
com.android.server.pm.PreInstallServiceTrack .-$$Nest$fgetmService ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 37 */
return;
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 41 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.pm.PreInstallServiceTrack .-$$Nest$fputmService ( v0,v1 );
/* .line 42 */
com.android.server.pm.PreInstallServiceTrack .-$$Nest$sfgetTAG ( );
final String v1 = "onServiceDisconnected"; // const-string v1, "onServiceDisconnected"
android.util.Slog .d ( v0,v1 );
/* .line 43 */
return;
} // .end method
