.class public Lcom/android/server/pm/PackageManagerCloudHelper;
.super Ljava/lang/Object;
.source "PackageManagerCloudHelper.java"


# static fields
.field private static final CLOUD_DATA_VERSION:Ljava/lang/String; = "version"

.field private static final CLOUD_MODULE_NAME:Ljava/lang/String; = "block_update"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mCloudNotSupportUpdateSystemApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mLastCloudConfigVersion:I

.field private final mNotSupportUpdateLock:Ljava/lang/Object;

.field private mPms:Lcom/android/server/pm/PackageManagerService;

.field private mPreviousSettingsFilename:Ljava/io/File;

.field private mSettingsFilename:Ljava/io/File;

.field private mSettingsReserveCopyFilename:Ljava/io/File;

.field private mSystemDir:Ljava/io/File;


# direct methods
.method public static synthetic $r8$lambda$TIso4l0kD6HUgso-hP0AXn4I5kw(Lcom/android/server/pm/PackageManagerCloudHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->lambda$readCloudData$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/pm/PackageManagerCloudHelper;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPms(Lcom/android/server/pm/PackageManagerCloudHelper;)Lcom/android/server/pm/PackageManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misCloudConfigUpdate(Lcom/android/server/pm/PackageManagerCloudHelper;Landroid/content/Context;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerCloudHelper;->isCloudConfigUpdate(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mreadCloudData(Lcom/android/server/pm/PackageManagerCloudHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->readCloudData()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 33
    const-class v0, Lcom/android/server/pm/PackageManagerCloudHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/android/server/pm/PackageManagerService;Landroid/content/Context;)V
    .locals 1
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mCloudNotSupportUpdateSystemApps:Ljava/util/Set;

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mNotSupportUpdateLock:Ljava/lang/Object;

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I

    .line 52
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 53
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mContext:Landroid/content/Context;

    .line 54
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->initPackageCloudSettings()V

    .line 55
    return-void
.end method

.method private getCloudSettingsFile()Lcom/android/server/pm/ResilientAtomicFile;
    .locals 8

    .line 112
    new-instance v7, Lcom/android/server/pm/ResilientAtomicFile;

    iget-object v1, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mSettingsFilename:Ljava/io/File;

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mPreviousSettingsFilename:Ljava/io/File;

    iget-object v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mSettingsReserveCopyFilename:Ljava/io/File;

    const/16 v4, 0x1b0

    const-string v5, "package manager cloud settings"

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/android/server/pm/ResilientAtomicFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;ILjava/lang/String;Lcom/android/server/pm/ResilientAtomicFile$ReadEventLogger;)V

    return-object v7
.end method

.method private initPackageCloudSettings()V
    .locals 3

    .line 58
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "system"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mSystemDir:Ljava/io/File;

    .line 59
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mSystemDir:Ljava/io/File;

    const-string v2, "packagemanger_cloud.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mSettingsFilename:Ljava/io/File;

    .line 60
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mSystemDir:Ljava/io/File;

    const-string v2, "packagemanger_cloud.xml.reservecopy"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mSettingsReserveCopyFilename:Ljava/io/File;

    .line 61
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mSystemDir:Ljava/io/File;

    const-string v2, "packagemanger_cloud-backup.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mPreviousSettingsFilename:Ljava/io/File;

    .line 62
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->readCloudSetting()V

    .line 63
    return-void
.end method

.method private isCloudConfigUpdate(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 88
    nop

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "version"

    const/4 v2, 0x0

    const-string v3, "block_update"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "dataVersion":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 92
    sget-object v1, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    const-string v3, "data version is empty, or control module block_update is not exist"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return v2

    .line 96
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 99
    .local v1, "remoteVersion":I
    iget v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I

    if-gt v1, v3, :cond_1

    .line 100
    sget-object v3, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cloud data is not modify, don\'t need update. data version = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return v2

    .line 106
    :cond_1
    iput v1, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I

    .line 107
    sget-object v2, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "package manager cloud data new version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v2, 0x1

    return v2
.end method

.method private synthetic lambda$readCloudData$0()V
    .locals 0

    .line 146
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->writeCloudSetting()V

    return-void
.end method

.method private readCloudData()V
    .locals 10

    .line 120
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mContext:Landroid/content/Context;

    .line 121
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "block_update"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 122
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_3

    .line 126
    :cond_0
    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 127
    .local v1, "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    .line 128
    .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v4

    .line 129
    .local v4, "json":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 130
    goto :goto_0

    .line 133
    :cond_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 134
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v6, "packageName"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 135
    .local v6, "jsonArray":Lorg/json/JSONArray;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v7, v8, :cond_3

    .line 136
    invoke-virtual {v6, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 137
    .local v8, "pkg":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 138
    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 135
    .end local v8    # "pkg":Ljava/lang/String;
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 141
    .end local v3    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    .end local v4    # "json":Ljava/lang/String;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "jsonArray":Lorg/json/JSONArray;
    .end local v7    # "i":I
    :cond_3
    goto :goto_0

    .line 142
    :cond_4
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mNotSupportUpdateLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :try_start_1
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mCloudNotSupportUpdateSystemApps:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 144
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mCloudNotSupportUpdateSystemApps:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 145
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    :try_start_2
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/server/pm/PackageManagerCloudHelper$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/android/server/pm/PackageManagerCloudHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/PackageManagerCloudHelper;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 149
    nop

    .end local v1    # "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    goto :goto_2

    .line 145
    .restart local v1    # "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .end local p0    # "this":Lcom/android/server/pm/PackageManagerCloudHelper;
    :try_start_4
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 147
    .end local v1    # "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
    .restart local p0    # "this":Lcom/android/server/pm/PackageManagerCloudHelper;
    :catch_0
    move-exception v1

    .line 148
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 150
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 123
    :cond_5
    :goto_3
    return-void
.end method

.method private readCloudSetting()V
    .locals 13

    .line 199
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 200
    .local v0, "startTime":J
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->getCloudSettingsFile()Lcom/android/server/pm/ResilientAtomicFile;

    move-result-object v2

    .line 201
    .local v2, "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    const/4 v3, 0x0

    .line 203
    .local v3, "str":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v3, v4

    .line 204
    if-nez v3, :cond_1

    .line 252
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->close()V

    .line 205
    :cond_0
    return-void

    .line 207
    :cond_1
    :try_start_1
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 208
    .local v4, "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v3}, Landroid/util/Xml;->resolvePullParser(Ljava/io/InputStream;)Lcom/android/modules/utils/TypedXmlPullParser;

    move-result-object v5

    .line 210
    .local v5, "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    :goto_0
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v6

    move v7, v6

    .local v7, "type":I
    const/4 v8, 0x1

    const/4 v9, 0x2

    if-eq v6, v9, :cond_2

    if-eq v7, v8, :cond_2

    goto :goto_0

    .line 215
    :cond_2
    if-eq v7, v9, :cond_4

    .line 216
    sget-object v6, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    const-string v8, "No start tag found in package manager cloud settings"

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->close()V

    .line 217
    :cond_3
    return-void

    .line 220
    :cond_4
    :try_start_2
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlPullParser;->getDepth()I

    move-result v6

    .line 221
    .local v6, "outerDepth":I
    :cond_5
    :goto_1
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v9

    move v7, v9

    if-eq v9, v8, :cond_a

    const/4 v9, 0x3

    if-ne v7, v9, :cond_6

    .line 222
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlPullParser;->getDepth()I

    move-result v10

    if-le v10, v6, :cond_a

    .line 223
    :cond_6
    if-eq v7, v9, :cond_5

    const/4 v9, 0x4

    if-ne v7, v9, :cond_7

    .line 224
    goto :goto_1

    .line 227
    :cond_7
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    .line 228
    .local v9, "tagName":Ljava/lang/String;
    const-string v10, "packages"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 229
    const-string/jumbo v10, "version"

    const/4 v11, 0x0

    invoke-interface {v5, v11, v10}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 230
    .local v10, "version":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 231
    sget-object v8, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    const-string v11, "read block-update config version is null"

    invoke-static {v8, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    goto :goto_3

    .line 234
    :cond_8
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    iput v11, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I

    .line 235
    invoke-direct {p0, v4, v5}, Lcom/android/server/pm/PackageManagerCloudHelper;->readPackageNames(Ljava/util/Set;Lcom/android/modules/utils/TypedXmlPullParser;)V

    .line 236
    .end local v10    # "version":Ljava/lang/String;
    goto :goto_2

    .line 237
    :cond_9
    sget-object v10, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unknown element under <package-cloudconfig>: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 238
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 237
    invoke-static {v10, v11}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-static {v5}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 241
    .end local v9    # "tagName":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .line 242
    :cond_a
    :goto_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 243
    sget-object v8, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "read packageCloudSetting took "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 244
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v0

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 243
    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    .end local v4    # "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .end local v6    # "outerDepth":I
    .end local v7    # "type":I
    goto :goto_4

    .line 200
    .end local v3    # "str":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v3

    goto :goto_5

    .line 245
    .restart local v3    # "str":Ljava/io/FileInputStream;
    :catch_0
    move-exception v4

    .line 246
    .local v4, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v5, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "readNotSupportUpdateConfig fail: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    const/4 v5, 0x0

    iput v5, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I

    .line 249
    invoke-virtual {v2, v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->failRead(Ljava/io/FileInputStream;Ljava/lang/Exception;)V

    .line 250
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->readCloudSetting()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 252
    .end local v3    # "str":Ljava/io/FileInputStream;
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_4
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->close()V

    .line 254
    .end local v2    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    :cond_b
    return-void

    .line 200
    .restart local v2    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    :goto_5
    if-eqz v2, :cond_c

    :try_start_4
    invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_6

    :catchall_1
    move-exception v4

    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :cond_c
    :goto_6
    throw v3
.end method

.method private readPackageNames(Ljava/util/Set;Lcom/android/modules/utils/TypedXmlPullParser;)V
    .locals 6
    .param p2, "parser"    # Lcom/android/modules/utils/TypedXmlPullParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/modules/utils/TypedXmlPullParser;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .line 258
    .local p1, "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Lcom/android/modules/utils/TypedXmlPullParser;->getDepth()I

    move-result v0

    .line 260
    .local v0, "outerDepth":I
    :cond_0
    :goto_0
    invoke-interface {p2}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v1

    move v2, v1

    .local v2, "type":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_5

    const/4 v1, 0x3

    if-ne v2, v1, :cond_1

    .line 261
    invoke-interface {p2}, Lcom/android/modules/utils/TypedXmlPullParser;->getDepth()I

    move-result v3

    if-le v3, v0, :cond_5

    .line 262
    :cond_1
    if-eq v2, v1, :cond_0

    const/4 v1, 0x4

    if-ne v2, v1, :cond_2

    .line 263
    goto :goto_0

    .line 265
    :cond_2
    invoke-interface {p2}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "tagName":Ljava/lang/String;
    const-string v3, "package"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 267
    const/4 v3, 0x0

    const-string v4, "name"

    invoke-interface {p2, v3, v4}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 268
    .local v3, "pkgName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 269
    sget-object v4, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    const-string v5, "read block-update config pkgName is null"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    goto :goto_2

    .line 272
    :cond_3
    invoke-interface {p1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 273
    .end local v3    # "pkgName":Ljava/lang/String;
    goto :goto_1

    .line 274
    :cond_4
    sget-object v3, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown element under <packages>: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 275
    invoke-interface {p2}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 274
    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-static {p2}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 278
    .end local v1    # "tagName":Ljava/lang/String;
    :goto_1
    goto :goto_0

    .line 279
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mNotSupportUpdateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 280
    :try_start_0
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mCloudNotSupportUpdateSystemApps:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 281
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mCloudNotSupportUpdateSystemApps:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 282
    monitor-exit v1

    .line 283
    return-void

    .line 282
    :catchall_0
    move-exception v3

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private writeCloudSetting()V
    .locals 10

    .line 153
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 154
    .local v0, "startTime":J
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 155
    .local v2, "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mNotSupportUpdateLock:Ljava/lang/Object;

    monitor-enter v3

    .line 156
    :try_start_0
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mCloudNotSupportUpdateSystemApps:Ljava/util/Set;

    invoke-interface {v2, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 157
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 159
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->getCloudSettingsFile()Lcom/android/server/pm/ResilientAtomicFile;

    move-result-object v3

    .line 160
    .local v3, "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    const/4 v4, 0x0

    .line 162
    .local v4, "str":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v5

    move-object v4, v5

    .line 164
    invoke-static {v4}, Landroid/util/Xml;->resolveSerializer(Ljava/io/OutputStream;)Lcom/android/modules/utils/TypedXmlSerializer;

    move-result-object v5

    .line 165
    .local v5, "serializer":Lcom/android/modules/utils/TypedXmlSerializer;
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v5, v8, v7}, Lcom/android/modules/utils/TypedXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 166
    const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v5, v7, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 169
    const-string v6, "package-cloudconfig"

    invoke-interface {v5, v8, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 170
    const-string v6, "packages"

    invoke-interface {v5, v8, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 171
    const-string/jumbo v6, "version"

    iget v7, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I

    invoke-interface {v5, v8, v6, v7}, Lcom/android/modules/utils/TypedXmlSerializer;->attributeInt(Ljava/lang/String;Ljava/lang/String;I)Lorg/xmlpull/v1/XmlSerializer;

    .line 173
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 174
    .local v7, "packageName":Ljava/lang/String;
    const-string v9, "package"

    invoke-interface {v5, v8, v9}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 175
    const-string v9, "name"

    invoke-interface {v5, v8, v9, v7}, Lcom/android/modules/utils/TypedXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 176
    const-string v9, "package"

    invoke-interface {v5, v8, v9}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 177
    nop

    .end local v7    # "packageName":Ljava/lang/String;
    goto :goto_0

    .line 179
    :cond_0
    const-string v6, "packages"

    invoke-interface {v5, v8, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 180
    const-string v6, "package-cloudconfig"

    invoke-interface {v5, v8, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 181
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlSerializer;->endDocument()V

    .line 183
    invoke-virtual {v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    .line 185
    const-string v6, "package_cloud"

    .line 186
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v0

    .line 185
    invoke-static {v6, v7, v8}, Lcom/android/internal/logging/EventLogTags;->writeCommitSysConfigFile(Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    .end local v5    # "serializer":Lcom/android/modules/utils/TypedXmlSerializer;
    goto :goto_1

    .line 159
    .end local v4    # "str":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v4

    goto :goto_2

    .line 188
    .restart local v4    # "str":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v5

    .line 189
    .local v5, "e":Ljava/io/IOException;
    const/4 v6, 0x0

    :try_start_2
    iput v6, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I

    .line 190
    sget-object v6, Lcom/android/server/pm/PackageManagerCloudHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to write package manager cloud settings: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    if-eqz v4, :cond_1

    .line 192
    invoke-virtual {v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 195
    .end local v4    # "str":Ljava/io/FileOutputStream;
    .end local v5    # "e":Ljava/io/IOException;
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->close()V

    .line 196
    .end local v3    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    :cond_2
    return-void

    .line 159
    .restart local v3    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    :goto_2
    if-eqz v3, :cond_3

    :try_start_3
    invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v5

    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :cond_3
    :goto_3
    throw v4

    .line 157
    .end local v3    # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
    :catchall_2
    move-exception v4

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v4
.end method


# virtual methods
.method getCloudNotSupportUpdateSystemApps()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mCloudNotSupportUpdateSystemApps:Ljava/util/Set;

    return-object v0
.end method

.method registerCloudDataObserver()V
    .locals 4

    .line 70
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 71
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/pm/PackageManagerCloudHelper$1;

    iget-object v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/pm/PackageManagerCloudHelper$1;-><init>(Lcom/android/server/pm/PackageManagerCloudHelper;Landroid/os/Handler;)V

    .line 70
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 84
    return-void
.end method
