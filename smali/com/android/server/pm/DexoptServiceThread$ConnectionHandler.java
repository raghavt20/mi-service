class com.android.server.pm.DexoptServiceThread$ConnectionHandler implements java.lang.Runnable {
	 /* .source "DexoptServiceThread.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/DexoptServiceThread; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ConnectionHandler" */
} // .end annotation
/* # instance fields */
private java.io.InputStreamReader mInput;
private Boolean mIsContinue;
private android.net.LocalSocket mSocket;
final com.android.server.pm.DexoptServiceThread this$0; //synthetic
/* # direct methods */
public com.android.server.pm.DexoptServiceThread$ConnectionHandler ( ) {
/* .locals 0 */
/* .param p2, "clientSocket" # Landroid/net/LocalSocket; */
/* .line 86 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 84 */
int p1 = 1; // const/4 p1, 0x1
/* iput-boolean p1, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mIsContinue:Z */
/* .line 85 */
int p1 = 0; // const/4 p1, 0x0
this.mInput = p1;
/* .line 87 */
this.mSocket = p2;
/* .line 88 */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 11 */
/* .line 95 */
final String v0 = "DexoptServiceThread"; // const-string v0, "DexoptServiceThread"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mi_dexopt new connection:"; // const-string v2, "mi_dexopt new connection:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSocket;
(( android.net.LocalSocket ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 96 */
int v0 = 0; // const/4 v0, 0x0
/* .line 98 */
/* .local v0, "bufferedReader":Ljava/io/BufferedReader; */
} // :goto_0
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mIsContinue:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
	 /* .line 99 */
	 /* new-instance v1, Ljava/io/InputStreamReader; */
	 v2 = this.mSocket;
	 (( android.net.LocalSocket ) v2 ).getInputStream ( ); // invoke-virtual {v2}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
	 /* invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
	 this.mInput = v1;
	 /* .line 100 */
	 /* new-instance v1, Ljava/io/BufferedReader; */
	 v2 = this.mInput;
	 /* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
	 /* move-object v0, v1 */
	 /* .line 101 */
	 (( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
	 /* .line 102 */
	 /* .local v1, "data":Ljava/lang/String; */
	 final String v2 = ":"; // const-string v2, ":"
	 (( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
	 /* .line 103 */
	 /* .local v2, "temp":[Ljava/lang/String; */
	 /* array-length v3, v2 */
	 /* .line 104 */
	 /* .local v3, "len":I */
	 int v4 = 0; // const/4 v4, 0x0
	 /* .line 105 */
	 /* .local v4, "secondaryId":I */
	 int v5 = -1; // const/4 v5, -0x1
	 /* .line 106 */
	 /* .local v5, "result":I */
	 int v6 = 0; // const/4 v6, 0x0
	 /* aget-object v6, v2, v6 */
	 /* .line 107 */
	 /* .local v6, "packageName":Ljava/lang/String; */
	 int v7 = 3; // const/4 v7, 0x3
	 int v8 = 1; // const/4 v8, 0x1
	 /* if-ne v3, v7, :cond_0 */
	 /* .line 108 */
	 /* aget-object v7, v2, v8 */
	 (( java.lang.String ) v7 ).trim ( ); // invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
	 v7 = 	 java.lang.Integer .parseInt ( v7 );
	 /* move v4, v7 */
	 /* .line 109 */
	 int v7 = 2; // const/4 v7, 0x2
	 /* aget-object v7, v2, v7 */
	 (( java.lang.String ) v7 ).trim ( ); // invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
	 v7 = 	 java.lang.Integer .parseInt ( v7 );
	 /* move v5, v7 */
	 /* .line 111 */
} // :cond_0
/* aget-object v7, v2, v8 */
(( java.lang.String ) v7 ).trim ( ); // invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
v7 = java.lang.Integer .parseInt ( v7 );
/* move v5, v7 */
/* .line 113 */
} // :goto_1
com.android.server.pm.DexoptServiceThread .-$$Nest$sfgetmWaitLock ( );
/* monitor-enter v7 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 114 */
/* if-nez v6, :cond_1 */
/* .line 115 */
try { // :try_start_1
final String v8 = "DexoptServiceThread"; // const-string v8, "DexoptServiceThread"
final String v9 = "Received packageName error"; // const-string v9, "Received packageName error"
android.util.Slog .e ( v8,v9 );
/* .line 116 */
/* monitor-exit v7 */
/* .line 135 */
/* :catchall_0 */
/* move-exception v8 */
/* goto/16 :goto_3 */
/* .line 117 */
} // :cond_1
/* if-nez v4, :cond_2 */
/* .line 118 */
v8 = this.this$0;
v8 = com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptPackageNameList ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 119 */
v8 = this.this$0;
com.android.server.pm.DexoptServiceThread .-$$Nest$fputmDexoptResult ( v8,v5 );
/* .line 120 */
v8 = this.this$0;
com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptPackageNameList ( v8 );
/* .line 121 */
final String v8 = "DexoptServiceThread"; // const-string v8, "DexoptServiceThread"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "packageName : "; // const-string v10, "packageName : "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = " result : "; // const-string v10, " result : "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.this$0;
v10 = com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptResult ( v10 );
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " finished, notify next."; // const-string v10, " finished, notify next."
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v9 );
/* .line 123 */
com.android.server.pm.DexoptServiceThread .-$$Nest$sfgetmWaitLock ( );
(( java.lang.Object ) v8 ).notifyAll ( ); // invoke-virtual {v8}, Ljava/lang/Object;->notifyAll()V
/* .line 125 */
} // :cond_2
/* if-lez v4, :cond_3 */
/* .line 126 */
v8 = this.this$0;
v8 = this.mDexoptSecondaryPath;
java.lang.Integer .valueOf ( v4 );
v8 = (( java.util.concurrent.ConcurrentHashMap ) v8 ).containsKey ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 127 */
v8 = this.this$0;
com.android.server.pm.DexoptServiceThread .-$$Nest$fputmDexoptSecondaryResult ( v8,v5 );
/* .line 128 */
v8 = this.this$0;
v8 = this.mDexoptSecondaryPath;
java.lang.Integer .valueOf ( v4 );
(( java.util.concurrent.ConcurrentHashMap ) v8 ).remove ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 129 */
final String v8 = "DexoptServiceThread"; // const-string v8, "DexoptServiceThread"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "packageName : "; // const-string v10, "packageName : "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = " secondaryId : "; // const-string v10, " secondaryId : "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " result : "; // const-string v10, " result : "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.this$0;
v10 = com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptSecondaryResult ( v10 );
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " finished, notify next."; // const-string v10, " finished, notify next."
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v9 );
/* .line 132 */
com.android.server.pm.DexoptServiceThread .-$$Nest$sfgetmWaitLock ( );
(( java.lang.Object ) v8 ).notifyAll ( ); // invoke-virtual {v8}, Ljava/lang/Object;->notifyAll()V
/* .line 135 */
} // :cond_3
} // :goto_2
/* monitor-exit v7 */
/* .line 136 */
} // .end local v1 # "data":Ljava/lang/String;
} // .end local v2 # "temp":[Ljava/lang/String;
} // .end local v3 # "len":I
} // .end local v4 # "secondaryId":I
} // .end local v5 # "result":I
} // .end local v6 # "packageName":Ljava/lang/String;
/* goto/16 :goto_0 */
/* .line 135 */
/* .restart local v1 # "data":Ljava/lang/String; */
/* .restart local v2 # "temp":[Ljava/lang/String; */
/* .restart local v3 # "len":I */
/* .restart local v4 # "secondaryId":I */
/* .restart local v5 # "result":I */
/* .restart local v6 # "packageName":Ljava/lang/String; */
} // :goto_3
/* monitor-exit v7 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "bufferedReader":Ljava/io/BufferedReader;
} // .end local p0 # "this":Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;
try { // :try_start_2
/* throw v8 */
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 144 */
} // .end local v1 # "data":Ljava/lang/String;
} // .end local v2 # "temp":[Ljava/lang/String;
} // .end local v3 # "len":I
} // .end local v4 # "secondaryId":I
} // .end local v5 # "result":I
} // .end local v6 # "packageName":Ljava/lang/String;
/* .restart local v0 # "bufferedReader":Ljava/io/BufferedReader; */
/* .restart local p0 # "this":Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler; */
} // :cond_4
if ( v0 != null) { // if-eqz v0, :cond_5
try { // :try_start_3
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 145 */
/* :catch_0 */
/* move-exception v1 */
/* .line 148 */
/* .line 147 */
} // :cond_5
} // :goto_4
/* .line 143 */
/* :catchall_1 */
/* move-exception v1 */
/* .line 139 */
/* :catch_1 */
/* move-exception v1 */
/* .line 140 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_4
(( com.android.server.pm.DexoptServiceThread$ConnectionHandler ) p0 ).terminate ( ); // invoke-virtual {p0}, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->terminate()V
/* .line 141 */
final String v2 = "DexoptServiceThread"; // const-string v2, "DexoptServiceThread"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mi_dexopt connection:"; // const-string v4, "mi_dexopt connection:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).toString ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " Exception"; // const-string v4, " Exception"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 144 */
} // .end local v1 # "e":Ljava/lang/Exception;
if ( v0 != null) { // if-eqz v0, :cond_5
try { // :try_start_5
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 137 */
/* :catch_2 */
/* move-exception v1 */
/* .line 138 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_6
final String v2 = "DexoptServiceThread"; // const-string v2, "DexoptServiceThread"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mi_dexopt connection:"; // const-string v4, "mi_dexopt connection:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).toString ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " IOException"; // const-string v4, " IOException"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 144 */
} // .end local v1 # "e":Ljava/io/IOException;
if ( v0 != null) { // if-eqz v0, :cond_5
try { // :try_start_7
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_0 */
/* .line 149 */
} // :goto_5
return;
/* .line 144 */
} // :goto_6
if ( v0 != null) { // if-eqz v0, :cond_6
try { // :try_start_8
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_3 */
/* .line 145 */
/* :catch_3 */
/* move-exception v2 */
/* .line 147 */
} // :cond_6
} // :goto_7
/* nop */
/* .line 148 */
} // :goto_8
/* throw v1 */
} // .end method
public void terminate ( ) {
/* .locals 2 */
/* .line 90 */
final String v0 = "DexoptServiceThread"; // const-string v0, "DexoptServiceThread"
final String v1 = "dexopt trigger terminate!"; // const-string v1, "dexopt trigger terminate!"
android.util.Slog .d ( v0,v1 );
/* .line 91 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mIsContinue:Z */
/* .line 92 */
return;
} // .end method
