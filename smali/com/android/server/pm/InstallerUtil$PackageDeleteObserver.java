class com.android.server.pm.InstallerUtil$PackageDeleteObserver extends android.content.pm.IPackageDeleteObserver2$Stub {
	 /* .source "InstallerUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/InstallerUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "PackageDeleteObserver" */
} // .end annotation
/* # instance fields */
Boolean finished;
Boolean result;
/* # direct methods */
private com.android.server.pm.InstallerUtil$PackageDeleteObserver ( ) {
/* .locals 0 */
/* .line 49 */
/* invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver2$Stub;-><init>()V */
return;
} // .end method
 com.android.server.pm.InstallerUtil$PackageDeleteObserver ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onPackageDeleted ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "returnCode" # I */
/* .param p3, "msg" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 60 */
/* monitor-enter p0 */
/* .line 61 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* iput-boolean v0, p0, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;->finished:Z */
/* .line 62 */
/* if-ne p2, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;->result:Z */
/* .line 63 */
(( java.lang.Object ) p0 ).notifyAll ( ); // invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
/* .line 64 */
/* monitor-exit p0 */
/* .line 65 */
return;
/* .line 64 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void onUserActionRequired ( android.content.Intent p0 ) {
/* .locals 0 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 55 */
return;
} // .end method
