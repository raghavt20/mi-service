public class com.android.server.pm.PackageEventRecorder implements com.android.server.pm.PackageEventRecorderInternal {
	 /* .source "PackageEventRecorder.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;, */
	 /* Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;, */
	 /* Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;, */
	 /* Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;, */
	 /* Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;, */
	 /* Lcom/android/server/pm/PackageEventRecorder$RecorderHandler; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Object ATTR_DELIMITER;
private static final java.lang.String ATTR_EVENT_TIME_MILLIS;
private static final java.lang.String ATTR_ID;
private static final java.lang.String ATTR_INSTALLER;
private static final java.lang.String ATTR_IS_REMOVED_FULLY;
private static final java.lang.String ATTR_PACKAGE_NAME;
private static final java.lang.String ATTR_TYPE;
private static final java.lang.String ATTR_USER_IDS;
private static final Object ATTR_USER_IDS_DELIMITER;
private static final Object ATTR_VALUE_DELIMITER;
private static final java.lang.String BUNDLE_KEY_RECORDS;
private static final Boolean DEBUG;
private static final Integer ERROR_PENDING_ADDED_NUM;
private static final Integer ERROR_PENDING_DELETED_NUM;
private static final Long MAX_DELAY_TIME_MILLIS;
private static final Long MAX_FILE_SIZE;
private static final Integer MAX_NUM_ACTIVATION_RECORDS;
private static final Integer MAX_NUM_RECORDS_READ_ONCE;
private static final Integer MESSAGE_DELETE_RECORDS;
private static final Integer MESSAGE_WRITE_FILE;
private static final Long SCHEDULE_DELAY_TIME_MILLIS;
private static final java.lang.String TAG;
public static final Integer TYPE_FIRST_LAUNCH;
public static final Integer TYPE_REMOVE;
public static final Integer TYPE_UPDATE;
private static final java.util.List VALID_TYPES;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final Integer WARN_PENDING_ADDED_NUM;
private static final Integer WARN_PENDING_DELETED_NUM;
/* # instance fields */
private final java.util.LinkedList mActivationRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.util.SparseArray mAllPendingAddedEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private final android.util.SparseArray mAllPendingDeletedEvents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
Boolean mCheckCalling;
final android.os.Handler mHandler;
private Long mLastScheduledTimeMillis;
private final android.util.SparseArray mTypeToFile;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$mdeleteEventRecordsLocked ( com.android.server.pm.PackageEventRecorder p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->deleteEventRecordsLocked(I)V */
return;
} // .end method
static java.lang.Object -$$Nest$mgetLock ( com.android.server.pm.PackageEventRecorder p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object; */
} // .end method
static void -$$Nest$mwriteAppendLocked ( com.android.server.pm.PackageEventRecorder p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->writeAppendLocked(I)V */
return;
} // .end method
static void -$$Nest$smprintLogWhileResolveTxt ( Integer p0, java.lang.String p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.pm.PackageEventRecorder .printLogWhileResolveTxt ( p0,p1,p2 );
return;
} // .end method
static void -$$Nest$smprintLogWhileResolveTxt ( Integer p0, java.lang.String p1, Integer p2, java.lang.Throwable p3 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.pm.PackageEventRecorder .printLogWhileResolveTxt ( p0,p1,p2,p3 );
return;
} // .end method
static com.android.server.pm.PackageEventRecorder ( ) {
/* .locals 2 */
/* .line 47 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 50 */
int v1 = 1; // const/4 v1, 0x1
java.lang.Integer .valueOf ( v1 );
/* .line 51 */
int v1 = 2; // const/4 v1, 0x2
java.lang.Integer .valueOf ( v1 );
/* .line 52 */
int v1 = 3; // const/4 v1, 0x3
java.lang.Integer .valueOf ( v1 );
/* .line 53 */
return;
} // .end method
 com.android.server.pm.PackageEventRecorder ( ) {
/* .locals 4 */
/* .param p1, "dir" # Ljava/io/File; */
/* .param p3, "checkCalling" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/io/File;", */
/* "Ljava/util/function/Function<", */
/* "Lcom/android/server/pm/PackageEventRecorder;", */
/* "Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 98 */
/* .local p2, "handlerSuppler":Ljava/util/function/Function;, "Ljava/util/function/Function<Lcom/android/server/pm/PackageEventRecorder;Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 55 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mTypeToFile = v0;
/* .line 57 */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
this.mAllPendingAddedEvents = v1;
/* .line 61 */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
this.mAllPendingDeletedEvents = v1;
/* .line 135 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J */
/* .line 834 */
/* new-instance v1, Ljava/util/LinkedList; */
/* invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V */
this.mActivationRecords = v1;
/* .line 99 */
v1 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
/* .line 100 */
(( java.io.File ) p1 ).mkdir ( ); // invoke-virtual {p1}, Ljava/io/File;->mkdir()Z
/* .line 102 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "active-events.txt"; // const-string v2, "active-events.txt"
/* invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
int v2 = 1; // const/4 v2, 0x1
(( android.util.SparseArray ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 103 */
/* new-instance v1, Ljava/io/File; */
/* const-string/jumbo v2, "update-events.txt" */
/* invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
int v2 = 2; // const/4 v2, 0x2
(( android.util.SparseArray ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 104 */
/* new-instance v1, Ljava/io/File; */
final String v2 = "remove-events.txt"; // const-string v2, "remove-events.txt"
/* invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
int v2 = 3; // const/4 v2, 0x3
(( android.util.SparseArray ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 105 */
v0 = com.android.server.pm.PackageEventRecorder.VALID_TYPES;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 106 */
/* .local v1, "type":I */
v2 = this.mAllPendingAddedEvents;
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
(( android.util.SparseArray ) v2 ).put ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 107 */
v2 = this.mAllPendingDeletedEvents;
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
(( android.util.SparseArray ) v2 ).put ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 108 */
} // .end local v1 # "type":I
/* .line 109 */
} // :cond_1
/* iput-boolean p3, p0, Lcom/android/server/pm/PackageEventRecorder;->mCheckCalling:Z */
/* .line 110 */
/* check-cast v0, Landroid/os/Handler; */
this.mHandler = v0;
/* .line 111 */
return;
} // .end method
private Boolean checkCallingPackage ( ) {
/* .locals 8 */
/* .line 566 */
final String v0 = "PackageEventRecorder"; // const-string v0, "PackageEventRecorder"
/* iget-boolean v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mCheckCalling:Z */
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_0 */
/* .line 567 */
/* .line 572 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
v4 = android.os.Binder .getCallingUid ( );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 576 */
/* .local v3, "pkgNames":[Ljava/lang/String; */
/* nop */
/* .line 577 */
/* if-nez v3, :cond_1 */
/* .line 578 */
/* .line 581 */
} // :cond_1
/* array-length v4, v3 */
/* move v5, v1 */
} // :goto_0
/* if-ge v5, v4, :cond_3 */
/* aget-object v6, v3, v5 */
/* .line 582 */
/* .local v6, "pkgName":Ljava/lang/String; */
final String v7 = "com.miui.analytics"; // const-string v7, "com.miui.analytics"
v7 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 581 */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v5, v5, 0x1 */
/* .line 584 */
} // :cond_3
final String v2 = "open only to com.miui.analytics now"; // const-string v2, "open only to com.miui.analytics now"
android.util.Slog .w ( v0,v2 );
/* .line 585 */
/* .line 573 */
} // .end local v3 # "pkgNames":[Ljava/lang/String;
/* :catch_0 */
/* move-exception v2 */
/* .line 574 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "fail to get package names for uid "; // const-string v4, "fail to get package names for uid "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3,v2 );
/* .line 575 */
} // .end method
private Boolean commitAddedEvents ( com.android.server.pm.PackageEventRecorder$PackageEventBase p0 ) {
/* .locals 6 */
/* .param p1, "packageEvent" # Lcom/android/server/pm/PackageEventRecorder$PackageEventBase; */
/* .line 532 */
v0 = (( com.android.server.pm.PackageEventRecorder$PackageEventBase ) p1 ).isValid ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->isValid()Z
/* if-nez v0, :cond_0 */
/* .line 533 */
final String v0 = "PackageEventRecorder"; // const-string v0, "PackageEventRecorder"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "invalid package event "; // const-string v2, "invalid package event "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " ,reject to write to file"; // const-string v2, " ,reject to write to file"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 535 */
int v0 = 0; // const/4 v0, 0x0
/* .line 538 */
} // :cond_0
v0 = com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$mgetEventType ( p1 );
/* invoke-direct {p0, v0}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object; */
/* monitor-enter v0 */
/* .line 539 */
try { // :try_start_0
v1 = com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$mgetEventType ( p1 );
/* invoke-direct {p0, v1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List; */
/* .line 540 */
v2 = /* .local v1, "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;" */
int v3 = 1; // const/4 v3, 0x1
/* add-int/2addr v2, v3 */
/* const/16 v4, 0x1388 */
/* if-lt v2, v4, :cond_1 */
/* .line 541 */
final String v2 = "PackageEventRecorder"; // const-string v2, "PackageEventRecorder"
/* const-string/jumbo v4, "too many pending added package events in memory, clear it" */
android.util.Slog .e ( v2,v4 );
/* .line 542 */
/* .line 543 */
v2 = } // :cond_1
/* const/16 v4, 0x9c4 */
/* if-lt v2, v4, :cond_2 */
/* .line 544 */
final String v2 = "PackageEventRecorder"; // const-string v2, "PackageEventRecorder"
/* const-string/jumbo v4, "too many pending added package events in memory, please try later" */
android.util.Slog .e ( v2,v4 );
/* .line 547 */
} // :cond_2
} // :goto_0
v2 = this.mHandler;
v4 = com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$mgetEventType ( p1 );
java.lang.Integer .valueOf ( v4 );
(( android.os.Handler ) v2 ).removeMessages ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
/* .line 548 */
/* .line 549 */
/* nop */
} // .end local v1 # "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 550 */
v0 = this.mHandler;
v1 = com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$mgetEventType ( p1 );
java.lang.Integer .valueOf ( v1 );
(( android.os.Handler ) v0 ).obtainMessage ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 551 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mHandler;
/* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->getNextScheduledTime()J */
/* move-result-wide v4 */
(( android.os.Handler ) v1 ).sendMessageAtTime ( v0, v4, v5 ); // invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z
/* .line 552 */
/* .line 549 */
} // .end local v0 # "message":Landroid/os/Message;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void deleteEventRecordsLocked ( Integer p0 ) {
/* .locals 12 */
/* .param p1, "type" # I */
/* .line 402 */
final String v0 = "PackageEventRecorder"; // const-string v0, "PackageEventRecorder"
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingDeletedEvents(I)Ljava/util/List; */
/* .line 403 */
v2 = /* .local v1, "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 404 */
return;
/* .line 407 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* .line 410 */
/* .local v2, "start":J */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getReadFileLocked(I)Ljava/io/File; */
/* .line 411 */
/* .local v4, "targetFile":Ljava/io/File; */
/* if-nez v4, :cond_1 */
/* .line 412 */
return;
/* .line 415 */
} // :cond_1
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 416 */
/* .local v5, "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
try { // :try_start_0
/* new-instance v6, Ljava/io/BufferedReader; */
/* new-instance v7, Ljava/io/FileReader; */
/* invoke-direct {v7, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 419 */
/* .local v6, "bufferedReader":Ljava/io/BufferedReader; */
int v7 = 0; // const/4 v7, 0x0
/* .line 420 */
/* .local v7, "lineNum":I */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v6 ).readLine ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v9, v8 */
/* .local v9, "line":Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 421 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 422 */
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$smresolveIdFromRecord ( v9 );
/* .line 423 */
/* .local v8, "id":Ljava/lang/String; */
v10 = android.text.TextUtils .isEmpty ( v8 );
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 424 */
final String v10 = "fail to resolve attr id "; // const-string v10, "fail to resolve attr id "
int v11 = 6; // const/4 v11, 0x6
com.android.server.pm.PackageEventRecorder .printLogWhileResolveTxt ( v11,v10,v7 );
/* .line 425 */
/* .line 427 */
v10 = } // :cond_2
/* if-nez v10, :cond_3 */
/* .line 428 */
(( java.util.ArrayList ) v5 ).add ( v9 ); // invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 430 */
} // .end local v8 # "id":Ljava/lang/String;
} // :cond_3
/* .line 431 */
} // .end local v7 # "lineNum":I
} // .end local v9 # "line":Ljava/lang/String;
} // :cond_4
try { // :try_start_2
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 434 */
} // .end local v6 # "bufferedReader":Ljava/io/BufferedReader;
/* nop */
/* .line 436 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFileLocked(I)Ljava/io/File; */
/* .line 437 */
/* if-nez v4, :cond_5 */
/* .line 438 */
return;
/* .line 441 */
} // :cond_5
try { // :try_start_3
/* new-instance v6, Ljava/io/BufferedWriter; */
/* new-instance v7, Ljava/io/FileWriter; */
/* invoke-direct {v7, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V */
/* invoke-direct {v6, v7}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 443 */
/* .local v6, "bufferedWriter":Ljava/io/BufferedWriter; */
try { // :try_start_4
(( java.util.ArrayList ) v5 ).iterator ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v8 = } // :goto_1
if ( v8 != null) { // if-eqz v8, :cond_6
/* check-cast v8, Ljava/lang/String; */
/* .line 444 */
/* .local v8, "line":Ljava/lang/String; */
(( java.io.BufferedWriter ) v6 ).write ( v8 ); // invoke-virtual {v6, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 445 */
(( java.io.BufferedWriter ) v6 ).newLine ( ); // invoke-virtual {v6}, Ljava/io/BufferedWriter;->newLine()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 446 */
} // .end local v8 # "line":Ljava/lang/String;
/* .line 447 */
} // :cond_6
try { // :try_start_5
(( java.io.BufferedWriter ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 453 */
} // .end local v6 # "bufferedWriter":Ljava/io/BufferedWriter;
/* nop */
/* .line 455 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File; */
/* .line 456 */
/* .local v6, "backupFile":Ljava/io/File; */
v7 = (( java.io.File ) v6 ).delete ( ); // invoke-virtual {v6}, Ljava/io/File;->delete()Z
/* if-nez v7, :cond_7 */
/* .line 457 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "succeed to write to file " */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " ,but fail to delete backup file "; // const-string v8, " ,but fail to delete backup file "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 458 */
(( java.io.File ) v6 ).getAbsolutePath ( ); // invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 457 */
android.util.Slog .e ( v0,v7 );
/* .line 462 */
} // :cond_7
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List; */
/* .line 463 */
/* .local v7, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;" */
v8 = } // :goto_2
if ( v8 != null) { // if-eqz v8, :cond_8
/* .line 464 */
/* check-cast v8, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase; */
/* .line 465 */
/* .local v8, "packageEvent":Lcom/android/server/pm/PackageEventRecorder$PackageEventBase; */
v9 = com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$fgetid ( v8 );
if ( v9 != null) { // if-eqz v9, :cond_8
/* .line 466 */
/* .line 471 */
} // .end local v8 # "packageEvent":Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
/* .line 473 */
} // :cond_8
/* .line 475 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "cost "; // const-string v9, "cost "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v9 */
/* sub-long/2addr v9, v2 */
(( java.lang.StringBuilder ) v8 ).append ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = "ms in deleteEventRecordsLocked"; // const-string v9, "ms in deleteEventRecordsLocked"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v8 );
/* .line 477 */
return;
/* .line 441 */
} // .end local v7 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
/* .local v6, "bufferedWriter":Ljava/io/BufferedWriter; */
/* :catchall_0 */
/* move-exception v7 */
try { // :try_start_6
(( java.io.BufferedWriter ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* :catchall_1 */
/* move-exception v8 */
try { // :try_start_7
(( java.lang.Throwable ) v7 ).addSuppressed ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "start":J
} // .end local v4 # "targetFile":Ljava/io/File;
} // .end local v5 # "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local p0 # "this":Lcom/android/server/pm/PackageEventRecorder;
} // .end local p1 # "type":I
} // :goto_3
/* throw v7 */
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_0 */
/* .line 447 */
} // .end local v6 # "bufferedWriter":Ljava/io/BufferedWriter;
/* .restart local v1 # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .restart local v2 # "start":J */
/* .restart local v4 # "targetFile":Ljava/io/File; */
/* .restart local v5 # "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageEventRecorder; */
/* .restart local p1 # "type":I */
/* :catch_0 */
/* move-exception v6 */
/* .line 448 */
/* .local v6, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "fail to write to "; // const-string v8, "fail to write to "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7,v6 );
/* .line 449 */
v7 = (( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* if-nez v7, :cond_9 */
/* .line 450 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "fail to delete witten file "; // const-string v8, "fail to delete witten file "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7 );
/* .line 452 */
} // :cond_9
return;
/* .line 416 */
/* .local v6, "bufferedReader":Ljava/io/BufferedReader; */
/* :catchall_2 */
/* move-exception v7 */
try { // :try_start_8
(( java.io.BufferedReader ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_3 */
/* :catchall_3 */
/* move-exception v8 */
try { // :try_start_9
(( java.lang.Throwable ) v7 ).addSuppressed ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "start":J
} // .end local v4 # "targetFile":Ljava/io/File;
} // .end local v5 # "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local p0 # "this":Lcom/android/server/pm/PackageEventRecorder;
} // .end local p1 # "type":I
} // :goto_4
/* throw v7 */
/* :try_end_9 */
/* .catch Ljava/io/IOException; {:try_start_9 ..:try_end_9} :catch_1 */
/* .line 431 */
} // .end local v6 # "bufferedReader":Ljava/io/BufferedReader;
/* .restart local v1 # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .restart local v2 # "start":J */
/* .restart local v4 # "targetFile":Ljava/io/File; */
/* .restart local v5 # "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageEventRecorder; */
/* .restart local p1 # "type":I */
/* :catch_1 */
/* move-exception v6 */
/* .line 432 */
/* .local v6, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "fail to read from "; // const-string v8, "fail to read from "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v7,v6 );
/* .line 433 */
return;
} // .end method
private java.io.File getBackupFile ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .line 122 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File; */
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".backup"; // const-string v2, ".backup"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
} // .end method
private java.lang.Object getLock ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 114 */
v0 = this.mTypeToFile;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
} // .end method
private synchronized Long getNextScheduledTime ( ) {
/* .locals 7 */
/* monitor-enter p0 */
/* .line 142 */
try { // :try_start_0
/* iget-wide v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* const-wide/16 v1, 0x2710 */
/* if-gtz v0, :cond_0 */
/* .line 143 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* add-long/2addr v3, v1 */
/* iput-wide v3, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J */
/* .line 144 */
} // .end local p0 # "this":Lcom/android/server/pm/PackageEventRecorder;
} // :cond_0
/* iget-wide v3, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
/* sub-long/2addr v3, v5 */
/* const-wide/32 v5, 0xc350 */
/* cmp-long v0, v3, v5 */
/* if-gtz v0, :cond_1 */
/* .line 146 */
/* iget-wide v3, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J */
/* add-long/2addr v3, v1 */
/* iput-wide v3, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J */
/* .line 148 */
} // :cond_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* const-wide/32 v2, 0xea60 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J */
/* .line 150 */
} // :goto_0
/* iget-wide v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit p0 */
/* return-wide v0 */
/* .line 141 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private java.util.List getPendingAddedEvents ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 127 */
v0 = this.mAllPendingAddedEvents;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
} // .end method
private java.util.List getPendingDeletedEvents ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 132 */
v0 = this.mAllPendingDeletedEvents;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
} // .end method
private java.io.File getReadFileLocked ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "type" # I */
/* .line 267 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File; */
/* .line 268 */
/* .local v0, "writtenFile":Ljava/io/File; */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File; */
/* .line 270 */
/* .local v1, "backupFile":Ljava/io/File; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
final String v3 = "PackageEventRecorder"; // const-string v3, "PackageEventRecorder"
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 271 */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 272 */
v2 = (( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* if-nez v2, :cond_0 */
/* .line 273 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "fail to delete damaged file "; // const-string v5, "fail to delete damaged file "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 274 */
(( java.io.File ) v0 ).getAbsolutePath ( ); // invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 273 */
android.util.Slog .e ( v3,v2 );
/* .line 275 */
/* .line 277 */
} // :cond_0
/* .line 279 */
} // :cond_1
/* .line 282 */
} // :cond_2
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 283 */
/* .line 287 */
} // :cond_3
try { // :try_start_0
v2 = (( java.io.File ) v0 ).createNewFile ( ); // invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 291 */
/* .local v2, "success":Z */
/* nop */
/* .line 292 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* move-object v4, v0 */
} // :cond_4
/* .line 288 */
} // .end local v2 # "success":Z
/* :catch_0 */
/* move-exception v2 */
/* .line 289 */
/* .local v2, "e":Ljava/io/IOException; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "fail to create file "; // const-string v6, "fail to create file "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v0 ).getAbsolutePath ( ); // invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v5,v2 );
/* .line 290 */
} // .end method
private android.os.Bundle getRecordsLocked ( Integer p0 ) {
/* .locals 12 */
/* .param p1, "type" # I */
/* .line 313 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 315 */
/* .local v0, "start":J */
v2 = com.android.server.pm.PackageEventRecorder.VALID_TYPES;
v2 = java.lang.Integer .valueOf ( p1 );
final String v3 = "PackageEventRecorder"; // const-string v3, "PackageEventRecorder"
int v4 = 0; // const/4 v4, 0x0
/* if-nez v2, :cond_0 */
/* .line 316 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "invalid package event type "; // const-string v5, "invalid package event type "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v2 );
/* .line 317 */
/* .line 321 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getReadFileLocked(I)Ljava/io/File; */
/* .line 322 */
/* .local v2, "targetFile":Ljava/io/File; */
/* if-nez v2, :cond_1 */
/* .line 323 */
/* .line 326 */
} // :cond_1
/* new-instance v5, Landroid/os/Bundle; */
/* invoke-direct {v5}, Landroid/os/Bundle;-><init>()V */
/* .line 327 */
/* .local v5, "result":Landroid/os/Bundle; */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
/* .line 329 */
/* .local v6, "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;" */
try { // :try_start_0
/* new-instance v7, Ljava/io/BufferedReader; */
/* new-instance v8, Ljava/io/FileReader; */
/* invoke-direct {v8, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 332 */
/* .local v7, "bufferedReader":Ljava/io/BufferedReader; */
int v8 = 0; // const/4 v8, 0x0
/* .line 333 */
/* .local v8, "lineNum":I */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v7 ).readLine ( ); // invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v10, v9 */
/* .local v10, "line":Ljava/lang/String; */
/* const/16 v11, 0x32 */
if ( v9 != null) { // if-eqz v9, :cond_3
v9 = /* .line 334 */
/* if-gt v9, v11, :cond_3 */
/* .line 335 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 336 */
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$smbuildBundleFromRecord ( v10,v8,p1 );
/* .line 337 */
/* .local v9, "oneRecord":Landroid/os/Bundle; */
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 338 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 340 */
} // .end local v9 # "oneRecord":Landroid/os/Bundle;
} // :cond_2
/* .line 341 */
} // .end local v8 # "lineNum":I
} // .end local v10 # "line":Ljava/lang/String;
} // :cond_3
try { // :try_start_2
(( java.io.BufferedReader ) v7 ).close ( ); // invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 344 */
} // .end local v7 # "bufferedReader":Ljava/io/BufferedReader;
/* nop */
/* .line 347 */
int v3 = 0; // const/4 v3, 0x0
/* .line 348 */
/* .local v3, "i":I */
} // :goto_1
/* nop */
v4 = /* .line 347 */
/* add-int/2addr v4, v3 */
/* if-ge v4, v11, :cond_4 */
/* .line 348 */
v4 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List; */
/* if-ge v3, v4, :cond_4 */
/* .line 349 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List; */
/* check-cast v4, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase; */
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$mbuildBundle ( v4 );
/* .line 348 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 352 */
} // .end local v3 # "i":I
} // :cond_4
final String v3 = "packageEventRecords"; // const-string v3, "packageEventRecords"
(( android.os.Bundle ) v5 ).putParcelableList ( v3, v6 ); // invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putParcelableList(Ljava/lang/String;Ljava/util/List;)V
/* .line 356 */
/* .line 329 */
/* .restart local v7 # "bufferedReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v8 */
try { // :try_start_3
(( java.io.BufferedReader ) v7 ).close ( ); // invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v9 */
try { // :try_start_4
(( java.lang.Throwable ) v8 ).addSuppressed ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "start":J
} // .end local v2 # "targetFile":Ljava/io/File;
} // .end local v5 # "result":Landroid/os/Bundle;
} // .end local v6 # "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
} // .end local p0 # "this":Lcom/android/server/pm/PackageEventRecorder;
} // .end local p1 # "type":I
} // :goto_2
/* throw v8 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 341 */
} // .end local v7 # "bufferedReader":Ljava/io/BufferedReader;
/* .restart local v0 # "start":J */
/* .restart local v2 # "targetFile":Ljava/io/File; */
/* .restart local v5 # "result":Landroid/os/Bundle; */
/* .restart local v6 # "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;" */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageEventRecorder; */
/* .restart local p1 # "type":I */
/* :catch_0 */
/* move-exception v7 */
/* .line 342 */
/* .local v7, "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "fail to resolve records from "; // const-string v9, "fail to resolve records from "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v8,v7 );
/* .line 343 */
} // .end method
private java.io.File getWrittenFile ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .line 118 */
v0 = this.mTypeToFile;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/io/File; */
} // .end method
private java.io.File getWrittenFileLocked ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "type" # I */
/* .line 183 */
final String v0 = "fail to delete damaged file "; // const-string v0, "fail to delete damaged file "
final String v1 = "PackageEventRecorder"; // const-string v1, "PackageEventRecorder"
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File; */
/* .line 184 */
/* .local v2, "writtenFile":Ljava/io/File; */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File; */
/* .line 187 */
/* .local v3, "backupFile":Ljava/io/File; */
int v4 = 0; // const/4 v4, 0x0
try { // :try_start_0
v5 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 188 */
v5 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 190 */
v5 = (( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* if-nez v5, :cond_1 */
/* .line 191 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 192 */
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 191 */
android.util.Slog .e ( v1,v5 );
/* .line 193 */
/* .line 196 */
} // :cond_0
v5 = (( java.io.File ) v2 ).renameTo ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
/* if-nez v5, :cond_1 */
/* .line 197 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "fail to rename "; // const-string v6, "fail to rename "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " to "; // const-string v6, " to "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 198 */
(( java.io.File ) v3 ).getAbsolutePath ( ); // invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 197 */
android.util.Slog .e ( v1,v5 );
/* .line 199 */
/* .line 202 */
} // :cond_1
android.os.FileUtils .copy ( v3,v2 );
/* .line 204 */
} // :cond_2
v5 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 205 */
android.os.FileUtils .copy ( v3,v2 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 215 */
} // :cond_3
} // :goto_0
/* nop */
/* .line 216 */
/* .line 208 */
/* :catch_0 */
/* move-exception v5 */
/* .line 209 */
/* .local v5, "e":Ljava/io/IOException; */
final String v6 = "error happened in getWrittenFileLocked"; // const-string v6, "error happened in getWrittenFileLocked"
android.util.Slog .e ( v1,v6,v5 );
/* .line 210 */
v6 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v6 != null) { // if-eqz v6, :cond_4
v6 = (( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* if-nez v6, :cond_4 */
/* .line 211 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 212 */
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 211 */
android.util.Slog .e ( v1,v0,v5 );
/* .line 214 */
} // :cond_4
} // .end method
public static Boolean isEnabled ( ) {
/* .locals 1 */
/* .line 178 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
private static void printLogWhileResolveTxt ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 3 */
/* .param p0, "priority" # I */
/* .param p1, "message" # Ljava/lang/String; */
/* .param p2, "lineNum" # I */
/* .line 556 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " at line "; // const-string v1, " at line "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 3; // const/4 v1, 0x3
final String v2 = "PackageEventRecorder"; // const-string v2, "PackageEventRecorder"
android.util.Log .println_native ( v1,p0,v2,v0 );
/* .line 557 */
return;
} // .end method
private static void printLogWhileResolveTxt ( Integer p0, java.lang.String p1, Integer p2, java.lang.Throwable p3 ) {
/* .locals 3 */
/* .param p0, "priority" # I */
/* .param p1, "message" # Ljava/lang/String; */
/* .param p2, "lineNum" # I */
/* .param p3, "tr" # Ljava/lang/Throwable; */
/* .line 561 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " at line "; // const-string v1, " at line "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v1, 0xa */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 562 */
android.util.Log .getStackTraceString ( p3 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 561 */
int v1 = 3; // const/4 v1, 0x3
final String v2 = "PackageEventRecorder"; // const-string v2, "PackageEventRecorder"
android.util.Log .println_native ( v1,p0,v2,v0 );
/* .line 563 */
return;
} // .end method
static Boolean shouldRecordPackageActivate ( java.lang.String p0, java.lang.String p1, Integer p2, com.android.server.pm.pkg.PackageStateInternal p3 ) {
/* .locals 1 */
/* .param p0, "activatedPackage" # Ljava/lang/String; */
/* .param p1, "sourcePackage" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "pkgState" # Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .line 839 */
v0 = com.android.server.pm.PackageEventRecorder .isEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
v0 = /* .line 840 */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* .line 841 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 842 */
v0 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v0, :cond_0 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 839 */
} // :goto_0
} // .end method
private void writeAppendLocked ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "type" # I */
/* .line 221 */
final String v0 = "PackageEventRecorder"; // const-string v0, "PackageEventRecorder"
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List; */
/* .line 222 */
v2 = /* .local v1, "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 223 */
return;
/* .line 226 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* .line 228 */
/* .local v2, "start":J */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFileLocked(I)Ljava/io/File; */
/* .line 229 */
/* .local v4, "targetFile":Ljava/io/File; */
/* if-nez v4, :cond_1 */
/* .line 230 */
return;
/* .line 234 */
} // :cond_1
(( java.io.File ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/io/File;->length()J
/* move-result-wide v5 */
/* const-wide/32 v7, 0x500000 */
/* cmp-long v5, v5, v7 */
/* if-ltz v5, :cond_2 */
/* .line 235 */
(( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* .line 238 */
} // :cond_2
try { // :try_start_0
/* new-instance v5, Ljava/io/BufferedWriter; */
/* new-instance v6, Ljava/io/FileWriter; */
int v7 = 1; // const/4 v7, 0x1
/* invoke-direct {v6, v4, v7}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* invoke-direct {v5, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 241 */
/* .local v5, "bufferedWriter":Ljava/io/BufferedWriter; */
try { // :try_start_1
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_3
/* check-cast v7, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase; */
/* .line 242 */
/* .local v7, "packageEvent":Lcom/android/server/pm/PackageEventRecorder$PackageEventBase; */
(( com.android.server.pm.PackageEventRecorder$PackageEventBase ) v7 ).WriteToTxt ( v5 ); // invoke-virtual {v7, v5}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->WriteToTxt(Ljava/io/BufferedWriter;)V
/* .line 243 */
(( java.io.BufferedWriter ) v5 ).newLine ( ); // invoke-virtual {v5}, Ljava/io/BufferedWriter;->newLine()V
/* .line 244 */
} // .end local v7 # "packageEvent":Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
/* .line 245 */
} // :cond_3
(( java.io.BufferedWriter ) v5 ).flush ( ); // invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 246 */
try { // :try_start_2
(( java.io.BufferedWriter ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedWriter;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 252 */
} // .end local v5 # "bufferedWriter":Ljava/io/BufferedWriter;
/* nop */
/* .line 254 */
/* .line 255 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File; */
/* .line 256 */
/* .local v5, "backupFile":Ljava/io/File; */
v6 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
if ( v6 != null) { // if-eqz v6, :cond_4
v6 = (( java.io.File ) v5 ).delete ( ); // invoke-virtual {v5}, Ljava/io/File;->delete()Z
/* if-nez v6, :cond_4 */
/* .line 257 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "succeed to write to file " */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " ,but fail to delete backup file "; // const-string v7, " ,but fail to delete backup file "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 258 */
(( java.io.File ) v5 ).getAbsolutePath ( ); // invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 257 */
android.util.Slog .e ( v0,v6 );
/* .line 263 */
} // :cond_4
return;
/* .line 238 */
/* .local v5, "bufferedWriter":Ljava/io/BufferedWriter; */
/* :catchall_0 */
/* move-exception v6 */
try { // :try_start_3
(( java.io.BufferedWriter ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedWriter;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v7 */
try { // :try_start_4
(( java.lang.Throwable ) v6 ).addSuppressed ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
} // .end local v2 # "start":J
} // .end local v4 # "targetFile":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/pm/PackageEventRecorder;
} // .end local p1 # "type":I
} // :goto_1
/* throw v6 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 246 */
} // .end local v5 # "bufferedWriter":Ljava/io/BufferedWriter;
/* .restart local v1 # "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;" */
/* .restart local v2 # "start":J */
/* .restart local v4 # "targetFile":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageEventRecorder; */
/* .restart local p1 # "type":I */
/* :catch_0 */
/* move-exception v5 */
/* .line 247 */
/* .local v5, "e":Ljava/io/IOException; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "fail to write append to "; // const-string v7, "fail to write append to "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v6,v5 );
/* .line 248 */
v6 = (( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* if-nez v6, :cond_5 */
/* .line 249 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "fail to delete witten file "; // const-string v7, "fail to delete witten file "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v6 );
/* .line 251 */
} // :cond_5
return;
} // .end method
/* # virtual methods */
public void commitDeletedEvents ( Integer p0, java.util.List p1 ) {
/* .locals 6 */
/* .param p1, "type" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 375 */
/* .local p2, "eventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->checkCallingPackage()Z */
/* if-nez v0, :cond_0 */
/* .line 376 */
return;
/* .line 378 */
v0 = } // :cond_0
/* const/16 v1, 0x1770 */
/* if-lt v0, v1, :cond_1 */
/* .line 379 */
final String v0 = "PackageEventRecorder"; // const-string v0, "PackageEventRecorder"
final String v1 = "add too many deleted package events, abandon it"; // const-string v1, "add too many deleted package events, abandon it"
android.util.Slog .e ( v0,v1 );
/* .line 380 */
return;
/* .line 383 */
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object; */
/* monitor-enter v0 */
/* .line 384 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingDeletedEvents(I)Ljava/util/List; */
/* .line 385 */
v4 = v3 = /* .local v2, "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* add-int/2addr v3, v4 */
/* .line 386 */
/* .local v3, "total":I */
/* if-lt v3, v1, :cond_2 */
/* .line 387 */
final String v1 = "PackageEventRecorder"; // const-string v1, "PackageEventRecorder"
/* const-string/jumbo v4, "too many pending deleted package events in memory, clear it" */
android.util.Slog .e ( v1,v4 );
/* .line 388 */
/* .line 389 */
} // :cond_2
/* const/16 v1, 0xbb8 */
/* if-lt v3, v1, :cond_3 */
/* .line 390 */
final String v1 = "PackageEventRecorder"; // const-string v1, "PackageEventRecorder"
/* const-string/jumbo v4, "too many pending deleted package events in memory, please try later" */
android.util.Slog .e ( v1,v4 );
/* .line 393 */
} // :cond_3
} // :goto_0
v1 = this.mHandler;
java.lang.Integer .valueOf ( p1 );
int v5 = 2; // const/4 v5, 0x2
(( android.os.Handler ) v1 ).removeMessages ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
/* .line 394 */
/* .line 395 */
/* nop */
} // .end local v2 # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v3 # "total":I
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 396 */
v0 = this.mHandler;
java.lang.Integer .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v5, v1 ); // invoke-virtual {v0, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 397 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mHandler;
/* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->getNextScheduledTime()J */
/* move-result-wide v2 */
(( android.os.Handler ) v1 ).sendMessageAtTime ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z
/* .line 398 */
return;
/* .line 395 */
} // .end local v0 # "message":Landroid/os/Message;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean deleteAllEventRecords ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .line 360 */
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->checkCallingPackage()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 361 */
/* .line 364 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object; */
/* monitor-enter v0 */
/* .line 365 */
try { // :try_start_0
final String v2 = "PackageEventRecorder"; // const-string v2, "PackageEventRecorder"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "deleting all package event records, type "; // const-string v4, "deleting all package event records, type "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 366 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List; */
/* .line 367 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingDeletedEvents(I)Ljava/util/List; */
/* .line 368 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File; */
(( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* .line 369 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File; */
(( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* .line 370 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File; */
v2 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_1 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File; */
v2 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* monitor-exit v0 */
/* .line 371 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public android.os.Bundle getPackageEventRecords ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .line 302 */
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->checkCallingPackage()Z */
/* if-nez v0, :cond_0 */
/* .line 303 */
int v0 = 0; // const/4 v0, 0x0
/* .line 306 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object; */
/* monitor-enter v0 */
/* .line 307 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getRecordsLocked(I)Landroid/os/Bundle; */
/* monitor-exit v0 */
/* .line 308 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public android.os.Bundle getSourcePackage ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 6 */
/* .param p1, "activatedPackage" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "clear" # Z */
/* .line 854 */
v0 = com.android.server.pm.PackageEventRecorder .isEnabled ( );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_6
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 858 */
} // :cond_0
v0 = this.mActivationRecords;
(( java.util.LinkedList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;
/* .line 859 */
/* .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 860 */
/* .local v2, "sourcePackage":Ljava/lang/String; */
} // :cond_1
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 861 */
/* check-cast v3, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord; */
/* .line 862 */
/* .local v3, "item":Lcom/android/server/pm/PackageEventRecorder$ActivationRecord; */
v4 = this.activatedPackage;
v4 = (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* iget v4, v3, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;->userId:I */
/* if-eq p2, v4, :cond_2 */
/* .line 863 */
/* .line 866 */
} // :cond_2
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 867 */
/* .line 868 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "remove package first launch record : "; // const-string v5, "remove package first launch record : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "PackageEventRecorder"; // const-string v5, "PackageEventRecorder"
android.util.Log .d ( v5,v4 );
/* .line 870 */
} // :cond_3
v2 = this.sourcePackage;
/* .line 871 */
/* nop */
/* .line 873 */
} // .end local v3 # "item":Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;
} // :cond_4
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 874 */
/* .line 877 */
} // :cond_5
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 878 */
/* .local v1, "result":Landroid/os/Bundle; */
final String v3 = "activate_source"; // const-string v3, "activate_source"
(( android.os.Bundle ) v1 ).putString ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 879 */
/* .line 855 */
} // .end local v0 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;>;"
} // .end local v1 # "result":Landroid/os/Bundle;
} // .end local v2 # "sourcePackage":Ljava/lang/String;
} // :cond_6
} // :goto_1
} // .end method
public void recordPackageActivate ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "activatedPackage" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "sourcePackage" # Ljava/lang/String; */
/* .line 846 */
v0 = this.mActivationRecords;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* const/16 v1, 0x32 */
/* if-lt v0, v1, :cond_0 */
/* .line 847 */
v0 = this.mActivationRecords;
(( java.util.LinkedList ) v0 ).removeFirst ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
/* .line 849 */
} // :cond_0
v0 = this.mActivationRecords;
/* new-instance v1, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p1, p2, p3, v2}, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$ActivationRecord-IA;)V */
(( java.util.LinkedList ) v0 ).addLast ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V
/* .line 850 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "new package first launch record : "; // const-string v1, "new package first launch record : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mActivationRecords;
(( java.util.LinkedList ) v1 ).getLast ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PackageEventRecorder"; // const-string v1, "PackageEventRecorder"
android.util.Log .d ( v1,v0 );
/* .line 851 */
return;
} // .end method
public void recordPackageFirstLaunch ( Integer p0, java.lang.String p1, java.lang.String p2, android.content.Intent p3 ) {
/* .locals 8 */
/* .param p1, "userId" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "installer" # Ljava/lang/String; */
/* .param p4, "intent" # Landroid/content/Intent; */
/* .line 481 */
v0 = com.android.server.pm.PackageEventRecorder .isEnabled ( );
/* if-nez v0, :cond_0 */
/* .line 482 */
return;
/* .line 485 */
} // :cond_0
/* new-instance v0, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* filled-new-array {p1}, [I */
int v7 = 0; // const/4 v7, 0x0
/* move-object v1, v0 */
/* move-object v4, p2 */
/* move-object v6, p3 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent-IA;)V */
/* .line 487 */
/* .local v0, "event":Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent; */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/pm/PackageEventRecorder;->commitAddedEvents(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Z */
/* if-nez v1, :cond_1 */
/* .line 488 */
return;
/* .line 491 */
} // :cond_1
final String v1 = "miuiActiveId"; // const-string v1, "miuiActiveId"
(( com.android.server.pm.PackageEventRecorder$PackageFirstLaunchEvent ) v0 ).getId ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;->getId()Ljava/lang/String;
(( android.content.Intent ) p4 ).putExtra ( v1, v2 ); // invoke-virtual {p4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 492 */
(( com.android.server.pm.PackageEventRecorder$PackageFirstLaunchEvent ) v0 ).getId ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;->getId()Ljava/lang/String;
com.android.server.pm.PackageEventRecorder$PackageEventBase .resolveEventTimeMillis ( v1 );
/* move-result-wide v1 */
final String v3 = "miuiActiveTime"; // const-string v3, "miuiActiveTime"
(( android.content.Intent ) p4 ).putExtra ( v3, v1, v2 ); // invoke-virtual {p4, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 493 */
return;
} // .end method
public void recordPackageRemove ( Integer[] p0, java.lang.String p1, java.lang.String p2, Boolean p3, android.os.Bundle p4 ) {
/* .locals 13 */
/* .param p1, "userIds" # [I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "installer" # Ljava/lang/String; */
/* .param p4, "isRemovedFully" # Z */
/* .param p5, "extras" # Landroid/os/Bundle; */
/* .line 512 */
/* move-object/from16 v1, p5 */
v0 = com.android.server.pm.PackageEventRecorder .isEnabled ( );
/* if-nez v0, :cond_0 */
/* .line 513 */
return;
/* .line 517 */
} // :cond_0
/* const-class v2, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent; */
/* monitor-enter v2 */
/* .line 519 */
try { // :try_start_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* .line 520 */
/* .local v3, "eventTimeMillis":J */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 521 */
/* new-instance v0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent; */
int v12 = 0; // const/4 v12, 0x0
/* move-object v5, v0 */
/* move-wide v6, v3 */
/* move-object v8, p2 */
/* move-object v9, p1 */
/* move-object/from16 v10, p3 */
/* move/from16 v11, p4 */
/* invoke-direct/range {v5 ..v12}, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;ZLcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent-IA;)V */
/* .line 523 */
/* .local v0, "event":Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent; */
/* move-object v5, p0 */
v2 = /* invoke-direct {p0, v0}, Lcom/android/server/pm/PackageEventRecorder;->commitAddedEvents(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Z */
/* if-nez v2, :cond_1 */
/* .line 524 */
return;
/* .line 527 */
} // :cond_1
final String v2 = "miuiRemoveId"; // const-string v2, "miuiRemoveId"
(( com.android.server.pm.PackageEventRecorder$PackageRemoveEvent ) v0 ).getId ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->getId()Ljava/lang/String;
(( android.os.Bundle ) v1 ).putString ( v2, v6 ); // invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 528 */
final String v2 = "miuiRemoveTime"; // const-string v2, "miuiRemoveTime"
(( android.os.Bundle ) v1 ).putLong ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 529 */
return;
/* .line 520 */
} // .end local v0 # "event":Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;
} // .end local v3 # "eventTimeMillis":J
/* :catchall_0 */
/* move-exception v0 */
/* move-object v5, p0 */
} // :goto_0
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v0 */
/* :catchall_1 */
/* move-exception v0 */
} // .end method
public void recordPackageUpdate ( Integer[] p0, java.lang.String p1, java.lang.String p2, android.os.Bundle p3 ) {
/* .locals 8 */
/* .param p1, "userIds" # [I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "installer" # Ljava/lang/String; */
/* .param p4, "extras" # Landroid/os/Bundle; */
/* .line 496 */
v0 = com.android.server.pm.PackageEventRecorder .isEnabled ( );
/* if-nez v0, :cond_0 */
/* .line 497 */
return;
/* .line 500 */
} // :cond_0
/* new-instance v0, Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
int v7 = 0; // const/4 v7, 0x0
/* move-object v1, v0 */
/* move-object v4, p2 */
/* move-object v5, p1 */
/* move-object v6, p3 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent-IA;)V */
/* .line 502 */
/* .local v0, "event":Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent; */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/pm/PackageEventRecorder;->commitAddedEvents(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Z */
/* if-nez v1, :cond_1 */
/* .line 503 */
return;
/* .line 506 */
} // :cond_1
final String v1 = "miuiUpdateId"; // const-string v1, "miuiUpdateId"
(( com.android.server.pm.PackageEventRecorder$PackageUpdateEvent ) v0 ).getId ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;->getId()Ljava/lang/String;
(( android.os.Bundle ) p4 ).putString ( v1, v2 ); // invoke-virtual {p4, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 507 */
(( com.android.server.pm.PackageEventRecorder$PackageUpdateEvent ) v0 ).getId ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;->getId()Ljava/lang/String;
com.android.server.pm.PackageEventRecorder$PackageEventBase .resolveEventTimeMillis ( v1 );
/* move-result-wide v1 */
final String v3 = "miuiUpdateTime"; // const-string v3, "miuiUpdateTime"
(( android.os.Bundle ) p4 ).putLong ( v3, v1, v2 ); // invoke-virtual {p4, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 508 */
return;
} // .end method
