.class Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;
.super Ljava/lang/Object;
.source "DexoptServiceThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/DexoptServiceThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionHandler"
.end annotation


# instance fields
.field private mInput:Ljava/io/InputStreamReader;

.field private mIsContinue:Z

.field private mSocket:Landroid/net/LocalSocket;

.field final synthetic this$0:Lcom/android/server/pm/DexoptServiceThread;


# direct methods
.method public constructor <init>(Lcom/android/server/pm/DexoptServiceThread;Landroid/net/LocalSocket;)V
    .locals 0
    .param p2, "clientSocket"    # Landroid/net/LocalSocket;

    .line 86
    iput-object p1, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mIsContinue:Z

    .line 85
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mInput:Ljava/io/InputStreamReader;

    .line 87
    iput-object p2, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    .line 88
    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .line 95
    const-string v0, "DexoptServiceThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mi_dexopt new connection:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v0, 0x0

    .line 98
    .local v0, "bufferedReader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mIsContinue:Z

    if-eqz v1, :cond_4

    .line 99
    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mInput:Ljava/io/InputStreamReader;

    .line 100
    new-instance v1, Ljava/io/BufferedReader;

    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mInput:Ljava/io/InputStreamReader;

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v0, v1

    .line 101
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "data":Ljava/lang/String;
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 103
    .local v2, "temp":[Ljava/lang/String;
    array-length v3, v2

    .line 104
    .local v3, "len":I
    const/4 v4, 0x0

    .line 105
    .local v4, "secondaryId":I
    const/4 v5, -0x1

    .line 106
    .local v5, "result":I
    const/4 v6, 0x0

    aget-object v6, v2, v6

    .line 107
    .local v6, "packageName":Ljava/lang/String;
    const/4 v7, 0x3

    const/4 v8, 0x1

    if-ne v3, v7, :cond_0

    .line 108
    aget-object v7, v2, v8

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move v4, v7

    .line 109
    const/4 v7, 0x2

    aget-object v7, v2, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move v5, v7

    goto :goto_1

    .line 111
    :cond_0
    aget-object v7, v2, v8

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move v5, v7

    .line 113
    :goto_1
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$sfgetmWaitLock()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 114
    if-nez v6, :cond_1

    .line 115
    :try_start_1
    const-string v8, "DexoptServiceThread"

    const-string v9, "Received packageName error"

    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    monitor-exit v7

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v8

    goto/16 :goto_3

    .line 117
    :cond_1
    if-nez v4, :cond_2

    .line 118
    iget-object v8, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v8}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptPackageNameList(Lcom/android/server/pm/DexoptServiceThread;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 119
    iget-object v8, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v8, v5}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fputmDexoptResult(Lcom/android/server/pm/DexoptServiceThread;I)V

    .line 120
    iget-object v8, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v8}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptPackageNameList(Lcom/android/server/pm/DexoptServiceThread;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 121
    const-string v8, "DexoptServiceThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "packageName : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " result : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v10}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptResult(Lcom/android/server/pm/DexoptServiceThread;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " finished, notify next."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$sfgetmWaitLock()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->notifyAll()V

    goto :goto_2

    .line 125
    :cond_2
    if-lez v4, :cond_3

    .line 126
    iget-object v8, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v8, v8, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 127
    iget-object v8, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v8, v5}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fputmDexoptSecondaryResult(Lcom/android/server/pm/DexoptServiceThread;I)V

    .line 128
    iget-object v8, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v8, v8, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const-string v8, "DexoptServiceThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "packageName : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " secondaryId : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " result : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v10}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptSecondaryResult(Lcom/android/server/pm/DexoptServiceThread;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " finished, notify next."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$sfgetmWaitLock()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->notifyAll()V

    .line 135
    :cond_3
    :goto_2
    monitor-exit v7

    .line 136
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "temp":[Ljava/lang/String;
    .end local v3    # "len":I
    .end local v4    # "secondaryId":I
    .end local v5    # "result":I
    .end local v6    # "packageName":Ljava/lang/String;
    goto/16 :goto_0

    .line 135
    .restart local v1    # "data":Ljava/lang/String;
    .restart local v2    # "temp":[Ljava/lang/String;
    .restart local v3    # "len":I
    .restart local v4    # "secondaryId":I
    .restart local v5    # "result":I
    .restart local v6    # "packageName":Ljava/lang/String;
    :goto_3
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .end local p0    # "this":Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;
    :try_start_2
    throw v8
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 144
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "temp":[Ljava/lang/String;
    .end local v3    # "len":I
    .end local v4    # "secondaryId":I
    .end local v5    # "result":I
    .end local v6    # "packageName":Ljava/lang/String;
    .restart local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local p0    # "this":Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;
    :cond_4
    if-eqz v0, :cond_5

    :try_start_3
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_4

    .line 145
    :catch_0
    move-exception v1

    .line 148
    goto :goto_5

    .line 147
    :cond_5
    :goto_4
    goto :goto_5

    .line 143
    :catchall_1
    move-exception v1

    goto :goto_6

    .line 139
    :catch_1
    move-exception v1

    .line 140
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {p0}, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->terminate()V

    .line 141
    const-string v2, "DexoptServiceThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mi_dexopt connection:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 144
    .end local v1    # "e":Ljava/lang/Exception;
    if-eqz v0, :cond_5

    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_4

    .line 137
    :catch_2
    move-exception v1

    .line 138
    .local v1, "e":Ljava/io/IOException;
    :try_start_6
    const-string v2, "DexoptServiceThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mi_dexopt connection:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " IOException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 144
    .end local v1    # "e":Ljava/io/IOException;
    if-eqz v0, :cond_5

    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_4

    .line 149
    :goto_5
    return-void

    .line 144
    :goto_6
    if-eqz v0, :cond_6

    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_7

    .line 145
    :catch_3
    move-exception v2

    goto :goto_8

    .line 147
    :cond_6
    :goto_7
    nop

    .line 148
    :goto_8
    throw v1
.end method

.method public terminate()V
    .locals 2

    .line 90
    const-string v0, "DexoptServiceThread"

    const-string v1, "dexopt trigger terminate!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;->mIsContinue:Z

    .line 92
    return-void
.end method
