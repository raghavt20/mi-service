.class public Lcom/android/server/pm/BackgroundPreinstalloptService;
.super Landroid/app/job/JobService;
.source "BackgroundPreinstalloptService.java"


# static fields
.field public static final JOBID:I = 0x1ceea9


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method private getHelper()Lcom/android/server/pm/MiuiPreinstallHelper;
    .locals 1

    .line 31
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 1
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 22
    invoke-direct {p0}, Lcom/android/server/pm/BackgroundPreinstalloptService;->getHelper()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->onStartJob(Lcom/android/server/pm/BackgroundPreinstalloptService;Landroid/app/job/JobParameters;)Z

    move-result v0

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 27
    const/4 v0, 0x0

    return v0
.end method
