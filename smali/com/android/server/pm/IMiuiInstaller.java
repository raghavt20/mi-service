public abstract class com.android.server.pm.IMiuiInstaller {
	 /* .source "IMiuiInstaller.java" */
	 /* # virtual methods */
	 public abstract Integer getDataFileInfo ( java.lang.String p0, java.util.List p1 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/lang/String;", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;)I" */
		 /* } */
	 } // .end annotation
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Lcom/android/server/pm/Installer$InstallerException; */
	 /* } */
} // .end annotation
} // .end method
public abstract Integer getDataFileStat ( java.lang.String p0, java.util.List p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)I" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/pm/Installer$InstallerException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer listDataDir ( java.lang.String p0, Long p1, Long p2, java.util.List p3, Long[] p4 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "JJ", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;[J)I" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/pm/Installer$InstallerException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer transferData ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Boolean p3, Integer p4, Integer p5, Integer p6, java.lang.String p7 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/pm/Installer$InstallerException; */
/* } */
} // .end annotation
} // .end method
