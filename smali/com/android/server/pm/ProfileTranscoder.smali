.class public Lcom/android/server/pm/ProfileTranscoder;
.super Ljava/lang/Object;
.source "ProfileTranscoder.java"


# static fields
.field private static final HOT:I = 0x1

.field private static final INLINE_CACHE_MEGAMORPHIC_ENCODING:I = 0x7

.field private static final INLINE_CACHE_MISSING_TYPES_ENCODING:I = 0x6

.field static final MAGIC_PROF:[B

.field static final MAGIC_PROFM:[B

.field static final METADATA_V001_N:[B

.field static final METADATA_V002:[B

.field static final MIN_SUPPORTED_SDK:I = 0x1f

.field private static final POST_STARTUP:I = 0x4

.field private static final PROFILE_META_LOCATION:Ljava/lang/String; = "dexopt/baseline.profm"

.field private static final PROFILE_SOURCE_LOCATION:Ljava/lang/String; = "dexopt/baseline.prof"

.field private static final STARTUP:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ProfileTranscode"

.field static final V010_P:[B

.field static final V015_S:[B


# instance fields
.field private final mApkName:Ljava/lang/String;

.field private final mAssetManager:Landroid/content/res/AssetManager;

.field private final mBasePath:Ljava/lang/String;

.field private final mDesiredVersion:[B

.field private mDeviceSupportTranscode:Z

.field private mEncoding:Lcom/android/server/pm/Encoding;

.field private final mPackageName:Ljava/lang/String;

.field private mProfile:[Lcom/android/server/pm/DexProfileData;

.field private final mTarget:Ljava/io/File;

.field private mTranscodedProfile:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 35
    const/4 v0, 0x4

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/server/pm/ProfileTranscoder;->MAGIC_PROF:[B

    .line 36
    new-array v1, v0, [B

    fill-array-data v1, :array_1

    sput-object v1, Lcom/android/server/pm/ProfileTranscoder;->MAGIC_PROFM:[B

    .line 37
    new-array v1, v0, [B

    fill-array-data v1, :array_2

    sput-object v1, Lcom/android/server/pm/ProfileTranscoder;->V015_S:[B

    .line 38
    new-array v1, v0, [B

    fill-array-data v1, :array_3

    sput-object v1, Lcom/android/server/pm/ProfileTranscoder;->V010_P:[B

    .line 39
    new-array v1, v0, [B

    fill-array-data v1, :array_4

    sput-object v1, Lcom/android/server/pm/ProfileTranscoder;->METADATA_V001_N:[B

    .line 40
    new-array v0, v0, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/server/pm/ProfileTranscoder;->METADATA_V002:[B

    return-void

    :array_0
    .array-data 1
        0x70t
        0x72t
        0x6ft
        0x0t
    .end array-data

    :array_1
    .array-data 1
        0x70t
        0x72t
        0x6dt
        0x0t
    .end array-data

    :array_2
    .array-data 1
        0x30t
        0x31t
        0x35t
        0x0t
    .end array-data

    :array_3
    .array-data 1
        0x30t
        0x31t
        0x30t
        0x0t
    .end array-data

    :array_4
    .array-data 1
        0x30t
        0x30t
        0x31t
        0x0t
    .end array-data

    :array_5
    .array-data 1
        0x30t
        0x30t
        0x32t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/AssetManager;)V
    .locals 1
    .param p1, "target"    # Ljava/io/File;
    .param p2, "apkName"    # Ljava/lang/String;
    .param p3, "basePath"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "assetManager"    # Landroid/content/res/AssetManager;

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mDeviceSupportTranscode:Z

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mTranscodedProfile:[B

    .line 58
    iput-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;

    .line 67
    iput-object p1, p0, Lcom/android/server/pm/ProfileTranscoder;->mTarget:Ljava/io/File;

    .line 68
    iput-object p2, p0, Lcom/android/server/pm/ProfileTranscoder;->mApkName:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lcom/android/server/pm/ProfileTranscoder;->mBasePath:Ljava/lang/String;

    .line 70
    iput-object p4, p0, Lcom/android/server/pm/ProfileTranscoder;->mPackageName:Ljava/lang/String;

    .line 71
    iput-object p5, p0, Lcom/android/server/pm/ProfileTranscoder;->mAssetManager:Landroid/content/res/AssetManager;

    .line 72
    invoke-direct {p0}, Lcom/android/server/pm/ProfileTranscoder;->desiredVersion()[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mDesiredVersion:[B

    .line 73
    invoke-direct {p0}, Lcom/android/server/pm/ProfileTranscoder;->isSupportTranscode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mDeviceSupportTranscode:Z

    .line 74
    new-instance v0, Lcom/android/server/pm/Encoding;

    invoke-direct {v0}, Lcom/android/server/pm/Encoding;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    .line 75
    return-void
.end method

.method private computeMethodFlags(Lcom/android/server/pm/DexProfileData;)I
    .locals 4
    .param p1, "profileData"    # Lcom/android/server/pm/DexProfileData;

    .line 407
    const/4 v0, 0x0

    .line 408
    .local v0, "methodFlags":I
    iget-object v1, p1, Lcom/android/server/pm/DexProfileData;->methods:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 409
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 410
    .local v3, "flagValue":I
    or-int/2addr v0, v3

    .line 411
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v3    # "flagValue":I
    goto :goto_0

    .line 412
    :cond_0
    return v0
.end method

.method private createCompressibleClassSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection;
    .locals 6
    .param p1, "profileData"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 315
    const/4 v0, 0x0

    .line 316
    .local v0, "expectedSize":I
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 317
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 318
    aget-object v3, p1, v2

    .line 320
    .local v3, "profile":Lcom/android/server/pm/DexProfileData;
    add-int/lit8 v0, v0, 0x2

    .line 321
    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v4, v1, v2}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 323
    add-int/lit8 v0, v0, 0x2

    .line 324
    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    iget v5, v3, Lcom/android/server/pm/DexProfileData;->classSetSize:I

    invoke-virtual {v4, v1, v5}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 326
    iget v4, v3, Lcom/android/server/pm/DexProfileData;->classSetSize:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    .line 327
    invoke-direct {p0, v1, v3}, Lcom/android/server/pm/ProfileTranscoder;->writeClasses(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V

    .line 317
    .end local v3    # "profile":Lcom/android/server/pm/DexProfileData;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 329
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 330
    .local v2, "contents":[B
    array-length v3, v2

    if-ne v0, v3, :cond_1

    .line 336
    new-instance v3, Lcom/android/server/pm/WritableFileSection;

    sget-object v4, Lcom/android/server/pm/FileSectionType;->CLASSES:Lcom/android/server/pm/FileSectionType;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v0, v2, v5}, Lcom/android/server/pm/WritableFileSection;-><init>(Lcom/android/server/pm/FileSectionType;I[BZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 336
    return-object v3

    .line 331
    :cond_1
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", does not match actual size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v3

    .end local v0    # "expectedSize":I
    .end local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    .end local p1    # "profileData":[Lcom/android/server/pm/DexProfileData;
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    .end local v2    # "contents":[B
    .restart local v0    # "expectedSize":I
    .restart local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    .restart local p1    # "profileData":[Lcom/android/server/pm/DexProfileData;
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_1
    throw v2
.end method

.method private createCompressibleMethodsSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection;
    .locals 11
    .param p1, "profileData"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, "expectedSize":I
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 350
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 351
    aget-object v3, p1, v2

    .line 353
    .local v3, "profile":Lcom/android/server/pm/DexProfileData;
    invoke-direct {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->computeMethodFlags(Lcom/android/server/pm/DexProfileData;)I

    move-result v4

    .line 355
    .local v4, "methodFlags":I
    invoke-direct {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->createMethodBitmapRegion(Lcom/android/server/pm/DexProfileData;)[B

    move-result-object v5

    .line 357
    .local v5, "bitmapContents":[B
    invoke-direct {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->createMethodsWithInlineCaches(Lcom/android/server/pm/DexProfileData;)[B

    move-result-object v6

    .line 359
    .local v6, "methodRegionContents":[B
    add-int/lit8 v0, v0, 0x2

    .line 360
    iget-object v7, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v7, v1, v2}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 362
    array-length v7, v5

    add-int/lit8 v7, v7, 0x2

    array-length v8, v6

    add-int/2addr v7, v8

    .line 364
    .local v7, "followingDataSize":I
    add-int/lit8 v0, v0, 0x4

    .line 365
    iget-object v8, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    int-to-long v9, v7

    invoke-virtual {v8, v1, v9, v10}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 367
    iget-object v8, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v8, v1, v4}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 368
    invoke-virtual {v1, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 369
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 370
    add-int/2addr v0, v7

    .line 350
    .end local v3    # "profile":Lcom/android/server/pm/DexProfileData;
    .end local v4    # "methodFlags":I
    .end local v5    # "bitmapContents":[B
    .end local v6    # "methodRegionContents":[B
    .end local v7    # "followingDataSize":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 372
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 373
    .local v2, "contents":[B
    array-length v3, v2

    if-ne v0, v3, :cond_1

    .line 379
    new-instance v3, Lcom/android/server/pm/WritableFileSection;

    sget-object v4, Lcom/android/server/pm/FileSectionType;->METHODS:Lcom/android/server/pm/FileSectionType;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v0, v2, v5}, Lcom/android/server/pm/WritableFileSection;-><init>(Lcom/android/server/pm/FileSectionType;I[BZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 379
    return-object v3

    .line 374
    :cond_1
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", does not match actual size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v3

    .end local v0    # "expectedSize":I
    .end local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    .end local p1    # "profileData":[Lcom/android/server/pm/DexProfileData;
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349
    .end local v2    # "contents":[B
    .restart local v0    # "expectedSize":I
    .restart local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    .restart local p1    # "profileData":[Lcom/android/server/pm/DexProfileData;
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_1
    throw v2
.end method

.method private createMethodBitmapRegion(Lcom/android/server/pm/DexProfileData;)[B
    .locals 3
    .param p1, "profile"    # Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 391
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 392
    .local v0, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-direct {p0, v0, p1}, Lcom/android/server/pm/ProfileTranscoder;->writeMethodBitmap(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V

    .line 393
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 393
    return-object v1

    .line 391
    :catchall_0
    move-exception v1

    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v2

    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_0
    throw v1
.end method

.method private createMethodsWithInlineCaches(Lcom/android/server/pm/DexProfileData;)[B
    .locals 3
    .param p1, "profile"    # Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 400
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 401
    .local v0, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-direct {p0, v0, p1}, Lcom/android/server/pm/ProfileTranscoder;->writeMethodsWithInlineCaches(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V

    .line 402
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 402
    return-object v1

    .line 400
    :catchall_0
    move-exception v1

    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v2

    invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_0
    throw v1
.end method

.method private desiredVersion()[B
    .locals 1

    .line 160
    nop

    .line 161
    sget-object v0, Lcom/android/server/pm/ProfileTranscoder;->V015_S:[B

    return-object v0
.end method

.method private enforceSeparator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "separator"    # Ljava/lang/String;

    .line 690
    const-string v0, "!"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, ":"

    if-eqz v1, :cond_0

    .line 691
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 692
    :cond_0
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 693
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 695
    :cond_1
    return-object p1
.end method

.method private extractKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "profileKey"    # Ljava/lang/String;

    .line 700
    const-string v0, "!"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 701
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 702
    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 704
    :cond_0
    if-lez v0, :cond_1

    .line 706
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 708
    :cond_1
    return-object p1
.end method

.method private findByDexName([Lcom/android/server/pm/DexProfileData;Ljava/lang/String;)Lcom/android/server/pm/DexProfileData;
    .locals 4
    .param p1, "profile"    # [Lcom/android/server/pm/DexProfileData;
    .param p2, "profileKey"    # Ljava/lang/String;

    .line 644
    array-length v0, p1

    const/4 v1, 0x0

    if-gtz v0, :cond_0

    return-object v1

    .line 649
    :cond_0
    invoke-direct {p0, p2}, Lcom/android/server/pm/ProfileTranscoder;->extractKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 650
    .local v0, "dexName":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_2

    .line 651
    aget-object v3, p1, v2

    iget-object v3, v3, Lcom/android/server/pm/DexProfileData;->dexName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 652
    aget-object v1, p1, v2

    return-object v1

    .line 650
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 655
    .end local v2    # "i":I
    :cond_2
    return-object v1
.end method

.method private generateDexKey(Ljava/lang/String;Ljava/lang/String;[B)Ljava/lang/String;
    .locals 2
    .param p1, "apkName"    # Ljava/lang/String;
    .param p2, "dexName"    # Ljava/lang/String;
    .param p3, "version"    # [B

    .line 678
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const-string v1, "!"

    if-gtz v0, :cond_0

    invoke-direct {p0, p2, v1}, Lcom/android/server/pm/ProfileTranscoder;->enforceSeparator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 679
    :cond_0
    const-string v0, "classes.dex"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p1

    .line 680
    :cond_1
    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 683
    :cond_2
    const-string v0, ".apk"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    return-object p2

    .line 684
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 681
    :cond_4
    :goto_0
    invoke-direct {p0, p2, v1}, Lcom/android/server/pm/ProfileTranscoder;->enforceSeparator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMethodBitmapStorageSize(I)I
    .locals 2
    .param p1, "numMethodIds"    # I

    .line 416
    mul-int/lit8 v0, p1, 0x2

    .line 417
    .local v0, "methodBitmapBits":I
    invoke-direct {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->roundUpToByte(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x8

    return v1
.end method

.method private isSupportTranscode()Z
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mDesiredVersion:[B

    if-eqz v0, :cond_0

    sget-object v1, Lcom/android/server/pm/ProfileTranscoder;->V015_S:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private methodFlagBitmapIndex(III)I
    .locals 2
    .param p1, "flag"    # I
    .param p2, "methodIndex"    # I
    .param p3, "numMethodIds"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 863
    packed-switch p1, :pswitch_data_0

    .line 871
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected flag: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 869
    :pswitch_1
    add-int v0, p2, p3

    return v0

    .line 867
    :pswitch_2
    return p2

    .line 865
    :pswitch_3
    const-string v0, "HOT methods are not stored in the bitmap"

    invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private readClasses(Ljava/io/InputStream;I)[I
    .locals 5
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "classSetSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 821
    new-array v0, p2, [I

    .line 822
    .local v0, "classes":[I
    const/4 v1, 0x0

    .line 823
    .local v1, "lastClassIndex":I
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_0
    if-ge v2, p2, :cond_0

    .line 824
    iget-object v3, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v3, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    move-result v3

    .line 825
    .local v3, "diffWithTheLastClassIndex":I
    add-int v4, v1, v3

    .line 826
    .local v4, "classDexIndex":I
    aput v4, v0, v2

    .line 827
    move v1, v4

    .line 823
    .end local v3    # "diffWithTheLastClassIndex":I
    .end local v4    # "classDexIndex":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 829
    .end local v2    # "k":I
    :cond_0
    return-object v0
.end method

.method private readFlagsFromBitmap(Ljava/util/BitSet;II)I
    .locals 2
    .param p1, "bs"    # Ljava/util/BitSet;
    .param p2, "methodIndex"    # I
    .param p3, "numMethodIds"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 850
    const/4 v0, 0x0

    .line 851
    .local v0, "result":I
    const/4 v1, 0x2

    invoke-direct {p0, v1, p2, p3}, Lcom/android/server/pm/ProfileTranscoder;->methodFlagBitmapIndex(III)I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 852
    or-int/lit8 v0, v0, 0x2

    .line 854
    :cond_0
    const/4 v1, 0x4

    invoke-direct {p0, v1, p2, p3}, Lcom/android/server/pm/ProfileTranscoder;->methodFlagBitmapIndex(III)I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 855
    or-int/lit8 v0, v0, 0x4

    .line 857
    :cond_1
    return v0
.end method

.method private readHotMethodRegion(Ljava/io/InputStream;Lcom/android/server/pm/DexProfileData;)V
    .locals 7
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "data"    # Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 767
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    iget v1, p2, Lcom/android/server/pm/DexProfileData;->hotMethodRegionSize:I

    sub-int/2addr v0, v1

    .line 768
    .local v0, "expectedBytesAvailableAfterRead":I
    const/4 v1, 0x0

    .line 770
    .local v1, "lastMethodIndex":I
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v2

    if-le v2, v0, :cond_1

    .line 773
    iget-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v2, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    move-result v2

    .line 774
    .local v2, "diffWithLastMethodDexIndex":I
    add-int v3, v1, v2

    .line 775
    .local v3, "methodDexIndex":I
    iget-object v4, p2, Lcom/android/server/pm/DexProfileData;->methods:Ljava/util/TreeMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 777
    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v4, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    move-result v4

    .line 778
    .local v4, "inlineCacheSize":I
    :goto_1
    if-lez v4, :cond_0

    .line 779
    invoke-direct {p0, p1}, Lcom/android/server/pm/ProfileTranscoder;->skipInlineCache(Ljava/io/InputStream;)V

    .line 780
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 783
    :cond_0
    move v1, v3

    .line 784
    .end local v2    # "diffWithLastMethodDexIndex":I
    .end local v3    # "methodDexIndex":I
    .end local v4    # "inlineCacheSize":I
    goto :goto_0

    .line 786
    :cond_1
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 791
    return-void

    .line 787
    :cond_2
    const-string v2, "Read too much data during profile line parse"

    invoke-virtual {p0, v2}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v2

    throw v2
.end method

.method private readMetadataV002Body(Ljava/io/InputStream;[BI[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData;
    .locals 9
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "desiredProfileVersion"    # [B
    .param p3, "dexFileCount"    # I
    .param p4, "profile"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 611
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    if-nez v0, :cond_0

    .line 612
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/android/server/pm/DexProfileData;

    return-object v0

    .line 614
    :cond_0
    array-length v0, p4

    if-ne p3, v0, :cond_3

    .line 617
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_2

    .line 619
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    .line 621
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    move-result v1

    .line 622
    .local v1, "profileKeySize":I
    iget-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v2, p1, v1}, Lcom/android/server/pm/Encoding;->readString(Ljava/io/InputStream;I)Ljava/lang/String;

    move-result-object v2

    .line 624
    .local v2, "profileKey":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v3, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J

    move-result-wide v3

    .line 626
    .local v3, "typeIdCount":J
    iget-object v5, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v5, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    move-result v5

    .line 627
    .local v5, "classIdSetSize":I
    invoke-direct {p0, p4, v2}, Lcom/android/server/pm/ProfileTranscoder;->findByDexName([Lcom/android/server/pm/DexProfileData;Ljava/lang/String;)Lcom/android/server/pm/DexProfileData;

    move-result-object v6

    .line 628
    .local v6, "data":Lcom/android/server/pm/DexProfileData;
    if-eqz v6, :cond_1

    .line 632
    iput-wide v3, v6, Lcom/android/server/pm/DexProfileData;->mTypeIdCount:J

    .line 636
    invoke-direct {p0, p1, v5}, Lcom/android/server/pm/ProfileTranscoder;->readClasses(Ljava/io/InputStream;I)[I

    .line 617
    .end local v1    # "profileKeySize":I
    .end local v2    # "profileKey":Ljava/lang/String;
    .end local v3    # "typeIdCount":J
    .end local v5    # "classIdSetSize":I
    .end local v6    # "data":Lcom/android/server/pm/DexProfileData;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 629
    .restart local v1    # "profileKeySize":I
    .restart local v2    # "profileKey":Ljava/lang/String;
    .restart local v3    # "typeIdCount":J
    .restart local v5    # "classIdSetSize":I
    .restart local v6    # "data":Lcom/android/server/pm/DexProfileData;
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Missing profile key: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v7

    throw v7

    .line 638
    .end local v0    # "i":I
    .end local v1    # "profileKeySize":I
    .end local v2    # "profileKey":Ljava/lang/String;
    .end local v3    # "typeIdCount":J
    .end local v5    # "classIdSetSize":I
    .end local v6    # "data":Lcom/android/server/pm/DexProfileData;
    :cond_2
    return-object p4

    .line 615
    :cond_3
    const-string v0, "Mismatched number of dex files found in metadata"

    invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private readMethodBitmap(Ljava/io/InputStream;Lcom/android/server/pm/DexProfileData;)V
    .locals 9
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "data"    # Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 836
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    iget v1, p2, Lcom/android/server/pm/DexProfileData;->numMethodIds:I

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/pm/Encoding;->bitsToBytes(I)I

    move-result v0

    .line 837
    .local v0, "methodBitmapStorageSize":I
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B

    move-result-object v1

    .line 838
    .local v1, "methodBitmap":[B
    invoke-static {v1}, Ljava/util/BitSet;->valueOf([B)Ljava/util/BitSet;

    move-result-object v2

    .line 839
    .local v2, "bs":Ljava/util/BitSet;
    const/4 v3, 0x0

    .local v3, "methodIndex":I
    :goto_0
    iget v4, p2, Lcom/android/server/pm/DexProfileData;->numMethodIds:I

    if-ge v3, v4, :cond_2

    .line 840
    iget v4, p2, Lcom/android/server/pm/DexProfileData;->numMethodIds:I

    invoke-direct {p0, v2, v3, v4}, Lcom/android/server/pm/ProfileTranscoder;->readFlagsFromBitmap(Ljava/util/BitSet;II)I

    move-result v4

    .line 841
    .local v4, "newFlags":I
    if-eqz v4, :cond_1

    .line 842
    iget-object v5, p2, Lcom/android/server/pm/DexProfileData;->methods:Ljava/util/TreeMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 843
    .local v5, "current":Ljava/lang/Integer;
    if-nez v5, :cond_0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 844
    :cond_0
    iget-object v6, p2, Lcom/android/server/pm/DexProfileData;->methods:Ljava/util/TreeMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    or-int/2addr v8, v4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 839
    .end local v4    # "newFlags":I
    .end local v5    # "current":Ljava/lang/Integer;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 847
    .end local v3    # "methodIndex":I
    :cond_2
    return-void
.end method

.method private readUncompressedBody(Ljava/io/InputStream;Ljava/lang/String;I)[Lcom/android/server/pm/DexProfileData;
    .locals 27
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "apkName"    # Ljava/lang/String;
    .param p3, "numberOfDexFiles"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 724
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->available()I

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_0

    .line 725
    new-array v3, v4, [Lcom/android/server/pm/DexProfileData;

    return-object v3

    .line 728
    :cond_0
    new-array v3, v2, [Lcom/android/server/pm/DexProfileData;

    .line 729
    .local v3, "lines":[Lcom/android/server/pm/DexProfileData;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v2, :cond_1

    .line 730
    iget-object v6, v0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v6, v1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    move-result v6

    .line 731
    .local v6, "dexNameSize":I
    iget-object v7, v0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v7, v1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    move-result v7

    .line 732
    .local v7, "classSetSize":I
    iget-object v8, v0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v8, v1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J

    move-result-wide v13

    .line 733
    .local v13, "hotMethodRegionSize":J
    iget-object v8, v0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v8, v1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J

    move-result-wide v20

    .line 734
    .local v20, "dexChecksum":J
    iget-object v8, v0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v8, v1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J

    move-result-wide v11

    .line 735
    .local v11, "numMethodIds":J
    new-instance v22, Lcom/android/server/pm/DexProfileData;

    iget-object v8, v0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    .line 737
    invoke-virtual {v8, v1, v6}, Lcom/android/server/pm/Encoding;->readString(Ljava/io/InputStream;I)Ljava/lang/String;

    move-result-object v10

    const-wide/16 v15, 0x0

    long-to-int v9, v13

    long-to-int v8, v11

    new-array v4, v7, [I

    new-instance v19, Ljava/util/TreeMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/TreeMap;-><init>()V

    move/from16 v17, v8

    move-object/from16 v8, v22

    move/from16 v18, v9

    move-object/from16 v9, p2

    move-wide/from16 v23, v11

    .end local v11    # "numMethodIds":J
    .local v23, "numMethodIds":J
    move-wide/from16 v11, v20

    move-wide/from16 v25, v13

    .end local v13    # "hotMethodRegionSize":J
    .local v25, "hotMethodRegionSize":J
    move-wide v13, v15

    move v15, v7

    move/from16 v16, v18

    move-object/from16 v18, v4

    invoke-direct/range {v8 .. v19}, Lcom/android/server/pm/DexProfileData;-><init>(Ljava/lang/String;Ljava/lang/String;JJIII[ILjava/util/TreeMap;)V

    aput-object v22, v3, v5

    .line 729
    .end local v6    # "dexNameSize":I
    .end local v7    # "classSetSize":I
    .end local v20    # "dexChecksum":J
    .end local v23    # "numMethodIds":J
    .end local v25    # "hotMethodRegionSize":J
    add-int/lit8 v5, v5, 0x1

    const/4 v4, 0x0

    goto :goto_0

    .line 750
    .end local v5    # "i":I
    :cond_1
    array-length v4, v3

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_2

    aget-object v6, v3, v5

    .line 752
    .local v6, "data":Lcom/android/server/pm/DexProfileData;
    invoke-direct {v0, v1, v6}, Lcom/android/server/pm/ProfileTranscoder;->readHotMethodRegion(Ljava/io/InputStream;Lcom/android/server/pm/DexProfileData;)V

    .line 754
    iget v7, v6, Lcom/android/server/pm/DexProfileData;->classSetSize:I

    invoke-direct {v0, v1, v7}, Lcom/android/server/pm/ProfileTranscoder;->readClasses(Ljava/io/InputStream;I)[I

    move-result-object v7

    iput-object v7, v6, Lcom/android/server/pm/DexProfileData;->classes:[I

    .line 758
    invoke-direct {v0, v1, v6}, Lcom/android/server/pm/ProfileTranscoder;->readMethodBitmap(Ljava/io/InputStream;Lcom/android/server/pm/DexProfileData;)V

    .line 750
    .end local v6    # "data":Lcom/android/server/pm/DexProfileData;
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 760
    :cond_2
    return-object v3
.end method

.method private roundUpToByte(I)I
    .locals 1
    .param p1, "bits"    # I

    .line 421
    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, -0x1

    and-int/lit8 v0, v0, -0x8

    return v0
.end method

.method private setMethodBitmapBit([BIILcom/android/server/pm/DexProfileData;)V
    .locals 5
    .param p1, "bitmap"    # [B
    .param p2, "flag"    # I
    .param p3, "methodIndex"    # I
    .param p4, "dexData"    # Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 438
    iget v0, p4, Lcom/android/server/pm/DexProfileData;->numMethodIds:I

    invoke-direct {p0, p2, p3, v0}, Lcom/android/server/pm/ProfileTranscoder;->methodFlagBitmapIndex(III)I

    move-result v0

    .line 439
    .local v0, "bitIndex":I
    div-int/lit8 v1, v0, 0x8

    .line 440
    .local v1, "bitmapIndex":I
    aget-byte v2, p1, v1

    const/4 v3, 0x1

    rem-int/lit8 v4, v0, 0x8

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    int-to-byte v2, v2

    .line 441
    .local v2, "value":B
    aput-byte v2, p1, v1

    .line 442
    return-void
.end method

.method private skipInlineCache(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 794
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    .line 795
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/Encoding;->readUInt8(Ljava/io/InputStream;)I

    move-result v0

    .line 797
    .local v0, "dexPcMapSize":I
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 798
    return-void

    .line 801
    :cond_0
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 802
    return-void

    .line 806
    :cond_1
    :goto_0
    if-lez v0, :cond_3

    .line 807
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt8(Ljava/io/InputStream;)I

    .line 808
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt8(Ljava/io/InputStream;)I

    move-result v1

    .line 809
    .local v1, "numClasses":I
    :goto_1
    if-lez v1, :cond_2

    .line 810
    iget-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v2, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    .line 811
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 813
    :cond_2
    nop

    .end local v1    # "numClasses":I
    add-int/lit8 v0, v0, -0x1

    .line 814
    goto :goto_0

    .line 815
    :cond_3
    return-void
.end method

.method private writeClasses(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V
    .locals 7
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "dexData"    # Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 482
    const/4 v0, 0x0

    .line 485
    .local v0, "lastClassIndex":I
    iget-object v1, p2, Lcom/android/server/pm/DexProfileData;->classes:[I

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget v4, v1, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 486
    .local v4, "classIndex":Ljava/lang/Integer;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int/2addr v5, v0

    .line 487
    .local v5, "diffWithTheLastClassIndex":I
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v6, p1, v5}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 488
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 485
    .end local v4    # "classIndex":Ljava/lang/Integer;
    .end local v5    # "diffWithTheLastClassIndex":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 490
    :cond_0
    return-void
.end method

.method private writeDexFileSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection;
    .locals 7
    .param p1, "profileData"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 265
    const/4 v0, 0x0

    .line 266
    .local v0, "expectedSize":I
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 268
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    add-int/lit8 v0, v0, 0x2

    .line 269
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    array-length v3, p1

    invoke-virtual {v2, v1, v3}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 270
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 271
    aget-object v3, p1, v2

    .line 273
    .local v3, "profile":Lcom/android/server/pm/DexProfileData;
    add-int/lit8 v0, v0, 0x4

    .line 274
    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    iget-wide v5, v3, Lcom/android/server/pm/DexProfileData;->dexChecksum:J

    invoke-virtual {v4, v1, v5, v6}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 276
    add-int/lit8 v0, v0, 0x4

    .line 280
    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    iget-wide v5, v3, Lcom/android/server/pm/DexProfileData;->mTypeIdCount:J

    invoke-virtual {v4, v1, v5, v6}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 282
    add-int/lit8 v0, v0, 0x4

    .line 283
    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    iget v5, v3, Lcom/android/server/pm/DexProfileData;->numMethodIds:I

    int-to-long v5, v5

    invoke-virtual {v4, v1, v5, v6}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 285
    iget-object v4, v3, Lcom/android/server/pm/DexProfileData;->apkName:Ljava/lang/String;

    iget-object v5, v3, Lcom/android/server/pm/DexProfileData;->dexName:Ljava/lang/String;

    sget-object v6, Lcom/android/server/pm/ProfileTranscoder;->V015_S:[B

    invoke-direct {p0, v4, v5, v6}, Lcom/android/server/pm/ProfileTranscoder;->generateDexKey(Ljava/lang/String;Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v4

    .line 290
    .local v4, "profileKey":Ljava/lang/String;
    add-int/lit8 v0, v0, 0x2

    .line 291
    iget-object v5, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v5, v4}, Lcom/android/server/pm/Encoding;->utf8Length(Ljava/lang/String;)I

    move-result v5

    .line 292
    .local v5, "keyLength":I
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v6, v1, v5}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 293
    mul-int/lit8 v6, v5, 0x1

    add-int/2addr v0, v6

    .line 294
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v6, v1, v4}, Lcom/android/server/pm/Encoding;->writeString(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 270
    .end local v3    # "profile":Lcom/android/server/pm/DexProfileData;
    .end local v4    # "profileKey":Ljava/lang/String;
    .end local v5    # "keyLength":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 296
    .end local v2    # "i":I
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 297
    .local v2, "contents":[B
    array-length v3, v2

    if-ne v0, v3, :cond_1

    .line 303
    new-instance v3, Lcom/android/server/pm/WritableFileSection;

    sget-object v4, Lcom/android/server/pm/FileSectionType;->DEX_FILES:Lcom/android/server/pm/FileSectionType;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v0, v2, v5}, Lcom/android/server/pm/WritableFileSection;-><init>(Lcom/android/server/pm/FileSectionType;I[BZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 303
    return-object v3

    .line 298
    :cond_1
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", does not match actual size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v3

    .end local v0    # "expectedSize":I
    .end local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    .end local p1    # "profileData":[Lcom/android/server/pm/DexProfileData;
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266
    .end local v2    # "contents":[B
    .restart local v0    # "expectedSize":I
    .restart local v1    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    .restart local p1    # "profileData":[Lcom/android/server/pm/DexProfileData;
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_1
    throw v2
.end method

.method private writeMethodBitmap(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V
    .locals 6
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "dexData"    # Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 501
    iget v0, p2, Lcom/android/server/pm/DexProfileData;->numMethodIds:I

    invoke-direct {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->getMethodBitmapStorageSize(I)I

    move-result v0

    new-array v0, v0, [B

    .line 502
    .local v0, "bitmap":[B
    iget-object v1, p2, Lcom/android/server/pm/DexProfileData;->methods:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 503
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 504
    .local v3, "methodIndex":I
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 505
    .local v4, "flagValue":I
    and-int/lit8 v5, v4, 0x2

    if-eqz v5, :cond_0

    .line 506
    const/4 v5, 0x2

    invoke-direct {p0, v0, v5, v3, p2}, Lcom/android/server/pm/ProfileTranscoder;->setMethodBitmapBit([BIILcom/android/server/pm/DexProfileData;)V

    .line 508
    :cond_0
    and-int/lit8 v5, v4, 0x4

    if-eqz v5, :cond_1

    .line 509
    const/4 v5, 0x4

    invoke-direct {p0, v0, v5, v3, p2}, Lcom/android/server/pm/ProfileTranscoder;->setMethodBitmapBit([BIILcom/android/server/pm/DexProfileData;)V

    .line 511
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v3    # "methodIndex":I
    .end local v4    # "flagValue":I
    :cond_1
    goto :goto_0

    .line 512
    :cond_2
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 513
    return-void
.end method

.method private writeMethodsWithInlineCaches(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V
    .locals 8
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "dexData"    # Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 456
    const/4 v0, 0x0

    .line 457
    .local v0, "lastMethodIndex":I
    iget-object v1, p2, Lcom/android/server/pm/DexProfileData;->methods:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 458
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 459
    .local v3, "methodId":I
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 460
    .local v4, "flags":I
    and-int/lit8 v5, v4, 0x1

    if-nez v5, :cond_0

    .line 461
    goto :goto_0

    .line 463
    :cond_0
    sub-int v5, v3, v0

    .line 464
    .local v5, "diffWithTheLastMethodIndex":I
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v6, p1, v5}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 465
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V

    .line 466
    move v0, v3

    .line 467
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v3    # "methodId":I
    .end local v4    # "flags":I
    .end local v5    # "diffWithTheLastMethodIndex":I
    goto :goto_0

    .line 468
    :cond_1
    return-void
.end method

.method private writeProfileForS(Ljava/io/OutputStream;[Lcom/android/server/pm/DexProfileData;)V
    .locals 0
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "profileData"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 211
    invoke-direct {p0, p1, p2}, Lcom/android/server/pm/ProfileTranscoder;->writeProfileSections(Ljava/io/OutputStream;[Lcom/android/server/pm/DexProfileData;)V

    .line 212
    return-void
.end method

.method private writeProfileSections(Ljava/io/OutputStream;[Lcom/android/server/pm/DexProfileData;)V
    .locals 12
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "profileData"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 221
    .local v0, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/WritableFileSection;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object v1, v2

    .line 222
    .local v1, "sectionContents":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-direct {p0, p2}, Lcom/android/server/pm/ProfileTranscoder;->writeDexFileSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    invoke-direct {p0, p2}, Lcom/android/server/pm/ProfileTranscoder;->createCompressibleClassSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    invoke-direct {p0, p2}, Lcom/android/server/pm/ProfileTranscoder;->createCompressibleMethodsSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v2, Lcom/android/server/pm/ProfileTranscoder;->V015_S:[B

    array-length v2, v2

    int-to-long v2, v2

    sget-object v4, Lcom/android/server/pm/ProfileTranscoder;->MAGIC_PROF:[B

    array-length v4, v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 227
    .local v2, "offset":J
    const-wide/16 v4, 0x4

    add-long/2addr v2, v4

    .line 229
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x10

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 230
    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v4, p1, v5, v6}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 231
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 232
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/pm/WritableFileSection;

    .line 234
    .local v5, "section":Lcom/android/server/pm/WritableFileSection;
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    iget-object v7, v5, Lcom/android/server/pm/WritableFileSection;->mType:Lcom/android/server/pm/FileSectionType;

    invoke-virtual {v7}, Lcom/android/server/pm/FileSectionType;->getValue()J

    move-result-wide v7

    invoke-virtual {v6, p1, v7, v8}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 236
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v6, p1, v2, v3}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 238
    iget-boolean v6, v5, Lcom/android/server/pm/WritableFileSection;->mNeedsCompression:Z

    if-eqz v6, :cond_0

    .line 239
    iget-object v6, v5, Lcom/android/server/pm/WritableFileSection;->mContents:[B

    array-length v6, v6

    int-to-long v6, v6

    .line 240
    .local v6, "inflatedSize":J
    iget-object v8, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    iget-object v9, v5, Lcom/android/server/pm/WritableFileSection;->mContents:[B

    invoke-virtual {v8, v9}, Lcom/android/server/pm/Encoding;->compress([B)[B

    move-result-object v8

    .line 241
    .local v8, "compressed":[B
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    iget-object v9, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    array-length v10, v8

    int-to-long v10, v10

    invoke-virtual {v9, p1, v10, v11}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 245
    iget-object v9, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v9, p1, v6, v7}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 246
    array-length v9, v8

    int-to-long v9, v9

    add-long/2addr v2, v9

    .line 247
    .end local v6    # "inflatedSize":J
    .end local v8    # "compressed":[B
    goto :goto_1

    .line 248
    :cond_0
    iget-object v6, v5, Lcom/android/server/pm/WritableFileSection;->mContents:[B

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    iget-object v7, v5, Lcom/android/server/pm/WritableFileSection;->mContents:[B

    array-length v7, v7

    int-to-long v7, v7

    invoke-virtual {v6, p1, v7, v8}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 252
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    const-wide/16 v7, 0x0

    invoke-virtual {v6, p1, v7, v8}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V

    .line 253
    iget-object v6, v5, Lcom/android/server/pm/WritableFileSection;->mContents:[B

    array-length v6, v6

    int-to-long v6, v6

    add-long/2addr v2, v6

    .line 231
    .end local v5    # "section":Lcom/android/server/pm/WritableFileSection;
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 257
    .end local v4    # "i":I
    :cond_1
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 258
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V

    .line 257
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 260
    .end local v4    # "i":I
    :cond_2
    return-void
.end method


# virtual methods
.method error(Ljava/lang/String;)Ljava/lang/Exception;
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .line 876
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public read()Lcom/android/server/pm/ProfileTranscoder;
    .locals 8

    .line 78
    const-string v0, "ProfileTranscode"

    iget-boolean v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mDeviceSupportTranscode:Z

    if-nez v1, :cond_0

    .line 79
    return-object p0

    .line 82
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v3, "dexopt/baseline.prof"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 83
    .local v2, "profile_fd":Landroid/content/res/AssetFileDescriptor;
    :try_start_1
    iget-object v3, p0, Lcom/android/server/pm/ProfileTranscoder;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v4, "dexopt/baseline.profm"

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    .line 85
    .local v3, "meta_fd":Landroid/content/res/AssetFileDescriptor;
    :try_start_2
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 86
    .local v4, "is":Ljava/io/InputStream;
    :try_start_3
    sget-object v5, Lcom/android/server/pm/ProfileTranscoder;->MAGIC_PROF:[B

    invoke-virtual {p0, v4, v5}, Lcom/android/server/pm/ProfileTranscoder;->readHeader(Ljava/io/InputStream;[B)[B

    move-result-object v5

    .line 87
    .local v5, "baselineVersion":[B
    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mApkName:Ljava/lang/String;

    invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/pm/ProfileTranscoder;->readProfile(Ljava/io/InputStream;[BLjava/lang/String;)[Lcom/android/server/pm/DexProfileData;

    move-result-object v6

    iput-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 88
    .end local v5    # "baselineVersion":[B
    if-eqz v4, :cond_1

    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 91
    .end local v4    # "is":Ljava/io/InputStream;
    :cond_1
    goto :goto_1

    .line 85
    .restart local v4    # "is":Ljava/io/InputStream;
    :catchall_0
    move-exception v5

    if-eqz v4, :cond_2

    :try_start_5
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v6

    :try_start_6
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    .end local v3    # "meta_fd":Landroid/content/res/AssetFileDescriptor;
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :cond_2
    :goto_0
    throw v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 81
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    .restart local v3    # "meta_fd":Landroid/content/res/AssetFileDescriptor;
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :catchall_2
    move-exception v4

    goto :goto_4

    .line 88
    :catch_0
    move-exception v4

    .line 89
    .local v4, "e":Ljava/lang/Exception;
    :try_start_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " read exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iput-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;

    .line 92
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_1
    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 93
    .local v4, "profile":[Lcom/android/server/pm/DexProfileData;
    if-eqz v4, :cond_5

    .line 94
    :try_start_8
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v5
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 95
    .local v5, "is":Ljava/io/InputStream;
    :try_start_9
    sget-object v6, Lcom/android/server/pm/ProfileTranscoder;->MAGIC_PROFM:[B

    invoke-virtual {p0, v5, v6}, Lcom/android/server/pm/ProfileTranscoder;->readHeader(Ljava/io/InputStream;[B)[B

    move-result-object v6

    .line 96
    .local v6, "metaVersion":[B
    iget-object v7, p0, Lcom/android/server/pm/ProfileTranscoder;->mDesiredVersion:[B

    invoke-virtual {p0, v5, v6, v7, v4}, Lcom/android/server/pm/ProfileTranscoder;->readMeta(Ljava/io/InputStream;[B[B[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData;

    move-result-object v7

    iput-object v7, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 97
    .end local v6    # "metaVersion":[B
    if-eqz v5, :cond_3

    :try_start_a
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 100
    .end local v5    # "is":Ljava/io/InputStream;
    :cond_3
    goto :goto_3

    .line 94
    .restart local v5    # "is":Ljava/io/InputStream;
    :catchall_3
    move-exception v6

    if-eqz v5, :cond_4

    :try_start_b
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    goto :goto_2

    :catchall_4
    move-exception v7

    :try_start_c
    invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    .end local v3    # "meta_fd":Landroid/content/res/AssetFileDescriptor;
    .end local v4    # "profile":[Lcom/android/server/pm/DexProfileData;
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :cond_4
    :goto_2
    throw v6
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 97
    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    .restart local v3    # "meta_fd":Landroid/content/res/AssetFileDescriptor;
    .restart local v4    # "profile":[Lcom/android/server/pm/DexProfileData;
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :catch_1
    move-exception v5

    .line 98
    .local v5, "e":Ljava/lang/Exception;
    :try_start_d
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/android/server/pm/ProfileTranscoder;->mPackageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " read meta profile exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iput-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 102
    .end local v4    # "profile":[Lcom/android/server/pm/DexProfileData;
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_3
    if-eqz v3, :cond_6

    :try_start_e
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    .end local v3    # "meta_fd":Landroid/content/res/AssetFileDescriptor;
    :cond_6
    if-eqz v2, :cond_7

    :try_start_f
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_2

    .line 105
    .end local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    :cond_7
    goto :goto_7

    .line 81
    .restart local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    .restart local v3    # "meta_fd":Landroid/content/res/AssetFileDescriptor;
    :goto_4
    if-eqz v3, :cond_8

    :try_start_10
    invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    goto :goto_5

    :catchall_5
    move-exception v5

    :try_start_11
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :cond_8
    :goto_5
    throw v4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    .end local v3    # "meta_fd":Landroid/content/res/AssetFileDescriptor;
    .restart local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :catchall_6
    move-exception v3

    if-eqz v2, :cond_9

    :try_start_12
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    goto :goto_6

    :catchall_7
    move-exception v4

    :try_start_13
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :cond_9
    :goto_6
    throw v3
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_2

    .line 102
    .end local v2    # "profile_fd":Landroid/content/res/AssetFileDescriptor;
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :catch_2
    move-exception v2

    .line 103
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/pm/ProfileTranscoder;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " read profile exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iput-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;

    .line 106
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_7
    return-object p0
.end method

.method readHeader(Ljava/io/InputStream;[B)[B
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "magic"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    array-length v1, p2

    invoke-virtual {v0, p1, v1}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 168
    .local v0, "fileMagic":[B
    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    sget-object v2, Lcom/android/server/pm/ProfileTranscoder;->V010_P:[B

    array-length v2, v2

    invoke-virtual {v1, p1, v2}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B

    move-result-object v1

    return-object v1

    .line 171
    :cond_0
    const-string v1, "Invalid magic"

    invoke-virtual {p0, v1}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v1

    throw v1
.end method

.method readMeta(Ljava/io/InputStream;[B[B[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData;
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "metadataVersion"    # [B
    .param p3, "desiredProfileVersion"    # [B
    .param p4, "profile"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 556
    sget-object v0, Lcom/android/server/pm/ProfileTranscoder;->METADATA_V001_N:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 560
    sget-object v0, Lcom/android/server/pm/ProfileTranscoder;->METADATA_V002:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    invoke-virtual {p0, p1, p3, p4}, Lcom/android/server/pm/ProfileTranscoder;->readMetadataV002(Ljava/io/InputStream;[B[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData;

    move-result-object v0

    return-object v0

    .line 563
    :cond_0
    const-string v0, "Unsupported meta version"

    invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    .line 557
    :cond_1
    const-string v0, "Requires new Baseline Profile Metadata. Please rebuild the APK with Android Gradle Plugin 7.2 Canary 7 or higher"

    invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method readMetadataV002(Ljava/io/InputStream;[B[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData;
    .locals 9
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "desiredProfileVersion"    # [B
    .param p3, "profile"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 581
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I

    move-result v0

    .line 583
    .local v0, "dexFileCount":I
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J

    move-result-wide v1

    .line 585
    .local v1, "uncompressed":J
    iget-object v3, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v3, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J

    move-result-wide v3

    .line 588
    .local v3, "compressed":J
    iget-object v5, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    long-to-int v6, v3

    long-to-int v7, v1

    invoke-virtual {v5, p1, v6, v7}, Lcom/android/server/pm/Encoding;->readCompressed(Ljava/io/InputStream;II)[B

    move-result-object v5

    .line 593
    .local v5, "contents":[B
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v6

    if-gtz v6, :cond_0

    .line 594
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 595
    .local v6, "dataStream":Ljava/io/InputStream;
    :try_start_0
    invoke-direct {p0, v6, p2, v0, p3}, Lcom/android/server/pm/ProfileTranscoder;->readMetadataV002Body(Ljava/io/InputStream;[BI[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData;

    move-result-object v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 595
    return-object v7

    .line 594
    :catchall_0
    move-exception v7

    :try_start_1
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v8

    invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_0
    throw v7

    .line 593
    .end local v6    # "dataStream":Ljava/io/InputStream;
    :cond_0
    const-string v6, "Content found after the end of file"

    invoke-virtual {p0, v6}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v6

    throw v6
.end method

.method readProfile(Ljava/io/InputStream;[BLjava/lang/String;)[Lcom/android/server/pm/DexProfileData;
    .locals 9
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "version"    # [B
    .param p3, "apkName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 531
    sget-object v0, Lcom/android/server/pm/ProfileTranscoder;->V010_P:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/Encoding;->readUInt8(Ljava/io/InputStream;)I

    move-result v0

    .line 535
    .local v0, "numberOfDexFiles":I
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J

    move-result-wide v1

    .line 536
    .local v1, "uncompressedDataSize":J
    iget-object v3, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v3, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J

    move-result-wide v3

    .line 539
    .local v3, "compressedDataSize":J
    iget-object v5, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    long-to-int v6, v3

    long-to-int v7, v1

    invoke-virtual {v5, p1, v6, v7}, Lcom/android/server/pm/Encoding;->readCompressed(Ljava/io/InputStream;II)[B

    move-result-object v5

    .line 544
    .local v5, "uncompressedData":[B
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v6

    if-gtz v6, :cond_0

    .line 545
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 546
    .local v6, "dataStream":Ljava/io/InputStream;
    :try_start_0
    invoke-direct {p0, v6, p3, v0}, Lcom/android/server/pm/ProfileTranscoder;->readUncompressedBody(Ljava/io/InputStream;Ljava/lang/String;I)[Lcom/android/server/pm/DexProfileData;

    move-result-object v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 547
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 546
    return-object v7

    .line 545
    :catchall_0
    move-exception v7

    :try_start_1
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v8

    invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_0
    throw v7

    .line 544
    .end local v6    # "dataStream":Ljava/io/InputStream;
    :cond_0
    const-string v6, "Content found after the end of file"

    invoke-virtual {p0, v6}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v6

    throw v6

    .line 532
    .end local v0    # "numberOfDexFiles":I
    .end local v1    # "uncompressedDataSize":J
    .end local v3    # "compressedDataSize":J
    .end local v5    # "uncompressedData":[B
    :cond_1
    const-string v0, "Unsupported version"

    invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method transcodeAndWriteBody(Ljava/io/OutputStream;[B[Lcom/android/server/pm/DexProfileData;)Z
    .locals 1
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "desiredVersion"    # [B
    .param p3, "data"    # [Lcom/android/server/pm/DexProfileData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 195
    sget-object v0, Lcom/android/server/pm/ProfileTranscoder;->V015_S:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    invoke-direct {p0, p1, p3}, Lcom/android/server/pm/ProfileTranscoder;->writeProfileForS(Ljava/io/OutputStream;[Lcom/android/server/pm/DexProfileData;)V

    .line 197
    const/4 v0, 0x1

    return v0

    .line 199
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public transcodeIfNeeded()Lcom/android/server/pm/ProfileTranscoder;
    .locals 6

    .line 110
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;

    .line 111
    .local v0, "profile":[Lcom/android/server/pm/DexProfileData;
    iget-object v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mDesiredVersion:[B

    .line 112
    .local v1, "desiredVersion":[B
    if-eqz v0, :cond_2

    if-nez v1, :cond_0

    goto :goto_2

    .line 115
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    .local v3, "os":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    invoke-virtual {p0, v3, v1}, Lcom/android/server/pm/ProfileTranscoder;->writeHeader(Ljava/io/OutputStream;[B)V

    .line 117
    invoke-virtual {p0, v3, v1, v0}, Lcom/android/server/pm/ProfileTranscoder;->transcodeAndWriteBody(Ljava/io/OutputStream;[B[Lcom/android/server/pm/DexProfileData;)Z

    move-result v4

    .line 122
    .local v4, "success":Z
    if-nez v4, :cond_1

    .line 123
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    nop

    .line 127
    :try_start_2
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 124
    return-object p0

    .line 126
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/pm/ProfileTranscoder;->mTranscodedProfile:[B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 127
    .end local v4    # "success":Z
    :try_start_4
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 130
    .end local v3    # "os":Ljava/io/ByteArrayOutputStream;
    goto :goto_1

    .line 115
    .restart local v3    # "os":Ljava/io/ByteArrayOutputStream;
    :catchall_0
    move-exception v4

    :try_start_5
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v5

    :try_start_6
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "profile":[Lcom/android/server/pm/DexProfileData;
    .end local v1    # "desiredVersion":[B
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :goto_0
    throw v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 127
    .end local v3    # "os":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "profile":[Lcom/android/server/pm/DexProfileData;
    .restart local v1    # "desiredVersion":[B
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :catch_0
    move-exception v3

    .line 128
    .local v3, "e":Ljava/lang/Exception;
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mTranscodedProfile:[B

    .line 129
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/server/pm/ProfileTranscoder;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " transcodeIfNeeded Exception:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ProfileTranscode"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;

    .line 132
    return-object p0

    .line 113
    :cond_2
    :goto_2
    return-object p0
.end method

.method public write()Z
    .locals 7

    .line 136
    iget-object v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mTranscodedProfile:[B

    .line 137
    .local v0, "transcodedProfile":[B
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 138
    return v1

    .line 141
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 142
    .local v3, "bis":Ljava/io/InputStream;
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/android/server/pm/ProfileTranscoder;->mTarget:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 144
    .local v4, "os":Ljava/io/OutputStream;
    :try_start_2
    iget-object v5, p0, Lcom/android/server/pm/ProfileTranscoder;->mEncoding:Lcom/android/server/pm/Encoding;

    invoke-virtual {v5, v3, v4}, Lcom/android/server/pm/Encoding;->writeAll(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 145
    nop

    .line 146
    :try_start_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 149
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mTranscodedProfile:[B

    .line 150
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;

    .line 145
    const/4 v1, 0x1

    return v1

    .line 140
    :catchall_0
    move-exception v5

    :try_start_5
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v6

    :try_start_6
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "transcodedProfile":[B
    .end local v3    # "bis":Ljava/io/InputStream;
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :goto_0
    throw v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .end local v4    # "os":Ljava/io/OutputStream;
    .restart local v0    # "transcodedProfile":[B
    .restart local v3    # "bis":Ljava/io/InputStream;
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :catchall_2
    move-exception v4

    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v5

    :try_start_8
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "transcodedProfile":[B
    .end local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :goto_1
    throw v4
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 149
    .end local v3    # "bis":Ljava/io/InputStream;
    .restart local v0    # "transcodedProfile":[B
    .restart local p0    # "this":Lcom/android/server/pm/ProfileTranscoder;
    :catchall_4
    move-exception v1

    goto :goto_2

    .line 146
    :catch_0
    move-exception v3

    .line 147
    .local v3, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v4, "ProfileTranscode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/server/pm/ProfileTranscoder;->mPackageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " write Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 149
    nop

    .end local v3    # "e":Ljava/lang/Exception;
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mTranscodedProfile:[B

    .line 150
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;

    .line 151
    nop

    .line 152
    return v1

    .line 149
    :goto_2
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mTranscodedProfile:[B

    .line 150
    iput-object v2, p0, Lcom/android/server/pm/ProfileTranscoder;->mProfile:[Lcom/android/server/pm/DexProfileData;

    .line 151
    throw v1
.end method

.method writeHeader(Ljava/io/OutputStream;[B)V
    .locals 1
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "version"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 177
    sget-object v0, Lcom/android/server/pm/ProfileTranscoder;->MAGIC_PROF:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 178
    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write([B)V

    .line 179
    return-void
.end method
