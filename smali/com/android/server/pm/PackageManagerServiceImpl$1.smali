.class Lcom/android/server/pm/PackageManagerServiceImpl$1;
.super Landroid/database/ContentObserver;
.source "PackageManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/PackageManagerServiceImpl;->beforeSystemReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/PackageManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PackageManagerServiceImpl;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/PackageManagerServiceImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 283
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerServiceImpl$1;->this$0:Lcom/android/server/pm/PackageManagerServiceImpl;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 286
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl$1;->this$0:Lcom/android/server/pm/PackageManagerServiceImpl;

    invoke-static {v0}, Lcom/android/server/pm/PackageManagerServiceImpl;->-$$Nest$fgetmDomainVerificationService(Lcom/android/server/pm/PackageManagerServiceImpl;)Lcom/android/server/pm/verify/domain/DomainVerificationService;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/verify/domain/DomainVerificationService;->verifyPackages(Ljava/util/List;Z)V

    .line 287
    return-void
.end method
