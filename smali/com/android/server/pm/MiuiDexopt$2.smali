.class Lcom/android/server/pm/MiuiDexopt$2;
.super Ljava/lang/Object;
.source "MiuiDexopt.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/MiuiDexopt;->asyncDexMetadataDexopt(Lcom/android/server/pm/pkg/AndroidPackage;[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/MiuiDexopt;

.field final synthetic val$pkg:Lcom/android/server/pm/pkg/AndroidPackage;

.field final synthetic val$userId:[I


# direct methods
.method constructor <init>(Lcom/android/server/pm/MiuiDexopt;Lcom/android/server/pm/pkg/AndroidPackage;[I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/MiuiDexopt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 263
    iput-object p1, p0, Lcom/android/server/pm/MiuiDexopt$2;->this$0:Lcom/android/server/pm/MiuiDexopt;

    iput-object p2, p0, Lcom/android/server/pm/MiuiDexopt$2;->val$pkg:Lcom/android/server/pm/pkg/AndroidPackage;

    iput-object p3, p0, Lcom/android/server/pm/MiuiDexopt$2;->val$userId:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 266
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 267
    .local v0, "asyncDexoptBeginTime":J
    iget-object v2, p0, Lcom/android/server/pm/MiuiDexopt$2;->this$0:Lcom/android/server/pm/MiuiDexopt;

    iget-object v3, p0, Lcom/android/server/pm/MiuiDexopt$2;->val$pkg:Lcom/android/server/pm/pkg/AndroidPackage;

    iget-object v4, p0, Lcom/android/server/pm/MiuiDexopt$2;->val$userId:[I

    invoke-static {v2, v3, v4}, Lcom/android/server/pm/MiuiDexopt;->-$$Nest$mprepareDexMetadata(Lcom/android/server/pm/MiuiDexopt;Lcom/android/server/pm/pkg/AndroidPackage;[I)Z

    move-result v2

    .line 268
    .local v2, "success":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    .line 269
    .local v3, "prepareDexMetadataTime":J
    if-eqz v2, :cond_0

    .line 270
    const/16 v5, 0x10

    .line 271
    .local v5, "compilationReason":I
    const/4 v6, 0x5

    .line 273
    .local v6, "dexoptFlags":I
    new-instance v7, Lcom/android/server/pm/dex/DexoptOptions;

    iget-object v8, p0, Lcom/android/server/pm/MiuiDexopt$2;->val$pkg:Lcom/android/server/pm/pkg/AndroidPackage;

    .line 274
    invoke-interface {v8}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x10

    const/4 v10, 0x5

    invoke-direct {v7, v8, v9, v10}, Lcom/android/server/pm/dex/DexoptOptions;-><init>(Ljava/lang/String;II)V

    .line 276
    .local v7, "dexoptOptions":Lcom/android/server/pm/dex/DexoptOptions;
    iget-object v8, p0, Lcom/android/server/pm/MiuiDexopt$2;->this$0:Lcom/android/server/pm/MiuiDexopt;

    invoke-static {v8}, Lcom/android/server/pm/MiuiDexopt;->-$$Nest$fgetmDexOptHelper(Lcom/android/server/pm/MiuiDexopt;)Lcom/android/server/pm/DexOptHelper;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/android/server/pm/DexOptHelper;->performDexOpt(Lcom/android/server/pm/dex/DexoptOptions;)Z

    move-result v2

    .line 278
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v0

    .line 279
    .local v8, "totalTime":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "packageName: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/server/pm/MiuiDexopt$2;->val$pkg:Lcom/android/server/pm/pkg/AndroidPackage;

    invoke-interface {v11}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " prepareDexMetadataTime: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " totalTime: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " asyncDexMetadataDexopt success: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "BaselineProfile"

    invoke-static {v11, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    .end local v5    # "compilationReason":I
    .end local v6    # "dexoptFlags":I
    .end local v7    # "dexoptOptions":Lcom/android/server/pm/dex/DexoptOptions;
    .end local v8    # "totalTime":J
    :cond_0
    return-void
.end method
