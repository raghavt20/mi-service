.class Lcom/android/server/pm/CloudControlPreinstallService$1;
.super Landroid/content/BroadcastReceiver;
.source "CloudControlPreinstallService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/CloudControlPreinstallService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/CloudControlPreinstallService;


# direct methods
.method constructor <init>(Lcom/android/server/pm/CloudControlPreinstallService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/CloudControlPreinstallService;

    .line 230
    iput-object p1, p0, Lcom/android/server/pm/CloudControlPreinstallService$1;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 233
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceive:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudControlPreinstall"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    if-nez p2, :cond_0

    return-void

    .line 236
    :cond_0
    const-string v0, "com.miui.action.SIM_DETECTION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService$1;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/CloudControlPreinstallService;->-$$Nest$fgetmReceivedSIM(Lcom/android/server/pm/CloudControlPreinstallService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService$1;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/pm/CloudControlPreinstallService;->-$$Nest$fputmReceivedSIM(Lcom/android/server/pm/CloudControlPreinstallService;Z)V

    .line 238
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService$1;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/CloudControlPreinstallService;->-$$Nest$muninstallPreinstallAppsDelay(Lcom/android/server/pm/CloudControlPreinstallService;)V

    .line 240
    :cond_1
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService$1;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/CloudControlPreinstallService;->-$$Nest$fgetisUninstallPreinstallApps(Lcom/android/server/pm/CloudControlPreinstallService;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService$1;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/CloudControlPreinstallService;->-$$Nest$muninstallPreinstallAppsDelay(Lcom/android/server/pm/CloudControlPreinstallService;)V

    .line 243
    :cond_2
    return-void
.end method
