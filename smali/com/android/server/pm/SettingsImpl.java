public class com.android.server.pm.SettingsImpl extends com.android.server.pm.SettingsStub {
	 /* .source "SettingsImpl.java" */
	 /* # static fields */
	 private static final java.lang.String ANDROID_INSTALLER;
	 private static final java.lang.String GOOGLE_INSTALLER;
	 private static final java.lang.String MIUI_ACTION_PACKAGE_FIRST_LAUNCH;
	 private static final java.lang.String MIUI_INSTALLER;
	 private static final java.lang.String MIUI_PERMISSION;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static com.android.server.pm.SettingsImpl ( ) {
		 /* .locals 1 */
		 /* .line 25 */
		 /* const-class v0, Lcom/android/server/pm/SettingsImpl; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.pm.SettingsImpl ( ) {
		 /* .locals 0 */
		 /* .line 24 */
		 /* invoke-direct {p0}, Lcom/android/server/pm/SettingsStub;-><init>()V */
		 return;
	 } // .end method
	 private static Boolean findComponent ( java.util.List p0, java.lang.String p1 ) {
		 /* .locals 4 */
		 /* .param p1, "targetComponent" # Ljava/lang/String; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "+", */
		 /* "Lcom/android/server/pm/pkg/component/ParsedMainComponent;", */
		 /* ">;", */
		 /* "Ljava/lang/String;", */
		 /* ")Z" */
		 /* } */
	 } // .end annotation
	 /* .line 35 */
	 /* .local p0, "components":Ljava/util/List;, "Ljava/util/List<+Lcom/android/server/pm/pkg/component/ParsedMainComponent;>;" */
	 int v0 = 0; // const/4 v0, 0x0
	 /* if-nez p0, :cond_0 */
	 /* .line 36 */
} // :cond_0
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/pm/pkg/component/ParsedMainComponent; */
/* .line 37 */
/* .local v2, "component":Lcom/android/server/pm/pkg/component/ParsedMainComponent; */
v3 = android.text.TextUtils .equals ( v3,p1 );
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* .line 38 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 40 */
} // .end local v2 # "component":Lcom/android/server/pm/pkg/component/ParsedMainComponent;
} // :cond_1
/* .line 41 */
} // :cond_2
} // .end method
static void lambda$noftifyFirstLaunch$0 ( com.android.server.pm.pkg.PackageStateInternal p0, Integer p1 ) { //synthethic
/* .locals 22 */
/* .param p0, "ps" # Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .param p1, "userId" # I */
/* .line 94 */
/* move/from16 v1, p1 */
try { // :try_start_0
v0 = com.android.server.pm.SettingsImpl.TAG;
final String v2 = "notify first launch"; // const-string v2, "notify first launch"
android.util.Log .i ( v0,v2 );
/* .line 95 */
/* new-instance v0, Landroid/content/Intent; */
final String v2 = "miui.intent.action.PACKAGE_FIRST_LAUNCH"; // const-string v2, "miui.intent.action.PACKAGE_FIRST_LAUNCH"
/* invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 96 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v2 = "package"; // const-string v2, "package"
/* invoke-interface/range {p0 ..p0}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPackageName()Ljava/lang/String; */
(( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 97 */
/* invoke-interface/range {p0 ..p0}, Lcom/android/server/pm/pkg/PackageStateInternal;->getInstallSource()Lcom/android/server/pm/InstallSource; */
v2 = this.mInstallerPackageName;
v2 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v2, :cond_0 */
/* .line 98 */
final String v2 = "installer"; // const-string v2, "installer"
/* invoke-interface/range {p0 ..p0}, Lcom/android/server/pm/pkg/PackageStateInternal;->getInstallSource()Lcom/android/server/pm/InstallSource; */
v3 = this.mInstallerPackageName;
(( android.content.Intent ) v0 ).putExtra ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 100 */
} // :cond_0
/* const-string/jumbo v2, "userId" */
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 107 */
/* const/high16 v2, 0x1000000 */
(( android.content.Intent ) v0 ).addFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 109 */
/* const-class v2, Lcom/android/server/pm/PackageEventRecorderInternal; */
com.android.server.LocalServices .getService ( v2 );
/* check-cast v2, Lcom/android/server/pm/PackageEventRecorderInternal; */
/* .line 111 */
/* .local v2, "peri":Lcom/android/server/pm/PackageEventRecorderInternal; */
/* invoke-interface/range {p0 ..p0}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPackageName()Ljava/lang/String; */
/* .line 112 */
/* invoke-interface/range {p0 ..p0}, Lcom/android/server/pm/pkg/PackageStateInternal;->getInstallSource()Lcom/android/server/pm/InstallSource; */
v4 = this.mInstallerPackageName;
/* .line 111 */
/* .line 113 */
/* invoke-interface/range {p0 ..p0}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPackageName()Ljava/lang/String; */
int v4 = 1; // const/4 v4, 0x1
/* move-object v12, v3 */
/* .line 115 */
/* .local v12, "activeBundle":Landroid/os/Bundle; */
if ( v12 != null) { // if-eqz v12, :cond_1
/* .line 116 */
(( android.content.Intent ) v0 ).putExtras ( v12 ); // invoke-virtual {v0, v12}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 119 */
} // :cond_1
android.app.ActivityManager .getService ( );
/* .line 120 */
/* .local v3, "am":Landroid/app/IActivityManager; */
/* new-array v4, v4, [Ljava/lang/String; */
/* move-object/from16 v20, v4 */
/* .line 121 */
/* .local v20, "requiredPermissions":[Ljava/lang/String; */
final String v4 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v4, "miui.permission.USE_INTERNAL_GENERAL_API"
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v20, v5 */
/* .line 122 */
int v4 = 0; // const/4 v4, 0x0
final String v5 = "FirstLaunch"; // const-string v5, "FirstLaunch"
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
int v13 = 0; // const/4 v13, 0x0
int v14 = 0; // const/4 v14, 0x0
int v15 = -1; // const/4 v15, -0x1
/* const/16 v16, 0x0 */
/* const/16 v17, 0x0 */
/* const/16 v18, 0x0 */
/* const/16 v19, 0x0 */
/* move-object v6, v0 */
/* move-object/from16 v21, v12 */
} // .end local v12 # "activeBundle":Landroid/os/Bundle;
/* .local v21, "activeBundle":Landroid/os/Bundle; */
/* move-object/from16 v12, v20 */
/* invoke-interface/range {v3 ..v19}, Landroid/app/IActivityManager;->broadcastIntentWithFeature(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/content/IIntentReceiver;ILjava/lang/String;Landroid/os/Bundle;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ILandroid/os/Bundle;ZZI)I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 141 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v2 # "peri":Lcom/android/server/pm/PackageEventRecorderInternal;
} // .end local v3 # "am":Landroid/app/IActivityManager;
} // .end local v20 # "requiredPermissions":[Ljava/lang/String;
} // .end local v21 # "activeBundle":Landroid/os/Bundle;
/* .line 139 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 140 */
/* .local v0, "t":Ljava/lang/Throwable; */
v2 = com.android.server.pm.SettingsImpl.TAG;
final String v3 = "notify first launch exception"; // const-string v3, "notify first launch exception"
android.util.Log .e ( v2,v3,v0 );
/* .line 142 */
} // .end local v0 # "t":Ljava/lang/Throwable;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean checkXSpaceApp ( com.android.server.pm.PackageSetting p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .param p2, "userHandle" # I */
/* .line 45 */
v0 = miui.securityspace.XSpaceUserHandle .isXSpaceUserId ( p2 );
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 46 */
v0 = miui.securityspace.XSpaceConstant.REQUIRED_APPS;
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
v0 = (( java.util.ArrayList ) v0 ).contains ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 47 */
(( com.android.server.pm.PackageSetting ) p1 ).setInstalled ( v2, p2 ); // invoke-virtual {p1, v2, p2}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 49 */
} // :cond_0
(( com.android.server.pm.PackageSetting ) p1 ).setInstalled ( v1, p2 ); // invoke-virtual {p1, v1, p2}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 51 */
} // :goto_0
v0 = miui.securityspace.XSpaceConstant.SPECIAL_APPS;
v0 = (( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 52 */
v0 = miui.securityspace.XSpaceConstant.SPECIAL_APPS;
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
/* check-cast v0, Ljava/util/ArrayList; */
/* .line 53 */
/* .local v0, "requiredComponent":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 54 */
/* .local v1, "components":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/pm/pkg/component/ParsedMainComponent;>;" */
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
(( java.util.ArrayList ) v1 ).addAll ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 55 */
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
(( java.util.ArrayList ) v1 ).addAll ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 56 */
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
(( java.util.ArrayList ) v1 ).addAll ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 57 */
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
(( java.util.ArrayList ) v1 ).addAll ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 58 */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Lcom/android/server/pm/pkg/component/ParsedMainComponent; */
/* .line 59 */
/* .local v4, "component":Lcom/android/server/pm/pkg/component/ParsedMainComponent; */
v5 = (( java.util.ArrayList ) v0 ).contains ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 60 */
/* .line 62 */
} // :cond_1
(( com.android.server.pm.PackageSetting ) p1 ).addDisabledComponent ( v5, p2 ); // invoke-virtual {p1, v5, p2}, Lcom/android/server/pm/PackageSetting;->addDisabledComponent(Ljava/lang/String;I)V
/* .line 63 */
} // .end local v4 # "component":Lcom/android/server/pm/pkg/component/ParsedMainComponent;
/* .line 65 */
} // .end local v0 # "requiredComponent":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v1 # "components":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/pm/pkg/component/ParsedMainComponent;>;"
} // :cond_2
/* .line 68 */
} // :cond_3
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_6 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
/* if-nez v0, :cond_6 */
/* .line 69 */
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
final String v3 = "com.miui.packageinstaller"; // const-string v3, "com.miui.packageinstaller"
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 70 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
/* xor-int/2addr v0, v2 */
(( com.android.server.pm.PackageSetting ) p1 ).setInstalled ( v0, p2 ); // invoke-virtual {p1, v0, p2}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 71 */
/* .line 73 */
} // :cond_4
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
final String v3 = "com.google.android.packageinstaller"; // const-string v3, "com.google.android.packageinstaller"
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 74 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
(( com.android.server.pm.PackageSetting ) p1 ).setInstalled ( v0, p2 ); // invoke-virtual {p1, v0, p2}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 75 */
/* .line 77 */
} // :cond_5
final String v0 = "com.android.packageinstaller"; // const-string v0, "com.android.packageinstaller"
(( com.android.server.pm.PackageSetting ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 78 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
(( com.android.server.pm.PackageSetting ) p1 ).setInstalled ( v0, p2 ); // invoke-virtual {p1, v0, p2}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 79 */
/* .line 83 */
} // :cond_6
} // .end method
public void noftifyFirstLaunch ( com.android.server.pm.PackageManagerService p0, com.android.server.pm.pkg.PackageStateInternal p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p2, "ps" # Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .param p3, "userId" # I */
/* .line 89 */
v0 = if ( p2 != null) { // if-eqz p2, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 92 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/pm/SettingsImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p2, p3}, Lcom/android/server/pm/SettingsImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/pkg/PackageStateInternal;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 143 */
return;
/* .line 90 */
} // :cond_1
} // :goto_0
return;
} // .end method
