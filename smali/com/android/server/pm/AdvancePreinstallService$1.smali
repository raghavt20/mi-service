.class Lcom/android/server/pm/AdvancePreinstallService$1;
.super Landroid/content/BroadcastReceiver;
.source "AdvancePreinstallService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/AdvancePreinstallService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/AdvancePreinstallService;


# direct methods
.method constructor <init>(Lcom/android/server/pm/AdvancePreinstallService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/AdvancePreinstallService;

    .line 128
    iput-object p1, p0, Lcom/android/server/pm/AdvancePreinstallService$1;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceive:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdvancePreinstallService"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    if-nez p2, :cond_0

    .line 133
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService$1;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$fgetmHasReceived(Lcom/android/server/pm/AdvancePreinstallService;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService$1;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$fputmHasReceived(Lcom/android/server/pm/AdvancePreinstallService;Z)V

    .line 137
    const-string v0, "com.miui.action.SIM_DETECTION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService$1;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$mhandleAdvancePreinstallAppsDelay(Lcom/android/server/pm/AdvancePreinstallService;)V

    .line 141
    :cond_1
    return-void
.end method
