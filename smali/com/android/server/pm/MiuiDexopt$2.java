class com.android.server.pm.MiuiDexopt$2 implements java.lang.Runnable {
	 /* .source "MiuiDexopt.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/pm/MiuiDexopt;->asyncDexMetadataDexopt(Lcom/android/server/pm/pkg/AndroidPackage;[I)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.pm.MiuiDexopt this$0; //synthetic
final com.android.server.pm.pkg.AndroidPackage val$pkg; //synthetic
final val$userId; //synthetic
/* # direct methods */
 com.android.server.pm.MiuiDexopt$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/MiuiDexopt; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 263 */
this.this$0 = p1;
this.val$pkg = p2;
this.val$userId = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 12 */
/* .line 266 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 267 */
/* .local v0, "asyncDexoptBeginTime":J */
v2 = this.this$0;
v3 = this.val$pkg;
v4 = this.val$userId;
v2 = com.android.server.pm.MiuiDexopt .-$$Nest$mprepareDexMetadata ( v2,v3,v4 );
/* .line 268 */
/* .local v2, "success":Z */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* sub-long/2addr v3, v0 */
/* .line 269 */
/* .local v3, "prepareDexMetadataTime":J */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 270 */
/* const/16 v5, 0x10 */
/* .line 271 */
/* .local v5, "compilationReason":I */
int v6 = 5; // const/4 v6, 0x5
/* .line 273 */
/* .local v6, "dexoptFlags":I */
/* new-instance v7, Lcom/android/server/pm/dex/DexoptOptions; */
v8 = this.val$pkg;
/* .line 274 */
/* const/16 v9, 0x10 */
int v10 = 5; // const/4 v10, 0x5
/* invoke-direct {v7, v8, v9, v10}, Lcom/android/server/pm/dex/DexoptOptions;-><init>(Ljava/lang/String;II)V */
/* .line 276 */
/* .local v7, "dexoptOptions":Lcom/android/server/pm/dex/DexoptOptions; */
v8 = this.this$0;
com.android.server.pm.MiuiDexopt .-$$Nest$fgetmDexOptHelper ( v8 );
v2 = (( com.android.server.pm.DexOptHelper ) v8 ).performDexOpt ( v7 ); // invoke-virtual {v8, v7}, Lcom/android/server/pm/DexOptHelper;->performDexOpt(Lcom/android/server/pm/dex/DexoptOptions;)Z
/* .line 278 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v8 */
/* sub-long/2addr v8, v0 */
/* .line 279 */
/* .local v8, "totalTime":J */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "packageName: "; // const-string v11, "packageName: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v11 = this.val$pkg;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " prepareDexMetadataTime: "; // const-string v11, " prepareDexMetadataTime: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v3, v4 ); // invoke-virtual {v10, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = " totalTime: "; // const-string v11, " totalTime: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v8, v9 ); // invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = " asyncDexMetadataDexopt success: "; // const-string v11, " asyncDexMetadataDexopt success: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v2 ); // invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v11 = "BaselineProfile"; // const-string v11, "BaselineProfile"
android.util.Slog .d ( v11,v10 );
/* .line 284 */
} // .end local v5 # "compilationReason":I
} // .end local v6 # "dexoptFlags":I
} // .end local v7 # "dexoptOptions":Lcom/android/server/pm/dex/DexoptOptions;
} // .end local v8 # "totalTime":J
} // :cond_0
return;
} // .end method
