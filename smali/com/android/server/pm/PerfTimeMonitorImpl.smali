.class public Lcom/android/server/pm/PerfTimeMonitorImpl;
.super Lcom/android/server/pm/PerfTimeMonitorStub;
.source "PerfTimeMonitorImpl.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "PerfTimeMonitorImpl"


# instance fields
.field private collectingCerts:I

.field private createdAt:J

.field private dexopt:I

.field private extractNativeLib:I

.field private extractNativeLibStartedAt:J

.field private fileCopying:I

.field private googleVerification:I

.field private installer:Ljava/lang/String;

.field private miuiVerification:I

.field private persistAppCount:I

.field private phraseStartedAt:J

.field private pkg:Ljava/lang/String;

.field private systemAppCount:I

.field private thirdAppCount:I

.field private total:I


# direct methods
.method public static synthetic $r8$lambda$YYToNR7Q1q_B2gTpg2XaWvJtIm0(Lcom/android/server/pm/PerfTimeMonitorImpl;Lcom/android/server/pm/Settings;Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageSetting;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/pm/PerfTimeMonitorImpl;->lambda$markPmsScanDetail$0(Lcom/android/server/pm/Settings;Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageSetting;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 18
    invoke-direct {p0}, Lcom/android/server/pm/PerfTimeMonitorStub;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->pkg:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->installer:Ljava/lang/String;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->fileCopying:I

    .line 24
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I

    .line 25
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->miuiVerification:I

    .line 26
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->googleVerification:I

    .line 27
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->dexopt:I

    .line 28
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I

    .line 29
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->createdAt:J

    .line 30
    iput-wide v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    .line 31
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLib:I

    .line 33
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I

    .line 34
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->systemAppCount:I

    .line 35
    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->persistAppCount:I

    .line 36
    iput-wide v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLibStartedAt:J

    return-void
.end method

.method private synthetic lambda$markPmsScanDetail$0(Lcom/android/server/pm/Settings;Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageSetting;)V
    .locals 2
    .param p1, "settings"    # Lcom/android/server/pm/Settings;
    .param p2, "pms"    # Lcom/android/server/pm/PackageManagerService;
    .param p3, "ps"    # Lcom/android/server/pm/PackageSetting;

    .line 175
    invoke-virtual {p3}, Lcom/android/server/pm/PackageSetting;->isSystem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->systemAppCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->systemAppCount:I

    .line 177
    invoke-virtual {p3}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/server/pm/Settings;->isDisabledSystemPackageLPr(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I

    goto :goto_0

    .line 181
    :cond_0
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I

    .line 183
    :cond_1
    :goto_0
    invoke-virtual {p3}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v0

    .line 184
    .local v0, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/android/server/pm/pkg/AndroidPackage;->isPersistent()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService;->getSafeMode()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p3}, Lcom/android/server/pm/PackageSetting;->isSystem()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 185
    :cond_2
    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->persistAppCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->persistAppCount:I

    .line 187
    :cond_3
    return-void
.end method


# virtual methods
.method public dump()V
    .locals 3

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->pkg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->installer:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/server/pm/PerfTimeMonitorImpl;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InstallationTiming"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    return-void
.end method

.method public getCollectingCerts()I
    .locals 1

    .line 147
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I

    return v0
.end method

.method public getDexopt()I
    .locals 1

    .line 162
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->dexopt:I

    return v0
.end method

.method public getFileCopying()I
    .locals 1

    .line 142
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->fileCopying:I

    return v0
.end method

.method public getGoogleVerification()I
    .locals 1

    .line 157
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->googleVerification:I

    return v0
.end method

.method public getMiuiVerification()I
    .locals 1

    .line 152
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->miuiVerification:I

    return v0
.end method

.method public getTotal()I
    .locals 1

    .line 167
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I

    return v0
.end method

.method public isFinished()Z
    .locals 1

    .line 40
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public markPackageOptimized(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/pkg/AndroidPackage;)V
    .locals 3
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;
    .param p2, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;

    .line 200
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    .line 201
    .local v0, "manager":Lmiui/mqsas/sdk/BootEventManager;
    invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v1

    invoke-interface {p2}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v1

    .line 202
    .local v1, "psi":Lcom/android/server/pm/pkg/PackageStateInternal;
    invoke-interface {v1}, Lcom/android/server/pm/pkg/PackageStateInternal;->isSystem()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Lcom/android/server/pm/pkg/PackageStateInternal;->isUpdatedSystemApp()Z

    move-result v2

    if-nez v2, :cond_0

    .line 203
    invoke-virtual {v0}, Lmiui/mqsas/sdk/BootEventManager;->getDexoptSystemAppCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/BootEventManager;->setDexoptSystemAppCount(I)V

    goto :goto_0

    .line 205
    :cond_0
    invoke-virtual {v0}, Lmiui/mqsas/sdk/BootEventManager;->getDexoptThirdAppCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/BootEventManager;->setDexoptThirdAppCount(I)V

    .line 207
    :goto_0
    return-void
.end method

.method public markPmsScanDetail(Lcom/android/server/pm/PackageManagerService;)V
    .locals 3
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;

    .line 172
    iget-object v0, p1, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v0

    .line 173
    :try_start_0
    iget-object v1, p1, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    .line 174
    .local v1, "settings":Lcom/android/server/pm/Settings;
    new-instance v2, Lcom/android/server/pm/PerfTimeMonitorImpl$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v1, p1}, Lcom/android/server/pm/PerfTimeMonitorImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/PerfTimeMonitorImpl;Lcom/android/server/pm/Settings;Lcom/android/server/pm/PackageManagerService;)V

    invoke-virtual {p1, v2}, Lcom/android/server/pm/PackageManagerService;->forEachPackageSetting(Ljava/util/function/Consumer;)V

    .line 188
    .end local v1    # "settings":Lcom/android/server/pm/Settings;
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->systemAppCount:I

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setSystemAppCount(I)V

    .line 191
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setThirdAppCount(I)V

    .line 192
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v0

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->persistAppCount:I

    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setPersistAppCount(I)V

    .line 193
    invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 194
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    nop

    .line 195
    .local v0, "type":I
    invoke-static {}, Lmiui/mqsas/sdk/BootEventManager;->getInstance()Lmiui/mqsas/sdk/BootEventManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/BootEventManager;->setBootType(I)V

    .line 196
    return-void

    .line 188
    .end local v0    # "type":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setCollectCertsFinished()V
    .locals 6

    .line 86
    iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I

    int-to-long v0, v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I

    .line 90
    return-void
.end method

.method public setCollectCertsStarted()V
    .locals 2

    .line 61
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    .line 62
    return-void
.end method

.method public setDexoptFinished()V
    .locals 4

    .line 126
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->dexopt:I

    .line 130
    return-void
.end method

.method public setDexoptStarted()V
    .locals 2

    .line 81
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    .line 82
    return-void
.end method

.method setExtractNativeLibFinished()V
    .locals 4

    .line 94
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLibStartedAt:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLib:I

    .line 98
    return-void
.end method

.method setExtractNativeLibStarted()V
    .locals 2

    .line 66
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLibStartedAt:J

    .line 67
    return-void
.end method

.method public setFileCopied()V
    .locals 4

    .line 102
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->createdAt:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->fileCopying:I

    .line 106
    return-void
.end method

.method public setFinished()V
    .locals 4

    .line 134
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->createdAt:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I

    .line 138
    return-void
.end method

.method public setGoogleVerificationFinished()V
    .locals 4

    .line 118
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->googleVerification:I

    .line 122
    return-void
.end method

.method public setGoogleVerificationStarted()V
    .locals 2

    .line 76
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    .line 77
    return-void
.end method

.method public setInstaller(Ljava/lang/String;)V
    .locals 0
    .param p1, "installer"    # Ljava/lang/String;

    .line 50
    iput-object p1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->installer:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setMiuiVerificationFinished()V
    .locals 4

    .line 110
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->miuiVerification:I

    .line 114
    return-void
.end method

.method public setMiuiVerificationStarted()V
    .locals 2

    .line 71
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J

    .line 72
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "pkg"    # Ljava/lang/String;

    .line 45
    iput-object p1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->pkg:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setStarted(I)V
    .locals 2
    .param p1, "sessionId"    # I

    .line 55
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->createdAt:J

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "beginInstall_sessionId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "InstallationTiming"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .line 216
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I

    .line 217
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->fileCopying:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLib:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->miuiVerification:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->googleVerification:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->dexopt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    filled-new-array/range {v2 .. v8}, [Ljava/lang/Object;

    move-result-object v1

    .line 216
    const-string v2, "%d|%d|%d|%d|%d|%d|%d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
