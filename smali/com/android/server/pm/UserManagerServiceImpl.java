public class com.android.server.pm.UserManagerServiceImpl implements com.android.server.pm.UserManagerServiceStub {
	 /* .source "UserManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer STATUS_CHECKING;
	 private static final Integer STATUS_DONE;
	 private static final Integer STATUS_LOCK_CONTENTION;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static com.android.server.pm.UserManagerServiceImpl ( ) {
		 /* .locals 1 */
		 /* .line 27 */
		 /* const-class v0, Lcom/android/server/pm/UserManagerServiceImpl; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.pm.UserManagerServiceImpl ( ) {
		 /* .locals 0 */
		 /* .line 26 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static void lambda$prepareUserDataWithContentionCheck$0 ( Integer[] p0, Integer p1, Integer p2, com.android.server.pm.Installer p3 ) { //synthethic
		 /* .locals 4 */
		 /* .param p0, "lockContention" # [I */
		 /* .param p1, "userId" # I */
		 /* .param p2, "flags" # I */
		 /* .param p3, "installer" # Lcom/android/server/pm/Installer; */
		 /* .line 72 */
		 /* monitor-enter p0 */
		 /* .line 73 */
		 int v0 = 0; // const/4 v0, 0x0
		 try { // :try_start_0
			 /* aget v1, p0, v0 */
			 /* if-nez v1, :cond_0 */
			 /* .line 74 */
			 int v1 = 1; // const/4 v1, 0x1
			 /* aput v1, p0, v0 */
			 /* .line 75 */
			 v0 = com.android.server.pm.UserManagerServiceImpl.TAG;
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v3 = "Lock contention detected while prepare user data: userId="; // const-string v3, "Lock contention detected while prepare user data: userId="
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 final String v3 = ", flags="; // const-string v3, ", flags="
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 final String v3 = ", disable dexopt"; // const-string v3, ", disable dexopt"
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .i ( v0,v2 );
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 78 */
			 try { // :try_start_1
				 (( com.android.server.pm.Installer ) p3 ).controlDexOptBlocking ( v1 ); // invoke-virtual {p3, v1}, Lcom/android/server/pm/Installer;->controlDexOptBlocking(Z)V
				 /* :try_end_1 */
				 /* .catch Lcom/android/server/pm/Installer$LegacyDexoptDisabledException; {:try_start_1 ..:try_end_1} :catch_0 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* .line 81 */
				 /* .line 79 */
				 /* :catch_0 */
				 /* move-exception v0 */
				 /* .line 80 */
				 /* .local v0, "e":Lcom/android/server/pm/Installer$LegacyDexoptDisabledException; */
				 try { // :try_start_2
					 v1 = com.android.server.pm.UserManagerServiceImpl.TAG;
					 final String v2 = "LegacyDexoptDisabledException"; // const-string v2, "LegacyDexoptDisabledException"
					 android.util.Slog .d ( v1,v2 );
					 /* .line 83 */
				 } // .end local v0 # "e":Lcom/android/server/pm/Installer$LegacyDexoptDisabledException;
			 } // :cond_0
		 } // :goto_0
		 /* monitor-exit p0 */
		 /* .line 84 */
		 return;
		 /* .line 83 */
		 /* :catchall_0 */
		 /* move-exception v0 */
		 /* monitor-exit p0 */
		 /* :try_end_2 */
		 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
		 /* throw v0 */
	 } // .end method
	 /* # virtual methods */
	 public Integer checkAndGetNewUserId ( Integer p0, Integer p1 ) {
		 /* .locals 1 */
		 /* .param p1, "flags" # I */
		 /* .param p2, "defUserId" # I */
		 /* .line 36 */
		 /* const/high16 v0, 0x8000000 */
		 /* and-int/2addr v0, p1 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* const/16 v0, 0x6e */
		 } // :cond_0
		 /* move v0, p2 */
	 } // :goto_0
} // .end method
public Boolean getIsXSpaceUpdate ( android.util.SparseArray p0, java.util.Set p1 ) {
	 /* .locals 4 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Landroid/util/SparseArray<", */
	 /* "Lcom/android/server/pm/UserManagerService$UserData;", */
	 /* ">;", */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;)Z" */
	 /* } */
} // .end annotation
/* .line 116 */
/* .local p1, "mUsers":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/pm/UserManagerService$UserData;>;" */
/* .local p2, "userIdsToWrite":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 117 */
/* .local v0, "isXSpaceUpdate":Z */
com.miui.xspace.XSpaceManagerStub .getInstance ( );
v1 = (( com.miui.xspace.XSpaceManagerStub ) v1 ).getXSpaceUserId ( ); // invoke-virtual {v1}, Lcom/miui/xspace/XSpaceManagerStub;->getXSpaceUserId()I
(( android.util.SparseArray ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/pm/UserManagerService$UserData; */
/* .line 118 */
/* .local v1, "xspaceUserData":Lcom/android/server/pm/UserManagerService$UserData; */
if ( v1 != null) { // if-eqz v1, :cond_0
	 v2 = this.info;
	 v2 = this.userType;
	 final String v3 = "android.os.usertype.profile.CLONE"; // const-string v3, "android.os.usertype.profile.CLONE"
	 /* if-eq v2, v3, :cond_0 */
	 /* .line 119 */
	 v2 = this.info;
	 this.userType = v3;
	 /* .line 120 */
	 v2 = this.info;
	 com.miui.xspace.XSpaceManagerStub .getInstance ( );
	 v3 = 	 (( com.miui.xspace.XSpaceManagerStub ) v3 ).getXSpaceUserFlag ( ); // invoke-virtual {v3}, Lcom/miui/xspace/XSpaceManagerStub;->getXSpaceUserFlag()I
	 /* or-int/lit16 v3, v3, 0x1010 */
	 /* iput v3, v2, Landroid/content/pm/UserInfo;->flags:I */
	 /* .line 121 */
	 v2 = this.info;
	 /* iget v2, v2, Landroid/content/pm/UserInfo;->id:I */
	 java.lang.Integer .valueOf ( v2 );
	 /* .line 122 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 124 */
} // :cond_0
} // .end method
public Boolean isInMaintenanceMode ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 48 */
try { // :try_start_0
	 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 final String v1 = "maintenance_mode_user_id"; // const-string v1, "maintenance_mode_user_id"
	 v0 = 	 android.provider.Settings$Global .getInt ( v0,v1 );
	 /* :try_end_0 */
	 /* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* const/16 v1, 0x6e */
	 /* if-ne v0, v1, :cond_0 */
	 int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 52 */
/* .local v0, "isInMaintenanceMode":Z */
} // :goto_0
/* .line 50 */
} // .end local v0 # "isInMaintenanceMode":Z
/* :catch_0 */
/* move-exception v0 */
/* .line 51 */
/* .local v0, "e":Landroid/provider/Settings$SettingNotFoundException; */
int v1 = 0; // const/4 v1, 0x0
/* move v0, v1 */
/* .line 53 */
/* .local v0, "isInMaintenanceMode":Z */
} // :goto_1
} // .end method
public Boolean prepareUserDataWithContentionCheck ( com.android.server.pm.UserDataPreparer p0, java.lang.Object p1, com.android.server.pm.Installer p2, Integer p3, Integer p4, Integer p5 ) {
/* .locals 9 */
/* .param p1, "userDataPreparer" # Lcom/android/server/pm/UserDataPreparer; */
/* .param p2, "installLock" # Ljava/lang/Object; */
/* .param p3, "installer" # Lcom/android/server/pm/Installer; */
/* .param p4, "userId" # I */
/* .param p5, "userSerial" # I */
/* .param p6, "flags" # I */
/* .line 62 */
v0 = java.lang.Thread .holdsLock ( p2 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 63 */
/* .line 65 */
} // :cond_0
android.os.Looper .myLooper ( );
android.os.Looper .getMainLooper ( );
/* if-ne v0, v2, :cond_1 */
/* .line 66 */
v0 = com.android.server.pm.UserManagerServiceImpl.TAG;
final String v2 = "prepareUserData isn\'t supposed to run on the main thread"; // const-string v2, "prepareUserData isn\'t supposed to run on the main thread"
/* new-instance v3, Ljava/lang/RuntimeException; */
/* invoke-direct {v3}, Ljava/lang/RuntimeException;-><init>()V */
android.util.Log .wtf ( v0,v2,v3 );
/* .line 68 */
/* .line 70 */
} // :cond_1
/* filled-new-array {v1}, [I */
/* .line 71 */
/* .local v0, "lockContention":[I */
/* new-instance v2, Lcom/android/server/pm/UserManagerServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, v0, p4, p6, p3}, Lcom/android/server/pm/UserManagerServiceImpl$$ExternalSyntheticLambda0;-><init>([IIILcom/android/server/pm/Installer;)V */
/* .line 86 */
/* .local v2, "contentionCheckTask":Ljava/lang/Runnable; */
int v3 = 2; // const/4 v3, 0x2
int v4 = 1; // const/4 v4, 0x1
try { // :try_start_0
/* new-instance v5, Landroid/os/Handler; */
android.os.Looper .getMainLooper ( );
/* invoke-direct {v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 88 */
/* .local v5, "handler":Landroid/os/Handler; */
/* if-nez p4, :cond_2 */
/* const/16 v6, 0x1f4 */
} // :cond_2
/* const/16 v6, 0x2710 */
/* .line 89 */
/* .local v6, "timeoutMillis":I */
} // :goto_0
/* int-to-long v7, v6 */
(( android.os.Handler ) v5 ).postDelayed ( v2, v7, v8 ); // invoke-virtual {v5, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 93 */
/* monitor-enter p2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 94 */
try { // :try_start_1
(( android.os.Handler ) v5 ).removeCallbacks ( v2 ); // invoke-virtual {v5, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 95 */
(( com.android.server.pm.UserDataPreparer ) p1 ).prepareUserData ( p4, p5, p6 ); // invoke-virtual {p1, p4, p5, p6}, Lcom/android/server/pm/UserDataPreparer;->prepareUserData(III)V
/* .line 96 */
/* monitor-exit p2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 98 */
} // .end local v5 # "handler":Landroid/os/Handler;
} // .end local v6 # "timeoutMillis":I
/* monitor-enter v0 */
/* .line 99 */
try { // :try_start_2
/* aget v5, v0, v1 */
/* if-ne v5, v4, :cond_3 */
/* .line 100 */
v5 = com.android.server.pm.UserManagerServiceImpl.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Enable dexopt, userId="; // const-string v7, "Enable dexopt, userId="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p4 ); // invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", flags="; // const-string v7, ", flags="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p6 ); // invoke-virtual {v6, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v6 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 102 */
try { // :try_start_3
(( com.android.server.pm.Installer ) p3 ).controlDexOptBlocking ( v1 ); // invoke-virtual {p3, v1}, Lcom/android/server/pm/Installer;->controlDexOptBlocking(Z)V
/* :try_end_3 */
/* .catch Lcom/android/server/pm/Installer$LegacyDexoptDisabledException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 105 */
/* .line 103 */
/* :catch_0 */
/* move-exception v5 */
/* .line 104 */
/* .local v5, "e":Lcom/android/server/pm/Installer$LegacyDexoptDisabledException; */
try { // :try_start_4
v6 = com.android.server.pm.UserManagerServiceImpl.TAG;
final String v7 = "LegacyDexoptDisabledException"; // const-string v7, "LegacyDexoptDisabledException"
android.util.Slog .d ( v6,v7 );
/* .line 107 */
} // .end local v5 # "e":Lcom/android/server/pm/Installer$LegacyDexoptDisabledException;
} // :cond_3
} // :goto_1
/* aput v3, v0, v1 */
/* .line 108 */
/* monitor-exit v0 */
/* .line 109 */
/* nop */
/* .line 110 */
/* .line 108 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v1 */
/* .line 96 */
/* .local v5, "handler":Landroid/os/Handler; */
/* .restart local v6 # "timeoutMillis":I */
/* :catchall_1 */
/* move-exception v7 */
try { // :try_start_5
/* monitor-exit p2 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
} // .end local v0 # "lockContention":[I
} // .end local v2 # "contentionCheckTask":Ljava/lang/Runnable;
} // .end local p0 # "this":Lcom/android/server/pm/UserManagerServiceImpl;
} // .end local p1 # "userDataPreparer":Lcom/android/server/pm/UserDataPreparer;
} // .end local p2 # "installLock":Ljava/lang/Object;
} // .end local p3 # "installer":Lcom/android/server/pm/Installer;
} // .end local p4 # "userId":I
} // .end local p5 # "userSerial":I
} // .end local p6 # "flags":I
try { // :try_start_6
/* throw v7 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* .line 98 */
} // .end local v5 # "handler":Landroid/os/Handler;
} // .end local v6 # "timeoutMillis":I
/* .restart local v0 # "lockContention":[I */
/* .restart local v2 # "contentionCheckTask":Ljava/lang/Runnable; */
/* .restart local p0 # "this":Lcom/android/server/pm/UserManagerServiceImpl; */
/* .restart local p1 # "userDataPreparer":Lcom/android/server/pm/UserDataPreparer; */
/* .restart local p2 # "installLock":Ljava/lang/Object; */
/* .restart local p3 # "installer":Lcom/android/server/pm/Installer; */
/* .restart local p4 # "userId":I */
/* .restart local p5 # "userSerial":I */
/* .restart local p6 # "flags":I */
/* :catchall_2 */
/* move-exception v5 */
/* monitor-enter v0 */
/* .line 99 */
try { // :try_start_7
/* aget v6, v0, v1 */
/* if-ne v6, v4, :cond_4 */
/* .line 100 */
v4 = com.android.server.pm.UserManagerServiceImpl.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Enable dexopt, userId="; // const-string v7, "Enable dexopt, userId="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p4 ); // invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", flags="; // const-string v7, ", flags="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p6 ); // invoke-virtual {v6, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v4,v6 );
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* .line 102 */
try { // :try_start_8
(( com.android.server.pm.Installer ) p3 ).controlDexOptBlocking ( v1 ); // invoke-virtual {p3, v1}, Lcom/android/server/pm/Installer;->controlDexOptBlocking(Z)V
/* :try_end_8 */
/* .catch Lcom/android/server/pm/Installer$LegacyDexoptDisabledException; {:try_start_8 ..:try_end_8} :catch_1 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_3 */
/* .line 105 */
/* .line 103 */
/* :catch_1 */
/* move-exception v4 */
/* .line 104 */
/* .local v4, "e":Lcom/android/server/pm/Installer$LegacyDexoptDisabledException; */
try { // :try_start_9
v6 = com.android.server.pm.UserManagerServiceImpl.TAG;
final String v7 = "LegacyDexoptDisabledException"; // const-string v7, "LegacyDexoptDisabledException"
android.util.Slog .d ( v6,v7 );
/* .line 107 */
} // .end local v4 # "e":Lcom/android/server/pm/Installer$LegacyDexoptDisabledException;
} // :cond_4
} // :goto_2
/* aput v3, v0, v1 */
/* .line 108 */
/* monitor-exit v0 */
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_3 */
/* .line 109 */
/* throw v5 */
/* .line 108 */
/* :catchall_3 */
/* move-exception v1 */
try { // :try_start_a
/* monitor-exit v0 */
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_3 */
/* throw v1 */
} // .end method
