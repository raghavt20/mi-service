.class public Lcom/android/server/pm/DexoptServiceThread;
.super Ljava/lang/Thread;
.source "DexoptServiceThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;
    }
.end annotation


# static fields
.field public static final HOST_NAME:Ljava/lang/String; = "mi_dexopt"

.field public static final SOCKET_BUFFER_SIZE:I = 0x100

.field public static final TAG:Ljava/lang/String; = "DexoptServiceThread"

.field private static mWaitLock:Ljava/lang/Object;

.field private static volatile sAsynDexOptHandler:Landroid/os/Handler;

.field private static sAsynDexOptLock:Ljava/lang/Object;

.field private static sAsynDexOptThread:Landroid/os/HandlerThread;


# instance fields
.field private mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

.field private mDexoptPackageNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDexoptResult:I

.field public mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDexoptSecondaryResult:I

.field private mPdo:Lcom/android/server/pm/PackageDexOptimizer;

.field private mPms:Lcom/android/server/pm/PackageManagerService;

.field public secondaryId:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmDexoptPackageNameList(Lcom/android/server/pm/DexoptServiceThread;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptPackageNameList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDexoptResult(Lcom/android/server/pm/DexoptServiceThread;)I
    .locals 0

    iget p0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDexoptSecondaryResult(Lcom/android/server/pm/DexoptServiceThread;)I
    .locals 0

    iget p0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryResult:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPdo(Lcom/android/server/pm/DexoptServiceThread;)Lcom/android/server/pm/PackageDexOptimizer;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/DexoptServiceThread;->mPdo:Lcom/android/server/pm/PackageDexOptimizer;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDexoptResult(Lcom/android/server/pm/DexoptServiceThread;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDexoptSecondaryResult(Lcom/android/server/pm/DexoptServiceThread;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryResult:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mperformDexOptInternal(Lcom/android/server/pm/DexoptServiceThread;Lcom/android/server/pm/dex/DexoptOptions;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/DexoptServiceThread;->performDexOptInternal(Lcom/android/server/pm/dex/DexoptOptions;)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetmWaitLock()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/server/pm/DexoptServiceThread;->mWaitLock:Ljava/lang/Object;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/pm/DexoptServiceThread;->mWaitLock:Ljava/lang/Object;

    .line 154
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/pm/DexoptServiceThread;->sAsynDexOptLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageDexOptimizer;)V
    .locals 2
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;
    .param p2, "pdo"    # Lcom/android/server/pm/PackageDexOptimizer;

    .line 46
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I

    .line 36
    iput v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I

    .line 37
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptPackageNameList:Ljava/util/List;

    .line 43
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;

    .line 44
    iput v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryResult:I

    .line 47
    iput-object p1, p0, Lcom/android/server/pm/DexoptServiceThread;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 48
    iput-object p2, p0, Lcom/android/server/pm/DexoptServiceThread;->mPdo:Lcom/android/server/pm/PackageDexOptimizer;

    .line 49
    new-instance v0, Lcom/android/server/pm/DexOptHelper;

    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-direct {v0, v1}, Lcom/android/server/pm/DexOptHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V

    iput-object v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

    .line 50
    return-void
.end method

.method private static getAsynDexOptHandler()Landroid/os/Handler;
    .locals 3

    .line 156
    sget-object v0, Lcom/android/server/pm/DexoptServiceThread;->sAsynDexOptHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 157
    sget-object v0, Lcom/android/server/pm/DexoptServiceThread;->sAsynDexOptLock:Ljava/lang/Object;

    monitor-enter v0

    .line 158
    :try_start_0
    sget-object v1, Lcom/android/server/pm/DexoptServiceThread;->sAsynDexOptHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 159
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "asyn_dexopt_thread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/android/server/pm/DexoptServiceThread;->sAsynDexOptThread:Landroid/os/HandlerThread;

    .line 160
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 161
    new-instance v1, Landroid/os/Handler;

    sget-object v2, Lcom/android/server/pm/DexoptServiceThread;->sAsynDexOptThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/android/server/pm/DexoptServiceThread;->sAsynDexOptHandler:Landroid/os/Handler;

    .line 163
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 165
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/pm/DexoptServiceThread;->sAsynDexOptHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private performDexOptInternal(Lcom/android/server/pm/dex/DexoptOptions;)I
    .locals 6
    .param p1, "options"    # Lcom/android/server/pm/dex/DexoptOptions;

    .line 209
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 210
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v2, "performDexOptInternal"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Lcom/android/server/pm/dex/DexoptOptions;

    aput-object v5, v4, v0

    .line 211
    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 212
    .local v2, "declaredMethod":Ljava/lang/reflect/Method;
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 213
    iget-object v3, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 214
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v2    # "declaredMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 215
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DexoptServiceThread"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    return v0
.end method

.method private waitDexOptRequest()V
    .locals 9

    .line 169
    sget-object v0, Lcom/android/server/pm/DexoptServiceThread;->mWaitLock:Ljava/lang/Object;

    monitor-enter v0

    .line 170
    :try_start_0
    const-string v1, "DexoptServiceThread"

    const-string v2, "Start to wait dexopt result."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    .local v1, "startTime":J
    :try_start_1
    sget-object v3, Lcom/android/server/pm/DexoptServiceThread;->mWaitLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    goto :goto_0

    .line 174
    :catch_0
    move-exception v3

    .line 175
    .local v3, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 176
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 178
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 179
    .local v3, "endTime":J
    const-string v5, "DexoptServiceThread"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Dexopt finished, waiting time("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v7, v3, v1

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms), result:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    nop

    .end local v1    # "startTime":J
    .end local v3    # "endTime":J
    monitor-exit v0

    .line 182
    return-void

    .line 181
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method public dexOptSecondaryDexPath(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;ILcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)I
    .locals 10
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "secondaryId"    # I
    .param p4, "dexUseInfo"    # Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;
    .param p5, "options"    # Lcom/android/server/pm/dex/DexoptOptions;

    .line 252
    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/Class;

    .line 253
    .local v1, "arg":[Ljava/lang/Class;
    const-class v2, Landroid/content/pm/ApplicationInfo;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 254
    const-class v2, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 255
    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    .line 256
    const-class v2, Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    .line 257
    const-class v2, Lcom/android/server/pm/dex/DexoptOptions;

    const/4 v7, 0x4

    aput-object v2, v1, v7

    .line 259
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread;->mPdo:Lcom/android/server/pm/PackageDexOptimizer;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 260
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    const-string v8, "dexOptSecondaryDexPath"

    .line 261
    invoke-virtual {v2, v8, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v8

    .line 262
    .local v8, "declaredMethod":Ljava/lang/reflect/Method;
    invoke-virtual {v8, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 263
    iget-object v9, p0, Lcom/android/server/pm/DexoptServiceThread;->mPdo:Lcom/android/server/pm/PackageDexOptimizer;

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v3

    aput-object p2, v0, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v5

    aput-object p4, v0, v6

    aput-object p5, v0, v7

    invoke-virtual {v8, v9, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 264
    .end local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
    .end local v8    # "declaredMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "DexoptServiceThread"

    invoke-static {v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    return v3
.end method

.method public getDexOptResult()I
    .locals 1

    .line 275
    iget v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I

    return v0
.end method

.method public getDexoptSecondaryResult()I
    .locals 1

    .line 271
    iget v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryResult:I

    return v0
.end method

.method public performDexOptAsyncTask(Lcom/android/server/pm/dex/DexoptOptions;)V
    .locals 2
    .param p1, "options"    # Lcom/android/server/pm/dex/DexoptOptions;

    .line 185
    new-instance v0, Lcom/android/server/pm/DexoptServiceThread$1;

    invoke-direct {v0, p0, p1}, Lcom/android/server/pm/DexoptServiceThread$1;-><init>(Lcom/android/server/pm/DexoptServiceThread;Lcom/android/server/pm/dex/DexoptOptions;)V

    .line 203
    .local v0, "dexoptTask":Ljava/lang/Runnable;
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->getAsynDexOptHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 204
    invoke-direct {p0}, Lcom/android/server/pm/DexoptServiceThread;->waitDexOptRequest()V

    .line 205
    return-void
.end method

.method public performDexOptSecondary(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V
    .locals 7
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "dexUseInfo"    # Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;
    .param p4, "options"    # Lcom/android/server/pm/dex/DexoptOptions;

    .line 222
    new-instance v6, Lcom/android/server/pm/DexoptServiceThread$2;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/server/pm/DexoptServiceThread$2;-><init>(Lcom/android/server/pm/DexoptServiceThread;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V

    .line 246
    .local v0, "task":Ljava/lang/Runnable;
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->getAsynDexOptHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 247
    invoke-direct {p0}, Lcom/android/server/pm/DexoptServiceThread;->waitDexOptRequest()V

    .line 248
    return-void
.end method

.method public run()V
    .locals 7

    .line 54
    const-string v0, "mi_dexopt connection finally shutdown."

    const-string v1, "DexoptServiceThread"

    const/4 v2, 0x0

    .line 55
    .local v2, "serverSocket":Landroid/net/LocalServerSocket;
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    .line 57
    .local v3, "threadExecutor":Ljava/util/concurrent/ExecutorService;
    :try_start_0
    const-string v4, "Create local socket: mi_dexopt"

    invoke-static {v1, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    new-instance v4, Landroid/net/LocalServerSocket;

    const-string v5, "mi_dexopt"

    invoke-direct {v4, v5}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    move-object v2, v4

    .line 60
    :goto_0
    const-string v4, "Waiting dexopt client connected..."

    invoke-static {v1, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-virtual {v2}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v4

    .line 62
    .local v4, "clientSocket":Landroid/net/LocalSocket;
    const/16 v5, 0x100

    invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->setReceiveBufferSize(I)V

    .line 63
    invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->setSendBufferSize(I)V

    .line 64
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "There is a dexopt client is accepted:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    new-instance v5, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;

    invoke-direct {v5, p0, v4}, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;-><init>(Lcom/android/server/pm/DexoptServiceThread;Landroid/net/LocalSocket;)V

    invoke-interface {v3, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    .end local v4    # "clientSocket":Landroid/net/LocalSocket;
    goto :goto_0

    .line 70
    :catchall_0
    move-exception v4

    goto :goto_3

    .line 67
    :catch_0
    move-exception v4

    .line 68
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mi_dexopt connection catch Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    nop

    .end local v4    # "e":Ljava/lang/Exception;
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 72
    :cond_0
    if-eqz v2, :cond_1

    .line 74
    :try_start_2
    invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 77
    :goto_1
    goto :goto_2

    .line 75
    :catch_1
    move-exception v0

    .line 76
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 80
    :cond_1
    :goto_2
    return-void

    .line 70
    :goto_3
    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 72
    :cond_2
    if-eqz v2, :cond_3

    .line 74
    :try_start_3
    invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 77
    goto :goto_4

    .line 75
    :catch_2
    move-exception v0

    .line 76
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 79
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    :goto_4
    throw v4
.end method
