public class com.android.server.pm.CloudControlPreinstallService extends com.android.server.SystemService {
	 /* .source "CloudControlPreinstallService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;, */
	 /* Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;, */
	 /* Lcom/android/server/pm/CloudControlPreinstallService$PackageDeleteObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BEGIN_UNINSTALL;
public static final java.lang.String CLAUSE_AGREED;
public static final java.lang.String CLAUSE_AGREED_ACTION;
private static final java.lang.String CONF_ID;
private static final Boolean DEBUG;
private static final Integer DEFAULT_BIND_DELAY;
private static final Integer DEFAULT_CONNECT_TIME_OUT;
private static final Integer DEFAULT_READ_TIME_OUT;
private static final java.lang.String IMEI_MD5;
private static final java.lang.String JSON_EXCEPTION;
private static final java.lang.String NETWORK_TYPE;
private static final java.lang.String OFFLINE_COUNT;
private static final java.lang.String PACKAGE_NAME;
private static final java.lang.String PREINSTALL_CONFIG;
private static final java.lang.String REGION;
private static final java.lang.String REMOVE_FROM_LIST_BEGIN;
private static final java.lang.String REQUEST_CONNECT_EXCEPTION;
private static final java.lang.String SALESE_CHANNEL;
private static final java.lang.String SERVER_ADDRESS;
private static final java.lang.String SERVER_ADDRESS_GLOBAL;
private static final java.lang.String SIM_DETECTION_ACTION;
private static final java.lang.String SKU;
private static final java.lang.String TAG;
private static final java.lang.String TRACK_EVENT_NAME;
private static final java.lang.String UNINSTALL_FAILED;
private static final java.lang.String UNINSTALL_SUCCESS;
/* # instance fields */
private Boolean isUninstallPreinstallApps;
private org.json.JSONObject mConfigObj;
private java.lang.String mImeiMe5;
private java.lang.String mNetworkType;
private Boolean mReceivedSIM;
private android.content.BroadcastReceiver mReceiver;
private com.android.server.pm.PreInstallServiceTrack mTrack;
/* # direct methods */
static Boolean -$$Nest$fgetisUninstallPreinstallApps ( com.android.server.pm.CloudControlPreinstallService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->isUninstallPreinstallApps:Z */
} // .end method
static Boolean -$$Nest$fgetmReceivedSIM ( com.android.server.pm.CloudControlPreinstallService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mReceivedSIM:Z */
} // .end method
static void -$$Nest$fputmReceivedSIM ( com.android.server.pm.CloudControlPreinstallService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mReceivedSIM:Z */
	 return;
} // .end method
static java.util.List -$$Nest$mgetUninstallApps ( com.android.server.pm.CloudControlPreinstallService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getUninstallApps()Ljava/util/List; */
} // .end method
static void -$$Nest$mrecordUnInstallApps ( com.android.server.pm.CloudControlPreinstallService p0, java.util.List p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/pm/CloudControlPreinstallService;->recordUnInstallApps(Ljava/util/List;)V */
	 return;
} // .end method
static void -$$Nest$muninstallAppsUpdateList ( com.android.server.pm.CloudControlPreinstallService p0, java.util.List p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/pm/CloudControlPreinstallService;->uninstallAppsUpdateList(Ljava/util/List;)V */
	 return;
} // .end method
static void -$$Nest$muninstallPreinstallAppsDelay ( com.android.server.pm.CloudControlPreinstallService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->uninstallPreinstallAppsDelay()V */
	 return;
} // .end method
public com.android.server.pm.CloudControlPreinstallService ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 268 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
	 /* .line 230 */
	 /* new-instance v0, Lcom/android/server/pm/CloudControlPreinstallService$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/pm/CloudControlPreinstallService$1;-><init>(Lcom/android/server/pm/CloudControlPreinstallService;)V */
	 this.mReceiver = v0;
	 /* .line 269 */
	 return;
} // .end method
private void addPkgNameToCloudControlUninstallMap ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p1, "packageName" # Ljava/lang/String; */
	 /* .line 361 */
	 com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
	 v0 = 	 (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 362 */
		 v0 = com.android.server.pm.MiuiBusinessPreinstallConfig.sCloudControlUninstall;
		 (( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
		 /* .line 364 */
	 } // :cond_0
	 v0 = com.android.server.pm.PreinstallApp.sCloudControlUninstall;
	 (( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
	 /* .line 366 */
} // :goto_0
return;
} // .end method
private void closeBufferedReader ( java.io.BufferedReader p0 ) {
/* .locals 1 */
/* .param p1, "br" # Ljava/io/BufferedReader; */
/* .line 561 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 563 */
	 try { // :try_start_0
		 (( java.io.BufferedReader ) p1 ).close ( ); // invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 566 */
		 /* .line 564 */
		 /* :catch_0 */
		 /* move-exception v0 */
		 /* .line 565 */
		 /* .local v0, "e":Ljava/io/IOException; */
		 (( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
		 /* .line 568 */
	 } // .end local v0 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
return;
} // .end method
public static Boolean exists ( android.content.pm.PackageManager p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p0, "pm" # Landroid/content/pm/PackageManager; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 628 */
/* const/16 v0, 0x80 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
(( android.content.pm.PackageManager ) p0 ).getApplicationInfo ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 630 */
/* :catch_0 */
/* move-exception v0 */
/* .line 631 */
/* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
} // .end method
private java.lang.String findTrackingApk ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 369 */
v0 = this.mConfigObj;
/* if-nez v0, :cond_0 */
/* .line 370 */
final String v0 = "/cust/etc/cust_apps_config"; // const-string v0, "/cust/etc/cust_apps_config"
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).getFileContent ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->getFileContent(Ljava/lang/String;)Ljava/lang/String;
/* .line 371 */
/* .local v0, "config":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 372 */
/* new-instance v1, Lorg/json/JSONObject; */
/* invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
this.mConfigObj = v1;
/* .line 376 */
} // .end local v0 # "config":Ljava/lang/String;
} // :cond_0
v0 = this.mConfigObj;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 377 */
final String v2 = "data"; // const-string v2, "data"
(( org.json.JSONObject ) v0 ).getJSONArray ( v2 ); // invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 378 */
/* .local v0, "array":Lorg/json/JSONArray; */
v2 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-gtz v2, :cond_1 */
/* .line 379 */
/* .line 381 */
} // :cond_1
v2 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* .line 382 */
/* .local v2, "count":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v2, :cond_3 */
/* .line 383 */
(( org.json.JSONArray ) v0 ).getJSONObject ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 384 */
/* .local v4, "obj":Lorg/json/JSONObject; */
if ( v4 != null) { // if-eqz v4, :cond_2
final String v5 = "packageName"; // const-string v5, "packageName"
(( org.json.JSONObject ) v4 ).getString ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v5 = android.text.TextUtils .equals ( v5,p1 );
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 385 */
/* const-string/jumbo v1, "trackApkPackageName" */
(( org.json.JSONObject ) v4 ).getString ( v1 ); // invoke-virtual {v4, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 382 */
} // .end local v4 # "obj":Lorg/json/JSONObject;
} // :cond_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 390 */
} // .end local v0 # "array":Lorg/json/JSONArray;
} // .end local v2 # "count":I
} // .end local v3 # "i":I
} // :cond_3
} // .end method
private java.lang.String getAddress ( ) {
/* .locals 1 */
/* .line 465 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 466 */
final String v0 = "https://control.preload.xiaomi.com/offline_app_list/get?"; // const-string v0, "https://control.preload.xiaomi.com/offline_app_list/get?"
/* .line 468 */
} // :cond_0
final String v0 = "https://global.control.preload.xiaomi.com/offline_app_list/get?"; // const-string v0, "https://global.control.preload.xiaomi.com/offline_app_list/get?"
} // .end method
private java.lang.String getChannel ( ) {
/* .locals 2 */
/* .line 458 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 459 */
miui.os.Build .getCustVariant ( );
/* .line 461 */
} // :cond_0
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
final String v1 = "Public Version"; // const-string v1, "Public Version"
android.os.SystemProperties .get ( v0,v1 );
} // .end method
private com.android.server.pm.CloudControlPreinstallService$ConnectEntity getConnectEntity ( ) {
/* .locals 24 */
/* .line 408 */
/* move-object/from16 v1, p0 */
int v2 = 0; // const/4 v2, 0x0
/* .line 410 */
/* .local v2, "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
try { // :try_start_0
final String v0 = "get_device_info"; // const-string v0, "get_device_info"
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 411 */
miui.telephony.TelephonyManager .getDefault ( );
(( miui.telephony.TelephonyManager ) v0 ).getImeiList ( ); // invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getImeiList()Ljava/util/List;
java.util.Collections .min ( v0 );
/* check-cast v0, Ljava/lang/String; */
/* .line 413 */
/* .local v0, "imei":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v3, :cond_0 */
com.android.server.pm.CloudSignUtil .md5 ( v0 );
} // :cond_0
final String v3 = ""; // const-string v3, ""
} // :goto_0
this.mImeiMe5 = v3;
/* .line 415 */
v5 = miui.os.Build.DEVICE;
/* .line 416 */
/* .local v5, "device":Ljava/lang/String; */
v6 = android.os.Build$VERSION.INCREMENTAL;
/* .line 417 */
/* .local v6, "miuiVersion":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getChannel()Ljava/lang/String; */
/* .line 418 */
/* .local v7, "channel":Ljava/lang/String; */
java.util.Locale .getDefault ( );
(( java.util.Locale ) v3 ).getLanguage ( ); // invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
/* .line 419 */
/* .local v10, "lang":Ljava/lang/String; */
com.android.server.pm.CloudSignUtil .getNonceStr ( );
/* .line 420 */
/* .local v11, "nonceStr":Ljava/lang/String; */
/* sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* xor-int/lit8 v3, v3, 0x1 */
/* .line 421 */
/* .local v3, "isCn":Z */
if ( v3 != null) { // if-eqz v3, :cond_1
final String v4 = "CN"; // const-string v4, "CN"
} // :cond_1
miui.os.Build .getCustVariant ( );
(( java.lang.String ) v4 ).toUpperCase ( ); // invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
} // :goto_1
/* move-object v8, v4 */
/* .line 422 */
/* .local v8, "region":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getSku()Ljava/lang/String; */
/* .line 423 */
/* .local v12, "sku":Ljava/lang/String; */
v4 = this.mImeiMe5;
/* move v9, v3 */
/* invoke-static/range {v4 ..v12}, Lcom/android/server/pm/CloudSignUtil;->getSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 425 */
/* .local v20, "sign":Ljava/lang/String; */
v4 = /* invoke-static/range {v20 ..v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v4 != null) { // if-eqz v4, :cond_2
int v4 = 0; // const/4 v4, 0x0
/* .line 426 */
} // :cond_2
/* new-instance v4, Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
v14 = this.mImeiMe5;
/* move-object v13, v4 */
/* move-object v15, v5 */
/* move-object/from16 v16, v6 */
/* move-object/from16 v17, v7 */
/* move-object/from16 v18, v10 */
/* move-object/from16 v19, v11 */
/* move/from16 v21, v3 */
/* move-object/from16 v22, v8 */
/* move-object/from16 v23, v12 */
/* invoke-direct/range {v13 ..v23}, Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/util/NoSuchElementException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
} // :goto_2
/* move-object v2, v4 */
/* .line 431 */
} // .end local v0 # "imei":Ljava/lang/String;
} // .end local v3 # "isCn":Z
} // .end local v5 # "device":Ljava/lang/String;
} // .end local v6 # "miuiVersion":Ljava/lang/String;
} // .end local v7 # "channel":Ljava/lang/String;
} // .end local v8 # "region":Ljava/lang/String;
} // .end local v10 # "lang":Ljava/lang/String;
} // .end local v11 # "nonceStr":Ljava/lang/String;
} // .end local v12 # "sku":Ljava/lang/String;
} // .end local v20 # "sign":Ljava/lang/String;
} // :goto_3
/* .line 429 */
/* :catch_0 */
/* move-exception v0 */
/* .line 430 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 427 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* .line 428 */
/* .local v0, "ex":Ljava/util/NoSuchElementException; */
(( java.util.NoSuchElementException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/util/NoSuchElementException;->printStackTrace()V
} // .end local v0 # "ex":Ljava/util/NoSuchElementException;
/* .line 433 */
} // :goto_4
/* if-nez v2, :cond_3 */
/* .line 434 */
final String v0 = "get_device_info_failed"; // const-string v0, "get_device_info_failed"
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 437 */
} // :cond_3
} // .end method
private java.lang.String getSku ( ) {
/* .locals 5 */
/* .line 441 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
final String v1 = ""; // const-string v1, ""
/* if-nez v0, :cond_0 */
/* .line 442 */
/* .line 444 */
} // :cond_0
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
android.os.SystemProperties .get ( v0,v1 );
/* .line 445 */
/* .local v0, "buildRegion":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
final String v2 = "MI"; // const-string v2, "MI"
/* if-nez v1, :cond_2 */
/* .line 446 */
(( java.lang.String ) v0 ).toUpperCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
final String v3 = "GLOBAL"; // const-string v3, "GLOBAL"
v1 = android.text.TextUtils .equals ( v1,v3 );
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_1
(( java.lang.String ) v0 ).toUpperCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
} // :goto_0
/* .line 448 */
} // :cond_2
/* sget-boolean v1, Lmiui/os/Build;->IS_STABLE_VERSION:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 449 */
v1 = android.os.Build$VERSION.INCREMENTAL;
/* .line 450 */
/* .local v1, "buildVersion":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v3, :cond_3 */
v3 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
int v4 = 4; // const/4 v4, 0x4
/* if-le v3, v4, :cond_3 */
/* .line 451 */
v2 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* sub-int/2addr v2, v4 */
v3 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* add-int/lit8 v3, v3, -0x2 */
(( java.lang.String ) v1 ).substring ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.String ) v2 ).toUpperCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 454 */
} // .end local v1 # "buildVersion":Ljava/lang/String;
} // :cond_3
} // .end method
private java.util.List getUninstallApps ( ) {
/* .locals 22 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 472 */
/* move-object/from16 v1, p0 */
final String v2 = "request_connect_exception"; // const-string v2, "request_connect_exception"
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v3, v0 */
/* .line 474 */
/* .local v3, "uninstallApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;>;" */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getConnectEntity()Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .line 476 */
/* .local v4, "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* if-nez v4, :cond_0 */
/* .line 478 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getAddress()Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.CloudControlPreinstallService$ConnectEntity ) v4 ).toString ( ); // invoke-virtual {v4}, Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 480 */
/* .local v5, "url":Ljava/lang/String; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "url:" */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "CloudControlPreinstall"; // const-string v6, "CloudControlPreinstall"
android.util.Slog .i ( v6,v0 );
/* .line 482 */
int v7 = 0; // const/4 v7, 0x0
/* .line 483 */
/* .local v7, "conn":Ljava/net/HttpURLConnection; */
int v8 = 0; // const/4 v8, 0x0
/* .line 486 */
/* .local v8, "br":Ljava/io/BufferedReader; */
try { // :try_start_0
final String v0 = "request_connect_start"; // const-string v0, "request_connect_start"
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 488 */
/* new-instance v0, Ljava/net/URL; */
/* invoke-direct {v0, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V */
(( java.net.URL ) v0 ).openConnection ( ); // invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
/* check-cast v0, Ljava/net/HttpURLConnection; */
/* move-object v7, v0 */
/* .line 489 */
/* const/16 v0, 0x7d0 */
(( java.net.HttpURLConnection ) v7 ).setConnectTimeout ( v0 ); // invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V
/* .line 490 */
(( java.net.HttpURLConnection ) v7 ).setReadTimeout ( v0 ); // invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
/* .line 491 */
final String v0 = "GET"; // const-string v0, "GET"
(( java.net.HttpURLConnection ) v7 ).setRequestMethod ( v0 ); // invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
/* .line 492 */
(( java.net.HttpURLConnection ) v7 ).connect ( ); // invoke-virtual {v7}, Ljava/net/HttpURLConnection;->connect()V
/* .line 494 */
v0 = (( java.net.HttpURLConnection ) v7 ).getResponseCode ( ); // invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I
/* .line 495 */
/* .local v0, "responeCode":I */
/* const/16 v9, 0xc8 */
/* if-ne v0, v9, :cond_4 */
/* .line 496 */
final String v9 = "request_connect_success"; // const-string v9, "request_connect_success"
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v9 ); // invoke-virtual {v1, v9}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 498 */
/* new-instance v9, Ljava/io/BufferedReader; */
/* new-instance v10, Ljava/io/InputStreamReader; */
(( java.net.HttpURLConnection ) v7 ).getInputStream ( ); // invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
/* invoke-direct {v10, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* const/16 v11, 0x400 */
/* invoke-direct {v9, v10, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V */
/* :try_end_0 */
/* .catch Ljava/net/ProtocolException; {:try_start_0 ..:try_end_0} :catch_1b */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1a */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_19 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_18 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_5 */
/* move-object v8, v9 */
/* .line 499 */
try { // :try_start_1
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 501 */
/* .local v9, "sb":Ljava/lang/StringBuilder; */
} // :goto_0
(( java.io.BufferedReader ) v8 ).readLine ( ); // invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_1 */
/* .catch Ljava/net/ProtocolException; {:try_start_1 ..:try_end_1} :catch_13 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_12 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_11 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_10 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_4 */
/* move-object v11, v10 */
/* .local v11, "line":Ljava/lang/String; */
if ( v10 != null) { // if-eqz v10, :cond_1
/* .line 502 */
try { // :try_start_2
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_2 */
/* .catch Ljava/net/ProtocolException; {:try_start_2 ..:try_end_2} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catch Lorg/json/JSONException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 550 */
} // .end local v0 # "responeCode":I
} // .end local v9 # "sb":Ljava/lang/StringBuilder;
} // .end local v11 # "line":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* goto/16 :goto_9 */
/* .line 546 */
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* goto/16 :goto_4 */
/* .line 543 */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* goto/16 :goto_5 */
/* .line 540 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* goto/16 :goto_6 */
/* .line 537 */
/* :catch_3 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* goto/16 :goto_7 */
/* .line 505 */
/* .restart local v0 # "responeCode":I */
/* .restart local v9 # "sb":Ljava/lang/StringBuilder; */
/* .restart local v11 # "line":Ljava/lang/String; */
} // :cond_1
try { // :try_start_3
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "result:"; // const-string v12, "result:"
(( java.lang.StringBuilder ) v10 ).append ( v12 ); // invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v12 ); // invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v10 );
/* .line 506 */
/* new-instance v10, Lorg/json/JSONObject; */
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v10, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 508 */
/* .local v10, "result":Lorg/json/JSONObject; */
final String v12 = "code"; // const-string v12, "code"
v12 = (( org.json.JSONObject ) v10 ).getInt ( v12 ); // invoke-virtual {v10, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 509 */
/* .local v12, "code":I */
final String v13 = "message"; // const-string v13, "message"
(( org.json.JSONObject ) v10 ).getString ( v13 ); // invoke-virtual {v10, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 510 */
/* .local v13, "message":Ljava/lang/String; */
/* if-nez v12, :cond_3 */
final String v14 = "Success"; // const-string v14, "Success"
v14 = (( java.lang.String ) v14 ).equals ( v13 ); // invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_3
/* .line 511 */
final String v6 = "request_list_success"; // const-string v6, "request_list_success"
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v6 ); // invoke-virtual {v1, v6}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 513 */
final String v6 = "data"; // const-string v6, "data"
(( org.json.JSONObject ) v10 ).getJSONObject ( v6 ); // invoke-virtual {v10, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* .line 514 */
/* .local v6, "data":Lorg/json/JSONObject; */
final String v14 = "appList"; // const-string v14, "appList"
(( org.json.JSONObject ) v6 ).getJSONArray ( v14 ); // invoke-virtual {v6, v14}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 515 */
/* .local v14, "appList":Lorg/json/JSONArray; */
final String v15 = "channel"; // const-string v15, "channel"
(( org.json.JSONObject ) v6 ).getString ( v15 ); // invoke-virtual {v6, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* :try_end_3 */
/* .catch Ljava/net/ProtocolException; {:try_start_3 ..:try_end_3} :catch_13 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_12 */
/* .catch Lorg/json/JSONException; {:try_start_3 ..:try_end_3} :catch_11 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_10 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_4 */
/* .line 517 */
/* .local v15, "channelResult":Ljava/lang/String; */
/* const/16 v16, 0x0 */
/* move/from16 v17, v0 */
/* move/from16 v0, v16 */
/* .local v0, "index":I */
/* .local v17, "responeCode":I */
} // :goto_1
/* move-object/from16 v16, v4 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
/* .local v16, "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
try { // :try_start_4
v4 = (( org.json.JSONArray ) v14 ).length ( ); // invoke-virtual {v14}, Lorg/json/JSONArray;->length()I
/* if-ge v0, v4, :cond_2 */
/* .line 518 */
(( org.json.JSONArray ) v14 ).getJSONObject ( v0 ); // invoke-virtual {v14, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* :try_end_4 */
/* .catch Ljava/net/ProtocolException; {:try_start_4 ..:try_end_4} :catch_b */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_a */
/* .catch Lorg/json/JSONException; {:try_start_4 ..:try_end_4} :catch_9 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_8 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* .line 520 */
/* .local v4, "appinfo":Lorg/json/JSONObject; */
/* move-object/from16 v18, v5 */
} // .end local v5 # "url":Ljava/lang/String;
/* .local v18, "url":Ljava/lang/String; */
try { // :try_start_5
final String v5 = "packageName"; // const-string v5, "packageName"
(( org.json.JSONObject ) v4 ).getString ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 521 */
/* .local v5, "pkgName":Ljava/lang/String; */
/* move-object/from16 v19, v6 */
} // .end local v6 # "data":Lorg/json/JSONObject;
/* .local v19, "data":Lorg/json/JSONObject; */
final String v6 = "confId"; // const-string v6, "confId"
v6 = (( org.json.JSONObject ) v4 ).getInt ( v6 ); // invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* :try_end_5 */
/* .catch Ljava/net/ProtocolException; {:try_start_5 ..:try_end_5} :catch_7 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_6 */
/* .catch Lorg/json/JSONException; {:try_start_5 ..:try_end_5} :catch_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_4 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 522 */
/* .local v6, "confId":I */
/* move-object/from16 v20, v8 */
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .local v20, "br":Ljava/io/BufferedReader; */
try { // :try_start_6
final String v8 = "offlineCount"; // const-string v8, "offlineCount"
v8 = (( org.json.JSONObject ) v4 ).getInt ( v8 ); // invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 523 */
/* .local v8, "offlineCount":I */
/* move-object/from16 v21, v4 */
} // .end local v4 # "appinfo":Lorg/json/JSONObject;
/* .local v21, "appinfo":Lorg/json/JSONObject; */
/* new-instance v4, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
/* invoke-direct {v4, v5, v15, v6, v8}, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;-><init>(Ljava/lang/String;Ljava/lang/String;II)V */
/* .line 517 */
/* nop */
} // .end local v5 # "pkgName":Ljava/lang/String;
} // .end local v6 # "confId":I
} // .end local v8 # "offlineCount":I
} // .end local v21 # "appinfo":Lorg/json/JSONObject;
/* add-int/lit8 v0, v0, 0x1 */
/* move-object/from16 v4, v16 */
/* move-object/from16 v5, v18 */
/* move-object/from16 v6, v19 */
/* move-object/from16 v8, v20 */
/* .line 550 */
} // .end local v0 # "index":I
} // .end local v9 # "sb":Ljava/lang/StringBuilder;
} // .end local v10 # "result":Lorg/json/JSONObject;
} // .end local v11 # "line":Ljava/lang/String;
} // .end local v12 # "code":I
} // .end local v13 # "message":Ljava/lang/String;
} // .end local v14 # "appList":Lorg/json/JSONArray;
} // .end local v15 # "channelResult":Ljava/lang/String;
} // .end local v17 # "responeCode":I
} // .end local v19 # "data":Lorg/json/JSONObject;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .local v8, "br":Ljava/io/BufferedReader; */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v20, v8 */
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_9 */
/* .line 546 */
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_4 */
/* move-exception v0 */
/* move-object/from16 v20, v8 */
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_4 */
/* .line 543 */
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_5 */
/* move-exception v0 */
/* move-object/from16 v20, v8 */
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_5 */
/* .line 540 */
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_6 */
/* move-exception v0 */
/* move-object/from16 v20, v8 */
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_6 */
/* .line 537 */
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_7 */
/* move-exception v0 */
/* move-object/from16 v20, v8 */
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_7 */
/* .line 517 */
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v0 # "index":I */
/* .local v5, "url":Ljava/lang/String; */
/* .local v6, "data":Lorg/json/JSONObject; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* .restart local v9 # "sb":Ljava/lang/StringBuilder; */
/* .restart local v10 # "result":Lorg/json/JSONObject; */
/* .restart local v11 # "line":Ljava/lang/String; */
/* .restart local v12 # "code":I */
/* .restart local v13 # "message":Ljava/lang/String; */
/* .restart local v14 # "appList":Lorg/json/JSONArray; */
/* .restart local v15 # "channelResult":Ljava/lang/String; */
/* .restart local v17 # "responeCode":I */
} // :cond_2
/* move-object/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* move-object/from16 v20, v8 */
/* .line 526 */
} // .end local v0 # "index":I
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v6 # "data":Lorg/json/JSONObject;
} // .end local v8 # "br":Ljava/io/BufferedReader;
} // .end local v14 # "appList":Lorg/json/JSONArray;
} // .end local v15 # "channelResult":Ljava/lang/String;
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* .line 550 */
} // .end local v9 # "sb":Ljava/lang/StringBuilder;
} // .end local v10 # "result":Lorg/json/JSONObject;
} // .end local v11 # "line":Ljava/lang/String;
} // .end local v12 # "code":I
} // .end local v13 # "message":Ljava/lang/String;
} // .end local v17 # "responeCode":I
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_9 */
/* .line 546 */
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_8 */
/* move-exception v0 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_4 */
/* .line 543 */
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_9 */
/* move-exception v0 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_5 */
/* .line 540 */
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_a */
/* move-exception v0 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_6 */
/* .line 537 */
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_b */
/* move-exception v0 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_7 */
/* .line 510 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .local v0, "responeCode":I */
/* .local v4, "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* .restart local v9 # "sb":Ljava/lang/StringBuilder; */
/* .restart local v10 # "result":Lorg/json/JSONObject; */
/* .restart local v11 # "line":Ljava/lang/String; */
/* .restart local v12 # "code":I */
/* .restart local v13 # "message":Ljava/lang/String; */
} // :cond_3
/* move/from16 v17, v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
/* .line 527 */
} // .end local v0 # "responeCode":I
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v17 # "responeCode":I */
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
final String v0 = "request_list_failed"; // const-string v0, "request_list_failed"
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 529 */
final String v0 = "request result is failed"; // const-string v0, "request result is failed"
android.util.Slog .e ( v6,v0 );
/* :try_end_6 */
/* .catch Ljava/net/ProtocolException; {:try_start_6 ..:try_end_6} :catch_f */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_e */
/* .catch Lorg/json/JSONException; {:try_start_6 ..:try_end_6} :catch_d */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_c */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_3 */
/* .line 532 */
} // .end local v9 # "sb":Ljava/lang/StringBuilder;
} // .end local v10 # "result":Lorg/json/JSONObject;
} // .end local v11 # "line":Ljava/lang/String;
} // .end local v12 # "code":I
} // .end local v13 # "message":Ljava/lang/String;
} // :goto_2
/* move-object/from16 v8, v20 */
/* .line 550 */
} // .end local v17 # "responeCode":I
/* :catchall_3 */
/* move-exception v0 */
/* move-object/from16 v8, v20 */
/* goto/16 :goto_9 */
/* .line 546 */
/* :catch_c */
/* move-exception v0 */
/* move-object/from16 v8, v20 */
/* goto/16 :goto_4 */
/* .line 543 */
/* :catch_d */
/* move-exception v0 */
/* move-object/from16 v8, v20 */
/* goto/16 :goto_5 */
/* .line 540 */
/* :catch_e */
/* move-exception v0 */
/* move-object/from16 v8, v20 */
/* goto/16 :goto_6 */
/* .line 537 */
/* :catch_f */
/* move-exception v0 */
/* move-object/from16 v8, v20 */
/* goto/16 :goto_7 */
/* .line 550 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catchall_4 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* goto/16 :goto_9 */
/* .line 546 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_10 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* .line 543 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_11 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* .line 540 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_12 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* .line 537 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
/* :catch_13 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v8 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
} // .end local v8 # "br":Ljava/io/BufferedReader;
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
/* .restart local v20 # "br":Ljava/io/BufferedReader; */
/* .line 533 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
} // .end local v20 # "br":Ljava/io/BufferedReader;
/* .restart local v0 # "responeCode":I */
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* .restart local v8 # "br":Ljava/io/BufferedReader; */
} // :cond_4
/* move/from16 v17, v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
} // .end local v0 # "responeCode":I
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v17 # "responeCode":I */
/* .restart local v18 # "url":Ljava/lang/String; */
try { // :try_start_7
final String v0 = "request_connect_failed"; // const-string v0, "request_connect_failed"
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 535 */
/* const-string/jumbo v0, "server can not connected" */
android.util.Slog .i ( v6,v0 );
/* :try_end_7 */
/* .catch Ljava/net/ProtocolException; {:try_start_7 ..:try_end_7} :catch_17 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_16 */
/* .catch Lorg/json/JSONException; {:try_start_7 ..:try_end_7} :catch_15 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_14 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_6 */
/* .line 550 */
} // .end local v17 # "responeCode":I
} // :goto_3
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 551 */
/* .line 546 */
/* :catch_14 */
/* move-exception v0 */
/* .line 543 */
/* :catch_15 */
/* move-exception v0 */
/* .line 540 */
/* :catch_16 */
/* move-exception v0 */
/* .line 537 */
/* :catch_17 */
/* move-exception v0 */
/* .line 550 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* :catchall_5 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
/* .line 546 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* :catch_18 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* .line 547 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
} // :goto_4
try { // :try_start_8
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 548 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 550 */
} // .end local v0 # "e":Ljava/lang/Exception;
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 551 */
/* .line 543 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* :catch_19 */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* .line 544 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
/* .local v0, "e":Lorg/json/JSONException; */
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
} // :goto_5
final String v2 = "json_exception"; // const-string v2, "json_exception"
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 545 */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 550 */
} // .end local v0 # "e":Lorg/json/JSONException;
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 551 */
/* .line 550 */
/* :catchall_6 */
/* move-exception v0 */
/* .line 540 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* :catch_1a */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* .line 541 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
/* .local v0, "e":Ljava/io/IOException; */
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
} // :goto_6
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 542 */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 550 */
} // .end local v0 # "e":Ljava/io/IOException;
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 551 */
/* .line 537 */
} // .end local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v18 # "url":Ljava/lang/String;
/* .restart local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v5 # "url":Ljava/lang/String; */
/* :catch_1b */
/* move-exception v0 */
/* move-object/from16 v16, v4 */
/* move-object/from16 v18, v5 */
/* .line 538 */
} // .end local v4 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
} // .end local v5 # "url":Ljava/lang/String;
/* .local v0, "e":Ljava/net/ProtocolException; */
/* .restart local v16 # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity; */
/* .restart local v18 # "url":Ljava/lang/String; */
} // :goto_7
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 539 */
(( java.net.ProtocolException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/net/ProtocolException;->printStackTrace()V
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_6 */
/* .line 550 */
} // .end local v0 # "e":Ljava/net/ProtocolException;
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 551 */
} // :goto_8
(( java.net.HttpURLConnection ) v7 ).disconnect ( ); // invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 554 */
} // :cond_5
/* invoke-direct {v1, v8}, Lcom/android/server/pm/CloudControlPreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 555 */
/* nop */
/* .line 557 */
/* .line 550 */
} // :goto_9
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 551 */
(( java.net.HttpURLConnection ) v7 ).disconnect ( ); // invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 554 */
} // :cond_6
/* invoke-direct {v1, v8}, Lcom/android/server/pm/CloudControlPreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 555 */
/* throw v0 */
} // .end method
private void initOneTrack ( ) {
/* .locals 2 */
/* .line 141 */
/* new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack; */
/* invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack;-><init>()V */
this.mTrack = v0;
/* .line 142 */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;
(( com.android.server.pm.PreInstallServiceTrack ) v0 ).bindTrackService ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/pm/PreInstallServiceTrack;->bindTrackService(Landroid/content/Context;)V
/* .line 143 */
return;
} // .end method
private Boolean isNetworkConnected ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 247 */
int v0 = 0; // const/4 v0, 0x0
/* .line 250 */
/* .local v0, "isAvailable":Z */
try { // :try_start_0
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/net/ConnectivityManager; */
/* .line 251 */
/* .local v1, "connectivityManager":Landroid/net/ConnectivityManager; */
(( android.net.ConnectivityManager ) v1 ).getActiveNetworkInfo ( ); // invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
/* .line 253 */
/* .local v2, "networkInfo":Landroid/net/NetworkInfo; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 254 */
(( android.net.NetworkInfo ) v2 ).getTypeName ( ); // invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;
this.mNetworkType = v3;
/* .line 255 */
v3 = (( android.net.NetworkInfo ) v2 ).isAvailable ( ); // invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z
/* move v0, v3 */
/* .line 257 */
} // :cond_0
final String v3 = ""; // const-string v3, ""
this.mNetworkType = v3;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 262 */
} // .end local v1 # "connectivityManager":Landroid/net/ConnectivityManager;
} // .end local v2 # "networkInfo":Landroid/net/NetworkInfo;
} // :goto_0
/* .line 260 */
/* :catch_0 */
/* move-exception v1 */
/* .line 261 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 264 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static Boolean isProvisioned ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 661 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 662 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* const-string/jumbo v1, "user_setup_complete" */
int v2 = 0; // const/4 v2, 0x0
v1 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
private void recordUnInstallApps ( java.util.List p0 ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 336 */
/* .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;>;" */
/* const-string/jumbo v0, "uninstall_success" */
v1 = if ( p1 != null) { // if-eqz p1, :cond_2
/* if-nez v1, :cond_2 */
/* .line 337 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
/* .line 338 */
/* .local v2, "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 339 */
final String v3 = "remove_from_list_begin"; // const-string v3, "remove_from_list_begin"
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v3, v2 ); // invoke-virtual {p0, v3, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 340 */
final String v4 = "begin_uninstall"; // const-string v4, "begin_uninstall"
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v4, v2 ); // invoke-virtual {p0, v4, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 341 */
v5 = this.packageName;
/* invoke-direct {p0, v5}, Lcom/android/server/pm/CloudControlPreinstallService;->addPkgNameToCloudControlUninstallMap(Ljava/lang/String;)V */
/* .line 343 */
try { // :try_start_0
v5 = this.packageName;
/* invoke-direct {p0, v5}, Lcom/android/server/pm/CloudControlPreinstallService;->findTrackingApk(Ljava/lang/String;)Ljava/lang/String; */
/* .line 344 */
/* .local v5, "trackingApk":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v6, :cond_0 */
/* .line 345 */
/* new-instance v6, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
int v7 = 0; // const/4 v7, 0x0
int v8 = -1; // const/4 v8, -0x1
/* invoke-direct {v6, v5, v7, v8, v8}, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;-><init>(Ljava/lang/String;Ljava/lang/String;II)V */
/* .line 346 */
/* .local v6, "tracking":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v3, v6 ); // invoke-virtual {p0, v3, v6}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 347 */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v4, v6 ); // invoke-virtual {p0, v4, v6}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 348 */
/* invoke-direct {p0, v5}, Lcom/android/server/pm/CloudControlPreinstallService;->addPkgNameToCloudControlUninstallMap(Ljava/lang/String;)V */
/* .line 349 */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v0, v6 ); // invoke-virtual {p0, v0, v6}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 353 */
} // .end local v5 # "trackingApk":Ljava/lang/String;
} // .end local v6 # "tracking":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
} // :cond_0
/* .line 351 */
/* :catch_0 */
/* move-exception v3 */
/* .line 352 */
/* .local v3, "e":Lorg/json/JSONException; */
final String v4 = "CloudControlPreinstall"; // const-string v4, "CloudControlPreinstall"
(( org.json.JSONException ) v3 ).getMessage ( ); // invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* .line 354 */
} // .end local v3 # "e":Lorg/json/JSONException;
} // :goto_1
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 356 */
} // .end local v2 # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
} // :cond_1
/* .line 358 */
} // :cond_2
return;
} // .end method
public static void startCloudControlService ( ) {
/* .locals 2 */
/* .line 272 */
/* const-class v0, Lcom/android/server/SystemServiceManager; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/SystemServiceManager; */
/* .line 274 */
/* .local v0, "systemServiceManager":Lcom/android/server/SystemServiceManager; */
/* const-class v1, Lcom/android/server/pm/CloudControlPreinstallService; */
(( com.android.server.SystemServiceManager ) v0 ).startService ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;
/* .line 275 */
/* const-class v1, Lcom/android/server/pm/AdvancePreinstallService; */
(( com.android.server.SystemServiceManager ) v0 ).startService ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;
/* .line 276 */
return;
} // .end method
private void track ( com.android.server.pm.PreInstallServiceTrack$Action p0 ) {
/* .locals 3 */
/* .param p1, "action" # Lcom/android/server/pm/PreInstallServiceTrack$Action; */
/* .line 146 */
v0 = this.mTrack;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
(( com.android.server.pm.PreInstallServiceTrack$Action ) p1 ).getContent ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 148 */
v0 = this.mTrack;
(( com.android.server.pm.PreInstallServiceTrack$Action ) p1 ).getContent ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;
(( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.pm.PreInstallServiceTrack ) v0 ).trackEvent ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack;->trackEvent(Ljava/lang/String;I)V
/* .line 149 */
(( com.android.server.pm.PreInstallServiceTrack$Action ) p1 ).getContent ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
final String v1 = "CloudControlPreinstall"; // const-string v1, "CloudControlPreinstall"
android.util.Slog .i ( v1,v0 );
/* .line 151 */
} // :cond_0
return;
} // .end method
private void uninstallAppsUpdateList ( java.util.List p0 ) {
/* .locals 10 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 572 */
/* .local p1, "uninstallApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_8
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_5 */
/* .line 577 */
} // :cond_0
final String v0 = "ro.miui.cust_variant"; // const-string v0, "ro.miui.cust_variant"
android.os.SystemProperties .get ( v0 );
/* .line 579 */
/* .local v0, "currentCustVariant":Ljava/lang/String; */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 581 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
/* if-nez v1, :cond_1 */
return;
/* .line 584 */
} // :cond_1
v3 = } // :goto_0
final String v4 = " failed:"; // const-string v4, " failed:"
int v5 = 2; // const/4 v5, 0x2
final String v6 = "CloudControlPreinstall"; // const-string v6, "CloudControlPreinstall"
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
/* .line 585 */
/* .local v3, "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
v7 = this.custVariant;
v7 = android.text.TextUtils .equals ( v0,v7 );
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 587 */
final String v7 = "remove_from_list_begin"; // const-string v7, "remove_from_list_begin"
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v7, v3 ); // invoke-virtual {p0, v7, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 589 */
com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
v7 = (( com.android.server.pm.MiuiPreinstallHelper ) v7 ).isSupportNewFrame ( ); // invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 590 */
com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
(( com.android.server.pm.MiuiPreinstallHelper ) v7 ).getBusinessPreinstallConfig ( ); // invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
v8 = this.packageName;
/* .line 591 */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v7 ).removeFromPreinstallList ( v8 ); // invoke-virtual {v7, v8}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->removeFromPreinstallList(Ljava/lang/String;)V
/* .line 593 */
} // :cond_2
v7 = this.packageName;
com.android.server.pm.PreinstallApp .removeFromPreinstallList ( v7 );
/* .line 597 */
} // :goto_1
try { // :try_start_0
v7 = this.packageName;
int v8 = 0; // const/4 v8, 0x0
(( android.content.pm.PackageManager ) v1 ).setApplicationEnabledSetting ( v7, v5, v8 ); // invoke-virtual {v1, v7, v5, v8}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 601 */
/* .line 599 */
/* :catch_0 */
/* move-exception v5 */
/* .line 600 */
/* .local v5, "e":Ljava/lang/Exception; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "disable Package "; // const-string v8, "disable Package "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.packageName;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v4 );
/* .line 603 */
} // .end local v3 # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
} // .end local v5 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_2
/* .line 606 */
} // :cond_4
v3 = } // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_7
/* check-cast v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
/* .line 607 */
/* .restart local v3 # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
v7 = this.custVariant;
v7 = android.text.TextUtils .equals ( v0,v7 );
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 608 */
v7 = this.packageName;
v7 = com.android.server.pm.CloudControlPreinstallService .exists ( v1,v7 );
/* if-nez v7, :cond_5 */
/* .line 609 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Package "; // const-string v8, "Package "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.packageName;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " not exist"; // const-string v8, " not exist"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v7 );
/* .line 611 */
final String v7 = "package_not_exist"; // const-string v7, "package_not_exist"
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v7, v3 ); // invoke-virtual {p0, v7, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 614 */
} // :cond_5
try { // :try_start_1
final String v7 = "begin_uninstall"; // const-string v7, "begin_uninstall"
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v7, v3 ); // invoke-virtual {p0, v7, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 615 */
v7 = this.packageName;
/* new-instance v8, Lcom/android/server/pm/CloudControlPreinstallService$PackageDeleteObserver; */
/* invoke-direct {v8, p0, v3}, Lcom/android/server/pm/CloudControlPreinstallService$PackageDeleteObserver;-><init>(Lcom/android/server/pm/CloudControlPreinstallService;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V */
(( android.content.pm.PackageManager ) v1 ).deletePackage ( v7, v8, v5 ); // invoke-virtual {v1, v7, v8, v5}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 620 */
/* .line 616 */
/* :catch_1 */
/* move-exception v7 */
/* .line 617 */
/* .local v7, "e":Ljava/lang/Exception; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "uninstall Package " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.packageName;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v8 );
/* .line 619 */
/* const-string/jumbo v8, "uninstall_failed" */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v8, v3 ); // invoke-virtual {p0, v8, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 623 */
} // .end local v3 # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
} // .end local v7 # "e":Ljava/lang/Exception;
} // :cond_6
} // :goto_4
/* .line 624 */
} // :cond_7
return;
/* .line 573 */
} // .end local v0 # "currentCustVariant":Ljava/lang/String;
} // .end local v1 # "pm":Landroid/content/pm/PackageManager;
} // :cond_8
} // :goto_5
/* const-string/jumbo v0, "uninstall_list_empty" */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).trackEvent ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 574 */
return;
} // .end method
private void uninstallPreinstallAppsDelay ( ) {
/* .locals 4 */
/* .line 299 */
/* invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->initOneTrack()V */
/* .line 300 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
/* new-instance v1, Lcom/android/server/pm/CloudControlPreinstallService$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/pm/CloudControlPreinstallService$2;-><init>(Lcom/android/server/pm/CloudControlPreinstallService;)V */
/* const-wide/16 v2, 0x1f4 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 306 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getFileContent ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 394 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 395 */
/* .local v0, "file":Ljava/io/File; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 396 */
/* .local v1, "in":Ljava/io/FileInputStream; */
try { // :try_start_1
(( java.io.File ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/io/File;->length()J
/* move-result-wide v2 */
/* .line 397 */
/* .local v2, "length":J */
/* long-to-int v4, v2 */
/* new-array v4, v4, [B */
/* .line 398 */
/* .local v4, "content":[B */
(( java.io.FileInputStream ) v1 ).read ( v4 ); // invoke-virtual {v1, v4}, Ljava/io/FileInputStream;->read([B)I
/* .line 399 */
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* .line 400 */
/* new-instance v5, Ljava/lang/String; */
v6 = java.nio.charset.StandardCharsets.UTF_8;
/* invoke-direct {v5, v4, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 401 */
try { // :try_start_2
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 400 */
/* .line 395 */
} // .end local v2 # "length":J
} // .end local v4 # "content":[B
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/pm/CloudControlPreinstallService;
} // .end local p1 # "filePath":Ljava/lang/String;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 401 */
} // .end local v1 # "in":Ljava/io/FileInputStream;
/* .restart local v0 # "file":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/pm/CloudControlPreinstallService; */
/* .restart local p1 # "filePath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 402 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "CloudControlPreinstall"; // const-string v2, "CloudControlPreinstall"
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 404 */
} // .end local v1 # "e":Ljava/lang/Exception;
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "phase" # I */
/* .line 285 */
/* const/16 v0, 0x1f4 */
/* if-ne p1, v0, :cond_1 */
/* .line 286 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onBootPhase:"; // const-string v1, "onBootPhase:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CloudControlPreinstall"; // const-string v1, "CloudControlPreinstall"
android.util.Slog .d ( v1,v0 );
/* .line 287 */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;
v0 = com.android.server.pm.CloudControlPreinstallService .isProvisioned ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 289 */
} // :cond_0
final String v0 = "onBootPhase:register broadcast receiver"; // const-string v0, "onBootPhase:register broadcast receiver"
android.util.Slog .d ( v1,v0 );
/* .line 291 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 292 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
final String v1 = "com.miui.action.SIM_DETECTION"; // const-string v1, "com.miui.action.SIM_DETECTION"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 293 */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 294 */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;
v2 = this.mReceiver;
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 296 */
} // .end local v0 # "intentFilter":Landroid/content/IntentFilter;
} // :cond_1
return;
} // .end method
public void onStart ( ) {
/* .locals 2 */
/* .line 280 */
final String v0 = "CloudControlPreinstall"; // const-string v0, "CloudControlPreinstall"
final String v1 = "onStart"; // const-string v1, "onStart"
android.util.Slog .i ( v0,v1 );
/* .line 281 */
return;
} // .end method
public void trackEvent ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "event" # Ljava/lang/String; */
/* .line 125 */
/* new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$Action; */
/* invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V */
/* .line 126 */
/* .local v0, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action; */
final String v1 = "saleschannels"; // const-string v1, "saleschannels"
/* invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getChannel()Ljava/lang/String; */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 128 */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* xor-int/lit8 v1, v1, 0x1 */
/* .line 129 */
/* .local v1, "isCn":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v2 = "CN"; // const-string v2, "CN"
} // :cond_0
miui.os.Build .getCustVariant ( );
(( java.lang.String ) v2 ).toUpperCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 130 */
/* .local v2, "region":Ljava/lang/String; */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getSku()Ljava/lang/String; */
/* .line 131 */
/* .local v3, "sku":Ljava/lang/String; */
final String v4 = "region"; // const-string v4, "region"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 132 */
/* const-string/jumbo v4, "sku" */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 134 */
final String v4 = "imeiMd5"; // const-string v4, "imeiMd5"
v5 = this.mImeiMe5;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 135 */
final String v4 = "networkType"; // const-string v4, "networkType"
v5 = this.mNetworkType;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 136 */
final String v4 = "EVENT_NAME"; // const-string v4, "EVENT_NAME"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, p1 ); // invoke-virtual {v0, v4, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 137 */
/* invoke-direct {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V */
/* .line 138 */
return;
} // .end method
public void trackEvent ( java.lang.String p0, com.android.server.pm.CloudControlPreinstallService$UninstallApp p1 ) {
/* .locals 5 */
/* .param p1, "event" # Ljava/lang/String; */
/* .param p2, "app" # Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
/* .line 102 */
/* new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$Action; */
/* invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V */
/* .line 103 */
/* .local v0, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action; */
final String v1 = "packageName"; // const-string v1, "packageName"
v2 = this.packageName;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 104 */
final String v1 = "confId"; // const-string v1, "confId"
/* iget v2, p2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->confId:I */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 105 */
final String v1 = "offlineCount"; // const-string v1, "offlineCount"
/* iget v2, p2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->offlineCount:I */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 106 */
final String v1 = "saleschannels"; // const-string v1, "saleschannels"
/* invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getChannel()Ljava/lang/String; */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 107 */
v1 = this.packageName;
v1 = miui.os.MiuiInit .isPreinstalledPackage ( v1 );
java.lang.String .valueOf ( v1 );
final String v2 = "isPreinstalled"; // const-string v2, "isPreinstalled"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 108 */
v1 = this.packageName;
miui.os.MiuiInit .getMiuiChannelPath ( v1 );
java.lang.String .valueOf ( v1 );
final String v2 = "miuiChannelPath"; // const-string v2, "miuiChannelPath"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 110 */
final String v1 = "imeiMd5"; // const-string v1, "imeiMd5"
v2 = this.mImeiMe5;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 111 */
final String v1 = "networkType"; // const-string v1, "networkType"
v2 = this.mNetworkType;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 113 */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* xor-int/lit8 v1, v1, 0x1 */
/* .line 114 */
/* .local v1, "isCn":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v2 = "CN"; // const-string v2, "CN"
} // :cond_0
miui.os.Build .getCustVariant ( );
(( java.lang.String ) v2 ).toUpperCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 115 */
/* .local v2, "region":Ljava/lang/String; */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getSku()Ljava/lang/String; */
/* .line 116 */
/* .local v3, "sku":Ljava/lang/String; */
final String v4 = "region"; // const-string v4, "region"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 117 */
/* const-string/jumbo v4, "sku" */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 118 */
final String v4 = "EVENT_NAME"; // const-string v4, "EVENT_NAME"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, p1 ); // invoke-virtual {v0, v4, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 120 */
/* invoke-direct {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V */
/* .line 121 */
return;
} // .end method
public void uninstallPreinstallApps ( ) {
/* .locals 2 */
/* .line 309 */
/* const-string/jumbo v0, "uninstallPreinstallApps" */
final String v1 = "CloudControlPreinstall"; // const-string v1, "CloudControlPreinstall"
android.util.Slog .d ( v1,v0 );
/* .line 311 */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;
v0 = com.android.server.pm.CloudControlPreinstallService .isProvisioned ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 313 */
} // :cond_0
/* const-string/jumbo v0, "uninstallPreinstallApps:isProvisioned true" */
android.util.Slog .d ( v1,v0 );
/* .line 315 */
(( com.android.server.pm.CloudControlPreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->isNetworkConnected(Landroid/content/Context;)Z */
/* if-nez v0, :cond_1 */
return;
/* .line 316 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->isUninstallPreinstallApps:Z */
/* .line 317 */
/* const-string/jumbo v0, "uninstallPreinstallApps:isNetworkConnected true" */
android.util.Slog .d ( v1,v0 );
/* .line 319 */
/* new-instance v0, Lcom/android/server/pm/CloudControlPreinstallService$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/pm/CloudControlPreinstallService$3;-><init>(Lcom/android/server/pm/CloudControlPreinstallService;)V */
android.os.AsyncTask .execute ( v0 );
/* .line 333 */
return;
} // .end method
