class com.android.server.pm.CloudControlPreinstallService$3 implements java.lang.Runnable {
	 /* .source "CloudControlPreinstallService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/pm/CloudControlPreinstallService;->uninstallPreinstallApps()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.pm.CloudControlPreinstallService this$0; //synthetic
/* # direct methods */
 com.android.server.pm.CloudControlPreinstallService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/CloudControlPreinstallService; */
/* .line 319 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 2 */
/* .line 323 */
v0 = this.this$0;
final String v1 = "cloud_uninstall_start"; // const-string v1, "cloud_uninstall_start"
(( com.android.server.pm.CloudControlPreinstallService ) v0 ).trackEvent ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V
/* .line 324 */
v0 = this.this$0;
com.android.server.pm.CloudControlPreinstallService .-$$Nest$mgetUninstallApps ( v0 );
/* .line 326 */
/* .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;>;" */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 327 */
	 v1 = this.this$0;
	 com.android.server.pm.CloudControlPreinstallService .-$$Nest$mrecordUnInstallApps ( v1,v0 );
	 /* .line 329 */
} // :cond_0
v1 = this.this$0;
com.android.server.pm.CloudControlPreinstallService .-$$Nest$muninstallAppsUpdateList ( v1,v0 );
/* .line 331 */
} // :goto_0
return;
} // .end method
