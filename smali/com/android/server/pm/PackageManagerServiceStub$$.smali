.class public final Lcom/android/server/pm/PackageManagerServiceStub$$;
.super Ljava/lang/Object;
.source "PackageManagerServiceStub$$.java"

# interfaces
.implements Lcom/miui/base/MiuiStubRegistry$ImplProviderManifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final collectImplProviders(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/base/MiuiStubRegistry$ImplProvider<",
            "*>;>;)V"
        }
    .end annotation

    .line 19
    .local p1, "outProviders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/miui/base/MiuiStubRegistry$ImplProvider<*>;>;"
    new-instance v0, Lcom/android/server/pm/PerfTimeMonitorImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/PerfTimeMonitorImpl$Provider;-><init>()V

    const-string v1, "com.android.server.pm.PerfTimeMonitorStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    new-instance v0, Lcom/android/server/pm/Miui32BitTranslateHelper$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/Miui32BitTranslateHelper$Provider;-><init>()V

    const-string v1, "com.android.server.pm.Miui32BitTranslateHelperStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v0, Lcom/android/server/pm/UserManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/UserManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.pm.UserManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    new-instance v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl$Provider;-><init>()V

    const-string v1, "com.android.server.pm.MiuiPreinstallCompressStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    new-instance v0, Lcom/android/server/pm/SettingsImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/SettingsImpl$Provider;-><init>()V

    const-string v1, "com.android.server.pm.SettingsStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    new-instance v0, Lcom/android/server/pm/PackageManagerServiceUtilsImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl$Provider;-><init>()V

    const-string v1, "com.android.server.pm.PackageManagerServiceUtilsStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy$Provider;-><init>()V

    const-string v1, "com.android.server.pm.permission.DefaultPermissionGrantPolicyStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    new-instance v0, Lcom/android/server/pm/MiuiPreinstallHelper$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/MiuiPreinstallHelper$Provider;-><init>()V

    const-string v1, "com.android.server.pm.MiuiPreinstallHelperStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/android/server/pm/PackageManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/PackageManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.pm.PackageManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    new-instance v0, Lcom/android/server/pm/permission/MiPermissionManagerServiceImpl$Provider;

    invoke-direct {v0}, Lcom/android/server/pm/permission/MiPermissionManagerServiceImpl$Provider;-><init>()V

    const-string v1, "com.android.server.pm.permission.PermissionManagerServiceStub"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    return-void
.end method
