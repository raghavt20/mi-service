public class com.android.server.pm.PerfTimeMonitorImpl extends com.android.server.pm.PerfTimeMonitorStub {
	 /* .source "PerfTimeMonitorImpl.java" */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Integer collectingCerts;
	 private Long createdAt;
	 private Integer dexopt;
	 private Integer extractNativeLib;
	 private Long extractNativeLibStartedAt;
	 private Integer fileCopying;
	 private Integer googleVerification;
	 private java.lang.String installer;
	 private Integer miuiVerification;
	 private Integer persistAppCount;
	 private Long phraseStartedAt;
	 private java.lang.String pkg;
	 private Integer systemAppCount;
	 private Integer thirdAppCount;
	 private Integer total;
	 /* # direct methods */
	 public static void $r8$lambda$YYToNR7Q1q_B2gTpg2XaWvJtIm0 ( com.android.server.pm.PerfTimeMonitorImpl p0, com.android.server.pm.Settings p1, com.android.server.pm.PackageManagerService p2, com.android.server.pm.PackageSetting p3 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/pm/PerfTimeMonitorImpl;->lambda$markPmsScanDetail$0(Lcom/android/server/pm/Settings;Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageSetting;)V */
		 return;
	 } // .end method
	 public com.android.server.pm.PerfTimeMonitorImpl ( ) {
		 /* .locals 3 */
		 /* .line 18 */
		 /* invoke-direct {p0}, Lcom/android/server/pm/PerfTimeMonitorStub;-><init>()V */
		 /* .line 21 */
		 final String v0 = ""; // const-string v0, ""
		 this.pkg = v0;
		 /* .line 22 */
		 this.installer = v0;
		 /* .line 23 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->fileCopying:I */
		 /* .line 24 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I */
		 /* .line 25 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->miuiVerification:I */
		 /* .line 26 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->googleVerification:I */
		 /* .line 27 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->dexopt:I */
		 /* .line 28 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I */
		 /* .line 29 */
		 /* const-wide/16 v1, 0x0 */
		 /* iput-wide v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->createdAt:J */
		 /* .line 30 */
		 /* iput-wide v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
		 /* .line 31 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLib:I */
		 /* .line 33 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I */
		 /* .line 34 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->systemAppCount:I */
		 /* .line 35 */
		 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->persistAppCount:I */
		 /* .line 36 */
		 /* iput-wide v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLibStartedAt:J */
		 return;
	 } // .end method
	 private void lambda$markPmsScanDetail$0 ( com.android.server.pm.Settings p0, com.android.server.pm.PackageManagerService p1, com.android.server.pm.PackageSetting p2 ) { //synthethic
		 /* .locals 2 */
		 /* .param p1, "settings" # Lcom/android/server/pm/Settings; */
		 /* .param p2, "pms" # Lcom/android/server/pm/PackageManagerService; */
		 /* .param p3, "ps" # Lcom/android/server/pm/PackageSetting; */
		 /* .line 175 */
		 v0 = 		 (( com.android.server.pm.PackageSetting ) p3 ).isSystem ( ); // invoke-virtual {p3}, Lcom/android/server/pm/PackageSetting;->isSystem()Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 176 */
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->systemAppCount:I */
			 /* add-int/lit8 v0, v0, 0x1 */
			 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->systemAppCount:I */
			 /* .line 177 */
			 (( com.android.server.pm.PackageSetting ) p3 ).getPackageName ( ); // invoke-virtual {p3}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;
			 v0 = 			 (( com.android.server.pm.Settings ) p1 ).isDisabledSystemPackageLPr ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/pm/Settings;->isDisabledSystemPackageLPr(Ljava/lang/String;)Z
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 /* .line 178 */
				 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I */
				 /* add-int/lit8 v0, v0, 0x1 */
				 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I */
				 /* .line 181 */
			 } // :cond_0
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I */
			 /* add-int/lit8 v0, v0, 0x1 */
			 /* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I */
			 /* .line 183 */
		 } // :cond_1
	 } // :goto_0
	 (( com.android.server.pm.PackageSetting ) p3 ).getPkg ( ); // invoke-virtual {p3}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
	 /* .line 184 */
	 /* .local v0, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
		 v1 = 	 if ( v0 != null) { // if-eqz v0, :cond_3
		 if ( v1 != null) { // if-eqz v1, :cond_3
			 v1 = 			 (( com.android.server.pm.PackageManagerService ) p2 ).getSafeMode ( ); // invoke-virtual {p2}, Lcom/android/server/pm/PackageManagerService;->getSafeMode()Z
			 if ( v1 != null) { // if-eqz v1, :cond_2
				 v1 = 				 (( com.android.server.pm.PackageSetting ) p3 ).isSystem ( ); // invoke-virtual {p3}, Lcom/android/server/pm/PackageSetting;->isSystem()Z
				 if ( v1 != null) { // if-eqz v1, :cond_3
					 /* .line 185 */
				 } // :cond_2
				 /* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->persistAppCount:I */
				 /* add-int/lit8 v1, v1, 0x1 */
				 /* iput v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->persistAppCount:I */
				 /* .line 187 */
			 } // :cond_3
			 return;
		 } // .end method
		 /* # virtual methods */
		 public void dump ( ) {
			 /* .locals 3 */
			 /* .line 211 */
			 /* new-instance v0, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
			 v1 = this.pkg;
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 /* const-string/jumbo v1, "|" */
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v2 = this.installer;
			 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( com.android.server.pm.PerfTimeMonitorImpl ) p0 ).toString ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PerfTimeMonitorImpl;->toString()Ljava/lang/String;
			 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v1 = "InstallationTiming"; // const-string v1, "InstallationTiming"
			 android.util.Slog .i ( v1,v0 );
			 /* .line 212 */
			 return;
		 } // .end method
		 public Integer getCollectingCerts ( ) {
			 /* .locals 1 */
			 /* .line 147 */
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I */
		 } // .end method
		 public Integer getDexopt ( ) {
			 /* .locals 1 */
			 /* .line 162 */
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->dexopt:I */
		 } // .end method
		 public Integer getFileCopying ( ) {
			 /* .locals 1 */
			 /* .line 142 */
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->fileCopying:I */
		 } // .end method
		 public Integer getGoogleVerification ( ) {
			 /* .locals 1 */
			 /* .line 157 */
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->googleVerification:I */
		 } // .end method
		 public Integer getMiuiVerification ( ) {
			 /* .locals 1 */
			 /* .line 152 */
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->miuiVerification:I */
		 } // .end method
		 public Integer getTotal ( ) {
			 /* .locals 1 */
			 /* .line 167 */
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I */
		 } // .end method
		 public Boolean isFinished ( ) {
			 /* .locals 1 */
			 /* .line 40 */
			 /* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I */
			 /* if-lez v0, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public void markPackageOptimized ( com.android.server.pm.PackageManagerService p0, com.android.server.pm.pkg.AndroidPackage p1 ) {
	 /* .locals 3 */
	 /* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
	 /* .param p2, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
	 /* .line 200 */
	 miui.mqsas.sdk.BootEventManager .getInstance ( );
	 /* .line 201 */
	 /* .local v0, "manager":Lmiui/mqsas/sdk/BootEventManager; */
	 (( com.android.server.pm.PackageManagerService ) p1 ).snapshotComputer ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
	 /* .line 202 */
	 v2 = 	 /* .local v1, "psi":Lcom/android/server/pm/pkg/PackageStateInternal; */
		 v2 = 	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* if-nez v2, :cond_0 */
		 /* .line 203 */
		 v2 = 		 (( miui.mqsas.sdk.BootEventManager ) v0 ).getDexoptSystemAppCount ( ); // invoke-virtual {v0}, Lmiui/mqsas/sdk/BootEventManager;->getDexoptSystemAppCount()I
		 /* add-int/lit8 v2, v2, 0x1 */
		 (( miui.mqsas.sdk.BootEventManager ) v0 ).setDexoptSystemAppCount ( v2 ); // invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/BootEventManager;->setDexoptSystemAppCount(I)V
		 /* .line 205 */
	 } // :cond_0
	 v2 = 	 (( miui.mqsas.sdk.BootEventManager ) v0 ).getDexoptThirdAppCount ( ); // invoke-virtual {v0}, Lmiui/mqsas/sdk/BootEventManager;->getDexoptThirdAppCount()I
	 /* add-int/lit8 v2, v2, 0x1 */
	 (( miui.mqsas.sdk.BootEventManager ) v0 ).setDexoptThirdAppCount ( v2 ); // invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/BootEventManager;->setDexoptThirdAppCount(I)V
	 /* .line 207 */
} // :goto_0
return;
} // .end method
public void markPmsScanDetail ( com.android.server.pm.PackageManagerService p0 ) {
/* .locals 3 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .line 172 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 173 */
try { // :try_start_0
	 v1 = this.mSettings;
	 /* .line 174 */
	 /* .local v1, "settings":Lcom/android/server/pm/Settings; */
	 /* new-instance v2, Lcom/android/server/pm/PerfTimeMonitorImpl$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0, v1, p1}, Lcom/android/server/pm/PerfTimeMonitorImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/PerfTimeMonitorImpl;Lcom/android/server/pm/Settings;Lcom/android/server/pm/PackageManagerService;)V */
	 (( com.android.server.pm.PackageManagerService ) p1 ).forEachPackageSetting ( v2 ); // invoke-virtual {p1, v2}, Lcom/android/server/pm/PackageManagerService;->forEachPackageSetting(Ljava/util/function/Consumer;)V
	 /* .line 188 */
} // .end local v1 # "settings":Lcom/android/server/pm/Settings;
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 190 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->systemAppCount:I */
(( miui.mqsas.sdk.BootEventManager ) v0 ).setSystemAppCount ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setSystemAppCount(I)V
/* .line 191 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->thirdAppCount:I */
(( miui.mqsas.sdk.BootEventManager ) v0 ).setThirdAppCount ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setThirdAppCount(I)V
/* .line 192 */
miui.mqsas.sdk.BootEventManager .getInstance ( );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->persistAppCount:I */
(( miui.mqsas.sdk.BootEventManager ) v0 ).setPersistAppCount ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/BootEventManager;->setPersistAppCount(I)V
/* .line 193 */
v0 = (( com.android.server.pm.PackageManagerService ) p1 ).isFirstBoot ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 int v0 = 2; // const/4 v0, 0x2
	 /* .line 194 */
} // :cond_0
v0 = (( com.android.server.pm.PackageManagerService ) p1 ).isDeviceUpgrading ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 int v0 = 3; // const/4 v0, 0x3
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :goto_0
/* nop */
/* .line 195 */
/* .local v0, "type":I */
miui.mqsas.sdk.BootEventManager .getInstance ( );
(( miui.mqsas.sdk.BootEventManager ) v1 ).setBootType ( v0 ); // invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/BootEventManager;->setBootType(I)V
/* .line 196 */
return;
/* .line 188 */
} // .end local v0 # "type":I
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void setCollectCertsFinished ( ) {
/* .locals 6 */
/* .line 86 */
/* iget v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I */
/* int-to-long v0, v0 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* iget-wide v4, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
/* sub-long/2addr v2, v4 */
/* add-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I */
/* .line 90 */
return;
} // .end method
public void setCollectCertsStarted ( ) {
/* .locals 2 */
/* .line 61 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
/* .line 62 */
return;
} // .end method
public void setDexoptFinished ( ) {
/* .locals 4 */
/* .line 126 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
/* sub-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->dexopt:I */
/* .line 130 */
return;
} // .end method
public void setDexoptStarted ( ) {
/* .locals 2 */
/* .line 81 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
/* .line 82 */
return;
} // .end method
void setExtractNativeLibFinished ( ) {
/* .locals 4 */
/* .line 94 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLibStartedAt:J */
/* sub-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLib:I */
/* .line 98 */
return;
} // .end method
void setExtractNativeLibStarted ( ) {
/* .locals 2 */
/* .line 66 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLibStartedAt:J */
/* .line 67 */
return;
} // .end method
public void setFileCopied ( ) {
/* .locals 4 */
/* .line 102 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->createdAt:J */
/* sub-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->fileCopying:I */
/* .line 106 */
return;
} // .end method
public void setFinished ( ) {
/* .locals 4 */
/* .line 134 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->createdAt:J */
/* sub-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I */
/* .line 138 */
return;
} // .end method
public void setGoogleVerificationFinished ( ) {
/* .locals 4 */
/* .line 118 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
/* sub-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->googleVerification:I */
/* .line 122 */
return;
} // .end method
public void setGoogleVerificationStarted ( ) {
/* .locals 2 */
/* .line 76 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
/* .line 77 */
return;
} // .end method
public void setInstaller ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "installer" # Ljava/lang/String; */
/* .line 50 */
this.installer = p1;
/* .line 51 */
return;
} // .end method
public void setMiuiVerificationFinished ( ) {
/* .locals 4 */
/* .line 110 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
/* sub-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* iput v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->miuiVerification:I */
/* .line 114 */
return;
} // .end method
public void setMiuiVerificationStarted ( ) {
/* .locals 2 */
/* .line 71 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->phraseStartedAt:J */
/* .line 72 */
return;
} // .end method
public void setPackageName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 45 */
this.pkg = p1;
/* .line 46 */
return;
} // .end method
public void setStarted ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "sessionId" # I */
/* .line 55 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->createdAt:J */
/* .line 56 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "beginInstall_sessionId: "; // const-string v1, "beginInstall_sessionId: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "InstallationTiming"; // const-string v1, "InstallationTiming"
android.util.Slog .i ( v1,v0 );
/* .line 57 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 9 */
/* .line 216 */
v0 = java.util.Locale.ENGLISH;
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->total:I */
/* .line 217 */
java.lang.Integer .valueOf ( v1 );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->fileCopying:I */
java.lang.Integer .valueOf ( v1 );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->collectingCerts:I */
java.lang.Integer .valueOf ( v1 );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->extractNativeLib:I */
java.lang.Integer .valueOf ( v1 );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->miuiVerification:I */
java.lang.Integer .valueOf ( v1 );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->googleVerification:I */
java.lang.Integer .valueOf ( v1 );
/* iget v1, p0, Lcom/android/server/pm/PerfTimeMonitorImpl;->dexopt:I */
java.lang.Integer .valueOf ( v1 );
/* filled-new-array/range {v2 ..v8}, [Ljava/lang/Object; */
/* .line 216 */
final String v2 = "%d|%d|%d|%d|%d|%d|%d"; // const-string v2, "%d|%d|%d|%d|%d|%d|%d"
java.lang.String .format ( v0,v2,v1 );
} // .end method
