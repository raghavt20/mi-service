public class com.android.server.pm.MiuiPAIPreinstallConfig {
	 /* .source "MiuiPAIPreinstallConfig.java" */
	 /* # static fields */
	 private static final java.lang.String OLD_PREINSTALL_PACKAGE_PAI_TRACKING_FILE;
	 private static final java.lang.String OPERATOR_PREINSTALL_PACKAGE_TRACKING_FILE;
	 private static final java.lang.String PREINSTALL_PACKAGE_MIUI_TRACKING_DIR;
	 private static final java.lang.String PREINSTALL_PACKAGE_MIUI_TRACKING_DIR_CONTEXT;
	 private static final java.lang.String PREINSTALL_PACKAGE_PAI_LIST;
	 private static final java.lang.String PREINSTALL_PACKAGE_PAI_TRACKING_FILE;
	 private static final java.lang.String TAG;
	 private static final java.lang.String TYPE_TRACKING_APPSFLYER;
	 private static final java.lang.String TYPE_TRACKING_MIUI;
	 private static java.util.List sNewTrackContentList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static java.util.List sPackagePAIList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sTraditionalTrackContentList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.pm.MiuiPAIPreinstallConfig ( ) {
/* .locals 1 */
/* .line 28 */
/* const-class v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 41 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 42 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 43 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
return;
} // .end method
public com.android.server.pm.MiuiPAIPreinstallConfig ( ) {
/* .locals 0 */
/* .line 27 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static void copyPreinstallPAITrackingFile ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 10 */
/* .param p0, "type" # Ljava/lang/String; */
/* .param p1, "fileName" # Ljava/lang/String; */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 272 */
v0 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v0, :cond_9 */
v0 = android.text.TextUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_3 */
/* .line 275 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 276 */
/* .local v0, "isAppsflyer":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 277 */
/* .local v1, "filePath":Ljava/lang/String; */
final String v2 = "appsflyer"; // const-string v2, "appsflyer"
v3 = android.text.TextUtils .equals ( v2,p0 );
/* const-string/jumbo v4, "xiaomi" */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 278 */
final String v1 = "/data/miui/pai/pre_install.appsflyer"; // const-string v1, "/data/miui/pai/pre_install.appsflyer"
/* .line 279 */
int v0 = 1; // const/4 v0, 0x1
/* .line 280 */
} // :cond_1
v3 = android.text.TextUtils .equals ( v4,p0 );
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 281 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "/data/miui/pai/"; // const-string v5, "/data/miui/pai/"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 287 */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
v3 = v3 = com.android.server.pm.MiuiPAIPreinstallConfig.sNewTrackContentList;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 288 */
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Content duplication dose not need to be written again! content is :"; // const-string v4, "Content duplication dose not need to be written again! content is :"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 289 */
return;
/* .line 292 */
} // :cond_2
v3 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "use " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p0 ); // invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " tracking method!"; // const-string v6, " tracking method!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v5 );
/* .line 294 */
int v5 = 0; // const/4 v5, 0x0
/* .line 296 */
/* .local v5, "bw":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v6, Ljava/io/File; */
/* invoke-direct {v6, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 297 */
/* .local v6, "file":Ljava/io/File; */
v7 = (( java.io.File ) v6 ).exists ( ); // invoke-virtual {v6}, Ljava/io/File;->exists()Z
int v8 = -1; // const/4 v8, -0x1
/* if-nez v7, :cond_4 */
/* .line 298 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "create tracking file\uff1a"; // const-string v9, "create tracking file\uff1a"
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v6 ).getAbsolutePath ( ); // invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v7 );
/* .line 299 */
(( java.io.File ) v6 ).getParentFile ( ); // invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;
v7 = (( java.io.File ) v7 ).exists ( ); // invoke-virtual {v7}, Ljava/io/File;->exists()Z
/* if-nez v7, :cond_3 */
/* .line 300 */
(( java.io.File ) v6 ).getParentFile ( ); // invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v7 ).mkdir ( ); // invoke-virtual {v7}, Ljava/io/File;->mkdir()Z
/* .line 301 */
(( java.io.File ) v6 ).getParentFile ( ); // invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* const/16 v9, 0x1fd */
android.os.FileUtils .setPermissions ( v7,v9,v8,v8 );
/* .line 303 */
} // :cond_3
(( java.io.File ) v6 ).createNewFile ( ); // invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
/* .line 305 */
} // :cond_4
/* const/16 v7, 0x1b4 */
android.os.FileUtils .setPermissions ( v6,v7,v8,v8 );
/* .line 306 */
/* new-instance v7, Ljava/io/BufferedWriter; */
/* new-instance v8, Ljava/io/FileWriter; */
/* invoke-direct {v8, v6, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* invoke-direct {v7, v8}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v5, v7 */
/* .line 307 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 308 */
v7 = com.android.server.pm.MiuiPAIPreinstallConfig.sNewTrackContentList;
/* .line 310 */
} // :cond_5
(( java.io.BufferedWriter ) v5 ).write ( p2 ); // invoke-virtual {v5, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 311 */
final String v7 = "\n"; // const-string v7, "\n"
(( java.io.BufferedWriter ) v5 ).write ( v7 ); // invoke-virtual {v5, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 312 */
(( java.io.BufferedWriter ) v5 ).flush ( ); // invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V
/* .line 313 */
final String v7 = "Copy PAI tracking content Success!"; // const-string v7, "Copy PAI tracking content Success!"
android.util.Slog .i ( v3,v7 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 325 */
/* nop */
} // .end local v6 # "file":Ljava/io/File;
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 326 */
/* nop */
/* .line 327 */
com.android.server.pm.MiuiPAIPreinstallConfig .restoreconPreinstallDir ( );
/* .line 328 */
return;
/* .line 325 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 315 */
/* :catch_0 */
/* move-exception v3 */
/* .line 316 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_1
v2 = android.text.TextUtils .equals ( v2,p0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
final String v6 = ":"; // const-string v6, ":"
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 317 */
try { // :try_start_2
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Error occurs when to copy PAI tracking content("; // const-string v7, "Error occurs when to copy PAI tracking content("
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ") into "; // const-string v7, ") into "
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v4 );
/* .line 319 */
} // :cond_6
v2 = android.text.TextUtils .equals ( v4,p0 );
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 320 */
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Error occurs when to create "; // const-string v7, "Error occurs when to create "
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " PAI tracking file into "; // const-string v7, " PAI tracking file into "
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 325 */
} // :cond_7
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 323 */
return;
/* .line 325 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 326 */
/* throw v2 */
/* .line 283 */
} // .end local v5 # "bw":Ljava/io/BufferedWriter;
} // :cond_8
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Used invalid pai tracking type ="; // const-string v4, "Used invalid pai tracking type ="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "! you can use type:appsflyer or xiaomi"; // const-string v4, "! you can use type:appsflyer or xiaomi"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 285 */
return;
/* .line 273 */
} // .end local v0 # "isAppsflyer":Z
} // .end local v1 # "filePath":Ljava/lang/String;
} // :cond_9
} // :goto_3
return;
} // .end method
private static void copyTraditionalTrackFileToNewLocationIfNeed ( ) {
/* .locals 6 */
/* .line 75 */
com.android.server.pm.MiuiPAIPreinstallConfig .readNewPAITrackFileIfNeed ( );
/* .line 76 */
final String v0 = "ro.appsflyer.preinstall.path"; // const-string v0, "ro.appsflyer.preinstall.path"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 77 */
/* .local v0, "appsflyerPath":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 78 */
v1 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
final String v2 = "no system property ro.appsflyer.preinstall.path"; // const-string v2, "no system property ro.appsflyer.preinstall.path"
android.util.Slog .e ( v1,v2 );
/* .line 79 */
return;
/* .line 81 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "/data/miui/pai/pre_install.appsflyer"; // const-string v2, "/data/miui/pai/pre_install.appsflyer"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 82 */
/* .local v1, "paiTrackingPath":Ljava/io/File; */
v2 = android.text.TextUtils .equals ( v0,v2 );
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_2 */
/* .line 84 */
try { // :try_start_0
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "create new appsflyer tracking file\uff1a"; // const-string v4, "create new appsflyer tracking file\uff1a"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 85 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
v2 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
int v3 = -1; // const/4 v3, -0x1
/* if-nez v2, :cond_1 */
/* .line 86 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v2 ).mkdir ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdir()Z
/* .line 87 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* const/16 v4, 0x1fd */
android.os.FileUtils .setPermissions ( v2,v4,v3,v3 );
/* .line 89 */
} // :cond_1
(( java.io.File ) v1 ).createNewFile ( ); // invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
/* .line 90 */
/* const/16 v2, 0x1b4 */
android.os.FileUtils .setPermissions ( v1,v2,v3,v3 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 93 */
/* .line 91 */
/* :catch_0 */
/* move-exception v2 */
/* .line 92 */
/* .local v2, "e":Ljava/io/IOException; */
v3 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error occurs when to create new appsflyer tracking"; // const-string v5, "Error occurs when to create new appsflyer tracking"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 94 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_0
com.android.server.pm.MiuiPAIPreinstallConfig .readTraditionalPAITrackFile ( );
/* .line 95 */
com.android.server.pm.MiuiPAIPreinstallConfig .readOperatorTrackFile ( );
/* .line 96 */
com.android.server.pm.MiuiPAIPreinstallConfig .writeNewPAITrackFile ( );
/* .line 98 */
} // :cond_2
com.android.server.pm.MiuiPAIPreinstallConfig .restoreconPreinstallDir ( );
/* .line 99 */
return;
} // .end method
public static void init ( ) {
/* .locals 0 */
/* .line 46 */
com.android.server.pm.MiuiPAIPreinstallConfig .readPackagePAIList ( );
/* .line 47 */
com.android.server.pm.MiuiPAIPreinstallConfig .copyTraditionalTrackFileToNewLocationIfNeed ( );
/* .line 48 */
return;
} // .end method
public static Boolean isPreinstalledPAIPackage ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 221 */
v0 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* monitor-enter v0 */
/* .line 222 */
try { // :try_start_0
v1 = v1 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* monitor-exit v0 */
/* .line 223 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private static void readNewPAITrackFileIfNeed ( ) {
/* .locals 6 */
/* .line 172 */
v0 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
final String v1 = "read new track file content from /data/miui/pai/pre_install.appsflyer"; // const-string v1, "read new track file content from /data/miui/pai/pre_install.appsflyer"
android.util.Slog .i ( v0,v1 );
/* .line 173 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miui/pai/pre_install.appsflyer"; // const-string v1, "/data/miui/pai/pre_install.appsflyer"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 174 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 175 */
int v1 = 0; // const/4 v1, 0x0
/* .line 177 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v2 */
/* .line 178 */
int v2 = 0; // const/4 v2, 0x0
/* .line 179 */
/* .local v2, "line":Ljava/lang/String; */
} // :cond_0
} // :goto_0
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 180 */
v3 = v3 = com.android.server.pm.MiuiPAIPreinstallConfig.sNewTrackContentList;
/* if-nez v3, :cond_0 */
/* .line 181 */
v3 = com.android.server.pm.MiuiPAIPreinstallConfig.sNewTrackContentList;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 187 */
} // .end local v2 # "line":Ljava/lang/String;
} // :cond_1
/* nop */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 188 */
/* .line 187 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 184 */
/* :catch_0 */
/* move-exception v2 */
/* .line 185 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
v3 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error occurs while read new track file content on pkms start"; // const-string v5, "Error occurs while read new track file content on pkms start"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 187 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 188 */
/* throw v2 */
/* .line 190 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
} // :cond_2
} // :goto_3
return;
} // .end method
private static void readOperatorTrackFile ( ) {
/* .locals 5 */
/* .line 122 */
v0 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
final String v1 = "read Operator track file content from /mi_ext/product/etc/pre_install.appsflyer"; // const-string v1, "read Operator track file content from /mi_ext/product/etc/pre_install.appsflyer"
android.util.Slog .i ( v0,v1 );
/* .line 124 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/mi_ext/product/etc/pre_install.appsflyer"; // const-string v1, "/mi_ext/product/etc/pre_install.appsflyer"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 125 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 126 */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
/* invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 127 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
int v2 = 0; // const/4 v2, 0x0
/* .line 128 */
/* .local v2, "line":Ljava/lang/String; */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 129 */
v3 = com.android.server.pm.MiuiPAIPreinstallConfig.sTraditionalTrackContentList;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 131 */
} // .end local v2 # "line":Ljava/lang/String;
} // :cond_0
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 133 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
/* .line 126 */
/* .restart local v1 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // :goto_1
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 131 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
/* .restart local v0 # "file":Ljava/io/File; */
/* :catch_0 */
/* move-exception v1 */
/* .line 132 */
/* .local v1, "e":Ljava/io/IOException; */
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurs while read Operator track file content"; // const-string v4, "Error occurs while read Operator track file content"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 135 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_2
return;
} // .end method
private static void readPackagePAIList ( ) {
/* .locals 5 */
/* .line 52 */
int v0 = 0; // const/4 v0, 0x0
/* .line 54 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
final String v3 = "/data/system/preinstallPAI.list"; // const-string v3, "/data/system/preinstallPAI.list"
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v0, v1 */
/* .line 55 */
int v1 = 0; // const/4 v1, 0x0
/* .line 56 */
/* .local v1, "line":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 57 */
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 62 */
} // .end local v1 # "line":Ljava/lang/String;
} // :cond_0
/* nop */
/* .line 63 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 62 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 59 */
/* :catch_0 */
/* move-exception v1 */
/* .line 60 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_1
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurs while read preinstalled PAI packages "; // const-string v4, "Error occurs while read preinstalled PAI packages "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 62 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 63 */
/* .line 66 */
} // :cond_1
} // :goto_2
return;
/* .line 62 */
} // :goto_3
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 63 */
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 65 */
} // :cond_2
/* throw v1 */
} // .end method
private static void readTraditionalPAITrackFile ( ) {
/* .locals 6 */
/* .line 102 */
v0 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
final String v1 = "read traditional track file content from /cust/etc/pre_install.appsflyer"; // const-string v1, "read traditional track file content from /cust/etc/pre_install.appsflyer"
android.util.Slog .i ( v0,v1 );
/* .line 103 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/cust/etc/pre_install.appsflyer"; // const-string v1, "/cust/etc/pre_install.appsflyer"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 104 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 105 */
v1 = com.android.server.pm.MiuiPAIPreinstallConfig.sTraditionalTrackContentList;
/* .line 106 */
int v1 = 0; // const/4 v1, 0x0
/* .line 108 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v2 */
/* .line 109 */
int v2 = 0; // const/4 v2, 0x0
/* .line 110 */
/* .local v2, "line":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 111 */
v3 = com.android.server.pm.MiuiPAIPreinstallConfig.sTraditionalTrackContentList;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 116 */
} // .end local v2 # "line":Ljava/lang/String;
} // :cond_0
/* nop */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 117 */
/* .line 116 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 113 */
/* :catch_0 */
/* move-exception v2 */
/* .line 114 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
v3 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error occurs while read traditional track file content"; // const-string v5, "Error occurs while read traditional track file content"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 116 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 117 */
/* throw v2 */
/* .line 119 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
} // :cond_1
} // :goto_3
return;
} // .end method
public static void removeFromPreinstallPAIList ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 228 */
v0 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* monitor-enter v0 */
/* .line 229 */
try { // :try_start_0
v1 = v1 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* if-nez v1, :cond_0 */
/* .line 230 */
/* monitor-exit v0 */
return;
/* .line 232 */
} // :cond_0
v1 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* .line 233 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 236 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/preinstallPAI.list.tmp"; // const-string v1, "/data/system/preinstallPAI.list.tmp"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 237 */
/* .local v0, "tempFile":Ljava/io/File; */
/* new-instance v1, Lcom/android/internal/util/JournaledFile; */
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/system/preinstallPAI.list"; // const-string v3, "/data/system/preinstallPAI.list"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2, v0}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V */
/* .line 238 */
/* .local v1, "journal":Lcom/android/internal/util/JournaledFile; */
(( com.android.internal.util.JournaledFile ) v1 ).chooseForWrite ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;
/* .line 239 */
/* .local v2, "writeTarget":Ljava/io/File; */
int v3 = 0; // const/4 v3, 0x0
/* .line 240 */
/* .local v3, "fstr":Ljava/io/FileOutputStream; */
int v4 = 0; // const/4 v4, 0x0
/* .line 242 */
/* .local v4, "str":Ljava/io/BufferedOutputStream; */
try { // :try_start_1
/* new-instance v5, Ljava/io/FileOutputStream; */
/* invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v3, v5 */
/* .line 243 */
/* new-instance v5, Ljava/io/BufferedOutputStream; */
/* invoke-direct {v5, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* move-object v4, v5 */
/* .line 244 */
(( java.io.FileOutputStream ) v3 ).getFD ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;
/* const/16 v6, 0x3e8 */
/* const/16 v7, 0x408 */
/* const/16 v8, 0x1a0 */
android.os.FileUtils .setPermissions ( v5,v8,v6,v7 );
/* .line 246 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 247 */
/* .local v5, "sb":Ljava/lang/StringBuilder; */
v6 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* monitor-enter v6 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 248 */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_0
try { // :try_start_2
v8 = v8 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* if-ge v7, v8, :cond_1 */
/* .line 249 */
int v8 = 0; // const/4 v8, 0x0
(( java.lang.StringBuilder ) v5 ).setLength ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->setLength(I)V
/* .line 250 */
v8 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* check-cast v8, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 251 */
final String v8 = "\n"; // const-string v8, "\n"
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 252 */
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) v8 ).getBytes ( ); // invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B
(( java.io.BufferedOutputStream ) v4 ).write ( v8 ); // invoke-virtual {v4, v8}, Ljava/io/BufferedOutputStream;->write([B)V
/* .line 248 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 254 */
} // .end local v7 # "i":I
} // :cond_1
/* monitor-exit v6 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 256 */
try { // :try_start_3
(( java.io.BufferedOutputStream ) v4 ).flush ( ); // invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V
/* .line 257 */
android.os.FileUtils .sync ( v3 );
/* .line 258 */
(( com.android.internal.util.JournaledFile ) v1 ).commit ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->commit()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
/* .line 254 */
/* .restart local v5 # "sb":Ljava/lang/StringBuilder; */
/* :catchall_0 */
/* move-exception v7 */
try { // :try_start_4
/* monitor-exit v6 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
} // .end local v0 # "tempFile":Ljava/io/File;
} // .end local v1 # "journal":Lcom/android/internal/util/JournaledFile;
} // .end local v2 # "writeTarget":Ljava/io/File;
} // .end local v3 # "fstr":Ljava/io/FileOutputStream;
} // .end local v4 # "str":Ljava/io/BufferedOutputStream;
} // .end local p0 # "pkg":Ljava/lang/String;
try { // :try_start_5
/* throw v7 */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_0 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 263 */
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
/* .restart local v0 # "tempFile":Ljava/io/File; */
/* .restart local v1 # "journal":Lcom/android/internal/util/JournaledFile; */
/* .restart local v2 # "writeTarget":Ljava/io/File; */
/* .restart local v3 # "fstr":Ljava/io/FileOutputStream; */
/* .restart local v4 # "str":Ljava/io/BufferedOutputStream; */
/* .restart local p0 # "pkg":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v5 */
/* .line 259 */
/* :catch_0 */
/* move-exception v5 */
/* .line 260 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_6
v6 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
final String v7 = "Failed to delete preinstallPAI.list + "; // const-string v7, "Failed to delete preinstallPAI.list + "
android.util.Slog .wtf ( v6,v7,v5 );
/* .line 261 */
(( com.android.internal.util.JournaledFile ) v1 ).rollback ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->rollback()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 263 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 264 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 265 */
/* nop */
/* .line 267 */
v5 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Delete package:"; // const-string v7, "Delete package:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p0 ); // invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " from preinstallPAI.list"; // const-string v7, " from preinstallPAI.list"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 268 */
return;
/* .line 263 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 264 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 265 */
/* throw v5 */
/* .line 233 */
} // .end local v0 # "tempFile":Ljava/io/File;
} // .end local v1 # "journal":Lcom/android/internal/util/JournaledFile;
} // .end local v2 # "writeTarget":Ljava/io/File;
} // .end local v3 # "fstr":Ljava/io/FileOutputStream;
} // .end local v4 # "str":Ljava/io/BufferedOutputStream;
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_7
/* monitor-exit v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_2 */
/* throw v1 */
} // .end method
private static void restoreconPreinstallDir ( ) {
/* .locals 3 */
/* .line 193 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miui/pai/"; // const-string v1, "/data/miui/pai/"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 194 */
/* .local v0, "file":Ljava/io/File; */
android.os.SELinux .getFileContext ( v1 );
/* .line 195 */
/* .local v1, "fileContext":Ljava/lang/String; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-string/jumbo v2, "u:object_r:miui_pai_file:s0" */
v2 = android.text.TextUtils .equals ( v1,v2 );
/* if-nez v2, :cond_0 */
/* .line 196 */
android.os.SELinux .restoreconRecursive ( v0 );
/* .line 198 */
} // :cond_0
return;
} // .end method
private static void writeNewPAITrackFile ( ) {
/* .locals 6 */
/* .line 138 */
v0 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
final String v1 = "Write old track file content to /data/miui/pai/pre_install.appsflyer"; // const-string v1, "Write old track file content to /data/miui/pai/pre_install.appsflyer"
android.util.Slog .i ( v0,v1 );
/* .line 139 */
v1 = com.android.server.pm.MiuiPAIPreinstallConfig.sTraditionalTrackContentList;
v1 = if ( v1 != null) { // if-eqz v1, :cond_4
if ( v1 != null) { // if-eqz v1, :cond_0
/* goto/16 :goto_3 */
/* .line 143 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 145 */
/* .local v0, "bufferWriter":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "/data/miui/pai/pre_install.appsflyer"; // const-string v2, "/data/miui/pai/pre_install.appsflyer"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 146 */
/* .local v1, "newFile":Ljava/io/File; */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
v2 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
int v3 = -1; // const/4 v3, -0x1
/* if-nez v2, :cond_1 */
/* .line 147 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v2 ).mkdirs ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
/* .line 148 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* const/16 v4, 0x1fd */
android.os.FileUtils .setPermissions ( v2,v4,v3,v3 );
/* .line 150 */
} // :cond_1
/* new-instance v2, Ljava/io/BufferedWriter; */
/* new-instance v4, Ljava/io/FileWriter; */
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {v4, v1, v5}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* invoke-direct {v2, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v0, v2 */
/* .line 151 */
/* const/16 v2, 0x1b4 */
android.os.FileUtils .setPermissions ( v1,v2,v3,v3 );
/* .line 152 */
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.sTraditionalTrackContentList;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 153 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
try { // :try_start_1
v4 = v4 = com.android.server.pm.MiuiPAIPreinstallConfig.sTraditionalTrackContentList;
/* if-ge v3, v4, :cond_3 */
/* .line 154 */
v4 = com.android.server.pm.MiuiPAIPreinstallConfig.sTraditionalTrackContentList;
/* check-cast v4, Ljava/lang/String; */
/* .line 155 */
/* .local v4, "content":Ljava/lang/String; */
v5 = v5 = com.android.server.pm.MiuiPAIPreinstallConfig.sNewTrackContentList;
/* if-nez v5, :cond_2 */
/* .line 156 */
v5 = com.android.server.pm.MiuiPAIPreinstallConfig.sNewTrackContentList;
/* .line 157 */
(( java.io.BufferedWriter ) v0 ).write ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 158 */
final String v5 = "\n"; // const-string v5, "\n"
(( java.io.BufferedWriter ) v0 ).write ( v5 ); // invoke-virtual {v0, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 153 */
} // .end local v4 # "content":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 161 */
} // .end local v3 # "i":I
} // :cond_3
/* monitor-exit v2 */
/* .line 167 */
} // .end local v1 # "newFile":Ljava/io/File;
/* .line 161 */
/* .restart local v1 # "newFile":Ljava/io/File; */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
try { // :try_start_2
/* throw v3 */
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 167 */
} // .end local v1 # "newFile":Ljava/io/File;
/* .restart local v0 # "bufferWriter":Ljava/io/BufferedWriter; */
/* :catchall_1 */
/* move-exception v1 */
/* .line 162 */
/* :catch_0 */
/* move-exception v1 */
/* .line 163 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_3
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
final String v3 = "Error occurs when to write track file from /cust/etc/pre_install.appsflyer to /data/miui/pai/pre_install.appsflyer"; // const-string v3, "Error occurs when to write track file from /cust/etc/pre_install.appsflyer to /data/miui/pai/pre_install.appsflyer"
android.util.Slog .e ( v2,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 167 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 168 */
/* nop */
/* .line 169 */
return;
/* .line 167 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 168 */
/* throw v1 */
/* .line 140 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
} // :cond_4
} // :goto_3
final String v1 = "no content write to new appsflyer file"; // const-string v1, "no content write to new appsflyer file"
android.util.Slog .e ( v0,v1 );
/* .line 141 */
return;
} // .end method
public static void writePreinstallPAIPackage ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 201 */
v0 = android.text.TextUtils .isEmpty ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 202 */
return;
/* .line 204 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 206 */
/* .local v0, "bufferWriter":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedWriter; */
/* new-instance v2, Ljava/io/FileWriter; */
final String v3 = "/data/system/preinstallPAI.list"; // const-string v3, "/data/system/preinstallPAI.list"
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {v2, v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v0, v1 */
/* .line 207 */
v1 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 208 */
try { // :try_start_1
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.sPackagePAIList;
/* .line 209 */
(( java.io.BufferedWriter ) v0 ).write ( p0 ); // invoke-virtual {v0, p0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 210 */
final String v2 = "\n"; // const-string v2, "\n"
(( java.io.BufferedWriter ) v0 ).write ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 211 */
/* monitor-exit v1 */
/* .line 215 */
/* .line 211 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
} // .end local p0 # "pkg":Ljava/lang/String;
try { // :try_start_2
/* throw v2 */
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 215 */
/* .restart local v0 # "bufferWriter":Ljava/io/BufferedWriter; */
/* .restart local p0 # "pkg":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v1 */
/* .line 212 */
/* :catch_0 */
/* move-exception v1 */
/* .line 213 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_3
v2 = com.android.server.pm.MiuiPAIPreinstallConfig.TAG;
final String v3 = "Error occurs when to write PAI package name."; // const-string v3, "Error occurs when to write PAI package name."
android.util.Slog .e ( v2,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 215 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 216 */
/* nop */
/* .line 217 */
return;
/* .line 215 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 216 */
/* throw v1 */
} // .end method
