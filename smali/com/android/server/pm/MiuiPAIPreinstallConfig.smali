.class public Lcom/android/server/pm/MiuiPAIPreinstallConfig;
.super Ljava/lang/Object;
.source "MiuiPAIPreinstallConfig.java"


# static fields
.field private static final OLD_PREINSTALL_PACKAGE_PAI_TRACKING_FILE:Ljava/lang/String; = "/cust/etc/pre_install.appsflyer"

.field private static final OPERATOR_PREINSTALL_PACKAGE_TRACKING_FILE:Ljava/lang/String; = "/mi_ext/product/etc/pre_install.appsflyer"

.field private static final PREINSTALL_PACKAGE_MIUI_TRACKING_DIR:Ljava/lang/String; = "/data/miui/pai/"

.field private static final PREINSTALL_PACKAGE_MIUI_TRACKING_DIR_CONTEXT:Ljava/lang/String; = "u:object_r:miui_pai_file:s0"

.field private static final PREINSTALL_PACKAGE_PAI_LIST:Ljava/lang/String; = "/data/system/preinstallPAI.list"

.field private static final PREINSTALL_PACKAGE_PAI_TRACKING_FILE:Ljava/lang/String; = "/data/miui/pai/pre_install.appsflyer"

.field private static final TAG:Ljava/lang/String;

.field private static final TYPE_TRACKING_APPSFLYER:Ljava/lang/String; = "appsflyer"

.field private static final TYPE_TRACKING_MIUI:Ljava/lang/String; = "xiaomi"

.field private static sNewTrackContentList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sPackagePAIList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sTraditionalTrackContentList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    const-class v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sTraditionalTrackContentList:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sNewTrackContentList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyPreinstallPAITrackingFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .line 272
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 275
    :cond_0
    const/4 v0, 0x0

    .line 276
    .local v0, "isAppsflyer":Z
    const/4 v1, 0x0

    .line 277
    .local v1, "filePath":Ljava/lang/String;
    const-string v2, "appsflyer"

    invoke-static {v2, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    const-string/jumbo v4, "xiaomi"

    if-eqz v3, :cond_1

    .line 278
    const-string v1, "/data/miui/pai/pre_install.appsflyer"

    .line 279
    const/4 v0, 0x1

    goto :goto_0

    .line 280
    :cond_1
    invoke-static {v4, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 281
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/data/miui/pai/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 287
    :goto_0
    if-eqz v0, :cond_2

    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sNewTrackContentList:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 288
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Content duplication dose not need to be written again! content is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    return-void

    .line 292
    :cond_2
    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "use "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " tracking method!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v5, 0x0

    .line 296
    .local v5, "bw":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 297
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    const/4 v8, -0x1

    if-nez v7, :cond_4

    .line 298
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "create tracking file\uff1a"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_3

    .line 300
    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->mkdir()Z

    .line 301
    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    const/16 v9, 0x1fd

    invoke-static {v7, v9, v8, v8}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 303
    :cond_3
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z

    .line 305
    :cond_4
    const/16 v7, 0x1b4

    invoke-static {v6, v7, v8, v8}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 306
    new-instance v7, Ljava/io/BufferedWriter;

    new-instance v8, Ljava/io/FileWriter;

    invoke-direct {v8, v6, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct {v7, v8}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object v5, v7

    .line 307
    if-eqz v0, :cond_5

    .line 308
    sget-object v7, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sNewTrackContentList:Ljava/util/List;

    invoke-interface {v7, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    :cond_5
    invoke-virtual {v5, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 311
    const-string v7, "\n"

    invoke-virtual {v5, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 312
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V

    .line 313
    const-string v7, "Copy PAI tracking content Success!"

    invoke-static {v3, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    nop

    .end local v6    # "file":Ljava/io/File;
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 326
    nop

    .line 327
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->restoreconPreinstallDir()V

    .line 328
    return-void

    .line 325
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 315
    :catch_0
    move-exception v3

    .line 316
    .local v3, "e":Ljava/io/IOException;
    :try_start_1
    invoke-static {v2, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v6, ":"

    if-eqz v2, :cond_6

    .line 317
    :try_start_2
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error occurs when to copy PAI tracking content("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ") into "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 319
    :cond_6
    invoke-static {v4, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 320
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error occurs when to create "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " PAI tracking file into "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 325
    :cond_7
    :goto_1
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 323
    return-void

    .line 325
    .end local v3    # "e":Ljava/io/IOException;
    :goto_2
    invoke-static {v5}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 326
    throw v2

    .line 283
    .end local v5    # "bw":Ljava/io/BufferedWriter;
    :cond_8
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Used invalid pai tracking type ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "! you can use type:appsflyer or xiaomi"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    return-void

    .line 273
    .end local v0    # "isAppsflyer":Z
    .end local v1    # "filePath":Ljava/lang/String;
    :cond_9
    :goto_3
    return-void
.end method

.method private static copyTraditionalTrackFileToNewLocationIfNeed()V
    .locals 6

    .line 75
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->readNewPAITrackFileIfNeed()V

    .line 76
    const-string v0, "ro.appsflyer.preinstall.path"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "appsflyerPath":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    sget-object v1, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v2, "no system property ro.appsflyer.preinstall.path"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-void

    .line 81
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/miui/pai/pre_install.appsflyer"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    .local v1, "paiTrackingPath":Ljava/io/File;
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 84
    :try_start_0
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create new appsflyer tracking file\uff1a"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    const/4 v3, -0x1

    if-nez v2, :cond_1

    .line 86
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 87
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    const/16 v4, 0x1fd

    invoke-static {v2, v4, v3, v3}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 89
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 90
    const/16 v2, 0x1b4

    invoke-static {v1, v2, v3, v3}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    goto :goto_0

    .line 91
    :catch_0
    move-exception v2

    .line 92
    .local v2, "e":Ljava/io/IOException;
    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error occurs when to create new appsflyer tracking"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    .end local v2    # "e":Ljava/io/IOException;
    :goto_0
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->readTraditionalPAITrackFile()V

    .line 95
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->readOperatorTrackFile()V

    .line 96
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->writeNewPAITrackFile()V

    .line 98
    :cond_2
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->restoreconPreinstallDir()V

    .line 99
    return-void
.end method

.method public static init()V
    .locals 0

    .line 46
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->readPackagePAIList()V

    .line 47
    invoke-static {}, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->copyTraditionalTrackFileToNewLocationIfNeed()V

    .line 48
    return-void
.end method

.method public static isPreinstalledPAIPackage(Ljava/lang/String;)Z
    .locals 2
    .param p0, "pkg"    # Ljava/lang/String;

    .line 221
    sget-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    monitor-enter v0

    .line 222
    :try_start_0
    sget-object v1, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 223
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static readNewPAITrackFileIfNeed()V
    .locals 6

    .line 172
    sget-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v1, "read new track file content from /data/miui/pai/pre_install.appsflyer"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/miui/pai/pre_install.appsflyer"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 174
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 175
    const/4 v1, 0x0

    .line 177
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v1, v2

    .line 178
    const/4 v2, 0x0

    .line 179
    .local v2, "line":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_1

    .line 180
    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sNewTrackContentList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 181
    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sNewTrackContentList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 187
    .end local v2    # "line":Ljava/lang/String;
    :cond_1
    nop

    :goto_1
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 188
    goto :goto_3

    .line 187
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 184
    :catch_0
    move-exception v2

    .line 185
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error occurs while read new track file content on pkms start"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_1

    :goto_2
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 188
    throw v2

    .line 190
    .end local v1    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :goto_3
    return-void
.end method

.method private static readOperatorTrackFile()V
    .locals 5

    .line 122
    sget-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v1, "read Operator track file content from /mi_ext/product/etc/pre_install.appsflyer"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    new-instance v0, Ljava/io/File;

    const-string v1, "/mi_ext/product/etc/pre_install.appsflyer"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 125
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .local v1, "reader":Ljava/io/BufferedReader;
    const/4 v2, 0x0

    .line 128
    .local v2, "line":Ljava/lang/String;
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_0

    .line 129
    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sTraditionalTrackContentList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 131
    .end local v2    # "line":Ljava/lang/String;
    :cond_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 133
    .end local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 126
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "file":Ljava/io/File;
    :goto_1
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 131
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "file":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Ljava/io/IOException;
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error occurs while read Operator track file content"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    return-void
.end method

.method private static readPackagePAIList()V
    .locals 5

    .line 52
    const/4 v0, 0x0

    .line 54
    .local v0, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    const-string v3, "/data/system/preinstallPAI.list"

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v0, v1

    .line 55
    const/4 v1, 0x0

    .line 56
    .local v1, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    if-eqz v2, :cond_0

    .line 57
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 62
    .end local v1    # "line":Ljava/lang/String;
    :cond_0
    nop

    .line 63
    :goto_1
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    goto :goto_2

    .line 62
    :catchall_0
    move-exception v1

    goto :goto_3

    .line 59
    :catch_0
    move-exception v1

    .line 60
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error occurs while read preinstalled PAI packages "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    nop

    .end local v1    # "e":Ljava/io/IOException;
    if-eqz v0, :cond_1

    .line 63
    goto :goto_1

    .line 66
    :cond_1
    :goto_2
    return-void

    .line 62
    :goto_3
    if-eqz v0, :cond_2

    .line 63
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 65
    :cond_2
    throw v1
.end method

.method private static readTraditionalPAITrackFile()V
    .locals 6

    .line 102
    sget-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v1, "read traditional track file content from /cust/etc/pre_install.appsflyer"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v0, Ljava/io/File;

    const-string v1, "/cust/etc/pre_install.appsflyer"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 104
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    sget-object v1, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sTraditionalTrackContentList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 106
    const/4 v1, 0x0

    .line 108
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v1, v2

    .line 109
    const/4 v2, 0x0

    .line 110
    .local v2, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    if-eqz v3, :cond_0

    .line 111
    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sTraditionalTrackContentList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 116
    .end local v2    # "line":Ljava/lang/String;
    :cond_0
    nop

    :goto_1
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 117
    goto :goto_3

    .line 116
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 113
    :catch_0
    move-exception v2

    .line 114
    .local v2, "e":Ljava/io/IOException;
    :try_start_1
    sget-object v3, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error occurs while read traditional track file content"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    nop

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_1

    :goto_2
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 117
    throw v2

    .line 119
    .end local v1    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_3
    return-void
.end method

.method public static removeFromPreinstallPAIList(Ljava/lang/String;)V
    .locals 9
    .param p0, "pkg"    # Ljava/lang/String;

    .line 228
    sget-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    monitor-enter v0

    .line 229
    :try_start_0
    sget-object v1, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 230
    monitor-exit v0

    return-void

    .line 232
    :cond_0
    sget-object v1, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 233
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 236
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/preinstallPAI.list.tmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 237
    .local v0, "tempFile":Ljava/io/File;
    new-instance v1, Lcom/android/internal/util/JournaledFile;

    new-instance v2, Ljava/io/File;

    const-string v3, "/data/system/preinstallPAI.list"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2, v0}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V

    .line 238
    .local v1, "journal":Lcom/android/internal/util/JournaledFile;
    invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;

    move-result-object v2

    .line 239
    .local v2, "writeTarget":Ljava/io/File;
    const/4 v3, 0x0

    .line 240
    .local v3, "fstr":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .line 242
    .local v4, "str":Ljava/io/BufferedOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v3, v5

    .line 243
    new-instance v5, Ljava/io/BufferedOutputStream;

    invoke-direct {v5, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v4, v5

    .line 244
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    const/16 v6, 0x3e8

    const/16 v7, 0x408

    const/16 v8, 0x1a0

    invoke-static {v5, v8, v6, v7}, Landroid/os/FileUtils;->setPermissions(Ljava/io/FileDescriptor;III)I

    .line 246
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    .local v5, "sb":Ljava/lang/StringBuilder;
    sget-object v6, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    monitor-enter v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 248
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    :try_start_2
    sget-object v8, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v7, v8, :cond_1

    .line 249
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 250
    sget-object v8, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string v8, "\n"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 248
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 254
    .end local v7    # "i":I
    :cond_1
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 256
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V

    .line 257
    invoke-static {v3}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    .line 258
    invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->commit()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .end local v5    # "sb":Ljava/lang/StringBuilder;
    goto :goto_1

    .line 254
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v7

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .end local v0    # "tempFile":Ljava/io/File;
    .end local v1    # "journal":Lcom/android/internal/util/JournaledFile;
    .end local v2    # "writeTarget":Ljava/io/File;
    .end local v3    # "fstr":Ljava/io/FileOutputStream;
    .end local v4    # "str":Ljava/io/BufferedOutputStream;
    .end local p0    # "pkg":Ljava/lang/String;
    :try_start_5
    throw v7
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 263
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local v0    # "tempFile":Ljava/io/File;
    .restart local v1    # "journal":Lcom/android/internal/util/JournaledFile;
    .restart local v2    # "writeTarget":Ljava/io/File;
    .restart local v3    # "fstr":Ljava/io/FileOutputStream;
    .restart local v4    # "str":Ljava/io/BufferedOutputStream;
    .restart local p0    # "pkg":Ljava/lang/String;
    :catchall_1
    move-exception v5

    goto :goto_2

    .line 259
    :catch_0
    move-exception v5

    .line 260
    .local v5, "e":Ljava/lang/Exception;
    :try_start_6
    sget-object v6, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v7, "Failed to delete preinstallPAI.list + "

    invoke-static {v6, v7, v5}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 261
    invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->rollback()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 263
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 264
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 265
    nop

    .line 267
    sget-object v5, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Delete package:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from preinstallPAI.list"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    return-void

    .line 263
    :goto_2
    invoke-static {v3}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 264
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 265
    throw v5

    .line 233
    .end local v0    # "tempFile":Ljava/io/File;
    .end local v1    # "journal":Lcom/android/internal/util/JournaledFile;
    .end local v2    # "writeTarget":Ljava/io/File;
    .end local v3    # "fstr":Ljava/io/FileOutputStream;
    .end local v4    # "str":Ljava/io/BufferedOutputStream;
    :catchall_2
    move-exception v1

    :try_start_7
    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v1
.end method

.method private static restoreconPreinstallDir()V
    .locals 3

    .line 193
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/miui/pai/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 194
    .local v0, "file":Ljava/io/File;
    invoke-static {v1}, Landroid/os/SELinux;->getFileContext(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "fileContext":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "u:object_r:miui_pai_file:s0"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 196
    invoke-static {v0}, Landroid/os/SELinux;->restoreconRecursive(Ljava/io/File;)Z

    .line 198
    :cond_0
    return-void
.end method

.method private static writeNewPAITrackFile()V
    .locals 6

    .line 138
    sget-object v0, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v1, "Write old track file content to  /data/miui/pai/pre_install.appsflyer"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    sget-object v1, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sTraditionalTrackContentList:Ljava/util/List;

    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_3

    .line 143
    :cond_0
    const/4 v0, 0x0

    .line 145
    .local v0, "bufferWriter":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v2, "/data/miui/pai/pre_install.appsflyer"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 146
    .local v1, "newFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    const/4 v3, -0x1

    if-nez v2, :cond_1

    .line 147
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 148
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    const/16 v4, 0x1fd

    invoke-static {v2, v4, v3, v3}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 150
    :cond_1
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/FileWriter;

    const/4 v5, 0x1

    invoke-direct {v4, v1, v5}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct {v2, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object v0, v2

    .line 151
    const/16 v2, 0x1b4

    invoke-static {v1, v2, v3, v3}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 152
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sTraditionalTrackContentList:Ljava/util/List;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 153
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    :try_start_1
    sget-object v4, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sTraditionalTrackContentList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    .line 154
    sget-object v4, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sTraditionalTrackContentList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 155
    .local v4, "content":Ljava/lang/String;
    sget-object v5, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sNewTrackContentList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 156
    sget-object v5, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sNewTrackContentList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-virtual {v0, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 158
    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 153
    .end local v4    # "content":Ljava/lang/String;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 161
    .end local v3    # "i":I
    :cond_3
    monitor-exit v2

    .line 167
    .end local v1    # "newFile":Ljava/io/File;
    goto :goto_1

    .line 161
    .restart local v1    # "newFile":Ljava/io/File;
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 167
    .end local v1    # "newFile":Ljava/io/File;
    .restart local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v1

    goto :goto_2

    .line 162
    :catch_0
    move-exception v1

    .line 163
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v3, "Error occurs when to write track file from /cust/etc/pre_install.appsflyer to /data/miui/pai/pre_install.appsflyer"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 167
    nop

    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 168
    nop

    .line 169
    return-void

    .line 167
    :goto_2
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 168
    throw v1

    .line 140
    .end local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    :cond_4
    :goto_3
    const-string v1, "no content write to new appsflyer file"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    return-void
.end method

.method public static writePreinstallPAIPackage(Ljava/lang/String;)V
    .locals 5
    .param p0, "pkg"    # Ljava/lang/String;

    .line 201
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x0

    .line 206
    .local v0, "bufferWriter":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/FileWriter;

    const-string v3, "/data/system/preinstallPAI.list"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object v0, v1

    .line 207
    sget-object v1, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 208
    :try_start_1
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->sPackagePAIList:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    invoke-virtual {v0, p0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 210
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 211
    monitor-exit v1

    .line 215
    goto :goto_0

    .line 211
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    .end local p0    # "pkg":Ljava/lang/String;
    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 215
    .restart local v0    # "bufferWriter":Ljava/io/BufferedWriter;
    .restart local p0    # "pkg":Ljava/lang/String;
    :catchall_1
    move-exception v1

    goto :goto_1

    .line 212
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v2, Lcom/android/server/pm/MiuiPAIPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v3, "Error occurs when to write PAI package name."

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 215
    nop

    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 216
    nop

    .line 217
    return-void

    .line 215
    :goto_1
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 216
    throw v1
.end method
