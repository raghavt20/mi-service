.class Lcom/android/server/pm/PreinstallApp$Item;
.super Ljava/lang/Object;
.source "PreinstallApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PreinstallApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Item"
.end annotation


# static fields
.field static final TYPE_CLUSTER:I = 0x2

.field static final TYPE_MONOLITHIC:I = 0x1

.field static final TYPE_SPLIT:I = 0x3


# instance fields
.field apkFile:Ljava/io/File;

.field app:Ljava/io/File;

.field packageName:Ljava/lang/String;

.field pkg:Landroid/content/pm/parsing/ApkLite;

.field pkgLite:Landroid/content/pm/parsing/PackageLite;

.field type:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/io/File;Landroid/content/pm/parsing/PackageLite;Landroid/content/pm/parsing/ApkLite;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "pkgLite"    # Landroid/content/pm/parsing/PackageLite;
    .param p4, "pkg"    # Landroid/content/pm/parsing/ApkLite;

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/android/server/pm/PreinstallApp$Item;->packageName:Ljava/lang/String;

    .line 86
    iput-object p2, p0, Lcom/android/server/pm/PreinstallApp$Item;->app:Ljava/io/File;

    .line 87
    invoke-static {p2}, Lcom/android/server/pm/PreinstallApp;->-$$Nest$smgetApkFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->apkFile:Ljava/io/File;

    .line 88
    iput-object p3, p0, Lcom/android/server/pm/PreinstallApp$Item;->pkgLite:Landroid/content/pm/parsing/PackageLite;

    .line 89
    iput-object p4, p0, Lcom/android/server/pm/PreinstallApp$Item;->pkg:Landroid/content/pm/parsing/ApkLite;

    .line 90
    invoke-static {p2}, Lcom/android/server/pm/PreinstallApp;->-$$Nest$smisSplitApk(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->type:I

    goto :goto_1

    .line 93
    :cond_0
    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->type:I

    .line 95
    :goto_1
    return-void
.end method

.method static betterThan(Lcom/android/server/pm/PreinstallApp$Item;Lcom/android/server/pm/PreinstallApp$Item;)Z
    .locals 4
    .param p0, "newItem"    # Lcom/android/server/pm/PreinstallApp$Item;
    .param p1, "oldItem"    # Lcom/android/server/pm/PreinstallApp$Item;

    .line 98
    nop

    .line 105
    iget v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->type:I

    iget v1, p1, Lcom/android/server/pm/PreinstallApp$Item;->type:I

    const/4 v2, 0x1

    if-le v0, v1, :cond_0

    .line 107
    return v2

    .line 108
    :cond_0
    const/4 v3, 0x0

    if-ne v0, v1, :cond_2

    .line 110
    iget-object v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->pkgLite:Landroid/content/pm/parsing/PackageLite;

    invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I

    move-result v0

    iget-object v1, p1, Lcom/android/server/pm/PreinstallApp$Item;->pkgLite:Landroid/content/pm/parsing/PackageLite;

    invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I

    move-result v1

    if-le v0, v1, :cond_1

    goto :goto_0

    :cond_1
    move v2, v3

    :goto_0
    return v2

    .line 113
    :cond_2
    return v3
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/pm/PreinstallApp$Item;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/pm/PreinstallApp$Item;->apkFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/pm/PreinstallApp$Item;->pkgLite:Landroid/content/pm/parsing/PackageLite;

    invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
