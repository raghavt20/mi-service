.class public Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "AdvancePreinstallService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/AdvancePreinstallService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PackageDeleteObserver"
.end annotation


# instance fields
.field private mAppType:Ljava/lang/String;

.field private mConfId:I

.field private mOfflineCount:I

.field private mPackageName:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/server/pm/AdvancePreinstallService;


# direct methods
.method public constructor <init>(Lcom/android/server/pm/AdvancePreinstallService;Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/AdvancePreinstallService;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "confId"    # I
    .param p4, "offlineCount"    # I
    .param p5, "appType"    # Ljava/lang/String;

    .line 915
    iput-object p1, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    .line 916
    iput-object p2, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mPackageName:Ljava/lang/String;

    .line 917
    iput p3, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mConfId:I

    .line 918
    iput p4, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mOfflineCount:I

    .line 919
    iput-object p5, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mAppType:Ljava/lang/String;

    .line 920
    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .line 923
    const-string v0, "AdvancePreinstallService"

    if-ltz p2, :cond_0

    .line 924
    iget-object v1, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    const-string/jumbo v2, "uninstall_success"

    iget-object v3, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mPackageName:Ljava/lang/String;

    iget v4, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mConfId:I

    iget v5, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mOfflineCount:I

    iget-object v6, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mAppType:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 926
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was uninstalled."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 929
    :cond_0
    iget-object v2, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    const-string/jumbo v3, "uninstall_failed"

    iget-object v4, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mPackageName:Ljava/lang/String;

    iget v5, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mConfId:I

    iget v6, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mOfflineCount:I

    iget-object v7, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mAppType:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 931
    iget-object v1, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-static {v1}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$fgetmPackageManager(Lcom/android/server/pm/AdvancePreinstallService;)Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/AdvancePreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 932
    iget-object v1, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    iget-object v2, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mPackageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$mtrackAdvancePreloadSuccess(Lcom/android/server/pm/AdvancePreinstallService;Ljava/lang/String;)V

    .line 934
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Package uninstall failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", returnCode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    :goto_0
    return-void
.end method
