.class Lcom/android/server/pm/DexoptServiceThread$1;
.super Ljava/lang/Object;
.source "DexoptServiceThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/DexoptServiceThread;->performDexOptAsyncTask(Lcom/android/server/pm/dex/DexoptOptions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/DexoptServiceThread;

.field final synthetic val$options:Lcom/android/server/pm/dex/DexoptOptions;


# direct methods
.method constructor <init>(Lcom/android/server/pm/DexoptServiceThread;Lcom/android/server/pm/dex/DexoptOptions;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/DexoptServiceThread;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 185
    iput-object p1, p0, Lcom/android/server/pm/DexoptServiceThread$1;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iput-object p2, p0, Lcom/android/server/pm/DexoptServiceThread$1;->val$options:Lcom/android/server/pm/dex/DexoptOptions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 188
    iget-object v0, p0, Lcom/android/server/pm/DexoptServiceThread$1;->val$options:Lcom/android/server/pm/dex/DexoptOptions;

    invoke-virtual {v0}, Lcom/android/server/pm/dex/DexoptOptions;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, "dexoptPackagename":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$1;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v1}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptPackageNameList(Lcom/android/server/pm/DexoptServiceThread;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 190
    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$1;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v1}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptPackageNameList(Lcom/android/server/pm/DexoptServiceThread;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$1;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$1;->val$options:Lcom/android/server/pm/dex/DexoptOptions;

    invoke-static {v1, v2}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$mperformDexOptInternal(Lcom/android/server/pm/DexoptServiceThread;Lcom/android/server/pm/dex/DexoptOptions;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fputmDexoptResult(Lcom/android/server/pm/DexoptServiceThread;I)V

    .line 194
    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$1;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v1}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptResult(Lcom/android/server/pm/DexoptServiceThread;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$1;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v1}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptResult(Lcom/android/server/pm/DexoptServiceThread;)I

    move-result v1

    if-nez v1, :cond_2

    .line 196
    :cond_1
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$sfgetmWaitLock()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 197
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$1;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v2}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptPackageNameList(Lcom/android/server/pm/DexoptServiceThread;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 198
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$sfgetmWaitLock()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 199
    monitor-exit v1

    .line 201
    :cond_2
    return-void

    .line 199
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
