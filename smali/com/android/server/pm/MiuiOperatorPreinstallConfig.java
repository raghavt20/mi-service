public class com.android.server.pm.MiuiOperatorPreinstallConfig extends com.android.server.pm.MiuiPreinstallConfig {
	 /* .source "MiuiOperatorPreinstallConfig.java" */
	 /* # static fields */
	 private static final java.lang.String ALLOW_INSTALL_THIRD_APP_LIST;
	 private static final java.lang.String DEL_APPS_LIST;
	 private static final java.lang.String DISALLOW_INSTALL_THIRD_APP_LIST;
	 private static final java.lang.String MIUI_OPERATOR_PREINALL_PATH;
	 private static final java.lang.String O2SPACE_PACKAGE_NAME;
	 private static final java.lang.String OPERATOR_PREINSTALL_INFO_DIR;
	 private static final java.lang.String OPERATOR_PRODUCT_DATA_APP_LIST;
	 private static final java.io.File PRODUCT_CARRIER_APP_DIR;
	 private static final java.lang.String PRODUCT_CARRIER_DIR;
	 private static final java.io.File PRODUCT_DATA_APP_DIR;
	 private static final java.lang.String TAG;
	 private static final java.lang.String sCustomizedRegion;
	 /* # instance fields */
	 private final java.util.Set sAllowOnlyInstallThirdApps;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final java.util.Set sCotaDelApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Set sDisallowInstallThirdApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.pm.MiuiOperatorPreinstallConfig ( ) {
/* .locals 2 */
/* .line 26 */
/* const-class v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 28 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/opcust/data-app"; // const-string v1, "/product/opcust/data-app"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 31 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/data-app"; // const-string v1, "/product/data-app"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 46 */
/* nop */
/* .line 47 */
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 46 */
return;
} // .end method
protected com.android.server.pm.MiuiOperatorPreinstallConfig ( ) {
/* .locals 1 */
/* .line 50 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallConfig;-><init>()V */
/* .line 41 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.sAllowOnlyInstallThirdApps = v0;
/* .line 42 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.sDisallowInstallThirdApps = v0;
/* .line 43 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.sCotaDelApps = v0;
/* .line 51 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->initCotaDelAppsList()V */
/* .line 52 */
return;
} // .end method
private void addPreinstallAppToList ( java.util.List p0, java.io.File p1, java.util.Set p2 ) {
/* .locals 8 */
/* .param p2, "appDir" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;", */
/* "Ljava/io/File;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 140 */
/* .local p1, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .local p3, "filterSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
(( java.io.File ) p2 ).listFiles ( ); // invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 141 */
/* .local v0, "apps":[Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 143 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_4 */
/* aget-object v3, v0, v2 */
/* .line 144 */
/* .local v3, "app":Ljava/io/File; */
v4 = (( java.io.File ) v3 ).isDirectory ( ); // invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z
final String v5 = ".apk"; // const-string v5, ".apk"
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 146 */
/* new-instance v4, Ljava/io/File; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) v3 ).getName ( ); // invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 147 */
/* .local v4, "apk":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* if-nez v5, :cond_1 */
/* .line 148 */
/* .line 152 */
} // .end local v4 # "apk":Ljava/io/File;
} // :cond_0
/* move-object v4, v3 */
/* .line 153 */
/* .restart local v4 # "apk":Ljava/io/File; */
v6 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v6 != null) { // if-eqz v6, :cond_3
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
v5 = (( java.lang.String ) v6 ).endsWith ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
/* if-nez v5, :cond_1 */
/* .line 154 */
/* .line 157 */
} // :cond_1
if ( p3 != null) { // if-eqz p3, :cond_2
v5 = (( java.io.File ) v4 ).getName ( ); // invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;
/* if-nez v5, :cond_2 */
/* .line 158 */
/* .line 160 */
} // :cond_2
/* .line 143 */
} // .end local v3 # "app":Ljava/io/File;
} // :cond_3
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 163 */
} // .end local v4 # "apk":Ljava/io/File;
} // :cond_4
return;
} // .end method
private void initCotaDelAppsList ( ) {
/* .locals 8 */
/* .line 221 */
int v0 = 0; // const/4 v0, 0x0
/* .line 223 */
/* .local v0, "inputStream":Ljava/io/InputStream; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
final String v2 = "/product/opcust/common/del_apps_list.xml"; // const-string v2, "/product/opcust/common/del_apps_list.xml"
/* invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 224 */
org.xmlpull.v1.XmlPullParserFactory .newInstance ( );
/* .line 225 */
/* .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory; */
(( org.xmlpull.v1.XmlPullParserFactory ) v1 ).newPullParser ( ); // invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
/* .line 226 */
/* .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser; */
final String v3 = "UTF-8"; // const-string v3, "UTF-8"
v3 = /* .line 227 */
/* .line 228 */
/* .local v3, "event":I */
} // :goto_0
int v4 = 1; // const/4 v4, 0x1
/* if-eq v3, v4, :cond_2 */
/* .line 229 */
/* packed-switch v3, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 244 */
/* :pswitch_1 */
/* .line 233 */
/* :pswitch_2 */
/* .line 234 */
/* .local v4, "name":Ljava/lang/String; */
final String v5 = "package"; // const-string v5, "package"
v5 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 235 */
final String v5 = "name"; // const-string v5, "name"
int v6 = 0; // const/4 v6, 0x0
/* .line 236 */
/* .local v5, "pkgName":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 237 */
v6 = com.android.server.pm.MiuiOperatorPreinstallConfig.TAG;
final String v7 = "initCotaDelApps pkgName is null, skip parse this tag"; // const-string v7, "initCotaDelApps pkgName is null, skip parse this tag"
android.util.Slog .e ( v6,v7 );
/* .line 238 */
/* .line 240 */
} // :cond_0
v6 = this.sCotaDelApps;
/* .line 241 */
/* nop */
} // .end local v5 # "pkgName":Ljava/lang/String;
/* .line 231 */
} // .end local v4 # "name":Ljava/lang/String;
/* :pswitch_3 */
/* nop */
/* .line 248 */
} // :cond_1
v4 = } // :goto_1
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v3, v4 */
/* .line 254 */
} // .end local v1 # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
} // .end local v2 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v3 # "event":I
} // :cond_2
/* nop */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 255 */
/* .line 254 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 251 */
/* :catch_0 */
/* move-exception v1 */
/* .line 252 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
v2 = com.android.server.pm.MiuiOperatorPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "initCotaDelApps fail: "; // const-string v4, "initCotaDelApps fail: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 254 */
/* nop */
} // .end local v1 # "e":Ljava/lang/Exception;
/* .line 256 */
} // :goto_3
return;
/* .line 254 */
} // :goto_4
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 255 */
/* throw v1 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void readAllowOnlyInstallThirdAppForCarrier ( ) {
/* .locals 9 */
/* .line 170 */
final String v0 = "disallow_install_third_app.list"; // const-string v0, "disallow_install_third_app.list"
final String v1 = "allow_install_third_app.list"; // const-string v1, "allow_install_third_app.list"
final String v2 = "/product/opcust/"; // const-string v2, "/product/opcust/"
final String v3 = "persist.sys.cota.carrier"; // const-string v3, "persist.sys.cota.carrier"
final String v4 = ""; // const-string v4, ""
android.os.SystemProperties .get ( v3,v4 );
/* .line 171 */
/* .local v3, "cotaCarrier":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_4 */
final String v4 = "XM"; // const-string v4, "XM"
v4 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_4 */
/* .line 173 */
try { // :try_start_0
final String v4 = "ro.miui.region"; // const-string v4, "ro.miui.region"
final String v5 = "cn"; // const-string v5, "cn"
android.os.SystemProperties .get ( v4,v5 );
(( java.lang.String ) v4 ).toLowerCase ( ); // invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
/* .line 174 */
/* .local v4, "region":Ljava/lang/String; */
/* new-instance v5, Ljava/io/File; */
/* new-instance v6, Ljava/io/File; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v6, v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v5, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 176 */
/* .local v5, "customizedAppSetFile":Ljava/io/File; */
v6 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v7 = "/product/opcust"; // const-string v7, "/product/opcust"
/* if-nez v6, :cond_0 */
/* .line 177 */
try { // :try_start_1
/* new-instance v6, Ljava/io/File; */
/* new-instance v8, Ljava/io/File; */
/* invoke-direct {v8, v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v6, v8, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v5, v6 */
/* .line 180 */
} // :cond_0
v1 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 181 */
v0 = this.sAllowOnlyInstallThirdApps;
(( com.android.server.pm.MiuiOperatorPreinstallConfig ) p0 ).readLineToSet ( v5, v0 ); // invoke-virtual {p0, v5, v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 182 */
return;
/* .line 185 */
} // :cond_1
/* new-instance v1, Ljava/io/File; */
/* new-instance v6, Ljava/io/File; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v6, v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v1, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 187 */
} // .end local v5 # "customizedAppSetFile":Ljava/io/File;
/* .local v1, "customizedAppSetFile":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_2 */
/* .line 188 */
/* new-instance v2, Ljava/io/File; */
/* new-instance v5, Ljava/io/File; */
/* invoke-direct {v5, v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v2, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 191 */
} // :cond_2
v0 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 192 */
v0 = this.sDisallowInstallThirdApps;
(( com.android.server.pm.MiuiOperatorPreinstallConfig ) p0 ).readLineToSet ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 196 */
} // .end local v1 # "customizedAppSetFile":Ljava/io/File;
} // .end local v4 # "region":Ljava/lang/String;
} // :cond_3
/* .line 194 */
/* :catch_0 */
/* move-exception v0 */
/* .line 195 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.pm.MiuiOperatorPreinstallConfig.TAG;
final String v2 = "Error occurs when to read allow install third app list."; // const-string v2, "Error occurs when to read allow install third app list."
android.util.Slog .e ( v1,v2 );
/* .line 198 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_4
} // :goto_0
return;
} // .end method
private Boolean skipInstallByCota ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "apkFilePath" # Ljava/lang/String; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 202 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 203 */
if ( p1 != null) { // if-eqz p1, :cond_3
final String v1 = "/system/data-app"; // const-string v1, "/system/data-app"
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v1, :cond_3 */
/* .line 204 */
final String v1 = "/vendor/data-app"; // const-string v1, "/vendor/data-app"
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v1, :cond_3 */
/* .line 205 */
final String v1 = "/product/data-app"; // const-string v1, "/product/data-app"
v1 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v1, :cond_3 */
/* .line 206 */
v1 = v1 = this.sAllowOnlyInstallThirdApps;
/* if-lez v1, :cond_0 */
v1 = this.sAllowOnlyInstallThirdApps;
v1 = /* .line 207 */
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_0
v1 = this.sDisallowInstallThirdApps;
v1 = /* .line 208 */
/* if-lez v1, :cond_2 */
v1 = this.sDisallowInstallThirdApps;
v1 = /* .line 209 */
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
/* nop */
/* .line 206 */
} // :goto_0
/* .line 212 */
} // :cond_3
} // .end method
private Boolean skipInstallDataAppByCota ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 260 */
v0 = v0 = this.sCotaDelApps;
int v1 = 0; // const/4 v1, 0x0
/* if-lez v0, :cond_1 */
v0 = v0 = this.sCotaDelApps;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 261 */
final String v0 = "persist.sys.cota.carrier"; // const-string v0, "persist.sys.cota.carrier"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v0,v2 );
/* .line 262 */
/* .local v0, "cotaCarrier":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_0 */
final String v2 = "XM"; // const-string v2, "XM"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 264 */
} // .end local v0 # "cotaCarrier":Ljava/lang/String;
} // :cond_1
} // .end method
private Boolean skipInstallO2Space ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 216 */
final String v0 = "persist.sys.carrier.subnetwork"; // const-string v0, "persist.sys.carrier.subnetwork"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* const-string/jumbo v1, "tef_o2" */
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 217 */
/* const-string/jumbo v0, "uk.co.o2.android.o2space" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 216 */
} // :goto_0
} // .end method
/* # virtual methods */
protected java.util.List getCustAppList ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 118 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 119 */
/* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 120 */
/* .local v1, "productCarrierAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
miui.util.CustomizeUtil .getProductCarrierRegionAppFile ( );
/* .line 121 */
/* .local v2, "CarrierRegionAppFile":Ljava/io/File; */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readAllowOnlyInstallThirdAppForCarrier()V */
/* .line 122 */
if ( v2 != null) { // if-eqz v2, :cond_0
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 123 */
(( com.android.server.pm.MiuiOperatorPreinstallConfig ) p0 ).readLineToSet ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 124 */
v3 = com.android.server.pm.MiuiOperatorPreinstallConfig.PRODUCT_CARRIER_APP_DIR;
/* invoke-direct {p0, v0, v3, v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V */
/* .line 126 */
} // :cond_0
} // .end method
protected java.util.List getLegacyPreinstallList ( Boolean p0, Boolean p1 ) {
/* .locals 8 */
/* .param p1, "isFirstBoot" # Z */
/* .param p2, "isDeviceUpgrading" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(ZZ)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 56 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 57 */
/* .local v0, "legacyPreinstallApkList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 58 */
/* .local v1, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 60 */
/* .local v2, "productCustomizedAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 62 */
/* .local v3, "productDataAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v4, Ljava/io/File; */
final String v5 = "/mi_ext/product/etc/"; // const-string v5, "/mi_ext/product/etc/"
final String v6 = "operator_product_data_app.list"; // const-string v6, "operator_product_data_app.list"
/* invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 65 */
/* .local v4, "productCustomizedAppListFile":Ljava/io/File; */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 66 */
(( com.android.server.pm.MiuiOperatorPreinstallConfig ) p0 ).getCustAppList ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->getCustAppList()Ljava/util/List;
v5 = /* .line 67 */
/* if-nez v5, :cond_0 */
/* .line 68 */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_0
/* check-cast v6, Ljava/io/File; */
/* .line 69 */
/* .local v6, "app":Ljava/io/File; */
(( java.io.File ) v6 ).getPath ( ); // invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 70 */
} // .end local v6 # "app":Ljava/io/File;
/* .line 74 */
} // :cond_0
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 75 */
(( com.android.server.pm.MiuiOperatorPreinstallConfig ) p0 ).readLineToSet ( v4, v3 ); // invoke-virtual {p0, v4, v3}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 76 */
v5 = com.android.server.pm.MiuiOperatorPreinstallConfig.PRODUCT_DATA_APP_DIR;
/* invoke-direct {p0, v2, v5, v3}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V */
v5 = /* .line 78 */
/* if-nez v5, :cond_1 */
/* .line 79 */
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_1
/* check-cast v6, Ljava/io/File; */
/* .line 80 */
/* .restart local v6 # "app":Ljava/io/File; */
(( java.io.File ) v6 ).getPath ( ); // invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 81 */
} // .end local v6 # "app":Ljava/io/File;
/* .line 84 */
} // :cond_1
} // .end method
protected java.util.List getPreinstallDirs ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 108 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected java.util.List getVanwardAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean isOperatorPreinstall ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 135 */
final String v0 = "/product/opcust/data-app"; // const-string v0, "/product/opcust/data-app"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
} // .end method
protected Boolean needIgnore ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 89 */
if ( p1 != null) { // if-eqz p1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 90 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->skipInstallByCota(Ljava/lang/String;Ljava/lang/String;)Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 91 */
v0 = com.android.server.pm.MiuiOperatorPreinstallConfig.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "cota skip install cust preinstall data-app, :"; // const-string v3, "cota skip install cust preinstall data-app, :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v2 );
/* .line 92 */
/* .line 94 */
} // :cond_0
final String v0 = "es_telefonica"; // const-string v0, "es_telefonica"
v2 = com.android.server.pm.MiuiOperatorPreinstallConfig.sCustomizedRegion;
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->skipInstallO2Space(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 95 */
v0 = com.android.server.pm.MiuiOperatorPreinstallConfig.TAG;
/* const-string/jumbo v2, "skip install O2Space" */
android.util.Slog .i ( v0,v2 );
/* .line 96 */
/* .line 98 */
} // :cond_1
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->skipInstallDataAppByCota(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 99 */
v0 = com.android.server.pm.MiuiOperatorPreinstallConfig.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skip install app because cota-reject :" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v2 );
/* .line 100 */
/* .line 103 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean needLegacyPreinstall ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 131 */
v0 = (( com.android.server.pm.MiuiOperatorPreinstallConfig ) p0 ).isOperatorPreinstall ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->isOperatorPreinstall(Ljava/lang/String;)Z
} // .end method
