class com.android.server.pm.InstallerUtil$LocalIntentReceiver {
	 /* .source "InstallerUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/InstallerUtil; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "LocalIntentReceiver" */
} // .end annotation
/* # instance fields */
private android.content.IIntentSender$Stub mLocalSender;
private final java.util.concurrent.LinkedBlockingQueue mResult;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/LinkedBlockingQueue<", */
/* "Landroid/content/Intent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.util.concurrent.LinkedBlockingQueue -$$Nest$fgetmResult ( com.android.server.pm.InstallerUtil$LocalIntentReceiver p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mResult;
} // .end method
public com.android.server.pm.InstallerUtil$LocalIntentReceiver ( ) {
/* .locals 1 */
/* .param p1, "capacity" # I */
/* .line 336 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 340 */
/* new-instance v0, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver$1;-><init>(Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;)V */
this.mLocalSender = v0;
/* .line 337 */
/* new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue; */
/* invoke-direct {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V */
this.mResult = v0;
/* .line 338 */
return;
} // .end method
/* # virtual methods */
public android.content.IntentSender getIntentSender ( ) {
/* .locals 2 */
/* .line 349 */
/* new-instance v0, Landroid/content/IntentSender; */
v1 = this.mLocalSender;
/* invoke-direct {v0, v1}, Landroid/content/IntentSender;-><init>(Landroid/content/IIntentSender;)V */
} // .end method
public android.content.Intent getResult ( ) {
/* .locals 5 */
/* .line 362 */
int v0 = 0; // const/4 v0, 0x0
/* .line 364 */
/* .local v0, "intent":Landroid/content/Intent; */
try { // :try_start_0
v1 = this.mResult;
v2 = java.util.concurrent.TimeUnit.SECONDS;
/* const-wide/16 v3, 0x1e */
(( java.util.concurrent.LinkedBlockingQueue ) v1 ).poll ( v3, v4, v2 ); // invoke-virtual {v1, v3, v4, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
/* check-cast v1, Landroid/content/Intent; */
/* :try_end_0 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 368 */
/* .line 365 */
/* :catch_0 */
/* move-exception v1 */
/* .line 367 */
/* .local v1, "e":Ljava/lang/InterruptedException; */
final String v2 = "InstallerUtil"; // const-string v2, "InstallerUtil"
final String v3 = "LocalIntentReceiver poll timeout in 30 seconds."; // const-string v3, "LocalIntentReceiver poll timeout in 30 seconds."
android.util.Slog .e ( v2,v3 );
/* .line 369 */
} // .end local v1 # "e":Ljava/lang/InterruptedException;
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v1, v0 */
} // :cond_0
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
} // :goto_1
} // .end method
public java.util.List getResultsNoWait ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/content/Intent;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 353 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 355 */
/* .local v0, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;" */
try { // :try_start_0
v1 = this.mResult;
(( java.util.concurrent.LinkedBlockingQueue ) v1 ).drainTo ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->drainTo(Ljava/util/Collection;)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 357 */
/* .line 356 */
/* :catch_0 */
/* move-exception v1 */
/* .line 358 */
} // :goto_0
} // .end method
