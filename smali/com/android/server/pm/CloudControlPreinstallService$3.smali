.class Lcom/android/server/pm/CloudControlPreinstallService$3;
.super Ljava/lang/Object;
.source "CloudControlPreinstallService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/CloudControlPreinstallService;->uninstallPreinstallApps()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/CloudControlPreinstallService;


# direct methods
.method constructor <init>(Lcom/android/server/pm/CloudControlPreinstallService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/CloudControlPreinstallService;

    .line 319
    iput-object p1, p0, Lcom/android/server/pm/CloudControlPreinstallService$3;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 323
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService$3;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    const-string v1, "cloud_uninstall_start"

    invoke-virtual {v0, v1}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService$3;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/CloudControlPreinstallService;->-$$Nest$mgetUninstallApps(Lcom/android/server/pm/CloudControlPreinstallService;)Ljava/util/List;

    move-result-object v0

    .line 326
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;>;"
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/android/server/pm/CloudControlPreinstallService$3;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-static {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->-$$Nest$mrecordUnInstallApps(Lcom/android/server/pm/CloudControlPreinstallService;Ljava/util/List;)V

    goto :goto_0

    .line 329
    :cond_0
    iget-object v1, p0, Lcom/android/server/pm/CloudControlPreinstallService$3;->this$0:Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-static {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->-$$Nest$muninstallAppsUpdateList(Lcom/android/server/pm/CloudControlPreinstallService;Ljava/util/List;)V

    .line 331
    :goto_0
    return-void
.end method
