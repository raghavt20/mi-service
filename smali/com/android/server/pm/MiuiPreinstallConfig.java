public abstract class com.android.server.pm.MiuiPreinstallConfig {
	 /* .source "MiuiPreinstallConfig.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static com.android.server.pm.MiuiPreinstallConfig ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 /* const-class v0, Lcom/android/server/pm/MiuiPreinstallConfig; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.server.pm.MiuiPreinstallConfig ( ) {
		 /* .locals 0 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static Boolean isValidIme ( java.lang.String p0, java.util.Locale p1 ) {
		 /* .locals 5 */
		 /* .param p0, "locale" # Ljava/lang/String; */
		 /* .param p1, "curLocale" # Ljava/util/Locale; */
		 /* .line 62 */
		 final String v0 = ","; // const-string v0, ","
		 (( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
		 /* .line 63 */
		 /* .local v0, "locales":[Ljava/lang/String; */
		 int v1 = 0; // const/4 v1, 0x0
		 /* .local v1, "i":I */
	 } // :goto_0
	 /* array-length v2, v0 */
	 /* if-ge v1, v2, :cond_2 */
	 /* .line 64 */
	 /* aget-object v2, v0, v1 */
	 (( java.util.Locale ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;
	 v2 = 	 (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
	 /* if-nez v2, :cond_1 */
	 /* aget-object v2, v0, v1 */
	 /* .line 65 */
	 final String v3 = "*"; // const-string v3, "*"
	 v2 = 	 (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 /* if-nez v2, :cond_1 */
	 /* aget-object v2, v0, v1 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 /* .line 66 */
	 (( java.util.Locale ) p1 ).getLanguage ( ); // invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v4 = "_*"; // const-string v4, "_*"
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 v2 = 	 (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 63 */
	 } // :cond_0
	 /* add-int/lit8 v1, v1, 0x1 */
	 /* .line 67 */
} // :cond_1
} // :goto_1
int v2 = 1; // const/4 v2, 0x1
/* .line 70 */
} // .end local v1 # "i":I
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
/* # virtual methods */
protected abstract java.util.List getCustAppList ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
protected abstract java.util.List getLegacyPreinstallList ( Boolean p0, Boolean p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(ZZ)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
protected abstract java.util.List getPreinstallDirs ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
protected abstract java.util.List getVanwardAppList ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
protected abstract Boolean needIgnore ( java.lang.String p0, java.lang.String p1 ) {
} // .end method
protected abstract Boolean needLegacyPreinstall ( java.lang.String p0, java.lang.String p1 ) {
} // .end method
protected void readLineToSet ( java.io.File p0, java.util.Set p1 ) {
/* .locals 7 */
/* .param p1, "file" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/io/File;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 37 */
/* .local p2, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v0 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 38 */
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v1, Ljava/io/InputStreamReader; */
/* new-instance v2, Ljava/io/FileInputStream; */
/* invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 41 */
/* .local v0, "bufferedReader":Ljava/io/BufferedReader; */
try { // :try_start_1
(( java.io.File ) p1 ).getName ( ); // invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;
/* const-string/jumbo v2, "vanward_applist" */
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 42 */
android.app.ActivityManagerNative .getDefault ( );
/* .line 43 */
/* .local v1, "am":Landroid/app/IActivityManager; */
v2 = this.locale;
/* .line 44 */
/* .local v2, "curLocale":Ljava/util/Locale; */
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "line":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 45 */
(( java.lang.String ) v4 ).trim ( ); // invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
final String v5 = "\\s+"; // const-string v5, "\\s+"
(( java.lang.String ) v3 ).split ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 46 */
/* .local v3, "ss":[Ljava/lang/String; */
/* array-length v5, v3 */
int v6 = 2; // const/4 v6, 0x2
/* if-ne v5, v6, :cond_0 */
int v5 = 1; // const/4 v5, 0x1
/* aget-object v5, v3, v5 */
v5 = com.android.server.pm.MiuiPreinstallConfig .isValidIme ( v5,v2 );
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 47 */
int v5 = 0; // const/4 v5, 0x0
/* aget-object v5, v3, v5 */
/* .line 49 */
} // .end local v3 # "ss":[Ljava/lang/String;
} // :cond_0
/* .line 50 */
} // .end local v1 # "am":Landroid/app/IActivityManager;
} // .end local v2 # "curLocale":Ljava/util/Locale;
} // :cond_1
/* .line 51 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_2
} // :goto_1
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v1 */
/* .local v2, "line":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 52 */
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 55 */
} // .end local v2 # "line":Ljava/lang/String;
} // :cond_3
} // :goto_2
try { // :try_start_2
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 57 */
} // .end local v0 # "bufferedReader":Ljava/io/BufferedReader;
/* .line 38 */
/* .restart local v0 # "bufferedReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_3
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_4
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/pm/MiuiPreinstallConfig;
} // .end local p1 # "file":Ljava/io/File;
} // .end local p2 # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // :goto_3
/* throw v1 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catch Landroid/os/RemoteException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 55 */
} // .end local v0 # "bufferedReader":Ljava/io/BufferedReader;
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiPreinstallConfig; */
/* .restart local p1 # "file":Ljava/io/File; */
/* .restart local p2 # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* :catch_0 */
/* move-exception v0 */
/* .line 56 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.pm.MiuiPreinstallConfig.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "readLineToSet: "; // const-string v3, "readLineToSet: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 59 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_4
} // :goto_4
return;
} // .end method
