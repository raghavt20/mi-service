class com.android.server.pm.AdvancePreinstallService$1 extends android.content.BroadcastReceiver {
	 /* .source "AdvancePreinstallService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/AdvancePreinstallService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.pm.AdvancePreinstallService this$0; //synthetic
/* # direct methods */
 com.android.server.pm.AdvancePreinstallService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/AdvancePreinstallService; */
/* .line 128 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 131 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onReceive:"; // const-string v1, "onReceive:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AdvancePreinstallService"; // const-string v1, "AdvancePreinstallService"
android.util.Slog .i ( v1,v0 );
/* .line 132 */
/* if-nez p2, :cond_0 */
/* .line 133 */
return;
/* .line 135 */
} // :cond_0
v0 = this.this$0;
v0 = com.android.server.pm.AdvancePreinstallService .-$$Nest$fgetmHasReceived ( v0 );
/* if-nez v0, :cond_1 */
/* .line 136 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.pm.AdvancePreinstallService .-$$Nest$fputmHasReceived ( v0,v1 );
/* .line 137 */
final String v0 = "com.miui.action.SIM_DETECTION"; // const-string v0, "com.miui.action.SIM_DETECTION"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 138 */
v0 = this.this$0;
com.android.server.pm.AdvancePreinstallService .-$$Nest$mhandleAdvancePreinstallAppsDelay ( v0 );
/* .line 141 */
} // :cond_1
return;
} // .end method
