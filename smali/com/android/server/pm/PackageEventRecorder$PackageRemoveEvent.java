class com.android.server.pm.PackageEventRecorder$PackageRemoveEvent extends com.android.server.pm.PackageEventRecorder$PackageEventBase {
	 /* .source "PackageEventRecorder.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/PackageEventRecorder; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "PackageRemoveEvent" */
} // .end annotation
/* # instance fields */
private final Boolean isRemovedFully;
/* # direct methods */
private com.android.server.pm.PackageEventRecorder$PackageRemoveEvent ( ) {
/* .locals 7 */
/* .param p1, "eventTime" # J */
/* .param p3, "pkgName" # Ljava/lang/String; */
/* .param p4, "userIds" # [I */
/* .param p5, "installer" # Ljava/lang/String; */
/* .param p6, "isRemovedFully" # Z */
/* .line 610 */
int v0 = 3; // const/4 v0, 0x3
com.android.server.pm.PackageEventRecorder$PackageRemoveEvent .buildPackageEventId ( p1,p2,v0 );
int v6 = 0; // const/4 v6, 0x0
/* move-object v1, p0 */
/* move-object v3, p3 */
/* move-object v4, p4 */
/* move-object v5, p5 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;-><init>(Ljava/lang/String;Ljava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageEventBase-IA;)V */
/* .line 611 */
/* iput-boolean p6, p0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->isRemovedFully:Z */
/* .line 612 */
return;
} // .end method
 com.android.server.pm.PackageEventRecorder$PackageRemoveEvent ( ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p6}, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;Z)V */
return;
} // .end method
private android.os.Bundle buildBundle ( ) {
/* .locals 3 */
/* .line 621 */
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$mbuildBundle ( p0 );
/* .line 622 */
/* .local v0, "result":Landroid/os/Bundle; */
final String v1 = "isRemovedFully"; // const-string v1, "isRemovedFully"
/* iget-boolean v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->isRemovedFully:Z */
(( android.os.Bundle ) v0 ).putBoolean ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 623 */
} // .end method
/* # virtual methods */
void WriteToTxt ( java.io.BufferedWriter p0 ) {
/* .locals 2 */
/* .param p1, "bufferedWriter" # Ljava/io/BufferedWriter; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 615 */
/* invoke-super {p0, p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->WriteToTxt(Ljava/io/BufferedWriter;)V */
/* .line 616 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "isRemovedFully="; // const-string v1, "isRemovedFully="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->isRemovedFully:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.BufferedWriter ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 617 */
/* const/16 v0, 0x20 */
(( java.io.BufferedWriter ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V
/* .line 618 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 628 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "PackageEvent{id=\'"; // const-string v1, "PackageEvent{id=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$fgetid ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", type=\'"; // const-string v2, ", type=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$fgetid ( p0 );
/* .line 630 */
v2 = com.android.server.pm.PackageEventRecorder$PackageRemoveEvent .resolveEventType ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", packageName=\'"; // const-string v2, ", packageName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$fgetpackageName ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", userIds="; // const-string v2, ", userIds="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$fgetuserIds ( p0 );
/* .line 632 */
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", installerPackageName=\'"; // const-string v2, ", installerPackageName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.pm.PackageEventRecorder$PackageEventBase .-$$Nest$fgetinstaller ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", isRemovedFully="; // const-string v1, ", isRemovedFully="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->isRemovedFully:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 628 */
} // .end method
