public class com.android.server.pm.AdvancePreinstallService$PackageDeleteObserver extends android.content.pm.IPackageDeleteObserver$Stub {
	 /* .source "AdvancePreinstallService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/AdvancePreinstallService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "PackageDeleteObserver" */
} // .end annotation
/* # instance fields */
private java.lang.String mAppType;
private Integer mConfId;
private Integer mOfflineCount;
private java.lang.String mPackageName;
final com.android.server.pm.AdvancePreinstallService this$0; //synthetic
/* # direct methods */
public com.android.server.pm.AdvancePreinstallService$PackageDeleteObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/AdvancePreinstallService; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "confId" # I */
/* .param p4, "offlineCount" # I */
/* .param p5, "appType" # Ljava/lang/String; */
/* .line 915 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V */
/* .line 916 */
this.mPackageName = p2;
/* .line 917 */
/* iput p3, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mConfId:I */
/* .line 918 */
/* iput p4, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mOfflineCount:I */
/* .line 919 */
this.mAppType = p5;
/* .line 920 */
return;
} // .end method
/* # virtual methods */
public void packageDeleted ( java.lang.String p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "returnCode" # I */
/* .line 923 */
final String v0 = "AdvancePreinstallService"; // const-string v0, "AdvancePreinstallService"
/* if-ltz p2, :cond_0 */
/* .line 924 */
v1 = this.this$0;
/* const-string/jumbo v2, "uninstall_success" */
v3 = this.mPackageName;
/* iget v4, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mConfId:I */
/* iget v5, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mOfflineCount:I */
v6 = this.mAppType;
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 926 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Package "; // const-string v2, "Package "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " was uninstalled."; // const-string v2, " was uninstalled."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 929 */
} // :cond_0
v2 = this.this$0;
/* const-string/jumbo v3, "uninstall_failed" */
v4 = this.mPackageName;
/* iget v5, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mConfId:I */
/* iget v6, p0, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;->mOfflineCount:I */
v7 = this.mAppType;
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 931 */
v1 = this.this$0;
com.android.server.pm.AdvancePreinstallService .-$$Nest$fgetmPackageManager ( v1 );
v3 = this.mPackageName;
v1 = (( com.android.server.pm.AdvancePreinstallService ) v1 ).exists ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/AdvancePreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 932 */
v1 = this.this$0;
v2 = this.mPackageName;
com.android.server.pm.AdvancePreinstallService .-$$Nest$mtrackAdvancePreloadSuccess ( v1,v2 );
/* .line 934 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Package uninstall failed "; // const-string v2, "Package uninstall failed "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", returnCode "; // const-string v2, ", returnCode "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 937 */
} // :goto_0
return;
} // .end method
