abstract class com.android.server.pm.PackageEventRecorder$PackageEventBase {
	 /* .source "PackageEventRecorder.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/PackageEventRecorder; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x408 */
/* name = "PackageEventBase" */
} // .end annotation
/* # instance fields */
private final java.lang.String id;
private final java.lang.String installer;
private final java.lang.String packageName;
private final userIds;
/* # direct methods */
static java.lang.String -$$Nest$fgetid ( com.android.server.pm.PackageEventRecorder$PackageEventBase p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.id;
} // .end method
static java.lang.String -$$Nest$fgetinstaller ( com.android.server.pm.PackageEventRecorder$PackageEventBase p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.installer;
} // .end method
static java.lang.String -$$Nest$fgetpackageName ( com.android.server.pm.PackageEventRecorder$PackageEventBase p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.packageName;
} // .end method
static -$$Nest$fgetuserIds ( com.android.server.pm.PackageEventRecorder$PackageEventBase p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.userIds;
} // .end method
static android.os.Bundle -$$Nest$mbuildBundle ( com.android.server.pm.PackageEventRecorder$PackageEventBase p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->buildBundle()Landroid/os/Bundle; */
} // .end method
static Integer -$$Nest$mgetEventType ( com.android.server.pm.PackageEventRecorder$PackageEventBase p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->getEventType()I */
} // .end method
static android.os.Bundle -$$Nest$smbuildBundleFromRecord ( java.lang.String p0, Integer p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.pm.PackageEventRecorder$PackageEventBase .buildBundleFromRecord ( p0,p1,p2 );
} // .end method
static java.lang.String -$$Nest$smresolveIdFromRecord ( java.lang.String p0 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.pm.PackageEventRecorder$PackageEventBase .resolveIdFromRecord ( p0 );
} // .end method
private com.android.server.pm.PackageEventRecorder$PackageEventBase ( ) {
/* .locals 0 */
/* .param p1, "id" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "userIds" # [I */
/* .param p4, "installerPackageName" # Ljava/lang/String; */
/* .line 652 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 653 */
this.id = p1;
/* .line 654 */
this.packageName = p2;
/* .line 655 */
this.userIds = p3;
/* .line 656 */
this.installer = p4;
/* .line 657 */
return;
} // .end method
 com.android.server.pm.PackageEventRecorder$PackageEventBase ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;-><init>(Ljava/lang/String;Ljava/lang/String;[ILjava/lang/String;)V */
return;
} // .end method
private android.os.Bundle buildBundle ( ) {
/* .locals 4 */
/* .line 784 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 785 */
/* .local v0, "result":Landroid/os/Bundle; */
final String v1 = "id"; // const-string v1, "id"
v2 = this.id;
(( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 786 */
v1 = this.id;
com.android.server.pm.PackageEventRecorder$PackageEventBase .resolveEventTimeMillis ( v1 );
/* move-result-wide v1 */
final String v3 = "eventTimeMillis"; // const-string v3, "eventTimeMillis"
(( android.os.Bundle ) v0 ).putLong ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 787 */
final String v1 = "packageName"; // const-string v1, "packageName"
v2 = this.packageName;
(( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 788 */
v1 = this.userIds;
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* array-length v2, v1 */
	 /* if-lez v2, :cond_0 */
	 /* .line 789 */
	 /* const-string/jumbo v2, "userIds" */
	 (( android.os.Bundle ) v0 ).putIntArray ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V
	 /* .line 791 */
} // :cond_0
v1 = this.installer;
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_1 */
/* .line 792 */
final String v1 = "installerPackageName"; // const-string v1, "installerPackageName"
v2 = this.installer;
(( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 794 */
} // :cond_1
} // .end method
private static android.os.Bundle buildBundleFromRecord ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 20 */
/* .param p0, "record" # Ljava/lang/String; */
/* .param p1, "lineNum" # I */
/* .param p2, "desireType" # I */
/* .line 715 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
final String v0 = "isRemovedFully"; // const-string v0, "isRemovedFully"
final String v3 = "installerPackageName"; // const-string v3, "installerPackageName"
/* const-string/jumbo v4, "userIds" */
final String v5 = "packageName"; // const-string v5, "packageName"
/* new-instance v6, Landroid/os/Bundle; */
/* invoke-direct {v6}, Landroid/os/Bundle;-><init>()V */
/* .line 718 */
/* .local v6, "result":Landroid/os/Bundle; */
int v7 = 0; // const/4 v7, 0x0
int v8 = 6; // const/4 v8, 0x6
try { // :try_start_0
/* invoke-static/range {p0 ..p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveIdFromRecord(Ljava/lang/String;)Ljava/lang/String; */
/* .line 719 */
/* .local v9, "id":Ljava/lang/String; */
v10 = android.text.TextUtils .isEmpty ( v9 );
if ( v10 != null) { // if-eqz v10, :cond_0
	 /* .line 720 */
	 final String v0 = "fail to resolve attr id"; // const-string v0, "fail to resolve attr id"
	 com.android.server.pm.PackageEventRecorder .-$$Nest$smprintLogWhileResolveTxt ( v8,v0,v2 );
	 /* .line 722 */
	 /* .line 724 */
} // :cond_0
final String v10 = "id"; // const-string v10, "id"
(( android.os.Bundle ) v6 ).putString ( v10, v9 ); // invoke-virtual {v6, v10, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 725 */
final String v10 = "eventTimeMillis"; // const-string v10, "eventTimeMillis"
com.android.server.pm.PackageEventRecorder$PackageEventBase .resolveEventTimeMillis ( v9 );
/* move-result-wide v11 */
(( android.os.Bundle ) v6 ).putLong ( v10, v11, v12 ); // invoke-virtual {v6, v10, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 726 */
v10 = com.android.server.pm.PackageEventRecorder$PackageEventBase .resolveEventType ( v9 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 727 */
/* .local v10, "actualType":I */
/* move/from16 v11, p2 */
/* if-eq v10, v11, :cond_1 */
/* .line 728 */
/* .line 731 */
} // :cond_1
try { // :try_start_1
/* new-instance v12, Landroid/util/ArrayMap; */
/* invoke-direct {v12}, Landroid/util/ArrayMap;-><init>()V */
/* .line 732 */
/* .local v12, "attrs":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/String;>;" */
/* const/16 v13, 0x20 */
v14 = (( java.lang.String ) v1 ).indexOf ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/String;->indexOf(I)I
int v15 = 1; // const/4 v15, 0x1
/* add-int/2addr v14, v15 */
(( java.lang.String ) v1 ).substring ( v14 ); // invoke-virtual {v1, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 733 */
java.lang.String .valueOf ( v13 );
(( java.lang.String ) v14 ).split ( v13 ); // invoke-virtual {v14, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 732 */
/* array-length v14, v13 */
/* const/16 v16, 0x0 */
/* move/from16 v7, v16 */
} // :goto_0
/* if-ge v7, v14, :cond_3 */
/* aget-object v17, v13, v7 */
/* move-object/from16 v18, v17 */
/* .line 734 */
/* .local v18, "attr":Ljava/lang/String; */
/* const/16 v17, 0x3d */
/* invoke-static/range {v17 ..v17}, Ljava/lang/String;->valueOf(C)Ljava/lang/String; */
/* move-object/from16 v8, v18 */
} // .end local v18 # "attr":Ljava/lang/String;
/* .local v8, "attr":Ljava/lang/String; */
(( java.lang.String ) v8 ).split ( v15 ); // invoke-virtual {v8, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 735 */
/* .local v15, "pv":[Ljava/lang/String; */
/* array-length v1, v15 */
/* move-object/from16 v18, v9 */
} // .end local v9 # "id":Ljava/lang/String;
/* .local v18, "id":Ljava/lang/String; */
int v9 = 2; // const/4 v9, 0x2
/* if-eq v1, v9, :cond_2 */
/* .line 736 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "bad attr format :"; // const-string v9, "bad attr format :"
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v9 = 6; // const/4 v9, 0x6
com.android.server.pm.PackageEventRecorder .-$$Nest$smprintLogWhileResolveTxt ( v9,v1,v2 );
/* .line 738 */
int v9 = 1; // const/4 v9, 0x1
/* .line 740 */
} // :cond_2
/* aget-object v1, v15, v16 */
/* move-object/from16 v19, v8 */
int v9 = 1; // const/4 v9, 0x1
} // .end local v8 # "attr":Ljava/lang/String;
/* .local v19, "attr":Ljava/lang/String; */
/* aget-object v8, v15, v9 */
(( android.util.ArrayMap ) v12 ).put ( v1, v8 ); // invoke-virtual {v12, v1, v8}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 732 */
} // .end local v15 # "pv":[Ljava/lang/String;
} // .end local v19 # "attr":Ljava/lang/String;
} // :goto_1
/* add-int/lit8 v7, v7, 0x1 */
int v8 = 6; // const/4 v8, 0x6
/* move-object/from16 v1, p0 */
/* move v15, v9 */
/* move-object/from16 v9, v18 */
/* .line 743 */
} // .end local v18 # "id":Ljava/lang/String;
/* .restart local v9 # "id":Ljava/lang/String; */
} // :cond_3
/* move-object/from16 v18, v9 */
} // .end local v9 # "id":Ljava/lang/String;
/* .restart local v18 # "id":Ljava/lang/String; */
(( android.util.ArrayMap ) v12 ).get ( v5 ); // invoke-virtual {v12, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/CharSequence; */
v1 = android.text.TextUtils .isEmpty ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 744 */
final String v0 = "attr packageName is missing or invalid"; // const-string v0, "attr packageName is missing or invalid"
int v1 = 6; // const/4 v1, 0x6
com.android.server.pm.PackageEventRecorder .-$$Nest$smprintLogWhileResolveTxt ( v1,v0,v2 );
/* .line 746 */
int v1 = 0; // const/4 v1, 0x0
/* .line 748 */
} // :cond_4
(( android.util.ArrayMap ) v12 ).get ( v5 ); // invoke-virtual {v12, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
(( android.os.Bundle ) v6 ).putString ( v5, v1 ); // invoke-virtual {v6, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 750 */
(( android.util.ArrayMap ) v12 ).get ( v4 ); // invoke-virtual {v12, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/CharSequence; */
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_7 */
/* .line 751 */
(( android.util.ArrayMap ) v12 ).get ( v4 ); // invoke-virtual {v12, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
/* .line 752 */
/* const/16 v5, 0x2c */
java.lang.String .valueOf ( v5 );
(( java.lang.String ) v1 ).split ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 751 */
java.util.Arrays .stream ( v1 );
/* new-instance v5, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase$$ExternalSyntheticLambda0; */
/* invoke-direct {v5}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase$$ExternalSyntheticLambda0;-><init>()V */
/* .line 753 */
/* .line 754 */
/* .local v1, "userIds":[I */
if ( v1 != null) { // if-eqz v1, :cond_6
/* array-length v5, v1 */
/* if-nez v5, :cond_5 */
/* .line 759 */
} // :cond_5
(( android.os.Bundle ) v6 ).putIntArray ( v4, v1 ); // invoke-virtual {v6, v4, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V
/* .line 755 */
} // :cond_6
} // :goto_2
final String v0 = "fail to resolve attr userIds"; // const-string v0, "fail to resolve attr userIds"
int v3 = 6; // const/4 v3, 0x6
com.android.server.pm.PackageEventRecorder .-$$Nest$smprintLogWhileResolveTxt ( v3,v0,v2 );
/* .line 757 */
int v3 = 0; // const/4 v3, 0x0
/* .line 762 */
} // .end local v1 # "userIds":[I
} // :cond_7
} // :goto_3
(( android.util.ArrayMap ) v12 ).get ( v3 ); // invoke-virtual {v12, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/CharSequence; */
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_8 */
/* .line 763 */
(( android.util.ArrayMap ) v12 ).get ( v3 ); // invoke-virtual {v12, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
(( android.os.Bundle ) v6 ).putString ( v3, v1 ); // invoke-virtual {v6, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 766 */
} // :cond_8
int v1 = 3; // const/4 v1, 0x3
/* if-ne v10, v1, :cond_a */
/* .line 767 */
v1 = (( android.util.ArrayMap ) v12 ).containsKey ( v0 ); // invoke-virtual {v12, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v1, :cond_9 */
/* .line 768 */
final String v0 = "missing attr isRemovedFully"; // const-string v0, "missing attr isRemovedFully"
int v1 = 6; // const/4 v1, 0x6
com.android.server.pm.PackageEventRecorder .-$$Nest$smprintLogWhileResolveTxt ( v1,v0,v2 );
/* .line 770 */
int v1 = 0; // const/4 v1, 0x0
/* .line 772 */
} // :cond_9
/* nop */
/* .line 773 */
(( android.util.ArrayMap ) v12 ).get ( v0 ); // invoke-virtual {v12, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
v1 = java.lang.Boolean .parseBoolean ( v1 );
/* .line 772 */
(( android.os.Bundle ) v6 ).putBoolean ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 778 */
} // .end local v10 # "actualType":I
} // .end local v12 # "attrs":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v18 # "id":Ljava/lang/String;
} // :cond_a
/* nop */
/* .line 780 */
/* .line 775 */
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v11, p2 */
/* .line 776 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_4
final String v1 = "fail to resolve record"; // const-string v1, "fail to resolve record"
int v3 = 6; // const/4 v3, 0x6
com.android.server.pm.PackageEventRecorder .-$$Nest$smprintLogWhileResolveTxt ( v3,v1,v2,v0 );
/* .line 777 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
static java.lang.String buildPackageEventId ( Long p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "eventTime" # J */
/* .param p2, "type" # I */
/* .line 694 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private Integer getEventType ( ) {
/* .locals 1 */
/* .line 690 */
v0 = this.id;
v0 = com.android.server.pm.PackageEventRecorder$PackageEventBase .resolveEventType ( v0 );
} // .end method
private Boolean isUserIdsValid ( ) {
/* .locals 2 */
/* .line 665 */
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->getEventType()I */
int v1 = 3; // const/4 v1, 0x3
/* if-eq v0, v1, :cond_0 */
v0 = this.userIds;
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v0, v0 */
/* if-gtz v0, :cond_1 */
/* .line 666 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->getEventType()I */
/* if-ne v0, v1, :cond_2 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 665 */
} // :goto_0
} // .end method
static Long resolveEventTimeMillis ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "id" # Ljava/lang/String; */
/* .line 698 */
v0 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
/* add-int/lit8 v0, v0, -0x1 */
int v1 = 0; // const/4 v1, 0x0
(( java.lang.String ) p0 ).substring ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
java.lang.Long .parseLong ( v0 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
static Integer resolveEventType ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "id" # Ljava/lang/String; */
/* .line 702 */
v0 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
/* add-int/lit8 v0, v0, -0x1 */
(( java.lang.String ) p0 ).substring ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;
v0 = java.lang.Integer .parseInt ( v0 );
} // .end method
private static java.lang.String resolveIdFromRecord ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p0, "record" # Ljava/lang/String; */
/* .line 706 */
v0 = android.text.TextUtils .isEmpty ( p0 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 707 */
/* .line 709 */
} // :cond_0
/* const/16 v0, 0x20 */
java.lang.String .valueOf ( v0 );
v2 = (( java.lang.String ) p0 ).contains ( v2 ); // invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 710 */
int v1 = 0; // const/4 v1, 0x0
v0 = (( java.lang.String ) p0 ).indexOf ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I
(( java.lang.String ) p0 ).substring ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
} // :cond_1
/* nop */
/* .line 709 */
} // :goto_0
} // .end method
/* # virtual methods */
void WriteToTxt ( java.io.BufferedWriter p0 ) {
/* .locals 4 */
/* .param p1, "bufferedWriter" # Ljava/io/BufferedWriter; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 670 */
v0 = this.id;
(( java.io.BufferedWriter ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 671 */
/* const/16 v0, 0x20 */
(( java.io.BufferedWriter ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V
/* .line 672 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "packageName="; // const-string v2, "packageName="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.BufferedWriter ) p1 ).write ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 673 */
(( java.io.BufferedWriter ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V
/* .line 674 */
v1 = this.userIds;
if ( v1 != null) { // if-eqz v1, :cond_1
/* array-length v1, v1 */
/* if-lez v1, :cond_1 */
/* .line 675 */
/* const-string/jumbo v1, "userIds=" */
(( java.io.BufferedWriter ) p1 ).write ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 676 */
v1 = this.userIds;
int v2 = 0; // const/4 v2, 0x0
/* aget v1, v1, v2 */
java.lang.String .valueOf ( v1 );
(( java.io.BufferedWriter ) p1 ).write ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 677 */
int v1 = 1; // const/4 v1, 0x1
/* .local v1, "i":I */
} // :goto_0
v2 = this.userIds;
/* array-length v2, v2 */
/* if-ge v1, v2, :cond_0 */
/* .line 678 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const/16 v3, 0x2c */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
v3 = this.userIds;
/* aget v3, v3, v1 */
java.lang.String .valueOf ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.BufferedWriter ) p1 ).write ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 677 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 680 */
} // .end local v1 # "i":I
} // :cond_0
(( java.io.BufferedWriter ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V
/* .line 682 */
} // :cond_1
v1 = this.installer;
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_2 */
/* .line 683 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "installerPackageName="; // const-string v2, "installerPackageName="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.installer;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.BufferedWriter ) p1 ).write ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 685 */
(( java.io.BufferedWriter ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V
/* .line 687 */
} // :cond_2
return;
} // .end method
public java.lang.String getId ( ) {
/* .locals 1 */
/* .line 809 */
v0 = this.id;
} // .end method
public Boolean isValid ( ) {
/* .locals 1 */
/* .line 660 */
v0 = this.id;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_0 */
v0 = this.packageName;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v0, :cond_0 */
/* .line 661 */
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->isUserIdsValid()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 660 */
} // :goto_0
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 799 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "PackageEvent{id=\'"; // const-string v1, "PackageEvent{id=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.id;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", type=\'"; // const-string v2, ", type=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.id;
/* .line 801 */
v2 = com.android.server.pm.PackageEventRecorder$PackageEventBase .resolveEventType ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", packageName=\'"; // const-string v2, ", packageName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", userIds="; // const-string v2, ", userIds="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.userIds;
/* .line 803 */
java.util.Arrays .toString ( v2 );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", installerPackageName=\'"; // const-string v2, ", installerPackageName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.installer;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 799 */
} // .end method
