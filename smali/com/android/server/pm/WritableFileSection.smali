.class Lcom/android/server/pm/WritableFileSection;
.super Ljava/lang/Object;
.source "ProfileTranscoder.java"


# instance fields
.field final mContents:[B

.field final mExpectedInflateSize:I

.field final mNeedsCompression:Z

.field final mType:Lcom/android/server/pm/FileSectionType;


# direct methods
.method constructor <init>(Lcom/android/server/pm/FileSectionType;I[BZ)V
    .locals 0
    .param p1, "type"    # Lcom/android/server/pm/FileSectionType;
    .param p2, "expectedInflateSize"    # I
    .param p3, "contents"    # [B
    .param p4, "needsCompression"    # Z

    .line 1080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1081
    iput-object p1, p0, Lcom/android/server/pm/WritableFileSection;->mType:Lcom/android/server/pm/FileSectionType;

    .line 1082
    iput p2, p0, Lcom/android/server/pm/WritableFileSection;->mExpectedInflateSize:I

    .line 1083
    iput-object p3, p0, Lcom/android/server/pm/WritableFileSection;->mContents:[B

    .line 1084
    iput-boolean p4, p0, Lcom/android/server/pm/WritableFileSection;->mNeedsCompression:Z

    .line 1085
    return-void
.end method
