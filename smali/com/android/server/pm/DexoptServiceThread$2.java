class com.android.server.pm.DexoptServiceThread$2 implements java.lang.Runnable {
	 /* .source "DexoptServiceThread.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/pm/DexoptServiceThread;->performDexOptSecondary(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.pm.DexoptServiceThread this$0; //synthetic
final com.android.server.pm.dex.PackageDexUsage$DexUseInfo val$dexUseInfo; //synthetic
final android.content.pm.ApplicationInfo val$info; //synthetic
final com.android.server.pm.dex.DexoptOptions val$options; //synthetic
final java.lang.String val$path; //synthetic
/* # direct methods */
 com.android.server.pm.DexoptServiceThread$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/DexoptServiceThread; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 222 */
this.this$0 = p1;
this.val$path = p2;
this.val$info = p3;
this.val$dexUseInfo = p4;
this.val$options = p5;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 9 */
/* .line 225 */
v0 = this.this$0;
/* iget v1, v0, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, v0, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I */
/* .line 226 */
v0 = this.this$0;
v0 = this.mDexoptSecondaryPath;
v1 = this.val$path;
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).containsValue ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 227 */
v0 = this.this$0;
v0 = this.mDexoptSecondaryPath;
v1 = this.this$0;
/* iget v1, v1, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I */
java.lang.Integer .valueOf ( v1 );
v2 = this.val$path;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 229 */
} // :cond_0
v0 = this.this$0;
v0 = this.mDexoptSecondaryPath;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 230 */
/* .local v1, "getSecondaryId":I */
v2 = this.this$0;
v2 = this.mDexoptSecondaryPath;
java.lang.Integer .valueOf ( v1 );
(( java.util.concurrent.ConcurrentHashMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
v3 = this.val$path;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 231 */
v2 = this.this$0;
/* iput v1, v2, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I */
/* .line 233 */
} // .end local v1 # "getSecondaryId":I
} // :cond_1
/* .line 235 */
} // :cond_2
} // :goto_1
v0 = this.this$0;
v4 = this.val$info;
v5 = this.val$path;
/* iget v6, v0, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I */
v7 = this.val$dexUseInfo;
v8 = this.val$options;
/* move-object v3, v0 */
v1 = /* invoke-virtual/range {v3 ..v8}, Lcom/android/server/pm/DexoptServiceThread;->dexOptSecondaryDexPath(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;ILcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)I */
com.android.server.pm.DexoptServiceThread .-$$Nest$fputmDexoptSecondaryResult ( v0,v1 );
/* .line 237 */
v0 = this.this$0;
v0 = com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptSecondaryResult ( v0 );
v1 = this.this$0;
com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmPdo ( v1 );
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_3 */
v0 = this.this$0;
v0 = com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptSecondaryResult ( v0 );
v1 = this.this$0;
com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmPdo ( v1 );
/* if-nez v0, :cond_4 */
/* .line 239 */
} // :cond_3
com.android.server.pm.DexoptServiceThread .-$$Nest$sfgetmWaitLock ( );
/* monitor-enter v0 */
/* .line 240 */
try { // :try_start_0
v1 = this.this$0;
v1 = this.mDexoptSecondaryPath;
v2 = this.this$0;
/* iget v2, v2, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I */
java.lang.Integer .valueOf ( v2 );
(( java.util.concurrent.ConcurrentHashMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
/* .line 241 */
/* .local v1, "path":Ljava/lang/String; */
com.android.server.pm.DexoptServiceThread .-$$Nest$sfgetmWaitLock ( );
(( java.lang.Object ) v2 ).notifyAll ( ); // invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V
/* .line 242 */
} // .end local v1 # "path":Ljava/lang/String;
/* monitor-exit v0 */
/* .line 244 */
} // :cond_4
return;
/* .line 242 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
