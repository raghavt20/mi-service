class com.android.server.pm.Encoding {
	 /* .source "ProfileTranscoder.java" */
	 /* # static fields */
	 static final Integer SIZEOF_BYTE;
	 static final Integer UINT_16_SIZE;
	 static final Integer UINT_32_SIZE;
	 static final Integer UINT_8_SIZE;
	 /* # direct methods */
	 com.android.server.pm.Encoding ( ) {
		 /* .locals 0 */
		 /* .line 882 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 Integer bitsToBytes ( Integer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "numberOfBits" # I */
		 /* .line 908 */
		 /* add-int/lit8 v0, p1, 0x8 */
		 /* add-int/lit8 v0, v0, -0x1 */
		 /* and-int/lit8 v0, v0, -0x8 */
		 /* div-int/lit8 v0, v0, 0x8 */
	 } // .end method
	 compress ( Object[] p0 ) {
		 /* .locals 5 */
		 /* .param p1, "data" # [B */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/lang/Exception; */
		 /* } */
	 } // .end annotation
	 /* .line 995 */
	 /* new-instance v0, Ljava/util/zip/Deflater; */
	 int v1 = 1; // const/4 v1, 0x1
	 /* invoke-direct {v0, v1}, Ljava/util/zip/Deflater;-><init>(I)V */
	 /* .line 996 */
	 /* .local v0, "compressor":Ljava/util/zip/Deflater; */
	 /* new-instance v1, Ljava/io/ByteArrayOutputStream; */
	 /* invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V */
	 /* .line 997 */
	 /* .local v1, "out":Ljava/io/ByteArrayOutputStream; */
	 try { // :try_start_0
		 /* new-instance v2, Ljava/util/zip/DeflaterOutputStream; */
		 /* invoke-direct {v2, v1, v0}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
		 /* .line 998 */
		 /* .local v2, "deflater":Ljava/util/zip/DeflaterOutputStream; */
		 try { // :try_start_1
			 (( java.util.zip.DeflaterOutputStream ) v2 ).write ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/zip/DeflaterOutputStream;->write([B)V
			 /* :try_end_1 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 999 */
			 try { // :try_start_2
				 (( java.util.zip.DeflaterOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/util/zip/DeflaterOutputStream;->close()V
				 /* :try_end_2 */
				 /* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
				 /* .line 1000 */
			 } // .end local v2 # "deflater":Ljava/util/zip/DeflaterOutputStream;
			 (( java.util.zip.Deflater ) v0 ).end ( ); // invoke-virtual {v0}, Ljava/util/zip/Deflater;->end()V
			 /* .line 1001 */
			 /* nop */
			 /* .line 1002 */
			 (( java.io.ByteArrayOutputStream ) v1 ).toByteArray ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
			 /* .line 997 */
			 /* .restart local v2 # "deflater":Ljava/util/zip/DeflaterOutputStream; */
			 /* :catchall_0 */
			 /* move-exception v3 */
			 try { // :try_start_3
				 (( java.util.zip.DeflaterOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/util/zip/DeflaterOutputStream;->close()V
				 /* :try_end_3 */
				 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
				 /* :catchall_1 */
				 /* move-exception v4 */
				 try { // :try_start_4
					 (( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
				 } // .end local v0 # "compressor":Ljava/util/zip/Deflater;
			 } // .end local v1 # "out":Ljava/io/ByteArrayOutputStream;
		 } // .end local p0 # "this":Lcom/android/server/pm/Encoding;
	 } // .end local p1 # "data":[B
} // :goto_0
/* throw v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* .line 1000 */
} // .end local v2 # "deflater":Ljava/util/zip/DeflaterOutputStream;
/* .restart local v0 # "compressor":Ljava/util/zip/Deflater; */
/* .restart local v1 # "out":Ljava/io/ByteArrayOutputStream; */
/* .restart local p0 # "this":Lcom/android/server/pm/Encoding; */
/* .restart local p1 # "data":[B */
/* :catchall_2 */
/* move-exception v2 */
(( java.util.zip.Deflater ) v0 ).end ( ); // invoke-virtual {v0}, Ljava/util/zip/Deflater;->end()V
/* .line 1001 */
/* throw v2 */
} // .end method
java.lang.Exception error ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "message" # Ljava/lang/String; */
/* .line 1012 */
/* new-instance v0, Ljava/lang/Exception; */
/* invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V */
} // .end method
 read ( java.io.InputStream p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "length" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 911 */
/* new-array v0, p2, [B */
/* .line 912 */
/* .local v0, "buffer":[B */
int v1 = 0; // const/4 v1, 0x0
/* .line 913 */
/* .local v1, "offset":I */
} // :goto_0
/* if-ge v1, p2, :cond_1 */
/* .line 914 */
/* sub-int v2, p2, v1 */
v2 = (( java.io.InputStream ) p1 ).read ( v0, v1, v2 ); // invoke-virtual {p1, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I
/* .line 915 */
/* .local v2, "result":I */
/* if-ltz v2, :cond_0 */
/* .line 918 */
/* add-int/2addr v1, v2 */
/* .line 919 */
} // .end local v2 # "result":I
/* .line 916 */
/* .restart local v2 # "result":I */
} // :cond_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Not enough bytes to read: "; // const-string v4, "Not enough bytes to read: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.pm.Encoding ) p0 ).error ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v3 */
/* .line 920 */
} // .end local v2 # "result":I
} // :cond_1
} // .end method
 readCompressed ( java.io.InputStream p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "compressedDataSize" # I */
/* .param p3, "uncompressedDataSize" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 949 */
/* new-instance v0, Ljava/util/zip/Inflater; */
/* invoke-direct {v0}, Ljava/util/zip/Inflater;-><init>()V */
/* .line 951 */
/* .local v0, "inf":Ljava/util/zip/Inflater; */
try { // :try_start_0
/* new-array v1, p3, [B */
/* .line 952 */
/* .local v1, "result":[B */
int v2 = 0; // const/4 v2, 0x0
/* .line 953 */
/* .local v2, "totalBytesRead":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 954 */
/* .local v3, "totalBytesInflated":I */
/* const/16 v4, 0x800 */
/* new-array v4, v4, [B */
/* .line 955 */
/* .local v4, "input":[B */
} // :goto_0
/* nop */
/* .line 956 */
v5 = (( java.util.zip.Inflater ) v0 ).finished ( ); // invoke-virtual {v0}, Ljava/util/zip/Inflater;->finished()Z
/* if-nez v5, :cond_1 */
/* .line 957 */
v5 = (( java.util.zip.Inflater ) v0 ).needsDictionary ( ); // invoke-virtual {v0}, Ljava/util/zip/Inflater;->needsDictionary()Z
/* if-nez v5, :cond_1 */
/* if-ge v2, p2, :cond_1 */
/* .line 960 */
v5 = (( java.io.InputStream ) p1 ).read ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/InputStream;->read([B)I
/* .line 961 */
/* .local v5, "bytesRead":I */
/* if-ltz v5, :cond_0 */
/* .line 967 */
int v6 = 0; // const/4 v6, 0x0
(( java.util.zip.Inflater ) v0 ).setInput ( v4, v6, v5 ); // invoke-virtual {v0, v4, v6, v5}, Ljava/util/zip/Inflater;->setInput([BII)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 969 */
/* sub-int v6, p3, v3 */
try { // :try_start_1
v6 = (( java.util.zip.Inflater ) v0 ).inflate ( v1, v3, v6 ); // invoke-virtual {v0, v1, v3, v6}, Ljava/util/zip/Inflater;->inflate([BII)I
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* add-int/2addr v3, v6 */
/* .line 976 */
/* nop */
/* .line 977 */
/* add-int/2addr v2, v5 */
/* .line 978 */
} // .end local v5 # "bytesRead":I
/* .line 974 */
/* .restart local v5 # "bytesRead":I */
/* :catch_0 */
/* move-exception v6 */
/* .line 975 */
/* .local v6, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v6 ).getMessage ( ); // invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( com.android.server.pm.Encoding ) p0 ).error ( v7 ); // invoke-virtual {p0, v7}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;
} // .end local v0 # "inf":Ljava/util/zip/Inflater;
} // .end local p0 # "this":Lcom/android/server/pm/Encoding;
} // .end local p1 # "is":Ljava/io/InputStream;
} // .end local p2 # "compressedDataSize":I
} // .end local p3 # "uncompressedDataSize":I
/* throw v7 */
/* .line 962 */
} // .end local v6 # "e":Ljava/lang/Exception;
/* .restart local v0 # "inf":Ljava/util/zip/Inflater; */
/* .restart local p0 # "this":Lcom/android/server/pm/Encoding; */
/* .restart local p1 # "is":Ljava/io/InputStream; */
/* .restart local p2 # "compressedDataSize":I */
/* .restart local p3 # "uncompressedDataSize":I */
} // :cond_0
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Invalid zip data.Stream ended after $totalBytesRead bytes.Expected "; // const-string v7, "Invalid zip data.Stream ended after $totalBytesRead bytes.Expected "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " bytes"; // const-string v7, " bytes"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.pm.Encoding ) p0 ).error ( v6 ); // invoke-virtual {p0, v6}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;
} // .end local v0 # "inf":Ljava/util/zip/Inflater;
} // .end local p0 # "this":Lcom/android/server/pm/Encoding;
} // .end local p1 # "is":Ljava/io/InputStream;
} // .end local p2 # "compressedDataSize":I
} // .end local p3 # "uncompressedDataSize":I
/* throw v6 */
/* .line 979 */
} // .end local v5 # "bytesRead":I
/* .restart local v0 # "inf":Ljava/util/zip/Inflater; */
/* .restart local p0 # "this":Lcom/android/server/pm/Encoding; */
/* .restart local p1 # "is":Ljava/io/InputStream; */
/* .restart local p2 # "compressedDataSize":I */
/* .restart local p3 # "uncompressedDataSize":I */
} // :cond_1
/* if-ne v2, p2, :cond_3 */
/* .line 986 */
v5 = (( java.util.zip.Inflater ) v0 ).finished ( ); // invoke-virtual {v0}, Ljava/util/zip/Inflater;->finished()Z
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 989 */
/* nop */
/* .line 991 */
(( java.util.zip.Inflater ) v0 ).end ( ); // invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V
/* .line 989 */
/* .line 987 */
} // :cond_2
try { // :try_start_3
final String v5 = "Inflater did not finish"; // const-string v5, "Inflater did not finish"
(( com.android.server.pm.Encoding ) p0 ).error ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;
} // .end local v0 # "inf":Ljava/util/zip/Inflater;
} // .end local p0 # "this":Lcom/android/server/pm/Encoding;
} // .end local p1 # "is":Ljava/io/InputStream;
} // .end local p2 # "compressedDataSize":I
} // .end local p3 # "uncompressedDataSize":I
/* throw v5 */
/* .line 980 */
/* .restart local v0 # "inf":Ljava/util/zip/Inflater; */
/* .restart local p0 # "this":Lcom/android/server/pm/Encoding; */
/* .restart local p1 # "is":Ljava/io/InputStream; */
/* .restart local p2 # "compressedDataSize":I */
/* .restart local p3 # "uncompressedDataSize":I */
} // :cond_3
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Didn\'t read enough bytes during decompression.expected="; // const-string v6, "Didn\'t read enough bytes during decompression.expected="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " actual="; // const-string v6, " actual="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.pm.Encoding ) p0 ).error ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/pm/Encoding;->error(Ljava/lang/String;)Ljava/lang/Exception;
} // .end local v0 # "inf":Ljava/util/zip/Inflater;
} // .end local p0 # "this":Lcom/android/server/pm/Encoding;
} // .end local p1 # "is":Ljava/io/InputStream;
} // .end local p2 # "compressedDataSize":I
} // .end local p3 # "uncompressedDataSize":I
/* throw v5 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 991 */
} // .end local v1 # "result":[B
} // .end local v2 # "totalBytesRead":I
} // .end local v3 # "totalBytesInflated":I
} // .end local v4 # "input":[B
/* .restart local v0 # "inf":Ljava/util/zip/Inflater; */
/* .restart local p0 # "this":Lcom/android/server/pm/Encoding; */
/* .restart local p1 # "is":Ljava/io/InputStream; */
/* .restart local p2 # "compressedDataSize":I */
/* .restart local p3 # "uncompressedDataSize":I */
/* :catchall_0 */
/* move-exception v1 */
(( java.util.zip.Inflater ) v0 ).end ( ); // invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V
/* .line 992 */
/* throw v1 */
} // .end method
java.lang.String readString ( java.io.InputStream p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "size" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 941 */
/* new-instance v0, Ljava/lang/String; */
(( com.android.server.pm.Encoding ) p0 ).read ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B
v2 = java.nio.charset.StandardCharsets.UTF_8;
/* invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V */
} // .end method
Long readUInt ( java.io.InputStream p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "numberOfBytes" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 923 */
(( com.android.server.pm.Encoding ) p0 ).read ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B
/* .line 924 */
/* .local v0, "buffer":[B */
/* const-wide/16 v1, 0x0 */
/* .line 925 */
/* .local v1, "value":J */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, p2, :cond_0 */
/* .line 926 */
/* aget-byte v4, v0, v3 */
/* and-int/lit16 v4, v4, 0xff */
/* int-to-long v4, v4 */
/* .line 927 */
/* .local v4, "next":J */
/* mul-int/lit8 v6, v3, 0x8 */
/* shl-long v6, v4, v6 */
/* add-long/2addr v1, v6 */
/* .line 925 */
} // .end local v4 # "next":J
/* add-int/lit8 v3, v3, 0x1 */
/* .line 929 */
} // .end local v3 # "i":I
} // :cond_0
/* return-wide v1 */
} // .end method
Integer readUInt16 ( java.io.InputStream p0 ) {
/* .locals 2 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 935 */
int v0 = 2; // const/4 v0, 0x2
(( com.android.server.pm.Encoding ) p0 ).readUInt ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/pm/Encoding;->readUInt(Ljava/io/InputStream;I)J
/* move-result-wide v0 */
/* long-to-int v0, v0 */
} // .end method
Long readUInt32 ( java.io.InputStream p0 ) {
/* .locals 2 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 938 */
int v0 = 4; // const/4 v0, 0x4
(( com.android.server.pm.Encoding ) p0 ).readUInt ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/pm/Encoding;->readUInt(Ljava/io/InputStream;I)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
Integer readUInt8 ( java.io.InputStream p0 ) {
/* .locals 2 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 932 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.pm.Encoding ) p0 ).readUInt ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/pm/Encoding;->readUInt(Ljava/io/InputStream;I)J
/* move-result-wide v0 */
/* long-to-int v0, v0 */
} // .end method
Integer utf8Length ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "s" # Ljava/lang/String; */
/* .line 888 */
v0 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) p1 ).getBytes ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
/* array-length v0, v0 */
} // .end method
void writeAll ( java.io.InputStream p0, java.io.OutputStream p1 ) {
/* .locals 3 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "os" # Ljava/io/OutputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 1005 */
/* const/16 v0, 0x200 */
/* new-array v0, v0, [B */
/* .line 1007 */
/* .local v0, "buf":[B */
} // :goto_0
v1 = (( java.io.InputStream ) p1 ).read ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I
/* move v2, v1 */
/* .local v2, "length":I */
/* if-lez v1, :cond_0 */
/* .line 1008 */
int v1 = 0; // const/4 v1, 0x0
(( java.io.OutputStream ) p2 ).write ( v0, v1, v2 ); // invoke-virtual {p2, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V
/* .line 1010 */
} // :cond_0
return;
} // .end method
void writeString ( java.io.OutputStream p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "s" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 905 */
v0 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) p2 ).getBytes ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
(( java.io.OutputStream ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V
/* .line 906 */
return;
} // .end method
void writeUInt ( java.io.OutputStream p0, Long p1, Integer p2 ) {
/* .locals 6 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "value" # J */
/* .param p4, "numberOfBytes" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 892 */
/* new-array v0, p4, [B */
/* .line 893 */
/* .local v0, "buffer":[B */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, p4, :cond_0 */
/* .line 894 */
/* mul-int/lit8 v2, v1, 0x8 */
/* shr-long v2, p2, v2 */
/* const-wide/16 v4, 0xff */
/* and-long/2addr v2, v4 */
/* long-to-int v2, v2 */
/* int-to-byte v2, v2 */
/* aput-byte v2, v0, v1 */
/* .line 893 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 896 */
} // .end local v1 # "i":I
} // :cond_0
(( java.io.OutputStream ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V
/* .line 897 */
return;
} // .end method
void writeUInt16 ( java.io.OutputStream p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "value" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 899 */
/* int-to-long v0, p2 */
int v2 = 2; // const/4 v2, 0x2
(( com.android.server.pm.Encoding ) p0 ).writeUInt ( p1, v0, v1, v2 ); // invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/server/pm/Encoding;->writeUInt(Ljava/io/OutputStream;JI)V
/* .line 900 */
return;
} // .end method
void writeUInt32 ( java.io.OutputStream p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "value" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 902 */
int v0 = 4; // const/4 v0, 0x4
(( com.android.server.pm.Encoding ) p0 ).writeUInt ( p1, p2, p3, v0 ); // invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/server/pm/Encoding;->writeUInt(Ljava/io/OutputStream;JI)V
/* .line 903 */
return;
} // .end method
