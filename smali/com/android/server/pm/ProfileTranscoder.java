public class com.android.server.pm.ProfileTranscoder {
	 /* .source "ProfileTranscoder.java" */
	 /* # static fields */
	 private static final Integer HOT;
	 private static final Integer INLINE_CACHE_MEGAMORPHIC_ENCODING;
	 private static final Integer INLINE_CACHE_MISSING_TYPES_ENCODING;
	 static final MAGIC_PROF;
	 static final MAGIC_PROFM;
	 static final METADATA_V001_N;
	 static final METADATA_V002;
	 static final Integer MIN_SUPPORTED_SDK;
	 private static final Integer POST_STARTUP;
	 private static final java.lang.String PROFILE_META_LOCATION;
	 private static final java.lang.String PROFILE_SOURCE_LOCATION;
	 private static final Integer STARTUP;
	 private static final java.lang.String TAG;
	 static final V010_P;
	 static final V015_S;
	 /* # instance fields */
	 private final java.lang.String mApkName;
	 private final android.content.res.AssetManager mAssetManager;
	 private final java.lang.String mBasePath;
	 private final mDesiredVersion;
	 private Boolean mDeviceSupportTranscode;
	 private com.android.server.pm.Encoding mEncoding;
	 private final java.lang.String mPackageName;
	 private com.android.server.pm.DexProfileData mProfile;
	 private final java.io.File mTarget;
	 private mTranscodedProfile;
	 /* # direct methods */
	 static com.android.server.pm.ProfileTranscoder ( ) {
		 /* .locals 2 */
		 /* .line 35 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* new-array v1, v0, [B */
		 /* fill-array-data v1, :array_0 */
		 /* .line 36 */
		 /* new-array v1, v0, [B */
		 /* fill-array-data v1, :array_1 */
		 /* .line 37 */
		 /* new-array v1, v0, [B */
		 /* fill-array-data v1, :array_2 */
		 /* .line 38 */
		 /* new-array v1, v0, [B */
		 /* fill-array-data v1, :array_3 */
		 /* .line 39 */
		 /* new-array v1, v0, [B */
		 /* fill-array-data v1, :array_4 */
		 /* .line 40 */
		 /* new-array v0, v0, [B */
		 /* fill-array-data v0, :array_5 */
		 return;
		 /* :array_0 */
		 /* .array-data 1 */
		 /* 0x70t */
		 /* 0x72t */
		 /* 0x6ft */
		 /* 0x0t */
	 } // .end array-data
	 /* :array_1 */
	 /* .array-data 1 */
	 /* 0x70t */
	 /* 0x72t */
	 /* 0x6dt */
	 /* 0x0t */
} // .end array-data
/* :array_2 */
/* .array-data 1 */
/* 0x30t */
/* 0x31t */
/* 0x35t */
/* 0x0t */
} // .end array-data
/* :array_3 */
/* .array-data 1 */
/* 0x30t */
/* 0x31t */
/* 0x30t */
/* 0x0t */
} // .end array-data
/* :array_4 */
/* .array-data 1 */
/* 0x30t */
/* 0x30t */
/* 0x31t */
/* 0x0t */
} // .end array-data
/* :array_5 */
/* .array-data 1 */
/* 0x30t */
/* 0x30t */
/* 0x32t */
/* 0x0t */
} // .end array-data
} // .end method
public com.android.server.pm.ProfileTranscoder ( ) {
/* .locals 1 */
/* .param p1, "target" # Ljava/io/File; */
/* .param p2, "apkName" # Ljava/lang/String; */
/* .param p3, "basePath" # Ljava/lang/String; */
/* .param p4, "packageName" # Ljava/lang/String; */
/* .param p5, "assetManager" # Landroid/content/res/AssetManager; */
/* .line 66 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 56 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mDeviceSupportTranscode:Z */
/* .line 57 */
int v0 = 0; // const/4 v0, 0x0
this.mTranscodedProfile = v0;
/* .line 58 */
this.mProfile = v0;
/* .line 67 */
this.mTarget = p1;
/* .line 68 */
this.mApkName = p2;
/* .line 69 */
this.mBasePath = p3;
/* .line 70 */
this.mPackageName = p4;
/* .line 71 */
this.mAssetManager = p5;
/* .line 72 */
/* invoke-direct {p0}, Lcom/android/server/pm/ProfileTranscoder;->desiredVersion()[B */
this.mDesiredVersion = v0;
/* .line 73 */
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/ProfileTranscoder;->isSupportTranscode()Z */
/* iput-boolean v0, p0, Lcom/android/server/pm/ProfileTranscoder;->mDeviceSupportTranscode:Z */
/* .line 74 */
/* new-instance v0, Lcom/android/server/pm/Encoding; */
/* invoke-direct {v0}, Lcom/android/server/pm/Encoding;-><init>()V */
this.mEncoding = v0;
/* .line 75 */
return;
} // .end method
private Integer computeMethodFlags ( com.android.server.pm.DexProfileData p0 ) {
/* .locals 4 */
/* .param p1, "profileData" # Lcom/android/server/pm/DexProfileData; */
/* .line 407 */
int v0 = 0; // const/4 v0, 0x0
/* .line 408 */
/* .local v0, "methodFlags":I */
v1 = this.methods;
(( java.util.TreeMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 409 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 410 */
/* .local v3, "flagValue":I */
/* or-int/2addr v0, v3 */
/* .line 411 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
} // .end local v3 # "flagValue":I
/* .line 412 */
} // :cond_0
} // .end method
private com.android.server.pm.WritableFileSection createCompressibleClassSection ( com.android.server.pm.DexProfileData[] p0 ) {
/* .locals 6 */
/* .param p1, "profileData" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 315 */
int v0 = 0; // const/4 v0, 0x0
/* .line 316 */
/* .local v0, "expectedSize":I */
/* new-instance v1, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 317 */
/* .local v1, "out":Ljava/io/ByteArrayOutputStream; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
/* array-length v3, p1 */
/* if-ge v2, v3, :cond_0 */
/* .line 318 */
/* aget-object v3, p1, v2 */
/* .line 320 */
/* .local v3, "profile":Lcom/android/server/pm/DexProfileData; */
/* add-int/lit8 v0, v0, 0x2 */
/* .line 321 */
v4 = this.mEncoding;
(( com.android.server.pm.Encoding ) v4 ).writeUInt16 ( v1, v2 ); // invoke-virtual {v4, v1, v2}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 323 */
/* add-int/lit8 v0, v0, 0x2 */
/* .line 324 */
v4 = this.mEncoding;
/* iget v5, v3, Lcom/android/server/pm/DexProfileData;->classSetSize:I */
(( com.android.server.pm.Encoding ) v4 ).writeUInt16 ( v1, v5 ); // invoke-virtual {v4, v1, v5}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 326 */
/* iget v4, v3, Lcom/android/server/pm/DexProfileData;->classSetSize:I */
/* mul-int/lit8 v4, v4, 0x2 */
/* add-int/2addr v0, v4 */
/* .line 327 */
/* invoke-direct {p0, v1, v3}, Lcom/android/server/pm/ProfileTranscoder;->writeClasses(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V */
/* .line 317 */
} // .end local v3 # "profile":Lcom/android/server/pm/DexProfileData;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 329 */
} // .end local v2 # "i":I
} // :cond_0
(( java.io.ByteArrayOutputStream ) v1 ).toByteArray ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* .line 330 */
/* .local v2, "contents":[B */
/* array-length v3, v2 */
/* if-ne v0, v3, :cond_1 */
/* .line 336 */
/* new-instance v3, Lcom/android/server/pm/WritableFileSection; */
v4 = com.android.server.pm.FileSectionType.CLASSES;
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {v3, v4, v0, v2, v5}, Lcom/android/server/pm/WritableFileSection;-><init>(Lcom/android/server/pm/FileSectionType;I[BZ)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 342 */
(( java.io.ByteArrayOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
/* .line 336 */
/* .line 331 */
} // :cond_1
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Expected size "; // const-string v4, "Expected size "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", does not match actual size "; // const-string v4, ", does not match actual size "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v4, v2 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
} // .end local v0 # "expectedSize":I
} // .end local v1 # "out":Ljava/io/ByteArrayOutputStream;
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // .end local p1 # "profileData":[Lcom/android/server/pm/DexProfileData;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 316 */
} // .end local v2 # "contents":[B
/* .restart local v0 # "expectedSize":I */
/* .restart local v1 # "out":Ljava/io/ByteArrayOutputStream; */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* .restart local p1 # "profileData":[Lcom/android/server/pm/DexProfileData; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_2
(( java.io.ByteArrayOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :goto_1
/* throw v2 */
} // .end method
private com.android.server.pm.WritableFileSection createCompressibleMethodsSection ( com.android.server.pm.DexProfileData[] p0 ) {
/* .locals 11 */
/* .param p1, "profileData" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 348 */
int v0 = 0; // const/4 v0, 0x0
/* .line 349 */
/* .local v0, "expectedSize":I */
/* new-instance v1, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 350 */
/* .local v1, "out":Ljava/io/ByteArrayOutputStream; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
/* array-length v3, p1 */
/* if-ge v2, v3, :cond_0 */
/* .line 351 */
/* aget-object v3, p1, v2 */
/* .line 353 */
/* .local v3, "profile":Lcom/android/server/pm/DexProfileData; */
v4 = /* invoke-direct {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->computeMethodFlags(Lcom/android/server/pm/DexProfileData;)I */
/* .line 355 */
/* .local v4, "methodFlags":I */
/* invoke-direct {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->createMethodBitmapRegion(Lcom/android/server/pm/DexProfileData;)[B */
/* .line 357 */
/* .local v5, "bitmapContents":[B */
/* invoke-direct {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->createMethodsWithInlineCaches(Lcom/android/server/pm/DexProfileData;)[B */
/* .line 359 */
/* .local v6, "methodRegionContents":[B */
/* add-int/lit8 v0, v0, 0x2 */
/* .line 360 */
v7 = this.mEncoding;
(( com.android.server.pm.Encoding ) v7 ).writeUInt16 ( v1, v2 ); // invoke-virtual {v7, v1, v2}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 362 */
/* array-length v7, v5 */
/* add-int/lit8 v7, v7, 0x2 */
/* array-length v8, v6 */
/* add-int/2addr v7, v8 */
/* .line 364 */
/* .local v7, "followingDataSize":I */
/* add-int/lit8 v0, v0, 0x4 */
/* .line 365 */
v8 = this.mEncoding;
/* int-to-long v9, v7 */
(( com.android.server.pm.Encoding ) v8 ).writeUInt32 ( v1, v9, v10 ); // invoke-virtual {v8, v1, v9, v10}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 367 */
v8 = this.mEncoding;
(( com.android.server.pm.Encoding ) v8 ).writeUInt16 ( v1, v4 ); // invoke-virtual {v8, v1, v4}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 368 */
(( java.io.ByteArrayOutputStream ) v1 ).write ( v5 ); // invoke-virtual {v1, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V
/* .line 369 */
(( java.io.ByteArrayOutputStream ) v1 ).write ( v6 ); // invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V
/* .line 370 */
/* add-int/2addr v0, v7 */
/* .line 350 */
} // .end local v3 # "profile":Lcom/android/server/pm/DexProfileData;
} // .end local v4 # "methodFlags":I
} // .end local v5 # "bitmapContents":[B
} // .end local v6 # "methodRegionContents":[B
} // .end local v7 # "followingDataSize":I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 372 */
} // .end local v2 # "i":I
} // :cond_0
(( java.io.ByteArrayOutputStream ) v1 ).toByteArray ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* .line 373 */
/* .local v2, "contents":[B */
/* array-length v3, v2 */
/* if-ne v0, v3, :cond_1 */
/* .line 379 */
/* new-instance v3, Lcom/android/server/pm/WritableFileSection; */
v4 = com.android.server.pm.FileSectionType.METHODS;
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {v3, v4, v0, v2, v5}, Lcom/android/server/pm/WritableFileSection;-><init>(Lcom/android/server/pm/FileSectionType;I[BZ)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 385 */
(( java.io.ByteArrayOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
/* .line 379 */
/* .line 374 */
} // :cond_1
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Expected size "; // const-string v4, "Expected size "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", does not match actual size "; // const-string v4, ", does not match actual size "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v4, v2 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
} // .end local v0 # "expectedSize":I
} // .end local v1 # "out":Ljava/io/ByteArrayOutputStream;
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // .end local p1 # "profileData":[Lcom/android/server/pm/DexProfileData;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 349 */
} // .end local v2 # "contents":[B
/* .restart local v0 # "expectedSize":I */
/* .restart local v1 # "out":Ljava/io/ByteArrayOutputStream; */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* .restart local p1 # "profileData":[Lcom/android/server/pm/DexProfileData; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_2
(( java.io.ByteArrayOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :goto_1
/* throw v2 */
} // .end method
private createMethodBitmapRegion ( com.android.server.pm.DexProfileData p0 ) {
/* .locals 3 */
/* .param p1, "profile" # Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 391 */
/* new-instance v0, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 392 */
/* .local v0, "out":Ljava/io/ByteArrayOutputStream; */
try { // :try_start_0
/* invoke-direct {p0, v0, p1}, Lcom/android/server/pm/ProfileTranscoder;->writeMethodBitmap(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V */
/* .line 393 */
(( java.io.ByteArrayOutputStream ) v0 ).toByteArray ( ); // invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 394 */
(( java.io.ByteArrayOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
/* .line 393 */
/* .line 391 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
(( java.io.ByteArrayOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :goto_0
/* throw v1 */
} // .end method
private createMethodsWithInlineCaches ( com.android.server.pm.DexProfileData p0 ) {
/* .locals 3 */
/* .param p1, "profile" # Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 400 */
/* new-instance v0, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 401 */
/* .local v0, "out":Ljava/io/ByteArrayOutputStream; */
try { // :try_start_0
/* invoke-direct {p0, v0, p1}, Lcom/android/server/pm/ProfileTranscoder;->writeMethodsWithInlineCaches(Ljava/io/OutputStream;Lcom/android/server/pm/DexProfileData;)V */
/* .line 402 */
(( java.io.ByteArrayOutputStream ) v0 ).toByteArray ( ); // invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 403 */
(( java.io.ByteArrayOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
/* .line 402 */
/* .line 400 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
(( java.io.ByteArrayOutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :goto_0
/* throw v1 */
} // .end method
private desiredVersion ( ) {
/* .locals 1 */
/* .line 160 */
/* nop */
/* .line 161 */
v0 = com.android.server.pm.ProfileTranscoder.V015_S;
} // .end method
private java.lang.String enforceSeparator ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "value" # Ljava/lang/String; */
/* .param p2, "separator" # Ljava/lang/String; */
/* .line 690 */
final String v0 = "!"; // const-string v0, "!"
v1 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v2 = ":"; // const-string v2, ":"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 691 */
(( java.lang.String ) p1 ).replace ( v2, v0 ); // invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* .line 692 */
} // :cond_0
v1 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 693 */
(( java.lang.String ) p1 ).replace ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* .line 695 */
} // :cond_1
} // .end method
private java.lang.String extractKey ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "profileKey" # Ljava/lang/String; */
/* .line 700 */
final String v0 = "!"; // const-string v0, "!"
v0 = (( java.lang.String ) p1 ).indexOf ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* .line 701 */
/* .local v0, "index":I */
/* if-gez v0, :cond_0 */
/* .line 702 */
final String v1 = ":"; // const-string v1, ":"
v0 = (( java.lang.String ) p1 ).indexOf ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
/* .line 704 */
} // :cond_0
/* if-lez v0, :cond_1 */
/* .line 706 */
/* add-int/lit8 v1, v0, 0x1 */
(( java.lang.String ) p1 ).substring ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 708 */
} // :cond_1
} // .end method
private com.android.server.pm.DexProfileData findByDexName ( com.android.server.pm.DexProfileData[] p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "profile" # [Lcom/android/server/pm/DexProfileData; */
/* .param p2, "profileKey" # Ljava/lang/String; */
/* .line 644 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
/* if-gtz v0, :cond_0 */
/* .line 649 */
} // :cond_0
/* invoke-direct {p0, p2}, Lcom/android/server/pm/ProfileTranscoder;->extractKey(Ljava/lang/String;)Ljava/lang/String; */
/* .line 650 */
/* .local v0, "dexName":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, p1 */
/* if-ge v2, v3, :cond_2 */
/* .line 651 */
/* aget-object v3, p1, v2 */
v3 = this.dexName;
v3 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 652 */
/* aget-object v1, p1, v2 */
/* .line 650 */
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 655 */
} // .end local v2 # "i":I
} // :cond_2
} // .end method
private java.lang.String generateDexKey ( java.lang.String p0, java.lang.String p1, Object[] p2 ) {
/* .locals 2 */
/* .param p1, "apkName" # Ljava/lang/String; */
/* .param p2, "dexName" # Ljava/lang/String; */
/* .param p3, "version" # [B */
/* .line 678 */
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
final String v1 = "!"; // const-string v1, "!"
/* if-gtz v0, :cond_0 */
/* invoke-direct {p0, p2, v1}, Lcom/android/server/pm/ProfileTranscoder;->enforceSeparator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
/* .line 679 */
} // :cond_0
final String v0 = "classes.dex"; // const-string v0, "classes.dex"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 680 */
} // :cond_1
v0 = (( java.lang.String ) p2 ).contains ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_4 */
final String v0 = ":"; // const-string v0, ":"
v0 = (( java.lang.String ) p2 ).contains ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 683 */
} // :cond_2
final String v0 = ".apk"; // const-string v0, ".apk"
v0 = (( java.lang.String ) p2 ).endsWith ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 684 */
} // :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 681 */
} // :cond_4
} // :goto_0
/* invoke-direct {p0, p2, v1}, Lcom/android/server/pm/ProfileTranscoder;->enforceSeparator(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; */
} // .end method
private Integer getMethodBitmapStorageSize ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "numMethodIds" # I */
/* .line 416 */
/* mul-int/lit8 v0, p1, 0x2 */
/* .line 417 */
/* .local v0, "methodBitmapBits":I */
v1 = /* invoke-direct {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->roundUpToByte(I)I */
/* div-int/lit8 v1, v1, 0x8 */
} // .end method
private Boolean isSupportTranscode ( ) {
/* .locals 2 */
/* .line 156 */
v0 = this.mDesiredVersion;
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = com.android.server.pm.ProfileTranscoder.V015_S;
v0 = java.util.Arrays .equals ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Integer methodFlagBitmapIndex ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "flag" # I */
/* .param p2, "methodIndex" # I */
/* .param p3, "numMethodIds" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 863 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 871 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Unexpected flag: "; // const-string v1, "Unexpected flag: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v0 */
/* .line 869 */
/* :pswitch_1 */
/* add-int v0, p2, p3 */
/* .line 867 */
/* :pswitch_2 */
/* .line 865 */
/* :pswitch_3 */
final String v0 = "HOT methods are not stored in the bitmap"; // const-string v0, "HOT methods are not stored in the bitmap"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v0 */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private readClasses ( java.io.InputStream p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "classSetSize" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 821 */
/* new-array v0, p2, [I */
/* .line 822 */
/* .local v0, "classes":[I */
int v1 = 0; // const/4 v1, 0x0
/* .line 823 */
/* .local v1, "lastClassIndex":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "k":I */
} // :goto_0
/* if-ge v2, p2, :cond_0 */
/* .line 824 */
v3 = this.mEncoding;
v3 = (( com.android.server.pm.Encoding ) v3 ).readUInt16 ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 825 */
/* .local v3, "diffWithTheLastClassIndex":I */
/* add-int v4, v1, v3 */
/* .line 826 */
/* .local v4, "classDexIndex":I */
/* aput v4, v0, v2 */
/* .line 827 */
/* move v1, v4 */
/* .line 823 */
} // .end local v3 # "diffWithTheLastClassIndex":I
} // .end local v4 # "classDexIndex":I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 829 */
} // .end local v2 # "k":I
} // :cond_0
} // .end method
private Integer readFlagsFromBitmap ( java.util.BitSet p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "bs" # Ljava/util/BitSet; */
/* .param p2, "methodIndex" # I */
/* .param p3, "numMethodIds" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 850 */
int v0 = 0; // const/4 v0, 0x0
/* .line 851 */
/* .local v0, "result":I */
int v1 = 2; // const/4 v1, 0x2
v1 = /* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/pm/ProfileTranscoder;->methodFlagBitmapIndex(III)I */
v1 = (( java.util.BitSet ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Ljava/util/BitSet;->get(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 852 */
/* or-int/lit8 v0, v0, 0x2 */
/* .line 854 */
} // :cond_0
int v1 = 4; // const/4 v1, 0x4
v1 = /* invoke-direct {p0, v1, p2, p3}, Lcom/android/server/pm/ProfileTranscoder;->methodFlagBitmapIndex(III)I */
v1 = (( java.util.BitSet ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Ljava/util/BitSet;->get(I)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 855 */
/* or-int/lit8 v0, v0, 0x4 */
/* .line 857 */
} // :cond_1
} // .end method
private void readHotMethodRegion ( java.io.InputStream p0, com.android.server.pm.DexProfileData p1 ) {
/* .locals 7 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "data" # Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 767 */
v0 = (( java.io.InputStream ) p1 ).available ( ); // invoke-virtual {p1}, Ljava/io/InputStream;->available()I
/* iget v1, p2, Lcom/android/server/pm/DexProfileData;->hotMethodRegionSize:I */
/* sub-int/2addr v0, v1 */
/* .line 768 */
/* .local v0, "expectedBytesAvailableAfterRead":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 770 */
/* .local v1, "lastMethodIndex":I */
} // :goto_0
v2 = (( java.io.InputStream ) p1 ).available ( ); // invoke-virtual {p1}, Ljava/io/InputStream;->available()I
/* if-le v2, v0, :cond_1 */
/* .line 773 */
v2 = this.mEncoding;
v2 = (( com.android.server.pm.Encoding ) v2 ).readUInt16 ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 774 */
/* .local v2, "diffWithLastMethodDexIndex":I */
/* add-int v3, v1, v2 */
/* .line 775 */
/* .local v3, "methodDexIndex":I */
v4 = this.methods;
java.lang.Integer .valueOf ( v3 );
int v6 = 1; // const/4 v6, 0x1
java.lang.Integer .valueOf ( v6 );
(( java.util.TreeMap ) v4 ).put ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 777 */
v4 = this.mEncoding;
v4 = (( com.android.server.pm.Encoding ) v4 ).readUInt16 ( p1 ); // invoke-virtual {v4, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 778 */
/* .local v4, "inlineCacheSize":I */
} // :goto_1
/* if-lez v4, :cond_0 */
/* .line 779 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/ProfileTranscoder;->skipInlineCache(Ljava/io/InputStream;)V */
/* .line 780 */
/* add-int/lit8 v4, v4, -0x1 */
/* .line 783 */
} // :cond_0
/* move v1, v3 */
/* .line 784 */
} // .end local v2 # "diffWithLastMethodDexIndex":I
} // .end local v3 # "methodDexIndex":I
} // .end local v4 # "inlineCacheSize":I
/* .line 786 */
} // :cond_1
v2 = (( java.io.InputStream ) p1 ).available ( ); // invoke-virtual {p1}, Ljava/io/InputStream;->available()I
/* if-ne v2, v0, :cond_2 */
/* .line 791 */
return;
/* .line 787 */
} // :cond_2
final String v2 = "Read too much data during profile line parse"; // const-string v2, "Read too much data during profile line parse"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v2 */
} // .end method
private com.android.server.pm.DexProfileData readMetadataV002Body ( java.io.InputStream p0, Object[] p1, Integer p2, com.android.server.pm.DexProfileData[] p3 ) {
/* .locals 9 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "desiredProfileVersion" # [B */
/* .param p3, "dexFileCount" # I */
/* .param p4, "profile" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 611 */
v0 = (( java.io.InputStream ) p1 ).available ( ); // invoke-virtual {p1}, Ljava/io/InputStream;->available()I
/* if-nez v0, :cond_0 */
/* .line 612 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Lcom/android/server/pm/DexProfileData; */
/* .line 614 */
} // :cond_0
/* array-length v0, p4 */
/* if-ne p3, v0, :cond_3 */
/* .line 617 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* if-ge v0, p3, :cond_2 */
/* .line 619 */
v1 = this.mEncoding;
(( com.android.server.pm.Encoding ) v1 ).readUInt16 ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 621 */
v1 = this.mEncoding;
v1 = (( com.android.server.pm.Encoding ) v1 ).readUInt16 ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 622 */
/* .local v1, "profileKeySize":I */
v2 = this.mEncoding;
(( com.android.server.pm.Encoding ) v2 ).readString ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Lcom/android/server/pm/Encoding;->readString(Ljava/io/InputStream;I)Ljava/lang/String;
/* .line 624 */
/* .local v2, "profileKey":Ljava/lang/String; */
v3 = this.mEncoding;
(( com.android.server.pm.Encoding ) v3 ).readUInt32 ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J
/* move-result-wide v3 */
/* .line 626 */
/* .local v3, "typeIdCount":J */
v5 = this.mEncoding;
v5 = (( com.android.server.pm.Encoding ) v5 ).readUInt16 ( p1 ); // invoke-virtual {v5, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 627 */
/* .local v5, "classIdSetSize":I */
/* invoke-direct {p0, p4, v2}, Lcom/android/server/pm/ProfileTranscoder;->findByDexName([Lcom/android/server/pm/DexProfileData;Ljava/lang/String;)Lcom/android/server/pm/DexProfileData; */
/* .line 628 */
/* .local v6, "data":Lcom/android/server/pm/DexProfileData; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 632 */
/* iput-wide v3, v6, Lcom/android/server/pm/DexProfileData;->mTypeIdCount:J */
/* .line 636 */
/* invoke-direct {p0, p1, v5}, Lcom/android/server/pm/ProfileTranscoder;->readClasses(Ljava/io/InputStream;I)[I */
/* .line 617 */
} // .end local v1 # "profileKeySize":I
} // .end local v2 # "profileKey":Ljava/lang/String;
} // .end local v3 # "typeIdCount":J
} // .end local v5 # "classIdSetSize":I
} // .end local v6 # "data":Lcom/android/server/pm/DexProfileData;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 629 */
/* .restart local v1 # "profileKeySize":I */
/* .restart local v2 # "profileKey":Ljava/lang/String; */
/* .restart local v3 # "typeIdCount":J */
/* .restart local v5 # "classIdSetSize":I */
/* .restart local v6 # "data":Lcom/android/server/pm/DexProfileData; */
} // :cond_1
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Missing profile key: "; // const-string v8, "Missing profile key: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v7 ); // invoke-virtual {p0, v7}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v7 */
/* .line 638 */
} // .end local v0 # "i":I
} // .end local v1 # "profileKeySize":I
} // .end local v2 # "profileKey":Ljava/lang/String;
} // .end local v3 # "typeIdCount":J
} // .end local v5 # "classIdSetSize":I
} // .end local v6 # "data":Lcom/android/server/pm/DexProfileData;
} // :cond_2
/* .line 615 */
} // :cond_3
final String v0 = "Mismatched number of dex files found in metadata"; // const-string v0, "Mismatched number of dex files found in metadata"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v0 */
} // .end method
private void readMethodBitmap ( java.io.InputStream p0, com.android.server.pm.DexProfileData p1 ) {
/* .locals 9 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "data" # Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 836 */
v0 = this.mEncoding;
/* iget v1, p2, Lcom/android/server/pm/DexProfileData;->numMethodIds:I */
/* mul-int/lit8 v1, v1, 0x2 */
v0 = (( com.android.server.pm.Encoding ) v0 ).bitsToBytes ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/pm/Encoding;->bitsToBytes(I)I
/* .line 837 */
/* .local v0, "methodBitmapStorageSize":I */
v1 = this.mEncoding;
(( com.android.server.pm.Encoding ) v1 ).read ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B
/* .line 838 */
/* .local v1, "methodBitmap":[B */
java.util.BitSet .valueOf ( v1 );
/* .line 839 */
/* .local v2, "bs":Ljava/util/BitSet; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "methodIndex":I */
} // :goto_0
/* iget v4, p2, Lcom/android/server/pm/DexProfileData;->numMethodIds:I */
/* if-ge v3, v4, :cond_2 */
/* .line 840 */
/* iget v4, p2, Lcom/android/server/pm/DexProfileData;->numMethodIds:I */
v4 = /* invoke-direct {p0, v2, v3, v4}, Lcom/android/server/pm/ProfileTranscoder;->readFlagsFromBitmap(Ljava/util/BitSet;II)I */
/* .line 841 */
/* .local v4, "newFlags":I */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 842 */
v5 = this.methods;
java.lang.Integer .valueOf ( v3 );
(( java.util.TreeMap ) v5 ).get ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Integer; */
/* .line 843 */
/* .local v5, "current":Ljava/lang/Integer; */
/* if-nez v5, :cond_0 */
int v6 = 0; // const/4 v6, 0x0
java.lang.Integer .valueOf ( v6 );
/* .line 844 */
} // :cond_0
v6 = this.methods;
java.lang.Integer .valueOf ( v3 );
v8 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* or-int/2addr v8, v4 */
java.lang.Integer .valueOf ( v8 );
(( java.util.TreeMap ) v6 ).put ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 839 */
} // .end local v4 # "newFlags":I
} // .end local v5 # "current":Ljava/lang/Integer;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 847 */
} // .end local v3 # "methodIndex":I
} // :cond_2
return;
} // .end method
private com.android.server.pm.DexProfileData readUncompressedBody ( java.io.InputStream p0, java.lang.String p1, Integer p2 ) {
/* .locals 27 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "apkName" # Ljava/lang/String; */
/* .param p3, "numberOfDexFiles" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 724 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move/from16 v2, p3 */
v3 = /* invoke-virtual/range {p1 ..p1}, Ljava/io/InputStream;->available()I */
int v4 = 0; // const/4 v4, 0x0
/* if-nez v3, :cond_0 */
/* .line 725 */
/* new-array v3, v4, [Lcom/android/server/pm/DexProfileData; */
/* .line 728 */
} // :cond_0
/* new-array v3, v2, [Lcom/android/server/pm/DexProfileData; */
/* .line 729 */
/* .local v3, "lines":[Lcom/android/server/pm/DexProfileData; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* if-ge v5, v2, :cond_1 */
/* .line 730 */
v6 = this.mEncoding;
v6 = (( com.android.server.pm.Encoding ) v6 ).readUInt16 ( v1 ); // invoke-virtual {v6, v1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 731 */
/* .local v6, "dexNameSize":I */
v7 = this.mEncoding;
v7 = (( com.android.server.pm.Encoding ) v7 ).readUInt16 ( v1 ); // invoke-virtual {v7, v1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 732 */
/* .local v7, "classSetSize":I */
v8 = this.mEncoding;
(( com.android.server.pm.Encoding ) v8 ).readUInt32 ( v1 ); // invoke-virtual {v8, v1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J
/* move-result-wide v13 */
/* .line 733 */
/* .local v13, "hotMethodRegionSize":J */
v8 = this.mEncoding;
(( com.android.server.pm.Encoding ) v8 ).readUInt32 ( v1 ); // invoke-virtual {v8, v1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J
/* move-result-wide v20 */
/* .line 734 */
/* .local v20, "dexChecksum":J */
v8 = this.mEncoding;
(( com.android.server.pm.Encoding ) v8 ).readUInt32 ( v1 ); // invoke-virtual {v8, v1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J
/* move-result-wide v11 */
/* .line 735 */
/* .local v11, "numMethodIds":J */
/* new-instance v22, Lcom/android/server/pm/DexProfileData; */
v8 = this.mEncoding;
/* .line 737 */
(( com.android.server.pm.Encoding ) v8 ).readString ( v1, v6 ); // invoke-virtual {v8, v1, v6}, Lcom/android/server/pm/Encoding;->readString(Ljava/io/InputStream;I)Ljava/lang/String;
/* const-wide/16 v15, 0x0 */
/* long-to-int v9, v13 */
/* long-to-int v8, v11 */
/* new-array v4, v7, [I */
/* new-instance v19, Ljava/util/TreeMap; */
/* invoke-direct/range {v19 ..v19}, Ljava/util/TreeMap;-><init>()V */
/* move/from16 v17, v8 */
/* move-object/from16 v8, v22 */
/* move/from16 v18, v9 */
/* move-object/from16 v9, p2 */
/* move-wide/from16 v23, v11 */
} // .end local v11 # "numMethodIds":J
/* .local v23, "numMethodIds":J */
/* move-wide/from16 v11, v20 */
/* move-wide/from16 v25, v13 */
} // .end local v13 # "hotMethodRegionSize":J
/* .local v25, "hotMethodRegionSize":J */
/* move-wide v13, v15 */
/* move v15, v7 */
/* move/from16 v16, v18 */
/* move-object/from16 v18, v4 */
/* invoke-direct/range {v8 ..v19}, Lcom/android/server/pm/DexProfileData;-><init>(Ljava/lang/String;Ljava/lang/String;JJIII[ILjava/util/TreeMap;)V */
/* aput-object v22, v3, v5 */
/* .line 729 */
} // .end local v6 # "dexNameSize":I
} // .end local v7 # "classSetSize":I
} // .end local v20 # "dexChecksum":J
} // .end local v23 # "numMethodIds":J
} // .end local v25 # "hotMethodRegionSize":J
/* add-int/lit8 v5, v5, 0x1 */
int v4 = 0; // const/4 v4, 0x0
/* .line 750 */
} // .end local v5 # "i":I
} // :cond_1
/* array-length v4, v3 */
int v5 = 0; // const/4 v5, 0x0
} // :goto_1
/* if-ge v5, v4, :cond_2 */
/* aget-object v6, v3, v5 */
/* .line 752 */
/* .local v6, "data":Lcom/android/server/pm/DexProfileData; */
/* invoke-direct {v0, v1, v6}, Lcom/android/server/pm/ProfileTranscoder;->readHotMethodRegion(Ljava/io/InputStream;Lcom/android/server/pm/DexProfileData;)V */
/* .line 754 */
/* iget v7, v6, Lcom/android/server/pm/DexProfileData;->classSetSize:I */
/* invoke-direct {v0, v1, v7}, Lcom/android/server/pm/ProfileTranscoder;->readClasses(Ljava/io/InputStream;I)[I */
this.classes = v7;
/* .line 758 */
/* invoke-direct {v0, v1, v6}, Lcom/android/server/pm/ProfileTranscoder;->readMethodBitmap(Ljava/io/InputStream;Lcom/android/server/pm/DexProfileData;)V */
/* .line 750 */
} // .end local v6 # "data":Lcom/android/server/pm/DexProfileData;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 760 */
} // :cond_2
} // .end method
private Integer roundUpToByte ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "bits" # I */
/* .line 421 */
/* add-int/lit8 v0, p1, 0x8 */
/* add-int/lit8 v0, v0, -0x1 */
/* and-int/lit8 v0, v0, -0x8 */
} // .end method
private void setMethodBitmapBit ( Object[] p0, Integer p1, Integer p2, com.android.server.pm.DexProfileData p3 ) {
/* .locals 5 */
/* .param p1, "bitmap" # [B */
/* .param p2, "flag" # I */
/* .param p3, "methodIndex" # I */
/* .param p4, "dexData" # Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 438 */
/* iget v0, p4, Lcom/android/server/pm/DexProfileData;->numMethodIds:I */
v0 = /* invoke-direct {p0, p2, p3, v0}, Lcom/android/server/pm/ProfileTranscoder;->methodFlagBitmapIndex(III)I */
/* .line 439 */
/* .local v0, "bitIndex":I */
/* div-int/lit8 v1, v0, 0x8 */
/* .line 440 */
/* .local v1, "bitmapIndex":I */
/* aget-byte v2, p1, v1 */
int v3 = 1; // const/4 v3, 0x1
/* rem-int/lit8 v4, v0, 0x8 */
/* shl-int/2addr v3, v4 */
/* or-int/2addr v2, v3 */
/* int-to-byte v2, v2 */
/* .line 441 */
/* .local v2, "value":B */
/* aput-byte v2, p1, v1 */
/* .line 442 */
return;
} // .end method
private void skipInlineCache ( java.io.InputStream p0 ) {
/* .locals 3 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 794 */
v0 = this.mEncoding;
(( com.android.server.pm.Encoding ) v0 ).readUInt16 ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 795 */
v0 = this.mEncoding;
v0 = (( com.android.server.pm.Encoding ) v0 ).readUInt8 ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/Encoding;->readUInt8(Ljava/io/InputStream;)I
/* .line 797 */
/* .local v0, "dexPcMapSize":I */
int v1 = 6; // const/4 v1, 0x6
/* if-ne v0, v1, :cond_0 */
/* .line 798 */
return;
/* .line 801 */
} // :cond_0
int v1 = 7; // const/4 v1, 0x7
/* if-ne v0, v1, :cond_1 */
/* .line 802 */
return;
/* .line 806 */
} // :cond_1
} // :goto_0
/* if-lez v0, :cond_3 */
/* .line 807 */
v1 = this.mEncoding;
(( com.android.server.pm.Encoding ) v1 ).readUInt8 ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt8(Ljava/io/InputStream;)I
/* .line 808 */
v1 = this.mEncoding;
v1 = (( com.android.server.pm.Encoding ) v1 ).readUInt8 ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt8(Ljava/io/InputStream;)I
/* .line 809 */
/* .local v1, "numClasses":I */
} // :goto_1
/* if-lez v1, :cond_2 */
/* .line 810 */
v2 = this.mEncoding;
(( com.android.server.pm.Encoding ) v2 ).readUInt16 ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 811 */
/* add-int/lit8 v1, v1, -0x1 */
/* .line 813 */
} // :cond_2
/* nop */
} // .end local v1 # "numClasses":I
/* add-int/lit8 v0, v0, -0x1 */
/* .line 814 */
/* .line 815 */
} // :cond_3
return;
} // .end method
private void writeClasses ( java.io.OutputStream p0, com.android.server.pm.DexProfileData p1 ) {
/* .locals 7 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "dexData" # Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 482 */
int v0 = 0; // const/4 v0, 0x0
/* .line 485 */
/* .local v0, "lastClassIndex":I */
v1 = this.classes;
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_0 */
/* aget v4, v1, v3 */
java.lang.Integer .valueOf ( v4 );
/* .line 486 */
/* .local v4, "classIndex":Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* sub-int/2addr v5, v0 */
/* .line 487 */
/* .local v5, "diffWithTheLastClassIndex":I */
v6 = this.mEncoding;
(( com.android.server.pm.Encoding ) v6 ).writeUInt16 ( p1, v5 ); // invoke-virtual {v6, p1, v5}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 488 */
v0 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 485 */
} // .end local v4 # "classIndex":Ljava/lang/Integer;
} // .end local v5 # "diffWithTheLastClassIndex":I
/* add-int/lit8 v3, v3, 0x1 */
/* .line 490 */
} // :cond_0
return;
} // .end method
private com.android.server.pm.WritableFileSection writeDexFileSection ( com.android.server.pm.DexProfileData[] p0 ) {
/* .locals 7 */
/* .param p1, "profileData" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 265 */
int v0 = 0; // const/4 v0, 0x0
/* .line 266 */
/* .local v0, "expectedSize":I */
/* new-instance v1, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* .line 268 */
/* .local v1, "out":Ljava/io/ByteArrayOutputStream; */
/* add-int/lit8 v0, v0, 0x2 */
/* .line 269 */
try { // :try_start_0
v2 = this.mEncoding;
/* array-length v3, p1 */
(( com.android.server.pm.Encoding ) v2 ).writeUInt16 ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 270 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, p1 */
/* if-ge v2, v3, :cond_0 */
/* .line 271 */
/* aget-object v3, p1, v2 */
/* .line 273 */
/* .local v3, "profile":Lcom/android/server/pm/DexProfileData; */
/* add-int/lit8 v0, v0, 0x4 */
/* .line 274 */
v4 = this.mEncoding;
/* iget-wide v5, v3, Lcom/android/server/pm/DexProfileData;->dexChecksum:J */
(( com.android.server.pm.Encoding ) v4 ).writeUInt32 ( v1, v5, v6 ); // invoke-virtual {v4, v1, v5, v6}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 276 */
/* add-int/lit8 v0, v0, 0x4 */
/* .line 280 */
v4 = this.mEncoding;
/* iget-wide v5, v3, Lcom/android/server/pm/DexProfileData;->mTypeIdCount:J */
(( com.android.server.pm.Encoding ) v4 ).writeUInt32 ( v1, v5, v6 ); // invoke-virtual {v4, v1, v5, v6}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 282 */
/* add-int/lit8 v0, v0, 0x4 */
/* .line 283 */
v4 = this.mEncoding;
/* iget v5, v3, Lcom/android/server/pm/DexProfileData;->numMethodIds:I */
/* int-to-long v5, v5 */
(( com.android.server.pm.Encoding ) v4 ).writeUInt32 ( v1, v5, v6 ); // invoke-virtual {v4, v1, v5, v6}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 285 */
v4 = this.apkName;
v5 = this.dexName;
v6 = com.android.server.pm.ProfileTranscoder.V015_S;
/* invoke-direct {p0, v4, v5, v6}, Lcom/android/server/pm/ProfileTranscoder;->generateDexKey(Ljava/lang/String;Ljava/lang/String;[B)Ljava/lang/String; */
/* .line 290 */
/* .local v4, "profileKey":Ljava/lang/String; */
/* add-int/lit8 v0, v0, 0x2 */
/* .line 291 */
v5 = this.mEncoding;
v5 = (( com.android.server.pm.Encoding ) v5 ).utf8Length ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/pm/Encoding;->utf8Length(Ljava/lang/String;)I
/* .line 292 */
/* .local v5, "keyLength":I */
v6 = this.mEncoding;
(( com.android.server.pm.Encoding ) v6 ).writeUInt16 ( v1, v5 ); // invoke-virtual {v6, v1, v5}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 293 */
/* mul-int/lit8 v6, v5, 0x1 */
/* add-int/2addr v0, v6 */
/* .line 294 */
v6 = this.mEncoding;
(( com.android.server.pm.Encoding ) v6 ).writeString ( v1, v4 ); // invoke-virtual {v6, v1, v4}, Lcom/android/server/pm/Encoding;->writeString(Ljava/io/OutputStream;Ljava/lang/String;)V
/* .line 270 */
} // .end local v3 # "profile":Lcom/android/server/pm/DexProfileData;
} // .end local v4 # "profileKey":Ljava/lang/String;
} // .end local v5 # "keyLength":I
/* add-int/lit8 v2, v2, 0x1 */
/* .line 296 */
} // .end local v2 # "i":I
} // :cond_0
(( java.io.ByteArrayOutputStream ) v1 ).toByteArray ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
/* .line 297 */
/* .local v2, "contents":[B */
/* array-length v3, v2 */
/* if-ne v0, v3, :cond_1 */
/* .line 303 */
/* new-instance v3, Lcom/android/server/pm/WritableFileSection; */
v4 = com.android.server.pm.FileSectionType.DEX_FILES;
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v3, v4, v0, v2, v5}, Lcom/android/server/pm/WritableFileSection;-><init>(Lcom/android/server/pm/FileSectionType;I[BZ)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 309 */
(( java.io.ByteArrayOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
/* .line 303 */
/* .line 298 */
} // :cond_1
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Expected size "; // const-string v4, "Expected size "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", does not match actual size "; // const-string v4, ", does not match actual size "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v4, v2 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
} // .end local v0 # "expectedSize":I
} // .end local v1 # "out":Ljava/io/ByteArrayOutputStream;
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // .end local p1 # "profileData":[Lcom/android/server/pm/DexProfileData;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 266 */
} // .end local v2 # "contents":[B
/* .restart local v0 # "expectedSize":I */
/* .restart local v1 # "out":Ljava/io/ByteArrayOutputStream; */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* .restart local p1 # "profileData":[Lcom/android/server/pm/DexProfileData; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_2
(( java.io.ByteArrayOutputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :goto_1
/* throw v2 */
} // .end method
private void writeMethodBitmap ( java.io.OutputStream p0, com.android.server.pm.DexProfileData p1 ) {
/* .locals 6 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "dexData" # Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 501 */
/* iget v0, p2, Lcom/android/server/pm/DexProfileData;->numMethodIds:I */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->getMethodBitmapStorageSize(I)I */
/* new-array v0, v0, [B */
/* .line 502 */
/* .local v0, "bitmap":[B */
v1 = this.methods;
(( java.util.TreeMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 503 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 504 */
/* .local v3, "methodIndex":I */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 505 */
/* .local v4, "flagValue":I */
/* and-int/lit8 v5, v4, 0x2 */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 506 */
int v5 = 2; // const/4 v5, 0x2
/* invoke-direct {p0, v0, v5, v3, p2}, Lcom/android/server/pm/ProfileTranscoder;->setMethodBitmapBit([BIILcom/android/server/pm/DexProfileData;)V */
/* .line 508 */
} // :cond_0
/* and-int/lit8 v5, v4, 0x4 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 509 */
int v5 = 4; // const/4 v5, 0x4
/* invoke-direct {p0, v0, v5, v3, p2}, Lcom/android/server/pm/ProfileTranscoder;->setMethodBitmapBit([BIILcom/android/server/pm/DexProfileData;)V */
/* .line 511 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
} // .end local v3 # "methodIndex":I
} // .end local v4 # "flagValue":I
} // :cond_1
/* .line 512 */
} // :cond_2
(( java.io.OutputStream ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V
/* .line 513 */
return;
} // .end method
private void writeMethodsWithInlineCaches ( java.io.OutputStream p0, com.android.server.pm.DexProfileData p1 ) {
/* .locals 8 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "dexData" # Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 456 */
int v0 = 0; // const/4 v0, 0x0
/* .line 457 */
/* .local v0, "lastMethodIndex":I */
v1 = this.methods;
(( java.util.TreeMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 458 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 459 */
/* .local v3, "methodId":I */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 460 */
/* .local v4, "flags":I */
/* and-int/lit8 v5, v4, 0x1 */
/* if-nez v5, :cond_0 */
/* .line 461 */
/* .line 463 */
} // :cond_0
/* sub-int v5, v3, v0 */
/* .line 464 */
/* .local v5, "diffWithTheLastMethodIndex":I */
v6 = this.mEncoding;
(( com.android.server.pm.Encoding ) v6 ).writeUInt16 ( p1, v5 ); // invoke-virtual {v6, p1, v5}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 465 */
v6 = this.mEncoding;
int v7 = 0; // const/4 v7, 0x0
(( com.android.server.pm.Encoding ) v6 ).writeUInt16 ( p1, v7 ); // invoke-virtual {v6, p1, v7}, Lcom/android/server/pm/Encoding;->writeUInt16(Ljava/io/OutputStream;I)V
/* .line 466 */
/* move v0, v3 */
/* .line 467 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
} // .end local v3 # "methodId":I
} // .end local v4 # "flags":I
} // .end local v5 # "diffWithTheLastMethodIndex":I
/* .line 468 */
} // :cond_1
return;
} // .end method
private void writeProfileForS ( java.io.OutputStream p0, com.android.server.pm.DexProfileData[] p1 ) {
/* .locals 0 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "profileData" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 211 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/pm/ProfileTranscoder;->writeProfileSections(Ljava/io/OutputStream;[Lcom/android/server/pm/DexProfileData;)V */
/* .line 212 */
return;
} // .end method
private void writeProfileSections ( java.io.OutputStream p0, com.android.server.pm.DexProfileData[] p1 ) {
/* .locals 12 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "profileData" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 220 */
/* new-instance v0, Ljava/util/ArrayList; */
int v1 = 3; // const/4 v1, 0x3
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 221 */
/* .local v0, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/WritableFileSection;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v1, v2 */
/* .line 222 */
/* .local v1, "sectionContents":Ljava/util/List;, "Ljava/util/List<[B>;" */
/* invoke-direct {p0, p2}, Lcom/android/server/pm/ProfileTranscoder;->writeDexFileSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection; */
/* .line 223 */
/* invoke-direct {p0, p2}, Lcom/android/server/pm/ProfileTranscoder;->createCompressibleClassSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection; */
/* .line 224 */
/* invoke-direct {p0, p2}, Lcom/android/server/pm/ProfileTranscoder;->createCompressibleMethodsSection([Lcom/android/server/pm/DexProfileData;)Lcom/android/server/pm/WritableFileSection; */
/* .line 225 */
v2 = com.android.server.pm.ProfileTranscoder.V015_S;
/* array-length v2, v2 */
/* int-to-long v2, v2 */
v4 = com.android.server.pm.ProfileTranscoder.MAGIC_PROF;
/* array-length v4, v4 */
/* int-to-long v4, v4 */
/* add-long/2addr v2, v4 */
/* .line 227 */
/* .local v2, "offset":J */
/* const-wide/16 v4, 0x4 */
/* add-long/2addr v2, v4 */
v4 = /* .line 229 */
/* mul-int/lit8 v4, v4, 0x10 */
/* int-to-long v4, v4 */
/* add-long/2addr v2, v4 */
/* .line 230 */
v5 = v4 = this.mEncoding;
/* int-to-long v5, v5 */
(( com.android.server.pm.Encoding ) v4 ).writeUInt32 ( p1, v5, v6 ); // invoke-virtual {v4, p1, v5, v6}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 231 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
v5 = } // :goto_0
/* if-ge v4, v5, :cond_1 */
/* .line 232 */
/* check-cast v5, Lcom/android/server/pm/WritableFileSection; */
/* .line 234 */
/* .local v5, "section":Lcom/android/server/pm/WritableFileSection; */
v6 = this.mEncoding;
v7 = this.mType;
(( com.android.server.pm.FileSectionType ) v7 ).getValue ( ); // invoke-virtual {v7}, Lcom/android/server/pm/FileSectionType;->getValue()J
/* move-result-wide v7 */
(( com.android.server.pm.Encoding ) v6 ).writeUInt32 ( p1, v7, v8 ); // invoke-virtual {v6, p1, v7, v8}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 236 */
v6 = this.mEncoding;
(( com.android.server.pm.Encoding ) v6 ).writeUInt32 ( p1, v2, v3 ); // invoke-virtual {v6, p1, v2, v3}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 238 */
/* iget-boolean v6, v5, Lcom/android/server/pm/WritableFileSection;->mNeedsCompression:Z */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 239 */
v6 = this.mContents;
/* array-length v6, v6 */
/* int-to-long v6, v6 */
/* .line 240 */
/* .local v6, "inflatedSize":J */
v8 = this.mEncoding;
v9 = this.mContents;
(( com.android.server.pm.Encoding ) v8 ).compress ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/pm/Encoding;->compress([B)[B
/* .line 241 */
/* .local v8, "compressed":[B */
/* .line 243 */
v9 = this.mEncoding;
/* array-length v10, v8 */
/* int-to-long v10, v10 */
(( com.android.server.pm.Encoding ) v9 ).writeUInt32 ( p1, v10, v11 ); // invoke-virtual {v9, p1, v10, v11}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 245 */
v9 = this.mEncoding;
(( com.android.server.pm.Encoding ) v9 ).writeUInt32 ( p1, v6, v7 ); // invoke-virtual {v9, p1, v6, v7}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 246 */
/* array-length v9, v8 */
/* int-to-long v9, v9 */
/* add-long/2addr v2, v9 */
/* .line 247 */
} // .end local v6 # "inflatedSize":J
} // .end local v8 # "compressed":[B
/* .line 248 */
} // :cond_0
v6 = this.mContents;
/* .line 250 */
v6 = this.mEncoding;
v7 = this.mContents;
/* array-length v7, v7 */
/* int-to-long v7, v7 */
(( com.android.server.pm.Encoding ) v6 ).writeUInt32 ( p1, v7, v8 ); // invoke-virtual {v6, p1, v7, v8}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 252 */
v6 = this.mEncoding;
/* const-wide/16 v7, 0x0 */
(( com.android.server.pm.Encoding ) v6 ).writeUInt32 ( p1, v7, v8 ); // invoke-virtual {v6, p1, v7, v8}, Lcom/android/server/pm/Encoding;->writeUInt32(Ljava/io/OutputStream;J)V
/* .line 253 */
v6 = this.mContents;
/* array-length v6, v6 */
/* int-to-long v6, v6 */
/* add-long/2addr v2, v6 */
/* .line 231 */
} // .end local v5 # "section":Lcom/android/server/pm/WritableFileSection;
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 257 */
} // .end local v4 # "i":I
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
v5 = } // :goto_2
/* if-ge v4, v5, :cond_2 */
/* .line 258 */
/* check-cast v5, [B */
(( java.io.OutputStream ) p1 ).write ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V
/* .line 257 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 260 */
} // .end local v4 # "i":I
} // :cond_2
return;
} // .end method
/* # virtual methods */
java.lang.Exception error ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "message" # Ljava/lang/String; */
/* .line 876 */
/* new-instance v0, Ljava/lang/Exception; */
/* invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V */
} // .end method
public com.android.server.pm.ProfileTranscoder read ( ) {
/* .locals 8 */
/* .line 78 */
final String v0 = "ProfileTranscode"; // const-string v0, "ProfileTranscode"
/* iget-boolean v1, p0, Lcom/android/server/pm/ProfileTranscoder;->mDeviceSupportTranscode:Z */
/* if-nez v1, :cond_0 */
/* .line 79 */
/* .line 82 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = this.mAssetManager;
final String v3 = "dexopt/baseline.prof"; // const-string v3, "dexopt/baseline.prof"
(( android.content.res.AssetManager ) v2 ).openFd ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .line 83 */
/* .local v2, "profile_fd":Landroid/content/res/AssetFileDescriptor; */
try { // :try_start_1
v3 = this.mAssetManager;
final String v4 = "dexopt/baseline.profm"; // const-string v4, "dexopt/baseline.profm"
(( android.content.res.AssetManager ) v3 ).openFd ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_6 */
/* .line 85 */
/* .local v3, "meta_fd":Landroid/content/res/AssetFileDescriptor; */
try { // :try_start_2
(( android.content.res.AssetFileDescriptor ) v2 ).createInputStream ( ); // invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* .line 86 */
/* .local v4, "is":Ljava/io/InputStream; */
try { // :try_start_3
v5 = com.android.server.pm.ProfileTranscoder.MAGIC_PROF;
(( com.android.server.pm.ProfileTranscoder ) p0 ).readHeader ( v4, v5 ); // invoke-virtual {p0, v4, v5}, Lcom/android/server/pm/ProfileTranscoder;->readHeader(Ljava/io/InputStream;[B)[B
/* .line 87 */
/* .local v5, "baselineVersion":[B */
v6 = this.mApkName;
(( com.android.server.pm.ProfileTranscoder ) p0 ).readProfile ( v4, v5, v6 ); // invoke-virtual {p0, v4, v5, v6}, Lcom/android/server/pm/ProfileTranscoder;->readProfile(Ljava/io/InputStream;[BLjava/lang/String;)[Lcom/android/server/pm/DexProfileData;
this.mProfile = v6;
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 88 */
} // .end local v5 # "baselineVersion":[B
if ( v4 != null) { // if-eqz v4, :cond_1
try { // :try_start_4
(( java.io.InputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/InputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* .line 91 */
} // .end local v4 # "is":Ljava/io/InputStream;
} // :cond_1
/* .line 85 */
/* .restart local v4 # "is":Ljava/io/InputStream; */
/* :catchall_0 */
/* move-exception v5 */
if ( v4 != null) { // if-eqz v4, :cond_2
try { // :try_start_5
(( java.io.InputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/InputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v6 */
try { // :try_start_6
(( java.lang.Throwable ) v5 ).addSuppressed ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor;
} // .end local v3 # "meta_fd":Landroid/content/res/AssetFileDescriptor;
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // :cond_2
} // :goto_0
/* throw v5 */
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_0 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* .line 81 */
} // .end local v4 # "is":Ljava/io/InputStream;
/* .restart local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor; */
/* .restart local v3 # "meta_fd":Landroid/content/res/AssetFileDescriptor; */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* :catchall_2 */
/* move-exception v4 */
/* .line 88 */
/* :catch_0 */
/* move-exception v4 */
/* .line 89 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_7
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.mPackageName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " read exception: "; // const-string v6, " read exception: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v5 );
/* .line 90 */
this.mProfile = v1;
/* .line 92 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_1
v4 = this.mProfile;
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_2 */
/* .line 93 */
/* .local v4, "profile":[Lcom/android/server/pm/DexProfileData; */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 94 */
try { // :try_start_8
(( android.content.res.AssetFileDescriptor ) v3 ).createInputStream ( ); // invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_1 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_2 */
/* .line 95 */
/* .local v5, "is":Ljava/io/InputStream; */
try { // :try_start_9
v6 = com.android.server.pm.ProfileTranscoder.MAGIC_PROFM;
(( com.android.server.pm.ProfileTranscoder ) p0 ).readHeader ( v5, v6 ); // invoke-virtual {p0, v5, v6}, Lcom/android/server/pm/ProfileTranscoder;->readHeader(Ljava/io/InputStream;[B)[B
/* .line 96 */
/* .local v6, "metaVersion":[B */
v7 = this.mDesiredVersion;
(( com.android.server.pm.ProfileTranscoder ) p0 ).readMeta ( v5, v6, v7, v4 ); // invoke-virtual {p0, v5, v6, v7, v4}, Lcom/android/server/pm/ProfileTranscoder;->readMeta(Ljava/io/InputStream;[B[B[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData;
this.mProfile = v7;
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_3 */
/* .line 97 */
} // .end local v6 # "metaVersion":[B
if ( v5 != null) { // if-eqz v5, :cond_3
try { // :try_start_a
(( java.io.InputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/InputStream;->close()V
/* :try_end_a */
/* .catch Ljava/lang/Exception; {:try_start_a ..:try_end_a} :catch_1 */
/* .catchall {:try_start_a ..:try_end_a} :catchall_2 */
/* .line 100 */
} // .end local v5 # "is":Ljava/io/InputStream;
} // :cond_3
/* .line 94 */
/* .restart local v5 # "is":Ljava/io/InputStream; */
/* :catchall_3 */
/* move-exception v6 */
if ( v5 != null) { // if-eqz v5, :cond_4
try { // :try_start_b
(( java.io.InputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/InputStream;->close()V
/* :try_end_b */
/* .catchall {:try_start_b ..:try_end_b} :catchall_4 */
/* :catchall_4 */
/* move-exception v7 */
try { // :try_start_c
(( java.lang.Throwable ) v6 ).addSuppressed ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor;
} // .end local v3 # "meta_fd":Landroid/content/res/AssetFileDescriptor;
} // .end local v4 # "profile":[Lcom/android/server/pm/DexProfileData;
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // :cond_4
} // :goto_2
/* throw v6 */
/* :try_end_c */
/* .catch Ljava/lang/Exception; {:try_start_c ..:try_end_c} :catch_1 */
/* .catchall {:try_start_c ..:try_end_c} :catchall_2 */
/* .line 97 */
} // .end local v5 # "is":Ljava/io/InputStream;
/* .restart local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor; */
/* .restart local v3 # "meta_fd":Landroid/content/res/AssetFileDescriptor; */
/* .restart local v4 # "profile":[Lcom/android/server/pm/DexProfileData; */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* :catch_1 */
/* move-exception v5 */
/* .line 98 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_d
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
v7 = this.mPackageName;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " read meta profile exception: "; // const-string v7, " read meta profile exception: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v6 );
/* .line 99 */
this.mProfile = v1;
/* :try_end_d */
/* .catchall {:try_start_d ..:try_end_d} :catchall_2 */
/* .line 102 */
} // .end local v4 # "profile":[Lcom/android/server/pm/DexProfileData;
} // .end local v5 # "e":Ljava/lang/Exception;
} // :cond_5
} // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_6
try { // :try_start_e
(( android.content.res.AssetFileDescriptor ) v3 ).close ( ); // invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->close()V
/* :try_end_e */
/* .catchall {:try_start_e ..:try_end_e} :catchall_6 */
} // .end local v3 # "meta_fd":Landroid/content/res/AssetFileDescriptor;
} // :cond_6
if ( v2 != null) { // if-eqz v2, :cond_7
try { // :try_start_f
(( android.content.res.AssetFileDescriptor ) v2 ).close ( ); // invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
/* :try_end_f */
/* .catch Ljava/lang/Exception; {:try_start_f ..:try_end_f} :catch_2 */
/* .line 105 */
} // .end local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor;
} // :cond_7
/* .line 81 */
/* .restart local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor; */
/* .restart local v3 # "meta_fd":Landroid/content/res/AssetFileDescriptor; */
} // :goto_4
if ( v3 != null) { // if-eqz v3, :cond_8
try { // :try_start_10
(( android.content.res.AssetFileDescriptor ) v3 ).close ( ); // invoke-virtual {v3}, Landroid/content/res/AssetFileDescriptor;->close()V
/* :try_end_10 */
/* .catchall {:try_start_10 ..:try_end_10} :catchall_5 */
/* :catchall_5 */
/* move-exception v5 */
try { // :try_start_11
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor;
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // :cond_8
} // :goto_5
/* throw v4 */
/* :try_end_11 */
/* .catchall {:try_start_11 ..:try_end_11} :catchall_6 */
} // .end local v3 # "meta_fd":Landroid/content/res/AssetFileDescriptor;
/* .restart local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor; */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* :catchall_6 */
/* move-exception v3 */
if ( v2 != null) { // if-eqz v2, :cond_9
try { // :try_start_12
(( android.content.res.AssetFileDescriptor ) v2 ).close ( ); // invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
/* :try_end_12 */
/* .catchall {:try_start_12 ..:try_end_12} :catchall_7 */
/* :catchall_7 */
/* move-exception v4 */
try { // :try_start_13
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // :cond_9
} // :goto_6
/* throw v3 */
/* :try_end_13 */
/* .catch Ljava/lang/Exception; {:try_start_13 ..:try_end_13} :catch_2 */
/* .line 102 */
} // .end local v2 # "profile_fd":Landroid/content/res/AssetFileDescriptor;
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* :catch_2 */
/* move-exception v2 */
/* .line 103 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.mPackageName;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " read profile exception: "; // const-string v4, " read profile exception: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v3 );
/* .line 104 */
this.mProfile = v1;
/* .line 106 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_7
} // .end method
 readHeader ( java.io.InputStream p0, Object[] p1 ) {
/* .locals 3 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "magic" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 167 */
v0 = this.mEncoding;
/* array-length v1, p2 */
(( com.android.server.pm.Encoding ) v0 ).read ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B
/* .line 168 */
/* .local v0, "fileMagic":[B */
v1 = java.util.Arrays .equals ( p2,v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 173 */
v1 = this.mEncoding;
v2 = com.android.server.pm.ProfileTranscoder.V010_P;
/* array-length v2, v2 */
(( com.android.server.pm.Encoding ) v1 ).read ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Lcom/android/server/pm/Encoding;->read(Ljava/io/InputStream;I)[B
/* .line 171 */
} // :cond_0
final String v1 = "Invalid magic"; // const-string v1, "Invalid magic"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v1 */
} // .end method
com.android.server.pm.DexProfileData readMeta ( java.io.InputStream p0, Object[] p1, Object[] p2, com.android.server.pm.DexProfileData[] p3 ) {
/* .locals 1 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "metadataVersion" # [B */
/* .param p3, "desiredProfileVersion" # [B */
/* .param p4, "profile" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 556 */
v0 = com.android.server.pm.ProfileTranscoder.METADATA_V001_N;
v0 = java.util.Arrays .equals ( p2,v0 );
/* if-nez v0, :cond_1 */
/* .line 560 */
v0 = com.android.server.pm.ProfileTranscoder.METADATA_V002;
v0 = java.util.Arrays .equals ( p2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 561 */
(( com.android.server.pm.ProfileTranscoder ) p0 ).readMetadataV002 ( p1, p3, p4 ); // invoke-virtual {p0, p1, p3, p4}, Lcom/android/server/pm/ProfileTranscoder;->readMetadataV002(Ljava/io/InputStream;[B[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData;
/* .line 563 */
} // :cond_0
final String v0 = "Unsupported meta version"; // const-string v0, "Unsupported meta version"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v0 */
/* .line 557 */
} // :cond_1
final String v0 = "Requires new Baseline Profile Metadata.Please rebuild the APK with Android Gradle Plugin 7.2 Canary 7 or higher"; // const-string v0, "Requires new Baseline Profile Metadata.Please rebuild the APK with Android Gradle Plugin 7.2 Canary 7 or higher"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v0 */
} // .end method
com.android.server.pm.DexProfileData readMetadataV002 ( java.io.InputStream p0, Object[] p1, com.android.server.pm.DexProfileData[] p2 ) {
/* .locals 9 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "desiredProfileVersion" # [B */
/* .param p3, "profile" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 581 */
v0 = this.mEncoding;
v0 = (( com.android.server.pm.Encoding ) v0 ).readUInt16 ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/Encoding;->readUInt16(Ljava/io/InputStream;)I
/* .line 583 */
/* .local v0, "dexFileCount":I */
v1 = this.mEncoding;
(( com.android.server.pm.Encoding ) v1 ).readUInt32 ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J
/* move-result-wide v1 */
/* .line 585 */
/* .local v1, "uncompressed":J */
v3 = this.mEncoding;
(( com.android.server.pm.Encoding ) v3 ).readUInt32 ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J
/* move-result-wide v3 */
/* .line 588 */
/* .local v3, "compressed":J */
v5 = this.mEncoding;
/* long-to-int v6, v3 */
/* long-to-int v7, v1 */
(( com.android.server.pm.Encoding ) v5 ).readCompressed ( p1, v6, v7 ); // invoke-virtual {v5, p1, v6, v7}, Lcom/android/server/pm/Encoding;->readCompressed(Ljava/io/InputStream;II)[B
/* .line 593 */
/* .local v5, "contents":[B */
v6 = (( java.io.InputStream ) p1 ).read ( ); // invoke-virtual {p1}, Ljava/io/InputStream;->read()I
/* if-gtz v6, :cond_0 */
/* .line 594 */
/* new-instance v6, Ljava/io/ByteArrayInputStream; */
/* invoke-direct {v6, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V */
/* .line 595 */
/* .local v6, "dataStream":Ljava/io/InputStream; */
try { // :try_start_0
/* invoke-direct {p0, v6, p2, v0, p3}, Lcom/android/server/pm/ProfileTranscoder;->readMetadataV002Body(Ljava/io/InputStream;[BI[Lcom/android/server/pm/DexProfileData;)[Lcom/android/server/pm/DexProfileData; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 601 */
(( java.io.InputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/InputStream;->close()V
/* .line 595 */
/* .line 594 */
/* :catchall_0 */
/* move-exception v7 */
try { // :try_start_1
(( java.io.InputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/InputStream;->close()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* :catchall_1 */
/* move-exception v8 */
(( java.lang.Throwable ) v7 ).addSuppressed ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :goto_0
/* throw v7 */
/* .line 593 */
} // .end local v6 # "dataStream":Ljava/io/InputStream;
} // :cond_0
final String v6 = "Content found after the end of file"; // const-string v6, "Content found after the end of file"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v6 ); // invoke-virtual {p0, v6}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v6 */
} // .end method
com.android.server.pm.DexProfileData readProfile ( java.io.InputStream p0, Object[] p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "version" # [B */
/* .param p3, "apkName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 531 */
v0 = com.android.server.pm.ProfileTranscoder.V010_P;
v0 = java.util.Arrays .equals ( p2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 534 */
v0 = this.mEncoding;
v0 = (( com.android.server.pm.Encoding ) v0 ).readUInt8 ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/Encoding;->readUInt8(Ljava/io/InputStream;)I
/* .line 535 */
/* .local v0, "numberOfDexFiles":I */
v1 = this.mEncoding;
(( com.android.server.pm.Encoding ) v1 ).readUInt32 ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J
/* move-result-wide v1 */
/* .line 536 */
/* .local v1, "uncompressedDataSize":J */
v3 = this.mEncoding;
(( com.android.server.pm.Encoding ) v3 ).readUInt32 ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/pm/Encoding;->readUInt32(Ljava/io/InputStream;)J
/* move-result-wide v3 */
/* .line 539 */
/* .local v3, "compressedDataSize":J */
v5 = this.mEncoding;
/* long-to-int v6, v3 */
/* long-to-int v7, v1 */
(( com.android.server.pm.Encoding ) v5 ).readCompressed ( p1, v6, v7 ); // invoke-virtual {v5, p1, v6, v7}, Lcom/android/server/pm/Encoding;->readCompressed(Ljava/io/InputStream;II)[B
/* .line 544 */
/* .local v5, "uncompressedData":[B */
v6 = (( java.io.InputStream ) p1 ).read ( ); // invoke-virtual {p1}, Ljava/io/InputStream;->read()I
/* if-gtz v6, :cond_0 */
/* .line 545 */
/* new-instance v6, Ljava/io/ByteArrayInputStream; */
/* invoke-direct {v6, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V */
/* .line 546 */
/* .local v6, "dataStream":Ljava/io/InputStream; */
try { // :try_start_0
/* invoke-direct {p0, v6, p3, v0}, Lcom/android/server/pm/ProfileTranscoder;->readUncompressedBody(Ljava/io/InputStream;Ljava/lang/String;I)[Lcom/android/server/pm/DexProfileData; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 547 */
(( java.io.InputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/InputStream;->close()V
/* .line 546 */
/* .line 545 */
/* :catchall_0 */
/* move-exception v7 */
try { // :try_start_1
(( java.io.InputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/InputStream;->close()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* :catchall_1 */
/* move-exception v8 */
(( java.lang.Throwable ) v7 ).addSuppressed ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :goto_0
/* throw v7 */
/* .line 544 */
} // .end local v6 # "dataStream":Ljava/io/InputStream;
} // :cond_0
final String v6 = "Content found after the end of file"; // const-string v6, "Content found after the end of file"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v6 ); // invoke-virtual {p0, v6}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v6 */
/* .line 532 */
} // .end local v0 # "numberOfDexFiles":I
} // .end local v1 # "uncompressedDataSize":J
} // .end local v3 # "compressedDataSize":J
} // .end local v5 # "uncompressedData":[B
} // :cond_1
final String v0 = "Unsupported version"; // const-string v0, "Unsupported version"
(( com.android.server.pm.ProfileTranscoder ) p0 ).error ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/ProfileTranscoder;->error(Ljava/lang/String;)Ljava/lang/Exception;
/* throw v0 */
} // .end method
Boolean transcodeAndWriteBody ( java.io.OutputStream p0, Object[] p1, com.android.server.pm.DexProfileData[] p2 ) {
/* .locals 1 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "desiredVersion" # [B */
/* .param p3, "data" # [Lcom/android/server/pm/DexProfileData; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 195 */
v0 = com.android.server.pm.ProfileTranscoder.V015_S;
v0 = java.util.Arrays .equals ( p2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 196 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/pm/ProfileTranscoder;->writeProfileForS(Ljava/io/OutputStream;[Lcom/android/server/pm/DexProfileData;)V */
/* .line 197 */
int v0 = 1; // const/4 v0, 0x1
/* .line 199 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public com.android.server.pm.ProfileTranscoder transcodeIfNeeded ( ) {
/* .locals 6 */
/* .line 110 */
v0 = this.mProfile;
/* .line 111 */
/* .local v0, "profile":[Lcom/android/server/pm/DexProfileData; */
v1 = this.mDesiredVersion;
/* .line 112 */
/* .local v1, "desiredVersion":[B */
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-nez v1, :cond_0 */
/* .line 115 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
/* new-instance v3, Ljava/io/ByteArrayOutputStream; */
/* invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 116 */
/* .local v3, "os":Ljava/io/ByteArrayOutputStream; */
try { // :try_start_1
(( com.android.server.pm.ProfileTranscoder ) p0 ).writeHeader ( v3, v1 ); // invoke-virtual {p0, v3, v1}, Lcom/android/server/pm/ProfileTranscoder;->writeHeader(Ljava/io/OutputStream;[B)V
/* .line 117 */
v4 = (( com.android.server.pm.ProfileTranscoder ) p0 ).transcodeAndWriteBody ( v3, v1, v0 ); // invoke-virtual {p0, v3, v1, v0}, Lcom/android/server/pm/ProfileTranscoder;->transcodeAndWriteBody(Ljava/io/OutputStream;[B[Lcom/android/server/pm/DexProfileData;)Z
/* .line 122 */
/* .local v4, "success":Z */
/* if-nez v4, :cond_1 */
/* .line 123 */
this.mProfile = v2;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 124 */
/* nop */
/* .line 127 */
try { // :try_start_2
(( java.io.ByteArrayOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 124 */
/* .line 126 */
} // :cond_1
try { // :try_start_3
(( java.io.ByteArrayOutputStream ) v3 ).toByteArray ( ); // invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
this.mTranscodedProfile = v5;
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 127 */
} // .end local v4 # "success":Z
try { // :try_start_4
(( java.io.ByteArrayOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 130 */
} // .end local v3 # "os":Ljava/io/ByteArrayOutputStream;
/* .line 115 */
/* .restart local v3 # "os":Ljava/io/ByteArrayOutputStream; */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_5
(( java.io.ByteArrayOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_6
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "profile":[Lcom/android/server/pm/DexProfileData;
} // .end local v1 # "desiredVersion":[B
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // :goto_0
/* throw v4 */
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_0 */
/* .line 127 */
} // .end local v3 # "os":Ljava/io/ByteArrayOutputStream;
/* .restart local v0 # "profile":[Lcom/android/server/pm/DexProfileData; */
/* .restart local v1 # "desiredVersion":[B */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* :catch_0 */
/* move-exception v3 */
/* .line 128 */
/* .local v3, "e":Ljava/lang/Exception; */
this.mTranscodedProfile = v2;
/* .line 129 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
v5 = this.mPackageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " transcodeIfNeeded Exception:"; // const-string v5, " transcodeIfNeeded Exception:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "ProfileTranscode"; // const-string v5, "ProfileTranscode"
android.util.Log .d ( v5,v4 );
/* .line 131 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_1
this.mProfile = v2;
/* .line 132 */
/* .line 113 */
} // :cond_2
} // :goto_2
} // .end method
public Boolean write ( ) {
/* .locals 7 */
/* .line 136 */
v0 = this.mTranscodedProfile;
/* .line 137 */
/* .local v0, "transcodedProfile":[B */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 138 */
/* .line 141 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
/* new-instance v3, Ljava/io/ByteArrayInputStream; */
/* invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_4 */
/* .line 142 */
/* .local v3, "bis":Ljava/io/InputStream; */
try { // :try_start_1
/* new-instance v4, Ljava/io/FileOutputStream; */
v5 = this.mTarget;
/* invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 144 */
/* .local v4, "os":Ljava/io/OutputStream; */
try { // :try_start_2
v5 = this.mEncoding;
(( com.android.server.pm.Encoding ) v5 ).writeAll ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Lcom/android/server/pm/Encoding;->writeAll(Ljava/io/InputStream;Ljava/io/OutputStream;)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 145 */
/* nop */
/* .line 146 */
try { // :try_start_3
(( java.io.OutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
try { // :try_start_4
(( java.io.InputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_4 */
/* .line 149 */
this.mTranscodedProfile = v2;
/* .line 150 */
this.mProfile = v2;
/* .line 145 */
int v1 = 1; // const/4 v1, 0x1
/* .line 140 */
/* :catchall_0 */
/* move-exception v5 */
try { // :try_start_5
(( java.io.OutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v6 */
try { // :try_start_6
(( java.lang.Throwable ) v5 ).addSuppressed ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "transcodedProfile":[B
} // .end local v3 # "bis":Ljava/io/InputStream;
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // :goto_0
/* throw v5 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
} // .end local v4 # "os":Ljava/io/OutputStream;
/* .restart local v0 # "transcodedProfile":[B */
/* .restart local v3 # "bis":Ljava/io/InputStream; */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* :catchall_2 */
/* move-exception v4 */
try { // :try_start_7
(( java.io.InputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/InputStream;->close()V
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* :catchall_3 */
/* move-exception v5 */
try { // :try_start_8
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "transcodedProfile":[B
} // .end local p0 # "this":Lcom/android/server/pm/ProfileTranscoder;
} // :goto_1
/* throw v4 */
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_0 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_4 */
/* .line 149 */
} // .end local v3 # "bis":Ljava/io/InputStream;
/* .restart local v0 # "transcodedProfile":[B */
/* .restart local p0 # "this":Lcom/android/server/pm/ProfileTranscoder; */
/* :catchall_4 */
/* move-exception v1 */
/* .line 146 */
/* :catch_0 */
/* move-exception v3 */
/* .line 147 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_9
final String v4 = "ProfileTranscode"; // const-string v4, "ProfileTranscode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = this.mPackageName;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " write Exception: "; // const-string v6, " write Exception: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v5 );
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_4 */
/* .line 149 */
/* nop */
} // .end local v3 # "e":Ljava/lang/Exception;
this.mTranscodedProfile = v2;
/* .line 150 */
this.mProfile = v2;
/* .line 151 */
/* nop */
/* .line 152 */
/* .line 149 */
} // :goto_2
this.mTranscodedProfile = v2;
/* .line 150 */
this.mProfile = v2;
/* .line 151 */
/* throw v1 */
} // .end method
void writeHeader ( java.io.OutputStream p0, Object[] p1 ) {
/* .locals 1 */
/* .param p1, "os" # Ljava/io/OutputStream; */
/* .param p2, "version" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 177 */
v0 = com.android.server.pm.ProfileTranscoder.MAGIC_PROF;
(( java.io.OutputStream ) p1 ).write ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V
/* .line 178 */
(( java.io.OutputStream ) p1 ).write ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write([B)V
/* .line 179 */
return;
} // .end method
