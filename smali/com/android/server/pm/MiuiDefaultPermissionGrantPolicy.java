public class com.android.server.pm.MiuiDefaultPermissionGrantPolicy extends com.android.server.pm.permission.DefaultPermissionGrantPolicyStub {
	 /* .source "MiuiDefaultPermissionGrantPolicy.java" */
	 /* # static fields */
	 private static final Integer DEFAULT_PACKAGE_INFO_QUERY_FLAGS;
	 private static final java.lang.String GRANT_RUNTIME_VERSION;
	 private static final java.lang.String INCALL_UI;
	 private static final java.lang.String MIUI_GLOBAL_APPS;
	 public static final java.lang.String MIUI_SYSTEM_APPS;
	 private static final java.lang.String REQUIRED_PERMISSIONS;
	 private static final java.util.Set RUNTIME_PERMISSIONS;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.lang.String RUNTIME_PERMSSION_PROPTERY;
private static final Integer STATE_DEF;
private static final Integer STATE_GRANT;
private static final Integer STATE_REVOKE;
private static final java.lang.String TAG;
public static final java.lang.String sAllowAutoStartForOTAPkgs;
private static final android.util.ArrayMap sMiuiAppDefaultGrantedPermissions;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.pm.MiuiDefaultPermissionGrantPolicy ( ) {
/* .locals 3 */
/* .line 62 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 63 */
final String v0 = "com.xiaomi.finddevice"; // const-string v0, "com.xiaomi.finddevice"
/* filled-new-array {v0}, [Ljava/lang/String; */
/* .line 83 */
v1 = miui.content.pm.ExtraPackageManager.MIUI_SYSTEM_APPS;
/* .line 85 */
final String v1 = "co.sitic.pp"; // const-string v1, "co.sitic.pp"
final String v2 = "com.miui.backup"; // const-string v2, "com.miui.backup"
/* filled-new-array {v1, v2, v0}, [Ljava/lang/String; */
/* .line 91 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
return;
} // .end method
public com.android.server.pm.MiuiDefaultPermissionGrantPolicy ( ) {
/* .locals 0 */
/* .line 40 */
/* invoke-direct {p0}, Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;-><init>()V */
return;
} // .end method
private static Boolean doesPackageSupportRuntimePermissions ( android.content.pm.PackageInfo p0 ) {
/* .locals 2 */
/* .param p0, "pkg" # Landroid/content/pm/PackageInfo; */
/* .line 159 */
v0 = this.applicationInfo;
/* iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I */
/* const/16 v1, 0x16 */
/* if-le v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static synchronized void ensureDangerousSetInit ( ) {
/* .locals 7 */
/* const-class v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy; */
/* monitor-enter v0 */
/* .line 73 */
try { // :try_start_0
v1 = v1 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.RUNTIME_PERMISSIONS;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-lez v1, :cond_0 */
/* .line 74 */
/* monitor-exit v0 */
return;
/* .line 76 */
} // :cond_0
try { // :try_start_1
/* const-class v1, Lcom/android/server/pm/permission/PermissionManagerServiceInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/pm/permission/PermissionManagerServiceInternal; */
/* .line 77 */
/* .local v1, "pm":Lcom/android/server/pm/permission/PermissionManagerServiceInternal; */
int v2 = 1; // const/4 v2, 0x1
/* .line 78 */
/* .local v2, "dangerous":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PermissionInfo;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Landroid/content/pm/PermissionInfo; */
/* .line 79 */
/* .local v4, "permissionInfo":Landroid/content/pm/PermissionInfo; */
v5 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.RUNTIME_PERMISSIONS;
v6 = this.name;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 80 */
/* nop */
} // .end local v4 # "permissionInfo":Landroid/content/pm/PermissionInfo;
/* .line 81 */
} // :cond_1
/* monitor-exit v0 */
return;
/* .line 72 */
} // .end local v1 # "pm":Lcom/android/server/pm/permission/PermissionManagerServiceInternal;
} // .end local v2 # "dangerous":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PermissionInfo;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
private static void grantIncallUiPermission ( com.android.server.pm.PackageManagerService p0, Integer p1 ) {
/* .locals 7 */
/* .param p0, "service" # Lcom/android/server/pm/PackageManagerService; */
/* .param p1, "userId" # I */
/* .line 302 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 303 */
/* .local v0, "perms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
final String v1 = "android.permission.READ_CONTACTS"; // const-string v1, "android.permission.READ_CONTACTS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 304 */
final String v1 = "android.permission.POST_NOTIFICATIONS"; // const-string v1, "android.permission.POST_NOTIFICATIONS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 306 */
android.app.AppGlobals .getPermissionManager ( );
/* check-cast v1, Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 307 */
/* .local v1, "permissionService":Lcom/android/server/pm/permission/PermissionManagerService; */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/String; */
/* .line 308 */
/* .local v3, "p":Ljava/lang/String; */
final String v4 = "com.android.incallui"; // const-string v4, "com.android.incallui"
v5 = (( com.android.server.pm.PackageManagerService ) p0 ).checkPermission ( v3, v4, p1 ); // invoke-virtual {p0, v3, v4, p1}, Lcom/android/server/pm/PackageManagerService;->checkPermission(Ljava/lang/String;Ljava/lang/String;I)I
/* .line 309 */
/* .local v5, "result":I */
int v6 = -1; // const/4 v6, -0x1
/* if-ne v5, v6, :cond_0 */
/* .line 310 */
(( com.android.server.pm.permission.PermissionManagerService ) v1 ).grantRuntimePermission ( v4, v3, p1 ); // invoke-virtual {v1, v4, v3, p1}, Lcom/android/server/pm/permission/PermissionManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V
/* .line 312 */
} // .end local v3 # "p":Ljava/lang/String;
} // .end local v5 # "result":I
} // :cond_0
/* .line 313 */
} // :cond_1
return;
} // .end method
private static void grantPermissionsForCTS ( Integer p0 ) {
/* .locals 11 */
/* .param p0, "userId" # I */
/* .line 100 */
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 101 */
/* .local v0, "miuiCustomizedRegion":Ljava/lang/String; */
final String v1 = "lm_cr"; // const-string v1, "lm_cr"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
final String v1 = "mx_telcel"; // const-string v1, "mx_telcel"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 102 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* .line 103 */
/* .local v1, "newFlag":I */
final String v9 = "co.sitic.pp"; // const-string v9, "co.sitic.pp"
/* .line 104 */
/* .local v9, "packageName":Ljava/lang/String; */
android.app.AppGlobals .getPermissionManager ( );
/* move-object v10, v2 */
/* check-cast v10, Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 105 */
/* .local v10, "permissionService":Lcom/android/server/pm/permission/PermissionManagerService; */
final String v4 = "android.permission.READ_PHONE_STATE"; // const-string v4, "android.permission.READ_PHONE_STATE"
int v7 = 1; // const/4 v7, 0x1
/* move-object v2, v10 */
/* move-object v3, v9 */
/* move v5, v1 */
/* move v6, v1 */
/* move v8, p0 */
/* invoke-virtual/range {v2 ..v8}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V */
/* .line 106 */
final String v4 = "android.permission.RECEIVE_SMS"; // const-string v4, "android.permission.RECEIVE_SMS"
/* invoke-virtual/range {v2 ..v8}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V */
/* .line 107 */
final String v4 = "android.permission.CALL_PHONE"; // const-string v4, "android.permission.CALL_PHONE"
/* invoke-virtual/range {v2 ..v8}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V */
/* .line 108 */
final String v2 = "DefaultPermGrantPolicyI"; // const-string v2, "DefaultPermGrantPolicyI"
final String v3 = "grant permissions for Sysdll because of CTS"; // const-string v3, "grant permissions for Sysdll because of CTS"
android.util.Log .i ( v2,v3 );
/* .line 110 */
} // .end local v1 # "newFlag":I
} // .end local v9 # "packageName":Ljava/lang/String;
} // .end local v10 # "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
} // :cond_1
return;
} // .end method
public static void grantRuntimePermission ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "userId" # I */
/* .line 295 */
com.android.server.pm.PackageManagerServiceStub .get ( );
(( com.android.server.pm.PackageManagerServiceStub ) v0 ).getService ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;
/* .line 296 */
/* .local v0, "service":Lcom/android/server/pm/PackageManagerService; */
final String v1 = "com.android.incallui"; // const-string v1, "com.android.incallui"
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 297 */
com.android.server.pm.MiuiDefaultPermissionGrantPolicy .grantIncallUiPermission ( v0,p1 );
/* .line 299 */
} // :cond_0
return;
} // .end method
private static void grantRuntimePermissionsLPw ( com.android.server.pm.PackageManagerService p0, java.lang.String p1, Boolean p2, Integer p3 ) {
/* .locals 24 */
/* .param p0, "service" # Lcom/android/server/pm/PackageManagerService; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "overrideUserChoice" # Z */
/* .param p3, "userId" # I */
/* .line 163 */
/* move-object/from16 v0, p0 */
/* move/from16 v8, p3 */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer; */
/* const-wide/32 v2, 0x2000b080 */
/* move-object/from16 v9, p1 */
/* .line 164 */
/* .local v10, "pkg":Landroid/content/pm/PackageInfo; */
if ( v10 != null) { // if-eqz v10, :cond_17
v1 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy .doesPackageSupportRuntimePermissions ( v10 );
if ( v1 != null) { // if-eqz v1, :cond_17
v1 = this.requestedPermissions;
if ( v1 != null) { // if-eqz v1, :cond_17
v1 = this.requestedPermissions;
/* array-length v1, v1 */
/* if-nez v1, :cond_0 */
/* goto/16 :goto_8 */
/* .line 167 */
} // :cond_0
android.app.AppGlobals .getPermissionManager ( );
/* move-object v11, v1 */
/* check-cast v11, Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 168 */
/* .local v11, "permissionService":Lcom/android/server/pm/permission/PermissionManagerService; */
v1 = this.requestedPermissions;
java.util.Arrays .asList ( v1 );
/* .line 169 */
/* .local v1, "requestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 170 */
/* .local v2, "requiredPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* move-object v12, v1 */
/* .line 171 */
/* .local v12, "allRequestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = this.applicationInfo;
v3 = this.metaData;
final String v4 = ";"; // const-string v4, ";"
final String v5 = "required_permissions"; // const-string v5, "required_permissions"
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 172 */
v3 = this.applicationInfo;
v3 = this.metaData;
(( android.os.Bundle ) v3 ).getString ( v5 ); // invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 173 */
/* .local v3, "declareStr":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v6, :cond_1 */
/* .line 174 */
(( java.lang.String ) v3 ).split ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v6 );
/* .line 177 */
} // .end local v3 # "declareStr":Ljava/lang/String;
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .line 179 */
/* .local v3, "grantablePermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v6 = this.applicationInfo;
v6 = (( android.content.pm.ApplicationInfo ) v6 ).isUpdatedSystemApp ( ); // invoke-virtual {v6}, Landroid/content/pm/ApplicationInfo;->isUpdatedSystemApp()Z
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 180 */
v6 = this.mSettings;
v7 = this.packageName;
(( com.android.server.pm.Settings ) v6 ).getDisabledSystemPkgLPr ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/pm/Settings;->getDisabledSystemPkgLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
/* .line 181 */
/* .local v6, "sysPs":Lcom/android/server/pm/PackageSetting; */
if ( v6 != null) { // if-eqz v6, :cond_5
(( com.android.server.pm.PackageSetting ) v6 ).getPkg ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 182 */
v7 = (( com.android.server.pm.PackageSetting ) v6 ).getPkg ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 183 */
return;
/* .line 185 */
} // :cond_2
(( com.android.server.pm.PackageSetting ) v6 ).getPkg ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
/* .line 186 */
/* .local v7, "disablePermissionsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 187 */
(( com.android.server.pm.PackageSetting ) v6 ).getPkg ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
if ( v13 != null) { // if-eqz v13, :cond_3
/* .line 188 */
(( com.android.server.pm.PackageSetting ) v6 ).getPkg ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
(( android.os.Bundle ) v13 ).getString ( v5 ); // invoke-virtual {v13, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 189 */
/* .local v5, "disableStr":Ljava/lang/String; */
v13 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v13, :cond_3 */
/* .line 190 */
(( java.lang.String ) v5 ).split ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v4 );
/* .line 193 */
} // .end local v5 # "disableStr":Ljava/lang/String;
v4 = } // :cond_3
/* if-nez v4, :cond_4 */
/* .line 194 */
/* new-instance v4, Landroid/util/ArraySet; */
/* invoke-direct {v4, v1}, Landroid/util/ArraySet;-><init>(Ljava/util/Collection;)V */
/* move-object v3, v4 */
/* .line 195 */
/* move-object v1, v7 */
/* move-object v13, v1 */
/* move-object v14, v2 */
/* move-object v15, v3 */
/* .line 193 */
} // :cond_4
/* move-object v13, v1 */
/* move-object v14, v2 */
/* move-object v15, v3 */
/* .line 200 */
} // .end local v6 # "sysPs":Lcom/android/server/pm/PackageSetting;
} // .end local v7 # "disablePermissionsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_5
/* move-object v13, v1 */
/* move-object v14, v2 */
/* move-object v15, v3 */
} // .end local v1 # "requestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v2 # "requiredPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v3 # "grantablePermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .local v13, "requestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local v14, "requiredPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local v15, "grantablePermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v7 = } // :goto_0
/* .line 201 */
/* .local v7, "grantablePermissionCount":I */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object v6, v1 */
/* .line 202 */
/* .local v6, "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
/* move v5, v1 */
/* .local v5, "i":I */
} // :goto_1
/* if-ge v5, v7, :cond_15 */
/* .line 203 */
/* move-object v4, v1 */
/* check-cast v4, Ljava/lang/String; */
/* .line 207 */
/* .local v4, "permission":Ljava/lang/String; */
v1 = if ( v15 != null) { // if-eqz v15, :cond_6
/* if-nez v1, :cond_6 */
/* .line 208 */
/* move/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move/from16 v17, v7 */
/* goto/16 :goto_6 */
/* .line 211 */
} // :cond_6
v1 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy .isDangerousPermission ( v4 );
v1 = if ( v1 != null) { // if-eqz v1, :cond_13
if ( v1 != null) { // if-eqz v1, :cond_13
/* .line 212 */
v1 = this.packageName;
v3 = (( com.android.server.pm.permission.PermissionManagerService ) v11 ).getPermissionFlags ( v4, v1, v8 ); // invoke-virtual {v11, v4, v1, v8}, Lcom/android/server/pm/permission/PermissionManagerService;->getPermissionFlags(Ljava/lang/String;Ljava/lang/String;I)I
/* .line 213 */
/* .local v3, "flags":I */
v1 = if ( v14 != null) { // if-eqz v14, :cond_8
/* if-nez v1, :cond_8 */
/* .line 214 */
/* and-int/lit8 v1, v3, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 215 */
/* const/16 v16, 0x1032 */
/* .line 217 */
/* .local v16, "revokeFlags":I */
v2 = this.packageName;
/* const/16 v17, 0x0 */
/* const/16 v18, 0x1 */
/* move-object v1, v11 */
/* move/from16 v19, v3 */
} // .end local v3 # "flags":I
/* .local v19, "flags":I */
/* move-object v3, v4 */
/* move-object/from16 v20, v4 */
} // .end local v4 # "permission":Ljava/lang/String;
/* .local v20, "permission":Ljava/lang/String; */
/* move/from16 v4, v16 */
/* move/from16 v21, v5 */
} // .end local v5 # "i":I
/* .local v21, "i":I */
/* move/from16 v5, v17 */
/* move-object/from16 v22, v6 */
} // .end local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .local v22, "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* move/from16 v6, v18 */
/* move/from16 v17, v7 */
} // .end local v7 # "grantablePermissionCount":I
/* .local v17, "grantablePermissionCount":I */
/* move/from16 v7, p3 */
/* invoke-virtual/range {v1 ..v7}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V */
/* .line 219 */
} // .end local v16 # "revokeFlags":I
/* goto/16 :goto_6 */
/* .line 214 */
} // .end local v17 # "grantablePermissionCount":I
} // .end local v19 # "flags":I
} // .end local v20 # "permission":Ljava/lang/String;
} // .end local v21 # "i":I
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v3 # "flags":I */
/* .restart local v4 # "permission":Ljava/lang/String; */
/* .restart local v5 # "i":I */
/* .restart local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .restart local v7 # "grantablePermissionCount":I */
} // :cond_7
/* move/from16 v19, v3 */
/* move-object/from16 v20, v4 */
/* move/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move/from16 v17, v7 */
} // .end local v3 # "flags":I
} // .end local v4 # "permission":Ljava/lang/String;
} // .end local v5 # "i":I
} // .end local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v7 # "grantablePermissionCount":I
/* .restart local v17 # "grantablePermissionCount":I */
/* .restart local v19 # "flags":I */
/* .restart local v20 # "permission":Ljava/lang/String; */
/* .restart local v21 # "i":I */
/* .restart local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* goto/16 :goto_6 */
/* .line 213 */
} // .end local v17 # "grantablePermissionCount":I
} // .end local v19 # "flags":I
} // .end local v20 # "permission":Ljava/lang/String;
} // .end local v21 # "i":I
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v3 # "flags":I */
/* .restart local v4 # "permission":Ljava/lang/String; */
/* .restart local v5 # "i":I */
/* .restart local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .restart local v7 # "grantablePermissionCount":I */
} // :cond_8
/* move/from16 v19, v3 */
/* move-object/from16 v20, v4 */
/* move/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move/from16 v17, v7 */
/* .line 222 */
} // .end local v3 # "flags":I
} // .end local v4 # "permission":Ljava/lang/String;
} // .end local v5 # "i":I
} // .end local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v7 # "grantablePermissionCount":I
/* .restart local v17 # "grantablePermissionCount":I */
/* .restart local v19 # "flags":I */
/* .restart local v20 # "permission":Ljava/lang/String; */
/* .restart local v21 # "i":I */
/* .restart local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v1 = /* invoke-static/range {v19 ..v19}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->isUserChanged(I)Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* if-nez p2, :cond_9 */
v1 = /* invoke-static/range {v19 ..v19}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->isOTAUpdated(I)Z */
if ( v1 != null) { // if-eqz v1, :cond_14
/* .line 226 */
} // :cond_9
/* move/from16 v7, v19 */
} // .end local v19 # "flags":I
/* .local v7, "flags":I */
/* and-int/lit8 v1, v7, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 227 */
/* goto/16 :goto_6 */
/* .line 229 */
} // :cond_a
/* const/16 v16, 0x0 */
/* .line 230 */
/* .local v16, "updateSystemFixed":Z */
/* and-int/lit8 v1, v7, 0x10 */
int v6 = -1; // const/4 v6, -0x1
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 231 */
v1 = this.packageName;
/* move-object/from16 v5, v20 */
} // .end local v20 # "permission":Ljava/lang/String;
/* .local v5, "permission":Ljava/lang/String; */
v1 = (( com.android.server.pm.PackageManagerService ) v0 ).checkPermission ( v5, v1, v8 ); // invoke-virtual {v0, v5, v1, v8}, Lcom/android/server/pm/PackageManagerService;->checkPermission(Ljava/lang/String;Ljava/lang/String;I)I
/* if-ne v1, v6, :cond_b */
/* .line 232 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " with "; // const-string v2, " with "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " is required but is deny now, grant again"; // const-string v2, " is required but is deny now, grant again"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "DefaultPermGrantPolicyI"; // const-string v2, "DefaultPermGrantPolicyI"
android.util.Log .i ( v2,v1 );
/* .line 233 */
v2 = this.packageName;
/* const/16 v4, 0x10 */
/* const/16 v18, 0x0 */
/* const/16 v19, 0x1 */
/* move-object v1, v11 */
/* move-object v3, v5 */
/* move-object/from16 v23, v5 */
} // .end local v5 # "permission":Ljava/lang/String;
/* .local v23, "permission":Ljava/lang/String; */
/* move/from16 v5, v18 */
/* move/from16 v6, v19 */
/* move v9, v7 */
} // .end local v7 # "flags":I
/* .local v9, "flags":I */
/* move/from16 v7, p3 */
/* invoke-virtual/range {v1 ..v7}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V */
/* .line 236 */
/* const/16 v16, 0x1 */
/* .line 231 */
} // .end local v9 # "flags":I
} // .end local v23 # "permission":Ljava/lang/String;
/* .restart local v5 # "permission":Ljava/lang/String; */
/* .restart local v7 # "flags":I */
} // :cond_b
/* move-object/from16 v23, v5 */
/* move v9, v7 */
} // .end local v5 # "permission":Ljava/lang/String;
} // .end local v7 # "flags":I
/* .restart local v9 # "flags":I */
/* .restart local v23 # "permission":Ljava/lang/String; */
/* goto/16 :goto_6 */
/* .line 230 */
} // .end local v9 # "flags":I
} // .end local v23 # "permission":Ljava/lang/String;
/* .restart local v7 # "flags":I */
/* .restart local v20 # "permission":Ljava/lang/String; */
} // :cond_c
/* move v9, v7 */
/* move-object/from16 v23, v20 */
/* .line 242 */
} // .end local v7 # "flags":I
} // .end local v20 # "permission":Ljava/lang/String;
/* .restart local v9 # "flags":I */
/* .restart local v23 # "permission":Ljava/lang/String; */
} // :goto_2
/* if-nez v8, :cond_d */
v1 = this.packageName;
/* .line 243 */
/* move-object/from16 v7, v23 */
} // .end local v23 # "permission":Ljava/lang/String;
/* .local v7, "permission":Ljava/lang/String; */
v1 = (( com.android.server.pm.PackageManagerService ) v0 ).checkPermission ( v7, v1, v8 ); // invoke-virtual {v0, v7, v1, v8}, Lcom/android/server/pm/PackageManagerService;->checkPermission(Ljava/lang/String;Ljava/lang/String;I)I
int v2 = -1; // const/4 v2, -0x1
/* if-ne v1, v2, :cond_e */
v1 = this.applicationInfo;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I */
/* const/16 v2, 0x21 */
/* if-ge v1, v2, :cond_f */
/* .line 244 */
final String v1 = "android.permission.POST_NOTIFICATIONS"; // const-string v1, "android.permission.POST_NOTIFICATIONS"
v1 = (( java.lang.String ) v1 ).equals ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_f
/* .line 242 */
} // .end local v7 # "permission":Ljava/lang/String;
/* .restart local v23 # "permission":Ljava/lang/String; */
} // :cond_d
/* move-object/from16 v7, v23 */
/* .line 244 */
} // .end local v23 # "permission":Ljava/lang/String;
/* .restart local v7 # "permission":Ljava/lang/String; */
} // :cond_e
} // :goto_3
/* nop */
/* .line 245 */
final String v1 = "android.permission.ACCESS_COARSE_LOCATION"; // const-string v1, "android.permission.ACCESS_COARSE_LOCATION"
v1 = (( java.lang.String ) v1 ).equals ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_10
/* .line 246 */
} // :cond_f
/* move-object/from16 v6, v22 */
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( java.util.ArrayList ) v6 ).add ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 245 */
} // .end local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
} // :cond_10
/* move-object/from16 v6, v22 */
/* .line 248 */
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
} // :goto_4
/* const/16 v1, 0x20 */
/* .line 249 */
/* .local v1, "newFlags":I */
/* and-int/lit16 v2, v9, 0x3800 */
/* or-int v18, v1, v2 */
/* .line 251 */
} // .end local v1 # "newFlags":I
/* .local v18, "newFlags":I */
v1 = this.mContext;
(( android.content.Context ) v1 ).getOpPackageName ( ); // invoke-virtual {v1}, Landroid/content/Context;->getOpPackageName()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.pm.permission.PermissionManagerService ) v11 ).getPermissionInfo ( v7, v1, v2 ); // invoke-virtual {v11, v7, v1, v2}, Lcom/android/server/pm/permission/PermissionManagerService;->getPermissionInfo(Ljava/lang/String;Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;
v1 = (( android.content.pm.PermissionInfo ) v1 ).isRestricted ( ); // invoke-virtual {v1}, Landroid/content/pm/PermissionInfo;->isRestricted()Z
if ( v1 != null) { // if-eqz v1, :cond_11
/* .line 252 */
v2 = this.packageName;
/* const/16 v4, 0x1000 */
/* const/16 v5, 0x1000 */
/* const/16 v19, 0x1 */
/* move-object v1, v11 */
/* move-object v3, v7 */
/* move-object/from16 v22, v6 */
} // .end local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* move/from16 v6, v19 */
/* move-object v0, v7 */
} // .end local v7 # "permission":Ljava/lang/String;
/* .local v0, "permission":Ljava/lang/String; */
/* move/from16 v7, p3 */
/* invoke-virtual/range {v1 ..v7}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V */
/* .line 251 */
} // .end local v0 # "permission":Ljava/lang/String;
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .restart local v7 # "permission":Ljava/lang/String; */
} // :cond_11
/* move-object/from16 v22, v6 */
/* move-object v0, v7 */
/* .line 256 */
} // .end local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v7 # "permission":Ljava/lang/String;
/* .restart local v0 # "permission":Ljava/lang/String; */
/* .restart local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
} // :goto_5
v1 = this.packageName;
(( com.android.server.pm.permission.PermissionManagerService ) v11 ).grantRuntimePermission ( v1, v0, v8 ); // invoke-virtual {v11, v1, v0, v8}, Lcom/android/server/pm/permission/PermissionManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V
/* .line 258 */
if ( v16 != null) { // if-eqz v16, :cond_12
/* .line 259 */
/* or-int/lit8 v18, v18, 0x10 */
/* .line 262 */
} // :cond_12
v2 = this.packageName;
int v6 = 1; // const/4 v6, 0x1
/* move-object v1, v11 */
/* move-object v3, v0 */
/* move/from16 v4, v18 */
/* move/from16 v5, v18 */
/* move/from16 v7, p3 */
/* invoke-virtual/range {v1 ..v7}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V */
/* .line 211 */
} // .end local v0 # "permission":Ljava/lang/String;
} // .end local v9 # "flags":I
} // .end local v16 # "updateSystemFixed":Z
} // .end local v17 # "grantablePermissionCount":I
} // .end local v18 # "newFlags":I
} // .end local v21 # "i":I
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v4 # "permission":Ljava/lang/String; */
/* .local v5, "i":I */
/* .restart local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .local v7, "grantablePermissionCount":I */
} // :cond_13
/* move-object v0, v4 */
/* move/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move/from16 v17, v7 */
/* .line 202 */
} // .end local v4 # "permission":Ljava/lang/String;
} // .end local v5 # "i":I
} // .end local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v7 # "grantablePermissionCount":I
/* .restart local v17 # "grantablePermissionCount":I */
/* .restart local v21 # "i":I */
/* .restart local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
} // :cond_14
} // :goto_6
/* add-int/lit8 v5, v21, 0x1 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v9, p1 */
/* move/from16 v7, v17 */
/* move-object/from16 v6, v22 */
} // .end local v21 # "i":I
/* .restart local v5 # "i":I */
/* goto/16 :goto_1 */
} // .end local v17 # "grantablePermissionCount":I
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .restart local v7 # "grantablePermissionCount":I */
} // :cond_15
/* move/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move/from16 v17, v7 */
/* .line 268 */
} // .end local v5 # "i":I
} // .end local v6 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v7 # "grantablePermissionCount":I
/* .restart local v17 # "grantablePermissionCount":I */
/* .restart local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* if-nez v8, :cond_16 */
v0 = /* invoke-virtual/range {v22 ..v22}, Ljava/util/ArrayList;->isEmpty()Z */
/* if-nez v0, :cond_16 */
/* .line 269 */
v0 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.sMiuiAppDefaultGrantedPermissions;
v1 = this.packageName;
/* move-object/from16 v2, v22 */
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .local v2, "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.util.ArrayMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 268 */
} // .end local v2 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
} // :cond_16
/* move-object/from16 v2, v22 */
/* .line 271 */
} // .end local v22 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .restart local v2 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
} // :goto_7
return;
/* .line 165 */
} // .end local v2 # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v11 # "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
} // .end local v12 # "allRequestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v13 # "requestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v14 # "requiredPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v15 # "grantablePermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local v17 # "grantablePermissionCount":I
} // :cond_17
} // :goto_8
return;
} // .end method
private static Boolean isAdaptedRequiredPermissions ( android.content.pm.PackageInfo p0 ) {
/* .locals 2 */
/* .param p0, "pi" # Landroid/content/pm/PackageInfo; */
/* .line 409 */
if ( p0 != null) { // if-eqz p0, :cond_1
v0 = this.applicationInfo;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.applicationInfo;
v0 = this.metaData;
/* if-nez v0, :cond_0 */
/* .line 412 */
} // :cond_0
v0 = this.applicationInfo;
v0 = this.metaData;
final String v1 = "required_permissions"; // const-string v1, "required_permissions"
(( android.os.Bundle ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 413 */
/* .local v0, "permission":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* xor-int/lit8 v1, v1, 0x1 */
/* .line 410 */
} // .end local v0 # "permission":Ljava/lang/String;
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static Boolean isDangerousPermission ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "permission" # Ljava/lang/String; */
/* .line 68 */
com.android.server.pm.MiuiDefaultPermissionGrantPolicy .ensureDangerousSetInit ( );
/* .line 69 */
v0 = v0 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.RUNTIME_PERMISSIONS;
} // .end method
private static Boolean isOTAUpdated ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "flags" # I */
/* .line 278 */
/* and-int/lit8 v0, p0, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* and-int/lit8 v0, p0, 0x20 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isUserChanged ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "flags" # I */
/* .line 274 */
/* and-int/lit8 v0, p0, 0x3 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static void realGrantDefaultPermissions ( com.android.server.pm.PackageManagerService p0, Integer p1 ) {
/* .locals 12 */
/* .param p0, "service" # Lcom/android/server/pm/PackageManagerService; */
/* .param p1, "userId" # I */
/* .line 144 */
v0 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.MIUI_SYSTEM_APPS;
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_0 */
/* aget-object v4, v0, v3 */
/* .line 145 */
/* .local v4, "miuiSystemApp":Ljava/lang/String; */
int v5 = 1; // const/4 v5, 0x1
com.android.server.pm.MiuiDefaultPermissionGrantPolicy .grantRuntimePermissionsLPw ( p0,v4,v5,p1 );
/* .line 144 */
} // .end local v4 # "miuiSystemApp":Ljava/lang/String;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 147 */
} // :cond_0
/* const-class v0, Landroid/app/AppOpsManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/app/AppOpsManagerInternal; */
/* .line 149 */
/* .local v0, "appOpsManagerInternal":Landroid/app/AppOpsManagerInternal; */
v1 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.sAllowAutoStartForOTAPkgs;
/* array-length v9, v1 */
} // :goto_1
/* if-ge v2, v9, :cond_2 */
/* aget-object v10, v1, v2 */
/* .line 150 */
/* .local v10, "sAllowAutoStartForOTAPkg":Ljava/lang/String; */
(( com.android.server.pm.PackageManagerService ) p0 ).snapshotComputer ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* const-wide/32 v4, 0x2000b080 */
/* .line 151 */
/* .local v11, "pkg":Landroid/content/pm/PackageInfo; */
if ( v11 != null) { // if-eqz v11, :cond_1
/* .line 152 */
/* const/16 v4, 0x2718 */
v3 = this.applicationInfo;
/* iget v5, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v0 */
/* move-object v6, v10 */
/* invoke-virtual/range {v3 ..v8}, Landroid/app/AppOpsManagerInternal;->setModeFromPermissionPolicy(IILjava/lang/String;ILcom/android/internal/app/IAppOpsCallback;)V */
/* .line 149 */
} // .end local v10 # "sAllowAutoStartForOTAPkg":Ljava/lang/String;
} // .end local v11 # "pkg":Landroid/content/pm/PackageInfo;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 156 */
} // :cond_2
return;
} // .end method
public static void setCoreRuntimePermissionEnabled ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p0, "grant" # Z */
/* .param p1, "flags" # I */
/* .param p2, "userId" # I */
/* .line 282 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 283 */
return;
/* .line 285 */
} // :cond_0
com.android.server.pm.PackageManagerServiceStub .get ( );
(( com.android.server.pm.PackageManagerServiceStub ) v0 ).getService ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;
/* .line 286 */
/* .local v0, "service":Lcom/android/server/pm/PackageManagerService; */
final String v1 = "persist.sys.runtime_perm"; // const-string v1, "persist.sys.runtime_perm"
if ( p0 != null) { // if-eqz p0, :cond_1
/* .line 287 */
com.android.server.pm.MiuiDefaultPermissionGrantPolicy .realGrantDefaultPermissions ( v0,p2 );
/* .line 288 */
int v2 = 0; // const/4 v2, 0x0
java.lang.String .valueOf ( v2 );
android.os.SystemProperties .set ( v1,v2 );
/* .line 290 */
} // :cond_1
int v2 = 1; // const/4 v2, 0x1
java.lang.String .valueOf ( v2 );
android.os.SystemProperties .set ( v1,v2 );
/* .line 292 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void grantDefaultPermissions ( Integer p0 ) {
/* .locals 10 */
/* .param p1, "userId" # I */
/* .line 114 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
final String v1 = "DefaultPermGrantPolicyI"; // const-string v1, "DefaultPermGrantPolicyI"
/* if-nez v0, :cond_4 */
final String v0 = "persist.sys.grant_version"; // const-string v0, "persist.sys.grant_version"
/* if-nez p1, :cond_0 */
v2 = android.os.Build$VERSION.INCREMENTAL;
/* .line 116 */
android.os.SystemProperties .get ( v0 );
/* .line 115 */
v2 = android.text.TextUtils .equals ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 124 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* .line 125 */
/* .local v2, "startTime":J */
com.android.server.pm.PackageManagerServiceStub .get ( );
(( com.android.server.pm.PackageManagerServiceStub ) v4 ).getService ( ); // invoke-virtual {v4}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;
/* .line 126 */
/* .local v4, "service":Lcom/android/server/pm/PackageManagerService; */
/* sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 127 */
v5 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.MIUI_GLOBAL_APPS;
/* array-length v6, v5 */
int v7 = 0; // const/4 v7, 0x0
/* move v8, v7 */
} // :goto_0
/* if-ge v8, v6, :cond_1 */
/* aget-object v9, v5, v8 */
/* .line 128 */
/* .local v9, "miuiGlobalApp":Ljava/lang/String; */
com.android.server.pm.MiuiDefaultPermissionGrantPolicy .grantRuntimePermissionsLPw ( v4,v9,v7,p1 );
/* .line 127 */
} // .end local v9 # "miuiGlobalApp":Ljava/lang/String;
/* add-int/lit8 v8, v8, 0x1 */
/* .line 130 */
} // :cond_1
final String v5 = "grant permissions for miui global apps"; // const-string v5, "grant permissions for miui global apps"
android.util.Log .i ( v1,v5 );
/* .line 132 */
final String v5 = "persist.sys.miui_optimization"; // const-string v5, "persist.sys.miui_optimization"
int v6 = 1; // const/4 v6, 0x1
v5 = android.os.SystemProperties .getBoolean ( v5,v6 );
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 133 */
final String v5 = "grant permissions for miui global apps: com.android.incallui"; // const-string v5, "grant permissions for miui global apps: com.android.incallui"
android.util.Log .i ( v1,v5 );
/* .line 134 */
com.android.server.pm.MiuiDefaultPermissionGrantPolicy .grantIncallUiPermission ( v4,p1 );
/* .line 137 */
} // :cond_2
com.android.server.pm.MiuiDefaultPermissionGrantPolicy .realGrantDefaultPermissions ( v4,p1 );
/* .line 139 */
} // :cond_3
} // :goto_1
v5 = android.os.Build$VERSION.INCREMENTAL;
android.os.SystemProperties .set ( v0,v5 );
/* .line 140 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "grantDefaultPermissions cost "; // const-string v5, "grantDefaultPermissions cost "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* sub-long/2addr v5, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = " ms"; // const-string v5, " ms"
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v0 );
/* .line 141 */
return;
/* .line 118 */
} // .end local v2 # "startTime":J
} // .end local v4 # "service":Lcom/android/server/pm/PackageManagerService;
} // :cond_4
} // :goto_2
final String v0 = "Don\'t need grant default permission to apps"; // const-string v0, "Don\'t need grant default permission to apps"
android.util.Log .i ( v1,v0 );
/* .line 119 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 120 */
com.android.server.pm.MiuiDefaultPermissionGrantPolicy .grantPermissionsForCTS ( p1 );
/* .line 122 */
} // :cond_5
return;
} // .end method
public void grantMiuiPackageInstallerPermssions ( ) {
/* .locals 7 */
/* .line 358 */
final String v0 = "com.miui.packageinstaller"; // const-string v0, "com.miui.packageinstaller"
/* .line 359 */
/* .local v0, "miuiPkgInstaller":Ljava/lang/String; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 360 */
/* .local v1, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v2 = "android.permission.READ_EXTERNAL_STORAGE"; // const-string v2, "android.permission.READ_EXTERNAL_STORAGE"
/* .line 361 */
final String v2 = "android.permission.WRITE_EXTERNAL_STORAGE"; // const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"
/* .line 362 */
final String v2 = "android.permission.READ_PHONE_STATE"; // const-string v2, "android.permission.READ_PHONE_STATE"
/* .line 364 */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/lang/String; */
/* .line 366 */
/* .local v3, "permItem":Ljava/lang/String; */
try { // :try_start_0
android.app.AppGlobals .getPermissionManager ( );
/* check-cast v4, Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 367 */
/* .local v4, "permissionService":Lcom/android/server/pm/permission/PermissionManagerService; */
final String v5 = "com.miui.packageinstaller"; // const-string v5, "com.miui.packageinstaller"
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.pm.permission.PermissionManagerService ) v4 ).grantRuntimePermission ( v5, v3, v6 ); // invoke-virtual {v4, v5, v3, v6}, Lcom/android/server/pm/permission/PermissionManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 370 */
} // .end local v4 # "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
/* .line 368 */
/* :catch_0 */
/* move-exception v4 */
/* .line 369 */
/* .local v4, "e":Ljava/lang/Exception; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "grantMiuiPackageInstallerPermssions error:"; // const-string v6, "grantMiuiPackageInstallerPermssions error:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "DefaultPermGrantPolicyI"; // const-string v6, "DefaultPermGrantPolicyI"
android.util.Log .d ( v6,v5 );
/* .line 371 */
} // .end local v3 # "permItem":Ljava/lang/String;
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_1
/* .line 372 */
} // :cond_0
return;
} // .end method
public Boolean isSpecialUidNeedDefaultGrant ( android.content.pm.PackageInfo p0 ) {
/* .locals 13 */
/* .param p1, "info" # Landroid/content/pm/PackageInfo; */
/* .line 376 */
v0 = this.applicationInfo;
/* iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x7d0 */
int v2 = 1; // const/4 v2, 0x1
/* if-le v0, v1, :cond_7 */
v0 = this.applicationInfo;
/* iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 377 */
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x2710 */
/* if-lt v0, v1, :cond_0 */
/* .line 380 */
} // :cond_0
com.android.server.pm.PackageManagerServiceStub .get ( );
(( com.android.server.pm.PackageManagerServiceStub ) v0 ).getService ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;
/* .line 381 */
/* .local v0, "service":Lcom/android/server/pm/PackageManagerService; */
v1 = this.applicationInfo;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
v1 = android.os.UserHandle .getUserId ( v1 );
/* .line 382 */
/* .local v1, "userId":I */
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
v4 = this.packageName;
/* const-wide/16 v5, 0x80 */
/* .line 383 */
/* .local v3, "metaInfo":Landroid/content/pm/PackageInfo; */
/* if-nez v3, :cond_1 */
/* .line 384 */
/* .line 386 */
} // :cond_1
v4 = this.sharedUserId;
v4 = android.text.TextUtils .isEmpty ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 387 */
v2 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy .isAdaptedRequiredPermissions ( v3 );
/* .line 389 */
} // :cond_2
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
v7 = this.applicationInfo;
/* iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 390 */
/* .local v4, "sharedPackages":[Ljava/lang/String; */
/* if-nez v4, :cond_3 */
/* .line 391 */
/* .line 393 */
} // :cond_3
/* array-length v7, v4 */
int v8 = 0; // const/4 v8, 0x0
/* move v9, v8 */
} // :goto_0
/* if-ge v9, v7, :cond_6 */
/* aget-object v10, v4, v9 */
/* .line 395 */
/* .local v10, "sharedPackage":Ljava/lang/String; */
v11 = this.packageName;
v11 = android.text.TextUtils .equals ( v11,v10 );
if ( v11 != null) { // if-eqz v11, :cond_4
/* .line 396 */
/* move-object v11, v3 */
/* .local v11, "sharedMetaInfo":Landroid/content/pm/PackageInfo; */
/* .line 398 */
} // .end local v11 # "sharedMetaInfo":Landroid/content/pm/PackageInfo;
} // :cond_4
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 400 */
/* .restart local v11 # "sharedMetaInfo":Landroid/content/pm/PackageInfo; */
} // :goto_1
v12 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy .isAdaptedRequiredPermissions ( v11 );
/* if-nez v12, :cond_5 */
/* .line 401 */
/* .line 393 */
} // .end local v10 # "sharedPackage":Ljava/lang/String;
} // .end local v11 # "sharedMetaInfo":Landroid/content/pm/PackageInfo;
} // :cond_5
/* add-int/lit8 v9, v9, 0x1 */
/* .line 404 */
} // :cond_6
/* .line 378 */
} // .end local v0 # "service":Lcom/android/server/pm/PackageManagerService;
} // .end local v1 # "userId":I
} // .end local v3 # "metaInfo":Landroid/content/pm/PackageInfo;
} // .end local v4 # "sharedPackages":[Ljava/lang/String;
} // :cond_7
} // :goto_2
} // .end method
public void miReadOutPermissionsInExt ( java.util.ArrayList p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/io/File;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 418 */
/* .local p1, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;" */
/* new-instance v0, Ljava/io/File; */
final String v1 = "mi_ext/product"; // const-string v1, "mi_ext/product"
final String v2 = "etc/default-permissions"; // const-string v2, "etc/default-permissions"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 419 */
/* .local v0, "dir":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).isDirectory ( ); // invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( java.io.File ) v0 ).canRead ( ); // invoke-virtual {v0}, Ljava/io/File;->canRead()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 420 */
(( java.io.File ) v0 ).listFiles ( ); // invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
java.util.Collections .addAll ( p1,v1 );
/* .line 422 */
} // :cond_0
return;
} // .end method
public void revokeAllPermssions ( ) {
/* .locals 13 */
/* .line 316 */
final String v0 = "com.miui.packageinstaller"; // const-string v0, "com.miui.packageinstaller"
v1 = android.miui.AppOpsUtils .isXOptMode ( );
/* if-nez v1, :cond_0 */
/* .line 317 */
return;
/* .line 319 */
} // :cond_0
final String v1 = "persist.sys.grant_version"; // const-string v1, "persist.sys.grant_version"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .set ( v1,v2 );
/* .line 321 */
try { // :try_start_0
android.app.AppGlobals .getPermissionManager ( );
/* check-cast v1, Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 322 */
/* .local v1, "permissionManagerService":Lcom/android/server/pm/permission/PermissionManagerService; */
v2 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.sMiuiAppDefaultGrantedPermissions;
(( android.util.ArrayMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
} // :cond_1
v3 = } // :goto_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
final String v4 = "DefaultPermGrantPolicyI"; // const-string v4, "DefaultPermGrantPolicyI"
final String v5 = "revokeMiuiOpt"; // const-string v5, "revokeMiuiOpt"
int v6 = 0; // const/4 v6, 0x0
if ( v3 != null) { // if-eqz v3, :cond_6
try { // :try_start_1
/* check-cast v3, Ljava/lang/String; */
/* .line 323 */
/* .local v3, "pkg":Ljava/lang/String; */
v7 = com.android.server.pm.MiuiDefaultPermissionGrantPolicy.sMiuiAppDefaultGrantedPermissions;
(( android.util.ArrayMap ) v7 ).get ( v3 ); // invoke-virtual {v7, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v7, Ljava/util/ArrayList; */
/* .line 324 */
/* .local v7, "permissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
final String v8 = "com.google.android.packageinstaller"; // const-string v8, "com.google.android.packageinstaller"
v8 = (( java.lang.String ) v8 ).equals ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v8, :cond_1 */
/* if-nez v7, :cond_2 */
/* .line 325 */
/* .line 327 */
} // :cond_2
(( java.util.ArrayList ) v7 ).iterator ( ); // invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :cond_3
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_5
/* check-cast v9, Ljava/lang/String; */
/* .line 328 */
/* .local v9, "p":Ljava/lang/String; */
final String v10 = "com.google.android.gms"; // const-string v10, "com.google.android.gms"
v10 = (( java.lang.String ) v10 ).equals ( v3 ); // invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_4
/* .line 329 */
final String v10 = "android.permission.RECORD_AUDIO"; // const-string v10, "android.permission.RECORD_AUDIO"
v10 = (( java.lang.String ) v10 ).equals ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v10, :cond_3 */
final String v10 = "android.permission.ACCESS_FINE_LOCATION"; // const-string v10, "android.permission.ACCESS_FINE_LOCATION"
/* .line 330 */
v10 = (( java.lang.String ) v10 ).equals ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
if ( v10 != null) { // if-eqz v10, :cond_4
/* .line 331 */
/* .line 335 */
} // :cond_4
try { // :try_start_2
(( com.android.server.pm.permission.PermissionManagerService ) v1 ).revokeRuntimePermission ( v3, v9, v6, v5 ); // invoke-virtual {v1, v3, v9, v6, v5}, Lcom/android/server/pm/permission/PermissionManagerService;->revokeRuntimePermission(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 338 */
/* .line 336 */
/* :catch_0 */
/* move-exception v10 */
/* .line 337 */
/* .local v10, "e":Ljava/lang/Exception; */
try { // :try_start_3
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "revokeAllPermssions error:"; // const-string v12, "revokeAllPermssions error:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v11 );
/* .line 339 */
} // .end local v9 # "p":Ljava/lang/String;
} // .end local v10 # "e":Ljava/lang/Exception;
} // :goto_2
/* .line 340 */
} // .end local v3 # "pkg":Ljava/lang/String;
} // .end local v7 # "permissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :cond_5
/* .line 341 */
} // :cond_6
/* move-object v2, v0 */
/* .line 342 */
/* .local v2, "miuiPkgInstaller":Ljava/lang/String; */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 343 */
/* .local v3, "permissionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
final String v7 = "android.permission.READ_EXTERNAL_STORAGE"; // const-string v7, "android.permission.READ_EXTERNAL_STORAGE"
(( java.util.ArrayList ) v3 ).add ( v7 ); // invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 344 */
final String v7 = "android.permission.WRITE_EXTERNAL_STORAGE"; // const-string v7, "android.permission.WRITE_EXTERNAL_STORAGE"
(( java.util.ArrayList ) v3 ).add ( v7 ); // invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 345 */
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v8 = } // :goto_3
if ( v8 != null) { // if-eqz v8, :cond_7
/* check-cast v8, Ljava/lang/String; */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 347 */
/* .local v8, "permItem":Ljava/lang/String; */
try { // :try_start_4
(( com.android.server.pm.permission.PermissionManagerService ) v1 ).revokeRuntimePermission ( v0, v8, v6, v5 ); // invoke-virtual {v1, v0, v8, v6, v5}, Lcom/android/server/pm/permission/PermissionManagerService;->revokeRuntimePermission(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_1 */
/* .line 350 */
/* .line 348 */
/* :catch_1 */
/* move-exception v9 */
/* .line 349 */
/* .local v9, "e":Ljava/lang/Exception; */
try { // :try_start_5
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "revokeRuntimePermissionInternal error:"; // const-string v11, "revokeRuntimePermissionInternal error:"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v4,v10 );
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_2 */
/* .line 351 */
} // .end local v8 # "permItem":Ljava/lang/String;
} // .end local v9 # "e":Ljava/lang/Exception;
} // :goto_4
/* .line 354 */
} // .end local v1 # "permissionManagerService":Lcom/android/server/pm/permission/PermissionManagerService;
} // .end local v2 # "miuiPkgInstaller":Ljava/lang/String;
} // .end local v3 # "permissionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :cond_7
/* .line 352 */
/* :catch_2 */
/* move-exception v0 */
/* .line 353 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 355 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_5
return;
} // .end method
