.class public Lcom/android/server/pm/PackageManagerServiceCompat;
.super Ljava/lang/Object;
.source "PackageManagerServiceCompat.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getWakePathComponents(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p0, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lmiui/security/WakePathComponent;",
            ">;"
        }
    .end annotation

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lmiui/security/WakePathComponent;>;"
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24
    return-object v0

    .line 27
    :cond_0
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceStub;->get()Lcom/android/server/pm/PackageManagerServiceStub;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;

    move-result-object v1

    .line 28
    .local v1, "service":Lcom/android/server/pm/PackageManagerService;
    iget-object v2, v1, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v2

    .line 29
    :try_start_0
    iget-object v3, v1, Lcom/android/server/pm/PackageManagerService;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v3, p0}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/pkg/AndroidPackage;

    .line 30
    .local v3, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    if-nez v3, :cond_1

    .line 31
    monitor-exit v2

    return-object v0

    .line 34
    :cond_1
    invoke-interface {v3}, Lcom/android/server/pm/pkg/AndroidPackage;->getActivities()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x3

    invoke-static {v0, v4, v5}, Lcom/android/server/pm/PackageManagerServiceCompat;->parsePkgCompentLock(Ljava/util/List;Ljava/util/List;I)V

    .line 35
    invoke-interface {v3}, Lcom/android/server/pm/pkg/AndroidPackage;->getReceivers()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v4, v5}, Lcom/android/server/pm/PackageManagerServiceCompat;->parsePkgCompentLock(Ljava/util/List;Ljava/util/List;I)V

    .line 36
    invoke-interface {v3}, Lcom/android/server/pm/pkg/AndroidPackage;->getProviders()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static {v0, v4, v5}, Lcom/android/server/pm/PackageManagerServiceCompat;->parsePkgCompentLock(Ljava/util/List;Ljava/util/List;I)V

    .line 37
    invoke-interface {v3}, Lcom/android/server/pm/pkg/AndroidPackage;->getServices()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v0, v4, v5}, Lcom/android/server/pm/PackageManagerServiceCompat;->parsePkgCompentLock(Ljava/util/List;Ljava/util/List;I)V

    .line 38
    .end local v3    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    monitor-exit v2

    .line 39
    return-object v0

    .line 38
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private static parsePkgCompentLock(Ljava/util/List;Ljava/util/List;I)V
    .locals 6
    .param p2, "componentType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiui/security/WakePathComponent;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/android/server/pm/pkg/component/ParsedMainComponent;",
            ">;I)V"
        }
    .end annotation

    .line 45
    .local p0, "wakePathComponents":Ljava/util/List;, "Ljava/util/List<Lmiui/security/WakePathComponent;>;"
    .local p1, "components":Ljava/util/List;, "Ljava/util/List<+Lcom/android/server/pm/pkg/component/ParsedMainComponent;>;"
    if-eqz p0, :cond_5

    if-nez p1, :cond_0

    goto :goto_4

    .line 49
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_4

    .line 50
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/pkg/component/ParsedMainComponent;

    invoke-interface {v1}, Lcom/android/server/pm/pkg/component/ParsedMainComponent;->isExported()Z

    move-result v1

    if-nez v1, :cond_1

    .line 51
    goto :goto_3

    .line 54
    :cond_1
    new-instance v1, Lmiui/security/WakePathComponent;

    invoke-direct {v1}, Lmiui/security/WakePathComponent;-><init>()V

    .line 55
    .local v1, "wakePathComponent":Lmiui/security/WakePathComponent;
    invoke-virtual {v1, p2}, Lmiui/security/WakePathComponent;->setType(I)V

    .line 56
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/pm/pkg/component/ParsedMainComponent;

    invoke-interface {v2}, Lcom/android/server/pm/pkg/component/ParsedMainComponent;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/security/WakePathComponent;->setClassname(Ljava/lang/String;)V

    .line 57
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/pm/pkg/component/ParsedMainComponent;

    invoke-interface {v2}, Lcom/android/server/pm/pkg/component/ParsedMainComponent;->getIntents()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "j":I
    :goto_1
    if-ltz v2, :cond_3

    .line 58
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/pkg/component/ParsedMainComponent;

    invoke-interface {v3}, Lcom/android/server/pm/pkg/component/ParsedMainComponent;->getIntents()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/pkg/component/ParsedIntentInfo;

    invoke-interface {v3}, Lcom/android/server/pm/pkg/component/ParsedIntentInfo;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object v3

    .line 59
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    invoke-virtual {v3}, Landroid/content/IntentFilter;->countActions()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .local v4, "k":I
    :goto_2
    if-ltz v4, :cond_2

    .line 60
    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->getAction(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lmiui/security/WakePathComponent;->addIntentAction(Ljava/lang/String;)V

    .line 59
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 57
    .end local v3    # "intentFilter":Landroid/content/IntentFilter;
    .end local v4    # "k":I
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 63
    .end local v2    # "j":I
    :cond_3
    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    .end local v1    # "wakePathComponent":Lmiui/security/WakePathComponent;
    :goto_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 65
    .end local v0    # "i":I
    :cond_4
    return-void

    .line 46
    :cond_5
    :goto_4
    return-void
.end method
