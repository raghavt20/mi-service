public abstract class com.android.server.pm.PackageEventRecorderInternal {
	 /* .source "PackageEventRecorderInternal.java" */
	 /* # virtual methods */
	 public abstract void commitDeletedEvents ( Integer p0, java.util.List p1 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(I", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;)V" */
		 /* } */
	 } // .end annotation
} // .end method
public abstract Boolean deleteAllEventRecords ( Integer p0 ) {
} // .end method
public abstract android.os.Bundle getPackageEventRecords ( Integer p0 ) {
} // .end method
public abstract android.os.Bundle getSourcePackage ( java.lang.String p0, Integer p1, Boolean p2 ) {
} // .end method
public abstract void recordPackageActivate ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
} // .end method
public abstract void recordPackageFirstLaunch ( Integer p0, java.lang.String p1, java.lang.String p2, android.content.Intent p3 ) {
} // .end method
public abstract void recordPackageRemove ( Integer[] p0, java.lang.String p1, java.lang.String p2, Boolean p3, android.os.Bundle p4 ) {
} // .end method
public abstract void recordPackageUpdate ( Integer[] p0, java.lang.String p1, java.lang.String p2, android.os.Bundle p3 ) {
} // .end method
