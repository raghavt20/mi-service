public class com.android.server.pm.permission.MiPermissionManagerServiceImpl extends com.android.server.pm.permission.PermissionManagerServiceStub {
	 /* .source "MiPermissionManagerServiceImpl.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static final Boolean isRestrictImei;
	 private static final java.util.Set sAllowedList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.util.Set sGetImeiMessage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean mIsAndroidPermission;
/* # direct methods */
static com.android.server.pm.permission.MiPermissionManagerServiceImpl ( ) {
/* .locals 4 */
/* .line 31 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 32 */
final String v0 = "ro.miui.restrict_imei"; // const-string v0, "ro.miui.restrict_imei"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
com.android.server.pm.permission.MiPermissionManagerServiceImpl.isRestrictImei = (v0!= 0);
/* .line 37 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 38 */
final String v1 = "android"; // const-string v1, "android"
/* .line 39 */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
/* .line 40 */
final String v1 = "com.miui.cit"; // const-string v1, "com.miui.cit"
/* .line 41 */
final String v1 = "com.xiaomi.finddevice"; // const-string v1, "com.xiaomi.finddevice"
/* .line 42 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
/* .line 43 */
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
/* .line 44 */
final String v1 = "com.android.vending"; // const-string v1, "com.android.vending"
/* .line 45 */
final String v1 = "com.google.android.gms"; // const-string v1, "com.google.android.gms"
/* .line 46 */
final String v1 = "com.xiaomi.factory.mmi"; // const-string v1, "com.xiaomi.factory.mmi"
/* .line 47 */
final String v1 = "com.miui.qr"; // const-string v1, "com.miui.qr"
/* .line 48 */
final String v1 = "com.android.contacts"; // const-string v1, "com.android.contacts"
/* .line 49 */
final String v1 = "com.xiaomi.registration"; // const-string v1, "com.xiaomi.registration"
/* .line 50 */
final String v1 = "com.miui.tsmclient"; // const-string v1, "com.miui.tsmclient"
/* .line 51 */
final String v1 = "com.miui.sekeytool"; // const-string v1, "com.miui.sekeytool"
/* .line 52 */
final String v1 = "com.android.updater"; // const-string v1, "com.android.updater"
/* .line 53 */
final String v1 = "com.miui.vipservice"; // const-string v1, "com.miui.vipservice"
/* .line 54 */
final String v1 = "com.xiaomi.uatalive"; // const-string v1, "com.xiaomi.uatalive"
/* .line 55 */
final String v1 = "com.android.phone"; // const-string v1, "com.android.phone"
/* .line 56 */
final String v1 = "com.xiaomi.ab"; // const-string v1, "com.xiaomi.ab"
/* .line 57 */
final String v1 = "cn_chinamobile"; // const-string v1, "cn_chinamobile"
final String v2 = "ro.miui.cust_variant"; // const-string v2, "ro.miui.cust_variant"
android.os.SystemProperties .get ( v2 );
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 58 */
android.os.SystemProperties .get ( v2 );
final String v2 = "cn_chinatelecom"; // const-string v2, "cn_chinatelecom"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 59 */
} // :cond_1
final String v1 = "com.mobiletools.systemhelper"; // const-string v1, "com.mobiletools.systemhelper"
/* .line 60 */
final String v1 = "com.miui.dmregservice"; // const-string v1, "com.miui.dmregservice"
/* .line 62 */
} // :cond_2
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 64 */
final String v1 = "getImeiForSlot"; // const-string v1, "getImeiForSlot"
/* .line 65 */
final String v1 = "getDeviceId"; // const-string v1, "getDeviceId"
/* .line 66 */
final String v1 = "getMeidForSlot"; // const-string v1, "getMeidForSlot"
/* .line 68 */
final String v1 = "getSmallDeviceId"; // const-string v1, "getSmallDeviceId"
/* .line 69 */
final String v1 = "getDeviceIdList"; // const-string v1, "getDeviceIdList"
/* .line 70 */
final String v1 = "getSortedImeiList"; // const-string v1, "getSortedImeiList"
/* .line 71 */
final String v1 = "getSortedMeidList"; // const-string v1, "getSortedMeidList"
/* .line 72 */
final String v1 = "getMeid"; // const-string v1, "getMeid"
/* .line 73 */
final String v1 = "getImei"; // const-string v1, "getImei"
/* .line 74 */
return;
} // .end method
public com.android.server.pm.permission.MiPermissionManagerServiceImpl ( ) {
/* .locals 0 */
/* .line 23 */
/* invoke-direct {p0}, Lcom/android/server/pm/permission/PermissionManagerServiceStub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer checkPhoneNumberAccess ( android.content.Context p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Integer p4, Integer p5 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "message" # Ljava/lang/String; */
/* .param p4, "callingFeatureId" # Ljava/lang/String; */
/* .param p5, "pid" # I */
/* .param p6, "uid" # I */
/* .line 115 */
final String v0 = "appops"; // const-string v0, "appops"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AppOpsManager; */
/* .line 117 */
/* .local v0, "appOpsManager":Landroid/app/AppOpsManager; */
/* const/16 v2, 0x2730 */
/* move-object v1, v0 */
/* move v3, p6 */
/* move-object v4, p2 */
/* move-object v5, p4 */
/* move-object v6, p3 */
v1 = /* invoke-virtual/range {v1 ..v6}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 119 */
int v1 = 1; // const/4 v1, 0x1
/* .line 121 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isAllowedAccessDeviceIdentifiers ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "callingPackage" # Ljava/lang/String; */
/* .param p2, "message" # Ljava/lang/String; */
/* .line 78 */
/* sget-boolean v0, Lcom/android/server/pm/permission/MiPermissionManagerServiceImpl;->isRestrictImei:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = android.miui.AppOpsUtils .isXOptMode ( );
/* if-nez v0, :cond_1 */
v0 = v0 = com.android.server.pm.permission.MiPermissionManagerServiceImpl.sGetImeiMessage;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 79 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_1 */
v0 = v0 = com.android.server.pm.permission.MiPermissionManagerServiceImpl.sAllowedList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 82 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "MIUILOG- Permission Denied Get Telephony identifier : pkg : "; // const-string v1, "MIUILOG- Permission Denied Get Telephony identifier : pkg : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " action : "; // const-string v1, " action : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PackageManager"; // const-string v1, "PackageManager"
android.util.Slog .d ( v1,v0 );
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* .line 80 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean isFlagPermission ( com.android.server.pm.permission.Permission p0 ) {
/* .locals 2 */
/* .param p1, "permission" # Lcom/android/server/pm/permission/Permission; */
/* .line 109 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "android"; // const-string v0, "android"
(( com.android.server.pm.permission.Permission ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/permission/Permission;->getPackageName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isNeedUpdatePermissions ( android.content.pm.PackageManagerInternal p0, java.lang.String p1, Integer[] p2, Integer[] p3 ) {
/* .locals 8 */
/* .param p1, "pm" # Landroid/content/pm/PackageManagerInternal; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "allUsers" # [I */
/* .param p4, "affectedUsers" # [I */
/* .line 90 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_4
if ( p3 != null) { // if-eqz p3, :cond_4
/* if-nez p4, :cond_0 */
/* .line 93 */
} // :cond_0
(( android.content.pm.PackageManagerInternal ) p1 ).getPackageStateInternal ( p2 ); // invoke-virtual {p1, p2}, Landroid/content/pm/PackageManagerInternal;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;
/* .line 94 */
/* .local v1, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
/* if-nez v1, :cond_1 */
/* .line 95 */
/* .line 97 */
} // :cond_1
/* array-length v2, p3 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v2, :cond_3 */
/* aget v5, p3, v4 */
/* .line 98 */
/* .local v5, "userId":I */
/* .line 99 */
/* .local v6, "state":Lcom/android/server/pm/pkg/PackageUserStateInternal; */
v7 = if ( v6 != null) { // if-eqz v6, :cond_2
v7 = if ( v7 != null) { // if-eqz v7, :cond_2
/* if-nez v7, :cond_2 */
/* .line 100 */
v7 = com.android.internal.util.ArrayUtils .contains ( p4,v5 );
/* if-nez v7, :cond_2 */
/* .line 101 */
/* .line 97 */
} // .end local v5 # "userId":I
} // .end local v6 # "state":Lcom/android/server/pm/pkg/PackageUserStateInternal;
} // :cond_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 104 */
} // :cond_3
/* .line 91 */
} // .end local v1 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
} // :cond_4
} // :goto_1
} // .end method
