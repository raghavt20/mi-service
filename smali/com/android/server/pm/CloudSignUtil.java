public class com.android.server.pm.CloudSignUtil {
	 /* .source "CloudSignUtil.java" */
	 /* # direct methods */
	 private com.android.server.pm.CloudSignUtil ( ) {
		 /* .locals 0 */
		 /* .line 19 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 20 */
		 return;
	 } // .end method
	 public static java.lang.String getNonceStr ( ) {
		 /* .locals 1 */
		 /* .line 28 */
		 java.util.UUID .randomUUID ( );
		 (( java.util.UUID ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;
	 } // .end method
	 public static java.lang.String getSign ( java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, Boolean p5, java.lang.String p6, java.lang.String p7, java.lang.String p8 ) {
		 /* .locals 6 */
		 /* .param p0, "imeiMd5" # Ljava/lang/String; */
		 /* .param p1, "device" # Ljava/lang/String; */
		 /* .param p2, "miuiVersion" # Ljava/lang/String; */
		 /* .param p3, "channel" # Ljava/lang/String; */
		 /* .param p4, "region" # Ljava/lang/String; */
		 /* .param p5, "isCn" # Z */
		 /* .param p6, "lang" # Ljava/lang/String; */
		 /* .param p7, "nonceStr" # Ljava/lang/String; */
		 /* .param p8, "sku" # Ljava/lang/String; */
		 /* .line 67 */
		 /* new-instance v0, Ljava/util/TreeMap; */
		 /* invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V */
		 /* .line 68 */
		 /* .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;" */
		 final String v1 = "imeiMd5"; // const-string v1, "imeiMd5"
		 /* .line 69 */
		 final String v1 = "device"; // const-string v1, "device"
		 /* .line 70 */
		 final String v1 = "miuiVersion"; // const-string v1, "miuiVersion"
		 /* .line 71 */
		 final String v1 = "channel"; // const-string v1, "channel"
		 /* .line 72 */
		 final String v1 = "region"; // const-string v1, "region"
		 /* .line 73 */
		 final String v1 = "isCn"; // const-string v1, "isCn"
		 java.lang.Boolean .valueOf ( p5 );
		 /* .line 74 */
		 final String v1 = "lang"; // const-string v1, "lang"
		 /* .line 75 */
		 v1 = 		 android.text.TextUtils .isEmpty ( p8 );
		 /* if-nez v1, :cond_0 */
		 /* .line 76 */
		 /* const-string/jumbo v1, "sku" */
		 /* .line 79 */
	 } // :cond_0
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 /* .line 80 */
	 /* .local v1, "unEncryptedStr":Ljava/lang/StringBuilder; */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* check-cast v3, Ljava/util/Map$Entry; */
	 /* .line 81 */
	 /* .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;" */
	 /* check-cast v4, Ljava/lang/String; */
	 (( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v5 = "&"; // const-string v5, "&"
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 /* .line 82 */
} // .end local v3 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
/* .line 84 */
} // :cond_1
/* nop */
/* .line 85 */
final String v2 = "#"; // const-string v2, "#"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 86 */
(( java.lang.StringBuilder ) v2 ).append ( p7 ); // invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 87 */
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.pm.CloudSignUtil .md5 ( v2 );
} // .end method
public static java.lang.String getSign ( java.util.TreeMap p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "nonceStr" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/TreeMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/lang/String;" */
/* } */
} // .end annotation
/* .line 42 */
/* .local p0, "paramsMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Object;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 43 */
/* .local v0, "unEncryptedStr":Ljava/lang/StringBuilder; */
/* new-instance v1, Lcom/android/server/pm/CloudSignUtil$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, v0}, Lcom/android/server/pm/CloudSignUtil$$ExternalSyntheticLambda0;-><init>(Ljava/lang/StringBuilder;)V */
(( java.util.TreeMap ) p0 ).forEach ( v1 ); // invoke-virtual {p0, v1}, Ljava/util/TreeMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 47 */
/* nop */
/* .line 48 */
final String v1 = "#"; // const-string v1, "#"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 49 */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 50 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.pm.CloudSignUtil .md5 ( v1 );
} // .end method
static void lambda$getSign$0 ( java.lang.StringBuilder p0, java.lang.String p1, java.lang.Object p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "unEncryptedStr" # Ljava/lang/StringBuilder; */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .line 43 */
(( java.lang.StringBuilder ) p0 ).append ( p1 ); // invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 44 */
final String v1 = "&"; // const-string v1, "&"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 45 */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 43 */
return;
} // .end method
public static java.lang.String md5 ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "unEncryptedStr" # Ljava/lang/String; */
/* .line 99 */
int v0 = 0; // const/4 v0, 0x0
/* .line 102 */
/* .local v0, "secretBytes":[B */
try { // :try_start_0
final String v1 = "MD5"; // const-string v1, "MD5"
java.security.MessageDigest .getInstance ( v1 );
/* .line 104 */
/* .local v1, "md":Ljava/security/MessageDigest; */
(( java.lang.String ) p0 ).getBytes ( ); // invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B
(( java.security.MessageDigest ) v1 ).update ( v2 ); // invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V
/* .line 106 */
(( java.security.MessageDigest ) v1 ).digest ( ); // invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B
/* move-object v0, v2 */
/* .line 109 */
/* new-instance v2, Ljava/math/BigInteger; */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v2, v3, v0}, Ljava/math/BigInteger;-><init>(I[B)V */
/* const/16 v3, 0x10 */
(( java.math.BigInteger ) v2 ).toString ( v3 ); // invoke-virtual {v2, v3}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;
/* .line 111 */
/* .local v2, "md5code":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 112 */
/* .local v3, "md5CodeSb":Ljava/lang/StringBuilder; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* rsub-int/lit8 v5, v5, 0x20 */
/* if-ge v4, v5, :cond_0 */
/* .line 113 */
final String v5 = "0"; // const-string v5, "0"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 112 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 115 */
} // .end local v4 # "i":I
} // :cond_0
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 116 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 119 */
} // .end local v0 # "secretBytes":[B
} // .end local v1 # "md":Ljava/security/MessageDigest;
} // .end local v2 # "md5code":Ljava/lang/String;
} // .end local v3 # "md5CodeSb":Ljava/lang/StringBuilder;
/* :catch_0 */
/* move-exception v0 */
/* .line 120 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 117 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* .line 118 */
/* .local v0, "e":Ljava/security/NoSuchAlgorithmException; */
(( java.security.NoSuchAlgorithmException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
/* .line 121 */
} // .end local v0 # "e":Ljava/security/NoSuchAlgorithmException;
/* nop */
/* .line 122 */
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
