.class public Lcom/android/server/pm/PackageEventRecorder;
.super Ljava/lang/Object;
.source "PackageEventRecorder.java"

# interfaces
.implements Lcom/android/server/pm/PackageEventRecorderInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;,
        Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;,
        Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;,
        Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;,
        Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;,
        Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;
    }
.end annotation


# static fields
.field private static final ATTR_DELIMITER:C = ' '

.field private static final ATTR_EVENT_TIME_MILLIS:Ljava/lang/String; = "eventTimeMillis"

.field private static final ATTR_ID:Ljava/lang/String; = "id"

.field private static final ATTR_INSTALLER:Ljava/lang/String; = "installerPackageName"

.field private static final ATTR_IS_REMOVED_FULLY:Ljava/lang/String; = "isRemovedFully"

.field private static final ATTR_PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field private static final ATTR_TYPE:Ljava/lang/String; = "type"

.field private static final ATTR_USER_IDS:Ljava/lang/String; = "userIds"

.field private static final ATTR_USER_IDS_DELIMITER:C = ','

.field private static final ATTR_VALUE_DELIMITER:C = '='

.field private static final BUNDLE_KEY_RECORDS:Ljava/lang/String; = "packageEventRecords"

.field private static final DEBUG:Z = false

.field private static final ERROR_PENDING_ADDED_NUM:I = 0x1388

.field private static final ERROR_PENDING_DELETED_NUM:I = 0x1770

.field private static final MAX_DELAY_TIME_MILLIS:J = 0xea60L

.field private static final MAX_FILE_SIZE:J = 0x500000L

.field private static final MAX_NUM_ACTIVATION_RECORDS:I = 0x32

.field private static final MAX_NUM_RECORDS_READ_ONCE:I = 0x32

.field private static final MESSAGE_DELETE_RECORDS:I = 0x2

.field private static final MESSAGE_WRITE_FILE:I = 0x1

.field private static final SCHEDULE_DELAY_TIME_MILLIS:J = 0x2710L

.field private static final TAG:Ljava/lang/String; = "PackageEventRecorder"

.field public static final TYPE_FIRST_LAUNCH:I = 0x1

.field public static final TYPE_REMOVE:I = 0x3

.field public static final TYPE_UPDATE:I = 0x2

.field private static final VALID_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final WARN_PENDING_ADDED_NUM:I = 0x9c4

.field private static final WARN_PENDING_DELETED_NUM:I = 0xbb8


# instance fields
.field private final mActivationRecords:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mAllPendingAddedEvents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mAllPendingDeletedEvents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field mCheckCalling:Z

.field final mHandler:Landroid/os/Handler;

.field private mLastScheduledTimeMillis:J

.field private final mTypeToFile:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mdeleteEventRecordsLocked(Lcom/android/server/pm/PackageEventRecorder;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->deleteEventRecordsLocked(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetLock(Lcom/android/server/pm/PackageEventRecorder;I)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mwriteAppendLocked(Lcom/android/server/pm/PackageEventRecorder;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->writeAppendLocked(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smprintLogWhileResolveTxt(ILjava/lang/String;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/android/server/pm/PackageEventRecorder;->printLogWhileResolveTxt(ILjava/lang/String;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smprintLogWhileResolveTxt(ILjava/lang/String;ILjava/lang/Throwable;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/android/server/pm/PackageEventRecorder;->printLogWhileResolveTxt(ILjava/lang/String;ILjava/lang/Throwable;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/pm/PackageEventRecorder;->VALID_TYPES:Ljava/util/List;

    .line 50
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method

.method constructor <init>(Ljava/io/File;Ljava/util/function/Function;Z)V
    .locals 4
    .param p1, "dir"    # Ljava/io/File;
    .param p3, "checkCalling"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/function/Function<",
            "Lcom/android/server/pm/PackageEventRecorder;",
            "Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;",
            ">;Z)V"
        }
    .end annotation

    .line 98
    .local p2, "handlerSuppler":Ljava/util/function/Function;, "Ljava/util/function/Function<Lcom/android/server/pm/PackageEventRecorder;Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mTypeToFile:Landroid/util/SparseArray;

    .line 57
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mAllPendingAddedEvents:Landroid/util/SparseArray;

    .line 61
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mAllPendingDeletedEvents:Landroid/util/SparseArray;

    .line 135
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J

    .line 834
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mActivationRecords:Ljava/util/LinkedList;

    .line 99
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 100
    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    .line 102
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "active-events.txt"

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 103
    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "update-events.txt"

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 104
    new-instance v1, Ljava/io/File;

    const-string v2, "remove-events.txt"

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 105
    sget-object v0, Lcom/android/server/pm/PackageEventRecorder;->VALID_TYPES:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 106
    .local v1, "type":I
    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder;->mAllPendingAddedEvents:Landroid/util/SparseArray;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 107
    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder;->mAllPendingDeletedEvents:Landroid/util/SparseArray;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 108
    .end local v1    # "type":I
    goto :goto_0

    .line 109
    :cond_1
    iput-boolean p3, p0, Lcom/android/server/pm/PackageEventRecorder;->mCheckCalling:Z

    .line 110
    invoke-interface {p2, p0}, Ljava/util/function/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mHandler:Landroid/os/Handler;

    .line 111
    return-void
.end method

.method private checkCallingPackage()Z
    .locals 8

    .line 566
    const-string v0, "PackageEventRecorder"

    iget-boolean v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mCheckCalling:Z

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 567
    return v2

    .line 572
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-interface {v3, v4}, Landroid/content/pm/IPackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 576
    .local v3, "pkgNames":[Ljava/lang/String;
    nop

    .line 577
    if-nez v3, :cond_1

    .line 578
    return v1

    .line 581
    :cond_1
    array-length v4, v3

    move v5, v1

    :goto_0
    if-ge v5, v4, :cond_3

    aget-object v6, v3, v5

    .line 582
    .local v6, "pkgName":Ljava/lang/String;
    const-string v7, "com.miui.analytics"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    return v2

    .line 581
    .end local v6    # "pkgName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 584
    :cond_3
    const-string v2, "open only to com.miui.analytics now"

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    return v1

    .line 573
    .end local v3    # "pkgNames":[Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 574
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to get package names for uid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 575
    return v1
.end method

.method private commitAddedEvents(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Z
    .locals 6
    .param p1, "packageEvent"    # Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;

    .line 532
    invoke-virtual {p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    const-string v0, "PackageEventRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid package event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,reject to write to file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    const/4 v0, 0x0

    return v0

    .line 538
    :cond_0
    invoke-static {p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$mgetEventType(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 539
    :try_start_0
    invoke-static {p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$mgetEventType(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List;

    move-result-object v1

    .line 540
    .local v1, "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    add-int/2addr v2, v3

    const/16 v4, 0x1388

    if-lt v2, v4, :cond_1

    .line 541
    const-string v2, "PackageEventRecorder"

    const-string/jumbo v4, "too many pending added package events in memory, clear it"

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 543
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/16 v4, 0x9c4

    if-lt v2, v4, :cond_2

    .line 544
    const-string v2, "PackageEventRecorder"

    const-string/jumbo v4, "too many pending added package events in memory, please try later"

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$mgetEventType(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 548
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 549
    nop

    .end local v1    # "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 550
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$mgetEventType(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 551
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->getNextScheduledTime()J

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 552
    return v3

    .line 549
    .end local v0    # "message":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private deleteEventRecordsLocked(I)V
    .locals 12
    .param p1, "type"    # I

    .line 402
    const-string v0, "PackageEventRecorder"

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingDeletedEvents(I)Ljava/util/List;

    move-result-object v1

    .line 403
    .local v1, "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 404
    return-void

    .line 407
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 410
    .local v2, "start":J
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getReadFileLocked(I)Ljava/io/File;

    move-result-object v4

    .line 411
    .local v4, "targetFile":Ljava/io/File;
    if-nez v4, :cond_1

    .line 412
    return-void

    .line 415
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 416
    .local v5, "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/FileReader;

    invoke-direct {v7, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 419
    .local v6, "bufferedReader":Ljava/io/BufferedReader;
    const/4 v7, 0x0

    .line 420
    .local v7, "lineNum":I
    :goto_0
    :try_start_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    move-object v9, v8

    .local v9, "line":Ljava/lang/String;
    if-eqz v8, :cond_4

    .line 421
    add-int/lit8 v7, v7, 0x1

    .line 422
    invoke-static {v9}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$smresolveIdFromRecord(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 423
    .local v8, "id":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 424
    const-string v10, "fail to resolve attr id "

    const/4 v11, 0x6

    invoke-static {v11, v10, v7}, Lcom/android/server/pm/PackageEventRecorder;->printLogWhileResolveTxt(ILjava/lang/String;I)V

    .line 425
    goto :goto_0

    .line 427
    :cond_2
    invoke-interface {v1, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 428
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 430
    .end local v8    # "id":Ljava/lang/String;
    :cond_3
    goto :goto_0

    .line 431
    .end local v7    # "lineNum":I
    .end local v9    # "line":Ljava/lang/String;
    :cond_4
    :try_start_2
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 434
    .end local v6    # "bufferedReader":Ljava/io/BufferedReader;
    nop

    .line 436
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFileLocked(I)Ljava/io/File;

    move-result-object v4

    .line 437
    if-nez v4, :cond_5

    .line 438
    return-void

    .line 441
    :cond_5
    :try_start_3
    new-instance v6, Ljava/io/BufferedWriter;

    new-instance v7, Ljava/io/FileWriter;

    invoke-direct {v7, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 443
    .local v6, "bufferedWriter":Ljava/io/BufferedWriter;
    :try_start_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 444
    .local v8, "line":Ljava/lang/String;
    invoke-virtual {v6, v8}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 445
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 446
    .end local v8    # "line":Ljava/lang/String;
    goto :goto_1

    .line 447
    :cond_6
    :try_start_5
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 453
    .end local v6    # "bufferedWriter":Ljava/io/BufferedWriter;
    nop

    .line 455
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File;

    move-result-object v6

    .line 456
    .local v6, "backupFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v7

    if-nez v7, :cond_7

    .line 457
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "succeed to write to file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ,but fail to delete backup file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 458
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 457
    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_7
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 463
    .local v7, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 464
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;

    .line 465
    .local v8, "packageEvent":Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
    invoke-static {v8}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$fgetid(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 466
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    .line 471
    .end local v8    # "packageEvent":Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
    goto :goto_2

    .line 473
    :cond_8
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 475
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cost "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v2

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "ms in deleteEventRecordsLocked"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    return-void

    .line 441
    .end local v7    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
    .local v6, "bufferedWriter":Ljava/io/BufferedWriter;
    :catchall_0
    move-exception v7

    :try_start_6
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    :catchall_1
    move-exception v8

    :try_start_7
    invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "start":J
    .end local v4    # "targetFile":Ljava/io/File;
    .end local v5    # "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    .end local p1    # "type":I
    :goto_3
    throw v7
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    .line 447
    .end local v6    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "start":J
    .restart local v4    # "targetFile":Ljava/io/File;
    .restart local v5    # "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    .restart local p1    # "type":I
    :catch_0
    move-exception v6

    .line 448
    .local v6, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fail to write to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 449
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v7

    if-nez v7, :cond_9

    .line 450
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fail to delete witten file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_9
    return-void

    .line 416
    .local v6, "bufferedReader":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v7

    :try_start_8
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto :goto_4

    :catchall_3
    move-exception v8

    :try_start_9
    invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "start":J
    .end local v4    # "targetFile":Ljava/io/File;
    .end local v5    # "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    .end local p1    # "type":I
    :goto_4
    throw v7
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 431
    .end local v6    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v1    # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "start":J
    .restart local v4    # "targetFile":Ljava/io/File;
    .restart local v5    # "reservedRecords":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    .restart local p1    # "type":I
    :catch_1
    move-exception v6

    .line 432
    .local v6, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fail to read from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 433
    return-void
.end method

.method private getBackupFile(I)Ljava/io/File;
    .locals 3
    .param p1, "type"    # I

    .line 122
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".backup"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getLock(I)Ljava/lang/Object;
    .locals 1
    .param p1, "type"    # I

    .line 114
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mTypeToFile:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized getNextScheduledTime()J
    .locals 7

    monitor-enter p0

    .line 142
    :try_start_0
    iget-wide v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    const-wide/16 v1, 0x2710

    if-gtz v0, :cond_0

    .line 143
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J

    goto :goto_0

    .line 144
    .end local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    :cond_0
    iget-wide v3, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/32 v5, 0xc350

    cmp-long v0, v3, v5

    if-gtz v0, :cond_1

    .line 146
    iget-wide v3, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J

    add-long/2addr v3, v1

    iput-wide v3, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J

    goto :goto_0

    .line 148
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J

    .line 150
    :goto_0
    iget-wide v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mLastScheduledTimeMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getPendingAddedEvents(I)Ljava/util/List;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;",
            ">;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mAllPendingAddedEvents:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private getPendingDeletedEvents(I)Ljava/util/List;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mAllPendingDeletedEvents:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private getReadFileLocked(I)Ljava/io/File;
    .locals 7
    .param p1, "type"    # I

    .line 267
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File;

    move-result-object v0

    .line 268
    .local v0, "writtenFile":Ljava/io/File;
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File;

    move-result-object v1

    .line 270
    .local v1, "backupFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    const-string v3, "PackageEventRecorder"

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    .line 271
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 272
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fail to delete damaged file "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 274
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 273
    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    return-object v4

    .line 277
    :cond_0
    return-object v1

    .line 279
    :cond_1
    return-object v0

    .line 282
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 283
    return-object v1

    .line 287
    :cond_3
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 291
    .local v2, "success":Z
    nop

    .line 292
    if-eqz v2, :cond_4

    move-object v4, v0

    :cond_4
    return-object v4

    .line 288
    .end local v2    # "success":Z
    :catch_0
    move-exception v2

    .line 289
    .local v2, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fail to create file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 290
    return-object v4
.end method

.method private getRecordsLocked(I)Landroid/os/Bundle;
    .locals 12
    .param p1, "type"    # I

    .line 313
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 315
    .local v0, "start":J
    sget-object v2, Lcom/android/server/pm/PackageEventRecorder;->VALID_TYPES:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "PackageEventRecorder"

    const/4 v4, 0x0

    if-nez v2, :cond_0

    .line 316
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid package event type "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    return-object v4

    .line 321
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getReadFileLocked(I)Ljava/io/File;

    move-result-object v2

    .line 322
    .local v2, "targetFile":Ljava/io/File;
    if-nez v2, :cond_1

    .line 323
    return-object v4

    .line 326
    :cond_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 327
    .local v5, "result":Landroid/os/Bundle;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 329
    .local v6, "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :try_start_0
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    invoke-direct {v8, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    .local v7, "bufferedReader":Ljava/io/BufferedReader;
    const/4 v8, 0x0

    .line 333
    .local v8, "lineNum":I
    :goto_0
    :try_start_1
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    move-object v10, v9

    .local v10, "line":Ljava/lang/String;
    const/16 v11, 0x32

    if-eqz v9, :cond_3

    .line 334
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    if-gt v9, v11, :cond_3

    .line 335
    add-int/lit8 v8, v8, 0x1

    .line 336
    invoke-static {v10, v8, p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$smbuildBundleFromRecord(Ljava/lang/String;II)Landroid/os/Bundle;

    move-result-object v9

    .line 337
    .local v9, "oneRecord":Landroid/os/Bundle;
    if-eqz v9, :cond_2

    .line 338
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    .end local v9    # "oneRecord":Landroid/os/Bundle;
    :cond_2
    goto :goto_0

    .line 341
    .end local v8    # "lineNum":I
    .end local v10    # "line":Ljava/lang/String;
    :cond_3
    :try_start_2
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 344
    .end local v7    # "bufferedReader":Ljava/io/BufferedReader;
    nop

    .line 347
    const/4 v3, 0x0

    .line 348
    .local v3, "i":I
    :goto_1
    nop

    .line 347
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v4, v3

    if-ge v4, v11, :cond_4

    .line 348
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 349
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;

    invoke-static {v4}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$mbuildBundle(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Landroid/os/Bundle;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 352
    .end local v3    # "i":I
    :cond_4
    const-string v3, "packageEventRecords"

    invoke-virtual {v5, v3, v6}, Landroid/os/Bundle;->putParcelableList(Ljava/lang/String;Ljava/util/List;)V

    .line 356
    return-object v5

    .line 329
    .restart local v7    # "bufferedReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v8

    :try_start_3
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v9

    :try_start_4
    invoke-virtual {v8, v9}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "start":J
    .end local v2    # "targetFile":Ljava/io/File;
    .end local v5    # "result":Landroid/os/Bundle;
    .end local v6    # "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .end local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    .end local p1    # "type":I
    :goto_2
    throw v8
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 341
    .end local v7    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v0    # "start":J
    .restart local v2    # "targetFile":Ljava/io/File;
    .restart local v5    # "result":Landroid/os/Bundle;
    .restart local v6    # "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .restart local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    .restart local p1    # "type":I
    :catch_0
    move-exception v7

    .line 342
    .local v7, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fail to resolve records from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 343
    return-object v4
.end method

.method private getWrittenFile(I)Ljava/io/File;
    .locals 1
    .param p1, "type"    # I

    .line 118
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mTypeToFile:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    return-object v0
.end method

.method private getWrittenFileLocked(I)Ljava/io/File;
    .locals 7
    .param p1, "type"    # I

    .line 183
    const-string v0, "fail to delete damaged file "

    const-string v1, "PackageEventRecorder"

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File;

    move-result-object v2

    .line 184
    .local v2, "writtenFile":Ljava/io/File;
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File;

    move-result-object v3

    .line 187
    .local v3, "backupFile":Ljava/io/File;
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 188
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 190
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v5

    if-nez v5, :cond_1

    .line 191
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 192
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 191
    invoke-static {v1, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    return-object v4

    .line 196
    :cond_0
    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 197
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fail to rename "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 198
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 197
    invoke-static {v1, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    return-object v4

    .line 202
    :cond_1
    invoke-static {v3, v2}, Landroid/os/FileUtils;->copy(Ljava/io/File;Ljava/io/File;)J

    goto :goto_0

    .line 204
    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 205
    invoke-static {v3, v2}, Landroid/os/FileUtils;->copy(Ljava/io/File;Ljava/io/File;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :cond_3
    :goto_0
    nop

    .line 216
    return-object v2

    .line 208
    :catch_0
    move-exception v5

    .line 209
    .local v5, "e":Ljava/io/IOException;
    const-string v6, "error happened in getWrittenFileLocked"

    invoke-static {v1, v6, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 210
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_4

    .line 211
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 212
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-static {v1, v0, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 214
    :cond_4
    return-object v4
.end method

.method public static isEnabled()Z
    .locals 1

    .line 178
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private static printLogWhileResolveTxt(ILjava/lang/String;I)V
    .locals 3
    .param p0, "priority"    # I
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "lineNum"    # I

    .line 556
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " at line "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    const-string v2, "PackageEventRecorder"

    invoke-static {v1, p0, v2, v0}, Landroid/util/Log;->println_native(IILjava/lang/String;Ljava/lang/String;)I

    .line 557
    return-void
.end method

.method private static printLogWhileResolveTxt(ILjava/lang/String;ILjava/lang/Throwable;)V
    .locals 3
    .param p0, "priority"    # I
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "lineNum"    # I
    .param p3, "tr"    # Ljava/lang/Throwable;

    .line 561
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " at line "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 562
    invoke-static {p3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 561
    const/4 v1, 0x3

    const-string v2, "PackageEventRecorder"

    invoke-static {v1, p0, v2, v0}, Landroid/util/Log;->println_native(IILjava/lang/String;Ljava/lang/String;)I

    .line 563
    return-void
.end method

.method static shouldRecordPackageActivate(Ljava/lang/String;Ljava/lang/String;ILcom/android/server/pm/pkg/PackageStateInternal;)Z
    .locals 1
    .param p0, "activatedPackage"    # Ljava/lang/String;
    .param p1, "sourcePackage"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "pkgState"    # Lcom/android/server/pm/pkg/PackageStateInternal;

    .line 839
    invoke-static {}, Lcom/android/server/pm/PackageEventRecorder;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 840
    invoke-interface {p3, p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getUserStateOrDefault(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/pm/pkg/PackageUserStateInternal;->isNotLaunched()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 841
    invoke-interface {p3, p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getUserStateOrDefault(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/pm/pkg/PackageUserStateInternal;->isStopped()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 839
    :goto_0
    return v0
.end method

.method private writeAppendLocked(I)V
    .locals 9
    .param p1, "type"    # I

    .line 221
    const-string v0, "PackageEventRecorder"

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List;

    move-result-object v1

    .line 222
    .local v1, "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 223
    return-void

    .line 226
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 228
    .local v2, "start":J
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFileLocked(I)Ljava/io/File;

    move-result-object v4

    .line 229
    .local v4, "targetFile":Ljava/io/File;
    if-nez v4, :cond_1

    .line 230
    return-void

    .line 234
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/32 v7, 0x500000

    cmp-long v5, v5, v7

    if-ltz v5, :cond_2

    .line 235
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 238
    :cond_2
    :try_start_0
    new-instance v5, Ljava/io/BufferedWriter;

    new-instance v6, Ljava/io/FileWriter;

    const/4 v7, 0x1

    invoke-direct {v6, v4, v7}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct {v5, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    .local v5, "bufferedWriter":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;

    .line 242
    .local v7, "packageEvent":Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
    invoke-virtual {v7, v5}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->WriteToTxt(Ljava/io/BufferedWriter;)V

    .line 243
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->newLine()V

    .line 244
    .end local v7    # "packageEvent":Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
    goto :goto_0

    .line 245
    :cond_3
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 252
    .end local v5    # "bufferedWriter":Ljava/io/BufferedWriter;
    nop

    .line 254
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 255
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File;

    move-result-object v5

    .line 256
    .local v5, "backupFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_4

    .line 257
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "succeed to write to file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ,but fail to delete backup file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 258
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 257
    invoke-static {v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_4
    return-void

    .line 238
    .local v5, "bufferedWriter":Ljava/io/BufferedWriter;
    :catchall_0
    move-exception v6

    :try_start_3
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v7

    :try_start_4
    invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
    .end local v2    # "start":J
    .end local v4    # "targetFile":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    .end local p1    # "type":I
    :goto_1
    throw v6
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 246
    .end local v5    # "bufferedWriter":Ljava/io/BufferedWriter;
    .restart local v1    # "pendingAdded":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;>;"
    .restart local v2    # "start":J
    .restart local v4    # "targetFile":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/pm/PackageEventRecorder;
    .restart local p1    # "type":I
    :catch_0
    move-exception v5

    .line 247
    .local v5, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fail to write append to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 248
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_5

    .line 249
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fail to delete witten file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_5
    return-void
.end method


# virtual methods
.method public commitDeletedEvents(ILjava/util/List;)V
    .locals 6
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 375
    .local p2, "eventIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->checkCallingPackage()Z

    move-result v0

    if-nez v0, :cond_0

    .line 376
    return-void

    .line 378
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x1770

    if-lt v0, v1, :cond_1

    .line 379
    const-string v0, "PackageEventRecorder"

    const-string v1, "add too many deleted package events, abandon it"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    return-void

    .line 383
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 384
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingDeletedEvents(I)Ljava/util/List;

    move-result-object v2

    .line 385
    .local v2, "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    .line 386
    .local v3, "total":I
    if-lt v3, v1, :cond_2

    .line 387
    const-string v1, "PackageEventRecorder"

    const-string/jumbo v4, "too many pending deleted package events in memory, clear it"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    invoke-interface {v2}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 389
    :cond_2
    const/16 v1, 0xbb8

    if-lt v3, v1, :cond_3

    .line 390
    const-string v1, "PackageEventRecorder"

    const-string/jumbo v4, "too many pending deleted package events in memory, please try later"

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v1, v5, v4}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 394
    invoke-interface {v2, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 395
    nop

    .end local v2    # "pendingDeleted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "total":I
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 397
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->getNextScheduledTime()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    .line 398
    return-void

    .line 395
    .end local v0    # "message":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public deleteAllEventRecords(I)Z
    .locals 5
    .param p1, "type"    # I

    .line 360
    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->checkCallingPackage()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 361
    return v1

    .line 364
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 365
    :try_start_0
    const-string v2, "PackageEventRecorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleting all package event records, type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingAddedEvents(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 367
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getPendingDeletedEvents(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 368
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 369
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 370
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getWrittenFile(I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getBackupFile(I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    monitor-exit v0

    return v1

    .line 371
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getPackageEventRecords(I)Landroid/os/Bundle;
    .locals 2
    .param p1, "type"    # I

    .line 302
    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder;->checkCallingPackage()Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    const/4 v0, 0x0

    return-object v0

    .line 306
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getLock(I)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 307
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageEventRecorder;->getRecordsLocked(I)Landroid/os/Bundle;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 308
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getSourcePackage(Ljava/lang/String;IZ)Landroid/os/Bundle;
    .locals 6
    .param p1, "activatedPackage"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "clear"    # Z

    .line 854
    invoke-static {}, Lcom/android/server/pm/PackageEventRecorder;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 858
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mActivationRecords:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 859
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;>;"
    const/4 v2, 0x0

    .line 860
    .local v2, "sourcePackage":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 861
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;

    .line 862
    .local v3, "item":Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;
    iget-object v4, v3, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;->activatedPackage:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, v3, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;->userId:I

    if-eq p2, v4, :cond_2

    .line 863
    goto :goto_0

    .line 866
    :cond_2
    if-eqz p3, :cond_3

    .line 867
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 868
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "remove package first launch record : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PackageEventRecorder"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    :cond_3
    iget-object v2, v3, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;->sourcePackage:Ljava/lang/String;

    .line 871
    nop

    .line 873
    .end local v3    # "item":Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;
    :cond_4
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 874
    return-object v1

    .line 877
    :cond_5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 878
    .local v1, "result":Landroid/os/Bundle;
    const-string v3, "activate_source"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    return-object v1

    .line 855
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;>;"
    .end local v1    # "result":Landroid/os/Bundle;
    .end local v2    # "sourcePackage":Ljava/lang/String;
    :cond_6
    :goto_1
    return-object v1
.end method

.method public recordPackageActivate(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "activatedPackage"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "sourcePackage"    # Ljava/lang/String;

    .line 846
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mActivationRecords:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 847
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mActivationRecords:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 849
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder;->mActivationRecords:Ljava/util/LinkedList;

    new-instance v1, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, p3, v2}, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$ActivationRecord-IA;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 850
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "new package first launch record : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder;->mActivationRecords:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PackageEventRecorder"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    return-void
.end method

.method public recordPackageFirstLaunch(ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 8
    .param p1, "userId"    # I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "installer"    # Ljava/lang/String;
    .param p4, "intent"    # Landroid/content/Intent;

    .line 481
    invoke-static {}, Lcom/android/server/pm/PackageEventRecorder;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 482
    return-void

    .line 485
    :cond_0
    new-instance v0, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    filled-new-array {p1}, [I

    move-result-object v5

    const/4 v7, 0x0

    move-object v1, v0

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent-IA;)V

    .line 487
    .local v0, "event":Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;
    invoke-direct {p0, v0}, Lcom/android/server/pm/PackageEventRecorder;->commitAddedEvents(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 488
    return-void

    .line 491
    :cond_1
    const-string v1, "miuiActiveId"

    invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 492
    invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveEventTimeMillis(Ljava/lang/String;)J

    move-result-wide v1

    const-string v3, "miuiActiveTime"

    invoke-virtual {p4, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 493
    return-void
.end method

.method public recordPackageRemove([ILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V
    .locals 13
    .param p1, "userIds"    # [I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "installer"    # Ljava/lang/String;
    .param p4, "isRemovedFully"    # Z
    .param p5, "extras"    # Landroid/os/Bundle;

    .line 512
    move-object/from16 v1, p5

    invoke-static {}, Lcom/android/server/pm/PackageEventRecorder;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 513
    return-void

    .line 517
    :cond_0
    const-class v2, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;

    monitor-enter v2

    .line 519
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 520
    .local v3, "eventTimeMillis":J
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 521
    new-instance v0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;

    const/4 v12, 0x0

    move-object v5, v0

    move-wide v6, v3

    move-object v8, p2

    move-object v9, p1

    move-object/from16 v10, p3

    move/from16 v11, p4

    invoke-direct/range {v5 .. v12}, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;ZLcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent-IA;)V

    .line 523
    .local v0, "event":Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;
    move-object v5, p0

    invoke-direct {p0, v0}, Lcom/android/server/pm/PackageEventRecorder;->commitAddedEvents(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 524
    return-void

    .line 527
    :cond_1
    const-string v2, "miuiRemoveId"

    invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    const-string v2, "miuiRemoveTime"

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 529
    return-void

    .line 520
    .end local v0    # "event":Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;
    .end local v3    # "eventTimeMillis":J
    :catchall_0
    move-exception v0

    move-object v5, p0

    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public recordPackageUpdate([ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "userIds"    # [I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "installer"    # Ljava/lang/String;
    .param p4, "extras"    # Landroid/os/Bundle;

    .line 496
    invoke-static {}, Lcom/android/server/pm/PackageEventRecorder;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    return-void

    .line 500
    :cond_0
    new-instance v0, Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v7, 0x0

    move-object v1, v0

    move-object v4, p2

    move-object v5, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent-IA;)V

    .line 502
    .local v0, "event":Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;
    invoke-direct {p0, v0}, Lcom/android/server/pm/PackageEventRecorder;->commitAddedEvents(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 503
    return-void

    .line 506
    :cond_1
    const-string v1, "miuiUpdateId"

    invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    invoke-virtual {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageUpdateEvent;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveEventTimeMillis(Ljava/lang/String;)J

    move-result-wide v1

    const-string v3, "miuiUpdateTime"

    invoke-virtual {p4, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 508
    return-void
.end method
