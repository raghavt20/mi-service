class com.android.server.pm.DexoptServiceThread$1 implements java.lang.Runnable {
	 /* .source "DexoptServiceThread.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/pm/DexoptServiceThread;->performDexOptAsyncTask(Lcom/android/server/pm/dex/DexoptOptions;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.pm.DexoptServiceThread this$0; //synthetic
final com.android.server.pm.dex.DexoptOptions val$options; //synthetic
/* # direct methods */
 com.android.server.pm.DexoptServiceThread$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/DexoptServiceThread; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 185 */
this.this$0 = p1;
this.val$options = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 188 */
v0 = this.val$options;
(( com.android.server.pm.dex.DexoptOptions ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/pm/dex/DexoptOptions;->getPackageName()Ljava/lang/String;
/* .line 189 */
/* .local v0, "dexoptPackagename":Ljava/lang/String; */
v1 = this.this$0;
v1 = com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptPackageNameList ( v1 );
/* if-nez v1, :cond_0 */
/* .line 190 */
v1 = this.this$0;
com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptPackageNameList ( v1 );
/* .line 193 */
} // :cond_0
v1 = this.this$0;
v2 = this.val$options;
v2 = com.android.server.pm.DexoptServiceThread .-$$Nest$mperformDexOptInternal ( v1,v2 );
com.android.server.pm.DexoptServiceThread .-$$Nest$fputmDexoptResult ( v1,v2 );
/* .line 194 */
v1 = this.this$0;
v1 = com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptResult ( v1 );
int v2 = -1; // const/4 v2, -0x1
/* if-eq v1, v2, :cond_1 */
v1 = this.this$0;
v1 = com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptResult ( v1 );
/* if-nez v1, :cond_2 */
/* .line 196 */
} // :cond_1
com.android.server.pm.DexoptServiceThread .-$$Nest$sfgetmWaitLock ( );
/* monitor-enter v1 */
/* .line 197 */
try { // :try_start_0
v2 = this.this$0;
com.android.server.pm.DexoptServiceThread .-$$Nest$fgetmDexoptPackageNameList ( v2 );
/* .line 198 */
com.android.server.pm.DexoptServiceThread .-$$Nest$sfgetmWaitLock ( );
(( java.lang.Object ) v2 ).notifyAll ( ); // invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V
/* .line 199 */
/* monitor-exit v1 */
/* .line 201 */
} // :cond_2
return;
/* .line 199 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
