public class com.android.server.pm.Miui32BitTranslateHelper extends com.android.server.pm.Miui32BitTranslateHelperStub {
	 /* .source "Miui32BitTranslateHelper.java" */
	 /* # static fields */
	 private static java.lang.String TAG;
	 private static volatile android.os.Handler s32BitAsyncPreTransHandler;
	 private static java.lang.Object s32BitAsyncPreTransLock;
	 private static android.os.HandlerThread s32BitAsyncPreTransThread;
	 /* # direct methods */
	 public static void $r8$lambda$Aifxrp0iA8VLLsX3tQMokZU7w-s ( com.android.server.pm.Miui32BitTranslateHelper p0, com.android.server.pm.pkg.AndroidPackage p1, com.android.server.pm.pkg.PackageStateInternal p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/pm/Miui32BitTranslateHelper;->lambda$do32BitPretranslateOnInstall$0(Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V */
		 return;
	 } // .end method
	 static com.android.server.pm.Miui32BitTranslateHelper ( ) {
		 /* .locals 1 */
		 /* .line 23 */
		 /* const-class v0, Lcom/android/server/pm/Miui32BitTranslateHelper; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* .line 27 */
		 /* new-instance v0, Ljava/lang/Object; */
		 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public com.android.server.pm.Miui32BitTranslateHelper ( ) {
		 /* .locals 0 */
		 /* .line 22 */
		 /* invoke-direct {p0}, Lcom/android/server/pm/Miui32BitTranslateHelperStub;-><init>()V */
		 return;
	 } // .end method
	 private void do32BitPretranslateOnArtService ( java.lang.String p0, Integer p1, Boolean p2 ) {
		 /* .locals 8 */
		 /* .param p1, "path" # Ljava/lang/String; */
		 /* .param p2, "uid" # I */
		 /* .param p3, "isApk" # Z */
		 /* .line 108 */
		 try { // :try_start_0
			 com.android.server.pm.DexOptHelper .getArtManagerLocal ( );
			 (( java.lang.Object ) v0 ).getClass ( ); // invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
			 /* .line 109 */
			 /* .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
			 final String v1 = "do32BitPretranslate"; // const-string v1, "do32BitPretranslate"
			 int v2 = 3; // const/4 v2, 0x3
			 /* new-array v3, v2, [Ljava/lang/Class; */
			 /* const-class v4, Ljava/lang/String; */
			 int v5 = 0; // const/4 v5, 0x0
			 /* aput-object v4, v3, v5 */
			 v4 = java.lang.Integer.TYPE;
			 int v6 = 1; // const/4 v6, 0x1
			 /* aput-object v4, v3, v6 */
			 v4 = java.lang.Boolean.TYPE;
			 int v7 = 2; // const/4 v7, 0x2
			 /* aput-object v4, v3, v7 */
			 (( java.lang.Class ) v0 ).getDeclaredMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
			 /* .line 113 */
			 /* .local v1, "declaredMethod":Ljava/lang/reflect/Method; */
			 (( java.lang.reflect.Method ) v1 ).setAccessible ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V
			 /* .line 114 */
			 com.android.server.pm.DexOptHelper .getArtManagerLocal ( );
			 /* new-array v2, v2, [Ljava/lang/Object; */
			 /* aput-object p1, v2, v5 */
			 java.lang.Integer .valueOf ( p2 );
			 /* aput-object v4, v2, v6 */
			 java.lang.Boolean .valueOf ( p3 );
			 /* aput-object v4, v2, v7 */
			 (( java.lang.reflect.Method ) v1 ).invoke ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 117 */
			 /* nop */
		 } // .end local v0 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
	 } // .end local v1 # "declaredMethod":Ljava/lang/reflect/Method;
	 /* .line 115 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 116 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 v1 = com.android.server.pm.Miui32BitTranslateHelper.TAG;
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "do32BitPretranslate failed."; // const-string v3, "do32BitPretranslate failed."
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.Exception ) v0 ).getStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v1,v2 );
	 /* .line 118 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static android.os.Handler get32BitTransHandler ( ) {
/* .locals 3 */
/* .line 30 */
v0 = com.android.server.pm.Miui32BitTranslateHelper.s32BitAsyncPreTransHandler;
/* if-nez v0, :cond_1 */
/* .line 31 */
v0 = com.android.server.pm.Miui32BitTranslateHelper.s32BitAsyncPreTransLock;
/* monitor-enter v0 */
/* .line 32 */
try { // :try_start_0
v1 = com.android.server.pm.Miui32BitTranslateHelper.s32BitAsyncPreTransHandler;
/* if-nez v1, :cond_0 */
/* .line 33 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "32bit_pretranslate_thread"; // const-string v2, "32bit_pretranslate_thread"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 34 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 35 */
/* new-instance v1, Landroid/os/Handler; */
v2 = com.android.server.pm.Miui32BitTranslateHelper.s32BitAsyncPreTransThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 37 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 39 */
} // :cond_1
} // :goto_0
v0 = com.android.server.pm.Miui32BitTranslateHelper.s32BitAsyncPreTransHandler;
} // .end method
private void lambda$do32BitPretranslateOnInstall$0 ( com.android.server.pm.pkg.AndroidPackage p0, com.android.server.pm.pkg.PackageStateInternal p1 ) { //synthethic
/* .locals 0 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "pkgSetting" # Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .line 47 */
(( com.android.server.pm.Miui32BitTranslateHelper ) p0 ).do32BitPretranslate ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/Miui32BitTranslateHelper;->do32BitPretranslate(Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V
return;
} // .end method
/* # virtual methods */
public void do32BitPretranslate ( com.android.server.pm.pkg.AndroidPackage p0, com.android.server.pm.pkg.PackageStateInternal p1 ) {
/* .locals 12 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "pkgSetting" # Lcom/android/server/pm/pkg/PackageStateInternal; */
v0 = /* .line 56 */
v0 = android.os.UserHandle .getSharedAppGid ( v0 );
/* .line 59 */
/* .local v0, "sharedGid":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 60 */
/* .local v1, "libDir":Ljava/lang/String; */
/* .line 61 */
/* .local v2, "primaryCpuAbi":Ljava/lang/String; */
/* .line 62 */
/* .local v3, "secondaryCpuAbi":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 63 */
/* .local v4, "is32BitApp":Z */
final String v5 = "arm"; // const-string v5, "arm"
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 64 */
dalvik.system.VMRuntime .getInstructionSet ( v2 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 65 */
dalvik.system.VMRuntime .getInstructionSet ( v2 );
v6 = (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 66 */
/* .line 67 */
int v4 = 1; // const/4 v4, 0x1
/* .line 68 */
} // :cond_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 69 */
dalvik.system.VMRuntime .getInstructionSet ( v3 );
if ( v6 != null) { // if-eqz v6, :cond_1
	 /* .line 70 */
	 dalvik.system.VMRuntime .getInstructionSet ( v3 );
	 v5 = 	 (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v5 != null) { // if-eqz v5, :cond_1
		 /* .line 71 */
		 /* .line 72 */
		 int v4 = 1; // const/4 v4, 0x1
		 /* .line 78 */
	 } // :cond_1
} // :goto_0
/* const-string/jumbo v5, "tango.pretrans.apk" */
int v6 = 0; // const/4 v6, 0x0
v5 = android.os.SystemProperties .getBoolean ( v5,v6 );
if ( v5 != null) { // if-eqz v5, :cond_3
	 if ( v4 != null) { // if-eqz v4, :cond_3
		 /* .line 79 */
		 com.android.server.pm.parsing.pkg.AndroidPackageUtils .getAllCodePathsExcludingResourceOnly ( p1 );
	 v7 = 	 } // :goto_1
	 if ( v7 != null) { // if-eqz v7, :cond_2
		 /* check-cast v7, Ljava/lang/String; */
		 /* .line 80 */
		 /* .local v7, "apk":Ljava/lang/String; */
		 int v8 = 1; // const/4 v8, 0x1
		 /* invoke-direct {p0, v7, v0, v8}, Lcom/android/server/pm/Miui32BitTranslateHelper;->do32BitPretranslateOnArtService(Ljava/lang/String;IZ)V */
		 /* .line 81 */
	 } // .end local v7 # "apk":Ljava/lang/String;
} // :cond_2
/* .line 83 */
} // :cond_3
v5 = com.android.server.pm.Miui32BitTranslateHelper.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "32BitPretranslate: skipping APK pre-translation for "; // const-string v8, "32BitPretranslate: skipping APK pre-translation for "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v7 );
/* .line 87 */
} // :goto_2
/* const-string/jumbo v5, "tango.pretrans.lib" */
v5 = android.os.SystemProperties .getBoolean ( v5,v6 );
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 88 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 89 */
/* new-instance v5, Ljava/io/File; */
/* invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 90 */
/* .local v5, "f":Ljava/io/File; */
v7 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
if ( v7 != null) { // if-eqz v7, :cond_4
	 v7 = 	 (( java.io.File ) v5 ).isDirectory ( ); // invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z
	 if ( v7 != null) { // if-eqz v7, :cond_4
		 /* .line 91 */
		 /* new-instance v7, Lcom/android/server/pm/Miui32BitTranslateHelper$1; */
		 /* invoke-direct {v7, p0}, Lcom/android/server/pm/Miui32BitTranslateHelper$1;-><init>(Lcom/android/server/pm/Miui32BitTranslateHelper;)V */
		 (( java.io.File ) v5 ).listFiles ( v7 ); // invoke-virtual {v5, v7}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;
		 /* array-length v8, v7 */
		 /* move v9, v6 */
	 } // :goto_3
	 /* if-ge v9, v8, :cond_4 */
	 /* aget-object v10, v7, v9 */
	 /* .line 96 */
	 /* .local v10, "lib":Ljava/io/File; */
	 (( java.io.File ) v10 ).getAbsolutePath ( ); // invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
	 /* invoke-direct {p0, v11, v0, v6}, Lcom/android/server/pm/Miui32BitTranslateHelper;->do32BitPretranslateOnArtService(Ljava/lang/String;IZ)V */
	 /* .line 91 */
} // .end local v10 # "lib":Ljava/io/File;
/* add-int/lit8 v9, v9, 0x1 */
/* .line 99 */
} // .end local v5 # "f":Ljava/io/File;
} // :cond_4
/* .line 101 */
} // :cond_5
v5 = com.android.server.pm.Miui32BitTranslateHelper.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "32BitPretranslate: skipping shared library pre-translation for "; // const-string v7, "32BitPretranslate: skipping shared library pre-translation for "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 103 */
} // :cond_6
} // :goto_4
return;
} // .end method
public void do32BitPretranslateOnInstall ( com.android.server.pm.pkg.AndroidPackage p0, com.android.server.pm.pkg.PackageStateInternal p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "pkgSetting" # Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .line 46 */
/* const-string/jumbo v0, "tango.pretrans_on_install" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 47 */
com.android.server.pm.Miui32BitTranslateHelper .get32BitTransHandler ( );
/* new-instance v1, Lcom/android/server/pm/Miui32BitTranslateHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/pm/Miui32BitTranslateHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/Miui32BitTranslateHelper;Lcom/android/server/pm/pkg/AndroidPackage;Lcom/android/server/pm/pkg/PackageStateInternal;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 49 */
} // :cond_0
return;
} // .end method
