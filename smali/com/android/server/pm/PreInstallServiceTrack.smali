.class public Lcom/android/server/pm/PreInstallServiceTrack;
.super Ljava/lang/Object;
.source "PreInstallServiceTrack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/pm/PreInstallServiceTrack$Action;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "31000000142"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.xiaomi.preload"

.field private static final SERVICE_NAME:Ljava/lang/String; = "com.miui.analytics.onetrack.TrackService"

.field private static final SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.miui.analytics"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mConnection:Landroid/content/ServiceConnection;

.field private mIsBound:Z

.field private mService:Lcom/miui/analytics/ITrackBinder;


# direct methods
.method static bridge synthetic -$$Nest$fgetmService(Lcom/android/server/pm/PreInstallServiceTrack;)Lcom/miui/analytics/ITrackBinder;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mService:Lcom/miui/analytics/ITrackBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmService(Lcom/android/server/pm/PreInstallServiceTrack;Lcom/miui/analytics/ITrackBinder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mService:Lcom/miui/analytics/ITrackBinder;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/pm/PreInstallServiceTrack;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 22
    const-string v0, "PreInstallServiceTrack"

    sput-object v0, Lcom/android/server/pm/PreInstallServiceTrack;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$1;

    invoke-direct {v0, p0}, Lcom/android/server/pm/PreInstallServiceTrack$1;-><init>(Lcom/android/server/pm/PreInstallServiceTrack;)V

    iput-object v0, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method


# virtual methods
.method public bindTrackService(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 47
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 48
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.analytics"

    const-string v2, "com.miui.analytics.onetrack.TrackService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    iget-object v1, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mIsBound:Z

    .line 50
    sget-object v1, Lcom/android/server/pm/PreInstallServiceTrack;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bindTrackService: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mIsBound:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    return-void
.end method

.method public trackEvent(Ljava/lang/String;I)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .line 60
    iget-object v0, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mService:Lcom/miui/analytics/ITrackBinder;

    if-nez v0, :cond_0

    .line 61
    sget-object v0, Lcom/android/server/pm/PreInstallServiceTrack;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "trackEvent: track service not bound"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-void

    .line 65
    :cond_0
    :try_start_0
    const-string v1, "31000000142"

    const-string v2, "com.xiaomi.preload"

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/android/server/pm/PreInstallServiceTrack;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "trackEvent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public unbindTrackService(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 54
    iget-boolean v0, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mIsBound:Z

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 57
    :cond_0
    return-void
.end method
