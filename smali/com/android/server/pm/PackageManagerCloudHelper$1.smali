.class Lcom/android/server/pm/PackageManagerCloudHelper$1;
.super Landroid/database/ContentObserver;
.source "PackageManagerCloudHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/PackageManagerCloudHelper;->registerCloudDataObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/PackageManagerCloudHelper;


# direct methods
.method public static synthetic $r8$lambda$sVyqjR0hzLoj43PoOYrbtaEAyBw(Lcom/android/server/pm/PackageManagerCloudHelper$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper$1;->lambda$onChange$0()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/pm/PackageManagerCloudHelper;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/PackageManagerCloudHelper;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 72
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerCloudHelper$1;->this$0:Lcom/android/server/pm/PackageManagerCloudHelper;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method private synthetic lambda$onChange$0()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper$1;->this$0:Lcom/android/server/pm/PackageManagerCloudHelper;

    invoke-static {v0}, Lcom/android/server/pm/PackageManagerCloudHelper;->-$$Nest$mreadCloudData(Lcom/android/server/pm/PackageManagerCloudHelper;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .line 75
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper$1;->this$0:Lcom/android/server/pm/PackageManagerCloudHelper;

    invoke-static {v0}, Lcom/android/server/pm/PackageManagerCloudHelper;->-$$Nest$fgetmContext(Lcom/android/server/pm/PackageManagerCloudHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/pm/PackageManagerCloudHelper;->-$$Nest$misCloudConfigUpdate(Lcom/android/server/pm/PackageManagerCloudHelper;Landroid/content/Context;)Z

    move-result v0

    .line 76
    .local v0, "isCloudConfigChanged":Z
    invoke-static {}, Lcom/android/server/pm/PackageManagerCloudHelper;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MiuiSettings notify cloud data is changed. Whether is applying new package cloud data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    if-eqz v0, :cond_0

    .line 80
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerCloudHelper$1;->this$0:Lcom/android/server/pm/PackageManagerCloudHelper;

    invoke-static {v1}, Lcom/android/server/pm/PackageManagerCloudHelper;->-$$Nest$fgetmPms(Lcom/android/server/pm/PackageManagerCloudHelper;)Lcom/android/server/pm/PackageManagerService;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/pm/PackageManagerCloudHelper$1$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/android/server/pm/PackageManagerCloudHelper$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/PackageManagerCloudHelper$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 82
    :cond_0
    return-void
.end method
