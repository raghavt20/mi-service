public class com.android.server.pm.PreInstallServiceTrack$Action {
	 /* .source "PreInstallServiceTrack.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/PreInstallServiceTrack; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Action" */
} // .end annotation
/* # static fields */
protected static final java.lang.String ACTION_KEY;
protected static final java.lang.String CATEGORY;
protected static final java.lang.String EVENT_ID;
protected static final java.lang.String LABEL;
protected static final java.lang.String VALUE;
/* # instance fields */
private org.json.JSONObject mContent;
private org.json.JSONObject mExtra;
private java.util.Set sKeywords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.pm.PreInstallServiceTrack$Action ( ) {
/* .locals 2 */
/* .line 83 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 78 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.mContent = v0;
/* .line 79 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
this.mExtra = v0;
/* .line 81 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.sKeywords = v0;
/* .line 84 */
final String v1 = "_event_id_"; // const-string v1, "_event_id_"
/* .line 85 */
v0 = this.sKeywords;
final String v1 = "_category_"; // const-string v1, "_category_"
/* .line 86 */
v0 = this.sKeywords;
final String v1 = "_action_"; // const-string v1, "_action_"
/* .line 87 */
v0 = this.sKeywords;
final String v1 = "_label_"; // const-string v1, "_label_"
/* .line 88 */
v0 = this.sKeywords;
final String v1 = "_value_"; // const-string v1, "_value_"
/* .line 89 */
return;
} // .end method
private void ensureKey ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/String; */
/* .line 146 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_1 */
v0 = v0 = this.sKeywords;
/* if-nez v0, :cond_0 */
/* .line 147 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "this key " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " is built-in, please pick another key."; // const-string v2, " is built-in, please pick another key."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 149 */
} // :cond_1
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void addContent ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .line 116 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 118 */
try { // :try_start_0
v0 = this.mContent;
(( org.json.JSONObject ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 121 */
/* .line 119 */
/* :catch_0 */
/* move-exception v0 */
/* .line 120 */
/* .local v0, "e":Ljava/lang/Exception; */
com.android.server.pm.PreInstallServiceTrack .-$$Nest$sfgetTAG ( );
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 123 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public void addContent ( java.lang.String p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # J */
/* .line 126 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 128 */
try { // :try_start_0
v0 = this.mContent;
(( org.json.JSONObject ) v0 ).put ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 131 */
/* .line 129 */
/* :catch_0 */
/* move-exception v0 */
/* .line 130 */
/* .local v0, "e":Ljava/lang/Exception; */
com.android.server.pm.PreInstallServiceTrack .-$$Nest$sfgetTAG ( );
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 133 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public void addContent ( java.lang.String p0, java.lang.Object p1 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .line 136 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 138 */
try { // :try_start_0
v0 = this.mContent;
(( org.json.JSONObject ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 141 */
/* .line 139 */
/* :catch_0 */
/* move-exception v0 */
/* .line 140 */
/* .local v0, "e":Ljava/lang/Exception; */
com.android.server.pm.PreInstallServiceTrack .-$$Nest$sfgetTAG ( );
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 143 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public com.android.server.pm.PreInstallServiceTrack$Action addParam ( java.lang.String p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .line 98 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->ensureKey(Ljava/lang/String;)V */
/* .line 99 */
(( com.android.server.pm.PreInstallServiceTrack$Action ) p0 ).addContent ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addContent(Ljava/lang/String;I)V
/* .line 100 */
} // .end method
public com.android.server.pm.PreInstallServiceTrack$Action addParam ( java.lang.String p0, Long p1 ) {
/* .locals 0 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # J */
/* .line 104 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->ensureKey(Ljava/lang/String;)V */
/* .line 105 */
(( com.android.server.pm.PreInstallServiceTrack$Action ) p0 ).addContent ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addContent(Ljava/lang/String;J)V
/* .line 106 */
} // .end method
public com.android.server.pm.PreInstallServiceTrack$Action addParam ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .line 110 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->ensureKey(Ljava/lang/String;)V */
/* .line 111 */
(( com.android.server.pm.PreInstallServiceTrack$Action ) p0 ).addContent ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addContent(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 112 */
} // .end method
public com.android.server.pm.PreInstallServiceTrack$Action addParam ( java.lang.String p0, org.json.JSONObject p1 ) {
/* .locals 0 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Lorg/json/JSONObject; */
/* .line 92 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->ensureKey(Ljava/lang/String;)V */
/* .line 93 */
(( com.android.server.pm.PreInstallServiceTrack$Action ) p0 ).addContent ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addContent(Ljava/lang/String;Ljava/lang/Object;)V
/* .line 94 */
} // .end method
final org.json.JSONObject getContent ( ) {
/* .locals 1 */
/* .line 152 */
v0 = this.mContent;
} // .end method
final org.json.JSONObject getExtra ( ) {
/* .locals 1 */
/* .line 156 */
v0 = this.mExtra;
} // .end method
