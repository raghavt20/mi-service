public class com.android.server.pm.CloudControlPreinstallService$UninstallApp {
	 /* .source "CloudControlPreinstallService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/CloudControlPreinstallService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "UninstallApp" */
} // .end annotation
/* # instance fields */
Integer confId;
java.lang.String custVariant;
Integer offlineCount;
java.lang.String packageName;
/* # direct methods */
public com.android.server.pm.CloudControlPreinstallService$UninstallApp ( ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "custVariant" # Ljava/lang/String; */
/* .param p3, "confId" # I */
/* .param p4, "offlineCount" # I */
/* .line 159 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 160 */
this.packageName = p1;
/* .line 161 */
this.custVariant = p2;
/* .line 162 */
/* iput p3, p0, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->confId:I */
/* .line 163 */
/* iput p4, p0, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->offlineCount:I */
/* .line 164 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 4 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 168 */
/* instance-of v0, p1, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 169 */
	 /* move-object v0, p1 */
	 /* check-cast v0, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
	 /* .line 170 */
	 /* .local v0, "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
	 v2 = this.packageName;
	 v3 = this.packageName;
	 v2 = 	 android.text.TextUtils .equals ( v2,v3 );
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 v2 = this.custVariant;
		 v3 = this.custVariant;
		 /* .line 171 */
		 v2 = 		 android.text.TextUtils .equals ( v2,v3 );
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 int v1 = 1; // const/4 v1, 0x1
		 } // :cond_0
		 /* nop */
		 /* .line 170 */
	 } // :goto_0
	 /* .line 173 */
} // .end local v0 # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
} // :cond_1
} // .end method
