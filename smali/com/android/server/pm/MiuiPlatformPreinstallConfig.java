public class com.android.server.pm.MiuiPlatformPreinstallConfig extends com.android.server.pm.MiuiPreinstallConfig {
	 /* .source "MiuiPlatformPreinstallConfig.java" */
	 /* # static fields */
	 private static final java.lang.String MIUI_PLATFORM_PREINSTALL_PATH;
	 /* # instance fields */
	 private java.util.Set mNeedIgnoreSet;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
protected com.android.server.pm.MiuiPlatformPreinstallConfig ( ) {
/* .locals 1 */
/* .line 15 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallConfig;-><init>()V */
/* .line 13 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mNeedIgnoreSet = v0;
/* .line 16 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->initIgnoreSet()V */
/* .line 17 */
return;
} // .end method
private void initIgnoreSet ( ) {
/* .locals 2 */
/* .line 21 */
final String v0 = "POCO"; // const-string v0, "POCO"
v1 = android.os.Build.BRAND;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 22 */
	 v0 = this.mNeedIgnoreSet;
	 final String v1 = "com.mi.global.bbs"; // const-string v1, "com.mi.global.bbs"
	 /* .line 23 */
	 v0 = this.mNeedIgnoreSet;
	 final String v1 = "com.mi.global.shop"; // const-string v1, "com.mi.global.shop"
	 /* .line 25 */
} // :cond_0
v0 = this.mNeedIgnoreSet;
final String v1 = "com.mi.global.pocobbs"; // const-string v1, "com.mi.global.pocobbs"
/* .line 26 */
v0 = this.mNeedIgnoreSet;
final String v1 = "com.mi.global.pocostore"; // const-string v1, "com.mi.global.pocostore"
/* .line 28 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
protected java.util.List getCustAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 59 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected java.util.List getLegacyPreinstallList ( Boolean p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "isFirstBoot" # Z */
/* .param p2, "isDeviceUpgrading" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(ZZ)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 32 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 33 */
/* .local v0, "legacyPreinstallList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
} // .end method
protected java.util.List getPreinstallDirs ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 47 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 48 */
/* .local v0, "preinstallDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/product/data-app"; // const-string v2, "/product/data-app"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 49 */
} // .end method
protected java.util.List getVanwardAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 54 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean isPlatformPreinstall ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 68 */
final String v0 = "/product/data-app"; // const-string v0, "/product/data-app"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
} // .end method
protected Boolean needIgnore ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 39 */
v0 = v0 = this.mNeedIgnoreSet;
/* if-nez v0, :cond_0 */
/* .line 40 */
v0 = v0 = this.mNeedIgnoreSet;
/* .line 42 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean needLegacyPreinstall ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 64 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
