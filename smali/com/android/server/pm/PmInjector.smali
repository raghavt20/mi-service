.class public final Lcom/android/server/pm/PmInjector;
.super Ljava/lang/Object;
.source "PmInjector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/pm/PmInjector$InstallObserver;
    }
.end annotation


# static fields
.field private static final PM:Ljava/lang/String; = "Pm"

.field public static final RESULT_NO_RESPONSE:I = 0xa

.field public static final STATUS_INVALID_APK:I = 0x3

.field public static final STATUS_REJECT:I = -0x1

.field public static final STATUS_SUCESS:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultUserId()I
    .locals 1

    .line 153
    const/4 v0, -0x1

    return v0
.end method

.method public static installVerify(I)I
    .locals 15
    .param p0, "sessionId"    # I

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.securitycenter"

    const-string v2, "com.miui.permcenter.install.AdbInstallActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    new-instance v1, Lcom/android/server/pm/PmInjector$InstallObserver;

    invoke-direct {v1}, Lcom/android/server/pm/PmInjector$InstallObserver;-><init>()V

    .line 60
    .local v1, "activityObs":Lcom/android/server/pm/PmInjector$InstallObserver;
    const-string v2, "observer"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/IBinder;)Landroid/content/Intent;

    .line 61
    const-string v2, "android.content.pm.extra.SESSION_ID"

    invoke-virtual {v0, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 62
    const/high16 v2, 0x18000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 64
    const/4 v13, -0x1

    const/4 v14, 0x2

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v2

    .line 65
    .local v2, "am":Landroid/app/IActivityManager;
    const/4 v3, 0x0

    const-string v4, "pm"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v5, v0

    invoke-interface/range {v2 .. v12}, Landroid/app/IActivityManager;->startActivity(Landroid/app/IApplicationThread;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILandroid/app/ProfilerInfo;Landroid/os/Bundle;)I

    move-result v3

    .line 67
    .local v3, "res":I
    if-eqz v3, :cond_2

    .line 68
    const-string v4, "Pm"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "start PackageInstallerActivity failed ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    sget-boolean v4, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z

    if-eqz v4, :cond_1

    sget-boolean v4, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsBootLoaderLocked:Z

    if-eqz v4, :cond_1

    .line 71
    invoke-static {}, Lcom/android/server/pm/PmInjector;->isSecurityCenterExist()Z

    move-result v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v4, :cond_0

    goto :goto_0

    .line 74
    :cond_0
    return v13

    .line 72
    :cond_1
    :goto_0
    return v14

    .line 85
    .end local v2    # "am":Landroid/app/IActivityManager;
    .end local v3    # "res":I
    :cond_2
    nop

    .line 88
    monitor-enter v1

    .line 89
    :goto_1
    :try_start_1
    iget-boolean v2, v1, Lcom/android/server/pm/PmInjector$InstallObserver;->finished:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_6

    .line 91
    const-wide/16 v2, 0x1388

    :try_start_2
    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 92
    iget v2, v1, Lcom/android/server/pm/PmInjector$InstallObserver;->result:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_4

    sget-boolean v2, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z

    if-eqz v2, :cond_3

    sget-boolean v2, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsBootLoaderLocked:Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v2, :cond_4

    .line 95
    :cond_3
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return v14

    .line 96
    :cond_4
    :try_start_4
    iget v2, v1, Lcom/android/server/pm/PmInjector$InstallObserver;->result:I

    const/4 v4, 0x1

    if-eq v2, v3, :cond_5

    .line 97
    iput-boolean v4, v1, Lcom/android/server/pm/PmInjector$InstallObserver;->finished:Z

    .line 98
    goto :goto_1

    .line 100
    :cond_5
    const-wide/16 v2, 0x4e20

    invoke-virtual {v1, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 101
    iput-boolean v4, v1, Lcom/android/server/pm/PmInjector$InstallObserver;->finished:Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 102
    :catch_0
    move-exception v2

    .line 103
    :goto_2
    goto :goto_1

    .line 105
    :cond_6
    :try_start_5
    iget v2, v1, Lcom/android/server/pm/PmInjector$InstallObserver;->result:I

    if-ne v2, v13, :cond_7

    .line 106
    monitor-exit v1

    return v14

    .line 108
    :cond_7
    iget-object v2, v1, Lcom/android/server/pm/PmInjector$InstallObserver;->msg:Ljava/lang/String;

    .line 109
    .local v2, "msg":Ljava/lang/String;
    if-nez v2, :cond_8

    .line 110
    const-string v3, "Failure [INSTALL_CANCELED_BY_USER]"

    move-object v2, v3

    .line 112
    :cond_8
    const-string v3, "Pm"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "install msg : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v3, "Invalid apk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 114
    invoke-static {}, Lcom/android/server/pm/PmInjector;->isSecurityCenterExist()Z

    move-result v3

    if-nez v3, :cond_9

    monitor-exit v1

    return v14

    .line 115
    :cond_9
    monitor-exit v1

    const/4 v3, 0x3

    return v3

    .line 117
    :cond_a
    invoke-static {}, Lcom/android/server/pm/PmInjector;->isSecurityCenterExist()Z

    move-result v3

    if-nez v3, :cond_b

    monitor-exit v1

    return v14

    .line 118
    :cond_b
    monitor-exit v1

    return v13

    .line 120
    .end local v2    # "msg":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v2

    .line 76
    :catch_1
    move-exception v2

    .line 77
    .local v2, "e1":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 78
    const-string v3, "Pm"

    const-string/jumbo v4, "start PackageInstallerActivity RemoteException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    sget-boolean v3, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z

    if-eqz v3, :cond_d

    sget-boolean v3, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsBootLoaderLocked:Z

    if-eqz v3, :cond_d

    .line 81
    invoke-static {}, Lcom/android/server/pm/PmInjector;->isSecurityCenterExist()Z

    move-result v3

    if-nez v3, :cond_c

    goto :goto_3

    .line 84
    :cond_c
    return v13

    .line 82
    :cond_d
    :goto_3
    return v14
.end method

.method public static isSecurityCenterExist()Z
    .locals 7

    .line 157
    const-string v0, "Pm"

    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 158
    return v2

    .line 161
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v1

    const-string v3, "com.miui.securitycenter"

    .line 162
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    .line 161
    const-wide/16 v5, 0x1

    invoke-interface {v1, v3, v5, v6, v4}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 163
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    const-string v3, "checkSecurityCenterInstalled:getPackageInfo:true"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    if-eqz v1, :cond_1

    .line 165
    return v2

    .line 169
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_1
    goto :goto_0

    .line 167
    :catch_0
    move-exception v1

    .line 168
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPackageInfo error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const-string v1, "checkSecurityCenterInstalled:Exception:false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public static statusToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "status"    # I

    .line 37
    const-string v0, ""

    .line 38
    .local v0, "msg":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 46
    :pswitch_1
    const-string v0, "Invalid apk"

    .line 47
    goto :goto_0

    .line 43
    :pswitch_2
    const-string v0, "Sucess"

    .line 44
    goto :goto_0

    .line 40
    :pswitch_3
    const-string v0, "Install canceled by user"

    .line 41
    nop

    .line 52
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
