.class public Lcom/android/server/pm/MiuiPlatformPreinstallConfig;
.super Lcom/android/server/pm/MiuiPreinstallConfig;
.source "MiuiPlatformPreinstallConfig.java"


# static fields
.field private static final MIUI_PLATFORM_PREINSTALL_PATH:Ljava/lang/String; = "/product/data-app"


# instance fields
.field private mNeedIgnoreSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 15
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallConfig;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->mNeedIgnoreSet:Ljava/util/Set;

    .line 16
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->initIgnoreSet()V

    .line 17
    return-void
.end method

.method private initIgnoreSet()V
    .locals 2

    .line 21
    const-string v0, "POCO"

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->mNeedIgnoreSet:Ljava/util/Set;

    const-string v1, "com.mi.global.bbs"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 23
    iget-object v0, p0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->mNeedIgnoreSet:Ljava/util/Set;

    const-string v1, "com.mi.global.shop"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->mNeedIgnoreSet:Ljava/util/Set;

    const-string v1, "com.mi.global.pocobbs"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 26
    iget-object v0, p0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->mNeedIgnoreSet:Ljava/util/Set;

    const-string v1, "com.mi.global.pocostore"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 28
    :goto_0
    return-void
.end method


# virtual methods
.method protected getCustAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLegacyPreinstallList(ZZ)Ljava/util/List;
    .locals 1
    .param p1, "isFirstBoot"    # Z
    .param p2, "isDeviceUpgrading"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v0, "legacyPreinstallList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-object v0
.end method

.method protected getPreinstallDirs()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v0, "preinstallDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v1, Ljava/io/File;

    const-string v2, "/product/data-app"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-object v0
.end method

.method protected getVanwardAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isPlatformPreinstall(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .line 68
    const-string v0, "/product/data-app"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 39
    iget-object v0, p0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->mNeedIgnoreSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->mNeedIgnoreSet:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 42
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected needLegacyPreinstall(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 64
    const/4 v0, 0x0

    return v0
.end method
