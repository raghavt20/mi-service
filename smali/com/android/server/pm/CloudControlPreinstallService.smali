.class public Lcom/android/server/pm/CloudControlPreinstallService;
.super Lcom/android/server/SystemService;
.source "CloudControlPreinstallService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;,
        Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;,
        Lcom/android/server/pm/CloudControlPreinstallService$PackageDeleteObserver;
    }
.end annotation


# static fields
.field private static final BEGIN_UNINSTALL:Ljava/lang/String; = "begin_uninstall"

.field public static final CLAUSE_AGREED:Ljava/lang/String; = "clause_agreed"

.field public static final CLAUSE_AGREED_ACTION:Ljava/lang/String; = "com.miui.clause_agreed"

.field private static final CONF_ID:Ljava/lang/String; = "confId"

.field private static final DEBUG:Z = true

.field private static final DEFAULT_BIND_DELAY:I = 0x1f4

.field private static final DEFAULT_CONNECT_TIME_OUT:I = 0x7d0

.field private static final DEFAULT_READ_TIME_OUT:I = 0x7d0

.field private static final IMEI_MD5:Ljava/lang/String; = "imeiMd5"

.field private static final JSON_EXCEPTION:Ljava/lang/String; = "json_exception"

.field private static final NETWORK_TYPE:Ljava/lang/String; = "networkType"

.field private static final OFFLINE_COUNT:Ljava/lang/String; = "offlineCount"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field private static final PREINSTALL_CONFIG:Ljava/lang/String; = "/cust/etc/cust_apps_config"

.field private static final REGION:Ljava/lang/String; = "region"

.field private static final REMOVE_FROM_LIST_BEGIN:Ljava/lang/String; = "remove_from_list_begin"

.field private static final REQUEST_CONNECT_EXCEPTION:Ljava/lang/String; = "request_connect_exception"

.field private static final SALESE_CHANNEL:Ljava/lang/String; = "saleschannels"

.field private static final SERVER_ADDRESS:Ljava/lang/String; = "https://control.preload.xiaomi.com/offline_app_list/get?"

.field private static final SERVER_ADDRESS_GLOBAL:Ljava/lang/String; = "https://global.control.preload.xiaomi.com/offline_app_list/get?"

.field private static final SIM_DETECTION_ACTION:Ljava/lang/String; = "com.miui.action.SIM_DETECTION"

.field private static final SKU:Ljava/lang/String; = "sku"

.field private static final TAG:Ljava/lang/String; = "CloudControlPreinstall"

.field private static final TRACK_EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static final UNINSTALL_FAILED:Ljava/lang/String; = "uninstall_failed"

.field private static final UNINSTALL_SUCCESS:Ljava/lang/String; = "uninstall_success"


# instance fields
.field private isUninstallPreinstallApps:Z

.field private mConfigObj:Lorg/json/JSONObject;

.field private mImeiMe5:Ljava/lang/String;

.field private mNetworkType:Ljava/lang/String;

.field private mReceivedSIM:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mTrack:Lcom/android/server/pm/PreInstallServiceTrack;


# direct methods
.method static bridge synthetic -$$Nest$fgetisUninstallPreinstallApps(Lcom/android/server/pm/CloudControlPreinstallService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->isUninstallPreinstallApps:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmReceivedSIM(Lcom/android/server/pm/CloudControlPreinstallService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mReceivedSIM:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmReceivedSIM(Lcom/android/server/pm/CloudControlPreinstallService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mReceivedSIM:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetUninstallApps(Lcom/android/server/pm/CloudControlPreinstallService;)Ljava/util/List;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getUninstallApps()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mrecordUnInstallApps(Lcom/android/server/pm/CloudControlPreinstallService;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/CloudControlPreinstallService;->recordUnInstallApps(Ljava/util/List;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$muninstallAppsUpdateList(Lcom/android/server/pm/CloudControlPreinstallService;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/CloudControlPreinstallService;->uninstallAppsUpdateList(Ljava/util/List;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$muninstallPreinstallAppsDelay(Lcom/android/server/pm/CloudControlPreinstallService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->uninstallPreinstallAppsDelay()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 268
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 230
    new-instance v0, Lcom/android/server/pm/CloudControlPreinstallService$1;

    invoke-direct {v0, p0}, Lcom/android/server/pm/CloudControlPreinstallService$1;-><init>(Lcom/android/server/pm/CloudControlPreinstallService;)V

    iput-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 269
    return-void
.end method

.method private addPkgNameToCloudControlUninstallMap(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 361
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    sget-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->sCloudControlUninstall:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 364
    :cond_0
    sget-object v0, Lcom/android/server/pm/PreinstallApp;->sCloudControlUninstall:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    :goto_0
    return-void
.end method

.method private closeBufferedReader(Ljava/io/BufferedReader;)V
    .locals 1
    .param p1, "br"    # Ljava/io/BufferedReader;

    .line 561
    if-eqz p1, :cond_0

    .line 563
    :try_start_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    goto :goto_0

    .line 564
    :catch_0
    move-exception v0

    .line 565
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 568
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void
.end method

.method public static exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 2
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "packageName"    # Ljava/lang/String;

    .line 628
    const/16 v0, 0x80

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 630
    :catch_0
    move-exception v0

    .line 631
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    return v1
.end method

.method private findTrackingApk(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 369
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mConfigObj:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 370
    const-string v0, "/cust/etc/cust_apps_config"

    invoke-virtual {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->getFileContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 371
    .local v0, "config":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 372
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mConfigObj:Lorg/json/JSONObject;

    .line 376
    .end local v0    # "config":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mConfigObj:Lorg/json/JSONObject;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 377
    const-string v2, "data"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 378
    .local v0, "array":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 379
    return-object v1

    .line 381
    :cond_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 382
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_3

    .line 383
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 384
    .local v4, "obj":Lorg/json/JSONObject;
    if-eqz v4, :cond_2

    const-string v5, "packageName"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 385
    const-string/jumbo v1, "trackApkPackageName"

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 382
    .end local v4    # "obj":Lorg/json/JSONObject;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 390
    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v2    # "count":I
    .end local v3    # "i":I
    :cond_3
    return-object v1
.end method

.method private getAddress()Ljava/lang/String;
    .locals 1

    .line 465
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 466
    const-string v0, "https://control.preload.xiaomi.com/offline_app_list/get?"

    return-object v0

    .line 468
    :cond_0
    const-string v0, "https://global.control.preload.xiaomi.com/offline_app_list/get?"

    return-object v0
.end method

.method private getChannel()Ljava/lang/String;
    .locals 2

    .line 458
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 459
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 461
    :cond_0
    const-string v0, "ro.miui.customized.region"

    const-string v1, "Public Version"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getConnectEntity()Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .locals 24

    .line 408
    move-object/from16 v1, p0

    const/4 v2, 0x0

    .line 410
    .local v2, "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    :try_start_0
    const-string v0, "get_device_info"

    invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 411
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getImeiList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 413
    .local v0, "imei":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Lcom/android/server/pm/CloudSignUtil;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    const-string v3, ""

    :goto_0
    iput-object v3, v1, Lcom/android/server/pm/CloudControlPreinstallService;->mImeiMe5:Ljava/lang/String;

    .line 415
    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    .line 416
    .local v5, "device":Ljava/lang/String;
    sget-object v6, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 417
    .local v6, "miuiVersion":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getChannel()Ljava/lang/String;

    move-result-object v7

    .line 418
    .local v7, "channel":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    .line 419
    .local v10, "lang":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/pm/CloudSignUtil;->getNonceStr()Ljava/lang/String;

    move-result-object v11

    .line 420
    .local v11, "nonceStr":Ljava/lang/String;
    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v3, v3, 0x1

    .line 421
    .local v3, "isCn":Z
    if-eqz v3, :cond_1

    const-string v4, "CN"

    goto :goto_1

    :cond_1
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    :goto_1
    move-object v8, v4

    .line 422
    .local v8, "region":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getSku()Ljava/lang/String;

    move-result-object v12

    .line 423
    .local v12, "sku":Ljava/lang/String;
    iget-object v4, v1, Lcom/android/server/pm/CloudControlPreinstallService;->mImeiMe5:Ljava/lang/String;

    move v9, v3

    invoke-static/range {v4 .. v12}, Lcom/android/server/pm/CloudSignUtil;->getSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 425
    .local v20, "sign":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    .line 426
    :cond_2
    new-instance v4, Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;

    iget-object v14, v1, Lcom/android/server/pm/CloudControlPreinstallService;->mImeiMe5:Ljava/lang/String;

    move-object v13, v4

    move-object v15, v5

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    move-object/from16 v18, v10

    move-object/from16 v19, v11

    move/from16 v21, v3

    move-object/from16 v22, v8

    move-object/from16 v23, v12

    invoke-direct/range {v13 .. v23}, Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object v2, v4

    .line 431
    .end local v0    # "imei":Ljava/lang/String;
    .end local v3    # "isCn":Z
    .end local v5    # "device":Ljava/lang/String;
    .end local v6    # "miuiVersion":Ljava/lang/String;
    .end local v7    # "channel":Ljava/lang/String;
    .end local v8    # "region":Ljava/lang/String;
    .end local v10    # "lang":Ljava/lang/String;
    .end local v11    # "nonceStr":Ljava/lang/String;
    .end local v12    # "sku":Ljava/lang/String;
    .end local v20    # "sign":Ljava/lang/String;
    :goto_3
    goto :goto_4

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 427
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 428
    .local v0, "ex":Ljava/util/NoSuchElementException;
    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->printStackTrace()V

    .end local v0    # "ex":Ljava/util/NoSuchElementException;
    goto :goto_3

    .line 433
    :goto_4
    if-nez v2, :cond_3

    .line 434
    const-string v0, "get_device_info_failed"

    invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 437
    :cond_3
    return-object v2
.end method

.method private getSku()Ljava/lang/String;
    .locals 5

    .line 441
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const-string v1, ""

    if-nez v0, :cond_0

    .line 442
    return-object v1

    .line 444
    :cond_0
    const-string v0, "ro.miui.build.region"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    .local v0, "buildRegion":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "MI"

    if-nez v1, :cond_2

    .line 446
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v3, "GLOBAL"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    .line 448
    :cond_2
    sget-boolean v1, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-eqz v1, :cond_3

    .line 449
    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 450
    .local v1, "buildVersion":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x4

    if-le v3, v4, :cond_3

    .line 451
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 454
    .end local v1    # "buildVersion":Ljava/lang/String;
    :cond_3
    return-object v2
.end method

.method private getUninstallApps()Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;",
            ">;"
        }
    .end annotation

    .line 472
    move-object/from16 v1, p0

    const-string v2, "request_connect_exception"

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v3, v0

    .line 474
    .local v3, "uninstallApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;>;"
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getConnectEntity()Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;

    move-result-object v4

    .line 476
    .local v4, "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    if-nez v4, :cond_0

    return-object v3

    .line 478
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 480
    .local v5, "url":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "url:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v6, "CloudControlPreinstall"

    invoke-static {v6, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const/4 v7, 0x0

    .line 483
    .local v7, "conn":Ljava/net/HttpURLConnection;
    const/4 v8, 0x0

    .line 486
    .local v8, "br":Ljava/io/BufferedReader;
    :try_start_0
    const-string v0, "request_connect_start"

    invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 488
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v7, v0

    .line 489
    const/16 v0, 0x7d0

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 490
    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 491
    const-string v0, "GET"

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 492
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->connect()V

    .line 494
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 495
    .local v0, "responeCode":I
    const/16 v9, 0xc8

    if-ne v0, v9, :cond_4

    .line 496
    const-string v9, "request_connect_success"

    invoke-virtual {v1, v9}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 498
    new-instance v9, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v11, 0x400

    invoke-direct {v9, v10, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_1b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1a
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_19
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_18
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    move-object v8, v9

    .line 499
    :try_start_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 501
    .local v9, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10
    :try_end_1
    .catch Ljava/net/ProtocolException; {:try_start_1 .. :try_end_1} :catch_13
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_12
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_10
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    move-object v11, v10

    .local v11, "line":Ljava/lang/String;
    if-eqz v10, :cond_1

    .line 502
    :try_start_2
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/net/ProtocolException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 550
    .end local v0    # "responeCode":I
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    .end local v11    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    goto/16 :goto_9

    .line 546
    :catch_0
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    goto/16 :goto_4

    .line 543
    :catch_1
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    goto/16 :goto_5

    .line 540
    :catch_2
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    goto/16 :goto_6

    .line 537
    :catch_3
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    goto/16 :goto_7

    .line 505
    .restart local v0    # "responeCode":I
    .restart local v9    # "sb":Ljava/lang/StringBuilder;
    .restart local v11    # "line":Ljava/lang/String;
    :cond_1
    :try_start_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "result:"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    new-instance v10, Lorg/json/JSONObject;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 508
    .local v10, "result":Lorg/json/JSONObject;
    const-string v12, "code"

    invoke-virtual {v10, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 509
    .local v12, "code":I
    const-string v13, "message"

    invoke-virtual {v10, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 510
    .local v13, "message":Ljava/lang/String;
    if-nez v12, :cond_3

    const-string v14, "Success"

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 511
    const-string v6, "request_list_success"

    invoke-virtual {v1, v6}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 513
    const-string v6, "data"

    invoke-virtual {v10, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 514
    .local v6, "data":Lorg/json/JSONObject;
    const-string v14, "appList"

    invoke-virtual {v6, v14}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 515
    .local v14, "appList":Lorg/json/JSONArray;
    const-string v15, "channel"

    invoke-virtual {v6, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15
    :try_end_3
    .catch Ljava/net/ProtocolException; {:try_start_3 .. :try_end_3} :catch_13
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_12
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_11
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_10
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 517
    .local v15, "channelResult":Ljava/lang/String;
    const/16 v16, 0x0

    move/from16 v17, v0

    move/from16 v0, v16

    .local v0, "index":I
    .local v17, "responeCode":I
    :goto_1
    move-object/from16 v16, v4

    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .local v16, "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    :try_start_4
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 518
    invoke-virtual {v14, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4
    :try_end_4
    .catch Ljava/net/ProtocolException; {:try_start_4 .. :try_end_4} :catch_b
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 520
    .local v4, "appinfo":Lorg/json/JSONObject;
    move-object/from16 v18, v5

    .end local v5    # "url":Ljava/lang/String;
    .local v18, "url":Ljava/lang/String;
    :try_start_5
    const-string v5, "packageName"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 521
    .local v5, "pkgName":Ljava/lang/String;
    move-object/from16 v19, v6

    .end local v6    # "data":Lorg/json/JSONObject;
    .local v19, "data":Lorg/json/JSONObject;
    const-string v6, "confId"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6
    :try_end_5
    .catch Ljava/net/ProtocolException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 522
    .local v6, "confId":I
    move-object/from16 v20, v8

    .end local v8    # "br":Ljava/io/BufferedReader;
    .local v20, "br":Ljava/io/BufferedReader;
    :try_start_6
    const-string v8, "offlineCount"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 523
    .local v8, "offlineCount":I
    move-object/from16 v21, v4

    .end local v4    # "appinfo":Lorg/json/JSONObject;
    .local v21, "appinfo":Lorg/json/JSONObject;
    new-instance v4, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;

    invoke-direct {v4, v5, v15, v6, v8}, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 517
    nop

    .end local v5    # "pkgName":Ljava/lang/String;
    .end local v6    # "confId":I
    .end local v8    # "offlineCount":I
    .end local v21    # "appinfo":Lorg/json/JSONObject;
    add-int/lit8 v0, v0, 0x1

    move-object/from16 v4, v16

    move-object/from16 v5, v18

    move-object/from16 v6, v19

    move-object/from16 v8, v20

    goto :goto_1

    .line 550
    .end local v0    # "index":I
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    .end local v10    # "result":Lorg/json/JSONObject;
    .end local v11    # "line":Ljava/lang/String;
    .end local v12    # "code":I
    .end local v13    # "message":Ljava/lang/String;
    .end local v14    # "appList":Lorg/json/JSONArray;
    .end local v15    # "channelResult":Ljava/lang/String;
    .end local v17    # "responeCode":I
    .end local v19    # "data":Lorg/json/JSONObject;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .local v8, "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v0

    move-object/from16 v20, v8

    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_9

    .line 546
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object/from16 v20, v8

    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_4

    .line 543
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_5
    move-exception v0

    move-object/from16 v20, v8

    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_5

    .line 540
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_6
    move-exception v0

    move-object/from16 v20, v8

    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_6

    .line 537
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_7
    move-exception v0

    move-object/from16 v20, v8

    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_7

    .line 517
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "index":I
    .local v5, "url":Ljava/lang/String;
    .local v6, "data":Lorg/json/JSONObject;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "sb":Ljava/lang/StringBuilder;
    .restart local v10    # "result":Lorg/json/JSONObject;
    .restart local v11    # "line":Ljava/lang/String;
    .restart local v12    # "code":I
    .restart local v13    # "message":Ljava/lang/String;
    .restart local v14    # "appList":Lorg/json/JSONArray;
    .restart local v15    # "channelResult":Ljava/lang/String;
    .restart local v17    # "responeCode":I
    :cond_2
    move-object/from16 v18, v5

    move-object/from16 v19, v6

    move-object/from16 v20, v8

    .line 526
    .end local v0    # "index":I
    .end local v5    # "url":Ljava/lang/String;
    .end local v6    # "data":Lorg/json/JSONObject;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .end local v14    # "appList":Lorg/json/JSONArray;
    .end local v15    # "channelResult":Ljava/lang/String;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 550
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    .end local v10    # "result":Lorg/json/JSONObject;
    .end local v11    # "line":Ljava/lang/String;
    .end local v12    # "code":I
    .end local v13    # "message":Ljava/lang/String;
    .end local v17    # "responeCode":I
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v0

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_9

    .line 546
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_8
    move-exception v0

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_4

    .line 543
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_9
    move-exception v0

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_5

    .line 540
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_a
    move-exception v0

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_6

    .line 537
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_b
    move-exception v0

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_7

    .line 510
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .local v0, "responeCode":I
    .local v4, "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "sb":Ljava/lang/StringBuilder;
    .restart local v10    # "result":Lorg/json/JSONObject;
    .restart local v11    # "line":Ljava/lang/String;
    .restart local v12    # "code":I
    .restart local v13    # "message":Ljava/lang/String;
    :cond_3
    move/from16 v17, v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .line 527
    .end local v0    # "responeCode":I
    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v17    # "responeCode":I
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    const-string v0, "request_list_failed"

    invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 529
    const-string v0, "request result is failed"

    invoke-static {v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/net/ProtocolException; {:try_start_6 .. :try_end_6} :catch_f
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_e
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_d
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_c
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 532
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    .end local v10    # "result":Lorg/json/JSONObject;
    .end local v11    # "line":Ljava/lang/String;
    .end local v12    # "code":I
    .end local v13    # "message":Ljava/lang/String;
    :goto_2
    move-object/from16 v8, v20

    goto :goto_3

    .line 550
    .end local v17    # "responeCode":I
    :catchall_3
    move-exception v0

    move-object/from16 v8, v20

    goto/16 :goto_9

    .line 546
    :catch_c
    move-exception v0

    move-object/from16 v8, v20

    goto/16 :goto_4

    .line 543
    :catch_d
    move-exception v0

    move-object/from16 v8, v20

    goto/16 :goto_5

    .line 540
    :catch_e
    move-exception v0

    move-object/from16 v8, v20

    goto/16 :goto_6

    .line 537
    :catch_f
    move-exception v0

    move-object/from16 v8, v20

    goto/16 :goto_7

    .line 550
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catchall_4
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_9

    .line 546
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_10
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto :goto_4

    .line 543
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_11
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto :goto_5

    .line 540
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_12
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto :goto_6

    .line 537
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :catch_13
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    move-object/from16 v20, v8

    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .end local v8    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    .restart local v20    # "br":Ljava/io/BufferedReader;
    goto :goto_7

    .line 533
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .end local v20    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "responeCode":I
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    .restart local v8    # "br":Ljava/io/BufferedReader;
    :cond_4
    move/from16 v17, v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    .end local v0    # "responeCode":I
    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v17    # "responeCode":I
    .restart local v18    # "url":Ljava/lang/String;
    :try_start_7
    const-string v0, "request_connect_failed"

    invoke-virtual {v1, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 535
    const-string/jumbo v0, "server can not connected"

    invoke-static {v6, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/net/ProtocolException; {:try_start_7 .. :try_end_7} :catch_17
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_16
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_15
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_14
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 550
    .end local v17    # "responeCode":I
    :goto_3
    if-eqz v7, :cond_5

    .line 551
    goto :goto_8

    .line 546
    :catch_14
    move-exception v0

    goto :goto_4

    .line 543
    :catch_15
    move-exception v0

    goto :goto_5

    .line 540
    :catch_16
    move-exception v0

    goto :goto_6

    .line 537
    :catch_17
    move-exception v0

    goto :goto_7

    .line 550
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    :catchall_5
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    goto :goto_9

    .line 546
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_18
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    .line 547
    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    :goto_4
    :try_start_8
    invoke-virtual {v1, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 548
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 550
    .end local v0    # "e":Ljava/lang/Exception;
    if-eqz v7, :cond_5

    .line 551
    goto :goto_8

    .line 543
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_19
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    .line 544
    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    :goto_5
    const-string v2, "json_exception"

    invoke-virtual {v1, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 545
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 550
    .end local v0    # "e":Lorg/json/JSONException;
    if-eqz v7, :cond_5

    .line 551
    goto :goto_8

    .line 550
    :catchall_6
    move-exception v0

    goto :goto_9

    .line 540
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_1a
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    .line 541
    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .local v0, "e":Ljava/io/IOException;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    :goto_6
    invoke-virtual {v1, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 542
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 550
    .end local v0    # "e":Ljava/io/IOException;
    if-eqz v7, :cond_5

    .line 551
    goto :goto_8

    .line 537
    .end local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v18    # "url":Ljava/lang/String;
    .restart local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_1b
    move-exception v0

    move-object/from16 v16, v4

    move-object/from16 v18, v5

    .line 538
    .end local v4    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .end local v5    # "url":Ljava/lang/String;
    .local v0, "e":Ljava/net/ProtocolException;
    .restart local v16    # "entity":Lcom/android/server/pm/CloudControlPreinstallService$ConnectEntity;
    .restart local v18    # "url":Ljava/lang/String;
    :goto_7
    invoke-virtual {v1, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 539
    invoke-virtual {v0}, Ljava/net/ProtocolException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    .line 550
    .end local v0    # "e":Ljava/net/ProtocolException;
    if-eqz v7, :cond_5

    .line 551
    :goto_8
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 554
    :cond_5
    invoke-direct {v1, v8}, Lcom/android/server/pm/CloudControlPreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 555
    nop

    .line 557
    return-object v3

    .line 550
    :goto_9
    if-eqz v7, :cond_6

    .line 551
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 554
    :cond_6
    invoke-direct {v1, v8}, Lcom/android/server/pm/CloudControlPreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 555
    throw v0
.end method

.method private initOneTrack()V
    .locals 2

    .line 141
    new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack;

    invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mTrack:Lcom/android/server/pm/PreInstallServiceTrack;

    .line 142
    invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/pm/PreInstallServiceTrack;->bindTrackService(Landroid/content/Context;)V

    .line 143
    return-void
.end method

.method private isNetworkConnected(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 247
    const/4 v0, 0x0

    .line 250
    .local v0, "isAvailable":Z
    :try_start_0
    const-string v1, "connectivity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 251
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 253
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    .line 254
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mNetworkType:Ljava/lang/String;

    .line 255
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    move v0, v3

    goto :goto_0

    .line 257
    :cond_0
    const-string v3, ""

    iput-object v3, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mNetworkType:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    .end local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "networkInfo":Landroid/net/NetworkInfo;
    :goto_0
    goto :goto_1

    .line 260
    :catch_0
    move-exception v1

    .line 261
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 264
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return v0
.end method

.method public static isProvisioned(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 661
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 662
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v1, "user_setup_complete"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private recordUnInstallApps(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;",
            ">;)V"
        }
    .end annotation

    .line 336
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;>;"
    const-string/jumbo v0, "uninstall_success"

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 337
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;

    .line 338
    .local v2, "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
    if-eqz v2, :cond_1

    .line 339
    const-string v3, "remove_from_list_begin"

    invoke-virtual {p0, v3, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    .line 340
    const-string v4, "begin_uninstall"

    invoke-virtual {p0, v4, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    .line 341
    iget-object v5, v2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/android/server/pm/CloudControlPreinstallService;->addPkgNameToCloudControlUninstallMap(Ljava/lang/String;)V

    .line 343
    :try_start_0
    iget-object v5, v2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/android/server/pm/CloudControlPreinstallService;->findTrackingApk(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 344
    .local v5, "trackingApk":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 345
    new-instance v6, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;

    const/4 v7, 0x0

    const/4 v8, -0x1

    invoke-direct {v6, v5, v7, v8, v8}, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 346
    .local v6, "tracking":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
    invoke-virtual {p0, v3, v6}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    .line 347
    invoke-virtual {p0, v4, v6}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    .line 348
    invoke-direct {p0, v5}, Lcom/android/server/pm/CloudControlPreinstallService;->addPkgNameToCloudControlUninstallMap(Ljava/lang/String;)V

    .line 349
    invoke-virtual {p0, v0, v6}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    .end local v5    # "trackingApk":Ljava/lang/String;
    .end local v6    # "tracking":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
    :cond_0
    goto :goto_1

    .line 351
    :catch_0
    move-exception v3

    .line 352
    .local v3, "e":Lorg/json/JSONException;
    const-string v4, "CloudControlPreinstall"

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {p0, v0, v2}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    .line 356
    .end local v2    # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
    :cond_1
    goto :goto_0

    .line 358
    :cond_2
    return-void
.end method

.method public static startCloudControlService()V
    .locals 2

    .line 272
    const-class v0, Lcom/android/server/SystemServiceManager;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/SystemServiceManager;

    .line 274
    .local v0, "systemServiceManager":Lcom/android/server/SystemServiceManager;
    const-class v1, Lcom/android/server/pm/CloudControlPreinstallService;

    invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 275
    const-class v1, Lcom/android/server/pm/AdvancePreinstallService;

    invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 276
    return-void
.end method

.method private track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V
    .locals 3
    .param p1, "action"    # Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 146
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mTrack:Lcom/android/server/pm/PreInstallServiceTrack;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mTrack:Lcom/android/server/pm/PreInstallServiceTrack;

    invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack;->trackEvent(Ljava/lang/String;I)V

    .line 149
    invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudControlPreinstall"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_0
    return-void
.end method

.method private uninstallAppsUpdateList(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;",
            ">;)V"
        }
    .end annotation

    .line 572
    .local p1, "uninstallApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;>;"
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_5

    .line 577
    :cond_0
    const-string v0, "ro.miui.cust_variant"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 579
    .local v0, "currentCustVariant":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 581
    .local v1, "pm":Landroid/content/pm/PackageManager;
    if-nez v1, :cond_1

    return-void

    .line 584
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const-string v4, " failed:"

    const/4 v5, 0x2

    const-string v6, "CloudControlPreinstall"

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;

    .line 585
    .local v3, "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
    iget-object v7, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->custVariant:Ljava/lang/String;

    invoke-static {v0, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 587
    const-string v7, "remove_from_list_begin"

    invoke-virtual {p0, v7, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    .line 589
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 590
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    move-result-object v7

    iget-object v8, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    .line 591
    invoke-virtual {v7, v8}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->removeFromPreinstallList(Ljava/lang/String;)V

    goto :goto_1

    .line 593
    :cond_2
    iget-object v7, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/server/pm/PreinstallApp;->removeFromPreinstallList(Ljava/lang/String;)V

    .line 597
    :goto_1
    :try_start_0
    iget-object v7, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v5, v8}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 601
    goto :goto_2

    .line 599
    :catch_0
    move-exception v5

    .line 600
    .local v5, "e":Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "disable Package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    .end local v3    # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    goto :goto_0

    .line 606
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;

    .line 607
    .restart local v3    # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
    iget-object v7, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->custVariant:Ljava/lang/String;

    invoke-static {v0, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 608
    iget-object v7, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-static {v1, v7}, Lcom/android/server/pm/CloudControlPreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 609
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " not exist"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    const-string v7, "package_not_exist"

    invoke-virtual {p0, v7, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    goto :goto_4

    .line 614
    :cond_5
    :try_start_1
    const-string v7, "begin_uninstall"

    invoke-virtual {p0, v7, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    .line 615
    iget-object v7, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    new-instance v8, Lcom/android/server/pm/CloudControlPreinstallService$PackageDeleteObserver;

    invoke-direct {v8, p0, v3}, Lcom/android/server/pm/CloudControlPreinstallService$PackageDeleteObserver;-><init>(Lcom/android/server/pm/CloudControlPreinstallService;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    invoke-virtual {v1, v7, v8, v5}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 620
    goto :goto_4

    .line 616
    :catch_1
    move-exception v7

    .line 617
    .local v7, "e":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "uninstall Package "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    const-string/jumbo v8, "uninstall_failed"

    invoke-virtual {p0, v8, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V

    .line 623
    .end local v3    # "app":Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_4
    goto :goto_3

    .line 624
    :cond_7
    return-void

    .line 573
    .end local v0    # "currentCustVariant":Ljava/lang/String;
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :cond_8
    :goto_5
    const-string/jumbo v0, "uninstall_list_empty"

    invoke-virtual {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 574
    return-void
.end method

.method private uninstallPreinstallAppsDelay()V
    .locals 4

    .line 299
    invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->initOneTrack()V

    .line 300
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/server/pm/CloudControlPreinstallService$2;

    invoke-direct {v1, p0}, Lcom/android/server/pm/CloudControlPreinstallService$2;-><init>(Lcom/android/server/pm/CloudControlPreinstallService;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 306
    return-void
.end method


# virtual methods
.method public getFileContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .line 394
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 395
    .local v0, "file":Ljava/io/File;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 396
    .local v1, "in":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 397
    .local v2, "length":J
    long-to-int v4, v2

    new-array v4, v4, [B

    .line 398
    .local v4, "content":[B
    invoke-virtual {v1, v4}, Ljava/io/FileInputStream;->read([B)I

    .line 399
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 400
    new-instance v5, Ljava/lang/String;

    sget-object v6, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v5, v4, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 400
    return-object v5

    .line 395
    .end local v2    # "length":J
    .end local v4    # "content":[B
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "file":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/pm/CloudControlPreinstallService;
    .end local p1    # "filePath":Ljava/lang/String;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 401
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v0    # "file":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/pm/CloudControlPreinstallService;
    .restart local p1    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 402
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "CloudControlPreinstall"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    .end local v1    # "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    return-object v1
.end method

.method public onBootPhase(I)V
    .locals 4
    .param p1, "phase"    # I

    .line 285
    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_1

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onBootPhase:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CloudControlPreinstall"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/pm/CloudControlPreinstallService;->isProvisioned(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 289
    :cond_0
    const-string v0, "onBootPhase:register broadcast receiver"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 292
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.miui.action.SIM_DETECTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 293
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 294
    invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 296
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 280
    const-string v0, "CloudControlPreinstall"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    return-void
.end method

.method public trackEvent(Ljava/lang/String;)V
    .locals 6
    .param p1, "event"    # Ljava/lang/String;

    .line 125
    new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$Action;

    invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V

    .line 126
    .local v0, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action;
    const-string v1, "saleschannels"

    invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getChannel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 128
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v1, v1, 0x1

    .line 129
    .local v1, "isCn":Z
    if-eqz v1, :cond_0

    const-string v2, "CN"

    goto :goto_0

    :cond_0
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 130
    .local v2, "region":Ljava/lang/String;
    :goto_0
    invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getSku()Ljava/lang/String;

    move-result-object v3

    .line 131
    .local v3, "sku":Ljava/lang/String;
    const-string v4, "region"

    invoke-virtual {v0, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 132
    const-string/jumbo v4, "sku"

    invoke-virtual {v0, v4, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 134
    const-string v4, "imeiMd5"

    iget-object v5, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mImeiMe5:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 135
    const-string v4, "networkType"

    iget-object v5, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mNetworkType:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 136
    const-string v4, "EVENT_NAME"

    invoke-virtual {v0, v4, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 137
    invoke-direct {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V

    .line 138
    return-void
.end method

.method public trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
    .locals 5
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "app"    # Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;

    .line 102
    new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$Action;

    invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V

    .line 103
    .local v0, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action;
    const-string v1, "packageName"

    iget-object v2, p2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 104
    const-string v1, "confId"

    iget v2, p2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->confId:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 105
    const-string v1, "offlineCount"

    iget v2, p2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->offlineCount:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 106
    const-string v1, "saleschannels"

    invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getChannel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 107
    iget-object v1, p2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-static {v1}, Lmiui/os/MiuiInit;->isPreinstalledPackage(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "isPreinstalled"

    invoke-virtual {v0, v2, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 108
    iget-object v1, p2, Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;->packageName:Ljava/lang/String;

    invoke-static {v1}, Lmiui/os/MiuiInit;->getMiuiChannelPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "miuiChannelPath"

    invoke-virtual {v0, v2, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 110
    const-string v1, "imeiMd5"

    iget-object v2, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mImeiMe5:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 111
    const-string v1, "networkType"

    iget-object v2, p0, Lcom/android/server/pm/CloudControlPreinstallService;->mNetworkType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 113
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v1, v1, 0x1

    .line 114
    .local v1, "isCn":Z
    if-eqz v1, :cond_0

    const-string v2, "CN"

    goto :goto_0

    :cond_0
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "region":Ljava/lang/String;
    :goto_0
    invoke-direct {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getSku()Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "sku":Ljava/lang/String;
    const-string v4, "region"

    invoke-virtual {v0, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 117
    const-string/jumbo v4, "sku"

    invoke-virtual {v0, v4, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 118
    const-string v4, "EVENT_NAME"

    invoke-virtual {v0, v4, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 120
    invoke-direct {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V

    .line 121
    return-void
.end method

.method public uninstallPreinstallApps()V
    .locals 2

    .line 309
    const-string/jumbo v0, "uninstallPreinstallApps"

    const-string v1, "CloudControlPreinstall"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/pm/CloudControlPreinstallService;->isProvisioned(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 313
    :cond_0
    const-string/jumbo v0, "uninstallPreinstallApps:isProvisioned true"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    invoke-virtual {p0}, Lcom/android/server/pm/CloudControlPreinstallService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/pm/CloudControlPreinstallService;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 316
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/pm/CloudControlPreinstallService;->isUninstallPreinstallApps:Z

    .line 317
    const-string/jumbo v0, "uninstallPreinstallApps:isNetworkConnected true"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    new-instance v0, Lcom/android/server/pm/CloudControlPreinstallService$3;

    invoke-direct {v0, p0}, Lcom/android/server/pm/CloudControlPreinstallService$3;-><init>(Lcom/android/server/pm/CloudControlPreinstallService;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 333
    return-void
.end method
