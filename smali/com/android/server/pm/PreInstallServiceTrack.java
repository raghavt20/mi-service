public class com.android.server.pm.PreInstallServiceTrack {
	 /* .source "PreInstallServiceTrack.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/pm/PreInstallServiceTrack$Action; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static final java.lang.String PACKAGE_NAME;
private static final java.lang.String SERVICE_NAME;
private static final java.lang.String SERVICE_PACKAGE_NAME;
private static java.lang.String TAG;
/* # instance fields */
private final android.content.ServiceConnection mConnection;
private Boolean mIsBound;
private com.miui.analytics.ITrackBinder mService;
/* # direct methods */
static com.miui.analytics.ITrackBinder -$$Nest$fgetmService ( com.android.server.pm.PreInstallServiceTrack p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mService;
} // .end method
static void -$$Nest$fputmService ( com.android.server.pm.PreInstallServiceTrack p0, com.miui.analytics.ITrackBinder p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mService = p1;
	 return;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
	 /* .locals 1 */
	 v0 = com.android.server.pm.PreInstallServiceTrack.TAG;
} // .end method
static com.android.server.pm.PreInstallServiceTrack ( ) {
	 /* .locals 1 */
	 /* .line 22 */
	 final String v0 = "PreInstallServiceTrack"; // const-string v0, "PreInstallServiceTrack"
	 return;
} // .end method
public com.android.server.pm.PreInstallServiceTrack ( ) {
	 /* .locals 1 */
	 /* .line 20 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 32 */
	 /* new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/pm/PreInstallServiceTrack$1;-><init>(Lcom/android/server/pm/PreInstallServiceTrack;)V */
	 this.mConnection = v0;
	 return;
} // .end method
/* # virtual methods */
public void bindTrackService ( android.content.Context p0 ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 47 */
	 /* new-instance v0, Landroid/content/Intent; */
	 /* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
	 /* .line 48 */
	 /* .local v0, "intent":Landroid/content/Intent; */
	 final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
	 final String v2 = "com.miui.analytics.onetrack.TrackService"; // const-string v2, "com.miui.analytics.onetrack.TrackService"
	 (( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 49 */
	 v1 = this.mConnection;
	 int v2 = 1; // const/4 v2, 0x1
	 v1 = 	 (( android.content.Context ) p1 ).bindService ( v0, v1, v2 ); // invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
	 /* iput-boolean v1, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mIsBound:Z */
	 /* .line 50 */
	 v1 = com.android.server.pm.PreInstallServiceTrack.TAG;
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "bindTrackService: "; // const-string v3, "bindTrackService: "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget-boolean v3, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mIsBound:Z */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v2 );
	 /* .line 51 */
	 return;
} // .end method
public void trackEvent ( java.lang.String p0, Integer p1 ) {
	 /* .locals 4 */
	 /* .param p1, "data" # Ljava/lang/String; */
	 /* .param p2, "flags" # I */
	 /* .line 60 */
	 v0 = this.mService;
	 /* if-nez v0, :cond_0 */
	 /* .line 61 */
	 v0 = com.android.server.pm.PreInstallServiceTrack.TAG;
	 /* const-string/jumbo v1, "trackEvent: track service not bound" */
	 android.util.Slog .d ( v0,v1 );
	 /* .line 62 */
	 return;
	 /* .line 65 */
} // :cond_0
try { // :try_start_0
	 final String v1 = "31000000142"; // const-string v1, "31000000142"
	 final String v2 = "com.xiaomi.preload"; // const-string v2, "com.xiaomi.preload"
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 68 */
	 /* .line 66 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 67 */
	 /* .local v0, "e":Landroid/os/RemoteException; */
	 v1 = com.android.server.pm.PreInstallServiceTrack.TAG;
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v3, "trackEvent: " */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( android.os.RemoteException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v1,v2 );
	 /* .line 69 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
public void unbindTrackService ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 54 */
/* iget-boolean v0, p0, Lcom/android/server/pm/PreInstallServiceTrack;->mIsBound:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 55 */
v0 = this.mConnection;
(( android.content.Context ) p1 ).unbindService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 57 */
} // :cond_0
return;
} // .end method
