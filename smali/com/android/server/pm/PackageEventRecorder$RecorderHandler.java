class com.android.server.pm.PackageEventRecorder$RecorderHandler extends android.os.Handler {
	 /* .source "PackageEventRecorder.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/PackageEventRecorder; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "RecorderHandler" */
} // .end annotation
/* # instance fields */
private final com.android.server.pm.PackageEventRecorder recorder;
/* # direct methods */
 com.android.server.pm.PackageEventRecorder$RecorderHandler ( ) {
/* .locals 0 */
/* .param p1, "recorder" # Lcom/android/server/pm/PackageEventRecorder; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 75 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 76 */
this.recorder = p1;
/* .line 77 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 80 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 92 */
final String v0 = "PackageEventRecorder"; // const-string v0, "PackageEventRecorder"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "unknown message " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 87 */
/* :pswitch_0 */
v0 = this.recorder;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
com.android.server.pm.PackageEventRecorder .-$$Nest$mgetLock ( v0,v1 );
/* monitor-enter v0 */
/* .line 88 */
try { // :try_start_0
	 v1 = this.recorder;
	 v2 = this.obj;
	 /* check-cast v2, Ljava/lang/Integer; */
	 v2 = 	 (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
	 com.android.server.pm.PackageEventRecorder .-$$Nest$mdeleteEventRecordsLocked ( v1,v2 );
	 /* .line 89 */
	 /* monitor-exit v0 */
	 /* .line 90 */
	 /* .line 89 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 82 */
	 /* :pswitch_1 */
	 v0 = this.recorder;
	 v1 = this.obj;
	 /* check-cast v1, Ljava/lang/Integer; */
	 v1 = 	 (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
	 com.android.server.pm.PackageEventRecorder .-$$Nest$mgetLock ( v0,v1 );
	 /* monitor-enter v0 */
	 /* .line 83 */
	 try { // :try_start_1
		 v1 = this.recorder;
		 v2 = this.obj;
		 /* check-cast v2, Ljava/lang/Integer; */
		 v2 = 		 (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
		 com.android.server.pm.PackageEventRecorder .-$$Nest$mwriteAppendLocked ( v1,v2 );
		 /* .line 84 */
		 /* monitor-exit v0 */
		 /* .line 85 */
		 /* .line 84 */
		 /* :catchall_1 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
		 /* throw v1 */
		 /* .line 94 */
	 } // :goto_0
	 return;
	 /* :pswitch_data_0 */
	 /* .packed-switch 0x1 */
	 /* :pswitch_1 */
	 /* :pswitch_0 */
} // .end packed-switch
} // .end method
