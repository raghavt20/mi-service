.class public Lcom/android/server/pm/MiuiDexopt;
.super Ljava/lang/Object;
.source "MiuiDexopt.java"


# static fields
.field private static final BASLINE_TAG:Ljava/lang/String; = "BaselineProfile"

.field private static final MAX_TIME_INTERVALS:J = 0x927c0L

.field private static final disableBaselineFile:Ljava/io/File;

.field private static volatile sMiuiDexoptHandler:Landroid/os/Handler;

.field private static sMiuiDexoptLock:Ljava/lang/Object;

.field private static sMiuiDexoptThread:Landroid/os/HandlerThread;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

.field private mDisableBaselineLock:Ljava/lang/Object;

.field private mDisableBaselineSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEnableBaseline:Z

.field private mInvokeDisableBaselineMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mPms:Lcom/android/server/pm/PackageManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDexOptHelper(Lcom/android/server/pm/MiuiDexopt;)Lcom/android/server/pm/DexOptHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/MiuiDexopt;->mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mprepareDexMetadata(Lcom/android/server/pm/MiuiDexopt;Lcom/android/server/pm/pkg/AndroidPackage;[I)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/pm/MiuiDexopt;->prepareDexMetadata(Lcom/android/server/pm/pkg/AndroidPackage;[I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateDexMetaDataState(Lcom/android/server/pm/MiuiDexopt;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiDexopt;->updateDexMetaDataState(Landroid/content/Context;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/pm/MiuiDexopt;->sMiuiDexoptLock:Ljava/lang/Object;

    .line 46
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/disable_baseline.xml"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/pm/MiuiDexopt;->disableBaselineFile:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/DexOptHelper;Landroid/content/Context;)V
    .locals 1
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;
    .param p2, "dexOptHelper"    # Lcom/android/server/pm/DexOptHelper;
    .param p3, "context"    # Landroid/content/Context;

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/android/server/pm/MiuiDexopt;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 50
    iput-object p2, p0, Lcom/android/server/pm/MiuiDexopt;->mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

    .line 51
    iput-object p3, p0, Lcom/android/server/pm/MiuiDexopt;->mContext:Landroid/content/Context;

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiDexopt;->mDisableBaselineLock:Ljava/lang/Object;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiDexopt;->mInvokeDisableBaselineMap:Ljava/util/Map;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiDexopt;->mDisableBaselineSet:Ljava/util/Set;

    .line 55
    invoke-direct {p0}, Lcom/android/server/pm/MiuiDexopt;->loadDisableBaselineList()V

    .line 56
    return-void
.end method

.method private static getMiuiDexoptHandler()Landroid/os/Handler;
    .locals 3

    .line 131
    sget-object v0, Lcom/android/server/pm/MiuiDexopt;->sMiuiDexoptHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 132
    sget-object v0, Lcom/android/server/pm/MiuiDexopt;->sMiuiDexoptLock:Ljava/lang/Object;

    monitor-enter v0

    .line 133
    :try_start_0
    sget-object v1, Lcom/android/server/pm/MiuiDexopt;->sMiuiDexoptHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 134
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "dex_metadata_async_thread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/android/server/pm/MiuiDexopt;->sMiuiDexoptThread:Landroid/os/HandlerThread;

    .line 135
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 136
    new-instance v1, Landroid/os/Handler;

    sget-object v2, Lcom/android/server/pm/MiuiDexopt;->sMiuiDexoptThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/android/server/pm/MiuiDexopt;->sMiuiDexoptHandler:Landroid/os/Handler;

    .line 138
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 140
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/pm/MiuiDexopt;->sMiuiDexoptHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private loadDisableBaselineList()V
    .locals 5

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/android/server/pm/MiuiDexopt;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/android/server/pm/MiuiDexopt;->disableBaselineFile:Ljava/io/File;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 75
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    .line 76
    .local v1, "allPackageNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 77
    .local v3, "packageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/pm/MiuiDexopt;->mDisableBaselineSet:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    nop

    .end local v3    # "packageName":Ljava/lang/String;
    goto :goto_0

    .line 81
    .end local v0    # "sp":Landroid/content/SharedPreferences;
    .end local v1    # "allPackageNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    :cond_0
    goto :goto_1

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "load disabled baseline list fail: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BaselineProfile"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method private prepareDexMetadata(Lcom/android/server/pm/pkg/AndroidPackage;[I)Z
    .locals 6
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "userId"    # [I

    .line 242
    const-string v0, "BaselineProfile"

    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 243
    .local v1, "packageName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 245
    .local v2, "success":Z
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiDexopt;->preparepProfile(Lcom/android/server/pm/pkg/AndroidPackage;)Z

    move-result v3

    move v2, v3

    .line 247
    invoke-static {}, Lcom/android/server/pm/DexOptHelper;->useArtService()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    .line 248
    iget-object v3, p0, Lcom/android/server/pm/MiuiDexopt;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mArtManagerService:Lcom/android/server/pm/dex/ArtManagerService;

    const/4 v4, 0x1

    invoke-virtual {v3, p1, p2, v4}, Lcom/android/server/pm/dex/ArtManagerService;->prepareAppProfiles(Lcom/android/server/pm/pkg/AndroidPackage;[IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :cond_0
    goto :goto_0

    .line 253
    :catch_0
    move-exception v3

    .line 254
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " prepareDexMetadata exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const/4 v2, 0x0

    .line 257
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " prepareDexMetadata success: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    return v2
.end method

.method private preparepProfile(Lcom/android/server/pm/pkg/AndroidPackage;)Z
    .locals 19
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 152
    move-object/from16 v1, p0

    const-string v2, "BaselineProfile"

    invoke-interface/range {p1 .. p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getBaseApkPath()Ljava/lang/String;

    move-result-object v9

    .line 153
    .local v9, "apkPath":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPath()Ljava/lang/String;

    move-result-object v10

    .line 154
    .local v10, "codePath":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 155
    .local v11, "packageName":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    const-string v12, "primary.prof"

    invoke-direct {v0, v10, v12}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v13, v0

    .line 156
    .local v13, "profile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const-string v3, "base.dm"

    invoke-direct {v0, v10, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v14, v0

    .line 157
    .local v14, "dexmetadataFile":Ljava/io/File;
    const/4 v15, 0x0

    .line 160
    .local v15, "isDexmetadataDexopt":Z
    const/16 v16, 0x0

    .line 161
    .local v16, "prepareBaselineSuccess":Z
    const/4 v3, 0x0

    .line 163
    .local v3, "assetManager":Landroid/content/res/AssetManager;
    :try_start_0
    new-instance v0, Landroid/content/res/AssetManager;

    invoke-direct {v0}, Landroid/content/res/AssetManager;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-object v8, v0

    .line 164
    .end local v3    # "assetManager":Landroid/content/res/AssetManager;
    .local v8, "assetManager":Landroid/content/res/AssetManager;
    :try_start_1
    invoke-virtual {v8, v9}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    .line 165
    new-instance v0, Lcom/android/server/pm/ProfileTranscoder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v3, v0

    move-object v4, v13

    move-object v5, v9

    move-object v6, v10

    move-object v7, v11

    move-object/from16 v17, v8

    .end local v8    # "assetManager":Landroid/content/res/AssetManager;
    .local v17, "assetManager":Landroid/content/res/AssetManager;
    :try_start_2
    invoke-direct/range {v3 .. v8}, Lcom/android/server/pm/ProfileTranscoder;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/AssetManager;)V

    .line 167
    .local v0, "profileTranscoder":Lcom/android/server/pm/ProfileTranscoder;
    invoke-virtual {v0}, Lcom/android/server/pm/ProfileTranscoder;->read()Lcom/android/server/pm/ProfileTranscoder;

    move-result-object v3

    .line 168
    invoke-virtual {v3}, Lcom/android/server/pm/ProfileTranscoder;->transcodeIfNeeded()Lcom/android/server/pm/ProfileTranscoder;

    move-result-object v3

    .line 169
    invoke-virtual {v3}, Lcom/android/server/pm/ProfileTranscoder;->write()Z

    move-result v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move/from16 v16, v3

    .line 173
    .end local v0    # "profileTranscoder":Lcom/android/server/pm/ProfileTranscoder;
    nop

    .line 174
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/AssetManager;->close()V

    .line 177
    move-object/from16 v8, v17

    goto :goto_1

    .line 173
    :catchall_0
    move-exception v0

    move-object/from16 v3, v17

    goto/16 :goto_11

    .line 170
    :catch_0
    move-exception v0

    move-object/from16 v3, v17

    goto :goto_0

    .line 173
    .end local v17    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v8    # "assetManager":Landroid/content/res/AssetManager;
    :catchall_1
    move-exception v0

    move-object/from16 v17, v8

    move-object/from16 v3, v17

    .end local v8    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v17    # "assetManager":Landroid/content/res/AssetManager;
    goto/16 :goto_11

    .line 170
    .end local v17    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v8    # "assetManager":Landroid/content/res/AssetManager;
    :catch_1
    move-exception v0

    move-object/from16 v17, v8

    move-object/from16 v3, v17

    .end local v8    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v17    # "assetManager":Landroid/content/res/AssetManager;
    goto :goto_0

    .line 173
    .end local v17    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v3    # "assetManager":Landroid/content/res/AssetManager;
    :catchall_2
    move-exception v0

    goto/16 :goto_11

    .line 170
    :catch_2
    move-exception v0

    .line 171
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " prepare Baseline Profile exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 173
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    if-eqz v3, :cond_0

    .line 174
    invoke-virtual {v3}, Landroid/content/res/AssetManager;->close()V

    .line 177
    :cond_0
    move-object v8, v3

    .end local v3    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v8    # "assetManager":Landroid/content/res/AssetManager;
    :goto_1
    move/from16 v3, v16

    .line 180
    .end local v15    # "isDexmetadataDexopt":Z
    .local v3, "isDexmetadataDexopt":Z
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 181
    new-instance v0, Ljava/util/zip/ZipFile;

    invoke-direct {v0, v14}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    move-object v4, v0

    .line 183
    .local v4, "zipDmFile":Ljava/util/zip/ZipFile;
    :try_start_4
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_a

    move-object v5, v0

    .local v5, "entries":Ljava/util/Enumeration;
    :goto_2
    :try_start_5
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 184
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/ZipEntry;

    move-object v6, v0

    .line 185
    .local v6, "entry":Ljava/util/zip/ZipEntry;
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 186
    .local v7, "curEntryName":Ljava/lang/String;
    invoke-virtual {v7, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_9

    if-eqz v0, :cond_1

    .line 187
    :try_start_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v12, " primary.prof file exists, no need for DexMetadata dexopt"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_a

    .line 188
    const/4 v0, 0x0

    .line 189
    .end local v3    # "isDexmetadataDexopt":Z
    .local v0, "isDexmetadataDexopt":Z
    move v3, v0

    goto/16 :goto_7

    .line 194
    .end local v0    # "isDexmetadataDexopt":Z
    .restart local v3    # "isDexmetadataDexopt":Z
    :cond_1
    :try_start_7
    const-string v0, "cloud.prof"

    invoke-virtual {v7, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez v16, :cond_4

    .line 196
    invoke-virtual {v4, v6}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_9

    move-object v15, v0

    .line 197
    .local v15, "is":Ljava/io/InputStream;
    :try_start_8
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    move-object/from16 v17, v0

    .line 199
    .local v17, "os":Ljava/io/OutputStream;
    move/from16 v18, v3

    move-object/from16 v3, v17

    .end local v17    # "os":Ljava/io/OutputStream;
    .local v3, "os":Ljava/io/OutputStream;
    .local v18, "isDexmetadataDexopt":Z
    :try_start_9
    invoke-direct {v1, v15, v3}, Lcom/android/server/pm/MiuiDexopt;->writeFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 200
    :try_start_a
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .end local v3    # "os":Ljava/io/OutputStream;
    if-eqz v15, :cond_2

    :try_start_b
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_8

    .line 201
    .end local v15    # "is":Ljava/io/InputStream;
    :cond_2
    const/4 v0, 0x1

    move v3, v0

    .end local v18    # "isDexmetadataDexopt":Z
    .restart local v0    # "isDexmetadataDexopt":Z
    goto :goto_6

    .line 195
    .end local v0    # "isDexmetadataDexopt":Z
    .restart local v15    # "is":Ljava/io/InputStream;
    .restart local v18    # "isDexmetadataDexopt":Z
    :catchall_3
    move-exception v0

    move-object v3, v0

    goto :goto_4

    .restart local v3    # "os":Ljava/io/OutputStream;
    :catchall_4
    move-exception v0

    move-object v12, v0

    :try_start_c
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    move-object/from16 v17, v3

    goto :goto_3

    :catchall_5
    move-exception v0

    move-object/from16 v17, v3

    move-object v3, v0

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v17    # "os":Ljava/io/OutputStream;
    :try_start_d
    invoke-virtual {v12, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v4    # "zipDmFile":Ljava/util/zip/ZipFile;
    .end local v5    # "entries":Ljava/util/Enumeration;
    .end local v6    # "entry":Ljava/util/zip/ZipEntry;
    .end local v7    # "curEntryName":Ljava/lang/String;
    .end local v8    # "assetManager":Landroid/content/res/AssetManager;
    .end local v9    # "apkPath":Ljava/lang/String;
    .end local v10    # "codePath":Ljava/lang/String;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v13    # "profile":Ljava/io/File;
    .end local v14    # "dexmetadataFile":Ljava/io/File;
    .end local v15    # "is":Ljava/io/InputStream;
    .end local v16    # "prepareBaselineSuccess":Z
    .end local v18    # "isDexmetadataDexopt":Z
    .end local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .end local p1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :goto_3
    throw v12
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .end local v17    # "os":Ljava/io/OutputStream;
    .local v3, "isDexmetadataDexopt":Z
    .restart local v4    # "zipDmFile":Ljava/util/zip/ZipFile;
    .restart local v5    # "entries":Ljava/util/Enumeration;
    .restart local v6    # "entry":Ljava/util/zip/ZipEntry;
    .restart local v7    # "curEntryName":Ljava/lang/String;
    .restart local v8    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v9    # "apkPath":Ljava/lang/String;
    .restart local v10    # "codePath":Ljava/lang/String;
    .restart local v11    # "packageName":Ljava/lang/String;
    .restart local v13    # "profile":Ljava/io/File;
    .restart local v14    # "dexmetadataFile":Ljava/io/File;
    .restart local v15    # "is":Ljava/io/InputStream;
    .restart local v16    # "prepareBaselineSuccess":Z
    .restart local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .restart local p1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :catchall_6
    move-exception v0

    move/from16 v18, v3

    move-object v3, v0

    .end local v3    # "isDexmetadataDexopt":Z
    .restart local v18    # "isDexmetadataDexopt":Z
    :goto_4
    if-eqz v15, :cond_3

    :try_start_e
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    goto :goto_5

    :catchall_7
    move-exception v0

    move-object v12, v0

    :try_start_f
    invoke-virtual {v3, v12}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v4    # "zipDmFile":Ljava/util/zip/ZipFile;
    .end local v8    # "assetManager":Landroid/content/res/AssetManager;
    .end local v9    # "apkPath":Ljava/lang/String;
    .end local v10    # "codePath":Ljava/lang/String;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v13    # "profile":Ljava/io/File;
    .end local v14    # "dexmetadataFile":Ljava/io/File;
    .end local v16    # "prepareBaselineSuccess":Z
    .end local v18    # "isDexmetadataDexopt":Z
    .end local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .end local p1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :cond_3
    :goto_5
    throw v3
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_8

    .line 207
    .end local v5    # "entries":Ljava/util/Enumeration;
    .end local v6    # "entry":Ljava/util/zip/ZipEntry;
    .end local v7    # "curEntryName":Ljava/lang/String;
    .end local v15    # "is":Ljava/io/InputStream;
    .restart local v4    # "zipDmFile":Ljava/util/zip/ZipFile;
    .restart local v8    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v9    # "apkPath":Ljava/lang/String;
    .restart local v10    # "codePath":Ljava/lang/String;
    .restart local v11    # "packageName":Ljava/lang/String;
    .restart local v13    # "profile":Ljava/io/File;
    .restart local v14    # "dexmetadataFile":Ljava/io/File;
    .restart local v16    # "prepareBaselineSuccess":Z
    .restart local v18    # "isDexmetadataDexopt":Z
    .restart local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .restart local p1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :catchall_8
    move-exception v0

    move/from16 v3, v18

    goto :goto_a

    .line 204
    :catch_3
    move-exception v0

    move/from16 v3, v18

    goto :goto_9

    .line 194
    .end local v18    # "isDexmetadataDexopt":Z
    .restart local v3    # "isDexmetadataDexopt":Z
    .restart local v5    # "entries":Ljava/util/Enumeration;
    .restart local v6    # "entry":Ljava/util/zip/ZipEntry;
    .restart local v7    # "curEntryName":Ljava/lang/String;
    :cond_4
    move/from16 v18, v3

    .line 203
    .end local v3    # "isDexmetadataDexopt":Z
    .end local v6    # "entry":Ljava/util/zip/ZipEntry;
    .end local v7    # "curEntryName":Ljava/lang/String;
    .restart local v18    # "isDexmetadataDexopt":Z
    move/from16 v3, v18

    .end local v18    # "isDexmetadataDexopt":Z
    .restart local v3    # "isDexmetadataDexopt":Z
    :goto_6
    goto/16 :goto_2

    .line 183
    :cond_5
    move/from16 v18, v3

    .line 207
    .end local v5    # "entries":Ljava/util/Enumeration;
    :goto_7
    nop

    :goto_8
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V

    .line 208
    goto :goto_b

    .line 207
    :catchall_9
    move-exception v0

    move/from16 v18, v3

    .end local v3    # "isDexmetadataDexopt":Z
    .restart local v18    # "isDexmetadataDexopt":Z
    goto :goto_a

    .line 204
    .end local v18    # "isDexmetadataDexopt":Z
    .restart local v3    # "isDexmetadataDexopt":Z
    :catch_4
    move-exception v0

    move/from16 v18, v3

    .end local v3    # "isDexmetadataDexopt":Z
    .restart local v18    # "isDexmetadataDexopt":Z
    goto :goto_9

    .line 207
    .end local v18    # "isDexmetadataDexopt":Z
    .restart local v3    # "isDexmetadataDexopt":Z
    :catchall_a
    move-exception v0

    goto :goto_a

    .line 204
    :catch_5
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Exception;
    :goto_9
    :try_start_10
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " prepare Cloud Profile exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_a

    .line 207
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_8

    :goto_a
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V

    .line 208
    throw v0

    .line 212
    .end local v4    # "zipDmFile":Ljava/util/zip/ZipFile;
    :cond_6
    :goto_b
    const/4 v4, 0x0

    if-eqz v3, :cond_c

    .line 213
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 214
    invoke-virtual {v14}, Ljava/io/File;->delete()Z

    .line 217
    :cond_7
    :try_start_11
    new-instance v0, Ljava/util/zip/ZipOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v5}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_6
    .catchall {:try_start_11 .. :try_end_11} :catchall_f

    move-object v5, v0

    .line 218
    .local v5, "os":Ljava/util/zip/ZipOutputStream;
    :try_start_12
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_d

    move-object v6, v0

    .line 220
    .local v6, "is":Ljava/io/InputStream;
    :try_start_13
    new-instance v0, Ljava/util/zip/ZipEntry;

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 221
    invoke-direct {v1, v6, v5}, Lcom/android/server/pm/MiuiDexopt;->writeFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_b

    .line 222
    :try_start_14
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_d

    .end local v6    # "is":Ljava/io/InputStream;
    :try_start_15
    invoke-virtual {v5}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_6
    .catchall {:try_start_15 .. :try_end_15} :catchall_f

    .line 226
    .end local v5    # "os":Ljava/util/zip/ZipOutputStream;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 227
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 230
    :cond_8
    if-eqz v16, :cond_9

    const-string v0, "baseline"

    goto :goto_c

    :cond_9
    const-string v0, "cloud"

    .line 231
    .local v0, "effectProfile":Ljava/lang/String;
    :goto_c
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " prepare DexMetadata profile success, effective profile is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    .end local v0    # "effectProfile":Ljava/lang/String;
    goto :goto_10

    .line 216
    .restart local v5    # "os":Ljava/util/zip/ZipOutputStream;
    .restart local v6    # "is":Ljava/io/InputStream;
    :catchall_b
    move-exception v0

    move-object v7, v0

    :try_start_16
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_c

    goto :goto_d

    :catchall_c
    move-exception v0

    move-object v12, v0

    :try_start_17
    invoke-virtual {v7, v12}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v3    # "isDexmetadataDexopt":Z
    .end local v5    # "os":Ljava/util/zip/ZipOutputStream;
    .end local v8    # "assetManager":Landroid/content/res/AssetManager;
    .end local v9    # "apkPath":Ljava/lang/String;
    .end local v10    # "codePath":Ljava/lang/String;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v13    # "profile":Ljava/io/File;
    .end local v14    # "dexmetadataFile":Ljava/io/File;
    .end local v16    # "prepareBaselineSuccess":Z
    .end local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .end local p1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :goto_d
    throw v7
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_d

    .end local v6    # "is":Ljava/io/InputStream;
    .restart local v3    # "isDexmetadataDexopt":Z
    .restart local v5    # "os":Ljava/util/zip/ZipOutputStream;
    .restart local v8    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v9    # "apkPath":Ljava/lang/String;
    .restart local v10    # "codePath":Ljava/lang/String;
    .restart local v11    # "packageName":Ljava/lang/String;
    .restart local v13    # "profile":Ljava/io/File;
    .restart local v14    # "dexmetadataFile":Ljava/io/File;
    .restart local v16    # "prepareBaselineSuccess":Z
    .restart local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .restart local p1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :catchall_d
    move-exception v0

    move-object v6, v0

    :try_start_18
    invoke-virtual {v5}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_e

    goto :goto_e

    :catchall_e
    move-exception v0

    move-object v7, v0

    :try_start_19
    invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v3    # "isDexmetadataDexopt":Z
    .end local v8    # "assetManager":Landroid/content/res/AssetManager;
    .end local v9    # "apkPath":Ljava/lang/String;
    .end local v10    # "codePath":Ljava/lang/String;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v13    # "profile":Ljava/io/File;
    .end local v14    # "dexmetadataFile":Ljava/io/File;
    .end local v16    # "prepareBaselineSuccess":Z
    .end local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .end local p1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :goto_e
    throw v6
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_6
    .catchall {:try_start_19 .. :try_end_19} :catchall_f

    .line 226
    .end local v5    # "os":Ljava/util/zip/ZipOutputStream;
    .restart local v3    # "isDexmetadataDexopt":Z
    .restart local v8    # "assetManager":Landroid/content/res/AssetManager;
    .restart local v9    # "apkPath":Ljava/lang/String;
    .restart local v10    # "codePath":Ljava/lang/String;
    .restart local v11    # "packageName":Ljava/lang/String;
    .restart local v13    # "profile":Ljava/io/File;
    .restart local v14    # "dexmetadataFile":Ljava/io/File;
    .restart local v16    # "prepareBaselineSuccess":Z
    .restart local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .restart local p1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :catchall_f
    move-exception v0

    goto :goto_f

    .line 222
    :catch_6
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1a
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " prepare DexMetadata archive fail: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_f

    .line 224
    nop

    .line 226
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 227
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 224
    :cond_a
    return v4

    .line 226
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_f
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 227
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 229
    :cond_b
    throw v0

    .line 233
    :cond_c
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 234
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 237
    :cond_d
    :goto_10
    if-eqz v3, :cond_e

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v4, 0x1

    :cond_e
    return v4

    .line 173
    .end local v8    # "assetManager":Landroid/content/res/AssetManager;
    .local v3, "assetManager":Landroid/content/res/AssetManager;
    .local v15, "isDexmetadataDexopt":Z
    :goto_11
    if-eqz v3, :cond_f

    .line 174
    invoke-virtual {v3}, Landroid/content/res/AssetManager;->close()V

    .line 176
    :cond_f
    throw v0
.end method

.method private updateDexMetaDataState(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 123
    nop

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 123
    const-string v1, "DexMetaData"

    const-string v2, "enable"

    const-string v3, "false"

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "enable":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepare DexMetadata enable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BaselineProfile"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const-string v1, "persist.sys.baseline.enable"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/pm/MiuiDexopt;->mEnableBaseline:Z

    .line 128
    return-void
.end method

.method private writeFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 144
    const/16 v0, 0x200

    new-array v0, v0, [B

    .line 146
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    move v2, v1

    .local v2, "length":I
    if-lez v1, :cond_0

    .line 147
    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 149
    :cond_0
    return-void
.end method


# virtual methods
.method public asyncDexMetadataDexopt(Lcom/android/server/pm/pkg/AndroidPackage;[I)V
    .locals 2
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "userId"    # [I

    .line 262
    iget-boolean v0, p0, Lcom/android/server/pm/MiuiDexopt;->mEnableBaseline:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/pm/MiuiDexopt;->isBaselineDisabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    new-instance v0, Lcom/android/server/pm/MiuiDexopt$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/pm/MiuiDexopt$2;-><init>(Lcom/android/server/pm/MiuiDexopt;Lcom/android/server/pm/pkg/AndroidPackage;[I)V

    .line 286
    .local v0, "task":Ljava/lang/Runnable;
    invoke-static {}, Lcom/android/server/pm/MiuiDexopt;->getMiuiDexoptHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 287
    .end local v0    # "task":Ljava/lang/Runnable;
    goto :goto_0

    .line 288
    :cond_0
    const-string v0, "BaselineProfile"

    const-string v1, "asyncDexMetadataDexopt not enable"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :goto_0
    return-void
.end method

.method public isBaselineDisabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lcom/android/server/pm/MiuiDexopt;->mDisableBaselineSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public preConfigMiuiDexopt(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 59
    const-string v0, "persist.sys.baseline.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/pm/MiuiDexopt;->mEnableBaseline:Z

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 62
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/pm/MiuiDexopt$1;

    .line 63
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1}, Lcom/android/server/pm/MiuiDexopt$1;-><init>(Lcom/android/server/pm/MiuiDexopt;Landroid/os/Handler;Landroid/content/Context;)V

    .line 61
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 69
    return-void
.end method

.method public setBaselineDisabled(Ljava/lang/String;Z)Z
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "disabled"    # Z

    .line 87
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/MiuiDexopt;->mDisableBaselineLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 89
    .local v2, "currentTime":J
    iget-object v4, p0, Lcom/android/server/pm/MiuiDexopt;->mInvokeDisableBaselineMap:Ljava/util/Map;

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 91
    .local v4, "lastInvokeTime":J
    sub-long v6, v2, v4

    const-wide/32 v8, 0x927c0

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    .line 92
    const-string v6, "BaselineProfile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " invalid invoke"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    monitor-exit v1

    return v0

    .line 95
    :cond_0
    iget-object v6, p0, Lcom/android/server/pm/MiuiDexopt;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/android/server/pm/MiuiDexopt;->disableBaselineFile:Ljava/io/File;

    invoke-virtual {v6, v7, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 97
    .local v6, "sp":Landroid/content/SharedPreferences;
    iget-object v7, p0, Lcom/android/server/pm/MiuiDexopt;->mDisableBaselineSet:Ljava/util/Set;

    invoke-interface {v7, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    .line 98
    .local v7, "isContain":Z
    const/4 v8, 0x1

    if-eqz p2, :cond_1

    if-nez v7, :cond_1

    .line 99
    iget-object v9, p0, Lcom/android/server/pm/MiuiDexopt;->mDisableBaselineSet:Ljava/util/Set;

    invoke-interface {v9, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    .line 101
    .local v9, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v9, p1, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 102
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 103
    nop

    .end local v9    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    if-eqz v7, :cond_2

    .line 104
    iget-object v9, p0, Lcom/android/server/pm/MiuiDexopt;->mDisableBaselineSet:Ljava/util/Set;

    invoke-interface {v9, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 105
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    .line 106
    .restart local v9    # "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v9, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 107
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 109
    .end local v9    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    :goto_0
    iget-object v9, p0, Lcom/android/server/pm/MiuiDexopt;->mInvokeDisableBaselineMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v9, p1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    nop

    .end local v2    # "currentTime":J
    .end local v4    # "lastInvokeTime":J
    .end local v6    # "sp":Landroid/content/SharedPreferences;
    .end local v7    # "isContain":Z
    monitor-exit v1

    .line 114
    nop

    .line 115
    return v8

    .line 110
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .end local p1    # "packageName":Ljava/lang/String;
    .end local p2    # "disabled":Z
    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 111
    .restart local p0    # "this":Lcom/android/server/pm/MiuiDexopt;
    .restart local p1    # "packageName":Ljava/lang/String;
    .restart local p2    # "disabled":Z
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "BaselineProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "set baseline state fail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return v0
.end method
