class com.android.server.pm.WritableFileSection {
	 /* .source "ProfileTranscoder.java" */
	 /* # instance fields */
	 final mContents;
	 final Integer mExpectedInflateSize;
	 final Boolean mNeedsCompression;
	 final com.android.server.pm.FileSectionType mType;
	 /* # direct methods */
	 com.android.server.pm.WritableFileSection ( ) {
		 /* .locals 0 */
		 /* .param p1, "type" # Lcom/android/server/pm/FileSectionType; */
		 /* .param p2, "expectedInflateSize" # I */
		 /* .param p3, "contents" # [B */
		 /* .param p4, "needsCompression" # Z */
		 /* .line 1080 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 1081 */
		 this.mType = p1;
		 /* .line 1082 */
		 /* iput p2, p0, Lcom/android/server/pm/WritableFileSection;->mExpectedInflateSize:I */
		 /* .line 1083 */
		 this.mContents = p3;
		 /* .line 1084 */
		 /* iput-boolean p4, p0, Lcom/android/server/pm/WritableFileSection;->mNeedsCompression:Z */
		 /* .line 1085 */
		 return;
	 } // .end method
