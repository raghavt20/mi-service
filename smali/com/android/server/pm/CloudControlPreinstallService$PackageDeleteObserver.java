public class com.android.server.pm.CloudControlPreinstallService$PackageDeleteObserver extends android.content.pm.IPackageDeleteObserver$Stub {
	 /* .source "CloudControlPreinstallService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/CloudControlPreinstallService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "PackageDeleteObserver" */
} // .end annotation
/* # instance fields */
com.android.server.pm.CloudControlPreinstallService$UninstallApp mApp;
final com.android.server.pm.CloudControlPreinstallService this$0; //synthetic
/* # direct methods */
public com.android.server.pm.CloudControlPreinstallService$PackageDeleteObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/CloudControlPreinstallService; */
/* .param p2, "app" # Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp; */
/* .line 639 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V */
/* .line 640 */
this.mApp = p2;
/* .line 641 */
return;
} // .end method
/* # virtual methods */
public void packageDeleted ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "returnCode" # I */
/* .line 646 */
final String v0 = "CloudControlPreinstall"; // const-string v0, "CloudControlPreinstall"
/* if-ltz p2, :cond_0 */
/* .line 648 */
v1 = this.this$0;
/* const-string/jumbo v2, "uninstall_success" */
v3 = this.mApp;
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 650 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Package "; // const-string v2, "Package "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " was uninstalled."; // const-string v2, " was uninstalled."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 652 */
} // :cond_0
v1 = this.this$0;
/* const-string/jumbo v2, "uninstall_failed" */
v3 = this.mApp;
(( com.android.server.pm.CloudControlPreinstallService ) v1 ).trackEvent ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/CloudControlPreinstallService;->trackEvent(Ljava/lang/String;Lcom/android/server/pm/CloudControlPreinstallService$UninstallApp;)V
/* .line 654 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Package uninstall failed "; // const-string v2, "Package uninstall failed "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", returnCode "; // const-string v2, ", returnCode "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 657 */
} // :goto_0
return;
} // .end method
