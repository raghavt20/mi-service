public class com.android.server.pm.MiuiPreinstallApp {
	 /* .source "MiuiPreinstallApp.java" */
	 /* # instance fields */
	 private java.lang.String apkPath;
	 private java.lang.String packageName;
	 private Long versionCode;
	 /* # direct methods */
	 public com.android.server.pm.MiuiPreinstallApp ( ) {
		 /* .locals 0 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .param p2, "versionCode" # J */
		 /* .param p4, "apkPath" # Ljava/lang/String; */
		 /* .line 11 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 12 */
		 this.packageName = p1;
		 /* .line 13 */
		 /* iput-wide p2, p0, Lcom/android/server/pm/MiuiPreinstallApp;->versionCode:J */
		 /* .line 14 */
		 this.apkPath = p4;
		 /* .line 15 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getApkPath ( ) {
		 /* .locals 1 */
		 /* .line 26 */
		 v0 = this.apkPath;
	 } // .end method
	 public java.lang.String getPackageName ( ) {
		 /* .locals 1 */
		 /* .line 18 */
		 v0 = this.packageName;
	 } // .end method
	 public Long getVersionCode ( ) {
		 /* .locals 2 */
		 /* .line 22 */
		 /* iget-wide v0, p0, Lcom/android/server/pm/MiuiPreinstallApp;->versionCode:J */
		 /* return-wide v0 */
	 } // .end method
