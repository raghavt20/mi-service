class com.android.server.pm.PackageEventRecorder$ActivationRecord {
	 /* .source "PackageEventRecorder.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/PackageEventRecorder; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ActivationRecord" */
} // .end annotation
/* # instance fields */
final java.lang.String activatedPackage;
final java.lang.String sourcePackage;
final Integer userId;
/* # direct methods */
private com.android.server.pm.PackageEventRecorder$ActivationRecord ( ) {
/* .locals 0 */
/* .param p1, "activatedPackage" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "sourcePackage" # Ljava/lang/String; */
/* .line 818 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 819 */
this.activatedPackage = p1;
/* .line 820 */
/* iput p2, p0, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;->userId:I */
/* .line 821 */
this.sourcePackage = p3;
/* .line 822 */
return;
} // .end method
 com.android.server.pm.PackageEventRecorder$ActivationRecord ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;-><init>(Ljava/lang/String;ILjava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 826 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ActivationRecord{activatedPackage=\'"; // const-string v1, "ActivationRecord{activatedPackage=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.activatedPackage;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", userId="; // const-string v2, ", userId="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/pm/PackageEventRecorder$ActivationRecord;->userId:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", sourcePackage=\'"; // const-string v2, ", sourcePackage=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.sourcePackage;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
