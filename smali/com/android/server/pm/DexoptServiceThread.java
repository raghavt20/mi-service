public class com.android.server.pm.DexoptServiceThread extends java.lang.Thread {
	 /* .source "DexoptServiceThread.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String HOST_NAME;
public static final Integer SOCKET_BUFFER_SIZE;
public static final java.lang.String TAG;
private static java.lang.Object mWaitLock;
private static volatile android.os.Handler sAsynDexOptHandler;
private static java.lang.Object sAsynDexOptLock;
private static android.os.HandlerThread sAsynDexOptThread;
/* # instance fields */
private com.android.server.pm.DexOptHelper mDexOptHelper;
private java.util.List mDexoptPackageNameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mDexoptResult;
public java.util.concurrent.ConcurrentHashMap mDexoptSecondaryPath;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mDexoptSecondaryResult;
private com.android.server.pm.PackageDexOptimizer mPdo;
private com.android.server.pm.PackageManagerService mPms;
public Integer secondaryId;
/* # direct methods */
static java.util.List -$$Nest$fgetmDexoptPackageNameList ( com.android.server.pm.DexoptServiceThread p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDexoptPackageNameList;
} // .end method
static Integer -$$Nest$fgetmDexoptResult ( com.android.server.pm.DexoptServiceThread p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I */
} // .end method
static Integer -$$Nest$fgetmDexoptSecondaryResult ( com.android.server.pm.DexoptServiceThread p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryResult:I */
} // .end method
static com.android.server.pm.PackageDexOptimizer -$$Nest$fgetmPdo ( com.android.server.pm.DexoptServiceThread p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPdo;
} // .end method
static void -$$Nest$fputmDexoptResult ( com.android.server.pm.DexoptServiceThread p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I */
return;
} // .end method
static void -$$Nest$fputmDexoptSecondaryResult ( com.android.server.pm.DexoptServiceThread p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryResult:I */
return;
} // .end method
static Integer -$$Nest$mperformDexOptInternal ( com.android.server.pm.DexoptServiceThread p0, com.android.server.pm.dex.DexoptOptions p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/DexoptServiceThread;->performDexOptInternal(Lcom/android/server/pm/dex/DexoptOptions;)I */
} // .end method
static java.lang.Object -$$Nest$sfgetmWaitLock ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.pm.DexoptServiceThread.mWaitLock;
} // .end method
static com.android.server.pm.DexoptServiceThread ( ) {
/* .locals 1 */
/* .line 41 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 154 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public com.android.server.pm.DexoptServiceThread ( ) {
/* .locals 2 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p2, "pdo" # Lcom/android/server/pm/PackageDexOptimizer; */
/* .line 46 */
/* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
/* .line 34 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I */
/* .line 36 */
/* iput v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I */
/* .line 37 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mDexoptPackageNameList = v1;
/* .line 43 */
/* new-instance v1, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mDexoptSecondaryPath = v1;
/* .line 44 */
/* iput v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryResult:I */
/* .line 47 */
this.mPms = p1;
/* .line 48 */
this.mPdo = p2;
/* .line 49 */
/* new-instance v0, Lcom/android/server/pm/DexOptHelper; */
v1 = this.mPms;
/* invoke-direct {v0, v1}, Lcom/android/server/pm/DexOptHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V */
this.mDexOptHelper = v0;
/* .line 50 */
return;
} // .end method
private static android.os.Handler getAsynDexOptHandler ( ) {
/* .locals 3 */
/* .line 156 */
v0 = com.android.server.pm.DexoptServiceThread.sAsynDexOptHandler;
/* if-nez v0, :cond_1 */
/* .line 157 */
v0 = com.android.server.pm.DexoptServiceThread.sAsynDexOptLock;
/* monitor-enter v0 */
/* .line 158 */
try { // :try_start_0
v1 = com.android.server.pm.DexoptServiceThread.sAsynDexOptHandler;
/* if-nez v1, :cond_0 */
/* .line 159 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "asyn_dexopt_thread"; // const-string v2, "asyn_dexopt_thread"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 160 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 161 */
/* new-instance v1, Landroid/os/Handler; */
v2 = com.android.server.pm.DexoptServiceThread.sAsynDexOptThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 163 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 165 */
} // :cond_1
} // :goto_0
v0 = com.android.server.pm.DexoptServiceThread.sAsynDexOptHandler;
} // .end method
private Integer performDexOptInternal ( com.android.server.pm.dex.DexoptOptions p0 ) {
/* .locals 6 */
/* .param p1, "options" # Lcom/android/server/pm/dex/DexoptOptions; */
/* .line 209 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mDexOptHelper;
(( java.lang.Object ) v1 ).getClass ( ); // invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 210 */
/* .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
final String v2 = "performDexOptInternal"; // const-string v2, "performDexOptInternal"
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
/* const-class v5, Lcom/android/server/pm/dex/DexoptOptions; */
/* aput-object v5, v4, v0 */
/* .line 211 */
(( java.lang.Class ) v1 ).getDeclaredMethod ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 212 */
/* .local v2, "declaredMethod":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 213 */
v3 = this.mDexOptHelper;
/* filled-new-array {p1}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v2 ).invoke ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 214 */
} // .end local v1 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
} // .end local v2 # "declaredMethod":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v1 */
/* .line 215 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Exception: "; // const-string v3, "Exception: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "DexoptServiceThread"; // const-string v3, "DexoptServiceThread"
android.util.Slog .w ( v3,v2 );
/* .line 216 */
} // .end method
private void waitDexOptRequest ( ) {
/* .locals 9 */
/* .line 169 */
v0 = com.android.server.pm.DexoptServiceThread.mWaitLock;
/* monitor-enter v0 */
/* .line 170 */
try { // :try_start_0
final String v1 = "DexoptServiceThread"; // const-string v1, "DexoptServiceThread"
final String v2 = "Start to wait dexopt result."; // const-string v2, "Start to wait dexopt result."
android.util.Slog .d ( v1,v2 );
/* .line 171 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 173 */
/* .local v1, "startTime":J */
try { // :try_start_1
v3 = com.android.server.pm.DexoptServiceThread.mWaitLock;
(( java.lang.Object ) v3 ).wait ( ); // invoke-virtual {v3}, Ljava/lang/Object;->wait()V
/* :try_end_1 */
/* .catch Ljava/lang/InterruptedException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 177 */
/* .line 174 */
/* :catch_0 */
/* move-exception v3 */
/* .line 175 */
/* .local v3, "e":Ljava/lang/InterruptedException; */
try { // :try_start_2
(( java.lang.InterruptedException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V
/* .line 176 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v4 ).interrupt ( ); // invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V
/* .line 178 */
} // .end local v3 # "e":Ljava/lang/InterruptedException;
} // :goto_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* .line 179 */
/* .local v3, "endTime":J */
final String v5 = "DexoptServiceThread"; // const-string v5, "DexoptServiceThread"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Dexopt finished, waiting time("; // const-string v7, "Dexopt finished, waiting time("
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sub-long v7, v3, v1 */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms), result:"; // const-string v7, "ms), result:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 181 */
/* nop */
} // .end local v1 # "startTime":J
} // .end local v3 # "endTime":J
/* monitor-exit v0 */
/* .line 182 */
return;
/* .line 181 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public Integer dexOptSecondaryDexPath ( android.content.pm.ApplicationInfo p0, java.lang.String p1, Integer p2, com.android.server.pm.dex.PackageDexUsage$DexUseInfo p3, com.android.server.pm.dex.DexoptOptions p4 ) {
/* .locals 10 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "secondaryId" # I */
/* .param p4, "dexUseInfo" # Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo; */
/* .param p5, "options" # Lcom/android/server/pm/dex/DexoptOptions; */
/* .line 252 */
int v0 = 5; // const/4 v0, 0x5
/* new-array v1, v0, [Ljava/lang/Class; */
/* .line 253 */
/* .local v1, "arg":[Ljava/lang/Class; */
/* const-class v2, Landroid/content/pm/ApplicationInfo; */
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
/* .line 254 */
/* const-class v2, Ljava/lang/String; */
int v4 = 1; // const/4 v4, 0x1
/* aput-object v2, v1, v4 */
/* .line 255 */
v2 = java.lang.Integer.TYPE;
int v5 = 2; // const/4 v5, 0x2
/* aput-object v2, v1, v5 */
/* .line 256 */
/* const-class v2, Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo; */
int v6 = 3; // const/4 v6, 0x3
/* aput-object v2, v1, v6 */
/* .line 257 */
/* const-class v2, Lcom/android/server/pm/dex/DexoptOptions; */
int v7 = 4; // const/4 v7, 0x4
/* aput-object v2, v1, v7 */
/* .line 259 */
try { // :try_start_0
v2 = this.mPdo;
(( java.lang.Object ) v2 ).getClass ( ); // invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 260 */
/* .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;" */
final String v8 = "dexOptSecondaryDexPath"; // const-string v8, "dexOptSecondaryDexPath"
/* .line 261 */
(( java.lang.Class ) v2 ).getDeclaredMethod ( v8, v1 ); // invoke-virtual {v2, v8, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 262 */
/* .local v8, "declaredMethod":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v8 ).setAccessible ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 263 */
v9 = this.mPdo;
/* new-array v0, v0, [Ljava/lang/Object; */
/* aput-object p1, v0, v3 */
/* aput-object p2, v0, v4 */
java.lang.Integer .valueOf ( p3 );
/* aput-object v4, v0, v5 */
/* aput-object p4, v0, v6 */
/* aput-object p5, v0, v7 */
(( java.lang.reflect.Method ) v8 ).invoke ( v9, v0 ); // invoke-virtual {v8, v9, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 264 */
} // .end local v2 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Object;>;"
} // .end local v8 # "declaredMethod":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v0 */
/* .line 265 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Exception: "; // const-string v4, "Exception: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "DexoptServiceThread"; // const-string v4, "DexoptServiceThread"
android.util.Slog .w ( v4,v2 );
/* .line 266 */
} // .end method
public Integer getDexOptResult ( ) {
/* .locals 1 */
/* .line 275 */
/* iget v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptResult:I */
} // .end method
public Integer getDexoptSecondaryResult ( ) {
/* .locals 1 */
/* .line 271 */
/* iget v0, p0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryResult:I */
} // .end method
public void performDexOptAsyncTask ( com.android.server.pm.dex.DexoptOptions p0 ) {
/* .locals 2 */
/* .param p1, "options" # Lcom/android/server/pm/dex/DexoptOptions; */
/* .line 185 */
/* new-instance v0, Lcom/android/server/pm/DexoptServiceThread$1; */
/* invoke-direct {v0, p0, p1}, Lcom/android/server/pm/DexoptServiceThread$1;-><init>(Lcom/android/server/pm/DexoptServiceThread;Lcom/android/server/pm/dex/DexoptOptions;)V */
/* .line 203 */
/* .local v0, "dexoptTask":Ljava/lang/Runnable; */
com.android.server.pm.DexoptServiceThread .getAsynDexOptHandler ( );
(( android.os.Handler ) v1 ).post ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 204 */
/* invoke-direct {p0}, Lcom/android/server/pm/DexoptServiceThread;->waitDexOptRequest()V */
/* .line 205 */
return;
} // .end method
public void performDexOptSecondary ( android.content.pm.ApplicationInfo p0, java.lang.String p1, com.android.server.pm.dex.PackageDexUsage$DexUseInfo p2, com.android.server.pm.dex.DexoptOptions p3 ) {
/* .locals 7 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "dexUseInfo" # Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo; */
/* .param p4, "options" # Lcom/android/server/pm/dex/DexoptOptions; */
/* .line 222 */
/* new-instance v6, Lcom/android/server/pm/DexoptServiceThread$2; */
/* move-object v0, v6 */
/* move-object v1, p0 */
/* move-object v2, p2 */
/* move-object v3, p1 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/pm/DexoptServiceThread$2;-><init>(Lcom/android/server/pm/DexoptServiceThread;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V */
/* .line 246 */
/* .local v0, "task":Ljava/lang/Runnable; */
com.android.server.pm.DexoptServiceThread .getAsynDexOptHandler ( );
(( android.os.Handler ) v1 ).post ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 247 */
/* invoke-direct {p0}, Lcom/android/server/pm/DexoptServiceThread;->waitDexOptRequest()V */
/* .line 248 */
return;
} // .end method
public void run ( ) {
/* .locals 7 */
/* .line 54 */
final String v0 = "mi_dexopt connection finally shutdown."; // const-string v0, "mi_dexopt connection finally shutdown."
final String v1 = "DexoptServiceThread"; // const-string v1, "DexoptServiceThread"
int v2 = 0; // const/4 v2, 0x0
/* .line 55 */
/* .local v2, "serverSocket":Landroid/net/LocalServerSocket; */
java.util.concurrent.Executors .newCachedThreadPool ( );
/* .line 57 */
/* .local v3, "threadExecutor":Ljava/util/concurrent/ExecutorService; */
try { // :try_start_0
final String v4 = "Create local socket: mi_dexopt"; // const-string v4, "Create local socket: mi_dexopt"
android.util.Slog .w ( v1,v4 );
/* .line 58 */
/* new-instance v4, Landroid/net/LocalServerSocket; */
final String v5 = "mi_dexopt"; // const-string v5, "mi_dexopt"
/* invoke-direct {v4, v5}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V */
/* move-object v2, v4 */
/* .line 60 */
} // :goto_0
final String v4 = "Waiting dexopt client connected..."; // const-string v4, "Waiting dexopt client connected..."
android.util.Slog .i ( v1,v4 );
/* .line 61 */
(( android.net.LocalServerSocket ) v2 ).accept ( ); // invoke-virtual {v2}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;
/* .line 62 */
/* .local v4, "clientSocket":Landroid/net/LocalSocket; */
/* const/16 v5, 0x100 */
(( android.net.LocalSocket ) v4 ).setReceiveBufferSize ( v5 ); // invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->setReceiveBufferSize(I)V
/* .line 63 */
(( android.net.LocalSocket ) v4 ).setSendBufferSize ( v5 ); // invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->setSendBufferSize(I)V
/* .line 64 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "There is a dexopt client is accepted:"; // const-string v6, "There is a dexopt client is accepted:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.net.LocalSocket ) v4 ).toString ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v5 );
/* .line 65 */
/* new-instance v5, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler; */
/* invoke-direct {v5, p0, v4}, Lcom/android/server/pm/DexoptServiceThread$ConnectionHandler;-><init>(Lcom/android/server/pm/DexoptServiceThread;Landroid/net/LocalSocket;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 66 */
} // .end local v4 # "clientSocket":Landroid/net/LocalSocket;
/* .line 70 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 67 */
/* :catch_0 */
/* move-exception v4 */
/* .line 68 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "mi_dexopt connection catch Exception: "; // const-string v6, "mi_dexopt connection catch Exception: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v5 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 70 */
/* nop */
} // .end local v4 # "e":Ljava/lang/Exception;
android.util.Slog .w ( v1,v0 );
/* .line 71 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 72 */
} // :cond_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 74 */
try { // :try_start_2
(( android.net.LocalServerSocket ) v2 ).close ( ); // invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 77 */
} // :goto_1
/* .line 75 */
/* :catch_1 */
/* move-exception v0 */
/* .line 76 */
/* .local v0, "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
} // .end local v0 # "e":Ljava/io/IOException;
/* .line 80 */
} // :cond_1
} // :goto_2
return;
/* .line 70 */
} // :goto_3
android.util.Slog .w ( v1,v0 );
/* .line 71 */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 72 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 74 */
try { // :try_start_3
(( android.net.LocalServerSocket ) v2 ).close ( ); // invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 77 */
/* .line 75 */
/* :catch_2 */
/* move-exception v0 */
/* .line 76 */
/* .restart local v0 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 79 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_3
} // :goto_4
/* throw v4 */
} // .end method
