class com.android.server.pm.InstallerUtil {
	 /* .source "InstallerUtil.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;, */
	 /* Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # direct methods */
 com.android.server.pm.InstallerUtil ( ) {
	 /* .locals 0 */
	 /* .line 46 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
static Boolean deleteApp ( android.content.pm.IPackageManager p0, java.lang.String p1, Boolean p2 ) {
	 /* .locals 7 */
	 /* .param p0, "pm" # Landroid/content/pm/IPackageManager; */
	 /* .param p1, "pkgName" # Ljava/lang/String; */
	 /* .param p2, "keepData" # Z */
	 /* .line 69 */
	 int v0 = 2; // const/4 v0, 0x2
	 /* .line 70 */
	 /* .local v0, "flags":I */
	 if ( p2 != null) { // if-eqz p2, :cond_0
		 /* .line 71 */
		 /* or-int/lit8 v0, v0, 0x1 */
		 /* .line 73 */
	 } // :cond_0
	 /* new-instance v1, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* invoke-direct {v1, v2}, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;-><init>(Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver-IA;)V */
	 /* .line 75 */
	 /* .local v1, "obs":Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver; */
	 int v2 = 0; // const/4 v2, 0x0
	 try { // :try_start_0
		 /* new-instance v3, Landroid/content/pm/VersionedPackage; */
		 int v4 = -1; // const/4 v4, -0x1
		 /* invoke-direct {v3, p1, v4}, Landroid/content/pm/VersionedPackage;-><init>(Ljava/lang/String;I)V */
		 /* :try_end_0 */
		 /* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_1 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
		 /* .line 80 */
		 /* nop */
		 /* .line 81 */
		 /* monitor-enter v1 */
		 /* .line 82 */
	 } // :goto_0
	 try { // :try_start_1
		 /* iget-boolean v2, v1, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;->finished:Z */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* if-nez v2, :cond_1 */
		 /* .line 84 */
		 try { // :try_start_2
			 (( java.lang.Object ) v1 ).wait ( ); // invoke-virtual {v1}, Ljava/lang/Object;->wait()V
			 /* :try_end_2 */
			 /* .catch Ljava/lang/InterruptedException; {:try_start_2 ..:try_end_2} :catch_0 */
			 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
			 /* .line 86 */
		 } // :goto_1
		 /* .line 85 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 88 */
	 } // :cond_1
	 try { // :try_start_3
		 /* monitor-exit v1 */
		 /* :try_end_3 */
		 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
		 /* .line 89 */
		 /* iget-boolean v2, v1, Lcom/android/server/pm/InstallerUtil$PackageDeleteObserver;->result:Z */
		 /* .line 88 */
		 /* :catchall_0 */
		 /* move-exception v2 */
		 try { // :try_start_4
			 /* monitor-exit v1 */
			 /* :try_end_4 */
			 /* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
			 /* throw v2 */
			 /* .line 77 */
			 /* :catch_1 */
			 /* move-exception v3 */
			 /* .line 78 */
			 /* .local v3, "e":Ljava/lang/Exception; */
			 final String v4 = "InstallerUtil"; // const-string v4, "InstallerUtil"
			 /* new-instance v5, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v6 = "Failed to delete "; // const-string v6, "Failed to delete "
			 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .e ( v4,v5,v3 );
			 /* .line 79 */
		 } // .end method
		 private static void doAandonSession ( android.content.Context p0, Integer p1 ) {
			 /* .locals 5 */
			 /* .param p0, "context" # Landroid/content/Context; */
			 /* .param p1, "sessionId" # I */
			 /* .line 295 */
			 (( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
			 (( android.content.pm.PackageManager ) v0 ).getPackageInstaller ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
			 /* .line 296 */
			 /* .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller; */
			 int v1 = 0; // const/4 v1, 0x0
			 /* .line 298 */
			 /* .local v1, "session":Landroid/content/pm/PackageInstaller$Session; */
			 try { // :try_start_0
				 (( android.content.pm.PackageInstaller ) v0 ).openSession ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;
				 /* move-object v1, v2 */
				 /* .line 299 */
				 (( android.content.pm.PackageInstaller$Session ) v1 ).abandon ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageInstaller$Session;->abandon()V
				 /* :try_end_0 */
				 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* .line 303 */
				 /* nop */
			 } // :goto_0
			 libcore.io.IoUtils .closeQuietly ( v1 );
			 /* .line 304 */
			 /* .line 303 */
			 /* :catchall_0 */
			 /* move-exception v2 */
			 /* .line 300 */
			 /* :catch_0 */
			 /* move-exception v2 */
			 /* .line 301 */
			 /* .local v2, "e":Ljava/io/IOException; */
			 try { // :try_start_1
				 final String v3 = "InstallerUtil"; // const-string v3, "InstallerUtil"
				 final String v4 = "doAandonSession failed: "; // const-string v4, "doAandonSession failed: "
				 android.util.Slog .e ( v3,v4,v2 );
				 /* :try_end_1 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* .line 303 */
				 /* nop */
			 } // .end local v2 # "e":Ljava/io/IOException;
			 /* .line 305 */
		 } // :goto_1
		 return;
		 /* .line 303 */
	 } // :goto_2
	 libcore.io.IoUtils .closeQuietly ( v1 );
	 /* .line 304 */
	 /* throw v2 */
} // .end method
private static Boolean doCommitSession ( android.content.Context p0, Integer p1, android.content.IntentSender p2 ) {
	 /* .locals 5 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .param p1, "sessionId" # I */
	 /* .param p2, "target" # Landroid/content/IntentSender; */
	 /* .line 183 */
	 (( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
	 (( android.content.pm.PackageManager ) v0 ).getPackageInstaller ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
	 /* .line 184 */
	 /* .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 186 */
	 /* .local v1, "session":Landroid/content/pm/PackageInstaller$Session; */
	 try { // :try_start_0
		 (( android.content.pm.PackageInstaller ) v0 ).openSession ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;
		 /* move-object v1, v2 */
		 /* .line 187 */
		 (( android.content.pm.PackageInstaller$Session ) v1 ).commit ( p2 ); // invoke-virtual {v1, p2}, Landroid/content/pm/PackageInstaller$Session;->commit(Landroid/content/IntentSender;)V
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 188 */
		 /* nop */
		 /* .line 192 */
		 libcore.io.IoUtils .closeQuietly ( v1 );
		 /* .line 188 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* .line 192 */
		 /* :catchall_0 */
		 /* move-exception v2 */
		 /* .line 189 */
		 /* :catch_0 */
		 /* move-exception v2 */
		 /* .line 190 */
		 /* .local v2, "e":Ljava/lang/Exception; */
		 try { // :try_start_1
			 final String v3 = "InstallerUtil"; // const-string v3, "InstallerUtil"
			 final String v4 = "doCommitSession failed: "; // const-string v4, "doCommitSession failed: "
			 android.util.Slog .e ( v3,v4,v2 );
			 /* :try_end_1 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 192 */
			 /* nop */
		 } // .end local v2 # "e":Ljava/lang/Exception;
		 libcore.io.IoUtils .closeQuietly ( v1 );
		 /* .line 193 */
		 /* nop */
		 /* .line 194 */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .line 192 */
	 } // :goto_0
	 libcore.io.IoUtils .closeQuietly ( v1 );
	 /* .line 193 */
	 /* throw v2 */
} // .end method
private static Boolean doCommitSession ( android.content.Context p0, java.io.File p1, Integer p2, android.content.IntentSender p3 ) {
	 /* .locals 16 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .param p1, "apkFile" # Ljava/io/File; */
	 /* .param p2, "sessionId" # I */
	 /* .param p3, "intentSender" # Landroid/content/IntentSender; */
	 /* .line 267 */
	 /* move-object/from16 v1, p1 */
	 final String v2 = "InstallerUtil"; // const-string v2, "InstallerUtil"
	 int v3 = 0; // const/4 v3, 0x0
	 /* .line 268 */
	 /* .local v3, "session":Landroid/content/pm/PackageInstaller$Session; */
	 int v4 = 0; // const/4 v4, 0x0
	 /* .line 269 */
	 /* .local v4, "pfd":Landroid/os/ParcelFileDescriptor; */
	 android.os.Binder .clearCallingIdentity ( );
	 /* move-result-wide v5 */
	 /* .line 271 */
	 /* .local v5, "iden":J */
	 /* const/high16 v0, 0x10000000 */
	 int v7 = 0; // const/4 v7, 0x0
	 try { // :try_start_0
		 android.os.ParcelFileDescriptor .open ( v1,v0 );
		 /* move-object v4, v0 */
		 /* .line 272 */
		 /* if-nez v4, :cond_0 */
		 /* .line 273 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v8 = "doWriteSession failed: can\'t open "; // const-string v8, "doWriteSession failed: can\'t open "
		 (( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .e ( v2,v0 );
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
		 /* .line 274 */
		 /* nop */
		 /* .line 287 */
		 libcore.io.IoUtils .closeQuietly ( v4 );
		 /* .line 288 */
		 libcore.io.IoUtils .closeQuietly ( v3 );
		 /* .line 289 */
		 android.os.Binder .restoreCallingIdentity ( v5,v6 );
		 /* .line 274 */
		 /* .line 276 */
	 } // :cond_0
	 try { // :try_start_1
		 /* invoke-virtual/range {p0 ..p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager; */
		 (( android.content.pm.PackageManager ) v0 ).getPackageInstaller ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
		 /* :try_end_1 */
		 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
		 /* move/from16 v15, p2 */
		 try { // :try_start_2
			 (( android.content.pm.PackageInstaller ) v0 ).openSession ( v15 ); // invoke-virtual {v0, v15}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;
			 /* move-object v3, v0 */
			 /* .line 277 */
			 final String v9 = "base.apk"; // const-string v9, "base.apk"
			 /* const-wide/16 v10, 0x0 */
			 (( android.os.ParcelFileDescriptor ) v4 ).getStatSize ( ); // invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
			 /* move-result-wide v12 */
			 /* move-object v8, v3 */
			 /* move-object v14, v4 */
			 /* invoke-virtual/range {v8 ..v14}, Landroid/content/pm/PackageInstaller$Session;->write(Ljava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V */
			 /* .line 278 */
			 (( android.os.ParcelFileDescriptor ) v4 ).close ( ); // invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
			 /* :try_end_2 */
			 /* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_1 */
			 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
			 /* .line 279 */
			 /* move-object/from16 v8, p3 */
			 try { // :try_start_3
				 (( android.content.pm.PackageInstaller$Session ) v3 ).commit ( v8 ); // invoke-virtual {v3, v8}, Landroid/content/pm/PackageInstaller$Session;->commit(Landroid/content/IntentSender;)V
				 /* :try_end_3 */
				 /* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
				 /* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
				 /* .line 280 */
				 /* nop */
				 /* .line 287 */
				 libcore.io.IoUtils .closeQuietly ( v4 );
				 /* .line 288 */
				 libcore.io.IoUtils .closeQuietly ( v3 );
				 /* .line 289 */
				 android.os.Binder .restoreCallingIdentity ( v5,v6 );
				 /* .line 280 */
				 int v0 = 1; // const/4 v0, 0x1
				 /* .line 281 */
				 /* :catch_0 */
				 /* move-exception v0 */
				 /* .line 287 */
				 /* :catchall_0 */
				 /* move-exception v0 */
				 /* .line 281 */
				 /* :catch_1 */
				 /* move-exception v0 */
				 /* .line 287 */
				 /* :catchall_1 */
				 /* move-exception v0 */
				 /* move/from16 v15, p2 */
			 } // :goto_0
			 /* move-object/from16 v8, p3 */
			 /* .line 281 */
			 /* :catch_2 */
			 /* move-exception v0 */
			 /* move/from16 v15, p2 */
		 } // :goto_1
		 /* move-object/from16 v8, p3 */
		 /* .line 282 */
		 /* .local v0, "e":Ljava/lang/Exception; */
	 } // :goto_2
	 try { // :try_start_4
		 final String v9 = "doWriteSession failed"; // const-string v9, "doWriteSession failed"
		 android.util.Slog .e ( v2,v9,v0 );
		 /* .line 283 */
		 if ( v3 != null) { // if-eqz v3, :cond_1
			 /* .line 284 */
			 (( android.content.pm.PackageInstaller$Session ) v3 ).abandon ( ); // invoke-virtual {v3}, Landroid/content/pm/PackageInstaller$Session;->abandon()V
			 /* :try_end_4 */
			 /* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
			 /* .line 287 */
		 } // .end local v0 # "e":Ljava/lang/Exception;
	 } // :cond_1
	 libcore.io.IoUtils .closeQuietly ( v4 );
	 /* .line 288 */
	 libcore.io.IoUtils .closeQuietly ( v3 );
	 /* .line 289 */
	 android.os.Binder .restoreCallingIdentity ( v5,v6 );
	 /* .line 290 */
	 /* nop */
	 /* .line 291 */
	 /* .line 287 */
	 /* :catchall_2 */
	 /* move-exception v0 */
} // :goto_3
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 288 */
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 289 */
android.os.Binder .restoreCallingIdentity ( v5,v6 );
/* .line 290 */
/* throw v0 */
} // .end method
private static Integer doCreateSession ( android.content.Context p0, android.content.pm.PackageInstaller$SessionParams p1 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "sessionParams" # Landroid/content/pm/PackageInstaller$SessionParams; */
/* .line 254 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackageInstaller ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
/* .line 255 */
/* .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller; */
int v1 = 0; // const/4 v1, 0x0
/* .line 257 */
/* .local v1, "sessionId":I */
try { // :try_start_0
	 v2 = 	 (( android.content.pm.PackageInstaller ) v0 ).createSession ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->createSession(Landroid/content/pm/PackageInstaller$SessionParams;)I
	 /* :try_end_0 */
	 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* move v1, v2 */
	 /* .line 261 */
	 /* .line 259 */
	 /* :catch_0 */
	 /* move-exception v2 */
	 /* .line 260 */
	 /* .local v2, "e":Ljava/io/IOException; */
	 final String v3 = "InstallerUtil"; // const-string v3, "InstallerUtil"
	 final String v4 = "doCreateSession failed: "; // const-string v4, "doCreateSession failed: "
	 android.util.Slog .e ( v3,v4,v2 );
	 /* .line 262 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_0
} // .end method
private static Boolean doWriteSession ( android.content.Context p0, java.lang.String p1, java.io.File p2, Integer p3 ) {
/* .locals 10 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "apkFile" # Ljava/io/File; */
/* .param p3, "sessionId" # I */
/* .line 166 */
int v0 = 0; // const/4 v0, 0x0
/* .line 167 */
/* .local v0, "session":Landroid/content/pm/PackageInstaller$Session; */
int v1 = 0; // const/4 v1, 0x0
/* .line 169 */
/* .local v1, "pfd":Landroid/os/ParcelFileDescriptor; */
/* const/high16 v2, 0x10000000 */
try { // :try_start_0
android.os.ParcelFileDescriptor .open ( p2,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 170 */
} // .end local v1 # "pfd":Landroid/os/ParcelFileDescriptor;
/* .local v9, "pfd":Landroid/os/ParcelFileDescriptor; */
try { // :try_start_1
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v1 ).getPackageInstaller ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
(( android.content.pm.PackageInstaller ) v1 ).openSession ( p3 ); // invoke-virtual {v1, p3}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 171 */
} // .end local v0 # "session":Landroid/content/pm/PackageInstaller$Session;
/* .local v3, "session":Landroid/content/pm/PackageInstaller$Session; */
/* const-wide/16 v5, 0x0 */
try { // :try_start_2
(( android.os.ParcelFileDescriptor ) v9 ).getStatSize ( ); // invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
/* move-result-wide v7 */
/* move-object v4, p1 */
/* invoke-virtual/range {v3 ..v9}, Landroid/content/pm/PackageInstaller$Session;->write(Ljava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 172 */
/* nop */
/* .line 176 */
libcore.io.IoUtils .closeQuietly ( v9 );
/* .line 177 */
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 172 */
int v0 = 1; // const/4 v0, 0x1
/* .line 176 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object v1, v9 */
/* .line 173 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v1, v9 */
/* .line 176 */
} // .end local v3 # "session":Landroid/content/pm/PackageInstaller$Session;
/* .restart local v0 # "session":Landroid/content/pm/PackageInstaller$Session; */
/* :catchall_1 */
/* move-exception v1 */
/* move-object v3, v0 */
/* move-object v0, v1 */
/* move-object v1, v9 */
/* .line 173 */
/* :catch_1 */
/* move-exception v1 */
/* move-object v3, v0 */
/* move-object v0, v1 */
/* move-object v1, v9 */
/* .line 176 */
} // .end local v9 # "pfd":Landroid/os/ParcelFileDescriptor;
/* .restart local v1 # "pfd":Landroid/os/ParcelFileDescriptor; */
/* :catchall_2 */
/* move-exception v2 */
/* move-object v3, v0 */
/* move-object v0, v2 */
/* .line 173 */
/* :catch_2 */
/* move-exception v2 */
/* move-object v3, v0 */
/* move-object v0, v2 */
/* .line 174 */
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v3 # "session":Landroid/content/pm/PackageInstaller$Session; */
} // :goto_0
try { // :try_start_3
final String v2 = "InstallerUtil"; // const-string v2, "InstallerUtil"
final String v4 = "doWriteSession failed: "; // const-string v4, "doWriteSession failed: "
android.util.Slog .e ( v2,v4,v0 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_3 */
/* .line 176 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 177 */
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 178 */
/* nop */
/* .line 179 */
int v0 = 0; // const/4 v0, 0x0
/* .line 176 */
/* :catchall_3 */
/* move-exception v0 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 177 */
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 178 */
/* throw v0 */
} // .end method
static java.util.Map installAppList ( android.content.Context p0, java.util.Collection p1 ) {
/* .locals 13 */
/* .param p0, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/util/Collection<", */
/* "Ljava/io/File;", */
/* ">;)", */
/* "Ljava/util/Map<", */
/* "Ljava/io/File;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 200 */
/* .local p1, "apkFileList":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/io/File;>;" */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 201 */
v1 = /* .local v0, "installedResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/io/File;Ljava/lang/Integer;>;" */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 202 */
/* .line 204 */
} // :cond_0
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
/* .line 205 */
/* .local v1, "sessions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/io/File;>;" */
v3 = /* new-instance v2, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver; */
/* invoke-direct {v2, v3}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;-><init>(I)V */
/* .line 206 */
/* .local v2, "receiver":Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver; */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 208 */
/* .local v3, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
v5 = } // :goto_0
final String v6 = "InstallerUtil"; // const-string v6, "InstallerUtil"
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Ljava/io/File; */
/* .line 209 */
/* .local v5, "apkFile":Ljava/io/File; */
com.android.server.pm.InstallerUtil .parsePackageLite ( v5 );
/* .line 210 */
/* .local v7, "apkLite":Landroid/content/pm/parsing/PackageLite; */
/* if-nez v7, :cond_1 */
/* .line 211 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Failed to installAppList: "; // const-string v9, "Failed to installAppList: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v8 );
/* .line 212 */
/* .line 215 */
} // :cond_1
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v8 = (( java.util.HashSet ) v3 ).contains ( v8 ); // invoke-virtual {v3, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 216 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Failed to installApp: "; // const-string v9, "Failed to installApp: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v9 = ", duplicate package name, version: "; // const-string v9, ", duplicate package name, version: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 217 */
v9 = (( android.content.pm.parsing.PackageLite ) v7 ).getVersionCode ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 216 */
android.util.Slog .e ( v6,v8 );
/* .line 218 */
/* .line 220 */
} // :cond_2
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( java.util.HashSet ) v3 ).add ( v6 ); // invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 222 */
com.android.server.pm.InstallerUtil .makeSessionParams ( v7 );
/* .line 223 */
/* .local v6, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams; */
v8 = com.android.server.pm.InstallerUtil .doCreateSession ( p0,v6 );
/* .line 224 */
/* .local v8, "sessionId":I */
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 225 */
(( com.android.server.pm.InstallerUtil$LocalIntentReceiver ) v2 ).getIntentSender ( ); // invoke-virtual {v2}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;->getIntentSender()Landroid/content/IntentSender;
v9 = com.android.server.pm.InstallerUtil .doCommitSession ( p0,v5,v8,v9 );
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 226 */
(( android.util.SparseArray ) v1 ).put ( v8, v5 ); // invoke-virtual {v1, v8, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 228 */
} // .end local v5 # "apkFile":Ljava/io/File;
} // .end local v6 # "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
} // .end local v7 # "apkLite":Landroid/content/pm/parsing/PackageLite;
} // .end local v8 # "sessionId":I
} // :cond_3
/* .line 230 */
} // :cond_4
v4 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* .line 231 */
/* .local v4, "size":I */
} // :goto_1
/* if-lez v4, :cond_7 */
/* .line 232 */
(( com.android.server.pm.InstallerUtil$LocalIntentReceiver ) v2 ).getResult ( ); // invoke-virtual {v2}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;->getResult()Landroid/content/Intent;
/* .line 233 */
/* .local v5, "result":Landroid/content/Intent; */
final String v7 = "android.content.pm.extra.SESSION_ID"; // const-string v7, "android.content.pm.extra.SESSION_ID"
int v8 = 0; // const/4 v8, 0x0
v7 = (( android.content.Intent ) v5 ).getIntExtra ( v7, v8 ); // invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 234 */
/* .local v7, "sessionId":I */
v8 = (( android.util.SparseArray ) v1 ).indexOfKey ( v7 ); // invoke-virtual {v1, v7}, Landroid/util/SparseArray;->indexOfKey(I)I
/* if-gez v8, :cond_5 */
/* .line 235 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "InstallApp received invalid sessionId:"; // const-string v9, "InstallApp received invalid sessionId:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v8 );
/* .line 236 */
/* .line 238 */
} // :cond_5
final String v8 = "android.content.pm.extra.STATUS"; // const-string v8, "android.content.pm.extra.STATUS"
int v9 = 1; // const/4 v9, 0x1
v8 = (( android.content.Intent ) v5 ).getIntExtra ( v8, v9 ); // invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 240 */
/* .local v8, "status":I */
(( android.util.SparseArray ) v1 ).get ( v7 ); // invoke-virtual {v1, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v9, Ljava/io/File; */
java.lang.Integer .valueOf ( v8 );
/* .line 241 */
if ( v8 != null) { // if-eqz v8, :cond_6
/* .line 242 */
final String v9 = "android.content.pm.extra.PACKAGE_NAME"; // const-string v9, "android.content.pm.extra.PACKAGE_NAME"
(( android.content.Intent ) v5 ).getStringExtra ( v9 ); // invoke-virtual {v5, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 243 */
/* .local v9, "packageName":Ljava/lang/String; */
final String v10 = "android.content.pm.extra.STATUS_MESSAGE"; // const-string v10, "android.content.pm.extra.STATUS_MESSAGE"
(( android.content.Intent ) v5 ).getStringExtra ( v10 ); // invoke-virtual {v5, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 244 */
/* .local v10, "errorMsg":Ljava/lang/String; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "InstallApp failed for id:"; // const-string v12, "InstallApp failed for id:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v7 ); // invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v12 = " pkg:"; // const-string v12, " pkg:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " status:"; // const-string v12, " status:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v8 ); // invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v12 = " msg:"; // const-string v12, " msg:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v11 );
/* .line 248 */
} // .end local v9 # "packageName":Ljava/lang/String;
} // .end local v10 # "errorMsg":Ljava/lang/String;
} // :cond_6
/* nop */
} // .end local v5 # "result":Landroid/content/Intent;
} // .end local v7 # "sessionId":I
} // .end local v8 # "status":I
/* add-int/lit8 v4, v4, -0x1 */
/* .line 249 */
/* goto/16 :goto_1 */
/* .line 250 */
} // :cond_7
} // .end method
static java.util.List installApps ( android.content.Context p0, java.util.List p1 ) {
/* .locals 18 */
/* .param p0, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 94 */
/* .local p1, "codePathList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 95 */
/* .local v0, "installedApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;" */
v1 = /* invoke-interface/range {p1 ..p1}, Ljava/util/List;->isEmpty()Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 96 */
/* .line 98 */
} // :cond_0
/* new-instance v1, Landroid/util/SparseArray; */
v2 = /* invoke-interface/range {p1 ..p1}, Ljava/util/List;->size()I */
/* invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V */
/* .line 99 */
/* .local v1, "activeSessions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/io/File;>;" */
/* const-wide/16 v2, 0x0 */
/* .line 100 */
/* .local v2, "waitDuration":J */
/* new-instance v4, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver; */
v5 = /* invoke-interface/range {p1 ..p1}, Ljava/util/List;->size()I */
/* invoke-direct {v4, v5}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;-><init>(I)V */
/* .line 101 */
/* .local v4, "receiver":Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver; */
/* invoke-interface/range {p1 ..p1}, Ljava/util/List;->iterator()Ljava/util/Iterator; */
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_2
/* check-cast v6, Ljava/io/File; */
/* .line 102 */
/* .local v6, "codePath":Ljava/io/File; */
/* move-object/from16 v7, p0 */
v8 = com.android.server.pm.InstallerUtil .installOne ( v7,v4,v6 );
/* .line 103 */
/* .local v8, "sessionId":I */
/* if-lez v8, :cond_1 */
/* .line 104 */
(( android.util.SparseArray ) v1 ).put ( v8, v6 ); // invoke-virtual {v1, v8, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 105 */
v9 = java.util.concurrent.TimeUnit.MINUTES;
/* const-wide/16 v10, 0x2 */
(( java.util.concurrent.TimeUnit ) v9 ).toMillis ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
/* move-result-wide v9 */
/* add-long/2addr v2, v9 */
/* .line 107 */
} // .end local v6 # "codePath":Ljava/io/File;
} // .end local v8 # "sessionId":I
} // :cond_1
/* .line 108 */
} // :cond_2
/* move-object/from16 v7, p0 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
/* .line 109 */
/* .local v5, "start":J */
} // :goto_1
v8 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
final String v9 = "Failed to install "; // const-string v9, "Failed to install "
final String v10 = "InstallerUtil"; // const-string v10, "InstallerUtil"
int v11 = 0; // const/4 v11, 0x0
/* if-lez v8, :cond_6 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v12 */
/* sub-long/2addr v12, v5 */
/* cmp-long v8, v12, v2 */
/* if-gez v8, :cond_6 */
/* .line 110 */
/* const-wide/16 v12, 0x3e8 */
android.os.SystemClock .sleep ( v12,v13 );
/* .line 111 */
(( com.android.server.pm.InstallerUtil$LocalIntentReceiver ) v4 ).getResultsNoWait ( ); // invoke-virtual {v4}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;->getResultsNoWait()Ljava/util/List;
v12 = } // :goto_2
if ( v12 != null) { // if-eqz v12, :cond_5
/* check-cast v12, Landroid/content/Intent; */
/* .line 112 */
/* .local v12, "intent":Landroid/content/Intent; */
final String v13 = "android.content.pm.extra.SESSION_ID"; // const-string v13, "android.content.pm.extra.SESSION_ID"
v13 = (( android.content.Intent ) v12 ).getIntExtra ( v13, v11 ); // invoke-virtual {v12, v13, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 113 */
/* .local v13, "sessionId":I */
v14 = (( android.util.SparseArray ) v1 ).indexOfKey ( v13 ); // invoke-virtual {v1, v13}, Landroid/util/SparseArray;->indexOfKey(I)I
/* if-gez v14, :cond_3 */
/* .line 114 */
/* .line 116 */
} // :cond_3
(( android.util.SparseArray ) v1 ).removeReturnOld ( v13 ); // invoke-virtual {v1, v13}, Landroid/util/SparseArray;->removeReturnOld(I)Ljava/lang/Object;
/* check-cast v14, Ljava/io/File; */
/* .line 117 */
/* .local v14, "file":Ljava/io/File; */
final String v15 = "android.content.pm.extra.STATUS"; // const-string v15, "android.content.pm.extra.STATUS"
int v11 = 1; // const/4 v11, 0x1
v11 = (( android.content.Intent ) v12 ).getIntExtra ( v15, v11 ); // invoke-virtual {v12, v15, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 119 */
/* .local v11, "status":I */
/* if-nez v11, :cond_4 */
/* .line 120 */
(( java.util.ArrayList ) v0 ).add ( v14 ); // invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 121 */
int v11 = 0; // const/4 v11, 0x0
/* .line 123 */
} // :cond_4
final String v15 = "android.content.pm.extra.STATUS_MESSAGE"; // const-string v15, "android.content.pm.extra.STATUS_MESSAGE"
(( android.content.Intent ) v12 ).getStringExtra ( v15 ); // invoke-virtual {v12, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 124 */
/* .local v15, "errorMsg":Ljava/lang/String; */
/* move-wide/from16 v16, v2 */
} // .end local v2 # "waitDuration":J
/* .local v16, "waitDuration":J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ": error code="; // const-string v3, ": error code="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", msg="; // const-string v3, ", msg="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v15 ); // invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v10,v2 );
/* .line 126 */
} // .end local v11 # "status":I
} // .end local v12 # "intent":Landroid/content/Intent;
} // .end local v13 # "sessionId":I
} // .end local v14 # "file":Ljava/io/File;
} // .end local v15 # "errorMsg":Ljava/lang/String;
/* move-wide/from16 v2, v16 */
int v11 = 0; // const/4 v11, 0x0
} // .end local v16 # "waitDuration":J
/* .restart local v2 # "waitDuration":J */
} // :cond_5
/* move-wide/from16 v16, v2 */
} // .end local v2 # "waitDuration":J
/* .restart local v16 # "waitDuration":J */
/* goto/16 :goto_1 */
/* .line 109 */
} // .end local v16 # "waitDuration":J
/* .restart local v2 # "waitDuration":J */
} // :cond_6
/* move-wide/from16 v16, v2 */
/* .line 128 */
} // .end local v2 # "waitDuration":J
/* .restart local v16 # "waitDuration":J */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_3
v3 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_7 */
/* .line 129 */
int v3 = 0; // const/4 v3, 0x0
v8 = (( android.util.SparseArray ) v1 ).keyAt ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 130 */
/* .restart local v8 # "sessionId":I */
(( android.util.SparseArray ) v1 ).get ( v8 ); // invoke-virtual {v1, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v11, Ljava/io/File; */
/* .line 131 */
/* .local v11, "file":Ljava/io/File; */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v12 ).append ( v9 ); // invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v11 ); // invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v13 = ": timeout, sessionId="; // const-string v13, ": timeout, sessionId="
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v8 ); // invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v12 );
/* .line 128 */
} // .end local v8 # "sessionId":I
} // .end local v11 # "file":Ljava/io/File;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 133 */
} // .end local v2 # "i":I
} // :cond_7
} // .end method
private static Integer installOne ( android.content.Context p0, com.android.server.pm.InstallerUtil$LocalIntentReceiver p1, java.io.File p2 ) {
/* .locals 9 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "receiver" # Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver; */
/* .param p2, "codePath" # Ljava/io/File; */
/* .line 138 */
final String v0 = "InstallerUtil"; // const-string v0, "InstallerUtil"
int v1 = -1; // const/4 v1, -0x1
try { // :try_start_0
com.android.server.pm.InstallerUtil .parsePackageLite ( p2 );
/* .line 139 */
/* .local v2, "pkgLite":Landroid/content/pm/parsing/PackageLite; */
/* if-nez v2, :cond_0 */
/* .line 140 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to installOne: "; // const-string v4, "Failed to installOne: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 141 */
int v0 = -4; // const/4 v0, -0x4
/* .line 144 */
} // :cond_0
com.android.server.pm.InstallerUtil .makeSessionParams ( v2 );
/* .line 145 */
/* .local v3, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams; */
v4 = com.android.server.pm.InstallerUtil .doCreateSession ( p0,v3 );
/* .line 146 */
/* .local v4, "sessionId":I */
/* if-gtz v4, :cond_1 */
/* .line 147 */
/* .line 150 */
} // :cond_1
(( android.content.pm.parsing.PackageLite ) v2 ).getAllApkPaths ( ); // invoke-virtual {v2}, Landroid/content/pm/parsing/PackageLite;->getAllApkPaths()Ljava/util/List;
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_3
/* check-cast v6, Ljava/lang/String; */
/* .line 151 */
/* .local v6, "splitCodePath":Ljava/lang/String; */
/* new-instance v7, Ljava/io/File; */
/* invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 152 */
/* .local v7, "splitFile":Ljava/io/File; */
(( java.io.File ) v7 ).getName ( ); // invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;
v8 = com.android.server.pm.InstallerUtil .doWriteSession ( p0,v8,v7,v4 );
/* if-nez v8, :cond_2 */
/* .line 153 */
com.android.server.pm.InstallerUtil .doAandonSession ( p0,v4 );
/* .line 154 */
/* .line 156 */
} // .end local v6 # "splitCodePath":Ljava/lang/String;
} // .end local v7 # "splitFile":Ljava/io/File;
} // :cond_2
/* .line 157 */
} // :cond_3
(( com.android.server.pm.InstallerUtil$LocalIntentReceiver ) p1 ).getIntentSender ( ); // invoke-virtual {p1}, Lcom/android/server/pm/InstallerUtil$LocalIntentReceiver;->getIntentSender()Landroid/content/IntentSender;
v0 = com.android.server.pm.InstallerUtil .doCommitSession ( p0,v4,v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* move v1, v4 */
/* .line 158 */
} // :cond_4
/* nop */
/* .line 157 */
} // :goto_1
/* .line 159 */
} // .end local v2 # "pkgLite":Landroid/content/pm/parsing/PackageLite;
} // .end local v3 # "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
} // .end local v4 # "sessionId":I
/* :catch_0 */
/* move-exception v2 */
/* .line 160 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to install "; // const-string v4, "Failed to install "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3,v2 );
/* .line 161 */
} // .end method
private static android.content.pm.PackageInstaller$SessionParams makeSessionParams ( android.content.pm.parsing.PackageLite p0 ) {
/* .locals 4 */
/* .param p0, "pkgLite" # Landroid/content/pm/parsing/PackageLite; */
/* .line 320 */
/* new-instance v0, Landroid/content/pm/PackageInstaller$SessionParams; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;-><init>(I)V */
/* .line 321 */
/* .local v0, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams; */
int v1 = 0; // const/4 v1, 0x0
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setInstallAsInstantApp ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallAsInstantApp(Z)V
/* .line 322 */
final String v1 = "android"; // const-string v1, "android"
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setInstallerPackageName ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallerPackageName(Ljava/lang/String;)V
/* .line 323 */
(( android.content.pm.parsing.PackageLite ) p0 ).getPackageName ( ); // invoke-virtual {p0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setAppPackageName ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setAppPackageName(Ljava/lang/String;)V
/* .line 324 */
v1 = (( android.content.pm.parsing.PackageLite ) p0 ).getInstallLocation ( ); // invoke-virtual {p0}, Landroid/content/pm/parsing/PackageLite;->getInstallLocation()I
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setInstallLocation ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallLocation(I)V
/* .line 326 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
com.android.internal.content.InstallLocationUtils .calculateInstalledSize ( p0,v1 );
/* move-result-wide v1 */
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setSize ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 329 */
/* .line 327 */
/* :catch_0 */
/* move-exception v1 */
/* .line 328 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/io/File; */
(( android.content.pm.parsing.PackageLite ) p0 ).getBaseApkPath ( ); // invoke-virtual {p0}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
(( java.io.File ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/io/File;->length()J
/* move-result-wide v2 */
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setSize ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V
/* .line 330 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
} // .end method
private static android.content.pm.parsing.PackageLite parsePackageLite ( java.io.File p0 ) {
/* .locals 5 */
/* .param p0, "codePath" # Ljava/io/File; */
/* .line 308 */
android.content.pm.parsing.result.ParseTypeImpl .forDefaultParsing ( );
/* .line 309 */
/* .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl; */
int v1 = 0; // const/4 v1, 0x0
android.content.pm.parsing.ApkLiteParseUtils .parsePackageLite ( v0,p0,v1 );
/* .line 311 */
v2 = /* .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/PackageLite;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 312 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to parsePackageLite: "; // const-string v3, "Failed to parsePackageLite: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " error: "; // const-string v3, " error: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 313 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 312 */
final String v4 = "InstallerUtil"; // const-string v4, "InstallerUtil"
android.util.Slog .e ( v4,v2,v3 );
/* .line 314 */
int v2 = 0; // const/4 v2, 0x0
/* .line 316 */
} // :cond_0
/* check-cast v2, Landroid/content/pm/parsing/PackageLite; */
} // .end method
