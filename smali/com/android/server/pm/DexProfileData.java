class com.android.server.pm.DexProfileData {
	 /* .source "ProfileTranscoder.java" */
	 /* # instance fields */
	 final java.lang.String apkName;
	 Integer classSetSize;
	 classes;
	 final Long dexChecksum;
	 final java.lang.String dexName;
	 final Integer hotMethodRegionSize;
	 Long mTypeIdCount;
	 final java.util.TreeMap methods;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/TreeMap<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
final Integer numMethodIds;
/* # direct methods */
 com.android.server.pm.DexProfileData ( ) {
/* .locals 0 */
/* .param p1, "apkName" # Ljava/lang/String; */
/* .param p2, "dexName" # Ljava/lang/String; */
/* .param p3, "dexChecksum" # J */
/* .param p5, "typeIdCount" # J */
/* .param p7, "classSetSize" # I */
/* .param p8, "hotMethodRegionSize" # I */
/* .param p9, "numMethodIds" # I */
/* .param p10, "classes" # [I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "JJIII[I", */
/* "Ljava/util/TreeMap<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1036 */
/* .local p11, "methods":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1037 */
this.apkName = p1;
/* .line 1038 */
this.dexName = p2;
/* .line 1039 */
/* iput-wide p3, p0, Lcom/android/server/pm/DexProfileData;->dexChecksum:J */
/* .line 1040 */
/* iput-wide p5, p0, Lcom/android/server/pm/DexProfileData;->mTypeIdCount:J */
/* .line 1041 */
/* iput p7, p0, Lcom/android/server/pm/DexProfileData;->classSetSize:I */
/* .line 1042 */
/* iput p8, p0, Lcom/android/server/pm/DexProfileData;->hotMethodRegionSize:I */
/* .line 1043 */
/* iput p9, p0, Lcom/android/server/pm/DexProfileData;->numMethodIds:I */
/* .line 1044 */
this.classes = p10;
/* .line 1045 */
this.methods = p11;
/* .line 1046 */
return;
} // .end method
