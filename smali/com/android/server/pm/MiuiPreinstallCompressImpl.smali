.class public Lcom/android/server/pm/MiuiPreinstallCompressImpl;
.super Lcom/android/server/pm/MiuiPreinstallCompressStub;
.source "MiuiPreinstallCompressImpl.java"


# static fields
.field private static final F2FS_COMPR_SUPPORT:Z

.field private static final MIUI_APP_PREDIR:Ljava/lang/String; = "/product/data-app"

.field private static final OTA_COMPR_ENABLE:Z

.field private static final PARTNER_DIR_PREFIX:Ljava/lang/String; = "partner"

.field private static final RANDOM_DIR_PREFIX:Ljava/lang/String; = "~~"

.field private static final REGION_IS_CN:Z

.field private static final TAG:Ljava/lang/String; = "MiuiPreinstallComprImpl"

.field private static mHandler:Landroid/os/Handler; = null

.field private static mIsDeviceUpgrading:Z = false

.field private static mIsFirstBoot:Z = false

.field private static mPackageList:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final preinstallPackageList:Ljava/lang/String; = "/data/app/preinstall_package_path"


# instance fields
.field private mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

.field private mPms:Lcom/android/server/pm/PackageManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 33
    nop

    .line 34
    const-string v0, "ro.miui.region"

    const-string/jumbo v1, "unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->REGION_IS_CN:Z

    .line 35
    nop

    .line 36
    const-string v0, "ro.miui.ota_compr_enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->OTA_COMPR_ENABLE:Z

    .line 37
    invoke-static {}, Lcom/android/internal/content/F2fsUtils;->isCompressSupport()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->F2FS_COMPR_SUPPORT:Z

    .line 38
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->getPackageList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mPackageList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallCompressStub;-><init>()V

    return-void
.end method

.method private compressLib(Ljava/lang/String;)V
    .locals 2
    .param p1, "libDir"    # Ljava/lang/String;

    .line 116
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/pm/MiuiPreinstallCompressImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/android/server/pm/MiuiPreinstallCompressImpl$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 119
    return-void
.end method

.method private static getPackageList()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 122
    sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->OTA_COMPR_ENABLE:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 123
    return-object v1

    .line 125
    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v2, "/data/app/preinstall_package_path"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 127
    const-string v2, "MiuiPreinstallComprImpl"

    const-string v3, "Preinstall file is not exist."

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    return-object v1

    .line 130
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 134
    .local v3, "read":Ljava/io/InputStreamReader;
    const/4 v4, 0x0

    .line 136
    .local v4, "buffer":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object v2, v5

    .line 137
    .local v2, "inputStream":Ljava/io/InputStream;
    new-instance v5, Ljava/io/InputStreamReader;

    const-string/jumbo v6, "utf-8"

    invoke-direct {v5, v2, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object v3, v5

    .line 138
    new-instance v5, Ljava/io/BufferedReader;

    invoke-direct {v5, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v4, v5

    .line 139
    :cond_2
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .local v6, "line":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 140
    const-string v5, "/product/data-app"

    invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 141
    const-string v5, ":"

    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 142
    .local v5, "name":Ljava/lang/String;
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 153
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v5    # "name":Ljava/lang/String;
    :cond_3
    nop

    .line 155
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 158
    goto :goto_1

    .line 156
    :catch_0
    move-exception v2

    .line 157
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 160
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    nop

    .line 162
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 165
    goto :goto_6

    .line 163
    :catch_1
    move-exception v2

    .line 164
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 165
    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_6

    .line 153
    .end local v6    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v2

    goto :goto_7

    .line 150
    :catch_2
    move-exception v2

    .line 151
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 153
    .end local v2    # "e":Ljava/io/IOException;
    if-eqz v3, :cond_4

    .line 155
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 158
    goto :goto_2

    .line 156
    :catch_3
    move-exception v2

    .line 157
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 160
    .end local v2    # "e":Ljava/io/IOException;
    :cond_4
    :goto_2
    if-eqz v4, :cond_7

    .line 162
    :try_start_5
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 165
    :goto_3
    goto :goto_6

    .line 163
    :catch_4
    move-exception v2

    .line 164
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 165
    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_6

    .line 148
    :catch_5
    move-exception v2

    .line 149
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 153
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    if-eqz v3, :cond_5

    .line 155
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 158
    goto :goto_4

    .line 156
    :catch_6
    move-exception v2

    .line 157
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 160
    .end local v2    # "e":Ljava/io/IOException;
    :cond_5
    :goto_4
    if-eqz v4, :cond_7

    .line 162
    :try_start_8
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_3

    .line 146
    :catch_7
    move-exception v2

    .line 147
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_9
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 153
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    if-eqz v3, :cond_6

    .line 155
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 158
    goto :goto_5

    .line 156
    :catch_8
    move-exception v2

    .line 157
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 160
    .end local v2    # "e":Ljava/io/IOException;
    :cond_6
    :goto_5
    if-eqz v4, :cond_7

    .line 162
    :try_start_b
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_3

    .line 168
    :cond_7
    :goto_6
    return-object v1

    .line 153
    :goto_7
    if-eqz v3, :cond_8

    .line 155
    :try_start_c
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 158
    goto :goto_8

    .line 156
    :catch_9
    move-exception v5

    .line 157
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 160
    .end local v5    # "e":Ljava/io/IOException;
    :cond_8
    :goto_8
    if-eqz v4, :cond_9

    .line 162
    :try_start_d
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 165
    goto :goto_9

    .line 163
    :catch_a
    move-exception v5

    .line 164
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 167
    .end local v5    # "e":Ljava/io/IOException;
    :cond_9
    :goto_9
    throw v2
.end method

.method private isCompressApkFirstBootOrUpgradeNewFrame(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "rootLibPath"    # Ljava/lang/String;
    .param p2, "scanFlags"    # I

    .line 68
    const/high16 v0, 0x40000000    # 2.0f

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->REGION_IS_CN:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->F2FS_COMPR_SUPPORT:Z

    if-eqz v0, :cond_0

    .line 70
    sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mIsFirstBoot:Z

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    return v0

    .line 74
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isCompressApkFirstBootOrUpgradeOldFrame(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "rootLibPath"    # Ljava/lang/String;

    .line 78
    sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->OTA_COMPR_ENABLE:Z

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mPackageList:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    const-string/jumbo v0, "~~"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x1

    return v0

    .line 84
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isCompressApkInstallNewFrame(Lcom/android/server/pm/pkg/AndroidPackage;)Z
    .locals 2
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;

    .line 99
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->isMiuiPreinstallApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->REGION_IS_CN:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->F2FS_COMPR_SUPPORT:Z

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    return v0

    .line 103
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isCompressApkInstallOldFrame(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 107
    sget-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->OTA_COMPR_ENABLE:Z

    if-eqz v0, :cond_0

    .line 108
    sget-object v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mPackageList:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x1

    return v0

    .line 112
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic lambda$compressLib$0(Ljava/lang/String;)V
    .locals 1
    .param p0, "libDir"    # Ljava/lang/String;

    .line 117
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/internal/content/F2fsUtils;->compressAndReleaseBlocks(Ljava/io/File;)V

    return-void
.end method


# virtual methods
.method public compressPreinstallAppFirstBootOrUpgrade(Lcom/android/server/pm/pkg/AndroidPackage;I)V
    .locals 2
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "scanFlags"    # I

    .line 58
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getNativeLibraryDir()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->isCompressApkFirstBootOrUpgradeNewFrame(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getNativeLibraryDir()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->compressLib(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getNativeLibraryDir()Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-direct {p0, v0, v1}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->isCompressApkFirstBootOrUpgradeOldFrame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getNativeLibraryDir()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->compressLib(Ljava/lang/String;)V

    .line 66
    :cond_1
    :goto_0
    return-void
.end method

.method public compressPreinstallAppInstall(Lcom/android/server/pm/pkg/AndroidPackage;)V
    .locals 1
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;

    .line 89
    invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->isCompressApkInstallNewFrame(Lcom/android/server/pm/pkg/AndroidPackage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getNativeLibraryDir()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->compressLib(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_0
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->isCompressApkInstallOldFrame(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getNativeLibraryDir()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->compressLib(Ljava/lang/String;)V

    .line 96
    :cond_1
    :goto_0
    return-void
.end method

.method public init(Lcom/android/server/pm/PackageManagerService;)V
    .locals 3
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;

    .line 46
    const-string v0, "MiuiPreinstallComprImpl"

    const-string v1, "init Miui Preinstall Compress Stub."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iput-object p1, p0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 48
    invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mIsFirstBoot:Z

    .line 49
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mIsDeviceUpgrading:Z

    .line 50
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    .line 51
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Compress"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 52
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 53
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/android/server/pm/MiuiPreinstallCompressImpl;->mHandler:Landroid/os/Handler;

    .line 54
    return-void
.end method
