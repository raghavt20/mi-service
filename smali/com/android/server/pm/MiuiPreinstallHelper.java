public class com.android.server.pm.MiuiPreinstallHelper extends com.android.server.pm.MiuiPreinstallHelperStub {
	 /* .source "MiuiPreinstallHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String DATA_APP_DIR;
private static final java.lang.String ENCRYPTING_STATE;
private static final java.lang.String RANDOM_DIR_PREFIX;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.pm.MiuiBusinessPreinstallConfig mBusinessPreinstallConfig;
private Boolean mIsDeviceUpgrading;
private Boolean mIsFirstBoot;
private java.util.List mLegacyPreinstallAppPaths;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.Object mLock;
private android.util.ArrayMap mMiuiPreinstallApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/pm/MiuiPreinstallApp;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.pm.MiuiOperatorPreinstallConfig mOperatorPreinstallConfig;
private com.android.server.pm.MiuiPlatformPreinstallConfig mPlatformPreinstallConfig;
private com.android.server.pm.PackageManagerService mPms;
private java.util.List mPreinstallDirs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.io.File mPreviousSettingsFilename;
private java.io.File mSettingsFilename;
private java.io.File mSettingsReserveCopyFilename;
/* # direct methods */
public static Boolean $r8$lambda$0wP2YGMYBTMdPJxedgXQ90mAHGk ( com.android.server.pm.MiuiPreinstallHelper p0, java.io.File p1 ) { //synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->lambda$cleanUpResource$0(Ljava/io/File;)Z */
} // .end method
public static void $r8$lambda$JrB0RJqFiMgNTQ2A5Mwlb0EJ3-w ( com.android.server.pm.MiuiPreinstallHelper p0, com.android.server.pm.BackgroundPreinstalloptService p1, android.app.job.JobParameters p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/pm/MiuiPreinstallHelper;->lambda$onStartJob$1(Lcom/android/server/pm/BackgroundPreinstalloptService;Landroid/app/job/JobParameters;)V */
return;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
} // .end method
static com.android.server.pm.MiuiPreinstallHelper ( ) {
/* .locals 1 */
/* .line 66 */
/* const-class v0, Lcom/android/server/pm/MiuiPreinstallHelper; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
return;
} // .end method
public com.android.server.pm.MiuiPreinstallHelper ( ) {
/* .locals 1 */
/* .line 65 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelperStub;-><init>()V */
/* .line 70 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mPreinstallDirs = v0;
/* .line 72 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mMiuiPreinstallApps = v0;
/* .line 73 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mLegacyPreinstallAppPaths = v0;
/* .line 74 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
return;
} // .end method
private void addMiuiPreinstallApp ( com.android.server.pm.MiuiPreinstallApp p0 ) {
/* .locals 4 */
/* .param p1, "miuiPreinstallApp" # Lcom/android/server/pm/MiuiPreinstallApp; */
/* .line 998 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "addMiuiPreinstallApp: "; // const-string v2, "addMiuiPreinstallApp: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.MiuiPreinstallApp ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/MiuiPreinstallApp;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " versionCode: "; // const-string v2, " versionCode: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 999 */
(( com.android.server.pm.MiuiPreinstallApp ) p1 ).getVersionCode ( ); // invoke-virtual {p1}, Lcom/android/server/pm/MiuiPreinstallApp;->getVersionCode()J
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = " apkPath: "; // const-string v2, " apkPath: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1000 */
(( com.android.server.pm.MiuiPreinstallApp ) p1 ).getApkPath ( ); // invoke-virtual {p1}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 998 */
android.util.Slog .d ( v0,v1 );
/* .line 1001 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1002 */
try { // :try_start_0
v1 = this.mMiuiPreinstallApps;
(( com.android.server.pm.MiuiPreinstallApp ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/MiuiPreinstallApp;->getPackageName()Ljava/lang/String;
(( android.util.ArrayMap ) v1 ).put ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1003 */
/* monitor-exit v0 */
/* .line 1004 */
return;
/* .line 1003 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void batchInstallApps ( java.util.List p0 ) {
/* .locals 20 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/parsing/PackageLite;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 345 */
/* .local p1, "apkList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/parsing/PackageLite;>;" */
/* move-object/from16 v0, p0 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 346 */
/* .local v1, "installedApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/parsing/PackageLite;>;" */
/* new-instance v2, Landroid/util/SparseArray; */
v3 = /* invoke-interface/range {p1 ..p1}, Ljava/util/List;->size()I */
/* invoke-direct {v2, v3}, Landroid/util/SparseArray;-><init>(I)V */
/* .line 347 */
/* .local v2, "activeSessions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/content/pm/parsing/PackageLite;>;" */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 348 */
/* .local v3, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* const-wide/16 v4, 0x0 */
/* .line 349 */
/* .local v4, "waitDuration":J */
/* new-instance v6, Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
v7 = /* invoke-interface/range {p1 ..p1}, Ljava/util/List;->size()I */
/* invoke-direct {v6, v7}, Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;-><init>(I)V */
/* .line 350 */
/* .local v6, "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
v7 = this.mPms;
v7 = this.mContext;
/* .line 351 */
/* .local v7, "context":Landroid/content/Context; */
/* if-nez v7, :cond_0 */
/* .line 352 */
v8 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v9 = "batchInstallApps context is null!"; // const-string v9, "batchInstallApps context is null!"
android.util.Slog .e ( v8,v9 );
/* .line 354 */
} // :cond_0
/* invoke-interface/range {p1 ..p1}, Ljava/util/List;->iterator()Ljava/util/Iterator; */
v9 = } // :goto_0
if ( v9 != null) { // if-eqz v9, :cond_3
/* check-cast v9, Landroid/content/pm/parsing/PackageLite; */
/* .line 355 */
/* .local v9, "packageLite":Landroid/content/pm/parsing/PackageLite; */
(( android.content.pm.parsing.PackageLite ) v9 ).getPackageName ( ); // invoke-virtual {v9}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v10 = (( java.util.HashSet ) v3 ).contains ( v10 ); // invoke-virtual {v3, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_1
/* .line 356 */
v10 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "Fail to installApp: "; // const-string v12, "Fail to installApp: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.pm.parsing.PackageLite ) v9 ).getPackageName ( ); // invoke-virtual {v9}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = ", duplicate package name, version: "; // const-string v12, ", duplicate package name, version: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 357 */
v12 = (( android.content.pm.parsing.PackageLite ) v9 ).getVersionCode ( ); // invoke-virtual {v9}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 356 */
android.util.Slog .e ( v10,v11 );
/* .line 358 */
/* .line 360 */
} // :cond_1
(( android.content.pm.parsing.PackageLite ) v9 ).getPackageName ( ); // invoke-virtual {v9}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( java.util.HashSet ) v3 ).add ( v10 ); // invoke-virtual {v3, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 361 */
v10 = /* invoke-direct {v0, v7, v6, v9}, Lcom/android/server/pm/MiuiPreinstallHelper;->installOne(Landroid/content/Context;Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;Landroid/content/pm/parsing/PackageLite;)I */
/* .line 362 */
/* .local v10, "sessionId":I */
/* if-lez v10, :cond_2 */
/* .line 363 */
(( android.util.SparseArray ) v2 ).put ( v10, v9 ); // invoke-virtual {v2, v10, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 364 */
v11 = java.util.concurrent.TimeUnit.MINUTES;
/* const-wide/16 v12, 0x2 */
(( java.util.concurrent.TimeUnit ) v11 ).toMillis ( v12, v13 ); // invoke-virtual {v11, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
/* move-result-wide v11 */
/* add-long/2addr v4, v11 */
/* .line 366 */
} // .end local v9 # "packageLite":Landroid/content/pm/parsing/PackageLite;
} // .end local v10 # "sessionId":I
} // :cond_2
/* .line 367 */
} // :cond_3
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v8 */
/* .line 368 */
/* .local v8, "start":J */
} // :goto_1
v10 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
final String v11 = "Failed to install "; // const-string v11, "Failed to install "
int v12 = 0; // const/4 v12, 0x0
/* if-lez v10, :cond_7 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v13 */
/* sub-long/2addr v13, v8 */
/* cmp-long v10, v13, v4 */
/* if-gez v10, :cond_7 */
/* .line 369 */
/* const-wide/16 v13, 0x3e8 */
android.os.SystemClock .sleep ( v13,v14 );
/* .line 370 */
(( com.android.server.pm.MiuiPreinstallHelper$LocalIntentReceiver ) v6 ).getResultsNoWait ( ); // invoke-virtual {v6}, Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;->getResultsNoWait()Ljava/util/List;
v13 = } // :goto_2
if ( v13 != null) { // if-eqz v13, :cond_6
/* check-cast v13, Landroid/content/Intent; */
/* .line 371 */
/* .local v13, "intent":Landroid/content/Intent; */
/* nop */
/* .line 372 */
final String v14 = "android.content.pm.extra.SESSION_ID"; // const-string v14, "android.content.pm.extra.SESSION_ID"
v14 = (( android.content.Intent ) v13 ).getIntExtra ( v14, v12 ); // invoke-virtual {v13, v14, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 373 */
/* .local v14, "sessionId":I */
v15 = (( android.util.SparseArray ) v2 ).indexOfKey ( v14 ); // invoke-virtual {v2, v14}, Landroid/util/SparseArray;->indexOfKey(I)I
/* if-gez v15, :cond_4 */
/* .line 374 */
/* .line 376 */
} // :cond_4
(( android.util.SparseArray ) v2 ).removeReturnOld ( v14 ); // invoke-virtual {v2, v14}, Landroid/util/SparseArray;->removeReturnOld(I)Ljava/lang/Object;
/* check-cast v15, Landroid/content/pm/parsing/PackageLite; */
/* .line 377 */
/* .local v15, "pkgLite":Landroid/content/pm/parsing/PackageLite; */
final String v12 = "android.content.pm.extra.STATUS"; // const-string v12, "android.content.pm.extra.STATUS"
/* move-object/from16 v16, v3 */
} // .end local v3 # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
/* .local v16, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
int v3 = 1; // const/4 v3, 0x1
v3 = (( android.content.Intent ) v13 ).getIntExtra ( v12, v3 ); // invoke-virtual {v13, v12, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 379 */
/* .local v3, "status":I */
/* if-nez v3, :cond_5 */
/* .line 380 */
(( java.util.ArrayList ) v1 ).add ( v15 ); // invoke-virtual {v1, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 381 */
/* move-object/from16 v3, v16 */
int v12 = 0; // const/4 v12, 0x0
/* .line 383 */
} // :cond_5
/* nop */
/* .line 384 */
final String v12 = "android.content.pm.extra.STATUS_MESSAGE"; // const-string v12, "android.content.pm.extra.STATUS_MESSAGE"
(( android.content.Intent ) v13 ).getStringExtra ( v12 ); // invoke-virtual {v13, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 385 */
/* .local v12, "errorMsg":Ljava/lang/String; */
/* move-wide/from16 v17, v4 */
} // .end local v4 # "waitDuration":J
/* .local v17, "waitDuration":J */
v4 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v19, v6 */
} // .end local v6 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
/* .local v19, "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
(( android.content.pm.parsing.PackageLite ) v15 ).getPath ( ); // invoke-virtual {v15}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ": error code="; // const-string v6, ": error code="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", msg="; // const-string v6, ", msg="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v12 ); // invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 388 */
} // .end local v3 # "status":I
} // .end local v12 # "errorMsg":Ljava/lang/String;
} // .end local v13 # "intent":Landroid/content/Intent;
} // .end local v14 # "sessionId":I
} // .end local v15 # "pkgLite":Landroid/content/pm/parsing/PackageLite;
/* move-object/from16 v3, v16 */
/* move-wide/from16 v4, v17 */
/* move-object/from16 v6, v19 */
int v12 = 0; // const/4 v12, 0x0
} // .end local v16 # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // .end local v17 # "waitDuration":J
} // .end local v19 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
/* .local v3, "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* .restart local v4 # "waitDuration":J */
/* .restart local v6 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
} // :cond_6
/* move-object/from16 v16, v3 */
/* move-wide/from16 v17, v4 */
/* move-object/from16 v19, v6 */
} // .end local v3 # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // .end local v4 # "waitDuration":J
} // .end local v6 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
/* .restart local v16 # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* .restart local v17 # "waitDuration":J */
/* .restart local v19 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
/* goto/16 :goto_1 */
/* .line 368 */
} // .end local v16 # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // .end local v17 # "waitDuration":J
} // .end local v19 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
/* .restart local v3 # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* .restart local v4 # "waitDuration":J */
/* .restart local v6 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
} // :cond_7
/* move-object/from16 v16, v3 */
/* move-wide/from16 v17, v4 */
/* move-object/from16 v19, v6 */
/* .line 390 */
} // .end local v3 # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
} // .end local v4 # "waitDuration":J
} // .end local v6 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;
/* .restart local v16 # "packages":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
/* .restart local v17 # "waitDuration":J */
/* .restart local v19 # "receiver":Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_3
v4 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v3, v4, :cond_8 */
/* .line 391 */
int v4 = 0; // const/4 v4, 0x0
v5 = (( android.util.SparseArray ) v2 ).keyAt ( v4 ); // invoke-virtual {v2, v4}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 392 */
/* .local v5, "sessionId":I */
(( android.util.SparseArray ) v2 ).get ( v5 ); // invoke-virtual {v2, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v6, Landroid/content/pm/parsing/PackageLite; */
/* .line 393 */
/* .local v6, "pkgLite":Landroid/content/pm/parsing/PackageLite; */
v10 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v12 ).append ( v11 ); // invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.pm.parsing.PackageLite ) v6 ).getPath ( ); // invoke-virtual {v6}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v13 = ": timeout, sessionId="; // const-string v13, ": timeout, sessionId="
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v5 ); // invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v12 );
/* .line 390 */
} // .end local v5 # "sessionId":I
} // .end local v6 # "pkgLite":Landroid/content/pm/parsing/PackageLite;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 397 */
} // .end local v3 # "i":I
} // :cond_8
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_4
if ( v4 != null) { // if-eqz v4, :cond_9
/* check-cast v4, Landroid/content/pm/parsing/PackageLite; */
/* .line 398 */
/* .local v4, "pkgLite":Landroid/content/pm/parsing/PackageLite; */
/* new-instance v5, Lcom/android/server/pm/MiuiPreinstallApp; */
(( android.content.pm.parsing.PackageLite ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* .line 399 */
(( android.content.pm.parsing.PackageLite ) v4 ).getLongVersionCode ( ); // invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getLongVersionCode()J
/* move-result-wide v10 */
(( android.content.pm.parsing.PackageLite ) v4 ).getPath ( ); // invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
/* invoke-direct {v5, v6, v10, v11, v12}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V */
/* .line 398 */
/* invoke-direct {v0, v5}, Lcom/android/server/pm/MiuiPreinstallHelper;->addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V */
/* .line 400 */
} // .end local v4 # "pkgLite":Landroid/content/pm/parsing/PackageLite;
/* .line 401 */
} // :cond_9
return;
} // .end method
private void cleanUpResource ( java.io.File p0 ) {
/* .locals 1 */
/* .param p1, "dstCodePath" # Ljava/io/File; */
/* .line 785 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( java.io.File ) p1 ).isDirectory ( ); // invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 786 */
/* new-instance v0, Lcom/android/server/pm/MiuiPreinstallHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/pm/MiuiPreinstallHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/MiuiPreinstallHelper;)V */
(( java.io.File ) p1 ).listFiles ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;
/* .line 801 */
} // :cond_0
return;
} // .end method
private void copyLegacyPreisntallApp ( java.lang.String p0, com.android.server.pm.pkg.PackageStateInternal p1, android.content.pm.parsing.PackageLite p2 ) {
/* .locals 16 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "ps" # Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .param p3, "packageLite" # Landroid/content/pm/parsing/PackageLite; */
/* .line 696 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v3, v0 */
/* .line 697 */
/* .local v3, "targetAppDir":Ljava/io/File; */
(( java.io.File ) v3 ).listFiles ( ); // invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 698 */
/* .local v4, "files":[Ljava/io/File; */
v0 = com.android.internal.util.ArrayUtils .isEmpty ( v4 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 699 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "there is none apk file in path: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v5 );
/* .line 700 */
return;
/* .line 702 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 703 */
/* .local v0, "dstCodePath":Ljava/io/File; */
final String v5 = "/data/app/"; // const-string v5, "/data/app/"
if ( p2 != null) { // if-eqz p2, :cond_1
/* invoke-interface/range {p2 ..p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPathString()Ljava/lang/String; */
v6 = (( java.lang.String ) v6 ).startsWith ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 704 */
/* invoke-interface/range {p2 ..p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPath()Ljava/io/File; */
/* .line 705 */
/* invoke-direct {v1, v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->cleanUpResource(Ljava/io/File;)V */
/* .line 708 */
} // :cond_1
/* if-nez v0, :cond_2 */
/* .line 709 */
/* new-instance v6, Ljava/io/File; */
/* invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 710 */
/* invoke-virtual/range {p3 ..p3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String; */
com.android.server.pm.PackageManagerServiceUtils .getNextCodePath ( v6,v5 );
/* move-object v5, v0 */
/* .line 708 */
} // :cond_2
/* move-object v5, v0 */
/* .line 713 */
} // .end local v0 # "dstCodePath":Ljava/io/File;
/* .local v5, "dstCodePath":Ljava/io/File; */
} // :goto_0
v0 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_3 */
/* .line 714 */
(( java.io.File ) v5 ).mkdirs ( ); // invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z
/* .line 717 */
} // :cond_3
v0 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
int v6 = -1; // const/4 v6, -0x1
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 718 */
(( java.io.File ) v5 ).getParentFile ( ); // invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* .line 719 */
/* .local v0, "codePathParent":Ljava/io/File; */
(( java.io.File ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;
/* const-string/jumbo v8, "~~" */
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* const/16 v8, 0x1fd */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 720 */
(( java.io.File ) v0 ).getAbsolutePath ( ); // invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
android.os.FileUtils .setPermissions ( v7,v8,v6,v6 );
/* .line 723 */
} // :cond_4
android.os.FileUtils .setPermissions ( v5,v8,v6,v6 );
/* .line 726 */
} // .end local v0 # "codePathParent":Ljava/io/File;
} // :cond_5
int v0 = 0; // const/4 v0, 0x0
/* .line 727 */
/* .local v0, "res":Z */
/* array-length v7, v4 */
int v8 = 0; // const/4 v8, 0x0
/* move v9, v0 */
/* move v10, v8 */
} // .end local v0 # "res":Z
/* .local v9, "res":Z */
} // :goto_1
/* if-ge v10, v7, :cond_7 */
/* aget-object v11, v4, v10 */
/* .line 728 */
/* .local v11, "file":Ljava/io/File; */
/* new-instance v0, Ljava/io/File; */
(( java.io.File ) v11 ).getName ( ); // invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;
/* invoke-direct {v0, v5, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v12, v0 */
/* .line 731 */
/* .local v12, "dstFile":Ljava/io/File; */
try { // :try_start_0
android.os.FileUtils .copy ( v11,v12 );
/* .line 732 */
/* const/16 v0, 0x1a4 */
v0 = android.os.FileUtils .setPermissions ( v12,v0,v6,v6 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v0, :cond_6 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_6
/* move v0, v8 */
/* .line 736 */
} // .end local v9 # "res":Z
/* .restart local v0 # "res":Z */
} // :goto_2
/* move v9, v0 */
/* .line 733 */
} // .end local v0 # "res":Z
/* .restart local v9 # "res":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 734 */
/* .local v0, "e":Ljava/io/IOException; */
v13 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "Copy failed from: "; // const-string v15, "Copy failed from: "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v11 ).getPath ( ); // invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " to "; // const-string v15, " to "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 735 */
(( java.io.File ) v12 ).getPath ( ); // invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = ":"; // const-string v15, ":"
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 734 */
android.util.Slog .d ( v13,v14 );
/* .line 727 */
} // .end local v0 # "e":Ljava/io/IOException;
} // .end local v11 # "file":Ljava/io/File;
} // .end local v12 # "dstFile":Ljava/io/File;
} // :goto_3
/* add-int/lit8 v10, v10, 0x1 */
/* .line 738 */
} // :cond_7
if ( v9 != null) { // if-eqz v9, :cond_8
/* .line 739 */
/* new-instance v0, Lcom/android/server/pm/MiuiPreinstallApp; */
/* invoke-virtual/range {p3 ..p3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String; */
/* .line 740 */
/* invoke-virtual/range {p3 ..p3}, Landroid/content/pm/parsing/PackageLite;->getLongVersionCode()J */
/* move-result-wide v7 */
/* invoke-virtual/range {p3 ..p3}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String; */
/* invoke-direct {v0, v6, v7, v8, v10}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V */
/* .line 739 */
/* invoke-direct {v1, v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V */
/* .line 742 */
} // :cond_8
return;
} // .end method
private void copyMiuiPreinstallApp ( java.lang.String p0, com.android.server.pm.pkg.PackageStateInternal p1, com.android.server.pm.parsing.pkg.ParsedPackage p2 ) {
/* .locals 16 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "ps" # Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .param p3, "parsedPackage" # Lcom/android/server/pm/parsing/pkg/ParsedPackage; */
/* .line 746 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v3, v0 */
/* .line 747 */
/* .local v3, "targetAppDir":Ljava/io/File; */
(( java.io.File ) v3 ).listFiles ( ); // invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 748 */
/* .local v4, "files":[Ljava/io/File; */
v0 = com.android.internal.util.ArrayUtils .isEmpty ( v4 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 749 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "there is none apk file in path: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v5 );
/* .line 750 */
return;
/* .line 752 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 753 */
/* .local v0, "dstCodePath":Ljava/io/File; */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 754 */
/* invoke-interface/range {p2 ..p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPath()Ljava/io/File; */
/* .line 755 */
/* invoke-direct {v1, v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->cleanUpResource(Ljava/io/File;)V */
/* move-object v5, v0 */
/* .line 753 */
} // :cond_1
/* move-object v5, v0 */
/* .line 758 */
} // .end local v0 # "dstCodePath":Ljava/io/File;
/* .local v5, "dstCodePath":Ljava/io/File; */
} // :goto_0
v0 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
int v6 = -1; // const/4 v6, -0x1
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 759 */
(( java.io.File ) v5 ).getParentFile ( ); // invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* .line 760 */
/* .local v0, "codePathParent":Ljava/io/File; */
(( java.io.File ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;
/* const-string/jumbo v8, "~~" */
v7 = (( java.lang.String ) v7 ).startsWith ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* const/16 v8, 0x1fd */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 761 */
(( java.io.File ) v0 ).getAbsolutePath ( ); // invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
android.os.FileUtils .setPermissions ( v7,v8,v6,v6 );
/* .line 763 */
} // :cond_2
android.os.FileUtils .setPermissions ( v5,v8,v6,v6 );
/* .line 766 */
} // .end local v0 # "codePathParent":Ljava/io/File;
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
/* .line 767 */
/* .local v0, "res":Z */
/* array-length v7, v4 */
int v8 = 0; // const/4 v8, 0x0
/* move v9, v0 */
/* move v10, v8 */
} // .end local v0 # "res":Z
/* .local v9, "res":Z */
} // :goto_1
/* if-ge v10, v7, :cond_5 */
/* aget-object v11, v4, v10 */
/* .line 768 */
/* .local v11, "file":Ljava/io/File; */
/* new-instance v0, Ljava/io/File; */
(( java.io.File ) v11 ).getName ( ); // invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;
/* invoke-direct {v0, v5, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v12, v0 */
/* .line 771 */
/* .local v12, "dstFile":Ljava/io/File; */
try { // :try_start_0
android.os.FileUtils .copy ( v11,v12 );
/* .line 772 */
/* const/16 v0, 0x1a4 */
v0 = android.os.FileUtils .setPermissions ( v12,v0,v6,v6 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v0, :cond_4 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_4
/* move v0, v8 */
/* .line 776 */
} // .end local v9 # "res":Z
/* .restart local v0 # "res":Z */
} // :goto_2
/* move v9, v0 */
/* .line 773 */
} // .end local v0 # "res":Z
/* .restart local v9 # "res":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 774 */
/* .local v0, "e":Ljava/io/IOException; */
v13 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "Copy failed from: "; // const-string v15, "Copy failed from: "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v11 ).getPath ( ); // invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " to "; // const-string v15, " to "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 775 */
(( java.io.File ) v12 ).getPath ( ); // invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = ":"; // const-string v15, ":"
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 774 */
android.util.Slog .d ( v13,v14 );
/* .line 767 */
} // .end local v0 # "e":Ljava/io/IOException;
} // .end local v11 # "file":Ljava/io/File;
} // .end local v12 # "dstFile":Ljava/io/File;
} // :goto_3
/* add-int/lit8 v10, v10, 0x1 */
/* .line 778 */
} // :cond_5
if ( v9 != null) { // if-eqz v9, :cond_6
/* .line 779 */
/* new-instance v0, Lcom/android/server/pm/MiuiPreinstallApp; */
/* invoke-interface/range {p3 ..p3}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPackageName()Ljava/lang/String; */
/* .line 780 */
/* invoke-interface/range {p3 ..p3}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getLongVersionCode()J */
/* move-result-wide v7 */
/* invoke-interface/range {p3 ..p3}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPath()Ljava/lang/String; */
/* invoke-direct {v0, v6, v7, v8, v10}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V */
/* .line 779 */
/* invoke-direct {v1, v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V */
/* .line 782 */
} // :cond_6
return;
} // .end method
private Boolean deleteContentsRecursive ( java.io.File p0 ) {
/* .locals 8 */
/* .param p1, "dir" # Ljava/io/File; */
/* .line 804 */
(( java.io.File ) p1 ).listFiles ( ); // invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 805 */
/* .local v0, "files":[Ljava/io/File; */
int v1 = 1; // const/4 v1, 0x1
/* .line 806 */
/* .local v1, "success":Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 807 */
/* array-length v2, v0 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, v0, v3 */
/* .line 808 */
/* .local v4, "file":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).isDirectory ( ); // invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 809 */
v5 = /* invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->deleteContentsRecursive(Ljava/io/File;)Z */
/* and-int/2addr v1, v5 */
/* .line 811 */
} // :cond_0
v5 = (( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* if-nez v5, :cond_1 */
/* .line 812 */
v5 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Failed to delete "; // const-string v7, "Failed to delete "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v6 );
/* .line 813 */
int v1 = 0; // const/4 v1, 0x0
/* .line 807 */
} // .end local v4 # "file":Ljava/io/File;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 817 */
} // :cond_2
} // .end method
private java.io.File deriveAppLib ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .line 1076 */
final String v0 = "/"; // const-string v0, "/"
v1 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
(( java.lang.String ) p1 ).substring ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;
} // :cond_0
/* move-object v1, p1 */
/* .line 1077 */
/* .local v1, "trimmedInput":Ljava/lang/String; */
} // :goto_0
(( java.lang.String ) v1 ).split ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1078 */
/* .local v0, "parts":[Ljava/lang/String; */
/* array-length v3, v0 */
int v4 = 0; // const/4 v4, 0x0
/* if-lez v3, :cond_1 */
/* array-length v3, v0 */
/* sub-int/2addr v3, v2 */
/* aget-object v2, v0, v3 */
} // :cond_1
/* move-object v2, v4 */
/* .line 1079 */
/* .local v2, "apkName":Ljava/lang/String; */
} // :goto_1
/* new-instance v3, Ljava/io/File; */
com.android.server.pm.ScanPackageUtils .getAppLib32InstallDir ( );
/* invoke-direct {v3, v5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 1080 */
/* .local v3, "applib":Ljava/io/File; */
v5 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1081 */
/* .line 1083 */
} // :cond_2
} // .end method
private static void doAandonSession ( android.content.Context p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "sessionId" # I */
/* .line 467 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackageInstaller ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
/* .line 468 */
/* .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller; */
int v1 = 0; // const/4 v1, 0x0
/* .line 470 */
/* .local v1, "session":Landroid/content/pm/PackageInstaller$Session; */
try { // :try_start_0
(( android.content.pm.PackageInstaller ) v0 ).openSession ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;
/* move-object v1, v2 */
/* .line 471 */
(( android.content.pm.PackageInstaller$Session ) v1 ).abandon ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageInstaller$Session;->abandon()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 475 */
/* nop */
} // :goto_0
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 476 */
/* .line 475 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 472 */
/* :catch_0 */
/* move-exception v2 */
/* .line 473 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
v3 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v4 = "doAandonSession failed: "; // const-string v4, "doAandonSession failed: "
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 475 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
/* .line 477 */
} // :goto_1
return;
/* .line 475 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 476 */
/* throw v2 */
} // .end method
private static Boolean doCommitSession ( android.content.Context p0, Integer p1, android.content.IntentSender p2 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "sessionId" # I */
/* .param p2, "target" # Landroid/content/IntentSender; */
/* .line 497 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackageInstaller ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
/* .line 498 */
/* .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller; */
int v1 = 0; // const/4 v1, 0x0
/* .line 500 */
/* .local v1, "session":Landroid/content/pm/PackageInstaller$Session; */
try { // :try_start_0
(( android.content.pm.PackageInstaller ) v0 ).openSession ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;
/* move-object v1, v2 */
/* .line 501 */
(( android.content.pm.PackageInstaller$Session ) v1 ).commit ( p2 ); // invoke-virtual {v1, p2}, Landroid/content/pm/PackageInstaller$Session;->commit(Landroid/content/IntentSender;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 502 */
/* nop */
/* .line 506 */
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 502 */
int v2 = 1; // const/4 v2, 0x1
/* .line 506 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 503 */
/* :catch_0 */
/* move-exception v2 */
/* .line 504 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
v3 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v4 = "doCommitSession failed: "; // const-string v4, "doCommitSession failed: "
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 506 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 507 */
/* nop */
/* .line 508 */
int v2 = 0; // const/4 v2, 0x0
/* .line 506 */
} // :goto_0
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 507 */
/* throw v2 */
} // .end method
private static Integer doCreateSession ( android.content.Context p0, android.content.pm.PackageInstaller$SessionParams p1 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "sessionParams" # Landroid/content/pm/PackageInstaller$SessionParams; */
/* .line 455 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackageInstaller ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
/* .line 456 */
/* .local v0, "packageInstaller":Landroid/content/pm/PackageInstaller; */
int v1 = 0; // const/4 v1, 0x0
/* .line 458 */
/* .local v1, "sessionId":I */
try { // :try_start_0
v2 = (( android.content.pm.PackageInstaller ) v0 ).createSession ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->createSession(Landroid/content/pm/PackageInstaller$SessionParams;)I
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v1, v2 */
/* .line 462 */
/* .line 460 */
/* :catch_0 */
/* move-exception v2 */
/* .line 461 */
/* .local v2, "e":Ljava/io/IOException; */
v3 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v4 = "doCreateSession failed: "; // const-string v4, "doCreateSession failed: "
android.util.Slog .e ( v3,v4,v2 );
/* .line 463 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_0
} // .end method
private Boolean doWriteSession ( android.content.Context p0, java.lang.String p1, java.io.File p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "apkFile" # Ljava/io/File; */
/* .param p4, "sessionId" # I */
/* .line 480 */
int v0 = 0; // const/4 v0, 0x0
/* .line 481 */
/* .local v0, "session":Landroid/content/pm/PackageInstaller$Session; */
int v1 = 0; // const/4 v1, 0x0
/* .line 483 */
/* .local v1, "pfd":Landroid/os/ParcelFileDescriptor; */
/* const/high16 v2, 0x10000000 */
try { // :try_start_0
android.os.ParcelFileDescriptor .open ( p3,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 484 */
} // .end local v1 # "pfd":Landroid/os/ParcelFileDescriptor;
/* .local v9, "pfd":Landroid/os/ParcelFileDescriptor; */
try { // :try_start_1
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v1 ).getPackageInstaller ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
(( android.content.pm.PackageInstaller ) v1 ).openSession ( p4 ); // invoke-virtual {v1, p4}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 485 */
} // .end local v0 # "session":Landroid/content/pm/PackageInstaller$Session;
/* .local v3, "session":Landroid/content/pm/PackageInstaller$Session; */
/* const-wide/16 v5, 0x0 */
try { // :try_start_2
(( android.os.ParcelFileDescriptor ) v9 ).getStatSize ( ); // invoke-virtual {v9}, Landroid/os/ParcelFileDescriptor;->getStatSize()J
/* move-result-wide v7 */
/* move-object v4, p2 */
/* invoke-virtual/range {v3 ..v9}, Landroid/content/pm/PackageInstaller$Session;->write(Ljava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 486 */
/* nop */
/* .line 490 */
libcore.io.IoUtils .closeQuietly ( v9 );
/* .line 491 */
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 486 */
int v0 = 1; // const/4 v0, 0x1
/* .line 490 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object v1, v9 */
/* .line 487 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v1, v9 */
/* .line 490 */
} // .end local v3 # "session":Landroid/content/pm/PackageInstaller$Session;
/* .restart local v0 # "session":Landroid/content/pm/PackageInstaller$Session; */
/* :catchall_1 */
/* move-exception v1 */
/* move-object v3, v0 */
/* move-object v0, v1 */
/* move-object v1, v9 */
/* .line 487 */
/* :catch_1 */
/* move-exception v1 */
/* move-object v3, v0 */
/* move-object v0, v1 */
/* move-object v1, v9 */
/* .line 490 */
} // .end local v9 # "pfd":Landroid/os/ParcelFileDescriptor;
/* .restart local v1 # "pfd":Landroid/os/ParcelFileDescriptor; */
/* :catchall_2 */
/* move-exception v2 */
/* move-object v3, v0 */
/* move-object v0, v2 */
/* .line 487 */
/* :catch_2 */
/* move-exception v2 */
/* move-object v3, v0 */
/* move-object v0, v2 */
/* .line 488 */
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v3 # "session":Landroid/content/pm/PackageInstaller$Session; */
} // :goto_0
try { // :try_start_3
v2 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v4 = "doWriteSession failed: "; // const-string v4, "doWriteSession failed: "
android.util.Slog .e ( v2,v4,v0 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_3 */
/* .line 490 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 491 */
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 492 */
/* nop */
/* .line 493 */
int v0 = 0; // const/4 v0, 0x0
/* .line 490 */
/* :catchall_3 */
/* move-exception v0 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 491 */
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 492 */
/* throw v0 */
} // .end method
private java.util.List getCustAppList ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 325 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 326 */
/* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
v1 = this.mBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v1 ).getCustAppList ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustAppList()Ljava/util/List;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 327 */
v1 = this.mBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v1 ).getCustAppList ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustAppList()Ljava/util/List;
/* .line 329 */
} // :cond_0
v1 = this.mOperatorPreinstallConfig;
(( com.android.server.pm.MiuiOperatorPreinstallConfig ) v1 ).getCustAppList ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->getCustAppList()Ljava/util/List;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 330 */
v1 = this.mOperatorPreinstallConfig;
(( com.android.server.pm.MiuiOperatorPreinstallConfig ) v1 ).getCustAppList ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->getCustAppList()Ljava/util/List;
/* .line 332 */
} // :cond_1
} // .end method
public static com.android.server.pm.MiuiPreinstallHelper getInstance ( ) {
/* .locals 1 */
/* .line 92 */
com.android.server.pm.MiuiPreinstallHelperStub .getInstance ( );
/* check-cast v0, Lcom/android/server/pm/MiuiPreinstallHelper; */
} // .end method
private com.android.server.pm.ResilientAtomicFile getSettingsFile ( ) {
/* .locals 8 */
/* .line 1007 */
/* new-instance v7, Lcom/android/server/pm/ResilientAtomicFile; */
v1 = this.mSettingsFilename;
v2 = this.mPreviousSettingsFilename;
v3 = this.mSettingsReserveCopyFilename;
/* const/16 v4, 0x1b0 */
final String v5 = "package manager preinstall settings"; // const-string v5, "package manager preinstall settings"
/* move-object v0, v7 */
/* move-object v6, p0 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/pm/ResilientAtomicFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;ILjava/lang/String;Lcom/android/server/pm/ResilientAtomicFile$ReadEventLogger;)V */
} // .end method
private java.util.List getVanwardAppList ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 317 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 318 */
/* .local v0, "vanwardAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
v1 = this.mBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v1 ).getVanwardAppList ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getVanwardAppList()Ljava/util/List;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 319 */
v1 = this.mBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v1 ).getVanwardAppList ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getVanwardAppList()Ljava/util/List;
/* .line 321 */
} // :cond_0
} // .end method
private void initLegacyPreinstallList ( ) {
/* .locals 4 */
/* .line 584 */
v0 = this.mLegacyPreinstallAppPaths;
/* .line 585 */
v0 = this.mLegacyPreinstallAppPaths;
v1 = this.mPlatformPreinstallConfig;
/* iget-boolean v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z */
/* iget-boolean v3, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z */
/* .line 586 */
(( com.android.server.pm.MiuiPlatformPreinstallConfig ) v1 ).getLegacyPreinstallList ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->getLegacyPreinstallList(ZZ)Ljava/util/List;
/* .line 587 */
v0 = this.mLegacyPreinstallAppPaths;
v1 = this.mBusinessPreinstallConfig;
/* iget-boolean v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z */
/* iget-boolean v3, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z */
/* .line 588 */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v1 ).getLegacyPreinstallList ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getLegacyPreinstallList(ZZ)Ljava/util/List;
/* .line 589 */
v0 = this.mLegacyPreinstallAppPaths;
v1 = this.mOperatorPreinstallConfig;
/* iget-boolean v2, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z */
/* iget-boolean v3, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z */
/* .line 590 */
(( com.android.server.pm.MiuiOperatorPreinstallConfig ) v1 ).getLegacyPreinstallList ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->getLegacyPreinstallList(ZZ)Ljava/util/List;
/* .line 591 */
return;
} // .end method
private void initPreinstallDirs ( ) {
/* .locals 2 */
/* .line 561 */
v0 = this.mPlatformPreinstallConfig;
(( com.android.server.pm.MiuiPlatformPreinstallConfig ) v0 ).getPreinstallDirs ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->getPreinstallDirs()Ljava/util/List;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 562 */
v0 = this.mPreinstallDirs;
v1 = this.mPlatformPreinstallConfig;
(( com.android.server.pm.MiuiPlatformPreinstallConfig ) v1 ).getPreinstallDirs ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->getPreinstallDirs()Ljava/util/List;
/* .line 566 */
} // :cond_0
final String v0 = "ro.miui.product_to_cust"; // const-string v0, "ro.miui.product_to_cust"
int v1 = -1; // const/4 v1, -0x1
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
v0 = this.mBusinessPreinstallConfig;
/* .line 567 */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).getCustMiuiPreinstallDirs ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustMiuiPreinstallDirs()Ljava/util/List;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 568 */
v0 = this.mPreinstallDirs;
v1 = this.mBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v1 ).getCustMiuiPreinstallDirs ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustMiuiPreinstallDirs()Ljava/util/List;
/* .line 571 */
} // :cond_1
v0 = this.mBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).getPreinstallDirs ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getPreinstallDirs()Ljava/util/List;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 577 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.mPms;
v0 = (( com.android.server.pm.PackageManagerService ) v0 ).isFirstBoot ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
/* if-nez v0, :cond_3 */
/* .line 578 */
} // :cond_2
v0 = this.mPreinstallDirs;
v1 = this.mBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v1 ).getPreinstallDirs ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getPreinstallDirs()Ljava/util/List;
/* .line 581 */
} // :cond_3
return;
} // .end method
private Integer installOne ( android.content.Context p0, com.android.server.pm.MiuiPreinstallHelper$LocalIntentReceiver p1, android.content.pm.parsing.PackageLite p2 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "receiver" # Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver; */
/* .param p3, "packageLite" # Landroid/content/pm/parsing/PackageLite; */
/* .line 405 */
/* nop */
/* .line 406 */
int v0 = -1; // const/4 v0, -0x1
try { // :try_start_0
(( android.content.pm.parsing.PackageLite ) p3 ).getPath ( ); // invoke-virtual {p3}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
(( android.content.pm.parsing.PackageLite ) p3 ).getPackageName ( ); // invoke-virtual {p3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v1 = /* invoke-direct {p0, v1, v2}, Lcom/android/server/pm/MiuiPreinstallHelper;->needLegacyBatchPreinstall(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 407 */
/* .local v1, "useLegacyPreinstall":Z */
/* nop */
/* .line 408 */
/* invoke-direct {p0, p3, v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->makeSessionParams(Landroid/content/pm/parsing/PackageLite;Z)Landroid/content/pm/PackageInstaller$SessionParams; */
/* .line 409 */
/* .local v2, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams; */
v3 = com.android.server.pm.MiuiPreinstallHelper .doCreateSession ( p1,v2 );
/* .line 410 */
/* .local v3, "sessionId":I */
/* if-gtz v3, :cond_0 */
/* .line 411 */
/* .line 414 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 415 */
(( android.content.pm.parsing.PackageLite ) p3 ).getAllApkPaths ( ); // invoke-virtual {p3}, Landroid/content/pm/parsing/PackageLite;->getAllApkPaths()Ljava/util/List;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/lang/String; */
/* .line 416 */
/* .local v5, "splitCodePath":Ljava/lang/String; */
/* new-instance v6, Ljava/io/File; */
/* invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 417 */
/* .local v6, "splitFile":Ljava/io/File; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 418 */
(( java.io.File ) v6 ).getName ( ); // invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;
v7 = /* invoke-direct {p0, p1, v7, v6, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->doWriteSession(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;I)Z */
/* if-nez v7, :cond_1 */
/* .line 419 */
com.android.server.pm.MiuiPreinstallHelper .doAandonSession ( p1,v3 );
/* .line 420 */
/* .line 422 */
} // .end local v5 # "splitCodePath":Ljava/lang/String;
} // .end local v6 # "splitFile":Ljava/io/File;
} // :cond_1
/* .line 425 */
} // :cond_2
(( com.android.server.pm.MiuiPreinstallHelper$LocalIntentReceiver ) p2 ).getIntentSender ( ); // invoke-virtual {p2}, Lcom/android/server/pm/MiuiPreinstallHelper$LocalIntentReceiver;->getIntentSender()Landroid/content/IntentSender;
v4 = com.android.server.pm.MiuiPreinstallHelper .doCommitSession ( p1,v3,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* move v0, v3 */
/* .line 426 */
} // :cond_3
/* nop */
/* .line 425 */
} // :goto_1
/* .line 427 */
} // .end local v1 # "useLegacyPreinstall":Z
} // .end local v2 # "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams;
} // .end local v3 # "sessionId":I
/* :catch_0 */
/* move-exception v1 */
/* .line 428 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to install "; // const-string v4, "Failed to install "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.pm.parsing.PackageLite ) p3 ).getPath ( ); // invoke-virtual {p3}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3,v1 );
/* .line 429 */
} // .end method
private Boolean lambda$cleanUpResource$0 ( java.io.File p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "f" # Ljava/io/File; */
/* .line 788 */
(( java.io.File ) p1 ).getName ( ); // invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;
final String v1 = ".apk"; // const-string v1, ".apk"
v0 = (( java.lang.String ) v0 ).endsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 789 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "list and delete "; // const-string v2, "list and delete "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p1 ).getName ( ); // invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 790 */
(( java.io.File ) p1 ).delete ( ); // invoke-virtual {p1}, Ljava/io/File;->delete()Z
/* .line 791 */
} // :cond_0
v0 = (( java.io.File ) p1 ).isDirectory ( ); // invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 793 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->deleteContentsRecursive(Ljava/io/File;)Z */
/* .line 794 */
(( java.io.File ) p1 ).delete ( ); // invoke-virtual {p1}, Ljava/io/File;->delete()Z
/* .line 796 */
} // :cond_1
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "list unknown file: "; // const-string v2, "list unknown file: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p1 ).getName ( ); // invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 798 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void lambda$onStartJob$1 ( com.android.server.pm.BackgroundPreinstalloptService p0, android.app.job.JobParameters p1 ) { //synthethic
/* .locals 7 */
/* .param p1, "jobService" # Lcom/android/server/pm/BackgroundPreinstalloptService; */
/* .param p2, "params" # Landroid/app/job/JobParameters; */
/* .line 1041 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1042 */
try { // :try_start_0
v1 = this.mMiuiPreinstallApps;
(( android.util.ArrayMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1043 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/MiuiPreinstallApp;>;" */
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/pm/MiuiPreinstallHelper;->shouldCleanUp(Ljava/util/Map$Entry;)Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1044 */
/* check-cast v3, Lcom/android/server/pm/MiuiPreinstallApp; */
(( com.android.server.pm.MiuiPreinstallApp ) v3 ).getApkPath ( ); // invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;
/* invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->deriveAppLib(Ljava/lang/String;)Ljava/io/File; */
/* .line 1045 */
/* .local v3, "appLib":Ljava/io/File; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1046 */
v4 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "will clean up " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v3 ).getAbsolutePath ( ); // invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 1047 */
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {p0, v3, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->removeNativeBinariesFromDir(Ljava/io/File;Z)V */
/* .line 1050 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/MiuiPreinstallApp;>;"
} // .end local v3 # "appLib":Ljava/io/File;
} // :cond_0
/* .line 1051 */
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1053 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.pm.BackgroundPreinstalloptService ) p1 ).jobFinished ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Lcom/android/server/pm/BackgroundPreinstalloptService;->jobFinished(Landroid/app/job/JobParameters;Z)V
/* .line 1054 */
return;
/* .line 1051 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private android.content.pm.PackageInstaller$SessionParams makeSessionParams ( android.content.pm.parsing.PackageLite p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "pkgLite" # Landroid/content/pm/parsing/PackageLite; */
/* .param p2, "useLegacyPreinstall" # Z */
/* .line 434 */
/* new-instance v0, Landroid/content/pm/PackageInstaller$SessionParams; */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;-><init>(I)V */
/* .line 436 */
/* .local v0, "sessionParams":Landroid/content/pm/PackageInstaller$SessionParams; */
int v1 = 0; // const/4 v1, 0x0
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setInstallAsInstantApp ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallAsInstantApp(Z)V
/* .line 437 */
final String v1 = "android"; // const-string v1, "android"
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setInstallerPackageName ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallerPackageName(Ljava/lang/String;)V
/* .line 438 */
(( android.content.pm.parsing.PackageLite ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setAppPackageName ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setAppPackageName(Ljava/lang/String;)V
/* .line 439 */
v1 = (( android.content.pm.parsing.PackageLite ) p1 ).getInstallLocation ( ); // invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getInstallLocation()I
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setInstallLocation ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallLocation(I)V
/* .line 440 */
/* if-nez p2, :cond_0 */
/* .line 442 */
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setIsMiuiPreinstall ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionParams;->setIsMiuiPreinstall()V
/* .line 443 */
(( android.content.pm.parsing.PackageLite ) p1 ).getPath ( ); // invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setApkLocation ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller$SessionParams;->setApkLocation(Ljava/lang/String;)V
/* .line 446 */
} // :cond_0
/* nop */
/* .line 447 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
com.android.internal.content.InstallLocationUtils .calculateInstalledSize ( p1,v1 );
/* move-result-wide v1 */
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setSize ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 450 */
/* .line 448 */
/* :catch_0 */
/* move-exception v1 */
/* .line 449 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/io/File; */
(( android.content.pm.parsing.PackageLite ) p1 ).getBaseApkPath ( ); // invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
(( java.io.File ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/io/File;->length()J
/* move-result-wide v2 */
(( android.content.pm.PackageInstaller$SessionParams ) v0 ).setSize ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V
/* .line 451 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
} // .end method
private Boolean needIgnore ( android.content.pm.parsing.PackageLite p0 ) {
/* .locals 3 */
/* .param p1, "packageLite" # Landroid/content/pm/parsing/PackageLite; */
/* .line 550 */
(( android.content.pm.parsing.PackageLite ) p1 ).getPath ( ); // invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
/* .line 551 */
/* .local v0, "apkPath":Ljava/lang/String; */
(( android.content.pm.parsing.PackageLite ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* .line 552 */
/* .local v1, "pkgName":Ljava/lang/String; */
v2 = this.mPlatformPreinstallConfig;
v2 = (( com.android.server.pm.MiuiPlatformPreinstallConfig ) v2 ).needIgnore ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v2, :cond_1 */
v2 = this.mBusinessPreinstallConfig;
/* .line 553 */
v2 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v2 ).needIgnore ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v2, :cond_1 */
v2 = this.mOperatorPreinstallConfig;
/* .line 554 */
v2 = (( com.android.server.pm.MiuiOperatorPreinstallConfig ) v2 ).needIgnore ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 557 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 555 */
} // :cond_1
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
} // .end method
private Boolean needLegacyBatchPreinstall ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 336 */
v0 = this.mBusinessPreinstallConfig;
v0 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).needLegacyPreinstall ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->needLegacyPreinstall(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
v0 = this.mOperatorPreinstallConfig;
/* .line 337 */
v0 = (( com.android.server.pm.MiuiOperatorPreinstallConfig ) v0 ).needLegacyPreinstall ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->needLegacyPreinstall(Ljava/lang/String;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 340 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 338 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private android.content.pm.parsing.ApkLite parseApkLite ( java.io.File p0 ) {
/* .locals 5 */
/* .param p1, "apkFile" # Ljava/io/File; */
/* .line 833 */
android.content.pm.parsing.result.ParseTypeImpl .forDefaultParsing ( );
/* .line 834 */
/* .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl; */
/* nop */
/* .line 835 */
(( android.content.pm.parsing.result.ParseTypeImpl ) v0 ).reset ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/result/ParseTypeImpl;->reset()Landroid/content/pm/parsing/result/ParseInput;
/* .line 834 */
/* const/16 v2, 0x20 */
android.content.pm.parsing.ApkLiteParseUtils .parseApkLite ( v1,p1,v2 );
/* .line 836 */
v2 = /* .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/ApkLite;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 837 */
v2 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to parseApkLite: "; // const-string v4, "Failed to parseApkLite: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " error: "; // const-string v4, " error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 838 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 837 */
android.util.Slog .e ( v2,v3,v4 );
/* .line 839 */
int v2 = 0; // const/4 v2, 0x0
/* .line 841 */
} // :cond_0
/* check-cast v2, Landroid/content/pm/parsing/ApkLite; */
} // .end method
private android.content.pm.parsing.PackageLite parsedPackageLite ( java.io.File p0 ) {
/* .locals 5 */
/* .param p1, "apkFile" # Ljava/io/File; */
/* .line 821 */
android.content.pm.parsing.result.ParseTypeImpl .forDefaultParsing ( );
/* .line 822 */
/* .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl; */
/* nop */
/* .line 823 */
(( android.content.pm.parsing.result.ParseTypeImpl ) v0 ).reset ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/result/ParseTypeImpl;->reset()Landroid/content/pm/parsing/result/ParseInput;
/* .line 822 */
int v2 = 0; // const/4 v2, 0x0
android.content.pm.parsing.ApkLiteParseUtils .parsePackageLite ( v1,p1,v2 );
/* .line 824 */
v2 = /* .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/PackageLite;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 825 */
v2 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to parsePackageLite: "; // const-string v4, "Failed to parsePackageLite: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " error: "; // const-string v4, " error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 826 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 825 */
android.util.Slog .e ( v2,v3,v4 );
/* .line 827 */
int v2 = 0; // const/4 v2, 0x0
/* .line 829 */
} // :cond_0
/* check-cast v2, Landroid/content/pm/parsing/PackageLite; */
} // .end method
private void readHistory ( ) {
/* .locals 12 */
/* .line 845 */
v0 = this.mPms;
v0 = (( com.android.server.pm.PackageManagerService ) v0 ).isFirstBoot ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 846 */
return;
/* .line 848 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getSettingsFile()Lcom/android/server/pm/ResilientAtomicFile; */
/* .line 849 */
/* .local v0, "atomicFile":Lcom/android/server/pm/ResilientAtomicFile; */
int v1 = 0; // const/4 v1, 0x0
/* .line 851 */
/* .local v1, "str":Ljava/io/FileInputStream; */
try { // :try_start_0
(( com.android.server.pm.ResilientAtomicFile ) v0 ).openRead ( ); // invoke-virtual {v0}, Lcom/android/server/pm/ResilientAtomicFile;->openRead()Ljava/io/FileInputStream;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v1, v2 */
/* .line 852 */
/* if-nez v1, :cond_2 */
/* .line 881 */
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.pm.ResilientAtomicFile ) v0 ).close ( ); // invoke-virtual {v0}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* .line 853 */
} // :cond_1
return;
/* .line 855 */
} // :cond_2
try { // :try_start_1
android.util.Xml .resolvePullParser ( v1 );
/* .line 857 */
/* .local v2, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
v3 = } // :goto_0
/* move v4, v3 */
/* .local v4, "type":I */
int v5 = 1; // const/4 v5, 0x1
int v6 = 2; // const/4 v6, 0x2
/* if-eq v3, v6, :cond_3 */
/* if-eq v4, v5, :cond_3 */
/* .line 861 */
} // :cond_3
/* if-eq v4, v6, :cond_4 */
/* .line 862 */
v3 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v6 = "No start tag found in preinstall settings"; // const-string v6, "No start tag found in preinstall settings"
android.util.Slog .wtf ( v3,v6 );
/* .line 864 */
v3 = } // :cond_4
/* .line 865 */
/* .local v3, "outerDepth":I */
} // :cond_5
v6 = } // :goto_1
/* move v4, v6 */
/* if-eq v6, v5, :cond_9 */
int v6 = 3; // const/4 v6, 0x3
/* if-ne v4, v6, :cond_6 */
v7 = /* .line 866 */
/* if-le v7, v3, :cond_9 */
/* .line 867 */
} // :cond_6
/* if-eq v4, v6, :cond_5 */
int v6 = 4; // const/4 v6, 0x4
/* if-ne v4, v6, :cond_7 */
/* .line 868 */
/* .line 870 */
} // :cond_7
/* .line 871 */
/* .local v6, "tagName":Ljava/lang/String; */
final String v7 = "preinstall_package"; // const-string v7, "preinstall_package"
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_8
/* .line 872 */
final String v7 = "name"; // const-string v7, "name"
int v8 = 0; // const/4 v8, 0x0
/* .line 873 */
/* .local v7, "packageName":Ljava/lang/String; */
/* const-string/jumbo v9, "version" */
/* const-wide/16 v10, 0x0 */
/* move-result-wide v9 */
/* .line 874 */
/* .local v9, "versionCode":J */
final String v11 = "path"; // const-string v11, "path"
/* .line 875 */
/* .local v8, "apkPath":Ljava/lang/String; */
/* new-instance v11, Lcom/android/server/pm/MiuiPreinstallApp; */
/* invoke-direct {v11, v7, v9, v10, v8}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V */
/* invoke-direct {p0, v11}, Lcom/android/server/pm/MiuiPreinstallHelper;->addMiuiPreinstallApp(Lcom/android/server/pm/MiuiPreinstallApp;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 877 */
} // .end local v6 # "tagName":Ljava/lang/String;
} // .end local v7 # "packageName":Ljava/lang/String;
} // .end local v8 # "apkPath":Ljava/lang/String;
} // .end local v9 # "versionCode":J
} // :cond_8
/* .line 880 */
} // .end local v2 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v3 # "outerDepth":I
} // .end local v4 # "type":I
} // :cond_9
/* .line 848 */
} // .end local v1 # "str":Ljava/io/FileInputStream;
/* :catchall_0 */
/* move-exception v1 */
if ( v0 != null) { // if-eqz v0, :cond_a
try { // :try_start_2
(( com.android.server.pm.ResilientAtomicFile ) v0 ).close ( ); // invoke-virtual {v0}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :cond_a
} // :goto_2
/* throw v1 */
/* .line 878 */
/* .restart local v1 # "str":Ljava/io/FileInputStream; */
/* :catch_0 */
/* move-exception v2 */
/* .line 881 */
} // .end local v1 # "str":Ljava/io/FileInputStream;
} // :goto_3
if ( v0 != null) { // if-eqz v0, :cond_b
(( com.android.server.pm.ResilientAtomicFile ) v0 ).close ( ); // invoke-virtual {v0}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* .line 882 */
} // .end local v0 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
} // :cond_b
return;
} // .end method
private void removeNativeBinariesFromDir ( java.io.File p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "nativeLibraryRoot" # Ljava/io/File; */
/* .param p2, "deleteRootDir" # Z */
/* .line 1092 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Deleting native binaries from: "; // const-string v2, "Deleting native binaries from: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 1099 */
v0 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1100 */
(( java.io.File ) p1 ).listFiles ( ); // invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 1101 */
/* .local v0, "files":[Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1102 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "nn":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_2 */
/* .line 1103 */
v2 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " Deleting "; // const-string v4, " Deleting "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v4, v0, v1 */
(( java.io.File ) v4 ).getName ( ); // invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1105 */
/* aget-object v3, v0, v1 */
v3 = (( java.io.File ) v3 ).isDirectory ( ); // invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1106 */
/* aget-object v2, v0, v1 */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {p0, v2, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->removeNativeBinariesFromDir(Ljava/io/File;Z)V */
/* .line 1107 */
} // :cond_0
/* aget-object v3, v0, v1 */
v3 = (( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* if-nez v3, :cond_1 */
/* .line 1108 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Could not delete native binary: "; // const-string v4, "Could not delete native binary: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v4, v0, v1 */
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 1102 */
} // :cond_1
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1114 */
} // .end local v1 # "nn":I
} // :cond_2
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 1115 */
v1 = (( java.io.File ) p1 ).delete ( ); // invoke-virtual {p1}, Ljava/io/File;->delete()Z
/* if-nez v1, :cond_3 */
/* .line 1116 */
v1 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Could not delete native binary directory: "; // const-string v3, "Could not delete native binary directory: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1117 */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1116 */
android.util.Slog .w ( v1,v2 );
/* .line 1121 */
} // .end local v0 # "files":[Ljava/io/File;
} // :cond_3
return;
} // .end method
private Boolean shouldCleanUp ( java.util.Map$Entry p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map$Entry<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/pm/MiuiPreinstallApp;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1060 */
/* .local p1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/pm/MiuiPreinstallApp;>;" */
v0 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* check-cast v1, Ljava/lang/String; */
/* .line 1061 */
/* .local v0, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
/* check-cast v1, Lcom/android/server/pm/MiuiPreinstallApp; */
(( com.android.server.pm.MiuiPreinstallApp ) v1 ).getApkPath ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;
/* .line 1062 */
/* .local v1, "apkPath":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1063 */
/* .line 1065 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_1 */
/* .line 1066 */
/* .line 1068 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_2
v4 = (( java.lang.String ) v4 ).equals ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_2 */
/* .line 1069 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
android.os.Environment .getDataDirectory ( );
(( java.io.File ) v6 ).getPath ( ); // invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "/app"; // const-string v6, "/app"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).startsWith ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1070 */
/* .line 1072 */
} // :cond_2
} // .end method
/* # virtual methods */
public void deleteArtifactsForPreinstall ( com.android.server.pm.PackageSetting p0, Boolean p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .param p2, "deleteCodeAndResources" # Z */
/* .param p3, "userId" # I */
/* .line 234 */
if ( p1 != null) { // if-eqz p1, :cond_4
(( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 235 */
v0 = (( com.android.server.pm.PackageSetting ) p1 ).getPkg ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;
if ( v0 != null) { // if-eqz v0, :cond_4
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 237 */
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
/* if-eq p3, v0, :cond_3 */
/* .line 238 */
v0 = this.mPms;
v0 = this.mInjector;
(( com.android.server.pm.PackageManagerServiceInjector ) v0 ).getUserManagerInternal ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceInjector;->getUserManagerInternal()Lcom/android/server/pm/UserManagerInternal;
(( com.android.server.pm.UserManagerInternal ) v0 ).getUserIds ( ); // invoke-virtual {v0}, Lcom/android/server/pm/UserManagerInternal;->getUserIds()[I
/* .line 239 */
/* .local v0, "userIds":[I */
/* array-length v1, v0 */
/* if-lez v1, :cond_3 */
/* .line 240 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_3 */
/* aget v3, v0, v2 */
/* .line 241 */
/* .local v3, "nextUserId":I */
/* if-ne v3, p3, :cond_1 */
/* .line 242 */
} // :cond_1
v4 = (( com.android.server.pm.PackageSetting ) p1 ).getUserStateOrDefault ( v3 ); // invoke-virtual {p1, v3}, Lcom/android/server/pm/PackageSetting;->getUserStateOrDefault(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 243 */
v1 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v2 = "Still installed by other users, don\'t delete artifacts!"; // const-string v2, "Still installed by other users, don\'t delete artifacts!"
android.util.Slog .d ( v1,v2 );
/* .line 244 */
return;
/* .line 240 */
} // .end local v3 # "nextUserId":I
} // :cond_2
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 249 */
} // .end local v0 # "userIds":[I
} // :cond_3
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.pm.PackageSetting ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " is not installed by other users, will delete oat artifacts and app-libs"; // const-string v2, " is not installed by other users, will delete oat artifacts and app-libs"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 252 */
/* new-instance v0, Ljava/io/File; */
(( com.android.server.pm.PackageSetting ) p1 ).getLegacyNativeLibraryPath ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getLegacyNativeLibraryPath()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 253 */
/* .local v0, "file":Ljava/io/File; */
int v1 = 1; // const/4 v1, 0x1
com.android.internal.content.NativeLibraryHelper .removeNativeBinariesFromDirLI ( v0,v1 );
/* .line 254 */
v1 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v1 ).snapshotComputer ( ); // invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
(( com.android.server.pm.PackageSetting ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;
(( com.android.server.pm.PackageManagerService ) v1 ).deleteOatArtifactsOfPackage ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PackageManagerService;->deleteOatArtifactsOfPackage(Lcom/android/server/pm/Computer;Ljava/lang/String;)J
/* .line 255 */
return;
/* .line 235 */
} // .end local v0 # "file":Ljava/io/File;
} // :cond_4
} // :goto_2
return;
} // .end method
public com.android.server.pm.MiuiBusinessPreinstallConfig getBusinessPreinstallConfig ( ) {
/* .locals 2 */
/* .line 300 */
v0 = this.mBusinessPreinstallConfig;
/* if-nez v0, :cond_0 */
/* .line 301 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v1 = "BusinessPreinstallConfig is not init!"; // const-string v1, "BusinessPreinstallConfig is not init!"
android.util.Slog .w ( v0,v1 );
/* .line 302 */
/* new-instance v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig; */
/* invoke-direct {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;-><init>()V */
this.mBusinessPreinstallConfig = v0;
/* .line 304 */
} // :cond_0
v0 = this.mBusinessPreinstallConfig;
} // .end method
public com.android.server.pm.MiuiOperatorPreinstallConfig getOperatorPreinstallConfig ( ) {
/* .locals 2 */
/* .line 309 */
v0 = this.mOperatorPreinstallConfig;
/* if-nez v0, :cond_0 */
/* .line 310 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v1 = "OperatorPreinstallConfig is not init!"; // const-string v1, "OperatorPreinstallConfig is not init!"
android.util.Slog .w ( v0,v1 );
/* .line 311 */
/* new-instance v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig; */
/* invoke-direct {v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;-><init>()V */
this.mOperatorPreinstallConfig = v0;
/* .line 313 */
} // :cond_0
v0 = this.mOperatorPreinstallConfig;
} // .end method
public com.android.server.pm.MiuiPlatformPreinstallConfig getPlatformPreinstallConfig ( ) {
/* .locals 2 */
/* .line 291 */
v0 = this.mPlatformPreinstallConfig;
/* if-nez v0, :cond_0 */
/* .line 292 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v1 = "PlatformPreinstallConfig is not init!"; // const-string v1, "PlatformPreinstallConfig is not init!"
android.util.Slog .w ( v0,v1 );
/* .line 293 */
/* new-instance v0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig; */
/* invoke-direct {v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;-><init>()V */
this.mPlatformPreinstallConfig = v0;
/* .line 295 */
} // :cond_0
v0 = this.mPlatformPreinstallConfig;
} // .end method
public Integer getPreinstallAppVersion ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 914 */
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) p0 ).isSupportNewFrame ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 915 */
/* .line 919 */
} // :cond_0
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 920 */
try { // :try_start_0
v2 = this.mMiuiPreinstallApps;
v2 = (( android.util.ArrayMap ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->isEmpty()Z
/* if-nez v2, :cond_1 */
v2 = this.mMiuiPreinstallApps;
/* .line 921 */
v2 = (( android.util.ArrayMap ) v2 ).containsKey ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.mBusinessPreinstallConfig;
/* .line 922 */
v2 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v2 ).isCloudOfflineApp ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isCloudOfflineApp(Ljava/lang/String;)Z
/* if-nez v2, :cond_1 */
/* .line 923 */
v1 = this.mMiuiPreinstallApps;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/pm/MiuiPreinstallApp; */
(( com.android.server.pm.MiuiPreinstallApp ) v1 ).getVersionCode ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiPreinstallApp;->getVersionCode()J
/* move-result-wide v1 */
/* long-to-int v1, v1 */
/* monitor-exit v0 */
/* .line 925 */
} // :cond_1
/* monitor-exit v0 */
/* .line 926 */
/* .line 925 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
protected java.util.List getPreinstallApps ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/pm/MiuiPreinstallApp;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 990 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 991 */
/* .local v0, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/MiuiPreinstallApp;>;" */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 992 */
try { // :try_start_0
v2 = this.mMiuiPreinstallApps;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
/* .line 993 */
/* monitor-exit v1 */
/* .line 994 */
/* .line 993 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public java.util.List getPreinstallDirs ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 153 */
v0 = this.mPreinstallDirs;
} // .end method
public void init ( com.android.server.pm.PackageManagerService p0 ) {
/* .locals 4 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .line 97 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* const-string/jumbo v1, "use Miui Preinstall Frame." */
android.util.Slog .d ( v0,v1 );
/* .line 98 */
this.mPms = p1;
/* .line 99 */
v0 = (( com.android.server.pm.PackageManagerService ) p1 ).isFirstBoot ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
/* iput-boolean v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z */
/* .line 100 */
v0 = this.mPms;
v0 = (( com.android.server.pm.PackageManagerService ) v0 ).isDeviceUpgrading ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z
/* iput-boolean v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z */
/* .line 102 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v2, "system" */
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 103 */
/* .local v0, "systemDir":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
/* .line 104 */
(( java.io.File ) v0 ).mkdirs ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
/* .line 105 */
(( java.io.File ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;
/* const/16 v2, 0x1fd */
int v3 = -1; // const/4 v3, -0x1
android.os.FileUtils .setPermissions ( v1,v2,v3,v3 );
/* .line 110 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "preinstall_packages.xml"; // const-string v2, "preinstall_packages.xml"
/* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mSettingsFilename = v1;
/* .line 111 */
/* new-instance v1, Ljava/io/File; */
final String v2 = "preinstall_packages.xml.reservecopy"; // const-string v2, "preinstall_packages.xml.reservecopy"
/* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mSettingsReserveCopyFilename = v1;
/* .line 112 */
/* new-instance v1, Ljava/io/File; */
final String v2 = "preinstall_packages-backup.xml"; // const-string v2, "preinstall_packages-backup.xml"
/* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mPreviousSettingsFilename = v1;
/* .line 114 */
/* new-instance v1, Lcom/android/server/pm/MiuiPlatformPreinstallConfig; */
/* invoke-direct {v1}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;-><init>()V */
this.mPlatformPreinstallConfig = v1;
/* .line 115 */
/* new-instance v1, Lcom/android/server/pm/MiuiBusinessPreinstallConfig; */
/* invoke-direct {v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;-><init>()V */
this.mBusinessPreinstallConfig = v1;
/* .line 116 */
/* new-instance v1, Lcom/android/server/pm/MiuiOperatorPreinstallConfig; */
/* invoke-direct {v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;-><init>()V */
this.mOperatorPreinstallConfig = v1;
/* .line 118 */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 119 */
try { // :try_start_0
v2 = this.mMiuiPreinstallApps;
(( android.util.ArrayMap ) v2 ).clear ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->clear()V
/* .line 120 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->readHistory()V */
/* .line 121 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 122 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->initPreinstallDirs()V */
/* .line 123 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->initLegacyPreinstallList()V */
/* .line 124 */
return;
/* .line 121 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public void init ( com.android.server.pm.PackageManagerService p0, java.util.List p1, java.util.Map p2, com.android.server.pm.MiuiBusinessPreinstallConfig p3 ) {
/* .locals 2 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p4, "miuiBusinessPreinstallConfig" # Lcom/android/server/pm/MiuiBusinessPreinstallConfig; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/pm/PackageManagerService;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/pm/MiuiPreinstallApp;", */
/* ">;", */
/* "Lcom/android/server/pm/MiuiBusinessPreinstallConfig;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 131 */
/* .local p2, "legacyPreinstallAppPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p3, "miuiPreinstallApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/pm/MiuiPreinstallApp;>;" */
this.mPms = p1;
/* .line 132 */
v0 = (( com.android.server.pm.PackageManagerService ) p1 ).isDeviceUpgrading ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z
/* iput-boolean v0, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z */
/* .line 133 */
v0 = this.mLegacyPreinstallAppPaths;
/* .line 134 */
this.mLegacyPreinstallAppPaths = p2;
/* .line 135 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 136 */
try { // :try_start_0
v1 = this.mMiuiPreinstallApps;
(( android.util.ArrayMap ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->clear()V
/* .line 137 */
v1 = this.mMiuiPreinstallApps;
(( android.util.ArrayMap ) v1 ).putAll ( p3 ); // invoke-virtual {v1, p3}, Landroid/util/ArrayMap;->putAll(Ljava/util/Map;)V
/* .line 138 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 139 */
/* new-instance v0, Lcom/android/server/pm/MiuiPlatformPreinstallConfig; */
/* invoke-direct {v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;-><init>()V */
this.mPlatformPreinstallConfig = v0;
/* .line 140 */
this.mBusinessPreinstallConfig = p4;
/* .line 141 */
/* new-instance v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig; */
/* invoke-direct {v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;-><init>()V */
this.mOperatorPreinstallConfig = v0;
/* .line 142 */
return;
/* .line 138 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void insertPreinstallPackageSetting ( com.android.server.pm.PackageSetting p0 ) {
/* .locals 5 */
/* .param p1, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .line 686 */
/* new-instance v0, Lcom/android/server/pm/MiuiPreinstallApp; */
(( com.android.server.pm.PackageSetting ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;
/* .line 687 */
(( com.android.server.pm.PackageSetting ) p1 ).getVersionCode ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getVersionCode()J
/* move-result-wide v2 */
(( com.android.server.pm.PackageSetting ) p1 ).getPath ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPath()Ljava/io/File;
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/pm/MiuiPreinstallApp;-><init>(Ljava/lang/String;JLjava/lang/String;)V */
/* .line 688 */
/* .local v0, "miuiPreinstallApp":Lcom/android/server/pm/MiuiPreinstallApp; */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 689 */
try { // :try_start_0
v2 = this.mMiuiPreinstallApps;
(( com.android.server.pm.PackageSetting ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;
(( android.util.ArrayMap ) v2 ).put ( v3, v0 ); // invoke-virtual {v2, v3, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 690 */
/* monitor-exit v1 */
/* .line 691 */
return;
/* .line 690 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void installCustApps ( ) {
/* .locals 10 */
/* .line 512 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getCustAppList()Ljava/util/List; */
/* .line 513 */
v1 = /* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 514 */
return;
/* .line 516 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 517 */
/* .local v1, "filterList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/parsing/PackageLite;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Ljava/io/File; */
/* .line 518 */
/* .local v3, "apkFile":Ljava/io/File; */
/* invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->parsedPackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite; */
/* .line 519 */
/* .local v4, "packageLite":Landroid/content/pm/parsing/PackageLite; */
/* if-nez v4, :cond_1 */
/* .line 520 */
v5 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "installCustApps parsedPackageLite failed: "; // const-string v7, "installCustApps parsedPackageLite failed: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v6 );
/* .line 521 */
/* .line 523 */
} // :cond_1
v5 = this.mPms;
/* .line 524 */
(( com.android.server.pm.PackageManagerService ) v5 ).snapshotComputer ( ); // invoke-virtual {v5}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
(( android.content.pm.parsing.PackageLite ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* .line 525 */
/* .local v5, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
v6 = this.mLock;
/* monitor-enter v6 */
/* .line 526 */
try { // :try_start_0
v7 = this.mMiuiPreinstallApps;
(( android.content.pm.parsing.PackageLite ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v7 = (( android.util.ArrayMap ) v7 ).containsKey ( v8 ); // invoke-virtual {v7, v8}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* if-nez v5, :cond_2 */
/* .line 527 */
v7 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "installCustApps package had beed uninstalled: "; // const-string v9, "installCustApps package had beed uninstalled: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 528 */
(( android.content.pm.parsing.PackageLite ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " skip!"; // const-string v9, " skip!"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 527 */
android.util.Slog .w ( v7,v8 );
/* .line 529 */
/* monitor-exit v6 */
/* .line 531 */
} // :cond_2
/* monitor-exit v6 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 532 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 533 */
v6 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "installCustApps the app had been installed: "; // const-string v8, "installCustApps the app had been installed: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 534 */
(( java.io.File ) v3 ).getAbsolutePath ( ); // invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " keep first: "; // const-string v8, " keep first: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 533 */
android.util.Slog .w ( v6,v7 );
/* .line 535 */
/* goto/16 :goto_0 */
/* .line 537 */
} // :cond_3
v6 = /* invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->needIgnore(Landroid/content/pm/parsing/PackageLite;)Z */
if ( v6 != null) { // if-eqz v6, :cond_4
/* goto/16 :goto_0 */
/* .line 538 */
} // :cond_4
/* .line 539 */
} // .end local v3 # "apkFile":Ljava/io/File;
} // .end local v4 # "packageLite":Landroid/content/pm/parsing/PackageLite;
} // .end local v5 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
/* goto/16 :goto_0 */
/* .line 531 */
/* .restart local v3 # "apkFile":Ljava/io/File; */
/* .restart local v4 # "packageLite":Landroid/content/pm/parsing/PackageLite; */
/* .restart local v5 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v6 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* .line 541 */
} // .end local v3 # "apkFile":Ljava/io/File;
} // .end local v4 # "packageLite":Landroid/content/pm/parsing/PackageLite;
} // .end local v5 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
v2 = } // :cond_5
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 542 */
return;
/* .line 545 */
} // :cond_6
/* invoke-direct {p0, v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->batchInstallApps(Ljava/util/List;)V */
/* .line 546 */
(( com.android.server.pm.MiuiPreinstallHelper ) p0 ).writeHistory ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->writeHistory()V
/* .line 547 */
return;
} // .end method
public void installVanwardApps ( ) {
/* .locals 9 */
/* .line 258 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getVanwardAppList()Ljava/util/List; */
/* .line 259 */
v1 = /* .local v0, "vanwardAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 260 */
return;
/* .line 262 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 263 */
/* .local v1, "filterList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/parsing/PackageLite;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Ljava/io/File; */
/* .line 264 */
/* .local v3, "apkFile":Ljava/io/File; */
/* invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiPreinstallHelper;->parsedPackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite; */
/* .line 265 */
/* .local v4, "packageLite":Landroid/content/pm/parsing/PackageLite; */
/* if-nez v4, :cond_1 */
/* .line 266 */
v5 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v6 = "installVanwardApps parsedPackageLite failed: apkFile"; // const-string v6, "installVanwardApps parsedPackageLite failed: apkFile"
android.util.Slog .w ( v5,v6 );
/* .line 267 */
/* .line 269 */
} // :cond_1
v5 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v5 ).snapshotComputer ( ); // invoke-virtual {v5}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 270 */
(( android.content.pm.parsing.PackageLite ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* .line 271 */
/* .local v5, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 272 */
v6 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "installVanwardApps the app had been installed: "; // const-string v8, "installVanwardApps the app had been installed: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 273 */
(( java.io.File ) v3 ).getAbsolutePath ( ); // invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " keep first: "; // const-string v8, " keep first: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 272 */
android.util.Slog .w ( v6,v7 );
/* .line 274 */
/* .line 277 */
} // :cond_2
v6 = /* invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->needIgnore(Landroid/content/pm/parsing/PackageLite;)Z */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 278 */
} // :cond_3
/* .line 279 */
} // .end local v3 # "apkFile":Ljava/io/File;
} // .end local v4 # "packageLite":Landroid/content/pm/parsing/PackageLite;
} // .end local v5 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
/* .line 281 */
v2 = } // :cond_4
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 282 */
return;
/* .line 285 */
} // :cond_5
/* invoke-direct {p0, v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->batchInstallApps(Ljava/util/List;)V */
/* .line 286 */
(( com.android.server.pm.MiuiPreinstallHelper ) p0 ).writeHistory ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->writeHistory()V
/* .line 287 */
return;
} // .end method
public Boolean isMiuiPreinstallApp ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 886 */
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) p0 ).isSupportNewFrame ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 887 */
/* .line 889 */
} // :cond_0
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 890 */
try { // :try_start_0
v2 = this.mMiuiPreinstallApps;
v2 = (( android.util.ArrayMap ) v2 ).containsKey ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 891 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 893 */
} // :cond_1
/* monitor-exit v0 */
/* .line 894 */
/* .line 893 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isMiuiPreinstallAppPath ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .line 931 */
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) p0 ).isSupportNewFrame ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 932 */
/* .line 934 */
} // :cond_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_2 */
v0 = this.mBusinessPreinstallConfig;
/* .line 935 */
v0 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).isBusinessPreinstall ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isBusinessPreinstall(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
v0 = this.mBusinessPreinstallConfig;
/* .line 936 */
v0 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).isCustMiuiPreinstall ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isCustMiuiPreinstall(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
v0 = this.mPlatformPreinstallConfig;
/* .line 937 */
v0 = (( com.android.server.pm.MiuiPlatformPreinstallConfig ) v0 ).isPlatformPreinstall ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->isPlatformPreinstall(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
v0 = this.mOperatorPreinstallConfig;
/* .line 938 */
v0 = (( com.android.server.pm.MiuiOperatorPreinstallConfig ) v0 ).isOperatorPreinstall ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->isOperatorPreinstall(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 939 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 941 */
} // :cond_2
} // .end method
public Boolean isPreinstalledPackage ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 898 */
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) p0 ).isSupportNewFrame ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 899 */
/* .line 903 */
} // :cond_0
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 904 */
try { // :try_start_0
v2 = this.mMiuiPreinstallApps;
v2 = (( android.util.ArrayMap ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->isEmpty()Z
/* if-nez v2, :cond_1 */
v2 = this.mMiuiPreinstallApps;
/* .line 905 */
v2 = (( android.util.ArrayMap ) v2 ).containsKey ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.mBusinessPreinstallConfig;
/* .line 906 */
v2 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v2 ).isCloudOfflineApp ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isCloudOfflineApp(Ljava/lang/String;)Z
/* if-nez v2, :cond_1 */
/* .line 907 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 909 */
} // :cond_1
/* monitor-exit v0 */
/* .line 910 */
/* .line 909 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isSupportNewFrame ( ) {
/* .locals 2 */
/* .line 147 */
/* const/16 v1, 0x21 */
/* if-le v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void logEvent ( Integer p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "priority" # I */
/* .param p2, "msg" # Ljava/lang/String; */
/* .line 1126 */
return;
} // .end method
public Boolean onStartJob ( com.android.server.pm.BackgroundPreinstalloptService p0, android.app.job.JobParameters p1 ) {
/* .locals 4 */
/* .param p1, "jobService" # Lcom/android/server/pm/BackgroundPreinstalloptService; */
/* .param p2, "params" # Landroid/app/job/JobParameters; */
/* .line 1029 */
v0 = this.mPms;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1030 */
v0 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v2 = "clean up faild, reason: pkms is null"; // const-string v2, "clean up faild, reason: pkms is null"
android.util.Slog .e ( v0,v2 );
/* .line 1031 */
/* .line 1033 */
} // :cond_0
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1034 */
try { // :try_start_0
v2 = this.mMiuiPreinstallApps;
v2 = (( android.util.ArrayMap ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1035 */
v2 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v3 = "clean up faild, reason: mMiuiPreinstallApps is empty"; // const-string v3, "clean up faild, reason: mMiuiPreinstallApps is empty"
android.util.Slog .e ( v2,v3 );
/* .line 1036 */
/* monitor-exit v0 */
/* .line 1038 */
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1040 */
v0 = this.mPms;
v0 = this.mBackgroundHandler;
/* new-instance v1, Lcom/android/server/pm/MiuiPreinstallHelper$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/pm/MiuiPreinstallHelper$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/pm/MiuiPreinstallHelper;Lcom/android/server/pm/BackgroundPreinstalloptService;Landroid/app/job/JobParameters;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1056 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1038 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void performLegacyCopyPreinstall ( ) {
/* .locals 19 */
/* .line 595 */
/* move-object/from16 v0, p0 */
/* const-string/jumbo v1, "vold.decrypt" */
android.os.SystemProperties .get ( v1 );
/* .line 596 */
/* .local v1, "cryptState":Ljava/lang/String; */
/* const-string/jumbo v2, "trigger_restart_min_framework" */
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 597 */
v2 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v3 = "Detected encryption in progress - can\'t copy preinstall apps now!"; // const-string v3, "Detected encryption in progress - can\'t copy preinstall apps now!"
android.util.Slog .w ( v2,v3 );
/* .line 598 */
return;
/* .line 602 */
} // :cond_0
/* iget-boolean v2, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z */
/* if-nez v2, :cond_1 */
/* iget-boolean v2, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z */
/* if-nez v2, :cond_1 */
/* .line 603 */
return;
/* .line 606 */
} // :cond_1
v2 = v2 = this.mLegacyPreinstallAppPaths;
/* if-nez v2, :cond_2 */
/* .line 607 */
return;
/* .line 609 */
} // :cond_2
/* new-instance v3, Lcom/android/server/pm/DeletePackageHelper; */
v2 = this.mPms;
/* invoke-direct {v3, v2}, Lcom/android/server/pm/DeletePackageHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V */
/* .line 611 */
/* .local v3, "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
v2 = this.mLegacyPreinstallAppPaths;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_f
/* move-object v10, v4 */
/* check-cast v10, Ljava/lang/String; */
/* .line 612 */
/* .local v10, "path":Ljava/lang/String; */
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v11, v4 */
/* .line 613 */
/* .local v11, "targetAppDir":Ljava/io/File; */
v4 = (( java.io.File ) v11 ).exists ( ); // invoke-virtual {v11}, Ljava/io/File;->exists()Z
final String v5 = "copyLegacyPreisntallApp: "; // const-string v5, "copyLegacyPreisntallApp: "
/* if-nez v4, :cond_3 */
/* .line 614 */
v4 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " is not exists!"; // const-string v6, " is not exists!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* .line 615 */
/* .line 617 */
} // :cond_3
(( java.io.File ) v11 ).listFiles ( ); // invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 618 */
/* .local v12, "files":[Ljava/io/File; */
v4 = com.android.internal.util.ArrayUtils .isEmpty ( v12 );
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 619 */
v4 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "there is none apk file in path: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* .line 620 */
/* .line 622 */
} // :cond_4
/* invoke-direct {v0, v11}, Lcom/android/server/pm/MiuiPreinstallHelper;->parsedPackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite; */
/* .line 623 */
/* .local v13, "packageLite":Landroid/content/pm/parsing/PackageLite; */
/* if-nez v13, :cond_5 */
/* .line 624 */
v4 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "parsedPackageLite failed: "; // const-string v6, "parsedPackageLite failed: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v5 );
/* .line 625 */
/* .line 627 */
} // :cond_5
(( android.content.pm.parsing.PackageLite ) v13 ).getPackageName ( ); // invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* .line 630 */
/* .local v14, "pkgName":Ljava/lang/String; */
v4 = /* invoke-direct {v0, v13}, Lcom/android/server/pm/MiuiPreinstallHelper;->needIgnore(Landroid/content/pm/parsing/PackageLite;)Z */
if ( v4 != null) { // if-eqz v4, :cond_6
/* goto/16 :goto_0 */
/* .line 632 */
} // :cond_6
v15 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.pm.parsing.PackageLite ) v13 ).getPackageName ( ); // invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v15,v4 );
/* .line 633 */
/* iget-boolean v4, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsFirstBoot:Z */
int v5 = 0; // const/4 v5, 0x0
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 634 */
/* invoke-direct {v0, v10, v5, v13}, Lcom/android/server/pm/MiuiPreinstallHelper;->copyLegacyPreisntallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Landroid/content/pm/parsing/PackageLite;)V */
/* move-object/from16 v18, v1 */
/* goto/16 :goto_2 */
/* .line 635 */
} // :cond_7
/* iget-boolean v4, v0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z */
if ( v4 != null) { // if-eqz v4, :cond_e
/* .line 639 */
v4 = this.mMiuiPreinstallApps;
v4 = (( android.util.ArrayMap ) v4 ).containsKey ( v14 ); // invoke-virtual {v4, v14}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_d
/* .line 640 */
v4 = this.mPms;
/* .line 641 */
(( com.android.server.pm.PackageManagerService ) v4 ).snapshotComputer ( ); // invoke-virtual {v4}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 642 */
/* .local v9, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
/* if-nez v9, :cond_8 */
/* .line 644 */
/* goto/16 :goto_0 */
/* .line 647 */
} // :cond_8
/* move-result-wide v4 */
(( android.content.pm.parsing.PackageLite ) v13 ).getLongVersionCode ( ); // invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getLongVersionCode()J
/* move-result-wide v6 */
/* cmp-long v4, v4, v6 */
/* if-ltz v4, :cond_9 */
/* .line 648 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( android.content.pm.parsing.PackageLite ) v13 ).getPath ( ); // invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " is not newer than "; // const-string v5, " is not newer than "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 649 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "["; // const-string v5, "["
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 650 */
/* move-result-wide v5 */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "], skip coping"; // const-string v5, "], skip coping"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 648 */
android.util.Slog .w ( v15,v4 );
/* .line 651 */
/* goto/16 :goto_0 */
/* .line 654 */
} // :cond_9
/* new-instance v4, Ljava/io/File; */
(( android.content.pm.parsing.PackageLite ) v13 ).getBaseApkPath ( ); // invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;
/* invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v4}, Lcom/android/server/pm/MiuiPreinstallHelper;->parseApkLite(Ljava/io/File;)Landroid/content/pm/parsing/ApkLite; */
/* .line 655 */
/* .local v16, "apkLite":Landroid/content/pm/parsing/ApkLite; */
/* if-nez v16, :cond_a */
/* .line 656 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "parseApkLite failed: "; // const-string v5, "parseApkLite failed: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.pm.parsing.PackageLite ) v13 ).getBaseApkPath ( ); // invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v15,v4 );
/* .line 657 */
/* goto/16 :goto_0 */
/* .line 659 */
} // :cond_a
/* nop */
/* .line 660 */
/* invoke-virtual/range {v16 ..v16}, Landroid/content/pm/parsing/ApkLite;->getSigningDetails()Landroid/content/pm/SigningDetails; */
(( android.content.pm.SigningDetails ) v4 ).getSignatures ( ); // invoke-virtual {v4}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;
/* .line 661 */
(( android.content.pm.SigningDetails ) v5 ).getSignatures ( ); // invoke-virtual {v5}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;
/* .line 660 */
v4 = com.android.server.pm.PackageManagerServiceUtils .compareSignatures ( v4,v5 );
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 663 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* invoke-virtual/range {v16 ..v16}, Landroid/content/pm/parsing/ApkLite;->getPath()Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " mismatch signature with "; // const-string v5, " mismatch signature with "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 664 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ", delete it\'s resources and data before coping"; // const-string v5, ", delete it\'s resources and data before coping"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 663 */
android.util.Slog .e ( v15,v4 );
/* .line 666 */
/* .line 667 */
/* move-result-wide v5 */
int v7 = 0; // const/4 v7, 0x0
int v8 = 2; // const/4 v8, 0x2
/* const/16 v17, 0x1 */
/* .line 666 */
/* move-object/from16 v18, v1 */
/* move-object v1, v9 */
} // .end local v9 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
/* .local v1, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .local v18, "cryptState":Ljava/lang/String; */
/* move/from16 v9, v17 */
v4 = /* invoke-virtual/range {v3 ..v9}, Lcom/android/server/pm/DeletePackageHelper;->deletePackageX(Ljava/lang/String;JIIZ)I */
int v5 = 1; // const/4 v5, 0x1
/* if-eq v4, v5, :cond_c */
/* .line 670 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Delete mismatch signature app "; // const-string v5, "Delete mismatch signature app "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v14 ); // invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " failed, skip coping "; // const-string v5, " failed, skip coping "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 671 */
(( android.content.pm.parsing.PackageLite ) v13 ).getPath ( ); // invoke-virtual {v13}, Landroid/content/pm/parsing/PackageLite;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 670 */
android.util.Slog .e ( v15,v4 );
/* .line 660 */
} // .end local v18 # "cryptState":Ljava/lang/String;
/* .local v1, "cryptState":Ljava/lang/String; */
/* .restart local v9 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
} // :cond_b
/* move-object/from16 v18, v1 */
/* move-object v1, v9 */
/* .line 675 */
} // .end local v9 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
/* .local v1, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .restart local v18 # "cryptState":Ljava/lang/String; */
} // :cond_c
} // :goto_1
/* invoke-direct {v0, v10, v1, v13}, Lcom/android/server/pm/MiuiPreinstallHelper;->copyLegacyPreisntallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Landroid/content/pm/parsing/PackageLite;)V */
/* .line 676 */
} // .end local v1 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
} // .end local v16 # "apkLite":Landroid/content/pm/parsing/ApkLite;
/* .line 678 */
} // .end local v18 # "cryptState":Ljava/lang/String;
/* .local v1, "cryptState":Ljava/lang/String; */
} // :cond_d
/* move-object/from16 v18, v1 */
} // .end local v1 # "cryptState":Ljava/lang/String;
/* .restart local v18 # "cryptState":Ljava/lang/String; */
/* invoke-direct {v0, v10, v5, v13}, Lcom/android/server/pm/MiuiPreinstallHelper;->copyLegacyPreisntallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Landroid/content/pm/parsing/PackageLite;)V */
/* .line 635 */
} // .end local v18 # "cryptState":Ljava/lang/String;
/* .restart local v1 # "cryptState":Ljava/lang/String; */
} // :cond_e
/* move-object/from16 v18, v1 */
/* .line 681 */
} // .end local v1 # "cryptState":Ljava/lang/String;
} // .end local v10 # "path":Ljava/lang/String;
} // .end local v11 # "targetAppDir":Ljava/io/File;
} // .end local v12 # "files":[Ljava/io/File;
} // .end local v13 # "packageLite":Landroid/content/pm/parsing/PackageLite;
} // .end local v14 # "pkgName":Ljava/lang/String;
/* .restart local v18 # "cryptState":Ljava/lang/String; */
} // :goto_2
/* move-object/from16 v1, v18 */
/* goto/16 :goto_0 */
/* .line 682 */
} // .end local v18 # "cryptState":Ljava/lang/String;
/* .restart local v1 # "cryptState":Ljava/lang/String; */
} // :cond_f
return;
} // .end method
public void scheduleJob ( ) {
/* .locals 4 */
/* .line 1014 */
/* new-instance v0, Landroid/app/job/JobInfo$Builder; */
/* new-instance v1, Landroid/content/ComponentName; */
/* const-class v2, Lcom/android/server/pm/BackgroundPreinstalloptService; */
/* .line 1016 */
(( java.lang.Class ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;
final String v3 = "android"; // const-string v3, "android"
/* invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* const v2, 0x1ceea9 */
/* invoke-direct {v0, v2, v1}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
v1 = java.util.concurrent.TimeUnit.DAYS;
/* .line 1017 */
/* const-wide/16 v2, 0x1 */
(( java.util.concurrent.TimeUnit ) v1 ).toMillis ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
/* move-result-wide v1 */
(( android.app.job.JobInfo$Builder ) v0 ).setPeriodic ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;
/* .line 1018 */
int v1 = 1; // const/4 v1, 0x1
(( android.app.job.JobInfo$Builder ) v0 ).setRequiresDeviceIdle ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/job/JobInfo$Builder;->setRequiresDeviceIdle(Z)Landroid/app/job/JobInfo$Builder;
/* .line 1019 */
(( android.app.job.JobInfo$Builder ) v0 ).setRequiresCharging ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/job/JobInfo$Builder;->setRequiresCharging(Z)Landroid/app/job/JobInfo$Builder;
/* .line 1020 */
(( android.app.job.JobInfo$Builder ) v0 ).setRequiresBatteryNotLow ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;
/* .line 1021 */
(( android.app.job.JobInfo$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
/* .line 1022 */
/* .local v0, "jobInfo":Landroid/app/job/JobInfo; */
v1 = this.mPms;
v1 = this.mContext;
/* .line 1023 */
final String v2 = "jobscheduler"; // const-string v2, "jobscheduler"
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/job/JobScheduler; */
/* .line 1024 */
/* .local v1, "jobScheduler":Landroid/app/job/JobScheduler; */
(( android.app.job.JobScheduler ) v1 ).schedule ( v0 ); // invoke-virtual {v1, v0}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
/* .line 1025 */
return;
} // .end method
public Boolean shouldIgnoreInstall ( com.android.server.pm.parsing.pkg.ParsedPackage p0 ) {
/* .locals 13 */
/* .param p1, "parsedPackage" # Lcom/android/server/pm/parsing/pkg/ParsedPackage; */
/* .line 158 */
/* .line 159 */
/* .local v0, "pkgName":Ljava/lang/String; */
/* move-result-wide v1 */
/* .line 160 */
/* .local v1, "versionCode":J */
/* .line 163 */
/* .local v3, "apkPath":Ljava/lang/String; */
v4 = v4 = this.mLegacyPreinstallAppPaths;
int v5 = 1; // const/4 v5, 0x1
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 164 */
v4 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "package use legacy preinstall: "; // const-string v7, "package use legacy preinstall: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " path: "; // const-string v7, " path: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v6 );
/* .line 165 */
/* .line 167 */
} // :cond_0
v4 = this.mLock;
/* monitor-enter v4 */
/* .line 169 */
try { // :try_start_0
v6 = this.mMiuiPreinstallApps;
v6 = (( android.util.ArrayMap ) v6 ).containsKey ( v0 ); // invoke-virtual {v6, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z
int v7 = 0; // const/4 v7, 0x0
if ( v6 != null) { // if-eqz v6, :cond_a
/* .line 170 */
v6 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v6 ).snapshotComputer ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 171 */
/* .local v6, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
/* if-nez v6, :cond_1 */
/* .line 173 */
v7 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "package had beed uninstalled: "; // const-string v9, "package had beed uninstalled: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " skip!"; // const-string v9, " skip!"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v8 );
/* .line 174 */
/* monitor-exit v4 */
/* .line 177 */
} // :cond_1
if ( v8 != null) { // if-eqz v8, :cond_3
v8 = this.mBusinessPreinstallConfig;
/* .line 178 */
v8 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v8 ).isBusinessPreinstall ( v9 ); // invoke-virtual {v8, v9}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isBusinessPreinstall(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 179 */
/* move-result-wide v8 */
/* cmp-long v8, v8, v1 */
/* if-nez v8, :cond_2 */
v8 = (( java.lang.String ) v3 ).equals ( v8 ); // invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v8, :cond_3 */
/* .line 180 */
} // :cond_2
v7 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "business preinstall app must not update: "; // const-string v9, "business preinstall app must not update: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " do not scan the path: "; // const-string v9, " do not scan the path: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v7,v8 );
/* .line 182 */
/* monitor-exit v4 */
/* .line 186 */
} // :cond_3
if ( v8 != null) { // if-eqz v8, :cond_9
/* .line 187 */
v8 = (( java.lang.String ) v8 ).equals ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v8, :cond_9 */
/* .line 188 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
android.os.Environment .getDataDirectory ( );
(( java.io.File ) v10 ).getPath ( ); // invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = "/app"; // const-string v10, "/app"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v8 = (( java.lang.String ) v8 ).startsWith ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v8 != null) { // if-eqz v8, :cond_9
/* .line 189 */
/* iget-boolean v8, p0, Lcom/android/server/pm/MiuiPreinstallHelper;->mIsDeviceUpgrading:Z */
if ( v8 != null) { // if-eqz v8, :cond_8
v8 = this.mBusinessPreinstallConfig;
/* .line 190 */
v8 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v8 ).isBusinessPreinstall ( v3 ); // invoke-virtual {v8, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isBusinessPreinstall(Ljava/lang/String;)Z
/* if-nez v8, :cond_8 */
/* .line 191 */
/* move-result-wide v8 */
/* cmp-long v8, v8, v1 */
/* if-gez v8, :cond_8 */
/* .line 192 */
/* new-instance v8, Ljava/io/File; */
/* invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {p0, v8}, Lcom/android/server/pm/MiuiPreinstallHelper;->parseApkLite(Ljava/io/File;)Landroid/content/pm/parsing/ApkLite; */
/* .line 193 */
/* .local v8, "apkLite":Landroid/content/pm/parsing/ApkLite; */
/* if-nez v8, :cond_4 */
/* .line 194 */
v9 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "parseApkLite failed: "; // const-string v11, "parseApkLite failed: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v9,v10 );
/* .line 197 */
} // :cond_4
if ( v8 != null) { // if-eqz v8, :cond_5
(( android.content.pm.parsing.ApkLite ) v8 ).getSigningDetails ( ); // invoke-virtual {v8}, Landroid/content/pm/parsing/ApkLite;->getSigningDetails()Landroid/content/pm/SigningDetails;
(( android.content.pm.SigningDetails ) v9 ).getSignatures ( ); // invoke-virtual {v9}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;
} // :cond_5
int v9 = 0; // const/4 v9, 0x0
/* .line 198 */
} // :goto_0
(( android.content.pm.SigningDetails ) v10 ).getSignatures ( ); // invoke-virtual {v10}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;
/* .line 196 */
v9 = com.android.server.pm.PackageManagerServiceUtils .compareSignatures ( v9,v10 );
/* if-nez v9, :cond_6 */
/* move v7, v5 */
/* .line 200 */
/* .local v7, "sameSignatures":Z */
} // :cond_6
if ( v7 != null) { // if-eqz v7, :cond_7
/* .line 201 */
v9 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "package had beed updated: "; // const-string v11, "package had beed updated: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " versionCode: "; // const-string v11, " versionCode: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 202 */
/* move-result-wide v11 */
(( java.lang.StringBuilder ) v10 ).append ( v11, v12 ); // invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = ", but current version is bertter: "; // const-string v11, ", but current version is bertter: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v1, v2 ); // invoke-virtual {v10, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 201 */
android.util.Slog .d ( v9,v10 );
/* .line 204 */
/* invoke-direct {p0, v3, v6, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->copyMiuiPreinstallApp(Ljava/lang/String;Lcom/android/server/pm/pkg/PackageStateInternal;Lcom/android/server/pm/parsing/pkg/ParsedPackage;)V */
/* .line 206 */
} // :cond_7
v9 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v3 ); // invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " mismatch signature with "; // const-string v11, " mismatch signature with "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", need skip copy!"; // const-string v11, ", need skip copy!"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v9,v10 );
/* .line 210 */
} // .end local v7 # "sameSignatures":Z
} // .end local v8 # "apkLite":Landroid/content/pm/parsing/ApkLite;
} // :cond_8
} // :goto_1
v7 = com.android.server.pm.MiuiPreinstallHelper.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "package had beed updated: "; // const-string v9, "package had beed updated: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " skip!"; // const-string v9, " skip!"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v8 );
/* .line 211 */
/* monitor-exit v4 */
/* .line 214 */
} // :cond_9
/* monitor-exit v4 */
/* .line 217 */
} // .end local v6 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
} // :cond_a
/* sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v6 != null) { // if-eqz v6, :cond_d
/* .line 218 */
v6 = this.mBusinessPreinstallConfig;
v6 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v6 ).isBusinessPreinstall ( v3 ); // invoke-virtual {v6, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isBusinessPreinstall(Ljava/lang/String;)Z
/* if-nez v6, :cond_c */
v6 = this.mPlatformPreinstallConfig;
/* .line 219 */
v6 = (( com.android.server.pm.MiuiPlatformPreinstallConfig ) v6 ).needIgnore ( v3, v0 ); // invoke-virtual {v6, v3, v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v6, :cond_c */
v6 = this.mOperatorPreinstallConfig;
/* .line 220 */
v6 = (( com.android.server.pm.MiuiOperatorPreinstallConfig ) v6 ).needIgnore ( v3, v0 ); // invoke-virtual {v6, v3, v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_b
} // :cond_b
/* move v5, v7 */
} // :cond_c
} // :goto_2
/* nop */
} // :goto_3
/* monitor-exit v4 */
/* .line 218 */
/* .line 223 */
} // :cond_d
v6 = this.mPlatformPreinstallConfig;
v6 = (( com.android.server.pm.MiuiPlatformPreinstallConfig ) v6 ).needIgnore ( v3, v0 ); // invoke-virtual {v6, v3, v0}, Lcom/android/server/pm/MiuiPlatformPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v6, :cond_f */
v6 = this.mBusinessPreinstallConfig;
/* .line 224 */
v6 = (( com.android.server.pm.MiuiBusinessPreinstallConfig ) v6 ).needIgnore ( v3, v0 ); // invoke-virtual {v6, v3, v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_e
} // :cond_e
/* move v5, v7 */
} // :cond_f
} // :goto_4
/* nop */
} // :goto_5
/* monitor-exit v4 */
/* .line 223 */
/* .line 227 */
/* :catchall_0 */
/* move-exception v5 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v5 */
} // .end method
public void writeHistory ( ) {
/* .locals 12 */
/* .line 946 */
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) p0 ).isSupportNewFrame ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
/* if-nez v0, :cond_0 */
/* .line 947 */
return;
/* .line 949 */
} // :cond_0
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 950 */
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 951 */
/* .local v1, "startTime":J */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getSettingsFile()Lcom/android/server/pm/ResilientAtomicFile; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 952 */
/* .local v3, "atomicFile":Lcom/android/server/pm/ResilientAtomicFile; */
int v4 = 0; // const/4 v4, 0x0
/* .line 954 */
/* .local v4, "str":Ljava/io/FileOutputStream; */
try { // :try_start_1
(( com.android.server.pm.ResilientAtomicFile ) v3 ).startWrite ( ); // invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v4, v5 */
/* .line 956 */
android.util.Xml .resolveSerializer ( v4 );
/* .line 957 */
/* .local v5, "serializer":Lcom/android/modules/utils/TypedXmlSerializer; */
int v6 = 1; // const/4 v6, 0x1
java.lang.Boolean .valueOf ( v6 );
int v8 = 0; // const/4 v8, 0x0
/* .line 958 */
final String v7 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 961 */
final String v6 = "preinstall_packages"; // const-string v6, "preinstall_packages"
/* .line 963 */
v6 = this.mMiuiPreinstallApps;
(( android.util.ArrayMap ) v6 ).values ( ); // invoke-virtual {v6}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_1
/* check-cast v7, Lcom/android/server/pm/MiuiPreinstallApp; */
/* .line 964 */
/* .local v7, "miuiPreinstallApp":Lcom/android/server/pm/MiuiPreinstallApp; */
final String v9 = "preinstall_package"; // const-string v9, "preinstall_package"
/* .line 965 */
final String v9 = "name"; // const-string v9, "name"
(( com.android.server.pm.MiuiPreinstallApp ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallApp;->getPackageName()Ljava/lang/String;
/* .line 966 */
/* const-string/jumbo v9, "version" */
(( com.android.server.pm.MiuiPreinstallApp ) v7 ).getVersionCode ( ); // invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallApp;->getVersionCode()J
/* move-result-wide v10 */
/* .line 967 */
final String v9 = "path"; // const-string v9, "path"
(( com.android.server.pm.MiuiPreinstallApp ) v7 ).getApkPath ( ); // invoke-virtual {v7}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;
/* .line 968 */
final String v9 = "preinstall_package"; // const-string v9, "preinstall_package"
/* .line 969 */
/* nop */
} // .end local v7 # "miuiPreinstallApp":Lcom/android/server/pm/MiuiPreinstallApp;
/* .line 971 */
} // :cond_1
final String v6 = "preinstall_packages"; // const-string v6, "preinstall_packages"
/* .line 973 */
/* .line 975 */
(( com.android.server.pm.ResilientAtomicFile ) v3 ).finishWrite ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* .line 976 */
final String v6 = "preinstall_package"; // const-string v6, "preinstall_package"
/* .line 977 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v1 */
/* .line 976 */
com.android.internal.logging.EventLogTags .writeCommitSysConfigFile ( v6,v7,v8 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 984 */
} // .end local v5 # "serializer":Lcom/android/modules/utils/TypedXmlSerializer;
/* .line 951 */
} // .end local v4 # "str":Ljava/io/FileOutputStream;
/* :catchall_0 */
/* move-exception v4 */
/* .line 978 */
/* .restart local v4 # "str":Ljava/io/FileOutputStream; */
/* :catch_0 */
/* move-exception v5 */
/* .line 979 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_2
v6 = com.android.server.pm.MiuiPreinstallHelper.TAG;
final String v7 = "Unable to write preinstall settings, current changes will be lost at reboot"; // const-string v7, "Unable to write preinstall settings, current changes will be lost at reboot"
android.util.Slog .e ( v6,v7,v5 );
/* .line 981 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 982 */
(( com.android.server.pm.ResilientAtomicFile ) v3 ).failWrite ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 985 */
} // .end local v4 # "str":Ljava/io/FileOutputStream;
} // .end local v5 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_3
try { // :try_start_3
(( com.android.server.pm.ResilientAtomicFile ) v3 ).close ( ); // invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* .line 986 */
} // .end local v1 # "startTime":J
} // .end local v3 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
} // :cond_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* .line 987 */
return;
/* .line 951 */
/* .restart local v1 # "startTime":J */
/* .restart local v3 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile; */
} // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_4
try { // :try_start_4
(( com.android.server.pm.ResilientAtomicFile ) v3 ).close ( ); // invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_5
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/pm/MiuiPreinstallHelper;
} // :cond_4
} // :goto_3
/* throw v4 */
/* .line 986 */
} // .end local v1 # "startTime":J
} // .end local v3 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiPreinstallHelper; */
/* :catchall_2 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* throw v1 */
} // .end method
