.class public Lcom/android/server/pm/MiuiOperatorPreinstallConfig;
.super Lcom/android/server/pm/MiuiPreinstallConfig;
.source "MiuiOperatorPreinstallConfig.java"


# static fields
.field private static final ALLOW_INSTALL_THIRD_APP_LIST:Ljava/lang/String; = "allow_install_third_app.list"

.field private static final DEL_APPS_LIST:Ljava/lang/String; = "/product/opcust/common/del_apps_list.xml"

.field private static final DISALLOW_INSTALL_THIRD_APP_LIST:Ljava/lang/String; = "disallow_install_third_app.list"

.field private static final MIUI_OPERATOR_PREINALL_PATH:Ljava/lang/String; = "/product/opcust/data-app"

.field private static final O2SPACE_PACKAGE_NAME:Ljava/lang/String; = "uk.co.o2.android.o2space"

.field private static final OPERATOR_PREINSTALL_INFO_DIR:Ljava/lang/String; = "/mi_ext/product/etc/"

.field private static final OPERATOR_PRODUCT_DATA_APP_LIST:Ljava/lang/String; = "operator_product_data_app.list"

.field private static final PRODUCT_CARRIER_APP_DIR:Ljava/io/File;

.field private static final PRODUCT_CARRIER_DIR:Ljava/lang/String; = "/product/opcust"

.field private static final PRODUCT_DATA_APP_DIR:Ljava/io/File;

.field private static final TAG:Ljava/lang/String;

.field private static final sCustomizedRegion:Ljava/lang/String;


# instance fields
.field private final sAllowOnlyInstallThirdApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sCotaDelApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sDisallowInstallThirdApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    const-class v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->TAG:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/io/File;

    const-string v1, "/product/opcust/data-app"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->PRODUCT_CARRIER_APP_DIR:Ljava/io/File;

    .line 31
    new-instance v0, Ljava/io/File;

    const-string v1, "/product/data-app"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->PRODUCT_DATA_APP_DIR:Ljava/io/File;

    .line 46
    nop

    .line 47
    const-string v0, "ro.miui.customized.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sCustomizedRegion:Ljava/lang/String;

    .line 46
    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .line 50
    invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallConfig;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sAllowOnlyInstallThirdApps:Ljava/util/Set;

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sDisallowInstallThirdApps:Ljava/util/Set;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sCotaDelApps:Ljava/util/Set;

    .line 51
    invoke-direct {p0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->initCotaDelAppsList()V

    .line 52
    return-void
.end method

.method private addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V
    .locals 8
    .param p2, "appDir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 140
    .local p1, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .local p3, "filterSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 141
    .local v0, "apps":[Ljava/io/File;
    if-eqz v0, :cond_4

    .line 143
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    aget-object v3, v0, v2

    .line 144
    .local v3, "app":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    const-string v5, ".apk"

    if-eqz v4, :cond_0

    .line 146
    new-instance v4, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 147
    .local v4, "apk":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 148
    goto :goto_1

    .line 152
    .end local v4    # "apk":Ljava/io/File;
    :cond_0
    move-object v4, v3

    .line 153
    .restart local v4    # "apk":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 154
    goto :goto_1

    .line 157
    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p3, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 158
    goto :goto_1

    .line 160
    :cond_2
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    .end local v3    # "app":Ljava/io/File;
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 163
    .end local v4    # "apk":Ljava/io/File;
    :cond_4
    return-void
.end method

.method private initCotaDelAppsList()V
    .locals 8

    .line 221
    const/4 v0, 0x0

    .line 223
    .local v0, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    const-string v2, "/product/opcust/common/del_apps_list.xml"

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 224
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 225
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 226
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string v3, "UTF-8"

    invoke-interface {v2, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 227
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 228
    .local v3, "event":I
    :goto_0
    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    .line 229
    packed-switch v3, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 244
    :pswitch_1
    goto :goto_1

    .line 233
    :pswitch_2
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 234
    .local v4, "name":Ljava/lang/String;
    const-string v5, "package"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 235
    const-string v5, "name"

    const/4 v6, 0x0

    invoke-interface {v2, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 236
    .local v5, "pkgName":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 237
    sget-object v6, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v7, "initCotaDelApps pkgName is null, skip parse this tag"

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    goto :goto_1

    .line 240
    :cond_0
    iget-object v6, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sCotaDelApps:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 241
    nop

    .end local v5    # "pkgName":Ljava/lang/String;
    goto :goto_1

    .line 231
    .end local v4    # "name":Ljava/lang/String;
    :pswitch_3
    nop

    .line 248
    :cond_1
    :goto_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v3, v4

    goto :goto_0

    .line 254
    .end local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v3    # "event":I
    :cond_2
    nop

    :goto_2
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 255
    goto :goto_3

    .line 254
    :catchall_0
    move-exception v1

    goto :goto_4

    .line 251
    :catch_0
    move-exception v1

    .line 252
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v2, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initCotaDelApps fail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254
    nop

    .end local v1    # "e":Ljava/lang/Exception;
    goto :goto_2

    .line 256
    :goto_3
    return-void

    .line 254
    :goto_4
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 255
    throw v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private readAllowOnlyInstallThirdAppForCarrier()V
    .locals 9

    .line 170
    const-string v0, "disallow_install_third_app.list"

    const-string v1, "allow_install_third_app.list"

    const-string v2, "/product/opcust/"

    const-string v3, "persist.sys.cota.carrier"

    const-string v4, ""

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 171
    .local v3, "cotaCarrier":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "XM"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 173
    :try_start_0
    const-string v4, "ro.miui.region"

    const-string v5, "cn"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 174
    .local v4, "region":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v5, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 176
    .local v5, "customizedAppSetFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v7, "/product/opcust"

    if-nez v6, :cond_0

    .line 177
    :try_start_1
    new-instance v6, Ljava/io/File;

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v8, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v5, v6

    .line 180
    :cond_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 181
    iget-object v0, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sAllowOnlyInstallThirdApps:Ljava/util/Set;

    invoke-virtual {p0, v5, v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 182
    return-void

    .line 185
    :cond_1
    new-instance v1, Ljava/io/File;

    new-instance v6, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 187
    .end local v5    # "customizedAppSetFile":Ljava/io/File;
    .local v1, "customizedAppSetFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 188
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v2

    .line 191
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sDisallowInstallThirdApps:Ljava/util/Set;

    invoke-virtual {p0, v1, v0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 196
    .end local v1    # "customizedAppSetFile":Ljava/io/File;
    .end local v4    # "region":Ljava/lang/String;
    :cond_3
    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->TAG:Ljava/lang/String;

    const-string v2, "Error occurs when to read allow install third app list."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_0
    return-void
.end method

.method private skipInstallByCota(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "apkFilePath"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 202
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 203
    if-eqz p1, :cond_3

    const-string v1, "/system/data-app"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 204
    const-string v1, "/vendor/data-app"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 205
    const-string v1, "/product/data-app"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 206
    iget-object v1, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sAllowOnlyInstallThirdApps:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sAllowOnlyInstallThirdApps:Ljava/util/Set;

    .line 207
    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sDisallowInstallThirdApps:Ljava/util/Set;

    .line 208
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sDisallowInstallThirdApps:Ljava/util/Set;

    .line 209
    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    nop

    .line 206
    :goto_0
    return v0

    .line 212
    :cond_3
    return v0
.end method

.method private skipInstallDataAppByCota(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 260
    iget-object v0, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sCotaDelApps:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sCotaDelApps:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    const-string v0, "persist.sys.cota.carrier"

    const-string v2, ""

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "cotaCarrier":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "XM"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 264
    .end local v0    # "cotaCarrier":Ljava/lang/String;
    :cond_1
    return v1
.end method

.method private skipInstallO2Space(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 216
    const-string v0, "persist.sys.carrier.subnetwork"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tef_o2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 217
    const-string/jumbo v0, "uk.co.o2.android.o2space"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 216
    :goto_0
    return v0
.end method


# virtual methods
.method protected getCustAppList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 120
    .local v1, "productCarrierAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lmiui/util/CustomizeUtil;->getProductCarrierRegionAppFile()Ljava/io/File;

    move-result-object v2

    .line 121
    .local v2, "CarrierRegionAppFile":Ljava/io/File;
    invoke-direct {p0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readAllowOnlyInstallThirdAppForCarrier()V

    .line 122
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 123
    invoke-virtual {p0, v2, v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 124
    sget-object v3, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->PRODUCT_CARRIER_APP_DIR:Ljava/io/File;

    invoke-direct {p0, v0, v3, v1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V

    .line 126
    :cond_0
    return-object v0
.end method

.method protected getLegacyPreinstallList(ZZ)Ljava/util/List;
    .locals 8
    .param p1, "isFirstBoot"    # Z
    .param p2, "isDeviceUpgrading"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v0, "legacyPreinstallApkList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .local v1, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v2, "productCustomizedAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 62
    .local v3, "productDataAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v4, Ljava/io/File;

    const-string v5, "/mi_ext/product/etc/"

    const-string v6, "operator_product_data_app.list"

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .local v4, "productCustomizedAppListFile":Ljava/io/File;
    if-eqz p2, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->getCustAppList()Ljava/util/List;

    move-result-object v1

    .line 67
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 68
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    .line 69
    .local v6, "app":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    .end local v6    # "app":Ljava/io/File;
    goto :goto_0

    .line 74
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 75
    invoke-virtual {p0, v4, v3}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V

    .line 76
    sget-object v5, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->PRODUCT_DATA_APP_DIR:Ljava/io/File;

    invoke-direct {p0, v2, v5, v3}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V

    .line 78
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 79
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    .line 80
    .restart local v6    # "app":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    .end local v6    # "app":Ljava/io/File;
    goto :goto_1

    .line 84
    :cond_1
    return-object v0
.end method

.method protected getPreinstallDirs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getVanwardAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isOperatorPreinstall(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .line 135
    const-string v0, "/product/opcust/data-app"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected needIgnore(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 89
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 90
    invoke-direct {p0, p1, p2}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->skipInstallByCota(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cota skip install cust preinstall data-app, :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return v1

    .line 94
    :cond_0
    const-string v0, "es_telefonica"

    sget-object v2, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->sCustomizedRegion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->skipInstallO2Space(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    sget-object v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "skip install O2Space"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    return v1

    .line 98
    :cond_1
    invoke-direct {p0, p2}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->skipInstallDataAppByCota(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    sget-object v0, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "skip install app because cota-reject :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    return v1

    .line 103
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method protected needLegacyPreinstall(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 131
    invoke-virtual {p0, p1}, Lcom/android/server/pm/MiuiOperatorPreinstallConfig;->isOperatorPreinstall(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
