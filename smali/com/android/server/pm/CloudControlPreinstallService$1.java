class com.android.server.pm.CloudControlPreinstallService$1 extends android.content.BroadcastReceiver {
	 /* .source "CloudControlPreinstallService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/CloudControlPreinstallService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.pm.CloudControlPreinstallService this$0; //synthetic
/* # direct methods */
 com.android.server.pm.CloudControlPreinstallService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/CloudControlPreinstallService; */
/* .line 230 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 233 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onReceive:"; // const-string v1, "onReceive:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "CloudControlPreinstall"; // const-string v1, "CloudControlPreinstall"
android.util.Slog .i ( v1,v0 );
/* .line 234 */
/* if-nez p2, :cond_0 */
return;
/* .line 236 */
} // :cond_0
final String v0 = "com.miui.action.SIM_DETECTION"; // const-string v0, "com.miui.action.SIM_DETECTION"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.this$0;
v0 = com.android.server.pm.CloudControlPreinstallService .-$$Nest$fgetmReceivedSIM ( v0 );
/* if-nez v0, :cond_1 */
/* .line 237 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.pm.CloudControlPreinstallService .-$$Nest$fputmReceivedSIM ( v0,v1 );
/* .line 238 */
v0 = this.this$0;
com.android.server.pm.CloudControlPreinstallService .-$$Nest$muninstallPreinstallAppsDelay ( v0 );
/* .line 240 */
} // :cond_1
final String v0 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.this$0;
v0 = com.android.server.pm.CloudControlPreinstallService .-$$Nest$fgetisUninstallPreinstallApps ( v0 );
/* if-nez v0, :cond_2 */
/* .line 241 */
v0 = this.this$0;
com.android.server.pm.CloudControlPreinstallService .-$$Nest$muninstallPreinstallAppsDelay ( v0 );
/* .line 243 */
} // :cond_2
return;
} // .end method
