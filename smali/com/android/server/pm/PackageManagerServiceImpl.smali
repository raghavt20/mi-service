.class public Lcom/android/server/pm/PackageManagerServiceImpl;
.super Lcom/android/server/pm/PackageManagerServiceStub;
.source "PackageManagerServiceImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.pm.PackageManagerServiceStub$$"
.end annotation


# static fields
.field private static final ANDROID_INSTALLER_PACKAGE:Ljava/lang/String; = "com.android.packageinstaller"

.field private static final APP_LIST_FILE:Ljava/lang/String; = "/system/etc/install_app_filter.xml"

.field private static final CARRIER_SYS_APPS_LIST:Ljava/lang/String; = "/product/opcust/common/carrier_sys_apps_list.xml"

.field private static final CUSTOMIZED_REGION:Ljava/lang/String;

.field static final DELETE_FAILED_FORBIDED_BY_MIUI:I = -0x3e8

.field static final EP_INSTALLER_PKG_WHITELIST:[Ljava/lang/String;

.field private static final GOOGLE_INSTALLER_PACKAGE:Ljava/lang/String; = "com.google.android.packageinstaller"

.field private static final GOOGLE_WEB_SEARCH_PACKAGE:Ljava/lang/String; = "com.google.android.googlequicksearchbox"

.field public static final INSTALL_FULL_APP:I = 0x4000

.field public static final INSTALL_REASON_USER:I = 0x4

.field private static final IS_GLOBAL_REGION_BUILD:Z

.field private static IS_INTERNATIONAL_BUILD:Z = false

.field private static final MANAGED_PROVISION:Ljava/lang/String; = "com.android.managedprovisioning"

.field static final MIUI_CORE_APPS:[Ljava/lang/String;

.field private static final MIUI_INSTALLER_PACKAGE:Ljava/lang/String; = "com.miui.packageinstaller"

.field private static final MIUI_MARKET_PACKAGE:Ljava/lang/String; = "com.xiaomi.market"

.field static final MIUI_VERIFICATION_TIMEOUT:I = -0x64

.field private static final PACKAGE_EVENT_DIR:Ljava/io/File;

.field private static final PACKAGE_MIME_TYPE:Ljava/lang/String; = "application/vnd.android.package-archive"

.field private static final PACKAGE_WEBVIEW:Ljava/lang/String; = "com.google.android.webview"

.field private static final PKMS_ATEST_NAME:Ljava/lang/String; = "com.android.server.pm.test.service.server"

.field private static final SHIM_PKG:Ljava/lang/String; = "com.android.cts.priv.ctsshim"

.field private static final SUPPORT_DEL_COTA_APP:Z

.field static final TAG:Ljava/lang/String; = "PKMSImpl"

.field private static final TAG_ADD_APPS:Ljava/lang/String; = "add_apps"

.field private static final TAG_APP:Ljava/lang/String; = "app"

.field private static final TAG_IGNORE_APPS:Ljava/lang/String; = "ignore_apps"

.field static final sAllowPackage:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sClearPermissionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sFirstUseHandler:Landroid/os/Handler;

.field private static sFirstUseLock:Ljava/lang/Object;

.field private static sFirstUseThread:Landroid/os/HandlerThread;

.field private static final sGLPhoneAppsSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final sInstallerSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final sIsBootLoaderLocked:Z

.field public static final sIsReleaseRom:Z

.field static final sNoVerifyAllowPackage:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sNotSupportUpdateSystemApps:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static sShellCheckPermissions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sSilentlyUninstallPackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sTHPhoneAppsSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCotaApps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentPackageInstaller:Ljava/lang/String;

.field private mDexManager:Lcom/android/server/pm/dex/DexManager;

.field private mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

.field private mDexoptServiceThread:Lcom/android/server/pm/DexoptServiceThread;

.field private mDomainVerificationService:Lcom/android/server/pm/verify/domain/DomainVerificationService;

.field private final mIgnoreApks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIgnorePackages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsGlobalCrbtSupported:Z

.field private mIsSupportAttention:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMiuiDexopt:Lcom/android/server/pm/MiuiDexopt;

.field private mMiuiInstallerPackage:Lcom/android/server/pm/pkg/AndroidPackage;

.field private mMiuiInstallerPackageSetting:Lcom/android/server/pm/PackageSetting;

.field private mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

.field private mPM:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

.field private mPackageManagerCloudHelper:Lcom/android/server/pm/PackageManagerCloudHelper;

.field private mPdo:Lcom/android/server/pm/PackageDexOptimizer;

.field private mPkgSettings:Lcom/android/server/pm/Settings;

.field private mPms:Lcom/android/server/pm/PackageManagerService;

.field private mRsaFeature:Ljava/lang/String;

.field private persistAppCount:I

.field private systemAppCount:I

.field private thirdAppCount:I


# direct methods
.method public static synthetic $r8$lambda$lK2NBL-uNMl0IZ57KeUxVKBIOYk(Lcom/android/server/pm/PackageManagerServiceImpl;Lcom/android/server/pm/pkg/AndroidPackage;JI)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/pm/PackageManagerServiceImpl;->lambda$getPackageInfoBySelf$5(Lcom/android/server/pm/pkg/AndroidPackage;JI)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$t2nlgQrmPQd-pT6SWmphpuoPtHA(Lcom/android/server/pm/PackageManagerServiceImpl;Lcom/android/server/pm/pkg/AndroidPackage;JI)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/pm/PackageManagerServiceImpl;->lambda$getApplicationInfoBySelf$6(Lcom/android/server/pm/pkg/AndroidPackage;JI)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDomainVerificationService(Lcom/android/server/pm/PackageManagerServiceImpl;)Lcom/android/server/pm/verify/domain/DomainVerificationService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDomainVerificationService:Lcom/android/server/pm/verify/domain/DomainVerificationService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mdoUpdateSystemAppDefaultUserStateForAllUsers(Lcom/android/server/pm/PackageManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->doUpdateSystemAppDefaultUserStateForAllUsers()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleFirstUseActivity(Lcom/android/server/pm/PackageManagerServiceImpl;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->handleFirstUseActivity(Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 13

    .line 122
    nop

    .line 123
    const-string v0, "ro.boot.vbmeta.device_state"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    const-string v1, "locked"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsBootLoaderLocked:Z

    .line 125
    sget-object v0, Lmiui/os/Build;->TAGS:Ljava/lang/String;

    const-string v1, "release-key"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z

    .line 149
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sInstallerSet:Ljava/util/Set;

    .line 151
    const-string v1, "com.miui.packageinstaller"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 152
    const-string v2, "com.google.android.packageinstaller"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    const-string v3, "com.android.packageinstaller"

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 156
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sNotSupportUpdateSystemApps:Ljava/util/Set;

    .line 158
    const-string v4, "com.android.bluetooth"

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    const-string v4, "com.miui.powerkeeper"

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 162
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sTHPhoneAppsSet:Ljava/util/Set;

    .line 163
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    sput-object v4, Lcom/android/server/pm/PackageManagerServiceImpl;->sGLPhoneAppsSet:Ljava/util/Set;

    .line 165
    const-string v5, "com.android.contacts"

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v5, "com.android.incallui"

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    const-string v0, "com.google.android.contacts"

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 168
    const-string v0, "com.google.android.dialer"

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 171
    const-string v0, "ro.miui.customized.region"

    const-string v4, ""

    invoke-static {v0, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->CUSTOMIZED_REGION:Ljava/lang/String;

    .line 172
    nop

    .line 173
    const-string v5, "ro.miui.cota_app_del_support"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/android/server/pm/PackageManagerServiceImpl;->SUPPORT_DEL_COTA_APP:Z

    .line 174
    nop

    .line 175
    const-string v5, "ro.miui.build.region"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "global"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    sput-boolean v5, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_GLOBAL_REGION_BUILD:Z

    .line 176
    const-string v5, "ro.product.mod_device"

    invoke-static {v5, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "_global"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    sput-boolean v4, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    .line 177
    new-instance v4, Ljava/io/File;

    const-string v5, "data/system/package-event"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v4, Lcom/android/server/pm/PackageManagerServiceImpl;->PACKAGE_EVENT_DIR:Ljava/io/File;

    .line 202
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    sput-object v4, Lcom/android/server/pm/PackageManagerServiceImpl;->sClearPermissionSet:Ljava/util/Set;

    .line 204
    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 206
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 207
    const-string v5, "com.miui.cleanmaster"

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 208
    const-string v5, "com.miui.thirdappassistant"

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 209
    const-string v5, "com.miui.screenrecorder"

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 577
    filled-new-array {v3, v1}, [Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->EP_INSTALLER_PKG_WHITELIST:[Ljava/lang/String;

    .line 644
    const-string v7, "com.lbe.security.miui"

    const-string v8, "com.miui.securitycenter"

    const-string v9, "com.android.updater"

    const-string v10, "com.xiaomi.market"

    const-string v11, "com.xiaomi.finddevice"

    const-string v12, "com.miui.home"

    filled-new-array/range {v7 .. v12}, [Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->MIUI_CORE_APPS:[Ljava/lang/String;

    .line 698
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sput-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sSilentlyUninstallPackages:Ljava/util/Set;

    .line 700
    const-string v3, "ro.miui.product.home"

    const-string v4, "com.miui.home"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 701
    const-string v3, "com.xiaomi.market"

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 702
    const-string v4, "com.xiaomi.mipicks"

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 703
    const-string v5, "com.xiaomi.discover"

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 704
    const-string v7, "com.xiaomi.gamecenter"

    invoke-interface {v1, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 705
    const-string v8, "com.xiaomi.gamecenter.pad"

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 706
    const-string v8, "com.miui.global.packageinstaller"

    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 707
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 708
    const-string v2, "com.miui.greenguard"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 709
    const-string v2, "com.miui.cleaner"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 710
    const-string v2, "com.miui.cloudbackup"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 711
    sget-boolean v2, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_0

    .line 712
    const-string v2, "de.telekom.tsc"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 713
    const-string v2, "com.sfr.android.sfrjeux"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 714
    const-string v2, "com.altice.android.myapps"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 715
    const-string v2, "jp_kd"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    const-string v0, "com.facebook.system"

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 868
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    .line 870
    const-string v1, "android.permission.SEND_SMS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 871
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.CALL_PHONE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 872
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 873
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.WRITE_CONTACTS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 874
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.CLEAR_APP_USER_DATA"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 875
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.WRITE_SECURE_SETTINGS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 876
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.WRITE_SETTINGS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 877
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.MANAGE_DEVICE_ADMINS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 878
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.UPDATE_APP_OPS_STATS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 879
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.INJECT_EVENTS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 880
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.INSTALL_GRANT_RUNTIME_PERMISSIONS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 881
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.GRANT_RUNTIME_PERMISSIONS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 882
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.REVOKE_RUNTIME_PERMISSIONS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 883
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    const-string v1, "android.permission.SET_PREFERRED_APPLICATIONS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 885
    sget-boolean v0, Lmiui/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v0, :cond_1

    .line 886
    const-string v0, "ro.secureboot.devicelock"

    invoke-static {v0, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 887
    const-string v0, "ro.secureboot.lockstate"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unlocked"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_3

    .line 889
    :cond_2
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 916
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sAllowPackage:Ljava/util/ArrayList;

    .line 918
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sNoVerifyAllowPackage:Ljava/util/ArrayList;

    .line 920
    const-string v1, "android"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 921
    const-string v1, "com.android.provision"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 922
    const-string v1, "com.miui.securitycore"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 923
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 924
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 925
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 926
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 927
    const-string v1, "com.android.updater"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 928
    const-string v1, "com.amazon.venezia"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 930
    sget-boolean v1, Lmiui/enterprise/EnterpriseManagerStub;->ENTERPRISE_ACTIVATED:Z

    if-eqz v1, :cond_4

    .line 931
    const-string v1, "com.xiaomi.ep"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 934
    :cond_4
    sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_5

    .line 935
    const-string v1, "com.miui.cotaservice"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 936
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 937
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 940
    :cond_5
    sget-boolean v1, Lmiui/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v1, :cond_6

    .line 941
    const-string v1, "com.android.server.pm.test.service.server"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1426
    :cond_6
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sFirstUseLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 119
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceStub;-><init>()V

    .line 138
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    .line 139
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnorePackages:Ljava/util/Set;

    .line 193
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->thirdAppCount:I

    .line 194
    iput v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->systemAppCount:I

    .line 195
    iput v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->persistAppCount:I

    .line 199
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mRsaFeature:Ljava/lang/String;

    .line 200
    iput-boolean v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsGlobalCrbtSupported:Z

    .line 212
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCotaApps:Ljava/util/HashMap;

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsSupportAttention:Ljava/util/List;

    return-void
.end method

.method private static addIgnoreApks(Ljava/lang/String;Ljava/util/Set;)V
    .locals 8
    .param p0, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 444
    .local p1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p0}, Lmiui/util/FeatureParser;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 445
    .local v0, "whiteList":[Ljava/lang/String;
    if-eqz v0, :cond_1

    array-length v1, v0

    if-lez v1, :cond_1

    .line 446
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 447
    .local v4, "str":Ljava/lang/String;
    const-string v5, ","

    invoke-static {v4, v5}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 448
    .local v5, "item":[Ljava/lang/String;
    sget v6, Lmiui/util/DeviceLevel;->TOTAL_RAM:I

    aget-object v7, v5, v2

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    if-gt v6, v7, :cond_0

    .line 449
    const/4 v6, 0x1

    aget-object v6, v5, v6

    invoke-interface {p1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 446
    .end local v4    # "str":Ljava/lang/String;
    .end local v5    # "item":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 453
    :cond_1
    return-void
.end method

.method private static addPersistentPackages(Landroid/content/pm/ApplicationInfo;Ljava/util/ArrayList;)V
    .locals 2
    .param p0, "ai"    # Landroid/content/pm/ApplicationInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/ApplicationInfo;",
            "Ljava/util/ArrayList<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    .line 1688
    .local p1, "finalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;"
    const-string v0, "PKMSImpl"

    if-nez p0, :cond_0

    .line 1689
    const-string v1, "ai is null!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1690
    return-void

    .line 1692
    :cond_0
    iget-object v1, p0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1693
    const-string v1, "processName is null!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1694
    return-void

    .line 1696
    :cond_1
    iget-object v0, p0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_2
    goto :goto_0

    :sswitch_0
    const-string v1, "com.android.nfc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "com.miui.daemon"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_2
    const-string v1, "com.goodix.fingerprint"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1700
    :pswitch_0
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1701
    nop

    .line 1705
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x75d739d1 -> :sswitch_2
        -0x399a9feb -> :sswitch_1
        -0x29760741 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static checkAndClearResiduePermissions(Lcom/android/server/pm/permission/LegacyPermissionSettings;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "settings"    # Lcom/android/server/pm/permission/LegacyPermissionSettings;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "perName"    # Ljava/lang/String;

    .line 1396
    if-eqz p0, :cond_2

    .line 1397
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1400
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/pm/permission/LegacyPermissionSettings;->getPermissions()Ljava/util/List;

    move-result-object v0

    .line 1401
    .local v0, "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/permission/LegacyPermission;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1402
    const-string v1, "PKMSImpl"

    const-string v2, "find residue permission"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1403
    invoke-static {p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->clearPermissions(Ljava/lang/String;)V

    .line 1405
    :cond_1
    return-void

    .line 1398
    .end local v0    # "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/permission/LegacyPermission;>;"
    :cond_2
    :goto_0
    return-void
.end method

.method private static clearPermissions(Ljava/lang/String;)V
    .locals 6
    .param p0, "packageName"    # Ljava/lang/String;

    .line 1380
    const-string v0, "PKMSImpl"

    sget-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sClearPermissionSet:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1381
    return-void

    .line 1384
    :cond_0
    :try_start_0
    const-string v1, "permissionmgr"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/permission/PermissionManagerService;

    .line 1385
    .local v1, "pms":Lcom/android/server/pm/permission/PermissionManagerService;
    const-string v2, "mPermissionManagerServiceImpl"

    const-class v3, Lcom/android/server/pm/permission/PermissionManagerServiceImpl;

    invoke-static {v1, v2, v3}, Lmiui/util/ReflectionUtils;->getObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/pm/permission/PermissionManagerServiceImpl;

    .line 1387
    .local v2, "impl":Lcom/android/server/pm/permission/PermissionManagerServiceImpl;
    const-string/jumbo v3, "updatePermissions"

    const-class v4, Ljava/lang/Void;

    const/4 v5, 0x0

    filled-new-array {p0, v5}, [Ljava/lang/Object;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lmiui/util/ReflectionUtils;->callMethod(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1388
    const-string v3, "clear residue permission finish"

    invoke-static {v0, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1391
    nop

    .end local v1    # "pms":Lcom/android/server/pm/permission/PermissionManagerService;
    .end local v2    # "impl":Lcom/android/server/pm/permission/PermissionManagerServiceImpl;
    goto :goto_0

    .line 1389
    :catch_0
    move-exception v1

    .line 1390
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clear residue permission error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1392
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private disableSystemApp(ILjava/lang/String;)V
    .locals 8
    .param p1, "userId"    # I
    .param p2, "pkg"    # Ljava/lang/String;

    .line 1535
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Disable "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for user "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1, v0}, Lcom/android/server/pm/PackageManagerServiceUtils;->logCriticalInfo(ILjava/lang/String;)V

    .line 1537
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPM:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const/4 v4, 0x3

    const/4 v5, 0x0

    const-string v7, "COTA"

    move-object v3, p2

    move v6, p1

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V

    .line 1539
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1540
    :try_start_1
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v1, p2}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;

    move-result-object v1

    .line 1541
    .local v1, "pkgSetting":Lcom/android/server/pm/PackageSetting;
    if-eqz v1, :cond_0

    .line 1542
    const-string v2, "cota-disabled"

    invoke-direct {p0, v1, v2, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->updatedefaultState(Lcom/android/server/pm/PackageSetting;Ljava/lang/String;I)V

    .line 1543
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v2, p1}, Lcom/android/server/pm/PackageManagerService;->scheduleWritePackageRestrictions(I)V

    .line 1545
    .end local v1    # "pkgSetting":Lcom/android/server/pm/PackageSetting;
    :cond_0
    monitor-exit v0

    .line 1548
    goto :goto_0

    .line 1545
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
    .end local p1    # "userId":I
    .end local p2    # "pkg":Ljava/lang/String;
    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1546
    .restart local p0    # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
    .restart local p1    # "userId":I
    .restart local p2    # "pkg":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1547
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to disable "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", msg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2, v1}, Lcom/android/server/pm/PackageManagerServiceUtils;->logCriticalInfo(ILjava/lang/String;)V

    .line 1549
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private doUpdateSystemAppDefaultUserStateForAllUsers()V
    .locals 5

    .line 1628
    const-class v0, Lcom/android/server/pm/UserManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/UserManagerInternal;

    .line 1629
    .local v0, "mUserManager":Lcom/android/server/pm/UserManagerInternal;
    invoke-virtual {v0}, Lcom/android/server/pm/UserManagerInternal;->getUserIds()[I

    move-result-object v1

    .line 1630
    .local v1, "allUsers":[I
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget v4, v1, v3

    .line 1631
    .local v4, "userId":I
    invoke-virtual {p0, v4}, Lcom/android/server/pm/PackageManagerServiceImpl;->updateSystemAppDefaultStateForUser(I)V

    .line 1630
    .end local v4    # "userId":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1633
    :cond_0
    return-void
.end method

.method private enableSystemApp(ILjava/lang/String;)V
    .locals 8
    .param p1, "userId"    # I
    .param p2, "pkg"    # Ljava/lang/String;

    .line 1552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Enable "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for user "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1, v0}, Lcom/android/server/pm/PackageManagerServiceUtils;->logCriticalInfo(ILjava/lang/String;)V

    .line 1554
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPM:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v7, "COTA"

    move-object v3, p2

    move v6, p1

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V

    .line 1556
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1557
    :try_start_1
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v1, p2}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;

    move-result-object v1

    .line 1558
    .local v1, "pkgSetting":Lcom/android/server/pm/PackageSetting;
    if-eqz v1, :cond_0

    .line 1559
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->updatedefaultState(Lcom/android/server/pm/PackageSetting;Ljava/lang/String;I)V

    .line 1560
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v2, p1}, Lcom/android/server/pm/PackageManagerService;->scheduleWritePackageRestrictions(I)V

    .line 1562
    .end local v1    # "pkgSetting":Lcom/android/server/pm/PackageSetting;
    :cond_0
    monitor-exit v0

    .line 1565
    goto :goto_0

    .line 1562
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
    .end local p1    # "userId":I
    .end local p2    # "pkg":Ljava/lang/String;
    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1563
    .restart local p0    # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
    .restart local p1    # "userId":I
    .restart local p2    # "pkg":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1564
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to enable "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", msg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-static {v2, v1}, Lcom/android/server/pm/PackageManagerServiceUtils;->logCriticalInfo(ILjava/lang/String;)V

    .line 1566
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static fatalIf(ZILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "condition"    # Z
    .param p1, "err"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "logPrefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/pm/PackageManagerException;
        }
    .end annotation

    .line 992
    if-nez p0, :cond_0

    .line 996
    return-void

    .line 993
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", msg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PKMSImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    new-instance v0, Lcom/android/server/pm/PackageManagerException;

    invoke-direct {v0, p1, p2}, Lcom/android/server/pm/PackageManagerException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method public static get()Lcom/android/server/pm/PackageManagerServiceImpl;
    .locals 1

    .line 217
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceStub;->get()Lcom/android/server/pm/PackageManagerServiceStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageManagerServiceImpl;

    return-object v0
.end method

.method public static getFirstUseHandler()Landroid/os/Handler;
    .locals 3

    .line 1429
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sFirstUseHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 1430
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sFirstUseLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1431
    :try_start_0
    sget-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sFirstUseHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 1432
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "first_use_thread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sFirstUseThread:Landroid/os/HandlerThread;

    .line 1433
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 1434
    new-instance v1, Landroid/os/Handler;

    sget-object v2, Lcom/android/server/pm/PackageManagerServiceImpl;->sFirstUseThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sFirstUseHandler:Landroid/os/Handler;

    .line 1436
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1438
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sFirstUseHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getdatedefaultState(Lcom/android/server/pm/PackageSetting;I)Ljava/lang/String;
    .locals 1
    .param p1, "ps"    # Lcom/android/server/pm/PackageSetting;
    .param p2, "userId"    # I

    .line 1602
    invoke-virtual {p1, p2}, Lcom/android/server/pm/PackageSetting;->readUserState(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/pm/pkg/PackageUserStateInternal;->getDefaultState()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleFirstUseActivity(Ljava/lang/String;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1442
    const/4 v0, 0x1

    .line 1443
    .local v0, "isDeviceProvisioned":Z
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 1445
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isProvisioned()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 1448
    goto :goto_0

    .line 1446
    :catch_0
    move-exception v1

    .line 1447
    .local v1, "e":Ljava/lang/IllegalStateException;
    const/4 v0, 0x0

    .line 1450
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :cond_0
    :goto_0
    const-string v1, "PKMSImpl"

    if-nez v0, :cond_1

    .line 1451
    const-string v2, "Skip dexopt, device is not provisioned."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    return-void

    .line 1454
    :cond_1
    const-string v2, "android"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1455
    const-string v2, "Skip dexopt, cannot dexopt the system server."

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1456
    return-void

    .line 1458
    :cond_2
    const/16 v2, 0x205

    .line 1461
    .local v2, "flags":I
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

    new-instance v4, Lcom/android/server/pm/dex/DexoptOptions;

    const/16 v5, 0xe

    invoke-direct {v4, p1, v5, v2}, Lcom/android/server/pm/dex/DexoptOptions;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v3, v4}, Lcom/android/server/pm/DexOptHelper;->performDexOpt(Lcom/android/server/pm/dex/DexoptOptions;)Z

    move-result v3

    .line 1462
    .local v3, "success":Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FirstUseActivity packageName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " success = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1463
    const-class v4, Lcom/android/server/PinnerService;

    invoke-static {v4}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/PinnerService;

    .line 1464
    .local v4, "pinnerService":Lcom/android/server/PinnerService;
    if-eqz v4, :cond_3

    .line 1465
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FirstUseActivity Pinning optimized code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1466
    new-instance v1, Landroid/util/ArraySet;

    invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V

    .line 1467
    .local v1, "packages":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 1468
    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Lcom/android/server/PinnerService;->update(Landroid/util/ArraySet;Z)V

    .line 1470
    .end local v1    # "packages":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    :cond_3
    return-void
.end method

.method private hasGoogleContacts()Z
    .locals 2

    .line 1865
    new-instance v0, Ljava/io/File;

    const-string v1, "/product/app/GoogleContacts"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private hasXiaomiContacts()Z
    .locals 3

    .line 1854
    new-instance v0, Ljava/io/File;

    const-string v1, "/product/priv-app/MIUIContactsT"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    const-string v1, "/product/priv-app/MIUIContactsFold"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1855
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    const-string v2, "/product/priv-app/MIUIContactsCetus"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1856
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    const-string v2, "/product/priv-app/MIUIContactsTGlobal"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1857
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    const-string v2, "/product/priv-app/MIUIContactsPadU"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1858
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1859
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    const-string v1, "/product/priv-app/MIUIContactsUGlobal"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1860
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/File;

    const-string v1, "/product/priv-app/MIUIContactsU"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1861
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 1854
    :goto_1
    return v0
.end method

.method protected static initAllowPackageList(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .line 951
    const-string v0, "add "

    const-string v1, "PKMSImpl"

    sget-object v2, Lcom/android/server/pm/PackageManagerServiceImpl;->sAllowPackage:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 953
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x1103000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 954
    .local v3, "stringArray":[Ljava/lang/String;
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 955
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v4, v3

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " common packages into sAllowPackage list"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    array-length v2, v3

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v2, :cond_0

    aget-object v6, v3, v5

    .line 957
    .local v6, "pkg":Ljava/lang/String;
    invoke-static {v1, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    nop

    .end local v6    # "pkg":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 960
    :cond_0
    sget-boolean v2, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_1

    .line 961
    return-void

    .line 963
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x1103000c

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 964
    .end local v3    # "stringArray":[Ljava/lang/String;
    .local v2, "stringArray":[Ljava/lang/String;
    sget-object v3, Lcom/android/server/pm/PackageManagerServiceImpl;->sAllowPackage:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 965
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v5, v2

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " international package into sAllowPackage list"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 966
    array-length v3, v2

    move v5, v4

    :goto_1
    if-ge v5, v3, :cond_2

    aget-object v6, v2, v5

    .line 967
    .restart local v6    # "pkg":Ljava/lang/String;
    invoke-static {v1, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 966
    nop

    .end local v6    # "pkg":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 970
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x1103000d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 971
    sget-object v3, Lcom/android/server/pm/PackageManagerServiceImpl;->sAllowPackage:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 972
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v5, v2

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " operator packages into sAllowPackage list"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    array-length v3, v2

    :goto_2
    if-ge v4, v3, :cond_3

    aget-object v5, v2, v4

    .line 974
    .local v5, "pkg":Ljava/lang/String;
    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    nop

    .end local v5    # "pkg":Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 977
    :cond_3
    sget-boolean v3, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z

    if-eqz v3, :cond_4

    sget-boolean v3, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsBootLoaderLocked:Z

    if-nez v3, :cond_5

    .line 978
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x1103000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 979
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " unlocked packages into sAllowPackage list"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 980
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sAllowPackage:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 984
    .end local v2    # "stringArray":[Ljava/lang/String;
    :cond_5
    goto :goto_3

    .line 982
    :catch_0
    move-exception v0

    .line 983
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    .line 985
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :goto_3
    return-void
.end method

.method private initCotaApps()V
    .locals 10

    .line 1959
    const-string v0, "PKMSImpl"

    const/4 v1, 0x0

    .line 1961
    .local v1, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    const-string v3, "/product/opcust/common/carrier_sys_apps_list.xml"

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object v1, v2

    .line 1962
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v2

    .line 1963
    .local v2, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 1964
    .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string v4, "UTF-8"

    invoke-interface {v3, v1, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 1965
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    .line 1966
    .local v4, "event":I
    :goto_0
    const/4 v5, 0x1

    if-eq v4, v5, :cond_3

    .line 1967
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 1987
    :pswitch_1
    goto :goto_1

    .line 1971
    :pswitch_2
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1972
    .local v5, "name":Ljava/lang/String;
    const-string v6, "package"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1973
    const-string v6, "name"

    const/4 v7, 0x0

    invoke-interface {v3, v7, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1974
    .local v6, "pkgName":Ljava/lang/String;
    const-string v8, "carrier"

    invoke-interface {v3, v7, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1975
    .local v7, "carrier":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1976
    const-string v8, "initCotaApps pkgName is null, skip parse this tag"

    invoke-static {v0, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1977
    goto :goto_1

    .line 1979
    :cond_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1980
    const-string v8, "initCotaApps carrier is null, skip parse this tag"

    invoke-static {v0, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1981
    goto :goto_1

    .line 1983
    :cond_1
    iget-object v8, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCotaApps:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v6, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1984
    nop

    .end local v6    # "pkgName":Ljava/lang/String;
    .end local v7    # "carrier":Ljava/lang/String;
    goto :goto_1

    .line 1969
    .end local v5    # "name":Ljava/lang/String;
    :pswitch_3
    nop

    .line 1991
    :cond_2
    :goto_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v4, v5

    goto :goto_0

    .line 1997
    .end local v2    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "event":I
    :cond_3
    nop

    :goto_2
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1998
    goto :goto_3

    .line 1997
    :catchall_0
    move-exception v0

    goto :goto_4

    .line 1994
    :catch_0
    move-exception v2

    .line 1995
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initCotaApps fail: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1997
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    goto :goto_2

    .line 1999
    :goto_3
    return-void

    .line 1997
    :goto_4
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 1998
    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private isAllowedToGetInstalledApps(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "callingUid"    # I
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "where"    # Ljava/lang/String;

    .line 1259
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 1260
    return v1

    .line 1263
    :cond_0
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    .line 1264
    .local v0, "callingAppId":I
    const/16 v2, 0x2710

    if-ge v0, v2, :cond_1

    .line 1265
    return v1

    .line 1268
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1269
    return v1

    .line 1272
    :cond_2
    sget-boolean v2, Lmiui/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v2, :cond_3

    const-string v2, "com.android.server.pm.test.service.server"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1273
    return v1

    .line 1276
    :cond_3
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v2

    const-class v3, Landroid/app/AppOpsManager;

    invoke-virtual {v2, v3}, Landroid/app/Application;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AppOpsManager;

    .line 1277
    .local v2, "appOpsManager":Landroid/app/AppOpsManager;
    const/16 v4, 0x2726

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v2

    move v5, p1

    move-object v6, p2

    invoke-virtual/range {v3 .. v8}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_4

    .line 1278
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MIUILOG- Permission Denied "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ". pkg : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " uid : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "PKMSImpl"

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1279
    const/4 v1, 0x0

    return v1

    .line 1281
    :cond_4
    return v1
.end method

.method public static isCTS()Z
    .locals 1

    .line 1408
    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    return v0
.end method

.method private isProvisioned()Z
    .locals 3

    .line 305
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_provisioned"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    return v2
.end method

.method private isRsa4()Z
    .locals 2

    .line 1177
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mRsaFeature:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1178
    const-string v0, "ro.com.miui.rsa.feature"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mRsaFeature:Ljava/lang/String;

    .line 1180
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mRsaFeature:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private static isSecondUserlocked(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 1095
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->isCTS()Z

    move-result v0

    .line 1096
    .local v0, "iscts":Z
    invoke-static {}, Lcom/android/server/pm/PmInjector;->getDefaultUserId()I

    move-result v1

    .line 1097
    .local v1, "userid":I
    const-string/jumbo v2, "user"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/UserManager;

    .line 1098
    .local v2, "userManager":Landroid/os/UserManager;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v2, v1}, Landroid/os/UserManager;->isUserUnlocked(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1099
    const/4 v3, 0x1

    return v3

    .line 1101
    :cond_0
    const/4 v3, 0x0

    return v3
.end method

.method static isTrustedEnterpriseInstaller(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callingUid"    # I
    .param p2, "installerPkg"    # Ljava/lang/String;

    .line 586
    sget-boolean v0, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 587
    return v1

    .line 589
    :cond_0
    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    invoke-static {p0, v0}, Lcom/miui/enterprise/ApplicationHelper;->isTrustedAppStoresEnabled(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->EP_INSTALLER_PKG_WHITELIST:[Ljava/lang/String;

    .line 590
    invoke-static {v0, p2}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 592
    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 591
    invoke-static {p0, v0}, Lcom/miui/enterprise/ApplicationHelper;->getTrustedAppStores(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    .line 592
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 593
    const/4 v0, 0x0

    return v0

    .line 595
    :cond_1
    return v1
.end method

.method private synthetic lambda$getApplicationInfoBySelf$6(Lcom/android/server/pm/pkg/AndroidPackage;JI)Ljava/util/List;
    .locals 2
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "flags"    # J
    .param p4, "userId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1769
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v0

    .line 1770
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/android/server/pm/Computer;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 1771
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v0, :cond_0

    .line 1772
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1773
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;"
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1774
    return-object v1

    .line 1776
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;"
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method private synthetic lambda$getPackageInfoBySelf$5(Lcom/android/server/pm/pkg/AndroidPackage;JI)Ljava/util/List;
    .locals 2
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "flags"    # J
    .param p4, "userId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1749
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v0

    .line 1750
    invoke-interface {p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/android/server/pm/Computer;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 1751
    .local v0, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v0, :cond_0

    .line 1752
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1753
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/PackageInfo;>;"
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1754
    return-object v1

    .line 1756
    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/PackageInfo;>;"
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method static synthetic lambda$getPersistentAppsForOtherUser$4(ZIILjava/util/ArrayList;Lcom/android/server/pm/pkg/PackageStateInternal;)V
    .locals 7
    .param p0, "safeMode"    # Z
    .param p1, "flags"    # I
    .param p2, "userId"    # I
    .param p3, "finalList"    # Ljava/util/ArrayList;
    .param p4, "packageState"    # Lcom/android/server/pm/pkg/PackageStateInternal;

    .line 1668
    invoke-interface {p4}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;->isPersistent()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_0

    invoke-interface {p4}, Lcom/android/server/pm/pkg/PackageStateInternal;->isSystem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1669
    :cond_0
    invoke-interface {p4}, Lcom/android/server/pm/pkg/PackageStateInternal;->getAndroidPackage()Lcom/android/server/pm/pkg/AndroidPackage;

    move-result-object v1

    int-to-long v2, p1

    .line 1670
    invoke-interface {p4, p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getUserStateOrDefault(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;

    move-result-object v4

    .line 1669
    move v5, p2

    move-object v6, p4

    invoke-static/range {v1 .. v6}, Lcom/android/server/pm/parsing/PackageInfoUtils;->generateApplicationInfo(Lcom/android/server/pm/pkg/AndroidPackage;JLcom/android/server/pm/pkg/PackageUserStateInternal;ILcom/android/server/pm/pkg/PackageStateInternal;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 1671
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 1674
    :pswitch_0
    invoke-static {v0, p3}, Lcom/android/server/pm/PackageManagerServiceImpl;->addPersistentPackages(Landroid/content/pm/ApplicationInfo;Ljava/util/ArrayList;)V

    .line 1675
    nop

    .line 1682
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_1
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x6e
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic lambda$init$0(Lcom/android/server/pm/PackageEventRecorder;)Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;
    .locals 2
    .param p0, "recorder"    # Lcom/android/server/pm/PackageEventRecorder;

    .line 241
    new-instance v0, Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;

    .line 242
    invoke-static {}, Lcom/android/server/IoThread;->get()Lcom/android/server/IoThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/IoThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;-><init>(Lcom/android/server/pm/PackageEventRecorder;Landroid/os/Looper;)V

    .line 241
    return-object v0
.end method

.method static synthetic lambda$notifyGlobalPackageInstaller$3(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p0, "callingPackage"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .line 1087
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.global.packageinstaller.action.verifypackage"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1088
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "installing"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1089
    const-string v1, "com.miui.securitycenter.permission.GLOBAL_PACKAGEINSTALLER"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1090
    return-void
.end method

.method static synthetic lambda$protectAppFromDeleting$1(Ljava/lang/String;IIZLandroid/content/pm/IPackageDeleteObserver2;)V
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "callingUid"    # I
    .param p2, "userId"    # I
    .param p3, "deleteSystem"    # Z
    .param p4, "observer"    # Landroid/content/pm/IPackageDeleteObserver2;

    .line 801
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Can\'t uninstall pkg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with user: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " deleteSystem: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " reason: shell preinstall"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PKMSImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    const/16 v0, -0x3e8

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p4, p0, v0, v1}, Landroid/content/pm/IPackageDeleteObserver2;->onPackageDeleted(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 808
    goto :goto_0

    .line 807
    :catch_0
    move-exception v0

    .line 809
    :goto_0
    return-void
.end method

.method static synthetic lambda$protectAppFromDeleting$2(Landroid/content/pm/IPackageDeleteObserver2;Ljava/lang/String;)V
    .locals 2
    .param p0, "observer"    # Landroid/content/pm/IPackageDeleteObserver2;
    .param p1, "packageName"    # Ljava/lang/String;

    .line 823
    const/16 v0, -0x3e8

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p0, p1, v0, v1}, Landroid/content/pm/IPackageDeleteObserver2;->onPackageDeleted(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 826
    goto :goto_0

    .line 824
    :catch_0
    move-exception v0

    .line 827
    :goto_0
    return-void
.end method

.method static synthetic lambda$recordPackageActivate$7(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p0, "activatedPkg"    # Ljava/lang/String;
    .param p1, "userId"    # I
    .param p2, "sourcePkg"    # Ljava/lang/String;

    .line 1801
    const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    .line 1802
    invoke-interface {v0, p0, p1, p2}, Lcom/android/server/pm/PackageEventRecorderInternal;->recordPackageActivate(Ljava/lang/String;ILjava/lang/String;)V

    .line 1801
    return-void
.end method

.method private static notifyGlobalPackageInstaller(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callingPackage"    # Ljava/lang/String;

    .line 1084
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    const-string v0, "com.android.vending"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1085
    const-string v0, "com.google.android.packageinstaller"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1086
    :cond_0
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1, p0}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1092
    :cond_1
    return-void
.end method

.method private readIgnoreApks()V
    .locals 12

    .line 456
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v0

    .line 457
    .local v0, "custVariant":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 458
    return-void

    .line 460
    :cond_0
    const/4 v1, 0x0

    .line 462
    .local v1, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    new-instance v3, Ljava/io/File;

    const-string v4, "/system/etc/install_app_filter.xml"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v1, v2

    .line 463
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v2

    .line 464
    .local v2, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 465
    .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string v4, "UTF-8"

    invoke-interface {v3, v1, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 466
    const/4 v4, 0x0

    .line 467
    .local v4, "tagName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 468
    .local v5, "appPath":Ljava/lang/String;
    const/4 v6, 0x0

    .line 469
    .local v6, "is_add_apps":Z
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    .local v7, "type":I
    :goto_0
    const/4 v8, 0x1

    if-eq v8, v7, :cond_c

    .line 471
    const-string v8, "ignore_apps"

    const-string v9, "add_apps"

    packed-switch v7, :pswitch_data_0

    goto/16 :goto_4

    .line 500
    :pswitch_0
    :try_start_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    .line 501
    .local v10, "end_tag_name":Ljava/lang/String;
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 502
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 503
    :cond_1
    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 504
    const/4 v6, 0x1

    goto/16 :goto_4

    .line 473
    .end local v10    # "end_tag_name":Ljava/lang/String;
    :pswitch_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    move-object v4, v10

    .line 474
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v10

    if-lez v10, :cond_2

    .line 475
    const/4 v10, 0x0

    invoke-interface {v3, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    move-object v5, v10

    .line 477
    :cond_2
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 478
    const/4 v6, 0x1

    goto :goto_4

    .line 479
    :cond_3
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 480
    const/4 v6, 0x0

    goto :goto_4

    .line 481
    :cond_4
    const-string v8, "app"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 482
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 483
    .local v8, "ss":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 484
    .local v9, "is_current_cust":Z
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    array-length v11, v8

    if-ge v10, v11, :cond_6

    .line 485
    aget-object v11, v8, v10

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 486
    const/4 v9, 0x1

    .line 487
    goto :goto_2

    .line 484
    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 490
    .end local v10    # "i":I
    :cond_6
    :goto_2
    if-eqz v6, :cond_7

    if-eqz v9, :cond_8

    :cond_7
    if-nez v6, :cond_9

    if-eqz v9, :cond_9

    .line 491
    :cond_8
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    invoke-interface {v10, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 492
    :cond_9
    if-eqz v6, :cond_a

    if-eqz v9, :cond_a

    .line 493
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    invoke-interface {v10, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 494
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    invoke-interface {v10, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 497
    .end local v8    # "ss":[Ljava/lang/String;
    .end local v9    # "is_current_cust":Z
    :cond_a
    :goto_3
    nop

    .line 510
    :cond_b
    :goto_4
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v7, v8

    goto/16 :goto_0

    .line 470
    .end local v2    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "tagName":Ljava/lang/String;
    .end local v5    # "appPath":Ljava/lang/String;
    .end local v6    # "is_add_apps":Z
    .end local v7    # "type":I
    :cond_c
    goto :goto_5

    .line 516
    :catchall_0
    move-exception v2

    goto :goto_6

    .line 513
    :catch_0
    move-exception v2

    .line 514
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 516
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_5
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 517
    nop

    .line 518
    return-void

    .line 516
    :goto_6
    invoke-static {v1}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 517
    throw v2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static shouldLockAppRegion()Z
    .locals 2

    .line 689
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->CUSTOMIZED_REGION:Ljava/lang/String;

    const-string v1, "lm_cr"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "mx_telcel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private shouldSkipInstall(Ljava/lang/String;)Z
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1882
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_GLOBAL_REGION_BUILD:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsGlobalCrbtSupported:Z

    if-eqz v0, :cond_5

    sget v0, Landroid/os/Build$VERSION;->DEVICE_INITIAL_SDK_INT:I

    const/16 v2, 0x21

    if-ge v0, v2, :cond_0

    goto :goto_2

    .line 1886
    :cond_0
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sTHPhoneAppsSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1887
    .local v0, "isTHPhoneApp":Z
    sget-object v2, Lcom/android/server/pm/PackageManagerServiceImpl;->sGLPhoneAppsSet:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 1889
    .local v2, "isGLPhoneApp":Z
    if-nez v2, :cond_2

    if-eqz v0, :cond_1

    goto :goto_0

    .line 1897
    :cond_1
    return v1

    .line 1890
    :cond_2
    :goto_0
    const-string v3, "ro.miui.region"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1891
    .local v3, "region":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    return v1

    .line 1894
    :cond_3
    const-string v1, "TH"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v0

    :goto_1
    return v1

    .line 1884
    .end local v0    # "isTHPhoneApp":Z
    .end local v2    # "isGLPhoneApp":Z
    .end local v3    # "region":Ljava/lang/String;
    :cond_5
    :goto_2
    return v1
.end method

.method private shouldSkipInstallCotaApp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1901
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->SUPPORT_DEL_COTA_APP:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1902
    return v1

    .line 1904
    :cond_0
    nop

    .line 1905
    const-string v0, "persist.sys.cota.carrier"

    const-string v2, ""

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1906
    .local v0, "cotaCarrier":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCotaApps:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1907
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCotaApps:Ljava/util/HashMap;

    .line 1908
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    nop

    .line 1907
    :goto_0
    return v1

    .line 1910
    :cond_2
    return v1
.end method

.method private updateDefaultPkgInstallerLocked()Z
    .locals 11

    .line 1184
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x0

    if-nez v0, :cond_10

    .line 1185
    const-string/jumbo v0, "updateDefaultPkgInstallerLocked"

    const-string v2, "PKMSImpl"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1186
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiInstallerPackageSetting:Lcom/android/server/pm/PackageSetting;

    .line 1187
    .local v0, "miuiInstaller":Lcom/android/server/pm/PackageSetting;
    const-string v3, "com.miui.packageinstaller"

    if-nez v0, :cond_0

    .line 1188
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    iget-object v4, v4, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v4, v3}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/android/server/pm/PackageSetting;

    .line 1190
    :cond_0
    if-eqz v0, :cond_1

    .line 1191
    const-string v4, "found miui installer"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1193
    :cond_1
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    iget-object v4, v4, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    const-string v5, "com.google.android.packageinstaller"

    invoke-virtual {v4, v5}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/pm/PackageSetting;

    .line 1194
    .local v4, "googleInstaller":Lcom/android/server/pm/PackageSetting;
    if-eqz v4, :cond_2

    .line 1195
    const-string v6, "found google installer"

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    :cond_2
    iget-object v6, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    iget-object v6, v6, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    const-string v7, "com.android.packageinstaller"

    invoke-virtual {v6, v7}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/pm/PackageSetting;

    .line 1198
    .local v6, "androidInstaller":Lcom/android/server/pm/PackageSetting;
    if-eqz v6, :cond_3

    .line 1199
    const-string v8, "found android installer"

    invoke-static {v2, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    :cond_3
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->isCTS()Z

    move-result v8

    const/4 v9, 0x1

    if-nez v8, :cond_5

    if-eqz v0, :cond_5

    .line 1202
    invoke-virtual {v0}, Lcom/android/server/pm/PackageSetting;->isSystem()Z

    move-result v8

    if-nez v8, :cond_4

    goto :goto_0

    :cond_4
    move v8, v1

    goto :goto_1

    :cond_5
    :goto_0
    move v8, v9

    .line 1203
    .local v8, "isUseGooglePackageInstaller":Z
    :goto_1
    if-eqz v8, :cond_a

    .line 1204
    iget-object v10, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_f

    iget-object v10, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    .line 1205
    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_f

    .line 1206
    if-eqz v0, :cond_6

    .line 1207
    invoke-virtual {v0, v1, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    .line 1209
    :cond_6
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    iget-object v2, v2, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v2, v3}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/pm/PackageSetting;

    iput-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiInstallerPackageSetting:Lcom/android/server/pm/PackageSetting;

    .line 1210
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v2, v3}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/pm/pkg/AndroidPackage;

    iput-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiInstallerPackage:Lcom/android/server/pm/pkg/AndroidPackage;

    .line 1211
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    iget-object v2, v2, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v2, v3}, Lcom/android/server/utils/WatchedArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v2, v3}, Lcom/android/server/pm/Settings;->isDisabledSystemPackageLPr(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1213
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v2, v3}, Lcom/android/server/pm/Settings;->removeDisabledSystemPackageLPw(Ljava/lang/String;)V

    .line 1215
    :cond_7
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v2, v3}, Lcom/android/server/utils/WatchedArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1216
    if-eqz v4, :cond_8

    .line 1217
    invoke-virtual {v4, v9, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    .line 1218
    iput-object v5, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    goto :goto_2

    .line 1219
    :cond_8
    if-eqz v6, :cond_9

    .line 1220
    invoke-virtual {v6, v9, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    .line 1221
    iput-object v7, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    .line 1223
    :cond_9
    :goto_2
    return v9

    .line 1226
    :cond_a
    iget-object v5, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 1227
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiInstallerPackageSetting:Lcom/android/server/pm/PackageSetting;

    if-eqz v2, :cond_b

    .line 1228
    invoke-virtual {v2, v9, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    .line 1229
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    iget-object v2, v2, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    iget-object v5, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiInstallerPackageSetting:Lcom/android/server/pm/PackageSetting;

    invoke-virtual {v2, v3, v5}, Lcom/android/server/utils/WatchedArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1230
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiInstallerPackageSetting:Lcom/android/server/pm/PackageSetting;

    invoke-virtual {v2}, Lcom/android/server/pm/PackageSetting;->getFlags()I

    move-result v2

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_b

    .line 1231
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v2, v3, v9}, Lcom/android/server/pm/Settings;->disableSystemPackageLPw(Ljava/lang/String;Z)Z

    .line 1234
    :cond_b
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiInstallerPackage:Lcom/android/server/pm/pkg/AndroidPackage;

    if-eqz v2, :cond_c

    .line 1235
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    iget-object v5, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiInstallerPackage:Lcom/android/server/pm/pkg/AndroidPackage;

    invoke-virtual {v2, v3, v5}, Lcom/android/server/utils/WatchedArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1237
    :cond_c
    iput-object v3, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    .line 1238
    if-eqz v4, :cond_d

    .line 1239
    invoke-virtual {v4, v1, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    .line 1241
    :cond_d
    if-eqz v6, :cond_e

    .line 1242
    invoke-virtual {v6, v1, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    .line 1244
    :cond_e
    return v9

    .line 1247
    :cond_f
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "set default package install as"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    .end local v0    # "miuiInstaller":Lcom/android/server/pm/PackageSetting;
    .end local v4    # "googleInstaller":Lcom/android/server/pm/PackageSetting;
    .end local v6    # "androidInstaller":Lcom/android/server/pm/PackageSetting;
    .end local v8    # "isUseGooglePackageInstaller":Z
    :cond_10
    return v1
.end method

.method private updateSystemAppState(IZLjava/lang/String;)V
    .locals 8
    .param p1, "userId"    # I
    .param p2, "isCTS"    # Z
    .param p3, "pkg"    # Ljava/lang/String;

    .line 1573
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v0

    .line 1574
    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v1, p3}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;

    move-result-object v1

    .line 1575
    .local v1, "pkgSetting":Lcom/android/server/pm/PackageSetting;
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/android/server/pm/PackageSetting;->isSystem()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_3

    .line 1578
    :cond_0
    nop

    .line 1579
    invoke-virtual {v1}, Lcom/android/server/pm/PackageSetting;->getFlags()I

    move-result v2

    and-int/lit16 v2, v2, 0x80

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 1580
    invoke-virtual {v1, p1}, Lcom/android/server/pm/PackageSetting;->getInstalled(I)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    move v2, v4

    .line 1581
    .local v2, "updatedSystemApp":Z
    :goto_0
    invoke-direct {p0, v1, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->getdatedefaultState(Lcom/android/server/pm/PackageSetting;I)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    move v5, v3

    goto :goto_1

    :cond_2
    move v5, v4

    .line 1582
    .local v5, "untouchedYet":Z
    :goto_1
    invoke-virtual {v1, p1}, Lcom/android/server/pm/PackageSetting;->getEnabled(I)I

    move-result v6

    .line 1583
    .local v6, "state":I
    const/4 v7, 0x2

    if-eq v6, v7, :cond_4

    const/4 v7, 0x3

    if-ne v6, v7, :cond_3

    goto :goto_2

    :cond_3
    move v3, v4

    .line 1585
    .local v3, "alreadyDisabled":Z
    :cond_4
    :goto_2
    const-string v4, "COTA"

    invoke-virtual {v1, p1}, Lcom/android/server/pm/PackageSetting;->readUserState(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;

    move-result-object v7

    invoke-interface {v7}, Lcom/android/server/pm/pkg/PackageUserStateInternal;->getLastDisableAppCaller()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    move v1, v4

    .line 1586
    .end local v6    # "state":I
    .local v1, "wasDisabledByUs":Z
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1587
    if-eqz v2, :cond_5

    .line 1588
    return-void

    .line 1590
    :cond_5
    if-eqz p2, :cond_7

    .line 1592
    if-eqz v3, :cond_6

    if-eqz v1, :cond_6

    invoke-direct {p0, p1, p3}, Lcom/android/server/pm/PackageManagerServiceImpl;->enableSystemApp(ILjava/lang/String;)V

    .line 1593
    :cond_6
    return-void

    .line 1596
    :cond_7
    if-nez v3, :cond_8

    if-eqz v5, :cond_8

    .line 1597
    invoke-direct {p0, p1, p3}, Lcom/android/server/pm/PackageManagerServiceImpl;->disableSystemApp(ILjava/lang/String;)V

    .line 1599
    :cond_8
    return-void

    .line 1576
    .end local v2    # "updatedSystemApp":Z
    .end local v3    # "alreadyDisabled":Z
    .end local v5    # "untouchedYet":Z
    .local v1, "pkgSetting":Lcom/android/server/pm/PackageSetting;
    :cond_9
    :goto_3
    :try_start_1
    monitor-exit v0

    return-void

    .line 1586
    .end local v1    # "pkgSetting":Lcom/android/server/pm/PackageSetting;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private updatedefaultState(Lcom/android/server/pm/PackageSetting;Ljava/lang/String;I)V
    .locals 1
    .param p1, "ps"    # Lcom/android/server/pm/PackageSetting;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "userId"    # I

    .line 1606
    invoke-virtual {p1, p3}, Lcom/android/server/pm/PackageSetting;->modifyUserState(I)Lcom/android/server/pm/pkg/PackageUserStateImpl;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/android/server/pm/pkg/PackageUserStateImpl;->setDefaultState(Ljava/lang/String;)V

    .line 1607
    return-void
.end method

.method private static verifyInstallFromShell(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sessionId"    # I
    .param p2, "log"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/pm/PackageManagerException;
        }
    .end annotation

    .line 1068
    const/4 v0, -0x1

    .line 1070
    .local v0, "result":I
    :try_start_0
    invoke-static {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isSecondUserlocked(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1071
    const/4 v0, 0x2

    goto :goto_0

    .line 1073
    :cond_0
    invoke-static {p1}, Lcom/android/server/pm/PmInjector;->installVerify(I)I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 1077
    :goto_0
    goto :goto_1

    .line 1075
    :catchall_0
    move-exception v1

    .line 1076
    .local v1, "e":Ljava/lang/Throwable;
    const-string v2, "PKMSImpl"

    const-string v3, "Error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1078
    .end local v1    # "e":Ljava/lang/Throwable;
    :goto_1
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x1

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    .line 1079
    :goto_2
    invoke-static {v0}, Lcom/android/server/pm/PmInjector;->statusToString(I)Ljava/lang/String;

    move-result-object v2

    .line 1078
    const/16 v3, -0x6f

    invoke-static {v1, v3, v2, p2}, Lcom/android/server/pm/PackageManagerServiceImpl;->fatalIf(ZILjava/lang/String;Ljava/lang/String;)V

    .line 1080
    return-void
.end method

.method private static verifyPackageForRelease(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/SigningDetails;ILjava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "signingDetails"    # Landroid/content/pm/SigningDetails;
    .param p3, "callingUid"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;
    .param p5, "log"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/pm/PackageManagerException;
        }
    .end annotation

    .line 1045
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsBootLoaderLocked:Z

    if-eqz v0, :cond_4

    .line 1046
    const-string v0, "com.google.android.webview"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sInstallerSet:Ljava/util/Set;

    invoke-interface {v0, p4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "FAILED_VERIFICATION_FAILURE MIUI WEBVIEW"

    const/16 v4, -0x16

    invoke-static {v0, v4, v3, p5}, Lcom/android/server/pm/PackageManagerServiceImpl;->fatalIf(ZILjava/lang/String;Ljava/lang/String;)V

    .line 1049
    invoke-static {p1}, Lmiui/content/pm/PreloadedAppPolicy;->isProtectedDataApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1050
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->isPackageAvailable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1051
    return v1

    .line 1053
    :cond_1
    invoke-static {p1}, Lmiui/content/pm/PreloadedAppPolicy;->getProtectedDataAppSign(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1054
    .local v0, "signSha256":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1055
    const-string v3, ":"

    const-string v5, ""

    invoke-virtual {v0, v3, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Llibcore/util/HexEncoding;->decode(Ljava/lang/String;Z)[B

    move-result-object v2

    .line 1054
    invoke-virtual {p2, v2}, Landroid/content/pm/SigningDetails;->hasSha256Certificate([B)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 1059
    :cond_2
    const-string v2, "FAILED_VERIFICATION_FAILURE SIGNATURE FAIL"

    invoke-static {v1, v4, v2, p5}, Lcom/android/server/pm/PackageManagerServiceImpl;->fatalIf(ZILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1057
    :cond_3
    :goto_1
    return v1

    .line 1063
    .end local v0    # "signSha256":Ljava/lang/String;
    :cond_4
    :goto_2
    return v1
.end method


# virtual methods
.method addMiuiSharedUids()V
    .locals 5

    .line 257
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    const-string v1, "android.uid.theme"

    const/16 v2, 0x17d5

    const/4 v3, 0x1

    const/16 v4, 0x8

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;III)Lcom/android/server/pm/SharedUserSetting;

    .line 261
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    const-string v1, "android.uid.backup"

    const/16 v2, 0x17d4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;III)Lcom/android/server/pm/SharedUserSetting;

    .line 264
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    const-string v1, "android.uid.updater"

    const/16 v2, 0x17d6

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;III)Lcom/android/server/pm/SharedUserSetting;

    .line 267
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    const-string v1, "android.uid.finddevice"

    const/16 v2, 0x17de

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;III)Lcom/android/server/pm/SharedUserSetting;

    .line 270
    return-void
.end method

.method public addPermierFlagIfNeedForGlobalROM(Landroid/util/ArrayMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/content/pm/FeatureInfo;",
            ">;)V"
        }
    .end annotation

    .line 1916
    .local p1, "availableFeatures":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/content/pm/FeatureInfo;>;"
    if-nez p1, :cond_0

    .line 1917
    return-void

    .line 1922
    :cond_0
    const-string v0, "com.google.android.feature.PREMIER_TIER"

    .line 1923
    .local v0, "FEATURE_PERMIER_TIER":Ljava/lang/String;
    const-string v1, "com.google.android.feature.PREMIER_TIER"

    invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1924
    const-string v2, "ro.com.miui.rsa"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "tier1"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1925
    const-string v2, "ro.miui.build.region"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "global"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1926
    const-string v2, "ro.miui.customized.region"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1927
    new-instance v2, Landroid/content/pm/FeatureInfo;

    invoke-direct {v2}, Landroid/content/pm/FeatureInfo;-><init>()V

    .line 1928
    .local v2, "fi":Landroid/content/pm/FeatureInfo;
    iput-object v1, v2, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    .line 1929
    invoke-virtual {p1, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931
    .end local v2    # "fi":Landroid/content/pm/FeatureInfo;
    :cond_1
    return-void
.end method

.method assertValidApkAndInstaller(Ljava/lang/String;Landroid/content/pm/SigningDetails;ILjava/lang/String;ZI)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "signingDetails"    # Landroid/content/pm/SigningDetails;
    .param p3, "callingUid"    # I
    .param p4, "callingPackage"    # Ljava/lang/String;
    .param p5, "isManuallyAccepted"    # Z
    .param p6, "sessionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/pm/PackageManagerException;
        }
    .end annotation

    .line 1001
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->isCTS()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1003
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MIUILOG- assertCallerAndPackage: uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", installerPkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1004
    .local v0, "log":Ljava/lang/String;
    invoke-static {p3}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v7

    .line 1005
    .local v7, "callingAppId":I
    sparse-switch v7, :sswitch_data_0

    .line 1014
    sget-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sNoVerifyAllowPackage:Ljava/util/ArrayList;

    invoke-virtual {v1, p4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1015
    return-void

    .line 1009
    :sswitch_0
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, p6, v0}, Lcom/android/server/pm/PackageManagerServiceImpl;->verifyInstallFromShell(Landroid/content/Context;ILjava/lang/String;)V

    .line 1010
    return-void

    .line 1007
    :sswitch_1
    return-void

    .line 1019
    :cond_1
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, p3, p4}, Lcom/android/server/pm/PackageManagerServiceImpl;->isTrustedEnterpriseInstaller(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v1

    const/4 v8, 0x1

    if-eqz v1, :cond_3

    .line 1020
    invoke-static {}, Lmiui/enterprise/ApplicationHelperStub;->getInstance()Lmiui/enterprise/IApplicationHelper;

    move-result-object v1

    .line 1021
    invoke-interface {v1, p4}, Lmiui/enterprise/IApplicationHelper;->isTrustedAppStoresDisable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v1, v8

    .line 1019
    :goto_1
    const/16 v2, -0x16

    const-string v3, "FAILED_VERIFICATION_FAILURE ENTERPRISE"

    invoke-static {v1, v2, v3, v0}, Lcom/android/server/pm/PackageManagerServiceImpl;->fatalIf(ZILjava/lang/String;Ljava/lang/String;)V

    .line 1026
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/android/server/pm/PackageManagerServiceImpl;->verifyPackageForRelease(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/SigningDetails;ILjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1027
    return-void

    .line 1030
    :cond_4
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, p4}, Lcom/android/server/pm/PackageManagerServiceImpl;->notifyGlobalPackageInstaller(Landroid/content/Context;Ljava/lang/String;)V

    .line 1032
    sget-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sAllowPackage:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1033
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/pm/PackageManagerServiceImpl;->initAllowPackageList(Landroid/content/Context;)V

    .line 1035
    :cond_5
    if-nez p5, :cond_7

    invoke-virtual {v1, p4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z

    if-nez v1, :cond_6

    goto :goto_2

    .line 1038
    :cond_6
    const/16 v1, -0x73

    const-string v2, "Permission denied"

    invoke-static {v8, v1, v2, v0}, Lcom/android/server/pm/PackageManagerServiceImpl;->fatalIf(ZILjava/lang/String;Ljava/lang/String;)V

    .line 1039
    return-void

    .line 1036
    :cond_7
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x7d0 -> :sswitch_0
    .end sparse-switch
.end method

.method public asyncDexMetadataDexopt(Lcom/android/server/pm/pkg/AndroidPackage;[I)V
    .locals 1
    .param p1, "pkg"    # Lcom/android/server/pm/pkg/AndroidPackage;
    .param p2, "userId"    # [I

    .line 1729
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiDexopt:Lcom/android/server/pm/MiuiDexopt;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/MiuiDexopt;->asyncDexMetadataDexopt(Lcom/android/server/pm/pkg/AndroidPackage;[I)V

    .line 1730
    return-void
.end method

.method public beforeSystemReady()V
    .locals 5

    .line 280
    invoke-static {}, Lcom/android/server/pm/PreinstallApp;->exemptPermissionRestrictions()V

    .line 281
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 282
    const-string v1, "device_provisioned"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/pm/PackageManagerServiceImpl$1;

    iget-object v3, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/server/pm/PackageManagerServiceImpl$1;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Landroid/os/Handler;)V

    .line 281
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 289
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPackageManagerCloudHelper:Lcom/android/server/pm/PackageManagerCloudHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerCloudHelper;->registerCloudDataObserver()V

    .line 290
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiDexopt:Lcom/android/server/pm/MiuiDexopt;

    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/server/pm/MiuiDexopt;->preConfigMiuiDexopt(Landroid/content/Context;)V

    .line 292
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    .line 293
    .local v0, "miuiPreinstallHelper":Lcom/android/server/pm/MiuiPreinstallHelper;
    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/server/pm/PackageManagerServiceImpl$2;

    invoke-direct {v2, p0, v0}, Lcom/android/server/pm/PackageManagerServiceImpl$2;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Lcom/android/server/pm/MiuiPreinstallHelper;)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 301
    :cond_0
    return-void
.end method

.method canBeDisabled(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "newState"    # I

    .line 655
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 656
    .local v0, "callingUid":I
    if-eqz p2, :cond_8

    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    goto :goto_3

    .line 661
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "maintenance_mode_user_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x6e

    if-ne v1, v2, :cond_1

    .line 663
    return-void

    .line 667
    :cond_1
    goto :goto_0

    .line 665
    :catch_0
    move-exception v1

    .line 666
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 668
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :goto_0
    const/16 v1, 0x7d0

    if-ne v0, v1, :cond_4

    .line 670
    const-string v1, "com.android.cts.priv.ctsshim"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 671
    return-void

    .line 673
    :cond_2
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v1

    .line 674
    .local v1, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    if-eqz v1, :cond_4

    invoke-interface {v1}, Lcom/android/server/pm/pkg/PackageStateInternal;->isSystem()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    if-eq p2, v2, :cond_3

    goto :goto_1

    .line 675
    :cond_3
    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "Cannot disable system packages."

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 678
    .end local v1    # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    :cond_4
    :goto_1
    sget-object v1, Lcom/android/server/pm/PackageManagerServiceImpl;->MIUI_CORE_APPS:[Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 682
    const-string v1, "co.sitic.pp"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldLockAppRegion()Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_2

    .line 683
    :cond_5
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Cannot disable carrier core packages."

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 685
    :cond_6
    :goto_2
    return-void

    .line 679
    :cond_7
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Cannot disable miui core packages."

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 658
    :cond_8
    :goto_3
    return-void
.end method

.method public canBeUpdate(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/server/pm/PrepareFailure;
        }
    .end annotation

    .line 1937
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPackageManagerCloudHelper:Lcom/android/server/pm/PackageManagerCloudHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerCloudHelper;->getCloudNotSupportUpdateSystemApps()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1938
    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sNotSupportUpdateSystemApps:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1940
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPackageManagerCloudHelper:Lcom/android/server/pm/PackageManagerCloudHelper;

    .line 1939
    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerCloudHelper;->getCloudNotSupportUpdateSystemApps()Ljava/util/Set;

    move-result-object v0

    .line 1940
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    nop

    .line 1941
    .local v0, "isNotSupportUpdate":Z
    if-nez v0, :cond_1

    .line 1945
    return-void

    .line 1942
    :cond_1
    new-instance v1, Lcom/android/server/pm/PrepareFailure;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " are not updateable."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x2

    invoke-direct {v1, v3, v2}, Lcom/android/server/pm/PrepareFailure;-><init>(ILjava/lang/String;)V

    throw v1
.end method

.method checkEnterpriseRestriction(Lcom/android/server/pm/parsing/pkg/ParsedPackage;)Z
    .locals 5
    .param p1, "pkg"    # Lcom/android/server/pm/parsing/pkg/ParsedPackage;

    .line 600
    invoke-static {}, Lmiui/enterprise/ApplicationHelperStub;->getInstance()Lmiui/enterprise/IApplicationHelper;

    move-result-object v0

    .line 601
    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getRequestedPermissions()Ljava/util/List;

    move-result-object v2

    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getBaseApkPath()Ljava/lang/String;

    move-result-object v3

    .line 600
    invoke-interface {v0, v1, v2, v3}, Lmiui/enterprise/IApplicationHelper;->isEnterpriseInstallRestriction(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 602
    return v1

    .line 604
    :cond_0
    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getRequestedPermissions()Ljava/util/List;

    move-result-object v0

    const-string v2, "com.miui.enterprise.permission.ACCESS_ENTERPRISE_API"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "PKMSImpl"

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 607
    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getBaseApkPath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 606
    invoke-static {v0, v3, v4}, Lcom/miui/enterprise/signature/EnterpriseVerifier;->verify(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Verify enterprise signature of package "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 609
    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " failed"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 608
    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    return v1

    .line 612
    :cond_1
    sget-boolean v0, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z

    const/4 v3, 0x0

    if-nez v0, :cond_2

    .line 613
    return v3

    .line 615
    :cond_2
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/miui/enterprise/ApplicationHelper;->checkEnterprisePackageRestriction(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 616
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Installation of package "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/android/server/pm/parsing/pkg/ParsedPackage;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " is restricted"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    return v1

    .line 619
    :cond_3
    return v3
.end method

.method public checkGTSSpecAppOptMode()V
    .locals 11

    .line 1336
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    .line 1337
    const-string v0, "com.miui.screenrecorder"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    .local v0, "pkgs":[Ljava/lang/String;
    goto :goto_0

    .line 1341
    .end local v0    # "pkgs":[Ljava/lang/String;
    :cond_0
    const-string v0, "com.miui.cleanmaster"

    const-string v1, "com.xiaomi.drivemode"

    const-string v2, "com.xiaomi.aiasst.service"

    const-string v3, "com.miui.thirdappassistant"

    const-string v4, "com.miui.screenrecorder"

    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    .line 1349
    .restart local v0    # "pkgs":[Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v1

    .line 1350
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    .line 1351
    .local v2, "mPkgSettings":Lcom/android/server/pm/Settings;
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->isCTS()Z

    move-result v3

    .line 1352
    .local v3, "isCtsBuild":Z
    array-length v4, v0

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v4, :cond_6

    aget-object v7, v0, v6

    .line 1353
    .local v7, "pkg":Ljava/lang/String;
    iget-object v8, v2, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v8, v7}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/pm/PackageSetting;

    .line 1354
    .local v8, "uninstallPkg":Lcom/android/server/pm/PackageSetting;
    const-string v9, "com.miui.cleanmaster"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    if-nez v8, :cond_1

    .line 1355
    iget-object v9, v2, Lcom/android/server/pm/Settings;->mPermissions:Lcom/android/server/pm/permission/LegacyPermissionSettings;

    const-string v10, "com.miui.cleanmaster.permission.Clean_Master"

    invoke-static {v9, v7, v10}, Lcom/android/server/pm/PackageManagerServiceImpl;->checkAndClearResiduePermissions(Lcom/android/server/pm/permission/LegacyPermissionSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 1358
    :cond_1
    const-string v9, "com.miui.screenrecorder"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    if-nez v8, :cond_2

    .line 1359
    iget-object v9, v2, Lcom/android/server/pm/Settings;->mPermissions:Lcom/android/server/pm/permission/LegacyPermissionSettings;

    const-string v10, "com.miui.screenrecorder.DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION"

    invoke-static {v9, v7, v10}, Lcom/android/server/pm/PackageManagerServiceImpl;->checkAndClearResiduePermissions(Lcom/android/server/pm/permission/LegacyPermissionSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    :cond_2
    if-eqz v3, :cond_5

    if-eqz v8, :cond_5

    invoke-virtual {v8}, Lcom/android/server/pm/PackageSetting;->isSystem()Z

    move-result v9

    if-nez v9, :cond_5

    .line 1363
    invoke-virtual {v8, v5, v5}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V

    .line 1365
    iget-object v9, v2, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v9, v7}, Lcom/android/server/utils/WatchedArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1366
    invoke-virtual {v2, v7}, Lcom/android/server/pm/Settings;->isDisabledSystemPackageLPr(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1367
    invoke-virtual {v2, v7}, Lcom/android/server/pm/Settings;->removeDisabledSystemPackageLPw(Ljava/lang/String;)V

    .line 1369
    :cond_3
    iget-object v9, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v9, v9, Lcom/android/server/pm/PackageManagerService;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v9, v7}, Lcom/android/server/utils/WatchedArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1371
    const-string v9, "com.miui.cleanmaster"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    const-string v9, "com.miui.thirdappassistant"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1372
    :cond_4
    invoke-static {v7}, Lcom/android/server/pm/PackageManagerServiceImpl;->clearPermissions(Ljava/lang/String;)V

    .line 1352
    .end local v7    # "pkg":Ljava/lang/String;
    .end local v8    # "uninstallPkg":Lcom/android/server/pm/PackageSetting;
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1376
    .end local v2    # "mPkgSettings":Lcom/android/server/pm/Settings;
    .end local v3    # "isCtsBuild":Z
    :cond_6
    monitor-exit v1

    .line 1377
    return-void

    .line 1376
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public checkReplaceSetting(ILcom/android/server/pm/SettingBase;)Z
    .locals 3
    .param p1, "appId"    # I
    .param p2, "setting"    # Lcom/android/server/pm/SettingBase;

    .line 1812
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    instance-of v0, p2, Lcom/android/server/pm/PackageSetting;

    if-eqz v0, :cond_0

    .line 1813
    move-object v0, p2

    check-cast v0, Lcom/android/server/pm/PackageSetting;

    .line 1814
    .local v0, "ps":Lcom/android/server/pm/PackageSetting;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1815
    invoke-virtual {v0}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with user id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot replace SYSTEM_UID"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1814
    const/4 v2, 0x6

    invoke-static {v2, v1}, Lcom/android/server/pm/PackageManagerService;->reportSettingsProblem(ILjava/lang/String;)V

    .line 1817
    const/4 v1, 0x1

    return v1

    .line 1819
    .end local v0    # "ps":Lcom/android/server/pm/PackageSetting;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method clearNoHistoryFlagIfNeed(Ljava/util/List;Landroid/content/Intent;)V
    .locals 7
    .param p2, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .line 547
    .local p1, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const-string v0, "PKMSImpl"

    sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_0

    .line 548
    return-void

    .line 551
    :cond_0
    if-nez p1, :cond_1

    .line 552
    return-void

    .line 555
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 556
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 558
    .local v3, "bundle":Landroid/os/Bundle;
    if-eqz v3, :cond_2

    :try_start_0
    const-string v4, "mi_use_custom_resolver"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v4, v4, Landroid/content/pm/ActivityInfo;->enabled:Z

    if-eqz v4, :cond_2

    .line 560
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Removing FLAG_ACTIVITY_NO_HISTORY flag for Intent {"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 561
    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p2, v5, v5, v5, v6}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 560
    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {p2, v4}, Landroid/content/Intent;->removeFlags(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 564
    :catch_0
    move-exception v4

    .line 565
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 566
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_1
    nop

    .line 567
    .end local v2    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v3    # "bundle":Landroid/os/Bundle;
    :goto_2
    goto :goto_0

    .line 568
    :cond_3
    return-void
.end method

.method public dumpSingleDexoptState(Lcom/android/internal/util/IndentingPrintWriter;Lcom/android/server/pm/pkg/PackageStateInternal;Ljava/lang/String;)V
    .locals 4
    .param p1, "pw"    # Lcom/android/internal/util/IndentingPrintWriter;
    .param p2, "pkgSetting"    # Lcom/android/server/pm/pkg/PackageStateInternal;
    .param p3, "packageName"    # Ljava/lang/String;

    .line 1490
    :try_start_0
    invoke-interface {p2}, Lcom/android/server/pm/pkg/PackageStateInternal;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v0

    .line 1491
    .local v0, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    if-nez v0, :cond_0

    .line 1492
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to find AndroidPackage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 1493
    return-void

    .line 1496
    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()Lcom/android/internal/util/IndentingPrintWriter;

    .line 1497
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "base APK odex file size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/content/pm/PackageParserStub;->get()Landroid/content/pm/PackageParserStub;

    move-result-object v2

    invoke-interface {v0}, Lcom/android/server/pm/pkg/AndroidPackage;->getBaseApkPath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/content/pm/PackageParserStub;->getDexFileSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 1498
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()Lcom/android/internal/util/IndentingPrintWriter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1502
    nop

    .end local v0    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    goto :goto_0

    .line 1500
    :catch_0
    move-exception v0

    .line 1501
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PKMSImpl"

    const-string v2, "Failed to dumpSingleDexoptState"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1503
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public exemptApplink(Landroid/content/Intent;Ljava/util/List;Ljava/util/List;)Z
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)Z"
        }
    .end annotation

    .line 1709
    .local p2, "candidates":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .local p3, "result":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual {p1}, Landroid/content/Intent;->isWebIntent()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1710
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "digitalkeypairing.org"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1711
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 1712
    .local v0, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1713
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 1714
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v2, :cond_0

    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v3, :cond_0

    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 1715
    const-string v4, "com.miui.tsmclient"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1716
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1717
    goto :goto_1

    .line 1712
    .end local v2    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1720
    .end local v1    # "i":I
    :cond_1
    :goto_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 1721
    const/4 v1, 0x1

    return v1

    .line 1724
    .end local v0    # "size":I
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public getApplicationInfoBySelf(IIJI)Ljava/util/List;
    .locals 9
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "flags"    # J
    .param p5, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJI)",
            "Ljava/util/List<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation

    .line 1763
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/pm/Computer;->getPackage(I)Lcom/android/server/pm/pkg/AndroidPackage;

    move-result-object v0

    .line 1764
    .local v0, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 1765
    :cond_0
    invoke-interface {v0}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getInstalledApplications"

    invoke-direct {p0, p1, v2, v3}, Lcom/android/server/pm/PackageManagerServiceImpl;->isAllowedToGetInstalledApps(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v7

    .line 1767
    .local v7, "allowed":Z
    if-nez v7, :cond_1

    .line 1768
    new-instance v8, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda1;

    move-object v1, v8

    move-object v2, p0

    move-object v3, v0

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Lcom/android/server/pm/pkg/AndroidPackage;JI)V

    invoke-static {v8}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingSupplier;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    return-object v1

    .line 1779
    :cond_1
    return-object v1
.end method

.method public getDexOptResult()I
    .locals 1

    .line 1513
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexoptServiceThread:Lcom/android/server/pm/DexoptServiceThread;

    if-eqz v0, :cond_0

    .line 1514
    invoke-virtual {v0}, Lcom/android/server/pm/DexoptServiceThread;->getDexOptResult()I

    move-result v0

    return v0

    .line 1516
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getDexoptSecondaryResult()I
    .locals 1

    .line 1527
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexoptServiceThread:Lcom/android/server/pm/DexoptServiceThread;

    if-eqz v0, :cond_0

    .line 1528
    invoke-virtual {v0}, Lcom/android/server/pm/DexoptServiceThread;->getDexoptSecondaryResult()I

    move-result v0

    return v0

    .line 1530
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method getGoogleWebSearchResolveInfo(Ljava/util/List;)Landroid/content/pm/ResolveInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Landroid/content/pm/ResolveInfo;"
        }
    .end annotation

    .line 1122
    .local p1, "riList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 1125
    :cond_0
    const/4 v0, 0x0

    .line 1126
    .local v0, "ret":Landroid/content/pm/ResolveInfo;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 1127
    .local v2, "ri":Landroid/content/pm/ResolveInfo;
    if-eqz v2, :cond_1

    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v3, :cond_1

    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v4, "com.google.android.googlequicksearchbox"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1128
    move-object v0, v2

    .line 1129
    goto :goto_1

    .line 1131
    .end local v2    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_1
    goto :goto_0

    .line 1132
    :cond_2
    :goto_1
    return-object v0

    .line 1123
    .end local v0    # "ret":Landroid/content/pm/ResolveInfo;
    :cond_3
    :goto_2
    const/4 v0, 0x0

    return-object v0
.end method

.method getMarketResolveInfo(Ljava/util/List;)Landroid/content/pm/ResolveInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Landroid/content/pm/ResolveInfo;"
        }
    .end annotation

    .line 1111
    .local p1, "riList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v0, 0x0

    .line 1112
    .local v0, "ret":Landroid/content/pm/ResolveInfo;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 1113
    .local v2, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v4, "com.xiaomi.market"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Landroid/content/pm/ResolveInfo;->system:Z

    if-eqz v3, :cond_0

    .line 1114
    move-object v0, v2

    .line 1115
    goto :goto_1

    .line 1117
    .end local v2    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_0
    goto :goto_0

    .line 1118
    :cond_1
    :goto_1
    return-object v0
.end method

.method public getPackageActivatedBundle(Ljava/lang/String;IZ)Landroid/os/Bundle;
    .locals 1
    .param p1, "activatedPackage"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "clear"    # Z

    .line 1806
    const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    .line 1807
    invoke-interface {v0, p1, p2, p3}, Lcom/android/server/pm/PackageEventRecorderInternal;->getSourcePackage(Ljava/lang/String;IZ)Landroid/os/Bundle;

    move-result-object v0

    .line 1806
    return-object v0
.end method

.method public getPackageInfoBySelf(IIJI)Ljava/util/List;
    .locals 9
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "flags"    # J
    .param p5, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIJI)",
            "Ljava/util/List<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .line 1743
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/pm/Computer;->getPackage(I)Lcom/android/server/pm/pkg/AndroidPackage;

    move-result-object v0

    .line 1744
    .local v0, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 1745
    :cond_0
    invoke-interface {v0}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getInstalledPackages"

    invoke-direct {p0, p1, v2, v3}, Lcom/android/server/pm/PackageManagerServiceImpl;->isAllowedToGetInstalledApps(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v7

    .line 1747
    .local v7, "allowed":Z
    if-nez v7, :cond_1

    .line 1748
    new-instance v8, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda4;

    move-object v1, v8

    move-object v2, p0

    move-object v3, v0

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Lcom/android/server/pm/pkg/AndroidPackage;JI)V

    invoke-static {v8}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingSupplier;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    return-object v1

    .line 1759
    :cond_1
    return-object v1
.end method

.method getPersistentAppsForOtherUser(ZII)Ljava/util/List;
    .locals 4
    .param p1, "safeMode"    # Z
    .param p2, "flags"    # I
    .param p3, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZII)",
            "Ljava/util/List<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation

    .line 1665
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1666
    .local v0, "finalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;"
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    if-eqz v1, :cond_0

    .line 1667
    invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v2

    new-instance v3, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda5;

    invoke-direct {v3, p1, p2, p3, v0}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda5;-><init>(ZIILjava/util/ArrayList;)V

    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PackageManagerService;->forEachPackageState(Lcom/android/server/pm/Computer;Ljava/util/function/Consumer;)V

    .line 1684
    :cond_0
    return-object v0
.end method

.method public getService()Lcom/android/server/pm/PackageManagerService;
    .locals 1

    .line 1661
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    return-object v0
.end method

.method public getSupportAonServicePackageName()Ljava/lang/String;
    .locals 5

    .line 2003
    const-string v0, ""

    .line 2004
    .local v0, "supportAonServicePackageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsSupportAttention:Ljava/util/List;

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2005
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 2006
    invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    const v4, 0x104025b

    invoke-virtual {v3, v4}, Lcom/android/server/pm/PackageManagerService;->getPackageFromComponentString(I)Ljava/lang/String;

    move-result-object v3

    .line 2005
    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PackageManagerService;->ensureSystemPackageName(Lcom/android/server/pm/Computer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2009
    :cond_0
    return-object v0
.end method

.method public hasDomainVerificationRestriction()Z
    .locals 2

    .line 1829
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 1830
    :cond_0
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isProvisioned()Z

    move-result v0

    .line 1831
    .local v0, "isDeviceProvisioned":Z
    if-eqz v0, :cond_1

    .line 1832
    return v1

    .line 1834
    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method hookChooseBestActivity(Landroid/content/Intent;Ljava/lang/String;JLjava/util/List;ILandroid/content/pm/ResolveInfo;)Landroid/content/pm/ResolveInfo;
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "resolvedType"    # Ljava/lang/String;
    .param p3, "flags"    # J
    .param p6, "userId"    # I
    .param p7, "defaultValue"    # Landroid/content/pm/ResolveInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;I",
            "Landroid/content/pm/ResolveInfo;",
            ")",
            "Landroid/content/pm/ResolveInfo;"
        }
    .end annotation

    .line 1138
    .local p5, "query":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    move-object v0, p0

    move-object v7, p1

    move-object/from16 v8, p5

    if-nez v7, :cond_0

    .line 1139
    return-object p7

    .line 1142
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v9

    .line 1143
    .local v9, "scheme":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    move-object v10, v1

    .line 1144
    .local v10, "host":Ljava/lang/String;
    sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    const-string v2, "android.intent.action.VIEW"

    if-nez v1, :cond_5

    if-eqz v9, :cond_5

    const-string v1, "mimarket"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1145
    const-string v1, "market"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v10, :cond_5

    .line 1146
    const-string v1, "details"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "search"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1147
    :cond_3
    invoke-virtual {p0, v8}, Lcom/android/server/pm/PackageManagerServiceImpl;->getMarketResolveInfo(Ljava/util/List;)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1148
    .local v1, "ri":Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_4

    .line 1149
    return-object v1

    .line 1151
    .end local v1    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_4
    goto/16 :goto_2

    :cond_5
    sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_8

    const-string v1, "application/vnd.android.package-archive"

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1152
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1154
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->isCTS()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1155
    iget-object v1, v0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    .line 1156
    .local v1, "realPkgName":Ljava/lang/String;
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v2}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v2

    iget-object v3, v0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/android/server/pm/Computer;->getRenamedPackage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1157
    iget-object v2, v0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v2}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v2

    iget-object v3, v0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/android/server/pm/Computer;->getRenamedPackage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    goto :goto_1

    .line 1156
    :cond_6
    move-object v11, v1

    goto :goto_1

    .line 1160
    .end local v1    # "realPkgName":Ljava/lang/String;
    :cond_7
    const-string v1, "com.miui.packageinstaller"

    move-object v11, v1

    .line 1163
    .local v11, "realPkgName":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1, v11}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1164
    iget-object v1, v0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPM:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move/from16 v6, p6

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    return-object v1

    .line 1165
    .end local v11    # "realPkgName":Ljava/lang/String;
    :cond_8
    sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_9

    .line 1166
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isRsa4()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1167
    invoke-virtual {p0, v8}, Lcom/android/server/pm/PackageManagerServiceImpl;->getGoogleWebSearchResolveInfo(Ljava/util/List;)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1168
    .local v1, "ri":Landroid/content/pm/ResolveInfo;
    if-eqz v1, :cond_9

    .line 1169
    return-object v1

    .line 1173
    .end local v1    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_9
    :goto_2
    return-object p7
.end method

.method hookPkgInfo(Landroid/content/pm/PackageInfo;Ljava/lang/String;J)Landroid/content/pm/PackageInfo;
    .locals 1
    .param p1, "origPkgInfo"    # Landroid/content/pm/PackageInfo;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "flags"    # J

    .line 629
    invoke-static {p1, p2, p3, p4}, Lcom/miui/hybrid/hook/HookClient;->hookPkgInfo(Landroid/content/pm/PackageInfo;Ljava/lang/String;J)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method

.method init(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/Settings;Landroid/content/Context;)V
    .locals 5
    .param p1, "pms"    # Lcom/android/server/pm/PackageManagerService;
    .param p2, "pkgSettings"    # Lcom/android/server/pm/Settings;
    .param p3, "context"    # Landroid/content/Context;

    .line 222
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 223
    iget-object v0, p1, Lcom/android/server/pm/PackageManagerService;->mInjector:Lcom/android/server/pm/PackageManagerServiceInjector;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceInjector;->getPackageDexOptimizer()Lcom/android/server/pm/PackageDexOptimizer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPdo:Lcom/android/server/pm/PackageDexOptimizer;

    .line 224
    iput-object p3, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 225
    iput-object p2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    .line 226
    new-instance v0, Lcom/android/server/pm/DexOptHelper;

    invoke-direct {v0, p1}, Lcom/android/server/pm/DexOptHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

    .line 227
    iget-object v0, p1, Lcom/android/server/pm/PackageManagerService;->mInjector:Lcom/android/server/pm/PackageManagerServiceInjector;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceInjector;->getDexManager()Lcom/android/server/pm/dex/DexManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexManager:Lcom/android/server/pm/dex/DexManager;

    .line 228
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    .line 229
    new-instance v0, Lcom/android/server/pm/MiuiDexopt;

    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexOptHelper:Lcom/android/server/pm/DexOptHelper;

    invoke-direct {v0, p1, v1, p3}, Lcom/android/server/pm/MiuiDexopt;-><init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/DexOptHelper;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiDexopt:Lcom/android/server/pm/MiuiDexopt;

    .line 230
    new-instance v0, Lcom/android/server/pm/PackageManagerCloudHelper;

    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/android/server/pm/PackageManagerCloudHelper;-><init>(Lcom/android/server/pm/PackageManagerService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPackageManagerCloudHelper:Lcom/android/server/pm/PackageManagerCloudHelper;

    .line 232
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 233
    const v1, 0x11030054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 232
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsSupportAttention:Ljava/util/List;

    .line 235
    const-string v0, "pm.dexopt.async.enabled"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    new-instance v0, Lcom/android/server/pm/DexoptServiceThread;

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v3, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPdo:Lcom/android/server/pm/PackageDexOptimizer;

    invoke-direct {v0, v2, v3}, Lcom/android/server/pm/DexoptServiceThread;-><init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageDexOptimizer;)V

    iput-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexoptServiceThread:Lcom/android/server/pm/DexoptServiceThread;

    .line 237
    invoke-virtual {v0}, Lcom/android/server/pm/DexoptServiceThread;->start()V

    .line 240
    :cond_0
    const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    new-instance v2, Lcom/android/server/pm/PackageEventRecorder;

    sget-object v3, Lcom/android/server/pm/PackageManagerServiceImpl;->PACKAGE_EVENT_DIR:Ljava/io/File;

    new-instance v4, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda2;

    invoke-direct {v4}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda2;-><init>()V

    invoke-direct {v2, v3, v4, v1}, Lcom/android/server/pm/PackageEventRecorder;-><init>(Ljava/io/File;Ljava/util/function/Function;Z)V

    invoke-static {v0, v2}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 243
    return-void
.end method

.method public initBeforeScanNonSystemApps()V
    .locals 0

    .line 321
    invoke-static {}, Lcom/android/server/pm/CloudControlPreinstallService;->startCloudControlService()V

    .line 322
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->updateDefaultPkgInstallerLocked()Z

    .line 323
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->checkGTSSpecAppOptMode()V

    .line 324
    return-void
.end method

.method initIPackageManagerImpl(Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;)V
    .locals 0
    .param p1, "pm"    # Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    .line 246
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPM:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    .line 247
    return-void
.end method

.method public initIgnoreApps()V
    .locals 9

    .line 371
    const-string/jumbo v0, "support_fm"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    const-string v1, "/system/app/FM.apk"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 373
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    const-string v1, "/system/app/FM"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 375
    :cond_0
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->readIgnoreApks()V

    .line 378
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnorePackages:Ljava/util/Set;

    const-string v1, "com.sogou.inputmethod.mi"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 380
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 381
    const v1, 0x1105003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 383
    .local v0, "isCmccCooperationDevice":Z
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 384
    const v2, 0x11050040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsGlobalCrbtSupported:Z

    .line 389
    sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lmiui/os/Build;->IS_CT_CUSTOMIZATION:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    .line 392
    :cond_1
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnorePackages:Ljava/util/Set;

    const-string v2, "com.miui.dmregservice"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 397
    :cond_2
    :try_start_0
    const-string v1, "ignoredAppsForPackages"

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnorePackages:Ljava/util/Set;

    invoke-static {v1, v2}, Lcom/android/server/pm/PackageManagerServiceImpl;->addIgnoreApks(Ljava/lang/String;Ljava/util/Set;)V

    .line 398
    const-string v1, "ignoredAppsForApkPath"

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    invoke-static {v1, v2}, Lcom/android/server/pm/PackageManagerServiceImpl;->addIgnoreApks(Ljava/lang/String;Ljava/util/Set;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    goto :goto_0

    .line 399
    :catch_0
    move-exception v1

    .line 400
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v2, "PKMSImpl"

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :goto_0
    invoke-static {}, Landroid/os/Environment;->getProductDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 405
    .local v1, "productPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/priv-app/MiuiHome"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 406
    .local v2, "MiuiHomePath":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/priv-app/MiLauncherGlobal"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 407
    .local v3, "MiLauncherGlobalPath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 408
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 409
    const-string v4, "POCO"

    sget-object v5, Lmiui/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 410
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 412
    :cond_3
    iget-object v4, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 417
    :cond_4
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/app/MITSMClientNoneNfc"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 418
    .local v4, "MITSMClientNoneNfcPath":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/app/MITSMClient"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 419
    .local v5, "MITSMClient":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    const-string v7, "android.hardware.nfc"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/android/server/pm/PackageManagerService;->hasSystemFeature(Ljava/lang/String;I)Z

    move-result v6

    .line 420
    .local v6, "isSupportNFC":Z
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_6

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 421
    if-eqz v6, :cond_5

    .line 422
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 424
    :cond_5
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    invoke-interface {v7, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 429
    :cond_6
    :goto_2
    sget-boolean v7, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-nez v7, :cond_8

    .line 430
    new-instance v7, Ljava/io/File;

    const-string v8, "/product/priv-app/GmsCore"

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 432
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnorePackages:Ljava/util/Set;

    const-string v8, "android.ext.shared"

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 433
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnorePackages:Ljava/util/Set;

    const-string v8, "com.android.printservice.recommendation"

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 436
    :cond_7
    iget-object v7, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnorePackages:Ljava/util/Set;

    const-string v8, "com.google.android.gsf"

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 440
    :cond_8
    :goto_3
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->initCotaApps()V

    .line 441
    return-void
.end method

.method public isBaselineDisabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1739
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiDexopt:Lcom/android/server/pm/MiuiDexopt;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiDexopt;->isBaselineDisabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method isCallerAllowedToSilentlyUninstall(I)Z
    .locals 9
    .param p1, "callingUid"    # I

    .line 723
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v0

    .line 724
    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/android/server/pm/Computer;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_1

    aget-object v5, v1, v4

    .line 725
    .local v5, "s":Ljava/lang/String;
    sget-object v6, Lcom/android/server/pm/PackageManagerServiceImpl;->sSilentlyUninstallPackages:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 726
    iget-object v6, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v6, v6, Lcom/android/server/pm/PackageManagerService;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v6, v5}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/pm/pkg/AndroidPackage;

    .line 727
    .local v6, "pkgSetting":Lcom/android/server/pm/pkg/AndroidPackage;
    if-eqz v6, :cond_0

    .line 728
    invoke-static {p1}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v7

    invoke-interface {v6}, Lcom/android/server/pm/pkg/AndroidPackage;->getUid()I

    move-result v8

    if-ne v7, v8, :cond_0

    .line 729
    const-string v1, "PKMSImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Allowed silently uninstall from callinguid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 724
    .end local v5    # "s":Ljava/lang/String;
    .end local v6    # "pkgSetting":Lcom/android/server/pm/pkg/AndroidPackage;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 734
    :cond_1
    monitor-exit v0

    .line 736
    return v3

    .line 734
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method isMiuiStubPackage(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 748
    const/4 v0, 0x0

    .line 749
    .local v0, "pkgSetting":Lcom/android/server/pm/PackageSetting;
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v1

    .line 750
    :try_start_0
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    invoke-virtual {v2, p1}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/pm/pkg/AndroidPackage;

    .line 751
    .local v2, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    if-eqz v2, :cond_0

    .line 752
    iget-object v3, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v3, v3, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v3, p1}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;

    move-result-object v3

    move-object v0, v3

    .line 754
    .end local v2    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 755
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/server/pm/PackageSetting;->getPkgState()Lcom/android/server/pm/pkg/PackageStateUnserialized;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/pm/pkg/PackageStateUnserialized;->isUpdatedSystemApp()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 756
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v2}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v2

    const-wide/32 v3, 0x200080

    invoke-interface {v2, p1, v3, v4, v1}, Lcom/android/server/pm/Computer;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 760
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v2, :cond_2

    iget-object v3, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v3, :cond_2

    .line 761
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 762
    .local v3, "meta":Landroid/os/Bundle;
    if-eqz v3, :cond_1

    const-string v4, "com.miui.stub.install"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 765
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "meta":Landroid/os/Bundle;
    :cond_2
    return v1

    .line 754
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method isPackageDeviceAdminEnabled()Z
    .locals 1

    .line 634
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    return v0
.end method

.method isPreinstallApp(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 334
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->isMiuiPreinstallApp(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 337
    :cond_0
    invoke-static {p1}, Lcom/android/server/pm/PreinstallApp;->isPreinstallApp(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method isVerificationEnabled(I)Z
    .locals 2
    .param p1, "installerUid"    # I

    .line 352
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isProvisioned()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 353
    return v1

    .line 356
    :cond_0
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->isCTS()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_2

    .line 357
    :cond_1
    return v1

    .line 359
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method public needSkipDomainVerifier(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1950
    sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1952
    const/4 v0, 0x1

    return v0

    .line 1954
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public performDexOptAsyncTask(Lcom/android/server/pm/dex/DexoptOptions;)V
    .locals 1
    .param p1, "options"    # Lcom/android/server/pm/dex/DexoptOptions;

    .line 1507
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexoptServiceThread:Lcom/android/server/pm/DexoptServiceThread;

    if-eqz v0, :cond_0

    .line 1508
    invoke-virtual {v0, p1}, Lcom/android/server/pm/DexoptServiceThread;->performDexOptAsyncTask(Lcom/android/server/pm/dex/DexoptOptions;)V

    .line 1510
    :cond_0
    return-void
.end method

.method public performDexOptSecondary(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V
    .locals 1
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "dexUseInfo"    # Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;
    .param p4, "options"    # Lcom/android/server/pm/dex/DexoptOptions;

    .line 1521
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDexoptServiceThread:Lcom/android/server/pm/DexoptServiceThread;

    if-eqz v0, :cond_0

    .line 1522
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/pm/DexoptServiceThread;->performDexOptSecondary(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V

    .line 1524
    :cond_0
    return-void
.end method

.method performPreinstallApp()V
    .locals 2

    .line 316
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    invoke-static {v0, v1}, Lcom/android/server/pm/PreinstallApp;->copyPreinstallApps(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/Settings;)V

    .line 317
    return-void
.end method

.method public preCheckUidPermission(Ljava/lang/String;I)I
    .locals 4
    .param p1, "permName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 895
    invoke-static {p2}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x7d0

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sShellCheckPermissions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 896
    const-string v0, "persist.security.adbinput"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 897
    const-string v0, "android.permission.CALL_PHONE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "PKMSImpl"

    if-eqz v0, :cond_0

    const-class v0, Landroid/app/ActivityManagerInternal;

    .line 899
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManagerInternal;

    invoke-virtual {v0}, Landroid/app/ActivityManagerInternal;->getCurrentUserId()I

    move-result v0

    const/16 v3, 0x6e

    if-ne v3, v0, :cond_0

    .line 900
    const-string v0, "preCheckUidPermission: permission granted call phone"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    return v2

    .line 903
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preCheckUidPermission: permission\u3000denied, perm="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    const/4 v0, -0x1

    return v0

    .line 906
    :cond_1
    return v2
.end method

.method public processFirstUseActivity(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1473
    new-instance v0, Lcom/android/server/pm/PackageManagerServiceImpl$3;

    invoke-direct {v0, p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl$3;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Ljava/lang/String;)V

    .line 1479
    .local v0, "task":Ljava/lang/Runnable;
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->getFirstUseHandler()Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x2328

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1480
    return-void
.end method

.method protectAppFromDeleting(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver2;III)Z
    .locals 18
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "observer"    # Landroid/content/pm/IPackageDeleteObserver2;
    .param p3, "callingUid"    # I
    .param p4, "userId"    # I
    .param p5, "deleteFlags"    # I

    .line 777
    move-object/from16 v1, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move/from16 v10, p3

    move/from16 v11, p4

    move/from16 v12, p5

    iget-object v0, v1, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v0

    invoke-interface {v0, v8}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v13

    .line 778
    .local v13, "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
    if-eqz v13, :cond_0

    invoke-interface {v13, v11}, Lcom/android/server/pm/pkg/PackageStateInternal;->getUserStateOrDefault(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/pm/pkg/PackageUserStateInternal;->isInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 779
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    .line 780
    .local v0, "callingPid":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uninstall pkg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " u"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " flags:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from u"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 784
    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 785
    .local v2, "msg":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceUtilsStub;->get()Lcom/android/server/pm/PackageManagerServiceUtilsStub;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v2}, Lcom/android/server/pm/PackageManagerServiceUtilsStub;->logMiuiCriticalInfo(ILjava/lang/String;)V

    .line 789
    .end local v0    # "callingPid":I
    .end local v2    # "msg":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->isMiuiStubPackage(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_6

    iget-object v0, v1, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 790
    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v0

    invoke-interface {v0, v10}, Lcom/android/server/pm/Computer;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.android.managedprovisioning"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 791
    and-int/lit8 v0, v12, 0x4

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 792
    .local v0, "deleteSystem":Z
    :goto_0
    and-int/lit8 v3, v12, 0x2

    const/4 v4, -0x1

    if-eqz v3, :cond_2

    .line 793
    move v3, v4

    goto :goto_1

    :cond_2
    move v3, v11

    :goto_1
    move v15, v3

    .line 796
    .local v15, "removeUser":I
    if-eq v15, v4, :cond_4

    if-nez v15, :cond_3

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v3, 0x1

    :goto_3
    move/from16 v16, v3

    .line 799
    .local v16, "fullRemove":Z
    if-eqz v0, :cond_5

    if-eqz v16, :cond_6

    .line 800
    :cond_5
    iget-object v2, v1, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v7, v2, Lcom/android/server/pm/PackageManagerService;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda6;

    move-object v2, v6

    move-object/from16 v3, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move-object v14, v6

    move v6, v0

    move/from16 v17, v0

    move-object v0, v7

    .end local v0    # "deleteSystem":Z
    .local v17, "deleteSystem":Z
    move-object/from16 v7, p2

    invoke-direct/range {v2 .. v7}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda6;-><init>(Ljava/lang/String;IIZLandroid/content/pm/IPackageDeleteObserver2;)V

    invoke-virtual {v0, v14}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 810
    const/4 v2, 0x1

    return v2

    .line 815
    .end local v15    # "removeUser":I
    .end local v16    # "fullRemove":Z
    .end local v17    # "deleteSystem":Z
    :cond_6
    iget-object v0, v1, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 816
    invoke-static/range {p3 .. p3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 815
    invoke-static {v0, v8, v3}, Lcom/miui/enterprise/ApplicationHelper;->protectedFromDelete(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    const-string v3, "PKMSImpl"

    if-nez v0, :cond_c

    .line 817
    invoke-static {}, Lmiui/enterprise/ApplicationHelperStub;->getInstance()Lmiui/enterprise/IApplicationHelper;

    move-result-object v0

    invoke-interface {v0, v8}, Lmiui/enterprise/IApplicationHelper;->isPreventUninstallation(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    goto/16 :goto_7

    .line 833
    :cond_7
    if-eqz v13, :cond_b

    invoke-interface {v13}, Lcom/android/server/pm/pkg/PackageStateInternal;->isSystem()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, v1, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 834
    invoke-static {v0, v8, v2}, Lmiui/content/pm/PreloadedAppPolicy;->isProtectedDataApp(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 835
    invoke-static/range {p3 .. p3}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v4

    .line 836
    .local v4, "appId":I
    if-eqz v4, :cond_b

    const/16 v0, 0x3e8

    if-eq v4, v0, :cond_b

    .line 837
    const/4 v0, 0x1

    .line 838
    .local v0, "isReject":Z
    invoke-static {}, Lmiui/content/pm/PreloadedAppPolicy;->getAllowDeleteSourceApps()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 839
    .local v6, "allowPkg":Ljava/lang/String;
    iget-object v7, v1, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v7}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v7

    const-wide/16 v14, 0x0

    invoke-interface {v7, v6, v14, v15, v2}, Lcom/android/server/pm/Computer;->getPackageUid(Ljava/lang/String;JI)I

    move-result v7

    if-ne v4, v7, :cond_8

    .line 840
    const/4 v0, 0x0

    .line 841
    move v5, v0

    goto :goto_5

    .line 843
    .end local v6    # "allowPkg":Ljava/lang/String;
    :cond_8
    goto :goto_4

    .line 838
    :cond_9
    move v5, v0

    .line 844
    .end local v0    # "isReject":Z
    .local v5, "isReject":Z
    :goto_5
    if-eqz v5, :cond_b

    .line 846
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MIUILOG- can\'t uninstall pkg : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " callingUid : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    if-eqz v9, :cond_a

    instance-of v0, v9, Landroid/content/pm/IPackageDeleteObserver2;

    if-eqz v0, :cond_a

    .line 848
    const/16 v0, -0x3e8

    const/4 v2, 0x0

    invoke-interface {v9, v8, v0, v2}, Landroid/content/pm/IPackageDeleteObserver2;->onPackageDeleted(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 853
    :cond_a
    goto :goto_6

    .line 851
    :catch_0
    move-exception v0

    .line 854
    :goto_6
    const/4 v2, 0x1

    return v2

    .line 859
    .end local v4    # "appId":I
    .end local v5    # "isReject":Z
    :cond_b
    return v2

    .line 819
    :cond_c
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device is in enterprise mode, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " uninstallation is restricted by enterprise!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    iget-object v0, v1, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda7;

    invoke-direct {v2, v9, v8}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda7;-><init>(Landroid/content/pm/IPackageDeleteObserver2;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 828
    const/4 v2, 0x1

    return v2
.end method

.method public recordPackageActivate(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "activatedPkg"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "sourcePkg"    # Ljava/lang/String;

    .line 1795
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    .line 1796
    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/server/pm/Computer;->getPackageStateInternal(Ljava/lang/String;)Lcom/android/server/pm/pkg/PackageStateInternal;

    move-result-object v0

    .line 1795
    invoke-static {p1, p3, p2, v0}, Lcom/android/server/pm/PackageEventRecorder;->shouldRecordPackageActivate(Ljava/lang/String;Ljava/lang/String;ILcom/android/server/pm/pkg/PackageStateInternal;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1797
    return-void

    .line 1800
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda3;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1803
    return-void
.end method

.method public recordPackageRemove([ILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V
    .locals 7
    .param p1, "userIds"    # [I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "installer"    # Ljava/lang/String;
    .param p4, "isRemovedFully"    # Z
    .param p5, "extras"    # Landroid/os/Bundle;

    .line 1790
    const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/android/server/pm/PackageEventRecorderInternal;

    .line 1791
    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/android/server/pm/PackageEventRecorderInternal;->recordPackageRemove([ILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 1792
    return-void
.end method

.method public recordPackageUpdate([ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "userIds"    # [I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "installer"    # Ljava/lang/String;
    .param p4, "extras"    # Landroid/os/Bundle;

    .line 1784
    const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal;

    .line 1785
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/server/pm/PackageEventRecorderInternal;->recordPackageUpdate([ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1786
    return-void
.end method

.method removePackageFromSharedUser(Lcom/android/server/pm/PackageSetting;)V
    .locals 1
    .param p1, "ps"    # Lcom/android/server/pm/PackageSetting;

    .line 1653
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v0, p1}, Lcom/android/server/pm/Settings;->getSharedUserSettingLPr(Lcom/android/server/pm/PackageSetting;)Lcom/android/server/pm/SharedUserSetting;

    move-result-object v0

    .line 1654
    .local v0, "sharedUserSetting":Lcom/android/server/pm/SharedUserSetting;
    if-eqz v0, :cond_0

    .line 1655
    invoke-virtual {v0, p1}, Lcom/android/server/pm/SharedUserSetting;->removePackage(Lcom/android/server/pm/PackageSetting;)Z

    .line 1657
    :cond_0
    return-void
.end method

.method public setBaselineDisabled(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "disabled"    # Z

    .line 1734
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mMiuiDexopt:Lcom/android/server/pm/MiuiDexopt;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/MiuiDexopt;->setBaselineDisabled(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method setCallingPackage(Lcom/android/server/pm/PackageInstallerSession;Ljava/lang/String;)V
    .locals 2
    .param p1, "session"    # Lcom/android/server/pm/PackageInstallerSession;
    .param p2, "callingPackageName"    # Ljava/lang/String;

    .line 1414
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1415
    invoke-virtual {p1, p2}, Lcom/android/server/pm/PackageInstallerSession;->setCallingPackage(Ljava/lang/String;)V

    .line 1416
    return-void

    .line 1418
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 1419
    .local v0, "realPkg":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/android/server/pm/PackageInstallerSession;->getInstallerPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    invoke-virtual {p1, v1}, Lcom/android/server/pm/PackageInstallerSession;->setCallingPackage(Ljava/lang/String;)V

    .line 1420
    return-void
.end method

.method public setDomainVerificationService(Lcom/android/server/pm/verify/domain/DomainVerificationService;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/pm/verify/domain/DomainVerificationService;

    .line 1824
    iput-object p1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mDomainVerificationService:Lcom/android/server/pm/verify/domain/DomainVerificationService;

    .line 1825
    return-void
.end method

.method shouldIgnoreApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "codePath"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;

    .line 522
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnorePackages:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIgnoreApks:Ljava/util/Set;

    .line 523
    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 524
    .local v0, "ignored":Z
    :goto_1
    if-eqz v0, :cond_2

    .line 525
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Skip scanning package: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PKMSImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mUserManager:Lcom/android/server/pm/UserManagerService;

    invoke-virtual {v1}, Lcom/android/server/pm/UserManagerService;->getUserIds()[I

    move-result-object v1

    .line 528
    .local v1, "userIds":[I
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    invoke-virtual {v2, p1}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;

    move-result-object v8

    .line 531
    .local v8, "ps":Lcom/android/server/pm/PackageSetting;
    if-eqz v8, :cond_2

    .line 532
    invoke-virtual {v8}, Lcom/android/server/pm/PackageSetting;->getPath()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v8}, Lcom/android/server/pm/PackageSetting;->getPath()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 533
    new-instance v2, Lcom/android/server/pm/RemovePackageHelper;

    iget-object v3, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    invoke-direct {v2, v3}, Lcom/android/server/pm/RemovePackageHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V

    .line 534
    .local v2, "removePackageHelper":Lcom/android/server/pm/RemovePackageHelper;
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, v8

    move-object v4, v1

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/pm/RemovePackageHelper;->removePackageDataLIF(Lcom/android/server/pm/PackageSetting;[ILcom/android/server/pm/PackageRemovedInfo;IZ)V

    .line 537
    .end local v1    # "userIds":[I
    .end local v2    # "removePackageHelper":Lcom/android/server/pm/RemovePackageHelper;
    .end local v8    # "ps":Lcom/android/server/pm/PackageSetting;
    :cond_2
    return v0
.end method

.method public shouldSkipInstallForNewUser(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 1871
    const/4 v0, 0x0

    if-nez p2, :cond_0

    return v0

    .line 1872
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldSkipInstall(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldSkipInstallCotaApp(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public shouldSkipInstallForUserType(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "userType"    # Ljava/lang/String;

    .line 1877
    const-string v0, "android.os.usertype.full.SYSTEM"

    const/4 v1, 0x0

    if-ne p2, v0, :cond_0

    return v1

    .line 1878
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldSkipInstall(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldSkipInstallCotaApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public switchPackageInstaller()V
    .locals 9

    .line 1297
    :try_start_0
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->isCTS()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1298
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    iget-object v0, v0, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    const-string v1, "com.google.android.packageinstaller"

    invoke-virtual {v0, v1}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/PackageSetting;

    .line 1299
    .local v0, "googleInstaller":Lcom/android/server/pm/PackageSetting;
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPkgSettings:Lcom/android/server/pm/Settings;

    iget-object v1, v1, Lcom/android/server/pm/Settings;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    const-string v2, "com.android.packageinstaller"

    invoke-virtual {v1, v2}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/PackageSetting;

    .line 1300
    .local v1, "androidInstaller":Lcom/android/server/pm/PackageSetting;
    const/4 v2, 0x0

    .line 1301
    .local v2, "ctsInstallerPackageName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1302
    const-string v3, "com.android.packageinstaller"

    move-object v2, v3

    move-object v8, v2

    goto :goto_0

    .line 1303
    :cond_0
    if-eqz v0, :cond_1

    .line 1304
    const-string v3, "com.google.android.packageinstaller"

    move-object v2, v3

    move-object v8, v2

    goto :goto_0

    .line 1303
    :cond_1
    move-object v8, v2

    .line 1306
    .end local v2    # "ctsInstallerPackageName":Ljava/lang/String;
    .local v8, "ctsInstallerPackageName":Ljava/lang/String;
    :goto_0
    if-eqz v8, :cond_2

    .line 1307
    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPM:Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;

    const/4 v4, 0x0

    const/16 v5, 0x4000

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v3, v8

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->installExistingPackageAsUser(Ljava/lang/String;IIILjava/util/List;)I

    .line 1310
    .end local v0    # "googleInstaller":Lcom/android/server/pm/PackageSetting;
    .end local v1    # "androidInstaller":Lcom/android/server/pm/PackageSetting;
    .end local v8    # "ctsInstallerPackageName":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v0, v0, Lcom/android/server/pm/PackageManagerService;->mLock:Lcom/android/server/pm/PackageManagerTracedLock;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1311
    :try_start_1
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->updateDefaultPkgInstallerLocked()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1312
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    iput-object v2, v1, Lcom/android/server/pm/PackageManagerService;->mRequiredInstallerPackage:Ljava/lang/String;

    .line 1313
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    iput-object v2, v1, Lcom/android/server/pm/PackageManagerService;->mRequiredUninstallerPackage:Ljava/lang/String;

    .line 1314
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v1, v1, Lcom/android/server/pm/PackageManagerService;->mPackages:Lcom/android/server/utils/WatchedArrayMap;

    iget-object v2, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v2, v2, Lcom/android/server/pm/PackageManagerService;->mRequiredInstallerPackage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/pkg/AndroidPackage;

    .line 1315
    .local v1, "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    const-string v2, "permissionmgr"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    check-cast v2, Lcom/android/server/pm/permission/PermissionManagerService;

    .line 1316
    .local v2, "pms":Lcom/android/server/pm/permission/PermissionManagerService;
    const-string v3, "mPermissionManagerServiceImpl"

    const-class v4, Lcom/android/server/pm/permission/PermissionManagerServiceImpl;

    invoke-static {v2, v3, v4}, Lmiui/util/ReflectionUtils;->getObjectField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/pm/permission/PermissionManagerServiceImpl;

    .line 1318
    .local v3, "impl":Lcom/android/server/pm/permission/PermissionManagerServiceImpl;
    const-string/jumbo v4, "updatePermissions"

    const-class v5, Ljava/lang/Void;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mCurrentPackageInstaller:Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v7, v6, v8

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-static {v3, v4, v5, v6}, Lmiui/util/ReflectionUtils;->callMethod(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320
    .end local v1    # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
    .end local v2    # "pms":Lcom/android/server/pm/permission/PermissionManagerService;
    .end local v3    # "impl":Lcom/android/server/pm/permission/PermissionManagerServiceImpl;
    :cond_3
    monitor-exit v0

    .line 1323
    goto :goto_1

    .line 1320
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local p0    # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1321
    .restart local p0    # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
    :catch_0
    move-exception v0

    .line 1322
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PKMSImpl"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1325
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method

.method updateSystemAppDefaultStateForAllUsers()V
    .locals 6

    .line 1637
    invoke-static {}, Lcom/android/server/SystemConfig;->getInstance()Lcom/android/server/SystemConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/SystemConfig;->getPackageDefaultState()Landroid/util/ArrayMap;

    move-result-object v0

    .line 1638
    .local v0, "defaultPkgState":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-virtual {v0}, Landroid/util/ArrayMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1639
    return-void

    .line 1641
    :cond_0
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->doUpdateSystemAppDefaultUserStateForAllUsers()V

    .line 1642
    iget-object v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1643
    const-string v2, "miui_optimization"

    invoke-static {v2}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Lcom/android/server/pm/PackageManagerServiceImpl$4;

    iget-object v4, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mPms:Lcom/android/server/pm/PackageManagerService;

    iget-object v4, v4, Lcom/android/server/pm/PackageManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v3, p0, v4}, Lcom/android/server/pm/PackageManagerServiceImpl$4;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Landroid/os/Handler;)V

    .line 1642
    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 1649
    return-void
.end method

.method updateSystemAppDefaultStateForUser(I)V
    .locals 6
    .param p1, "userId"    # I

    .line 1611
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceImpl;->isCTS()Z

    move-result v0

    .line 1612
    .local v0, "isCTS":Z
    invoke-static {}, Lcom/android/server/SystemConfig;->getInstance()Lcom/android/server/SystemConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/SystemConfig;->getPackageDefaultState()Landroid/util/ArrayMap;

    move-result-object v1

    .line 1614
    .local v1, "defaultPkgState":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-virtual {v1}, Landroid/util/ArrayMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1615
    return-void

    .line 1617
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v3

    .local v3, "size":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 1618
    invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1619
    .local v4, "pkg":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    .line 1620
    .local v5, "disableByDefault":Z
    if-nez v5, :cond_1

    .line 1621
    goto :goto_1

    .line 1623
    :cond_1
    invoke-direct {p0, p1, v0, v4}, Lcom/android/server/pm/PackageManagerServiceImpl;->updateSystemAppState(IZLjava/lang/String;)V

    .line 1617
    .end local v4    # "pkg":Ljava/lang/String;
    .end local v5    # "disableByDefault":Z
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1625
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_2
    return-void
.end method

.method public useMiuiDefaultCrossProfileIntentFilter()Z
    .locals 2

    .line 1842
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->hasGoogleContacts()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->hasXiaomiContacts()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843
    return v1

    .line 1846
    :cond_0
    invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->hasXiaomiContacts()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1847
    const/4 v0, 0x1

    return v0

    .line 1850
    :cond_1
    return v1
.end method
