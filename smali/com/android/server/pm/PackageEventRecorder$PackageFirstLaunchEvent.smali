.class Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;
.super Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
.source "PackageEventRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageEventRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PackageFirstLaunchEvent"
.end annotation


# direct methods
.method private constructor <init>(JLjava/lang/String;[ILjava/lang/String;)V
    .locals 7
    .param p1, "eventTime"    # J
    .param p3, "pkgName"    # Ljava/lang/String;
    .param p4, "userIds"    # [I
    .param p5, "installer"    # Ljava/lang/String;

    .line 600
    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;->buildPackageEventId(JI)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;-><init>(Ljava/lang/String;Ljava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageEventBase-IA;)V

    .line 601
    return-void
.end method

.method synthetic constructor <init>(JLjava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/server/pm/PackageEventRecorder$PackageFirstLaunchEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;)V

    return-void
.end method
