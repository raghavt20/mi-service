public class com.android.server.pm.PmInjector$InstallObserver extends android.os.IMessenger$Stub {
	 /* .source "PmInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/PmInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "InstallObserver" */
} // .end annotation
/* # instance fields */
Boolean finished;
java.lang.String msg;
Integer result;
/* # direct methods */
public com.android.server.pm.PmInjector$InstallObserver ( ) {
/* .locals 1 */
/* .line 123 */
/* invoke-direct {p0}, Landroid/os/IMessenger$Stub;-><init>()V */
/* .line 125 */
/* const/16 v0, 0xa */
/* iput v0, p0, Lcom/android/server/pm/PmInjector$InstallObserver;->result:I */
return;
} // .end method
/* # virtual methods */
public void send ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "message" # Landroid/os/Message; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 130 */
/* monitor-enter p0 */
/* .line 131 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* iput-boolean v0, p0, Lcom/android/server/pm/PmInjector$InstallObserver;->finished:Z */
/* .line 132 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* iput v0, p0, Lcom/android/server/pm/PmInjector$InstallObserver;->result:I */
/* .line 133 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 134 */
/* .local v0, "data":Landroid/os/Bundle; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 135 */
	 final String v1 = "msg"; // const-string v1, "msg"
	 (( android.os.Bundle ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
	 this.msg = v1;
	 /* .line 137 */
} // :cond_0
(( java.lang.Object ) p0 ).notifyAll ( ); // invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
/* .line 138 */
} // .end local v0 # "data":Landroid/os/Bundle;
/* monitor-exit p0 */
/* .line 139 */
return;
/* .line 138 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
