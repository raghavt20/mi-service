public class com.android.server.pm.PackageManagerServiceUtilsImpl extends com.android.server.pm.PackageManagerServiceUtilsStub {
	 /* .source "PackageManagerServiceUtilsImpl.java" */
	 /* # static fields */
	 private static final Long MAX_CRITICAL_INFO_DUMP_SIZE;
	 /* # direct methods */
	 public com.android.server.pm.PackageManagerServiceUtilsImpl ( ) {
		 /* .locals 0 */
		 /* .line 35 */
		 /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceUtilsStub;-><init>()V */
		 return;
	 } // .end method
	 private java.io.File getBackupMiuiPackageProblemFile ( ) {
		 /* .locals 4 */
		 /* .line 180 */
		 android.os.Environment .getDataDirectory ( );
		 /* .line 181 */
		 /* .local v0, "dataDir":Ljava/io/File; */
		 /* new-instance v1, Ljava/io/File; */
		 /* const-string/jumbo v2, "system" */
		 /* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 /* .line 182 */
		 /* .local v1, "systemDir":Ljava/io/File; */
		 /* new-instance v2, Ljava/io/File; */
		 final String v3 = "pkg_history_backup.txt"; // const-string v3, "pkg_history_backup.txt"
		 /* invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 /* .line 183 */
		 /* .local v2, "fname":Ljava/io/File; */
	 } // .end method
	 private java.io.File getBackupSettingsProblemFile ( ) {
		 /* .locals 4 */
		 /* .line 160 */
		 android.os.Environment .getDataDirectory ( );
		 /* .line 161 */
		 /* .local v0, "dataDir":Ljava/io/File; */
		 /* new-instance v1, Ljava/io/File; */
		 /* const-string/jumbo v2, "system" */
		 /* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 /* .line 162 */
		 /* .local v1, "systemDir":Ljava/io/File; */
		 /* new-instance v2, Ljava/io/File; */
		 /* const-string/jumbo v3, "uiderrors_backup.txt" */
		 /* invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 /* .line 163 */
		 /* .local v2, "fname":Ljava/io/File; */
	 } // .end method
	 private java.io.File getMiuiPackageProblemFile ( ) {
		 /* .locals 4 */
		 /* .line 167 */
		 android.os.Environment .getDataDirectory ( );
		 /* .line 168 */
		 /* .local v0, "dataDir":Ljava/io/File; */
		 /* new-instance v1, Ljava/io/File; */
		 /* const-string/jumbo v2, "system" */
		 /* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 /* .line 169 */
		 /* .local v1, "systemDir":Ljava/io/File; */
		 /* new-instance v2, Ljava/io/File; */
		 final String v3 = "pkg_history.txt"; // const-string v3, "pkg_history.txt"
		 /* invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
		 /* .line 170 */
		 /* .local v2, "fname":Ljava/io/File; */
		 v3 = 		 (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
		 /* if-nez v3, :cond_0 */
		 /* .line 172 */
		 try { // :try_start_0
			 (( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
			 /* :try_end_0 */
			 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 174 */
			 /* .line 173 */
			 /* :catch_0 */
			 /* move-exception v3 */
			 /* .line 176 */
		 } // :cond_0
	 } // :goto_0
} // .end method
private java.io.File getSettingsProblemFile ( ) {
	 /* .locals 4 */
	 /* .line 153 */
	 android.os.Environment .getDataDirectory ( );
	 /* .line 154 */
	 /* .local v0, "dataDir":Ljava/io/File; */
	 /* new-instance v1, Ljava/io/File; */
	 /* const-string/jumbo v2, "system" */
	 /* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
	 /* .line 155 */
	 /* .local v1, "systemDir":Ljava/io/File; */
	 /* new-instance v2, Ljava/io/File; */
	 /* const-string/jumbo v3, "uiderrors.txt" */
	 /* invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
	 /* .line 156 */
	 /* .local v2, "fname":Ljava/io/File; */
} // .end method
private void truncateSettingsProblemFileIfNeeded ( java.io.File p0, java.io.File p1 ) {
	 /* .locals 8 */
	 /* .param p1, "srcFile" # Ljava/io/File; */
	 /* .param p2, "backupFile" # Ljava/io/File; */
	 /* .line 121 */
	 /* const/high16 v0, 0x80000 */
	 /* .line 123 */
	 /* .local v0, "MAX_LOG_SIZE":I */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 124 */
	 /* .local v1, "srcPfd":Landroid/os/ParcelFileDescriptor; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 126 */
	 /* .local v2, "backupPfd":Landroid/os/ParcelFileDescriptor; */
	 try { // :try_start_0
		 v3 = 		 (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
		 if ( v3 != null) { // if-eqz v3, :cond_3
			 (( java.io.File ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/io/File;->length()J
			 /* move-result-wide v3 */
			 /* const-wide/32 v5, 0x80000 */
			 /* cmp-long v3, v3, v5 */
			 /* if-gez v3, :cond_0 */
			 /* .line 130 */
		 } // :cond_0
		 /* const/high16 v3, 0x10000000 */
		 android.os.ParcelFileDescriptor .open ( p1,v3 );
		 /* move-object v1, v3 */
		 /* .line 132 */
		 /* const/high16 v3, 0x38000000 */
		 android.os.ParcelFileDescriptor .open ( p2,v3 );
		 /* move-object v2, v3 */
		 /* .line 135 */
		 if ( v1 != null) { // if-eqz v1, :cond_2
			 /* if-nez v2, :cond_1 */
			 /* .line 136 */
		 } // :cond_1
		 (( android.os.ParcelFileDescriptor ) v1 ).getFileDescriptor ( ); // invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
		 (( java.io.File ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/io/File;->length()J
		 /* move-result-wide v4 */
		 /* const-wide/16 v6, 0x2 */
		 /* div-long/2addr v4, v6 */
		 android.system.Os .lseek ( v3,v4,v5,v6 );
		 /* .line 137 */
		 (( android.os.ParcelFileDescriptor ) v1 ).getFileDescriptor ( ); // invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
		 (( android.os.ParcelFileDescriptor ) v2 ).getFileDescriptor ( ); // invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
		 android.os.FileUtils .copy ( v3,v4 );
		 /* .line 138 */
		 (( java.io.File ) p2 ).renameTo ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
		 /* .line 140 */
		 /* nop */
		 /* .line 141 */
		 (( java.io.File ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;
		 /* .line 140 */
		 int v4 = -1; // const/4 v4, -0x1
		 /* const/16 v5, 0x1fc */
		 android.os.FileUtils .setPermissions ( v3,v5,v4,v4 );
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 147 */
	 } // :cond_2
} // :goto_0
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 148 */
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 135 */
return;
/* .line 147 */
} // :cond_3
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 148 */
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 127 */
return;
/* .line 147 */
/* :catchall_0 */
/* move-exception v3 */
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 148 */
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 149 */
/* throw v3 */
/* .line 144 */
/* :catch_0 */
/* move-exception v3 */
/* .line 147 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 148 */
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 149 */
/* nop */
/* .line 150 */
return;
} // .end method
/* # virtual methods */
public void dumpCriticalInfo ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "msg" # Ljava/lang/String; */
/* .line 66 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->getMiuiPackageProblemFile()Ljava/io/File; */
/* .line 67 */
/* .local v0, "file":Ljava/io/File; */
(( java.io.File ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/io/File;->length()J
/* move-result-wide v1 */
/* const-wide/32 v3, 0x2dc6c0 */
/* sub-long/2addr v1, v3 */
/* .line 68 */
/* .local v1, "skipSize":J */
try { // :try_start_0
/* new-instance v3, Ljava/io/BufferedReader; */
/* new-instance v4, Ljava/io/FileReader; */
/* invoke-direct {v4, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 69 */
/* .local v3, "in":Ljava/io/BufferedReader; */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v1, v4 */
/* if-lez v4, :cond_0 */
/* .line 70 */
try { // :try_start_1
(( java.io.BufferedReader ) v3 ).skip ( v1, v2 ); // invoke-virtual {v3, v1, v2}, Ljava/io/BufferedReader;->skip(J)J
/* .line 68 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 72 */
} // :cond_0
} // :goto_0
int v4 = 0; // const/4 v4, 0x0
/* .line 73 */
/* .local v4, "line":Ljava/lang/String; */
} // :goto_1
(( java.io.BufferedReader ) v3 ).readLine ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v5 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 74 */
final String v5 = "ignored: updated version"; // const-string v5, "ignored: updated version"
v5 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 75 */
} // :cond_1
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 76 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 78 */
} // :cond_2
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 80 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_3
try { // :try_start_2
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 81 */
} // .end local v3 # "in":Ljava/io/BufferedReader;
/* .line 68 */
/* .restart local v3 # "in":Ljava/io/BufferedReader; */
} // :goto_2
try { // :try_start_3
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_4
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // .end local v1 # "skipSize":J
} // .end local p0 # "this":Lcom/android/server/pm/PackageManagerServiceUtilsImpl;
} // .end local p1 # "pw":Ljava/io/PrintWriter;
} // .end local p2 # "msg":Ljava/lang/String;
} // :goto_3
/* throw v4 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 80 */
} // .end local v3 # "in":Ljava/io/BufferedReader;
/* .restart local v0 # "file":Ljava/io/File; */
/* .restart local v1 # "skipSize":J */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageManagerServiceUtilsImpl; */
/* .restart local p1 # "pw":Ljava/io/PrintWriter; */
/* .restart local p2 # "msg":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v3 */
/* .line 82 */
} // :goto_4
return;
} // .end method
public void logDisableComponent ( android.content.pm.PackageManager$ComponentEnabledSetting p0, Boolean p1, java.lang.String p2, Integer p3, Integer p4 ) {
/* .locals 3 */
/* .param p1, "setting" # Landroid/content/pm/PackageManager$ComponentEnabledSetting; */
/* .param p2, "isSystem" # Z */
/* .param p3, "callingPackage" # Ljava/lang/String; */
/* .param p4, "callingUid" # I */
/* .param p5, "callingPid" # I */
/* .line 90 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set app" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( android.content.pm.PackageManager$ComponentEnabledSetting ) p1 ).isComponent ( ); // invoke-virtual {p1}, Landroid/content/pm/PackageManager$ComponentEnabledSetting;->isComponent()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 91 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " component="; // const-string v2, " component="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.pm.PackageManager$ComponentEnabledSetting ) p1 ).getComponentName ( ); // invoke-virtual {p1}, Landroid/content/pm/PackageManager$ComponentEnabledSetting;->getComponentName()Landroid/content/ComponentName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 92 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " package: "; // const-string v2, " package: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.pm.PackageManager$ComponentEnabledSetting ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Landroid/content/pm/PackageManager$ComponentEnabledSetting;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " enabled state: "; // const-string v1, " enabled state: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 93 */
v1 = (( android.content.pm.PackageManager$ComponentEnabledSetting ) p1 ).getEnabledState ( ); // invoke-virtual {p1}, Landroid/content/pm/PackageManager$ComponentEnabledSetting;->getEnabledState()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " from "; // const-string v1, " from "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ",uid="; // const-string v1, ",uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ",pid="; // const-string v1, ",pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 97 */
/* .local v0, "msg":Ljava/lang/String; */
v1 = (( android.content.pm.PackageManager$ComponentEnabledSetting ) p1 ).isComponent ( ); // invoke-virtual {p1}, Landroid/content/pm/PackageManager$ComponentEnabledSetting;->isComponent()Z
/* if-nez v1, :cond_1 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 99 */
v1 = (( android.content.pm.PackageManager$ComponentEnabledSetting ) p1 ).getEnabledState ( ); // invoke-virtual {p1}, Landroid/content/pm/PackageManager$ComponentEnabledSetting;->getEnabledState()I
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_1 */
/* .line 100 */
(( com.android.server.pm.PackageManagerServiceUtilsImpl ) p0 ).logMiuiCriticalInfo ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->logMiuiCriticalInfo(ILjava/lang/String;)V
/* .line 102 */
} // :cond_1
final String v1 = "PackageManager"; // const-string v1, "PackageManager"
android.util.Slog .d ( v1,v0 );
/* .line 104 */
} // :goto_1
return;
} // .end method
public void logMiuiCriticalInfo ( Integer p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "priority" # I */
/* .param p2, "msg" # Ljava/lang/String; */
/* .line 46 */
final String v0 = "PackageManager"; // const-string v0, "PackageManager"
android.util.Slog .println ( p1,v0,p2 );
/* .line 47 */
com.android.server.EventLogTags .writePmCriticalInfo ( p2 );
/* .line 49 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->getMiuiPackageProblemFile()Ljava/io/File; */
/* .line 50 */
/* .local v0, "fname":Ljava/io/File; */
/* new-instance v1, Ljava/io/FileOutputStream; */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v1, v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* .line 51 */
/* .local v1, "out":Ljava/io/FileOutputStream; */
/* new-instance v2, Lcom/android/internal/util/FastPrintWriter; */
/* invoke-direct {v2, v1}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/OutputStream;)V */
/* .line 52 */
/* .local v2, "pw":Ljava/io/PrintWriter; */
/* new-instance v3, Ljava/text/SimpleDateFormat; */
/* invoke-direct {v3}, Ljava/text/SimpleDateFormat;-><init>()V */
/* .line 53 */
/* .local v3, "formatter":Ljava/text/SimpleDateFormat; */
/* new-instance v4, Ljava/util/Date; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v3 ).format ( v4 ); // invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 54 */
/* .local v4, "dateString":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ": "; // const-string v6, ": "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v2 ).println ( v5 ); // invoke-virtual {v2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 55 */
(( java.io.PrintWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
/* .line 56 */
/* nop */
/* .line 57 */
(( java.io.File ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;
/* .line 56 */
int v6 = -1; // const/4 v6, -0x1
/* const/16 v7, 0x1fc */
android.os.FileUtils .setPermissions ( v5,v7,v6,v6 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 61 */
/* nop */
} // .end local v0 # "fname":Ljava/io/File;
} // .end local v1 # "out":Ljava/io/FileOutputStream;
} // .end local v2 # "pw":Ljava/io/PrintWriter;
} // .end local v3 # "formatter":Ljava/text/SimpleDateFormat;
} // .end local v4 # "dateString":Ljava/lang/String;
/* .line 60 */
/* :catch_0 */
/* move-exception v0 */
/* .line 62 */
} // :goto_0
return;
} // .end method
public void logSuspendApp ( java.lang.String p0, com.android.server.pm.Computer p1, Boolean p2, java.lang.String p3, Integer p4 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "snapshot" # Lcom/android/server/pm/Computer; */
/* .param p3, "suspended" # Z */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .param p5, "callingUid" # I */
/* .line 112 */
/* .line 113 */
/* .local v0, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
if ( v1 != null) { // if-eqz v1, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 114 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set package: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " suspended from "; // const-string v2, " suspended from "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ",uid="; // const-string v2, ",uid="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p5 ); // invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 116 */
/* .local v1, "msg":Ljava/lang/String; */
com.android.server.pm.PackageManagerServiceUtilsStub .get ( );
int v3 = 3; // const/4 v3, 0x3
(( com.android.server.pm.PackageManagerServiceUtilsStub ) v2 ).logMiuiCriticalInfo ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lcom/android/server/pm/PackageManagerServiceUtilsStub;->logMiuiCriticalInfo(ILjava/lang/String;)V
/* .line 118 */
} // .end local v1 # "msg":Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void truncateSettingsProblemFileIfNeeded ( ) {
/* .locals 2 */
/* .line 40 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->getSettingsProblemFile()Ljava/io/File; */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->getBackupSettingsProblemFile()Ljava/io/File; */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->truncateSettingsProblemFileIfNeeded(Ljava/io/File;Ljava/io/File;)V */
/* .line 41 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->getMiuiPackageProblemFile()Ljava/io/File; */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->getBackupMiuiPackageProblemFile()Ljava/io/File; */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/pm/PackageManagerServiceUtilsImpl;->truncateSettingsProblemFileIfNeeded(Ljava/io/File;Ljava/io/File;)V */
/* .line 42 */
return;
} // .end method
