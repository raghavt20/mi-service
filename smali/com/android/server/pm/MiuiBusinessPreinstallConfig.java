public class com.android.server.pm.MiuiBusinessPreinstallConfig extends com.android.server.pm.MiuiPreinstallConfig {
	 /* .source "MiuiBusinessPreinstallConfig.java" */
	 /* # static fields */
	 private static final java.lang.String BUSINESS_PREINSTALL_IN_DATA_PATH;
	 private static final java.lang.String CLOUD_CONTROL_OFFLINE_PACKAGE_LIST;
	 private static final java.io.File CUSTOMIZED_APP_DIR;
	 private static final java.io.File CUST_MIUI_PREINSTALL_DIR;
	 private static final java.lang.String CUST_MIUI_PREINSTALL_PATH;
	 private static final java.io.File MIUI_BUSINESS_PREINALL_DIR;
	 private static final java.lang.String MIUI_BUSINESS_PREINALL_PATH;
	 private static final java.io.File OTA_SKIP_BUSINESS_APP_LIST_FILE;
	 private static final java.io.File RECOMMENDED_APP_DIR;
	 private static final java.lang.String TAG;
	 public static java.util.ArrayList sCloudControlUninstall;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.util.Set NOT_OTA_PACKAGE_NAMES;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String deviceName;
private Boolean hasLoadGlobalLegacyPreinstall;
java.util.List listLegacyApkPath;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mCloudControlOfflinePackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.pm.MiuiBusinessPreinstallConfig ( ) {
/* .locals 2 */
/* .line 38 */
/* const-class v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 43 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 46 */
miui.util.CustomizeUtil .getMiuiCustomizedAppDir ( );
/* .line 51 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miui/app/recommended"; // const-string v1, "/data/miui/app/recommended"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 53 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/cust/app/customized"; // const-string v1, "/cust/app/customized"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 57 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/cust/data-app"; // const-string v1, "/cust/data-app"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 66 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/system/etc/ota_skip_apps"; // const-string v1, "/system/etc/ota_skip_apps"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
return;
} // .end method
protected com.android.server.pm.MiuiBusinessPreinstallConfig ( ) {
/* .locals 1 */
/* .line 72 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiPreinstallConfig;-><init>()V */
/* .line 63 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mCloudControlOfflinePackages = v0;
/* .line 70 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.NOT_OTA_PACKAGE_NAMES = v0;
/* .line 302 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->hasLoadGlobalLegacyPreinstall:Z */
/* .line 303 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.listLegacyApkPath = v0;
/* .line 74 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readCloudControlOfflinePackage()V */
/* .line 76 */
com.android.server.pm.MiuiPAIPreinstallConfig .init ( );
/* .line 77 */
return;
} // .end method
private void addPreinstallAppPathToList ( java.util.List p0, java.io.File p1, java.util.Set p2 ) {
/* .locals 8 */
/* .param p2, "appDir" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/io/File;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 252 */
/* .local p1, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p3, "filterSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
(( java.io.File ) p2 ).listFiles ( ); // invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 253 */
/* .local v0, "apps":[Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 255 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_3 */
/* aget-object v3, v0, v2 */
/* .line 256 */
/* .local v3, "app":Ljava/io/File; */
v4 = (( java.io.File ) v3 ).isDirectory ( ); // invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 258 */
/* invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getBaseApkFile(Ljava/io/File;)Ljava/io/File; */
/* .line 259 */
/* .local v4, "apk":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* if-nez v5, :cond_0 */
/* .line 260 */
/* .line 265 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 266 */
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_2
/* check-cast v6, Ljava/lang/String; */
/* .line 267 */
/* .local v6, "pkgName":Ljava/lang/String; */
(( java.io.File ) v4 ).getName ( ); // invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;
v7 = (( java.lang.String ) v7 ).contains ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 268 */
(( java.io.File ) v3 ).getPath ( ); // invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 270 */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // :cond_1
/* .line 255 */
} // .end local v3 # "app":Ljava/io/File;
} // .end local v4 # "apk":Ljava/io/File;
} // :cond_2
} // :goto_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 274 */
} // :cond_3
return;
} // .end method
private void addPreinstallAppToList ( java.util.List p0, java.io.File p1, java.util.Set p2 ) {
/* .locals 6 */
/* .param p2, "appDir" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;", */
/* "Ljava/io/File;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 277 */
/* .local p1, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .local p3, "filterSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
(( java.io.File ) p2 ).listFiles ( ); // invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 278 */
/* .local v0, "apps":[Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 280 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_3 */
/* aget-object v3, v0, v2 */
/* .line 281 */
/* .local v3, "app":Ljava/io/File; */
v4 = (( java.io.File ) v3 ).isDirectory ( ); // invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 283 */
/* invoke-direct {p0, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getBaseApkFile(Ljava/io/File;)Ljava/io/File; */
/* .line 284 */
/* .local v4, "apk":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* if-nez v5, :cond_0 */
/* .line 285 */
/* .line 290 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
v5 = (( java.io.File ) v4 ).getName ( ); // invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;
/* if-nez v5, :cond_1 */
/* .line 291 */
/* .line 293 */
} // :cond_1
/* .line 280 */
} // .end local v3 # "app":Ljava/io/File;
} // .end local v4 # "apk":Ljava/io/File;
} // :cond_2
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 296 */
} // :cond_3
return;
} // .end method
private void addPreinstallChannelToList ( java.util.List p0, java.io.File p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p2, "channelDir" # Ljava/io/File; */
/* .param p3, "channelListFile" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/io/File;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 336 */
/* .local p1, "preinstallChannelList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v1, Ljava/io/FileReader; */
/* invoke-direct {v1, p3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* .line 338 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
int v1 = 0; // const/4 v1, 0x0
/* .line 339 */
/* .local v1, "channelName":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 340 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) p2 ).getPath ( ); // invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "/"; // const-string v3, "/"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 343 */
} // :cond_0
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 346 */
} // .end local v0 # "reader":Ljava/io/BufferedReader;
} // .end local v1 # "channelName":Ljava/lang/String;
/* .line 344 */
/* :catch_0 */
/* move-exception v0 */
/* .line 345 */
/* .local v0, "e":Ljava/io/IOException; */
v1 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Error occurs while read preinstalled channels "; // const-string v3, "Error occurs while read preinstalled channels "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 347 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
private java.io.File getBaseApkFile ( java.io.File p0 ) {
/* .locals 3 */
/* .param p1, "dir" # Ljava/io/File; */
/* .line 299 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) p1 ).getName ( ); // invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".apk"; // const-string v2, ".apk"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
} // .end method
private Boolean isUninstallByMccOrMnc ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 403 */
int v0 = 0; // const/4 v0, 0x0
/* .line 405 */
/* .local v0, "isUninstalled":Z */
final String v1 = "com.yandex.searchapp"; // const-string v1, "com.yandex.searchapp"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 406 */
final String v1 = "ro.miui.build.region"; // const-string v1, "ro.miui.build.region"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
/* .line 407 */
/* .local v1, "sku":Ljava/lang/String; */
final String v3 = "eea"; // const-string v3, "eea"
v3 = android.text.TextUtils .equals ( v1,v3 );
/* if-nez v3, :cond_0 */
final String v3 = "global"; // const-string v3, "global"
v3 = android.text.TextUtils .equals ( v1,v3 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 408 */
} // :cond_0
final String v3 = "persist.sys.carrier.subnetwork"; // const-string v3, "persist.sys.carrier.subnetwork"
android.os.SystemProperties .get ( v3,v2 );
final String v3 = "ru_operator"; // const-string v3, "ru_operator"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/lit8 v2, v2, 0x1 */
/* move v0, v2 */
/* .line 412 */
} // .end local v1 # "sku":Ljava/lang/String;
} // :cond_1
} // .end method
private Boolean isValidIme ( java.lang.String p0, java.util.Locale p1 ) {
/* .locals 5 */
/* .param p1, "locale" # Ljava/lang/String; */
/* .param p2, "curLocale" # Ljava/util/Locale; */
/* .line 240 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 241 */
/* .local v0, "locales":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_2 */
/* .line 242 */
/* aget-object v2, v0, v1 */
(( java.util.Locale ) p2 ).toString ( ); // invoke-virtual {p2}, Ljava/util/Locale;->toString()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v2, :cond_1 */
/* aget-object v2, v0, v1 */
/* .line 243 */
final String v3 = "*"; // const-string v3, "*"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* aget-object v2, v0, v1 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 244 */
(( java.util.Locale ) p2 ).getLanguage ( ); // invoke-virtual {p2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "_*"; // const-string v4, "_*"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 241 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 245 */
} // :cond_1
} // :goto_1
int v2 = 1; // const/4 v2, 0x1
/* .line 248 */
} // .end local v1 # "i":I
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void readCloudControlOfflinePackage ( ) {
/* .locals 4 */
/* .line 350 */
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v1, Ljava/io/FileReader; */
final String v2 = "/data/system/cloud_control_offline_package.list"; // const-string v2, "/data/system/cloud_control_offline_package.list"
/* invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 352 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v1 */
/* .local v2, "line":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 353 */
v1 = android.text.TextUtils .isEmpty ( v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 354 */
/* .line 356 */
} // :cond_0
v1 = this.mCloudControlOfflinePackages;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 358 */
} // .end local v2 # "line":Ljava/lang/String;
} // :cond_1
try { // :try_start_2
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 360 */
} // .end local v0 # "reader":Ljava/io/BufferedReader;
/* .line 350 */
/* .restart local v0 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_3
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_4
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
} // :goto_1
/* throw v1 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 358 */
} // .end local v0 # "reader":Ljava/io/BufferedReader;
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig; */
/* :catch_0 */
/* move-exception v0 */
/* .line 359 */
/* .local v0, "e":Ljava/io/IOException; */
v1 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Error occurs while offline preinstall packages "; // const-string v3, "Error occurs while offline preinstall packages "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 361 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_2
return;
} // .end method
private Boolean signCheckFailed ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .line 129 */
/* sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_4 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_1 */
/* .line 132 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->supportSignVerifyInCust()Z */
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "/cust/app/customized"; // const-string v0, "/cust/app/customized"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v0, v2 */
} // :cond_1
/* move v0, v1 */
/* .line 133 */
/* .local v0, "custAppSupportSign":Z */
} // :goto_0
v3 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Sign support is "; // const-string v5, "Sign support is "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 134 */
if ( v0 != null) { // if-eqz v0, :cond_2
miui.os.CustVerifier .getInstance ( );
/* if-nez v4, :cond_2 */
/* .line 135 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "CustVerifier init error !"; // const-string v4, "CustVerifier init error !"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " will won\'t install."; // const-string v4, " will won\'t install."
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v1 );
/* .line 136 */
/* .line 138 */
} // :cond_2
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getBaseApkFile(Ljava/io/File;)Ljava/io/File; */
/* .line 139 */
/* .local v4, "baseApkFile":Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_3
miui.os.CustVerifier .getInstance ( );
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
v5 = (( miui.os.CustVerifier ) v5 ).verifyApkSignatue ( v6, v1 ); // invoke-virtual {v5, v6, v1}, Lmiui/os/CustVerifier;->verifyApkSignatue(Ljava/lang/String;I)Z
/* if-nez v5, :cond_3 */
/* .line 140 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " verify failed!"; // const-string v5, " verify failed!"
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v1 );
/* .line 141 */
/* .line 143 */
} // :cond_3
/* .line 130 */
} // .end local v0 # "custAppSupportSign":Z
} // .end local v4 # "baseApkFile":Ljava/io/File;
} // :cond_4
} // :goto_1
} // .end method
private Boolean skipOTA ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 179 */
try { // :try_start_0
v0 = this.deviceName;
v0 = android.text.TextUtils .isEmpty ( v0 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 180 */
final String v0 = "android.os.SystemProperties"; // const-string v0, "android.os.SystemProperties"
java.lang.Class .forName ( v0 );
final String v2 = "get"; // const-string v2, "get"
int v3 = 2; // const/4 v3, 0x2
/* new-array v4, v3, [Ljava/lang/Class; */
/* const-class v5, Ljava/lang/String; */
/* aput-object v5, v4, v1 */
/* const-class v5, Ljava/lang/String; */
int v6 = 1; // const/4 v6, 0x1
/* aput-object v5, v4, v6 */
/* .line 181 */
(( java.lang.Class ) v0 ).getMethod ( v2, v4 ); // invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* new-array v2, v3, [Ljava/lang/Object; */
final String v3 = "ro.product.name"; // const-string v3, "ro.product.name"
/* aput-object v3, v2, v1 */
final String v3 = ""; // const-string v3, ""
/* aput-object v3, v2, v6 */
/* .line 182 */
int v3 = 0; // const/4 v3, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
this.deviceName = v0;
/* .line 184 */
} // :cond_0
final String v0 = "sagit"; // const-string v0, "sagit"
v2 = this.deviceName;
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "com.xiaomi.youpin"; // const-string v0, "com.xiaomi.youpin"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 185 */
v0 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skipOTA, deviceName is " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.deviceName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 186 */
/* .line 190 */
} // :cond_1
/* .line 188 */
/* :catch_0 */
/* move-exception v0 */
/* .line 189 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
final String v2 = "Get exception"; // const-string v2, "Get exception"
android.util.Slog .e ( v1,v2,v0 );
/* .line 191 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
v0 = v0 = this.NOT_OTA_PACKAGE_NAMES;
} // .end method
private Boolean supportSignVerifyInCust ( ) {
/* .locals 2 */
/* .line 147 */
/* const-string/jumbo v0, "support_sign_verify_in_cust" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
} // .end method
private void writeOfflinePreinstallPackage ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 374 */
v0 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
final String v1 = "Write offline preinstall package name into /data/system/cloud_control_offline_package.list"; // const-string v1, "Write offline preinstall package name into /data/system/cloud_control_offline_package.list"
android.util.Slog .i ( v0,v1 );
/* .line 375 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 376 */
return;
/* .line 378 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedWriter; */
/* new-instance v1, Ljava/io/FileWriter; */
final String v2 = "/data/system/cloud_control_offline_package.list"; // const-string v2, "/data/system/cloud_control_offline_package.list"
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v1, v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V */
/* invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 379 */
/* .local v0, "bufferWriter":Ljava/io/BufferedWriter; */
try { // :try_start_1
v1 = this.mCloudControlOfflinePackages;
/* monitor-enter v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 380 */
try { // :try_start_2
v2 = this.mCloudControlOfflinePackages;
/* .line 381 */
(( java.io.BufferedWriter ) v0 ).write ( p1 ); // invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 382 */
final String v2 = "\n"; // const-string v2, "\n"
(( java.io.BufferedWriter ) v0 ).write ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 383 */
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 384 */
try { // :try_start_3
(( java.io.BufferedWriter ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 386 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
/* .line 383 */
/* .restart local v0 # "bufferWriter":Ljava/io/BufferedWriter; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_4
/* monitor-exit v1 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
} // .end local p0 # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
} // .end local p1 # "pkg":Ljava/lang/String;
try { // :try_start_5
/* throw v2 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 378 */
/* .restart local v0 # "bufferWriter":Ljava/io/BufferedWriter; */
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig; */
/* .restart local p1 # "pkg":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_6
(( java.io.BufferedWriter ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* :catchall_2 */
/* move-exception v2 */
try { // :try_start_7
(( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
} // .end local p1 # "pkg":Ljava/lang/String;
} // :goto_0
/* throw v1 */
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_0 */
/* .line 384 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiBusinessPreinstallConfig; */
/* .restart local p1 # "pkg":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* .line 385 */
/* .local v0, "e":Ljava/io/IOException; */
v1 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
final String v2 = "Error occurs when to write offline preinstall package name."; // const-string v2, "Error occurs when to write offline preinstall package name."
android.util.Slog .e ( v1,v2 );
/* .line 387 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
/* # virtual methods */
public java.util.Map getAdvanceApps ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 390 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 391 */
/* .local v0, "sAdvanceApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
(( com.android.server.pm.MiuiPreinstallHelper ) v1 ).getPreinstallApps ( ); // invoke-virtual {v1}, Lcom/android/server/pm/MiuiPreinstallHelper;->getPreinstallApps()Ljava/util/List;
/* .line 392 */
/* .local v1, "preinstallApps":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/MiuiPreinstallApp;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/android/server/pm/MiuiPreinstallApp; */
/* .line 393 */
/* .local v3, "app":Lcom/android/server/pm/MiuiPreinstallApp; */
(( com.android.server.pm.MiuiPreinstallApp ) v3 ).getApkPath ( ); // invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;
/* if-nez v4, :cond_0 */
/* .line 395 */
} // :cond_0
(( com.android.server.pm.MiuiPreinstallApp ) v3 ).getApkPath ( ); // invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;
final String v5 = "miuiAdvancePreload"; // const-string v5, "miuiAdvancePreload"
v4 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v4, :cond_1 */
(( com.android.server.pm.MiuiPreinstallApp ) v3 ).getApkPath ( ); // invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;
final String v5 = "miuiAdvertisement"; // const-string v5, "miuiAdvertisement"
v4 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 396 */
} // :cond_1
(( com.android.server.pm.MiuiPreinstallApp ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getPackageName()Ljava/lang/String;
(( com.android.server.pm.MiuiPreinstallApp ) v3 ).getApkPath ( ); // invoke-virtual {v3}, Lcom/android/server/pm/MiuiPreinstallApp;->getApkPath()Ljava/lang/String;
/* .line 398 */
} // .end local v3 # "app":Lcom/android/server/pm/MiuiPreinstallApp;
} // :cond_2
/* .line 399 */
} // :cond_3
} // .end method
public java.util.List getCustAppList ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 221 */
v0 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
final String v1 = "getCustAppList"; // const-string v1, "getCustAppList"
android.util.Slog .w ( v0,v1 );
/* .line 222 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 223 */
/* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
miui.util.CustomizeUtil .getMiuiCustVariantDir ( );
/* .line 224 */
/* .local v1, "custVariantDir":Ljava/io/File; */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* .line 225 */
/* .local v2, "customizedAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 226 */
/* .local v3, "recommendedAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 228 */
/* new-instance v4, Ljava/io/File; */
final String v5 = "customized_applist"; // const-string v5, "customized_applist"
/* invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).readLineToSet ( v4, v2 ); // invoke-virtual {p0, v4, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 229 */
/* new-instance v4, Ljava/io/File; */
final String v5 = "ota_customized_applist"; // const-string v5, "ota_customized_applist"
/* invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).readLineToSet ( v4, v2 ); // invoke-virtual {p0, v4, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 230 */
/* new-instance v4, Ljava/io/File; */
final String v5 = "recommended_applist"; // const-string v5, "recommended_applist"
/* invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).readLineToSet ( v4, v3 ); // invoke-virtual {p0, v4, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 231 */
/* new-instance v4, Ljava/io/File; */
final String v5 = "ota_recommended_applist"; // const-string v5, "ota_recommended_applist"
/* invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).readLineToSet ( v4, v3 ); // invoke-virtual {p0, v4, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 233 */
} // :cond_0
v4 = com.android.server.pm.MiuiBusinessPreinstallConfig.CUSTOMIZED_APP_DIR;
/* invoke-direct {p0, v0, v4, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V */
/* .line 235 */
v4 = com.android.server.pm.MiuiBusinessPreinstallConfig.RECOMMENDED_APP_DIR;
/* invoke-direct {p0, v0, v4, v3}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallAppToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V */
/* .line 236 */
} // .end method
protected java.util.List getCustMiuiPreinstallDirs ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 89 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 90 */
/* .local v0, "custMiuiPreinstallDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
v1 = com.android.server.pm.MiuiBusinessPreinstallConfig.CUST_MIUI_PREINSTALL_DIR;
/* .line 91 */
} // .end method
protected java.util.List getLegacyPreinstallList ( Boolean p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "isFirstBoot" # Z */
/* .param p2, "isDeviceUpgrading" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(ZZ)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 162 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 163 */
v0 = com.android.server.pm.MiuiBusinessPreinstallConfig.OTA_SKIP_BUSINESS_APP_LIST_FILE;
v1 = this.NOT_OTA_PACKAGE_NAMES;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).readLineToSet ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 166 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 167 */
/* .local v0, "legacyPreinstallApkList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 169 */
/* .local v1, "legacyPreinstallApkSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v2, Ljava/io/File; */
miui.util.CustomizeUtil .getMiuiCustomizedDir ( );
final String v4 = "app_legacy_install_list"; // const-string v4, "app_legacy_install_list"
/* invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).readLineToSet ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
/* .line 170 */
v2 = com.android.server.pm.MiuiBusinessPreinstallConfig.CUSTOMIZED_APP_DIR;
/* invoke-direct {p0, v0, v2, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallAppPathToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V */
/* .line 171 */
v2 = com.android.server.pm.MiuiBusinessPreinstallConfig.RECOMMENDED_APP_DIR;
/* invoke-direct {p0, v0, v2, v1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallAppPathToList(Ljava/util/List;Ljava/io/File;Ljava/util/Set;)V */
/* .line 173 */
} // .end method
public java.util.ArrayList getPeinstalledChannelList ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 318 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 319 */
/* .local v0, "preinstalledChannelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
miui.util.CustomizeUtil .getMiuiCustVariantDir ( );
/* .line 320 */
/* .local v1, "custVariantDir":Ljava/io/File; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 321 */
(( java.io.File ) v1 ).getPath ( ); // invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 322 */
/* .local v2, "custVariantPath":Ljava/lang/String; */
v3 = com.android.server.pm.MiuiBusinessPreinstallConfig.CUSTOMIZED_APP_DIR;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/customized_channellist"; // const-string v5, "/customized_channellist"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0, v3, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V */
/* .line 324 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/ota_customized_channellist"; // const-string v5, "/ota_customized_channellist"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0, v3, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V */
/* .line 326 */
v3 = com.android.server.pm.MiuiBusinessPreinstallConfig.RECOMMENDED_APP_DIR;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/recommended_channellist"; // const-string v5, "/recommended_channellist"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0, v3, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V */
/* .line 328 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/ota_recommended_channellist"; // const-string v5, "/ota_recommended_channellist"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v0, v3, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->addPreinstallChannelToList(Ljava/util/List;Ljava/io/File;Ljava/lang/String;)V */
/* .line 331 */
} // .end local v2 # "custVariantPath":Ljava/lang/String;
} // :cond_0
} // .end method
protected java.util.List getPreinstallDirs ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 81 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 82 */
/* .local v0, "preinstallDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
v1 = com.android.server.pm.MiuiBusinessPreinstallConfig.MIUI_BUSINESS_PREINALL_DIR;
/* .line 83 */
v1 = com.android.server.pm.MiuiBusinessPreinstallConfig.RECOMMENDED_APP_DIR;
/* .line 84 */
} // .end method
protected java.util.List getVanwardAppList ( ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 198 */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).getCustAppList ( ); // invoke-virtual {p0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustAppList()Ljava/util/List;
/* .line 199 */
/* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 200 */
/* .local v1, "vanwardAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* .line 201 */
/* .local v2, "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v3, Ljava/io/File; */
miui.util.CustomizeUtil .getMiuiAppDir ( );
/* const-string/jumbo v5, "vanward_applist" */
/* invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).readLineToSet ( v3, v2 ); // invoke-virtual {p0, v3, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->readLineToSet(Ljava/io/File;Ljava/util/Set;)V
v3 = /* .line 203 */
v3 = /* if-nez v3, :cond_3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 207 */
} // :cond_0
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Ljava/io/File; */
/* .line 208 */
/* .local v4, "appDir":Ljava/io/File; */
/* invoke-direct {p0, v4}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getBaseApkFile(Ljava/io/File;)Ljava/io/File; */
/* .line 209 */
/* .local v5, "apkFile":Ljava/io/File; */
v6 = (( java.io.File ) v5 ).getName ( ); // invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;
/* if-nez v6, :cond_1 */
/* .line 210 */
/* .line 212 */
} // :cond_1
/* .line 213 */
} // .end local v4 # "appDir":Ljava/io/File;
} // .end local v5 # "apkFile":Ljava/io/File;
/* .line 214 */
} // :cond_2
/* .line 204 */
} // :cond_3
} // :goto_1
v3 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
final String v4 = "No vanward cust app need to install"; // const-string v4, "No vanward cust app need to install"
android.util.Slog .w ( v3,v4 );
/* .line 205 */
} // .end method
protected Boolean isBusinessPreinstall ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 151 */
final String v0 = "/cust/app/customized"; // const-string v0, "/cust/app/customized"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
final String v0 = "/data/miui/app/recommended"; // const-string v0, "/data/miui/app/recommended"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
protected Boolean isCloudOfflineApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 364 */
v0 = v0 = this.mCloudControlOfflinePackages;
/* if-nez v0, :cond_0 */
v0 = v0 = this.mCloudControlOfflinePackages;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
protected Boolean isCustMiuiPreinstall ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 156 */
final String v0 = "/cust/data-app"; // const-string v0, "/cust/data-app"
v0 = (( java.lang.String ) p1 ).startsWith ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
} // .end method
public Boolean needIgnore ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 96 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 97 */
v0 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
final String v2 = "apkPath is null, won\'t install."; // const-string v2, "apkPath is null, won\'t install."
android.util.Slog .i ( v0,v2 );
/* .line 98 */
/* .line 100 */
} // :cond_0
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 101 */
/* .local v0, "apk":Ljava/io/File; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_1 */
/* .line 102 */
v2 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "apk is not exist, won\'t install, apkPath="; // const-string v4, "apk is not exist, won\'t install, apkPath="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 103 */
/* .line 105 */
} // :cond_1
v2 = com.android.server.pm.MiuiBusinessPreinstallConfig.sCloudControlUninstall;
v2 = (( java.util.ArrayList ) v2 ).contains ( p2 ); // invoke-virtual {v2, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 106 */
v2 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "CloudControlUninstall apk won\'t install, packageName="; // const-string v4, "CloudControlUninstall apk won\'t install, packageName="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 107 */
/* .line 109 */
} // :cond_2
v2 = /* invoke-direct {p0, p2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->skipOTA(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 110 */
v2 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " not support ota, packageName="; // const-string v4, " not support ota, packageName="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 111 */
/* .line 113 */
} // :cond_3
v2 = /* invoke-direct {p0, p2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->isUninstallByMccOrMnc(Ljava/lang/String;)Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 114 */
v2 = com.android.server.pm.MiuiBusinessPreinstallConfig.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "region or mcc not support,won\'t install, packageName="; // const-string v4, "region or mcc not support,won\'t install, packageName="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 115 */
/* .line 117 */
} // :cond_4
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->signCheckFailed(Ljava/lang/String;)Z */
} // .end method
protected Boolean needLegacyPreinstall ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 307 */
/* iget-boolean v0, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->hasLoadGlobalLegacyPreinstall:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* .line 308 */
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) p0 ).getLegacyPreinstallList ( v2, v2 ); // invoke-virtual {p0, v2, v2}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getLegacyPreinstallList(ZZ)Ljava/util/List;
this.listLegacyApkPath = v0;
/* .line 309 */
/* iput-boolean v1, p0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->hasLoadGlobalLegacyPreinstall:Z */
/* .line 311 */
} // :cond_0
v0 = v0 = this.listLegacyApkPath;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 312 */
/* .line 314 */
} // :cond_1
} // .end method
public void removeFromPreinstallList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 368 */
v0 = v0 = this.mCloudControlOfflinePackages;
/* if-nez v0, :cond_0 */
/* .line 369 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->writeOfflinePreinstallPackage(Ljava/lang/String;)V */
/* .line 371 */
} // :cond_0
return;
} // .end method
