.class abstract Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
.super Ljava/lang/Object;
.source "PackageEventRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageEventRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "PackageEventBase"
.end annotation


# instance fields
.field private final id:Ljava/lang/String;

.field private final installer:Ljava/lang/String;

.field private final packageName:Ljava/lang/String;

.field private final userIds:[I


# direct methods
.method static bridge synthetic -$$Nest$fgetid(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetinstaller(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->installer:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpackageName(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->packageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetuserIds(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)[I
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mbuildBundle(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Landroid/os/Bundle;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->buildBundle()Landroid/os/Bundle;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetEventType(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->getEventType()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$smbuildBundleFromRecord(Ljava/lang/String;II)Landroid/os/Bundle;
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->buildBundleFromRecord(Ljava/lang/String;II)Landroid/os/Bundle;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smresolveIdFromRecord(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveIdFromRecord(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;[ILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "userIds"    # [I
    .param p4, "installerPackageName"    # Ljava/lang/String;

    .line 652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 653
    iput-object p1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    .line 654
    iput-object p2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->packageName:Ljava/lang/String;

    .line 655
    iput-object p3, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    .line 656
    iput-object p4, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->installer:Ljava/lang/String;

    .line 657
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageEventBase-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;-><init>(Ljava/lang/String;Ljava/lang/String;[ILjava/lang/String;)V

    return-void
.end method

.method private buildBundle()Landroid/os/Bundle;
    .locals 4

    .line 784
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 785
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "id"

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveEventTimeMillis(Ljava/lang/String;)J

    move-result-wide v1

    const-string v3, "eventTimeMillis"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 787
    const-string v1, "packageName"

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 789
    const-string/jumbo v2, "userIds"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 791
    :cond_0
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->installer:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 792
    const-string v1, "installerPackageName"

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->installer:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    :cond_1
    return-object v0
.end method

.method private static buildBundleFromRecord(Ljava/lang/String;II)Landroid/os/Bundle;
    .locals 20
    .param p0, "record"    # Ljava/lang/String;
    .param p1, "lineNum"    # I
    .param p2, "desireType"    # I

    .line 715
    move-object/from16 v1, p0

    move/from16 v2, p1

    const-string v0, "isRemovedFully"

    const-string v3, "installerPackageName"

    const-string/jumbo v4, "userIds"

    const-string v5, "packageName"

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 718
    .local v6, "result":Landroid/os/Bundle;
    const/4 v7, 0x0

    const/4 v8, 0x6

    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveIdFromRecord(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 719
    .local v9, "id":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 720
    const-string v0, "fail to resolve attr id"

    invoke-static {v8, v0, v2}, Lcom/android/server/pm/PackageEventRecorder;->-$$Nest$smprintLogWhileResolveTxt(ILjava/lang/String;I)V

    .line 722
    return-object v7

    .line 724
    :cond_0
    const-string v10, "id"

    invoke-virtual {v6, v10, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    const-string v10, "eventTimeMillis"

    invoke-static {v9}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveEventTimeMillis(Ljava/lang/String;)J

    move-result-wide v11

    invoke-virtual {v6, v10, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 726
    invoke-static {v9}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveEventType(Ljava/lang/String;)I

    move-result v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 727
    .local v10, "actualType":I
    move/from16 v11, p2

    if-eq v10, v11, :cond_1

    .line 728
    return-object v7

    .line 731
    :cond_1
    :try_start_1
    new-instance v12, Landroid/util/ArrayMap;

    invoke-direct {v12}, Landroid/util/ArrayMap;-><init>()V

    .line 732
    .local v12, "attrs":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v13, 0x20

    invoke-virtual {v1, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v14

    const/4 v15, 0x1

    add-int/2addr v14, v15

    invoke-virtual {v1, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 733
    invoke-static {v13}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v14, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 732
    array-length v14, v13

    const/16 v16, 0x0

    move/from16 v7, v16

    :goto_0
    if-ge v7, v14, :cond_3

    aget-object v17, v13, v7

    move-object/from16 v18, v17

    .line 734
    .local v18, "attr":Ljava/lang/String;
    const/16 v17, 0x3d

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v8, v18

    .end local v18    # "attr":Ljava/lang/String;
    .local v8, "attr":Ljava/lang/String;
    invoke-virtual {v8, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 735
    .local v15, "pv":[Ljava/lang/String;
    array-length v1, v15

    move-object/from16 v18, v9

    .end local v9    # "id":Ljava/lang/String;
    .local v18, "id":Ljava/lang/String;
    const/4 v9, 0x2

    if-eq v1, v9, :cond_2

    .line 736
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bad attr format :"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x6

    invoke-static {v9, v1, v2}, Lcom/android/server/pm/PackageEventRecorder;->-$$Nest$smprintLogWhileResolveTxt(ILjava/lang/String;I)V

    .line 738
    const/4 v9, 0x1

    goto :goto_1

    .line 740
    :cond_2
    aget-object v1, v15, v16

    move-object/from16 v19, v8

    const/4 v9, 0x1

    .end local v8    # "attr":Ljava/lang/String;
    .local v19, "attr":Ljava/lang/String;
    aget-object v8, v15, v9

    invoke-virtual {v12, v1, v8}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 732
    .end local v15    # "pv":[Ljava/lang/String;
    .end local v19    # "attr":Ljava/lang/String;
    :goto_1
    add-int/lit8 v7, v7, 0x1

    const/4 v8, 0x6

    move-object/from16 v1, p0

    move v15, v9

    move-object/from16 v9, v18

    goto :goto_0

    .line 743
    .end local v18    # "id":Ljava/lang/String;
    .restart local v9    # "id":Ljava/lang/String;
    :cond_3
    move-object/from16 v18, v9

    .end local v9    # "id":Ljava/lang/String;
    .restart local v18    # "id":Ljava/lang/String;
    invoke-virtual {v12, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 744
    const-string v0, "attr packageName is missing or invalid"

    const/4 v1, 0x6

    invoke-static {v1, v0, v2}, Lcom/android/server/pm/PackageEventRecorder;->-$$Nest$smprintLogWhileResolveTxt(ILjava/lang/String;I)V

    .line 746
    const/4 v1, 0x0

    return-object v1

    .line 748
    :cond_4
    invoke-virtual {v12, v5}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    invoke-virtual {v12, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 751
    invoke-virtual {v12, v4}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 752
    const/16 v5, 0x2c

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 751
    invoke-static {v1}, Ljava/util/Arrays;->stream([Ljava/lang/Object;)Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v5, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase$$ExternalSyntheticLambda0;

    invoke-direct {v5}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase$$ExternalSyntheticLambda0;-><init>()V

    .line 753
    invoke-interface {v1, v5}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v1

    .line 754
    .local v1, "userIds":[I
    if-eqz v1, :cond_6

    array-length v5, v1

    if-nez v5, :cond_5

    goto :goto_2

    .line 759
    :cond_5
    invoke-virtual {v6, v4, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_3

    .line 755
    :cond_6
    :goto_2
    const-string v0, "fail to resolve attr userIds"

    const/4 v3, 0x6

    invoke-static {v3, v0, v2}, Lcom/android/server/pm/PackageEventRecorder;->-$$Nest$smprintLogWhileResolveTxt(ILjava/lang/String;I)V

    .line 757
    const/4 v3, 0x0

    return-object v3

    .line 762
    .end local v1    # "userIds":[I
    :cond_7
    :goto_3
    invoke-virtual {v12, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 763
    invoke-virtual {v12, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :cond_8
    const/4 v1, 0x3

    if-ne v10, v1, :cond_a

    .line 767
    invoke-virtual {v12, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 768
    const-string v0, "missing attr isRemovedFully"

    const/4 v1, 0x6

    invoke-static {v1, v0, v2}, Lcom/android/server/pm/PackageEventRecorder;->-$$Nest$smprintLogWhileResolveTxt(ILjava/lang/String;I)V

    .line 770
    const/4 v1, 0x0

    return-object v1

    .line 772
    :cond_9
    nop

    .line 773
    invoke-virtual {v12, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 772
    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 778
    .end local v10    # "actualType":I
    .end local v12    # "attrs":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v18    # "id":Ljava/lang/String;
    :cond_a
    nop

    .line 780
    return-object v6

    .line 775
    :catch_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    move/from16 v11, p2

    .line 776
    .local v0, "e":Ljava/lang/Exception;
    :goto_4
    const-string v1, "fail to resolve record"

    const/4 v3, 0x6

    invoke-static {v3, v1, v2, v0}, Lcom/android/server/pm/PackageEventRecorder;->-$$Nest$smprintLogWhileResolveTxt(ILjava/lang/String;ILjava/lang/Throwable;)V

    .line 777
    const/4 v1, 0x0

    return-object v1
.end method

.method static buildPackageEventId(JI)Ljava/lang/String;
    .locals 2
    .param p0, "eventTime"    # J
    .param p2, "type"    # I

    .line 694
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getEventType()I
    .locals 1

    .line 690
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveEventType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private isUserIdsValid()Z
    .locals 2

    .line 665
    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->getEventType()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    if-eqz v0, :cond_0

    array-length v0, v0

    if-gtz v0, :cond_1

    .line 666
    :cond_0
    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->getEventType()I

    move-result v0

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 665
    :goto_0
    return v0
.end method

.method static resolveEventTimeMillis(Ljava/lang/String;)J
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .line 698
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static resolveEventType(Ljava/lang/String;)I
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .line 702
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static resolveIdFromRecord(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "record"    # Ljava/lang/String;

    .line 706
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 707
    return-object v1

    .line 709
    :cond_0
    const/16 v0, 0x20

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 710
    const/4 v1, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    nop

    .line 709
    :goto_0
    return-object v1
.end method


# virtual methods
.method WriteToTxt(Ljava/io/BufferedWriter;)V
    .locals 4
    .param p1, "bufferedWriter"    # Ljava/io/BufferedWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 670
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 671
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 672
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "packageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 673
    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 674
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    if-eqz v1, :cond_1

    array-length v1, v1

    if-lez v1, :cond_1

    .line 675
    const-string/jumbo v1, "userIds="

    invoke-virtual {p1, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 676
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 677
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 678
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    aget v3, v3, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 677
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 680
    .end local v1    # "i":I
    :cond_0
    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 682
    :cond_1
    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->installer:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 683
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "installerPackageName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->installer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 685
    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 687
    :cond_2
    return-void
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 809
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 660
    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->packageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 661
    invoke-direct {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->isUserIdsValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 660
    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 799
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PackageEvent{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", type=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->id:Ljava/lang/String;

    .line 801
    invoke-static {v2}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->resolveEventType(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", packageName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", userIds="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->userIds:[I

    .line 803
    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", installerPackageName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->installer:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 799
    return-object v0
.end method
