.class Lcom/android/server/pm/DexoptServiceThread$2;
.super Ljava/lang/Object;
.source "DexoptServiceThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/DexoptServiceThread;->performDexOptSecondary(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/DexoptServiceThread;

.field final synthetic val$dexUseInfo:Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;

.field final synthetic val$info:Landroid/content/pm/ApplicationInfo;

.field final synthetic val$options:Lcom/android/server/pm/dex/DexoptOptions;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/pm/DexoptServiceThread;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/DexoptServiceThread;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 222
    iput-object p1, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iput-object p2, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$path:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$info:Landroid/content/pm/ApplicationInfo;

    iput-object p4, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$dexUseInfo:Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;

    iput-object p5, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$options:Lcom/android/server/pm/dex/DexoptOptions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 225
    iget-object v0, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget v1, v0, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I

    .line 226
    iget-object v0, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v0, v0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v0, v0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget v1, v1, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$path:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v0, v0, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 230
    .local v1, "getSecondaryId":I
    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v2, v2, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$path:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iput v1, v2, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I

    .line 233
    .end local v1    # "getSecondaryId":I
    :cond_1
    goto :goto_0

    .line 235
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v4, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$info:Landroid/content/pm/ApplicationInfo;

    iget-object v5, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$path:Ljava/lang/String;

    iget v6, v0, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I

    iget-object v7, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$dexUseInfo:Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;

    iget-object v8, p0, Lcom/android/server/pm/DexoptServiceThread$2;->val$options:Lcom/android/server/pm/dex/DexoptOptions;

    move-object v3, v0

    invoke-virtual/range {v3 .. v8}, Lcom/android/server/pm/DexoptServiceThread;->dexOptSecondaryDexPath(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;ILcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fputmDexoptSecondaryResult(Lcom/android/server/pm/DexoptServiceThread;I)V

    .line 237
    iget-object v0, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v0}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptSecondaryResult(Lcom/android/server/pm/DexoptServiceThread;)I

    move-result v0

    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v1}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmPdo(Lcom/android/server/pm/DexoptServiceThread;)Lcom/android/server/pm/PackageDexOptimizer;

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v0}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmDexoptSecondaryResult(Lcom/android/server/pm/DexoptServiceThread;)I

    move-result v0

    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    invoke-static {v1}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$fgetmPdo(Lcom/android/server/pm/DexoptServiceThread;)Lcom/android/server/pm/PackageDexOptimizer;

    if-nez v0, :cond_4

    .line 239
    :cond_3
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$sfgetmWaitLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 240
    :try_start_0
    iget-object v1, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget-object v1, v1, Lcom/android/server/pm/DexoptServiceThread;->mDexoptSecondaryPath:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/android/server/pm/DexoptServiceThread$2;->this$0:Lcom/android/server/pm/DexoptServiceThread;

    iget v2, v2, Lcom/android/server/pm/DexoptServiceThread;->secondaryId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 241
    .local v1, "path":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/pm/DexoptServiceThread;->-$$Nest$sfgetmWaitLock()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 242
    .end local v1    # "path":Ljava/lang/String;
    monitor-exit v0

    .line 244
    :cond_4
    return-void

    .line 242
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
