public class com.android.server.pm.AdvancePreinstallService extends com.android.server.SystemService {
	 /* .source "AdvancePreinstallService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;, */
	 /* Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String ADVANCE_TAG;
public static final java.lang.String ADVERT_TAG;
private static final java.lang.String AD_ONLINE_SERVICE_NAME;
private static final java.lang.String AD_ONLINE_SERVICE_PACKAGE_NAME;
private static final java.lang.String ANDROID_VERSION;
private static final java.lang.String APP_TYPE;
private static final java.lang.String CHANNEL;
private static final java.lang.String CONF_ID;
private static final Boolean DEBUG;
private static final Integer DEFAULT_BIND_DELAY;
private static final Integer DEFAULT_CONNECT_TIME_OUT;
private static final Integer DEFAULT_READ_TIME_OUT;
private static final java.lang.String DEVICE;
private static final java.lang.String IMEI_ID;
private static final java.lang.String IMEI_MD5;
private static final java.lang.String IS_CN;
private static final java.lang.String KEY_IS_PREINSTALLED;
private static final java.lang.String KEY_MIUI_CHANNELPATH;
private static final java.lang.String LANG;
private static final java.lang.String MIUI_VERSION;
private static final java.lang.String MODEL;
private static final java.lang.String NETWORK_TYPE;
private static final java.lang.String NONCE;
private static final java.lang.String OFFLINE_COUNT;
private static final java.lang.String PACKAGE_NAME;
private static final java.lang.String PREINSTALL_CONFIG;
private static final java.lang.String REGION;
private static final java.lang.String REQUEST_TYPE;
private static final java.lang.String SALESE_CHANNEL;
private static final java.lang.String SERVER_ADDRESS;
private static final java.lang.String SERVER_ADDRESS_GLOBAL;
private static final java.lang.String SIGN;
private static final java.lang.String SIM_DETECTION_ACTION;
private static final java.lang.String SKU;
private static final java.lang.String TAG;
private static final java.lang.String TRACK_EVENT_NAME;
/* # instance fields */
private java.util.Map mAdvanceApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private org.json.JSONObject mConfigObj;
private Boolean mHasReceived;
private java.lang.String mImeiMe5;
private Boolean mIsNetworkConnected;
private Boolean mIsWifiConnected;
private com.android.server.pm.MiuiPreinstallHelper mMiuiPreinstallHelper;
private java.lang.String mNetworkTypeName;
private android.content.pm.PackageManager mPackageManager;
private android.content.BroadcastReceiver mReceiver;
private com.android.server.pm.PreInstallServiceTrack mTrack;
/* # direct methods */
static Boolean -$$Nest$fgetmHasReceived ( com.android.server.pm.AdvancePreinstallService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mHasReceived:Z */
} // .end method
static android.content.pm.PackageManager -$$Nest$fgetmPackageManager ( com.android.server.pm.AdvancePreinstallService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageManager;
} // .end method
static void -$$Nest$fputmHasReceived ( com.android.server.pm.AdvancePreinstallService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/pm/AdvancePreinstallService;->mHasReceived:Z */
return;
} // .end method
static void -$$Nest$mgetPreloadAppInfo ( com.android.server.pm.AdvancePreinstallService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getPreloadAppInfo()V */
return;
} // .end method
static void -$$Nest$mhandleAdvancePreinstallApps ( com.android.server.pm.AdvancePreinstallService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAdvancePreinstallApps()V */
return;
} // .end method
static void -$$Nest$mhandleAdvancePreinstallAppsDelay ( com.android.server.pm.AdvancePreinstallService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAdvancePreinstallAppsDelay()V */
return;
} // .end method
static Boolean -$$Nest$misNetworkConnected ( com.android.server.pm.AdvancePreinstallService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/AdvancePreinstallService;->isNetworkConnected(Landroid/content/Context;)Z */
} // .end method
static void -$$Nest$mtrackAdvancePreloadSuccess ( com.android.server.pm.AdvancePreinstallService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/AdvancePreinstallService;->trackAdvancePreloadSuccess(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mtrackEvent ( com.android.server.pm.AdvancePreinstallService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$muninstallAdvancePreinstallApps ( com.android.server.pm.AdvancePreinstallService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V */
return;
} // .end method
public com.android.server.pm.AdvancePreinstallService ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 145 */
/* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
/* .line 128 */
/* new-instance v0, Lcom/android/server/pm/AdvancePreinstallService$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/pm/AdvancePreinstallService$1;-><init>(Lcom/android/server/pm/AdvancePreinstallService;)V */
this.mReceiver = v0;
/* .line 146 */
return;
} // .end method
private void closeBufferedReader ( java.io.BufferedReader p0 ) {
/* .locals 1 */
/* .param p1, "br" # Ljava/io/BufferedReader; */
/* .line 838 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 840 */
try { // :try_start_0
	 (( java.io.BufferedReader ) p1 ).close ( ); // invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V
	 /* :try_end_0 */
	 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 843 */
	 /* .line 841 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 842 */
	 /* .local v0, "e":Ljava/io/IOException; */
	 (( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
	 /* .line 845 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
return;
} // .end method
private void doUninstallApps ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3 ) {
/* .locals 13 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "confId" # I */
/* .param p3, "offlineCount" # I */
/* .param p4, "appType" # Ljava/lang/String; */
/* .line 368 */
/* move-object v7, p0 */
/* move-object v8, p1 */
final String v9 = " failed:"; // const-string v9, " failed:"
final String v10 = "AdvancePreinstallService"; // const-string v10, "AdvancePreinstallService"
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_2 */
v0 = this.mPackageManager;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 370 */
final String v2 = "remove_from_list_begin"; // const-string v2, "remove_from_list_begin"
/* move-object v1, p0 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 371 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 372 */
v0 = this.mMiuiPreinstallHelper;
/* .line 373 */
(( com.android.server.pm.MiuiPreinstallHelper ) v0 ).getBusinessPreinstallConfig ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).removeFromPreinstallList ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->removeFromPreinstallList(Ljava/lang/String;)V
/* .line 375 */
} // :cond_0
com.android.server.pm.PreinstallApp .removeFromPreinstallList ( p1 );
/* .line 378 */
} // :goto_0
v0 = this.mPackageManager;
v0 = (( com.android.server.pm.AdvancePreinstallService ) p0 ).exists ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lcom/android/server/pm/AdvancePreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
/* if-nez v0, :cond_1 */
/* .line 379 */
final String v2 = "package_not_exist"; // const-string v2, "package_not_exist"
/* move-object v1, p0 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 380 */
return;
/* .line 383 */
} // :cond_1
int v11 = 2; // const/4 v11, 0x2
try { // :try_start_0
v0 = this.mPackageManager;
int v1 = 0; // const/4 v1, 0x0
(( android.content.pm.PackageManager ) v0 ).setApplicationEnabledSetting ( p1, v11, v1 ); // invoke-virtual {v0, p1, v11, v1}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 387 */
/* .line 384 */
/* :catch_0 */
/* move-exception v0 */
/* .line 385 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "disable Package "; // const-string v2, "disable Package "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v10,v1 );
/* .line 386 */
final String v2 = "disable_package_failed"; // const-string v2, "disable_package_failed"
/* move-object v1, p0 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 389 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
try { // :try_start_1
final String v2 = "begin_uninstall"; // const-string v2, "begin_uninstall"
/* move-object v1, p0 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 390 */
v0 = this.mPackageManager;
/* new-instance v12, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver; */
/* move-object v1, v12 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;-><init>(Lcom/android/server/pm/AdvancePreinstallService;Ljava/lang/String;IILjava/lang/String;)V */
(( android.content.pm.PackageManager ) v0 ).deletePackage ( p1, v12, v11 ); // invoke-virtual {v0, p1, v12, v11}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 394 */
/* .line 391 */
/* :catch_1 */
/* move-exception v0 */
/* .line 392 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "uninstall Package " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v1 );
/* .line 393 */
/* const-string/jumbo v2, "uninstall_failed" */
/* move-object v1, p0 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move/from16 v5, p3 */
/* move-object/from16 v6, p4 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 396 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_2
return;
} // .end method
private java.lang.String findTrackingApk ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 399 */
v0 = this.mConfigObj;
final String v1 = "AdvancePreinstallService"; // const-string v1, "AdvancePreinstallService"
/* if-nez v0, :cond_0 */
/* .line 400 */
final String v0 = "/cust/etc/cust_apps_config"; // const-string v0, "/cust/etc/cust_apps_config"
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getFileContent ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/AdvancePreinstallService;->getFileContent(Ljava/lang/String;)Ljava/lang/String;
/* .line 401 */
/* .local v0, "config":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 403 */
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
this.mConfigObj = v2;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 406 */
/* .line 404 */
/* :catch_0 */
/* move-exception v2 */
/* .line 405 */
/* .local v2, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v2 ).getMessage ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 410 */
} // .end local v0 # "config":Ljava/lang/String;
} // .end local v2 # "e":Lorg/json/JSONException;
} // :cond_0
} // :goto_0
v0 = this.mConfigObj;
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 412 */
try { // :try_start_1
final String v3 = "data"; // const-string v3, "data"
(( org.json.JSONObject ) v0 ).getJSONArray ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 413 */
/* .local v0, "array":Lorg/json/JSONArray; */
v3 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-gtz v3, :cond_1 */
/* .line 414 */
/* .line 416 */
} // :cond_1
v3 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* .line 417 */
/* .local v3, "count":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, v3, :cond_3 */
/* .line 418 */
(( org.json.JSONArray ) v0 ).getJSONObject ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 419 */
/* .local v5, "obj":Lorg/json/JSONObject; */
if ( v5 != null) { // if-eqz v5, :cond_2
final String v6 = "packageName"; // const-string v6, "packageName"
(( org.json.JSONObject ) v5 ).getString ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v6 = android.text.TextUtils .equals ( v6,p1 );
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 420 */
/* const-string/jumbo v6, "trackApkPackageName" */
(( org.json.JSONObject ) v5 ).getString ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 417 */
} // .end local v5 # "obj":Lorg/json/JSONObject;
} // :cond_2
/* add-int/lit8 v4, v4, 0x1 */
/* .line 425 */
} // .end local v0 # "array":Lorg/json/JSONArray;
} // .end local v3 # "count":I
} // .end local v4 # "i":I
} // :cond_3
/* .line 423 */
/* :catch_1 */
/* move-exception v0 */
/* .line 424 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 427 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :cond_4
} // :goto_2
} // .end method
private java.lang.String getAddress ( ) {
/* .locals 1 */
/* .line 677 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 678 */
final String v0 = "https://control.preload.xiaomi.com/preload_app_info/get?"; // const-string v0, "https://control.preload.xiaomi.com/preload_app_info/get?"
/* .line 680 */
} // :cond_0
final String v0 = "https://global.control.preload.xiaomi.com/preload_app_info/get?"; // const-string v0, "https://global.control.preload.xiaomi.com/preload_app_info/get?"
} // .end method
private java.util.Map getAdvanceApps ( ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 163 */
v0 = this.mAdvanceApps;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 164 */
/* .line 166 */
} // :cond_0
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_1 */
/* .line 167 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAdvanceAppsByFrame()Ljava/util/Map; */
this.mAdvanceApps = v0;
/* .line 168 */
/* .line 170 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getCustomizePreinstallAppList()Ljava/util/List; */
/* .line 171 */
/* .local v0, "custApps":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.mAdvanceApps = v1;
/* .line 172 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_4
/* check-cast v2, Ljava/io/File; */
/* .line 173 */
/* .local v2, "file":Ljava/io/File; */
if ( v2 != null) { // if-eqz v2, :cond_3
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 174 */
(( java.io.File ) v2 ).getPath ( ); // invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v4 = "miuiAdvancePreload"; // const-string v4, "miuiAdvancePreload"
v3 = (( java.lang.String ) v3 ).contains ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v3, :cond_2 */
(( java.io.File ) v2 ).getPath ( ); // invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v4 = "miuiAdvertisement"; // const-string v4, "miuiAdvertisement"
v3 = (( java.lang.String ) v3 ).contains ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 175 */
} // :cond_2
/* invoke-direct {p0, v2}, Lcom/android/server/pm/AdvancePreinstallService;->parsePackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite; */
/* .line 176 */
/* .local v3, "pl":Landroid/content/pm/parsing/PackageLite; */
if ( v3 != null) { // if-eqz v3, :cond_3
(( android.content.pm.parsing.PackageLite ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v4 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v4, :cond_3 */
/* .line 177 */
v4 = this.mAdvanceApps;
(( android.content.pm.parsing.PackageLite ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( java.io.File ) v2 ).getPath ( ); // invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 180 */
} // .end local v2 # "file":Ljava/io/File;
} // .end local v3 # "pl":Landroid/content/pm/parsing/PackageLite;
} // :cond_3
/* .line 181 */
} // :cond_4
v1 = this.mAdvanceApps;
} // .end method
private java.util.Map getAdvanceAppsByFrame ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 222 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 223 */
v0 = this.mMiuiPreinstallHelper;
(( com.android.server.pm.MiuiPreinstallHelper ) v0 ).getBusinessPreinstallConfig ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).getAdvanceApps ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getAdvanceApps()Ljava/util/Map;
/* .line 225 */
} // :cond_0
v0 = com.android.server.pm.PreinstallApp.sAdvanceApps;
} // .end method
private java.lang.String getChannel ( ) {
/* .locals 2 */
/* .line 670 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
/* .line 671 */
miui.os.Build .getCustVariant ( );
/* .line 673 */
} // :cond_0
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
final String v1 = "Public Version"; // const-string v1, "Public Version"
android.os.SystemProperties .get ( v0,v1 );
} // .end method
private com.android.server.pm.AdvancePreinstallService$ConnectEntity getConnectEntity ( ) {
/* .locals 32 */
/* .line 783 */
/* move-object/from16 v14, p0 */
int v15 = 0; // const/4 v15, 0x0
try { // :try_start_0
final String v0 = "get_device_info"; // const-string v0, "get_device_info"
/* invoke-direct {v14, v0}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 784 */
v5 = miui.os.Build.DEVICE;
/* .line 785 */
/* .local v5, "device":Ljava/lang/String; */
v6 = android.os.Build$VERSION.INCREMENTAL;
/* .line 786 */
/* .local v6, "miuiVersion":Ljava/lang/String; */
v7 = android.os.Build$VERSION.RELEASE;
/* .line 787 */
/* .local v7, "androidVersion":Ljava/lang/String; */
v4 = miui.os.Build.MODEL;
/* .line 788 */
/* .local v4, "model":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->getChannel()Ljava/lang/String; */
/* .line 789 */
/* .local v8, "channel":Ljava/lang/String; */
java.util.Locale .getDefault ( );
(( java.util.Locale ) v0 ).getLanguage ( ); // invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
/* .line 790 */
/* .local v12, "lang":Ljava/lang/String; */
com.android.server.pm.CloudSignUtil .getNonceStr ( );
/* .line 792 */
/* .local v0, "nonceStr":Ljava/lang/String; */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* xor-int/lit8 v1, v1, 0x1 */
/* move/from16 v31, v1 */
/* .line 793 */
/* .local v31, "isCn":Z */
if ( v31 != null) { // if-eqz v31, :cond_0
final String v1 = "CN"; // const-string v1, "CN"
} // :cond_0
miui.os.Build .getCustVariant ( );
(( java.lang.String ) v1 ).toUpperCase ( ); // invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
} // :goto_0
/* move-object v9, v1 */
/* .line 794 */
/* .local v9, "region":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->getSku()Ljava/lang/String; */
/* .line 795 */
/* .local v13, "sku":Ljava/lang/String; */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context; */
v10 = /* invoke-direct {v14, v1}, Lcom/android/server/pm/AdvancePreinstallService;->getNetworkType(Landroid/content/Context;)I */
/* .line 797 */
/* .local v10, "networkType":I */
/* invoke-virtual/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context; */
com.android.id.IdentifierManager .getOAID ( v1 );
/* .line 799 */
/* .local v3, "imeiId":Ljava/lang/String; */
v2 = this.mImeiMe5;
/* move-object/from16 v1, p0 */
/* move/from16 v11, v31 */
/* invoke-direct/range {v1 ..v13}, Lcom/android/server/pm/AdvancePreinstallService;->getParamsMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)Ljava/util/TreeMap; */
com.android.server.pm.CloudSignUtil .getSign ( v1,v0 );
/* .line 802 */
/* .local v24, "sign":Ljava/lang/String; */
v1 = /* invoke-static/range {v24 ..v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 803 */
} // :cond_1
/* new-instance v1, Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity; */
v2 = this.mImeiMe5;
/* move-object/from16 v16, v1 */
/* move-object/from16 v17, v2 */
/* move-object/from16 v18, v3 */
/* move-object/from16 v19, v5 */
/* move-object/from16 v20, v6 */
/* move-object/from16 v21, v8 */
/* move-object/from16 v22, v12 */
/* move-object/from16 v23, v0 */
/* move/from16 v25, v31 */
/* move-object/from16 v26, v9 */
/* move-object/from16 v27, v7 */
/* move-object/from16 v28, v4 */
/* move/from16 v29, v10 */
/* move-object/from16 v30, v13 */
/* invoke-direct/range {v16 ..v30}, Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/util/NoSuchElementException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v15, v1 */
/* .line 802 */
} // :goto_1
/* .line 807 */
} // .end local v0 # "nonceStr":Ljava/lang/String;
} // .end local v3 # "imeiId":Ljava/lang/String;
} // .end local v4 # "model":Ljava/lang/String;
} // .end local v5 # "device":Ljava/lang/String;
} // .end local v6 # "miuiVersion":Ljava/lang/String;
} // .end local v7 # "androidVersion":Ljava/lang/String;
} // .end local v8 # "channel":Ljava/lang/String;
} // .end local v9 # "region":Ljava/lang/String;
} // .end local v10 # "networkType":I
} // .end local v12 # "lang":Ljava/lang/String;
} // .end local v13 # "sku":Ljava/lang/String;
} // .end local v24 # "sign":Ljava/lang/String;
} // .end local v31 # "isCn":Z
/* :catch_0 */
/* move-exception v0 */
/* .line 808 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 805 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v0 */
/* .line 806 */
/* .local v0, "ex":Ljava/util/NoSuchElementException; */
(( java.util.NoSuchElementException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/util/NoSuchElementException;->printStackTrace()V
/* .line 809 */
} // .end local v0 # "ex":Ljava/util/NoSuchElementException;
/* nop */
/* .line 810 */
} // :goto_2
final String v0 = "get_device_info_failed"; // const-string v0, "get_device_info_failed"
/* invoke-direct {v14, v0}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 811 */
} // .end method
private java.util.List getCustomizePreinstallAppList ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 185 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 186 */
v0 = this.mMiuiPreinstallHelper;
/* .line 187 */
(( com.android.server.pm.MiuiPreinstallHelper ) v0 ).getBusinessPreinstallConfig ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;
(( com.android.server.pm.MiuiBusinessPreinstallConfig ) v0 ).getCustAppList ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustAppList()Ljava/util/List;
/* .line 188 */
/* .local v0, "appDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 189 */
/* .local v1, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/io/File; */
/* .line 190 */
/* .local v3, "appDir":Ljava/io/File; */
(( java.io.File ) v3 ).listFiles ( ); // invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 191 */
/* .local v4, "apps":[Ljava/io/File; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 192 */
/* array-length v5, v4 */
int v6 = 0; // const/4 v6, 0x0
} // :goto_1
/* if-ge v6, v5, :cond_1 */
/* aget-object v7, v4, v6 */
/* .line 193 */
/* .local v7, "app":Ljava/io/File; */
v8 = /* invoke-direct {p0, v7}, Lcom/android/server/pm/AdvancePreinstallService;->isApkFile(Ljava/io/File;)Z */
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 194 */
/* .line 192 */
} // .end local v7 # "app":Ljava/io/File;
} // :cond_0
/* add-int/lit8 v6, v6, 0x1 */
/* .line 198 */
} // .end local v3 # "appDir":Ljava/io/File;
} // .end local v4 # "apps":[Ljava/io/File;
} // :cond_1
/* .line 199 */
} // :cond_2
/* .line 201 */
} // .end local v0 # "appDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v1 # "appList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // :cond_3
com.android.server.pm.PreinstallApp .getCustomizePreinstallAppList ( );
} // .end method
private java.lang.String getIMEIMD5 ( ) {
/* .locals 3 */
/* .line 269 */
miui.telephony.TelephonyManager .getDefault ( );
(( miui.telephony.TelephonyManager ) v0 ).getImeiList ( ); // invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getImeiList()Ljava/util/List;
/* .line 271 */
/* .local v0, "imeiList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v1 = ""; // const-string v1, ""
/* .line 272 */
/* .local v1, "imei":Ljava/lang/String; */
v2 = if ( v0 != null) { // if-eqz v0, :cond_0
/* if-lez v2, :cond_0 */
/* .line 273 */
java.util.Collections .min ( v0 );
/* move-object v1, v2 */
/* check-cast v1, Ljava/lang/String; */
/* .line 276 */
} // :cond_0
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_1 */
com.android.server.pm.CloudSignUtil .md5 ( v1 );
} // :cond_1
final String v2 = ""; // const-string v2, ""
} // :goto_0
} // .end method
private Integer getNetworkType ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 339 */
/* iget-boolean v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsNetworkConnected:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 340 */
/* .line 342 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsWifiConnected:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 343 */
int v0 = -1; // const/4 v0, -0x1
/* .line 345 */
} // :cond_1
final String v0 = "phone"; // const-string v0, "phone"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/telephony/TelephonyManager; */
/* .line 346 */
/* .local v0, "tm":Landroid/telephony/TelephonyManager; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 347 */
v1 = (( android.telephony.TelephonyManager ) v0 ).getNetworkType ( ); // invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I
/* .line 349 */
} // :cond_2
} // .end method
private java.util.TreeMap getParamsMap ( java.lang.String p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, java.lang.String p5, java.lang.String p6, java.lang.String p7, Integer p8, Boolean p9, java.lang.String p10, java.lang.String p11 ) {
/* .locals 3 */
/* .param p1, "imeiMd5" # Ljava/lang/String; */
/* .param p2, "imeiId" # Ljava/lang/String; */
/* .param p3, "model" # Ljava/lang/String; */
/* .param p4, "device" # Ljava/lang/String; */
/* .param p5, "miuiVersion" # Ljava/lang/String; */
/* .param p6, "androidVersion" # Ljava/lang/String; */
/* .param p7, "channel" # Ljava/lang/String; */
/* .param p8, "region" # Ljava/lang/String; */
/* .param p9, "networkType" # I */
/* .param p10, "isCn" # Z */
/* .param p11, "lang" # Ljava/lang/String; */
/* .param p12, "sku" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* "IZ", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/TreeMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 818 */
/* new-instance v0, Ljava/util/TreeMap; */
/* invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V */
/* .line 819 */
/* .local v0, "params":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Object;>;" */
final String v1 = "imeiMd5"; // const-string v1, "imeiMd5"
(( java.util.TreeMap ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 820 */
final String v1 = "imId"; // const-string v1, "imId"
(( java.util.TreeMap ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 821 */
final String v1 = "model"; // const-string v1, "model"
(( java.util.TreeMap ) v0 ).put ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 822 */
final String v1 = "device"; // const-string v1, "device"
(( java.util.TreeMap ) v0 ).put ( v1, p4 ); // invoke-virtual {v0, v1, p4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 823 */
final String v1 = "miuiVersion"; // const-string v1, "miuiVersion"
(( java.util.TreeMap ) v0 ).put ( v1, p5 ); // invoke-virtual {v0, v1, p5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 824 */
final String v1 = "androidVersion"; // const-string v1, "androidVersion"
(( java.util.TreeMap ) v0 ).put ( v1, p6 ); // invoke-virtual {v0, v1, p6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 825 */
final String v1 = "channel"; // const-string v1, "channel"
(( java.util.TreeMap ) v0 ).put ( v1, p7 ); // invoke-virtual {v0, v1, p7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 826 */
final String v1 = "region"; // const-string v1, "region"
(( java.util.TreeMap ) v0 ).put ( v1, p8 ); // invoke-virtual {v0, v1, p8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 827 */
final String v1 = "isCn"; // const-string v1, "isCn"
java.lang.Boolean .valueOf ( p10 );
(( java.util.TreeMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 828 */
final String v1 = "lang"; // const-string v1, "lang"
(( java.util.TreeMap ) v0 ).put ( v1, p11 ); // invoke-virtual {v0, v1, p11}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 829 */
final String v1 = "networkType"; // const-string v1, "networkType"
java.lang.Integer .valueOf ( p9 );
(( java.util.TreeMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 830 */
v1 = android.text.TextUtils .isEmpty ( p12 );
/* if-nez v1, :cond_0 */
/* .line 831 */
/* const-string/jumbo v1, "sku" */
(( java.util.TreeMap ) v0 ).put ( v1, p12 ); // invoke-virtual {v0, v1, p12}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 834 */
} // :cond_0
} // .end method
private void getPreloadAppInfo ( ) {
/* .locals 17 */
/* .line 687 */
/* move-object/from16 v1, p0 */
final String v2 = "request_connect_exception"; // const-string v2, "request_connect_exception"
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->getConnectEntity()Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity; */
/* .line 688 */
/* .local v3, "entity":Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity; */
/* if-nez v3, :cond_0 */
/* .line 689 */
return;
/* .line 691 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAddress()Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.AdvancePreinstallService$ConnectEntity ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 692 */
/* .local v4, "url":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .line 693 */
/* .local v5, "conn":Ljava/net/HttpURLConnection; */
int v6 = 0; // const/4 v6, 0x0
/* .line 696 */
/* .local v6, "br":Ljava/io/BufferedReader; */
try { // :try_start_0
final String v0 = "request_connect_start"; // const-string v0, "request_connect_start"
/* invoke-direct {v1, v0}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 698 */
/* new-instance v0, Ljava/net/URL; */
/* invoke-direct {v0, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V */
(( java.net.URL ) v0 ).openConnection ( ); // invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
/* check-cast v0, Ljava/net/HttpURLConnection; */
/* move-object v5, v0 */
/* .line 699 */
/* const/16 v0, 0x7d0 */
(( java.net.HttpURLConnection ) v5 ).setConnectTimeout ( v0 ); // invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V
/* .line 700 */
(( java.net.HttpURLConnection ) v5 ).setReadTimeout ( v0 ); // invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
/* .line 701 */
final String v0 = "GET"; // const-string v0, "GET"
(( java.net.HttpURLConnection ) v5 ).setRequestMethod ( v0 ); // invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
/* .line 702 */
(( java.net.HttpURLConnection ) v5 ).connect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V
/* .line 704 */
v0 = (( java.net.HttpURLConnection ) v5 ).getResponseCode ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I
/* :try_end_0 */
/* .catch Ljava/net/ProtocolException; {:try_start_0 ..:try_end_0} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 705 */
/* .local v0, "responeCode":I */
/* const/16 v7, 0xc8 */
final String v8 = "AdvancePreinstallService"; // const-string v8, "AdvancePreinstallService"
/* if-ne v0, v7, :cond_9 */
/* .line 706 */
try { // :try_start_1
final String v7 = "request_connect_success"; // const-string v7, "request_connect_success"
/* invoke-direct {v1, v7}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 707 */
/* new-instance v7, Ljava/io/BufferedReader; */
/* new-instance v9, Ljava/io/InputStreamReader; */
(( java.net.HttpURLConnection ) v5 ).getInputStream ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
/* invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* const/16 v10, 0x400 */
/* invoke-direct {v7, v9, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V */
/* move-object v6, v7 */
/* .line 708 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 710 */
/* .local v7, "sb":Ljava/lang/StringBuilder; */
} // :goto_0
(( java.io.BufferedReader ) v6 ).readLine ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v10, v9 */
/* .local v10, "line":Ljava/lang/String; */
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 711 */
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 714 */
} // :cond_1
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "result:"; // const-string v11, "result:"
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v8,v9 );
/* .line 716 */
/* new-instance v9, Lorg/json/JSONObject; */
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v9, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 718 */
/* .local v9, "result":Lorg/json/JSONObject; */
final String v11 = "code"; // const-string v11, "code"
v11 = (( org.json.JSONObject ) v9 ).getInt ( v11 ); // invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 719 */
/* .local v11, "code":I */
final String v12 = "message"; // const-string v12, "message"
(( org.json.JSONObject ) v9 ).getString ( v12 ); // invoke-virtual {v9, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 720 */
/* .local v12, "message":Ljava/lang/String; */
/* if-nez v11, :cond_8 */
final String v13 = "Success"; // const-string v13, "Success"
v13 = (( java.lang.String ) v13 ).equals ( v12 ); // invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_8
/* .line 721 */
final String v8 = "request_list_success"; // const-string v8, "request_list_success"
/* invoke-direct {v1, v8}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 722 */
final String v8 = "data"; // const-string v8, "data"
(( org.json.JSONObject ) v9 ).getJSONObject ( v8 ); // invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
/* :try_end_1 */
/* .catch Ljava/net/ProtocolException; {:try_start_1 ..:try_end_1} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_2 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 723 */
/* .local v8, "data":Lorg/json/JSONObject; */
/* const-string/jumbo v13, "uninstall_list_empty" */
/* if-nez v8, :cond_3 */
/* .line 724 */
try { // :try_start_2
/* invoke-direct {v1, v13}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 725 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAllInstallSuccess()V */
/* :try_end_2 */
/* .catch Ljava/net/ProtocolException; {:try_start_2 ..:try_end_2} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .catch Lorg/json/JSONException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 769 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 770 */
(( java.net.HttpURLConnection ) v5 ).disconnect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 772 */
} // :cond_2
/* nop */
/* .line 773 */
/* invoke-direct {v1, v6}, Lcom/android/server/pm/AdvancePreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 726 */
return;
/* .line 728 */
} // :cond_3
try { // :try_start_3
final String v14 = "previewOfflineApps"; // const-string v14, "previewOfflineApps"
(( org.json.JSONObject ) v8 ).getJSONArray ( v14 ); // invoke-virtual {v8, v14}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 729 */
/* .local v14, "previewOfflineApps":Lorg/json/JSONArray; */
final String v15 = "adOfflineApps"; // const-string v15, "adOfflineApps"
(( org.json.JSONObject ) v8 ).getJSONArray ( v15 ); // invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 731 */
/* .local v15, "adOfflineApps":Lorg/json/JSONArray; */
if ( v14 != null) { // if-eqz v14, :cond_4
v16 = (( org.json.JSONArray ) v14 ).length ( ); // invoke-virtual {v14}, Lorg/json/JSONArray;->length()I
/* if-nez v16, :cond_5 */
} // :cond_4
if ( v15 != null) { // if-eqz v15, :cond_6
/* .line 732 */
v16 = (( org.json.JSONArray ) v15 ).length ( ); // invoke-virtual {v15}, Lorg/json/JSONArray;->length()I
/* if-nez v16, :cond_5 */
/* .line 738 */
} // :cond_5
final String v13 = "miuiAdvancePreload"; // const-string v13, "miuiAdvancePreload"
/* invoke-direct {v1, v14, v13}, Lcom/android/server/pm/AdvancePreinstallService;->handleOfflineApps(Lorg/json/JSONArray;Ljava/lang/String;)V */
/* .line 739 */
final String v13 = "miuiAdvertisement"; // const-string v13, "miuiAdvertisement"
/* invoke-direct {v1, v15, v13}, Lcom/android/server/pm/AdvancePreinstallService;->handleOfflineApps(Lorg/json/JSONArray;Ljava/lang/String;)V */
/* .line 740 */
/* invoke-direct {v1, v14}, Lcom/android/server/pm/AdvancePreinstallService;->handleAdvancePreloadTrack(Lorg/json/JSONArray;)V */
/* .line 741 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAdOnlineInfo()V */
/* .line 742 */
} // .end local v8 # "data":Lorg/json/JSONObject;
} // .end local v14 # "previewOfflineApps":Lorg/json/JSONArray;
} // .end local v15 # "adOfflineApps":Lorg/json/JSONArray;
/* .line 733 */
/* .restart local v8 # "data":Lorg/json/JSONObject; */
/* .restart local v14 # "previewOfflineApps":Lorg/json/JSONArray; */
/* .restart local v15 # "adOfflineApps":Lorg/json/JSONArray; */
} // :cond_6
} // :goto_1
/* invoke-direct {v1, v13}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 734 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAllInstallSuccess()V */
/* :try_end_3 */
/* .catch Ljava/net/ProtocolException; {:try_start_3 ..:try_end_3} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .catch Lorg/json/JSONException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 769 */
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 770 */
(( java.net.HttpURLConnection ) v5 ).disconnect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 772 */
} // :cond_7
/* nop */
/* .line 773 */
/* invoke-direct {v1, v6}, Lcom/android/server/pm/AdvancePreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 735 */
return;
/* .line 743 */
} // .end local v8 # "data":Lorg/json/JSONObject;
} // .end local v14 # "previewOfflineApps":Lorg/json/JSONArray;
} // .end local v15 # "adOfflineApps":Lorg/json/JSONArray;
} // :cond_8
try { // :try_start_4
final String v13 = "request_list_failed"; // const-string v13, "request_list_failed"
/* invoke-direct {v1, v13}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 744 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V */
/* .line 745 */
final String v13 = "advance_request result is failed"; // const-string v13, "advance_request result is failed"
android.util.Slog .e ( v8,v13 );
/* .line 747 */
} // .end local v7 # "sb":Ljava/lang/StringBuilder;
} // .end local v9 # "result":Lorg/json/JSONObject;
} // .end local v10 # "line":Ljava/lang/String;
} // .end local v11 # "code":I
} // .end local v12 # "message":Ljava/lang/String;
} // :goto_2
/* .line 748 */
} // :cond_9
final String v7 = "request_connect_failed"; // const-string v7, "request_connect_failed"
/* invoke-direct {v1, v7}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 749 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V */
/* .line 750 */
/* const-string/jumbo v7, "server can not connected" */
android.util.Slog .i ( v8,v7 );
/* :try_end_4 */
/* .catch Ljava/net/ProtocolException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catch Lorg/json/JSONException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 769 */
} // .end local v0 # "responeCode":I
} // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 770 */
(( java.net.HttpURLConnection ) v5 ).disconnect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 772 */
} // :cond_a
if ( v6 != null) { // if-eqz v6, :cond_f
/* .line 773 */
} // :goto_4
/* invoke-direct {v1, v6}, Lcom/android/server/pm/AdvancePreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 769 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 764 */
/* :catch_0 */
/* move-exception v0 */
/* .line 765 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_5
/* invoke-direct {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 766 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V */
/* .line 767 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 769 */
} // .end local v0 # "e":Ljava/lang/Exception;
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 770 */
(( java.net.HttpURLConnection ) v5 ).disconnect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 772 */
} // :cond_b
if ( v6 != null) { // if-eqz v6, :cond_f
/* .line 773 */
/* .line 760 */
/* :catch_1 */
/* move-exception v0 */
/* .line 761 */
/* .local v0, "e":Lorg/json/JSONException; */
try { // :try_start_6
final String v2 = "json_exception"; // const-string v2, "json_exception"
/* invoke-direct {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 762 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V */
/* .line 763 */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 769 */
} // .end local v0 # "e":Lorg/json/JSONException;
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 770 */
(( java.net.HttpURLConnection ) v5 ).disconnect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 772 */
} // :cond_c
if ( v6 != null) { // if-eqz v6, :cond_f
/* .line 773 */
/* .line 756 */
/* :catch_2 */
/* move-exception v0 */
/* .line 757 */
/* .local v0, "e":Ljava/io/IOException; */
try { // :try_start_7
/* invoke-direct {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 758 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V */
/* .line 759 */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_0 */
/* .line 769 */
} // .end local v0 # "e":Ljava/io/IOException;
if ( v5 != null) { // if-eqz v5, :cond_d
/* .line 770 */
(( java.net.HttpURLConnection ) v5 ).disconnect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 772 */
} // :cond_d
if ( v6 != null) { // if-eqz v6, :cond_f
/* .line 773 */
/* .line 752 */
/* :catch_3 */
/* move-exception v0 */
/* .line 753 */
/* .local v0, "e":Ljava/net/ProtocolException; */
try { // :try_start_8
/* invoke-direct {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V */
/* .line 754 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V */
/* .line 755 */
(( java.net.ProtocolException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/net/ProtocolException;->printStackTrace()V
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_0 */
/* .line 769 */
} // .end local v0 # "e":Ljava/net/ProtocolException;
if ( v5 != null) { // if-eqz v5, :cond_e
/* .line 770 */
(( java.net.HttpURLConnection ) v5 ).disconnect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 772 */
} // :cond_e
if ( v6 != null) { // if-eqz v6, :cond_f
/* .line 773 */
/* .line 776 */
} // :cond_f
} // :goto_5
return;
/* .line 769 */
} // :goto_6
if ( v5 != null) { // if-eqz v5, :cond_10
/* .line 770 */
(( java.net.HttpURLConnection ) v5 ).disconnect ( ); // invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
/* .line 772 */
} // :cond_10
if ( v6 != null) { // if-eqz v6, :cond_11
/* .line 773 */
/* invoke-direct {v1, v6}, Lcom/android/server/pm/AdvancePreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V */
/* .line 775 */
} // :cond_11
/* throw v0 */
} // .end method
private java.lang.String getSku ( ) {
/* .locals 5 */
/* .line 653 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
final String v1 = ""; // const-string v1, ""
/* if-nez v0, :cond_0 */
/* .line 654 */
/* .line 656 */
} // :cond_0
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
android.os.SystemProperties .get ( v0,v1 );
/* .line 657 */
/* .local v0, "buildRegion":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
final String v2 = "MI"; // const-string v2, "MI"
/* if-nez v1, :cond_2 */
/* .line 658 */
(( java.lang.String ) v0 ).toUpperCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
final String v3 = "GLOBAL"; // const-string v3, "GLOBAL"
v1 = android.text.TextUtils .equals ( v1,v3 );
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_1
(( java.lang.String ) v0 ).toUpperCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
} // :goto_0
/* .line 660 */
} // :cond_2
/* sget-boolean v1, Lmiui/os/Build;->IS_STABLE_VERSION:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 661 */
v1 = android.os.Build$VERSION.INCREMENTAL;
/* .line 662 */
/* .local v1, "buildVersion":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v3, :cond_3 */
v3 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
int v4 = 4; // const/4 v4, 0x4
/* if-le v3, v4, :cond_3 */
/* .line 663 */
v2 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* sub-int/2addr v2, v4 */
v3 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
/* add-int/lit8 v3, v3, -0x2 */
(( java.lang.String ) v1 ).substring ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.String ) v2 ).toUpperCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 666 */
} // .end local v1 # "buildVersion":Ljava/lang/String;
} // :cond_3
} // .end method
private void handleAdOnlineInfo ( ) {
/* .locals 3 */
/* .line 489 */
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/pm/AdvancePreinstallService;->isAdOnlineServiceAvailable(Landroid/content/Context;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 490 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 491 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.xiaomi.preload"; // const-string v1, "com.xiaomi.preload"
final String v2 = "com.xiaomi.preload.services.AdvancePreInstallService"; // const-string v2, "com.xiaomi.preload.services.AdvancePreInstallService"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 492 */
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
(( android.content.Context ) v1 ).startForegroundService ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 494 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
return;
} // .end method
private void handleAdvancePreinstallApps ( ) {
/* .locals 1 */
/* .line 290 */
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
v0 = com.android.server.pm.AdvancePreinstallService .isProvisioned ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 291 */
return;
/* .line 293 */
} // :cond_0
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPackageManager = v0;
/* .line 294 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getIMEIMD5()Ljava/lang/String; */
this.mImeiMe5 = v0;
/* .line 295 */
/* new-instance v0, Lcom/android/server/pm/AdvancePreinstallService$3; */
/* invoke-direct {v0, p0}, Lcom/android/server/pm/AdvancePreinstallService$3;-><init>(Lcom/android/server/pm/AdvancePreinstallService;)V */
android.os.AsyncTask .execute ( v0 );
/* .line 307 */
return;
} // .end method
private void handleAdvancePreinstallAppsDelay ( ) {
/* .locals 4 */
/* .line 280 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->initOneTrack()V */
/* .line 281 */
/* new-instance v0, Landroid/os/Handler; */
/* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
/* new-instance v1, Lcom/android/server/pm/AdvancePreinstallService$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/pm/AdvancePreinstallService$2;-><init>(Lcom/android/server/pm/AdvancePreinstallService;)V */
/* const-wide/16 v2, 0x1f4 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 287 */
return;
} // .end method
private void handleAdvancePreloadTrack ( org.json.JSONArray p0 ) {
/* .locals 7 */
/* .param p1, "array" # Lorg/json/JSONArray; */
/* .line 522 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAdvanceApps()Ljava/util/Map; */
/* .line 523 */
/* .local v0, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_5
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 524 */
/* .local v2, "entry":Ljava/util/Map$Entry; */
/* if-nez v2, :cond_0 */
/* .line 525 */
/* .line 528 */
} // :cond_0
/* check-cast v3, Ljava/lang/String; */
/* .line 529 */
/* .local v3, "path":Ljava/lang/String; */
v4 = /* invoke-direct {p0, v3}, Lcom/android/server/pm/AdvancePreinstallService;->isAdvanceApps(Ljava/lang/String;)Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 530 */
/* check-cast v4, Ljava/lang/String; */
/* .line 531 */
/* .local v4, "existPkgName":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .line 532 */
/* .local v5, "containsPkgName":Z */
v6 = this.mMiuiPreinstallHelper;
v6 = (( com.android.server.pm.MiuiPreinstallHelper ) v6 ).isSupportNewFrame ( ); // invoke-virtual {v6}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 533 */
v6 = com.android.server.pm.MiuiBusinessPreinstallConfig.sCloudControlUninstall;
v5 = (( java.util.ArrayList ) v6 ).contains ( v4 ); // invoke-virtual {v6, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* .line 535 */
} // :cond_1
v6 = com.android.server.pm.PreinstallApp.sCloudControlUninstall;
v5 = (( java.util.ArrayList ) v6 ).contains ( v4 ); // invoke-virtual {v6, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* .line 537 */
} // :goto_1
/* sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v6 != null) { // if-eqz v6, :cond_2
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 538 */
} // :cond_2
v6 = /* invoke-direct {p0, v4, p1}, Lcom/android/server/pm/AdvancePreinstallService;->isPackageToRemove(Ljava/lang/String;Lorg/json/JSONArray;)Z */
/* if-nez v6, :cond_4 */
v6 = this.mPackageManager;
v6 = (( com.android.server.pm.AdvancePreinstallService ) p0 ).exists ( v6, v4 ); // invoke-virtual {p0, v6, v4}, Lcom/android/server/pm/AdvancePreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 540 */
} // :cond_3
/* invoke-direct {p0, v4}, Lcom/android/server/pm/AdvancePreinstallService;->trackAdvancePreloadSuccess(Ljava/lang/String;)V */
/* .line 543 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;
} // .end local v3 # "path":Ljava/lang/String;
} // .end local v4 # "existPkgName":Ljava/lang/String;
} // .end local v5 # "containsPkgName":Z
} // :cond_4
/* .line 544 */
} // :cond_5
return;
} // .end method
private void handleAllInstallSuccess ( ) {
/* .locals 6 */
/* .line 547 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAdvanceApps()Ljava/util/Map; */
/* .line 548 */
/* .local v0, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 549 */
/* .local v2, "entry":Ljava/util/Map$Entry; */
/* if-nez v2, :cond_0 */
/* .line 550 */
/* .line 553 */
} // :cond_0
/* check-cast v3, Ljava/lang/String; */
/* .line 554 */
/* .local v3, "path":Ljava/lang/String; */
v4 = /* invoke-direct {p0, v3}, Lcom/android/server/pm/AdvancePreinstallService;->isAdvanceApps(Ljava/lang/String;)Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 555 */
/* check-cast v4, Ljava/lang/String; */
/* .line 556 */
/* .local v4, "existPkgName":Ljava/lang/String; */
/* sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v5, :cond_1 */
v5 = this.mPackageManager;
v5 = (( com.android.server.pm.AdvancePreinstallService ) p0 ).exists ( v5, v4 ); // invoke-virtual {p0, v5, v4}, Lcom/android/server/pm/AdvancePreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 558 */
} // :cond_1
/* invoke-direct {p0, v4}, Lcom/android/server/pm/AdvancePreinstallService;->trackAdvancePreloadSuccess(Ljava/lang/String;)V */
/* .line 561 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;
} // .end local v3 # "path":Ljava/lang/String;
} // .end local v4 # "existPkgName":Ljava/lang/String;
} // :cond_2
/* .line 562 */
} // :cond_3
return;
} // .end method
private void handleOfflineApps ( org.json.JSONArray p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "array" # Lorg/json/JSONArray; */
/* .param p2, "tag" # Ljava/lang/String; */
/* .line 461 */
if ( p1 != null) { // if-eqz p1, :cond_5
v0 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-nez v0, :cond_0 */
/* .line 464 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* if-ge v0, v1, :cond_4 */
/* .line 466 */
try { // :try_start_0
(( org.json.JSONArray ) p1 ).getJSONObject ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 467 */
/* .local v1, "appInfo":Lorg/json/JSONObject; */
/* if-nez v1, :cond_1 */
/* .line 468 */
/* .line 470 */
} // :cond_1
final String v2 = "packageName"; // const-string v2, "packageName"
(( org.json.JSONObject ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 471 */
/* .local v2, "pkgName":Ljava/lang/String; */
final String v3 = "confId"; // const-string v3, "confId"
v3 = (( org.json.JSONObject ) v1 ).getInt ( v3 ); // invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 472 */
/* .local v3, "confId":I */
final String v4 = "offlineCount"; // const-string v4, "offlineCount"
v4 = (( org.json.JSONObject ) v1 ).getInt ( v4 ); // invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* .line 473 */
/* .local v4, "offlineCount":I */
/* sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 474 */
/* invoke-direct {p0, v2, v3, v4, p2}, Lcom/android/server/pm/AdvancePreinstallService;->recordUnInstallApps(Ljava/lang/String;IILjava/lang/String;)V */
/* .line 475 */
/* invoke-direct {p0, v2}, Lcom/android/server/pm/AdvancePreinstallService;->findTrackingApk(Ljava/lang/String;)Ljava/lang/String; */
/* .line 476 */
/* .local v5, "trackingPkg":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v6, :cond_2 */
/* .line 477 */
final String v6 = ""; // const-string v6, ""
/* invoke-direct {p0, v5, v3, v4, v6}, Lcom/android/server/pm/AdvancePreinstallService;->recordUnInstallApps(Ljava/lang/String;IILjava/lang/String;)V */
/* .line 479 */
} // .end local v5 # "trackingPkg":Ljava/lang/String;
} // :cond_2
/* .line 480 */
} // :cond_3
/* invoke-direct {p0, v2, v3, v4, p2}, Lcom/android/server/pm/AdvancePreinstallService;->doUninstallApps(Ljava/lang/String;IILjava/lang/String;)V */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 484 */
} // .end local v1 # "appInfo":Lorg/json/JSONObject;
} // .end local v2 # "pkgName":Ljava/lang/String;
} // .end local v3 # "confId":I
} // .end local v4 # "offlineCount":I
} // :goto_1
/* .line 482 */
/* :catch_0 */
/* move-exception v1 */
/* .line 483 */
/* .local v1, "e":Lorg/json/JSONException; */
final String v2 = "AdvancePreinstallService"; // const-string v2, "AdvancePreinstallService"
(( org.json.JSONException ) v1 ).getMessage ( ); // invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 464 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_2
/* add-int/lit8 v0, v0, 0x1 */
/* .line 486 */
} // .end local v0 # "i":I
} // :cond_4
return;
/* .line 462 */
} // :cond_5
} // :goto_3
return;
} // .end method
private void initOneTrack ( ) {
/* .locals 2 */
/* .line 642 */
/* new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack; */
/* invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack;-><init>()V */
this.mTrack = v0;
/* .line 643 */
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
(( com.android.server.pm.PreInstallServiceTrack ) v0 ).bindTrackService ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/pm/PreInstallServiceTrack;->bindTrackService(Landroid/content/Context;)V
/* .line 644 */
return;
} // .end method
private Boolean isAdOnlineServiceAvailable ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 589 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 590 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.xiaomi.preload"; // const-string v1, "com.xiaomi.preload"
final String v2 = "com.xiaomi.preload.services.AdvancePreInstallService"; // const-string v2, "com.xiaomi.preload.services.AdvancePreInstallService"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 591 */
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 592 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 593 */
/* .line 595 */
} // :cond_0
(( android.content.pm.PackageManager ) v1 ).queryIntentServices ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
/* .line 596 */
v4 = /* .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
/* if-lez v4, :cond_1 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_1
} // .end method
private Boolean isAdvanceApps ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .line 256 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "miuiAdvancePreload"; // const-string v0, "miuiAdvancePreload"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isAdvertApps ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "apkPath" # Ljava/lang/String; */
/* .line 265 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "miuiAdvertisement"; // const-string v0, "miuiAdvertisement"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isApkFile ( java.io.File p0 ) {
/* .locals 2 */
/* .param p1, "apkFile" # Ljava/io/File; */
/* .line 206 */
if ( p1 != null) { // if-eqz p1, :cond_0
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v1 = ".apk"; // const-string v1, ".apk"
v0 = (( java.lang.String ) v0 ).endsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isNetworkConnected ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 314 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) p1 ).getSystemService ( v1 ); // invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/net/ConnectivityManager; */
/* .line 315 */
/* .local v1, "connectivityManager":Landroid/net/ConnectivityManager; */
(( android.net.ConnectivityManager ) v1 ).getActiveNetworkInfo ( ); // invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
/* .line 316 */
/* .local v2, "networkInfo":Landroid/net/NetworkInfo; */
/* if-nez v2, :cond_0 */
/* .line 317 */
/* .line 319 */
} // :cond_0
v3 = (( android.net.NetworkInfo ) v2 ).getType ( ); // invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I
/* .line 320 */
/* .local v3, "type":I */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_1 */
/* .line 321 */
/* iput-boolean v4, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsWifiConnected:Z */
/* .line 323 */
} // :cond_1
v4 = (( android.net.NetworkInfo ) v2 ).isAvailable ( ); // invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z
/* iput-boolean v4, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsNetworkConnected:Z */
/* .line 324 */
(( android.net.NetworkInfo ) v2 ).getTypeName ( ); // invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;
this.mNetworkTypeName = v4;
/* .line 325 */
/* iget-boolean v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsNetworkConnected:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 326 */
} // .end local v1 # "connectivityManager":Landroid/net/ConnectivityManager;
} // .end local v2 # "networkInfo":Landroid/net/NetworkInfo;
} // .end local v3 # "type":I
/* :catch_0 */
/* move-exception v1 */
/* .line 327 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 329 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
private Boolean isPackageToRemove ( java.lang.String p0, org.json.JSONArray p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "array" # Lorg/json/JSONArray; */
/* .line 565 */
int v0 = 0; // const/4 v0, 0x0
if ( p2 != null) { // if-eqz p2, :cond_4
v1 = (( org.json.JSONArray ) p2 ).length ( ); // invoke-virtual {p2}, Lorg/json/JSONArray;->length()I
/* if-nez v1, :cond_0 */
/* .line 568 */
} // :cond_0
v1 = (( org.json.JSONArray ) p2 ).length ( ); // invoke-virtual {p2}, Lorg/json/JSONArray;->length()I
/* .line 569 */
/* .local v1, "deleteCount":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v1, :cond_3 */
/* .line 571 */
try { // :try_start_0
(( org.json.JSONArray ) p2 ).getJSONObject ( v2 ); // invoke-virtual {p2, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 572 */
/* .local v3, "appInfo":Lorg/json/JSONObject; */
/* if-nez v3, :cond_1 */
/* .line 573 */
/* .line 576 */
} // :cond_1
final String v4 = "packageName"; // const-string v4, "packageName"
(( org.json.JSONObject ) v3 ).getString ( v4 ); // invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 577 */
/* .local v4, "pkgName":Ljava/lang/String; */
v5 = android.text.TextUtils .equals ( v4,p1 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 579 */
int v0 = 1; // const/4 v0, 0x1
/* .line 583 */
} // .end local v3 # "appInfo":Lorg/json/JSONObject;
} // .end local v4 # "pkgName":Ljava/lang/String;
} // :cond_2
/* .line 581 */
/* :catch_0 */
/* move-exception v3 */
/* .line 582 */
/* .local v3, "e":Lorg/json/JSONException; */
final String v4 = "AdvancePreinstallService"; // const-string v4, "AdvancePreinstallService"
(( org.json.JSONException ) v3 ).getMessage ( ); // invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* .line 569 */
} // .end local v3 # "e":Lorg/json/JSONException;
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 585 */
} // .end local v2 # "i":I
} // :cond_3
/* .line 566 */
} // .end local v1 # "deleteCount":I
} // :cond_4
} // :goto_2
} // .end method
public static Boolean isProvisioned ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 848 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 849 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
int v2 = 0; // const/4 v2, 0x0
v1 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
} // .end method
private android.content.pm.parsing.PackageLite parsePackageLite ( java.io.File p0 ) {
/* .locals 5 */
/* .param p1, "apkFile" # Ljava/io/File; */
/* .line 210 */
android.content.pm.parsing.result.ParseTypeImpl .forDefaultParsing ( );
/* .line 211 */
/* .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl; */
/* nop */
/* .line 212 */
(( android.content.pm.parsing.result.ParseTypeImpl ) v0 ).reset ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/result/ParseTypeImpl;->reset()Landroid/content/pm/parsing/result/ParseInput;
/* .line 211 */
int v2 = 0; // const/4 v2, 0x0
android.content.pm.parsing.ApkLiteParseUtils .parsePackageLite ( v1,p1,v2 );
/* .line 213 */
v2 = /* .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/PackageLite;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 214 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to parsePackageLite: "; // const-string v3, "Failed to parsePackageLite: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " error: "; // const-string v3, " error: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 215 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 214 */
final String v4 = "AdvancePreinstallService"; // const-string v4, "AdvancePreinstallService"
android.util.Slog .e ( v4,v2,v3 );
/* .line 216 */
int v2 = 0; // const/4 v2, 0x0
/* .line 218 */
} // :cond_0
/* check-cast v2, Landroid/content/pm/parsing/PackageLite; */
} // .end method
private void recordUnInstallApps ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "confId" # I */
/* .param p3, "offlineCount" # I */
/* .param p4, "appType" # Ljava/lang/String; */
/* .line 445 */
final String v1 = "remove_from_list_begin"; // const-string v1, "remove_from_list_begin"
/* move-object v0, p0 */
/* move-object v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move-object v5, p4 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 446 */
final String v3 = "begin_uninstall"; // const-string v3, "begin_uninstall"
/* move-object v2, p0 */
/* move-object v4, p1 */
/* move v5, p2 */
/* move v6, p3 */
/* move-object v7, p4 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 447 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 448 */
v0 = com.android.server.pm.MiuiBusinessPreinstallConfig.sCloudControlUninstall;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 450 */
} // :cond_0
v0 = com.android.server.pm.PreinstallApp.sCloudControlUninstall;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 452 */
} // :goto_0
/* const-string/jumbo v2, "uninstall_success" */
/* move-object v1, p0 */
/* move-object v3, p1 */
/* move v4, p2 */
/* move v5, p3 */
/* move-object v6, p4 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V */
/* .line 453 */
return;
} // .end method
private void track ( com.android.server.pm.PreInstallServiceTrack$Action p0 ) {
/* .locals 3 */
/* .param p1, "action" # Lcom/android/server/pm/PreInstallServiceTrack$Action; */
/* .line 647 */
v0 = this.mTrack;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
(( com.android.server.pm.PreInstallServiceTrack$Action ) p1 ).getContent ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 648 */
v0 = this.mTrack;
(( com.android.server.pm.PreInstallServiceTrack$Action ) p1 ).getContent ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;
(( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.pm.PreInstallServiceTrack ) v0 ).trackEvent ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack;->trackEvent(Ljava/lang/String;I)V
/* .line 650 */
} // :cond_0
return;
} // .end method
private void trackAdvancePreloadSuccess ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 497 */
final String v0 = "install_success"; // const-string v0, "install_success"
/* .line 498 */
/* .local v0, "eventName":Ljava/lang/String; */
/* new-instance v1, Lcom/android/server/pm/PreInstallServiceTrack$Action; */
/* invoke-direct {v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V */
/* .line 499 */
/* .local v1, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action; */
final String v2 = "request_type"; // const-string v2, "request_type"
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 500 */
final String v2 = "imeiMd5"; // const-string v2, "imeiMd5"
v4 = this.mImeiMe5;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 501 */
final String v2 = "packageName"; // const-string v2, "packageName"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 502 */
final String v2 = "networkType"; // const-string v2, "networkType"
v4 = this.mNetworkTypeName;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 503 */
final String v2 = "appType"; // const-string v2, "appType"
final String v4 = "miuiAdvancePreload"; // const-string v4, "miuiAdvancePreload"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 504 */
final String v2 = "saleschannels"; // const-string v2, "saleschannels"
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getChannel()Ljava/lang/String; */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 505 */
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
com.android.id.IdentifierManager .getOAID ( v2 );
final String v4 = "imId"; // const-string v4, "imId"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 507 */
/* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* xor-int/2addr v2, v3 */
/* .line 508 */
/* .local v2, "isCn":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
final String v3 = "CN"; // const-string v3, "CN"
} // :cond_0
miui.os.Build .getCustVariant ( );
(( java.lang.String ) v3 ).toUpperCase ( ); // invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 509 */
/* .local v3, "region":Ljava/lang/String; */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getSku()Ljava/lang/String; */
/* .line 510 */
/* .local v4, "sku":Ljava/lang/String; */
final String v5 = "region"; // const-string v5, "region"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v5, v3 ); // invoke-virtual {v1, v5, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 511 */
/* const-string/jumbo v5, "sku" */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v5, v4 ); // invoke-virtual {v1, v5, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 512 */
final String v5 = "EVENT_NAME"; // const-string v5, "EVENT_NAME"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v1 ).addParam ( v5, v0 ); // invoke-virtual {v1, v5, v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 513 */
/* invoke-direct {p0, v1}, Lcom/android/server/pm/AdvancePreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V */
/* .line 514 */
return;
} // .end method
private void trackEvent ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "event" # Ljava/lang/String; */
/* .line 625 */
/* new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$Action; */
/* invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V */
/* .line 626 */
/* .local v0, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action; */
final String v1 = "request_type"; // const-string v1, "request_type"
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 627 */
final String v1 = "imeiMd5"; // const-string v1, "imeiMd5"
v3 = this.mImeiMe5;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 628 */
final String v1 = "networkType"; // const-string v1, "networkType"
v3 = this.mNetworkTypeName;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 629 */
final String v1 = "saleschannels"; // const-string v1, "saleschannels"
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getChannel()Ljava/lang/String; */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 630 */
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
com.android.id.IdentifierManager .getOAID ( v1 );
final String v3 = "imId"; // const-string v3, "imId"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 632 */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* xor-int/2addr v1, v2 */
/* .line 633 */
/* .local v1, "isCn":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v2 = "CN"; // const-string v2, "CN"
} // :cond_0
miui.os.Build .getCustVariant ( );
(( java.lang.String ) v2 ).toUpperCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 634 */
/* .local v2, "region":Ljava/lang/String; */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getSku()Ljava/lang/String; */
/* .line 635 */
/* .local v3, "sku":Ljava/lang/String; */
final String v4 = "region"; // const-string v4, "region"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 636 */
/* const-string/jumbo v4, "sku" */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 637 */
final String v4 = "EVENT_NAME"; // const-string v4, "EVENT_NAME"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, p1 ); // invoke-virtual {v0, v4, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 638 */
/* invoke-direct {p0, v0}, Lcom/android/server/pm/AdvancePreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V */
/* .line 639 */
return;
} // .end method
private void uninstallAdvancePreinstallApps ( ) {
/* .locals 7 */
/* .line 233 */
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAdvanceApps()Ljava/util/Map; */
/* .line 234 */
/* .local v0, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "advance app size:"; // const-string v2, "advance app size:"
v2 = (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AdvancePreinstallService"; // const-string v2, "AdvancePreinstallService"
android.util.Slog .d ( v2,v1 );
/* .line 235 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 236 */
/* .local v2, "entry":Ljava/util/Map$Entry; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 237 */
/* check-cast v3, Ljava/lang/String; */
/* .line 238 */
/* .local v3, "path":Ljava/lang/String; */
v4 = /* invoke-direct {p0, v3}, Lcom/android/server/pm/AdvancePreinstallService;->isAdvanceApps(Ljava/lang/String;)Z */
if ( v4 != null) { // if-eqz v4, :cond_0
final String v4 = "miuiAdvancePreload"; // const-string v4, "miuiAdvancePreload"
} // :cond_0
final String v4 = "miuiAdvertisement"; // const-string v4, "miuiAdvertisement"
/* .line 240 */
/* .local v4, "appType":Ljava/lang/String; */
} // :goto_1
/* sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v6 = -1; // const/4 v6, -0x1
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 241 */
/* check-cast v5, Ljava/lang/String; */
/* .line 242 */
/* .local v5, "packageName":Ljava/lang/String; */
/* invoke-direct {p0, v5, v6, v6, v4}, Lcom/android/server/pm/AdvancePreinstallService;->recordUnInstallApps(Ljava/lang/String;IILjava/lang/String;)V */
/* .line 243 */
} // .end local v5 # "packageName":Ljava/lang/String;
/* .line 244 */
} // :cond_1
/* check-cast v5, Ljava/lang/String; */
/* invoke-direct {p0, v5, v6, v6, v4}, Lcom/android/server/pm/AdvancePreinstallService;->doUninstallApps(Ljava/lang/String;IILjava/lang/String;)V */
/* .line 247 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;
} // .end local v3 # "path":Ljava/lang/String;
} // .end local v4 # "appType":Ljava/lang/String;
} // :cond_2
} // :goto_2
/* .line 248 */
} // :cond_3
return;
} // .end method
/* # virtual methods */
public Boolean exists ( android.content.pm.PackageManager p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pm" # Landroid/content/pm/PackageManager; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 356 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
v1 = android.text.TextUtils .isEmpty ( p2 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 360 */
} // :cond_0
/* const/16 v1, 0x80 */
try { // :try_start_0
(( android.content.pm.PackageManager ) p1 ).getApplicationInfo ( p2, v1 ); // invoke-virtual {p1, p2, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v1 != null) { // if-eqz v1, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 362 */
/* :catch_0 */
/* move-exception v1 */
/* .line 363 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* .line 357 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :cond_2
} // :goto_0
} // .end method
public java.lang.String getFileContent ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "filePath" # Ljava/lang/String; */
/* .line 431 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 432 */
/* .local v0, "file":Ljava/io/File; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 433 */
/* .local v1, "in":Ljava/io/FileInputStream; */
try { // :try_start_1
(( java.io.File ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/io/File;->length()J
/* move-result-wide v2 */
/* .line 434 */
/* .local v2, "length":J */
/* long-to-int v4, v2 */
/* new-array v4, v4, [B */
/* .line 435 */
/* .local v4, "content":[B */
(( java.io.FileInputStream ) v1 ).read ( v4 ); // invoke-virtual {v1, v4}, Ljava/io/FileInputStream;->read([B)I
/* .line 436 */
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* .line 437 */
/* new-instance v5, Ljava/lang/String; */
v6 = java.nio.charset.StandardCharsets.UTF_8;
/* invoke-direct {v5, v4, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 438 */
try { // :try_start_2
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 437 */
/* .line 432 */
} // .end local v2 # "length":J
} // .end local v4 # "content":[B
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // .end local p0 # "this":Lcom/android/server/pm/AdvancePreinstallService;
} // .end local p1 # "filePath":Ljava/lang/String;
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 438 */
} // .end local v1 # "in":Ljava/io/FileInputStream;
/* .restart local v0 # "file":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/android/server/pm/AdvancePreinstallService; */
/* .restart local p1 # "filePath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v1 */
/* .line 439 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "AdvancePreinstallService"; // const-string v2, "AdvancePreinstallService"
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 441 */
} // .end local v1 # "e":Ljava/lang/Exception;
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "phase" # I */
/* .line 155 */
/* const/16 v0, 0x1f4 */
/* if-ne p1, v0, :cond_0 */
/* .line 156 */
/* new-instance v0, Landroid/content/IntentFilter; */
final String v1 = "com.miui.action.SIM_DETECTION"; // const-string v1, "com.miui.action.SIM_DETECTION"
/* invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 157 */
/* .local v0, "intentFilter":Landroid/content/IntentFilter; */
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
v2 = this.mReceiver;
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 158 */
com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
this.mMiuiPreinstallHelper = v1;
/* .line 160 */
} // .end local v0 # "intentFilter":Landroid/content/IntentFilter;
} // :cond_0
return;
} // .end method
public void onStart ( ) {
/* .locals 2 */
/* .line 150 */
final String v0 = "AdvancePreinstallService"; // const-string v0, "AdvancePreinstallService"
final String v1 = "onStart"; // const-string v1, "onStart"
android.util.Slog .i ( v0,v1 );
/* .line 151 */
return;
} // .end method
public void trackEvent ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3, java.lang.String p4 ) {
/* .locals 5 */
/* .param p1, "event" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "confId" # I */
/* .param p4, "offlineCount" # I */
/* .param p5, "tag" # Ljava/lang/String; */
/* .line 601 */
/* new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$Action; */
/* invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V */
/* .line 602 */
/* .local v0, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action; */
final String v1 = "packageName"; // const-string v1, "packageName"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 603 */
final String v1 = "confId"; // const-string v1, "confId"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 604 */
final String v1 = "offlineCount"; // const-string v1, "offlineCount"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, p4 ); // invoke-virtual {v0, v1, p4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 605 */
final String v1 = "appType"; // const-string v1, "appType"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, p5 ); // invoke-virtual {v0, v1, p5}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 606 */
final String v1 = "request_type"; // const-string v1, "request_type"
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 607 */
final String v1 = "saleschannels"; // const-string v1, "saleschannels"
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getChannel()Ljava/lang/String; */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 608 */
v1 = miui.os.MiuiInit .isPreinstalledPackage ( p2 );
java.lang.String .valueOf ( v1 );
final String v3 = "isPreinstalled"; // const-string v3, "isPreinstalled"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 609 */
miui.os.MiuiInit .getMiuiChannelPath ( p2 );
java.lang.String .valueOf ( v1 );
final String v3 = "miuiChannelPath"; // const-string v3, "miuiChannelPath"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 611 */
final String v1 = "imeiMd5"; // const-string v1, "imeiMd5"
v3 = this.mImeiMe5;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 612 */
(( com.android.server.pm.AdvancePreinstallService ) p0 ).getContext ( ); // invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;
com.android.id.IdentifierManager .getOAID ( v1 );
final String v3 = "imId"; // const-string v3, "imId"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 613 */
final String v1 = "networkType"; // const-string v1, "networkType"
v3 = this.mNetworkTypeName;
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 615 */
/* sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
/* xor-int/2addr v1, v2 */
/* .line 616 */
/* .local v1, "isCn":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
final String v2 = "CN"; // const-string v2, "CN"
} // :cond_0
miui.os.Build .getCustVariant ( );
(( java.lang.String ) v2 ).toUpperCase ( ); // invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 617 */
/* .local v2, "region":Ljava/lang/String; */
} // :goto_0
/* invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getSku()Ljava/lang/String; */
/* .line 618 */
/* .local v3, "sku":Ljava/lang/String; */
final String v4 = "region"; // const-string v4, "region"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v2 ); // invoke-virtual {v0, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 619 */
/* const-string/jumbo v4, "sku" */
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, v3 ); // invoke-virtual {v0, v4, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 620 */
final String v4 = "EVENT_NAME"; // const-string v4, "EVENT_NAME"
(( com.android.server.pm.PreInstallServiceTrack$Action ) v0 ).addParam ( v4, p1 ); // invoke-virtual {v0, v4, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;
/* .line 621 */
/* invoke-direct {p0, v0}, Lcom/android/server/pm/AdvancePreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V */
/* .line 622 */
return;
} // .end method
