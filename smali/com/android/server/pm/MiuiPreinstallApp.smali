.class public Lcom/android/server/pm/MiuiPreinstallApp;
.super Ljava/lang/Object;
.source "MiuiPreinstallApp.java"


# instance fields
.field private apkPath:Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private versionCode:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "versionCode"    # J
    .param p4, "apkPath"    # Ljava/lang/String;

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/android/server/pm/MiuiPreinstallApp;->packageName:Ljava/lang/String;

    .line 13
    iput-wide p2, p0, Lcom/android/server/pm/MiuiPreinstallApp;->versionCode:J

    .line 14
    iput-object p4, p0, Lcom/android/server/pm/MiuiPreinstallApp;->apkPath:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getApkPath()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallApp;->apkPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/android/server/pm/MiuiPreinstallApp;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()J
    .locals 2

    .line 22
    iget-wide v0, p0, Lcom/android/server/pm/MiuiPreinstallApp;->versionCode:J

    return-wide v0
.end method
