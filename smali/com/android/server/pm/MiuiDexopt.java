public class com.android.server.pm.MiuiDexopt {
	 /* .source "MiuiDexopt.java" */
	 /* # static fields */
	 private static final java.lang.String BASLINE_TAG;
	 private static final Long MAX_TIME_INTERVALS;
	 private static final java.io.File disableBaselineFile;
	 private static volatile android.os.Handler sMiuiDexoptHandler;
	 private static java.lang.Object sMiuiDexoptLock;
	 private static android.os.HandlerThread sMiuiDexoptThread;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private com.android.server.pm.DexOptHelper mDexOptHelper;
	 private java.lang.Object mDisableBaselineLock;
	 private java.util.Set mDisableBaselineSet;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Boolean mEnableBaseline;
private java.util.Map mInvokeDisableBaselineMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.pm.PackageManagerService mPms;
/* # direct methods */
static com.android.server.pm.DexOptHelper -$$Nest$fgetmDexOptHelper ( com.android.server.pm.MiuiDexopt p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDexOptHelper;
} // .end method
static Boolean -$$Nest$mprepareDexMetadata ( com.android.server.pm.MiuiDexopt p0, com.android.server.pm.pkg.AndroidPackage p1, Integer[] p2 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/pm/MiuiDexopt;->prepareDexMetadata(Lcom/android/server/pm/pkg/AndroidPackage;[I)Z */
} // .end method
static void -$$Nest$mupdateDexMetaDataState ( com.android.server.pm.MiuiDexopt p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiDexopt;->updateDexMetaDataState(Landroid/content/Context;)V */
return;
} // .end method
static com.android.server.pm.MiuiDexopt ( ) {
/* .locals 2 */
/* .line 39 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 46 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/disable_baseline.xml"; // const-string v1, "/data/system/disable_baseline.xml"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
return;
} // .end method
public com.android.server.pm.MiuiDexopt ( ) {
/* .locals 1 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p2, "dexOptHelper" # Lcom/android/server/pm/DexOptHelper; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 48 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 49 */
this.mPms = p1;
/* .line 50 */
this.mDexOptHelper = p2;
/* .line 51 */
this.mContext = p3;
/* .line 52 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mDisableBaselineLock = v0;
/* .line 53 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mInvokeDisableBaselineMap = v0;
/* .line 54 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mDisableBaselineSet = v0;
/* .line 55 */
/* invoke-direct {p0}, Lcom/android/server/pm/MiuiDexopt;->loadDisableBaselineList()V */
/* .line 56 */
return;
} // .end method
private static android.os.Handler getMiuiDexoptHandler ( ) {
/* .locals 3 */
/* .line 131 */
v0 = com.android.server.pm.MiuiDexopt.sMiuiDexoptHandler;
/* if-nez v0, :cond_1 */
/* .line 132 */
v0 = com.android.server.pm.MiuiDexopt.sMiuiDexoptLock;
/* monitor-enter v0 */
/* .line 133 */
try { // :try_start_0
v1 = com.android.server.pm.MiuiDexopt.sMiuiDexoptHandler;
/* if-nez v1, :cond_0 */
/* .line 134 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "dex_metadata_async_thread"; // const-string v2, "dex_metadata_async_thread"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 135 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 136 */
/* new-instance v1, Landroid/os/Handler; */
v2 = com.android.server.pm.MiuiDexopt.sMiuiDexoptThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 138 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 140 */
} // :cond_1
} // :goto_0
v0 = com.android.server.pm.MiuiDexopt.sMiuiDexoptHandler;
} // .end method
private void loadDisableBaselineList ( ) {
/* .locals 5 */
/* .line 73 */
try { // :try_start_0
v0 = this.mContext;
v1 = com.android.server.pm.MiuiDexopt.disableBaselineFile;
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).getSharedPreferences ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 75 */
/* .local v0, "sp":Landroid/content/SharedPreferences; */
/* .line 76 */
/* .local v1, "allPackageNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/lang/String; */
/* .line 77 */
/* .local v3, "packageName":Ljava/lang/String; */
v4 = this.mDisableBaselineSet;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 78 */
/* nop */
} // .end local v3 # "packageName":Ljava/lang/String;
/* .line 81 */
} // .end local v0 # "sp":Landroid/content/SharedPreferences;
} // .end local v1 # "allPackageNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
} // :cond_0
/* .line 79 */
/* :catch_0 */
/* move-exception v0 */
/* .line 80 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "load disabled baseline list fail: "; // const-string v2, "load disabled baseline list fail: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BaselineProfile"; // const-string v2, "BaselineProfile"
android.util.Slog .d ( v2,v1 );
/* .line 82 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
private Boolean prepareDexMetadata ( com.android.server.pm.pkg.AndroidPackage p0, Integer[] p1 ) {
/* .locals 6 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "userId" # [I */
/* .line 242 */
final String v0 = "BaselineProfile"; // const-string v0, "BaselineProfile"
/* .line 243 */
/* .local v1, "packageName":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .line 245 */
/* .local v2, "success":Z */
try { // :try_start_0
v3 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/MiuiDexopt;->preparepProfile(Lcom/android/server/pm/pkg/AndroidPackage;)Z */
/* move v2, v3 */
/* .line 247 */
v3 = com.android.server.pm.DexOptHelper .useArtService ( );
/* if-nez v3, :cond_0 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 248 */
v3 = this.mPms;
v3 = this.mArtManagerService;
int v4 = 1; // const/4 v4, 0x1
(( com.android.server.pm.dex.ArtManagerService ) v3 ).prepareAppProfiles ( p1, p2, v4 ); // invoke-virtual {v3, p1, p2, v4}, Lcom/android/server/pm/dex/ArtManagerService;->prepareAppProfiles(Lcom/android/server/pm/pkg/AndroidPackage;[IZ)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 256 */
} // :cond_0
/* .line 253 */
/* :catch_0 */
/* move-exception v3 */
/* .line 254 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " prepareDexMetadata exception: "; // const-string v5, " prepareDexMetadata exception: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v4 );
/* .line 255 */
int v2 = 0; // const/4 v2, 0x0
/* .line 257 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " prepareDexMetadata success: "; // const-string v4, " prepareDexMetadata success: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 258 */
} // .end method
private Boolean preparepProfile ( com.android.server.pm.pkg.AndroidPackage p0 ) {
/* .locals 19 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 152 */
/* move-object/from16 v1, p0 */
final String v2 = "BaselineProfile"; // const-string v2, "BaselineProfile"
/* invoke-interface/range {p1 ..p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getBaseApkPath()Ljava/lang/String; */
/* .line 153 */
/* .local v9, "apkPath":Ljava/lang/String; */
/* invoke-interface/range {p1 ..p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPath()Ljava/lang/String; */
/* .line 154 */
/* .local v10, "codePath":Ljava/lang/String; */
/* invoke-interface/range {p1 ..p1}, Lcom/android/server/pm/pkg/AndroidPackage;->getPackageName()Ljava/lang/String; */
/* .line 155 */
/* .local v11, "packageName":Ljava/lang/String; */
/* new-instance v0, Ljava/io/File; */
final String v12 = "primary.prof"; // const-string v12, "primary.prof"
/* invoke-direct {v0, v10, v12}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v13, v0 */
/* .line 156 */
/* .local v13, "profile":Ljava/io/File; */
/* new-instance v0, Ljava/io/File; */
final String v3 = "base.dm"; // const-string v3, "base.dm"
/* invoke-direct {v0, v10, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v14, v0 */
/* .line 157 */
/* .local v14, "dexmetadataFile":Ljava/io/File; */
int v15 = 0; // const/4 v15, 0x0
/* .line 160 */
/* .local v15, "isDexmetadataDexopt":Z */
/* const/16 v16, 0x0 */
/* .line 161 */
/* .local v16, "prepareBaselineSuccess":Z */
int v3 = 0; // const/4 v3, 0x0
/* .line 163 */
/* .local v3, "assetManager":Landroid/content/res/AssetManager; */
try { // :try_start_0
/* new-instance v0, Landroid/content/res/AssetManager; */
/* invoke-direct {v0}, Landroid/content/res/AssetManager;-><init>()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* move-object v8, v0 */
/* .line 164 */
} // .end local v3 # "assetManager":Landroid/content/res/AssetManager;
/* .local v8, "assetManager":Landroid/content/res/AssetManager; */
try { // :try_start_1
(( android.content.res.AssetManager ) v8 ).addAssetPath ( v9 ); // invoke-virtual {v8, v9}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I
/* .line 165 */
/* new-instance v0, Lcom/android/server/pm/ProfileTranscoder; */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* move-object v3, v0 */
/* move-object v4, v13 */
/* move-object v5, v9 */
/* move-object v6, v10 */
/* move-object v7, v11 */
/* move-object/from16 v17, v8 */
} // .end local v8 # "assetManager":Landroid/content/res/AssetManager;
/* .local v17, "assetManager":Landroid/content/res/AssetManager; */
try { // :try_start_2
/* invoke-direct/range {v3 ..v8}, Lcom/android/server/pm/ProfileTranscoder;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/AssetManager;)V */
/* .line 167 */
/* .local v0, "profileTranscoder":Lcom/android/server/pm/ProfileTranscoder; */
(( com.android.server.pm.ProfileTranscoder ) v0 ).read ( ); // invoke-virtual {v0}, Lcom/android/server/pm/ProfileTranscoder;->read()Lcom/android/server/pm/ProfileTranscoder;
/* .line 168 */
(( com.android.server.pm.ProfileTranscoder ) v3 ).transcodeIfNeeded ( ); // invoke-virtual {v3}, Lcom/android/server/pm/ProfileTranscoder;->transcodeIfNeeded()Lcom/android/server/pm/ProfileTranscoder;
/* .line 169 */
v3 = (( com.android.server.pm.ProfileTranscoder ) v3 ).write ( ); // invoke-virtual {v3}, Lcom/android/server/pm/ProfileTranscoder;->write()Z
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* move/from16 v16, v3 */
/* .line 173 */
} // .end local v0 # "profileTranscoder":Lcom/android/server/pm/ProfileTranscoder;
/* nop */
/* .line 174 */
/* invoke-virtual/range {v17 ..v17}, Landroid/content/res/AssetManager;->close()V */
/* .line 177 */
/* move-object/from16 v8, v17 */
/* .line 173 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v3, v17 */
/* goto/16 :goto_11 */
/* .line 170 */
/* :catch_0 */
/* move-exception v0 */
/* move-object/from16 v3, v17 */
/* .line 173 */
} // .end local v17 # "assetManager":Landroid/content/res/AssetManager;
/* .restart local v8 # "assetManager":Landroid/content/res/AssetManager; */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v17, v8 */
/* move-object/from16 v3, v17 */
} // .end local v8 # "assetManager":Landroid/content/res/AssetManager;
/* .restart local v17 # "assetManager":Landroid/content/res/AssetManager; */
/* goto/16 :goto_11 */
/* .line 170 */
} // .end local v17 # "assetManager":Landroid/content/res/AssetManager;
/* .restart local v8 # "assetManager":Landroid/content/res/AssetManager; */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v17, v8 */
/* move-object/from16 v3, v17 */
} // .end local v8 # "assetManager":Landroid/content/res/AssetManager;
/* .restart local v17 # "assetManager":Landroid/content/res/AssetManager; */
/* .line 173 */
} // .end local v17 # "assetManager":Landroid/content/res/AssetManager;
/* .restart local v3 # "assetManager":Landroid/content/res/AssetManager; */
/* :catchall_2 */
/* move-exception v0 */
/* goto/16 :goto_11 */
/* .line 170 */
/* :catch_2 */
/* move-exception v0 */
/* .line 171 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_0
try { // :try_start_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v11 ); // invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " prepare Baseline Profile exception: "; // const-string v5, " prepare Baseline Profile exception: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v4 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* .line 173 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 174 */
(( android.content.res.AssetManager ) v3 ).close ( ); // invoke-virtual {v3}, Landroid/content/res/AssetManager;->close()V
/* .line 177 */
} // :cond_0
/* move-object v8, v3 */
} // .end local v3 # "assetManager":Landroid/content/res/AssetManager;
/* .restart local v8 # "assetManager":Landroid/content/res/AssetManager; */
} // :goto_1
/* move/from16 v3, v16 */
/* .line 180 */
} // .end local v15 # "isDexmetadataDexopt":Z
/* .local v3, "isDexmetadataDexopt":Z */
v0 = (( java.io.File ) v14 ).exists ( ); // invoke-virtual {v14}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 181 */
/* new-instance v0, Ljava/util/zip/ZipFile; */
/* invoke-direct {v0, v14}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V */
/* move-object v4, v0 */
/* .line 183 */
/* .local v4, "zipDmFile":Ljava/util/zip/ZipFile; */
try { // :try_start_4
(( java.util.zip.ZipFile ) v4 ).entries ( ); // invoke-virtual {v4}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_5 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_a */
/* move-object v5, v0 */
/* .local v5, "entries":Ljava/util/Enumeration; */
} // :goto_2
v0 = try { // :try_start_5
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 184 */
/* check-cast v0, Ljava/util/zip/ZipEntry; */
/* move-object v6, v0 */
/* .line 185 */
/* .local v6, "entry":Ljava/util/zip/ZipEntry; */
(( java.util.zip.ZipEntry ) v6 ).getName ( ); // invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;
/* move-object v7, v0 */
/* .line 186 */
/* .local v7, "curEntryName":Ljava/lang/String; */
v0 = (( java.lang.String ) v7 ).endsWith ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_4 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_9 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 187 */
try { // :try_start_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " primary.prof file exists, no need for DexMetadata dexopt"; // const-string v12, " primary.prof file exists, no need for DexMetadata dexopt"
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_5 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_a */
/* .line 188 */
int v0 = 0; // const/4 v0, 0x0
/* .line 189 */
} // .end local v3 # "isDexmetadataDexopt":Z
/* .local v0, "isDexmetadataDexopt":Z */
/* move v3, v0 */
/* goto/16 :goto_7 */
/* .line 194 */
} // .end local v0 # "isDexmetadataDexopt":Z
/* .restart local v3 # "isDexmetadataDexopt":Z */
} // :cond_1
try { // :try_start_7
final String v0 = "cloud.prof"; // const-string v0, "cloud.prof"
v0 = (( java.lang.String ) v7 ).endsWith ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* if-nez v16, :cond_4 */
/* .line 196 */
(( java.util.zip.ZipFile ) v4 ).getInputStream ( v6 ); // invoke-virtual {v4, v6}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
/* :try_end_7 */
/* .catch Ljava/lang/Exception; {:try_start_7 ..:try_end_7} :catch_4 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_9 */
/* move-object v15, v0 */
/* .line 197 */
/* .local v15, "is":Ljava/io/InputStream; */
try { // :try_start_8
/* new-instance v0, Ljava/io/FileOutputStream; */
/* invoke-direct {v0, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_6 */
/* move-object/from16 v17, v0 */
/* .line 199 */
/* .local v17, "os":Ljava/io/OutputStream; */
/* move/from16 v18, v3 */
/* move-object/from16 v3, v17 */
} // .end local v17 # "os":Ljava/io/OutputStream;
/* .local v3, "os":Ljava/io/OutputStream; */
/* .local v18, "isDexmetadataDexopt":Z */
try { // :try_start_9
/* invoke-direct {v1, v15, v3}, Lcom/android/server/pm/MiuiDexopt;->writeFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V */
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_4 */
/* .line 200 */
try { // :try_start_a
(( java.io.OutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_3 */
} // .end local v3 # "os":Ljava/io/OutputStream;
if ( v15 != null) { // if-eqz v15, :cond_2
try { // :try_start_b
(( java.io.InputStream ) v15 ).close ( ); // invoke-virtual {v15}, Ljava/io/InputStream;->close()V
/* :try_end_b */
/* .catch Ljava/lang/Exception; {:try_start_b ..:try_end_b} :catch_3 */
/* .catchall {:try_start_b ..:try_end_b} :catchall_8 */
/* .line 201 */
} // .end local v15 # "is":Ljava/io/InputStream;
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* move v3, v0 */
} // .end local v18 # "isDexmetadataDexopt":Z
/* .restart local v0 # "isDexmetadataDexopt":Z */
/* .line 195 */
} // .end local v0 # "isDexmetadataDexopt":Z
/* .restart local v15 # "is":Ljava/io/InputStream; */
/* .restart local v18 # "isDexmetadataDexopt":Z */
/* :catchall_3 */
/* move-exception v0 */
/* move-object v3, v0 */
/* .restart local v3 # "os":Ljava/io/OutputStream; */
/* :catchall_4 */
/* move-exception v0 */
/* move-object v12, v0 */
try { // :try_start_c
(( java.io.OutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
/* :try_end_c */
/* .catchall {:try_start_c ..:try_end_c} :catchall_5 */
/* move-object/from16 v17, v3 */
/* :catchall_5 */
/* move-exception v0 */
/* move-object/from16 v17, v3 */
/* move-object v3, v0 */
} // .end local v3 # "os":Ljava/io/OutputStream;
/* .restart local v17 # "os":Ljava/io/OutputStream; */
try { // :try_start_d
(( java.lang.Throwable ) v12 ).addSuppressed ( v3 ); // invoke-virtual {v12, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v4 # "zipDmFile":Ljava/util/zip/ZipFile;
} // .end local v5 # "entries":Ljava/util/Enumeration;
} // .end local v6 # "entry":Ljava/util/zip/ZipEntry;
} // .end local v7 # "curEntryName":Ljava/lang/String;
} // .end local v8 # "assetManager":Landroid/content/res/AssetManager;
} // .end local v9 # "apkPath":Ljava/lang/String;
} // .end local v10 # "codePath":Ljava/lang/String;
} // .end local v11 # "packageName":Ljava/lang/String;
} // .end local v13 # "profile":Ljava/io/File;
} // .end local v14 # "dexmetadataFile":Ljava/io/File;
} // .end local v15 # "is":Ljava/io/InputStream;
} // .end local v16 # "prepareBaselineSuccess":Z
} // .end local v18 # "isDexmetadataDexopt":Z
} // .end local p0 # "this":Lcom/android/server/pm/MiuiDexopt;
} // .end local p1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
} // :goto_3
/* throw v12 */
/* :try_end_d */
/* .catchall {:try_start_d ..:try_end_d} :catchall_3 */
} // .end local v17 # "os":Ljava/io/OutputStream;
/* .local v3, "isDexmetadataDexopt":Z */
/* .restart local v4 # "zipDmFile":Ljava/util/zip/ZipFile; */
/* .restart local v5 # "entries":Ljava/util/Enumeration; */
/* .restart local v6 # "entry":Ljava/util/zip/ZipEntry; */
/* .restart local v7 # "curEntryName":Ljava/lang/String; */
/* .restart local v8 # "assetManager":Landroid/content/res/AssetManager; */
/* .restart local v9 # "apkPath":Ljava/lang/String; */
/* .restart local v10 # "codePath":Ljava/lang/String; */
/* .restart local v11 # "packageName":Ljava/lang/String; */
/* .restart local v13 # "profile":Ljava/io/File; */
/* .restart local v14 # "dexmetadataFile":Ljava/io/File; */
/* .restart local v15 # "is":Ljava/io/InputStream; */
/* .restart local v16 # "prepareBaselineSuccess":Z */
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiDexopt; */
/* .restart local p1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
/* :catchall_6 */
/* move-exception v0 */
/* move/from16 v18, v3 */
/* move-object v3, v0 */
} // .end local v3 # "isDexmetadataDexopt":Z
/* .restart local v18 # "isDexmetadataDexopt":Z */
} // :goto_4
if ( v15 != null) { // if-eqz v15, :cond_3
try { // :try_start_e
(( java.io.InputStream ) v15 ).close ( ); // invoke-virtual {v15}, Ljava/io/InputStream;->close()V
/* :try_end_e */
/* .catchall {:try_start_e ..:try_end_e} :catchall_7 */
/* :catchall_7 */
/* move-exception v0 */
/* move-object v12, v0 */
try { // :try_start_f
(( java.lang.Throwable ) v3 ).addSuppressed ( v12 ); // invoke-virtual {v3, v12}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v4 # "zipDmFile":Ljava/util/zip/ZipFile;
} // .end local v8 # "assetManager":Landroid/content/res/AssetManager;
} // .end local v9 # "apkPath":Ljava/lang/String;
} // .end local v10 # "codePath":Ljava/lang/String;
} // .end local v11 # "packageName":Ljava/lang/String;
} // .end local v13 # "profile":Ljava/io/File;
} // .end local v14 # "dexmetadataFile":Ljava/io/File;
} // .end local v16 # "prepareBaselineSuccess":Z
} // .end local v18 # "isDexmetadataDexopt":Z
} // .end local p0 # "this":Lcom/android/server/pm/MiuiDexopt;
} // .end local p1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
} // :cond_3
} // :goto_5
/* throw v3 */
/* :try_end_f */
/* .catch Ljava/lang/Exception; {:try_start_f ..:try_end_f} :catch_3 */
/* .catchall {:try_start_f ..:try_end_f} :catchall_8 */
/* .line 207 */
} // .end local v5 # "entries":Ljava/util/Enumeration;
} // .end local v6 # "entry":Ljava/util/zip/ZipEntry;
} // .end local v7 # "curEntryName":Ljava/lang/String;
} // .end local v15 # "is":Ljava/io/InputStream;
/* .restart local v4 # "zipDmFile":Ljava/util/zip/ZipFile; */
/* .restart local v8 # "assetManager":Landroid/content/res/AssetManager; */
/* .restart local v9 # "apkPath":Ljava/lang/String; */
/* .restart local v10 # "codePath":Ljava/lang/String; */
/* .restart local v11 # "packageName":Ljava/lang/String; */
/* .restart local v13 # "profile":Ljava/io/File; */
/* .restart local v14 # "dexmetadataFile":Ljava/io/File; */
/* .restart local v16 # "prepareBaselineSuccess":Z */
/* .restart local v18 # "isDexmetadataDexopt":Z */
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiDexopt; */
/* .restart local p1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
/* :catchall_8 */
/* move-exception v0 */
/* move/from16 v3, v18 */
/* .line 204 */
/* :catch_3 */
/* move-exception v0 */
/* move/from16 v3, v18 */
/* .line 194 */
} // .end local v18 # "isDexmetadataDexopt":Z
/* .restart local v3 # "isDexmetadataDexopt":Z */
/* .restart local v5 # "entries":Ljava/util/Enumeration; */
/* .restart local v6 # "entry":Ljava/util/zip/ZipEntry; */
/* .restart local v7 # "curEntryName":Ljava/lang/String; */
} // :cond_4
/* move/from16 v18, v3 */
/* .line 203 */
} // .end local v3 # "isDexmetadataDexopt":Z
} // .end local v6 # "entry":Ljava/util/zip/ZipEntry;
} // .end local v7 # "curEntryName":Ljava/lang/String;
/* .restart local v18 # "isDexmetadataDexopt":Z */
/* move/from16 v3, v18 */
} // .end local v18 # "isDexmetadataDexopt":Z
/* .restart local v3 # "isDexmetadataDexopt":Z */
} // :goto_6
/* goto/16 :goto_2 */
/* .line 183 */
} // :cond_5
/* move/from16 v18, v3 */
/* .line 207 */
} // .end local v5 # "entries":Ljava/util/Enumeration;
} // :goto_7
/* nop */
} // :goto_8
(( java.util.zip.ZipFile ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V
/* .line 208 */
/* .line 207 */
/* :catchall_9 */
/* move-exception v0 */
/* move/from16 v18, v3 */
} // .end local v3 # "isDexmetadataDexopt":Z
/* .restart local v18 # "isDexmetadataDexopt":Z */
/* .line 204 */
} // .end local v18 # "isDexmetadataDexopt":Z
/* .restart local v3 # "isDexmetadataDexopt":Z */
/* :catch_4 */
/* move-exception v0 */
/* move/from16 v18, v3 */
} // .end local v3 # "isDexmetadataDexopt":Z
/* .restart local v18 # "isDexmetadataDexopt":Z */
/* .line 207 */
} // .end local v18 # "isDexmetadataDexopt":Z
/* .restart local v3 # "isDexmetadataDexopt":Z */
/* :catchall_a */
/* move-exception v0 */
/* .line 204 */
/* :catch_5 */
/* move-exception v0 */
/* .line 205 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_9
try { // :try_start_10
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " prepare Cloud Profile exception: "; // const-string v6, " prepare Cloud Profile exception: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* :try_end_10 */
/* .catchall {:try_start_10 ..:try_end_10} :catchall_a */
/* .line 207 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_a
(( java.util.zip.ZipFile ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V
/* .line 208 */
/* throw v0 */
/* .line 212 */
} // .end local v4 # "zipDmFile":Ljava/util/zip/ZipFile;
} // :cond_6
} // :goto_b
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_c
/* .line 213 */
v0 = (( java.io.File ) v14 ).exists ( ); // invoke-virtual {v14}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 214 */
(( java.io.File ) v14 ).delete ( ); // invoke-virtual {v14}, Ljava/io/File;->delete()Z
/* .line 217 */
} // :cond_7
try { // :try_start_11
/* new-instance v0, Ljava/util/zip/ZipOutputStream; */
/* new-instance v5, Ljava/io/FileOutputStream; */
/* invoke-direct {v5, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* invoke-direct {v0, v5}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* :try_end_11 */
/* .catch Ljava/lang/Exception; {:try_start_11 ..:try_end_11} :catch_6 */
/* .catchall {:try_start_11 ..:try_end_11} :catchall_f */
/* move-object v5, v0 */
/* .line 218 */
/* .local v5, "os":Ljava/util/zip/ZipOutputStream; */
try { // :try_start_12
/* new-instance v0, Ljava/io/FileInputStream; */
/* invoke-direct {v0, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* :try_end_12 */
/* .catchall {:try_start_12 ..:try_end_12} :catchall_d */
/* move-object v6, v0 */
/* .line 220 */
/* .local v6, "is":Ljava/io/InputStream; */
try { // :try_start_13
/* new-instance v0, Ljava/util/zip/ZipEntry; */
(( java.io.File ) v13 ).getName ( ); // invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;
/* invoke-direct {v0, v7}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V */
(( java.util.zip.ZipOutputStream ) v5 ).putNextEntry ( v0 ); // invoke-virtual {v5, v0}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V
/* .line 221 */
/* invoke-direct {v1, v6, v5}, Lcom/android/server/pm/MiuiDexopt;->writeFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V */
/* :try_end_13 */
/* .catchall {:try_start_13 ..:try_end_13} :catchall_b */
/* .line 222 */
try { // :try_start_14
(( java.io.InputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/InputStream;->close()V
/* :try_end_14 */
/* .catchall {:try_start_14 ..:try_end_14} :catchall_d */
} // .end local v6 # "is":Ljava/io/InputStream;
try { // :try_start_15
(( java.util.zip.ZipOutputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/util/zip/ZipOutputStream;->close()V
/* :try_end_15 */
/* .catch Ljava/lang/Exception; {:try_start_15 ..:try_end_15} :catch_6 */
/* .catchall {:try_start_15 ..:try_end_15} :catchall_f */
/* .line 226 */
} // .end local v5 # "os":Ljava/util/zip/ZipOutputStream;
v0 = (( java.io.File ) v13 ).exists ( ); // invoke-virtual {v13}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 227 */
(( java.io.File ) v13 ).delete ( ); // invoke-virtual {v13}, Ljava/io/File;->delete()Z
/* .line 230 */
} // :cond_8
if ( v16 != null) { // if-eqz v16, :cond_9
final String v0 = "baseline"; // const-string v0, "baseline"
} // :cond_9
final String v0 = "cloud"; // const-string v0, "cloud"
/* .line 231 */
/* .local v0, "effectProfile":Ljava/lang/String; */
} // :goto_c
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " prepare DexMetadata profile success, effective profile is: "; // const-string v6, " prepare DexMetadata profile success, effective profile is: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* .line 232 */
} // .end local v0 # "effectProfile":Ljava/lang/String;
/* .line 216 */
/* .restart local v5 # "os":Ljava/util/zip/ZipOutputStream; */
/* .restart local v6 # "is":Ljava/io/InputStream; */
/* :catchall_b */
/* move-exception v0 */
/* move-object v7, v0 */
try { // :try_start_16
(( java.io.InputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/InputStream;->close()V
/* :try_end_16 */
/* .catchall {:try_start_16 ..:try_end_16} :catchall_c */
/* :catchall_c */
/* move-exception v0 */
/* move-object v12, v0 */
try { // :try_start_17
(( java.lang.Throwable ) v7 ).addSuppressed ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v3 # "isDexmetadataDexopt":Z
} // .end local v5 # "os":Ljava/util/zip/ZipOutputStream;
} // .end local v8 # "assetManager":Landroid/content/res/AssetManager;
} // .end local v9 # "apkPath":Ljava/lang/String;
} // .end local v10 # "codePath":Ljava/lang/String;
} // .end local v11 # "packageName":Ljava/lang/String;
} // .end local v13 # "profile":Ljava/io/File;
} // .end local v14 # "dexmetadataFile":Ljava/io/File;
} // .end local v16 # "prepareBaselineSuccess":Z
} // .end local p0 # "this":Lcom/android/server/pm/MiuiDexopt;
} // .end local p1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
} // :goto_d
/* throw v7 */
/* :try_end_17 */
/* .catchall {:try_start_17 ..:try_end_17} :catchall_d */
} // .end local v6 # "is":Ljava/io/InputStream;
/* .restart local v3 # "isDexmetadataDexopt":Z */
/* .restart local v5 # "os":Ljava/util/zip/ZipOutputStream; */
/* .restart local v8 # "assetManager":Landroid/content/res/AssetManager; */
/* .restart local v9 # "apkPath":Ljava/lang/String; */
/* .restart local v10 # "codePath":Ljava/lang/String; */
/* .restart local v11 # "packageName":Ljava/lang/String; */
/* .restart local v13 # "profile":Ljava/io/File; */
/* .restart local v14 # "dexmetadataFile":Ljava/io/File; */
/* .restart local v16 # "prepareBaselineSuccess":Z */
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiDexopt; */
/* .restart local p1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
/* :catchall_d */
/* move-exception v0 */
/* move-object v6, v0 */
try { // :try_start_18
(( java.util.zip.ZipOutputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/util/zip/ZipOutputStream;->close()V
/* :try_end_18 */
/* .catchall {:try_start_18 ..:try_end_18} :catchall_e */
/* :catchall_e */
/* move-exception v0 */
/* move-object v7, v0 */
try { // :try_start_19
(( java.lang.Throwable ) v6 ).addSuppressed ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v3 # "isDexmetadataDexopt":Z
} // .end local v8 # "assetManager":Landroid/content/res/AssetManager;
} // .end local v9 # "apkPath":Ljava/lang/String;
} // .end local v10 # "codePath":Ljava/lang/String;
} // .end local v11 # "packageName":Ljava/lang/String;
} // .end local v13 # "profile":Ljava/io/File;
} // .end local v14 # "dexmetadataFile":Ljava/io/File;
} // .end local v16 # "prepareBaselineSuccess":Z
} // .end local p0 # "this":Lcom/android/server/pm/MiuiDexopt;
} // .end local p1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
} // :goto_e
/* throw v6 */
/* :try_end_19 */
/* .catch Ljava/lang/Exception; {:try_start_19 ..:try_end_19} :catch_6 */
/* .catchall {:try_start_19 ..:try_end_19} :catchall_f */
/* .line 226 */
} // .end local v5 # "os":Ljava/util/zip/ZipOutputStream;
/* .restart local v3 # "isDexmetadataDexopt":Z */
/* .restart local v8 # "assetManager":Landroid/content/res/AssetManager; */
/* .restart local v9 # "apkPath":Ljava/lang/String; */
/* .restart local v10 # "codePath":Ljava/lang/String; */
/* .restart local v11 # "packageName":Ljava/lang/String; */
/* .restart local v13 # "profile":Ljava/io/File; */
/* .restart local v14 # "dexmetadataFile":Ljava/io/File; */
/* .restart local v16 # "prepareBaselineSuccess":Z */
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiDexopt; */
/* .restart local p1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
/* :catchall_f */
/* move-exception v0 */
/* .line 222 */
/* :catch_6 */
/* move-exception v0 */
/* .line 223 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1a
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " prepare DexMetadata archive fail: "; // const-string v6, " prepare DexMetadata archive fail: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v5 );
/* :try_end_1a */
/* .catchall {:try_start_1a ..:try_end_1a} :catchall_f */
/* .line 224 */
/* nop */
/* .line 226 */
v2 = (( java.io.File ) v13 ).exists ( ); // invoke-virtual {v13}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 227 */
(( java.io.File ) v13 ).delete ( ); // invoke-virtual {v13}, Ljava/io/File;->delete()Z
/* .line 224 */
} // :cond_a
/* .line 226 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_f
v2 = (( java.io.File ) v13 ).exists ( ); // invoke-virtual {v13}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 227 */
(( java.io.File ) v13 ).delete ( ); // invoke-virtual {v13}, Ljava/io/File;->delete()Z
/* .line 229 */
} // :cond_b
/* throw v0 */
/* .line 233 */
} // :cond_c
v0 = (( java.io.File ) v13 ).exists ( ); // invoke-virtual {v13}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 234 */
(( java.io.File ) v13 ).delete ( ); // invoke-virtual {v13}, Ljava/io/File;->delete()Z
/* .line 237 */
} // :cond_d
} // :goto_10
if ( v3 != null) { // if-eqz v3, :cond_e
v0 = (( java.io.File ) v14 ).exists ( ); // invoke-virtual {v14}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_e
int v4 = 1; // const/4 v4, 0x1
} // :cond_e
/* .line 173 */
} // .end local v8 # "assetManager":Landroid/content/res/AssetManager;
/* .local v3, "assetManager":Landroid/content/res/AssetManager; */
/* .local v15, "isDexmetadataDexopt":Z */
} // :goto_11
if ( v3 != null) { // if-eqz v3, :cond_f
/* .line 174 */
(( android.content.res.AssetManager ) v3 ).close ( ); // invoke-virtual {v3}, Landroid/content/res/AssetManager;->close()V
/* .line 176 */
} // :cond_f
/* throw v0 */
} // .end method
private void updateDexMetaDataState ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 123 */
/* nop */
/* .line 124 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 123 */
final String v1 = "DexMetaData"; // const-string v1, "DexMetaData"
final String v2 = "enable"; // const-string v2, "enable"
final String v3 = "false"; // const-string v3, "false"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v1,v2,v3 );
/* .line 125 */
/* .local v0, "enable":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "prepare DexMetadata enable: "; // const-string v2, "prepare DexMetadata enable: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BaselineProfile"; // const-string v2, "BaselineProfile"
android.util.Slog .d ( v2,v1 );
/* .line 126 */
final String v1 = "persist.sys.baseline.enable"; // const-string v1, "persist.sys.baseline.enable"
android.os.SystemProperties .set ( v1,v0 );
/* .line 127 */
/* const-string/jumbo v1, "true" */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* iput-boolean v1, p0, Lcom/android/server/pm/MiuiDexopt;->mEnableBaseline:Z */
/* .line 128 */
return;
} // .end method
private void writeFile ( java.io.InputStream p0, java.io.OutputStream p1 ) {
/* .locals 3 */
/* .param p1, "is" # Ljava/io/InputStream; */
/* .param p2, "os" # Ljava/io/OutputStream; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 144 */
/* const/16 v0, 0x200 */
/* new-array v0, v0, [B */
/* .line 146 */
/* .local v0, "buf":[B */
} // :goto_0
v1 = (( java.io.InputStream ) p1 ).read ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I
/* move v2, v1 */
/* .local v2, "length":I */
/* if-lez v1, :cond_0 */
/* .line 147 */
int v1 = 0; // const/4 v1, 0x0
(( java.io.OutputStream ) p2 ).write ( v0, v1, v2 ); // invoke-virtual {p2, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V
/* .line 149 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void asyncDexMetadataDexopt ( com.android.server.pm.pkg.AndroidPackage p0, Integer[] p1 ) {
/* .locals 2 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "userId" # [I */
/* .line 262 */
/* iget-boolean v0, p0, Lcom/android/server/pm/MiuiDexopt;->mEnableBaseline:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.pm.MiuiDexopt ) p0 ).isBaselineDisabled ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/pm/MiuiDexopt;->isBaselineDisabled(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 263 */
/* new-instance v0, Lcom/android/server/pm/MiuiDexopt$2; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/android/server/pm/MiuiDexopt$2;-><init>(Lcom/android/server/pm/MiuiDexopt;Lcom/android/server/pm/pkg/AndroidPackage;[I)V */
/* .line 286 */
/* .local v0, "task":Ljava/lang/Runnable; */
com.android.server.pm.MiuiDexopt .getMiuiDexoptHandler ( );
(( android.os.Handler ) v1 ).post ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 287 */
} // .end local v0 # "task":Ljava/lang/Runnable;
/* .line 288 */
} // :cond_0
final String v0 = "BaselineProfile"; // const-string v0, "BaselineProfile"
final String v1 = "asyncDexMetadataDexopt not enable"; // const-string v1, "asyncDexMetadataDexopt not enable"
android.util.Slog .d ( v0,v1 );
/* .line 290 */
} // :goto_0
return;
} // .end method
public Boolean isBaselineDisabled ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 119 */
v0 = v0 = this.mDisableBaselineSet;
} // .end method
public void preConfigMiuiDexopt ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 59 */
final String v0 = "persist.sys.baseline.enable"; // const-string v0, "persist.sys.baseline.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/pm/MiuiDexopt;->mEnableBaseline:Z */
/* .line 61 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 62 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/pm/MiuiDexopt$1; */
/* .line 63 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v2, p0, v3, p1}, Lcom/android/server/pm/MiuiDexopt$1;-><init>(Lcom/android/server/pm/MiuiDexopt;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 61 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 69 */
return;
} // .end method
public Boolean setBaselineDisabled ( java.lang.String p0, Boolean p1 ) {
/* .locals 11 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "disabled" # Z */
/* .line 87 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mDisableBaselineLock;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 88 */
try { // :try_start_1
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* .line 89 */
/* .local v2, "currentTime":J */
v4 = this.mInvokeDisableBaselineMap;
/* const-wide/16 v5, 0x0 */
java.lang.Long .valueOf ( v5,v6 );
/* check-cast v4, Ljava/lang/Long; */
(( java.lang.Long ) v4 ).longValue ( ); // invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* .line 91 */
/* .local v4, "lastInvokeTime":J */
/* sub-long v6, v2, v4 */
/* const-wide/32 v8, 0x927c0 */
/* cmp-long v6, v6, v8 */
/* if-gtz v6, :cond_0 */
/* .line 92 */
final String v6 = "BaselineProfile"; // const-string v6, "BaselineProfile"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " invalid invoke"; // const-string v8, " invalid invoke"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v7 );
/* .line 93 */
/* monitor-exit v1 */
/* .line 95 */
} // :cond_0
v6 = this.mContext;
v7 = com.android.server.pm.MiuiDexopt.disableBaselineFile;
(( android.content.Context ) v6 ).getSharedPreferences ( v7, v0 ); // invoke-virtual {v6, v7, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/io/File;I)Landroid/content/SharedPreferences;
/* .line 97 */
/* .local v6, "sp":Landroid/content/SharedPreferences; */
v7 = v7 = this.mDisableBaselineSet;
/* .line 98 */
/* .local v7, "isContain":Z */
int v8 = 1; // const/4 v8, 0x1
if ( p2 != null) { // if-eqz p2, :cond_1
/* if-nez v7, :cond_1 */
/* .line 99 */
v9 = this.mDisableBaselineSet;
/* .line 100 */
/* .line 101 */
/* .local v9, "editor":Landroid/content/SharedPreferences$Editor; */
/* .line 102 */
/* .line 103 */
/* nop */
} // .end local v9 # "editor":Landroid/content/SharedPreferences$Editor;
} // :cond_1
/* if-nez p2, :cond_2 */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 104 */
v9 = this.mDisableBaselineSet;
/* .line 105 */
/* .line 106 */
/* .restart local v9 # "editor":Landroid/content/SharedPreferences$Editor; */
/* .line 107 */
/* .line 109 */
} // .end local v9 # "editor":Landroid/content/SharedPreferences$Editor;
} // :cond_2
} // :goto_0
v9 = this.mInvokeDisableBaselineMap;
java.lang.Long .valueOf ( v2,v3 );
/* .line 110 */
/* nop */
} // .end local v2 # "currentTime":J
} // .end local v4 # "lastInvokeTime":J
} // .end local v6 # "sp":Landroid/content/SharedPreferences;
} // .end local v7 # "isContain":Z
/* monitor-exit v1 */
/* .line 114 */
/* nop */
/* .line 115 */
/* .line 110 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/pm/MiuiDexopt;
} // .end local p1 # "packageName":Ljava/lang/String;
} // .end local p2 # "disabled":Z
try { // :try_start_2
/* throw v2 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 111 */
/* .restart local p0 # "this":Lcom/android/server/pm/MiuiDexopt; */
/* .restart local p1 # "packageName":Ljava/lang/String; */
/* .restart local p2 # "disabled":Z */
/* :catch_0 */
/* move-exception v1 */
/* .line 112 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "BaselineProfile"; // const-string v2, "BaselineProfile"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "set baseline state fail: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 113 */
} // .end method
