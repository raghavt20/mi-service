public class com.android.server.pm.PreinstallApp {
	 /* .source "PreinstallApp.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/pm/PreinstallApp$Item; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String AFRICA_COUNTRY;
private static final java.lang.String ALLOW_INSTALL_THIRD_APP_LIST;
private static final java.io.File CUSTOMIZED_APP_DIR;
private static final Boolean DEBUG;
private static final java.lang.String DEL_APPS_LIST;
private static final java.lang.String DISALLOW_INSTALL_THIRD_APP_LIST;
private static final java.lang.String ENCRYPTING_STATE;
private static final java.lang.String GLOBAL_BROWSER_APK;
private static final java.lang.String GLOBAL_BROWSER_REMOVEABLE_APK;
private static final java.lang.String INSTALL_DIR;
private static final Integer MIUI_APP_PATH_L_LENGTH;
private static final java.io.File NONCUSTOMIZED_APP_DIR;
private static final java.util.Set NOT_OTA_PACKAGE_NAMES;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.io.File OLD_PREINSTALL_APP_DIR;
private static final java.lang.String OLD_PREINSTALL_HISTORY_FILE;
private static final java.lang.String OLD_PREINSTALL_PACKAGE_PAI_TRACKING_FILE;
private static final java.lang.String OPERATOR_PREINSTALL_PACKAGE_TRACKING_FILE;
private static final java.lang.String OPERA_BROWSER_APK;
private static final java.io.File OTA_SKIP_BUSINESS_APP_LIST_FILE;
private static final java.lang.String PREINSTALL_HISTORY_FILE;
private static final java.lang.String PREINSTALL_PACKAGE_LIST;
private static final java.lang.String PREINSTALL_PACKAGE_MIUI_TRACKING_DIR;
private static final java.lang.String PREINSTALL_PACKAGE_MIUI_TRACKING_DIR_CONTEXT;
private static final java.lang.String PREINSTALL_PACKAGE_PAI_LIST;
private static final java.lang.String PREINSTALL_PACKAGE_PAI_TRACKING_FILE;
private static final java.lang.String PREINSTALL_PACKAGE_PATH;
private static final java.lang.String PRODUCT_CARRIER_DIR;
private static final java.io.File PRODUCT_NONCUSTOMIZED_APP_DIR;
private static final java.lang.String RANDOM_DIR_PREFIX;
private static final java.io.File RECOMMENDED_APP_DIR;
private static final java.io.File S_DATA_PREINSTALL_APP_DIR;
private static final java.io.File S_SYSTEM_PREINSTALL_APP_DIR;
private static final java.lang.String TAG;
private static final java.lang.String TYPE_TRACKING_APPSFLYER;
private static final java.lang.String TYPE_TRACKING_MIUI;
private static final java.io.File T_SYSTEM_PREINSTALL_APP_DIR;
private static final java.io.File VENDOR_NONCUSTOMIZED_APP_DIR;
private static java.util.List mNewTrackContentList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mPackagePAIList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.Map mPackageVersionMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mTraditionalTrackContentList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static java.util.Map sAdvanceApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sAllowOnlyInstallThirdApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sCTPreinstallApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static java.util.ArrayList sCloudControlUninstall;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sCotaDelApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sDisallowInstallThirdApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sIgnorePreinstallApks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sNewOrUpdatePreinstallApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Lcom/android/server/pm/PreinstallApp$Item;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Map sPreinstallApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/pm/PreinstallApp$Item;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.io.File -$$Nest$smgetApkFile ( java.io.File p0 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.pm.PreinstallApp .getApkFile ( p0 );
} // .end method
static Boolean -$$Nest$smisSplitApk ( java.io.File p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = com.android.server.pm.PreinstallApp .isSplitApk ( p0 );
} // .end method
static com.android.server.pm.PreinstallApp ( ) {
/* .locals 53 */
/* .line 122 */
/* const-class v0, Lcom/android/server/pm/PreinstallApp; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 135 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miui/apps"; // const-string v1, "/data/miui/apps"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 145 */
miui.util.CustomizeUtil .getMiuiNoCustomizedAppDir ( );
/* .line 146 */
miui.util.CustomizeUtil .getMiuiVendorNoCustomizedAppDir ( );
/* .line 147 */
miui.util.CustomizeUtil .getMiuiProductNoCustomizedAppDir ( );
/* .line 150 */
miui.util.CustomizeUtil .getMiuiCustomizedAppDir ( );
/* .line 153 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miui/app/recommended"; // const-string v1, "/data/miui/app/recommended"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 156 */
/* new-instance v0, Ljava/io/File; */
final String v2 = "/system/etc/ota_skip_apps"; // const-string v2, "/system/etc/ota_skip_apps"
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 159 */
/* new-instance v0, Ljava/io/File; */
final String v2 = "/system/data-app"; // const-string v2, "/system/data-app"
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 161 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 163 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/data-app"; // const-string v1, "/product/data-app"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 165 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 166 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 170 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 178 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 184 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 185 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 186 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 189 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 192 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 193 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 194 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 197 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 205 */
/* sget-boolean v0, Lmiui/os/Build;->IS_MI2A:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lmiui/os/Build;->IS_MITHREE:Z */
/* if-nez v0, :cond_0 */
/* .line 206 */
final String v0 = "ota-miui-MiTagApp.apk"; // const-string v0, "ota-miui-MiTagApp.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 208 */
} // :cond_0
/* const-string/jumbo v0, "support_ir" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* if-nez v0, :cond_1 */
/* .line 209 */
final String v0 = "partner-XMRemoteController.apk"; // const-string v0, "partner-XMRemoteController.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 212 */
} // :cond_1
final String v0 = "POCO"; // const-string v0, "POCO"
v1 = android.os.Build.BRAND;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 213 */
final String v0 = "MICOMMUNITY_OVERSEA.apk"; // const-string v0, "MICOMMUNITY_OVERSEA.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 214 */
final String v0 = "MISTORE_OVERSEA.apk"; // const-string v0, "MISTORE_OVERSEA.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 216 */
} // :cond_2
final String v0 = "POCOCOMMUNITY_OVERSEA.apk"; // const-string v0, "POCOCOMMUNITY_OVERSEA.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 217 */
final String v0 = "POCOSTORE_OVERSEA.apk"; // const-string v0, "POCOSTORE_OVERSEA.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 219 */
} // :goto_0
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
android.os.SystemProperties .get ( v0 );
final String v1 = "global"; // const-string v1, "global"
v0 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 220 */
final String v0 = "MIBrowserGlobal.apk"; // const-string v0, "MIBrowserGlobal.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 221 */
final String v0 = "MIBrowserGlobal_removable.apk"; // const-string v0, "MIBrowserGlobal_removable.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 222 */
final String v0 = "Opera.apk"; // const-string v0, "Opera.apk"
com.android.server.pm.PreinstallApp .ignorePreinstallApks ( v0 );
/* .line 226 */
} // :cond_3
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 228 */
final String v1 = "com.cn21.ecloud"; // const-string v1, "com.cn21.ecloud"
/* .line 229 */
final String v1 = "com.ct.client"; // const-string v1, "com.ct.client"
/* .line 230 */
final String v1 = "com.chinatelecom.bestpayclient"; // const-string v1, "com.chinatelecom.bestpayclient"
/* .line 231 */
final String v1 = "com.yueme.itv"; // const-string v1, "com.yueme.itv"
/* .line 234 */
final String v2 = "DZ"; // const-string v2, "DZ"
final String v3 = "AO"; // const-string v3, "AO"
final String v4 = "BJ"; // const-string v4, "BJ"
final String v5 = "BF"; // const-string v5, "BF"
final String v6 = "BI"; // const-string v6, "BI"
final String v7 = "CM"; // const-string v7, "CM"
final String v8 = "TD"; // const-string v8, "TD"
final String v9 = "EG"; // const-string v9, "EG"
final String v10 = "ET"; // const-string v10, "ET"
final String v11 = "GH"; // const-string v11, "GH"
final String v12 = "GN"; // const-string v12, "GN"
final String v13 = "KE"; // const-string v13, "KE"
final String v14 = "LY"; // const-string v14, "LY"
final String v15 = "MG"; // const-string v15, "MG"
final String v16 = "MW"; // const-string v16, "MW"
final String v17 = "ML"; // const-string v17, "ML"
final String v18 = "MA"; // const-string v18, "MA"
final String v19 = "MZ"; // const-string v19, "MZ"
final String v20 = "NG"; // const-string v20, "NG"
final String v21 = "RW"; // const-string v21, "RW"
final String v22 = "SN"; // const-string v22, "SN"
final String v23 = "SL"; // const-string v23, "SL"
final String v24 = "SO"; // const-string v24, "SO"
final String v25 = "ZA"; // const-string v25, "ZA"
final String v26 = "SS"; // const-string v26, "SS"
final String v27 = "SD"; // const-string v27, "SD"
final String v28 = "TZ"; // const-string v28, "TZ"
final String v29 = "TG"; // const-string v29, "TG"
final String v30 = "TN"; // const-string v30, "TN"
final String v31 = "UG"; // const-string v31, "UG"
final String v32 = "ZM"; // const-string v32, "ZM"
final String v33 = "ZW"; // const-string v33, "ZW"
final String v34 = "ZR"; // const-string v34, "ZR"
final String v35 = "CI"; // const-string v35, "CI"
final String v36 = "LR"; // const-string v36, "LR"
final String v37 = "MR"; // const-string v37, "MR"
final String v38 = "ER"; // const-string v38, "ER"
final String v39 = "NA"; // const-string v39, "NA"
final String v40 = "GM"; // const-string v40, "GM"
final String v41 = "BW"; // const-string v41, "BW"
final String v42 = "GA"; // const-string v42, "GA"
final String v43 = "LS"; // const-string v43, "LS"
final String v44 = "GW"; // const-string v44, "GW"
final String v45 = "GQ"; // const-string v45, "GQ"
final String v46 = "MU"; // const-string v46, "MU"
final String v47 = "SZ"; // const-string v47, "SZ"
final String v48 = "DJ"; // const-string v48, "DJ"
final String v49 = "KM"; // const-string v49, "KM"
final String v50 = "CV"; // const-string v50, "CV"
final String v51 = "ST"; // const-string v51, "ST"
final String v52 = "SC"; // const-string v52, "SC"
/* filled-new-array/range {v2 ..v52}, [Ljava/lang/String; */
/* .line 1875 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
public com.android.server.pm.PreinstallApp ( ) {
/* .locals 0 */
/* .line 72 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private static void addPreinstallAppToList ( java.util.List p0, java.io.File p1, java.util.Set p2 ) {
/* .locals 7 */
/* .param p1, "appDir" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;", */
/* "Ljava/io/File;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 630 */
/* .local p0, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .local p2, "filterSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
(( java.io.File ) p1 ).listFiles ( ); // invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 631 */
/* .local v0, "apps":[Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 633 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_5 */
/* aget-object v3, v0, v2 */
/* .line 634 */
/* .local v3, "app":Ljava/io/File; */
v4 = com.android.server.pm.PreinstallApp .isSplitApk ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 635 */
/* .line 637 */
} // :cond_0
v4 = (( java.io.File ) v3 ).isDirectory ( ); // invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 639 */
com.android.server.pm.PreinstallApp .getBaseApkFile ( v3 );
/* .line 640 */
/* .local v4, "apk":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* if-nez v5, :cond_2 */
/* .line 641 */
/* .line 645 */
} // .end local v4 # "apk":Ljava/io/File;
} // :cond_1
/* move-object v4, v3 */
/* .line 646 */
/* .restart local v4 # "apk":Ljava/io/File; */
v5 = com.android.server.pm.PreinstallApp .isApkFile ( v4 );
/* if-nez v5, :cond_2 */
/* .line 647 */
/* .line 650 */
} // :cond_2
v5 = com.android.server.pm.PreinstallApp.sIgnorePreinstallApks;
v5 = (( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
/* if-nez v5, :cond_4 */
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 651 */
v5 = (( java.io.File ) v4 ).getName ( ); // invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;
/* if-nez v5, :cond_3 */
/* .line 652 */
/* .line 654 */
} // :cond_3
/* .line 633 */
} // .end local v3 # "app":Ljava/io/File;
} // .end local v4 # "apk":Ljava/io/File;
} // :cond_4
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 658 */
} // :cond_5
return;
} // .end method
private static void addPreinstallChannelToList ( java.util.List p0, java.io.File p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "channelDir" # Ljava/io/File; */
/* .param p2, "channelListFile" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/io/File;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1438 */
/* .local p0, "preinstallChannelList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
try { // :try_start_0
/* new-instance v0, Ljava/io/BufferedReader; */
/* new-instance v1, Ljava/io/FileReader; */
/* invoke-direct {v1, p2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* .line 1440 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1441 */
/* .local v1, "channelName":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1442 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "/"; // const-string v3, "/"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1445 */
} // :cond_0
(( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1448 */
} // .end local v0 # "reader":Ljava/io/BufferedReader;
} // .end local v1 # "channelName":Ljava/lang/String;
/* .line 1446 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1447 */
/* .local v0, "e":Ljava/io/IOException; */
v1 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Error occurs while read preinstalled channels "; // const-string v3, "Error occurs while read preinstalled channels "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 1449 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
private static void browserRSACustomize ( java.util.List p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 729 */
/* .local p0, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
android.os.SystemProperties .get ( v0 );
final String v1 = "global"; // const-string v1, "global"
v0 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 730 */
com.android.server.pm.PreinstallApp .getMiBrowserApkFile ( );
/* .line 731 */
/* .local v0, "apkFile":Ljava/io/File; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 732 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 733 */
/* .local v1, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
com.android.server.pm.PreinstallApp .readHistory ( v1 );
/* .line 734 */
v2 = com.android.server.pm.PreinstallApp .recorded ( v1,v0 );
/* .line 735 */
/* .local v2, "hasRecorded":Z */
final String v3 = "ro.com.miui.rsa"; // const-string v3, "ro.com.miui.rsa"
final String v4 = ""; // const-string v4, ""
android.os.SystemProperties .get ( v3,v4 );
/* .line 736 */
/* .local v3, "rsa":Ljava/lang/String; */
/* const-string/jumbo v4, "tier2" */
v4 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_0 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 737 */
} // :cond_0
/* .line 738 */
v4 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "globalBrowser PreInstall install apkFile: "; // const-string v6, "globalBrowser PreInstall install apkFile: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " hasRecorded="; // const-string v6, " hasRecorded="
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v5 );
/* .line 742 */
} // .end local v0 # "apkFile":Ljava/io/File;
} // .end local v1 # "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
} // .end local v2 # "hasRecorded":Z
} // .end local v3 # "rsa":Ljava/lang/String;
} // :cond_1
return;
} // .end method
static void cleanUpResource ( java.io.File p0 ) {
/* .locals 1 */
/* .param p0, "dstCodePath" # Ljava/io/File; */
/* .line 570 */
if ( p0 != null) { // if-eqz p0, :cond_0
v0 = (( java.io.File ) p0 ).isDirectory ( ); // invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 571 */
/* new-instance v0, Lcom/android/server/pm/PreinstallApp$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/pm/PreinstallApp$$ExternalSyntheticLambda0;-><init>()V */
(( java.io.File ) p0 ).listFiles ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;
/* .line 586 */
} // :cond_0
return;
} // .end method
private static Boolean copyCTPreinstallApp ( com.android.server.pm.PreinstallApp$Item p0, com.android.server.pm.PackageSetting p1 ) {
/* .locals 7 */
/* .param p0, "srcApp" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .param p1, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .line 505 */
v0 = this.packageName;
v0 = com.android.server.pm.PreinstallApp .isCTPreinstallApp ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 506 */
/* .line 509 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 510 */
/* .local v0, "dstCodePath":Ljava/io/File; */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 511 */
(( com.android.server.pm.PackageSetting ) p1 ).getPath ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPath()Ljava/io/File;
/* .line 512 */
com.android.server.pm.PreinstallApp .cleanUpResource ( v0 );
/* .line 515 */
} // :cond_1
/* if-nez v0, :cond_2 */
/* .line 516 */
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/app/"; // const-string v3, "/data/app/"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v3 = this.packageName;
com.android.server.pm.PreinstallApp .getNextCodePath ( v2,v3 );
/* .line 519 */
} // :cond_2
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v3 = -1; // const/4 v3, -0x1
/* if-nez v2, :cond_4 */
/* .line 520 */
(( java.io.File ) v0 ).mkdirs ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
/* .line 521 */
(( java.io.File ) v0 ).getParentFile ( ); // invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* .line 522 */
/* .local v2, "codePathParent":Ljava/io/File; */
(( java.io.File ) v2 ).getName ( ); // invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;
/* const-string/jumbo v5, "~~" */
v4 = (( java.lang.String ) v4 ).startsWith ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* const/16 v5, 0x1fd */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 523 */
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
android.os.FileUtils .setPermissions ( v4,v5,v3,v3 );
/* .line 525 */
} // :cond_3
android.os.FileUtils .setPermissions ( v0,v5,v3,v3 );
/* .line 528 */
} // .end local v2 # "codePathParent":Ljava/io/File;
} // :cond_4
/* new-instance v2, Ljava/io/File; */
final String v4 = "base.apk"; // const-string v4, "base.apk"
/* invoke-direct {v2, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 529 */
/* .local v2, "dstApkFile":Ljava/io/File; */
int v4 = 0; // const/4 v4, 0x0
/* .line 531 */
/* .local v4, "success":Z */
try { // :try_start_0
v5 = this.apkFile;
android.os.FileUtils .copy ( v5,v2 );
/* .line 532 */
/* const/16 v5, 0x1a4 */
v3 = android.os.FileUtils .setPermissions ( v2,v5,v3,v3 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v3, :cond_5 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_5
/* move v4, v1 */
/* .line 536 */
/* .line 533 */
/* :catch_0 */
/* move-exception v1 */
/* .line 534 */
/* .local v1, "e":Ljava/io/IOException; */
v3 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Copy failed from: "; // const-string v6, "Copy failed from: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.apkFile;
(( java.io.File ) v6 ).getPath ( ); // invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " to "; // const-string v6, " to "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 535 */
(( java.io.File ) v2 ).getPath ( ); // invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ":"; // const-string v6, ":"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 534 */
android.util.Slog .d ( v3,v5 );
/* .line 537 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
} // .end method
private static Boolean copyFile ( java.io.File p0, java.io.File p1 ) {
/* .locals 5 */
/* .param p0, "src" # Ljava/io/File; */
/* .param p1, "dst" # Ljava/io/File; */
/* .line 493 */
int v0 = 0; // const/4 v0, 0x0
/* .line 495 */
/* .local v0, "success":Z */
try { // :try_start_0
android.os.FileUtils .copy ( p0,p1 );
/* .line 496 */
/* const/16 v1, 0x1a4 */
int v2 = -1; // const/4 v2, -0x1
v1 = android.os.FileUtils .setPermissions ( p1,v1,v2,v2 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-nez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* move v0, v1 */
/* .line 500 */
/* .line 497 */
/* :catch_0 */
/* move-exception v1 */
/* .line 498 */
/* .local v1, "e":Ljava/io/IOException; */
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Copy failed from: "; // const-string v4, "Copy failed from: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p0 ).getPath ( ); // invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " to "; // const-string v4, " to "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 499 */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ":"; // const-string v4, ":"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 498 */
android.util.Slog .d ( v2,v3 );
/* .line 501 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
} // .end method
private static Boolean copyPreinstallApp ( com.android.server.pm.PreinstallApp$Item p0, com.android.server.pm.PackageSetting p1 ) {
/* .locals 8 */
/* .param p0, "srcApp" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .param p1, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .line 438 */
/* iget v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->type:I */
int v1 = 3; // const/4 v1, 0x3
/* if-eq v0, v1, :cond_0 */
v0 = this.apkFile;
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_0 */
/* .line 439 */
int v0 = 0; // const/4 v0, 0x0
/* .line 442 */
} // :cond_0
v0 = com.android.server.pm.PreinstallApp .copyCTPreinstallApp ( p0,p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 443 */
int v0 = 1; // const/4 v0, 0x1
/* .line 446 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 450 */
/* .local v0, "dstCodePath":Ljava/io/File; */
if ( p1 != null) { // if-eqz p1, :cond_2
v2 = com.android.server.pm.PreinstallApp .underData ( p1 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 451 */
(( com.android.server.pm.PackageSetting ) p1 ).getPath ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageSetting;->getPath()Ljava/io/File;
/* .line 454 */
com.android.server.pm.PreinstallApp .cleanUpResource ( v0 );
/* .line 459 */
} // :cond_2
/* if-nez v0, :cond_3 */
/* .line 460 */
/* new-instance v2, Ljava/io/File; */
v3 = this.apkFile;
(( java.io.File ) v3 ).getName ( ); // invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;
final String v4 = ".apk"; // const-string v4, ".apk"
final String v5 = ""; // const-string v5, ""
(( java.lang.String ) v3 ).replace ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
final String v4 = "/data/app/"; // const-string v4, "/data/app/"
/* invoke-direct {v2, v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* move-object v0, v2 */
/* .line 462 */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 463 */
com.android.server.pm.PreinstallApp .deleteContentsRecursive ( v0 );
/* .line 467 */
} // :cond_3
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_4 */
/* .line 468 */
(( java.io.File ) v0 ).mkdirs ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
/* .line 471 */
} // :cond_4
/* const/16 v2, 0x1fd */
int v3 = -1; // const/4 v3, -0x1
android.os.FileUtils .setPermissions ( v0,v2,v3,v3 );
/* .line 473 */
(( java.io.File ) v0 ).getParentFile ( ); // invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* .line 474 */
/* .local v4, "codePathParent":Ljava/io/File; */
(( java.io.File ) v4 ).getName ( ); // invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;
/* const-string/jumbo v6, "~~" */
v5 = (( java.lang.String ) v5 ).startsWith ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 475 */
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
android.os.FileUtils .setPermissions ( v5,v2,v3,v3 );
/* .line 478 */
} // :cond_5
int v2 = 1; // const/4 v2, 0x1
/* .line 479 */
/* .local v2, "success":Z */
/* iget v3, p0, Lcom/android/server/pm/PreinstallApp$Item;->type:I */
/* if-ne v3, v1, :cond_7 */
/* .line 480 */
v1 = this.pkgLite;
(( android.content.pm.parsing.PackageLite ) v1 ).getAllApkPaths ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getAllApkPaths()Ljava/util/List;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Ljava/lang/String; */
/* .line 481 */
/* .local v3, "apkPath":Ljava/lang/String; */
/* new-instance v5, Ljava/io/File; */
/* invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 482 */
/* .local v5, "apkFile":Ljava/io/File; */
/* new-instance v6, Ljava/io/File; */
(( java.io.File ) v5 ).getName ( ); // invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;
/* invoke-direct {v6, v0, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 483 */
/* .local v6, "dstFile":Ljava/io/File; */
v2 = com.android.server.pm.PreinstallApp .copyFile ( v5,v6 );
/* .line 484 */
} // .end local v3 # "apkPath":Ljava/lang/String;
} // .end local v5 # "apkFile":Ljava/io/File;
} // .end local v6 # "dstFile":Ljava/io/File;
} // :cond_6
/* .line 486 */
} // :cond_7
/* new-instance v1, Ljava/io/File; */
final String v3 = "base.apk"; // const-string v3, "base.apk"
/* invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 487 */
/* .local v1, "dstApkFile":Ljava/io/File; */
v3 = this.apkFile;
v2 = com.android.server.pm.PreinstallApp .copyFile ( v3,v1 );
/* .line 489 */
} // .end local v1 # "dstApkFile":Ljava/io/File;
} // :goto_1
} // .end method
public static void copyPreinstallApps ( com.android.server.pm.PackageManagerService p0, com.android.server.pm.Settings p1 ) {
/* .locals 3 */
/* .param p0, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p1, "settings" # Lcom/android/server/pm/Settings; */
/* .line 1123 */
/* const-string/jumbo v0, "vold.decrypt" */
android.os.SystemProperties .get ( v0 );
/* .line 1124 */
/* .local v0, "cryptState":Ljava/lang/String; */
/* const-string/jumbo v1, "trigger_restart_min_framework" */
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1125 */
v1 = com.android.server.pm.PreinstallApp.TAG;
final String v2 = "Detected encryption in progress - can\'t copy preinstall apps now!"; // const-string v2, "Detected encryption in progress - can\'t copy preinstall apps now!"
android.util.Slog .w ( v1,v2 );
/* .line 1126 */
return;
/* .line 1128 */
} // :cond_0
com.android.server.pm.PreinstallApp .readPackageVersionMap ( );
/* .line 1129 */
com.android.server.pm.PreinstallApp .readPackagePAIList ( );
/* .line 1130 */
com.android.server.pm.PreinstallApp .copyTraditionalTrackFileToNewLocationIfNeed ( );
/* .line 1131 */
v1 = (( com.android.server.pm.PackageManagerService ) p0 ).isFirstBoot ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
/* if-nez v1, :cond_1 */
v1 = (( com.android.server.pm.PackageManagerService ) p0 ).isDeviceUpgrading ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1132 */
} // :cond_1
com.android.server.pm.PreinstallApp .parseAndDeleteDuplicatePreinstallApps ( );
/* .line 1135 */
} // :cond_2
v1 = (( com.android.server.pm.PackageManagerService ) p0 ).isFirstBoot ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1136 */
com.android.server.pm.PreinstallApp .copyPreinstallAppsForFirstBoot ( p0,p1 );
/* .line 1137 */
} // :cond_3
v1 = (( com.android.server.pm.PackageManagerService ) p0 ).isDeviceUpgrading ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService;->isDeviceUpgrading()Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1138 */
com.android.server.pm.PreinstallApp .copyPreinstallAppsForBoot ( p0,p1 );
/* .line 1140 */
} // :cond_4
v1 = com.android.server.pm.PreinstallApp.TAG;
final String v2 = "Nothing need copy for normal boot."; // const-string v2, "Nothing need copy for normal boot."
android.util.Slog .i ( v1,v2 );
/* .line 1142 */
} // :goto_0
return;
} // .end method
private static void copyPreinstallAppsForBoot ( com.android.server.pm.PackageManagerService p0, com.android.server.pm.Settings p1 ) {
/* .locals 19 */
/* .param p0, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p1, "settings" # Lcom/android/server/pm/Settings; */
/* .line 988 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* .line 989 */
/* .local v2, "currentTime":J */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
/* .line 990 */
/* .local v4, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
/* new-instance v5, Lcom/android/server/pm/DeletePackageHelper; */
/* invoke-direct {v5, v0}, Lcom/android/server/pm/DeletePackageHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V */
/* .line 992 */
/* .local v5, "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
v6 = com.android.server.pm.PreinstallApp.OTA_SKIP_BUSINESS_APP_LIST_FILE;
v7 = com.android.server.pm.PreinstallApp.NOT_OTA_PACKAGE_NAMES;
com.android.server.pm.PreinstallApp .readLineToSet ( v6,v7 );
/* .line 999 */
com.android.server.pm.PreinstallApp .readHistory ( v4 );
/* .line 1000 */
com.android.server.pm.PreinstallApp .readAllowOnlyInstallThirdAppForCarrier ( );
/* .line 1001 */
com.android.server.pm.PreinstallApp .initCotaDelAppsList ( );
/* .line 1002 */
v6 = com.android.server.pm.PreinstallApp.TAG;
final String v7 = "copy preinstall apps start"; // const-string v7, "copy preinstall apps start"
android.util.Slog .i ( v6,v7 );
/* .line 1003 */
v6 = com.android.server.pm.PreinstallApp.sPreinstallApps;
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_13
/* check-cast v7, Lcom/android/server/pm/PreinstallApp$Item; */
/* .line 1004 */
/* .local v7, "item":Lcom/android/server/pm/PreinstallApp$Item; */
v8 = com.android.server.pm.PreinstallApp .skipYouPinIfHadPreinstall ( v4,v7 );
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 1005 */
v8 = com.android.server.pm.PreinstallApp.TAG;
/* const-string/jumbo v9, "skip YouPin because pre-installed" */
android.util.Slog .i ( v8,v9 );
/* .line 1006 */
/* .line 1008 */
} // :cond_0
v8 = com.android.server.pm.PreinstallApp .skipOTA ( v7 );
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 1009 */
v8 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "ota skip copy business preinstall data-app, :"; // const-string v10, "ota skip copy business preinstall data-app, :"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.packageName;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v8,v9 );
/* .line 1010 */
/* .line 1012 */
} // :cond_1
v8 = com.android.server.pm.PreinstallApp .skipInstallByCota ( v7 );
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 1013 */
v8 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "cota skip install cust preinstall data-app, :"; // const-string v10, "cota skip install cust preinstall data-app, :"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v8,v9 );
/* .line 1014 */
/* .line 1017 */
} // :cond_2
v8 = com.android.server.pm.PreinstallApp .dealed ( v4,v7 );
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 1018 */
/* .line 1021 */
} // :cond_3
v8 = this.apkFile;
v8 = com.android.server.pm.PreinstallApp .signCheck ( v8 );
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 1022 */
v8 = com.android.server.pm.PreinstallApp.TAG;
final String v9 = "Skip copying when the sign is false."; // const-string v9, "Skip copying when the sign is false."
android.util.Slog .i ( v8,v9 );
/* .line 1023 */
/* .line 1027 */
} // :cond_4
v8 = com.android.server.pm.PreinstallApp .recorded ( v4,v7 );
/* .line 1029 */
/* .local v8, "recorded":Z */
v9 = this.packageName;
(( com.android.server.pm.Settings ) v1 ).getPackageLPr ( v9 ); // invoke-virtual {v1, v9}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
/* .line 1031 */
/* .local v9, "ps":Lcom/android/server/pm/PackageSetting; */
v10 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "Copy "; // const-string v12, "Copy "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v10,v11 );
/* .line 1032 */
final String v11 = " failed"; // const-string v11, " failed"
final String v13 = ", skip coping"; // const-string v13, ", skip coping"
/* if-nez v9, :cond_a */
/* .line 1035 */
/* if-nez v8, :cond_9 */
/* .line 1038 */
final String v13 = "persist.sys.cota.carrier"; // const-string v13, "persist.sys.cota.carrier"
final String v14 = ""; // const-string v14, ""
android.os.SystemProperties .get ( v13,v14 );
/* .line 1039 */
/* .local v13, "cotaCarrier":Ljava/lang/String; */
v15 = android.text.TextUtils .isEmpty ( v13 );
/* if-nez v15, :cond_5 */
final String v15 = "XM"; // const-string v15, "XM"
v15 = (( java.lang.String ) v15 ).equals ( v13 ); // invoke-virtual {v15, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v15, :cond_5 */
v15 = com.android.server.pm.PreinstallApp.sCotaDelApps;
/* move-object/from16 v16, v5 */
} // .end local v5 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .local v16, "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
v5 = this.packageName;
v5 = /* .line 1040 */
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 1041 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "skip app because cota-reject: " */
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v13 ); // invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v10,v5 );
/* .line 1042 */
com.android.server.pm.PreinstallApp .recordHistory ( v4,v7 );
/* .line 1043 */
/* move-object/from16 v5, v16 */
/* goto/16 :goto_0 */
/* .line 1039 */
} // .end local v16 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .restart local v5 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
} // :cond_5
/* move-object/from16 v16, v5 */
/* .line 1046 */
} // .end local v5 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .restart local v16 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
} // :cond_6
final String v5 = "persist.sys.carrier.name"; // const-string v5, "persist.sys.carrier.name"
android.os.SystemProperties .get ( v5,v14 );
/* .line 1047 */
/* .local v5, "DTCarrier":Ljava/lang/String; */
v14 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v14, :cond_7 */
final String v14 = "DT"; // const-string v14, "DT"
v14 = (( java.lang.String ) v14 ).equals ( v5 ); // invoke-virtual {v14, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_7
v14 = this.packageName;
/* .line 1048 */
final String v15 = "com.xiaomi.smarthome"; // const-string v15, "com.xiaomi.smarthome"
v14 = (( java.lang.String ) v15 ).equals ( v14 ); // invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v14 != null) { // if-eqz v14, :cond_7
/* .line 1049 */
/* const-string/jumbo v11, "skip app because DTCarrier-reject" */
android.util.Slog .i ( v10,v11 );
/* .line 1050 */
com.android.server.pm.PreinstallApp .recordHistory ( v4,v7 );
/* .line 1051 */
/* move-object/from16 v5, v16 */
/* goto/16 :goto_0 */
/* .line 1054 */
} // :cond_7
int v14 = 0; // const/4 v14, 0x0
v14 = com.android.server.pm.PreinstallApp .copyPreinstallApp ( v7,v14 );
if ( v14 != null) { // if-eqz v14, :cond_8
/* .line 1055 */
v10 = com.android.server.pm.PreinstallApp.sNewOrUpdatePreinstallApps;
/* .line 1056 */
com.android.server.pm.PreinstallApp .recordHistory ( v4,v7 );
/* .line 1058 */
} // :cond_8
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v14 ).append ( v12 ); // invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v14 ); // invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v11 ); // invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v11 );
/* .line 1060 */
} // .end local v5 # "DTCarrier":Ljava/lang/String;
} // .end local v13 # "cotaCarrier":Ljava/lang/String;
} // :goto_1
/* goto/16 :goto_3 */
/* .line 1062 */
} // .end local v16 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .local v5, "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
} // :cond_9
/* move-object/from16 v16, v5 */
} // .end local v5 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .restart local v16 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "User had uninstalled "; // const-string v11, "User had uninstalled "
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v11 = this.packageName;
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v13 ); // invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v10,v5 );
/* .line 1063 */
com.android.server.pm.PreinstallApp .recordHistory ( v4,v7 );
/* goto/16 :goto_3 */
/* .line 1066 */
} // .end local v16 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .restart local v5 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
} // :cond_a
/* move-object/from16 v16, v5 */
} // .end local v5 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .restart local v16 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
v5 = this.pkgLite;
v5 = (( android.content.pm.parsing.PackageLite ) v5 ).getVersionCode ( ); // invoke-virtual {v5}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
/* int-to-long v14, v5 */
(( com.android.server.pm.PackageSetting ) v9 ).getVersionCode ( ); // invoke-virtual {v9}, Lcom/android/server/pm/PackageSetting;->getVersionCode()J
/* move-result-wide v17 */
/* cmp-long v5, v14, v17 */
/* if-gtz v5, :cond_b */
/* .line 1067 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " is not newer than "; // const-string v11, " is not newer than "
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1068 */
(( com.android.server.pm.PackageSetting ) v9 ).getPathString ( ); // invoke-virtual {v9}, Lcom/android/server/pm/PackageSetting;->getPathString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = "["; // const-string v11, "["
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PackageSetting ) v9 ).getVersionCode ( ); // invoke-virtual {v9}, Lcom/android/server/pm/PackageSetting;->getVersionCode()J
/* move-result-wide v11 */
(( java.lang.StringBuilder ) v5 ).append ( v11, v12 ); // invoke-virtual {v5, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = "], skip coping"; // const-string v11, "], skip coping"
(( java.lang.StringBuilder ) v5 ).append ( v11 ); // invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1067 */
android.util.Slog .w ( v10,v5 );
/* .line 1070 */
com.android.server.pm.PreinstallApp .recordHistory ( v4,v7 );
/* .line 1071 */
/* move-object/from16 v5, v16 */
/* goto/16 :goto_0 */
/* .line 1076 */
} // :cond_b
v5 = this.apkFile;
com.android.server.pm.PreinstallApp .parsePackage ( v5 );
/* .line 1077 */
/* .local v5, "pkg":Landroid/content/pm/parsing/ApkLite; */
if ( v5 != null) { // if-eqz v5, :cond_12
(( android.content.pm.parsing.ApkLite ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/pm/parsing/ApkLite;->getPackageName()Ljava/lang/String;
/* if-nez v14, :cond_c */
/* goto/16 :goto_4 */
/* .line 1083 */
} // :cond_c
(( android.content.pm.parsing.ApkLite ) v5 ).getSigningDetails ( ); // invoke-virtual {v5}, Landroid/content/pm/parsing/ApkLite;->getSigningDetails()Landroid/content/pm/SigningDetails;
(( android.content.pm.SigningDetails ) v14 ).getSignatures ( ); // invoke-virtual {v14}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;
/* .line 1084 */
(( com.android.server.pm.PackageSetting ) v9 ).getSignatures ( ); // invoke-virtual {v9}, Lcom/android/server/pm/PackageSetting;->getSignatures()Lcom/android/server/pm/PackageSignatures;
v15 = this.mSigningDetails;
(( android.content.pm.SigningDetails ) v15 ).getSignatures ( ); // invoke-virtual {v15}, Landroid/content/pm/SigningDetails;->getSignatures()[Landroid/content/pm/Signature;
/* .line 1083 */
v14 = com.android.server.pm.PackageManagerServiceUtils .compareSignatures ( v14,v15 );
/* if-nez v14, :cond_d */
int v14 = 1; // const/4 v14, 0x1
} // :cond_d
int v14 = 0; // const/4 v14, 0x0
/* .line 1085 */
/* .local v14, "sameSignatures":Z */
} // :goto_2
/* if-nez v14, :cond_e */
v17 = com.android.server.pm.PreinstallApp .isSystemApp ( v9 );
if ( v17 != null) { // if-eqz v17, :cond_e
/* .line 1086 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " mismatch signature with system app "; // const-string v12, " mismatch signature with system app "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1087 */
(( com.android.server.pm.PackageSetting ) v9 ).getPathString ( ); // invoke-virtual {v9}, Lcom/android/server/pm/PackageSetting;->getPathString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1086 */
android.util.Slog .e ( v10,v11 );
/* .line 1088 */
com.android.server.pm.PreinstallApp .recordHistory ( v4,v7 );
/* .line 1089 */
/* move-object/from16 v5, v16 */
/* goto/16 :goto_0 */
/* .line 1092 */
} // :cond_e
/* if-nez v14, :cond_f */
/* .line 1093 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " mismatch signature with "; // const-string v12, " mismatch signature with "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PackageSetting ) v9 ).getPathString ( ); // invoke-virtual {v9}, Lcom/android/server/pm/PackageSetting;->getPathString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = ", continue"; // const-string v12, ", continue"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v11 );
/* .line 1095 */
com.android.server.pm.PreinstallApp .recordHistory ( v4,v7 );
/* .line 1096 */
/* move-object/from16 v5, v16 */
/* goto/16 :goto_0 */
/* .line 1099 */
} // :cond_f
v13 = this.packageName;
v13 = com.android.server.pm.PreinstallApp .systemAppDeletedOrDisabled ( v0,v13 );
/* if-nez v13, :cond_10 */
v13 = com.android.server.pm.PreinstallApp .isSystemAndNotUpdatedSystemApp ( v9 );
if ( v13 != null) { // if-eqz v13, :cond_10
/* .line 1101 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "Copying new system updated preinstall app "; // const-string v15, "Copying new system updated preinstall app "
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = ", disable system package first."; // const-string v15, ", disable system package first."
(( java.lang.StringBuilder ) v13 ).append ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v10,v13 );
/* .line 1103 */
v13 = this.packageName;
int v15 = 1; // const/4 v15, 0x1
(( com.android.server.pm.Settings ) v1 ).disableSystemPackageLPw ( v13, v15 ); // invoke-virtual {v1, v13, v15}, Lcom/android/server/pm/Settings;->disableSystemPackageLPw(Ljava/lang/String;Z)Z
/* .line 1104 */
v13 = this.packageName;
com.android.server.pm.PreinstallApp .removePackageLI ( v0,v13,v15 );
/* .line 1107 */
} // :cond_10
v13 = com.android.server.pm.PreinstallApp .copyPreinstallApp ( v7,v9 );
if ( v13 != null) { // if-eqz v13, :cond_11
/* .line 1108 */
v10 = com.android.server.pm.PreinstallApp.sNewOrUpdatePreinstallApps;
/* .line 1109 */
com.android.server.pm.PreinstallApp .recordHistory ( v4,v7 );
/* .line 1112 */
} // :cond_11
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v13 ).append ( v12 ); // invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v11 ); // invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v11 );
/* .line 1115 */
} // .end local v5 # "pkg":Landroid/content/pm/parsing/ApkLite;
} // .end local v7 # "item":Lcom/android/server/pm/PreinstallApp$Item;
} // .end local v8 # "recorded":Z
} // .end local v9 # "ps":Lcom/android/server/pm/PackageSetting;
} // .end local v14 # "sameSignatures":Z
} // :goto_3
/* move-object/from16 v5, v16 */
/* goto/16 :goto_0 */
/* .line 1078 */
/* .restart local v5 # "pkg":Landroid/content/pm/parsing/ApkLite; */
/* .restart local v7 # "item":Lcom/android/server/pm/PreinstallApp$Item; */
/* .restart local v8 # "recorded":Z */
/* .restart local v9 # "ps":Lcom/android/server/pm/PackageSetting; */
} // :cond_12
} // :goto_4
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "Parse "; // const-string v12, "Parse "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " failed, skip"; // const-string v12, " failed, skip"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v11 );
/* .line 1079 */
/* move-object/from16 v5, v16 */
/* goto/16 :goto_0 */
/* .line 1116 */
} // .end local v7 # "item":Lcom/android/server/pm/PreinstallApp$Item;
} // .end local v8 # "recorded":Z
} // .end local v9 # "ps":Lcom/android/server/pm/PackageSetting;
} // .end local v16 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .local v5, "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
} // :cond_13
/* move-object/from16 v16, v5 */
} // .end local v5 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper;
/* .restart local v16 # "deletePackageHelper":Lcom/android/server/pm/DeletePackageHelper; */
v5 = com.android.server.pm.PreinstallApp.sAllowOnlyInstallThirdApps;
/* .line 1117 */
v5 = com.android.server.pm.PreinstallApp.sDisallowInstallThirdApps;
/* .line 1118 */
com.android.server.pm.PreinstallApp .writeHistory ( v4 );
/* .line 1119 */
v5 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "copy preinstall apps end, consume "; // const-string v7, "copy preinstall apps end, consume "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v2 */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms"; // const-string v7, "ms"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 1120 */
return;
} // .end method
private static void copyPreinstallAppsForFirstBoot ( com.android.server.pm.PackageManagerService p0, com.android.server.pm.Settings p1 ) {
/* .locals 13 */
/* .param p0, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p1, "settings" # Lcom/android/server/pm/Settings; */
/* .line 889 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 890 */
/* .local v0, "currentTime":J */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* .line 892 */
/* .local v2, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
v3 = com.android.server.pm.PreinstallApp .existHistory ( );
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 895 */
v3 = com.android.server.pm.PreinstallApp.TAG;
final String v4 = "Exist preinstall history, skip copying preinstall apps for first boot!"; // const-string v4, "Exist preinstall history, skip copying preinstall apps for first boot!"
android.util.Slog .w ( v3,v4 );
/* .line 896 */
return;
/* .line 898 */
} // :cond_0
v3 = com.android.server.pm.PreinstallApp.TAG;
final String v4 = "Copy preinstall apps start for first boot"; // const-string v4, "Copy preinstall apps start for first boot"
android.util.Slog .i ( v3,v4 );
/* .line 901 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 902 */
/* .local v3, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
/* .line 904 */
/* .local v4, "pkgPathMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
v5 = com.android.server.pm.PreinstallApp.sPreinstallApps;
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_6
/* check-cast v6, Lcom/android/server/pm/PreinstallApp$Item; */
/* .line 905 */
/* .local v6, "item":Lcom/android/server/pm/PreinstallApp$Item; */
v7 = this.apkFile;
v7 = com.android.server.pm.PreinstallApp .signCheck ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 906 */
v7 = com.android.server.pm.PreinstallApp.TAG;
final String v8 = "Skip copying when the sign is false for first boot."; // const-string v8, "Skip copying when the sign is false for first boot."
android.util.Slog .i ( v7,v8 );
/* .line 907 */
/* .line 910 */
} // :cond_1
v7 = this.pkgLite;
/* .line 911 */
/* .local v7, "pl":Landroid/content/pm/parsing/PackageLite; */
int v8 = 0; // const/4 v8, 0x0
/* .line 912 */
/* .local v8, "ps":Lcom/android/server/pm/PackageSetting; */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 913 */
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( com.android.server.pm.Settings ) p1 ).getPackageLPr ( v9 ); // invoke-virtual {p1, v9}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
/* .line 914 */
v9 = com.android.server.pm.PreinstallApp .isSystemAndNotUpdatedSystemApp ( v8 );
if ( v9 != null) { // if-eqz v9, :cond_2
v9 = (( android.content.pm.parsing.PackageLite ) v7 ).getVersionCode ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
/* int-to-long v9, v9 */
(( com.android.server.pm.PackageSetting ) v8 ).getVersionCode ( ); // invoke-virtual {v8}, Lcom/android/server/pm/PackageSetting;->getVersionCode()J
/* move-result-wide v11 */
/* cmp-long v9, v9, v11 */
/* if-ltz v9, :cond_2 */
/* .line 917 */
v9 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "Copying new system updated preinstall app "; // const-string v11, "Copying new system updated preinstall app "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v6 ).toString ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", disable system package firstly."; // const-string v11, ", disable system package firstly."
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v9,v10 );
/* .line 919 */
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
int v10 = 1; // const/4 v10, 0x1
(( com.android.server.pm.Settings ) p1 ).disableSystemPackageLPw ( v9, v10 ); // invoke-virtual {p1, v9, v10}, Lcom/android/server/pm/Settings;->disableSystemPackageLPw(Ljava/lang/String;Z)Z
/* .line 920 */
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
com.android.server.pm.PreinstallApp .removePackageLI ( p0,v9,v10 );
/* .line 924 */
} // :cond_2
v9 = com.android.server.pm.PreinstallApp .copyPreinstallApp ( v6,v8 );
final String v10 = "Copy "; // const-string v10, "Copy "
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 925 */
v9 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v6 ).toString ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " for first boot"; // const-string v11, " for first boot"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v9,v10 );
/* .line 927 */
if ( v7 != null) { // if-eqz v7, :cond_4
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v9 = android.text.TextUtils .isEmpty ( v9 );
/* if-nez v9, :cond_4 */
/* .line 928 */
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v10 = (( android.content.pm.parsing.PackageLite ) v7 ).getVersionCode ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
java.lang.Integer .valueOf ( v10 );
/* .line 929 */
/* iget v9, v6, Lcom/android/server/pm/PreinstallApp$Item;->type:I */
int v10 = 3; // const/4 v10, 0x3
/* if-ne v9, v10, :cond_3 */
/* .line 930 */
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v10 = this.pkgLite;
(( android.content.pm.parsing.PackageLite ) v10 ).getBaseApkPath ( ); // invoke-virtual {v10}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;
/* .line 932 */
} // :cond_3
(( android.content.pm.parsing.PackageLite ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v10 = this.apkFile;
(( java.io.File ) v10 ).getPath ( ); // invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 935 */
} // :cond_4
} // :goto_1
com.android.server.pm.PreinstallApp .recordHistory ( v2,v6 );
/* .line 936 */
com.android.server.pm.PreinstallApp .recordAdvanceAppsHistory ( v6 );
/* .line 938 */
} // :cond_5
v9 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v6 ).toString ( ); // invoke-virtual {v6}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " failed for first boot"; // const-string v11, " failed for first boot"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v9,v10 );
/* .line 940 */
} // .end local v6 # "item":Lcom/android/server/pm/PreinstallApp$Item;
} // .end local v7 # "pl":Landroid/content/pm/parsing/PackageLite;
} // .end local v8 # "ps":Lcom/android/server/pm/PackageSetting;
} // :goto_2
/* goto/16 :goto_0 */
/* .line 941 */
} // :cond_6
com.android.server.pm.PreinstallApp .writeHistory ( v2 );
/* .line 944 */
com.android.server.pm.PreinstallApp .writePreinstallPackage ( v3 );
/* .line 946 */
com.android.server.pm.PreinstallApp .writePreInstallPackagePath ( v4 );
/* .line 947 */
v5 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Copy preinstall apps end for first boot, consume "; // const-string v7, "Copy preinstall apps end for first boot, consume "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v0 */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms"; // const-string v7, "ms"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 948 */
return;
} // .end method
public static void copyPreinstallPAITrackingFile ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 10 */
/* .param p0, "type" # Ljava/lang/String; */
/* .param p1, "fileName" # Ljava/lang/String; */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 1649 */
v0 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v0, :cond_9 */
v0 = android.text.TextUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_3 */
/* .line 1652 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1653 */
/* .local v0, "isAppsflyer":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 1654 */
/* .local v1, "filePath":Ljava/lang/String; */
final String v2 = "appsflyer"; // const-string v2, "appsflyer"
v3 = android.text.TextUtils .equals ( v2,p0 );
/* const-string/jumbo v4, "xiaomi" */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1655 */
final String v1 = "/data/miui/pai/pre_install.appsflyer"; // const-string v1, "/data/miui/pai/pre_install.appsflyer"
/* .line 1656 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1657 */
} // :cond_1
v3 = android.text.TextUtils .equals ( v4,p0 );
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 1658 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "/data/miui/pai/"; // const-string v5, "/data/miui/pai/"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1664 */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
v3 = v3 = com.android.server.pm.PreinstallApp.mNewTrackContentList;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1665 */
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Content duplication dose not need to be written again! content is :"; // const-string v4, "Content duplication dose not need to be written again! content is :"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 1666 */
return;
/* .line 1669 */
} // :cond_2
v3 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "use " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p0 ); // invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " tracking method!"; // const-string v6, " tracking method!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v5 );
/* .line 1671 */
int v5 = 0; // const/4 v5, 0x0
/* .line 1673 */
/* .local v5, "bw":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v6, Ljava/io/File; */
/* invoke-direct {v6, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1674 */
/* .local v6, "file":Ljava/io/File; */
v7 = (( java.io.File ) v6 ).exists ( ); // invoke-virtual {v6}, Ljava/io/File;->exists()Z
int v8 = -1; // const/4 v8, -0x1
/* if-nez v7, :cond_4 */
/* .line 1675 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "create tracking file\uff1a"; // const-string v9, "create tracking file\uff1a"
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v6 ).getAbsolutePath ( ); // invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v7 );
/* .line 1676 */
(( java.io.File ) v6 ).getParentFile ( ); // invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;
v7 = (( java.io.File ) v7 ).exists ( ); // invoke-virtual {v7}, Ljava/io/File;->exists()Z
/* if-nez v7, :cond_3 */
/* .line 1677 */
(( java.io.File ) v6 ).getParentFile ( ); // invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v7 ).mkdir ( ); // invoke-virtual {v7}, Ljava/io/File;->mkdir()Z
/* .line 1678 */
(( java.io.File ) v6 ).getParentFile ( ); // invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* const/16 v9, 0x1fd */
android.os.FileUtils .setPermissions ( v7,v9,v8,v8 );
/* .line 1680 */
} // :cond_3
(( java.io.File ) v6 ).createNewFile ( ); // invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
/* .line 1682 */
} // :cond_4
/* const/16 v7, 0x1b4 */
android.os.FileUtils .setPermissions ( v6,v7,v8,v8 );
/* .line 1683 */
/* new-instance v7, Ljava/io/BufferedWriter; */
/* new-instance v8, Ljava/io/FileWriter; */
/* invoke-direct {v8, v6, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* invoke-direct {v7, v8}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v5, v7 */
/* .line 1684 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1685 */
v7 = com.android.server.pm.PreinstallApp.mNewTrackContentList;
/* .line 1687 */
} // :cond_5
(( java.io.BufferedWriter ) v5 ).write ( p2 ); // invoke-virtual {v5, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1688 */
final String v7 = "\n"; // const-string v7, "\n"
(( java.io.BufferedWriter ) v5 ).write ( v7 ); // invoke-virtual {v5, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1689 */
(( java.io.BufferedWriter ) v5 ).flush ( ); // invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V
/* .line 1690 */
final String v7 = "Copy PAI tracking content Success!"; // const-string v7, "Copy PAI tracking content Success!"
android.util.Slog .i ( v3,v7 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1702 */
/* nop */
} // .end local v6 # "file":Ljava/io/File;
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 1703 */
/* nop */
/* .line 1704 */
com.android.server.pm.PreinstallApp .restoreconPreinstallDir ( );
/* .line 1705 */
return;
/* .line 1702 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1692 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1693 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_1
v2 = android.text.TextUtils .equals ( v2,p0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
final String v6 = ":"; // const-string v6, ":"
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 1694 */
try { // :try_start_2
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Error occurs when to copy PAI tracking content("; // const-string v7, "Error occurs when to copy PAI tracking content("
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ") into "; // const-string v7, ") into "
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v4 );
/* .line 1696 */
} // :cond_6
v2 = android.text.TextUtils .equals ( v4,p0 );
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 1697 */
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Error occurs when to create "; // const-string v7, "Error occurs when to create "
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " PAI tracking file into "; // const-string v7, " PAI tracking file into "
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1702 */
} // :cond_7
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 1700 */
return;
/* .line 1702 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v5 );
/* .line 1703 */
/* throw v2 */
/* .line 1660 */
} // .end local v5 # "bw":Ljava/io/BufferedWriter;
} // :cond_8
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Used invalid pai tracking type ="; // const-string v4, "Used invalid pai tracking type ="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "! you can use type:appsflyer or xiaomi"; // const-string v4, "! you can use type:appsflyer or xiaomi"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 1662 */
return;
/* .line 1650 */
} // .end local v0 # "isAppsflyer":Z
} // .end local v1 # "filePath":Ljava/lang/String;
} // :cond_9
} // :goto_3
return;
} // .end method
private static void copyTraditionalTrackFileToNewLocationIfNeed ( ) {
/* .locals 6 */
/* .line 1713 */
com.android.server.pm.PreinstallApp .readNewPAITrackFileIfNeed ( );
/* .line 1714 */
final String v0 = "ro.appsflyer.preinstall.path"; // const-string v0, "ro.appsflyer.preinstall.path"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* .line 1715 */
/* .local v0, "appsflyerPath":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1716 */
v1 = com.android.server.pm.PreinstallApp.TAG;
final String v2 = "no system property ro.appsflyer.preinstall.path"; // const-string v2, "no system property ro.appsflyer.preinstall.path"
android.util.Slog .e ( v1,v2 );
/* .line 1717 */
return;
/* .line 1719 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "/data/miui/pai/pre_install.appsflyer"; // const-string v2, "/data/miui/pai/pre_install.appsflyer"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1720 */
/* .local v1, "paiTrackingPath":Ljava/io/File; */
v2 = android.text.TextUtils .equals ( v0,v2 );
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_2 */
/* .line 1722 */
try { // :try_start_0
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "create new appsflyer tracking file\uff1a"; // const-string v4, "create new appsflyer tracking file\uff1a"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v1 ).getAbsolutePath ( ); // invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1723 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
v2 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
int v3 = -1; // const/4 v3, -0x1
/* if-nez v2, :cond_1 */
/* .line 1724 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v2 ).mkdir ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdir()Z
/* .line 1725 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* const/16 v4, 0x1fd */
android.os.FileUtils .setPermissions ( v2,v4,v3,v3 );
/* .line 1727 */
} // :cond_1
(( java.io.File ) v1 ).createNewFile ( ); // invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
/* .line 1728 */
/* const/16 v2, 0x1b4 */
android.os.FileUtils .setPermissions ( v1,v2,v3,v3 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1731 */
/* .line 1729 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1730 */
/* .local v2, "e":Ljava/io/IOException; */
v3 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error occurs when to create new appsflyer tracking"; // const-string v5, "Error occurs when to create new appsflyer tracking"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 1732 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_0
com.android.server.pm.PreinstallApp .readTraditionalPAITrackFile ( );
/* .line 1733 */
com.android.server.pm.PreinstallApp .writeNewPAITrackFile ( );
/* .line 1735 */
} // :cond_2
com.android.server.pm.PreinstallApp .restoreconPreinstallDir ( );
/* .line 1736 */
return;
} // .end method
private static Boolean dealed ( java.util.Map p0, com.android.server.pm.PreinstallApp$Item p1 ) {
/* .locals 1 */
/* .param p1, "item" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;", */
/* "Lcom/android/server/pm/PreinstallApp$Item;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 810 */
/* .local p0, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
v0 = this.apkFile;
v0 = com.android.server.pm.PreinstallApp .dealed ( p0,v0 );
} // .end method
private static Boolean dealed ( java.util.Map p0, java.io.File p1 ) {
/* .locals 4 */
/* .param p1, "apkFile" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;", */
/* "Ljava/io/File;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 802 */
/* .local p0, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
v0 = (( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 803 */
(( java.io.File ) p1 ).lastModified ( ); // invoke-virtual {p1}, Ljava/io/File;->lastModified()J
/* move-result-wide v0 */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
/* check-cast v2, Ljava/lang/Long; */
(( java.lang.Long ) v2 ).longValue ( ); // invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 804 */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v1 = "/system/data-app"; // const-string v1, "/system/data-app"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_0 */
/* .line 805 */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v1 = "/vendor/data-app"; // const-string v1, "/vendor/data-app"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_0 */
/* .line 806 */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v1 = "/product/data-app"; // const-string v1, "/product/data-app"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 802 */
} // :goto_0
} // .end method
private static Boolean deleteContentsRecursive ( java.io.File p0 ) {
/* .locals 8 */
/* .param p0, "dir" # Ljava/io/File; */
/* .line 409 */
(( java.io.File ) p0 ).listFiles ( ); // invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 410 */
/* .local v0, "files":[Ljava/io/File; */
int v1 = 1; // const/4 v1, 0x1
/* .line 411 */
/* .local v1, "success":Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 412 */
/* array-length v2, v0 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, v0, v3 */
/* .line 413 */
/* .local v4, "file":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).isDirectory ( ); // invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 414 */
v5 = com.android.server.pm.PreinstallApp .deleteContentsRecursive ( v4 );
/* and-int/2addr v1, v5 */
/* .line 416 */
} // :cond_0
v5 = (( java.io.File ) v4 ).delete ( ); // invoke-virtual {v4}, Ljava/io/File;->delete()Z
/* if-nez v5, :cond_1 */
/* .line 417 */
v5 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Failed to delete "; // const-string v7, "Failed to delete "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v6 );
/* .line 418 */
int v1 = 0; // const/4 v1, 0x0
/* .line 412 */
} // .end local v4 # "file":Ljava/io/File;
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 422 */
} // :cond_2
} // .end method
private static Boolean doNotNeedUpgradeOnInstallCustApp ( java.util.Map p0, java.io.File p1 ) {
/* .locals 4 */
/* .param p1, "apkFile" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;", */
/* "Ljava/io/File;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 763 */
/* .local p0, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
try { // :try_start_0
v0 = com.android.server.pm.PreinstallApp .recorded ( p0,p1 );
/* .line 764 */
/* .local v0, "recorded":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 765 */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 766 */
/* .local v1, "path":Ljava/lang/String; */
final String v2 = "MIBrowserGlobal.apk"; // const-string v2, "MIBrowserGlobal.apk"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_0 */
final String v2 = "MIBrowserGlobal_removable.apk"; // const-string v2, "MIBrowserGlobal_removable.apk"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 767 */
} // :cond_0
v2 = com.android.server.pm.PreinstallApp.TAG;
final String v3 = "globalBrowser do not need upgrade when install cust apps."; // const-string v3, "globalBrowser do not need upgrade when install cust apps."
android.util.Slog .i ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 768 */
int v2 = 1; // const/4 v2, 0x1
/* .line 773 */
} // .end local v0 # "recorded":Z
} // .end local v1 # "path":Ljava/lang/String;
} // :cond_1
/* .line 771 */
/* :catch_0 */
/* move-exception v0 */
/* .line 772 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.pm.PreinstallApp.TAG;
final String v2 = "Get exception"; // const-string v2, "Get exception"
android.util.Slog .e ( v1,v2,v0 );
/* .line 774 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static void exemptPackagePermissionRestrictions ( com.android.server.pm.pkg.AndroidPackage p0, com.android.server.pm.permission.PermissionManagerService p1 ) {
/* .locals 5 */
/* .param p0, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p1, "permService" # Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 1905 */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 1906 */
/* .local v1, "permissionName":Ljava/lang/String; */
v2 = com.android.server.pm.PreinstallApp .isPermissionRestricted ( v2,v1,p1 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1907 */
int v3 = 4; // const/4 v3, 0x4
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.pm.permission.PermissionManagerService ) p1 ).addAllowlistedRestrictedPermission ( v2, v1, v3, v4 ); // invoke-virtual {p1, v2, v1, v3, v4}, Lcom/android/server/pm/permission/PermissionManagerService;->addAllowlistedRestrictedPermission(Ljava/lang/String;Ljava/lang/String;II)Z
/* .line 1910 */
} // .end local v1 # "permissionName":Ljava/lang/String;
} // :cond_0
/* .line 1911 */
} // :cond_1
return;
} // .end method
public static void exemptPermissionRestrictions ( ) {
/* .locals 8 */
/* .line 1889 */
/* const-class v0, Landroid/content/pm/PackageManagerInternal; */
/* .line 1890 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
/* .line 1891 */
/* .local v0, "packageManagerInt":Landroid/content/pm/PackageManagerInternal; */
/* nop */
/* .line 1892 */
final String v1 = "permissionmgr"; // const-string v1, "permissionmgr"
android.os.ServiceManager .getService ( v1 );
/* check-cast v1, Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 1893 */
/* .local v1, "permService":Lcom/android/server/pm/permission/PermissionManagerService; */
v2 = com.android.server.pm.PreinstallApp.sNewOrUpdatePreinstallApps;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/pm/PreinstallApp$Item; */
/* .line 1894 */
/* .local v3, "item":Lcom/android/server/pm/PreinstallApp$Item; */
v4 = this.packageName;
(( android.content.pm.PackageManagerInternal ) v0 ).getPackage ( v4 ); // invoke-virtual {v0, v4}, Landroid/content/pm/PackageManagerInternal;->getPackage(Ljava/lang/String;)Lcom/android/server/pm/pkg/AndroidPackage;
/* .line 1895 */
/* .local v4, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
/* if-nez v4, :cond_0 */
/* .line 1896 */
/* .line 1898 */
} // :cond_0
v5 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "exemptPermissionRestrictions package: "; // const-string v7, "exemptPermissionRestrictions package: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 1899 */
com.android.server.pm.PreinstallApp .exemptPackagePermissionRestrictions ( v4,v1 );
/* .line 1900 */
} // .end local v3 # "item":Lcom/android/server/pm/PreinstallApp$Item;
} // .end local v4 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
/* .line 1901 */
} // :cond_1
return;
} // .end method
private static Boolean existHistory ( ) {
/* .locals 3 */
/* .line 399 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/app/preinstall_history"; // const-string v1, "/data/app/preinstall_history"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 400 */
/* .line 402 */
} // :cond_0
/* new-instance v0, Ljava/io/File; */
final String v2 = "/data/system/preinstall_history"; // const-string v2, "/data/system/preinstall_history"
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 403 */
/* .line 405 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static java.util.List getAllPreinstallApplist ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 782 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.pm.PreinstallApp .getPreinstallApplist ( v0 );
} // .end method
private static java.io.File getApkFile ( java.io.File p0 ) {
/* .locals 1 */
/* .param p0, "app" # Ljava/io/File; */
/* .line 430 */
v0 = (( java.io.File ) p0 ).isDirectory ( ); // invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 431 */
com.android.server.pm.PreinstallApp .getBaseApkFile ( p0 );
/* .line 433 */
} // :cond_0
} // .end method
private static java.io.File getBaseApkFile ( java.io.File p0 ) {
/* .locals 3 */
/* .param p0, "dir" # Ljava/io/File; */
/* .line 626 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ".apk"; // const-string v2, ".apk"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
} // .end method
static java.util.List getCustomizePreinstallAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 778 */
int v0 = 1; // const/4 v0, 0x1
com.android.server.pm.PreinstallApp .getPreinstallApplist ( v0 );
} // .end method
private static java.io.File getMiBrowserApkFile ( ) {
/* .locals 10 */
/* .line 745 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = com.android.server.pm.PreinstallApp.NONCUSTOMIZED_APP_DIR;
(( java.io.File ) v2 ).getPath ( ); // invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/"; // const-string v2, "/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "MIBrowserGlobal.apk"; // const-string v3, "MIBrowserGlobal.apk"
final String v4 = ".apk"; // const-string v4, ".apk"
final String v5 = ""; // const-string v5, ""
(( java.lang.String ) v3 ).replace ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 746 */
/* .local v0, "apkFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 747 */
/* .line 749 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
v7 = com.android.server.pm.PreinstallApp.PRODUCT_NONCUSTOMIZED_APP_DIR;
(( java.io.File ) v7 ).getPath ( ); // invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "MIBrowserGlobal_removable.apk"; // const-string v8, "MIBrowserGlobal_removable.apk"
(( java.lang.String ) v8 ).replace ( v4, v5 ); // invoke-virtual {v8, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 750 */
/* .local v1, "apkFileRemovable":Ljava/io/File; */
v6 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 751 */
/* .line 754 */
} // :cond_1
/* new-instance v6, Ljava/io/File; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) v7 ).getPath ( ); // invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.String ) v3 ).replace ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v6, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v2, v6 */
/* .line 755 */
/* .local v2, "apkFileProduct":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 756 */
/* .line 758 */
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
} // .end method
private static java.io.File getNextCodePath ( java.io.File p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p0, "targetDir" # Ljava/io/File; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 555 */
/* new-instance v0, Ljava/security/SecureRandom; */
/* invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V */
/* .line 556 */
/* .local v0, "random":Ljava/security/SecureRandom; */
/* const/16 v1, 0x10 */
/* new-array v1, v1, [B */
/* .line 559 */
/* .local v1, "bytes":[B */
} // :cond_0
(( java.security.SecureRandom ) v0 ).nextBytes ( v1 ); // invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V
/* .line 560 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "~~" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 561 */
/* const/16 v3, 0xa */
android.util.Base64 .encodeToString ( v1,v3 );
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 562 */
/* .local v2, "dirName":Ljava/lang/String; */
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, p0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v2, v4 */
/* .line 563 */
/* .local v2, "firstLevelDir":Ljava/io/File; */
v4 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v4, :cond_0 */
/* .line 564 */
(( java.security.SecureRandom ) v0 ).nextBytes ( v1 ); // invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V
/* .line 565 */
android.util.Base64 .encodeToString ( v1,v3 );
/* .line 566 */
/* .local v3, "suffix":Ljava/lang/String; */
/* new-instance v4, Ljava/io/File; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "-"; // const-string v6, "-"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
} // .end method
private static android.content.pm.IPackageManager getPackageManager ( ) {
/* .locals 1 */
/* .line 261 */
final String v0 = "package"; // const-string v0, "package"
android.os.ServiceManager .getService ( v0 );
android.content.pm.IPackageManager$Stub .asInterface ( v0 );
} // .end method
public static java.util.ArrayList getPeinstalledChannelList ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1414 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1415 */
/* .local v0, "preinstalledChannelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
miui.util.CustomizeUtil .getMiuiCustVariantDir ( );
/* .line 1416 */
/* .local v1, "custVariantDir":Ljava/io/File; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1417 */
(( java.io.File ) v1 ).getPath ( ); // invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 1418 */
/* .local v2, "custVariantPath":Ljava/lang/String; */
v3 = com.android.server.pm.PreinstallApp.CUSTOMIZED_APP_DIR;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/customized_channellist"; // const-string v5, "/customized_channellist"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.pm.PreinstallApp .addPreinstallChannelToList ( v0,v3,v4 );
/* .line 1421 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/ota_customized_channellist"; // const-string v5, "/ota_customized_channellist"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.pm.PreinstallApp .addPreinstallChannelToList ( v0,v3,v4 );
/* .line 1424 */
v3 = com.android.server.pm.PreinstallApp.RECOMMENDED_APP_DIR;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/recommended_channellist"; // const-string v5, "/recommended_channellist"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.pm.PreinstallApp .addPreinstallChannelToList ( v0,v3,v4 );
/* .line 1427 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/ota_recommended_channellist"; // const-string v5, "/ota_recommended_channellist"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.pm.PreinstallApp .addPreinstallChannelToList ( v0,v3,v4 );
/* .line 1431 */
} // .end local v2 # "custVariantPath":Ljava/lang/String;
} // :cond_0
} // .end method
private static java.util.ArrayList getPreinstallApplist ( Boolean p0 ) {
/* .locals 8 */
/* .param p0, "onlyCust" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 665 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 666 */
/* .local v0, "preinstallAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;" */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 667 */
/* .local v1, "customizedAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* .line 668 */
/* .local v2, "recommendedAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 670 */
/* .local v3, "productCarrierAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
miui.util.CustomizeUtil .getMiuiCustVariantDir ( );
/* .line 671 */
/* .local v4, "custVariantDir":Ljava/io/File; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 673 */
/* new-instance v5, Ljava/io/File; */
final String v6 = "customized_applist"; // const-string v6, "customized_applist"
/* invoke-direct {v5, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
com.android.server.pm.PreinstallApp .readLineToSet ( v5,v1 );
/* .line 675 */
/* new-instance v5, Ljava/io/File; */
final String v6 = "ota_customized_applist"; // const-string v6, "ota_customized_applist"
/* invoke-direct {v5, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
com.android.server.pm.PreinstallApp .readLineToSet ( v5,v1 );
/* .line 677 */
/* new-instance v5, Ljava/io/File; */
final String v6 = "recommended_applist"; // const-string v6, "recommended_applist"
/* invoke-direct {v5, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
com.android.server.pm.PreinstallApp .readLineToSet ( v5,v2 );
/* .line 679 */
/* new-instance v5, Ljava/io/File; */
final String v6 = "ota_recommended_applist"; // const-string v6, "ota_recommended_applist"
/* invoke-direct {v5, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
com.android.server.pm.PreinstallApp .readLineToSet ( v5,v2 );
/* .line 682 */
} // :cond_0
miui.util.CustomizeUtil .getProductCarrierRegionAppFile ( );
/* .line 683 */
/* .local v5, "carrierRegionAppFile":Ljava/io/File; */
if ( v5 != null) { // if-eqz v5, :cond_1
v6 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 684 */
com.android.server.pm.PreinstallApp .readLineToSet ( v5,v3 );
/* .line 688 */
} // :cond_1
v6 = com.android.server.pm.PreinstallApp.CUSTOMIZED_APP_DIR;
com.android.server.pm.PreinstallApp .addPreinstallAppToList ( v0,v6,v1 );
/* .line 690 */
v6 = com.android.server.pm.PreinstallApp.RECOMMENDED_APP_DIR;
com.android.server.pm.PreinstallApp .addPreinstallAppToList ( v0,v6,v2 );
/* .line 692 */
/* if-nez p0, :cond_2 */
/* .line 693 */
v6 = com.android.server.pm.PreinstallApp.NONCUSTOMIZED_APP_DIR;
int v7 = 0; // const/4 v7, 0x0
com.android.server.pm.PreinstallApp .addPreinstallAppToList ( v0,v6,v7 );
/* .line 694 */
v6 = com.android.server.pm.PreinstallApp.VENDOR_NONCUSTOMIZED_APP_DIR;
com.android.server.pm.PreinstallApp .addPreinstallAppToList ( v0,v6,v7 );
/* .line 695 */
v6 = com.android.server.pm.PreinstallApp.PRODUCT_NONCUSTOMIZED_APP_DIR;
com.android.server.pm.PreinstallApp .addPreinstallAppToList ( v0,v6,v7 );
/* .line 698 */
} // :cond_2
miui.util.CustomizeUtil .getProductCarrierAppDir ( );
com.android.server.pm.PreinstallApp .addPreinstallAppToList ( v0,v6,v3 );
/* .line 699 */
com.android.server.pm.PreinstallApp .operaForCustomize ( v0 );
/* .line 700 */
com.android.server.pm.PreinstallApp .browserRSACustomize ( v0 );
/* .line 701 */
} // .end method
public static Integer getPreinstalledAppVersion ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 1839 */
v0 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* monitor-enter v0 */
/* .line 1840 */
try { // :try_start_0
v1 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* check-cast v1, Ljava/lang/Integer; */
/* .line 1841 */
/* .local v1, "version":Ljava/lang/Integer; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 1842 */
} // .end local v1 # "version":Ljava/lang/Integer;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
static void ignorePreinstallApks ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p0, "fileName" # Ljava/lang/String; */
/* .line 240 */
v0 = com.android.server.pm.PreinstallApp.NONCUSTOMIZED_APP_DIR;
v1 = com.android.server.pm.PreinstallApp.VENDOR_NONCUSTOMIZED_APP_DIR;
v2 = com.android.server.pm.PreinstallApp.PRODUCT_NONCUSTOMIZED_APP_DIR;
v3 = com.android.server.pm.PreinstallApp.CUSTOMIZED_APP_DIR;
v4 = com.android.server.pm.PreinstallApp.RECOMMENDED_APP_DIR;
/* filled-new-array {v0, v1, v2, v3, v4}, [Ljava/io/File; */
/* .line 248 */
/* .local v0, "preinstallDirs":[Ljava/io/File; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_2 */
/* aget-object v3, v0, v2 */
/* .line 249 */
/* .local v3, "dir":Ljava/io/File; */
/* new-instance v4, Ljava/io/File; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) v3 ).getPath ( ); // invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "/"; // const-string v6, "/"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p0 ); // invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 250 */
/* .local v4, "apkFile":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 251 */
v5 = com.android.server.pm.PreinstallApp.sIgnorePreinstallApks;
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 253 */
} // :cond_0
/* new-instance v5, Ljava/io/File; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) v3 ).getPath ( ); // invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ".apk"; // const-string v8, ".apk"
final String v9 = ""; // const-string v9, ""
(( java.lang.String ) p0 ).replace ( v8, v9 ); // invoke-virtual {p0, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p0 ); // invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v4, v5 */
/* .line 254 */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 255 */
v5 = com.android.server.pm.PreinstallApp.sIgnorePreinstallApks;
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 248 */
} // .end local v3 # "dir":Ljava/io/File;
} // .end local v4 # "apkFile":Ljava/io/File;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 258 */
} // :cond_2
return;
} // .end method
private static void initCotaDelAppsList ( ) {
/* .locals 8 */
/* .line 1921 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1923 */
/* .local v0, "inputStream":Ljava/io/InputStream; */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
final String v2 = "/product/opcust/common/del_apps_list.xml"; // const-string v2, "/product/opcust/common/del_apps_list.xml"
/* invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 1924 */
org.xmlpull.v1.XmlPullParserFactory .newInstance ( );
/* .line 1925 */
/* .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory; */
(( org.xmlpull.v1.XmlPullParserFactory ) v1 ).newPullParser ( ); // invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
/* .line 1926 */
/* .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser; */
final String v3 = "UTF-8"; // const-string v3, "UTF-8"
v3 = /* .line 1927 */
/* .line 1928 */
/* .local v3, "event":I */
} // :goto_0
int v4 = 1; // const/4 v4, 0x1
/* if-eq v3, v4, :cond_2 */
/* .line 1929 */
/* packed-switch v3, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 1944 */
/* :pswitch_1 */
/* .line 1933 */
/* :pswitch_2 */
/* .line 1934 */
/* .local v4, "name":Ljava/lang/String; */
final String v5 = "package"; // const-string v5, "package"
v5 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1935 */
final String v5 = "name"; // const-string v5, "name"
int v6 = 0; // const/4 v6, 0x0
/* .line 1936 */
/* .local v5, "pkgName":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 1937 */
v6 = com.android.server.pm.PreinstallApp.TAG;
final String v7 = "initCotaDelApps pkgName is null, skip parse this tag"; // const-string v7, "initCotaDelApps pkgName is null, skip parse this tag"
android.util.Slog .e ( v6,v7 );
/* .line 1938 */
/* .line 1940 */
} // :cond_0
v6 = com.android.server.pm.PreinstallApp.sCotaDelApps;
/* .line 1941 */
/* nop */
} // .end local v5 # "pkgName":Ljava/lang/String;
/* .line 1931 */
} // .end local v4 # "name":Ljava/lang/String;
/* :pswitch_3 */
/* nop */
/* .line 1948 */
} // :cond_1
v4 = } // :goto_1
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v3, v4 */
/* .line 1954 */
} // .end local v1 # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
} // .end local v2 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v3 # "event":I
} // :cond_2
/* nop */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1955 */
/* .line 1954 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 1951 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1952 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "initCotaDelApps fail: "; // const-string v4, "initCotaDelApps fail: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1954 */
/* nop */
} // .end local v1 # "e":Ljava/lang/Exception;
/* .line 1956 */
} // :goto_3
return;
/* .line 1954 */
} // :goto_4
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1955 */
/* throw v1 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public static void installCustApps ( android.content.Context p0 ) {
/* .locals 18 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 1211 */
com.android.server.pm.PreinstallApp .getCustomizePreinstallAppList ( );
/* .line 1212 */
v1 = /* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1213 */
v1 = com.android.server.pm.PreinstallApp.TAG;
final String v2 = " No cust app need to install"; // const-string v2, " No cust app need to install"
android.util.Slog .w ( v1,v2 );
/* .line 1214 */
return;
/* .line 1217 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* .line 1218 */
/* .local v1, "currentTime":J */
v3 = com.android.server.pm.PreinstallApp.TAG;
final String v4 = "Install cust apps start"; // const-string v4, "Install cust apps start"
android.util.Slog .i ( v3,v4 );
/* .line 1219 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 1220 */
/* .local v3, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
com.android.server.pm.PreinstallApp .readHistory ( v3 );
/* .line 1223 */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
/* .line 1224 */
/* .local v4, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* .line 1225 */
/* .local v5, "pkgPathMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
/* .line 1226 */
/* .local v6, "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
com.android.server.pm.PreinstallApp .readAllowOnlyInstallThirdAppForCarrier ( );
/* .line 1227 */
} // :cond_1
v8 = } // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_3
/* check-cast v8, Ljava/io/File; */
/* .line 1228 */
/* .local v8, "app":Ljava/io/File; */
com.android.server.pm.PreinstallApp .getApkFile ( v8 );
/* .line 1229 */
/* .local v9, "apkFile":Ljava/io/File; */
v10 = com.android.server.pm.PreinstallApp .dealed ( v3,v9 );
/* if-nez v10, :cond_1 */
v10 = com.android.server.pm.PreinstallApp .neednotInstall ( v9 );
/* if-nez v10, :cond_1 */
v10 = com.android.server.pm.PreinstallApp .doNotNeedUpgradeOnInstallCustApp ( v3,v9 );
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 1230 */
/* .line 1232 */
} // :cond_2
/* .line 1233 */
} // .end local v8 # "app":Ljava/io/File;
} // .end local v9 # "apkFile":Ljava/io/File;
/* .line 1234 */
} // :cond_3
v7 = com.android.server.pm.PreinstallApp.sCloudControlUninstall;
(( java.util.ArrayList ) v7 ).clear ( ); // invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V
/* .line 1235 */
v7 = com.android.server.pm.PreinstallApp.sAllowOnlyInstallThirdApps;
/* .line 1236 */
v7 = com.android.server.pm.PreinstallApp.sDisallowInstallThirdApps;
/* .line 1238 */
/* move-object/from16 v7, p0 */
com.android.server.pm.InstallerUtil .installAppList ( v7,v6 );
/* .line 1240 */
/* .local v8, "installedResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/io/File;Ljava/lang/Integer;>;" */
v10 = } // :goto_1
if ( v10 != null) { // if-eqz v10, :cond_6
/* check-cast v10, Ljava/util/Map$Entry; */
/* .line 1241 */
/* .local v10, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/io/File;Ljava/lang/Integer;>;" */
/* check-cast v11, Ljava/io/File; */
/* .line 1242 */
/* .local v11, "apkFile":Ljava/io/File; */
/* check-cast v12, Ljava/lang/Integer; */
v12 = (( java.lang.Integer ) v12 ).intValue ( ); // invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I
/* .line 1243 */
/* .local v12, "result":I */
final String v13 = "]"; // const-string v13, "]"
final String v14 = "Install cust app ["; // const-string v14, "Install cust app ["
/* if-nez v12, :cond_5 */
/* .line 1244 */
v15 = com.android.server.pm.PreinstallApp.TAG;
/* move-object/from16 v16, v0 */
} // .end local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .local v16, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v11 ).getPath ( ); // invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = "] mtime["; // const-string v14, "] mtime["
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v17, v6 */
} // .end local v6 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .local v17, "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
(( java.io.File ) v11 ).lastModified ( ); // invoke-virtual {v11}, Ljava/io/File;->lastModified()J
/* move-result-wide v6 */
(( java.lang.StringBuilder ) v0 ).append ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v15,v0 );
/* .line 1245 */
com.android.server.pm.PreinstallApp .parsePackageLite ( v11 );
/* .line 1246 */
/* .local v0, "pl":Landroid/content/pm/parsing/PackageLite; */
if ( v0 != null) { // if-eqz v0, :cond_4
(( android.content.pm.parsing.PackageLite ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v6 = android.text.TextUtils .isEmpty ( v6 );
/* if-nez v6, :cond_4 */
/* .line 1247 */
(( android.content.pm.parsing.PackageLite ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v7 = (( android.content.pm.parsing.PackageLite ) v0 ).getVersionCode ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
java.lang.Integer .valueOf ( v7 );
/* .line 1248 */
(( android.content.pm.parsing.PackageLite ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( java.io.File ) v11 ).getPath ( ); // invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 1250 */
} // :cond_4
com.android.server.pm.PreinstallApp .recordToHistory ( v3,v11 );
/* .line 1251 */
} // .end local v0 # "pl":Landroid/content/pm/parsing/PackageLite;
/* .line 1253 */
} // .end local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v17 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v6 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
} // :cond_5
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v6 */
} // .end local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v6 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .restart local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v17 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
v0 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v14 ); // invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v11 ).getPath ( ); // invoke-virtual {v11}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "] fail, result["; // const-string v7, "] fail, result["
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v12 ); // invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v13 ); // invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v6 );
/* .line 1255 */
} // .end local v10 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/io/File;Ljava/lang/Integer;>;"
} // .end local v11 # "apkFile":Ljava/io/File;
} // .end local v12 # "result":I
} // :goto_2
/* move-object/from16 v7, p0 */
/* move-object/from16 v0, v16 */
/* move-object/from16 v6, v17 */
/* goto/16 :goto_1 */
/* .line 1257 */
} // .end local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v17 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .restart local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v6 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
} // :cond_6
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v6 */
} // .end local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v6 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .restart local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v17 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
com.android.server.pm.PreinstallApp .writeHistory ( v3 );
/* .line 1260 */
com.android.server.pm.PreinstallApp .writePreinstallPackage ( v4 );
/* .line 1262 */
com.android.server.pm.PreinstallApp .writePreInstallPackagePath ( v5 );
/* .line 1263 */
v0 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Install cust apps end, consume "; // const-string v7, "Install cust apps end, consume "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v9 */
/* sub-long/2addr v9, v1 */
(( java.lang.StringBuilder ) v6 ).append ( v9, v10 ); // invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms"; // const-string v7, "ms"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v6 );
/* .line 1264 */
return;
} // .end method
public static void installVanwardCustApps ( android.content.Context p0 ) {
/* .locals 18 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 1271 */
com.android.server.pm.PreinstallApp .getCustomizePreinstallAppList ( );
/* .line 1272 */
/* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 1273 */
/* .local v1, "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v2, Ljava/io/File; */
/* .line 1274 */
miui.util.CustomizeUtil .getMiuiAppDir ( );
/* const-string/jumbo v4, "vanward_applist" */
/* invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 1273 */
com.android.server.pm.PreinstallApp .readLineToSet ( v2,v1 );
v2 = /* .line 1277 */
v2 = /* if-nez v2, :cond_7 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v1 */
/* goto/16 :goto_3 */
/* .line 1282 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* .line 1283 */
/* .local v2, "currentTime":J */
v4 = com.android.server.pm.PreinstallApp.TAG;
final String v5 = "Install vanward cust apps start"; // const-string v5, "Install vanward cust apps start"
android.util.Slog .i ( v4,v5 );
/* .line 1284 */
/* new-instance v4, Ljava/util/HashMap; */
/* invoke-direct {v4}, Ljava/util/HashMap;-><init>()V */
/* .line 1285 */
/* .local v4, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
com.android.server.pm.PreinstallApp .readHistory ( v4 );
/* .line 1288 */
/* new-instance v5, Ljava/util/HashMap; */
/* invoke-direct {v5}, Ljava/util/HashMap;-><init>()V */
/* .line 1289 */
/* .local v5, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v6, Ljava/util/HashMap; */
/* invoke-direct {v6}, Ljava/util/HashMap;-><init>()V */
/* .line 1290 */
/* .local v6, "pkgPathMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
/* new-instance v7, Ljava/util/ArrayList; */
/* invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V */
/* .line 1292 */
/* .local v7, "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
} // :cond_1
v9 = } // :goto_0
if ( v9 != null) { // if-eqz v9, :cond_3
/* check-cast v9, Ljava/io/File; */
/* .line 1293 */
/* .local v9, "app":Ljava/io/File; */
com.android.server.pm.PreinstallApp .getApkFile ( v9 );
/* .line 1294 */
/* .local v10, "apkFile":Ljava/io/File; */
v11 = (( java.io.File ) v10 ).getName ( ); // invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;
if ( v11 != null) { // if-eqz v11, :cond_1
/* .line 1295 */
v11 = com.android.server.pm.PreinstallApp .dealed ( v4,v10 );
if ( v11 != null) { // if-eqz v11, :cond_2
/* .line 1296 */
/* .line 1298 */
} // :cond_2
/* .line 1299 */
} // .end local v9 # "app":Ljava/io/File;
} // .end local v10 # "apkFile":Ljava/io/File;
/* .line 1301 */
} // :cond_3
/* move-object/from16 v8, p0 */
com.android.server.pm.InstallerUtil .installAppList ( v8,v7 );
/* .line 1303 */
/* .local v9, "installedResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/io/File;Ljava/lang/Integer;>;" */
v11 = } // :goto_1
if ( v11 != null) { // if-eqz v11, :cond_6
/* check-cast v11, Ljava/util/Map$Entry; */
/* .line 1304 */
/* .local v11, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/io/File;Ljava/lang/Integer;>;" */
/* check-cast v12, Ljava/io/File; */
/* .line 1305 */
/* .local v12, "apkFile":Ljava/io/File; */
/* check-cast v13, Ljava/lang/Integer; */
v13 = (( java.lang.Integer ) v13 ).intValue ( ); // invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I
/* .line 1306 */
/* .local v13, "result":I */
final String v14 = "]"; // const-string v14, "]"
/* if-nez v13, :cond_5 */
/* .line 1307 */
v15 = com.android.server.pm.PreinstallApp.TAG;
/* move-object/from16 v16, v0 */
} // .end local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .local v16, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v17, v1 */
} // .end local v1 # "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .local v17, "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
final String v1 = "install vanward cust app ["; // const-string v1, "install vanward cust app ["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v12 ).getPath ( ); // invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "] mtime["; // const-string v1, "] mtime["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object v1, v7 */
} // .end local v7 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .local v1, "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
(( java.io.File ) v12 ).lastModified ( ); // invoke-virtual {v12}, Ljava/io/File;->lastModified()J
/* move-result-wide v7 */
(( java.lang.StringBuilder ) v0 ).append ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v15,v0 );
/* .line 1309 */
com.android.server.pm.PreinstallApp .parsePackageLite ( v12 );
/* .line 1310 */
/* .local v0, "pl":Landroid/content/pm/parsing/PackageLite; */
if ( v0 != null) { // if-eqz v0, :cond_4
(( android.content.pm.parsing.PackageLite ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v7 = android.text.TextUtils .isEmpty ( v7 );
/* if-nez v7, :cond_4 */
/* .line 1311 */
(( android.content.pm.parsing.PackageLite ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( java.io.File ) v12 ).getPath ( ); // invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 1312 */
(( android.content.pm.parsing.PackageLite ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v8 = (( android.content.pm.parsing.PackageLite ) v0 ).getVersionCode ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
java.lang.Integer .valueOf ( v8 );
/* .line 1314 */
} // :cond_4
com.android.server.pm.PreinstallApp .recordToHistory ( v4,v12 );
/* .line 1315 */
} // .end local v0 # "pl":Landroid/content/pm/parsing/PackageLite;
/* .line 1317 */
} // .end local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v17 # "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .local v0, "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .local v1, "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .restart local v7 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
} // :cond_5
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v1 */
/* move-object v1, v7 */
} // .end local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v7 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .local v1, "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v17 # "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v0 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Install vanward cust app ["; // const-string v8, "Install vanward cust app ["
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v12 ).getPath ( ); // invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "] fail, result["; // const-string v8, "] fail, result["
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v13 ); // invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v14 ); // invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v7 );
/* .line 1319 */
} // .end local v11 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/io/File;Ljava/lang/Integer;>;"
} // .end local v12 # "apkFile":Ljava/io/File;
} // .end local v13 # "result":I
} // :goto_2
/* move-object/from16 v8, p0 */
/* move-object v7, v1 */
/* move-object/from16 v0, v16 */
/* move-object/from16 v1, v17 */
/* goto/16 :goto_1 */
/* .line 1321 */
} // .end local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v17 # "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .restart local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .local v1, "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .restart local v7 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
} // :cond_6
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v1 */
/* move-object v1, v7 */
} // .end local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v7 # "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
/* .local v1, "needToInstall":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v17 # "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
com.android.server.pm.PreinstallApp .writeHistory ( v4 );
/* .line 1324 */
com.android.server.pm.PreinstallApp .writePreinstallPackage ( v5 );
/* .line 1325 */
com.android.server.pm.PreinstallApp .writePreInstallPackagePath ( v6 );
/* .line 1326 */
v0 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Install vanward cust apps end, consume "; // const-string v8, "Install vanward cust apps end, consume "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v10 */
/* sub-long/2addr v10, v2 */
(( java.lang.StringBuilder ) v7 ).append ( v10, v11 ); // invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = "ms"; // const-string v8, "ms"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v7 );
/* .line 1327 */
return;
/* .line 1277 */
} // .end local v2 # "currentTime":J
} // .end local v4 # "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
} // .end local v5 # "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
} // .end local v6 # "pkgPathMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v9 # "installedResult":Ljava/util/Map;, "Ljava/util/Map<Ljava/io/File;Ljava/lang/Integer;>;"
} // .end local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v17 # "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .restart local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .local v1, "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
} // :cond_7
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v1 */
/* .line 1278 */
} // .end local v0 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
} // .end local v1 # "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .restart local v16 # "custAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* .restart local v17 # "vanwardCustAppSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
} // :goto_3
v0 = com.android.server.pm.PreinstallApp.TAG;
final String v1 = "No vanward cust app need to install"; // const-string v1, "No vanward cust app need to install"
android.util.Slog .w ( v0,v1 );
/* .line 1279 */
return;
} // .end method
private static final Boolean isApkFile ( java.io.File p0 ) {
/* .locals 2 */
/* .param p0, "apkFile" # Ljava/io/File; */
/* .line 589 */
if ( p0 != null) { // if-eqz p0, :cond_0
(( java.io.File ) p0 ).getPath ( ); // invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v1 = ".apk"; // const-string v1, ".apk"
v0 = (( java.lang.String ) v0 ).endsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isCTPreinstallApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 541 */
v0 = v0 = com.android.server.pm.PreinstallApp.sCTPreinstallApps;
} // .end method
private static Boolean isPermissionRestricted ( java.lang.String p0, java.lang.String p1, com.android.server.pm.permission.PermissionManagerService p2 ) {
/* .locals 3 */
/* .param p0, "pkgName" # Ljava/lang/String; */
/* .param p1, "permissionName" # Ljava/lang/String; */
/* .param p2, "permService" # Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 1915 */
/* nop */
/* .line 1916 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.pm.permission.PermissionManagerService ) p2 ).getPermissionInfo ( p1, p0, v0 ); // invoke-virtual {p2, p1, p0, v0}, Lcom/android/server/pm/permission/PermissionManagerService;->getPermissionInfo(Ljava/lang/String;Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;
/* .line 1917 */
/* .local v1, "permissionInfo":Landroid/content/pm/PermissionInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( android.content.pm.PermissionInfo ) v1 ).isRestricted ( ); // invoke-virtual {v1}, Landroid/content/pm/PermissionInfo;->isRestricted()Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
} // .end method
public static Boolean isPreinstallApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 1846 */
v0 = v0 = com.android.server.pm.PreinstallApp.sPreinstallApps;
} // .end method
public static Boolean isPreinstalledPAIPackage ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 1553 */
v0 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* monitor-enter v0 */
/* .line 1554 */
try { // :try_start_0
v1 = v1 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* monitor-exit v0 */
/* .line 1555 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static Boolean isPreinstalledPackage ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 1546 */
v0 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* monitor-enter v0 */
/* .line 1547 */
try { // :try_start_0
v1 = v1 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* monitor-exit v0 */
/* .line 1548 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private static Boolean isSamePackage ( java.io.File p0, java.io.File p1 ) {
/* .locals 7 */
/* .param p0, "appFileA" # Ljava/io/File; */
/* .param p1, "appFileB" # Ljava/io/File; */
/* .line 1460 */
com.android.server.pm.PreinstallApp .parsePackageLite ( p0 );
/* .line 1461 */
/* .local v0, "plA":Landroid/content/pm/parsing/PackageLite; */
com.android.server.pm.PreinstallApp .parsePackageLite ( p1 );
/* .line 1463 */
/* .local v1, "plB":Landroid/content/pm/parsing/PackageLite; */
final String v2 = " failed, return false"; // const-string v2, " failed, return false"
final String v3 = "Parse "; // const-string v3, "Parse "
int v4 = 0; // const/4 v4, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
(( android.content.pm.parsing.PackageLite ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* if-nez v5, :cond_0 */
/* .line 1468 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_3
(( android.content.pm.parsing.PackageLite ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* if-nez v5, :cond_1 */
/* .line 1473 */
} // :cond_1
(( android.content.pm.parsing.PackageLite ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
(( android.content.pm.parsing.PackageLite ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1474 */
int v2 = 1; // const/4 v2, 0x1
/* .line 1476 */
} // :cond_2
v2 = com.android.server.pm.PreinstallApp.TAG;
final String v3 = "isSamePackage return false."; // const-string v3, "isSamePackage return false."
android.util.Slog .e ( v2,v3 );
/* .line 1477 */
/* .line 1469 */
} // :cond_3
} // :goto_0
v5 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v2 );
/* .line 1470 */
/* .line 1464 */
} // :cond_4
} // :goto_1
v5 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p0 ).getPath ( ); // invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v2 );
/* .line 1465 */
} // .end method
private static Boolean isSplitApk ( java.io.File p0 ) {
/* .locals 2 */
/* .param p0, "file" # Ljava/io/File; */
/* .line 661 */
v0 = (( java.io.File ) p0 ).isDirectory ( ); // invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.io.File ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;
/* const-string/jumbo v1, "split-" */
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isSystemAndNotUpdatedSystemApp ( com.android.server.pm.PackageSetting p0 ) {
/* .locals 1 */
/* .param p0, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .line 794 */
if ( p0 != null) { // if-eqz p0, :cond_0
v0 = com.android.server.pm.PreinstallApp .isSystemApp ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = com.android.server.pm.PreinstallApp .isUpdatedSystemApp ( p0 );
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isSystemApp ( com.android.server.pm.PackageSetting p0 ) {
/* .locals 2 */
/* .param p0, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .line 786 */
v0 = (( com.android.server.pm.PackageSetting ) p0 ).getFlags ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageSetting;->getFlags()I
int v1 = 1; // const/4 v1, 0x1
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private static Boolean isUninstallByMccOrMnc ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 1193 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1195 */
/* .local v0, "isUninstalled":Z */
final String v1 = "com.yandex.searchapp"; // const-string v1, "com.yandex.searchapp"
v1 = (( java.lang.String ) v1 ).equals ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1196 */
final String v1 = "ro.miui.build.region"; // const-string v1, "ro.miui.build.region"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
/* .line 1197 */
/* .local v1, "sku":Ljava/lang/String; */
final String v3 = "eea"; // const-string v3, "eea"
v3 = android.text.TextUtils .equals ( v1,v3 );
/* if-nez v3, :cond_0 */
final String v3 = "global"; // const-string v3, "global"
v3 = android.text.TextUtils .equals ( v1,v3 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1198 */
} // :cond_0
final String v3 = "persist.sys.carrier.subnetwork"; // const-string v3, "persist.sys.carrier.subnetwork"
android.os.SystemProperties .get ( v3,v2 );
final String v3 = "ru_operator"; // const-string v3, "ru_operator"
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/lit8 v2, v2, 0x1 */
/* move v0, v2 */
/* .line 1202 */
} // .end local v1 # "sku":Ljava/lang/String;
} // :cond_1
} // .end method
private static Boolean isUpdatedSystemApp ( com.android.server.pm.PackageSetting p0 ) {
/* .locals 2 */
/* .param p0, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .line 790 */
(( com.android.server.pm.PackageSetting ) p0 ).getPathString ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageSetting;->getPathString()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.android.server.pm.PackageSetting ) p0 ).getPathString ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageSetting;->getPathString()Ljava/lang/String;
final String v1 = "/data/app/"; // const-string v1, "/data/app/"
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static Boolean isValidIme ( java.lang.String p0, java.util.Locale p1 ) {
/* .locals 5 */
/* .param p0, "locale" # Ljava/lang/String; */
/* .param p1, "curLocale" # Ljava/util/Locale; */
/* .line 1330 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p0 ).split ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1331 */
/* .local v0, "locales":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_2 */
/* .line 1332 */
/* aget-object v2, v0, v1 */
(( java.util.Locale ) p1 ).toString ( ); // invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v2, :cond_1 */
/* aget-object v2, v0, v1 */
/* .line 1333 */
final String v3 = "*"; // const-string v3, "*"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* aget-object v2, v0, v1 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1334 */
(( java.util.Locale ) p1 ).getLanguage ( ); // invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "_*"; // const-string v4, "_*"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).startsWith ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1331 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1335 */
} // :cond_1
} // :goto_1
int v2 = 1; // const/4 v2, 0x1
/* .line 1338 */
} // .end local v1 # "i":I
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
static Boolean lambda$cleanUpResource$0 ( java.io.File p0 ) { //synthethic
/* .locals 3 */
/* .param p0, "f" # Ljava/io/File; */
/* .line 573 */
(( java.io.File ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;
final String v1 = ".apk"; // const-string v1, ".apk"
v0 = (( java.lang.String ) v0 ).endsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 574 */
v0 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "list and delete "; // const-string v2, "list and delete "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 575 */
(( java.io.File ) p0 ).delete ( ); // invoke-virtual {p0}, Ljava/io/File;->delete()Z
/* .line 576 */
} // :cond_0
v0 = (( java.io.File ) p0 ).isDirectory ( ); // invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 578 */
com.android.server.pm.PreinstallApp .deleteContentsRecursive ( p0 );
/* .line 579 */
(( java.io.File ) p0 ).delete ( ); // invoke-virtual {p0}, Ljava/io/File;->delete()Z
/* .line 581 */
} // :cond_1
v0 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "list unknown file: "; // const-string v2, "list unknown file: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 583 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static Boolean neednotInstall ( java.io.File p0 ) {
/* .locals 4 */
/* .param p0, "apk" # Ljava/io/File; */
/* .line 1180 */
int v0 = 0; // const/4 v0, 0x0
if ( p0 != null) { // if-eqz p0, :cond_4
/* .line 1181 */
com.android.server.pm.PreinstallApp .parsePackageLite ( p0 );
/* .line 1182 */
/* .local v1, "pl":Landroid/content/pm/parsing/PackageLite; */
if ( v1 != null) { // if-eqz v1, :cond_3
v2 = com.android.server.pm.PreinstallApp.sCloudControlUninstall;
(( android.content.pm.parsing.PackageLite ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v2 = (( java.util.ArrayList ) v2 ).contains ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
v2 = com.android.server.pm.PreinstallApp.sAllowOnlyInstallThirdApps;
v3 = /* .line 1183 */
/* if-lez v3, :cond_0 */
/* .line 1184 */
v2 = (( android.content.pm.parsing.PackageLite ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_0
v2 = com.android.server.pm.PreinstallApp.sDisallowInstallThirdApps;
v3 = /* .line 1185 */
/* if-lez v3, :cond_1 */
/* .line 1186 */
v2 = (( android.content.pm.parsing.PackageLite ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* if-nez v2, :cond_2 */
/* .line 1187 */
} // :cond_1
(( android.content.pm.parsing.PackageLite ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
v2 = com.android.server.pm.PreinstallApp .isUninstallByMccOrMnc ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_3
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
} // :cond_3
/* nop */
/* .line 1182 */
} // :goto_0
/* .line 1189 */
} // .end local v1 # "pl":Landroid/content/pm/parsing/PackageLite;
} // :cond_4
} // .end method
private static void operaForCustomize ( java.util.List p0 ) {
/* .locals 11 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 705 */
/* .local p0, "preinstallAppList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
android.os.SystemProperties .get ( v0 );
final String v1 = "global"; // const-string v1, "global"
v0 = (( java.lang.String ) v1 ).equalsIgnoreCase ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 706 */
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 707 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
android.os.SystemProperties .get ( v0,v1 );
(( java.lang.String ) v0 ).toUpperCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
/* .line 708 */
/* .local v0, "region":Ljava/lang/String; */
v2 = com.android.server.pm.PreinstallApp.AFRICA_COUNTRY;
v2 = java.util.Arrays .asList ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 709 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* .line 710 */
/* .local v2, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
com.android.server.pm.PreinstallApp .readHistory ( v2 );
/* .line 711 */
v3 = com.android.server.pm.PreinstallApp.TAG;
final String v4 = "Opera Africa Customize"; // const-string v4, "Opera Africa Customize"
android.util.Slog .i ( v3,v4 );
/* .line 712 */
/* new-instance v4, Ljava/io/File; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = com.android.server.pm.PreinstallApp.NONCUSTOMIZED_APP_DIR;
(( java.io.File ) v6 ).getPath ( ); // invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "/"; // const-string v6, "/"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "Opera.apk"; // const-string v7, "Opera.apk"
final String v8 = ".apk"; // const-string v8, ".apk"
(( java.lang.String ) v7 ).replace ( v8, v1 ); // invoke-virtual {v7, v8, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 714 */
/* .local v4, "apkFile":Ljava/io/File; */
/* new-instance v5, Ljava/io/File; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
v10 = com.android.server.pm.PreinstallApp.PRODUCT_NONCUSTOMIZED_APP_DIR;
(( java.io.File ) v10 ).getPath ( ); // invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.String ) v7 ).replace ( v8, v1 ); // invoke-virtual {v7, v8, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
(( java.lang.StringBuilder ) v9 ).append ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v1, v5 */
/* .line 715 */
/* .local v1, "apkFileProduct":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_0
v5 = com.android.server.pm.PreinstallApp .recorded ( v2,v4 );
/* if-nez v5, :cond_0 */
/* .line 716 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Opera PreInstall install apkFile: "; // const-string v6, "Opera PreInstall install apkFile: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " hasInstallHistory : "; // const-string v6, " hasInstallHistory : "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = com.android.server.pm.PreinstallApp .recorded ( v2,v4 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v5 );
/* .line 717 */
/* .line 718 */
} // :cond_0
v5 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = com.android.server.pm.PreinstallApp .recorded ( v2,v1 );
/* if-nez v5, :cond_1 */
/* .line 719 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Opera PreInstall install apkFileProduct: "; // const-string v6, "Opera PreInstall install apkFileProduct: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v6 = " hasInstallHistory apkFileProduct: "; // const-string v6, " hasInstallHistory apkFileProduct: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = com.android.server.pm.PreinstallApp .recorded ( v2,v1 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v5 );
/* .line 720 */
/* .line 724 */
} // .end local v0 # "region":Ljava/lang/String;
} // .end local v1 # "apkFileProduct":Ljava/io/File;
} // .end local v2 # "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
} // .end local v4 # "apkFile":Ljava/io/File;
} // :cond_1
} // :goto_0
return;
} // .end method
private static void parseAndDeleteDuplicatePreinstallApps ( ) {
/* .locals 12 */
/* .line 852 */
com.android.server.pm.PreinstallApp .getAllPreinstallApplist ( );
/* .line 853 */
/* .local v0, "preinstallAppFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* .line 854 */
/* .local v1, "currentTime":J */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Ljava/io/File; */
/* .line 855 */
/* .local v4, "pa":Ljava/io/File; */
com.android.server.pm.PreinstallApp .parsePackageLite ( v4 );
/* .line 856 */
/* .local v5, "pl":Landroid/content/pm/parsing/PackageLite; */
if ( v5 != null) { // if-eqz v5, :cond_3
(( android.content.pm.parsing.PackageLite ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* if-nez v6, :cond_0 */
/* .line 861 */
} // :cond_0
(( android.content.pm.parsing.PackageLite ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;
/* .line 862 */
/* .local v6, "packageName":Ljava/lang/String; */
/* new-instance v7, Lcom/android/server/pm/PreinstallApp$Item; */
int v8 = 0; // const/4 v8, 0x0
/* invoke-direct {v7, v6, v4, v5, v8}, Lcom/android/server/pm/PreinstallApp$Item;-><init>(Ljava/lang/String;Ljava/io/File;Landroid/content/pm/parsing/PackageLite;Landroid/content/pm/parsing/ApkLite;)V */
/* .line 863 */
/* .local v7, "newItem":Lcom/android/server/pm/PreinstallApp$Item; */
v9 = v8 = com.android.server.pm.PreinstallApp.sPreinstallApps;
/* if-nez v9, :cond_1 */
/* .line 864 */
/* .line 866 */
} // :cond_1
/* check-cast v9, Lcom/android/server/pm/PreinstallApp$Item; */
/* .line 867 */
/* .local v9, "oldItem":Lcom/android/server/pm/PreinstallApp$Item; */
v10 = com.android.server.pm.PreinstallApp$Item .betterThan ( v7,v9 );
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 868 */
/* .line 872 */
} // :cond_2
/* move-object v8, v7 */
/* .line 873 */
/* .local v8, "tmp":Lcom/android/server/pm/PreinstallApp$Item; */
/* move-object v7, v9 */
/* .line 874 */
/* move-object v9, v8 */
/* .line 881 */
} // .end local v8 # "tmp":Lcom/android/server/pm/PreinstallApp$Item;
} // :goto_1
v8 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( com.android.server.pm.PreinstallApp$Item ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " is better than "; // const-string v11, " is better than "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.server.pm.PreinstallApp$Item ) v9 ).toString ( ); // invoke-virtual {v9}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", ignore "; // const-string v11, ", ignore "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 882 */
(( com.android.server.pm.PreinstallApp$Item ) v9 ).toString ( ); // invoke-virtual {v9}, Lcom/android/server/pm/PreinstallApp$Item;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " !!!"; // const-string v11, " !!!"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 881 */
android.util.Slog .w ( v8,v10 );
/* .line 884 */
} // .end local v4 # "pa":Ljava/io/File;
} // .end local v5 # "pl":Landroid/content/pm/parsing/PackageLite;
} // .end local v6 # "packageName":Ljava/lang/String;
} // .end local v7 # "newItem":Lcom/android/server/pm/PreinstallApp$Item;
} // .end local v9 # "oldItem":Lcom/android/server/pm/PreinstallApp$Item;
} // :goto_2
/* .line 857 */
/* .restart local v4 # "pa":Ljava/io/File; */
/* .restart local v5 # "pl":Landroid/content/pm/parsing/PackageLite; */
} // :cond_3
} // :goto_3
v6 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Parse "; // const-string v8, "Parse "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " failed, skip"; // const-string v8, " failed, skip"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v7 );
/* .line 858 */
/* goto/16 :goto_0 */
/* .line 885 */
} // .end local v4 # "pa":Ljava/io/File;
} // .end local v5 # "pl":Landroid/content/pm/parsing/PackageLite;
} // :cond_4
v3 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Parse preinstall apps, consume "; // const-string v5, "Parse preinstall apps, consume "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
/* sub-long/2addr v5, v1 */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "ms"; // const-string v5, "ms"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 886 */
return;
} // .end method
static android.content.pm.parsing.ApkLite parsePackage ( java.io.File p0 ) {
/* .locals 5 */
/* .param p0, "apkFile" # Ljava/io/File; */
/* .line 1851 */
android.content.pm.parsing.result.ParseTypeImpl .forDefaultParsing ( );
/* .line 1852 */
/* .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl; */
/* nop */
/* .line 1853 */
(( android.content.pm.parsing.result.ParseTypeImpl ) v0 ).reset ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/result/ParseTypeImpl;->reset()Landroid/content/pm/parsing/result/ParseInput;
/* .line 1852 */
/* const/16 v2, 0x20 */
android.content.pm.parsing.ApkLiteParseUtils .parseApkLite ( v1,p0,v2 );
/* .line 1854 */
v2 = /* .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/ApkLite;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1855 */
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to parseApkLite: "; // const-string v4, "Failed to parseApkLite: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " error: "; // const-string v4, " error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1856 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1855 */
android.util.Slog .e ( v2,v3,v4 );
/* .line 1857 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1859 */
} // :cond_0
/* check-cast v2, Landroid/content/pm/parsing/ApkLite; */
} // .end method
static android.content.pm.parsing.PackageLite parsePackageLite ( java.io.File p0 ) {
/* .locals 5 */
/* .param p0, "apkFile" # Ljava/io/File; */
/* .line 1863 */
android.content.pm.parsing.result.ParseTypeImpl .forDefaultParsing ( );
/* .line 1864 */
/* .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl; */
/* nop */
/* .line 1865 */
(( android.content.pm.parsing.result.ParseTypeImpl ) v0 ).reset ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/result/ParseTypeImpl;->reset()Landroid/content/pm/parsing/result/ParseInput;
/* .line 1864 */
int v2 = 0; // const/4 v2, 0x0
android.content.pm.parsing.ApkLiteParseUtils .parsePackageLite ( v1,p0,v2 );
/* .line 1866 */
v2 = /* .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/PackageLite;>;" */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1867 */
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to parsePackageLite: "; // const-string v4, "Failed to parsePackageLite: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = " error: "; // const-string v4, " error: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1868 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1867 */
android.util.Slog .e ( v2,v3,v4 );
/* .line 1869 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1871 */
} // :cond_0
/* check-cast v2, Landroid/content/pm/parsing/PackageLite; */
} // .end method
private static void readAllowOnlyInstallThirdAppForCarrier ( ) {
/* .locals 9 */
/* .line 1149 */
final String v0 = "disallow_install_third_app.list"; // const-string v0, "disallow_install_third_app.list"
final String v1 = "allow_install_third_app.list"; // const-string v1, "allow_install_third_app.list"
final String v2 = "/product/opcust/"; // const-string v2, "/product/opcust/"
final String v3 = "persist.sys.cota.carrier"; // const-string v3, "persist.sys.cota.carrier"
final String v4 = ""; // const-string v4, ""
android.os.SystemProperties .get ( v3,v4 );
/* .line 1150 */
/* .local v3, "cotaCarrier":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_4 */
final String v4 = "XM"; // const-string v4, "XM"
v4 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_4 */
/* .line 1152 */
try { // :try_start_0
final String v4 = "ro.miui.region"; // const-string v4, "ro.miui.region"
final String v5 = "cn"; // const-string v5, "cn"
android.os.SystemProperties .get ( v4,v5 );
(( java.lang.String ) v4 ).toLowerCase ( ); // invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
/* .line 1153 */
/* .local v4, "region":Ljava/lang/String; */
/* new-instance v5, Ljava/io/File; */
/* new-instance v6, Ljava/io/File; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v6, v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v5, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 1155 */
/* .local v5, "customizedAppSetFile":Ljava/io/File; */
v6 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
final String v7 = "/product/opcust"; // const-string v7, "/product/opcust"
/* if-nez v6, :cond_0 */
/* .line 1156 */
try { // :try_start_1
/* new-instance v6, Ljava/io/File; */
/* new-instance v8, Ljava/io/File; */
/* invoke-direct {v8, v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v6, v8, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v5, v6 */
/* .line 1159 */
} // :cond_0
v1 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1160 */
v0 = com.android.server.pm.PreinstallApp.sAllowOnlyInstallThirdApps;
com.android.server.pm.PreinstallApp .readLineToSet ( v5,v0 );
/* .line 1161 */
return;
/* .line 1164 */
} // :cond_1
/* new-instance v1, Ljava/io/File; */
/* new-instance v6, Ljava/io/File; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v6, v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v1, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 1166 */
} // .end local v5 # "customizedAppSetFile":Ljava/io/File;
/* .local v1, "customizedAppSetFile":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v2, :cond_2 */
/* .line 1167 */
/* new-instance v2, Ljava/io/File; */
/* new-instance v5, Ljava/io/File; */
/* invoke-direct {v5, v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* invoke-direct {v2, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 1170 */
} // :cond_2
v0 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1171 */
v0 = com.android.server.pm.PreinstallApp.sDisallowInstallThirdApps;
com.android.server.pm.PreinstallApp .readLineToSet ( v1,v0 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1175 */
} // .end local v1 # "customizedAppSetFile":Ljava/io/File;
} // .end local v4 # "region":Ljava/lang/String;
} // :cond_3
/* .line 1173 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1174 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.pm.PreinstallApp.TAG;
final String v2 = "Error occurs when to read allow install third app list."; // const-string v2, "Error occurs when to read allow install third app list."
android.util.Slog .e ( v1,v2 );
/* .line 1177 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_4
} // :goto_0
return;
} // .end method
private static void readHistory ( java.lang.String p0, java.util.Map p1 ) {
/* .locals 17 */
/* .param p0, "filePath" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 265 */
/* .local p1, "preinstallHistoryMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
/* move-object/from16 v1, p1 */
final String v2 = "/"; // const-string v2, "/"
int v3 = 0; // const/4 v3, 0x0
/* .line 266 */
/* .local v3, "bufferReader":Ljava/io/BufferedReader; */
int v4 = 0; // const/4 v4, 0x0
/* .line 268 */
/* .local v4, "fileReader":Ljava/io/FileReader; */
try { // :try_start_0
/* new-instance v0, Ljava/io/File; */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move-object/from16 v5, p0 */
try { // :try_start_1
/* invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v6, v0 */
/* .line 270 */
/* .local v6, "installHistoryFile":Ljava/io/File; */
v0 = (( java.io.File ) v6 ).exists ( ); // invoke-virtual {v6}, Ljava/io/File;->exists()Z
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* if-nez v0, :cond_0 */
/* .line 340 */
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 341 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 271 */
return;
/* .line 274 */
} // :cond_0
try { // :try_start_2
/* new-instance v0, Ljava/io/FileReader; */
/* invoke-direct {v0, v6}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* move-object v4, v0 */
/* .line 275 */
/* new-instance v0, Ljava/io/BufferedReader; */
/* invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v3, v0 */
/* .line 276 */
int v0 = 0; // const/4 v0, 0x0
/* .line 277 */
/* .local v0, "line":Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
/* .line 279 */
/* .local v7, "possibleNewPath":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v3 ).readLine ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v9, v8 */
} // .end local v0 # "line":Ljava/lang/String;
/* .local v9, "line":Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 280 */
final String v0 = ":"; // const-string v0, ":"
(( java.lang.String ) v9 ).split ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* move-object v8, v0 */
/* .line 281 */
/* .local v8, "ss":[Ljava/lang/String; */
if ( v8 != null) { // if-eqz v8, :cond_6
/* array-length v0, v8 */
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
int v10 = 2; // const/4 v10, 0x2
/* if-eq v0, v10, :cond_1 */
/* .line 282 */
/* goto/16 :goto_3 */
/* .line 285 */
} // :cond_1
/* const-wide/16 v10, 0x0 */
/* .line 287 */
/* .local v10, "mtime":J */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_3
/* aget-object v12, v8, v0 */
java.lang.Long .valueOf ( v12 );
(( java.lang.Long ) v12 ).longValue ( ); // invoke-virtual {v12}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* :try_end_3 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* move-wide v10, v12 */
/* .line 290 */
/* nop */
/* .line 292 */
int v12 = 0; // const/4 v12, 0x0
try { // :try_start_4
/* aget-object v13, v8, v12 */
v14 = com.android.server.pm.PreinstallApp.S_SYSTEM_PREINSTALL_APP_DIR;
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v13 = (( java.lang.String ) v13 ).startsWith ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v13 != null) { // if-eqz v13, :cond_2
/* .line 293 */
/* aget-object v13, v8, v12 */
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v15 = com.android.server.pm.PreinstallApp.T_SYSTEM_PREINSTALL_APP_DIR;
/* .line 294 */
(( java.io.File ) v15 ).getPath ( ); // invoke-virtual {v15}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 293 */
(( java.lang.String ) v13 ).replace ( v14, v15 ); // invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v7, v13 */
/* .line 295 */
java.lang.Long .valueOf ( v10,v11 );
/* .line 296 */
} // :cond_2
/* aget-object v13, v8, v12 */
v14 = com.android.server.pm.PreinstallApp.S_DATA_PREINSTALL_APP_DIR;
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v13 = (( java.lang.String ) v13 ).startsWith ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v13 != null) { // if-eqz v13, :cond_3
/* .line 297 */
/* aget-object v13, v8, v12 */
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v15 = com.android.server.pm.PreinstallApp.T_SYSTEM_PREINSTALL_APP_DIR;
/* .line 298 */
(( java.io.File ) v15 ).getPath ( ); // invoke-virtual {v15}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 297 */
(( java.lang.String ) v13 ).replace ( v14, v15 ); // invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v7, v13 */
/* .line 299 */
java.lang.Long .valueOf ( v10,v11 );
/* .line 302 */
} // :cond_3
} // :goto_1
/* aget-object v13, v8, v12 */
v14 = com.android.server.pm.PreinstallApp.OLD_PREINSTALL_APP_DIR;
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v13 = (( java.lang.String ) v13 ).startsWith ( v15 ); // invoke-virtual {v13, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v13 != null) { // if-eqz v13, :cond_4
/* .line 306 */
/* aget-object v0, v8, v12 */
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v15 = com.android.server.pm.PreinstallApp.CUSTOMIZED_APP_DIR;
(( java.io.File ) v15 ).getPath ( ); // invoke-virtual {v15}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.String ) v0 ).replace ( v13, v15 ); // invoke-virtual {v0, v13, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* .line 307 */
} // .end local v7 # "possibleNewPath":Ljava/lang/String;
/* .local v0, "possibleNewPath":Ljava/lang/String; */
java.lang.Long .valueOf ( v10,v11 );
/* .line 308 */
/* aget-object v7, v8, v12 */
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v15 = com.android.server.pm.PreinstallApp.NONCUSTOMIZED_APP_DIR;
(( java.io.File ) v15 ).getPath ( ); // invoke-virtual {v15}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.String ) v7 ).replace ( v13, v15 ); // invoke-virtual {v7, v13, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v0, v7 */
/* .line 309 */
java.lang.Long .valueOf ( v10,v11 );
/* .line 310 */
/* aget-object v7, v8, v12 */
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v15 = com.android.server.pm.PreinstallApp.VENDOR_NONCUSTOMIZED_APP_DIR;
(( java.io.File ) v15 ).getPath ( ); // invoke-virtual {v15}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.String ) v7 ).replace ( v13, v15 ); // invoke-virtual {v7, v13, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v0, v7 */
/* .line 311 */
java.lang.Long .valueOf ( v10,v11 );
/* .line 312 */
/* aget-object v7, v8, v12 */
(( java.io.File ) v14 ).getPath ( ); // invoke-virtual {v14}, Ljava/io/File;->getPath()Ljava/lang/String;
v13 = com.android.server.pm.PreinstallApp.PRODUCT_NONCUSTOMIZED_APP_DIR;
(( java.io.File ) v13 ).getPath ( ); // invoke-virtual {v13}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.String ) v7 ).replace ( v12, v13 ); // invoke-virtual {v7, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v0, v7 */
/* .line 313 */
java.lang.Long .valueOf ( v10,v11 );
/* move-object v7, v0 */
/* .line 315 */
} // .end local v0 # "possibleNewPath":Ljava/lang/String;
/* .restart local v7 # "possibleNewPath":Ljava/lang/String; */
} // :cond_4
/* nop */
/* .line 317 */
/* aget-object v13, v8, v12 */
(( java.lang.String ) v13 ).split ( v2 ); // invoke-virtual {v13, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 318 */
/* .local v13, "paths":[Ljava/lang/String; */
if ( v13 != null) { // if-eqz v13, :cond_5
/* array-length v14, v13 */
int v15 = 7; // const/4 v15, 0x7
/* if-eq v14, v15, :cond_5 */
/* .line 319 */
/* array-length v14, v13 */
/* sub-int/2addr v14, v0 */
/* aget-object v0, v13, v14 */
/* .line 320 */
/* .local v0, "fileName":Ljava/lang/String; */
final String v14 = ".apk"; // const-string v14, ".apk"
final String v15 = ""; // const-string v15, ""
(( java.lang.String ) v0 ).replace ( v14, v15 ); // invoke-virtual {v0, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* .line 321 */
/* .local v14, "apkName":Ljava/lang/String; */
/* aget-object v15, v8, v12 */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v12 ).append ( v14 ); // invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v2 ); // invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v0 ); // invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) v15 ).replace ( v0, v12 ); // invoke-virtual {v15, v0, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
/* move-object v7, v12 */
/* .line 327 */
/* new-instance v12, Ljava/io/File; */
/* move-object/from16 v16, v0 */
int v15 = 0; // const/4 v15, 0x0
} // .end local v0 # "fileName":Ljava/lang/String;
/* .local v16, "fileName":Ljava/lang/String; */
/* aget-object v0, v8, v15 */
/* invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object v0, v12 */
/* .line 328 */
/* .local v0, "oldAppFile":Ljava/io/File; */
/* new-instance v12, Ljava/io/File; */
/* invoke-direct {v12, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 329 */
/* .local v12, "newAppFile":Ljava/io/File; */
v15 = (( java.io.File ) v12 ).exists ( ); // invoke-virtual {v12}, Ljava/io/File;->exists()Z
if ( v15 != null) { // if-eqz v15, :cond_5
/* .line 330 */
v15 = com.android.server.pm.PreinstallApp .isSamePackage ( v0,v12 );
if ( v15 != null) { // if-eqz v15, :cond_5
/* .line 331 */
int v15 = 0; // const/4 v15, 0x0
/* aput-object v7, v8, v15 */
/* .line 335 */
} // .end local v0 # "oldAppFile":Ljava/io/File;
} // .end local v12 # "newAppFile":Ljava/io/File;
} // .end local v13 # "paths":[Ljava/lang/String;
} // .end local v14 # "apkName":Ljava/lang/String;
} // .end local v16 # "fileName":Ljava/lang/String;
} // :cond_5
int v0 = 0; // const/4 v0, 0x0
/* aget-object v0, v8, v0 */
java.lang.Long .valueOf ( v10,v11 );
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 337 */
} // .end local v8 # "ss":[Ljava/lang/String;
} // .end local v10 # "mtime":J
} // :goto_2
/* move-object v0, v9 */
/* goto/16 :goto_0 */
/* .line 288 */
/* .restart local v8 # "ss":[Ljava/lang/String; */
/* .restart local v10 # "mtime":J */
/* :catch_0 */
/* move-exception v0 */
/* .line 289 */
/* .local v0, "e":Ljava/lang/NumberFormatException; */
/* nop */
/* .line 279 */
} // .end local v0 # "e":Ljava/lang/NumberFormatException;
} // .end local v8 # "ss":[Ljava/lang/String;
} // .end local v10 # "mtime":J
} // :cond_6
} // :goto_3
/* move-object v0, v9 */
/* goto/16 :goto_0 */
} // .end local v6 # "installHistoryFile":Ljava/io/File;
} // .end local v7 # "possibleNewPath":Ljava/lang/String;
} // .end local v9 # "line":Ljava/lang/String;
} // :cond_7
/* .line 340 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 338 */
/* :catch_1 */
/* move-exception v0 */
/* .line 340 */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v5, p0 */
} // :goto_4
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 341 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 342 */
/* throw v0 */
/* .line 338 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v5, p0 */
/* .line 340 */
} // :goto_5
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 341 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 342 */
/* nop */
/* .line 343 */
return;
} // .end method
private static void readHistory ( java.util.Map p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 346 */
/* .local p0, "preinstallHistoryMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
final String v0 = "/data/system/preinstall_history"; // const-string v0, "/data/system/preinstall_history"
com.android.server.pm.PreinstallApp .readHistory ( v0,p0 );
/* .line 347 */
final String v0 = "/data/app/preinstall_history"; // const-string v0, "/data/app/preinstall_history"
com.android.server.pm.PreinstallApp .readHistory ( v0,p0 );
/* .line 348 */
return;
} // .end method
private static void readLineToSet ( java.io.File p0, java.util.Set p1 ) {
/* .locals 7 */
/* .param p0, "file" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/io/File;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 593 */
/* .local p1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v0 = (( java.io.File ) p0 ).exists ( ); // invoke-virtual {p0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 594 */
int v0 = 0; // const/4 v0, 0x0
/* .line 596 */
/* .local v0, "buffer":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/InputStreamReader; */
/* new-instance v3, Ljava/io/FileInputStream; */
/* invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v0, v1 */
/* .line 599 */
(( java.io.File ) p0 ).getName ( ); // invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;
/* const-string/jumbo v2, "vanward_applist" */
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 600 */
android.app.ActivityManagerNative .getDefault ( );
/* .line 601 */
/* .local v1, "am":Landroid/app/IActivityManager; */
v2 = this.locale;
/* .line 602 */
/* .local v2, "curLocale":Ljava/util/Locale; */
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "line":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 603 */
(( java.lang.String ) v4 ).trim ( ); // invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
final String v5 = "\\s+"; // const-string v5, "\\s+"
(( java.lang.String ) v3 ).split ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 604 */
/* .local v3, "ss":[Ljava/lang/String; */
/* array-length v5, v3 */
int v6 = 2; // const/4 v6, 0x2
/* if-ne v5, v6, :cond_0 */
int v5 = 1; // const/4 v5, 0x1
/* aget-object v5, v3, v5 */
v5 = com.android.server.pm.PreinstallApp .isValidIme ( v5,v2 );
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 605 */
int v5 = 0; // const/4 v5, 0x0
/* aget-object v5, v3, v5 */
/* .line 607 */
} // .end local v3 # "ss":[Ljava/lang/String;
} // :cond_0
/* .line 608 */
} // .end local v1 # "am":Landroid/app/IActivityManager;
} // .end local v2 # "curLocale":Ljava/util/Locale;
} // :cond_1
/* .line 609 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_2
} // :goto_1
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v1 */
/* .local v2, "line":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 610 */
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 620 */
} // .end local v2 # "line":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v1 */
/* .line 617 */
/* :catch_0 */
/* move-exception v1 */
/* .line 618 */
/* .local v1, "e":Landroid/os/RemoteException; */
try { // :try_start_1
(( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
} // .end local v1 # "e":Landroid/os/RemoteException;
/* .line 615 */
/* :catch_1 */
/* move-exception v1 */
/* .line 616 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
} // .end local v1 # "e":Ljava/io/IOException;
/* .line 613 */
/* :catch_2 */
/* move-exception v1 */
/* .line 614 */
/* .local v1, "e":Ljava/io/FileNotFoundException; */
(( java.io.FileNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 620 */
} // .end local v1 # "e":Ljava/io/FileNotFoundException;
} // :cond_3
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 621 */
/* .line 620 */
} // :goto_3
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 621 */
/* throw v1 */
/* .line 623 */
} // .end local v0 # "buffer":Ljava/io/BufferedReader;
} // :cond_4
} // :goto_4
return;
} // .end method
private static void readNewPAITrackFileIfNeed ( ) {
/* .locals 6 */
/* .line 1810 */
v0 = com.android.server.pm.PreinstallApp.TAG;
final String v1 = "read new track file content from /data/miui/pai/pre_install.appsflyer"; // const-string v1, "read new track file content from /data/miui/pai/pre_install.appsflyer"
android.util.Slog .i ( v0,v1 );
/* .line 1811 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miui/pai/pre_install.appsflyer"; // const-string v1, "/data/miui/pai/pre_install.appsflyer"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1812 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1813 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1815 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v2 */
/* .line 1816 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1817 */
/* .local v2, "line":Ljava/lang/String; */
} // :cond_0
} // :goto_0
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1818 */
v3 = v3 = com.android.server.pm.PreinstallApp.mNewTrackContentList;
/* if-nez v3, :cond_0 */
/* .line 1819 */
v3 = com.android.server.pm.PreinstallApp.mNewTrackContentList;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1825 */
} // .end local v2 # "line":Ljava/lang/String;
} // :cond_1
/* nop */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 1826 */
/* .line 1825 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1822 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1823 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
v3 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error occurs while read new track file content on pkms start"; // const-string v5, "Error occurs while read new track file content on pkms start"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1825 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 1826 */
/* throw v2 */
/* .line 1828 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
} // :cond_2
} // :goto_3
return;
} // .end method
private static void readOperatorTrackFile ( ) {
/* .locals 5 */
/* .line 1760 */
v0 = com.android.server.pm.PreinstallApp.TAG;
final String v1 = "read Operator track file content from /mi_ext/product/etc/pre_install.appsflyer"; // const-string v1, "read Operator track file content from /mi_ext/product/etc/pre_install.appsflyer"
android.util.Slog .i ( v0,v1 );
/* .line 1762 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/mi_ext/product/etc/pre_install.appsflyer"; // const-string v1, "/mi_ext/product/etc/pre_install.appsflyer"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1763 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1764 */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
/* invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1765 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
int v2 = 0; // const/4 v2, 0x0
/* .line 1766 */
/* .local v2, "line":Ljava/lang/String; */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1767 */
v3 = com.android.server.pm.PreinstallApp.mTraditionalTrackContentList;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1769 */
} // .end local v2 # "line":Ljava/lang/String;
} // :cond_0
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1771 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
/* .line 1764 */
/* .restart local v1 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // :goto_1
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 1769 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
/* .restart local v0 # "file":Ljava/io/File; */
/* :catch_0 */
/* move-exception v1 */
/* .line 1770 */
/* .local v1, "e":Ljava/io/IOException; */
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurs while read Operator track file content"; // const-string v4, "Error occurs while read Operator track file content"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 1773 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_2
return;
} // .end method
private static void readPackagePAIList ( ) {
/* .locals 5 */
/* .line 1514 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1516 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
final String v3 = "/data/system/preinstallPAI.list"; // const-string v3, "/data/system/preinstallPAI.list"
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v0, v1 */
/* .line 1517 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1518 */
/* .local v1, "line":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1519 */
v2 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1524 */
} // .end local v1 # "line":Ljava/lang/String;
} // :cond_0
/* nop */
/* .line 1525 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1524 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 1521 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1522 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_1
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurs while read preinstalled PAI packages "; // const-string v4, "Error occurs while read preinstalled PAI packages "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1524 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1525 */
/* .line 1528 */
} // :cond_1
} // :goto_2
return;
/* .line 1524 */
} // :goto_3
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1525 */
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1527 */
} // :cond_2
/* throw v1 */
} // .end method
private static void readPackageVersionMap ( ) {
/* .locals 7 */
/* .line 1483 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1485 */
/* .local v0, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
final String v3 = "/data/system/preinstall.list"; // const-string v3, "/data/system/preinstall.list"
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v0, v1 */
/* .line 1486 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1487 */
/* .local v1, "line":Ljava/lang/String; */
} // :cond_0
} // :goto_0
(( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1488 */
final String v2 = ":"; // const-string v2, ":"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1489 */
/* .local v2, "ss":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* array-length v3, v2 */
int v4 = 2; // const/4 v4, 0x2
int v5 = 1; // const/4 v5, 0x1
/* if-eq v3, v5, :cond_1 */
/* array-length v3, v2 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-eq v3, v4, :cond_1 */
/* .line 1490 */
/* .line 1492 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .line 1494 */
/* .local v3, "version":I */
try { // :try_start_1
/* array-length v6, v2 */
/* if-ne v6, v4, :cond_2 */
/* .line 1495 */
/* aget-object v4, v2, v5 */
java.lang.Integer .valueOf ( v4 );
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* :try_end_1 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move v3, v4 */
/* .line 1499 */
} // :cond_2
/* nop */
/* .line 1501 */
try { // :try_start_2
v4 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
int v5 = 0; // const/4 v5, 0x0
/* aget-object v5, v2, v5 */
java.lang.Integer .valueOf ( v3 );
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1502 */
/* nop */
} // .end local v2 # "ss":[Ljava/lang/String;
} // .end local v3 # "version":I
/* .line 1497 */
/* .restart local v2 # "ss":[Ljava/lang/String; */
/* .restart local v3 # "version":I */
/* :catch_0 */
/* move-exception v4 */
/* .line 1498 */
/* .local v4, "e":Ljava/lang/NumberFormatException; */
/* .line 1506 */
} // .end local v1 # "line":Ljava/lang/String;
} // .end local v2 # "ss":[Ljava/lang/String;
} // .end local v3 # "version":I
} // .end local v4 # "e":Ljava/lang/NumberFormatException;
} // :cond_3
/* nop */
/* .line 1507 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1506 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 1503 */
/* :catch_1 */
/* move-exception v1 */
/* .line 1504 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_3
v2 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurs while read preinstalled packages "; // const-string v4, "Error occurs while read preinstalled packages "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1506 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1507 */
/* .line 1510 */
} // :cond_4
} // :goto_2
return;
/* .line 1506 */
} // :goto_3
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1507 */
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1509 */
} // :cond_5
/* throw v1 */
} // .end method
private static void readTraditionalPAITrackFile ( ) {
/* .locals 6 */
/* .line 1739 */
v0 = com.android.server.pm.PreinstallApp.TAG;
final String v1 = "read traditional track file content from /cust/etc/pre_install.appsflyer"; // const-string v1, "read traditional track file content from /cust/etc/pre_install.appsflyer"
android.util.Slog .i ( v0,v1 );
/* .line 1740 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/cust/etc/pre_install.appsflyer"; // const-string v1, "/cust/etc/pre_install.appsflyer"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1741 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1742 */
v1 = com.android.server.pm.PreinstallApp.mTraditionalTrackContentList;
/* .line 1743 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1745 */
/* .local v1, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v2 */
/* .line 1746 */
int v2 = 0; // const/4 v2, 0x0
/* .line 1747 */
/* .local v2, "line":Ljava/lang/String; */
} // :goto_0
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v2, v3 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1748 */
v3 = com.android.server.pm.PreinstallApp.mTraditionalTrackContentList;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1753 */
} // .end local v2 # "line":Ljava/lang/String;
} // :cond_0
/* nop */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 1754 */
/* .line 1753 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1750 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1751 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
v3 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error occurs while read traditional track file content"; // const-string v5, "Error occurs while read traditional track file content"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1753 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 1754 */
/* throw v2 */
/* .line 1756 */
} // .end local v1 # "reader":Ljava/io/BufferedReader;
} // :cond_1
} // :goto_3
com.android.server.pm.PreinstallApp .readOperatorTrackFile ( );
/* .line 1757 */
return;
} // .end method
public static void recordAdvanceAppsHistory ( com.android.server.pm.PreinstallApp$Item p0 ) {
/* .locals 4 */
/* .param p0, "item" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .line 1878 */
v0 = this.apkFile;
(( java.io.File ) v0 ).getPath ( ); // invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 1879 */
/* .local v0, "path":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
return;
/* .line 1882 */
} // :cond_0
final String v1 = "miuiAdvancePreload"; // const-string v1, "miuiAdvancePreload"
v1 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v1, :cond_1 */
final String v1 = "miuiAdvertisement"; // const-string v1, "miuiAdvertisement"
v1 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1883 */
} // :cond_1
v1 = com.android.server.pm.PreinstallApp.sAdvanceApps;
v2 = this.packageName;
v3 = this.apkFile;
(( java.io.File ) v3 ).getPath ( ); // invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 1885 */
} // :cond_2
return;
} // .end method
private static void recordHistory ( java.util.Map p0, com.android.server.pm.PreinstallApp$Item p1 ) {
/* .locals 2 */
/* .param p1, "item" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;", */
/* "Lcom/android/server/pm/PreinstallApp$Item;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 390 */
/* .local p0, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
/* iget v0, p1, Lcom/android/server/pm/PreinstallApp$Item;->type:I */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v0, v1, :cond_0 */
/* .line 391 */
/* new-instance v0, Ljava/io/File; */
v1 = this.pkgLite;
(( android.content.pm.parsing.PackageLite ) v1 ).getBaseApkPath ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getBaseApkPath()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 392 */
/* .local v0, "baseApk":Ljava/io/File; */
com.android.server.pm.PreinstallApp .recordToHistory ( p0,v0 );
/* .line 393 */
} // .end local v0 # "baseApk":Ljava/io/File;
/* .line 394 */
} // :cond_0
v0 = this.apkFile;
com.android.server.pm.PreinstallApp .recordToHistory ( p0,v0 );
/* .line 396 */
} // :goto_0
return;
} // .end method
private static void recordToHistory ( java.util.Map p0, java.io.File p1 ) {
/* .locals 3 */
/* .param p1, "apkFile" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;", */
/* "Ljava/io/File;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 386 */
/* .local p0, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
(( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.io.File ) p1 ).lastModified ( ); // invoke-virtual {p1}, Ljava/io/File;->lastModified()J
/* move-result-wide v1 */
java.lang.Long .valueOf ( v1,v2 );
/* .line 387 */
return;
} // .end method
private static Boolean recorded ( java.util.Map p0, com.android.server.pm.PreinstallApp$Item p1 ) {
/* .locals 1 */
/* .param p1, "item" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;", */
/* "Lcom/android/server/pm/PreinstallApp$Item;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 818 */
/* .local p0, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
v0 = this.apkFile;
v0 = com.android.server.pm.PreinstallApp .recorded ( p0,v0 );
} // .end method
private static Boolean recorded ( java.util.Map p0, java.io.File p1 ) {
/* .locals 1 */
/* .param p1, "apkFile" # Ljava/io/File; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;", */
/* "Ljava/io/File;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 814 */
/* .local p0, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
v0 = (( java.io.File ) p1 ).getPath ( ); // invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;
} // .end method
public static void removeFromPreinstallList ( java.lang.String p0 ) {
/* .locals 10 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 1560 */
v0 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* monitor-enter v0 */
/* .line 1561 */
try { // :try_start_0
v1 = v1 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* if-nez v1, :cond_0 */
/* .line 1562 */
/* monitor-exit v0 */
return;
/* .line 1564 */
} // :cond_0
v1 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* .line 1565 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 1568 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/preinstall.list.tmp"; // const-string v1, "/data/system/preinstall.list.tmp"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1569 */
/* .local v0, "tempFile":Ljava/io/File; */
/* new-instance v1, Lcom/android/internal/util/JournaledFile; */
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/system/preinstall.list"; // const-string v3, "/data/system/preinstall.list"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2, v0}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V */
/* .line 1570 */
/* .local v1, "journal":Lcom/android/internal/util/JournaledFile; */
(( com.android.internal.util.JournaledFile ) v1 ).chooseForWrite ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;
/* .line 1571 */
/* .local v2, "writeTarget":Ljava/io/File; */
int v3 = 0; // const/4 v3, 0x0
/* .line 1572 */
/* .local v3, "fstr":Ljava/io/FileOutputStream; */
int v4 = 0; // const/4 v4, 0x0
/* .line 1574 */
/* .local v4, "str":Ljava/io/BufferedOutputStream; */
try { // :try_start_1
/* new-instance v5, Ljava/io/FileOutputStream; */
/* invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v3, v5 */
/* .line 1575 */
/* new-instance v5, Ljava/io/BufferedOutputStream; */
/* invoke-direct {v5, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* move-object v4, v5 */
/* .line 1576 */
(( java.io.FileOutputStream ) v3 ).getFD ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;
/* const/16 v6, 0x3e8 */
/* const/16 v7, 0x408 */
/* const/16 v8, 0x1a0 */
android.os.FileUtils .setPermissions ( v5,v8,v6,v7 );
/* .line 1578 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1579 */
/* .local v5, "sb":Ljava/lang/StringBuilder; */
v6 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* monitor-enter v6 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 1580 */
try { // :try_start_2
v7 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
v8 = } // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_1
/* check-cast v8, Ljava/util/Map$Entry; */
/* .line 1581 */
/* .local v8, "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
int v9 = 0; // const/4 v9, 0x0
(( java.lang.StringBuilder ) v5 ).setLength ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->setLength(I)V
/* .line 1582 */
/* check-cast v9, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1583 */
final String v9 = ":"; // const-string v9, ":"
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1584 */
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 1585 */
final String v9 = "\n"; // const-string v9, "\n"
(( java.lang.StringBuilder ) v5 ).append ( v9 ); // invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1586 */
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) v9 ).getBytes ( ); // invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B
(( java.io.BufferedOutputStream ) v4 ).write ( v9 ); // invoke-virtual {v4, v9}, Ljava/io/BufferedOutputStream;->write([B)V
/* .line 1587 */
} // .end local v8 # "map":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
/* .line 1588 */
} // :cond_1
/* monitor-exit v6 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1590 */
try { // :try_start_3
(( java.io.BufferedOutputStream ) v4 ).flush ( ); // invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V
/* .line 1591 */
android.os.FileUtils .sync ( v3 );
/* .line 1592 */
(( com.android.internal.util.JournaledFile ) v1 ).commit ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->commit()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
/* .line 1588 */
/* .restart local v5 # "sb":Ljava/lang/StringBuilder; */
/* :catchall_0 */
/* move-exception v7 */
try { // :try_start_4
/* monitor-exit v6 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
} // .end local v0 # "tempFile":Ljava/io/File;
} // .end local v1 # "journal":Lcom/android/internal/util/JournaledFile;
} // .end local v2 # "writeTarget":Ljava/io/File;
} // .end local v3 # "fstr":Ljava/io/FileOutputStream;
} // .end local v4 # "str":Ljava/io/BufferedOutputStream;
} // .end local p0 # "pkg":Ljava/lang/String;
try { // :try_start_5
/* throw v7 */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_0 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 1597 */
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
/* .restart local v0 # "tempFile":Ljava/io/File; */
/* .restart local v1 # "journal":Lcom/android/internal/util/JournaledFile; */
/* .restart local v2 # "writeTarget":Ljava/io/File; */
/* .restart local v3 # "fstr":Ljava/io/FileOutputStream; */
/* .restart local v4 # "str":Ljava/io/BufferedOutputStream; */
/* .restart local p0 # "pkg":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v5 */
/* .line 1593 */
/* :catch_0 */
/* move-exception v5 */
/* .line 1594 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_6
v6 = com.android.server.pm.PreinstallApp.TAG;
final String v7 = "Failed to write preinstall.list + "; // const-string v7, "Failed to write preinstall.list + "
android.util.Slog .wtf ( v6,v7,v5 );
/* .line 1595 */
(( com.android.internal.util.JournaledFile ) v1 ).rollback ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->rollback()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 1597 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 1598 */
/* nop */
/* .line 1600 */
v5 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Delete package:"; // const-string v7, "Delete package:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p0 ); // invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " from preinstall.list"; // const-string v7, " from preinstall.list"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1601 */
return;
/* .line 1597 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 1598 */
/* throw v5 */
/* .line 1565 */
} // .end local v0 # "tempFile":Ljava/io/File;
} // .end local v1 # "journal":Lcom/android/internal/util/JournaledFile;
} // .end local v2 # "writeTarget":Ljava/io/File;
} // .end local v3 # "fstr":Ljava/io/FileOutputStream;
} // .end local v4 # "str":Ljava/io/BufferedOutputStream;
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_7
/* monitor-exit v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_2 */
/* throw v1 */
} // .end method
public static void removeFromPreinstallPAIList ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 1605 */
v0 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* monitor-enter v0 */
/* .line 1606 */
try { // :try_start_0
v1 = v1 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* if-nez v1, :cond_0 */
/* .line 1607 */
/* monitor-exit v0 */
return;
/* .line 1609 */
} // :cond_0
v1 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* .line 1610 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 1613 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/preinstallPAI.list.tmp"; // const-string v1, "/data/system/preinstallPAI.list.tmp"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1614 */
/* .local v0, "tempFile":Ljava/io/File; */
/* new-instance v1, Lcom/android/internal/util/JournaledFile; */
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/system/preinstallPAI.list"; // const-string v3, "/data/system/preinstallPAI.list"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2, v0}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V */
/* .line 1615 */
/* .local v1, "journal":Lcom/android/internal/util/JournaledFile; */
(( com.android.internal.util.JournaledFile ) v1 ).chooseForWrite ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;
/* .line 1616 */
/* .local v2, "writeTarget":Ljava/io/File; */
int v3 = 0; // const/4 v3, 0x0
/* .line 1617 */
/* .local v3, "fstr":Ljava/io/FileOutputStream; */
int v4 = 0; // const/4 v4, 0x0
/* .line 1619 */
/* .local v4, "str":Ljava/io/BufferedOutputStream; */
try { // :try_start_1
/* new-instance v5, Ljava/io/FileOutputStream; */
/* invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v3, v5 */
/* .line 1620 */
/* new-instance v5, Ljava/io/BufferedOutputStream; */
/* invoke-direct {v5, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* move-object v4, v5 */
/* .line 1621 */
(( java.io.FileOutputStream ) v3 ).getFD ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;
/* const/16 v6, 0x3e8 */
/* const/16 v7, 0x408 */
/* const/16 v8, 0x1a0 */
android.os.FileUtils .setPermissions ( v5,v8,v6,v7 );
/* .line 1623 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1624 */
/* .local v5, "sb":Ljava/lang/StringBuilder; */
v6 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* monitor-enter v6 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 1625 */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_0
try { // :try_start_2
v8 = v8 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* if-ge v7, v8, :cond_1 */
/* .line 1626 */
int v8 = 0; // const/4 v8, 0x0
(( java.lang.StringBuilder ) v5 ).setLength ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->setLength(I)V
/* .line 1627 */
v8 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* check-cast v8, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1628 */
final String v8 = "\n"; // const-string v8, "\n"
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1629 */
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.String ) v8 ).getBytes ( ); // invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B
(( java.io.BufferedOutputStream ) v4 ).write ( v8 ); // invoke-virtual {v4, v8}, Ljava/io/BufferedOutputStream;->write([B)V
/* .line 1625 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 1631 */
} // .end local v7 # "i":I
} // :cond_1
/* monitor-exit v6 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1633 */
try { // :try_start_3
(( java.io.BufferedOutputStream ) v4 ).flush ( ); // invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V
/* .line 1634 */
android.os.FileUtils .sync ( v3 );
/* .line 1635 */
(( com.android.internal.util.JournaledFile ) v1 ).commit ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->commit()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
/* .line 1631 */
/* .restart local v5 # "sb":Ljava/lang/StringBuilder; */
/* :catchall_0 */
/* move-exception v7 */
try { // :try_start_4
/* monitor-exit v6 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
} // .end local v0 # "tempFile":Ljava/io/File;
} // .end local v1 # "journal":Lcom/android/internal/util/JournaledFile;
} // .end local v2 # "writeTarget":Ljava/io/File;
} // .end local v3 # "fstr":Ljava/io/FileOutputStream;
} // .end local v4 # "str":Ljava/io/BufferedOutputStream;
} // .end local p0 # "pkg":Ljava/lang/String;
try { // :try_start_5
/* throw v7 */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_0 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* .line 1640 */
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
/* .restart local v0 # "tempFile":Ljava/io/File; */
/* .restart local v1 # "journal":Lcom/android/internal/util/JournaledFile; */
/* .restart local v2 # "writeTarget":Ljava/io/File; */
/* .restart local v3 # "fstr":Ljava/io/FileOutputStream; */
/* .restart local v4 # "str":Ljava/io/BufferedOutputStream; */
/* .restart local p0 # "pkg":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v5 */
/* .line 1636 */
/* :catch_0 */
/* move-exception v5 */
/* .line 1637 */
/* .local v5, "e":Ljava/lang/Exception; */
try { // :try_start_6
v6 = com.android.server.pm.PreinstallApp.TAG;
final String v7 = "Failed to delete preinstallPAI.list + "; // const-string v7, "Failed to delete preinstallPAI.list + "
android.util.Slog .wtf ( v6,v7,v5 );
/* .line 1638 */
(( com.android.internal.util.JournaledFile ) v1 ).rollback ( ); // invoke-virtual {v1}, Lcom/android/internal/util/JournaledFile;->rollback()V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* .line 1640 */
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 1641 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 1642 */
/* nop */
/* .line 1644 */
v5 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Delete package:"; // const-string v7, "Delete package:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p0 ); // invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " from preinstallPAI.list"; // const-string v7, " from preinstallPAI.list"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1645 */
return;
/* .line 1640 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v3 );
/* .line 1641 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 1642 */
/* throw v5 */
/* .line 1610 */
} // .end local v0 # "tempFile":Ljava/io/File;
} // .end local v1 # "journal":Lcom/android/internal/util/JournaledFile;
} // .end local v2 # "writeTarget":Ljava/io/File;
} // .end local v3 # "fstr":Ljava/io/FileOutputStream;
} // .end local v4 # "str":Ljava/io/BufferedOutputStream;
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_7
/* monitor-exit v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_2 */
/* throw v1 */
} // .end method
private static void removePackageLI ( com.android.server.pm.PackageManagerService p0, java.lang.String p1, Boolean p2 ) {
/* .locals 1 */
/* .param p0, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "chatty" # Z */
/* .line 952 */
/* new-instance v0, Lcom/android/server/pm/RemovePackageHelper; */
/* invoke-direct {v0, p0}, Lcom/android/server/pm/RemovePackageHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V */
/* .line 953 */
/* .local v0, "removePackageHelper":Lcom/android/server/pm/RemovePackageHelper; */
(( com.android.server.pm.RemovePackageHelper ) v0 ).removePackageLI ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/RemovePackageHelper;->removePackageLI(Ljava/lang/String;Z)V
/* .line 954 */
return;
} // .end method
private static void restoreconPreinstallDir ( ) {
/* .locals 3 */
/* .line 1831 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miui/pai/"; // const-string v1, "/data/miui/pai/"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1832 */
/* .local v0, "file":Ljava/io/File; */
android.os.SELinux .getFileContext ( v1 );
/* .line 1833 */
/* .local v1, "fileContext":Ljava/lang/String; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-string/jumbo v2, "u:object_r:miui_pai_file:s0" */
v2 = android.text.TextUtils .equals ( v1,v2 );
/* if-nez v2, :cond_0 */
/* .line 1834 */
android.os.SELinux .restoreconRecursive ( v0 );
/* .line 1836 */
} // :cond_0
return;
} // .end method
private static Boolean signCheck ( java.io.File p0 ) {
/* .locals 6 */
/* .param p0, "apkFile" # Ljava/io/File; */
/* .line 835 */
/* sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 836 */
/* .line 838 */
} // :cond_0
v0 = miui.util.PreinstallAppUtils .supportSignVerifyInCust ( );
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
(( java.io.File ) p0 ).getPath ( ); // invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v3 = "/cust/app"; // const-string v3, "/cust/app"
v0 = (( java.lang.String ) v0 ).contains ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v0, v2 */
} // :cond_1
/* move v0, v1 */
/* .line 839 */
/* .local v0, "custAppSupportSign":Z */
} // :goto_0
v3 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Sign support is "; // const-string v5, "Sign support is "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 840 */
if ( v0 != null) { // if-eqz v0, :cond_2
miui.os.CustVerifier .getInstance ( );
/* if-nez v4, :cond_2 */
/* .line 841 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "CustVerifier init error !"; // const-string v4, "CustVerifier init error !"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p0 ).getPath ( ); // invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " will not be installed."; // const-string v4, " will not be installed."
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v1 );
/* .line 842 */
/* .line 844 */
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_3
miui.os.CustVerifier .getInstance ( );
(( java.io.File ) p0 ).getPath ( ); // invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;
v4 = (( miui.os.CustVerifier ) v4 ).verifyApkSignatue ( v5, v1 ); // invoke-virtual {v4, v5, v1}, Lmiui/os/CustVerifier;->verifyApkSignatue(Ljava/lang/String;I)Z
/* if-nez v4, :cond_3 */
/* .line 845 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.io.File ) p0 ).getPath ( ); // invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " verify failed!"; // const-string v4, " verify failed!"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v1 );
/* .line 846 */
/* .line 848 */
} // :cond_3
} // .end method
private static Boolean skipInstallByCota ( com.android.server.pm.PreinstallApp$Item p0 ) {
/* .locals 4 */
/* .param p0, "item" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .line 973 */
int v0 = 0; // const/4 v0, 0x0
if ( p0 != null) { // if-eqz p0, :cond_3
v1 = this.apkFile;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 974 */
v1 = this.apkFile;
(( java.io.File ) v1 ).getPath ( ); // invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 975 */
/* .local v1, "apkFilePath":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_3
final String v2 = "/system/data-app"; // const-string v2, "/system/data-app"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_3 */
/* .line 976 */
final String v2 = "/vendor/data-app"; // const-string v2, "/vendor/data-app"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_3 */
/* .line 977 */
final String v2 = "/product/data-app"; // const-string v2, "/product/data-app"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_3 */
/* .line 978 */
v3 = v2 = com.android.server.pm.PreinstallApp.sAllowOnlyInstallThirdApps;
/* if-lez v3, :cond_0 */
v3 = this.packageName;
v2 = /* .line 979 */
if ( v2 != null) { // if-eqz v2, :cond_1
} // :cond_0
v2 = com.android.server.pm.PreinstallApp.sDisallowInstallThirdApps;
v3 = /* .line 980 */
/* if-lez v3, :cond_2 */
v3 = this.packageName;
v2 = /* .line 981 */
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
/* nop */
/* .line 978 */
} // :goto_0
/* .line 984 */
} // .end local v1 # "apkFilePath":Ljava/lang/String;
} // :cond_3
} // .end method
private static Boolean skipOTA ( com.android.server.pm.PreinstallApp$Item p0 ) {
/* .locals 7 */
/* .param p0, "item" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .line 1532 */
try { // :try_start_0
final String v0 = "android.os.SystemProperties"; // const-string v0, "android.os.SystemProperties"
java.lang.Class .forName ( v0 );
final String v1 = "get"; // const-string v1, "get"
int v2 = 2; // const/4 v2, 0x2
/* new-array v3, v2, [Ljava/lang/Class; */
/* const-class v4, Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v3, v5 */
/* const-class v4, Ljava/lang/String; */
int v6 = 1; // const/4 v6, 0x1
/* aput-object v4, v3, v6 */
/* .line 1533 */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* new-array v1, v2, [Ljava/lang/Object; */
final String v2 = "ro.product.name"; // const-string v2, "ro.product.name"
/* aput-object v2, v1, v5 */
final String v2 = ""; // const-string v2, ""
/* aput-object v2, v1, v6 */
/* .line 1534 */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.reflect.Method ) v0 ).invoke ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
/* .line 1535 */
/* .local v0, "deviceName":Ljava/lang/String; */
final String v1 = "sagit"; // const-string v1, "sagit"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
final String v1 = "com.xiaomi.youpin"; // const-string v1, "com.xiaomi.youpin"
v2 = this.packageName;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1536 */
v1 = com.android.server.pm.PreinstallApp.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "skipOTA, deviceName is " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1537 */
/* .line 1541 */
} // .end local v0 # "deviceName":Ljava/lang/String;
} // :cond_0
/* .line 1539 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1540 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = com.android.server.pm.PreinstallApp.TAG;
final String v2 = "Get exception"; // const-string v2, "Get exception"
android.util.Slog .e ( v1,v2,v0 );
/* .line 1542 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
v0 = com.android.server.pm.PreinstallApp.NOT_OTA_PACKAGE_NAMES;
v0 = v1 = this.packageName;
} // .end method
private static Boolean skipYouPinIfHadPreinstall ( java.util.Map p0, com.android.server.pm.PreinstallApp$Item p1 ) {
/* .locals 8 */
/* .param p1, "item" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;", */
/* "Lcom/android/server/pm/PreinstallApp$Item;", */
/* ")Z" */
/* } */
} // .end annotation
/* .line 958 */
/* .local p0, "history":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
final String v0 = "/system/data-app/com.xiaomi.youpin/com.xiaomi.youpin.apk"; // const-string v0, "/system/data-app/com.xiaomi.youpin/com.xiaomi.youpin.apk"
/* .line 959 */
/* .local v0, "oldSystemPath":Ljava/lang/String; */
final String v1 = "/cust/app/customized/recommended-3rd-com.xiaomi.youpin/recommended-3rd-com.xiaomi.youpin.apk"; // const-string v1, "/cust/app/customized/recommended-3rd-com.xiaomi.youpin/recommended-3rd-com.xiaomi.youpin.apk"
/* .line 960 */
/* .local v1, "oldCustPath":Ljava/lang/String; */
final String v2 = "/system/data-app/Youpin/Youpin.apk"; // const-string v2, "/system/data-app/Youpin/Youpin.apk"
/* .line 961 */
/* .local v2, "newSystemPath":Ljava/lang/String; */
final String v3 = "/vendor/data-app/Youpin/Youpin.apk"; // const-string v3, "/vendor/data-app/Youpin/Youpin.apk"
/* .line 964 */
/* .local v3, "newVendorPath":Ljava/lang/String; */
v4 = this.apkFile;
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v5 = "/system/data-app/Youpin/Youpin.apk"; // const-string v5, "/system/data-app/Youpin/Youpin.apk"
v4 = (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v5 = 0; // const/4 v5, 0x0
int v6 = 1; // const/4 v6, 0x1
/* if-nez v4, :cond_1 */
v4 = this.apkFile;
/* .line 965 */
(( java.io.File ) v4 ).getPath ( ); // invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;
final String v7 = "/vendor/data-app/Youpin/Youpin.apk"; // const-string v7, "/vendor/data-app/Youpin/Youpin.apk"
v4 = (( java.lang.String ) v7 ).equals ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
} // :cond_0
/* move v4, v5 */
} // :cond_1
} // :goto_0
/* move v4, v6 */
/* .line 966 */
/* .local v4, "isNewYoupinPreinstall":Z */
} // :goto_1
v7 = final String v7 = "/system/data-app/com.xiaomi.youpin/com.xiaomi.youpin.apk"; // const-string v7, "/system/data-app/com.xiaomi.youpin/com.xiaomi.youpin.apk"
/* if-nez v7, :cond_3 */
v7 = final String v7 = "/cust/app/customized/recommended-3rd-com.xiaomi.youpin/recommended-3rd-com.xiaomi.youpin.apk"; // const-string v7, "/cust/app/customized/recommended-3rd-com.xiaomi.youpin/recommended-3rd-com.xiaomi.youpin.apk"
if ( v7 != null) { // if-eqz v7, :cond_2
} // :cond_2
/* move v7, v5 */
} // :cond_3
} // :goto_2
/* move v7, v6 */
/* .line 968 */
/* .local v7, "hadPreInstallYouPin":Z */
} // :goto_3
if ( v4 != null) { // if-eqz v4, :cond_4
if ( v7 != null) { // if-eqz v7, :cond_4
/* move v5, v6 */
} // :cond_4
} // .end method
private static Boolean systemAppDeletedOrDisabled ( com.android.server.pm.PackageManagerService p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p0, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 798 */
v0 = this.mPackages;
v0 = (( com.android.server.utils.WatchedArrayMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/utils/WatchedArrayMap;->containsKey(Ljava/lang/Object;)Z
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
private static Boolean underData ( com.android.server.pm.PackageSetting p0 ) {
/* .locals 2 */
/* .param p0, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .line 426 */
(( com.android.server.pm.PackageSetting ) p0 ).getPathString ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageSetting;->getPathString()Ljava/lang/String;
final String v1 = "/data/app/"; // const-string v1, "/data/app/"
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
} // .end method
private static void writeHistory ( java.lang.String p0, java.util.Map p1 ) {
/* .locals 7 */
/* .param p0, "filePath" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 351 */
/* .local p1, "preinstallHistoryMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 352 */
/* .local v0, "fileWriter":Ljava/io/FileWriter; */
int v1 = 0; // const/4 v1, 0x0
/* .line 354 */
/* .local v1, "bufferWriter":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 356 */
/* .local v2, "installHistoryFile":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_0 */
/* .line 357 */
(( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
/* .line 360 */
} // :cond_0
/* new-instance v3, Ljava/io/FileWriter; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v3, v2, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* move-object v0, v3 */
/* .line 361 */
/* new-instance v3, Ljava/io/BufferedWriter; */
/* invoke-direct {v3, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v1, v3 */
/* .line 363 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 364 */
/* .local v4, "r":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;" */
/* new-instance v5, Ljava/io/File; */
/* check-cast v6, Ljava/lang/String; */
/* invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v5 = (( java.io.File ) v5 ).exists ( ); // invoke-virtual {v5}, Ljava/io/File;->exists()Z
/* if-nez v5, :cond_1 */
/* .line 365 */
/* .line 367 */
} // :cond_1
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ":"; // const-string v6, ":"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.BufferedWriter ) v1 ).write ( v5 ); // invoke-virtual {v1, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 368 */
final String v5 = "\n"; // const-string v5, "\n"
(( java.io.BufferedWriter ) v1 ).write ( v5 ); // invoke-virtual {v1, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 369 */
} // .end local v4 # "r":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
/* .line 363 */
} // .end local v2 # "installHistoryFile":Ljava/io/File;
} // :cond_2
/* .line 372 */
/* :catchall_0 */
/* move-exception v2 */
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 373 */
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 374 */
/* throw v2 */
/* .line 370 */
/* :catch_0 */
/* move-exception v2 */
/* .line 372 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 373 */
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 374 */
/* nop */
/* .line 375 */
return;
} // .end method
private static void writeHistory ( java.util.Map p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 378 */
/* .local p0, "preinstallHistoryMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;" */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/preinstall_history"; // const-string v1, "/data/system/preinstall_history"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 379 */
/* .local v0, "old":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 380 */
(( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* .line 382 */
} // :cond_0
final String v1 = "/data/app/preinstall_history"; // const-string v1, "/data/app/preinstall_history"
com.android.server.pm.PreinstallApp .writeHistory ( v1,p0 );
/* .line 383 */
return;
} // .end method
private static void writeNewPAITrackFile ( ) {
/* .locals 6 */
/* .line 1776 */
v0 = com.android.server.pm.PreinstallApp.TAG;
final String v1 = "Write old track file content to /data/miui/pai/pre_install.appsflyer"; // const-string v1, "Write old track file content to /data/miui/pai/pre_install.appsflyer"
android.util.Slog .i ( v0,v1 );
/* .line 1777 */
v1 = com.android.server.pm.PreinstallApp.mTraditionalTrackContentList;
v1 = if ( v1 != null) { // if-eqz v1, :cond_4
if ( v1 != null) { // if-eqz v1, :cond_0
/* goto/16 :goto_3 */
/* .line 1781 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1783 */
/* .local v0, "bufferWriter":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "/data/miui/pai/pre_install.appsflyer"; // const-string v2, "/data/miui/pai/pre_install.appsflyer"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1784 */
/* .local v1, "newFile":Ljava/io/File; */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
v2 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
int v3 = -1; // const/4 v3, -0x1
/* if-nez v2, :cond_1 */
/* .line 1785 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
(( java.io.File ) v2 ).mkdirs ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
/* .line 1786 */
(( java.io.File ) v1 ).getParentFile ( ); // invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;
/* const/16 v4, 0x1fd */
android.os.FileUtils .setPermissions ( v2,v4,v3,v3 );
/* .line 1788 */
} // :cond_1
/* new-instance v2, Ljava/io/BufferedWriter; */
/* new-instance v4, Ljava/io/FileWriter; */
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {v4, v1, v5}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* invoke-direct {v2, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v0, v2 */
/* .line 1789 */
/* const/16 v2, 0x1b4 */
android.os.FileUtils .setPermissions ( v1,v2,v3,v3 );
/* .line 1790 */
v2 = com.android.server.pm.PreinstallApp.mTraditionalTrackContentList;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1791 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
try { // :try_start_1
v4 = v4 = com.android.server.pm.PreinstallApp.mTraditionalTrackContentList;
/* if-ge v3, v4, :cond_3 */
/* .line 1792 */
v4 = com.android.server.pm.PreinstallApp.mTraditionalTrackContentList;
/* check-cast v4, Ljava/lang/String; */
/* .line 1793 */
/* .local v4, "content":Ljava/lang/String; */
v5 = v5 = com.android.server.pm.PreinstallApp.mNewTrackContentList;
/* if-nez v5, :cond_2 */
/* .line 1794 */
v5 = com.android.server.pm.PreinstallApp.mNewTrackContentList;
/* .line 1795 */
(( java.io.BufferedWriter ) v0 ).write ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1796 */
final String v5 = "\n"; // const-string v5, "\n"
(( java.io.BufferedWriter ) v0 ).write ( v5 ); // invoke-virtual {v0, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1791 */
} // .end local v4 # "content":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1799 */
} // .end local v3 # "i":I
} // :cond_3
/* monitor-exit v2 */
/* .line 1805 */
} // .end local v1 # "newFile":Ljava/io/File;
/* .line 1799 */
/* .restart local v1 # "newFile":Ljava/io/File; */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
try { // :try_start_2
/* throw v3 */
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 1805 */
} // .end local v1 # "newFile":Ljava/io/File;
/* .restart local v0 # "bufferWriter":Ljava/io/BufferedWriter; */
/* :catchall_1 */
/* move-exception v1 */
/* .line 1800 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1801 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_3
v2 = com.android.server.pm.PreinstallApp.TAG;
final String v3 = "Error occurs when to write track file from /cust/etc/pre_install.appsflyer to /data/miui/pai/pre_install.appsflyer"; // const-string v3, "Error occurs when to write track file from /cust/etc/pre_install.appsflyer to /data/miui/pai/pre_install.appsflyer"
android.util.Slog .e ( v2,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 1805 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1806 */
/* nop */
/* .line 1807 */
return;
/* .line 1805 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1806 */
/* throw v1 */
/* .line 1778 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
} // :cond_4
} // :goto_3
final String v1 = "no content write to new appsflyer file"; // const-string v1, "no content write to new appsflyer file"
android.util.Slog .e ( v0,v1 );
/* .line 1779 */
return;
} // .end method
private static void writePreInstallPackagePath ( java.util.Map p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1343 */
/* .local p0, "maps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;" */
v0 = com.android.server.pm.PreinstallApp.TAG;
final String v1 = "Write preinstalled package name into: /data/app/preinstall_package_path"; // const-string v1, "Write preinstalled package name into: /data/app/preinstall_package_path"
android.util.Slog .i ( v0,v1 );
/* .line 1345 */
v0 = if ( p0 != null) { // if-eqz p0, :cond_6
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_4 */
/* .line 1348 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1350 */
/* .local v0, "bufferWriter":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedWriter; */
/* new-instance v2, Ljava/io/FileWriter; */
final String v3 = "/data/app/preinstall_package_path"; // const-string v3, "/data/app/preinstall_package_path"
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {v2, v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v0, v1 */
/* .line 1352 */
} // :cond_1
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1353 */
/* .local v2, "r":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;" */
/* check-cast v3, Ljava/lang/String; */
/* .line 1354 */
/* .local v3, "packageName":Ljava/lang/String; */
/* check-cast v4, Ljava/lang/String; */
/* .line 1356 */
/* .local v4, "path":Ljava/lang/String; */
v5 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v5, :cond_1 */
v5 = android.text.TextUtils .isEmpty ( v4 );
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1357 */
/* .line 1359 */
} // :cond_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ":"; // const-string v6, ":"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.BufferedWriter ) v0 ).write ( v5 ); // invoke-virtual {v0, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1360 */
final String v5 = "\n"; // const-string v5, "\n"
(( java.io.BufferedWriter ) v0 ).write ( v5 ); // invoke-virtual {v0, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1361 */
} // .end local v2 # "r":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
} // .end local v3 # "packageName":Ljava/lang/String;
} // .end local v4 # "path":Ljava/lang/String;
/* .line 1365 */
} // :cond_3
/* nop */
/* .line 1366 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1365 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 1362 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1363 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_1
v2 = com.android.server.pm.PreinstallApp.TAG;
final String v3 = "Error occurs when to write preinstalled package path."; // const-string v3, "Error occurs when to write preinstalled package path."
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1365 */
/* nop */
} // .end local v1 # "e":Ljava/lang/Exception;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1366 */
/* .line 1369 */
} // :cond_4
} // :goto_2
return;
/* .line 1365 */
} // :goto_3
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1366 */
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1368 */
} // :cond_5
/* throw v1 */
/* .line 1346 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
} // :cond_6
} // :goto_4
return;
} // .end method
public static void writePreinstallPAIPackage ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p0, "pkg" # Ljava/lang/String; */
/* .line 1394 */
v0 = com.android.server.pm.PreinstallApp.TAG;
final String v1 = "Write PAI package name into /data/system/preinstallPAI.list"; // const-string v1, "Write PAI package name into /data/system/preinstallPAI.list"
android.util.Slog .i ( v0,v1 );
/* .line 1395 */
v0 = android.text.TextUtils .isEmpty ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1396 */
return;
/* .line 1398 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1400 */
/* .local v0, "bufferWriter":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedWriter; */
/* new-instance v2, Ljava/io/FileWriter; */
final String v3 = "/data/system/preinstallPAI.list"; // const-string v3, "/data/system/preinstallPAI.list"
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {v2, v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v0, v1 */
/* .line 1401 */
v1 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1402 */
try { // :try_start_1
v2 = com.android.server.pm.PreinstallApp.mPackagePAIList;
/* .line 1403 */
(( java.io.BufferedWriter ) v0 ).write ( p0 ); // invoke-virtual {v0, p0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1404 */
final String v2 = "\n"; // const-string v2, "\n"
(( java.io.BufferedWriter ) v0 ).write ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1405 */
/* monitor-exit v1 */
/* .line 1409 */
/* .line 1405 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
} // .end local p0 # "pkg":Ljava/lang/String;
try { // :try_start_2
/* throw v2 */
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 1409 */
/* .restart local v0 # "bufferWriter":Ljava/io/BufferedWriter; */
/* .restart local p0 # "pkg":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v1 */
/* .line 1406 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1407 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_3
v2 = com.android.server.pm.PreinstallApp.TAG;
final String v3 = "Error occurs when to write PAI package name."; // const-string v3, "Error occurs when to write PAI package name."
android.util.Slog .e ( v2,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 1409 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1410 */
/* nop */
/* .line 1411 */
return;
/* .line 1409 */
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1410 */
/* throw v1 */
} // .end method
private static void writePreinstallPackage ( java.util.Map p0 ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1372 */
/* .local p0, "maps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
v0 = com.android.server.pm.PreinstallApp.TAG;
final String v1 = "Write preinstalled package name into /data/system/preinstall.list"; // const-string v1, "Write preinstalled package name into /data/system/preinstall.list"
android.util.Slog .i ( v0,v1 );
/* .line 1373 */
v0 = if ( p0 != null) { // if-eqz p0, :cond_2
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_3 */
/* .line 1376 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1378 */
/* .local v0, "bufferWriter":Ljava/io/BufferedWriter; */
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedWriter; */
/* new-instance v2, Ljava/io/FileWriter; */
final String v3 = "/data/system/preinstall.list"; // const-string v3, "/data/system/preinstall.list"
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {v2, v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V */
/* move-object v0, v1 */
/* .line 1379 */
v1 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1380 */
try { // :try_start_1
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/util/Map$Entry; */
/* .line 1381 */
/* .local v3, "r":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
v4 = com.android.server.pm.PreinstallApp.mPackageVersionMap;
/* check-cast v5, Ljava/lang/String; */
/* check-cast v6, Ljava/lang/Integer; */
/* .line 1382 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ":"; // const-string v5, ":"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.BufferedWriter ) v0 ).write ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1383 */
final String v4 = "\n"; // const-string v4, "\n"
(( java.io.BufferedWriter ) v0 ).write ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
/* .line 1384 */
} // .end local v3 # "r":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
/* .line 1385 */
} // :cond_1
/* monitor-exit v1 */
/* .line 1389 */
/* .line 1385 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
} // .end local p0 # "maps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
try { // :try_start_2
/* throw v2 */
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 1389 */
/* .restart local v0 # "bufferWriter":Ljava/io/BufferedWriter; */
/* .restart local p0 # "maps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* :catchall_1 */
/* move-exception v1 */
/* .line 1386 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1387 */
/* .local v1, "e":Ljava/io/IOException; */
try { // :try_start_3
v2 = com.android.server.pm.PreinstallApp.TAG;
final String v3 = "Error occurs when to write preinstalled package name."; // const-string v3, "Error occurs when to write preinstalled package name."
android.util.Slog .e ( v2,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 1389 */
/* nop */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1390 */
/* nop */
/* .line 1391 */
return;
/* .line 1389 */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 1390 */
/* throw v1 */
/* .line 1374 */
} // .end local v0 # "bufferWriter":Ljava/io/BufferedWriter;
} // :cond_2
} // :goto_3
return;
} // .end method
