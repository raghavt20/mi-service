.class public interface abstract Lcom/android/server/pm/PackageEventRecorderInternal;
.super Ljava/lang/Object;
.source "PackageEventRecorderInternal.java"


# virtual methods
.method public abstract commitDeletedEvents(ILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract deleteAllEventRecords(I)Z
.end method

.method public abstract getPackageEventRecords(I)Landroid/os/Bundle;
.end method

.method public abstract getSourcePackage(Ljava/lang/String;IZ)Landroid/os/Bundle;
.end method

.method public abstract recordPackageActivate(Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract recordPackageFirstLaunch(ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
.end method

.method public abstract recordPackageRemove([ILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V
.end method

.method public abstract recordPackageUpdate([ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
.end method
