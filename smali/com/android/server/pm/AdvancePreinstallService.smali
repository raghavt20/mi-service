.class public Lcom/android/server/pm/AdvancePreinstallService;
.super Lcom/android/server/SystemService;
.source "AdvancePreinstallService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;,
        Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;
    }
.end annotation


# static fields
.field public static final ADVANCE_TAG:Ljava/lang/String; = "miuiAdvancePreload"

.field public static final ADVERT_TAG:Ljava/lang/String; = "miuiAdvertisement"

.field private static final AD_ONLINE_SERVICE_NAME:Ljava/lang/String; = "com.xiaomi.preload.services.AdvancePreInstallService"

.field private static final AD_ONLINE_SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.xiaomi.preload"

.field private static final ANDROID_VERSION:Ljava/lang/String; = "androidVersion"

.field private static final APP_TYPE:Ljava/lang/String; = "appType"

.field private static final CHANNEL:Ljava/lang/String; = "channel"

.field private static final CONF_ID:Ljava/lang/String; = "confId"

.field private static final DEBUG:Z = true

.field private static final DEFAULT_BIND_DELAY:I = 0x1f4

.field private static final DEFAULT_CONNECT_TIME_OUT:I = 0x7d0

.field private static final DEFAULT_READ_TIME_OUT:I = 0x7d0

.field private static final DEVICE:Ljava/lang/String; = "device"

.field private static final IMEI_ID:Ljava/lang/String; = "imId"

.field private static final IMEI_MD5:Ljava/lang/String; = "imeiMd5"

.field private static final IS_CN:Ljava/lang/String; = "isCn"

.field private static final KEY_IS_PREINSTALLED:Ljava/lang/String; = "isPreinstalled"

.field private static final KEY_MIUI_CHANNELPATH:Ljava/lang/String; = "miuiChannelPath"

.field private static final LANG:Ljava/lang/String; = "lang"

.field private static final MIUI_VERSION:Ljava/lang/String; = "miuiVersion"

.field private static final MODEL:Ljava/lang/String; = "model"

.field private static final NETWORK_TYPE:Ljava/lang/String; = "networkType"

.field private static final NONCE:Ljava/lang/String; = "nonceStr"

.field private static final OFFLINE_COUNT:Ljava/lang/String; = "offlineCount"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field private static final PREINSTALL_CONFIG:Ljava/lang/String; = "/cust/etc/cust_apps_config"

.field private static final REGION:Ljava/lang/String; = "region"

.field private static final REQUEST_TYPE:Ljava/lang/String; = "request_type"

.field private static final SALESE_CHANNEL:Ljava/lang/String; = "saleschannels"

.field private static final SERVER_ADDRESS:Ljava/lang/String; = "https://control.preload.xiaomi.com/preload_app_info/get?"

.field private static final SERVER_ADDRESS_GLOBAL:Ljava/lang/String; = "https://global.control.preload.xiaomi.com/preload_app_info/get?"

.field private static final SIGN:Ljava/lang/String; = "sign"

.field private static final SIM_DETECTION_ACTION:Ljava/lang/String; = "com.miui.action.SIM_DETECTION"

.field private static final SKU:Ljava/lang/String; = "sku"

.field private static final TAG:Ljava/lang/String; = "AdvancePreinstallService"

.field private static final TRACK_EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"


# instance fields
.field private mAdvanceApps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mConfigObj:Lorg/json/JSONObject;

.field private mHasReceived:Z

.field private mImeiMe5:Ljava/lang/String;

.field private mIsNetworkConnected:Z

.field private mIsWifiConnected:Z

.field private mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

.field private mNetworkTypeName:Ljava/lang/String;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mTrack:Lcom/android/server/pm/PreInstallServiceTrack;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHasReceived(Lcom/android/server/pm/AdvancePreinstallService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mHasReceived:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageManager(Lcom/android/server/pm/AdvancePreinstallService;)Landroid/content/pm/PackageManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmHasReceived(Lcom/android/server/pm/AdvancePreinstallService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/pm/AdvancePreinstallService;->mHasReceived:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetPreloadAppInfo(Lcom/android/server/pm/AdvancePreinstallService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getPreloadAppInfo()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleAdvancePreinstallApps(Lcom/android/server/pm/AdvancePreinstallService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAdvancePreinstallApps()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleAdvancePreinstallAppsDelay(Lcom/android/server/pm/AdvancePreinstallService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAdvancePreinstallAppsDelay()V

    return-void
.end method

.method static bridge synthetic -$$Nest$misNetworkConnected(Lcom/android/server/pm/AdvancePreinstallService;Landroid/content/Context;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/AdvancePreinstallService;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mtrackAdvancePreloadSuccess(Lcom/android/server/pm/AdvancePreinstallService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/AdvancePreinstallService;->trackAdvancePreloadSuccess(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtrackEvent(Lcom/android/server/pm/AdvancePreinstallService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$muninstallAdvancePreinstallApps(Lcom/android/server/pm/AdvancePreinstallService;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 145
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 128
    new-instance v0, Lcom/android/server/pm/AdvancePreinstallService$1;

    invoke-direct {v0, p0}, Lcom/android/server/pm/AdvancePreinstallService$1;-><init>(Lcom/android/server/pm/AdvancePreinstallService;)V

    iput-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 146
    return-void
.end method

.method private closeBufferedReader(Ljava/io/BufferedReader;)V
    .locals 1
    .param p1, "br"    # Ljava/io/BufferedReader;

    .line 838
    if-eqz p1, :cond_0

    .line 840
    :try_start_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 843
    goto :goto_0

    .line 841
    :catch_0
    move-exception v0

    .line 842
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 845
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-void
.end method

.method private doUninstallApps(Ljava/lang/String;IILjava/lang/String;)V
    .locals 13
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "confId"    # I
    .param p3, "offlineCount"    # I
    .param p4, "appType"    # Ljava/lang/String;

    .line 368
    move-object v7, p0

    move-object v8, p1

    const-string v9, " failed:"

    const-string v10, "AdvancePreinstallService"

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v7, Lcom/android/server/pm/AdvancePreinstallService;->mPackageManager:Landroid/content/pm/PackageManager;

    if-eqz v0, :cond_2

    .line 370
    const-string v2, "remove_from_list_begin"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 371
    iget-object v0, v7, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, v7, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    .line 373
    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->removeFromPreinstallList(Ljava/lang/String;)V

    goto :goto_0

    .line 375
    :cond_0
    invoke-static {p1}, Lcom/android/server/pm/PreinstallApp;->removeFromPreinstallList(Ljava/lang/String;)V

    .line 378
    :goto_0
    iget-object v0, v7, Lcom/android/server/pm/AdvancePreinstallService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p0, v0, p1}, Lcom/android/server/pm/AdvancePreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 379
    const-string v2, "package_not_exist"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 380
    return-void

    .line 383
    :cond_1
    const/4 v11, 0x2

    :try_start_0
    iget-object v0, v7, Lcom/android/server/pm/AdvancePreinstallService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v11, v1}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    goto :goto_1

    .line 384
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disable Package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    const-string v2, "disable_package_failed"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 389
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    :try_start_1
    const-string v2, "begin_uninstall"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 390
    iget-object v0, v7, Lcom/android/server/pm/AdvancePreinstallService;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance v12, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;

    move-object v1, v12

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/pm/AdvancePreinstallService$PackageDeleteObserver;-><init>(Lcom/android/server/pm/AdvancePreinstallService;Ljava/lang/String;IILjava/lang/String;)V

    invoke-virtual {v0, p1, v12, v11}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 394
    goto :goto_2

    .line 391
    :catch_1
    move-exception v0

    .line 392
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "uninstall Package "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const-string/jumbo v2, "uninstall_failed"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 396
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_2
    return-void
.end method

.method private findTrackingApk(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .line 399
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mConfigObj:Lorg/json/JSONObject;

    const-string v1, "AdvancePreinstallService"

    if-nez v0, :cond_0

    .line 400
    const-string v0, "/cust/etc/cust_apps_config"

    invoke-virtual {p0, v0}, Lcom/android/server/pm/AdvancePreinstallService;->getFileContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "config":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 403
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/pm/AdvancePreinstallService;->mConfigObj:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 406
    goto :goto_0

    .line 404
    :catch_0
    move-exception v2

    .line 405
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    .end local v0    # "config":Ljava/lang/String;
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mConfigObj:Lorg/json/JSONObject;

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    .line 412
    :try_start_1
    const-string v3, "data"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 413
    .local v0, "array":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-gtz v3, :cond_1

    .line 414
    return-object v2

    .line 416
    :cond_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 417
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v3, :cond_3

    .line 418
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 419
    .local v5, "obj":Lorg/json/JSONObject;
    if-eqz v5, :cond_2

    const-string v6, "packageName"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 420
    const-string/jumbo v6, "trackApkPackageName"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    .line 417
    .end local v5    # "obj":Lorg/json/JSONObject;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 425
    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v3    # "count":I
    .end local v4    # "i":I
    :cond_3
    goto :goto_2

    .line 423
    :catch_1
    move-exception v0

    .line 424
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_4
    :goto_2
    return-object v2
.end method

.method private getAddress()Ljava/lang/String;
    .locals 1

    .line 677
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 678
    const-string v0, "https://control.preload.xiaomi.com/preload_app_info/get?"

    return-object v0

    .line 680
    :cond_0
    const-string v0, "https://global.control.preload.xiaomi.com/preload_app_info/get?"

    return-object v0
.end method

.method private getAdvanceApps()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 163
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mAdvanceApps:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 164
    return-object v0

    .line 166
    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_1

    .line 167
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAdvanceAppsByFrame()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mAdvanceApps:Ljava/util/Map;

    .line 168
    return-object v0

    .line 170
    :cond_1
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getCustomizePreinstallAppList()Ljava/util/List;

    move-result-object v0

    .line 171
    .local v0, "custApps":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/pm/AdvancePreinstallService;->mAdvanceApps:Ljava/util/Map;

    .line 172
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 173
    .local v2, "file":Ljava/io/File;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 174
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "miuiAdvancePreload"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "miuiAdvertisement"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 175
    :cond_2
    invoke-direct {p0, v2}, Lcom/android/server/pm/AdvancePreinstallService;->parsePackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;

    move-result-object v3

    .line 176
    .local v3, "pl":Landroid/content/pm/parsing/PackageLite;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 177
    iget-object v4, p0, Lcom/android/server/pm/AdvancePreinstallService;->mAdvanceApps:Ljava/util/Map;

    invoke-virtual {v3}, Landroid/content/pm/parsing/PackageLite;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "pl":Landroid/content/pm/parsing/PackageLite;
    :cond_3
    goto :goto_0

    .line 181
    :cond_4
    iget-object v1, p0, Lcom/android/server/pm/AdvancePreinstallService;->mAdvanceApps:Ljava/util/Map;

    return-object v1
.end method

.method private getAdvanceAppsByFrame()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 222
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getAdvanceApps()Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 225
    :cond_0
    sget-object v0, Lcom/android/server/pm/PreinstallApp;->sAdvanceApps:Ljava/util/Map;

    return-object v0
.end method

.method private getChannel()Ljava/lang/String;
    .locals 2

    .line 670
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    .line 671
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 673
    :cond_0
    const-string v0, "ro.miui.customized.region"

    const-string v1, "Public Version"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getConnectEntity()Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;
    .locals 32

    .line 783
    move-object/from16 v14, p0

    const/4 v15, 0x0

    :try_start_0
    const-string v0, "get_device_info"

    invoke-direct {v14, v0}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 784
    sget-object v5, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    .line 785
    .local v5, "device":Ljava/lang/String;
    sget-object v6, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 786
    .local v6, "miuiVersion":Ljava/lang/String;
    sget-object v7, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 787
    .local v7, "androidVersion":Ljava/lang/String;
    sget-object v4, Lmiui/os/Build;->MODEL:Ljava/lang/String;

    .line 788
    .local v4, "model":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->getChannel()Ljava/lang/String;

    move-result-object v8

    .line 789
    .local v8, "channel":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v12

    .line 790
    .local v12, "lang":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/pm/CloudSignUtil;->getNonceStr()Ljava/lang/String;

    move-result-object v0

    .line 792
    .local v0, "nonceStr":Ljava/lang/String;
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v1, v1, 0x1

    move/from16 v31, v1

    .line 793
    .local v31, "isCn":Z
    if-eqz v31, :cond_0

    const-string v1, "CN"

    goto :goto_0

    :cond_0
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v9, v1

    .line 794
    .local v9, "region":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->getSku()Ljava/lang/String;

    move-result-object v13

    .line 795
    .local v13, "sku":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v14, v1}, Lcom/android/server/pm/AdvancePreinstallService;->getNetworkType(Landroid/content/Context;)I

    move-result v10

    .line 797
    .local v10, "networkType":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/id/IdentifierManager;->getOAID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 799
    .local v3, "imeiId":Ljava/lang/String;
    iget-object v2, v14, Lcom/android/server/pm/AdvancePreinstallService;->mImeiMe5:Ljava/lang/String;

    move-object/from16 v1, p0

    move/from16 v11, v31

    invoke-direct/range {v1 .. v13}, Lcom/android/server/pm/AdvancePreinstallService;->getParamsMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)Ljava/util/TreeMap;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/server/pm/CloudSignUtil;->getSign(Ljava/util/TreeMap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 802
    .local v24, "sign":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 803
    :cond_1
    new-instance v1, Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;

    iget-object v2, v14, Lcom/android/server/pm/AdvancePreinstallService;->mImeiMe5:Ljava/lang/String;

    move-object/from16 v16, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move-object/from16 v19, v5

    move-object/from16 v20, v6

    move-object/from16 v21, v8

    move-object/from16 v22, v12

    move-object/from16 v23, v0

    move/from16 v25, v31

    move-object/from16 v26, v9

    move-object/from16 v27, v7

    move-object/from16 v28, v4

    move/from16 v29, v10

    move-object/from16 v30, v13

    invoke-direct/range {v16 .. v30}, Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v15, v1

    .line 802
    :goto_1
    return-object v15

    .line 807
    .end local v0    # "nonceStr":Ljava/lang/String;
    .end local v3    # "imeiId":Ljava/lang/String;
    .end local v4    # "model":Ljava/lang/String;
    .end local v5    # "device":Ljava/lang/String;
    .end local v6    # "miuiVersion":Ljava/lang/String;
    .end local v7    # "androidVersion":Ljava/lang/String;
    .end local v8    # "channel":Ljava/lang/String;
    .end local v9    # "region":Ljava/lang/String;
    .end local v10    # "networkType":I
    .end local v12    # "lang":Ljava/lang/String;
    .end local v13    # "sku":Ljava/lang/String;
    .end local v24    # "sign":Ljava/lang/String;
    .end local v31    # "isCn":Z
    :catch_0
    move-exception v0

    .line 808
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 805
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 806
    .local v0, "ex":Ljava/util/NoSuchElementException;
    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->printStackTrace()V

    .line 809
    .end local v0    # "ex":Ljava/util/NoSuchElementException;
    nop

    .line 810
    :goto_2
    const-string v0, "get_device_info_failed"

    invoke-direct {v14, v0}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 811
    return-object v15
.end method

.method private getCustomizePreinstallAppList()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 185
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 186
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    .line 187
    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->getBusinessPreinstallConfig()Lcom/android/server/pm/MiuiBusinessPreinstallConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->getCustAppList()Ljava/util/List;

    move-result-object v0

    .line 188
    .local v0, "appDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v1, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 190
    .local v3, "appDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 191
    .local v4, "apps":[Ljava/io/File;
    if-eqz v4, :cond_1

    .line 192
    array-length v5, v4

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v5, :cond_1

    aget-object v7, v4, v6

    .line 193
    .local v7, "app":Ljava/io/File;
    invoke-direct {p0, v7}, Lcom/android/server/pm/AdvancePreinstallService;->isApkFile(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 194
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    .end local v7    # "app":Ljava/io/File;
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 198
    .end local v3    # "appDir":Ljava/io/File;
    .end local v4    # "apps":[Ljava/io/File;
    :cond_1
    goto :goto_0

    .line 199
    :cond_2
    return-object v1

    .line 201
    .end local v0    # "appDirs":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v1    # "appList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    :cond_3
    invoke-static {}, Lcom/android/server/pm/PreinstallApp;->getCustomizePreinstallAppList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getIMEIMD5()Ljava/lang/String;
    .locals 3

    .line 269
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getImeiList()Ljava/util/List;

    move-result-object v0

    .line 271
    .local v0, "imeiList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, ""

    .line 272
    .local v1, "imei":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 273
    invoke-static {v0}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    .line 276
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Lcom/android/server/pm/CloudSignUtil;->md5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-string v2, ""

    :goto_0
    return-object v2
.end method

.method private getNetworkType(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 339
    iget-boolean v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsNetworkConnected:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 340
    return v1

    .line 342
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsWifiConnected:Z

    if-eqz v0, :cond_1

    .line 343
    const/4 v0, -0x1

    return v0

    .line 345
    :cond_1
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 346
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_2

    .line 347
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    return v1

    .line 349
    :cond_2
    return v1
.end method

.method private getParamsMap(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)Ljava/util/TreeMap;
    .locals 3
    .param p1, "imeiMd5"    # Ljava/lang/String;
    .param p2, "imeiId"    # Ljava/lang/String;
    .param p3, "model"    # Ljava/lang/String;
    .param p4, "device"    # Ljava/lang/String;
    .param p5, "miuiVersion"    # Ljava/lang/String;
    .param p6, "androidVersion"    # Ljava/lang/String;
    .param p7, "channel"    # Ljava/lang/String;
    .param p8, "region"    # Ljava/lang/String;
    .param p9, "networkType"    # I
    .param p10, "isCn"    # Z
    .param p11, "lang"    # Ljava/lang/String;
    .param p12, "sku"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/TreeMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 818
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 819
    .local v0, "params":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "imeiMd5"

    invoke-virtual {v0, v1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820
    const-string v1, "imId"

    invoke-virtual {v0, v1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    const-string v1, "model"

    invoke-virtual {v0, v1, p3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    const-string v1, "device"

    invoke-virtual {v0, v1, p4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 823
    const-string v1, "miuiVersion"

    invoke-virtual {v0, v1, p5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    const-string v1, "androidVersion"

    invoke-virtual {v0, v1, p6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 825
    const-string v1, "channel"

    invoke-virtual {v0, v1, p7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    const-string v1, "region"

    invoke-virtual {v0, v1, p8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 827
    const-string v1, "isCn"

    invoke-static {p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 828
    const-string v1, "lang"

    invoke-virtual {v0, v1, p11}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    const-string v1, "networkType"

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    invoke-static {p12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 831
    const-string/jumbo v1, "sku"

    invoke-virtual {v0, v1, p12}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 834
    :cond_0
    return-object v0
.end method

.method private getPreloadAppInfo()V
    .locals 17

    .line 687
    move-object/from16 v1, p0

    const-string v2, "request_connect_exception"

    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->getConnectEntity()Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;

    move-result-object v3

    .line 688
    .local v3, "entity":Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;
    if-nez v3, :cond_0

    .line 689
    return-void

    .line 691
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/android/server/pm/AdvancePreinstallService$ConnectEntity;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 692
    .local v4, "url":Ljava/lang/String;
    const/4 v5, 0x0

    .line 693
    .local v5, "conn":Ljava/net/HttpURLConnection;
    const/4 v6, 0x0

    .line 696
    .local v6, "br":Ljava/io/BufferedReader;
    :try_start_0
    const-string v0, "request_connect_start"

    invoke-direct {v1, v0}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 698
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v5, v0

    .line 699
    const/16 v0, 0x7d0

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 700
    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 701
    const-string v0, "GET"

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 702
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 704
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 705
    .local v0, "responeCode":I
    const/16 v7, 0xc8

    const-string v8, "AdvancePreinstallService"

    if-ne v0, v7, :cond_9

    .line 706
    :try_start_1
    const-string v7, "request_connect_success"

    invoke-direct {v1, v7}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 707
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v10, 0x400

    invoke-direct {v7, v9, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v6, v7

    .line 708
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 710
    .local v7, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    move-object v10, v9

    .local v10, "line":Ljava/lang/String;
    if-eqz v9, :cond_1

    .line 711
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 714
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "result:"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    new-instance v9, Lorg/json/JSONObject;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 718
    .local v9, "result":Lorg/json/JSONObject;
    const-string v11, "code"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 719
    .local v11, "code":I
    const-string v12, "message"

    invoke-virtual {v9, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 720
    .local v12, "message":Ljava/lang/String;
    if-nez v11, :cond_8

    const-string v13, "Success"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 721
    const-string v8, "request_list_success"

    invoke-direct {v1, v8}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 722
    const-string v8, "data"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8
    :try_end_1
    .catch Ljava/net/ProtocolException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 723
    .local v8, "data":Lorg/json/JSONObject;
    const-string/jumbo v13, "uninstall_list_empty"

    if-nez v8, :cond_3

    .line 724
    :try_start_2
    invoke-direct {v1, v13}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 725
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAllInstallSuccess()V
    :try_end_2
    .catch Ljava/net/ProtocolException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 769
    if-eqz v5, :cond_2

    .line 770
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 772
    :cond_2
    nop

    .line 773
    invoke-direct {v1, v6}, Lcom/android/server/pm/AdvancePreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 726
    return-void

    .line 728
    :cond_3
    :try_start_3
    const-string v14, "previewOfflineApps"

    invoke-virtual {v8, v14}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 729
    .local v14, "previewOfflineApps":Lorg/json/JSONArray;
    const-string v15, "adOfflineApps"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 731
    .local v15, "adOfflineApps":Lorg/json/JSONArray;
    if-eqz v14, :cond_4

    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v16

    if-nez v16, :cond_5

    :cond_4
    if-eqz v15, :cond_6

    .line 732
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v16

    if-nez v16, :cond_5

    goto :goto_1

    .line 738
    :cond_5
    const-string v13, "miuiAdvancePreload"

    invoke-direct {v1, v14, v13}, Lcom/android/server/pm/AdvancePreinstallService;->handleOfflineApps(Lorg/json/JSONArray;Ljava/lang/String;)V

    .line 739
    const-string v13, "miuiAdvertisement"

    invoke-direct {v1, v15, v13}, Lcom/android/server/pm/AdvancePreinstallService;->handleOfflineApps(Lorg/json/JSONArray;Ljava/lang/String;)V

    .line 740
    invoke-direct {v1, v14}, Lcom/android/server/pm/AdvancePreinstallService;->handleAdvancePreloadTrack(Lorg/json/JSONArray;)V

    .line 741
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAdOnlineInfo()V

    .line 742
    .end local v8    # "data":Lorg/json/JSONObject;
    .end local v14    # "previewOfflineApps":Lorg/json/JSONArray;
    .end local v15    # "adOfflineApps":Lorg/json/JSONArray;
    goto :goto_2

    .line 733
    .restart local v8    # "data":Lorg/json/JSONObject;
    .restart local v14    # "previewOfflineApps":Lorg/json/JSONArray;
    .restart local v15    # "adOfflineApps":Lorg/json/JSONArray;
    :cond_6
    :goto_1
    invoke-direct {v1, v13}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 734
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->handleAllInstallSuccess()V
    :try_end_3
    .catch Ljava/net/ProtocolException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 769
    if-eqz v5, :cond_7

    .line 770
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 772
    :cond_7
    nop

    .line 773
    invoke-direct {v1, v6}, Lcom/android/server/pm/AdvancePreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 735
    return-void

    .line 743
    .end local v8    # "data":Lorg/json/JSONObject;
    .end local v14    # "previewOfflineApps":Lorg/json/JSONArray;
    .end local v15    # "adOfflineApps":Lorg/json/JSONArray;
    :cond_8
    :try_start_4
    const-string v13, "request_list_failed"

    invoke-direct {v1, v13}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 744
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V

    .line 745
    const-string v13, "advance_request result is failed"

    invoke-static {v8, v13}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    .end local v9    # "result":Lorg/json/JSONObject;
    .end local v10    # "line":Ljava/lang/String;
    .end local v11    # "code":I
    .end local v12    # "message":Ljava/lang/String;
    :goto_2
    goto :goto_3

    .line 748
    :cond_9
    const-string v7, "request_connect_failed"

    invoke-direct {v1, v7}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 749
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V

    .line 750
    const-string/jumbo v7, "server can not connected"

    invoke-static {v8, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/net/ProtocolException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 769
    .end local v0    # "responeCode":I
    :goto_3
    if-eqz v5, :cond_a

    .line 770
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 772
    :cond_a
    if-eqz v6, :cond_f

    .line 773
    :goto_4
    invoke-direct {v1, v6}, Lcom/android/server/pm/AdvancePreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V

    goto :goto_5

    .line 769
    :catchall_0
    move-exception v0

    goto :goto_6

    .line 764
    :catch_0
    move-exception v0

    .line 765
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-direct {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 766
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V

    .line 767
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 769
    .end local v0    # "e":Ljava/lang/Exception;
    if-eqz v5, :cond_b

    .line 770
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 772
    :cond_b
    if-eqz v6, :cond_f

    .line 773
    goto :goto_4

    .line 760
    :catch_1
    move-exception v0

    .line 761
    .local v0, "e":Lorg/json/JSONException;
    :try_start_6
    const-string v2, "json_exception"

    invoke-direct {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 762
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V

    .line 763
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 769
    .end local v0    # "e":Lorg/json/JSONException;
    if-eqz v5, :cond_c

    .line 770
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 772
    :cond_c
    if-eqz v6, :cond_f

    .line 773
    goto :goto_4

    .line 756
    :catch_2
    move-exception v0

    .line 757
    .local v0, "e":Ljava/io/IOException;
    :try_start_7
    invoke-direct {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 758
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V

    .line 759
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 769
    .end local v0    # "e":Ljava/io/IOException;
    if-eqz v5, :cond_d

    .line 770
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 772
    :cond_d
    if-eqz v6, :cond_f

    .line 773
    goto :goto_4

    .line 752
    :catch_3
    move-exception v0

    .line 753
    .local v0, "e":Ljava/net/ProtocolException;
    :try_start_8
    invoke-direct {v1, v2}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;)V

    .line 754
    invoke-direct/range {p0 .. p0}, Lcom/android/server/pm/AdvancePreinstallService;->uninstallAdvancePreinstallApps()V

    .line 755
    invoke-virtual {v0}, Ljava/net/ProtocolException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 769
    .end local v0    # "e":Ljava/net/ProtocolException;
    if-eqz v5, :cond_e

    .line 770
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 772
    :cond_e
    if-eqz v6, :cond_f

    .line 773
    goto :goto_4

    .line 776
    :cond_f
    :goto_5
    return-void

    .line 769
    :goto_6
    if-eqz v5, :cond_10

    .line 770
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 772
    :cond_10
    if-eqz v6, :cond_11

    .line 773
    invoke-direct {v1, v6}, Lcom/android/server/pm/AdvancePreinstallService;->closeBufferedReader(Ljava/io/BufferedReader;)V

    .line 775
    :cond_11
    throw v0
.end method

.method private getSku()Ljava/lang/String;
    .locals 5

    .line 653
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const-string v1, ""

    if-nez v0, :cond_0

    .line 654
    return-object v1

    .line 656
    :cond_0
    const-string v0, "ro.miui.build.region"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 657
    .local v0, "buildRegion":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "MI"

    if-nez v1, :cond_2

    .line 658
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v3, "GLOBAL"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    .line 660
    :cond_2
    sget-boolean v1, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    if-eqz v1, :cond_3

    .line 661
    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 662
    .local v1, "buildVersion":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x4

    if-le v3, v4, :cond_3

    .line 663
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 666
    .end local v1    # "buildVersion":Ljava/lang/String;
    :cond_3
    return-object v2
.end method

.method private handleAdOnlineInfo()V
    .locals 3

    .line 489
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/pm/AdvancePreinstallService;->isAdOnlineServiceAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 491
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.xiaomi.preload"

    const-string v2, "com.xiaomi.preload.services.AdvancePreInstallService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 492
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 494
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private handleAdvancePreinstallApps()V
    .locals 1

    .line 290
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/pm/AdvancePreinstallService;->isProvisioned(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    return-void

    .line 293
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 294
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getIMEIMD5()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mImeiMe5:Ljava/lang/String;

    .line 295
    new-instance v0, Lcom/android/server/pm/AdvancePreinstallService$3;

    invoke-direct {v0, p0}, Lcom/android/server/pm/AdvancePreinstallService$3;-><init>(Lcom/android/server/pm/AdvancePreinstallService;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 307
    return-void
.end method

.method private handleAdvancePreinstallAppsDelay()V
    .locals 4

    .line 280
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->initOneTrack()V

    .line 281
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/server/pm/AdvancePreinstallService$2;

    invoke-direct {v1, p0}, Lcom/android/server/pm/AdvancePreinstallService$2;-><init>(Lcom/android/server/pm/AdvancePreinstallService;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 287
    return-void
.end method

.method private handleAdvancePreloadTrack(Lorg/json/JSONArray;)V
    .locals 7
    .param p1, "array"    # Lorg/json/JSONArray;

    .line 522
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAdvanceApps()Ljava/util/Map;

    move-result-object v0

    .line 523
    .local v0, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 524
    .local v2, "entry":Ljava/util/Map$Entry;
    if-nez v2, :cond_0

    .line 525
    goto :goto_0

    .line 528
    :cond_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 529
    .local v3, "path":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/android/server/pm/AdvancePreinstallService;->isAdvanceApps(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 530
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 531
    .local v4, "existPkgName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 532
    .local v5, "containsPkgName":Z
    iget-object v6, p0, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v6}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 533
    sget-object v6, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->sCloudControlUninstall:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto :goto_1

    .line 535
    :cond_1
    sget-object v6, Lcom/android/server/pm/PreinstallApp;->sCloudControlUninstall:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 537
    :goto_1
    sget-boolean v6, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v6, :cond_2

    if-eqz v5, :cond_3

    .line 538
    :cond_2
    invoke-direct {p0, v4, p1}, Lcom/android/server/pm/AdvancePreinstallService;->isPackageToRemove(Ljava/lang/String;Lorg/json/JSONArray;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/android/server/pm/AdvancePreinstallService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p0, v6, v4}, Lcom/android/server/pm/AdvancePreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 540
    :cond_3
    invoke-direct {p0, v4}, Lcom/android/server/pm/AdvancePreinstallService;->trackAdvancePreloadSuccess(Ljava/lang/String;)V

    .line 543
    .end local v2    # "entry":Ljava/util/Map$Entry;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "existPkgName":Ljava/lang/String;
    .end local v5    # "containsPkgName":Z
    :cond_4
    goto :goto_0

    .line 544
    :cond_5
    return-void
.end method

.method private handleAllInstallSuccess()V
    .locals 6

    .line 547
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAdvanceApps()Ljava/util/Map;

    move-result-object v0

    .line 548
    .local v0, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 549
    .local v2, "entry":Ljava/util/Map$Entry;
    if-nez v2, :cond_0

    .line 550
    goto :goto_0

    .line 553
    :cond_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 554
    .local v3, "path":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/android/server/pm/AdvancePreinstallService;->isAdvanceApps(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 555
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 556
    .local v4, "existPkgName":Ljava/lang/String;
    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/android/server/pm/AdvancePreinstallService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p0, v5, v4}, Lcom/android/server/pm/AdvancePreinstallService;->exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 558
    :cond_1
    invoke-direct {p0, v4}, Lcom/android/server/pm/AdvancePreinstallService;->trackAdvancePreloadSuccess(Ljava/lang/String;)V

    .line 561
    .end local v2    # "entry":Ljava/util/Map$Entry;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "existPkgName":Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 562
    :cond_3
    return-void
.end method

.method private handleOfflineApps(Lorg/json/JSONArray;Ljava/lang/String;)V
    .locals 7
    .param p1, "array"    # Lorg/json/JSONArray;
    .param p2, "tag"    # Ljava/lang/String;

    .line 461
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_3

    .line 464
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 466
    :try_start_0
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 467
    .local v1, "appInfo":Lorg/json/JSONObject;
    if-nez v1, :cond_1

    .line 468
    goto :goto_2

    .line 470
    :cond_1
    const-string v2, "packageName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 471
    .local v2, "pkgName":Ljava/lang/String;
    const-string v3, "confId"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 472
    .local v3, "confId":I
    const-string v4, "offlineCount"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 473
    .local v4, "offlineCount":I
    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v5, :cond_3

    .line 474
    invoke-direct {p0, v2, v3, v4, p2}, Lcom/android/server/pm/AdvancePreinstallService;->recordUnInstallApps(Ljava/lang/String;IILjava/lang/String;)V

    .line 475
    invoke-direct {p0, v2}, Lcom/android/server/pm/AdvancePreinstallService;->findTrackingApk(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 476
    .local v5, "trackingPkg":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 477
    const-string v6, ""

    invoke-direct {p0, v5, v3, v4, v6}, Lcom/android/server/pm/AdvancePreinstallService;->recordUnInstallApps(Ljava/lang/String;IILjava/lang/String;)V

    .line 479
    .end local v5    # "trackingPkg":Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 480
    :cond_3
    invoke-direct {p0, v2, v3, v4, p2}, Lcom/android/server/pm/AdvancePreinstallService;->doUninstallApps(Ljava/lang/String;IILjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    .end local v1    # "appInfo":Lorg/json/JSONObject;
    .end local v2    # "pkgName":Ljava/lang/String;
    .end local v3    # "confId":I
    .end local v4    # "offlineCount":I
    :goto_1
    goto :goto_2

    .line 482
    :catch_0
    move-exception v1

    .line 483
    .local v1, "e":Lorg/json/JSONException;
    const-string v2, "AdvancePreinstallService"

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 486
    .end local v0    # "i":I
    :cond_4
    return-void

    .line 462
    :cond_5
    :goto_3
    return-void
.end method

.method private initOneTrack()V
    .locals 2

    .line 642
    new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack;

    invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack;-><init>()V

    iput-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mTrack:Lcom/android/server/pm/PreInstallServiceTrack;

    .line 643
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/pm/PreInstallServiceTrack;->bindTrackService(Landroid/content/Context;)V

    .line 644
    return-void
.end method

.method private isAdOnlineServiceAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 589
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 590
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.xiaomi.preload"

    const-string v2, "com.xiaomi.preload.services.AdvancePreInstallService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 592
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 593
    return v2

    .line 595
    :cond_0
    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 596
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method private isAdvanceApps(Ljava/lang/String;)Z
    .locals 1
    .param p1, "apkPath"    # Ljava/lang/String;

    .line 256
    if-eqz p1, :cond_0

    const-string v0, "miuiAdvancePreload"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isAdvertApps(Ljava/lang/String;)Z
    .locals 1
    .param p1, "apkPath"    # Ljava/lang/String;

    .line 265
    if-eqz p1, :cond_0

    const-string v0, "miuiAdvertisement"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isApkFile(Ljava/io/File;)Z
    .locals 2
    .param p1, "apkFile"    # Ljava/io/File;

    .line 206
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".apk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isNetworkConnected(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 314
    const/4 v0, 0x0

    :try_start_0
    const-string v1, "connectivity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 315
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 316
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-nez v2, :cond_0

    .line 317
    return v0

    .line 319
    :cond_0
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    .line 320
    .local v3, "type":I
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 321
    iput-boolean v4, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsWifiConnected:Z

    .line 323
    :cond_1
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsNetworkConnected:Z

    .line 324
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/pm/AdvancePreinstallService;->mNetworkTypeName:Ljava/lang/String;

    .line 325
    iget-boolean v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mIsNetworkConnected:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 326
    .end local v1    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v3    # "type":I
    :catch_0
    move-exception v1

    .line 327
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 329
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method private isPackageToRemove(Ljava/lang/String;Lorg/json/JSONArray;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "array"    # Lorg/json/JSONArray;

    .line 565
    const/4 v0, 0x0

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 568
    :cond_0
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v1

    .line 569
    .local v1, "deleteCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 571
    :try_start_0
    invoke-virtual {p2, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 572
    .local v3, "appInfo":Lorg/json/JSONObject;
    if-nez v3, :cond_1

    .line 573
    goto :goto_1

    .line 576
    :cond_1
    const-string v4, "packageName"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 577
    .local v4, "pkgName":Ljava/lang/String;
    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v5, :cond_2

    .line 579
    const/4 v0, 0x1

    return v0

    .line 583
    .end local v3    # "appInfo":Lorg/json/JSONObject;
    .end local v4    # "pkgName":Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 581
    :catch_0
    move-exception v3

    .line 582
    .local v3, "e":Lorg/json/JSONException;
    const-string v4, "AdvancePreinstallService"

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 585
    .end local v2    # "i":I
    :cond_3
    return v0

    .line 566
    .end local v1    # "deleteCount":I
    :cond_4
    :goto_2
    return v0
.end method

.method public static isProvisioned(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 848
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 849
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "device_provisioned"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private parsePackageLite(Ljava/io/File;)Landroid/content/pm/parsing/PackageLite;
    .locals 5
    .param p1, "apkFile"    # Ljava/io/File;

    .line 210
    invoke-static {}, Landroid/content/pm/parsing/result/ParseTypeImpl;->forDefaultParsing()Landroid/content/pm/parsing/result/ParseTypeImpl;

    move-result-object v0

    .line 211
    .local v0, "input":Landroid/content/pm/parsing/result/ParseTypeImpl;
    nop

    .line 212
    invoke-virtual {v0}, Landroid/content/pm/parsing/result/ParseTypeImpl;->reset()Landroid/content/pm/parsing/result/ParseInput;

    move-result-object v1

    .line 211
    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Landroid/content/pm/parsing/ApkLiteParseUtils;->parsePackageLite(Landroid/content/pm/parsing/result/ParseInput;Ljava/io/File;I)Landroid/content/pm/parsing/result/ParseResult;

    move-result-object v1

    .line 213
    .local v1, "result":Landroid/content/pm/parsing/result/ParseResult;, "Landroid/content/pm/parsing/result/ParseResult<Landroid/content/pm/parsing/PackageLite;>;"
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->isError()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parsePackageLite: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 215
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getException()Ljava/lang/Exception;

    move-result-object v3

    .line 214
    const-string v4, "AdvancePreinstallService"

    invoke-static {v4, v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 216
    const/4 v2, 0x0

    return-object v2

    .line 218
    :cond_0
    invoke-interface {v1}, Landroid/content/pm/parsing/result/ParseResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/parsing/PackageLite;

    return-object v2
.end method

.method private recordUnInstallApps(Ljava/lang/String;IILjava/lang/String;)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "confId"    # I
    .param p3, "offlineCount"    # I
    .param p4, "appType"    # Ljava/lang/String;

    .line 445
    const-string v1, "remove_from_list_begin"

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 446
    const-string v3, "begin_uninstall"

    move-object v2, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448
    sget-object v0, Lcom/android/server/pm/MiuiBusinessPreinstallConfig;->sCloudControlUninstall:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 450
    :cond_0
    sget-object v0, Lcom/android/server/pm/PreinstallApp;->sCloudControlUninstall:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    :goto_0
    const-string/jumbo v2, "uninstall_success"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/server/pm/AdvancePreinstallService;->trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 453
    return-void
.end method

.method private track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V
    .locals 3
    .param p1, "action"    # Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 647
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mTrack:Lcom/android/server/pm/PreInstallServiceTrack;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService;->mTrack:Lcom/android/server/pm/PreInstallServiceTrack;

    invoke-virtual {p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->getContent()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack;->trackEvent(Ljava/lang/String;I)V

    .line 650
    :cond_0
    return-void
.end method

.method private trackAdvancePreloadSuccess(Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .line 497
    const-string v0, "install_success"

    .line 498
    .local v0, "eventName":Ljava/lang/String;
    new-instance v1, Lcom/android/server/pm/PreInstallServiceTrack$Action;

    invoke-direct {v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V

    .line 499
    .local v1, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action;
    const-string v2, "request_type"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 500
    const-string v2, "imeiMd5"

    iget-object v4, p0, Lcom/android/server/pm/AdvancePreinstallService;->mImeiMe5:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 501
    const-string v2, "packageName"

    invoke-virtual {v1, v2, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 502
    const-string v2, "networkType"

    iget-object v4, p0, Lcom/android/server/pm/AdvancePreinstallService;->mNetworkTypeName:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 503
    const-string v2, "appType"

    const-string v4, "miuiAdvancePreload"

    invoke-virtual {v1, v2, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 504
    const-string v2, "saleschannels"

    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getChannel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 505
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/id/IdentifierManager;->getOAID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "imId"

    invoke-virtual {v1, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 507
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/2addr v2, v3

    .line 508
    .local v2, "isCn":Z
    if-eqz v2, :cond_0

    const-string v3, "CN"

    goto :goto_0

    :cond_0
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 509
    .local v3, "region":Ljava/lang/String;
    :goto_0
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getSku()Ljava/lang/String;

    move-result-object v4

    .line 510
    .local v4, "sku":Ljava/lang/String;
    const-string v5, "region"

    invoke-virtual {v1, v5, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 511
    const-string/jumbo v5, "sku"

    invoke-virtual {v1, v5, v4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 512
    const-string v5, "EVENT_NAME"

    invoke-virtual {v1, v5, v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 513
    invoke-direct {p0, v1}, Lcom/android/server/pm/AdvancePreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V

    .line 514
    return-void
.end method

.method private trackEvent(Ljava/lang/String;)V
    .locals 5
    .param p1, "event"    # Ljava/lang/String;

    .line 625
    new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$Action;

    invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V

    .line 626
    .local v0, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action;
    const-string v1, "request_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 627
    const-string v1, "imeiMd5"

    iget-object v3, p0, Lcom/android/server/pm/AdvancePreinstallService;->mImeiMe5:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 628
    const-string v1, "networkType"

    iget-object v3, p0, Lcom/android/server/pm/AdvancePreinstallService;->mNetworkTypeName:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 629
    const-string v1, "saleschannels"

    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getChannel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 630
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/id/IdentifierManager;->getOAID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "imId"

    invoke-virtual {v0, v3, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 632
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/2addr v1, v2

    .line 633
    .local v1, "isCn":Z
    if-eqz v1, :cond_0

    const-string v2, "CN"

    goto :goto_0

    :cond_0
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 634
    .local v2, "region":Ljava/lang/String;
    :goto_0
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getSku()Ljava/lang/String;

    move-result-object v3

    .line 635
    .local v3, "sku":Ljava/lang/String;
    const-string v4, "region"

    invoke-virtual {v0, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 636
    const-string/jumbo v4, "sku"

    invoke-virtual {v0, v4, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 637
    const-string v4, "EVENT_NAME"

    invoke-virtual {v0, v4, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 638
    invoke-direct {p0, v0}, Lcom/android/server/pm/AdvancePreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V

    .line 639
    return-void
.end method

.method private uninstallAdvancePreinstallApps()V
    .locals 7

    .line 233
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getAdvanceApps()Ljava/util/Map;

    move-result-object v0

    .line 234
    .local v0, "pkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "advance app size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AdvancePreinstallService"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 236
    .local v2, "entry":Ljava/util/Map$Entry;
    if-eqz v2, :cond_2

    .line 237
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 238
    .local v3, "path":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/android/server/pm/AdvancePreinstallService;->isAdvanceApps(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "miuiAdvancePreload"

    goto :goto_1

    :cond_0
    const-string v4, "miuiAdvertisement"

    .line 240
    .local v4, "appType":Ljava/lang/String;
    :goto_1
    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v6, -0x1

    if-eqz v5, :cond_1

    .line 241
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 242
    .local v5, "packageName":Ljava/lang/String;
    invoke-direct {p0, v5, v6, v6, v4}, Lcom/android/server/pm/AdvancePreinstallService;->recordUnInstallApps(Ljava/lang/String;IILjava/lang/String;)V

    .line 243
    .end local v5    # "packageName":Ljava/lang/String;
    goto :goto_2

    .line 244
    :cond_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, v5, v6, v6, v4}, Lcom/android/server/pm/AdvancePreinstallService;->doUninstallApps(Ljava/lang/String;IILjava/lang/String;)V

    .line 247
    .end local v2    # "entry":Ljava/util/Map$Entry;
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "appType":Ljava/lang/String;
    :cond_2
    :goto_2
    goto :goto_0

    .line 248
    :cond_3
    return-void
.end method


# virtual methods
.method public exists(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 2
    .param p1, "pm"    # Landroid/content/pm/PackageManager;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 356
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 360
    :cond_0
    const/16 v1, 0x80

    :try_start_0
    invoke-virtual {p1, p2, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 362
    :catch_0
    move-exception v1

    .line 363
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    return v0

    .line 357
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    :goto_0
    return v0
.end method

.method public getFileContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .line 431
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 432
    .local v0, "file":Ljava/io/File;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    .local v1, "in":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 434
    .local v2, "length":J
    long-to-int v4, v2

    new-array v4, v4, [B

    .line 435
    .local v4, "content":[B
    invoke-virtual {v1, v4}, Ljava/io/FileInputStream;->read([B)I

    .line 436
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 437
    new-instance v5, Ljava/lang/String;

    sget-object v6, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v5, v4, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 438
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 437
    return-object v5

    .line 432
    .end local v2    # "length":J
    .end local v4    # "content":[B
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "file":Ljava/io/File;
    .end local p0    # "this":Lcom/android/server/pm/AdvancePreinstallService;
    .end local p1    # "filePath":Ljava/lang/String;
    :goto_0
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 438
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v0    # "file":Ljava/io/File;
    .restart local p0    # "this":Lcom/android/server/pm/AdvancePreinstallService;
    .restart local p1    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 439
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "AdvancePreinstallService"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    .end local v1    # "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    return-object v1
.end method

.method public onBootPhase(I)V
    .locals 4
    .param p1, "phase"    # I

    .line 155
    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_0

    .line 156
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.miui.action.SIM_DETECTION"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 157
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/pm/AdvancePreinstallService;->mReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 158
    invoke-static {}, Lcom/android/server/pm/MiuiPreinstallHelper;->getInstance()Lcom/android/server/pm/MiuiPreinstallHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/pm/AdvancePreinstallService;->mMiuiPreinstallHelper:Lcom/android/server/pm/MiuiPreinstallHelper;

    .line 160
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 150
    const-string v0, "AdvancePreinstallService"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    return-void
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 5
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "confId"    # I
    .param p4, "offlineCount"    # I
    .param p5, "tag"    # Ljava/lang/String;

    .line 601
    new-instance v0, Lcom/android/server/pm/PreInstallServiceTrack$Action;

    invoke-direct {v0}, Lcom/android/server/pm/PreInstallServiceTrack$Action;-><init>()V

    .line 602
    .local v0, "action":Lcom/android/server/pm/PreInstallServiceTrack$Action;
    const-string v1, "packageName"

    invoke-virtual {v0, v1, p2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 603
    const-string v1, "confId"

    invoke-virtual {v0, v1, p3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 604
    const-string v1, "offlineCount"

    invoke-virtual {v0, v1, p4}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 605
    const-string v1, "appType"

    invoke-virtual {v0, v1, p5}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 606
    const-string v1, "request_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;I)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 607
    const-string v1, "saleschannels"

    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getChannel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 608
    invoke-static {p2}, Lmiui/os/MiuiInit;->isPreinstalledPackage(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    const-string v3, "isPreinstalled"

    invoke-virtual {v0, v3, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 609
    invoke-static {p2}, Lmiui/os/MiuiInit;->getMiuiChannelPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "miuiChannelPath"

    invoke-virtual {v0, v3, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 611
    const-string v1, "imeiMd5"

    iget-object v3, p0, Lcom/android/server/pm/AdvancePreinstallService;->mImeiMe5:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 612
    invoke-virtual {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/id/IdentifierManager;->getOAID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "imId"

    invoke-virtual {v0, v3, v1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 613
    const-string v1, "networkType"

    iget-object v3, p0, Lcom/android/server/pm/AdvancePreinstallService;->mNetworkTypeName:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 615
    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/2addr v1, v2

    .line 616
    .local v1, "isCn":Z
    if-eqz v1, :cond_0

    const-string v2, "CN"

    goto :goto_0

    :cond_0
    invoke-static {}, Lmiui/os/Build;->getCustVariant()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 617
    .local v2, "region":Ljava/lang/String;
    :goto_0
    invoke-direct {p0}, Lcom/android/server/pm/AdvancePreinstallService;->getSku()Ljava/lang/String;

    move-result-object v3

    .line 618
    .local v3, "sku":Ljava/lang/String;
    const-string v4, "region"

    invoke-virtual {v0, v4, v2}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 619
    const-string/jumbo v4, "sku"

    invoke-virtual {v0, v4, v3}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 620
    const-string v4, "EVENT_NAME"

    invoke-virtual {v0, v4, p1}, Lcom/android/server/pm/PreInstallServiceTrack$Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PreInstallServiceTrack$Action;

    .line 621
    invoke-direct {p0, v0}, Lcom/android/server/pm/AdvancePreinstallService;->track(Lcom/android/server/pm/PreInstallServiceTrack$Action;)V

    .line 622
    return-void
.end method
