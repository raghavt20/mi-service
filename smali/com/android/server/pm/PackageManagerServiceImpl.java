public class com.android.server.pm.PackageManagerServiceImpl extends com.android.server.pm.PackageManagerServiceStub {
	 /* .source "PackageManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.pm.PackageManagerServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String ANDROID_INSTALLER_PACKAGE;
private static final java.lang.String APP_LIST_FILE;
private static final java.lang.String CARRIER_SYS_APPS_LIST;
private static final java.lang.String CUSTOMIZED_REGION;
static final Integer DELETE_FAILED_FORBIDED_BY_MIUI;
static final java.lang.String EP_INSTALLER_PKG_WHITELIST;
private static final java.lang.String GOOGLE_INSTALLER_PACKAGE;
private static final java.lang.String GOOGLE_WEB_SEARCH_PACKAGE;
public static final Integer INSTALL_FULL_APP;
public static final Integer INSTALL_REASON_USER;
private static final Boolean IS_GLOBAL_REGION_BUILD;
private static Boolean IS_INTERNATIONAL_BUILD;
private static final java.lang.String MANAGED_PROVISION;
static final java.lang.String MIUI_CORE_APPS;
private static final java.lang.String MIUI_INSTALLER_PACKAGE;
private static final java.lang.String MIUI_MARKET_PACKAGE;
static final Integer MIUI_VERIFICATION_TIMEOUT;
private static final java.io.File PACKAGE_EVENT_DIR;
private static final java.lang.String PACKAGE_MIME_TYPE;
private static final java.lang.String PACKAGE_WEBVIEW;
private static final java.lang.String PKMS_ATEST_NAME;
private static final java.lang.String SHIM_PKG;
private static final Boolean SUPPORT_DEL_COTA_APP;
static final java.lang.String TAG;
private static final java.lang.String TAG_ADD_APPS;
private static final java.lang.String TAG_APP;
private static final java.lang.String TAG_IGNORE_APPS;
static final java.util.ArrayList sAllowPackage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sClearPermissionSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static volatile android.os.Handler sFirstUseHandler;
private static java.lang.Object sFirstUseLock;
private static android.os.HandlerThread sFirstUseThread;
private static final java.util.Set sGLPhoneAppsSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.util.Set sInstallerSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final Boolean sIsBootLoaderLocked;
public static final Boolean sIsReleaseRom;
static final java.util.ArrayList sNoVerifyAllowPackage;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sNotSupportUpdateSystemApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
static java.util.ArrayList sShellCheckPermissions;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sSilentlyUninstallPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set sTHPhoneAppsSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.content.Context mContext;
private java.util.HashMap mCotaApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.lang.String mCurrentPackageInstaller;
private com.android.server.pm.dex.DexManager mDexManager;
private com.android.server.pm.DexOptHelper mDexOptHelper;
private com.android.server.pm.DexoptServiceThread mDexoptServiceThread;
private com.android.server.pm.verify.domain.DomainVerificationService mDomainVerificationService;
private final java.util.Set mIgnoreApks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Set mIgnorePackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mIsGlobalCrbtSupported;
private java.util.List mIsSupportAttention;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.pm.MiuiDexopt mMiuiDexopt;
private com.android.server.pm.pkg.AndroidPackage mMiuiInstallerPackage;
private com.android.server.pm.PackageSetting mMiuiInstallerPackageSetting;
private com.android.server.pm.MiuiPreinstallHelper mMiuiPreinstallHelper;
private com.android.server.pm.PackageManagerService$IPackageManagerImpl mPM;
private com.android.server.pm.PackageManagerCloudHelper mPackageManagerCloudHelper;
private com.android.server.pm.PackageDexOptimizer mPdo;
private com.android.server.pm.Settings mPkgSettings;
private com.android.server.pm.PackageManagerService mPms;
private java.lang.String mRsaFeature;
private Integer persistAppCount;
private Integer systemAppCount;
private Integer thirdAppCount;
/* # direct methods */
public static java.util.List $r8$lambda$lK2NBL-uNMl0IZ57KeUxVKBIOYk ( com.android.server.pm.PackageManagerServiceImpl p0, com.android.server.pm.pkg.AndroidPackage p1, Long p2, Integer p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/pm/PackageManagerServiceImpl;->lambda$getPackageInfoBySelf$5(Lcom/android/server/pm/pkg/AndroidPackage;JI)Ljava/util/List; */
} // .end method
public static java.util.List $r8$lambda$t2nlgQrmPQd-pT6SWmphpuoPtHA ( com.android.server.pm.PackageManagerServiceImpl p0, com.android.server.pm.pkg.AndroidPackage p1, Long p2, Integer p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/pm/PackageManagerServiceImpl;->lambda$getApplicationInfoBySelf$6(Lcom/android/server/pm/pkg/AndroidPackage;JI)Ljava/util/List; */
} // .end method
static com.android.server.pm.verify.domain.DomainVerificationService -$$Nest$fgetmDomainVerificationService ( com.android.server.pm.PackageManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDomainVerificationService;
} // .end method
static void -$$Nest$mdoUpdateSystemAppDefaultUserStateForAllUsers ( com.android.server.pm.PackageManagerServiceImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->doUpdateSystemAppDefaultUserStateForAllUsers()V */
return;
} // .end method
static void -$$Nest$mhandleFirstUseActivity ( com.android.server.pm.PackageManagerServiceImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->handleFirstUseActivity(Ljava/lang/String;)V */
return;
} // .end method
static com.android.server.pm.PackageManagerServiceImpl ( ) {
/* .locals 13 */
/* .line 122 */
/* nop */
/* .line 123 */
final String v0 = "ro.boot.vbmeta.device_state"; // const-string v0, "ro.boot.vbmeta.device_state"
android.os.SystemProperties .get ( v0 );
/* .line 122 */
final String v1 = "locked"; // const-string v1, "locked"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.android.server.pm.PackageManagerServiceImpl.sIsBootLoaderLocked = (v0!= 0);
/* .line 125 */
v0 = miui.os.Build.TAGS;
final String v1 = "release-key"; // const-string v1, "release-key"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
com.android.server.pm.PackageManagerServiceImpl.sIsReleaseRom = (v0!= 0);
/* .line 149 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 151 */
final String v1 = "com.miui.packageinstaller"; // const-string v1, "com.miui.packageinstaller"
/* .line 152 */
final String v2 = "com.google.android.packageinstaller"; // const-string v2, "com.google.android.packageinstaller"
/* .line 153 */
final String v3 = "com.android.packageinstaller"; // const-string v3, "com.android.packageinstaller"
/* .line 156 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 158 */
final String v4 = "com.android.bluetooth"; // const-string v4, "com.android.bluetooth"
/* .line 159 */
final String v4 = "com.miui.powerkeeper"; // const-string v4, "com.miui.powerkeeper"
/* .line 162 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 163 */
/* new-instance v4, Ljava/util/HashSet; */
/* invoke-direct {v4}, Ljava/util/HashSet;-><init>()V */
/* .line 165 */
final String v5 = "com.android.contacts"; // const-string v5, "com.android.contacts"
/* .line 166 */
final String v5 = "com.android.incallui"; // const-string v5, "com.android.incallui"
/* .line 167 */
final String v0 = "com.google.android.contacts"; // const-string v0, "com.google.android.contacts"
/* .line 168 */
final String v0 = "com.google.android.dialer"; // const-string v0, "com.google.android.dialer"
/* .line 171 */
final String v0 = "ro.miui.customized.region"; // const-string v0, "ro.miui.customized.region"
final String v4 = ""; // const-string v4, ""
android.os.SystemProperties .get ( v0,v4 );
/* .line 172 */
/* nop */
/* .line 173 */
final String v5 = "ro.miui.cota_app_del_support"; // const-string v5, "ro.miui.cota_app_del_support"
int v6 = 0; // const/4 v6, 0x0
v5 = android.os.SystemProperties .getBoolean ( v5,v6 );
com.android.server.pm.PackageManagerServiceImpl.SUPPORT_DEL_COTA_APP = (v5!= 0);
/* .line 174 */
/* nop */
/* .line 175 */
final String v5 = "ro.miui.build.region"; // const-string v5, "ro.miui.build.region"
android.os.SystemProperties .get ( v5 );
final String v7 = "global"; // const-string v7, "global"
v5 = (( java.lang.String ) v7 ).equalsIgnoreCase ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
com.android.server.pm.PackageManagerServiceImpl.IS_GLOBAL_REGION_BUILD = (v5!= 0);
/* .line 176 */
final String v5 = "ro.product.mod_device"; // const-string v5, "ro.product.mod_device"
android.os.SystemProperties .get ( v5,v4 );
final String v5 = "_global"; // const-string v5, "_global"
v4 = (( java.lang.String ) v4 ).contains ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
com.android.server.pm.PackageManagerServiceImpl.IS_INTERNATIONAL_BUILD = (v4!= 0);
/* .line 177 */
/* new-instance v4, Ljava/io/File; */
final String v5 = "data/system/package-event"; // const-string v5, "data/system/package-event"
/* invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 202 */
/* new-instance v4, Ljava/util/HashSet; */
/* invoke-direct {v4}, Ljava/util/HashSet;-><init>()V */
/* .line 204 */
/* .line 205 */
/* .line 206 */
/* .line 207 */
final String v5 = "com.miui.cleanmaster"; // const-string v5, "com.miui.cleanmaster"
/* .line 208 */
final String v5 = "com.miui.thirdappassistant"; // const-string v5, "com.miui.thirdappassistant"
/* .line 209 */
final String v5 = "com.miui.screenrecorder"; // const-string v5, "com.miui.screenrecorder"
/* .line 577 */
/* filled-new-array {v3, v1}, [Ljava/lang/String; */
/* .line 644 */
final String v7 = "com.lbe.security.miui"; // const-string v7, "com.lbe.security.miui"
final String v8 = "com.miui.securitycenter"; // const-string v8, "com.miui.securitycenter"
final String v9 = "com.android.updater"; // const-string v9, "com.android.updater"
final String v10 = "com.xiaomi.market"; // const-string v10, "com.xiaomi.market"
final String v11 = "com.xiaomi.finddevice"; // const-string v11, "com.xiaomi.finddevice"
final String v12 = "com.miui.home"; // const-string v12, "com.miui.home"
/* filled-new-array/range {v7 ..v12}, [Ljava/lang/String; */
/* .line 698 */
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 700 */
final String v3 = "ro.miui.product.home"; // const-string v3, "ro.miui.product.home"
final String v4 = "com.miui.home"; // const-string v4, "com.miui.home"
android.os.SystemProperties .get ( v3,v4 );
/* .line 701 */
final String v3 = "com.xiaomi.market"; // const-string v3, "com.xiaomi.market"
/* .line 702 */
final String v4 = "com.xiaomi.mipicks"; // const-string v4, "com.xiaomi.mipicks"
/* .line 703 */
final String v5 = "com.xiaomi.discover"; // const-string v5, "com.xiaomi.discover"
/* .line 704 */
final String v7 = "com.xiaomi.gamecenter"; // const-string v7, "com.xiaomi.gamecenter"
/* .line 705 */
final String v8 = "com.xiaomi.gamecenter.pad"; // const-string v8, "com.xiaomi.gamecenter.pad"
/* .line 706 */
final String v8 = "com.miui.global.packageinstaller"; // const-string v8, "com.miui.global.packageinstaller"
/* .line 707 */
/* .line 708 */
final String v2 = "com.miui.greenguard"; // const-string v2, "com.miui.greenguard"
/* .line 709 */
final String v2 = "com.miui.cleaner"; // const-string v2, "com.miui.cleaner"
/* .line 710 */
final String v2 = "com.miui.cloudbackup"; // const-string v2, "com.miui.cloudbackup"
/* .line 711 */
/* sget-boolean v2, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 712 */
final String v2 = "de.telekom.tsc"; // const-string v2, "de.telekom.tsc"
/* .line 713 */
final String v2 = "com.sfr.android.sfrjeux"; // const-string v2, "com.sfr.android.sfrjeux"
/* .line 714 */
final String v2 = "com.altice.android.myapps"; // const-string v2, "com.altice.android.myapps"
/* .line 715 */
final String v2 = "jp_kd"; // const-string v2, "jp_kd"
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 716 */
final String v0 = "com.facebook.system"; // const-string v0, "com.facebook.system"
/* .line 868 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 870 */
final String v1 = "android.permission.SEND_SMS"; // const-string v1, "android.permission.SEND_SMS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 871 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.CALL_PHONE"; // const-string v1, "android.permission.CALL_PHONE"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 872 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.READ_CONTACTS"; // const-string v1, "android.permission.READ_CONTACTS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 873 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.WRITE_CONTACTS"; // const-string v1, "android.permission.WRITE_CONTACTS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 874 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.CLEAR_APP_USER_DATA"; // const-string v1, "android.permission.CLEAR_APP_USER_DATA"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 875 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.WRITE_SECURE_SETTINGS"; // const-string v1, "android.permission.WRITE_SECURE_SETTINGS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 876 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.WRITE_SETTINGS"; // const-string v1, "android.permission.WRITE_SETTINGS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 877 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.MANAGE_DEVICE_ADMINS"; // const-string v1, "android.permission.MANAGE_DEVICE_ADMINS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 878 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.UPDATE_APP_OPS_STATS"; // const-string v1, "android.permission.UPDATE_APP_OPS_STATS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 879 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.INJECT_EVENTS"; // const-string v1, "android.permission.INJECT_EVENTS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 880 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.INSTALL_GRANT_RUNTIME_PERMISSIONS"; // const-string v1, "android.permission.INSTALL_GRANT_RUNTIME_PERMISSIONS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 881 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.GRANT_RUNTIME_PERMISSIONS"; // const-string v1, "android.permission.GRANT_RUNTIME_PERMISSIONS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 882 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.REVOKE_RUNTIME_PERMISSIONS"; // const-string v1, "android.permission.REVOKE_RUNTIME_PERMISSIONS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 883 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
final String v1 = "android.permission.SET_PREFERRED_APPLICATIONS"; // const-string v1, "android.permission.SET_PREFERRED_APPLICATIONS"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 885 */
/* sget-boolean v0, Lmiui/os/Build;->IS_DEBUGGABLE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 886 */
final String v0 = "ro.secureboot.devicelock"; // const-string v0, "ro.secureboot.devicelock"
v0 = android.os.SystemProperties .getInt ( v0,v6 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 887 */
final String v0 = "ro.secureboot.lockstate"; // const-string v0, "ro.secureboot.lockstate"
android.os.SystemProperties .get ( v0 );
/* const-string/jumbo v1, "unlocked" */
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
} // :cond_1
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 889 */
} // :cond_2
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
(( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
/* .line 916 */
} // :cond_3
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 918 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 920 */
final String v1 = "android"; // const-string v1, "android"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 921 */
final String v1 = "com.android.provision"; // const-string v1, "com.android.provision"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 922 */
final String v1 = "com.miui.securitycore"; // const-string v1, "com.miui.securitycore"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 923 */
final String v1 = "com.android.vending"; // const-string v1, "com.android.vending"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 924 */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 925 */
(( java.util.ArrayList ) v0 ).add ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 926 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 927 */
final String v1 = "com.android.updater"; // const-string v1, "com.android.updater"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 928 */
final String v1 = "com.amazon.venezia"; // const-string v1, "com.amazon.venezia"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 930 */
/* sget-boolean v1, Lmiui/enterprise/EnterpriseManagerStub;->ENTERPRISE_ACTIVATED:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 931 */
final String v1 = "com.xiaomi.ep"; // const-string v1, "com.xiaomi.ep"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 934 */
} // :cond_4
/* sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 935 */
final String v1 = "com.miui.cotaservice"; // const-string v1, "com.miui.cotaservice"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 936 */
(( java.util.ArrayList ) v0 ).add ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 937 */
(( java.util.ArrayList ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 940 */
} // :cond_5
/* sget-boolean v1, Lmiui/os/Build;->IS_DEBUGGABLE:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 941 */
final String v1 = "com.android.server.pm.test.service.server"; // const-string v1, "com.android.server.pm.test.service.server"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1426 */
} // :cond_6
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public com.android.server.pm.PackageManagerServiceImpl ( ) {
/* .locals 2 */
/* .line 119 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceStub;-><init>()V */
/* .line 138 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mIgnoreApks = v0;
/* .line 139 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mIgnorePackages = v0;
/* .line 193 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->thirdAppCount:I */
/* .line 194 */
/* iput v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->systemAppCount:I */
/* .line 195 */
/* iput v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->persistAppCount:I */
/* .line 199 */
int v1 = 0; // const/4 v1, 0x0
this.mRsaFeature = v1;
/* .line 200 */
/* iput-boolean v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsGlobalCrbtSupported:Z */
/* .line 212 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCotaApps = v0;
/* .line 214 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mIsSupportAttention = v0;
return;
} // .end method
private static void addIgnoreApks ( java.lang.String p0, java.util.Set p1 ) {
/* .locals 8 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 444 */
/* .local p1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
miui.util.FeatureParser .getStringArray ( p0 );
/* .line 445 */
/* .local v0, "whiteList":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* array-length v1, v0 */
/* if-lez v1, :cond_1 */
/* .line 446 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_1 */
/* aget-object v4, v0, v3 */
/* .line 447 */
/* .local v4, "str":Ljava/lang/String; */
final String v5 = ","; // const-string v5, ","
android.text.TextUtils .split ( v4,v5 );
/* .line 448 */
/* .local v5, "item":[Ljava/lang/String; */
/* aget-object v7, v5, v2 */
v7 = java.lang.Integer .parseInt ( v7 );
/* if-gt v6, v7, :cond_0 */
/* .line 449 */
int v6 = 1; // const/4 v6, 0x1
/* aget-object v6, v5, v6 */
/* .line 446 */
} // .end local v4 # "str":Ljava/lang/String;
} // .end local v5 # "item":[Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 453 */
} // :cond_1
return;
} // .end method
private static void addPersistentPackages ( android.content.pm.ApplicationInfo p0, java.util.ArrayList p1 ) {
/* .locals 2 */
/* .param p0, "ai" # Landroid/content/pm/ApplicationInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/pm/ApplicationInfo;", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/pm/ApplicationInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1688 */
/* .local p1, "finalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;" */
final String v0 = "PKMSImpl"; // const-string v0, "PKMSImpl"
/* if-nez p0, :cond_0 */
/* .line 1689 */
final String v1 = "ai is null!"; // const-string v1, "ai is null!"
android.util.Slog .w ( v0,v1 );
/* .line 1690 */
return;
/* .line 1692 */
} // :cond_0
v1 = this.processName;
/* if-nez v1, :cond_1 */
/* .line 1693 */
final String v1 = "processName is null!"; // const-string v1, "processName is null!"
android.util.Slog .w ( v0,v1 );
/* .line 1694 */
return;
/* .line 1696 */
} // :cond_1
v0 = this.processName;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_2
/* :sswitch_0 */
final String v1 = "com.android.nfc"; // const-string v1, "com.android.nfc"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 0; // const/4 v0, 0x0
/* :sswitch_1 */
final String v1 = "com.miui.daemon"; // const-string v1, "com.miui.daemon"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_2 */
final String v1 = "com.goodix.fingerprint"; // const-string v1, "com.goodix.fingerprint"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
int v0 = 1; // const/4 v0, 0x1
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
/* packed-switch v0, :pswitch_data_0 */
/* .line 1700 */
/* :pswitch_0 */
(( java.util.ArrayList ) p1 ).add ( p0 ); // invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1701 */
/* nop */
/* .line 1705 */
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x75d739d1 -> :sswitch_2 */
/* -0x399a9feb -> :sswitch_1 */
/* -0x29760741 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static void checkAndClearResiduePermissions ( com.android.server.pm.permission.LegacyPermissionSettings p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p0, "settings" # Lcom/android/server/pm/permission/LegacyPermissionSettings; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "perName" # Ljava/lang/String; */
/* .line 1396 */
if ( p0 != null) { // if-eqz p0, :cond_2
/* .line 1397 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_2 */
v0 = android.text.TextUtils .isEmpty ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1400 */
} // :cond_0
(( com.android.server.pm.permission.LegacyPermissionSettings ) p0 ).getPermissions ( ); // invoke-virtual {p0}, Lcom/android/server/pm/permission/LegacyPermissionSettings;->getPermissions()Ljava/util/List;
/* .line 1401 */
v1 = /* .local v0, "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/permission/LegacyPermission;>;" */
/* if-lez v1, :cond_1 */
/* .line 1402 */
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
final String v2 = "find residue permission"; // const-string v2, "find residue permission"
android.util.Slog .i ( v1,v2 );
/* .line 1403 */
com.android.server.pm.PackageManagerServiceImpl .clearPermissions ( p1 );
/* .line 1405 */
} // :cond_1
return;
/* .line 1398 */
} // .end local v0 # "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/pm/permission/LegacyPermission;>;"
} // :cond_2
} // :goto_0
return;
} // .end method
private static void clearPermissions ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 1380 */
final String v0 = "PKMSImpl"; // const-string v0, "PKMSImpl"
v1 = v1 = com.android.server.pm.PackageManagerServiceImpl.sClearPermissionSet;
/* if-nez v1, :cond_0 */
/* .line 1381 */
return;
/* .line 1384 */
} // :cond_0
try { // :try_start_0
final String v1 = "permissionmgr"; // const-string v1, "permissionmgr"
android.os.ServiceManager .getService ( v1 );
/* check-cast v1, Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 1385 */
/* .local v1, "pms":Lcom/android/server/pm/permission/PermissionManagerService; */
final String v2 = "mPermissionManagerServiceImpl"; // const-string v2, "mPermissionManagerServiceImpl"
/* const-class v3, Lcom/android/server/pm/permission/PermissionManagerServiceImpl; */
miui.util.ReflectionUtils .getObjectField ( v1,v2,v3 );
/* check-cast v2, Lcom/android/server/pm/permission/PermissionManagerServiceImpl; */
/* .line 1387 */
/* .local v2, "impl":Lcom/android/server/pm/permission/PermissionManagerServiceImpl; */
/* const-string/jumbo v3, "updatePermissions" */
/* const-class v4, Ljava/lang/Void; */
int v5 = 0; // const/4 v5, 0x0
/* filled-new-array {p0, v5}, [Ljava/lang/Object; */
miui.util.ReflectionUtils .callMethod ( v2,v3,v4,v5 );
/* .line 1388 */
final String v3 = "clear residue permission finish"; // const-string v3, "clear residue permission finish"
android.util.Slog .i ( v0,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1391 */
/* nop */
} // .end local v1 # "pms":Lcom/android/server/pm/permission/PermissionManagerService;
} // .end local v2 # "impl":Lcom/android/server/pm/permission/PermissionManagerServiceImpl;
/* .line 1389 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1390 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "clear residue permission error"; // const-string v3, "clear residue permission error"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).getLocalizedMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 1392 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void disableSystemApp ( Integer p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "userId" # I */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .line 1535 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Disable "; // const-string v1, "Disable "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " for user "; // const-string v1, " for user "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 4; // const/4 v1, 0x4
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v1,v0 );
/* .line 1537 */
try { // :try_start_0
v2 = this.mPM;
int v4 = 3; // const/4 v4, 0x3
int v5 = 0; // const/4 v5, 0x0
final String v7 = "COTA"; // const-string v7, "COTA"
/* move-object v3, p2 */
/* move v6, p1 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V */
/* .line 1539 */
v0 = this.mPms;
v0 = this.mLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1540 */
try { // :try_start_1
v1 = this.mPms;
v1 = this.mSettings;
(( com.android.server.pm.Settings ) v1 ).getPackageLPr ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
/* .line 1541 */
/* .local v1, "pkgSetting":Lcom/android/server/pm/PackageSetting; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1542 */
final String v2 = "cota-disabled"; // const-string v2, "cota-disabled"
/* invoke-direct {p0, v1, v2, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->updatedefaultState(Lcom/android/server/pm/PackageSetting;Ljava/lang/String;I)V */
/* .line 1543 */
v2 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v2 ).scheduleWritePackageRestrictions ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/pm/PackageManagerService;->scheduleWritePackageRestrictions(I)V
/* .line 1545 */
} // .end local v1 # "pkgSetting":Lcom/android/server/pm/PackageSetting;
} // :cond_0
/* monitor-exit v0 */
/* .line 1548 */
/* .line 1545 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
} // .end local p1 # "userId":I
} // .end local p2 # "pkg":Ljava/lang/String;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1546 */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageManagerServiceImpl; */
/* .restart local p1 # "userId":I */
/* .restart local p2 # "pkg":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* .line 1547 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to disable "; // const-string v2, "Failed to disable "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", msg="; // const-string v2, ", msg="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 6; // const/4 v2, 0x6
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v2,v1 );
/* .line 1549 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void doUpdateSystemAppDefaultUserStateForAllUsers ( ) {
/* .locals 5 */
/* .line 1628 */
/* const-class v0, Lcom/android/server/pm/UserManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/UserManagerInternal; */
/* .line 1629 */
/* .local v0, "mUserManager":Lcom/android/server/pm/UserManagerInternal; */
(( com.android.server.pm.UserManagerInternal ) v0 ).getUserIds ( ); // invoke-virtual {v0}, Lcom/android/server/pm/UserManagerInternal;->getUserIds()[I
/* .line 1630 */
/* .local v1, "allUsers":[I */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_0 */
/* aget v4, v1, v3 */
/* .line 1631 */
/* .local v4, "userId":I */
(( com.android.server.pm.PackageManagerServiceImpl ) p0 ).updateSystemAppDefaultStateForUser ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/pm/PackageManagerServiceImpl;->updateSystemAppDefaultStateForUser(I)V
/* .line 1630 */
} // .end local v4 # "userId":I
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1633 */
} // :cond_0
return;
} // .end method
private void enableSystemApp ( Integer p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "userId" # I */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .line 1552 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Enable "; // const-string v1, "Enable "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " for user "; // const-string v1, " for user "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 4; // const/4 v1, 0x4
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v1,v0 );
/* .line 1554 */
try { // :try_start_0
v2 = this.mPM;
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
final String v7 = "COTA"; // const-string v7, "COTA"
/* move-object v3, p2 */
/* move v6, p1 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V */
/* .line 1556 */
v0 = this.mPms;
v0 = this.mLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1557 */
try { // :try_start_1
v1 = this.mPms;
v1 = this.mSettings;
(( com.android.server.pm.Settings ) v1 ).getPackageLPr ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
/* .line 1558 */
/* .local v1, "pkgSetting":Lcom/android/server/pm/PackageSetting; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1559 */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v1, v2, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->updatedefaultState(Lcom/android/server/pm/PackageSetting;Ljava/lang/String;I)V */
/* .line 1560 */
v2 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v2 ).scheduleWritePackageRestrictions ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/pm/PackageManagerService;->scheduleWritePackageRestrictions(I)V
/* .line 1562 */
} // .end local v1 # "pkgSetting":Lcom/android/server/pm/PackageSetting;
} // :cond_0
/* monitor-exit v0 */
/* .line 1565 */
/* .line 1562 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
} // .end local p1 # "userId":I
} // .end local p2 # "pkg":Ljava/lang/String;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1563 */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageManagerServiceImpl; */
/* .restart local p1 # "userId":I */
/* .restart local p2 # "pkg":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v0 */
/* .line 1564 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to enable "; // const-string v2, "Failed to enable "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", msg="; // const-string v2, ", msg="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 6; // const/4 v2, 0x6
com.android.server.pm.PackageManagerServiceUtils .logCriticalInfo ( v2,v1 );
/* .line 1566 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void fatalIf ( Boolean p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 2 */
/* .param p0, "condition" # Z */
/* .param p1, "err" # I */
/* .param p2, "msg" # Ljava/lang/String; */
/* .param p3, "logPrefix" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/pm/PackageManagerException; */
/* } */
} // .end annotation
/* .line 992 */
/* if-nez p0, :cond_0 */
/* .line 996 */
return;
/* .line 993 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", msg="; // const-string v1, ", msg="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
android.util.Slog .e ( v1,v0 );
/* .line 994 */
/* new-instance v0, Lcom/android/server/pm/PackageManagerException; */
/* invoke-direct {v0, p1, p2}, Lcom/android/server/pm/PackageManagerException;-><init>(ILjava/lang/String;)V */
/* throw v0 */
} // .end method
public static com.android.server.pm.PackageManagerServiceImpl get ( ) {
/* .locals 1 */
/* .line 217 */
com.android.server.pm.PackageManagerServiceStub .get ( );
/* check-cast v0, Lcom/android/server/pm/PackageManagerServiceImpl; */
} // .end method
public static android.os.Handler getFirstUseHandler ( ) {
/* .locals 3 */
/* .line 1429 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sFirstUseHandler;
/* if-nez v0, :cond_1 */
/* .line 1430 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sFirstUseLock;
/* monitor-enter v0 */
/* .line 1431 */
try { // :try_start_0
v1 = com.android.server.pm.PackageManagerServiceImpl.sFirstUseHandler;
/* if-nez v1, :cond_0 */
/* .line 1432 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "first_use_thread"; // const-string v2, "first_use_thread"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 1433 */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 1434 */
/* new-instance v1, Landroid/os/Handler; */
v2 = com.android.server.pm.PackageManagerServiceImpl.sFirstUseThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 1436 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1438 */
} // :cond_1
} // :goto_0
v0 = com.android.server.pm.PackageManagerServiceImpl.sFirstUseHandler;
} // .end method
private java.lang.String getdatedefaultState ( com.android.server.pm.PackageSetting p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .param p2, "userId" # I */
/* .line 1602 */
(( com.android.server.pm.PackageSetting ) p1 ).readUserState ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/pm/PackageSetting;->readUserState(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;
} // .end method
private void handleFirstUseActivity ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1442 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1443 */
/* .local v0, "isDeviceProvisioned":Z */
v1 = this.mContext;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1445 */
try { // :try_start_0
v1 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isProvisioned()Z */
/* :try_end_0 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 1448 */
/* .line 1446 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1447 */
/* .local v1, "e":Ljava/lang/IllegalStateException; */
int v0 = 0; // const/4 v0, 0x0
/* .line 1450 */
} // .end local v1 # "e":Ljava/lang/IllegalStateException;
} // :cond_0
} // :goto_0
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
/* if-nez v0, :cond_1 */
/* .line 1451 */
final String v2 = "Skip dexopt, device is not provisioned."; // const-string v2, "Skip dexopt, device is not provisioned."
android.util.Slog .w ( v1,v2 );
/* .line 1452 */
return;
/* .line 1454 */
} // :cond_1
final String v2 = "android"; // const-string v2, "android"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1455 */
final String v2 = "Skip dexopt, cannot dexopt the system server."; // const-string v2, "Skip dexopt, cannot dexopt the system server."
android.util.Slog .w ( v1,v2 );
/* .line 1456 */
return;
/* .line 1458 */
} // :cond_2
/* const/16 v2, 0x205 */
/* .line 1461 */
/* .local v2, "flags":I */
v3 = this.mDexOptHelper;
/* new-instance v4, Lcom/android/server/pm/dex/DexoptOptions; */
/* const/16 v5, 0xe */
/* invoke-direct {v4, p1, v5, v2}, Lcom/android/server/pm/dex/DexoptOptions;-><init>(Ljava/lang/String;II)V */
v3 = (( com.android.server.pm.DexOptHelper ) v3 ).performDexOpt ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/pm/DexOptHelper;->performDexOpt(Lcom/android/server/pm/dex/DexoptOptions;)Z
/* .line 1462 */
/* .local v3, "success":Z */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "FirstUseActivity packageName = "; // const-string v5, "FirstUseActivity packageName = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " success = "; // const-string v5, " success = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 1463 */
/* const-class v4, Lcom/android/server/PinnerService; */
com.android.server.LocalServices .getService ( v4 );
/* check-cast v4, Lcom/android/server/PinnerService; */
/* .line 1464 */
/* .local v4, "pinnerService":Lcom/android/server/PinnerService; */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1465 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "FirstUseActivity Pinning optimized code "; // const-string v6, "FirstUseActivity Pinning optimized code "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v5 );
/* .line 1466 */
/* new-instance v1, Landroid/util/ArraySet; */
/* invoke-direct {v1}, Landroid/util/ArraySet;-><init>()V */
/* .line 1467 */
/* .local v1, "packages":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;" */
(( android.util.ArraySet ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 1468 */
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.PinnerService ) v4 ).update ( v1, v5 ); // invoke-virtual {v4, v1, v5}, Lcom/android/server/PinnerService;->update(Landroid/util/ArraySet;Z)V
/* .line 1470 */
} // .end local v1 # "packages":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
} // :cond_3
return;
} // .end method
private Boolean hasGoogleContacts ( ) {
/* .locals 2 */
/* .line 1865 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/app/GoogleContacts"; // const-string v1, "/product/app/GoogleContacts"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
} // .end method
private Boolean hasXiaomiContacts ( ) {
/* .locals 3 */
/* .line 1854 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/priv-app/MIUIContactsT"; // const-string v1, "/product/priv-app/MIUIContactsT"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_1 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/priv-app/MIUIContactsFold"; // const-string v1, "/product/priv-app/MIUIContactsFold"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1855 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_1 */
/* new-instance v0, Ljava/io/File; */
final String v2 = "/product/priv-app/MIUIContactsCetus"; // const-string v2, "/product/priv-app/MIUIContactsCetus"
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1856 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_1 */
/* new-instance v0, Ljava/io/File; */
final String v2 = "/product/priv-app/MIUIContactsTGlobal"; // const-string v2, "/product/priv-app/MIUIContactsTGlobal"
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1857 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_1 */
/* new-instance v0, Ljava/io/File; */
final String v2 = "/product/priv-app/MIUIContactsPadU"; // const-string v2, "/product/priv-app/MIUIContactsPadU"
/* invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1858 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_1 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1859 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_1 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/priv-app/MIUIContactsUGlobal"; // const-string v1, "/product/priv-app/MIUIContactsUGlobal"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1860 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v0, :cond_1 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/priv-app/MIUIContactsU"; // const-string v1, "/product/priv-app/MIUIContactsU"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1861 */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 1854 */
} // :goto_1
} // .end method
protected static void initAllowPackageList ( android.content.Context p0 ) {
/* .locals 7 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 951 */
final String v0 = "add "; // const-string v0, "add "
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
v2 = com.android.server.pm.PackageManagerServiceImpl.sAllowPackage;
(( java.util.ArrayList ) v2 ).clear ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
/* .line 953 */
try { // :try_start_0
(( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x1103000b */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 954 */
/* .local v3, "stringArray":[Ljava/lang/String; */
java.util.Arrays .asList ( v3 );
(( java.util.ArrayList ) v2 ).addAll ( v4 ); // invoke-virtual {v2, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 955 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v4, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " common packages into sAllowPackage list"; // const-string v4, " common packages into sAllowPackage list"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 956 */
/* array-length v2, v3 */
int v4 = 0; // const/4 v4, 0x0
/* move v5, v4 */
} // :goto_0
/* if-ge v5, v2, :cond_0 */
/* aget-object v6, v3, v5 */
/* .line 957 */
/* .local v6, "pkg":Ljava/lang/String; */
android.util.Slog .i ( v1,v6 );
/* .line 956 */
/* nop */
} // .end local v6 # "pkg":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 960 */
} // :cond_0
/* sget-boolean v2, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v2, :cond_1 */
/* .line 961 */
return;
/* .line 963 */
} // :cond_1
(( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v5, 0x1103000c */
(( android.content.res.Resources ) v2 ).getStringArray ( v5 ); // invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 964 */
} // .end local v3 # "stringArray":[Ljava/lang/String;
/* .local v2, "stringArray":[Ljava/lang/String; */
v3 = com.android.server.pm.PackageManagerServiceImpl.sAllowPackage;
java.util.Arrays .asList ( v2 );
(( java.util.ArrayList ) v3 ).addAll ( v5 ); // invoke-virtual {v3, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 965 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v5, v2 */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " international package into sAllowPackage list"; // const-string v5, " international package into sAllowPackage list"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* .line 966 */
/* array-length v3, v2 */
/* move v5, v4 */
} // :goto_1
/* if-ge v5, v3, :cond_2 */
/* aget-object v6, v2, v5 */
/* .line 967 */
/* .restart local v6 # "pkg":Ljava/lang/String; */
android.util.Slog .i ( v1,v6 );
/* .line 966 */
/* nop */
} // .end local v6 # "pkg":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 970 */
} // :cond_2
(( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v5, 0x1103000d */
(( android.content.res.Resources ) v3 ).getStringArray ( v5 ); // invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* move-object v2, v3 */
/* .line 971 */
v3 = com.android.server.pm.PackageManagerServiceImpl.sAllowPackage;
java.util.Arrays .asList ( v2 );
(( java.util.ArrayList ) v3 ).addAll ( v5 ); // invoke-virtual {v3, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 972 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v5, v2 */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " operator packages into sAllowPackage list"; // const-string v5, " operator packages into sAllowPackage list"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* .line 973 */
/* array-length v3, v2 */
} // :goto_2
/* if-ge v4, v3, :cond_3 */
/* aget-object v5, v2, v4 */
/* .line 974 */
/* .local v5, "pkg":Ljava/lang/String; */
android.util.Slog .i ( v1,v5 );
/* .line 973 */
/* nop */
} // .end local v5 # "pkg":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 977 */
} // :cond_3
/* sget-boolean v3, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z */
if ( v3 != null) { // if-eqz v3, :cond_4
/* sget-boolean v3, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsBootLoaderLocked:Z */
/* if-nez v3, :cond_5 */
/* .line 978 */
} // :cond_4
(( android.content.Context ) p0 ).getResources ( ); // invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v4, 0x1103000e */
(( android.content.res.Resources ) v3 ).getStringArray ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* move-object v2, v3 */
/* .line 979 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v3, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " unlocked packages into sAllowPackage list"; // const-string v3, " unlocked packages into sAllowPackage list"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 980 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sAllowPackage;
java.util.Arrays .asList ( v2 );
(( java.util.ArrayList ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* :try_end_0 */
/* .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 984 */
} // .end local v2 # "stringArray":[Ljava/lang/String;
} // :cond_5
/* .line 982 */
/* :catch_0 */
/* move-exception v0 */
/* .line 983 */
/* .local v0, "e":Landroid/content/res/Resources$NotFoundException; */
(( android.content.res.Resources$NotFoundException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V
/* .line 985 */
} // .end local v0 # "e":Landroid/content/res/Resources$NotFoundException;
} // :goto_3
return;
} // .end method
private void initCotaApps ( ) {
/* .locals 10 */
/* .line 1959 */
final String v0 = "PKMSImpl"; // const-string v0, "PKMSImpl"
int v1 = 0; // const/4 v1, 0x0
/* .line 1961 */
/* .local v1, "inputStream":Ljava/io/InputStream; */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileInputStream; */
final String v3 = "/product/opcust/common/carrier_sys_apps_list.xml"; // const-string v3, "/product/opcust/common/carrier_sys_apps_list.xml"
/* invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* move-object v1, v2 */
/* .line 1962 */
org.xmlpull.v1.XmlPullParserFactory .newInstance ( );
/* .line 1963 */
/* .local v2, "factory":Lorg/xmlpull/v1/XmlPullParserFactory; */
(( org.xmlpull.v1.XmlPullParserFactory ) v2 ).newPullParser ( ); // invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
/* .line 1964 */
/* .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser; */
final String v4 = "UTF-8"; // const-string v4, "UTF-8"
v4 = /* .line 1965 */
/* .line 1966 */
/* .local v4, "event":I */
} // :goto_0
int v5 = 1; // const/4 v5, 0x1
/* if-eq v4, v5, :cond_3 */
/* .line 1967 */
/* packed-switch v4, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 1987 */
/* :pswitch_1 */
/* .line 1971 */
/* :pswitch_2 */
/* .line 1972 */
/* .local v5, "name":Ljava/lang/String; */
final String v6 = "package"; // const-string v6, "package"
v6 = (( java.lang.String ) v6 ).equals ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 1973 */
final String v6 = "name"; // const-string v6, "name"
int v7 = 0; // const/4 v7, 0x0
/* .line 1974 */
/* .local v6, "pkgName":Ljava/lang/String; */
final String v8 = "carrier"; // const-string v8, "carrier"
/* .line 1975 */
/* .local v7, "carrier":Ljava/lang/String; */
v8 = android.text.TextUtils .isEmpty ( v6 );
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 1976 */
final String v8 = "initCotaApps pkgName is null, skip parse this tag"; // const-string v8, "initCotaApps pkgName is null, skip parse this tag"
android.util.Slog .e ( v0,v8 );
/* .line 1977 */
/* .line 1979 */
} // :cond_0
v8 = android.text.TextUtils .isEmpty ( v7 );
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 1980 */
final String v8 = "initCotaApps carrier is null, skip parse this tag"; // const-string v8, "initCotaApps carrier is null, skip parse this tag"
android.util.Slog .e ( v0,v8 );
/* .line 1981 */
/* .line 1983 */
} // :cond_1
v8 = this.mCotaApps;
(( java.lang.String ) v7 ).toLowerCase ( ); // invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
(( java.util.HashMap ) v8 ).put ( v6, v9 ); // invoke-virtual {v8, v6, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1984 */
/* nop */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // .end local v7 # "carrier":Ljava/lang/String;
/* .line 1969 */
} // .end local v5 # "name":Ljava/lang/String;
/* :pswitch_3 */
/* nop */
/* .line 1991 */
} // :cond_2
v5 = } // :goto_1
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v4, v5 */
/* .line 1997 */
} // .end local v2 # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
} // .end local v3 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v4 # "event":I
} // :cond_3
/* nop */
} // :goto_2
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 1998 */
/* .line 1997 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 1994 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1995 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "initCotaApps fail: "; // const-string v4, "initCotaApps fail: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).getMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1997 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
/* .line 1999 */
} // :goto_3
return;
/* .line 1997 */
} // :goto_4
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 1998 */
/* throw v0 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private Boolean isAllowedToGetInstalledApps ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p1, "callingUid" # I */
/* .param p2, "callingPackage" # Ljava/lang/String; */
/* .param p3, "where" # Ljava/lang/String; */
/* .line 1259 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1260 */
/* .line 1263 */
} // :cond_0
v0 = android.os.UserHandle .getAppId ( p1 );
/* .line 1264 */
/* .local v0, "callingAppId":I */
/* const/16 v2, 0x2710 */
/* if-ge v0, v2, :cond_1 */
/* .line 1265 */
/* .line 1268 */
} // :cond_1
v2 = android.text.TextUtils .isEmpty ( p2 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1269 */
/* .line 1272 */
} // :cond_2
/* sget-boolean v2, Lmiui/os/Build;->IS_DEBUGGABLE:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
final String v2 = "com.android.server.pm.test.service.server"; // const-string v2, "com.android.server.pm.test.service.server"
v2 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1273 */
/* .line 1276 */
} // :cond_3
android.app.ActivityThread .currentApplication ( );
/* const-class v3, Landroid/app/AppOpsManager; */
(( android.app.Application ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/app/Application;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v2, Landroid/app/AppOpsManager; */
/* .line 1277 */
/* .local v2, "appOpsManager":Landroid/app/AppOpsManager; */
/* const/16 v4, 0x2726 */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, v2 */
/* move v5, p1 */
/* move-object v6, p2 */
v3 = /* invoke-virtual/range {v3 ..v8}, Landroid/app/AppOpsManager;->noteOpNoThrow(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1278 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MIUILOG- Permission Denied "; // const-string v3, "MIUILOG- Permission Denied "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ".pkg : "; // const-string v3, ".pkg : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " uid : "; // const-string v3, " uid : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "PKMSImpl"; // const-string v3, "PKMSImpl"
android.util.Slog .e ( v3,v1 );
/* .line 1279 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1281 */
} // :cond_4
} // .end method
public static Boolean isCTS ( ) {
/* .locals 1 */
/* .line 1408 */
v0 = android.miui.AppOpsUtils .isXOptMode ( );
} // .end method
private Boolean isProvisioned ( ) {
/* .locals 3 */
/* .line 305 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
} // .end method
private Boolean isRsa4 ( ) {
/* .locals 2 */
/* .line 1177 */
v0 = this.mRsaFeature;
v0 = android.text.TextUtils .isEmpty ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1178 */
final String v0 = "ro.com.miui.rsa.feature"; // const-string v0, "ro.com.miui.rsa.feature"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
this.mRsaFeature = v0;
/* .line 1180 */
} // :cond_0
v0 = this.mRsaFeature;
v0 = android.text.TextUtils .isEmpty ( v0 );
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
private static Boolean isSecondUserlocked ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 1095 */
v0 = com.android.server.pm.PackageManagerServiceImpl .isCTS ( );
/* .line 1096 */
/* .local v0, "iscts":Z */
v1 = com.android.server.pm.PmInjector .getDefaultUserId ( );
/* .line 1097 */
/* .local v1, "userid":I */
/* const-string/jumbo v2, "user" */
(( android.content.Context ) p0 ).getSystemService ( v2 ); // invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/os/UserManager; */
/* .line 1098 */
/* .local v2, "userManager":Landroid/os/UserManager; */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( v1 != null) { // if-eqz v1, :cond_0
v3 = (( android.os.UserManager ) v2 ).isUserUnlocked ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/UserManager;->isUserUnlocked(I)Z
/* if-nez v3, :cond_0 */
/* .line 1099 */
int v3 = 1; // const/4 v3, 0x1
/* .line 1101 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
} // .end method
static Boolean isTrustedEnterpriseInstaller ( android.content.Context p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "callingUid" # I */
/* .param p2, "installerPkg" # Ljava/lang/String; */
/* .line 586 */
/* sget-boolean v0, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 587 */
/* .line 589 */
} // :cond_0
v0 = android.os.UserHandle .getUserId ( p1 );
v0 = com.miui.enterprise.ApplicationHelper .isTrustedAppStoresEnabled ( p0,v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = com.android.server.pm.PackageManagerServiceImpl.EP_INSTALLER_PKG_WHITELIST;
/* .line 590 */
v0 = com.android.internal.util.ArrayUtils .contains ( v0,p2 );
/* if-nez v0, :cond_1 */
/* .line 592 */
v0 = android.os.UserHandle .getUserId ( p1 );
/* .line 591 */
com.miui.enterprise.ApplicationHelper .getTrustedAppStores ( p0,v0 );
v0 = /* .line 592 */
/* if-nez v0, :cond_1 */
/* .line 593 */
int v0 = 0; // const/4 v0, 0x0
/* .line 595 */
} // :cond_1
} // .end method
private java.util.List lambda$getApplicationInfoBySelf$6 ( com.android.server.pm.pkg.AndroidPackage p0, Long p1, Integer p2 ) { //synthethic
/* .locals 2 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "flags" # J */
/* .param p4, "userId" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 1769 */
v0 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 1770 */
/* .line 1771 */
/* .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1772 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1773 */
/* .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;" */
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1774 */
/* .line 1776 */
} // .end local v1 # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;"
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private java.util.List lambda$getPackageInfoBySelf$5 ( com.android.server.pm.pkg.AndroidPackage p0, Long p1, Integer p2 ) { //synthethic
/* .locals 2 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "flags" # J */
/* .param p4, "userId" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 1749 */
v0 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 1750 */
/* .line 1751 */
/* .local v0, "packageInfo":Landroid/content/pm/PackageInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1752 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1753 */
/* .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/PackageInfo;>;" */
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1754 */
/* .line 1756 */
} // .end local v1 # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/PackageInfo;>;"
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
static void lambda$getPersistentAppsForOtherUser$4 ( Boolean p0, Integer p1, Integer p2, java.util.ArrayList p3, com.android.server.pm.pkg.PackageStateInternal p4 ) { //synthethic
/* .locals 7 */
/* .param p0, "safeMode" # Z */
/* .param p1, "flags" # I */
/* .param p2, "userId" # I */
/* .param p3, "finalList" # Ljava/util/ArrayList; */
/* .param p4, "packageState" # Lcom/android/server/pm/pkg/PackageStateInternal; */
v0 = /* .line 1668 */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = if ( p0 != null) { // if-eqz p0, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1669 */
} // :cond_0
/* int-to-long v2, p1 */
/* .line 1670 */
/* .line 1669 */
/* move v5, p2 */
/* move-object v6, p4 */
/* invoke-static/range {v1 ..v6}, Lcom/android/server/pm/parsing/PackageInfoUtils;->generateApplicationInfo(Lcom/android/server/pm/pkg/AndroidPackage;JLcom/android/server/pm/pkg/PackageUserStateInternal;ILcom/android/server/pm/pkg/PackageStateInternal;)Landroid/content/pm/ApplicationInfo; */
/* .line 1671 */
/* .local v0, "ai":Landroid/content/pm/ApplicationInfo; */
/* packed-switch p2, :pswitch_data_0 */
/* .line 1674 */
/* :pswitch_0 */
com.android.server.pm.PackageManagerServiceImpl .addPersistentPackages ( v0,p3 );
/* .line 1675 */
/* nop */
/* .line 1682 */
} // .end local v0 # "ai":Landroid/content/pm/ApplicationInfo;
} // :cond_1
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x6e */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static com.android.server.pm.PackageEventRecorder$RecorderHandler lambda$init$0 ( com.android.server.pm.PackageEventRecorder p0 ) { //synthethic
/* .locals 2 */
/* .param p0, "recorder" # Lcom/android/server/pm/PackageEventRecorder; */
/* .line 241 */
/* new-instance v0, Lcom/android/server/pm/PackageEventRecorder$RecorderHandler; */
/* .line 242 */
com.android.server.IoThread .get ( );
(( com.android.server.IoThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/IoThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/pm/PackageEventRecorder$RecorderHandler;-><init>(Lcom/android/server/pm/PackageEventRecorder;Landroid/os/Looper;)V */
/* .line 241 */
} // .end method
static void lambda$notifyGlobalPackageInstaller$3 ( java.lang.String p0, android.content.Context p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "callingPackage" # Ljava/lang/String; */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 1087 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.global.packageinstaller.action.verifypackage"; // const-string v1, "com.miui.global.packageinstaller.action.verifypackage"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1088 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "installing"; // const-string v1, "installing"
(( android.content.Intent ) v0 ).putExtra ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1089 */
final String v1 = "com.miui.securitycenter.permission.GLOBAL_PACKAGEINSTALLER"; // const-string v1, "com.miui.securitycenter.permission.GLOBAL_PACKAGEINSTALLER"
(( android.content.Context ) p1 ).sendBroadcast ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
/* .line 1090 */
return;
} // .end method
static void lambda$protectAppFromDeleting$1 ( java.lang.String p0, Integer p1, Integer p2, Boolean p3, android.content.pm.IPackageDeleteObserver2 p4 ) { //synthethic
/* .locals 2 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "callingUid" # I */
/* .param p2, "userId" # I */
/* .param p3, "deleteSystem" # Z */
/* .param p4, "observer" # Landroid/content/pm/IPackageDeleteObserver2; */
/* .line 801 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Can\'t uninstall pkg: "; // const-string v1, "Can\'t uninstall pkg: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " from uid: "; // const-string v1, " from uid: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " with user: "; // const-string v1, " with user: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " deleteSystem: "; // const-string v1, " deleteSystem: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " reason: shell preinstall"; // const-string v1, " reason: shell preinstall"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
android.util.Slog .d ( v1,v0 );
/* .line 805 */
/* const/16 v0, -0x3e8 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 808 */
/* .line 807 */
/* :catch_0 */
/* move-exception v0 */
/* .line 809 */
} // :goto_0
return;
} // .end method
static void lambda$protectAppFromDeleting$2 ( android.content.pm.IPackageDeleteObserver2 p0, java.lang.String p1 ) { //synthethic
/* .locals 2 */
/* .param p0, "observer" # Landroid/content/pm/IPackageDeleteObserver2; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 823 */
/* const/16 v0, -0x3e8 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 826 */
/* .line 824 */
/* :catch_0 */
/* move-exception v0 */
/* .line 827 */
} // :goto_0
return;
} // .end method
static void lambda$recordPackageActivate$7 ( java.lang.String p0, Integer p1, java.lang.String p2 ) { //synthethic
/* .locals 1 */
/* .param p0, "activatedPkg" # Ljava/lang/String; */
/* .param p1, "userId" # I */
/* .param p2, "sourcePkg" # Ljava/lang/String; */
/* .line 1801 */
/* const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
/* .line 1802 */
/* .line 1801 */
return;
} // .end method
private static void notifyGlobalPackageInstaller ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "callingPackage" # Ljava/lang/String; */
/* .line 1084 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "com.android.vending"; // const-string v0, "com.android.vending"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 1085 */
final String v0 = "com.google.android.packageinstaller"; // const-string v0, "com.google.android.packageinstaller"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1086 */
} // :cond_0
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v1, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1, p0}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Landroid/content/Context;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1092 */
} // :cond_1
return;
} // .end method
private void readIgnoreApks ( ) {
/* .locals 12 */
/* .line 456 */
miui.os.Build .getCustVariant ( );
/* .line 457 */
/* .local v0, "custVariant":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 458 */
return;
/* .line 460 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 462 */
/* .local v1, "inputStream":Ljava/io/InputStream; */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileInputStream; */
/* new-instance v3, Ljava/io/File; */
final String v4 = "/system/etc/install_app_filter.xml"; // const-string v4, "/system/etc/install_app_filter.xml"
/* invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v1, v2 */
/* .line 463 */
org.xmlpull.v1.XmlPullParserFactory .newInstance ( );
/* .line 464 */
/* .local v2, "factory":Lorg/xmlpull/v1/XmlPullParserFactory; */
(( org.xmlpull.v1.XmlPullParserFactory ) v2 ).newPullParser ( ); // invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
/* .line 465 */
/* .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser; */
final String v4 = "UTF-8"; // const-string v4, "UTF-8"
/* .line 466 */
int v4 = 0; // const/4 v4, 0x0
/* .line 467 */
/* .local v4, "tagName":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
/* .line 468 */
/* .local v5, "appPath":Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .line 469 */
v7 = /* .local v6, "is_add_apps":Z */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 470 */
/* .local v7, "type":I */
} // :goto_0
int v8 = 1; // const/4 v8, 0x1
/* if-eq v8, v7, :cond_c */
/* .line 471 */
final String v8 = "ignore_apps"; // const-string v8, "ignore_apps"
final String v9 = "add_apps"; // const-string v9, "add_apps"
/* packed-switch v7, :pswitch_data_0 */
/* goto/16 :goto_4 */
/* .line 500 */
/* :pswitch_0 */
try { // :try_start_1
/* .line 501 */
/* .local v10, "end_tag_name":Ljava/lang/String; */
v9 = (( java.lang.String ) v9 ).equals ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 502 */
int v6 = 0; // const/4 v6, 0x0
/* goto/16 :goto_4 */
/* .line 503 */
} // :cond_1
v8 = (( java.lang.String ) v8 ).equals ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_b
/* .line 504 */
int v6 = 1; // const/4 v6, 0x1
/* goto/16 :goto_4 */
/* .line 473 */
} // .end local v10 # "end_tag_name":Ljava/lang/String;
/* :pswitch_1 */
/* move-object v4, v10 */
v10 = /* .line 474 */
/* if-lez v10, :cond_2 */
/* .line 475 */
int v10 = 0; // const/4 v10, 0x0
/* move-object v5, v10 */
/* .line 477 */
} // :cond_2
v9 = (( java.lang.String ) v9 ).equals ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 478 */
int v6 = 1; // const/4 v6, 0x1
/* .line 479 */
} // :cond_3
v8 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 480 */
int v6 = 0; // const/4 v6, 0x0
/* .line 481 */
} // :cond_4
final String v8 = "app"; // const-string v8, "app"
v8 = (( java.lang.String ) v8 ).equals ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_b
/* .line 482 */
final String v9 = " "; // const-string v9, " "
(( java.lang.String ) v8 ).split ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 483 */
/* .local v8, "ss":[Ljava/lang/String; */
int v9 = 0; // const/4 v9, 0x0
/* .line 484 */
/* .local v9, "is_current_cust":Z */
int v10 = 0; // const/4 v10, 0x0
/* .local v10, "i":I */
} // :goto_1
/* array-length v11, v8 */
/* if-ge v10, v11, :cond_6 */
/* .line 485 */
/* aget-object v11, v8, v10 */
v11 = (( java.lang.String ) v11 ).equals ( v0 ); // invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v11 != null) { // if-eqz v11, :cond_5
/* .line 486 */
int v9 = 1; // const/4 v9, 0x1
/* .line 487 */
/* .line 484 */
} // :cond_5
/* add-int/lit8 v10, v10, 0x1 */
/* .line 490 */
} // .end local v10 # "i":I
} // :cond_6
} // :goto_2
if ( v6 != null) { // if-eqz v6, :cond_7
if ( v9 != null) { // if-eqz v9, :cond_8
} // :cond_7
/* if-nez v6, :cond_9 */
if ( v9 != null) { // if-eqz v9, :cond_9
/* .line 491 */
} // :cond_8
v10 = this.mIgnoreApks;
/* .line 492 */
} // :cond_9
if ( v6 != null) { // if-eqz v6, :cond_a
if ( v9 != null) { // if-eqz v9, :cond_a
/* .line 493 */
v10 = v10 = this.mIgnoreApks;
if ( v10 != null) { // if-eqz v10, :cond_a
/* .line 494 */
v10 = this.mIgnoreApks;
/* .line 497 */
} // .end local v8 # "ss":[Ljava/lang/String;
} // .end local v9 # "is_current_cust":Z
} // :cond_a
} // :goto_3
/* nop */
/* .line 510 */
} // :cond_b
v8 = } // :goto_4
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move v7, v8 */
/* goto/16 :goto_0 */
/* .line 470 */
} // .end local v2 # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
} // .end local v3 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v4 # "tagName":Ljava/lang/String;
} // .end local v5 # "appPath":Ljava/lang/String;
} // .end local v6 # "is_add_apps":Z
} // .end local v7 # "type":I
} // :cond_c
/* .line 516 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 513 */
/* :catch_0 */
/* move-exception v2 */
/* .line 514 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 516 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_5
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 517 */
/* nop */
/* .line 518 */
return;
/* .line 516 */
} // :goto_6
libcore.io.IoUtils .closeQuietly ( v1 );
/* .line 517 */
/* throw v2 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static Boolean shouldLockAppRegion ( ) {
/* .locals 2 */
/* .line 689 */
v0 = com.android.server.pm.PackageManagerServiceImpl.CUSTOMIZED_REGION;
final String v1 = "lm_cr"; // const-string v1, "lm_cr"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
final String v1 = "mx_telcel"; // const-string v1, "mx_telcel"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean shouldSkipInstall ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1882 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_GLOBAL_REGION_BUILD:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsGlobalCrbtSupported:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* const/16 v2, 0x21 */
/* if-ge v0, v2, :cond_0 */
/* .line 1886 */
} // :cond_0
v0 = v0 = com.android.server.pm.PackageManagerServiceImpl.sTHPhoneAppsSet;
/* .line 1887 */
/* .local v0, "isTHPhoneApp":Z */
v2 = v2 = com.android.server.pm.PackageManagerServiceImpl.sGLPhoneAppsSet;
/* .line 1889 */
/* .local v2, "isGLPhoneApp":Z */
/* if-nez v2, :cond_2 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1897 */
} // :cond_1
/* .line 1890 */
} // :cond_2
} // :goto_0
final String v3 = "ro.miui.region"; // const-string v3, "ro.miui.region"
android.os.SystemProperties .get ( v3 );
/* .line 1891 */
/* .local v3, "region":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1894 */
} // :cond_3
final String v1 = "TH"; // const-string v1, "TH"
v1 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* move v1, v2 */
} // :cond_4
/* move v1, v0 */
} // :goto_1
/* .line 1884 */
} // .end local v0 # "isTHPhoneApp":Z
} // .end local v2 # "isGLPhoneApp":Z
} // .end local v3 # "region":Ljava/lang/String;
} // :cond_5
} // :goto_2
} // .end method
private Boolean shouldSkipInstallCotaApp ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 1901 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->SUPPORT_DEL_COTA_APP:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1902 */
/* .line 1904 */
} // :cond_0
/* nop */
/* .line 1905 */
final String v0 = "persist.sys.cota.carrier"; // const-string v0, "persist.sys.cota.carrier"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v0,v2 );
(( java.lang.String ) v0 ).toLowerCase ( ); // invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
/* .line 1906 */
/* .local v0, "cotaCarrier":Ljava/lang/String; */
v2 = this.mCotaApps;
v2 = (( java.util.HashMap ) v2 ).containsKey ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1907 */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_1 */
v2 = this.mCotaApps;
/* .line 1908 */
(( java.util.HashMap ) v2 ).get ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* nop */
/* .line 1907 */
} // :goto_0
/* .line 1910 */
} // :cond_2
} // .end method
private Boolean updateDefaultPkgInstallerLocked ( ) {
/* .locals 11 */
/* .line 1184 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_10 */
/* .line 1185 */
/* const-string/jumbo v0, "updateDefaultPkgInstallerLocked" */
final String v2 = "PKMSImpl"; // const-string v2, "PKMSImpl"
android.util.Log .i ( v2,v0 );
/* .line 1186 */
v0 = this.mMiuiInstallerPackageSetting;
/* .line 1187 */
/* .local v0, "miuiInstaller":Lcom/android/server/pm/PackageSetting; */
final String v3 = "com.miui.packageinstaller"; // const-string v3, "com.miui.packageinstaller"
/* if-nez v0, :cond_0 */
/* .line 1188 */
v4 = this.mPkgSettings;
v4 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v0, v4 */
/* check-cast v0, Lcom/android/server/pm/PackageSetting; */
/* .line 1190 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1191 */
final String v4 = "found miui installer"; // const-string v4, "found miui installer"
android.util.Log .i ( v2,v4 );
/* .line 1193 */
} // :cond_1
v4 = this.mPkgSettings;
v4 = this.mPackages;
final String v5 = "com.google.android.packageinstaller"; // const-string v5, "com.google.android.packageinstaller"
(( com.android.server.utils.WatchedArrayMap ) v4 ).get ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/pm/PackageSetting; */
/* .line 1194 */
/* .local v4, "googleInstaller":Lcom/android/server/pm/PackageSetting; */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1195 */
final String v6 = "found google installer"; // const-string v6, "found google installer"
android.util.Log .i ( v2,v6 );
/* .line 1197 */
} // :cond_2
v6 = this.mPkgSettings;
v6 = this.mPackages;
final String v7 = "com.android.packageinstaller"; // const-string v7, "com.android.packageinstaller"
(( com.android.server.utils.WatchedArrayMap ) v6 ).get ( v7 ); // invoke-virtual {v6, v7}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/pm/PackageSetting; */
/* .line 1198 */
/* .local v6, "androidInstaller":Lcom/android/server/pm/PackageSetting; */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 1199 */
final String v8 = "found android installer"; // const-string v8, "found android installer"
android.util.Log .i ( v2,v8 );
/* .line 1201 */
} // :cond_3
v8 = com.android.server.pm.PackageManagerServiceImpl .isCTS ( );
int v9 = 1; // const/4 v9, 0x1
/* if-nez v8, :cond_5 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1202 */
v8 = (( com.android.server.pm.PackageSetting ) v0 ).isSystem ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageSetting;->isSystem()Z
/* if-nez v8, :cond_4 */
} // :cond_4
/* move v8, v1 */
} // :cond_5
} // :goto_0
/* move v8, v9 */
/* .line 1203 */
/* .local v8, "isUseGooglePackageInstaller":Z */
} // :goto_1
if ( v8 != null) { // if-eqz v8, :cond_a
/* .line 1204 */
v10 = this.mCurrentPackageInstaller;
v10 = (( java.lang.String ) v5 ).equals ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v10, :cond_f */
v10 = this.mCurrentPackageInstaller;
/* .line 1205 */
v10 = (( java.lang.String ) v7 ).equals ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v10, :cond_f */
/* .line 1206 */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 1207 */
(( com.android.server.pm.PackageSetting ) v0 ).setInstalled ( v1, v1 ); // invoke-virtual {v0, v1, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 1209 */
} // :cond_6
v2 = this.mPkgSettings;
v2 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/pm/PackageSetting; */
this.mMiuiInstallerPackageSetting = v2;
/* .line 1210 */
v2 = this.mPms;
v2 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/pm/pkg/AndroidPackage; */
this.mMiuiInstallerPackage = v2;
/* .line 1211 */
v2 = this.mPkgSettings;
v2 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v2 ).remove ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/utils/WatchedArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1212 */
v2 = this.mPkgSettings;
v2 = (( com.android.server.pm.Settings ) v2 ).isDisabledSystemPackageLPr ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/pm/Settings;->isDisabledSystemPackageLPr(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 1213 */
v2 = this.mPkgSettings;
(( com.android.server.pm.Settings ) v2 ).removeDisabledSystemPackageLPw ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/pm/Settings;->removeDisabledSystemPackageLPw(Ljava/lang/String;)V
/* .line 1215 */
} // :cond_7
v2 = this.mPms;
v2 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v2 ).remove ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/utils/WatchedArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1216 */
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 1217 */
(( com.android.server.pm.PackageSetting ) v4 ).setInstalled ( v9, v1 ); // invoke-virtual {v4, v9, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 1218 */
this.mCurrentPackageInstaller = v5;
/* .line 1219 */
} // :cond_8
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 1220 */
(( com.android.server.pm.PackageSetting ) v6 ).setInstalled ( v9, v1 ); // invoke-virtual {v6, v9, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 1221 */
this.mCurrentPackageInstaller = v7;
/* .line 1223 */
} // :cond_9
} // :goto_2
/* .line 1226 */
} // :cond_a
v5 = this.mCurrentPackageInstaller;
v5 = (( java.lang.String ) v3 ).equals ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_f */
/* .line 1227 */
v2 = this.mMiuiInstallerPackageSetting;
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 1228 */
(( com.android.server.pm.PackageSetting ) v2 ).setInstalled ( v9, v1 ); // invoke-virtual {v2, v9, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 1229 */
v2 = this.mPkgSettings;
v2 = this.mPackages;
v5 = this.mMiuiInstallerPackageSetting;
(( com.android.server.utils.WatchedArrayMap ) v2 ).put ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Lcom/android/server/utils/WatchedArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1230 */
v2 = this.mMiuiInstallerPackageSetting;
v2 = (( com.android.server.pm.PackageSetting ) v2 ).getFlags ( ); // invoke-virtual {v2}, Lcom/android/server/pm/PackageSetting;->getFlags()I
/* and-int/lit16 v2, v2, 0x80 */
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 1231 */
v2 = this.mPkgSettings;
(( com.android.server.pm.Settings ) v2 ).disableSystemPackageLPw ( v3, v9 ); // invoke-virtual {v2, v3, v9}, Lcom/android/server/pm/Settings;->disableSystemPackageLPw(Ljava/lang/String;Z)Z
/* .line 1234 */
} // :cond_b
v2 = this.mMiuiInstallerPackage;
if ( v2 != null) { // if-eqz v2, :cond_c
/* .line 1235 */
v2 = this.mPms;
v2 = this.mPackages;
v5 = this.mMiuiInstallerPackage;
(( com.android.server.utils.WatchedArrayMap ) v2 ).put ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Lcom/android/server/utils/WatchedArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1237 */
} // :cond_c
this.mCurrentPackageInstaller = v3;
/* .line 1238 */
if ( v4 != null) { // if-eqz v4, :cond_d
/* .line 1239 */
(( com.android.server.pm.PackageSetting ) v4 ).setInstalled ( v1, v1 ); // invoke-virtual {v4, v1, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 1241 */
} // :cond_d
if ( v6 != null) { // if-eqz v6, :cond_e
/* .line 1242 */
(( com.android.server.pm.PackageSetting ) v6 ).setInstalled ( v1, v1 ); // invoke-virtual {v6, v1, v1}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 1244 */
} // :cond_e
/* .line 1247 */
} // :cond_f
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "set default package install as" */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mCurrentPackageInstaller;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v2,v3 );
/* .line 1249 */
} // .end local v0 # "miuiInstaller":Lcom/android/server/pm/PackageSetting;
} // .end local v4 # "googleInstaller":Lcom/android/server/pm/PackageSetting;
} // .end local v6 # "androidInstaller":Lcom/android/server/pm/PackageSetting;
} // .end local v8 # "isUseGooglePackageInstaller":Z
} // :cond_10
} // .end method
private void updateSystemAppState ( Integer p0, Boolean p1, java.lang.String p2 ) {
/* .locals 8 */
/* .param p1, "userId" # I */
/* .param p2, "isCTS" # Z */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .line 1573 */
v0 = this.mPms;
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 1574 */
try { // :try_start_0
v1 = this.mPms;
v1 = this.mSettings;
(( com.android.server.pm.Settings ) v1 ).getPackageLPr ( p3 ); // invoke-virtual {v1, p3}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
/* .line 1575 */
/* .local v1, "pkgSetting":Lcom/android/server/pm/PackageSetting; */
if ( v1 != null) { // if-eqz v1, :cond_9
v2 = (( com.android.server.pm.PackageSetting ) v1 ).isSystem ( ); // invoke-virtual {v1}, Lcom/android/server/pm/PackageSetting;->isSystem()Z
/* if-nez v2, :cond_0 */
/* .line 1578 */
} // :cond_0
/* nop */
/* .line 1579 */
v2 = (( com.android.server.pm.PackageSetting ) v1 ).getFlags ( ); // invoke-virtual {v1}, Lcom/android/server/pm/PackageSetting;->getFlags()I
/* and-int/lit16 v2, v2, 0x80 */
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1580 */
v2 = (( com.android.server.pm.PackageSetting ) v1 ).getInstalled ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/PackageSetting;->getInstalled(I)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
} // :cond_1
/* move v2, v4 */
/* .line 1581 */
/* .local v2, "updatedSystemApp":Z */
} // :goto_0
/* invoke-direct {p0, v1, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->getdatedefaultState(Lcom/android/server/pm/PackageSetting;I)Ljava/lang/String; */
/* if-nez v5, :cond_2 */
/* move v5, v3 */
} // :cond_2
/* move v5, v4 */
/* .line 1582 */
/* .local v5, "untouchedYet":Z */
} // :goto_1
v6 = (( com.android.server.pm.PackageSetting ) v1 ).getEnabled ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/PackageSetting;->getEnabled(I)I
/* .line 1583 */
/* .local v6, "state":I */
int v7 = 2; // const/4 v7, 0x2
/* if-eq v6, v7, :cond_4 */
int v7 = 3; // const/4 v7, 0x3
/* if-ne v6, v7, :cond_3 */
} // :cond_3
/* move v3, v4 */
/* .line 1585 */
/* .local v3, "alreadyDisabled":Z */
} // :cond_4
} // :goto_2
final String v4 = "COTA"; // const-string v4, "COTA"
(( com.android.server.pm.PackageSetting ) v1 ).readUserState ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/pm/PackageSetting;->readUserState(I)Lcom/android/server/pm/pkg/PackageUserStateInternal;
v4 = (( java.lang.String ) v4 ).equals ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* move v1, v4 */
/* .line 1586 */
} // .end local v6 # "state":I
/* .local v1, "wasDisabledByUs":Z */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1587 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 1588 */
return;
/* .line 1590 */
} // :cond_5
if ( p2 != null) { // if-eqz p2, :cond_7
/* .line 1592 */
if ( v3 != null) { // if-eqz v3, :cond_6
if ( v1 != null) { // if-eqz v1, :cond_6
/* invoke-direct {p0, p1, p3}, Lcom/android/server/pm/PackageManagerServiceImpl;->enableSystemApp(ILjava/lang/String;)V */
/* .line 1593 */
} // :cond_6
return;
/* .line 1596 */
} // :cond_7
/* if-nez v3, :cond_8 */
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 1597 */
/* invoke-direct {p0, p1, p3}, Lcom/android/server/pm/PackageManagerServiceImpl;->disableSystemApp(ILjava/lang/String;)V */
/* .line 1599 */
} // :cond_8
return;
/* .line 1576 */
} // .end local v2 # "updatedSystemApp":Z
} // .end local v3 # "alreadyDisabled":Z
} // .end local v5 # "untouchedYet":Z
/* .local v1, "pkgSetting":Lcom/android/server/pm/PackageSetting; */
} // :cond_9
} // :goto_3
try { // :try_start_1
/* monitor-exit v0 */
return;
/* .line 1586 */
} // .end local v1 # "pkgSetting":Lcom/android/server/pm/PackageSetting;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void updatedefaultState ( com.android.server.pm.PackageSetting p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p1, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .param p2, "value" # Ljava/lang/String; */
/* .param p3, "userId" # I */
/* .line 1606 */
(( com.android.server.pm.PackageSetting ) p1 ).modifyUserState ( p3 ); // invoke-virtual {p1, p3}, Lcom/android/server/pm/PackageSetting;->modifyUserState(I)Lcom/android/server/pm/pkg/PackageUserStateImpl;
(( com.android.server.pm.pkg.PackageUserStateImpl ) v0 ).setDefaultState ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/pm/pkg/PackageUserStateImpl;->setDefaultState(Ljava/lang/String;)V
/* .line 1607 */
return;
} // .end method
private static void verifyInstallFromShell ( android.content.Context p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "sessionId" # I */
/* .param p2, "log" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/pm/PackageManagerException; */
/* } */
} // .end annotation
/* .line 1068 */
int v0 = -1; // const/4 v0, -0x1
/* .line 1070 */
/* .local v0, "result":I */
try { // :try_start_0
v1 = com.android.server.pm.PackageManagerServiceImpl .isSecondUserlocked ( p0 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1071 */
int v0 = 2; // const/4 v0, 0x2
/* .line 1073 */
} // :cond_0
v1 = com.android.server.pm.PmInjector .installVerify ( p1 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v0, v1 */
/* .line 1077 */
} // :goto_0
/* .line 1075 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 1076 */
/* .local v1, "e":Ljava/lang/Throwable; */
final String v2 = "PKMSImpl"; // const-string v2, "PKMSImpl"
final String v3 = "Error"; // const-string v3, "Error"
android.util.Log .e ( v2,v3,v1 );
/* .line 1078 */
} // .end local v1 # "e":Ljava/lang/Throwable;
} // :goto_1
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 1079 */
} // :goto_2
com.android.server.pm.PmInjector .statusToString ( v0 );
/* .line 1078 */
/* const/16 v3, -0x6f */
com.android.server.pm.PackageManagerServiceImpl .fatalIf ( v1,v3,v2,p2 );
/* .line 1080 */
return;
} // .end method
private static Boolean verifyPackageForRelease ( android.content.Context p0, java.lang.String p1, android.content.pm.SigningDetails p2, Integer p3, java.lang.String p4, java.lang.String p5 ) {
/* .locals 6 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "signingDetails" # Landroid/content/pm/SigningDetails; */
/* .param p3, "callingUid" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .param p5, "log" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/pm/PackageManagerException; */
/* } */
} // .end annotation
/* .line 1045 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_4
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsBootLoaderLocked:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1046 */
final String v0 = "com.google.android.webview"; // const-string v0, "com.google.android.webview"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = v0 = com.android.server.pm.PackageManagerServiceImpl.sInstallerSet;
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
} // :goto_0
final String v3 = "FAILED_VERIFICATION_FAILURE MIUI WEBVIEW"; // const-string v3, "FAILED_VERIFICATION_FAILURE MIUI WEBVIEW"
/* const/16 v4, -0x16 */
com.android.server.pm.PackageManagerServiceImpl .fatalIf ( v0,v4,v3,p5 );
/* .line 1049 */
v0 = miui.content.pm.PreloadedAppPolicy .isProtectedDataApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1050 */
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v0 = (( android.content.pm.PackageManager ) v0 ).isPackageAvailable ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->isPackageAvailable(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1051 */
/* .line 1053 */
} // :cond_1
miui.content.pm.PreloadedAppPolicy .getProtectedDataAppSign ( p1 );
/* .line 1054 */
/* .local v0, "signSha256":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v3, :cond_3 */
/* .line 1055 */
final String v3 = ":"; // const-string v3, ":"
final String v5 = ""; // const-string v5, ""
(( java.lang.String ) v0 ).replace ( v3, v5 ); // invoke-virtual {v0, v3, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
(( java.lang.String ) v3 ).toLowerCase ( ); // invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
libcore.util.HexEncoding .decode ( v3,v2 );
/* .line 1054 */
v2 = (( android.content.pm.SigningDetails ) p2 ).hasSha256Certificate ( v2 ); // invoke-virtual {p2, v2}, Landroid/content/pm/SigningDetails;->hasSha256Certificate([B)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1059 */
} // :cond_2
final String v2 = "FAILED_VERIFICATION_FAILURE SIGNATURE FAIL"; // const-string v2, "FAILED_VERIFICATION_FAILURE SIGNATURE FAIL"
com.android.server.pm.PackageManagerServiceImpl .fatalIf ( v1,v4,v2,p5 );
/* .line 1057 */
} // :cond_3
} // :goto_1
/* .line 1063 */
} // .end local v0 # "signSha256":Ljava/lang/String;
} // :cond_4
} // :goto_2
} // .end method
/* # virtual methods */
void addMiuiSharedUids ( ) {
/* .locals 5 */
/* .line 257 */
v0 = this.mPkgSettings;
final String v1 = "android.uid.theme"; // const-string v1, "android.uid.theme"
/* const/16 v2, 0x17d5 */
int v3 = 1; // const/4 v3, 0x1
/* const/16 v4, 0x8 */
(( com.android.server.pm.Settings ) v0 ).addSharedUserLPw ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;III)Lcom/android/server/pm/SharedUserSetting;
/* .line 261 */
v0 = this.mPkgSettings;
final String v1 = "android.uid.backup"; // const-string v1, "android.uid.backup"
/* const/16 v2, 0x17d4 */
(( com.android.server.pm.Settings ) v0 ).addSharedUserLPw ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;III)Lcom/android/server/pm/SharedUserSetting;
/* .line 264 */
v0 = this.mPkgSettings;
final String v1 = "android.uid.updater"; // const-string v1, "android.uid.updater"
/* const/16 v2, 0x17d6 */
(( com.android.server.pm.Settings ) v0 ).addSharedUserLPw ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;III)Lcom/android/server/pm/SharedUserSetting;
/* .line 267 */
v0 = this.mPkgSettings;
final String v1 = "android.uid.finddevice"; // const-string v1, "android.uid.finddevice"
/* const/16 v2, 0x17de */
(( com.android.server.pm.Settings ) v0 ).addSharedUserLPw ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/pm/Settings;->addSharedUserLPw(Ljava/lang/String;III)Lcom/android/server/pm/SharedUserSetting;
/* .line 270 */
return;
} // .end method
public void addPermierFlagIfNeedForGlobalROM ( android.util.ArrayMap p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Landroid/content/pm/FeatureInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 1916 */
/* .local p1, "availableFeatures":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/content/pm/FeatureInfo;>;" */
/* if-nez p1, :cond_0 */
/* .line 1917 */
return;
/* .line 1922 */
} // :cond_0
final String v0 = "com.google.android.feature.PREMIER_TIER"; // const-string v0, "com.google.android.feature.PREMIER_TIER"
/* .line 1923 */
/* .local v0, "FEATURE_PERMIER_TIER":Ljava/lang/String; */
final String v1 = "com.google.android.feature.PREMIER_TIER"; // const-string v1, "com.google.android.feature.PREMIER_TIER"
(( android.util.ArrayMap ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* if-nez v2, :cond_1 */
/* .line 1924 */
final String v2 = "ro.com.miui.rsa"; // const-string v2, "ro.com.miui.rsa"
final String v3 = ""; // const-string v3, ""
android.os.SystemProperties .get ( v2,v3 );
/* const-string/jumbo v4, "tier1" */
v2 = (( java.lang.String ) v4 ).equals ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1925 */
final String v2 = "ro.miui.build.region"; // const-string v2, "ro.miui.build.region"
android.os.SystemProperties .get ( v2,v3 );
final String v4 = "global"; // const-string v4, "global"
v2 = (( java.lang.String ) v4 ).equalsIgnoreCase ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1926 */
final String v2 = "ro.miui.customized.region"; // const-string v2, "ro.miui.customized.region"
android.os.SystemProperties .get ( v2,v3 );
v2 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1927 */
/* new-instance v2, Landroid/content/pm/FeatureInfo; */
/* invoke-direct {v2}, Landroid/content/pm/FeatureInfo;-><init>()V */
/* .line 1928 */
/* .local v2, "fi":Landroid/content/pm/FeatureInfo; */
this.name = v1;
/* .line 1929 */
(( android.util.ArrayMap ) p1 ).put ( v1, v2 ); // invoke-virtual {p1, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1931 */
} // .end local v2 # "fi":Landroid/content/pm/FeatureInfo;
} // :cond_1
return;
} // .end method
void assertValidApkAndInstaller ( java.lang.String p0, android.content.pm.SigningDetails p1, Integer p2, java.lang.String p3, Boolean p4, Integer p5 ) {
/* .locals 9 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "signingDetails" # Landroid/content/pm/SigningDetails; */
/* .param p3, "callingUid" # I */
/* .param p4, "callingPackage" # Ljava/lang/String; */
/* .param p5, "isManuallyAccepted" # Z */
/* .param p6, "sessionId" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/pm/PackageManagerException; */
/* } */
} // .end annotation
/* .line 1001 */
v0 = com.android.server.pm.PackageManagerServiceImpl .isCTS ( );
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 1003 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "MIUILOG- assertCallerAndPackage: uid="; // const-string v1, "MIUILOG- assertCallerAndPackage: uid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", installerPkg="; // const-string v1, ", installerPkg="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1004 */
/* .local v0, "log":Ljava/lang/String; */
v7 = android.os.UserHandle .getAppId ( p3 );
/* .line 1005 */
/* .local v7, "callingAppId":I */
/* sparse-switch v7, :sswitch_data_0 */
/* .line 1014 */
v1 = com.android.server.pm.PackageManagerServiceImpl.sNoVerifyAllowPackage;
v1 = (( java.util.ArrayList ) v1 ).contains ( p4 ); // invoke-virtual {v1, p4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1015 */
return;
/* .line 1009 */
/* :sswitch_0 */
v1 = this.mContext;
com.android.server.pm.PackageManagerServiceImpl .verifyInstallFromShell ( v1,p6,v0 );
/* .line 1010 */
return;
/* .line 1007 */
/* :sswitch_1 */
return;
/* .line 1019 */
} // :cond_1
v1 = this.mContext;
v1 = com.android.server.pm.PackageManagerServiceImpl .isTrustedEnterpriseInstaller ( v1,p3,p4 );
int v8 = 1; // const/4 v8, 0x1
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1020 */
miui.enterprise.ApplicationHelperStub .getInstance ( );
v1 = /* .line 1021 */
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :cond_3
} // :goto_0
/* move v1, v8 */
/* .line 1019 */
} // :goto_1
/* const/16 v2, -0x16 */
final String v3 = "FAILED_VERIFICATION_FAILURE ENTERPRISE"; // const-string v3, "FAILED_VERIFICATION_FAILURE ENTERPRISE"
com.android.server.pm.PackageManagerServiceImpl .fatalIf ( v1,v2,v3,v0 );
/* .line 1026 */
v1 = this.mContext;
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move v4, p3 */
/* move-object v5, p4 */
/* move-object v6, v0 */
v1 = /* invoke-static/range {v1 ..v6}, Lcom/android/server/pm/PackageManagerServiceImpl;->verifyPackageForRelease(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/SigningDetails;ILjava/lang/String;Ljava/lang/String;)Z */
/* if-nez v1, :cond_4 */
/* .line 1027 */
return;
/* .line 1030 */
} // :cond_4
v1 = this.mContext;
com.android.server.pm.PackageManagerServiceImpl .notifyGlobalPackageInstaller ( v1,p4 );
/* .line 1032 */
v1 = com.android.server.pm.PackageManagerServiceImpl.sAllowPackage;
v2 = (( java.util.ArrayList ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 1033 */
v2 = this.mContext;
com.android.server.pm.PackageManagerServiceImpl .initAllowPackageList ( v2 );
/* .line 1035 */
} // :cond_5
/* if-nez p5, :cond_7 */
v1 = (( java.util.ArrayList ) v1 ).contains ( p4 ); // invoke-virtual {v1, p4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_7 */
/* sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->sIsReleaseRom:Z */
/* if-nez v1, :cond_6 */
/* .line 1038 */
} // :cond_6
/* const/16 v1, -0x73 */
final String v2 = "Permission denied"; // const-string v2, "Permission denied"
com.android.server.pm.PackageManagerServiceImpl .fatalIf ( v8,v1,v2,v0 );
/* .line 1039 */
return;
/* .line 1036 */
} // :cond_7
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_1 */
/* 0x7d0 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void asyncDexMetadataDexopt ( com.android.server.pm.pkg.AndroidPackage p0, Integer[] p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Lcom/android/server/pm/pkg/AndroidPackage; */
/* .param p2, "userId" # [I */
/* .line 1729 */
v0 = this.mMiuiDexopt;
(( com.android.server.pm.MiuiDexopt ) v0 ).asyncDexMetadataDexopt ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/MiuiDexopt;->asyncDexMetadataDexopt(Lcom/android/server/pm/pkg/AndroidPackage;[I)V
/* .line 1730 */
return;
} // .end method
public void beforeSystemReady ( ) {
/* .locals 5 */
/* .line 280 */
com.android.server.pm.PreinstallApp .exemptPermissionRestrictions ( );
/* .line 281 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 282 */
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
android.provider.Settings$Global .getUriFor ( v1 );
/* new-instance v2, Lcom/android/server/pm/PackageManagerServiceImpl$1; */
v3 = this.mPms;
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/pm/PackageManagerServiceImpl$1;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Landroid/os/Handler;)V */
/* .line 281 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 289 */
v0 = this.mPackageManagerCloudHelper;
(( com.android.server.pm.PackageManagerCloudHelper ) v0 ).registerCloudDataObserver ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerCloudHelper;->registerCloudDataObserver()V
/* .line 290 */
v0 = this.mMiuiDexopt;
v1 = this.mContext;
(( com.android.server.pm.MiuiDexopt ) v0 ).preConfigMiuiDexopt ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/pm/MiuiDexopt;->preConfigMiuiDexopt(Landroid/content/Context;)V
/* .line 292 */
com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
/* .line 293 */
/* .local v0, "miuiPreinstallHelper":Lcom/android/server/pm/MiuiPreinstallHelper; */
v1 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 294 */
v1 = this.mContext;
/* new-instance v2, Lcom/android/server/pm/PackageManagerServiceImpl$2; */
/* invoke-direct {v2, p0, v0}, Lcom/android/server/pm/PackageManagerServiceImpl$2;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Lcom/android/server/pm/MiuiPreinstallHelper;)V */
/* new-instance v3, Landroid/content/IntentFilter; */
final String v4 = "android.intent.action.BOOT_COMPLETED"; // const-string v4, "android.intent.action.BOOT_COMPLETED"
/* invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 301 */
} // :cond_0
return;
} // .end method
void canBeDisabled ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "newState" # I */
/* .line 655 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 656 */
/* .local v0, "callingUid":I */
if ( p2 != null) { // if-eqz p2, :cond_8
int v1 = 1; // const/4 v1, 0x1
/* if-ne p2, v1, :cond_0 */
/* .line 661 */
} // :cond_0
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "maintenance_mode_user_id"; // const-string v2, "maintenance_mode_user_id"
v1 = android.provider.Settings$Global .getInt ( v1,v2 );
/* :try_end_0 */
/* .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* const/16 v2, 0x6e */
/* if-ne v1, v2, :cond_1 */
/* .line 663 */
return;
/* .line 667 */
} // :cond_1
/* .line 665 */
/* :catch_0 */
/* move-exception v1 */
/* .line 666 */
/* .local v1, "e":Landroid/provider/Settings$SettingNotFoundException; */
(( android.provider.Settings$SettingNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V
/* .line 668 */
} // .end local v1 # "e":Landroid/provider/Settings$SettingNotFoundException;
} // :goto_0
/* const/16 v1, 0x7d0 */
/* if-ne v0, v1, :cond_4 */
/* .line 670 */
final String v1 = "com.android.cts.priv.ctsshim"; // const-string v1, "com.android.cts.priv.ctsshim"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 671 */
return;
/* .line 673 */
} // :cond_2
v1 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v1 ).snapshotComputer ( ); // invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 674 */
/* .local v1, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
v2 = if ( v1 != null) { // if-eqz v1, :cond_4
if ( v2 != null) { // if-eqz v2, :cond_4
int v2 = 3; // const/4 v2, 0x3
/* if-eq p2, v2, :cond_3 */
/* .line 675 */
} // :cond_3
/* new-instance v2, Ljava/lang/SecurityException; */
final String v3 = "Cannot disable system packages."; // const-string v3, "Cannot disable system packages."
/* invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 678 */
} // .end local v1 # "ps":Lcom/android/server/pm/pkg/PackageStateInternal;
} // :cond_4
} // :goto_1
v1 = com.android.server.pm.PackageManagerServiceImpl.MIUI_CORE_APPS;
v1 = com.android.internal.util.ArrayUtils .contains ( v1,p1 );
/* if-nez v1, :cond_7 */
/* .line 682 */
final String v1 = "co.sitic.pp"; // const-string v1, "co.sitic.pp"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_6
v1 = com.android.server.pm.PackageManagerServiceImpl .shouldLockAppRegion ( );
/* if-nez v1, :cond_5 */
/* .line 683 */
} // :cond_5
/* new-instance v1, Ljava/lang/SecurityException; */
final String v2 = "Cannot disable carrier core packages."; // const-string v2, "Cannot disable carrier core packages."
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 685 */
} // :cond_6
} // :goto_2
return;
/* .line 679 */
} // :cond_7
/* new-instance v1, Ljava/lang/SecurityException; */
final String v2 = "Cannot disable miui core packages."; // const-string v2, "Cannot disable miui core packages."
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 658 */
} // :cond_8
} // :goto_3
return;
} // .end method
public void canBeUpdate ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lcom/android/server/pm/PrepareFailure; */
/* } */
} // .end annotation
/* .line 1937 */
v0 = this.mPackageManagerCloudHelper;
v0 = (( com.android.server.pm.PackageManagerCloudHelper ) v0 ).getCloudNotSupportUpdateSystemApps ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerCloudHelper;->getCloudNotSupportUpdateSystemApps()Ljava/util/Set;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1938 */
v0 = v0 = com.android.server.pm.PackageManagerServiceImpl.sNotSupportUpdateSystemApps;
/* .line 1940 */
} // :cond_0
v0 = this.mPackageManagerCloudHelper;
/* .line 1939 */
(( com.android.server.pm.PackageManagerCloudHelper ) v0 ).getCloudNotSupportUpdateSystemApps ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerCloudHelper;->getCloudNotSupportUpdateSystemApps()Ljava/util/Set;
v0 = /* .line 1940 */
} // :goto_0
/* nop */
/* .line 1941 */
/* .local v0, "isNotSupportUpdate":Z */
/* if-nez v0, :cond_1 */
/* .line 1945 */
return;
/* .line 1942 */
} // :cond_1
/* new-instance v1, Lcom/android/server/pm/PrepareFailure; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Package "; // const-string v3, "Package "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " are not updateable."; // const-string v3, " are not updateable."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = -2; // const/4 v3, -0x2
/* invoke-direct {v1, v3, v2}, Lcom/android/server/pm/PrepareFailure;-><init>(ILjava/lang/String;)V */
/* throw v1 */
} // .end method
Boolean checkEnterpriseRestriction ( com.android.server.pm.parsing.pkg.ParsedPackage p0 ) {
/* .locals 5 */
/* .param p1, "pkg" # Lcom/android/server/pm/parsing/pkg/ParsedPackage; */
/* .line 600 */
miui.enterprise.ApplicationHelperStub .getInstance ( );
/* .line 601 */
v0 = /* .line 600 */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 602 */
/* .line 604 */
} // :cond_0
v0 = final String v2 = "com.miui.enterprise.permission.ACCESS_ENTERPRISE_API"; // const-string v2, "com.miui.enterprise.permission.ACCESS_ENTERPRISE_API"
final String v2 = "PKMSImpl"; // const-string v2, "PKMSImpl"
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mContext;
/* .line 607 */
/* .line 606 */
v0 = com.miui.enterprise.signature.EnterpriseVerifier .verify ( v0,v3,v4 );
/* if-nez v0, :cond_1 */
/* .line 608 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Verify enterprise signature of package "; // const-string v3, "Verify enterprise signature of package "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 609 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " failed"; // const-string v3, " failed"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 608 */
android.util.Slog .d ( v2,v0 );
/* .line 610 */
/* .line 612 */
} // :cond_1
/* sget-boolean v0, Lcom/miui/enterprise/settings/EnterpriseSettings;->ENTERPRISE_ACTIVATED:Z */
int v3 = 0; // const/4 v3, 0x0
/* if-nez v0, :cond_2 */
/* .line 613 */
/* .line 615 */
} // :cond_2
v0 = this.mContext;
v0 = com.miui.enterprise.ApplicationHelper .checkEnterprisePackageRestriction ( v0,v4 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 616 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Installation of package "; // const-string v3, "Installation of package "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " is restricted"; // const-string v3, " is restricted"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 617 */
/* .line 619 */
} // :cond_3
} // .end method
public void checkGTSSpecAppOptMode ( ) {
/* .locals 11 */
/* .line 1336 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1337 */
final String v0 = "com.miui.screenrecorder"; // const-string v0, "com.miui.screenrecorder"
/* filled-new-array {v0}, [Ljava/lang/String; */
/* .local v0, "pkgs":[Ljava/lang/String; */
/* .line 1341 */
} // .end local v0 # "pkgs":[Ljava/lang/String;
} // :cond_0
final String v0 = "com.miui.cleanmaster"; // const-string v0, "com.miui.cleanmaster"
final String v1 = "com.xiaomi.drivemode"; // const-string v1, "com.xiaomi.drivemode"
final String v2 = "com.xiaomi.aiasst.service"; // const-string v2, "com.xiaomi.aiasst.service"
final String v3 = "com.miui.thirdappassistant"; // const-string v3, "com.miui.thirdappassistant"
final String v4 = "com.miui.screenrecorder"; // const-string v4, "com.miui.screenrecorder"
/* filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String; */
/* .line 1349 */
/* .restart local v0 # "pkgs":[Ljava/lang/String; */
} // :goto_0
v1 = this.mPms;
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 1350 */
try { // :try_start_0
v2 = this.mPms;
v2 = this.mSettings;
/* .line 1351 */
/* .local v2, "mPkgSettings":Lcom/android/server/pm/Settings; */
v3 = com.android.server.pm.PackageManagerServiceImpl .isCTS ( );
/* .line 1352 */
/* .local v3, "isCtsBuild":Z */
/* array-length v4, v0 */
int v5 = 0; // const/4 v5, 0x0
/* move v6, v5 */
} // :goto_1
/* if-ge v6, v4, :cond_6 */
/* aget-object v7, v0, v6 */
/* .line 1353 */
/* .local v7, "pkg":Ljava/lang/String; */
v8 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v8 ).get ( v7 ); // invoke-virtual {v8, v7}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v8, Lcom/android/server/pm/PackageSetting; */
/* .line 1354 */
/* .local v8, "uninstallPkg":Lcom/android/server/pm/PackageSetting; */
final String v9 = "com.miui.cleanmaster"; // const-string v9, "com.miui.cleanmaster"
v9 = (( java.lang.String ) v9 ).equals ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_1
/* if-nez v8, :cond_1 */
/* .line 1355 */
v9 = this.mPermissions;
final String v10 = "com.miui.cleanmaster.permission.Clean_Master"; // const-string v10, "com.miui.cleanmaster.permission.Clean_Master"
com.android.server.pm.PackageManagerServiceImpl .checkAndClearResiduePermissions ( v9,v7,v10 );
/* .line 1358 */
} // :cond_1
final String v9 = "com.miui.screenrecorder"; // const-string v9, "com.miui.screenrecorder"
v9 = (( java.lang.String ) v9 ).equals ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_2
/* if-nez v8, :cond_2 */
/* .line 1359 */
v9 = this.mPermissions;
final String v10 = "com.miui.screenrecorder.DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION"; // const-string v10, "com.miui.screenrecorder.DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION"
com.android.server.pm.PackageManagerServiceImpl .checkAndClearResiduePermissions ( v9,v7,v10 );
/* .line 1362 */
} // :cond_2
if ( v3 != null) { // if-eqz v3, :cond_5
if ( v8 != null) { // if-eqz v8, :cond_5
v9 = (( com.android.server.pm.PackageSetting ) v8 ).isSystem ( ); // invoke-virtual {v8}, Lcom/android/server/pm/PackageSetting;->isSystem()Z
/* if-nez v9, :cond_5 */
/* .line 1363 */
(( com.android.server.pm.PackageSetting ) v8 ).setInstalled ( v5, v5 ); // invoke-virtual {v8, v5, v5}, Lcom/android/server/pm/PackageSetting;->setInstalled(ZI)V
/* .line 1365 */
v9 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v9 ).remove ( v7 ); // invoke-virtual {v9, v7}, Lcom/android/server/utils/WatchedArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1366 */
v9 = (( com.android.server.pm.Settings ) v2 ).isDisabledSystemPackageLPr ( v7 ); // invoke-virtual {v2, v7}, Lcom/android/server/pm/Settings;->isDisabledSystemPackageLPr(Ljava/lang/String;)Z
if ( v9 != null) { // if-eqz v9, :cond_3
/* .line 1367 */
(( com.android.server.pm.Settings ) v2 ).removeDisabledSystemPackageLPw ( v7 ); // invoke-virtual {v2, v7}, Lcom/android/server/pm/Settings;->removeDisabledSystemPackageLPw(Ljava/lang/String;)V
/* .line 1369 */
} // :cond_3
v9 = this.mPms;
v9 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v9 ).remove ( v7 ); // invoke-virtual {v9, v7}, Lcom/android/server/utils/WatchedArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1371 */
final String v9 = "com.miui.cleanmaster"; // const-string v9, "com.miui.cleanmaster"
v9 = (( java.lang.String ) v9 ).equals ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v9, :cond_4 */
final String v9 = "com.miui.thirdappassistant"; // const-string v9, "com.miui.thirdappassistant"
v9 = (( java.lang.String ) v9 ).equals ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_5
/* .line 1372 */
} // :cond_4
com.android.server.pm.PackageManagerServiceImpl .clearPermissions ( v7 );
/* .line 1352 */
} // .end local v7 # "pkg":Ljava/lang/String;
} // .end local v8 # "uninstallPkg":Lcom/android/server/pm/PackageSetting;
} // :cond_5
/* add-int/lit8 v6, v6, 0x1 */
/* .line 1376 */
} // .end local v2 # "mPkgSettings":Lcom/android/server/pm/Settings;
} // .end local v3 # "isCtsBuild":Z
} // :cond_6
/* monitor-exit v1 */
/* .line 1377 */
return;
/* .line 1376 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean checkReplaceSetting ( Integer p0, com.android.server.pm.SettingBase p1 ) {
/* .locals 3 */
/* .param p1, "appId" # I */
/* .param p2, "setting" # Lcom/android/server/pm/SettingBase; */
/* .line 1812 */
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_0 */
/* instance-of v0, p2, Lcom/android/server/pm/PackageSetting; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1813 */
/* move-object v0, p2 */
/* check-cast v0, Lcom/android/server/pm/PackageSetting; */
/* .line 1814 */
/* .local v0, "ps":Lcom/android/server/pm/PackageSetting; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Package "; // const-string v2, "Package "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1815 */
(( com.android.server.pm.PackageSetting ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageSetting;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " with user id "; // const-string v2, " with user id "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " cannot replace SYSTEM_UID"; // const-string v2, " cannot replace SYSTEM_UID"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1814 */
int v2 = 6; // const/4 v2, 0x6
com.android.server.pm.PackageManagerService .reportSettingsProblem ( v2,v1 );
/* .line 1817 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1819 */
} // .end local v0 # "ps":Lcom/android/server/pm/PackageSetting;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
void clearNoHistoryFlagIfNeed ( java.util.List p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ResolveInfo;", */
/* ">;", */
/* "Landroid/content/Intent;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 547 */
/* .local p1, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
final String v0 = "PKMSImpl"; // const-string v0, "PKMSImpl"
/* sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v1, :cond_0 */
/* .line 548 */
return;
/* .line 551 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 552 */
return;
/* .line 555 */
} // :cond_1
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Landroid/content/pm/ResolveInfo; */
/* .line 556 */
/* .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo; */
v3 = this.activityInfo;
v3 = this.metaData;
/* .line 558 */
/* .local v3, "bundle":Landroid/os/Bundle; */
if ( v3 != null) { // if-eqz v3, :cond_2
try { // :try_start_0
final String v4 = "mi_use_custom_resolver"; // const-string v4, "mi_use_custom_resolver"
v4 = (( android.os.Bundle ) v3 ).getBoolean ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = this.activityInfo;
/* iget-boolean v4, v4, Landroid/content/pm/ActivityInfo;->enabled:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 560 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Removing FLAG_ACTIVITY_NO_HISTORY flag for Intent {"; // const-string v5, "Removing FLAG_ACTIVITY_NO_HISTORY flag for Intent {"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 561 */
int v5 = 1; // const/4 v5, 0x1
int v6 = 0; // const/4 v6, 0x0
(( android.content.Intent ) p2 ).toShortString ( v5, v5, v5, v6 ); // invoke-virtual {p2, v5, v5, v5, v6}, Landroid/content/Intent;->toShortString(ZZZZ)Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v5, "}" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 560 */
android.util.Log .i ( v0,v4 );
/* .line 562 */
/* const/high16 v4, 0x40000000 # 2.0f */
(( android.content.Intent ) p2 ).removeFlags ( v4 ); // invoke-virtual {p2, v4}, Landroid/content/Intent;->removeFlags(I)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 564 */
/* :catch_0 */
/* move-exception v4 */
/* .line 565 */
/* .local v4, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v4 ).getMessage ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Log .w ( v0,v5 );
/* .line 566 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_1
/* nop */
/* .line 567 */
} // .end local v2 # "resolveInfo":Landroid/content/pm/ResolveInfo;
} // .end local v3 # "bundle":Landroid/os/Bundle;
} // :goto_2
/* .line 568 */
} // :cond_3
return;
} // .end method
public void dumpSingleDexoptState ( com.android.internal.util.IndentingPrintWriter p0, com.android.server.pm.pkg.PackageStateInternal p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "pw" # Lcom/android/internal/util/IndentingPrintWriter; */
/* .param p2, "pkgSetting" # Lcom/android/server/pm/pkg/PackageStateInternal; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 1490 */
try { // :try_start_0
/* .line 1491 */
/* .local v0, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
/* if-nez v0, :cond_0 */
/* .line 1492 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unable to find AndroidPackage: "; // const-string v2, "Unable to find AndroidPackage: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 1493 */
return;
/* .line 1496 */
} // :cond_0
(( com.android.internal.util.IndentingPrintWriter ) p1 ).increaseIndent ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()Lcom/android/internal/util/IndentingPrintWriter;
/* .line 1497 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "base APK odex file size: "; // const-string v2, "base APK odex file size: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.content.pm.PackageParserStub .get ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.internal.util.IndentingPrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V
/* .line 1498 */
(( com.android.internal.util.IndentingPrintWriter ) p1 ).decreaseIndent ( ); // invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()Lcom/android/internal/util/IndentingPrintWriter;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1502 */
/* nop */
} // .end local v0 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
/* .line 1500 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1501 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
final String v2 = "Failed to dumpSingleDexoptState"; // const-string v2, "Failed to dumpSingleDexoptState"
android.util.Slog .w ( v1,v2,v0 );
/* .line 1503 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public Boolean exemptApplink ( android.content.Intent p0, java.util.List p1, java.util.List p2 ) {
/* .locals 5 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Intent;", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ResolveInfo;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ResolveInfo;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1709 */
/* .local p2, "candidates":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
/* .local p3, "result":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
v0 = (( android.content.Intent ) p1 ).isWebIntent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->isWebIntent()Z
if ( v0 != null) { // if-eqz v0, :cond_2
(( android.content.Intent ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1710 */
(( android.content.Intent ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;
(( android.net.Uri ) v0 ).getHost ( ); // invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;
final String v1 = "digitalkeypairing.org"; // const-string v1, "digitalkeypairing.org"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = /* .line 1711 */
/* .line 1712 */
/* .local v0, "size":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* .line 1713 */
/* check-cast v2, Landroid/content/pm/ResolveInfo; */
/* .line 1714 */
/* .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo; */
if ( v2 != null) { // if-eqz v2, :cond_0
v3 = this.activityInfo;
if ( v3 != null) { // if-eqz v3, :cond_0
v3 = this.activityInfo;
v3 = this.packageName;
/* .line 1715 */
final String v4 = "com.miui.tsmclient"; // const-string v4, "com.miui.tsmclient"
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1716 */
/* .line 1717 */
/* .line 1712 */
} // .end local v2 # "resolveInfo":Landroid/content/pm/ResolveInfo;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1720 */
} // .end local v1 # "i":I
} // :cond_1
v1 = } // :goto_1
/* if-lez v1, :cond_2 */
/* .line 1721 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1724 */
} // .end local v0 # "size":I
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.util.List getApplicationInfoBySelf ( Integer p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "flags" # J */
/* .param p5, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IIJI)", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ApplicationInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1763 */
v0 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 1764 */
/* .local v0, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1765 */
} // :cond_0
final String v3 = "getInstalledApplications"; // const-string v3, "getInstalledApplications"
v7 = /* invoke-direct {p0, p1, v2, v3}, Lcom/android/server/pm/PackageManagerServiceImpl;->isAllowedToGetInstalledApps(ILjava/lang/String;Ljava/lang/String;)Z */
/* .line 1767 */
/* .local v7, "allowed":Z */
/* if-nez v7, :cond_1 */
/* .line 1768 */
/* new-instance v8, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda1; */
/* move-object v1, v8 */
/* move-object v2, p0 */
/* move-object v3, v0 */
/* move-wide v4, p3 */
/* move v6, p5 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Lcom/android/server/pm/pkg/AndroidPackage;JI)V */
android.os.Binder .withCleanCallingIdentity ( v8 );
/* check-cast v1, Ljava/util/List; */
/* .line 1779 */
} // :cond_1
} // .end method
public Integer getDexOptResult ( ) {
/* .locals 1 */
/* .line 1513 */
v0 = this.mDexoptServiceThread;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1514 */
v0 = (( com.android.server.pm.DexoptServiceThread ) v0 ).getDexOptResult ( ); // invoke-virtual {v0}, Lcom/android/server/pm/DexoptServiceThread;->getDexOptResult()I
/* .line 1516 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getDexoptSecondaryResult ( ) {
/* .locals 1 */
/* .line 1527 */
v0 = this.mDexoptServiceThread;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1528 */
v0 = (( com.android.server.pm.DexoptServiceThread ) v0 ).getDexoptSecondaryResult ( ); // invoke-virtual {v0}, Lcom/android/server/pm/DexoptServiceThread;->getDexoptSecondaryResult()I
/* .line 1530 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
android.content.pm.ResolveInfo getGoogleWebSearchResolveInfo ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ResolveInfo;", */
/* ">;)", */
/* "Landroid/content/pm/ResolveInfo;" */
/* } */
} // .end annotation
/* .line 1122 */
/* .local p1, "riList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
v0 = if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez v0, :cond_0 */
/* .line 1125 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1126 */
/* .local v0, "ret":Landroid/content/pm/ResolveInfo; */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Landroid/content/pm/ResolveInfo; */
/* .line 1127 */
/* .local v2, "ri":Landroid/content/pm/ResolveInfo; */
if ( v2 != null) { // if-eqz v2, :cond_1
v3 = this.activityInfo;
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = this.activityInfo;
v3 = this.packageName;
final String v4 = "com.google.android.googlequicksearchbox"; // const-string v4, "com.google.android.googlequicksearchbox"
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1128 */
/* move-object v0, v2 */
/* .line 1129 */
/* .line 1131 */
} // .end local v2 # "ri":Landroid/content/pm/ResolveInfo;
} // :cond_1
/* .line 1132 */
} // :cond_2
} // :goto_1
/* .line 1123 */
} // .end local v0 # "ret":Landroid/content/pm/ResolveInfo;
} // :cond_3
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
android.content.pm.ResolveInfo getMarketResolveInfo ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ResolveInfo;", */
/* ">;)", */
/* "Landroid/content/pm/ResolveInfo;" */
/* } */
} // .end annotation
/* .line 1111 */
/* .local p1, "riList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 1112 */
/* .local v0, "ret":Landroid/content/pm/ResolveInfo; */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Landroid/content/pm/ResolveInfo; */
/* .line 1113 */
/* .local v2, "ri":Landroid/content/pm/ResolveInfo; */
v3 = this.activityInfo;
v3 = this.packageName;
final String v4 = "com.xiaomi.market"; // const-string v4, "com.xiaomi.market"
v3 = (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* iget-boolean v3, v2, Landroid/content/pm/ResolveInfo;->system:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1114 */
/* move-object v0, v2 */
/* .line 1115 */
/* .line 1117 */
} // .end local v2 # "ri":Landroid/content/pm/ResolveInfo;
} // :cond_0
/* .line 1118 */
} // :cond_1
} // :goto_1
} // .end method
public android.os.Bundle getPackageActivatedBundle ( java.lang.String p0, Integer p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "activatedPackage" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "clear" # Z */
/* .line 1806 */
/* const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
/* .line 1807 */
/* .line 1806 */
} // .end method
public java.util.List getPackageInfoBySelf ( Integer p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "flags" # J */
/* .param p5, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IIJI)", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/PackageInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1743 */
v0 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 1744 */
/* .local v0, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 1745 */
} // :cond_0
final String v3 = "getInstalledPackages"; // const-string v3, "getInstalledPackages"
v7 = /* invoke-direct {p0, p1, v2, v3}, Lcom/android/server/pm/PackageManagerServiceImpl;->isAllowedToGetInstalledApps(ILjava/lang/String;Ljava/lang/String;)Z */
/* .line 1747 */
/* .local v7, "allowed":Z */
/* if-nez v7, :cond_1 */
/* .line 1748 */
/* new-instance v8, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda4; */
/* move-object v1, v8 */
/* move-object v2, p0 */
/* move-object v3, v0 */
/* move-wide v4, p3 */
/* move v6, p5 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Lcom/android/server/pm/pkg/AndroidPackage;JI)V */
android.os.Binder .withCleanCallingIdentity ( v8 );
/* check-cast v1, Ljava/util/List; */
/* .line 1759 */
} // :cond_1
} // .end method
java.util.List getPersistentAppsForOtherUser ( Boolean p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "safeMode" # Z */
/* .param p2, "flags" # I */
/* .param p3, "userId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(ZII)", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ApplicationInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1665 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1666 */
/* .local v0, "finalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;" */
v1 = this.mPms;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1667 */
(( com.android.server.pm.PackageManagerService ) v1 ).snapshotComputer ( ); // invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* new-instance v3, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda5; */
/* invoke-direct {v3, p1, p2, p3, v0}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda5;-><init>(ZIILjava/util/ArrayList;)V */
(( com.android.server.pm.PackageManagerService ) v1 ).forEachPackageState ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PackageManagerService;->forEachPackageState(Lcom/android/server/pm/Computer;Ljava/util/function/Consumer;)V
/* .line 1684 */
} // :cond_0
} // .end method
public com.android.server.pm.PackageManagerService getService ( ) {
/* .locals 1 */
/* .line 1661 */
v0 = this.mPms;
} // .end method
public java.lang.String getSupportAonServicePackageName ( ) {
/* .locals 5 */
/* .line 2003 */
final String v0 = ""; // const-string v0, ""
/* .line 2004 */
/* .local v0, "supportAonServicePackageName":Ljava/lang/String; */
v1 = this.mIsSupportAttention;
v1 = v2 = miui.os.Build.DEVICE;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 2005 */
v1 = this.mPms;
/* .line 2006 */
(( com.android.server.pm.PackageManagerService ) v1 ).snapshotComputer ( ); // invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
v3 = this.mPms;
/* const v4, 0x104025b */
(( com.android.server.pm.PackageManagerService ) v3 ).getPackageFromComponentString ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/pm/PackageManagerService;->getPackageFromComponentString(I)Ljava/lang/String;
/* .line 2005 */
(( com.android.server.pm.PackageManagerService ) v1 ).ensureSystemPackageName ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/pm/PackageManagerService;->ensureSystemPackageName(Lcom/android/server/pm/Computer;Ljava/lang/String;)Ljava/lang/String;
/* .line 2009 */
} // :cond_0
} // .end method
public Boolean hasDomainVerificationRestriction ( ) {
/* .locals 2 */
/* .line 1829 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1830 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isProvisioned()Z */
/* .line 1831 */
/* .local v0, "isDeviceProvisioned":Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1832 */
/* .line 1834 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
} // .end method
android.content.pm.ResolveInfo hookChooseBestActivity ( android.content.Intent p0, java.lang.String p1, Long p2, java.util.List p3, Integer p4, android.content.pm.ResolveInfo p5 ) {
/* .locals 12 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "resolvedType" # Ljava/lang/String; */
/* .param p3, "flags" # J */
/* .param p6, "userId" # I */
/* .param p7, "defaultValue" # Landroid/content/pm/ResolveInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Intent;", */
/* "Ljava/lang/String;", */
/* "J", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/ResolveInfo;", */
/* ">;I", */
/* "Landroid/content/pm/ResolveInfo;", */
/* ")", */
/* "Landroid/content/pm/ResolveInfo;" */
/* } */
} // .end annotation
/* .line 1138 */
/* .local p5, "query":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
/* move-object v0, p0 */
/* move-object v7, p1 */
/* move-object/from16 v8, p5 */
/* if-nez v7, :cond_0 */
/* .line 1139 */
/* .line 1142 */
} // :cond_0
(( android.content.Intent ) p1 ).getScheme ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;
/* .line 1143 */
/* .local v9, "scheme":Ljava/lang/String; */
(( android.content.Intent ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;
if ( v1 != null) { // if-eqz v1, :cond_1
(( android.content.Intent ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;
(( android.net.Uri ) v1 ).getHost ( ); // invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* move-object v10, v1 */
/* .line 1144 */
/* .local v10, "host":Ljava/lang/String; */
/* sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
final String v2 = "android.intent.action.VIEW"; // const-string v2, "android.intent.action.VIEW"
/* if-nez v1, :cond_5 */
if ( v9 != null) { // if-eqz v9, :cond_5
final String v1 = "mimarket"; // const-string v1, "mimarket"
v1 = (( java.lang.String ) v9 ).equals ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_2 */
/* .line 1145 */
final String v1 = "market"; // const-string v1, "market"
v1 = (( java.lang.String ) v9 ).equals ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
} // :cond_2
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
if ( v10 != null) { // if-eqz v10, :cond_5
/* .line 1146 */
final String v1 = "details"; // const-string v1, "details"
v1 = (( java.lang.String ) v10 ).equals ( v1 ); // invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
/* const-string/jumbo v1, "search" */
v1 = (( java.lang.String ) v10 ).equals ( v1 ); // invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1147 */
} // :cond_3
(( com.android.server.pm.PackageManagerServiceImpl ) p0 ).getMarketResolveInfo ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/pm/PackageManagerServiceImpl;->getMarketResolveInfo(Ljava/util/List;)Landroid/content/pm/ResolveInfo;
/* .line 1148 */
/* .local v1, "ri":Landroid/content/pm/ResolveInfo; */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1149 */
/* .line 1151 */
} // .end local v1 # "ri":Landroid/content/pm/ResolveInfo;
} // :cond_4
/* goto/16 :goto_2 */
} // :cond_5
/* sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v1, :cond_8 */
final String v1 = "application/vnd.android.package-archive"; // const-string v1, "application/vnd.android.package-archive"
(( android.content.Intent ) p1 ).getType ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1152 */
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 1154 */
v1 = com.android.server.pm.PackageManagerServiceImpl .isCTS ( );
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1155 */
v1 = this.mCurrentPackageInstaller;
/* .line 1156 */
/* .local v1, "realPkgName":Ljava/lang/String; */
v2 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v2 ).snapshotComputer ( ); // invoke-virtual {v2}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
v3 = this.mCurrentPackageInstaller;
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 1157 */
v2 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v2 ).snapshotComputer ( ); // invoke-virtual {v2}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
v3 = this.mCurrentPackageInstaller;
/* move-object v11, v1 */
/* .line 1156 */
} // :cond_6
/* move-object v11, v1 */
/* .line 1160 */
} // .end local v1 # "realPkgName":Ljava/lang/String;
} // :cond_7
final String v1 = "com.miui.packageinstaller"; // const-string v1, "com.miui.packageinstaller"
/* move-object v11, v1 */
/* .line 1163 */
/* .local v11, "realPkgName":Ljava/lang/String; */
} // :goto_1
(( android.content.Intent ) p1 ).setPackage ( v11 ); // invoke-virtual {p1, v11}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1164 */
v1 = this.mPM;
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-wide v4, p3 */
/* move/from16 v6, p6 */
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->resolveIntent(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo; */
/* .line 1165 */
} // .end local v11 # "realPkgName":Ljava/lang/String;
} // :cond_8
/* sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 1166 */
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
final String v2 = "android.intent.action.WEB_SEARCH"; // const-string v2, "android.intent.action.WEB_SEARCH"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_9
v1 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isRsa4()Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 1167 */
(( com.android.server.pm.PackageManagerServiceImpl ) p0 ).getGoogleWebSearchResolveInfo ( v8 ); // invoke-virtual {p0, v8}, Lcom/android/server/pm/PackageManagerServiceImpl;->getGoogleWebSearchResolveInfo(Ljava/util/List;)Landroid/content/pm/ResolveInfo;
/* .line 1168 */
/* .local v1, "ri":Landroid/content/pm/ResolveInfo; */
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 1169 */
/* .line 1173 */
} // .end local v1 # "ri":Landroid/content/pm/ResolveInfo;
} // :cond_9
} // :goto_2
} // .end method
android.content.pm.PackageInfo hookPkgInfo ( android.content.pm.PackageInfo p0, java.lang.String p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "origPkgInfo" # Landroid/content/pm/PackageInfo; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "flags" # J */
/* .line 629 */
com.miui.hybrid.hook.HookClient .hookPkgInfo ( p1,p2,p3,p4 );
} // .end method
void init ( com.android.server.pm.PackageManagerService p0, com.android.server.pm.Settings p1, android.content.Context p2 ) {
/* .locals 5 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p2, "pkgSettings" # Lcom/android/server/pm/Settings; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 222 */
this.mPms = p1;
/* .line 223 */
v0 = this.mInjector;
(( com.android.server.pm.PackageManagerServiceInjector ) v0 ).getPackageDexOptimizer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceInjector;->getPackageDexOptimizer()Lcom/android/server/pm/PackageDexOptimizer;
this.mPdo = v0;
/* .line 224 */
this.mContext = p3;
/* .line 225 */
this.mPkgSettings = p2;
/* .line 226 */
/* new-instance v0, Lcom/android/server/pm/DexOptHelper; */
/* invoke-direct {v0, p1}, Lcom/android/server/pm/DexOptHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V */
this.mDexOptHelper = v0;
/* .line 227 */
v0 = this.mInjector;
(( com.android.server.pm.PackageManagerServiceInjector ) v0 ).getDexManager ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceInjector;->getDexManager()Lcom/android/server/pm/dex/DexManager;
this.mDexManager = v0;
/* .line 228 */
com.android.server.pm.MiuiPreinstallHelper .getInstance ( );
this.mMiuiPreinstallHelper = v0;
/* .line 229 */
/* new-instance v0, Lcom/android/server/pm/MiuiDexopt; */
v1 = this.mDexOptHelper;
/* invoke-direct {v0, p1, v1, p3}, Lcom/android/server/pm/MiuiDexopt;-><init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/DexOptHelper;Landroid/content/Context;)V */
this.mMiuiDexopt = v0;
/* .line 230 */
/* new-instance v0, Lcom/android/server/pm/PackageManagerCloudHelper; */
v1 = this.mPms;
v2 = this.mContext;
/* invoke-direct {v0, v1, v2}, Lcom/android/server/pm/PackageManagerCloudHelper;-><init>(Lcom/android/server/pm/PackageManagerService;Landroid/content/Context;)V */
this.mPackageManagerCloudHelper = v0;
/* .line 232 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 233 */
/* const v1, 0x11030054 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 232 */
java.util.Arrays .asList ( v0 );
this.mIsSupportAttention = v0;
/* .line 235 */
final String v0 = "pm.dexopt.async.enabled"; // const-string v0, "pm.dexopt.async.enabled"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 236 */
/* new-instance v0, Lcom/android/server/pm/DexoptServiceThread; */
v2 = this.mPms;
v3 = this.mPdo;
/* invoke-direct {v0, v2, v3}, Lcom/android/server/pm/DexoptServiceThread;-><init>(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageDexOptimizer;)V */
this.mDexoptServiceThread = v0;
/* .line 237 */
(( com.android.server.pm.DexoptServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/pm/DexoptServiceThread;->start()V
/* .line 240 */
} // :cond_0
/* const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
/* new-instance v2, Lcom/android/server/pm/PackageEventRecorder; */
v3 = com.android.server.pm.PackageManagerServiceImpl.PACKAGE_EVENT_DIR;
/* new-instance v4, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v4}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda2;-><init>()V */
/* invoke-direct {v2, v3, v4, v1}, Lcom/android/server/pm/PackageEventRecorder;-><init>(Ljava/io/File;Ljava/util/function/Function;Z)V */
com.android.server.LocalServices .addService ( v0,v2 );
/* .line 243 */
return;
} // .end method
public void initBeforeScanNonSystemApps ( ) {
/* .locals 0 */
/* .line 321 */
com.android.server.pm.CloudControlPreinstallService .startCloudControlService ( );
/* .line 322 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->updateDefaultPkgInstallerLocked()Z */
/* .line 323 */
(( com.android.server.pm.PackageManagerServiceImpl ) p0 ).checkGTSSpecAppOptMode ( ); // invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->checkGTSSpecAppOptMode()V
/* .line 324 */
return;
} // .end method
void initIPackageManagerImpl ( com.android.server.pm.PackageManagerService$IPackageManagerImpl p0 ) {
/* .locals 0 */
/* .param p1, "pm" # Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl; */
/* .line 246 */
this.mPM = p1;
/* .line 247 */
return;
} // .end method
public void initIgnoreApps ( ) {
/* .locals 9 */
/* .line 371 */
/* const-string/jumbo v0, "support_fm" */
int v1 = 1; // const/4 v1, 0x1
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 372 */
v0 = this.mIgnoreApks;
final String v1 = "/system/app/FM.apk"; // const-string v1, "/system/app/FM.apk"
/* .line 373 */
v0 = this.mIgnoreApks;
final String v1 = "/system/app/FM"; // const-string v1, "/system/app/FM"
/* .line 375 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->readIgnoreApks()V */
/* .line 378 */
v0 = this.mIgnorePackages;
final String v1 = "com.sogou.inputmethod.mi"; // const-string v1, "com.sogou.inputmethod.mi"
/* .line 380 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 381 */
/* const v1, 0x1105003b */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* .line 383 */
/* .local v0, "isCmccCooperationDevice":Z */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 384 */
/* const v2, 0x11050040 */
v1 = (( android.content.res.Resources ) v1 ).getBoolean ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v1, p0, Lcom/android/server/pm/PackageManagerServiceImpl;->mIsGlobalCrbtSupported:Z */
/* .line 389 */
/* sget-boolean v1, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v1, :cond_1 */
/* sget-boolean v1, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z */
/* if-nez v1, :cond_2 */
/* sget-boolean v1, Lmiui/os/Build;->IS_CT_CUSTOMIZATION:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* if-nez v0, :cond_2 */
/* .line 392 */
} // :cond_1
v1 = this.mIgnorePackages;
final String v2 = "com.miui.dmregservice"; // const-string v2, "com.miui.dmregservice"
/* .line 397 */
} // :cond_2
try { // :try_start_0
final String v1 = "ignoredAppsForPackages"; // const-string v1, "ignoredAppsForPackages"
v2 = this.mIgnorePackages;
com.android.server.pm.PackageManagerServiceImpl .addIgnoreApks ( v1,v2 );
/* .line 398 */
final String v1 = "ignoredAppsForApkPath"; // const-string v1, "ignoredAppsForApkPath"
v2 = this.mIgnoreApks;
com.android.server.pm.PackageManagerServiceImpl .addIgnoreApks ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 401 */
/* .line 399 */
/* :catch_0 */
/* move-exception v1 */
/* .line 400 */
/* .local v1, "e":Ljava/lang/NumberFormatException; */
final String v2 = "PKMSImpl"; // const-string v2, "PKMSImpl"
(( java.lang.NumberFormatException ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 404 */
} // .end local v1 # "e":Ljava/lang/NumberFormatException;
} // :goto_0
android.os.Environment .getProductDirectory ( );
(( java.io.File ) v1 ).getPath ( ); // invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;
/* .line 405 */
/* .local v1, "productPath":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "/priv-app/MiuiHome"; // const-string v3, "/priv-app/MiuiHome"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 406 */
/* .local v2, "MiuiHomePath":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "/priv-app/MiLauncherGlobal"; // const-string v4, "/priv-app/MiLauncherGlobal"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 407 */
/* .local v3, "MiLauncherGlobalPath":Ljava/lang/String; */
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v4 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 408 */
v4 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 409 */
final String v4 = "POCO"; // const-string v4, "POCO"
v5 = miui.os.Build.BRAND;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 410 */
v4 = this.mIgnoreApks;
/* .line 412 */
} // :cond_3
v4 = this.mIgnoreApks;
/* .line 417 */
} // :cond_4
} // :goto_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/app/MITSMClientNoneNfc"; // const-string v5, "/app/MITSMClientNoneNfc"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 418 */
/* .local v4, "MITSMClientNoneNfcPath":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "/app/MITSMClient"; // const-string v6, "/app/MITSMClient"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 419 */
/* .local v5, "MITSMClient":Ljava/lang/String; */
v6 = this.mPms;
final String v7 = "android.hardware.nfc"; // const-string v7, "android.hardware.nfc"
int v8 = 0; // const/4 v8, 0x0
v6 = (( com.android.server.pm.PackageManagerService ) v6 ).hasSystemFeature ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lcom/android/server/pm/PackageManagerService;->hasSystemFeature(Ljava/lang/String;I)Z
/* .line 420 */
/* .local v6, "isSupportNFC":Z */
/* new-instance v7, Ljava/io/File; */
/* invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v7 = (( java.io.File ) v7 ).exists ( ); // invoke-virtual {v7}, Ljava/io/File;->exists()Z
if ( v7 != null) { // if-eqz v7, :cond_6
/* new-instance v7, Ljava/io/File; */
/* invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v7 = (( java.io.File ) v7 ).exists ( ); // invoke-virtual {v7}, Ljava/io/File;->exists()Z
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 421 */
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 422 */
v7 = this.mIgnoreApks;
/* .line 424 */
} // :cond_5
v7 = this.mIgnoreApks;
/* .line 429 */
} // :cond_6
} // :goto_2
/* sget-boolean v7, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v7, :cond_8 */
/* .line 430 */
/* new-instance v7, Ljava/io/File; */
final String v8 = "/product/priv-app/GmsCore"; // const-string v8, "/product/priv-app/GmsCore"
/* invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v7 = (( java.io.File ) v7 ).exists ( ); // invoke-virtual {v7}, Ljava/io/File;->exists()Z
if ( v7 != null) { // if-eqz v7, :cond_7
/* .line 432 */
v7 = this.mIgnorePackages;
final String v8 = "android.ext.shared"; // const-string v8, "android.ext.shared"
/* .line 433 */
v7 = this.mIgnorePackages;
final String v8 = "com.android.printservice.recommendation"; // const-string v8, "com.android.printservice.recommendation"
/* .line 436 */
} // :cond_7
v7 = this.mIgnorePackages;
final String v8 = "com.google.android.gsf"; // const-string v8, "com.google.android.gsf"
/* .line 440 */
} // :cond_8
} // :goto_3
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->initCotaApps()V */
/* .line 441 */
return;
} // .end method
public Boolean isBaselineDisabled ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1739 */
v0 = this.mMiuiDexopt;
v0 = (( com.android.server.pm.MiuiDexopt ) v0 ).isBaselineDisabled ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiDexopt;->isBaselineDisabled(Ljava/lang/String;)Z
} // .end method
Boolean isCallerAllowedToSilentlyUninstall ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "callingUid" # I */
/* .line 723 */
v0 = this.mPms;
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 724 */
try { // :try_start_0
v1 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v1 ).snapshotComputer ( ); // invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v2, :cond_1 */
/* aget-object v5, v1, v4 */
/* .line 725 */
/* .local v5, "s":Ljava/lang/String; */
v6 = v6 = com.android.server.pm.PackageManagerServiceImpl.sSilentlyUninstallPackages;
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 726 */
v6 = this.mPms;
v6 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v6 ).get ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/pm/pkg/AndroidPackage; */
/* .line 727 */
/* .local v6, "pkgSetting":Lcom/android/server/pm/pkg/AndroidPackage; */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 728 */
v8 = v7 = android.os.UserHandle .getAppId ( p1 );
/* if-ne v7, v8, :cond_0 */
/* .line 729 */
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Allowed silently uninstall from callinguid:"; // const-string v3, "Allowed silently uninstall from callinguid:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 730 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 724 */
} // .end local v5 # "s":Ljava/lang/String;
} // .end local v6 # "pkgSetting":Lcom/android/server/pm/pkg/AndroidPackage;
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 734 */
} // :cond_1
/* monitor-exit v0 */
/* .line 736 */
/* .line 734 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean isMiuiStubPackage ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 748 */
int v0 = 0; // const/4 v0, 0x0
/* .line 749 */
/* .local v0, "pkgSetting":Lcom/android/server/pm/PackageSetting; */
v1 = this.mPms;
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 750 */
try { // :try_start_0
v2 = this.mPms;
v2 = this.mPackages;
(( com.android.server.utils.WatchedArrayMap ) v2 ).get ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/pm/pkg/AndroidPackage; */
/* .line 751 */
/* .local v2, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 752 */
v3 = this.mPms;
v3 = this.mSettings;
(( com.android.server.pm.Settings ) v3 ).getPackageLPr ( p1 ); // invoke-virtual {v3, p1}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
/* move-object v0, v3 */
/* .line 754 */
} // .end local v2 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
} // :cond_0
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 755 */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
(( com.android.server.pm.PackageSetting ) v0 ).getPkgState ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageSetting;->getPkgState()Lcom/android/server/pm/pkg/PackageStateUnserialized;
v2 = (( com.android.server.pm.pkg.PackageStateUnserialized ) v2 ).isUpdatedSystemApp ( ); // invoke-virtual {v2}, Lcom/android/server/pm/pkg/PackageStateUnserialized;->isUpdatedSystemApp()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 756 */
v2 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v2 ).snapshotComputer ( ); // invoke-virtual {v2}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* const-wide/32 v3, 0x200080 */
/* .line 760 */
/* .local v2, "packageInfo":Landroid/content/pm/PackageInfo; */
if ( v2 != null) { // if-eqz v2, :cond_2
v3 = this.applicationInfo;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 761 */
v3 = this.applicationInfo;
v3 = this.metaData;
/* .line 762 */
/* .local v3, "meta":Landroid/os/Bundle; */
if ( v3 != null) { // if-eqz v3, :cond_1
final String v4 = "com.miui.stub.install"; // const-string v4, "com.miui.stub.install"
v4 = (( android.os.Bundle ) v3 ).getBoolean ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
if ( v4 != null) { // if-eqz v4, :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* .line 765 */
} // .end local v2 # "packageInfo":Landroid/content/pm/PackageInfo;
} // .end local v3 # "meta":Landroid/os/Bundle;
} // :cond_2
/* .line 754 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
Boolean isPackageDeviceAdminEnabled ( ) {
/* .locals 1 */
/* .line 634 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
} // .end method
Boolean isPreinstallApp ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 334 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isSupportNewFrame ( ); // invoke-virtual {v0}, Lcom/android/server/pm/MiuiPreinstallHelper;->isSupportNewFrame()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 335 */
v0 = this.mMiuiPreinstallHelper;
v0 = (( com.android.server.pm.MiuiPreinstallHelper ) v0 ).isMiuiPreinstallApp ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/MiuiPreinstallHelper;->isMiuiPreinstallApp(Ljava/lang/String;)Z
/* .line 337 */
} // :cond_0
v0 = com.android.server.pm.PreinstallApp .isPreinstallApp ( p1 );
} // .end method
Boolean isVerificationEnabled ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "installerUid" # I */
/* .line 352 */
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->isProvisioned()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 353 */
/* .line 356 */
} // :cond_0
v0 = com.android.server.pm.PackageManagerServiceImpl .isCTS ( );
/* if-nez v0, :cond_2 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_2 */
/* .line 357 */
} // :cond_1
/* .line 359 */
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean needSkipDomainVerifier ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1950 */
/* sget-boolean v0, Lcom/android/server/pm/PackageManagerServiceImpl;->IS_INTERNATIONAL_BUILD:Z */
/* if-nez v0, :cond_0 */
v0 = this.mPms;
v0 = (( com.android.server.pm.PackageManagerService ) v0 ).isFirstBoot ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1951 */
final String v0 = "com.google.android.gms"; // const-string v0, "com.google.android.gms"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1952 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1954 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void performDexOptAsyncTask ( com.android.server.pm.dex.DexoptOptions p0 ) {
/* .locals 1 */
/* .param p1, "options" # Lcom/android/server/pm/dex/DexoptOptions; */
/* .line 1507 */
v0 = this.mDexoptServiceThread;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1508 */
(( com.android.server.pm.DexoptServiceThread ) v0 ).performDexOptAsyncTask ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/DexoptServiceThread;->performDexOptAsyncTask(Lcom/android/server/pm/dex/DexoptOptions;)V
/* .line 1510 */
} // :cond_0
return;
} // .end method
public void performDexOptSecondary ( android.content.pm.ApplicationInfo p0, java.lang.String p1, com.android.server.pm.dex.PackageDexUsage$DexUseInfo p2, com.android.server.pm.dex.DexoptOptions p3 ) {
/* .locals 1 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "dexUseInfo" # Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo; */
/* .param p4, "options" # Lcom/android/server/pm/dex/DexoptOptions; */
/* .line 1521 */
v0 = this.mDexoptServiceThread;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1522 */
(( com.android.server.pm.DexoptServiceThread ) v0 ).performDexOptSecondary ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/pm/DexoptServiceThread;->performDexOptSecondary(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;Lcom/android/server/pm/dex/PackageDexUsage$DexUseInfo;Lcom/android/server/pm/dex/DexoptOptions;)V
/* .line 1524 */
} // :cond_0
return;
} // .end method
void performPreinstallApp ( ) {
/* .locals 2 */
/* .line 316 */
v0 = this.mPms;
v1 = this.mPkgSettings;
com.android.server.pm.PreinstallApp .copyPreinstallApps ( v0,v1 );
/* .line 317 */
return;
} // .end method
public Integer preCheckUidPermission ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "permName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 895 */
v0 = android.os.UserHandle .getAppId ( p2 );
/* const/16 v1, 0x7d0 */
int v2 = 0; // const/4 v2, 0x0
/* if-ne v0, v1, :cond_1 */
v0 = com.android.server.pm.PackageManagerServiceImpl.sShellCheckPermissions;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 896 */
final String v0 = "persist.security.adbinput"; // const-string v0, "persist.security.adbinput"
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
/* if-nez v0, :cond_1 */
/* .line 897 */
final String v0 = "android.permission.CALL_PHONE"; // const-string v0, "android.permission.CALL_PHONE"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-class v0, Landroid/app/ActivityManagerInternal; */
/* .line 899 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/app/ActivityManagerInternal; */
v0 = (( android.app.ActivityManagerInternal ) v0 ).getCurrentUserId ( ); // invoke-virtual {v0}, Landroid/app/ActivityManagerInternal;->getCurrentUserId()I
/* const/16 v3, 0x6e */
/* if-ne v3, v0, :cond_0 */
/* .line 900 */
final String v0 = "preCheckUidPermission: permission granted call phone"; // const-string v0, "preCheckUidPermission: permission granted call phone"
android.util.Slog .d ( v1,v0 );
/* .line 901 */
/* .line 903 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "preCheckUidPermission: permission\u3000denied, perm="; // const-string v2, "preCheckUidPermission: permission\u3000denied, perm="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 904 */
int v0 = -1; // const/4 v0, -0x1
/* .line 906 */
} // :cond_1
} // .end method
public void processFirstUseActivity ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1473 */
/* new-instance v0, Lcom/android/server/pm/PackageManagerServiceImpl$3; */
/* invoke-direct {v0, p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl$3;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Ljava/lang/String;)V */
/* .line 1479 */
/* .local v0, "task":Ljava/lang/Runnable; */
com.android.server.pm.PackageManagerServiceImpl .getFirstUseHandler ( );
/* const-wide/16 v2, 0x2328 */
(( android.os.Handler ) v1 ).postDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 1480 */
return;
} // .end method
Boolean protectAppFromDeleting ( java.lang.String p0, android.content.pm.IPackageDeleteObserver2 p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 18 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "observer" # Landroid/content/pm/IPackageDeleteObserver2; */
/* .param p3, "callingUid" # I */
/* .param p4, "userId" # I */
/* .param p5, "deleteFlags" # I */
/* .line 777 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v8, p1 */
/* move-object/from16 v9, p2 */
/* move/from16 v10, p3 */
/* move/from16 v11, p4 */
/* move/from16 v12, p5 */
v0 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 778 */
/* .local v13, "ps":Lcom/android/server/pm/pkg/PackageStateInternal; */
v0 = if ( v13 != null) { // if-eqz v13, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 779 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 780 */
/* .local v0, "callingPid":I */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Uninstall pkg: "; // const-string v3, "Uninstall pkg: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " u"; // const-string v3, " u"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v11 ); // invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " flags:"; // const-string v3, " flags:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v12 ); // invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " from u"; // const-string v3, " from u"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "/"; // const-string v3, "/"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " of "; // const-string v3, " of "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 784 */
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 785 */
/* .local v2, "msg":Ljava/lang/String; */
com.android.server.pm.PackageManagerServiceUtilsStub .get ( );
int v4 = 3; // const/4 v4, 0x3
(( com.android.server.pm.PackageManagerServiceUtilsStub ) v3 ).logMiuiCriticalInfo ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Lcom/android/server/pm/PackageManagerServiceUtilsStub;->logMiuiCriticalInfo(ILjava/lang/String;)V
/* .line 789 */
} // .end local v0 # "callingPid":I
} // .end local v2 # "msg":Ljava/lang/String;
} // :cond_0
v0 = /* invoke-virtual/range {p0 ..p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->isMiuiStubPackage(Ljava/lang/String;)Z */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_6
v0 = this.mPms;
/* .line 790 */
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
final String v3 = "com.android.managedprovisioning"; // const-string v3, "com.android.managedprovisioning"
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_6 */
/* .line 791 */
/* and-int/lit8 v0, v12, 0x4 */
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* move v0, v2 */
/* .line 792 */
/* .local v0, "deleteSystem":Z */
} // :goto_0
/* and-int/lit8 v3, v12, 0x2 */
int v4 = -1; // const/4 v4, -0x1
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 793 */
/* move v3, v4 */
} // :cond_2
/* move v3, v11 */
} // :goto_1
/* move v15, v3 */
/* .line 796 */
/* .local v15, "removeUser":I */
/* if-eq v15, v4, :cond_4 */
/* if-nez v15, :cond_3 */
} // :cond_3
/* move v3, v2 */
} // :cond_4
} // :goto_2
int v3 = 1; // const/4 v3, 0x1
} // :goto_3
/* move/from16 v16, v3 */
/* .line 799 */
/* .local v16, "fullRemove":Z */
if ( v0 != null) { // if-eqz v0, :cond_5
if ( v16 != null) { // if-eqz v16, :cond_6
/* .line 800 */
} // :cond_5
v2 = this.mPms;
v7 = this.mHandler;
/* new-instance v6, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda6; */
/* move-object v2, v6 */
/* move-object/from16 v3, p1 */
/* move/from16 v4, p3 */
/* move/from16 v5, p4 */
/* move-object v14, v6 */
/* move v6, v0 */
/* move/from16 v17, v0 */
/* move-object v0, v7 */
} // .end local v0 # "deleteSystem":Z
/* .local v17, "deleteSystem":Z */
/* move-object/from16 v7, p2 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda6;-><init>(Ljava/lang/String;IIZLandroid/content/pm/IPackageDeleteObserver2;)V */
(( android.os.Handler ) v0 ).post ( v14 ); // invoke-virtual {v0, v14}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 810 */
int v2 = 1; // const/4 v2, 0x1
/* .line 815 */
} // .end local v15 # "removeUser":I
} // .end local v16 # "fullRemove":Z
} // .end local v17 # "deleteSystem":Z
} // :cond_6
v0 = this.mContext;
/* .line 816 */
v3 = /* invoke-static/range {p3 ..p3}, Landroid/os/UserHandle;->getUserId(I)I */
/* .line 815 */
v0 = com.miui.enterprise.ApplicationHelper .protectedFromDelete ( v0,v8,v3 );
final String v3 = "PKMSImpl"; // const-string v3, "PKMSImpl"
/* if-nez v0, :cond_c */
/* .line 817 */
v0 = miui.enterprise.ApplicationHelperStub .getInstance ( );
if ( v0 != null) { // if-eqz v0, :cond_7
/* goto/16 :goto_7 */
/* .line 833 */
} // :cond_7
v0 = if ( v13 != null) { // if-eqz v13, :cond_b
/* if-nez v0, :cond_b */
v0 = this.mContext;
/* .line 834 */
v0 = miui.content.pm.PreloadedAppPolicy .isProtectedDataApp ( v0,v8,v2 );
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 835 */
v4 = /* invoke-static/range {p3 ..p3}, Landroid/os/UserHandle;->getAppId(I)I */
/* .line 836 */
/* .local v4, "appId":I */
if ( v4 != null) { // if-eqz v4, :cond_b
/* const/16 v0, 0x3e8 */
/* if-eq v4, v0, :cond_b */
/* .line 837 */
int v0 = 1; // const/4 v0, 0x1
/* .line 838 */
/* .local v0, "isReject":Z */
miui.content.pm.PreloadedAppPolicy .getAllowDeleteSourceApps ( );
v6 = } // :goto_4
if ( v6 != null) { // if-eqz v6, :cond_9
/* check-cast v6, Ljava/lang/String; */
/* .line 839 */
/* .local v6, "allowPkg":Ljava/lang/String; */
v7 = this.mPms;
(( com.android.server.pm.PackageManagerService ) v7 ).snapshotComputer ( ); // invoke-virtual {v7}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
v7 = /* const-wide/16 v14, 0x0 */
/* if-ne v4, v7, :cond_8 */
/* .line 840 */
int v0 = 0; // const/4 v0, 0x0
/* .line 841 */
/* move v5, v0 */
/* .line 843 */
} // .end local v6 # "allowPkg":Ljava/lang/String;
} // :cond_8
/* .line 838 */
} // :cond_9
/* move v5, v0 */
/* .line 844 */
} // .end local v0 # "isReject":Z
/* .local v5, "isReject":Z */
} // :goto_5
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 846 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "MIUILOG- can\'t uninstall pkg : "; // const-string v2, "MIUILOG- can\'t uninstall pkg : "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " callingUid : "; // const-string v2, " callingUid : "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v0 );
/* .line 847 */
if ( v9 != null) { // if-eqz v9, :cond_a
/* instance-of v0, v9, Landroid/content/pm/IPackageDeleteObserver2; */
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 848 */
/* const/16 v0, -0x3e8 */
int v2 = 0; // const/4 v2, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 853 */
} // :cond_a
/* .line 851 */
/* :catch_0 */
/* move-exception v0 */
/* .line 854 */
} // :goto_6
int v2 = 1; // const/4 v2, 0x1
/* .line 859 */
} // .end local v4 # "appId":I
} // .end local v5 # "isReject":Z
} // :cond_b
/* .line 819 */
} // :cond_c
} // :goto_7
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Device is in enterprise mode, "; // const-string v2, "Device is in enterprise mode, "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " uninstallation is restricted by enterprise!"; // const-string v2, " uninstallation is restricted by enterprise!"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v0 );
/* .line 821 */
v0 = this.mPms;
v0 = this.mHandler;
/* new-instance v2, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda7; */
/* invoke-direct {v2, v9, v8}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda7;-><init>(Landroid/content/pm/IPackageDeleteObserver2;Ljava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 828 */
int v2 = 1; // const/4 v2, 0x1
} // .end method
public void recordPackageActivate ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "activatedPkg" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "sourcePkg" # Ljava/lang/String; */
/* .line 1795 */
v0 = this.mPms;
/* .line 1796 */
(( com.android.server.pm.PackageManagerService ) v0 ).snapshotComputer ( ); // invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;
/* .line 1795 */
v0 = com.android.server.pm.PackageEventRecorder .shouldRecordPackageActivate ( p1,p3,p2,v0 );
/* if-nez v0, :cond_0 */
/* .line 1797 */
return;
/* .line 1800 */
} // :cond_0
v0 = this.mPms;
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p1, p2, p3}, Lcom/android/server/pm/PackageManagerServiceImpl$$ExternalSyntheticLambda3;-><init>(Ljava/lang/String;ILjava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1803 */
return;
} // .end method
public void recordPackageRemove ( Integer[] p0, java.lang.String p1, java.lang.String p2, Boolean p3, android.os.Bundle p4 ) {
/* .locals 7 */
/* .param p1, "userIds" # [I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "installer" # Ljava/lang/String; */
/* .param p4, "isRemovedFully" # Z */
/* .param p5, "extras" # Landroid/os/Bundle; */
/* .line 1790 */
/* const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
com.android.server.LocalServices .getService ( v0 );
/* move-object v1, v0 */
/* check-cast v1, Lcom/android/server/pm/PackageEventRecorderInternal; */
/* .line 1791 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move v5, p4 */
/* move-object v6, p5 */
/* invoke-interface/range {v1 ..v6}, Lcom/android/server/pm/PackageEventRecorderInternal;->recordPackageRemove([ILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)V */
/* .line 1792 */
return;
} // .end method
public void recordPackageUpdate ( Integer[] p0, java.lang.String p1, java.lang.String p2, android.os.Bundle p3 ) {
/* .locals 1 */
/* .param p1, "userIds" # [I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .param p3, "installer" # Ljava/lang/String; */
/* .param p4, "extras" # Landroid/os/Bundle; */
/* .line 1784 */
/* const-class v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/PackageEventRecorderInternal; */
/* .line 1785 */
/* .line 1786 */
return;
} // .end method
void removePackageFromSharedUser ( com.android.server.pm.PackageSetting p0 ) {
/* .locals 1 */
/* .param p1, "ps" # Lcom/android/server/pm/PackageSetting; */
/* .line 1653 */
v0 = this.mPms;
v0 = this.mSettings;
(( com.android.server.pm.Settings ) v0 ).getSharedUserSettingLPr ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/Settings;->getSharedUserSettingLPr(Lcom/android/server/pm/PackageSetting;)Lcom/android/server/pm/SharedUserSetting;
/* .line 1654 */
/* .local v0, "sharedUserSetting":Lcom/android/server/pm/SharedUserSetting; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1655 */
(( com.android.server.pm.SharedUserSetting ) v0 ).removePackage ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/pm/SharedUserSetting;->removePackage(Lcom/android/server/pm/PackageSetting;)Z
/* .line 1657 */
} // :cond_0
return;
} // .end method
public Boolean setBaselineDisabled ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "disabled" # Z */
/* .line 1734 */
v0 = this.mMiuiDexopt;
v0 = (( com.android.server.pm.MiuiDexopt ) v0 ).setBaselineDisabled ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/pm/MiuiDexopt;->setBaselineDisabled(Ljava/lang/String;Z)Z
} // .end method
void setCallingPackage ( com.android.server.pm.PackageInstallerSession p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "session" # Lcom/android/server/pm/PackageInstallerSession; */
/* .param p2, "callingPackageName" # Ljava/lang/String; */
/* .line 1414 */
v0 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v0, :cond_0 */
/* .line 1415 */
(( com.android.server.pm.PackageInstallerSession ) p1 ).setCallingPackage ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/pm/PackageInstallerSession;->setCallingPackage(Ljava/lang/String;)V
/* .line 1416 */
return;
/* .line 1418 */
} // :cond_0
v0 = android.os.Binder .getCallingPid ( );
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 1419 */
/* .local v0, "realPkg":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_1
(( com.android.server.pm.PackageInstallerSession ) p1 ).getInstallerPackageName ( ); // invoke-virtual {p1}, Lcom/android/server/pm/PackageInstallerSession;->getInstallerPackageName()Ljava/lang/String;
} // :cond_1
/* move-object v1, v0 */
} // :goto_0
(( com.android.server.pm.PackageInstallerSession ) p1 ).setCallingPackage ( v1 ); // invoke-virtual {p1, v1}, Lcom/android/server/pm/PackageInstallerSession;->setCallingPackage(Ljava/lang/String;)V
/* .line 1420 */
return;
} // .end method
public void setDomainVerificationService ( com.android.server.pm.verify.domain.DomainVerificationService p0 ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/android/server/pm/verify/domain/DomainVerificationService; */
/* .line 1824 */
this.mDomainVerificationService = p1;
/* .line 1825 */
return;
} // .end method
Boolean shouldIgnoreApp ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "codePath" # Ljava/lang/String; */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 522 */
v0 = v0 = this.mIgnorePackages;
/* if-nez v0, :cond_1 */
v0 = this.mIgnoreApks;
v0 = /* .line 523 */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 524 */
/* .local v0, "ignored":Z */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 525 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Skip scanning package: "; // const-string v2, "Skip scanning package: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", path="; // const-string v2, ", path="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", reason: "; // const-string v2, ", reason: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "PKMSImpl"; // const-string v2, "PKMSImpl"
android.util.Slog .i ( v2,v1 );
/* .line 527 */
v1 = this.mPms;
v1 = this.mUserManager;
(( com.android.server.pm.UserManagerService ) v1 ).getUserIds ( ); // invoke-virtual {v1}, Lcom/android/server/pm/UserManagerService;->getUserIds()[I
/* .line 528 */
/* .local v1, "userIds":[I */
v2 = this.mPms;
v2 = this.mSettings;
(( com.android.server.pm.Settings ) v2 ).getPackageLPr ( p1 ); // invoke-virtual {v2, p1}, Lcom/android/server/pm/Settings;->getPackageLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
/* .line 531 */
/* .local v8, "ps":Lcom/android/server/pm/PackageSetting; */
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 532 */
(( com.android.server.pm.PackageSetting ) v8 ).getPath ( ); // invoke-virtual {v8}, Lcom/android/server/pm/PackageSetting;->getPath()Ljava/io/File;
if ( v2 != null) { // if-eqz v2, :cond_2
(( com.android.server.pm.PackageSetting ) v8 ).getPath ( ); // invoke-virtual {v8}, Lcom/android/server/pm/PackageSetting;->getPath()Ljava/io/File;
v2 = (( java.io.File ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 533 */
/* new-instance v2, Lcom/android/server/pm/RemovePackageHelper; */
v3 = this.mPms;
/* invoke-direct {v2, v3}, Lcom/android/server/pm/RemovePackageHelper;-><init>(Lcom/android/server/pm/PackageManagerService;)V */
/* .line 534 */
/* .local v2, "removePackageHelper":Lcom/android/server/pm/RemovePackageHelper; */
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move-object v3, v8 */
/* move-object v4, v1 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/pm/RemovePackageHelper;->removePackageDataLIF(Lcom/android/server/pm/PackageSetting;[ILcom/android/server/pm/PackageRemovedInfo;IZ)V */
/* .line 537 */
} // .end local v1 # "userIds":[I
} // .end local v2 # "removePackageHelper":Lcom/android/server/pm/RemovePackageHelper;
} // .end local v8 # "ps":Lcom/android/server/pm/PackageSetting;
} // :cond_2
} // .end method
public Boolean shouldSkipInstallForNewUser ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 1871 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p2, :cond_0 */
/* .line 1872 */
} // :cond_0
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldSkipInstall(Ljava/lang/String;)Z */
/* if-nez v1, :cond_1 */
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldSkipInstallCotaApp(Ljava/lang/String;)Z */
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_2
} // .end method
public Boolean shouldSkipInstallForUserType ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "userType" # Ljava/lang/String; */
/* .line 1877 */
final String v0 = "android.os.usertype.full.SYSTEM"; // const-string v0, "android.os.usertype.full.SYSTEM"
int v1 = 0; // const/4 v1, 0x0
/* if-ne p2, v0, :cond_0 */
/* .line 1878 */
} // :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldSkipInstall(Ljava/lang/String;)Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerServiceImpl;->shouldSkipInstallCotaApp(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
} // .end method
public void switchPackageInstaller ( ) {
/* .locals 9 */
/* .line 1297 */
try { // :try_start_0
v0 = com.android.server.pm.PackageManagerServiceImpl .isCTS ( );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1298 */
v0 = this.mPkgSettings;
v0 = this.mPackages;
final String v1 = "com.google.android.packageinstaller"; // const-string v1, "com.google.android.packageinstaller"
(( com.android.server.utils.WatchedArrayMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/pm/PackageSetting; */
/* .line 1299 */
/* .local v0, "googleInstaller":Lcom/android/server/pm/PackageSetting; */
v1 = this.mPkgSettings;
v1 = this.mPackages;
final String v2 = "com.android.packageinstaller"; // const-string v2, "com.android.packageinstaller"
(( com.android.server.utils.WatchedArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/pm/PackageSetting; */
/* .line 1300 */
/* .local v1, "androidInstaller":Lcom/android/server/pm/PackageSetting; */
int v2 = 0; // const/4 v2, 0x0
/* .line 1301 */
/* .local v2, "ctsInstallerPackageName":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1302 */
final String v3 = "com.android.packageinstaller"; // const-string v3, "com.android.packageinstaller"
/* move-object v2, v3 */
/* move-object v8, v2 */
/* .line 1303 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1304 */
final String v3 = "com.google.android.packageinstaller"; // const-string v3, "com.google.android.packageinstaller"
/* move-object v2, v3 */
/* move-object v8, v2 */
/* .line 1303 */
} // :cond_1
/* move-object v8, v2 */
/* .line 1306 */
} // .end local v2 # "ctsInstallerPackageName":Ljava/lang/String;
/* .local v8, "ctsInstallerPackageName":Ljava/lang/String; */
} // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 1307 */
v2 = this.mPM;
int v4 = 0; // const/4 v4, 0x0
/* const/16 v5, 0x4000 */
int v6 = 4; // const/4 v6, 0x4
int v7 = 0; // const/4 v7, 0x0
/* move-object v3, v8 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl;->installExistingPackageAsUser(Ljava/lang/String;IIILjava/util/List;)I */
/* .line 1310 */
} // .end local v0 # "googleInstaller":Lcom/android/server/pm/PackageSetting;
} // .end local v1 # "androidInstaller":Lcom/android/server/pm/PackageSetting;
} // .end local v8 # "ctsInstallerPackageName":Ljava/lang/String;
} // :cond_2
v0 = this.mPms;
v0 = this.mLock;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1311 */
try { // :try_start_1
v1 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->updateDefaultPkgInstallerLocked()Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1312 */
v1 = this.mPms;
v2 = this.mCurrentPackageInstaller;
this.mRequiredInstallerPackage = v2;
/* .line 1313 */
v1 = this.mPms;
v2 = this.mCurrentPackageInstaller;
this.mRequiredUninstallerPackage = v2;
/* .line 1314 */
v1 = this.mPms;
v1 = this.mPackages;
v2 = this.mPms;
v2 = this.mRequiredInstallerPackage;
(( com.android.server.utils.WatchedArrayMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/pm/pkg/AndroidPackage; */
/* .line 1315 */
/* .local v1, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
final String v2 = "permissionmgr"; // const-string v2, "permissionmgr"
android.os.ServiceManager .getService ( v2 );
/* check-cast v2, Lcom/android/server/pm/permission/PermissionManagerService; */
/* .line 1316 */
/* .local v2, "pms":Lcom/android/server/pm/permission/PermissionManagerService; */
final String v3 = "mPermissionManagerServiceImpl"; // const-string v3, "mPermissionManagerServiceImpl"
/* const-class v4, Lcom/android/server/pm/permission/PermissionManagerServiceImpl; */
miui.util.ReflectionUtils .getObjectField ( v2,v3,v4 );
/* check-cast v3, Lcom/android/server/pm/permission/PermissionManagerServiceImpl; */
/* .line 1318 */
/* .local v3, "impl":Lcom/android/server/pm/permission/PermissionManagerServiceImpl; */
/* const-string/jumbo v4, "updatePermissions" */
/* const-class v5, Ljava/lang/Void; */
int v6 = 2; // const/4 v6, 0x2
/* new-array v6, v6, [Ljava/lang/Object; */
v7 = this.mCurrentPackageInstaller;
int v8 = 0; // const/4 v8, 0x0
/* aput-object v7, v6, v8 */
int v7 = 1; // const/4 v7, 0x1
/* aput-object v1, v6, v7 */
miui.util.ReflectionUtils .callMethod ( v3,v4,v5,v6 );
/* .line 1320 */
} // .end local v1 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
} // .end local v2 # "pms":Lcom/android/server/pm/permission/PermissionManagerService;
} // .end local v3 # "impl":Lcom/android/server/pm/permission/PermissionManagerServiceImpl;
} // :cond_3
/* monitor-exit v0 */
/* .line 1323 */
/* .line 1320 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/pm/PackageManagerServiceImpl;
try { // :try_start_2
/* throw v1 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 1321 */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageManagerServiceImpl; */
/* :catch_0 */
/* move-exception v0 */
/* .line 1322 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "PKMSImpl"; // const-string v1, "PKMSImpl"
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 1325 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
void updateSystemAppDefaultStateForAllUsers ( ) {
/* .locals 6 */
/* .line 1637 */
com.android.server.SystemConfig .getInstance ( );
(( com.android.server.SystemConfig ) v0 ).getPackageDefaultState ( ); // invoke-virtual {v0}, Lcom/android/server/SystemConfig;->getPackageDefaultState()Landroid/util/ArrayMap;
/* .line 1638 */
/* .local v0, "defaultPkgState":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
v1 = (( android.util.ArrayMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1639 */
return;
/* .line 1641 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->doUpdateSystemAppDefaultUserStateForAllUsers()V */
/* .line 1642 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 1643 */
final String v2 = "miui_optimization"; // const-string v2, "miui_optimization"
android.provider.Settings$Secure .getUriFor ( v2 );
/* new-instance v3, Lcom/android/server/pm/PackageManagerServiceImpl$4; */
v4 = this.mPms;
v4 = this.mHandler;
/* invoke-direct {v3, p0, v4}, Lcom/android/server/pm/PackageManagerServiceImpl$4;-><init>(Lcom/android/server/pm/PackageManagerServiceImpl;Landroid/os/Handler;)V */
/* .line 1642 */
int v4 = 0; // const/4 v4, 0x0
int v5 = -1; // const/4 v5, -0x1
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v4, v3, v5 ); // invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 1649 */
return;
} // .end method
void updateSystemAppDefaultStateForUser ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "userId" # I */
/* .line 1611 */
v0 = com.android.server.pm.PackageManagerServiceImpl .isCTS ( );
/* .line 1612 */
/* .local v0, "isCTS":Z */
com.android.server.SystemConfig .getInstance ( );
(( com.android.server.SystemConfig ) v1 ).getPackageDefaultState ( ); // invoke-virtual {v1}, Lcom/android/server/SystemConfig;->getPackageDefaultState()Landroid/util/ArrayMap;
/* .line 1614 */
/* .local v1, "defaultPkgState":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
v2 = (( android.util.ArrayMap ) v1 ).isEmpty ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->isEmpty()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1615 */
return;
/* .line 1617 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
/* .local v3, "size":I */
} // :goto_0
/* if-ge v2, v3, :cond_2 */
/* .line 1618 */
(( android.util.ArrayMap ) v1 ).keyAt ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/String; */
/* .line 1619 */
/* .local v4, "pkg":Ljava/lang/String; */
int v5 = 0; // const/4 v5, 0x0
java.lang.Boolean .valueOf ( v5 );
(( android.util.ArrayMap ) v1 ).getOrDefault ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Landroid/util/ArrayMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Boolean; */
v5 = (( java.lang.Boolean ) v5 ).booleanValue ( ); // invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
/* xor-int/lit8 v5, v5, 0x1 */
/* .line 1620 */
/* .local v5, "disableByDefault":Z */
/* if-nez v5, :cond_1 */
/* .line 1621 */
/* .line 1623 */
} // :cond_1
/* invoke-direct {p0, p1, v0, v4}, Lcom/android/server/pm/PackageManagerServiceImpl;->updateSystemAppState(IZLjava/lang/String;)V */
/* .line 1617 */
} // .end local v4 # "pkg":Ljava/lang/String;
} // .end local v5 # "disableByDefault":Z
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1625 */
} // .end local v2 # "i":I
} // .end local v3 # "size":I
} // :cond_2
return;
} // .end method
public Boolean useMiuiDefaultCrossProfileIntentFilter ( ) {
/* .locals 2 */
/* .line 1842 */
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->hasGoogleContacts()Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->hasXiaomiContacts()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1843 */
/* .line 1846 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerServiceImpl;->hasXiaomiContacts()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1847 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1850 */
} // :cond_1
} // .end method
