public class com.android.server.pm.PackageManagerServiceCompat {
	 /* .source "PackageManagerServiceCompat.java" */
	 /* # direct methods */
	 public com.android.server.pm.PackageManagerServiceCompat ( ) {
		 /* .locals 0 */
		 /* .line 18 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static java.util.List getWakePathComponents ( java.lang.String p0 ) {
		 /* .locals 6 */
		 /* .param p0, "packageName" # Ljava/lang/String; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/lang/String;", */
		 /* ")", */
		 /* "Ljava/util/List<", */
		 /* "Lmiui/security/WakePathComponent;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 21 */
	 /* new-instance v0, Ljava/util/ArrayList; */
	 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 23 */
	 /* .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lmiui/security/WakePathComponent;>;" */
	 v1 = 	 android.text.TextUtils .isEmpty ( p0 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 24 */
		 /* .line 27 */
	 } // :cond_0
	 com.android.server.pm.PackageManagerServiceStub .get ( );
	 (( com.android.server.pm.PackageManagerServiceStub ) v1 ).getService ( ); // invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;
	 /* .line 28 */
	 /* .local v1, "service":Lcom/android/server/pm/PackageManagerService; */
	 v2 = this.mLock;
	 /* monitor-enter v2 */
	 /* .line 29 */
	 try { // :try_start_0
		 v3 = this.mPackages;
		 (( com.android.server.utils.WatchedArrayMap ) v3 ).get ( p0 ); // invoke-virtual {v3, p0}, Lcom/android/server/utils/WatchedArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
		 /* check-cast v3, Lcom/android/server/pm/pkg/AndroidPackage; */
		 /* .line 30 */
		 /* .local v3, "pkg":Lcom/android/server/pm/pkg/AndroidPackage; */
		 /* if-nez v3, :cond_1 */
		 /* .line 31 */
		 /* monitor-exit v2 */
		 /* .line 34 */
	 } // :cond_1
	 int v5 = 3; // const/4 v5, 0x3
	 com.android.server.pm.PackageManagerServiceCompat .parsePkgCompentLock ( v0,v4,v5 );
	 /* .line 35 */
	 int v5 = 1; // const/4 v5, 0x1
	 com.android.server.pm.PackageManagerServiceCompat .parsePkgCompentLock ( v0,v4,v5 );
	 /* .line 36 */
	 int v5 = 4; // const/4 v5, 0x4
	 com.android.server.pm.PackageManagerServiceCompat .parsePkgCompentLock ( v0,v4,v5 );
	 /* .line 37 */
	 int v5 = 2; // const/4 v5, 0x2
	 com.android.server.pm.PackageManagerServiceCompat .parsePkgCompentLock ( v0,v4,v5 );
	 /* .line 38 */
} // .end local v3 # "pkg":Lcom/android/server/pm/pkg/AndroidPackage;
/* monitor-exit v2 */
/* .line 39 */
/* .line 38 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
private static void parsePkgCompentLock ( java.util.List p0, java.util.List p1, Integer p2 ) {
/* .locals 6 */
/* .param p2, "componentType" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lmiui/security/WakePathComponent;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "+", */
/* "Lcom/android/server/pm/pkg/component/ParsedMainComponent;", */
/* ">;I)V" */
/* } */
} // .end annotation
/* .line 45 */
/* .local p0, "wakePathComponents":Ljava/util/List;, "Ljava/util/List<Lmiui/security/WakePathComponent;>;" */
/* .local p1, "components":Ljava/util/List;, "Ljava/util/List<+Lcom/android/server/pm/pkg/component/ParsedMainComponent;>;" */
if ( p0 != null) { // if-eqz p0, :cond_5
/* if-nez p1, :cond_0 */
/* .line 49 */
v0 = } // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_4 */
/* .line 50 */
v1 = /* check-cast v1, Lcom/android/server/pm/pkg/component/ParsedMainComponent; */
/* if-nez v1, :cond_1 */
/* .line 51 */
/* .line 54 */
} // :cond_1
/* new-instance v1, Lmiui/security/WakePathComponent; */
/* invoke-direct {v1}, Lmiui/security/WakePathComponent;-><init>()V */
/* .line 55 */
/* .local v1, "wakePathComponent":Lmiui/security/WakePathComponent; */
(( miui.security.WakePathComponent ) v1 ).setType ( p2 ); // invoke-virtual {v1, p2}, Lmiui/security/WakePathComponent;->setType(I)V
/* .line 56 */
/* check-cast v2, Lcom/android/server/pm/pkg/component/ParsedMainComponent; */
(( miui.security.WakePathComponent ) v1 ).setClassname ( v2 ); // invoke-virtual {v1, v2}, Lmiui/security/WakePathComponent;->setClassname(Ljava/lang/String;)V
/* .line 57 */
v2 = /* check-cast v2, Lcom/android/server/pm/pkg/component/ParsedMainComponent; */
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "j":I */
} // :goto_1
/* if-ltz v2, :cond_3 */
/* .line 58 */
/* check-cast v3, Lcom/android/server/pm/pkg/component/ParsedMainComponent; */
/* check-cast v3, Lcom/android/server/pm/pkg/component/ParsedIntentInfo; */
/* .line 59 */
/* .local v3, "intentFilter":Landroid/content/IntentFilter; */
v4 = (( android.content.IntentFilter ) v3 ).countActions ( ); // invoke-virtual {v3}, Landroid/content/IntentFilter;->countActions()I
/* add-int/lit8 v4, v4, -0x1 */
/* .local v4, "k":I */
} // :goto_2
/* if-ltz v4, :cond_2 */
/* .line 60 */
(( android.content.IntentFilter ) v3 ).getAction ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->getAction(I)Ljava/lang/String;
(( miui.security.WakePathComponent ) v1 ).addIntentAction ( v5 ); // invoke-virtual {v1, v5}, Lmiui/security/WakePathComponent;->addIntentAction(Ljava/lang/String;)V
/* .line 59 */
/* add-int/lit8 v4, v4, -0x1 */
/* .line 57 */
} // .end local v3 # "intentFilter":Landroid/content/IntentFilter;
} // .end local v4 # "k":I
} // :cond_2
/* add-int/lit8 v2, v2, -0x1 */
/* .line 63 */
} // .end local v2 # "j":I
} // :cond_3
/* .line 49 */
} // .end local v1 # "wakePathComponent":Lmiui/security/WakePathComponent;
} // :goto_3
/* add-int/lit8 v0, v0, -0x1 */
/* .line 65 */
} // .end local v0 # "i":I
} // :cond_4
return;
/* .line 46 */
} // :cond_5
} // :goto_4
return;
} // .end method
