.class public Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;
.super Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;
.source "MiuiDefaultPermissionGrantPolicy.java"


# static fields
.field private static final DEFAULT_PACKAGE_INFO_QUERY_FLAGS:I = 0x2000b080

.field private static final GRANT_RUNTIME_VERSION:Ljava/lang/String; = "persist.sys.grant_version"

.field private static final INCALL_UI:Ljava/lang/String; = "com.android.incallui"

.field private static final MIUI_GLOBAL_APPS:[Ljava/lang/String;

.field public static final MIUI_SYSTEM_APPS:[Ljava/lang/String;

.field private static final REQUIRED_PERMISSIONS:Ljava/lang/String; = "required_permissions"

.field private static final RUNTIME_PERMISSIONS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final RUNTIME_PERMSSION_PROPTERY:Ljava/lang/String; = "persist.sys.runtime_perm"

.field private static final STATE_DEF:I = -0x1

.field private static final STATE_GRANT:I = 0x0

.field private static final STATE_REVOKE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "DefaultPermGrantPolicyI"

.field public static final sAllowAutoStartForOTAPkgs:[Ljava/lang/String;

.field private static final sMiuiAppDefaultGrantedPermissions:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 62
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    .line 63
    const-string v0, "com.xiaomi.finddevice"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->sAllowAutoStartForOTAPkgs:[Ljava/lang/String;

    .line 83
    sget-object v1, Lmiui/content/pm/ExtraPackageManager;->MIUI_SYSTEM_APPS:[Ljava/lang/String;

    sput-object v1, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->MIUI_SYSTEM_APPS:[Ljava/lang/String;

    .line 85
    const-string v1, "co.sitic.pp"

    const-string v2, "com.miui.backup"

    filled-new-array {v1, v2, v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->MIUI_GLOBAL_APPS:[Ljava/lang/String;

    .line 91
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->sMiuiAppDefaultGrantedPermissions:Landroid/util/ArrayMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/android/server/pm/permission/DefaultPermissionGrantPolicyStub;-><init>()V

    return-void
.end method

.method private static doesPackageSupportRuntimePermissions(Landroid/content/pm/PackageInfo;)Z
    .locals 2
    .param p0, "pkg"    # Landroid/content/pm/PackageInfo;

    .line 159
    iget-object v0, p0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v1, 0x16

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static declared-synchronized ensureDangerousSetInit()V
    .locals 7

    const-class v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;

    monitor-enter v0

    .line 73
    :try_start_0
    sget-object v1, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v1, :cond_0

    .line 74
    monitor-exit v0

    return-void

    .line 76
    :cond_0
    :try_start_1
    const-class v1, Lcom/android/server/pm/permission/PermissionManagerServiceInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/permission/PermissionManagerServiceInternal;

    .line 77
    .local v1, "pm":Lcom/android/server/pm/permission/PermissionManagerServiceInternal;
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/android/server/pm/permission/PermissionManagerServiceInternal;->getAllPermissionsWithProtection(I)Ljava/util/List;

    move-result-object v2

    .line 78
    .local v2, "dangerous":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PermissionInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PermissionInfo;

    .line 79
    .local v4, "permissionInfo":Landroid/content/pm/PermissionInfo;
    sget-object v5, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    iget-object v6, v4, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    nop

    .end local v4    # "permissionInfo":Landroid/content/pm/PermissionInfo;
    goto :goto_0

    .line 81
    :cond_1
    monitor-exit v0

    return-void

    .line 72
    .end local v1    # "pm":Lcom/android/server/pm/permission/PermissionManagerServiceInternal;
    .end local v2    # "dangerous":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PermissionInfo;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static grantIncallUiPermission(Lcom/android/server/pm/PackageManagerService;I)V
    .locals 7
    .param p0, "service"    # Lcom/android/server/pm/PackageManagerService;
    .param p1, "userId"    # I

    .line 302
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 303
    .local v0, "perms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    const-string v1, "android.permission.POST_NOTIFICATIONS"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    invoke-static {}, Landroid/app/AppGlobals;->getPermissionManager()Landroid/permission/IPermissionManager;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/permission/PermissionManagerService;

    .line 307
    .local v1, "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 308
    .local v3, "p":Ljava/lang/String;
    const-string v4, "com.android.incallui"

    invoke-virtual {p0, v3, v4, p1}, Lcom/android/server/pm/PackageManagerService;->checkPermission(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    .line 309
    .local v5, "result":I
    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 310
    invoke-virtual {v1, v4, v3, p1}, Lcom/android/server/pm/permission/PermissionManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V

    .line 312
    .end local v3    # "p":Ljava/lang/String;
    .end local v5    # "result":I
    :cond_0
    goto :goto_0

    .line 313
    :cond_1
    return-void
.end method

.method private static grantPermissionsForCTS(I)V
    .locals 11
    .param p0, "userId"    # I

    .line 100
    const-string v0, "ro.miui.customized.region"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "miuiCustomizedRegion":Ljava/lang/String;
    const-string v1, "lm_cr"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mx_telcel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    :cond_0
    const/4 v1, 0x2

    .line 103
    .local v1, "newFlag":I
    const-string v9, "co.sitic.pp"

    .line 104
    .local v9, "packageName":Ljava/lang/String;
    invoke-static {}, Landroid/app/AppGlobals;->getPermissionManager()Landroid/permission/IPermissionManager;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/android/server/pm/permission/PermissionManagerService;

    .line 105
    .local v10, "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
    const-string v4, "android.permission.READ_PHONE_STATE"

    const/4 v7, 0x1

    move-object v2, v10

    move-object v3, v9

    move v5, v1

    move v6, v1

    move v8, p0

    invoke-virtual/range {v2 .. v8}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V

    .line 106
    const-string v4, "android.permission.RECEIVE_SMS"

    invoke-virtual/range {v2 .. v8}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V

    .line 107
    const-string v4, "android.permission.CALL_PHONE"

    invoke-virtual/range {v2 .. v8}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V

    .line 108
    const-string v2, "DefaultPermGrantPolicyI"

    const-string v3, "grant permissions for Sysdll because of CTS"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    .end local v1    # "newFlag":I
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
    :cond_1
    return-void
.end method

.method public static grantRuntimePermission(Ljava/lang/String;I)V
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "userId"    # I

    .line 295
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceStub;->get()Lcom/android/server/pm/PackageManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;

    move-result-object v0

    .line 296
    .local v0, "service":Lcom/android/server/pm/PackageManagerService;
    const-string v1, "com.android.incallui"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297
    invoke-static {v0, p1}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->grantIncallUiPermission(Lcom/android/server/pm/PackageManagerService;I)V

    .line 299
    :cond_0
    return-void
.end method

.method private static grantRuntimePermissionsLPw(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ZI)V
    .locals 24
    .param p0, "service"    # Lcom/android/server/pm/PackageManagerService;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "overrideUserChoice"    # Z
    .param p3, "userId"    # I

    .line 163
    move-object/from16 v0, p0

    move/from16 v8, p3

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v1

    const-wide/32 v2, 0x2000b080

    move-object/from16 v9, p1

    invoke-interface {v1, v9, v2, v3, v8}, Lcom/android/server/pm/Computer;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v10

    .line 164
    .local v10, "pkg":Landroid/content/pm/PackageInfo;
    if-eqz v10, :cond_17

    invoke-static {v10}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->doesPackageSupportRuntimePermissions(Landroid/content/pm/PackageInfo;)Z

    move-result v1

    if-eqz v1, :cond_17

    iget-object v1, v10, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v1, :cond_17

    iget-object v1, v10, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v1, v1

    if-nez v1, :cond_0

    goto/16 :goto_8

    .line 167
    :cond_0
    invoke-static {}, Landroid/app/AppGlobals;->getPermissionManager()Landroid/permission/IPermissionManager;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/android/server/pm/permission/PermissionManagerService;

    .line 168
    .local v11, "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
    iget-object v1, v10, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 169
    .local v1, "requestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 170
    .local v2, "requiredPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v12, v1

    .line 171
    .local v12, "allRequestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, ";"

    const-string v5, "required_permissions"

    if-eqz v3, :cond_1

    .line 172
    iget-object v3, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 173
    .local v3, "declareStr":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 174
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 177
    .end local v3    # "declareStr":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    .line 179
    .local v3, "grantablePermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v6, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6}, Landroid/content/pm/ApplicationInfo;->isUpdatedSystemApp()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 180
    iget-object v6, v0, Lcom/android/server/pm/PackageManagerService;->mSettings:Lcom/android/server/pm/Settings;

    iget-object v7, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/android/server/pm/Settings;->getDisabledSystemPkgLPr(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;

    move-result-object v6

    .line 181
    .local v6, "sysPs":Lcom/android/server/pm/PackageSetting;
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 182
    invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v7

    invoke-interface {v7}, Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;->getRequestedPermissions()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 183
    return-void

    .line 185
    :cond_2
    invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v7

    invoke-interface {v7}, Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;->getRequestedPermissions()Ljava/util/List;

    move-result-object v7

    .line 186
    .local v7, "disablePermissionsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 187
    invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v13

    invoke-interface {v13}, Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;->getMetaData()Landroid/os/Bundle;

    move-result-object v13

    if-eqz v13, :cond_3

    .line 188
    invoke-virtual {v6}, Lcom/android/server/pm/PackageSetting;->getPkg()Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;

    move-result-object v13

    invoke-interface {v13}, Lcom/android/server/pm/parsing/pkg/AndroidPackageInternal;->getMetaData()Landroid/os/Bundle;

    move-result-object v13

    invoke-virtual {v13, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 189
    .local v5, "disableStr":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 190
    invoke-virtual {v5, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 193
    .end local v5    # "disableStr":Ljava/lang/String;
    :cond_3
    invoke-interface {v1, v7}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 194
    new-instance v4, Landroid/util/ArraySet;

    invoke-direct {v4, v1}, Landroid/util/ArraySet;-><init>(Ljava/util/Collection;)V

    move-object v3, v4

    .line 195
    move-object v1, v7

    move-object v13, v1

    move-object v14, v2

    move-object v15, v3

    goto :goto_0

    .line 193
    :cond_4
    move-object v13, v1

    move-object v14, v2

    move-object v15, v3

    goto :goto_0

    .line 200
    .end local v6    # "sysPs":Lcom/android/server/pm/PackageSetting;
    .end local v7    # "disablePermissionsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    move-object v13, v1

    move-object v14, v2

    move-object v15, v3

    .end local v1    # "requestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "requiredPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "grantablePermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local v13, "requestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v14, "requiredPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v15, "grantablePermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v7

    .line 201
    .local v7, "grantablePermissionCount":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v6, v1

    .line 202
    .local v6, "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    move v5, v1

    .local v5, "i":I
    :goto_1
    if-ge v5, v7, :cond_15

    .line 203
    invoke-interface {v13, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ljava/lang/String;

    .line 207
    .local v4, "permission":Ljava/lang/String;
    if-eqz v15, :cond_6

    invoke-interface {v15, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 208
    move/from16 v21, v5

    move-object/from16 v22, v6

    move/from16 v17, v7

    goto/16 :goto_6

    .line 211
    :cond_6
    invoke-static {v4}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->isDangerousPermission(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-interface {v12, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 212
    iget-object v1, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v4, v1, v8}, Lcom/android/server/pm/permission/PermissionManagerService;->getPermissionFlags(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    .line 213
    .local v3, "flags":I
    if-eqz v14, :cond_8

    invoke-interface {v14, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 214
    and-int/lit8 v1, v3, 0x20

    if-eqz v1, :cond_7

    .line 215
    const/16 v16, 0x1032

    .line 217
    .local v16, "revokeFlags":I
    iget-object v2, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/16 v17, 0x0

    const/16 v18, 0x1

    move-object v1, v11

    move/from16 v19, v3

    .end local v3    # "flags":I
    .local v19, "flags":I
    move-object v3, v4

    move-object/from16 v20, v4

    .end local v4    # "permission":Ljava/lang/String;
    .local v20, "permission":Ljava/lang/String;
    move/from16 v4, v16

    move/from16 v21, v5

    .end local v5    # "i":I
    .local v21, "i":I
    move/from16 v5, v17

    move-object/from16 v22, v6

    .end local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v22, "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move/from16 v6, v18

    move/from16 v17, v7

    .end local v7    # "grantablePermissionCount":I
    .local v17, "grantablePermissionCount":I
    move/from16 v7, p3

    invoke-virtual/range {v1 .. v7}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V

    .line 219
    .end local v16    # "revokeFlags":I
    goto/16 :goto_6

    .line 214
    .end local v17    # "grantablePermissionCount":I
    .end local v19    # "flags":I
    .end local v20    # "permission":Ljava/lang/String;
    .end local v21    # "i":I
    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3    # "flags":I
    .restart local v4    # "permission":Ljava/lang/String;
    .restart local v5    # "i":I
    .restart local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "grantablePermissionCount":I
    :cond_7
    move/from16 v19, v3

    move-object/from16 v20, v4

    move/from16 v21, v5

    move-object/from16 v22, v6

    move/from16 v17, v7

    .end local v3    # "flags":I
    .end local v4    # "permission":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "grantablePermissionCount":I
    .restart local v17    # "grantablePermissionCount":I
    .restart local v19    # "flags":I
    .restart local v20    # "permission":Ljava/lang/String;
    .restart local v21    # "i":I
    .restart local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto/16 :goto_6

    .line 213
    .end local v17    # "grantablePermissionCount":I
    .end local v19    # "flags":I
    .end local v20    # "permission":Ljava/lang/String;
    .end local v21    # "i":I
    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3    # "flags":I
    .restart local v4    # "permission":Ljava/lang/String;
    .restart local v5    # "i":I
    .restart local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "grantablePermissionCount":I
    :cond_8
    move/from16 v19, v3

    move-object/from16 v20, v4

    move/from16 v21, v5

    move-object/from16 v22, v6

    move/from16 v17, v7

    .line 222
    .end local v3    # "flags":I
    .end local v4    # "permission":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "grantablePermissionCount":I
    .restart local v17    # "grantablePermissionCount":I
    .restart local v19    # "flags":I
    .restart local v20    # "permission":Ljava/lang/String;
    .restart local v21    # "i":I
    .restart local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static/range {v19 .. v19}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->isUserChanged(I)Z

    move-result v1

    if-eqz v1, :cond_9

    if-nez p2, :cond_9

    invoke-static/range {v19 .. v19}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->isOTAUpdated(I)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 226
    :cond_9
    move/from16 v7, v19

    .end local v19    # "flags":I
    .local v7, "flags":I
    and-int/lit8 v1, v7, 0x4

    if-eqz v1, :cond_a

    .line 227
    goto/16 :goto_6

    .line 229
    :cond_a
    const/16 v16, 0x0

    .line 230
    .local v16, "updateSystemFixed":Z
    and-int/lit8 v1, v7, 0x10

    const/4 v6, -0x1

    if-eqz v1, :cond_c

    .line 231
    iget-object v1, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v5, v20

    .end local v20    # "permission":Ljava/lang/String;
    .local v5, "permission":Ljava/lang/String;
    invoke-virtual {v0, v5, v1, v8}, Lcom/android/server/pm/PackageManagerService;->checkPermission(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v6, :cond_b

    .line 232
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is required but is deny now, grant again"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DefaultPermGrantPolicyI"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v2, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/16 v4, 0x10

    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object v1, v11

    move-object v3, v5

    move-object/from16 v23, v5

    .end local v5    # "permission":Ljava/lang/String;
    .local v23, "permission":Ljava/lang/String;
    move/from16 v5, v18

    move/from16 v6, v19

    move v9, v7

    .end local v7    # "flags":I
    .local v9, "flags":I
    move/from16 v7, p3

    invoke-virtual/range {v1 .. v7}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V

    .line 236
    const/16 v16, 0x1

    goto :goto_2

    .line 231
    .end local v9    # "flags":I
    .end local v23    # "permission":Ljava/lang/String;
    .restart local v5    # "permission":Ljava/lang/String;
    .restart local v7    # "flags":I
    :cond_b
    move-object/from16 v23, v5

    move v9, v7

    .end local v5    # "permission":Ljava/lang/String;
    .end local v7    # "flags":I
    .restart local v9    # "flags":I
    .restart local v23    # "permission":Ljava/lang/String;
    goto/16 :goto_6

    .line 230
    .end local v9    # "flags":I
    .end local v23    # "permission":Ljava/lang/String;
    .restart local v7    # "flags":I
    .restart local v20    # "permission":Ljava/lang/String;
    :cond_c
    move v9, v7

    move-object/from16 v23, v20

    .line 242
    .end local v7    # "flags":I
    .end local v20    # "permission":Ljava/lang/String;
    .restart local v9    # "flags":I
    .restart local v23    # "permission":Ljava/lang/String;
    :goto_2
    if-nez v8, :cond_d

    iget-object v1, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 243
    move-object/from16 v7, v23

    .end local v23    # "permission":Ljava/lang/String;
    .local v7, "permission":Ljava/lang/String;
    invoke-virtual {v0, v7, v1, v8}, Lcom/android/server/pm/PackageManagerService;->checkPermission(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_e

    iget-object v1, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v2, 0x21

    if-ge v1, v2, :cond_f

    .line 244
    const-string v1, "android.permission.POST_NOTIFICATIONS"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    goto :goto_3

    .line 242
    .end local v7    # "permission":Ljava/lang/String;
    .restart local v23    # "permission":Ljava/lang/String;
    :cond_d
    move-object/from16 v7, v23

    .line 244
    .end local v23    # "permission":Ljava/lang/String;
    .restart local v7    # "permission":Ljava/lang/String;
    :cond_e
    :goto_3
    nop

    .line 245
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 246
    :cond_f
    move-object/from16 v6, v22

    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 245
    .end local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_10
    move-object/from16 v6, v22

    .line 248
    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_4
    const/16 v1, 0x20

    .line 249
    .local v1, "newFlags":I
    and-int/lit16 v2, v9, 0x3800

    or-int v18, v1, v2

    .line 251
    .end local v1    # "newFlags":I
    .local v18, "newFlags":I
    iget-object v1, v0, Lcom/android/server/pm/PackageManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getOpPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v11, v7, v1, v2}, Lcom/android/server/pm/permission/PermissionManagerService;->getPermissionInfo(Ljava/lang/String;Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/PermissionInfo;->isRestricted()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 252
    iget-object v2, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/16 v4, 0x1000

    const/16 v5, 0x1000

    const/16 v19, 0x1

    move-object v1, v11

    move-object v3, v7

    move-object/from16 v22, v6

    .end local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move/from16 v6, v19

    move-object v0, v7

    .end local v7    # "permission":Ljava/lang/String;
    .local v0, "permission":Ljava/lang/String;
    move/from16 v7, p3

    invoke-virtual/range {v1 .. v7}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V

    goto :goto_5

    .line 251
    .end local v0    # "permission":Ljava/lang/String;
    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "permission":Ljava/lang/String;
    :cond_11
    move-object/from16 v22, v6

    move-object v0, v7

    .line 256
    .end local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "permission":Ljava/lang/String;
    .restart local v0    # "permission":Ljava/lang/String;
    .restart local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_5
    iget-object v1, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v1, v0, v8}, Lcom/android/server/pm/permission/PermissionManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V

    .line 258
    if-eqz v16, :cond_12

    .line 259
    or-int/lit8 v18, v18, 0x10

    .line 262
    :cond_12
    iget-object v2, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/4 v6, 0x1

    move-object v1, v11

    move-object v3, v0

    move/from16 v4, v18

    move/from16 v5, v18

    move/from16 v7, p3

    invoke-virtual/range {v1 .. v7}, Lcom/android/server/pm/permission/PermissionManagerService;->updatePermissionFlags(Ljava/lang/String;Ljava/lang/String;IIZI)V

    goto :goto_6

    .line 211
    .end local v0    # "permission":Ljava/lang/String;
    .end local v9    # "flags":I
    .end local v16    # "updateSystemFixed":Z
    .end local v17    # "grantablePermissionCount":I
    .end local v18    # "newFlags":I
    .end local v21    # "i":I
    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "permission":Ljava/lang/String;
    .local v5, "i":I
    .restart local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v7, "grantablePermissionCount":I
    :cond_13
    move-object v0, v4

    move/from16 v21, v5

    move-object/from16 v22, v6

    move/from16 v17, v7

    .line 202
    .end local v4    # "permission":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "grantablePermissionCount":I
    .restart local v17    # "grantablePermissionCount":I
    .restart local v21    # "i":I
    .restart local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_14
    :goto_6
    add-int/lit8 v5, v21, 0x1

    move-object/from16 v0, p0

    move-object/from16 v9, p1

    move/from16 v7, v17

    move-object/from16 v6, v22

    .end local v21    # "i":I
    .restart local v5    # "i":I
    goto/16 :goto_1

    .end local v17    # "grantablePermissionCount":I
    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "grantablePermissionCount":I
    :cond_15
    move/from16 v21, v5

    move-object/from16 v22, v6

    move/from16 v17, v7

    .line 268
    .end local v5    # "i":I
    .end local v6    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "grantablePermissionCount":I
    .restart local v17    # "grantablePermissionCount":I
    .restart local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v8, :cond_16

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 269
    sget-object v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->sMiuiAppDefaultGrantedPermissions:Landroid/util/ArrayMap;

    iget-object v1, v10, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v2, v22

    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v2, "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 268
    .end local v2    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_16
    move-object/from16 v2, v22

    .line 271
    .end local v22    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_7
    return-void

    .line 165
    .end local v2    # "grantPermissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v11    # "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
    .end local v12    # "allRequestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "requestedPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v14    # "requiredPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v15    # "grantablePermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v17    # "grantablePermissionCount":I
    :cond_17
    :goto_8
    return-void
.end method

.method private static isAdaptedRequiredPermissions(Landroid/content/pm/PackageInfo;)Z
    .locals 2
    .param p0, "pi"    # Landroid/content/pm/PackageInfo;

    .line 409
    if-eqz p0, :cond_1

    iget-object v0, p0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-nez v0, :cond_0

    goto :goto_0

    .line 412
    :cond_0
    iget-object v0, p0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v1, "required_permissions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 413
    .local v0, "permission":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    return v1

    .line 410
    .end local v0    # "permission":Ljava/lang/String;
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private static isDangerousPermission(Ljava/lang/String;)Z
    .locals 1
    .param p0, "permission"    # Ljava/lang/String;

    .line 68
    invoke-static {}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->ensureDangerousSetInit()V

    .line 69
    sget-object v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->RUNTIME_PERMISSIONS:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isOTAUpdated(I)Z
    .locals 1
    .param p0, "flags"    # I

    .line 278
    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_0

    and-int/lit8 v0, p0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isUserChanged(I)Z
    .locals 1
    .param p0, "flags"    # I

    .line 274
    and-int/lit8 v0, p0, 0x3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static realGrantDefaultPermissions(Lcom/android/server/pm/PackageManagerService;I)V
    .locals 12
    .param p0, "service"    # Lcom/android/server/pm/PackageManagerService;
    .param p1, "userId"    # I

    .line 144
    sget-object v0, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->MIUI_SYSTEM_APPS:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, v0, v3

    .line 145
    .local v4, "miuiSystemApp":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-static {p0, v4, v5, p1}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->grantRuntimePermissionsLPw(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ZI)V

    .line 144
    .end local v4    # "miuiSystemApp":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 147
    :cond_0
    const-class v0, Landroid/app/AppOpsManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManagerInternal;

    .line 149
    .local v0, "appOpsManagerInternal":Landroid/app/AppOpsManagerInternal;
    sget-object v1, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->sAllowAutoStartForOTAPkgs:[Ljava/lang/String;

    array-length v9, v1

    :goto_1
    if-ge v2, v9, :cond_2

    aget-object v10, v1, v2

    .line 150
    .local v10, "sAllowAutoStartForOTAPkg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v3

    const-wide/32 v4, 0x2000b080

    invoke-interface {v3, v10, v4, v5, p1}, Lcom/android/server/pm/Computer;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v11

    .line 151
    .local v11, "pkg":Landroid/content/pm/PackageInfo;
    if-eqz v11, :cond_1

    .line 152
    const/16 v4, 0x2718

    iget-object v3, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v0

    move-object v6, v10

    invoke-virtual/range {v3 .. v8}, Landroid/app/AppOpsManagerInternal;->setModeFromPermissionPolicy(IILjava/lang/String;ILcom/android/internal/app/IAppOpsCallback;)V

    .line 149
    .end local v10    # "sAllowAutoStartForOTAPkg":Ljava/lang/String;
    .end local v11    # "pkg":Landroid/content/pm/PackageInfo;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 156
    :cond_2
    return-void
.end method

.method public static setCoreRuntimePermissionEnabled(ZII)V
    .locals 3
    .param p0, "grant"    # Z
    .param p1, "flags"    # I
    .param p2, "userId"    # I

    .line 282
    if-eqz p2, :cond_0

    .line 283
    return-void

    .line 285
    :cond_0
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceStub;->get()Lcom/android/server/pm/PackageManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;

    move-result-object v0

    .line 286
    .local v0, "service":Lcom/android/server/pm/PackageManagerService;
    const-string v1, "persist.sys.runtime_perm"

    if-eqz p0, :cond_1

    .line 287
    invoke-static {v0, p2}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->realGrantDefaultPermissions(Lcom/android/server/pm/PackageManagerService;I)V

    .line 288
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 290
    :cond_1
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :goto_0
    return-void
.end method


# virtual methods
.method public grantDefaultPermissions(I)V
    .locals 10
    .param p1, "userId"    # I

    .line 114
    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    const-string v1, "DefaultPermGrantPolicyI"

    if-nez v0, :cond_4

    const-string v0, "persist.sys.grant_version"

    if-nez p1, :cond_0

    sget-object v2, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 116
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 115
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    .line 124
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 125
    .local v2, "startTime":J
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceStub;->get()Lcom/android/server/pm/PackageManagerServiceStub;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;

    move-result-object v4

    .line 126
    .local v4, "service":Lcom/android/server/pm/PackageManagerService;
    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v5, :cond_2

    .line 127
    sget-object v5, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->MIUI_GLOBAL_APPS:[Ljava/lang/String;

    array-length v6, v5

    const/4 v7, 0x0

    move v8, v7

    :goto_0
    if-ge v8, v6, :cond_1

    aget-object v9, v5, v8

    .line 128
    .local v9, "miuiGlobalApp":Ljava/lang/String;
    invoke-static {v4, v9, v7, p1}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->grantRuntimePermissionsLPw(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ZI)V

    .line 127
    .end local v9    # "miuiGlobalApp":Ljava/lang/String;
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 130
    :cond_1
    const-string v5, "grant permissions for miui global apps"

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const-string v5, "persist.sys.miui_optimization"

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 133
    const-string v5, "grant permissions for miui global apps: com.android.incallui"

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-static {v4, p1}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->grantIncallUiPermission(Lcom/android/server/pm/PackageManagerService;I)V

    goto :goto_1

    .line 137
    :cond_2
    invoke-static {v4, p1}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->realGrantDefaultPermissions(Lcom/android/server/pm/PackageManagerService;I)V

    .line 139
    :cond_3
    :goto_1
    sget-object v5, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "grantDefaultPermissions cost "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " ms"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    return-void

    .line 118
    .end local v2    # "startTime":J
    .end local v4    # "service":Lcom/android/server/pm/PackageManagerService;
    :cond_4
    :goto_2
    const-string v0, "Don\'t need grant default permission to apps"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 120
    invoke-static {p1}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->grantPermissionsForCTS(I)V

    .line 122
    :cond_5
    return-void
.end method

.method public grantMiuiPackageInstallerPermssions()V
    .locals 7

    .line 358
    const-string v0, "com.miui.packageinstaller"

    .line 359
    .local v0, "miuiPkgInstaller":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 360
    .local v1, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    const-string v2, "android.permission.READ_PHONE_STATE"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 366
    .local v3, "permItem":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPermissionManager()Landroid/permission/IPermissionManager;

    move-result-object v4

    check-cast v4, Lcom/android/server/pm/permission/PermissionManagerService;

    .line 367
    .local v4, "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
    const-string v5, "com.miui.packageinstaller"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v6}, Lcom/android/server/pm/permission/PermissionManagerService;->grantRuntimePermission(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    .end local v4    # "permissionService":Lcom/android/server/pm/permission/PermissionManagerService;
    goto :goto_1

    .line 368
    :catch_0
    move-exception v4

    .line 369
    .local v4, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "grantMiuiPackageInstallerPermssions error:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "DefaultPermGrantPolicyI"

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    .end local v3    # "permItem":Ljava/lang/String;
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_1
    goto :goto_0

    .line 372
    :cond_0
    return-void
.end method

.method public isSpecialUidNeedDefaultGrant(Landroid/content/pm/PackageInfo;)Z
    .locals 13
    .param p1, "info"    # Landroid/content/pm/PackageInfo;

    .line 376
    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x7d0

    const/4 v2, 0x1

    if-le v0, v1, :cond_7

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 377
    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_0

    goto :goto_2

    .line 380
    :cond_0
    invoke-static {}, Lcom/android/server/pm/PackageManagerServiceStub;->get()Lcom/android/server/pm/PackageManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerServiceStub;->getService()Lcom/android/server/pm/PackageManagerService;

    move-result-object v0

    .line 381
    .local v0, "service":Lcom/android/server/pm/PackageManagerService;
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    .line 382
    .local v1, "userId":I
    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v3

    iget-object v4, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-wide/16 v5, 0x80

    invoke-interface {v3, v4, v5, v6, v1}, Lcom/android/server/pm/Computer;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 383
    .local v3, "metaInfo":Landroid/content/pm/PackageInfo;
    if-nez v3, :cond_1

    .line 384
    return v2

    .line 386
    :cond_1
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 387
    invoke-static {v3}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->isAdaptedRequiredPermissions(Landroid/content/pm/PackageInfo;)Z

    move-result v2

    return v2

    .line 389
    :cond_2
    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v4

    iget-object v7, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-interface {v4, v7}, Lcom/android/server/pm/Computer;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v4

    .line 390
    .local v4, "sharedPackages":[Ljava/lang/String;
    if-nez v4, :cond_3

    .line 391
    return v2

    .line 393
    :cond_3
    array-length v7, v4

    const/4 v8, 0x0

    move v9, v8

    :goto_0
    if-ge v9, v7, :cond_6

    aget-object v10, v4, v9

    .line 395
    .local v10, "sharedPackage":Ljava/lang/String;
    iget-object v11, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v11, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 396
    move-object v11, v3

    .local v11, "sharedMetaInfo":Landroid/content/pm/PackageInfo;
    goto :goto_1

    .line 398
    .end local v11    # "sharedMetaInfo":Landroid/content/pm/PackageInfo;
    :cond_4
    invoke-virtual {v0}, Lcom/android/server/pm/PackageManagerService;->snapshotComputer()Lcom/android/server/pm/Computer;

    move-result-object v11

    invoke-interface {v11, v10, v5, v6, v1}, Lcom/android/server/pm/Computer;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v11

    .line 400
    .restart local v11    # "sharedMetaInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    invoke-static {v11}, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->isAdaptedRequiredPermissions(Landroid/content/pm/PackageInfo;)Z

    move-result v12

    if-nez v12, :cond_5

    .line 401
    return v2

    .line 393
    .end local v10    # "sharedPackage":Ljava/lang/String;
    .end local v11    # "sharedMetaInfo":Landroid/content/pm/PackageInfo;
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 404
    :cond_6
    return v8

    .line 378
    .end local v0    # "service":Lcom/android/server/pm/PackageManagerService;
    .end local v1    # "userId":I
    .end local v3    # "metaInfo":Landroid/content/pm/PackageInfo;
    .end local v4    # "sharedPackages":[Ljava/lang/String;
    :cond_7
    :goto_2
    return v2
.end method

.method public miReadOutPermissionsInExt(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .line 418
    .local p1, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v0, Ljava/io/File;

    const-string v1, "mi_ext/product"

    const-string v2, "etc/default-permissions"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 420
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    invoke-static {p1, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 422
    :cond_0
    return-void
.end method

.method public revokeAllPermssions()V
    .locals 13

    .line 316
    const-string v0, "com.miui.packageinstaller"

    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 317
    return-void

    .line 319
    :cond_0
    const-string v1, "persist.sys.grant_version"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPermissionManager()Landroid/permission/IPermissionManager;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/permission/PermissionManagerService;

    .line 322
    .local v1, "permissionManagerService":Lcom/android/server/pm/permission/PermissionManagerService;
    sget-object v2, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->sMiuiAppDefaultGrantedPermissions:Landroid/util/ArrayMap;

    invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const-string v4, "DefaultPermGrantPolicyI"

    const-string v5, "revokeMiuiOpt"

    const/4 v6, 0x0

    if-eqz v3, :cond_6

    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 323
    .local v3, "pkg":Ljava/lang/String;
    sget-object v7, Lcom/android/server/pm/MiuiDefaultPermissionGrantPolicy;->sMiuiAppDefaultGrantedPermissions:Landroid/util/ArrayMap;

    invoke-virtual {v7, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 324
    .local v7, "permissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v8, "com.google.android.packageinstaller"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    if-nez v7, :cond_2

    .line 325
    goto :goto_0

    .line 327
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 328
    .local v9, "p":Ljava/lang/String;
    const-string v10, "com.google.android.gms"

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 329
    const-string v10, "android.permission.RECORD_AUDIO"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "android.permission.ACCESS_FINE_LOCATION"

    .line 330
    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v10, :cond_4

    .line 331
    goto :goto_1

    .line 335
    :cond_4
    :try_start_2
    invoke-virtual {v1, v3, v9, v6, v5}, Lcom/android/server/pm/permission/PermissionManagerService;->revokeRuntimePermission(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 338
    goto :goto_2

    .line 336
    :catch_0
    move-exception v10

    .line 337
    .local v10, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "revokeAllPermssions error:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    .end local v9    # "p":Ljava/lang/String;
    .end local v10    # "e":Ljava/lang/Exception;
    :goto_2
    goto :goto_1

    .line 340
    .end local v3    # "pkg":Ljava/lang/String;
    .end local v7    # "permissions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    goto :goto_0

    .line 341
    :cond_6
    move-object v2, v0

    .line 342
    .local v2, "miuiPkgInstaller":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v3, "permissionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    const-string v7, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 347
    .local v8, "permItem":Ljava/lang/String;
    :try_start_4
    invoke-virtual {v1, v0, v8, v6, v5}, Lcom/android/server/pm/permission/PermissionManagerService;->revokeRuntimePermission(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 350
    goto :goto_4

    .line 348
    :catch_1
    move-exception v9

    .line 349
    .local v9, "e":Ljava/lang/Exception;
    :try_start_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "revokeRuntimePermissionInternal error:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 351
    .end local v8    # "permItem":Ljava/lang/String;
    .end local v9    # "e":Ljava/lang/Exception;
    :goto_4
    goto :goto_3

    .line 354
    .end local v1    # "permissionManagerService":Lcom/android/server/pm/permission/PermissionManagerService;
    .end local v2    # "miuiPkgInstaller":Ljava/lang/String;
    .end local v3    # "permissionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_7
    goto :goto_5

    .line 352
    :catch_2
    move-exception v0

    .line 353
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 355
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_5
    return-void
.end method
