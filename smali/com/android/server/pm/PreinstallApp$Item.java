class com.android.server.pm.PreinstallApp$Item {
	 /* .source "PreinstallApp.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/pm/PreinstallApp; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Item" */
} // .end annotation
/* # static fields */
static final Integer TYPE_CLUSTER;
static final Integer TYPE_MONOLITHIC;
static final Integer TYPE_SPLIT;
/* # instance fields */
java.io.File apkFile;
java.io.File app;
java.lang.String packageName;
android.content.pm.parsing.ApkLite pkg;
android.content.pm.parsing.PackageLite pkgLite;
Integer type;
/* # direct methods */
 com.android.server.pm.PreinstallApp$Item ( ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "file" # Ljava/io/File; */
/* .param p3, "pkgLite" # Landroid/content/pm/parsing/PackageLite; */
/* .param p4, "pkg" # Landroid/content/pm/parsing/ApkLite; */
/* .line 84 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 85 */
this.packageName = p1;
/* .line 86 */
this.app = p2;
/* .line 87 */
com.android.server.pm.PreinstallApp .-$$Nest$smgetApkFile ( p2 );
this.apkFile = v0;
/* .line 88 */
this.pkgLite = p3;
/* .line 89 */
this.pkg = p4;
/* .line 90 */
v0 = com.android.server.pm.PreinstallApp .-$$Nest$smisSplitApk ( p2 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 91 */
	 int v0 = 3; // const/4 v0, 0x3
	 /* iput v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->type:I */
	 /* .line 93 */
} // :cond_0
v0 = (( java.io.File ) p2 ).isDirectory ( ); // invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 int v0 = 2; // const/4 v0, 0x2
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :goto_0
/* iput v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->type:I */
/* .line 95 */
} // :goto_1
return;
} // .end method
static Boolean betterThan ( com.android.server.pm.PreinstallApp$Item p0, com.android.server.pm.PreinstallApp$Item p1 ) {
/* .locals 4 */
/* .param p0, "newItem" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .param p1, "oldItem" # Lcom/android/server/pm/PreinstallApp$Item; */
/* .line 98 */
/* nop */
/* .line 105 */
/* iget v0, p0, Lcom/android/server/pm/PreinstallApp$Item;->type:I */
/* iget v1, p1, Lcom/android/server/pm/PreinstallApp$Item;->type:I */
int v2 = 1; // const/4 v2, 0x1
/* if-le v0, v1, :cond_0 */
/* .line 107 */
/* .line 108 */
} // :cond_0
int v3 = 0; // const/4 v3, 0x0
/* if-ne v0, v1, :cond_2 */
/* .line 110 */
v0 = this.pkgLite;
v0 = (( android.content.pm.parsing.PackageLite ) v0 ).getVersionCode ( ); // invoke-virtual {v0}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
v1 = this.pkgLite;
v1 = (( android.content.pm.parsing.PackageLite ) v1 ).getVersionCode ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
/* if-le v0, v1, :cond_1 */
} // :cond_1
/* move v2, v3 */
} // :goto_0
/* .line 113 */
} // :cond_2
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 117 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = ""; // const-string v1, ""
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.packageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "["; // const-string v1, "["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.apkFile;
(( java.io.File ) v1 ).getPath ( ); // invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ","; // const-string v1, ","
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.pkgLite;
v1 = (( android.content.pm.parsing.PackageLite ) v1 ).getVersionCode ( ); // invoke-virtual {v1}, Landroid/content/pm/parsing/PackageLite;->getVersionCode()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
