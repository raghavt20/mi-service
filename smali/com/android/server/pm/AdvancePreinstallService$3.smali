.class Lcom/android/server/pm/AdvancePreinstallService$3;
.super Ljava/lang/Object;
.source "AdvancePreinstallService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/pm/AdvancePreinstallService;->handleAdvancePreinstallApps()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/AdvancePreinstallService;


# direct methods
.method constructor <init>(Lcom/android/server/pm/AdvancePreinstallService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/AdvancePreinstallService;

    .line 295
    iput-object p1, p0, Lcom/android/server/pm/AdvancePreinstallService$3;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 298
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService$3;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    const-string v1, "advance_uninstall_start"

    invoke-static {v0, v1}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$mtrackEvent(Lcom/android/server/pm/AdvancePreinstallService;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService$3;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-virtual {v0}, Lcom/android/server/pm/AdvancePreinstallService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$misNetworkConnected(Lcom/android/server/pm/AdvancePreinstallService;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService$3;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$muninstallAdvancePreinstallApps(Lcom/android/server/pm/AdvancePreinstallService;)V

    goto :goto_0

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/android/server/pm/AdvancePreinstallService$3;->this$0:Lcom/android/server/pm/AdvancePreinstallService;

    invoke-static {v0}, Lcom/android/server/pm/AdvancePreinstallService;->-$$Nest$mgetPreloadAppInfo(Lcom/android/server/pm/AdvancePreinstallService;)V

    .line 305
    :goto_0
    return-void
.end method
