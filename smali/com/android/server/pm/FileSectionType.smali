.class final enum Lcom/android/server/pm/FileSectionType;
.super Ljava/lang/Enum;
.source "ProfileTranscoder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/server/pm/FileSectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/pm/FileSectionType;

.field public static final enum AGGREGATION_COUNT:Lcom/android/server/pm/FileSectionType;

.field public static final enum CLASSES:Lcom/android/server/pm/FileSectionType;

.field public static final enum DEX_FILES:Lcom/android/server/pm/FileSectionType;

.field public static final enum EXTRA_DESCRIPTORS:Lcom/android/server/pm/FileSectionType;

.field public static final enum METHODS:Lcom/android/server/pm/FileSectionType;


# instance fields
.field private final mValue:J


# direct methods
.method private static synthetic $values()[Lcom/android/server/pm/FileSectionType;
    .locals 5

    .line 1050
    sget-object v0, Lcom/android/server/pm/FileSectionType;->DEX_FILES:Lcom/android/server/pm/FileSectionType;

    sget-object v1, Lcom/android/server/pm/FileSectionType;->EXTRA_DESCRIPTORS:Lcom/android/server/pm/FileSectionType;

    sget-object v2, Lcom/android/server/pm/FileSectionType;->CLASSES:Lcom/android/server/pm/FileSectionType;

    sget-object v3, Lcom/android/server/pm/FileSectionType;->METHODS:Lcom/android/server/pm/FileSectionType;

    sget-object v4, Lcom/android/server/pm/FileSectionType;->AGGREGATION_COUNT:Lcom/android/server/pm/FileSectionType;

    filled-new-array {v0, v1, v2, v3, v4}, [Lcom/android/server/pm/FileSectionType;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 1052
    new-instance v0, Lcom/android/server/pm/FileSectionType;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-string v4, "DEX_FILES"

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/android/server/pm/FileSectionType;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/android/server/pm/FileSectionType;->DEX_FILES:Lcom/android/server/pm/FileSectionType;

    .line 1057
    new-instance v0, Lcom/android/server/pm/FileSectionType;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1

    const-string v4, "EXTRA_DESCRIPTORS"

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/android/server/pm/FileSectionType;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/android/server/pm/FileSectionType;->EXTRA_DESCRIPTORS:Lcom/android/server/pm/FileSectionType;

    .line 1058
    new-instance v0, Lcom/android/server/pm/FileSectionType;

    const/4 v1, 0x2

    const-wide/16 v2, 0x2

    const-string v4, "CLASSES"

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/android/server/pm/FileSectionType;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/android/server/pm/FileSectionType;->CLASSES:Lcom/android/server/pm/FileSectionType;

    .line 1059
    new-instance v0, Lcom/android/server/pm/FileSectionType;

    const/4 v1, 0x3

    const-wide/16 v2, 0x3

    const-string v4, "METHODS"

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/android/server/pm/FileSectionType;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/android/server/pm/FileSectionType;->METHODS:Lcom/android/server/pm/FileSectionType;

    .line 1060
    new-instance v0, Lcom/android/server/pm/FileSectionType;

    const/4 v1, 0x4

    const-wide/16 v2, 0x4

    const-string v4, "AGGREGATION_COUNT"

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/android/server/pm/FileSectionType;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/android/server/pm/FileSectionType;->AGGREGATION_COUNT:Lcom/android/server/pm/FileSectionType;

    .line 1050
    invoke-static {}, Lcom/android/server/pm/FileSectionType;->$values()[Lcom/android/server/pm/FileSectionType;

    move-result-object v0

    sput-object v0, Lcom/android/server/pm/FileSectionType;->$VALUES:[Lcom/android/server/pm/FileSectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .param p3, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .line 1062
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1063
    iput-wide p3, p0, Lcom/android/server/pm/FileSectionType;->mValue:J

    .line 1064
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/pm/FileSectionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 1050
    const-class v0, Lcom/android/server/pm/FileSectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/pm/FileSectionType;

    return-object v0
.end method

.method public static values()[Lcom/android/server/pm/FileSectionType;
    .locals 1

    .line 1050
    sget-object v0, Lcom/android/server/pm/FileSectionType;->$VALUES:[Lcom/android/server/pm/FileSectionType;

    invoke-virtual {v0}, [Lcom/android/server/pm/FileSectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/pm/FileSectionType;

    return-object v0
.end method


# virtual methods
.method public getValue()J
    .locals 2

    .line 1066
    iget-wide v0, p0, Lcom/android/server/pm/FileSectionType;->mValue:J

    return-wide v0
.end method
