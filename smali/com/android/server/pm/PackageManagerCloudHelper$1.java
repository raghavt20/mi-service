class com.android.server.pm.PackageManagerCloudHelper$1 extends android.database.ContentObserver {
	 /* .source "PackageManagerCloudHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/pm/PackageManagerCloudHelper;->registerCloudDataObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.pm.PackageManagerCloudHelper this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$sVyqjR0hzLoj43PoOYrbtaEAyBw ( com.android.server.pm.PackageManagerCloudHelper$1 p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper$1;->lambda$onChange$0()V */
return;
} // .end method
 com.android.server.pm.PackageManagerCloudHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/pm/PackageManagerCloudHelper; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 72 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
private void lambda$onChange$0 ( ) { //synthethic
/* .locals 1 */
/* .line 80 */
v0 = this.this$0;
com.android.server.pm.PackageManagerCloudHelper .-$$Nest$mreadCloudData ( v0 );
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .line 75 */
v0 = this.this$0;
com.android.server.pm.PackageManagerCloudHelper .-$$Nest$fgetmContext ( v0 );
v0 = com.android.server.pm.PackageManagerCloudHelper .-$$Nest$misCloudConfigUpdate ( v0,v1 );
/* .line 76 */
/* .local v0, "isCloudConfigChanged":Z */
com.android.server.pm.PackageManagerCloudHelper .-$$Nest$sfgetTAG ( );
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MiuiSettings notify cloud data is changed.Whether is applying new package cloud data : "; // const-string v3, "MiuiSettings notify cloud data is changed.Whether is applying new package cloud data : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 79 */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 80 */
	 v1 = this.this$0;
	 com.android.server.pm.PackageManagerCloudHelper .-$$Nest$fgetmPms ( v1 );
	 v1 = this.mHandler;
	 /* new-instance v2, Lcom/android/server/pm/PackageManagerCloudHelper$1$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0}, Lcom/android/server/pm/PackageManagerCloudHelper$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/PackageManagerCloudHelper$1;)V */
	 (( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
	 /* .line 82 */
} // :cond_0
return;
} // .end method
