.class Lcom/android/server/pm/PreInstallServiceTrack$1;
.super Ljava/lang/Object;
.source "PreInstallServiceTrack.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PreInstallServiceTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/pm/PreInstallServiceTrack;


# direct methods
.method constructor <init>(Lcom/android/server/pm/PreInstallServiceTrack;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/pm/PreInstallServiceTrack;

    .line 32
    iput-object p1, p0, Lcom/android/server/pm/PreInstallServiceTrack$1;->this$0:Lcom/android/server/pm/PreInstallServiceTrack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 35
    iget-object v0, p0, Lcom/android/server/pm/PreInstallServiceTrack$1;->this$0:Lcom/android/server/pm/PreInstallServiceTrack;

    invoke-static {p2}, Lcom/miui/analytics/ITrackBinder$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/pm/PreInstallServiceTrack;->-$$Nest$fputmService(Lcom/android/server/pm/PreInstallServiceTrack;Lcom/miui/analytics/ITrackBinder;)V

    .line 36
    invoke-static {}, Lcom/android/server/pm/PreInstallServiceTrack;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceConnected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/pm/PreInstallServiceTrack$1;->this$0:Lcom/android/server/pm/PreInstallServiceTrack;

    invoke-static {v2}, Lcom/android/server/pm/PreInstallServiceTrack;->-$$Nest$fgetmService(Lcom/android/server/pm/PreInstallServiceTrack;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 41
    iget-object v0, p0, Lcom/android/server/pm/PreInstallServiceTrack$1;->this$0:Lcom/android/server/pm/PreInstallServiceTrack;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/pm/PreInstallServiceTrack;->-$$Nest$fputmService(Lcom/android/server/pm/PreInstallServiceTrack;Lcom/miui/analytics/ITrackBinder;)V

    .line 42
    invoke-static {}, Lcom/android/server/pm/PreInstallServiceTrack;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    return-void
.end method
