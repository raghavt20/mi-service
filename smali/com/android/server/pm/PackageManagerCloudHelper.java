public class com.android.server.pm.PackageManagerCloudHelper {
	 /* .source "PackageManagerCloudHelper.java" */
	 /* # static fields */
	 private static final java.lang.String CLOUD_DATA_VERSION;
	 private static final java.lang.String CLOUD_MODULE_NAME;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.util.Set mCloudNotSupportUpdateSystemApps;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private Integer mLastCloudConfigVersion;
private final java.lang.Object mNotSupportUpdateLock;
private com.android.server.pm.PackageManagerService mPms;
private java.io.File mPreviousSettingsFilename;
private java.io.File mSettingsFilename;
private java.io.File mSettingsReserveCopyFilename;
private java.io.File mSystemDir;
/* # direct methods */
public static void $r8$lambda$TIso4l0kD6HUgso-hP0AXn4I5kw ( com.android.server.pm.PackageManagerCloudHelper p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->lambda$readCloudData$0()V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.pm.PackageManagerCloudHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.android.server.pm.PackageManagerService -$$Nest$fgetmPms ( com.android.server.pm.PackageManagerCloudHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPms;
} // .end method
static Boolean -$$Nest$misCloudConfigUpdate ( com.android.server.pm.PackageManagerCloudHelper p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/pm/PackageManagerCloudHelper;->isCloudConfigUpdate(Landroid/content/Context;)Z */
} // .end method
static void -$$Nest$mreadCloudData ( com.android.server.pm.PackageManagerCloudHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->readCloudData()V */
return;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.pm.PackageManagerCloudHelper.TAG;
} // .end method
static com.android.server.pm.PackageManagerCloudHelper ( ) {
/* .locals 1 */
/* .line 33 */
/* const-class v0, Lcom/android/server/pm/PackageManagerCloudHelper; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
return;
} // .end method
 com.android.server.pm.PackageManagerCloudHelper ( ) {
/* .locals 1 */
/* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 51 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 43 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mCloudNotSupportUpdateSystemApps = v0;
/* .line 48 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mNotSupportUpdateLock = v0;
/* .line 49 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I */
/* .line 52 */
this.mPms = p1;
/* .line 53 */
this.mContext = p2;
/* .line 54 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->initPackageCloudSettings()V */
/* .line 55 */
return;
} // .end method
private com.android.server.pm.ResilientAtomicFile getCloudSettingsFile ( ) {
/* .locals 8 */
/* .line 112 */
/* new-instance v7, Lcom/android/server/pm/ResilientAtomicFile; */
v1 = this.mSettingsFilename;
v2 = this.mPreviousSettingsFilename;
v3 = this.mSettingsReserveCopyFilename;
/* const/16 v4, 0x1b0 */
final String v5 = "package manager cloud settings"; // const-string v5, "package manager cloud settings"
int v6 = 0; // const/4 v6, 0x0
/* move-object v0, v7 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/pm/ResilientAtomicFile;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;ILjava/lang/String;Lcom/android/server/pm/ResilientAtomicFile$ReadEventLogger;)V */
} // .end method
private void initPackageCloudSettings ( ) {
/* .locals 3 */
/* .line 58 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataDirectory ( );
/* const-string/jumbo v2, "system" */
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mSystemDir = v0;
/* .line 59 */
/* new-instance v0, Ljava/io/File; */
v1 = this.mSystemDir;
final String v2 = "packagemanger_cloud.xml"; // const-string v2, "packagemanger_cloud.xml"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mSettingsFilename = v0;
/* .line 60 */
/* new-instance v0, Ljava/io/File; */
v1 = this.mSystemDir;
final String v2 = "packagemanger_cloud.xml.reservecopy"; // const-string v2, "packagemanger_cloud.xml.reservecopy"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mSettingsReserveCopyFilename = v0;
/* .line 61 */
/* new-instance v0, Ljava/io/File; */
v1 = this.mSystemDir;
final String v2 = "packagemanger_cloud-backup.xml"; // const-string v2, "packagemanger_cloud-backup.xml"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mPreviousSettingsFilename = v0;
/* .line 62 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->readCloudSetting()V */
/* .line 63 */
return;
} // .end method
private Boolean isCloudConfigUpdate ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 88 */
/* nop */
/* .line 89 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "version" */
int v2 = 0; // const/4 v2, 0x0
final String v3 = "block_update"; // const-string v3, "block_update"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataString ( v0,v3,v1,v2 );
/* .line 91 */
/* .local v0, "dataVersion":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 92 */
	 v1 = com.android.server.pm.PackageManagerCloudHelper.TAG;
	 final String v3 = "data version is empty, or control module block_update is not exist"; // const-string v3, "data version is empty, or control module block_update is not exist"
	 android.util.Slog .d ( v1,v3 );
	 /* .line 94 */
	 /* .line 96 */
} // :cond_0
v1 = java.lang.Integer .parseInt ( v0 );
/* .line 99 */
/* .local v1, "remoteVersion":I */
/* iget v3, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I */
/* if-gt v1, v3, :cond_1 */
/* .line 100 */
v3 = com.android.server.pm.PackageManagerCloudHelper.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "cloud data is not modify, don\'t need update.data version = "; // const-string v5, "cloud data is not modify, don\'t need update.data version = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 102 */
/* .line 106 */
} // :cond_1
/* iput v1, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I */
/* .line 107 */
v2 = com.android.server.pm.PackageManagerCloudHelper.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "package manager cloud data new version "; // const-string v4, "package manager cloud data new version "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 108 */
int v2 = 1; // const/4 v2, 0x1
} // .end method
private void lambda$readCloudData$0 ( ) { //synthethic
/* .locals 0 */
/* .line 146 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->writeCloudSetting()V */
return;
} // .end method
private void readCloudData ( ) {
/* .locals 10 */
/* .line 120 */
v0 = this.mContext;
/* .line 121 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "block_update"; // const-string v1, "block_update"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v0,v1 );
/* .line 122 */
/* .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_5
/* if-nez v1, :cond_0 */
/* .line 126 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/util/HashSet; */
/* invoke-direct {v1}, Ljava/util/HashSet;-><init>()V */
/* .line 127 */
/* .local v1, "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
/* .line 128 */
/* .local v3, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).toString ( ); // invoke-virtual {v3}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;
/* .line 129 */
/* .local v4, "json":Ljava/lang/String; */
v5 = android.text.TextUtils .isEmpty ( v4 );
if ( v5 != null) { // if-eqz v5, :cond_1
	 /* .line 130 */
	 /* .line 133 */
} // :cond_1
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 134 */
/* .local v5, "jsonObject":Lorg/json/JSONObject; */
final String v6 = "packageName"; // const-string v6, "packageName"
(( org.json.JSONObject ) v5 ).getJSONArray ( v6 ); // invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 135 */
/* .local v6, "jsonArray":Lorg/json/JSONArray; */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_1
v8 = (( org.json.JSONArray ) v6 ).length ( ); // invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
/* if-ge v7, v8, :cond_3 */
/* .line 136 */
(( org.json.JSONArray ) v6 ).getString ( v7 ); // invoke-virtual {v6, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 137 */
/* .local v8, "pkg":Ljava/lang/String; */
v9 = android.text.TextUtils .isEmpty ( v8 );
/* if-nez v9, :cond_2 */
/* .line 138 */
/* .line 135 */
} // .end local v8 # "pkg":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v7, v7, 0x1 */
/* .line 141 */
} // .end local v3 # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
} // .end local v4 # "json":Ljava/lang/String;
} // .end local v5 # "jsonObject":Lorg/json/JSONObject;
} // .end local v6 # "jsonArray":Lorg/json/JSONArray;
} // .end local v7 # "i":I
} // :cond_3
/* .line 142 */
} // :cond_4
v2 = this.mNotSupportUpdateLock;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 143 */
try { // :try_start_1
v3 = this.mCloudNotSupportUpdateSystemApps;
/* .line 144 */
v3 = this.mCloudNotSupportUpdateSystemApps;
/* .line 145 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 146 */
try { // :try_start_2
v2 = this.mPms;
v2 = this.mHandler;
/* new-instance v3, Lcom/android/server/pm/PackageManagerCloudHelper$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p0}, Lcom/android/server/pm/PackageManagerCloudHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/pm/PackageManagerCloudHelper;)V */
(( android.os.Handler ) v2 ).post ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 149 */
/* nop */
} // .end local v1 # "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .line 145 */
/* .restart local v1 # "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
/* monitor-exit v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v0 # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;"
} // .end local p0 # "this":Lcom/android/server/pm/PackageManagerCloudHelper;
try { // :try_start_4
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 147 */
} // .end local v1 # "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
/* .restart local v0 # "dataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
/* .restart local p0 # "this":Lcom/android/server/pm/PackageManagerCloudHelper; */
/* :catch_0 */
/* move-exception v1 */
/* .line 148 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 150 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
return;
/* .line 123 */
} // :cond_5
} // :goto_3
return;
} // .end method
private void readCloudSetting ( ) {
/* .locals 13 */
/* .line 199 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 200 */
/* .local v0, "startTime":J */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->getCloudSettingsFile()Lcom/android/server/pm/ResilientAtomicFile; */
/* .line 201 */
/* .local v2, "atomicFile":Lcom/android/server/pm/ResilientAtomicFile; */
int v3 = 0; // const/4 v3, 0x0
/* .line 203 */
/* .local v3, "str":Ljava/io/FileInputStream; */
try { // :try_start_0
(( com.android.server.pm.ResilientAtomicFile ) v2 ).openRead ( ); // invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->openRead()Ljava/io/FileInputStream;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v3, v4 */
/* .line 204 */
/* if-nez v3, :cond_1 */
/* .line 252 */
if ( v2 != null) { // if-eqz v2, :cond_0
(( com.android.server.pm.ResilientAtomicFile ) v2 ).close ( ); // invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* .line 205 */
} // :cond_0
return;
/* .line 207 */
} // :cond_1
try { // :try_start_1
/* new-instance v4, Ljava/util/HashSet; */
/* invoke-direct {v4}, Ljava/util/HashSet;-><init>()V */
/* .line 208 */
/* .local v4, "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
android.util.Xml .resolvePullParser ( v3 );
/* .line 210 */
/* .local v5, "parser":Lcom/android/modules/utils/TypedXmlPullParser; */
v6 = } // :goto_0
/* move v7, v6 */
/* .local v7, "type":I */
int v8 = 1; // const/4 v8, 0x1
int v9 = 2; // const/4 v9, 0x2
/* if-eq v6, v9, :cond_2 */
/* if-eq v7, v8, :cond_2 */
/* .line 215 */
} // :cond_2
/* if-eq v7, v9, :cond_4 */
/* .line 216 */
v6 = com.android.server.pm.PackageManagerCloudHelper.TAG;
final String v8 = "No start tag found in package manager cloud settings"; // const-string v8, "No start tag found in package manager cloud settings"
android.util.Slog .e ( v6,v8 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 252 */
if ( v2 != null) { // if-eqz v2, :cond_3
(( com.android.server.pm.ResilientAtomicFile ) v2 ).close ( ); // invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* .line 217 */
} // :cond_3
return;
/* .line 220 */
} // :cond_4
v6 = try { // :try_start_2
/* .line 221 */
/* .local v6, "outerDepth":I */
} // :cond_5
v9 = } // :goto_1
/* move v7, v9 */
/* if-eq v9, v8, :cond_a */
int v9 = 3; // const/4 v9, 0x3
/* if-ne v7, v9, :cond_6 */
v10 = /* .line 222 */
/* if-le v10, v6, :cond_a */
/* .line 223 */
} // :cond_6
/* if-eq v7, v9, :cond_5 */
int v9 = 4; // const/4 v9, 0x4
/* if-ne v7, v9, :cond_7 */
/* .line 224 */
/* .line 227 */
} // :cond_7
/* .line 228 */
/* .local v9, "tagName":Ljava/lang/String; */
final String v10 = "packages"; // const-string v10, "packages"
v10 = (( java.lang.String ) v10 ).equals ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_9
/* .line 229 */
/* const-string/jumbo v10, "version" */
int v11 = 0; // const/4 v11, 0x0
/* .line 230 */
/* .local v10, "version":Ljava/lang/String; */
v11 = android.text.TextUtils .isEmpty ( v10 );
if ( v11 != null) { // if-eqz v11, :cond_8
/* .line 231 */
v8 = com.android.server.pm.PackageManagerCloudHelper.TAG;
final String v11 = "read block-update config version is null"; // const-string v11, "read block-update config version is null"
android.util.Slog .e ( v8,v11 );
/* .line 232 */
/* .line 234 */
} // :cond_8
v11 = java.lang.Integer .parseInt ( v10 );
/* iput v11, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I */
/* .line 235 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/pm/PackageManagerCloudHelper;->readPackageNames(Ljava/util/Set;Lcom/android/modules/utils/TypedXmlPullParser;)V */
/* .line 236 */
} // .end local v10 # "version":Ljava/lang/String;
/* .line 237 */
} // :cond_9
v10 = com.android.server.pm.PackageManagerCloudHelper.TAG;
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "Unknown element under <package-cloudconfig>: "; // const-string v12, "Unknown element under <package-cloudconfig>: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 238 */
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 237 */
android.util.Slog .w ( v10,v11 );
/* .line 239 */
com.android.internal.util.XmlUtils .skipCurrentTag ( v5 );
/* .line 241 */
} // .end local v9 # "tagName":Ljava/lang/String;
} // :goto_2
/* .line 242 */
} // :cond_a
} // :goto_3
(( java.io.FileInputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
/* .line 243 */
v8 = com.android.server.pm.PackageManagerCloudHelper.TAG;
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "read packageCloudSetting took "; // const-string v10, "read packageCloudSetting took "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 244 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v10 */
/* sub-long/2addr v10, v0 */
(( java.lang.StringBuilder ) v9 ).append ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = "ms"; // const-string v10, "ms"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 243 */
android.util.Slog .d ( v8,v9 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 251 */
} // .end local v4 # "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local v5 # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
} // .end local v6 # "outerDepth":I
} // .end local v7 # "type":I
/* .line 200 */
} // .end local v3 # "str":Ljava/io/FileInputStream;
/* :catchall_0 */
/* move-exception v3 */
/* .line 245 */
/* .restart local v3 # "str":Ljava/io/FileInputStream; */
/* :catch_0 */
/* move-exception v4 */
/* .line 246 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_3
v5 = com.android.server.pm.PackageManagerCloudHelper.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "readNotSupportUpdateConfig fail: "; // const-string v7, "readNotSupportUpdateConfig fail: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v4 ).getMessage ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v6 );
/* .line 247 */
int v5 = 0; // const/4 v5, 0x0
/* iput v5, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I */
/* .line 249 */
(( com.android.server.pm.ResilientAtomicFile ) v2 ).failRead ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->failRead(Ljava/io/FileInputStream;Ljava/lang/Exception;)V
/* .line 250 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->readCloudSetting()V */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 252 */
} // .end local v3 # "str":Ljava/io/FileInputStream;
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_4
if ( v2 != null) { // if-eqz v2, :cond_b
(( com.android.server.pm.ResilientAtomicFile ) v2 ).close ( ); // invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* .line 254 */
} // .end local v2 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
} // :cond_b
return;
/* .line 200 */
/* .restart local v2 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile; */
} // :goto_5
if ( v2 != null) { // if-eqz v2, :cond_c
try { // :try_start_4
(( com.android.server.pm.ResilientAtomicFile ) v2 ).close ( ); // invoke-virtual {v2}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :cond_c
} // :goto_6
/* throw v3 */
} // .end method
private void readPackageNames ( java.util.Set p0, com.android.modules.utils.TypedXmlPullParser p1 ) {
/* .locals 6 */
/* .param p2, "parser" # Lcom/android/modules/utils/TypedXmlPullParser; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Lcom/android/modules/utils/TypedXmlPullParser;", */
/* ")V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException;, */
/* Lorg/xmlpull/v1/XmlPullParserException; */
/* } */
} // .end annotation
/* .line 258 */
v0 = /* .local p1, "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .line 260 */
/* .local v0, "outerDepth":I */
} // :cond_0
v1 = } // :goto_0
/* move v2, v1 */
/* .local v2, "type":I */
int v3 = 1; // const/4 v3, 0x1
/* if-eq v1, v3, :cond_5 */
int v1 = 3; // const/4 v1, 0x3
/* if-ne v2, v1, :cond_1 */
v3 = /* .line 261 */
/* if-le v3, v0, :cond_5 */
/* .line 262 */
} // :cond_1
/* if-eq v2, v1, :cond_0 */
int v1 = 4; // const/4 v1, 0x4
/* if-ne v2, v1, :cond_2 */
/* .line 263 */
/* .line 265 */
} // :cond_2
/* .line 266 */
/* .local v1, "tagName":Ljava/lang/String; */
final String v3 = "package"; // const-string v3, "package"
v3 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 267 */
int v3 = 0; // const/4 v3, 0x0
final String v4 = "name"; // const-string v4, "name"
/* .line 268 */
/* .local v3, "pkgName":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 269 */
v4 = com.android.server.pm.PackageManagerCloudHelper.TAG;
final String v5 = "read block-update config pkgName is null"; // const-string v5, "read block-update config pkgName is null"
android.util.Slog .e ( v4,v5 );
/* .line 270 */
/* .line 272 */
} // :cond_3
/* .line 273 */
} // .end local v3 # "pkgName":Ljava/lang/String;
/* .line 274 */
} // :cond_4
v3 = com.android.server.pm.PackageManagerCloudHelper.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Unknown element under <packages>: "; // const-string v5, "Unknown element under <packages>: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 275 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 274 */
android.util.Slog .w ( v3,v4 );
/* .line 276 */
com.android.internal.util.XmlUtils .skipCurrentTag ( p2 );
/* .line 278 */
} // .end local v1 # "tagName":Ljava/lang/String;
} // :goto_1
/* .line 279 */
} // :cond_5
} // :goto_2
v1 = this.mNotSupportUpdateLock;
/* monitor-enter v1 */
/* .line 280 */
try { // :try_start_0
v3 = this.mCloudNotSupportUpdateSystemApps;
/* .line 281 */
v3 = this.mCloudNotSupportUpdateSystemApps;
/* .line 282 */
/* monitor-exit v1 */
/* .line 283 */
return;
/* .line 282 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
private void writeCloudSetting ( ) {
/* .locals 10 */
/* .line 153 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 154 */
/* .local v0, "startTime":J */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* .line 155 */
/* .local v2, "pkgs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v3 = this.mNotSupportUpdateLock;
/* monitor-enter v3 */
/* .line 156 */
try { // :try_start_0
v4 = this.mCloudNotSupportUpdateSystemApps;
/* .line 157 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 159 */
/* invoke-direct {p0}, Lcom/android/server/pm/PackageManagerCloudHelper;->getCloudSettingsFile()Lcom/android/server/pm/ResilientAtomicFile; */
/* .line 160 */
/* .local v3, "atomicFile":Lcom/android/server/pm/ResilientAtomicFile; */
int v4 = 0; // const/4 v4, 0x0
/* .line 162 */
/* .local v4, "str":Ljava/io/FileOutputStream; */
try { // :try_start_1
(( com.android.server.pm.ResilientAtomicFile ) v3 ).startWrite ( ); // invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->startWrite()Ljava/io/FileOutputStream;
/* move-object v4, v5 */
/* .line 164 */
android.util.Xml .resolveSerializer ( v4 );
/* .line 165 */
/* .local v5, "serializer":Lcom/android/modules/utils/TypedXmlSerializer; */
int v6 = 1; // const/4 v6, 0x1
java.lang.Boolean .valueOf ( v6 );
int v8 = 0; // const/4 v8, 0x0
/* .line 166 */
final String v7 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v7, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 169 */
final String v6 = "package-cloudconfig"; // const-string v6, "package-cloudconfig"
/* .line 170 */
final String v6 = "packages"; // const-string v6, "packages"
/* .line 171 */
/* const-string/jumbo v6, "version" */
/* iget v7, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I */
/* .line 173 */
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_0
/* check-cast v7, Ljava/lang/String; */
/* .line 174 */
/* .local v7, "packageName":Ljava/lang/String; */
final String v9 = "package"; // const-string v9, "package"
/* .line 175 */
final String v9 = "name"; // const-string v9, "name"
/* .line 176 */
final String v9 = "package"; // const-string v9, "package"
/* .line 177 */
/* nop */
} // .end local v7 # "packageName":Ljava/lang/String;
/* .line 179 */
} // :cond_0
final String v6 = "packages"; // const-string v6, "packages"
/* .line 180 */
final String v6 = "package-cloudconfig"; // const-string v6, "package-cloudconfig"
/* .line 181 */
/* .line 183 */
(( com.android.server.pm.ResilientAtomicFile ) v3 ).finishWrite ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V
/* .line 185 */
final String v6 = "package_cloud"; // const-string v6, "package_cloud"
/* .line 186 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v0 */
/* .line 185 */
com.android.internal.logging.EventLogTags .writeCommitSysConfigFile ( v6,v7,v8 );
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 194 */
} // .end local v5 # "serializer":Lcom/android/modules/utils/TypedXmlSerializer;
/* .line 159 */
} // .end local v4 # "str":Ljava/io/FileOutputStream;
/* :catchall_0 */
/* move-exception v4 */
/* .line 188 */
/* .restart local v4 # "str":Ljava/io/FileOutputStream; */
/* :catch_0 */
/* move-exception v5 */
/* .line 189 */
/* .local v5, "e":Ljava/io/IOException; */
int v6 = 0; // const/4 v6, 0x0
try { // :try_start_2
/* iput v6, p0, Lcom/android/server/pm/PackageManagerCloudHelper;->mLastCloudConfigVersion:I */
/* .line 190 */
v6 = com.android.server.pm.PackageManagerCloudHelper.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Unable to write package manager cloud settings: "; // const-string v8, "Unable to write package manager cloud settings: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v5 ).getMessage ( ); // invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v7 );
/* .line 191 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 192 */
(( com.android.server.pm.ResilientAtomicFile ) v3 ).failWrite ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/pm/ResilientAtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 195 */
} // .end local v4 # "str":Ljava/io/FileOutputStream;
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
(( com.android.server.pm.ResilientAtomicFile ) v3 ).close ( ); // invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* .line 196 */
} // .end local v3 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
} // :cond_2
return;
/* .line 159 */
/* .restart local v3 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile; */
} // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_3
try { // :try_start_3
(( com.android.server.pm.ResilientAtomicFile ) v3 ).close ( ); // invoke-virtual {v3}, Lcom/android/server/pm/ResilientAtomicFile;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v5 */
(( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // :cond_3
} // :goto_3
/* throw v4 */
/* .line 157 */
} // .end local v3 # "atomicFile":Lcom/android/server/pm/ResilientAtomicFile;
/* :catchall_2 */
/* move-exception v4 */
try { // :try_start_4
/* monitor-exit v3 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v4 */
} // .end method
/* # virtual methods */
java.util.Set getCloudNotSupportUpdateSystemApps ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 45 */
v0 = this.mCloudNotSupportUpdateSystemApps;
} // .end method
void registerCloudDataObserver ( ) {
/* .locals 4 */
/* .line 70 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 71 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* new-instance v2, Lcom/android/server/pm/PackageManagerCloudHelper$1; */
v3 = this.mPms;
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/android/server/pm/PackageManagerCloudHelper$1;-><init>(Lcom/android/server/pm/PackageManagerCloudHelper;Landroid/os/Handler;)V */
/* .line 70 */
int v3 = 1; // const/4 v3, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 84 */
return;
} // .end method
