.class Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;
.super Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;
.source "PackageEventRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/pm/PackageEventRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PackageRemoveEvent"
.end annotation


# instance fields
.field private final isRemovedFully:Z


# direct methods
.method private constructor <init>(JLjava/lang/String;[ILjava/lang/String;Z)V
    .locals 7
    .param p1, "eventTime"    # J
    .param p3, "pkgName"    # Ljava/lang/String;
    .param p4, "userIds"    # [I
    .param p5, "installer"    # Ljava/lang/String;
    .param p6, "isRemovedFully"    # Z

    .line 610
    const/4 v0, 0x3

    invoke-static {p1, p2, v0}, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->buildPackageEventId(JI)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;-><init>(Ljava/lang/String;Ljava/lang/String;[ILjava/lang/String;Lcom/android/server/pm/PackageEventRecorder$PackageEventBase-IA;)V

    .line 611
    iput-boolean p6, p0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->isRemovedFully:Z

    .line 612
    return-void
.end method

.method synthetic constructor <init>(JLjava/lang/String;[ILjava/lang/String;ZLcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;-><init>(JLjava/lang/String;[ILjava/lang/String;Z)V

    return-void
.end method

.method private buildBundle()Landroid/os/Bundle;
    .locals 3

    .line 621
    invoke-static {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$mbuildBundle(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Landroid/os/Bundle;

    move-result-object v0

    .line 622
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "isRemovedFully"

    iget-boolean v2, p0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->isRemovedFully:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 623
    return-object v0
.end method


# virtual methods
.method WriteToTxt(Ljava/io/BufferedWriter;)V
    .locals 2
    .param p1, "bufferedWriter"    # Ljava/io/BufferedWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 615
    invoke-super {p0, p1}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->WriteToTxt(Ljava/io/BufferedWriter;)V

    .line 616
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isRemovedFully="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->isRemovedFully:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 617
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 618
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 628
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PackageEvent{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$fgetid(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", type=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$fgetid(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Ljava/lang/String;

    move-result-object v2

    .line 630
    invoke-static {v2}, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->resolveEventType(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", packageName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$fgetpackageName(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", userIds="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$fgetuserIds(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)[I

    move-result-object v2

    .line 632
    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", installerPackageName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;->-$$Nest$fgetinstaller(Lcom/android/server/pm/PackageEventRecorder$PackageEventBase;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isRemovedFully="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/pm/PackageEventRecorder$PackageRemoveEvent;->isRemovedFully:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 628
    return-object v0
.end method
