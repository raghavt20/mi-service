.class Lcom/android/server/MiuiUiModeManagerServiceStubImpl$1;
.super Ljava/lang/Object;
.source "MiuiUiModeManagerServiceStubImpl.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MiuiUiModeManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/MiuiUiModeManagerServiceStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/MiuiUiModeManagerServiceStubImpl;

    .line 63
    iput-object p1, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$1;->this$0:Lcom/android/server/MiuiUiModeManagerServiceStubImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 66
    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 6
    .param p1, "displayId"    # I

    .line 75
    iget-object v0, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$1;->this$0:Lcom/android/server/MiuiUiModeManagerServiceStubImpl;

    invoke-static {v0}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getBrightnessInfo()Landroid/hardware/display/BrightnessInfo;

    move-result-object v0

    .line 76
    .local v0, "info":Landroid/hardware/display/BrightnessInfo;
    if-nez v0, :cond_0

    .line 77
    return-void

    .line 79
    :cond_0
    iget v1, v0, Landroid/hardware/display/BrightnessInfo;->brightnessMaximum:F

    .line 80
    .local v1, "mMaximumBrightness":F
    iget v2, v0, Landroid/hardware/display/BrightnessInfo;->brightnessMinimum:F

    .line 81
    .local v2, "mMinimumBrightness":F
    iget v3, v0, Landroid/hardware/display/BrightnessInfo;->brightness:F

    .line 82
    .local v3, "mBrightnessValue":F
    iget-object v4, p0, Lcom/android/server/MiuiUiModeManagerServiceStubImpl$1;->this$0:Lcom/android/server/MiuiUiModeManagerServiceStubImpl;

    invoke-static {v4}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5, v3, v1, v2}, Lcom/android/server/MiuiUiModeManagerServiceStubImpl;->-$$Nest$mupdateAlpha(Lcom/android/server/MiuiUiModeManagerServiceStubImpl;Landroid/content/Context;FFF)V

    .line 83
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 70
    return-void
.end method
