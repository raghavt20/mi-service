.class public Lcom/android/server/ForceDarkAppListProvider;
.super Ljava/lang/Object;
.source "ForceDarkAppListProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;
    }
.end annotation


# static fields
.field private static final FORCE_DARK_APP_SETTINGS_FILE_PATH:Ljava/lang/String; = "system_ext/etc/forcedarkconfig/ForceDarkAppSettings.json"

.field private static final FORCE_DARK_APP_SETTINGS_MODULE_NAME:Ljava/lang/String; = "force_dark_app_settings_list"

.field private static final TAG:Ljava/lang/String;

.field private static volatile sForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;


# instance fields
.field private mAppListChangeListener:Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;

.field private mCloudForceDarkAppSettings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/DarkModeAppSettingsInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalForceDarkAppSettings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/DarkModeAppSettingsInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    const-class v0, Lcom/android/server/ForceDarkAppListProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ForceDarkAppListProvider;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ForceDarkAppListProvider;->sForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/ForceDarkAppListProvider;->mLocalForceDarkAppSettings:Ljava/util/HashMap;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/ForceDarkAppListProvider;->mCloudForceDarkAppSettings:Ljava/util/HashMap;

    .line 40
    invoke-direct {p0}, Lcom/android/server/ForceDarkAppListProvider;->initLocalForceDarkAppList()V

    .line 41
    return-void
.end method

.method private fillDarkModeAppSettingsInfo(Lcom/android/server/DarkModeAppSettingsInfo;Lorg/json/JSONObject;)V
    .locals 8
    .param p1, "darkModeAppSettingsInfo"    # Lcom/android/server/DarkModeAppSettingsInfo;
    .param p2, "appConfig"    # Lorg/json/JSONObject;

    .line 172
    const-string v0, "interceptRelaunch"

    const-string v1, "forceDarkSplashScreen"

    const-string v2, "forceDarkOrigin"

    const-string v3, "overrideEnableValue"

    const-string v4, "adaptStat"

    const-string v5, "defaultEnable"

    const-string/jumbo v6, "showInSettings"

    :try_start_0
    invoke-virtual {p2, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 173
    invoke-virtual {p2, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {p1, v6}, Lcom/android/server/DarkModeAppSettingsInfo;->setShowInSettings(Z)V

    .line 175
    :cond_0
    invoke-virtual {p2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 176
    invoke-virtual {p2, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {p1, v5}, Lcom/android/server/DarkModeAppSettingsInfo;->setDefaultEnable(Z)V

    .line 178
    :cond_1
    invoke-virtual {p2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 179
    invoke-virtual {p2, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/android/server/DarkModeAppSettingsInfo;->setAdaptStat(I)V

    .line 181
    :cond_2
    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 182
    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/android/server/DarkModeAppSettingsInfo;->setOverrideEnableValue(I)V

    .line 184
    :cond_3
    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 185
    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p1, v2}, Lcom/android/server/DarkModeAppSettingsInfo;->setForceDarkOrigin(Z)V

    .line 187
    :cond_4
    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 188
    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/android/server/DarkModeAppSettingsInfo;->setForceDarkSplashScreen(I)V

    .line 190
    :cond_5
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 191
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/android/server/DarkModeAppSettingsInfo;->setInterceptRelaunch(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_6
    goto :goto_0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 197
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_0
    return-void
.end method

.method public static getInstance()Lcom/android/server/ForceDarkAppListProvider;
    .locals 2

    .line 44
    sget-object v0, Lcom/android/server/ForceDarkAppListProvider;->sForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    if-nez v0, :cond_1

    .line 45
    const-class v0, Lcom/android/server/ForceDarkAppListProvider;

    monitor-enter v0

    .line 46
    :try_start_0
    sget-object v1, Lcom/android/server/ForceDarkAppListProvider;->sForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    if-nez v1, :cond_0

    .line 47
    new-instance v1, Lcom/android/server/ForceDarkAppListProvider;

    invoke-direct {v1}, Lcom/android/server/ForceDarkAppListProvider;-><init>()V

    sput-object v1, Lcom/android/server/ForceDarkAppListProvider;->sForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    .line 49
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 51
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/ForceDarkAppListProvider;->sForceDarkAppListProvider:Lcom/android/server/ForceDarkAppListProvider;

    return-object v0
.end method

.method private initLocalForceDarkAppList()V
    .locals 5

    .line 86
    const-string v0, ""

    .line 88
    .local v0, "jsonString":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    const-string/jumbo v2, "system_ext/etc/forcedarkconfig/ForceDarkAppSettings.json"

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 89
    .local v1, "f":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 91
    .local v2, "bis":Ljava/io/BufferedReader;
    :goto_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .local v4, "line":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 92
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v3

    goto :goto_0

    .line 94
    .end local v4    # "line":Ljava/lang/String;
    :cond_0
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 96
    .end local v2    # "bis":Ljava/io/BufferedReader;
    goto :goto_2

    .line 89
    .restart local v2    # "bis":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_5
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "jsonString":Ljava/lang/String;
    .end local v1    # "f":Ljava/io/FileInputStream;
    .end local p0    # "this":Lcom/android/server/ForceDarkAppListProvider;
    :goto_1
    throw v3
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 88
    .end local v2    # "bis":Ljava/io/BufferedReader;
    .restart local v0    # "jsonString":Ljava/lang/String;
    .restart local v1    # "f":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/android/server/ForceDarkAppListProvider;
    :catchall_2
    move-exception v2

    goto :goto_3

    .line 94
    :catch_0
    move-exception v2

    .line 95
    .local v2, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 97
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    .line 99
    .end local v1    # "f":Ljava/io/FileInputStream;
    goto :goto_5

    .line 88
    .restart local v1    # "f":Ljava/io/FileInputStream;
    :goto_3
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto :goto_4

    :catchall_3
    move-exception v3

    :try_start_9
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "jsonString":Ljava/lang/String;
    .end local p0    # "this":Lcom/android/server/ForceDarkAppListProvider;
    :goto_4
    throw v2
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    .line 97
    .end local v1    # "f":Ljava/io/FileInputStream;
    .restart local v0    # "jsonString":Ljava/lang/String;
    .restart local p0    # "this":Lcom/android/server/ForceDarkAppListProvider;
    :catch_1
    move-exception v1

    .line 98
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 101
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    return-void

    .line 105
    :cond_1
    invoke-direct {p0, v0}, Lcom/android/server/ForceDarkAppListProvider;->parseAppSettings2Map(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/ForceDarkAppListProvider;->mLocalForceDarkAppSettings:Ljava/util/HashMap;

    .line 106
    return-void
.end method

.method static synthetic lambda$getForceDarkAppSettings$0(Lcom/android/server/DarkModeAppSettingsInfo;Lcom/android/server/DarkModeAppSettingsInfo;)Lcom/android/server/DarkModeAppSettingsInfo;
    .locals 0
    .param p0, "v1"    # Lcom/android/server/DarkModeAppSettingsInfo;
    .param p1, "v2"    # Lcom/android/server/DarkModeAppSettingsInfo;

    .line 70
    return-object p1
.end method

.method static synthetic lambda$getForceDarkAppSettings$1(Ljava/util/HashMap;Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;)V
    .locals 1
    .param p0, "result"    # Ljava/util/HashMap;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/android/server/DarkModeAppSettingsInfo;

    .line 70
    new-instance v0, Lcom/android/server/ForceDarkAppListProvider$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/ForceDarkAppListProvider$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Ljava/util/HashMap;->merge(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/function/BiFunction;)Ljava/lang/Object;

    return-void
.end method

.method private parseAppSettings2Map(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6
    .param p1, "jsonData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/DarkModeAppSettingsInfo;",
            ">;"
        }
    .end annotation

    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 131
    .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    return-object v0

    .line 135
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 136
    .local v1, "appConfigArray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 137
    new-instance v3, Lcom/android/server/DarkModeAppSettingsInfo;

    invoke-direct {v3}, Lcom/android/server/DarkModeAppSettingsInfo;-><init>()V

    .line 138
    .local v3, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 139
    .local v4, "appConfig":Lorg/json/JSONObject;
    const-string v5, "packageName"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 140
    .local v5, "packageName":Ljava/lang/String;
    invoke-virtual {v3, v5}, Lcom/android/server/DarkModeAppSettingsInfo;->setPackageName(Ljava/lang/String;)V

    .line 141
    invoke-direct {p0, v3, v4}, Lcom/android/server/ForceDarkAppListProvider;->fillDarkModeAppSettingsInfo(Lcom/android/server/DarkModeAppSettingsInfo;Lorg/json/JSONObject;)V

    .line 142
    invoke-virtual {v0, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    nop

    .end local v3    # "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    .end local v4    # "appConfig":Lorg/json/JSONObject;
    .end local v5    # "packageName":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 146
    .end local v1    # "appConfigArray":Lorg/json/JSONArray;
    .end local v2    # "i":I
    :cond_1
    goto :goto_1

    .line 144
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/android/server/ForceDarkAppListProvider;->TAG:Ljava/lang/String;

    const-string v3, "exception when parseAppConfig2Map: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 147
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_1
    return-object v0
.end method

.method private parseAppSettings2Map(Lorg/json/JSONArray;)Ljava/util/HashMap;
    .locals 5
    .param p1, "appConfigArray"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/DarkModeAppSettingsInfo;",
            ">;"
        }
    .end annotation

    .line 151
    if-nez p1, :cond_0

    .line 152
    const/4 v0, 0x0

    return-object v0

    .line 154
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 156
    .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 157
    new-instance v2, Lcom/android/server/DarkModeAppSettingsInfo;

    invoke-direct {v2}, Lcom/android/server/DarkModeAppSettingsInfo;-><init>()V

    .line 158
    .local v2, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 159
    .local v3, "appConfig":Lorg/json/JSONObject;
    const-string v4, "packageName"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 160
    .local v4, "packageName":Ljava/lang/String;
    invoke-virtual {v2, v4}, Lcom/android/server/DarkModeAppSettingsInfo;->setPackageName(Ljava/lang/String;)V

    .line 161
    invoke-direct {p0, v2, v3}, Lcom/android/server/ForceDarkAppListProvider;->fillDarkModeAppSettingsInfo(Lcom/android/server/DarkModeAppSettingsInfo;Lorg/json/JSONObject;)V

    .line 162
    invoke-virtual {v0, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    nop

    .end local v2    # "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    .end local v3    # "appConfig":Lorg/json/JSONObject;
    .end local v4    # "packageName":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 166
    .end local v1    # "i":I
    :cond_1
    goto :goto_1

    .line 164
    :catch_0
    move-exception v1

    .line 165
    .local v1, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/android/server/ForceDarkAppListProvider;->TAG:Ljava/lang/String;

    const-string v3, "exception when parseAppConfig2Map: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 167
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_1
    return-object v0
.end method

.method private registerDataObserver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 200
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 201
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/ForceDarkAppListProvider$1;

    .line 202
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1}, Lcom/android/server/ForceDarkAppListProvider$1;-><init>(Lcom/android/server/ForceDarkAppListProvider;Landroid/os/Handler;Landroid/content/Context;)V

    .line 200
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 209
    return-void
.end method


# virtual methods
.method public getForceDarkAppDefaultEnable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 75
    invoke-virtual {p0}, Lcom/android/server/ForceDarkAppListProvider;->getForceDarkAppSettings()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/DarkModeAppSettingsInfo;

    .line 76
    .local v0, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->isDefaultEnable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public getForceDarkAppSettings()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/DarkModeAppSettingsInfo;",
            ">;"
        }
    .end annotation

    .line 68
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/server/ForceDarkAppListProvider;->mLocalForceDarkAppSettings:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 69
    .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;"
    iget-object v1, p0, Lcom/android/server/ForceDarkAppListProvider;->mCloudForceDarkAppSettings:Ljava/util/HashMap;

    new-instance v2, Lcom/android/server/ForceDarkAppListProvider$$ExternalSyntheticLambda1;

    invoke-direct {v2, v0}, Lcom/android/server/ForceDarkAppListProvider$$ExternalSyntheticLambda1;-><init>(Ljava/util/HashMap;)V

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V

    .line 71
    return-object v0
.end method

.method public onBootPhase(ILandroid/content/Context;)V
    .locals 0
    .param p1, "phase"    # I
    .param p2, "context"    # Landroid/content/Context;

    .line 80
    invoke-virtual {p0, p2}, Lcom/android/server/ForceDarkAppListProvider;->updateCloudForceDarkAppList(Landroid/content/Context;)V

    .line 81
    invoke-direct {p0, p2}, Lcom/android/server/ForceDarkAppListProvider;->registerDataObserver(Landroid/content/Context;)V

    .line 82
    return-void
.end method

.method public setAppListChangeListener(Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;)V
    .locals 0
    .param p1, "listChangeListener"    # Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;

    .line 60
    iput-object p1, p0, Lcom/android/server/ForceDarkAppListProvider;->mAppListChangeListener:Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;

    .line 61
    return-void
.end method

.method public updateCloudForceDarkAppList(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 111
    const-string v0, "force_dark_app_settings_list"

    .line 112
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 111
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v0, v0, v2, v3}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v1

    .line 113
    .local v1, "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 114
    sget-object v2, Lcom/android/server/ForceDarkAppListProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCloudForceDarkAppList: CloudData: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v2, p0, Lcom/android/server/ForceDarkAppListProvider;->mCloudForceDarkAppSettings:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 116
    invoke-virtual {v1}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 117
    .local v0, "appConfigArray":Lorg/json/JSONArray;
    invoke-direct {p0, v0}, Lcom/android/server/ForceDarkAppListProvider;->parseAppSettings2Map(Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/ForceDarkAppListProvider;->mCloudForceDarkAppSettings:Ljava/util/HashMap;

    .line 119
    iget-object v2, p0, Lcom/android/server/ForceDarkAppListProvider;->mAppListChangeListener:Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;

    if-eqz v2, :cond_0

    .line 120
    invoke-interface {v2}, Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;->onChange()V

    .line 123
    .end local v0    # "appConfigArray":Lorg/json/JSONArray;
    :cond_0
    sget-object v0, Lcom/android/server/ForceDarkAppListProvider;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCloudForceDarkAppList: mCloudForceDarkAppSettings: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/ForceDarkAppListProvider;->mCloudForceDarkAppSettings:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    nop

    .end local v1    # "data":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/android/server/ForceDarkAppListProvider;->TAG:Ljava/lang/String;

    const-string v2, "exception when getCloudForceDarkAppList: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
