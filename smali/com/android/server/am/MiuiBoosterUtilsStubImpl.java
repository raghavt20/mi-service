public class com.android.server.am.MiuiBoosterUtilsStubImpl implements com.android.server.am.MiuiBoosterUtilsStub {
	 /* .source "MiuiBoosterUtilsStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLOUD_MIUIBOOSTER_PRIO;
public static final Integer COLD_LAUNCH_ATT_RT_DURATION_MS;
public static final Integer COLD_LAUNCH_REN_RT_DURATION_MS;
private static final Boolean DEBUG;
public static final Integer HOT_LAUNCH_RT_DURATION_MS;
private static final Integer MSG_REGISTER_CLOUD_OBSERVER;
private static final Integer REQUEST_THREAD_PRIORITY;
public static final Integer SCROLL_RT_DURATION_MS;
private static final java.lang.String TAG;
public static final Integer THREAD_PRIORITY_NORMAL_MAX;
private static final java.lang.String defaultPkg;
private static Boolean enable_miuibooster;
private static Boolean enable_miuibooster_launch;
private static Boolean hasPermission;
private static Boolean isInit;
private static android.os.IBinder sService;
/* # instance fields */
private android.content.Context mContext;
private android.os.Handler mH;
private android.os.HandlerThread mHandlerThread;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.am.MiuiBoosterUtilsStubImpl p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static com.android.server.am.MiuiBoosterUtilsStubImpl ( ) {
	 /* .locals 3 */
	 /* .line 31 */
	 final String v0 = "persist.miuibooster.debug"; // const-string v0, "persist.miuibooster.debug"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.am.MiuiBoosterUtilsStubImpl.DEBUG = (v0!= 0);
	 /* .line 35 */
	 /* nop */
	 /* .line 36 */
	 final String v0 = "persist.sys.miuibooster.rtmode"; // const-string v0, "persist.sys.miuibooster.rtmode"
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.am.MiuiBoosterUtilsStubImpl.enable_miuibooster = (v0!= 0);
	 /* .line 37 */
	 /* nop */
	 /* .line 38 */
	 final String v0 = "persist.sys.miuibooster.launch.rtmode"; // const-string v0, "persist.sys.miuibooster.launch.rtmode"
	 int v2 = 1; // const/4 v2, 0x1
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v2 );
	 com.android.server.am.MiuiBoosterUtilsStubImpl.enable_miuibooster_launch = (v0!= 0);
	 /* .line 39 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 40 */
	 com.android.server.am.MiuiBoosterUtilsStubImpl.isInit = (v1!= 0);
	 /* .line 41 */
	 com.android.server.am.MiuiBoosterUtilsStubImpl.hasPermission = (v1!= 0);
	 return;
} // .end method
public com.android.server.am.MiuiBoosterUtilsStubImpl ( ) {
	 /* .locals 1 */
	 /* .line 23 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 46 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mContext = v0;
	 return;
} // .end method
/* # virtual methods */
public void cancelBindCore ( Integer p0, Integer p1 ) {
	 /* .locals 5 */
	 /* .param p1, "uid" # I */
	 /* .param p2, "req_id" # I */
	 /* .line 225 */
	 /* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
	 final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 226 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v2, "thread affinity is canceled, uid:" */
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = " ,req_id: "; // const-string v2, " ,req_id: "
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .d ( v1,v0 );
		 /* .line 228 */
	 } // :cond_0
	 android.os.Parcel .obtain ( );
	 /* .line 229 */
	 /* .local v0, "data":Landroid/os/Parcel; */
	 android.os.Parcel .obtain ( );
	 /* .line 231 */
	 /* .local v2, "reply":Landroid/os/Parcel; */
	 try { // :try_start_0
		 v3 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
		 /* if-nez v3, :cond_1 */
		 final String v3 = "miuiboosterservice"; // const-string v3, "miuiboosterservice"
		 android.os.ServiceManager .getService ( v3 );
	 } // :cond_1
	 /* .line 232 */
	 /* if-nez v3, :cond_4 */
	 /* .line 233 */
	 final String v3 = "miuibooster service is null"; // const-string v3, "miuibooster service is null"
	 android.util.Slog .e ( v1,v3 );
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 245 */
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 246 */
		 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
		 /* .line 248 */
	 } // :cond_2
	 if ( v2 != null) { // if-eqz v2, :cond_3
		 /* .line 249 */
		 (( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
		 /* .line 234 */
	 } // :cond_3
	 return;
	 /* .line 236 */
} // :cond_4
try { // :try_start_1
	 final String v1 = "com.miui.performance.IMiuiBoosterManager"; // const-string v1, "com.miui.performance.IMiuiBoosterManager"
	 (( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
	 /* .line 237 */
	 (( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
	 /* .line 238 */
	 (( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
	 /* .line 239 */
	 v1 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
	 /* const/16 v3, 0x16 */
	 int v4 = 0; // const/4 v4, 0x0
	 /* :try_end_1 */
	 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 245 */
	 if ( v0 != null) { // if-eqz v0, :cond_5
		 /* .line 246 */
		 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
		 /* .line 248 */
	 } // :cond_5
	 if ( v2 != null) { // if-eqz v2, :cond_8
		 /* .line 249 */
		 /* .line 245 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* .line 240 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 241 */
		 /* .local v1, "e":Ljava/lang/Exception; */
		 try { // :try_start_2
			 /* sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
			 if ( v3 != null) { // if-eqz v3, :cond_6
				 /* .line 242 */
				 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
				 /* :try_end_2 */
				 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
				 /* .line 245 */
			 } // .end local v1 # "e":Ljava/lang/Exception;
		 } // :cond_6
		 if ( v0 != null) { // if-eqz v0, :cond_7
			 /* .line 246 */
			 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
			 /* .line 248 */
		 } // :cond_7
		 if ( v2 != null) { // if-eqz v2, :cond_8
			 /* .line 249 */
		 } // :goto_0
		 (( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
		 /* .line 252 */
	 } // :cond_8
	 return;
	 /* .line 245 */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_9
	 /* .line 246 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 248 */
} // :cond_9
if ( v2 != null) { // if-eqz v2, :cond_a
	 /* .line 249 */
	 (( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
	 /* .line 251 */
} // :cond_a
/* throw v1 */
} // .end method
public void cancelCpuHighFreq ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 292 */
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 293 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "CpuHighFreq is canceled, uid:"; // const-string v2, "CpuHighFreq is canceled, uid:"
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v0 );
	 /* .line 295 */
} // :cond_0
android.os.Parcel .obtain ( );
/* .line 296 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 298 */
/* .local v2, "reply":Landroid/os/Parcel; */
try { // :try_start_0
	 v3 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
	 /* if-nez v3, :cond_1 */
	 final String v3 = "miuiboosterservice"; // const-string v3, "miuiboosterservice"
	 android.os.ServiceManager .getService ( v3 );
} // :cond_1
/* .line 299 */
/* if-nez v3, :cond_4 */
/* .line 300 */
final String v3 = "miuibooster service is null"; // const-string v3, "miuibooster service is null"
android.util.Slog .e ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 311 */
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 312 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 314 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 315 */
	 (( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
	 /* .line 301 */
} // :cond_3
return;
/* .line 303 */
} // :cond_4
try { // :try_start_1
final String v1 = "com.miui.performance.IMiuiBoosterManager"; // const-string v1, "com.miui.performance.IMiuiBoosterManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 304 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 305 */
v1 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
int v3 = 5; // const/4 v3, 0x5
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 311 */
if ( v0 != null) { // if-eqz v0, :cond_5
	 /* .line 312 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 314 */
} // :cond_5
if ( v2 != null) { // if-eqz v2, :cond_8
	 /* .line 315 */
	 /* .line 311 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* .line 306 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 307 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 try { // :try_start_2
		 /* sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
		 if ( v3 != null) { // if-eqz v3, :cond_6
			 /* .line 308 */
			 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
			 /* :try_end_2 */
			 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
			 /* .line 311 */
		 } // .end local v1 # "e":Ljava/lang/Exception;
	 } // :cond_6
	 if ( v0 != null) { // if-eqz v0, :cond_7
		 /* .line 312 */
		 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
		 /* .line 314 */
	 } // :cond_7
	 if ( v2 != null) { // if-eqz v2, :cond_8
		 /* .line 315 */
	 } // :goto_0
	 (( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
	 /* .line 318 */
} // :cond_8
return;
/* .line 311 */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 312 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 314 */
} // :cond_9
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 315 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 317 */
} // :cond_a
/* throw v1 */
} // .end method
public void cancelThreadPriority ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "req_id" # I */
/* .line 149 */
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 150 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "thread_priority is canceled, uid:" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " ,req_id: "; // const-string v2, " ,req_id: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 152 */
} // :cond_0
android.os.Parcel .obtain ( );
/* .line 153 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 155 */
/* .local v2, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v3 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* if-nez v3, :cond_1 */
final String v3 = "miuiboosterservice"; // const-string v3, "miuiboosterservice"
android.os.ServiceManager .getService ( v3 );
} // :cond_1
/* .line 156 */
/* if-nez v3, :cond_4 */
/* .line 157 */
final String v3 = "miuibooster service is null"; // const-string v3, "miuibooster service is null"
android.util.Slog .e ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 169 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 170 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 172 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 173 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 158 */
} // :cond_3
return;
/* .line 160 */
} // :cond_4
try { // :try_start_1
final String v1 = "com.miui.performance.IMiuiBoosterManager"; // const-string v1, "com.miui.performance.IMiuiBoosterManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 161 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 162 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 163 */
v1 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* const/16 v3, 0x1c */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 169 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 170 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 172 */
} // :cond_5
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 173 */
/* .line 169 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 164 */
/* :catch_0 */
/* move-exception v1 */
/* .line 165 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
	 /* sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
	 if ( v3 != null) { // if-eqz v3, :cond_6
		 /* .line 166 */
		 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
		 /* :try_end_2 */
		 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
		 /* .line 169 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_6
if ( v0 != null) { // if-eqz v0, :cond_7
	 /* .line 170 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 172 */
} // :cond_7
if ( v2 != null) { // if-eqz v2, :cond_8
	 /* .line 173 */
} // :goto_0
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 176 */
} // :cond_8
return;
/* .line 169 */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 170 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 172 */
} // :cond_9
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 173 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 175 */
} // :cond_a
/* throw v1 */
} // .end method
public Boolean checkPermission ( java.lang.String p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 64 */
int v0 = -1; // const/4 v0, -0x1
/* .line 65 */
/* .local v0, "ret":I */
/* sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
final String v2 = "MiuiBoosterUtils"; // const-string v2, "MiuiBoosterUtils"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 66 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "check_permission ,uid: "; // const-string v3, "check_permission ,uid: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " ,package_name: "; // const-string v3, " ,package_name: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 68 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->hasPermission:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 69 */
/* .line 71 */
} // :cond_1
android.os.Parcel .obtain ( );
/* .line 72 */
/* .local v1, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 74 */
/* .local v3, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v4 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* if-nez v4, :cond_2 */
final String v4 = "miuiboosterservice"; // const-string v4, "miuiboosterservice"
android.os.ServiceManager .getService ( v4 );
} // :cond_2
/* .line 75 */
int v5 = 0; // const/4 v5, 0x0
/* if-nez v4, :cond_5 */
/* .line 76 */
final String v4 = "miuibooster service is null"; // const-string v4, "miuibooster service is null"
android.util.Slog .e ( v2,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 77 */
/* nop */
/* .line 86 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 87 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 89 */
} // :cond_3
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 90 */
(( android.os.Parcel ) v3 ).readException ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
/* .line 91 */
v0 = (( android.os.Parcel ) v3 ).readInt ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I
/* .line 92 */
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 77 */
} // :cond_4
/* .line 79 */
} // :cond_5
try { // :try_start_1
final String v2 = "com.miui.performance.IMiuiBoosterManager"; // const-string v2, "com.miui.performance.IMiuiBoosterManager"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 80 */
(( android.os.Parcel ) v1 ).writeString ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 81 */
(( android.os.Parcel ) v1 ).writeInt ( p2 ); // invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 82 */
v2 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
int v4 = 2; // const/4 v4, 0x2
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 86 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 87 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 89 */
} // :cond_6
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 90 */
/* .line 86 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 83 */
/* :catch_0 */
/* move-exception v2 */
/* .line 84 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 86 */
} // .end local v2 # "e":Ljava/lang/Exception;
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 87 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 89 */
} // :cond_7
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 90 */
} // :goto_0
(( android.os.Parcel ) v3 ).readException ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
/* .line 91 */
v0 = (( android.os.Parcel ) v3 ).readInt ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I
/* .line 92 */
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 95 */
} // :cond_8
/* if-nez v0, :cond_9 */
/* .line 96 */
/* sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->hasPermission:Z */
/* .line 98 */
} // :cond_9
int v2 = 1; // const/4 v2, 0x1
com.android.server.am.MiuiBoosterUtilsStubImpl.hasPermission = (v2!= 0);
/* .line 99 */
/* .line 86 */
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 87 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 89 */
} // :cond_a
if ( v3 != null) { // if-eqz v3, :cond_b
/* .line 90 */
(( android.os.Parcel ) v3 ).readException ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
/* .line 91 */
v0 = (( android.os.Parcel ) v3 ).readInt ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I
/* .line 92 */
(( android.os.Parcel ) v3 ).recycle ( ); // invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V
/* .line 94 */
} // :cond_b
/* throw v2 */
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 321 */
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 322 */
return;
/* .line 324 */
} // :cond_0
this.mContext = p1;
/* .line 325 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "MiuiBoosterUtilsTh"; // const-string v1, "MiuiBoosterUtilsTh"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 326 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 327 */
/* new-instance v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1;-><init>(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;Landroid/os/Looper;)V */
this.mH = v0;
/* .line 348 */
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 349 */
/* .local v0, "msg":Landroid/os/Message; */
v2 = this.mH;
(( android.os.Handler ) v2 ).sendMessage ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 350 */
com.android.server.am.MiuiBoosterUtilsStubImpl.isInit = (v1!= 0);
/* .line 351 */
final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
final String v2 = "init MiuiBoosterUtils"; // const-string v2, "init MiuiBoosterUtils"
android.util.Slog .w ( v1,v2 );
/* .line 352 */
return;
} // .end method
public void notifyForegroundAppChanged ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 380 */
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 381 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyForegroundAppChanged: packageName="; // const-string v2, "notifyForegroundAppChanged: packageName="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 384 */
} // :cond_0
android.os.Parcel .obtain ( );
/* .line 385 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 387 */
/* .local v2, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v3 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* if-nez v3, :cond_1 */
final String v3 = "miuiboosterservice"; // const-string v3, "miuiboosterservice"
android.os.ServiceManager .getService ( v3 );
} // :cond_1
/* .line 388 */
/* if-nez v3, :cond_4 */
/* .line 389 */
final String v3 = "miuibooster service is null"; // const-string v3, "miuibooster service is null"
android.util.Slog .e ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 398 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 399 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 401 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 402 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 390 */
} // :cond_3
return;
/* .line 392 */
} // :cond_4
try { // :try_start_1
final String v1 = "com.miui.performance.IMiuiBoosterManager"; // const-string v1, "com.miui.performance.IMiuiBoosterManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 393 */
(( android.os.Parcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 394 */
v1 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* const/16 v3, 0x34 */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 398 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 399 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 401 */
} // :cond_5
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 402 */
/* .line 398 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 395 */
/* :catch_0 */
/* move-exception v1 */
/* .line 396 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 398 */
} // .end local v1 # "e":Ljava/lang/Exception;
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 399 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 401 */
} // :cond_6
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 402 */
} // :goto_0
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 405 */
} // :cond_7
return;
/* .line 398 */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 399 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 401 */
} // :cond_8
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 402 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 404 */
} // :cond_9
/* throw v1 */
} // .end method
public void notifyPowerModeChanged ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "isPowerMode" # Z */
/* .line 408 */
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 409 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyPowerModeStatus: isPowerMode="; // const-string v2, "notifyPowerModeStatus: isPowerMode="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 412 */
} // :cond_0
android.os.Parcel .obtain ( );
/* .line 413 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 415 */
/* .local v2, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v3 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* if-nez v3, :cond_1 */
final String v3 = "miuiboosterservice"; // const-string v3, "miuiboosterservice"
android.os.ServiceManager .getService ( v3 );
} // :cond_1
/* .line 416 */
/* if-nez v3, :cond_4 */
/* .line 417 */
final String v3 = "miuibooster service is null"; // const-string v3, "miuibooster service is null"
android.util.Slog .e ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 426 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 427 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 429 */
} // :cond_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 430 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 418 */
} // :cond_3
return;
/* .line 420 */
} // :cond_4
try { // :try_start_1
final String v1 = "com.miui.performance.IMiuiBoosterManager"; // const-string v1, "com.miui.performance.IMiuiBoosterManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 421 */
(( android.os.Parcel ) v0 ).writeBoolean ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V
/* .line 422 */
v1 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* const/16 v3, 0x39 */
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 426 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 427 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 429 */
} // :cond_5
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 430 */
/* .line 426 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 423 */
/* :catch_0 */
/* move-exception v1 */
/* .line 424 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 426 */
} // .end local v1 # "e":Ljava/lang/Exception;
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 427 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 429 */
} // :cond_6
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 430 */
} // :goto_0
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 433 */
} // :cond_7
return;
/* .line 426 */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 427 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 429 */
} // :cond_8
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 430 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 432 */
} // :cond_9
/* throw v1 */
} // .end method
public void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 355 */
/* new-instance v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$2; */
v1 = this.mH;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$2;-><init>(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;Landroid/os/Handler;)V */
/* .line 363 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 364 */
final String v2 = "cloud_schedboost_enable"; // const-string v2, "cloud_schedboost_enable"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 363 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 366 */
final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
final String v2 = "registerCloudObserver: cloud_schedboost_enable"; // const-string v2, "registerCloudObserver: cloud_schedboost_enable"
android.util.Slog .w ( v1,v2 );
/* .line 367 */
return;
} // .end method
public Integer requestBindCore ( Integer p0, Integer[] p1, Integer p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .param p2, "req_tids" # [I */
/* .param p3, "timeout" # I */
/* .param p4, "level" # I */
/* .line 179 */
int v0 = -1; // const/4 v0, -0x1
/* .line 180 */
/* .local v0, "ret":I */
/* sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
final String v2 = "MiuiBoosterUtils"; // const-string v2, "MiuiBoosterUtils"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 181 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "thread affinity is reuqesting, uid:" */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " ,req_tids: "; // const-string v3, " ,req_tids: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p2 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " ,timeout:"; // const-string v3, " ,timeout:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 183 */
} // :cond_0
/* const-string/jumbo v1, "systemCall" */
v1 = (( com.android.server.am.MiuiBoosterUtilsStubImpl ) p0 ).checkPermission ( v1, p1 ); // invoke-virtual {p0, v1, p1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->checkPermission(Ljava/lang/String;I)Z
/* if-nez v1, :cond_1 */
/* .line 184 */
final String v1 = "check_permission : false"; // const-string v1, "check_permission : false"
android.util.Slog .w ( v2,v1 );
/* .line 185 */
/* .line 187 */
} // :cond_1
/* sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
if ( v1 != null) { // if-eqz v1, :cond_f
/* sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
/* if-nez v1, :cond_2 */
/* goto/16 :goto_4 */
/* .line 191 */
} // :cond_2
/* if-nez p2, :cond_3 */
/* .line 192 */
} // :cond_3
/* array-length v1, p2 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v1, :cond_5 */
/* aget v5, p2, v4 */
/* .line 193 */
/* .local v5, "tid":I */
/* if-gtz v5, :cond_4 */
/* .line 192 */
} // .end local v5 # "tid":I
} // :cond_4
/* add-int/lit8 v4, v4, 0x1 */
/* .line 195 */
} // :cond_5
android.os.Parcel .obtain ( );
/* .line 196 */
/* .local v1, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 198 */
/* .local v4, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v5 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* if-nez v5, :cond_6 */
final String v5 = "miuiboosterservice"; // const-string v5, "miuiboosterservice"
android.os.ServiceManager .getService ( v5 );
} // :cond_6
/* .line 199 */
/* if-nez v5, :cond_9 */
/* .line 200 */
final String v3 = "miuibooster service is null"; // const-string v3, "miuibooster service is null"
android.util.Slog .e ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 201 */
/* nop */
/* .line 212 */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 213 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 215 */
} // :cond_7
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 216 */
(( android.os.Parcel ) v4 ).readException ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
/* .line 217 */
v2 = (( android.os.Parcel ) v4 ).readInt ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I
/* .line 218 */
} // .end local v0 # "ret":I
/* .local v2, "ret":I */
(( android.os.Parcel ) v4 ).recycle ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V
/* .line 215 */
} // .end local v2 # "ret":I
/* .restart local v0 # "ret":I */
} // :cond_8
/* move v2, v0 */
/* .line 201 */
} // .end local v0 # "ret":I
/* .restart local v2 # "ret":I */
} // :goto_1
/* .line 203 */
} // .end local v2 # "ret":I
/* .restart local v0 # "ret":I */
} // :cond_9
try { // :try_start_1
final String v2 = "com.miui.performance.IMiuiBoosterManager"; // const-string v2, "com.miui.performance.IMiuiBoosterManager"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 204 */
(( android.os.Parcel ) v1 ).writeInt ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 205 */
(( android.os.Parcel ) v1 ).writeIntArray ( p2 ); // invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeIntArray([I)V
/* .line 206 */
(( android.os.Parcel ) v1 ).writeInt ( p3 ); // invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 207 */
(( android.os.Parcel ) v1 ).writeInt ( p4 ); // invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 208 */
v2 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* const/16 v5, 0x15 */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 212 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 213 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 215 */
} // :cond_a
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 216 */
/* .line 212 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 209 */
/* :catch_0 */
/* move-exception v2 */
/* .line 210 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 212 */
} // .end local v2 # "e":Ljava/lang/Exception;
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 213 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 215 */
} // :cond_b
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 216 */
} // :goto_2
(( android.os.Parcel ) v4 ).readException ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
/* .line 217 */
v0 = (( android.os.Parcel ) v4 ).readInt ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I
/* .line 218 */
(( android.os.Parcel ) v4 ).recycle ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V
/* .line 221 */
} // :cond_c
/* .line 212 */
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_d
/* .line 213 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 215 */
} // :cond_d
if ( v4 != null) { // if-eqz v4, :cond_e
/* .line 216 */
(( android.os.Parcel ) v4 ).readException ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
/* .line 217 */
v0 = (( android.os.Parcel ) v4 ).readInt ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I
/* .line 218 */
(( android.os.Parcel ) v4 ).recycle ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V
/* .line 220 */
} // :cond_e
/* throw v2 */
/* .line 188 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // .end local v4 # "reply":Landroid/os/Parcel;
} // :cond_f
} // :goto_4
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "enable_miuibooster : "; // const-string v3, "enable_miuibooster : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " ,isInit: "; // const-string v3, " ,isInit: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v1 );
/* .line 189 */
} // .end method
public void requestCpuHighFreq ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "level" # I */
/* .param p3, "timeout" # I */
/* .line 255 */
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 256 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "CpuHighFreq is reuqesting, uid:"; // const-string v2, "CpuHighFreq is reuqesting, uid:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " ,level: "; // const-string v2, " ,level: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " ,timeout:"; // const-string v2, " ,timeout:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 258 */
} // :cond_0
/* const-string/jumbo v0, "systemCall" */
v0 = (( com.android.server.am.MiuiBoosterUtilsStubImpl ) p0 ).checkPermission ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->checkPermission(Ljava/lang/String;I)Z
/* if-nez v0, :cond_1 */
/* .line 259 */
final String v0 = "check_permission : false"; // const-string v0, "check_permission : false"
android.util.Slog .w ( v1,v0 );
/* .line 260 */
return;
/* .line 262 */
} // :cond_1
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
if ( v0 != null) { // if-eqz v0, :cond_c
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
/* if-nez v0, :cond_2 */
/* .line 266 */
} // :cond_2
android.os.Parcel .obtain ( );
/* .line 267 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 269 */
/* .local v2, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v3 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* if-nez v3, :cond_3 */
final String v3 = "miuiboosterservice"; // const-string v3, "miuiboosterservice"
android.os.ServiceManager .getService ( v3 );
} // :cond_3
/* .line 270 */
/* if-nez v3, :cond_6 */
/* .line 271 */
final String v3 = "miuibooster service is null"; // const-string v3, "miuibooster service is null"
android.util.Slog .e ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 282 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 283 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 285 */
} // :cond_4
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 286 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 272 */
} // :cond_5
return;
/* .line 274 */
} // :cond_6
try { // :try_start_1
final String v1 = "com.miui.performance.IMiuiBoosterManager"; // const-string v1, "com.miui.performance.IMiuiBoosterManager"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 275 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 276 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 277 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 278 */
v1 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
int v3 = 4; // const/4 v3, 0x4
int v4 = 0; // const/4 v4, 0x0
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 282 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 283 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 285 */
} // :cond_7
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 286 */
/* .line 282 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 279 */
/* :catch_0 */
/* move-exception v1 */
/* .line 280 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 282 */
} // .end local v1 # "e":Ljava/lang/Exception;
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 283 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 285 */
} // :cond_8
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 286 */
} // :goto_0
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 289 */
} // :cond_9
return;
/* .line 282 */
} // :goto_1
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 283 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 285 */
} // :cond_a
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 286 */
(( android.os.Parcel ) v2 ).recycle ( ); // invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
/* .line 288 */
} // :cond_b
/* throw v1 */
/* .line 263 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v2 # "reply":Landroid/os/Parcel;
} // :cond_c
} // :goto_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enable_miuibooster : "; // const-string v2, "enable_miuibooster : "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " ,isInit: "; // const-string v2, " ,isInit: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 264 */
return;
} // .end method
public void requestLaunchThreadPriority ( Integer p0, Integer[] p1, Integer p2, Integer p3 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "req_tids" # [I */
/* .param p3, "timeout" # I */
/* .param p4, "level" # I */
/* .line 49 */
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster_launch:Z */
final String v1 = "MiuiBoosterUtils"; // const-string v1, "MiuiBoosterUtils"
/* if-nez v0, :cond_0 */
/* .line 50 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enable_miuibooster_launch : "; // const-string v2, "enable_miuibooster_launch : "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster_launch:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 51 */
return;
/* .line 53 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
/* if-nez v0, :cond_1 */
/* .line 57 */
} // :cond_1
/* new-instance v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo; */
/* move-object v2, v0 */
/* move-object v3, p0 */
/* move v4, p1 */
/* move-object v5, p2 */
/* move v6, p3 */
/* move v7, p4 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;-><init>(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;I[III)V */
/* .line 58 */
/* .local v0, "info":Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo; */
v1 = this.mH;
int v2 = 2; // const/4 v2, 0x2
(( android.os.Handler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 59 */
/* .local v1, "msg":Landroid/os/Message; */
this.obj = v0;
/* .line 60 */
v2 = this.mH;
(( android.os.Handler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 61 */
return;
/* .line 54 */
} // .end local v0 # "info":Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_2
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enable_miuibooster : "; // const-string v2, "enable_miuibooster : "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " ,isInit: "; // const-string v2, " ,isInit: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 55 */
return;
} // .end method
public Integer requestThreadPriority ( Integer p0, Integer[] p1, Integer p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .param p2, "req_tids" # [I */
/* .param p3, "timeout" # I */
/* .param p4, "level" # I */
/* .line 103 */
int v0 = -1; // const/4 v0, -0x1
/* .line 104 */
/* .local v0, "ret":I */
/* sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z */
final String v2 = "MiuiBoosterUtils"; // const-string v2, "MiuiBoosterUtils"
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 105 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "thread_priority is reuqesting, uid:" */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " ,req_tids: "; // const-string v3, " ,req_tids: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p2 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " ,timeout:"; // const-string v3, " ,timeout:"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 107 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
if ( v1 != null) { // if-eqz v1, :cond_f
/* sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
/* if-nez v1, :cond_1 */
/* goto/16 :goto_4 */
/* .line 111 */
} // :cond_1
/* const-string/jumbo v1, "systemCall" */
v1 = (( com.android.server.am.MiuiBoosterUtilsStubImpl ) p0 ).checkPermission ( v1, p1 ); // invoke-virtual {p0, v1, p1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->checkPermission(Ljava/lang/String;I)Z
/* if-nez v1, :cond_2 */
/* .line 112 */
final String v1 = "check_permission : false"; // const-string v1, "check_permission : false"
android.util.Slog .w ( v2,v1 );
/* .line 113 */
/* .line 115 */
} // :cond_2
/* if-nez p2, :cond_3 */
/* .line 116 */
} // :cond_3
/* array-length v1, p2 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v1, :cond_5 */
/* aget v5, p2, v4 */
/* .line 117 */
/* .local v5, "tid":I */
/* if-gtz v5, :cond_4 */
/* .line 116 */
} // .end local v5 # "tid":I
} // :cond_4
/* add-int/lit8 v4, v4, 0x1 */
/* .line 119 */
} // :cond_5
android.os.Parcel .obtain ( );
/* .line 120 */
/* .local v1, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 122 */
/* .local v4, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v5 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* if-nez v5, :cond_6 */
final String v5 = "miuiboosterservice"; // const-string v5, "miuiboosterservice"
android.os.ServiceManager .getService ( v5 );
} // :cond_6
/* .line 123 */
/* if-nez v5, :cond_9 */
/* .line 124 */
final String v3 = "miuibooster service is null"; // const-string v3, "miuibooster service is null"
android.util.Slog .e ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 125 */
/* nop */
/* .line 136 */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 137 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 139 */
} // :cond_7
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 140 */
(( android.os.Parcel ) v4 ).readException ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
/* .line 141 */
v2 = (( android.os.Parcel ) v4 ).readInt ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I
/* .line 142 */
} // .end local v0 # "ret":I
/* .local v2, "ret":I */
(( android.os.Parcel ) v4 ).recycle ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V
/* .line 139 */
} // .end local v2 # "ret":I
/* .restart local v0 # "ret":I */
} // :cond_8
/* move v2, v0 */
/* .line 125 */
} // .end local v0 # "ret":I
/* .restart local v2 # "ret":I */
} // :goto_1
/* .line 127 */
} // .end local v2 # "ret":I
/* .restart local v0 # "ret":I */
} // :cond_9
try { // :try_start_1
final String v2 = "com.miui.performance.IMiuiBoosterManager"; // const-string v2, "com.miui.performance.IMiuiBoosterManager"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 128 */
(( android.os.Parcel ) v1 ).writeInt ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 129 */
(( android.os.Parcel ) v1 ).writeIntArray ( p2 ); // invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeIntArray([I)V
/* .line 130 */
(( android.os.Parcel ) v1 ).writeInt ( p3 ); // invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 131 */
(( android.os.Parcel ) v1 ).writeInt ( p4 ); // invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 132 */
v2 = com.android.server.am.MiuiBoosterUtilsStubImpl.sService;
/* const/16 v5, 0x1b */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 136 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 137 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 139 */
} // :cond_a
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 140 */
/* .line 136 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 133 */
/* :catch_0 */
/* move-exception v2 */
/* .line 134 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 136 */
} // .end local v2 # "e":Ljava/lang/Exception;
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 137 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 139 */
} // :cond_b
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 140 */
} // :goto_2
(( android.os.Parcel ) v4 ).readException ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
/* .line 141 */
v0 = (( android.os.Parcel ) v4 ).readInt ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I
/* .line 142 */
(( android.os.Parcel ) v4 ).recycle ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V
/* .line 145 */
} // :cond_c
/* .line 136 */
} // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_d
/* .line 137 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 139 */
} // :cond_d
if ( v4 != null) { // if-eqz v4, :cond_e
/* .line 140 */
(( android.os.Parcel ) v4 ).readException ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
/* .line 141 */
v0 = (( android.os.Parcel ) v4 ).readInt ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I
/* .line 142 */
(( android.os.Parcel ) v4 ).recycle ( ); // invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V
/* .line 144 */
} // :cond_e
/* throw v2 */
/* .line 108 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // .end local v4 # "reply":Landroid/os/Parcel;
} // :cond_f
} // :goto_4
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "enable_miuibooster : "; // const-string v3, "enable_miuibooster : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v3 = " ,isInit: "; // const-string v3, " ,isInit: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v1 );
/* .line 109 */
} // .end method
public void updateCloudControlParas ( ) {
/* .locals 3 */
/* .line 370 */
v0 = this.mContext;
/* .line 371 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 370 */
final String v1 = "cloud_schedboost_enable"; // const-string v1, "cloud_schedboost_enable"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 372 */
/* .local v0, "cloudMiuiboosterPara":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 373 */
v1 = java.lang.Boolean .parseBoolean ( v0 );
com.android.server.am.MiuiBoosterUtilsStubImpl.enable_miuibooster = (v1!= 0);
/* .line 375 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "updateCloudControlParas cloudMiuiboosterPara: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " ,enable_miuibooster: "; // const-string v2, " ,enable_miuibooster: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiBoosterUtils"; // const-string v2, "MiuiBoosterUtils"
android.util.Slog .w ( v2,v1 );
/* .line 377 */
return;
} // .end method
