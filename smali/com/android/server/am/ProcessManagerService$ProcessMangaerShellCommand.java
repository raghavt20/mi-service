class com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand extends android.os.ShellCommand {
	 /* .source "ProcessManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ProcessMangaerShellCommand" */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessManagerService this$0; //synthetic
/* # direct methods */
 com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessManagerService; */
/* .line 1296 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 1300 */
/* if-nez p1, :cond_0 */
/* .line 1301 */
v0 = (( com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 1303 */
} // :cond_0
(( com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1305 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
int v1 = -1; // const/4 v1, -0x1
try { // :try_start_0
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* packed-switch v2, :pswitch_data_0 */
} // :cond_1
/* :pswitch_0 */
final String v2 = "pl"; // const-string v2, "pl"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* move v2, v1 */
} // :goto_1
/* packed-switch v2, :pswitch_data_1 */
/* .line 1312 */
v1 = (( com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 1307 */
/* :pswitch_1 */
v2 = this.this$0;
(( com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
/* .line 1308 */
(( com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand ) p0 ).getNextArg ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getNextArg()Ljava/lang/String;
v4 = java.lang.Boolean .parseBoolean ( v4 );
/* .line 1309 */
(( com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand ) p0 ).getNextArg ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getNextArg()Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
miui.process.LifecycleConfig .create ( v5 );
/* .line 1307 */
int v6 = 1; // const/4 v6, 0x1
(( com.android.server.am.ProcessManagerService ) v2 ).startPreloadApp ( v3, v4, v6, v5 ); // invoke-virtual {v2, v3, v4, v6, v5}, Lcom/android/server/am/ProcessManagerService;->startPreloadApp(Ljava/lang/String;ZZLmiui/process/LifecycleConfig;)I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1310 */
/* nop */
/* .line 1316 */
/* .line 1312 */
} // :goto_2
/* .line 1314 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1315 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 1317 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_3
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0xdfc */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void onHelp ( ) {
/* .locals 2 */
/* .line 1322 */
(( com.android.server.am.ProcessManagerService$ProcessMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessManagerService$ProcessMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1323 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "Process manager commands:"; // const-string v1, "Process manager commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1324 */
final String v1 = " pl PACKAGENAME"; // const-string v1, " pl PACKAGENAME"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1325 */
return;
} // .end method
