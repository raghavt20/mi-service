public class com.android.server.am.ProcessUtils {
	 /* .source "ProcessUtils.java" */
	 /* # static fields */
	 public static final Integer FREEFORM_WORKSPACE_STACK_ID;
	 public static final Integer FULLSCREEN_WORKSPACE_STACK_ID;
	 private static final Integer LOW_MEMORY_RATE;
	 public static final android.util.Pair PRIORITY_HEAVY;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/Pair<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public static final android.util.Pair PRIORITY_PERCEPTIBLE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final android.util.Pair PRIORITY_UNKNOW;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final android.util.Pair PRIORITY_VISIBLE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/Pair<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String TAG;
private static com.android.server.am.ActivityManagerService sAmInstance;
private static android.speech.tts.TtsEngines sTtsEngines;
/* # direct methods */
static com.android.server.am.ProcessUtils ( ) {
/* .locals 3 */
/* .line 67 */
/* new-instance v0, Landroid/util/Pair; */
/* .line 68 */
/* const/16 v1, 0x64 */
java.lang.Integer .valueOf ( v1 );
int v2 = 2; // const/4 v2, 0x2
java.lang.Integer .valueOf ( v2 );
/* invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 69 */
/* new-instance v0, Landroid/util/Pair; */
/* .line 70 */
/* const/16 v1, 0xc8 */
java.lang.Integer .valueOf ( v1 );
int v2 = 4; // const/4 v2, 0x4
java.lang.Integer .valueOf ( v2 );
/* invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 71 */
/* new-instance v0, Landroid/util/Pair; */
/* .line 72 */
/* const/16 v1, 0x190 */
java.lang.Integer .valueOf ( v1 );
/* const/16 v2, 0xd */
java.lang.Integer .valueOf ( v2 );
/* invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 73 */
/* new-instance v0, Landroid/util/Pair; */
/* .line 74 */
/* const/16 v1, 0x3e9 */
java.lang.Integer .valueOf ( v1 );
/* const/16 v2, 0x14 */
java.lang.Integer .valueOf ( v2 );
/* invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V */
/* .line 243 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
public com.android.server.am.ProcessUtils ( ) {
/* .locals 0 */
/* .line 52 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static java.lang.String getActiveTtsEngine ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 144 */
int v0 = 0; // const/4 v0, 0x0
/* .line 145 */
/* .local v0, "ttsEngine":Ljava/lang/String; */
/* nop */
/* .line 146 */
final String v1 = "accessibility"; // const-string v1, "accessibility"
(( android.content.Context ) p0 ).getSystemService ( v1 ); // invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/view/accessibility/AccessibilityManager; */
/* .line 147 */
/* .local v1, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 148 */
v2 = (( android.view.accessibility.AccessibilityManager ) v1 ).isEnabled ( ); // invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 149 */
v2 = (( android.view.accessibility.AccessibilityManager ) v1 ).isTouchExplorationEnabled ( ); // invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 150 */
v2 = com.android.server.am.ProcessUtils.sTtsEngines;
/* if-nez v2, :cond_0 */
/* .line 151 */
/* new-instance v2, Landroid/speech/tts/TtsEngines; */
/* invoke-direct {v2, p0}, Landroid/speech/tts/TtsEngines;-><init>(Landroid/content/Context;)V */
/* .line 153 */
} // :cond_0
v2 = com.android.server.am.ProcessUtils.sTtsEngines;
(( android.speech.tts.TtsEngines ) v2 ).getDefaultEngine ( ); // invoke-virtual {v2}, Landroid/speech/tts/TtsEngines;->getDefaultEngine()Ljava/lang/String;
/* .line 155 */
} // :cond_1
} // .end method
public static java.lang.String getActiveWallpaperPackage ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 122 */
int v0 = 0; // const/4 v0, 0x0
/* .line 123 */
/* .local v0, "wallpaperPkg":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 124 */
/* .local v1, "wInfo":Landroid/app/WallpaperInfo; */
/* nop */
/* .line 125 */
/* const-string/jumbo v2, "wallpaper" */
android.os.ServiceManager .getService ( v2 );
/* .line 124 */
android.app.IWallpaperManager$Stub .asInterface ( v2 );
/* .line 127 */
/* .local v2, "wpm":Landroid/app/IWallpaperManager; */
int v3 = -2; // const/4 v3, -0x2
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v1, v3 */
/* .line 130 */
/* .line 128 */
/* :catch_0 */
/* move-exception v3 */
/* .line 129 */
/* .local v3, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 132 */
} // .end local v3 # "e":Landroid/os/RemoteException;
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 133 */
(( android.app.WallpaperInfo ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;
/* .line 135 */
} // :cond_0
} // .end method
static synchronized com.android.server.am.ActivityManagerService getActivityManagerService ( ) {
/* .locals 2 */
/* const-class v0, Lcom/android/server/am/ProcessUtils; */
/* monitor-enter v0 */
/* .line 246 */
try { // :try_start_0
v1 = com.android.server.am.ProcessUtils.sAmInstance;
/* if-nez v1, :cond_0 */
/* .line 247 */
final String v1 = "activity"; // const-string v1, "activity"
android.os.ServiceManager .getService ( v1 );
/* check-cast v1, Lcom/android/server/am/ActivityManagerService; */
/* .line 249 */
} // :cond_0
v1 = com.android.server.am.ProcessUtils.sAmInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 245 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
public static java.lang.String getApplicationLabel ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 443 */
int v0 = 0; // const/4 v0, 0x0
/* .line 445 */
/* .local v0, "info":Landroid/content/pm/ApplicationInfo; */
try { // :try_start_0
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
int v2 = 0; // const/4 v2, 0x0
(( android.content.pm.PackageManager ) v1 ).getApplicationInfo ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v1 */
/* .line 448 */
/* .line 446 */
/* :catch_0 */
/* move-exception v1 */
/* .line 447 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 450 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 451 */
/* .line 454 */
} // :cond_0
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v2 ).getApplicationLabel ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
/* .line 456 */
/* .local v2, "label":Ljava/lang/CharSequence; */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 457 */
/* .line 460 */
} // :cond_1
} // .end method
public static Integer getCurAdjByPid ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "pid" # I */
/* .line 280 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 281 */
/* .local v0, "amService":Lcom/android/server/am/ActivityManagerService; */
/* monitor-enter v0 */
/* .line 282 */
try { // :try_start_0
v1 = this.mPidsSelfLocked;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 283 */
try { // :try_start_1
v2 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 284 */
/* .local v2, "processRecord":Lcom/android/server/am/ProcessRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 285 */
v3 = this.mState;
v3 = (( com.android.server.am.ProcessStateRecord ) v3 ).getCurAdj ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 287 */
} // .end local v2 # "processRecord":Lcom/android/server/am/ProcessRecord;
} // :cond_0
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 288 */
try { // :try_start_4
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 289 */
/* const v1, 0x7fffffff */
/* .line 287 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_5
/* monitor-exit v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
} // .end local v0 # "amService":Lcom/android/server/am/ActivityManagerService;
} // .end local p0 # "pid":I
try { // :try_start_6
/* throw v2 */
/* .line 288 */
/* .restart local v0 # "amService":Lcom/android/server/am/ActivityManagerService; */
/* .restart local p0 # "pid":I */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* throw v1 */
} // .end method
public static Integer getCurSchedGroupByPid ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "pid" # I */
/* .line 319 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 320 */
/* .local v0, "amService":Lcom/android/server/am/ActivityManagerService; */
/* monitor-enter v0 */
/* .line 321 */
try { // :try_start_0
v1 = this.mPidsSelfLocked;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 322 */
try { // :try_start_1
v2 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 323 */
/* .local v2, "proc":Lcom/android/server/am/ProcessRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 324 */
v3 = this.mState;
v3 = (( com.android.server.am.ProcessStateRecord ) v3 ).getCurrentSchedulingGroup ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getCurrentSchedulingGroup()I
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 326 */
} // .end local v2 # "proc":Lcom/android/server/am/ProcessRecord;
} // :cond_0
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 327 */
try { // :try_start_4
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 328 */
int v1 = -1; // const/4 v1, -0x1
/* .line 326 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_5
	 /* monitor-exit v1 */
	 /* :try_end_5 */
	 /* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
} // .end local v0 # "amService":Lcom/android/server/am/ActivityManagerService;
} // .end local p0 # "pid":I
try { // :try_start_6
/* throw v2 */
/* .line 327 */
/* .restart local v0 # "amService":Lcom/android/server/am/ActivityManagerService; */
/* .restart local p0 # "pid":I */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* throw v1 */
} // .end method
public static Integer getCurrentUserId ( ) {
/* .locals 2 */
/* .line 339 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 340 */
/* .local v0, "amService":Lcom/android/server/am/ActivityManagerService; */
v1 = (( com.android.server.am.ActivityManagerService ) v0 ).getCurrentUserId ( ); // invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerService;->getCurrentUserId()I
} // .end method
public static java.lang.String getDefaultInputMethod ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 104 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "default_input_method"; // const-string v1, "default_input_method"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 106 */
/* .local v0, "inputMethodId":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 107 */
/* .local v1, "inputMethodPkg":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_0 */
/* .line 108 */
/* const/16 v2, 0x2f */
v2 = (( java.lang.String ) v0 ).indexOf ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I
/* .line 109 */
/* .local v2, "endIndex":I */
/* if-lez v2, :cond_0 */
/* .line 110 */
int v3 = 0; // const/4 v3, 0x0
(( java.lang.String ) v0 ).substring ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 113 */
} // .end local v2 # "endIndex":I
} // :cond_0
} // .end method
public static Integer getMemoryTrimLevel ( ) {
/* .locals 3 */
/* .line 344 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 346 */
/* .local v0, "ams":Lcom/android/server/am/ActivityManagerService; */
v1 = android.os.Binder .getCallingUid ( );
v1 = android.os.UserHandle .isIsolated ( v1 );
/* if-nez v1, :cond_0 */
/* .line 350 */
/* monitor-enter v0 */
/* .line 351 */
try { // :try_start_0
v1 = (( com.android.server.am.ActivityManagerService ) v0 ).getMemoryTrimLevel ( ); // invoke-virtual {v0}, Lcom/android/server/am/ActivityManagerService;->getMemoryTrimLevel()I
/* monitor-exit v0 */
/* .line 352 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 347 */
} // :cond_0
/* new-instance v1, Ljava/lang/SecurityException; */
final String v2 = "Isolated process not allowed to call getMemoryTrimLevel"; // const-string v2, "Isolated process not allowed to call getMemoryTrimLevel"
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
public static Long getPackageLastPss ( java.lang.String p0, Integer p1 ) {
/* .locals 9 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "userId" # I */
/* .line 223 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 224 */
/* .local v0, "ams":Lcom/android/server/am/ActivityManagerService; */
/* const-wide/16 v1, 0x0 */
/* .line 225 */
/* .local v1, "totalPss":J */
/* monitor-enter v0 */
/* .line 226 */
try { // :try_start_0
v3 = this.mProcessList;
(( com.android.server.am.ProcessList ) v3 ).getLruProcessesLOSP ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
/* .line 227 */
v4 = /* .local v3, "lruProcesses":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* add-int/lit8 v4, v4, -0x1 */
/* .local v4, "i":I */
} // :goto_0
/* if-ltz v4, :cond_1 */
/* .line 228 */
/* check-cast v5, Lcom/android/server/am/ProcessRecord; */
/* .line 229 */
/* .local v5, "proc":Lcom/android/server/am/ProcessRecord; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* iget v6, v5, Lcom/android/server/am/ProcessRecord;->userId:I */
/* if-ne v6, p1, :cond_0 */
/* .line 231 */
(( com.android.server.am.ProcessRecord ) v5 ).getThread ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 232 */
v6 = (( com.android.server.am.ProcessRecord ) v5 ).isKilledByAm ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v6, :cond_0 */
/* .line 233 */
(( com.android.server.am.ProcessRecord ) v5 ).getPkgList ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;
v6 = (( com.android.server.am.PackageList ) v6 ).containsKey ( p0 ); // invoke-virtual {v6, p0}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
	 /* .line 234 */
	 v6 = this.mAppProfiler;
	 v6 = this.mProfilerLock;
	 /* monitor-enter v6 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
	 /* .line 235 */
	 try { // :try_start_1
		 v7 = this.mProfile;
		 (( com.android.server.am.ProcessProfileRecord ) v7 ).getLastPss ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
		 /* move-result-wide v7 */
		 /* add-long/2addr v1, v7 */
		 /* .line 236 */
		 /* monitor-exit v6 */
		 /* :catchall_0 */
		 /* move-exception v7 */
		 /* monitor-exit v6 */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 } // .end local v0 # "ams":Lcom/android/server/am/ActivityManagerService;
} // .end local v1 # "totalPss":J
} // .end local p0 # "packageName":Ljava/lang/String;
} // .end local p1 # "userId":I
try { // :try_start_2
/* throw v7 */
/* .line 227 */
} // .end local v5 # "proc":Lcom/android/server/am/ProcessRecord;
/* .restart local v0 # "ams":Lcom/android/server/am/ActivityManagerService; */
/* .restart local v1 # "totalPss":J */
/* .restart local p0 # "packageName":Ljava/lang/String; */
/* .restart local p1 # "userId":I */
} // :cond_0
} // :goto_1
/* add-int/lit8 v4, v4, -0x1 */
/* .line 239 */
} // .end local v3 # "lruProcesses":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v4 # "i":I
} // :cond_1
/* monitor-exit v0 */
/* .line 240 */
/* return-wide v1 */
/* .line 239 */
/* :catchall_1 */
/* move-exception v3 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v3 */
} // .end method
public static java.lang.String getPackageNameByApp ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p0, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 436 */
if ( p0 != null) { // if-eqz p0, :cond_1
v0 = this.info;
/* if-nez v0, :cond_0 */
/* .line 439 */
} // :cond_0
v0 = this.info;
v0 = this.packageName;
/* .line 437 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static java.lang.String getPackageNameByPid ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "pid" # I */
/* .line 261 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 262 */
/* .local v0, "amService":Lcom/android/server/am/ActivityManagerService; */
v1 = this.mPidsSelfLocked;
/* monitor-enter v1 */
/* .line 263 */
try { // :try_start_0
v2 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 264 */
/* .local v2, "processRecord":Lcom/android/server/am/ProcessRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 265 */
v3 = this.info;
v3 = this.packageName;
/* monitor-exit v1 */
/* .line 267 */
} // .end local v2 # "processRecord":Lcom/android/server/am/ProcessRecord;
} // :cond_0
/* monitor-exit v1 */
/* .line 268 */
int v1 = 0; // const/4 v1, 0x0
/* .line 267 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public static Integer getProcStateByPid ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "pid" # I */
/* .line 293 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 294 */
/* .local v0, "amService":Lcom/android/server/am/ActivityManagerService; */
/* monitor-enter v0 */
/* .line 295 */
try { // :try_start_0
v1 = this.mPidsSelfLocked;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 296 */
try { // :try_start_1
v2 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 297 */
/* .local v2, "processRecord":Lcom/android/server/am/ProcessRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 298 */
v3 = this.mState;
v3 = (( com.android.server.am.ProcessStateRecord ) v3 ).getCurProcState ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 300 */
} // .end local v2 # "processRecord":Lcom/android/server/am/ProcessRecord;
} // :cond_0
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 301 */
try { // :try_start_4
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 302 */
int v1 = -1; // const/4 v1, -0x1
/* .line 300 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_5
/* monitor-exit v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
} // .end local v0 # "amService":Lcom/android/server/am/ActivityManagerService;
} // .end local p0 # "pid":I
try { // :try_start_6
/* throw v2 */
/* .line 301 */
/* .restart local v0 # "amService":Lcom/android/server/am/ActivityManagerService; */
/* .restart local p0 # "pid":I */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* throw v1 */
} // .end method
public static Integer getProcTotalPss ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "pid" # I */
/* .line 206 */
/* new-instance v0, Landroid/os/Debug$MemoryInfo; */
/* invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V */
/* .line 207 */
/* .local v0, "info":Landroid/os/Debug$MemoryInfo; */
android.os.Debug .getMemoryInfo ( p0,v0 );
/* .line 208 */
v1 = (( android.os.Debug$MemoryInfo ) v0 ).getTotalPss ( ); // invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I
} // .end method
public static java.util.List getProcessListByAdj ( com.android.server.am.ActivityManagerService p0, Integer p1, java.util.List p2 ) {
/* .locals 8 */
/* .param p0, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p1, "minOomAdj" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/ActivityManagerService;", */
/* "I", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 177 */
/* .local p2, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 178 */
/* .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* monitor-enter p0 */
/* .line 179 */
try { // :try_start_0
(( com.android.server.am.ActivityManagerService ) p0 ).getProcessNamesLOSP ( ); // invoke-virtual {p0}, Lcom/android/server/am/ActivityManagerService;->getProcessNamesLOSP()Lcom/android/internal/app/ProcessMap;
v1 = (( com.android.internal.app.ProcessMap ) v1 ).size ( ); // invoke-virtual {v1}, Lcom/android/internal/app/ProcessMap;->size()I
/* .line 180 */
/* .local v1, "NP":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "ip":I */
} // :goto_0
/* if-ge v2, v1, :cond_4 */
/* .line 181 */
/* nop */
/* .line 182 */
(( com.android.server.am.ActivityManagerService ) p0 ).getProcessNamesLOSP ( ); // invoke-virtual {p0}, Lcom/android/server/am/ActivityManagerService;->getProcessNamesLOSP()Lcom/android/internal/app/ProcessMap;
(( com.android.internal.app.ProcessMap ) v3 ).getMap ( ); // invoke-virtual {v3}, Lcom/android/internal/app/ProcessMap;->getMap()Landroid/util/ArrayMap;
(( android.util.ArrayMap ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Landroid/util/SparseArray; */
/* .line 183 */
/* .local v3, "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;" */
v4 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* .line 184 */
/* .local v4, "NA":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "ia":I */
} // :goto_1
/* if-ge v5, v4, :cond_3 */
/* .line 185 */
(( android.util.SparseArray ) v3 ).valueAt ( v5 ); // invoke-virtual {v3, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/am/ProcessRecord; */
/* .line 186 */
/* .local v6, "app":Lcom/android/server/am/ProcessRecord; */
v7 = (( com.android.server.am.ProcessRecord ) v6 ).isPersistent ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z
/* if-nez v7, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_0
v7 = this.processName;
v7 = /* .line 187 */
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 189 */
/* .line 191 */
} // :cond_0
v7 = (( com.android.server.am.ProcessRecord ) v6 ).isRemoved ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessRecord;->isRemoved()Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 192 */
/* .line 193 */
} // :cond_1
v7 = this.mState;
v7 = (( com.android.server.am.ProcessStateRecord ) v7 ).getSetAdj ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
/* if-lt v7, p1, :cond_2 */
/* .line 194 */
/* .line 184 */
} // .end local v6 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_2
} // :goto_2
/* add-int/lit8 v5, v5, 0x1 */
/* .line 180 */
} // .end local v3 # "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v4 # "NA":I
} // .end local v5 # "ia":I
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 198 */
} // .end local v1 # "NP":I
} // .end local v2 # "ip":I
} // :cond_4
/* monitor-exit p0 */
/* .line 199 */
/* .line 198 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit p0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static java.lang.String getProcessNameByPid ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "pid" # I */
/* .line 272 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 273 */
/* .local v0, "amService":Lcom/android/server/am/ActivityManagerService; */
v1 = this.mPidsSelfLocked;
/* monitor-enter v1 */
/* .line 274 */
try { // :try_start_0
v2 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 275 */
/* .local v2, "app":Lcom/android/server/am/ProcessRecord; */
/* if-nez v2, :cond_0 */
java.lang.String .valueOf ( p0 );
} // :cond_0
v3 = this.processName;
} // :goto_0
/* monitor-exit v1 */
/* .line 276 */
} // .end local v2 # "app":Lcom/android/server/am/ProcessRecord;
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public static com.android.server.am.ProcessRecord getProcessRecordByPid ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "pid" # I */
/* .line 254 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 255 */
/* .local v0, "amService":Lcom/android/server/am/ActivityManagerService; */
v1 = this.mPidsSelfLocked;
/* monitor-enter v1 */
/* .line 256 */
try { // :try_start_0
v2 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* monitor-exit v1 */
/* .line 257 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public static java.util.List getRunningProcessInfos ( ) {
/* .locals 14 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/os/Bundle;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 356 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 357 */
/* .local v0, "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 358 */
/* .local v1, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 359 */
/* .local v2, "service":Lcom/android/server/am/ActivityManagerService; */
int v3 = 0; // const/4 v3, 0x0
/* .line 360 */
/* .local v3, "bundle":Landroid/os/Bundle; */
/* monitor-enter v2 */
/* .line 361 */
try { // :try_start_0
v4 = this.mProcessList;
v4 = (( com.android.server.am.ProcessList ) v4 ).getLruSizeLOSP ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I
/* add-int/lit8 v4, v4, -0x1 */
/* .local v4, "i":I */
} // :goto_0
/* if-ltz v4, :cond_1 */
/* .line 362 */
v5 = this.mProcessList;
(( com.android.server.am.ProcessList ) v5 ).getLruProcessesLOSP ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
(( java.util.ArrayList ) v5 ).get ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/am/ProcessRecord; */
/* .line 363 */
/* .local v5, "proc":Lcom/android/server/am/ProcessRecord; */
v6 = this.mState;
v6 = (( com.android.server.am.ProcessStateRecord ) v6 ).getSetAdjWithServices ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessStateRecord;->getSetAdjWithServices()I
/* .line 364 */
/* .local v6, "curAdj":I */
(( com.android.server.am.ProcessRecord ) v5 ).getThread ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v7 != null) { // if-eqz v7, :cond_0
/* const/16 v7, 0x1f4 */
/* if-ge v6, v7, :cond_0 */
/* .line 365 */
/* new-instance v7, Landroid/os/Bundle; */
/* invoke-direct {v7}, Landroid/os/Bundle;-><init>()V */
/* move-object v3, v7 */
/* .line 366 */
final String v7 = "pid"; // const-string v7, "pid"
v8 = (( com.android.server.am.ProcessRecord ) v5 ).getPid ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( android.os.Bundle ) v3 ).putInt ( v7, v8 ); // invoke-virtual {v3, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 367 */
final String v7 = "adj"; // const-string v7, "adj"
(( android.os.Bundle ) v3 ).putInt ( v7, v6 ); // invoke-virtual {v3, v7, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 368 */
final String v7 = "lastPss"; // const-string v7, "lastPss"
v8 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v8 ).getLastPss ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v8 */
(( android.os.Bundle ) v3 ).putLong ( v7, v8, v9 ); // invoke-virtual {v3, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 369 */
final String v7 = "lastPssTime"; // const-string v7, "lastPssTime"
v8 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v8 ).getLastPssTime ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessProfileRecord;->getLastPssTime()J
/* move-result-wide v8 */
(( android.os.Bundle ) v3 ).putLong ( v7, v8, v9 ); // invoke-virtual {v3, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 370 */
final String v7 = "processName"; // const-string v7, "processName"
v8 = this.processName;
(( android.os.Bundle ) v3 ).putString ( v7, v8 ); // invoke-virtual {v3, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 371 */
final String v7 = "packageUid"; // const-string v7, "packageUid"
v8 = this.info;
/* iget v8, v8, Landroid/content/pm/ApplicationInfo;->uid:I */
(( android.os.Bundle ) v3 ).putInt ( v7, v8 ); // invoke-virtual {v3, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 372 */
final String v7 = "packageName"; // const-string v7, "packageName"
v8 = this.info;
v8 = this.packageName;
(( android.os.Bundle ) v3 ).putString ( v7, v8 ); // invoke-virtual {v3, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 373 */
/* .line 375 */
} // :cond_0
v7 = (( com.android.server.am.ProcessRecord ) v5 ).getPid ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getPid()I
java.lang.Integer .valueOf ( v7 );
/* .line 361 */
/* nop */
} // .end local v5 # "proc":Lcom/android/server/am/ProcessRecord;
} // .end local v6 # "curAdj":I
/* add-int/lit8 v4, v4, -0x1 */
/* .line 377 */
} // .end local v4 # "i":I
} // :cond_1
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 378 */
(( com.android.server.am.ActivityManagerService ) v2 ).getProcessCpuTrackerLocked ( ); // invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->getProcessCpuTrackerLocked()Lcom/android/internal/os/ProcessCpuTracker;
/* monitor-enter v4 */
/* .line 379 */
try { // :try_start_1
(( com.android.server.am.ActivityManagerService ) v2 ).getProcessCpuTrackerLocked ( ); // invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->getProcessCpuTrackerLocked()Lcom/android/internal/os/ProcessCpuTracker;
v5 = (( com.android.internal.os.ProcessCpuTracker ) v5 ).countStats ( ); // invoke-virtual {v5}, Lcom/android/internal/os/ProcessCpuTracker;->countStats()I
/* .line 380 */
/* .local v5, "N":I */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_1
/* if-ge v6, v5, :cond_3 */
/* .line 381 */
(( com.android.server.am.ActivityManagerService ) v2 ).getProcessCpuTrackerLocked ( ); // invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService;->getProcessCpuTrackerLocked()Lcom/android/internal/os/ProcessCpuTracker;
(( com.android.internal.os.ProcessCpuTracker ) v7 ).getStats ( v6 ); // invoke-virtual {v7, v6}, Lcom/android/internal/os/ProcessCpuTracker;->getStats(I)Lcom/android/internal/os/ProcessCpuTracker$Stats;
/* .line 382 */
/* .local v7, "st":Lcom/android/internal/os/ProcessCpuTracker$Stats; */
/* iget-wide v8, v7, Lcom/android/internal/os/ProcessCpuTracker$Stats;->vsize:J */
/* const-wide/16 v10, 0x0 */
/* cmp-long v8, v8, v10 */
/* if-lez v8, :cond_2 */
/* iget v8, v7, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
v8 = java.lang.Integer .valueOf ( v8 );
/* if-nez v8, :cond_2 */
/* .line 383 */
/* new-instance v8, Landroid/os/Bundle; */
/* invoke-direct {v8}, Landroid/os/Bundle;-><init>()V */
/* move-object v3, v8 */
/* .line 384 */
final String v8 = "pid"; // const-string v8, "pid"
/* iget v9, v7, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
(( android.os.Bundle ) v3 ).putInt ( v8, v9 ); // invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 385 */
final String v8 = "adj"; // const-string v8, "adj"
/* const/16 v9, -0x3e8 */
(( android.os.Bundle ) v3 ).putInt ( v8, v9 ); // invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 386 */
final String v8 = "lastPss"; // const-string v8, "lastPss"
/* const-wide/16 v12, -0x1 */
(( android.os.Bundle ) v3 ).putLong ( v8, v12, v13 ); // invoke-virtual {v3, v8, v12, v13}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 387 */
final String v8 = "lastPssTime"; // const-string v8, "lastPssTime"
(( android.os.Bundle ) v3 ).putLong ( v8, v10, v11 ); // invoke-virtual {v3, v8, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
/* .line 388 */
final String v8 = "processName"; // const-string v8, "processName"
v9 = this.name;
(( android.os.Bundle ) v3 ).putString ( v8, v9 ); // invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 389 */
/* .line 380 */
} // .end local v7 # "st":Lcom/android/internal/os/ProcessCpuTracker$Stats;
} // :cond_2
/* add-int/lit8 v6, v6, 0x1 */
/* .line 392 */
} // .end local v5 # "N":I
} // .end local v6 # "i":I
} // :cond_3
/* monitor-exit v4 */
/* .line 393 */
/* .line 392 */
/* :catchall_0 */
/* move-exception v5 */
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v5 */
/* .line 377 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v4 */
} // .end method
public static java.lang.String getTopAppPackageName ( ) {
/* .locals 3 */
/* .line 332 */
/* const-class v0, Lcom/android/server/wm/ActivityTaskManagerInternal; */
/* .line 333 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/ActivityTaskManagerInternal; */
/* .line 334 */
/* .local v0, "aTm":Lcom/android/server/wm/ActivityTaskManagerInternal; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
(( com.android.server.wm.ActivityTaskManagerInternal ) v0 ).getTopApp ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getTopApp()Lcom/android/server/wm/WindowProcessController;
} // :cond_0
/* move-object v2, v1 */
/* .line 335 */
/* .local v2, "wpc":Lcom/android/server/wm/WindowProcessController; */
} // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
v1 = (( com.android.server.wm.WindowProcessController ) v2 ).getPid ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowProcessController;->getPid()I
com.android.server.am.ProcessUtils .getPackageNameByPid ( v1 );
} // :cond_1
} // .end method
public static Integer getTotalPss ( Integer[] p0 ) {
/* .locals 3 */
/* .param p0, "pids" # [I */
/* .line 212 */
if ( p0 != null) { // if-eqz p0, :cond_1
/* array-length v0, p0 */
/* if-lez v0, :cond_1 */
/* .line 213 */
int v0 = 0; // const/4 v0, 0x0
/* .line 214 */
/* .local v0, "totalPss":I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p0 */
/* if-ge v1, v2, :cond_0 */
/* aget v2, p0, v1 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 215 */
/* aget v2, p0, v1 */
v2 = com.android.server.am.ProcessUtils .getProcTotalPss ( v2 );
/* add-int/2addr v0, v2 */
/* .line 214 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 217 */
} // .end local v1 # "i":I
} // :cond_0
/* .line 219 */
} // .end local v0 # "totalPss":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean hasForegroundActivities ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "pid" # I */
/* .line 306 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 307 */
/* .local v0, "amService":Lcom/android/server/am/ActivityManagerService; */
/* monitor-enter v0 */
/* .line 308 */
try { // :try_start_0
v1 = this.mPidsSelfLocked;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 309 */
try { // :try_start_1
v2 = this.mPidsSelfLocked;
(( com.android.server.am.ActivityManagerService$PidMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Lcom/android/server/am/ActivityManagerService$PidMap;->get(I)Lcom/android/server/am/ProcessRecord;
/* .line 310 */
/* .local v2, "processRecord":Lcom/android/server/am/ProcessRecord; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 311 */
v3 = this.mState;
v3 = (( com.android.server.am.ProcessStateRecord ) v3 ).hasForegroundActivities ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->hasForegroundActivities()Z
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 313 */
} // .end local v2 # "processRecord":Lcom/android/server/am/ProcessRecord;
} // :cond_0
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 314 */
try { // :try_start_4
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 315 */
int v1 = 0; // const/4 v1, 0x0
/* .line 313 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_5
/* monitor-exit v1 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
} // .end local v0 # "amService":Lcom/android/server/am/ActivityManagerService;
} // .end local p0 # "pid":I
try { // :try_start_6
/* throw v2 */
/* .line 314 */
/* .restart local v0 # "amService":Lcom/android/server/am/ActivityManagerService; */
/* .restart local p0 # "pid":I */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* throw v1 */
} // .end method
public static final Boolean isDiedProcess ( Long p0, Long p1 ) {
/* .locals 6 */
/* .param p0, "uid" # J */
/* .param p2, "pid" # J */
/* .line 424 */
final String v0 = "Uid:"; // const-string v0, "Uid:"
final String v1 = "Tgid:"; // const-string v1, "Tgid:"
/* filled-new-array {v0, v1}, [Ljava/lang/String; */
/* .line 425 */
/* .local v0, "procStatusLabels":[Ljava/lang/String; */
int v1 = 2; // const/4 v1, 0x2
/* new-array v1, v1, [J */
/* .line 426 */
/* .local v1, "procStatusValues":[J */
int v2 = 0; // const/4 v2, 0x0
/* const-wide/16 v3, -0x1 */
/* aput-wide v3, v1, v2 */
/* .line 427 */
int v5 = 1; // const/4 v5, 0x1
/* aput-wide v3, v1, v5 */
/* .line 428 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "/proc/"; // const-string v4, "/proc/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2, p3 ); // invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = "/status"; // const-string v4, "/status"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.os.Process .readProcLines ( v3,v0,v1 );
/* .line 429 */
/* aget-wide v3, v1, v2 */
/* cmp-long v3, v3, p0 */
/* if-nez v3, :cond_0 */
/* aget-wide v3, v1, v2 */
/* cmp-long v3, v3, p2 */
/* if-nez v3, :cond_0 */
/* .line 430 */
/* .line 432 */
} // :cond_0
} // .end method
protected static Boolean isHomeProcess ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p0, "pr" # Lcom/android/server/am/ProcessRecord; */
/* .line 168 */
if ( p0 != null) { // if-eqz p0, :cond_0
(( com.android.server.am.ProcessRecord ) p0 ).getWindowProcessController ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
v0 = (( com.android.server.wm.WindowProcessController ) v0 ).isHomeProcess ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isHomeProcess()Z
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean isLowMemory ( ) {
/* .locals 6 */
/* .line 162 */
android.os.Process .getFreeMemory ( );
/* move-result-wide v0 */
/* .line 163 */
/* .local v0, "freeMemory":J */
android.os.Process .getTotalMemory ( );
/* move-result-wide v2 */
/* .line 164 */
/* .local v2, "totalMemory":J */
/* const-wide/16 v4, 0x14 */
/* mul-long/2addr v4, v0 */
/* cmp-long v4, v4, v2 */
/* if-gez v4, :cond_0 */
int v4 = 1; // const/4 v4, 0x1
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
} // .end method
public static Boolean isPersistent ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 464 */
int v0 = 0; // const/4 v0, 0x0
/* .line 466 */
/* .local v0, "info":Landroid/content/pm/ApplicationInfo; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v2 ).getApplicationInfo ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-object v0, v2 */
/* .line 469 */
/* .line 467 */
/* :catch_0 */
/* move-exception v2 */
/* .line 468 */
/* .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 471 */
} // .end local v2 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 472 */
/* iget v2, v0, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/lit8 v2, v2, 0x8 */
if ( v2 != null) { // if-eqz v2, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 474 */
} // :cond_1
} // .end method
public static Boolean isPhoneWorking ( ) {
/* .locals 6 */
/* .line 77 */
int v0 = 1; // const/4 v0, 0x1
/* .line 78 */
/* .local v0, "isWorking":Z */
final String v1 = "ro.radio.noril"; // const-string v1, "ro.radio.noril"
int v2 = 0; // const/4 v2, 0x0
v1 = android.os.SystemProperties .getBoolean ( v1,v2 );
/* .line 79 */
/* .local v1, "mWifiOnly":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 80 */
/* .line 83 */
} // :cond_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v2 */
/* .line 86 */
/* .local v2, "token":J */
try { // :try_start_0
android.app.ActivityThread .currentApplication ( );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 87 */
android.app.ActivityThread .currentApplication ( );
/* const-string/jumbo v5, "telecom" */
(( android.app.Application ) v4 ).getSystemService ( v5 ); // invoke-virtual {v4, v5}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v4, Landroid/telecom/TelecomManager; */
/* move-object v5, v4 */
/* .local v5, "tm":Landroid/telecom/TelecomManager; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 89 */
v4 = (( android.telecom.TelecomManager ) v5 ).isInCall ( ); // invoke-virtual {v5}, Landroid/telecom/TelecomManager;->isInCall()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v0, v4 */
/* .line 92 */
} // .end local v5 # "tm":Landroid/telecom/TelecomManager;
} // :cond_1
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 93 */
/* nop */
/* .line 95 */
/* .line 92 */
/* :catchall_0 */
/* move-exception v4 */
android.os.Binder .restoreCallingIdentity ( v2,v3 );
/* .line 93 */
/* throw v4 */
} // .end method
public static Boolean isSystem ( android.content.pm.ApplicationInfo p0 ) {
/* .locals 2 */
/* .param p0, "applicationInfo" # Landroid/content/pm/ApplicationInfo; */
/* .line 478 */
if ( p0 != null) { // if-eqz p0, :cond_1
v0 = (( android.content.pm.ApplicationInfo ) p0 ).isSystemApp ( ); // invoke-virtual {p0}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* if-nez v0, :cond_0 */
/* iget v0, p0, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 479 */
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x2710 */
/* if-ge v0, v1, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 478 */
} // :goto_0
} // .end method
public static Boolean isSystemApp ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 3 */
/* .param p0, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 483 */
int v0 = 0; // const/4 v0, 0x0
if ( p0 != null) { // if-eqz p0, :cond_2
v1 = this.info;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 484 */
v1 = this.info;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/lit16 v1, v1, 0x81 */
/* if-nez v1, :cond_0 */
/* const/16 v1, 0x3e8 */
/* iget v2, p0, Lcom/android/server/am/ProcessRecord;->uid:I */
/* if-ne v1, v2, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* .line 488 */
} // :cond_2
} // .end method
public static void killUnusedApp ( Integer p0, Integer p1 ) {
/* .locals 8 */
/* .param p0, "uid" # I */
/* .param p1, "pid" # I */
/* .line 397 */
com.android.server.am.ProcessUtils .getActivityManagerService ( );
/* .line 398 */
/* .local v0, "service":Lcom/android/server/am/ActivityManagerService; */
/* monitor-enter v0 */
/* .line 399 */
try { // :try_start_0
v1 = this.mProcessList;
v1 = (( com.android.server.am.ProcessList ) v1 ).getLruSizeLOSP ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I
int v2 = 1; // const/4 v2, 0x1
/* sub-int/2addr v1, v2 */
/* .local v1, "i":I */
} // :goto_0
/* if-ltz v1, :cond_2 */
/* .line 400 */
v3 = this.mProcessList;
(( com.android.server.am.ProcessList ) v3 ).getLruProcessesLOSP ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
(( java.util.ArrayList ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/ProcessRecord; */
/* .line 401 */
/* .local v3, "app":Lcom/android/server/am/ProcessRecord; */
/* if-nez v3, :cond_0 */
/* goto/16 :goto_1 */
/* .line 402 */
} // :cond_0
/* iget v4, v3, Lcom/android/server/am/ProcessRecord;->uid:I */
/* if-ne v4, p0, :cond_1 */
v4 = (( com.android.server.am.ProcessRecord ) v3 ).getPid ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getPid()I
/* if-ne v4, p1, :cond_1 */
/* .line 403 */
(( com.android.server.am.ProcessRecord ) v3 ).getThread ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v4 != null) { // if-eqz v4, :cond_1
v4 = this.mErrorState;
/* .line 404 */
v4 = (( com.android.server.am.ProcessErrorStateRecord ) v4 ).isCrashing ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessErrorStateRecord;->isCrashing()Z
/* if-nez v4, :cond_1 */
v4 = this.mErrorState;
/* .line 405 */
v4 = (( com.android.server.am.ProcessErrorStateRecord ) v4 ).isNotResponding ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessErrorStateRecord;->isNotResponding()Z
/* if-nez v4, :cond_1 */
/* .line 406 */
v4 = this.mState;
v4 = (( com.android.server.am.ProcessStateRecord ) v4 ).getSetAdj ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
/* .line 407 */
/* .local v4, "tempAdj":I */
final String v5 = "ProcessUtils"; // const-string v5, "ProcessUtils"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "check package : "; // const-string v7, "check package : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.info;
v7 = this.packageName;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " uid : "; // const-string v7, " uid : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p0 ); // invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " pid : "; // const-string v7, " pid : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " tempAdj : "; // const-string v7, " tempAdj : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v6 );
/* .line 411 */
/* const/16 v5, 0xc8 */
/* if-le v4, v5, :cond_2 */
v5 = (( com.android.server.am.ProcessRecord ) v3 ).isKilledByAm ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v5, :cond_2 */
/* .line 412 */
final String v5 = "ProcessUtils"; // const-string v5, "ProcessUtils"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "kill app : "; // const-string v7, "kill app : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.info;
v7 = this.packageName;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " uid : "; // const-string v7, " uid : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p0 ); // invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " pid : "; // const-string v7, " pid : "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v5,v6 );
/* .line 414 */
final String v5 = "User unused app kill it !!"; // const-string v5, "User unused app kill it !!"
/* const/16 v6, 0xd */
(( com.android.server.am.ProcessRecord ) v3 ).killLocked ( v5, v6, v2 ); // invoke-virtual {v3, v5, v6, v2}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V
/* .line 399 */
} // .end local v3 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v4 # "tempAdj":I
} // :cond_1
} // :goto_1
/* add-int/lit8 v1, v1, -0x1 */
/* goto/16 :goto_0 */
/* .line 420 */
} // .end local v1 # "i":I
} // :cond_2
} // :goto_2
/* monitor-exit v0 */
/* .line 421 */
return;
/* .line 420 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
