class com.android.server.am.MiuiMemServiceHelper$WorkHandler extends android.os.Handler {
	 /* .source "MiuiMemServiceHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MiuiMemServiceHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "WorkHandler" */
} // .end annotation
/* # static fields */
public static final Integer CHARGING_RECLAIM;
public static final Integer SCREENOFF_RECLAIM;
/* # instance fields */
final com.android.server.am.MiuiMemServiceHelper this$0; //synthetic
/* # direct methods */
public com.android.server.am.MiuiMemServiceHelper$WorkHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 36 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 40 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 41 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 44 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "start screen off reclaim..." */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMemoryService"; // const-string v1, "MiuiMemoryService"
android.util.Slog .v ( v1,v0 );
/* .line 45 */
v0 = this.this$0;
com.android.server.am.MiuiMemServiceHelper .-$$Nest$fgetmMemService ( v0 );
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.am.MiuiMemoryService ) v0 ).runProcsCompaction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MiuiMemoryService;->runProcsCompaction(I)V
/* .line 49 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
