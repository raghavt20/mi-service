public abstract class com.android.server.am.AppStateManager$IProcessStateCallback {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "IProcessStateCallback" */
} // .end annotation
/* # virtual methods */
public abstract void onAppStateChanged ( com.android.server.am.AppStateManager$AppState p0, Integer p1, Integer p2, Boolean p3, Boolean p4, java.lang.String p5 ) {
} // .end method
public abstract void onProcessStateChanged ( com.android.server.am.AppStateManager$AppState$RunningProcess p0, Integer p1, Integer p2, java.lang.String p3 ) {
} // .end method
