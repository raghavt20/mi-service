public class com.android.server.am.ProcessMemoryCleaner extends com.android.server.am.ProcessCleanerBase {
	 /* .source "ProcessMemoryCleaner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessMemoryCleaner$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer AUDIO_PROC_PAUSE_PROTECT_TIME;
private static final java.lang.String CAMERA_PACKAGE_NAME;
private static final Double COMM_USED_PSS_LIMIT_HIGH_FACTOR;
private static final Double COMM_USED_PSS_LIMIT_LITE_FACTOR;
private static final Double COMM_USED_PSS_LIMIT_USUAL_FACTOR;
public static final Boolean DEBUG;
private static final Integer DEF_MIN_KILL_PROC_ADJ;
private static final java.lang.String HOME_PACKAGE_NAME;
private static final java.lang.String KILLING_PROC_REASON;
private static final Integer KILL_PROC_COUNT_LIMIT;
private static final Double MEM_EXCEPTION_TH_HIGHH_FACTOR;
private static final Double MEM_EXCEPTION_TH_HIGHL_FACTOR;
private static final Double MEM_EXCEPTION_TH_HIGHM_FACTOR;
private static final Double MEM_EXCEPTION_TH_LITE_FACTOR;
private static final Double MEM_EXCEPTION_TH_MID_FACTOR;
private static final Double MEM_EXCEPTION_TH_USUAL_FACTOR;
private static final Integer MSG_APP_SWITCH_BG_EXCEPTION;
private static final Integer MSG_PAD_SMALL_WINDOW_CLEAN;
private static final Integer MSG_PROCESS_BG_COMPACT;
private static final Integer MSG_REGISTER_CLOUD_OBSERVER;
private static final Long PAD_SMALL_WINDOW_CLEAN_TIME;
private static final java.lang.String PERF_PROC_THRESHOLD_CLOUD_KEY;
private static final java.lang.String PERF_PROC_THRESHOLD_CLOUD_MOUDLE;
private static final Integer PROCESS_PRIORITY_FACTOR;
private static final Integer PROC_BG_COMPACT_DELAY_TIME;
private static final java.lang.String PROC_COMM_PSS_LIMIT_CLOUD;
private static final java.lang.String PROC_MEM_EXCEPTION_THRESHOLD_CLOUD;
private static final java.lang.String PROC_PROTECT_LIST_CLOUD;
private static final java.lang.String PROC_SWITCH_BG_DELAY_TIME_CLOUD;
private static final Long RAM_SIZE_1GB;
private static final java.lang.String REASON_PAD_SMALL_WINDOW_CLEAN;
private static final java.lang.String SUB_PROCESS_ADJ_BIND_LIST_CLOUD;
public static final java.lang.String TAG;
private static final Long TOTAL_MEMEORY_GB;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private Integer mAppSwitchBgExceptDelayTime;
private Long mCommonUsedPssLimitKB;
private android.content.Context mContext;
private java.lang.String mForegroundPkg;
private Integer mForegroundUid;
private com.android.server.am.ProcessMemoryCleaner$H mHandler;
private android.os.HandlerThread mHandlerTh;
private Long mLastPadSmallWindowUpdateTime;
private Long mMemExceptionThresholdKB;
private com.android.server.am.MiuiMemoryServiceInternal mMiuiMemoryService;
private com.android.server.am.ProcessManagerService mPMS;
private com.android.server.am.ProcessPolicy mProcessPolicy;
private com.android.server.am.ProcMemCleanerStatistics mStatistics;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.am.ProcessMemoryCleaner p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Integer -$$Nest$mcheckBackgroundAppException ( com.android.server.am.ProcessMemoryCleaner p0, java.lang.String p1, Integer p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->checkBackgroundAppException(Ljava/lang/String;I)I */
} // .end method
static void -$$Nest$mcheckBackgroundProcCompact ( com.android.server.am.ProcessMemoryCleaner p0, com.miui.server.smartpower.IAppState$IRunningProcess p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->checkBackgroundProcCompact(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)V */
	 return;
} // .end method
static void -$$Nest$mkillProcessByMinAdj ( com.android.server.am.ProcessMemoryCleaner p0, Integer p1, java.lang.String p2, java.util.List p3 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcessByMinAdj(ILjava/lang/String;Ljava/util/List;)V */
	 return;
} // .end method
static void -$$Nest$mregisterCloudObserver ( com.android.server.am.ProcessMemoryCleaner p0, android.content.Context p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->registerCloudObserver(Landroid/content/Context;)V */
	 return;
} // .end method
static void -$$Nest$mupdateCloudControlData ( com.android.server.am.ProcessMemoryCleaner p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/am/ProcessMemoryCleaner;->updateCloudControlData()V */
	 return;
} // .end method
static com.android.server.am.ProcessMemoryCleaner ( ) {
	 /* .locals 4 */
	 /* .line 38 */
	 /* sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z */
	 com.android.server.am.ProcessMemoryCleaner.DEBUG = (v0!= 0);
	 /* .line 51 */
	 android.os.Process .getTotalMemory ( );
	 /* move-result-wide v0 */
	 /* const-wide/32 v2, 0x40000000 */
	 /* div-long/2addr v0, v2 */
	 /* sput-wide v0, Lcom/android/server/am/ProcessMemoryCleaner;->TOTAL_MEMEORY_GB:J */
	 return;
} // .end method
public com.android.server.am.ProcessMemoryCleaner ( ) {
	 /* .locals 3 */
	 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
	 /* .line 115 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;-><init>(Lcom/android/server/am/ActivityManagerService;)V */
	 /* .line 88 */
	 /* const/16 v0, 0x7530 */
	 /* iput v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAppSwitchBgExceptDelayTime:I */
	 /* .line 90 */
	 /* sget-wide v0, Landroid/os/spc/PressureStateSettings;->PROC_COMMON_USED_PSS_LIMIT_KB:J */
	 /* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J */
	 /* .line 101 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "ProcessMemoryCleanerTh"; // const-string v1, "ProcessMemoryCleanerTh"
	 int v2 = -2; // const/4 v2, -0x2
	 /* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
	 this.mHandlerTh = v0;
	 /* .line 108 */
	 /* sget-wide v0, Landroid/os/spc/PressureStateSettings;->PROC_MEM_EXCEPTION_PSS_LIMIT_KB:J */
	 /* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J */
	 /* .line 110 */
	 final String v0 = ""; // const-string v0, ""
	 this.mForegroundPkg = v0;
	 /* .line 111 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* iput v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundUid:I */
	 /* .line 112 */
	 /* const-wide/16 v0, 0x0 */
	 /* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mLastPadSmallWindowUpdateTime:J */
	 /* .line 116 */
	 this.mAMS = p1;
	 /* .line 117 */
	 return;
} // .end method
private Integer checkBackgroundAppException ( java.lang.String p0, Integer p1 ) {
	 /* .locals 18 */
	 /* .param p1, "packageName" # Ljava/lang/String; */
	 /* .param p2, "uid" # I */
	 /* .line 673 */
	 /* move-object/from16 v0, p0 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 675 */
	 /* .local v1, "killType":I */
	 com.android.server.am.SystemPressureController .getInstance ( );
	 /* move-object/from16 v3, p1 */
	 v2 = 	 (( com.android.server.am.SystemPressureController ) v2 ).isGameApp ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 676 */
		 /* .line 678 */
	 } // :cond_0
	 /* const-wide/16 v4, 0x0 */
	 /* .line 679 */
	 /* .local v4, "mainPss":J */
	 /* const-wide/16 v6, 0x0 */
	 /* .line 680 */
	 /* .local v6, "subPss":J */
	 /* const-wide/16 v8, 0x0 */
	 /* .line 681 */
	 /* .local v8, "subPssWithActivity":J */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 682 */
	 /* .local v2, "mainProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
	 /* invoke-direct/range {p0 ..p2}, Lcom/android/server/am/ProcessMemoryCleaner;->getProcessGroup(Ljava/lang/String;I)Ljava/util/List; */
	 /* .line 683 */
	 /* .local v10, "procGroup":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v12 = } // :goto_0
if ( v12 != null) { // if-eqz v12, :cond_3
	 /* check-cast v12, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
	 /* .line 684 */
	 /* .local v12, "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
	 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v12 ).updatePss ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->updatePss()V
	 /* .line 685 */
	 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v12 ).getPackageName ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
	 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v12 ).getProcessName ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
	 v13 = 	 (( java.lang.String ) v13 ).equals ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v13 != null) { // if-eqz v13, :cond_1
		 /* .line 686 */
		 /* move-object v2, v12 */
		 /* .line 687 */
		 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v12 ).getPss ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
		 /* move-result-wide v4 */
		 /* .line 688 */
	 } // :cond_1
	 v13 = 	 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v12 ).hasActivity ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z
	 if ( v13 != null) { // if-eqz v13, :cond_2
		 /* .line 689 */
		 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v12 ).getPss ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
		 /* move-result-wide v13 */
		 /* add-long/2addr v8, v13 */
		 /* .line 691 */
	 } // :cond_2
	 (( com.miui.server.smartpower.IAppState$IRunningProcess ) v12 ).getPss ( ); // invoke-virtual {v12}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
	 /* move-result-wide v13 */
	 /* add-long/2addr v6, v13 */
	 /* .line 693 */
} // .end local v12 # "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :goto_1
/* .line 694 */
} // :cond_3
/* if-nez v2, :cond_4 */
/* .line 696 */
} // :cond_4
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
v11 = com.android.server.am.ProcMemCleanerStatistics .isCommonUsedApp ( v11 );
final String v12 = "ProcessMemoryCleaner"; // const-string v12, "ProcessMemoryCleaner"
final String v13 = "kill_bg_proc"; // const-string v13, "kill_bg_proc"
if ( v11 != null) { // if-eqz v11, :cond_6
/* .line 697 */
/* iget-wide v14, v0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J */
/* cmp-long v11, v4, v14 */
/* if-ltz v11, :cond_5 */
/* .line 698 */
(( com.android.server.am.ProcessMemoryCleaner ) v0 ).killPackage ( v2, v13 ); // invoke-virtual {v0, v2, v13}, Lcom/android/server/am/ProcessMemoryCleaner;->killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J
/* .line 699 */
int v1 = 2; // const/4 v1, 0x2
/* .line 700 */
/* sget-boolean v11, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v11 != null) { // if-eqz v11, :cond_5
/* .line 701 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "common used app takes up too much memory: "; // const-string v13, "common used app takes up too much memory: "
(( java.lang.StringBuilder ) v11 ).append ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 702 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 701 */
android.util.Slog .d ( v12,v11 );
/* .line 704 */
} // :cond_5
/* .line 707 */
} // :cond_6
/* add-long v14, v4, v6 */
/* add-long/2addr v14, v8 */
/* move-wide/from16 v16, v8 */
} // .end local v8 # "subPssWithActivity":J
/* .local v16, "subPssWithActivity":J */
/* iget-wide v8, v0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J */
/* cmp-long v8, v14, v8 */
/* if-lez v8, :cond_7 */
/* .line 708 */
v8 = this.mSmartPowerService;
/* .line 709 */
v9 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getUid ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getProcessName ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
/* .line 708 */
(( com.android.server.am.ProcessMemoryCleaner ) v0 ).killPackage ( v8, v13 ); // invoke-virtual {v0, v8, v13}, Lcom/android/server/am/ProcessMemoryCleaner;->killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J
/* .line 711 */
int v1 = 2; // const/4 v1, 0x2
/* .line 712 */
/* sget-boolean v8, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_a
/* .line 713 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "app takes up too much memory: "; // const-string v9, "app takes up too much memory: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ".pss:"; // const-string v9, ".pss:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4, v5 ); // invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = " sub:"; // const-string v9, " sub:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6, v7 ); // invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v12,v8 );
/* .line 716 */
} // :cond_7
/* sget-wide v8, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL1_PSS_LIMIT_KB:J */
/* cmp-long v8, v6, v8 */
/* if-ltz v8, :cond_a */
/* .line 718 */
v9 = } // :goto_2
if ( v9 != null) { // if-eqz v9, :cond_9
/* check-cast v9, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 719 */
/* .local v9, "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).getPackageName ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).getProcessName ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
v11 = (( java.lang.String ) v11 ).equals ( v14 ); // invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v11, :cond_8 */
v11 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).hasActivity ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z
/* if-nez v11, :cond_8 */
/* .line 720 */
v11 = this.mSmartPowerService;
/* .line 721 */
v14 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).getUid ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).getProcessName ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
/* .line 720 */
(( com.android.server.am.ProcessMemoryCleaner ) v0 ).killProcess ( v11, v13 ); // invoke-virtual {v0, v11, v13}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Ljava/lang/String;)J
/* .line 724 */
} // .end local v9 # "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_8
/* .line 725 */
} // :cond_9
int v1 = 1; // const/4 v1, 0x1
/* .line 726 */
/* sget-boolean v8, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_a
/* .line 727 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "subprocess takes up too much memory: " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 728 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = ".sub:"; // const-string v9, ".sub:"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6, v7 ); // invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 727 */
android.util.Slog .d ( v12,v8 );
/* .line 730 */
} // :cond_a
} // :goto_3
} // .end method
private void checkBackgroundProcCompact ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 3 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 599 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getCurrentState ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getCurrentState()I
int v1 = 4; // const/4 v1, 0x4
/* if-lt v0, v1, :cond_1 */
/* .line 600 */
/* const/16 v0, 0x64 */
v0 = /* invoke-direct {p0, p1, v0}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 601 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isNeedCompact(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mMiuiMemoryService;
v0 = /* .line 602 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 604 */
/* sget-boolean v0, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 605 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Compact memory: "; // const-string v1, "Compact memory: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = " pss:"; // const-string v1, " pss:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 606 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPss ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " swap:"; // const-string v1, " swap:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getSwapPss ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getSwapPss()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 605 */
final String v1 = "ProcessMemoryCleaner"; // const-string v1, "ProcessMemoryCleaner"
android.util.Slog .d ( v1,v0 );
/* .line 607 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->compactProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)V */
/* .line 609 */
} // :cond_1
return;
} // .end method
private Boolean checkRunningProcess ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "minAdj" # I */
/* .line 310 */
if ( p1 != null) { // if-eqz p1, :cond_0
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessRecord ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
v0 = (( com.android.server.am.ProcessMemoryCleaner ) p0 ).checkProcessDied ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessMemoryCleaner;->checkProcessDied(Lcom/android/server/am/ProcessRecord;)Z
/* if-nez v0, :cond_0 */
/* .line 311 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getAdj ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* if-le v0, p2, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 310 */
} // :goto_0
} // .end method
private Boolean cleanUpMemory ( java.util.List p0, Long p1 ) {
/* .locals 12 */
/* .param p2, "targetReleaseMem" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;J)Z" */
/* } */
} // .end annotation
/* .line 220 */
/* .local p1, "runningProcList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
/* new-instance v0, Lcom/android/server/am/ProcessMemoryCleaner$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/ProcessMemoryCleaner$1;-><init>(Lcom/android/server/am/ProcessMemoryCleaner;)V */
java.util.Collections .sort ( p1,v0 );
/* .line 234 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 235 */
/* .local v0, "nowTime":J */
v2 = this.mStatistics;
(( com.android.server.am.ProcMemCleanerStatistics ) v2 ).checkLastKillTime ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics;->checkLastKillTime(J)V
/* .line 236 */
/* sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->debugAppGroupToString(Ljava/util/List;)V */
/* .line 238 */
} // :cond_0
/* const-wide/16 v2, 0x0 */
/* .line 239 */
/* .local v2, "releasePssByProcClean":J */
int v4 = 0; // const/4 v4, 0x0
/* .line 240 */
/* .local v4, "killProcCount":I */
v6 = } // :goto_0
int v7 = 0; // const/4 v7, 0x0
if ( v6 != null) { // if-eqz v6, :cond_9
/* check-cast v6, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 241 */
/* .local v6, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v8 = this.mStatistics;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v6 ).getProcessName ( ); // invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
/* .line 242 */
v10 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v6 ).getUid ( ); // invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
/* .line 241 */
v8 = (( com.android.server.am.ProcMemCleanerStatistics ) v8 ).isLastKillProcess ( v9, v10, v0, v1 ); // invoke-virtual {v8, v9, v10, v0, v1}, Lcom/android/server/am/ProcMemCleanerStatistics;->isLastKillProcess(Ljava/lang/String;IJ)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 243 */
/* .line 246 */
} // :cond_1
v8 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v6 ).getAdj ( ); // invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* const/16 v9, 0x384 */
/* if-lt v8, v9, :cond_2 */
/* .line 247 */
v8 = /* invoke-direct {p0, v6}, Lcom/android/server/am/ProcessMemoryCleaner;->isIsolatedProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
/* if-nez v8, :cond_2 */
/* .line 248 */
/* .line 250 */
} // :cond_2
v8 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v6 ).hasActivity ( ); // invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z
int v9 = 1; // const/4 v9, 0x1
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 251 */
/* .line 253 */
} // :cond_3
final String v8 = "clean_up_mem"; // const-string v8, "clean_up_mem"
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).killProcess ( v6, v7, v8 ); // invoke-virtual {p0, v6, v7, v8}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J
/* move-result-wide v7 */
/* .line 255 */
/* .local v7, "pss":J */
/* const-wide/16 v10, 0x0 */
/* cmp-long v10, p2, v10 */
/* if-gtz v10, :cond_4 */
/* .line 256 */
/* .line 258 */
} // :cond_4
/* sget-boolean v10, Landroid/os/spc/PressureStateSettings;->ONLY_KILL_ONE_PKG:Z */
if ( v10 != null) { // if-eqz v10, :cond_6
v10 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v6 ).hasActivity ( ); // invoke-virtual {v6}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z
if ( v10 != null) { // if-eqz v10, :cond_6
/* .line 259 */
/* sget-boolean v5, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_5
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "----skip kill: "; // const-string v10, "----skip kill: "
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {p0, v6}, Lcom/android/server/am/ProcessMemoryCleaner;->processToString(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v10 ); // invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v10 = "ProcessMemoryCleaner"; // const-string v10, "ProcessMemoryCleaner"
android.util.Slog .d ( v10,v5 );
/* .line 260 */
} // :cond_5
/* .line 262 */
} // :cond_6
/* add-long/2addr v2, v7 */
/* .line 263 */
/* cmp-long v10, v2, p2 */
/* if-gez v10, :cond_8 */
/* add-int/lit8 v4, v4, 0x1 */
/* const/16 v10, 0xa */
/* if-lt v4, v10, :cond_7 */
/* .line 267 */
} // .end local v6 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v7 # "pss":J
} // :cond_7
/* .line 265 */
/* .restart local v6 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .restart local v7 # "pss":J */
} // :cond_8
} // :goto_1
/* .line 268 */
} // .end local v6 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v7 # "pss":J
} // :cond_9
} // .end method
private void compactProcess ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 2 */
/* .param p1, "processInfo" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 272 */
v0 = this.mMiuiMemoryService;
int v1 = 4; // const/4 v1, 0x4
/* .line 274 */
return;
} // .end method
private void computeCommonUsedAppPssThreshold ( Long p0 ) {
/* .locals 4 */
/* .param p1, "threshold" # J */
/* .line 660 */
/* sget-wide v0, Lcom/android/server/am/ProcessMemoryCleaner;->TOTAL_MEMEORY_GB:J */
/* const-wide/16 v2, 0x6 */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_0 */
/* .line 661 */
/* long-to-double v0, p1 */
/* const-wide v2, 0x3fe570a3d70a3d71L # 0.67 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J */
/* .line 663 */
} // :cond_0
/* const-wide/16 v2, 0x8 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_1 */
/* .line 664 */
/* long-to-double v0, p1 */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J */
/* .line 667 */
} // :cond_1
/* long-to-double v0, p1 */
/* const-wide v2, 0x3ff547ae147ae148L # 1.33 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J */
/* .line 670 */
} // :goto_0
return;
} // .end method
private void computeMemExceptionThreshold ( Long p0 ) {
/* .locals 4 */
/* .param p1, "threshold" # J */
/* .line 644 */
/* sget-wide v0, Lcom/android/server/am/ProcessMemoryCleaner;->TOTAL_MEMEORY_GB:J */
/* const-wide/16 v2, 0x5 */
/* cmp-long v2, v0, v2 */
/* if-gez v2, :cond_0 */
/* .line 645 */
/* long-to-double v0, p1 */
/* const-wide/high16 v2, 0x3fe4000000000000L # 0.625 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J */
/* .line 646 */
} // :cond_0
/* const-wide/16 v2, 0x6 */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_1 */
/* .line 647 */
/* long-to-double v0, p1 */
/* const-wide/high16 v2, 0x3fe8000000000000L # 0.75 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J */
/* .line 648 */
} // :cond_1
/* const-wide/16 v2, 0x8 */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_2 */
/* .line 649 */
/* long-to-double v0, p1 */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J */
/* .line 650 */
} // :cond_2
/* const-wide/16 v2, 0xc */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_3 */
/* .line 651 */
/* long-to-double v0, p1 */
/* const-wide/high16 v2, 0x3ff4000000000000L # 1.25 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J */
/* .line 652 */
} // :cond_3
/* const-wide/16 v2, 0x10 */
/* cmp-long v0, v0, v2 */
/* if-gtz v0, :cond_4 */
/* .line 653 */
/* long-to-double v0, p1 */
/* const-wide/high16 v2, 0x3ff8000000000000L # 1.5 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J */
/* .line 655 */
} // :cond_4
/* long-to-double v0, p1 */
/* const-wide/high16 v2, 0x4000000000000000L # 2.0 */
/* mul-double/2addr v0, v2 */
/* double-to-long v0, v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mMemExceptionThresholdKB:J */
/* .line 657 */
} // :goto_0
return;
} // .end method
private Boolean containInWhiteList ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 4 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 277 */
v0 = this.mPMS;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessRecord ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
/* const/16 v2, 0x15 */
int v3 = 0; // const/4 v3, 0x0
v0 = (( com.android.server.am.ProcessManagerService ) v0 ).isInWhiteList ( v1, v3, v2 ); // invoke-virtual {v0, v1, v3, v2}, Lcom/android/server/am/ProcessManagerService;->isInWhiteList(Lcom/android/server/am/ProcessRecord;II)Z
/* if-nez v0, :cond_0 */
/* .line 279 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
v0 = com.miui.server.process.ProcessManagerInternal .checkCtsProcess ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v3 = 1; // const/4 v3, 0x1
/* .line 277 */
} // :cond_1
} // .end method
private void debugAppGroupToString ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 531 */
/* .local p1, "runningProcList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 532 */
/* .local v1, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* invoke-direct {p0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->processToString(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String; */
/* .line 533 */
/* .local v2, "deg":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "ProcessInfo: "; // const-string v4, "ProcessInfo: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "ProcessMemoryCleaner"; // const-string v4, "ProcessMemoryCleaner"
android.util.Slog .d ( v4,v3 );
/* .line 534 */
} // .end local v1 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // .end local v2 # "deg":Ljava/lang/String;
/* .line 535 */
} // :cond_0
return;
} // .end method
private java.lang.String getKillReason ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 2 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 448 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "MiuiMemoryService("; // const-string v1, "MiuiMemoryService("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getAdjType ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdjType()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static Integer getProcPriority ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 2 */
/* .param p0, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 141 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p0 ).getAdj ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* mul-int/lit16 v0, v0, 0x3e8 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p0 ).getPriorityScore ( ); // invoke-virtual {p0}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPriorityScore()I
/* add-int/2addr v0, v1 */
} // .end method
private java.util.List getProcessGroup ( java.lang.String p0, Integer p1 ) {
/* .locals 13 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "I)", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 284 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 285 */
/* .local v0, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v1 = this.mAMS;
/* monitor-enter v1 */
/* .line 286 */
try { // :try_start_0
v2 = this.mAMS;
v2 = this.mProcessList;
/* .line 287 */
/* .local v2, "procList":Lcom/android/server/am/ProcessList; */
(( com.android.server.am.ProcessList ) v2 ).getProcessNamesLOSP ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
(( com.android.server.am.ProcessList$MyProcessMap ) v3 ).getMap ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;
v3 = (( android.util.ArrayMap ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I
/* .line 288 */
/* .local v3, "NP":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "ip":I */
} // :goto_0
/* if-ge v4, v3, :cond_4 */
/* .line 289 */
/* nop */
/* .line 290 */
(( com.android.server.am.ProcessList ) v2 ).getProcessNamesLOSP ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
(( com.android.server.am.ProcessList$MyProcessMap ) v5 ).getMap ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;
(( android.util.ArrayMap ) v5 ).valueAt ( v4 ); // invoke-virtual {v5, v4}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Landroid/util/SparseArray; */
/* .line 291 */
/* .local v5, "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;" */
v6 = (( android.util.SparseArray ) v5 ).size ( ); // invoke-virtual {v5}, Landroid/util/SparseArray;->size()I
/* .line 292 */
/* .local v6, "NA":I */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "ia":I */
} // :goto_1
/* if-ge v7, v6, :cond_3 */
/* .line 293 */
(( android.util.SparseArray ) v5 ).valueAt ( v7 ); // invoke-virtual {v5, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v8, Lcom/android/server/am/ProcessRecord; */
/* .line 294 */
/* .local v8, "app":Lcom/android/server/am/ProcessRecord; */
(( com.android.server.am.ProcessRecord ) v8 ).getPkgDeps ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getPkgDeps()Landroid/util/ArraySet;
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 295 */
(( com.android.server.am.ProcessRecord ) v8 ).getPkgDeps ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getPkgDeps()Landroid/util/ArraySet;
v9 = (( android.util.ArraySet ) v9 ).contains ( p1 ); // invoke-virtual {v9, p1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
if ( v9 != null) { // if-eqz v9, :cond_0
int v9 = 1; // const/4 v9, 0x1
} // :cond_0
int v9 = 0; // const/4 v9, 0x0
/* .line 296 */
/* .local v9, "isDep":Z */
} // :goto_2
/* iget v10, v8, Lcom/android/server/am/ProcessRecord;->uid:I */
v10 = android.os.UserHandle .getAppId ( v10 );
v11 = android.os.UserHandle .getAppId ( p2 );
/* if-eq v10, v11, :cond_1 */
/* .line 297 */
/* .line 299 */
} // :cond_1
(( com.android.server.am.ProcessRecord ) v8 ).getPkgList ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;
v10 = (( com.android.server.am.PackageList ) v10 ).containsKey ( p1 ); // invoke-virtual {v10, p1}, Lcom/android/server/am/PackageList;->containsKey(Ljava/lang/Object;)Z
/* if-nez v10, :cond_2 */
/* if-nez v9, :cond_2 */
/* .line 300 */
/* .line 302 */
} // :cond_2
v10 = this.mSmartPowerService;
/* iget v11, v8, Lcom/android/server/am/ProcessRecord;->uid:I */
v12 = this.processName;
(( java.util.ArrayList ) v0 ).add ( v10 ); // invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 292 */
} // .end local v8 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v9 # "isDep":Z
} // :goto_3
/* add-int/lit8 v7, v7, 0x1 */
/* .line 288 */
} // .end local v5 # "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v6 # "NA":I
} // .end local v7 # "ia":I
} // :cond_3
/* add-int/lit8 v4, v4, 0x1 */
/* .line 305 */
} // .end local v2 # "procList":Lcom/android/server/am/ProcessList;
} // .end local v3 # "NP":I
} // .end local v4 # "ip":I
} // :cond_4
/* monitor-exit v1 */
/* .line 306 */
/* .line 305 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private Boolean isAutoStartProcess ( com.miui.server.smartpower.IAppState p0, com.miui.server.smartpower.IAppState$IRunningProcess p1 ) {
/* .locals 2 */
/* .param p1, "appState" # Lcom/miui/server/smartpower/IAppState; */
/* .param p2, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 180 */
v0 = (( com.miui.server.smartpower.IAppState ) p1 ).isAutoStartApp ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->isAutoStartApp()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p2 ).getAdj ( ); // invoke-virtual {p2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* const/16 v1, 0x320 */
/* if-gt v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isImportantSubProc ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "procName" # Ljava/lang/String; */
/* .line 200 */
com.android.server.am.OomAdjusterImpl .getInstance ( );
v0 = (( com.android.server.am.OomAdjusterImpl ) v0 ).isImportantSubProc ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/OomAdjusterImpl;->isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z
} // .end method
private Boolean isIsolatedByAdj ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 1 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 190 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isIsolatedProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
} // .end method
private Boolean isIsolatedProcess ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 3 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 194 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
v0 = android.os.Process .isIsolated ( v0 );
/* if-nez v0, :cond_1 */
/* .line 195 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 196 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ":sandboxed_"; // const-string v2, ":sandboxed_"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 195 */
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 194 */
} // :goto_1
} // .end method
private Boolean isLastMusicPlayProcess ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "pid" # I */
/* .line 212 */
v0 = this.mSmartPowerService;
/* move-result-wide v0 */
/* .line 213 */
/* .local v0, "lastMusicPlayTime":J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* sub-long/2addr v2, v0 */
/* const-wide/32 v4, 0x493e0 */
/* cmp-long v2, v2, v4 */
/* if-gtz v2, :cond_0 */
/* .line 214 */
int v2 = 1; // const/4 v2, 0x1
/* .line 216 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
private Boolean isLaunchCameraForThirdApp ( com.android.server.am.ControllerActivityInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lcom/android/server/am/ControllerActivityInfo; */
/* .line 553 */
v0 = this.fromPkg;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.launchPkg;
/* .line 554 */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p1, Lcom/android/server/am/ControllerActivityInfo;->formUid:I */
/* .line 555 */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/am/ProcessMemoryCleaner;->isSystemApp(I)Z */
/* if-nez v0, :cond_0 */
/* .line 556 */
int v0 = 1; // const/4 v0, 0x1
/* .line 558 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isNeedCompact ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 4 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 612 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I
/* const/16 v1, 0x3e8 */
/* if-le v0, v1, :cond_1 */
/* .line 613 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).hasActivity ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z
/* if-nez v0, :cond_0 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessRecord ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getUserId ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUserId()I
int v2 = 2; // const/4 v2, 0x2
v3 = this.mPMS;
v0 = (( com.android.server.am.ProcessMemoryCleaner ) p0 ).isInWhiteListLock ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->isInWhiteListLock(Lcom/android/server/am/ProcessRecord;IILcom/android/server/am/ProcessManagerService;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 615 */
} // :cond_0
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPss ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
/* move-result-wide v0 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getSwapPss ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getSwapPss()J
/* move-result-wide v2 */
/* sub-long/2addr v0, v2 */
/* sget-wide v2, Lcom/android/server/am/MiuiMemReclaimer;->ANON_RSS_LIMIT_KB:J */
/* cmp-long v0, v0, v2 */
/* if-ltz v0, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 612 */
} // :goto_0
} // .end method
private Boolean isRunningComponent ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 476 */
v0 = this.mServices;
v0 = (( com.android.server.am.ProcessServiceRecord ) v0 ).numberOfExecutingServices ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessServiceRecord;->numberOfExecutingServices()I
/* if-gtz v0, :cond_1 */
v0 = this.mReceivers;
/* .line 477 */
v0 = (( com.android.server.am.ProcessReceiverRecord ) v0 ).numberOfCurReceivers ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessReceiverRecord;->numberOfCurReceivers()I
/* if-lez v0, :cond_0 */
/* .line 480 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 478 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean isSystemApp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 562 */
v0 = this.mSmartPowerService;
/* .line 563 */
/* .local v0, "appState":Lcom/miui/server/smartpower/IAppState; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( com.miui.server.smartpower.IAppState ) v0 ).isSystemApp ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/IAppState;->isSystemApp()Z
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private Boolean isSystemHighPrioProc ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 2 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 204 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getAdj ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* const/16 v1, 0xc8 */
/* if-gt v0, v1, :cond_0 */
/* .line 205 */
v0 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).isSystemApp ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isSystemApp()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 206 */
int v0 = 1; // const/4 v0, 0x1
/* .line 208 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void killProcessByMinAdj ( Integer p0, java.lang.String p1, java.util.List p2 ) {
/* .locals 7 */
/* .param p1, "minAdj" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 496 */
/* .local p3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* const/16 v0, 0x12c */
/* if-gt p1, v0, :cond_0 */
/* .line 497 */
/* const/16 p1, 0x12c */
/* .line 499 */
} // :cond_0
v0 = this.mSmartPowerService;
/* .line 500 */
/* .local v0, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 501 */
/* .local v1, "canForcePkg":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :cond_1
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 502 */
/* .local v3, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v4 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
/* if-nez v4, :cond_1 */
/* .line 503 */
v4 = /* invoke-direct {p0, v3, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z */
/* if-nez v4, :cond_2 */
/* .line 504 */
/* .line 506 */
} // :cond_2
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getProcessName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 507 */
/* .line 509 */
} // :cond_3
v4 = this.mAMS;
/* monitor-enter v4 */
/* .line 510 */
try { // :try_start_0
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getProcessRecord ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).killApplicationLock ( v5, p2 ); // invoke-virtual {p0, v5, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 511 */
/* monitor-exit v4 */
/* .line 513 */
} // .end local v3 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :goto_1
/* .line 511 */
/* .restart local v3 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 514 */
} // .end local v3 # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_4
v3 = } // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 515 */
/* .local v3, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
com.android.server.am.SystemPressureController .getInstance ( );
/* .line 516 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getProcessRecord ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
int v6 = 0; // const/4 v6, 0x0
v4 = (( com.android.server.am.SystemPressureController ) v4 ).isForceStopEnable ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Lcom/android/server/am/SystemPressureController;->isForceStopEnable(Lcom/android/server/am/ProcessRecord;I)Z
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 517 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
v5 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getUserId ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUserId()I
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).forceStopPackage ( v4, v5, p2 ); // invoke-virtual {p0, v4, v5, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 519 */
} // :cond_5
v4 = this.mAMS;
/* monitor-enter v4 */
/* .line 520 */
try { // :try_start_1
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v3 ).getProcessRecord ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).killApplicationLock ( v5, p2 ); // invoke-virtual {p0, v5, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 521 */
/* monitor-exit v4 */
/* .line 523 */
} // .end local v3 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :goto_3
/* .line 521 */
/* .restart local v3 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* :catchall_1 */
/* move-exception v2 */
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v2 */
/* .line 524 */
} // .end local v3 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_6
return;
} // .end method
private void logCloudControlParas ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "data" # Ljava/lang/String; */
/* .line 408 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "sync cloud control " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessMemoryCleaner"; // const-string v1, "ProcessMemoryCleaner"
android.util.Slog .d ( v1,v0 );
/* .line 409 */
return;
} // .end method
private java.lang.String processToString ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 3 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 538 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v1, 0x80 */
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V */
/* .line 539 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
final String v1 = "pck="; // const-string v1, "pck="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 540 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 541 */
final String v1 = ", prcName="; // const-string v1, ", prcName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 542 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 543 */
final String v1 = ", priority="; // const-string v1, ", priority="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 544 */
v1 = com.android.server.am.ProcessMemoryCleaner .getProcPriority ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 545 */
final String v1 = ", hasAct="; // const-string v1, ", hasAct="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 546 */
v1 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).hasActivity ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 547 */
final String v1 = ", pss="; // const-string v1, ", pss="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 548 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPss ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 549 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 315 */
/* new-instance v0, Lcom/android/server/am/ProcessMemoryCleaner$2; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessMemoryCleaner$2;-><init>(Lcom/android/server/am/ProcessMemoryCleaner;Landroid/os/Handler;)V */
/* .line 325 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 326 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
/* .line 325 */
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 327 */
return;
} // .end method
private void sendAppSwitchBgExceptionMsg ( com.android.server.am.ControllerActivityInfo p0 ) {
/* .locals 5 */
/* .param p1, "info" # Lcom/android/server/am/ControllerActivityInfo; */
/* .line 619 */
v0 = this.mHandler;
v1 = this.launchPkg;
int v2 = 1; // const/4 v2, 0x1
v0 = (( com.android.server.am.ProcessMemoryCleaner$H ) v0 ).hasEqualMessages ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->hasEqualMessages(ILjava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 620 */
v0 = this.mHandler;
v1 = this.launchPkg;
(( com.android.server.am.ProcessMemoryCleaner$H ) v0 ).removeMessages ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->removeMessages(ILjava/lang/Object;)V
/* .line 622 */
} // :cond_0
v0 = this.mProcessPolicy;
/* .line 623 */
/* const/16 v1, 0x15 */
v1 = (( com.android.server.am.ProcessPolicy ) v0 ).getPolicyFlags ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->getPolicyFlags(I)I
(( com.android.server.am.ProcessPolicy ) v0 ).getWhiteList ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;
/* .line 624 */
/* .local v0, "packageWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = v1 = this.mForegroundPkg;
/* if-nez v1, :cond_1 */
v1 = this.mProcessPolicy;
v3 = this.mForegroundPkg;
/* .line 625 */
v1 = (( com.android.server.am.ProcessPolicy ) v1 ).isInExtraPackageList ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->isInExtraPackageList(Ljava/lang/String;)Z
/* if-nez v1, :cond_1 */
/* .line 626 */
v1 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isLaunchCameraForThirdApp(Lcom/android/server/am/ControllerActivityInfo;)Z */
/* if-nez v1, :cond_1 */
/* .line 627 */
com.android.server.am.SystemPressureController .getInstance ( );
v3 = this.mForegroundPkg;
v1 = (( com.android.server.am.SystemPressureController ) v1 ).isGameApp ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z
/* if-nez v1, :cond_1 */
/* .line 628 */
v1 = this.mHandler;
v3 = this.mForegroundPkg;
(( com.android.server.am.ProcessMemoryCleaner$H ) v1 ).obtainMessage ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 629 */
/* .local v1, "msg":Landroid/os/Message; */
v2 = this.mForegroundPkg;
this.obj = v2;
/* .line 630 */
/* iget v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundUid:I */
/* iput v2, v1, Landroid/os/Message;->arg1:I */
/* .line 631 */
v2 = this.mHandler;
/* iget v3, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAppSwitchBgExceptDelayTime:I */
/* int-to-long v3, v3 */
(( com.android.server.am.ProcessMemoryCleaner$H ) v2 ).sendMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 633 */
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_1
return;
} // .end method
private void sendGlobalCompactMsg ( com.android.server.am.ControllerActivityInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lcom/android/server/am/ControllerActivityInfo; */
/* .line 636 */
v0 = this.launchPkg;
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 637 */
v0 = this.mMiuiMemoryService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 638 */
int v1 = 1; // const/4 v1, 0x1
/* .line 641 */
} // :cond_0
return;
} // .end method
private void updateAppSwitchBgDelayTime ( org.json.JSONObject p0 ) {
/* .locals 3 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 365 */
final String v0 = "perf_proc_switch_Bg_time"; // const-string v0, "perf_proc_switch_Bg_time"
(( org.json.JSONObject ) p1 ).optString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 366 */
/* .local v1, "SwitchBgTime":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_0 */
/* .line 367 */
v2 = java.lang.Integer .parseInt ( v1 );
/* iput v2, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mAppSwitchBgExceptDelayTime:I */
/* .line 368 */
/* sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 369 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 372 */
} // :cond_0
return;
} // .end method
private void updateCloudControlData ( ) {
/* .locals 6 */
/* .line 330 */
v0 = this.mContext;
/* .line 331 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "perf_process"; // const-string v1, "perf_process"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataList ( v0,v1 );
/* .line 333 */
/* .local v0, "cloudDataList":Ljava/util/List;, "Ljava/util/List<Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;>;" */
/* if-nez v0, :cond_0 */
/* .line 334 */
return;
/* .line 336 */
} // :cond_0
final String v1 = ""; // const-string v1, ""
/* .line 337 */
/* .local v1, "data":Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_0
/* if-ge v2, v3, :cond_2 */
/* .line 338 */
/* check-cast v3, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v4 = "perf_proc_threshold"; // const-string v4, "perf_proc_threshold"
final String v5 = ""; // const-string v5, ""
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v3 ).getString ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 339 */
/* .local v3, "cd":Ljava/lang/String; */
v4 = android.text.TextUtils .isEmpty ( v3 );
/* if-nez v4, :cond_1 */
/* .line 340 */
/* move-object v1, v3 */
/* .line 341 */
/* .line 337 */
} // .end local v3 # "cd":Ljava/lang/String;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 345 */
} // .end local v2 # "i":I
} // :cond_2
} // :goto_1
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 346 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
/* invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateAppSwitchBgDelayTime(Lorg/json/JSONObject;)V */
/* .line 347 */
/* invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateCommonUsedPssLimitKB(Lorg/json/JSONObject;)V */
/* .line 348 */
/* invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateMemExceptionThresholdKB(Lorg/json/JSONObject;)V */
/* .line 349 */
/* invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateProtectProcessList(Lorg/json/JSONObject;)V */
/* .line 350 */
/* invoke-direct {p0, v2}, Lcom/android/server/am/ProcessMemoryCleaner;->updateProcessAdjBindList(Lorg/json/JSONObject;)V */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 353 */
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
/* .line 351 */
/* :catch_0 */
/* move-exception v2 */
/* .line 352 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v3 = "ProcessMemoryCleaner"; // const-string v3, "ProcessMemoryCleaner"
/* const-string/jumbo v4, "updateCloudData error :" */
android.util.Slog .e ( v3,v4,v2 );
/* .line 354 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_2
return;
} // .end method
private void updateCommonUsedPssLimitKB ( org.json.JSONObject p0 ) {
/* .locals 4 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 375 */
final String v0 = "perf_proc_common_pss_limit"; // const-string v0, "perf_proc_common_pss_limit"
(( org.json.JSONObject ) p1 ).optString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 376 */
/* .local v1, "pssLimitKB":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_0 */
/* .line 377 */
java.lang.Long .parseLong ( v1 );
/* move-result-wide v2 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->computeCommonUsedAppPssThreshold(J)V */
/* .line 378 */
/* sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 379 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 382 */
} // :cond_0
return;
} // .end method
private void updateMemExceptionThresholdKB ( org.json.JSONObject p0 ) {
/* .locals 4 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 385 */
final String v0 = "perf_proc_mem_exception_threshold"; // const-string v0, "perf_proc_mem_exception_threshold"
(( org.json.JSONObject ) p1 ).optString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 386 */
/* .local v1, "memExceptionThresholdKB":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_0 */
/* .line 387 */
java.lang.Long .parseLong ( v1 );
/* move-result-wide v2 */
/* invoke-direct {p0, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->computeMemExceptionThreshold(J)V */
/* .line 388 */
/* sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 389 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 392 */
} // :cond_0
return;
} // .end method
private void updateProcessAdjBindList ( org.json.JSONObject p0 ) {
/* .locals 3 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 357 */
final String v0 = "perf_proc_adj_bind_list"; // const-string v0, "perf_proc_adj_bind_list"
(( org.json.JSONObject ) p1 ).optString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 358 */
/* .local v1, "procString":Ljava/lang/String; */
com.android.server.am.OomAdjusterImpl .getInstance ( );
(( com.android.server.am.OomAdjusterImpl ) v2 ).updateProcessAdjBindList ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/OomAdjusterImpl;->updateProcessAdjBindList(Ljava/lang/String;)V
/* .line 359 */
/* sget-boolean v2, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 360 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 362 */
} // :cond_0
return;
} // .end method
private void updateProtectProcessList ( org.json.JSONObject p0 ) {
/* .locals 7 */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .line 395 */
final String v0 = "perf_proc_protect_list"; // const-string v0, "perf_proc_protect_list"
(( org.json.JSONObject ) p1 ).optString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
/* .line 396 */
/* .local v1, "processString":Ljava/lang/String; */
v2 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v2, :cond_1 */
/* .line 397 */
final String v2 = ","; // const-string v2, ","
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 398 */
/* .local v2, "processArray":[Ljava/lang/String; */
/* array-length v3, v2 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_0 */
/* aget-object v5, v2, v4 */
/* .line 399 */
/* .local v5, "processName":Ljava/lang/String; */
v6 = this.mProcessPolicy;
(( com.android.server.am.ProcessPolicy ) v6 ).updateSystemCleanWhiteList ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/am/ProcessPolicy;->updateSystemCleanWhiteList(Ljava/lang/String;)V
/* .line 398 */
} // .end local v5 # "processName":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 401 */
} // :cond_0
/* sget-boolean v3, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 402 */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->logCloudControlParas(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 405 */
} // .end local v2 # "processArray":[Ljava/lang/String;
} // :cond_1
return;
} // .end method
/* # virtual methods */
public void KillProcessForPadSmallWindowMode ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 484 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 485 */
/* .local v0, "nowTime":J */
v2 = this.mHandler;
int v3 = 3; // const/4 v3, 0x3
v2 = (( com.android.server.am.ProcessMemoryCleaner$H ) v2 ).hasMessages ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner$H;->hasMessages(I)Z
/* if-nez v2, :cond_1 */
/* iget-wide v4, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mLastPadSmallWindowUpdateTime:J */
/* sub-long v4, v0, v4 */
/* const-wide/16 v6, 0x1388 */
/* cmp-long v2, v4, v6 */
/* if-gez v2, :cond_0 */
/* .line 489 */
} // :cond_0
/* iput-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mLastPadSmallWindowUpdateTime:J */
/* .line 490 */
v2 = this.mHandler;
(( com.android.server.am.ProcessMemoryCleaner$H ) v2 ).obtainMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(I)Landroid/os/Message;
/* .line 491 */
/* .local v2, "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 492 */
v3 = this.mHandler;
(( com.android.server.am.ProcessMemoryCleaner$H ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessage(Landroid/os/Message;)Z
/* .line 493 */
return;
/* .line 487 */
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_1
} // :goto_0
return;
} // .end method
public void compactBackgroundProcess ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "processName" # Ljava/lang/String; */
/* .line 579 */
v0 = this.mSmartPowerService;
/* .line 580 */
/* .local v0, "proc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).compactBackgroundProcess ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->compactBackgroundProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;Z)V
/* .line 581 */
return;
} // .end method
public void compactBackgroundProcess ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "isDelayed" # Z */
/* .line 584 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isNeedCompact(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 585 */
int v0 = 4; // const/4 v0, 0x4
/* if-nez p2, :cond_0 */
/* .line 586 */
v1 = this.mHandler;
(( com.android.server.am.ProcessMemoryCleaner$H ) v1 ).removeMessages ( v0, p1 ); // invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->removeMessages(ILjava/lang/Object;)V
/* .line 587 */
v1 = this.mHandler;
(( com.android.server.am.ProcessMemoryCleaner$H ) v1 ).obtainMessage ( v0, p1 ); // invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 588 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 589 */
v1 = this.mHandler;
(( com.android.server.am.ProcessMemoryCleaner$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessage(Landroid/os/Message;)Z
} // .end local v0 # "msg":Landroid/os/Message;
/* .line 590 */
} // :cond_0
v1 = this.mHandler;
v1 = (( com.android.server.am.ProcessMemoryCleaner$H ) v1 ).hasMessages ( v0, p1 ); // invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->hasMessages(ILjava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 591 */
v1 = this.mHandler;
(( com.android.server.am.ProcessMemoryCleaner$H ) v1 ).obtainMessage ( v0, p1 ); // invoke-virtual {v1, v0, p1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 592 */
/* .restart local v0 # "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 593 */
v1 = this.mHandler;
/* const-wide/16 v2, 0xbb8 */
(( com.android.server.am.ProcessMemoryCleaner$H ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 590 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_1
} // :goto_0
/* nop */
/* .line 596 */
} // :cond_2
} // :goto_1
return;
} // .end method
public void foregroundActivityChanged ( com.android.server.am.ControllerActivityInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lcom/android/server/am/ControllerActivityInfo; */
/* .line 570 */
v0 = this.mHandler;
/* if-nez v0, :cond_0 */
return;
/* .line 571 */
} // :cond_0
v0 = this.mMiuiMemoryService;
/* iget v1, p1, Lcom/android/server/am/ControllerActivityInfo;->launchPid:I */
/* .line 572 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->sendAppSwitchBgExceptionMsg(Lcom/android/server/am/ControllerActivityInfo;)V */
/* .line 573 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->sendGlobalCompactMsg(Lcom/android/server/am/ControllerActivityInfo;)V */
/* .line 574 */
v0 = this.launchPkg;
this.mForegroundPkg = v0;
/* .line 575 */
/* iget v0, p1, Lcom/android/server/am/ControllerActivityInfo;->launchUid:I */
/* iput v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mForegroundUid:I */
/* .line 576 */
return;
} // .end method
public com.android.server.am.ProcMemCleanerStatistics getProcMemStat ( ) {
/* .locals 1 */
/* .line 527 */
v0 = this.mStatistics;
} // .end method
public Long killPackage ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1, java.lang.String p2 ) {
/* .locals 15 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "minAdj" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 419 */
/* move-object v0, p0 */
/* move/from16 v1, p2 */
/* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord; */
/* const-wide/16 v3, 0x0 */
/* if-nez v2, :cond_0 */
/* .line 420 */
/* return-wide v3 */
/* .line 423 */
} // :cond_0
/* const-wide/16 v5, 0x0 */
/* .line 424 */
/* .local v5, "appCurPss":J */
v2 = this.mSmartPowerService;
/* .line 425 */
v7 = /* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUid()I */
/* invoke-virtual/range {p1 ..p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String; */
/* .line 426 */
/* .local v2, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
int v7 = 0; // const/4 v7, 0x0
/* .line 427 */
/* .local v7, "mainProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v9 = } // :goto_0
if ( v9 != null) { // if-eqz v9, :cond_3
/* check-cast v9, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 428 */
/* .local v9, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).getPss ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
/* move-result-wide v10 */
/* add-long/2addr v5, v10 */
/* .line 429 */
v10 = /* invoke-direct {p0, v9, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z */
/* if-nez v10, :cond_1 */
/* .line 430 */
/* return-wide v3 */
/* .line 432 */
} // :cond_1
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).getPackageName ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v9 ).getProcessName ( ); // invoke-virtual {v9}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
v10 = (( java.lang.String ) v10 ).equals ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 433 */
/* move-object v7, v9 */
/* .line 435 */
} // .end local v9 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_2
/* .line 437 */
} // :cond_3
if ( v7 != null) { // if-eqz v7, :cond_4
v8 = /* invoke-direct {p0, v7, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 438 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v7 ).getPackageName ( ); // invoke-virtual {v7}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
v4 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v7 ).getUserId ( ); // invoke-virtual {v7}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getUserId()I
/* .line 439 */
/* invoke-direct {p0, v7}, Lcom/android/server/am/ProcessMemoryCleaner;->getKillReason(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String; */
/* .line 438 */
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).forceStopPackage ( v3, v4, v8 ); // invoke-virtual {p0, v3, v4, v8}, Lcom/android/server/am/ProcessMemoryCleaner;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 440 */
v9 = this.mStatistics;
int v10 = 2; // const/4 v10, 0x2
/* move-object v11, v7 */
/* move-wide v12, v5 */
/* move-object/from16 v14, p3 */
/* invoke-virtual/range {v9 ..v14}, Lcom/android/server/am/ProcMemCleanerStatistics;->reportEvent(ILcom/miui/server/smartpower/IAppState$IRunningProcess;JLjava/lang/String;)V */
/* .line 442 */
/* return-wide v5 */
/* .line 444 */
} // :cond_4
/* return-wide v3 */
} // .end method
public Long killPackage ( com.miui.server.smartpower.IAppState$IRunningProcess p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 415 */
/* const/16 v0, 0xc8 */
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).killPackage ( p1, v0, p2 ); // invoke-virtual {p0, p1, v0, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killPackage(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Long killProcess ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1, java.lang.String p2 ) {
/* .locals 10 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "minAdj" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 460 */
v0 = (( com.android.server.am.ProcessMemoryCleaner ) p0 ).isCurrentProcessInBackup ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z
/* const-wide/16 v1, 0x0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 461 */
/* return-wide v1 */
/* .line 463 */
} // :cond_0
v0 = this.mAMS;
/* monitor-enter v0 */
/* .line 464 */
try { // :try_start_0
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessRecord ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
/* .line 465 */
/* .local v3, "proc":Lcom/android/server/am/ProcessRecord; */
v4 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->checkRunningProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z */
if ( v4 != null) { // if-eqz v4, :cond_1
v4 = /* invoke-direct {p0, v3}, Lcom/android/server/am/ProcessMemoryCleaner;->isRunningComponent(Lcom/android/server/am/ProcessRecord;)Z */
/* if-nez v4, :cond_1 */
/* .line 466 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessMemoryCleaner;->getKillReason(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Ljava/lang/String; */
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).killApplicationLock ( v3, v1 ); // invoke-virtual {p0, v3, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 467 */
v4 = this.mStatistics;
int v5 = 1; // const/4 v5, 0x1
/* .line 468 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPss ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
/* move-result-wide v7 */
/* .line 467 */
/* move-object v6, p1 */
/* move-object v9, p3 */
/* invoke-virtual/range {v4 ..v9}, Lcom/android/server/am/ProcMemCleanerStatistics;->reportEvent(ILcom/miui/server/smartpower/IAppState$IRunningProcess;JLjava/lang/String;)V */
/* .line 469 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPss ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPss()J
/* move-result-wide v1 */
/* monitor-exit v0 */
/* return-wide v1 */
/* .line 471 */
} // .end local v3 # "proc":Lcom/android/server/am/ProcessRecord;
} // :cond_1
/* monitor-exit v0 */
/* .line 472 */
/* return-wide v1 */
/* .line 471 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Long killProcess ( com.miui.server.smartpower.IAppState$IRunningProcess p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 452 */
/* const/16 v0, 0xc8 */
(( com.android.server.am.ProcessMemoryCleaner ) p0 ).killProcess ( p1, v0, p2 ); // invoke-virtual {p0, p1, v0, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->killProcess(Lcom/miui/server/smartpower/IAppState$IRunningProcess;ILjava/lang/String;)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public void onApplyOomAdjLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 184 */
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getSetAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
/* if-gtz v0, :cond_0 */
/* .line 185 */
v0 = this.mMiuiMemoryService;
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* .line 187 */
} // :cond_0
return;
} // .end method
public void onBootPhase ( ) {
/* .locals 2 */
/* .line 136 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.am.ProcessMemoryCleaner$H ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner$H;->obtainMessage(I)Landroid/os/Message;
/* .line 137 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.android.server.am.ProcessMemoryCleaner$H ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessMemoryCleaner$H;->sendMessage(Landroid/os/Message;)Z
/* .line 138 */
return;
} // .end method
public Boolean scanProcessAndCleanUpMemory ( Long p0 ) {
/* .locals 10 */
/* .param p1, "targetReleaseMem" # J */
/* .line 145 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 146 */
int v0 = 0; // const/4 v0, 0x0
/* .line 148 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v0 = "ProcessMemoryCleaner"; // const-string v0, "ProcessMemoryCleaner"
final String v1 = "Start clean up memory....."; // const-string v1, "Start clean up memory....."
android.util.Slog .d ( v0,v1 );
/* .line 149 */
} // :cond_1
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 151 */
/* .local v0, "runningProcList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v1 = this.mSmartPowerService;
/* .line 152 */
/* .line 153 */
/* .local v1, "appStateList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;" */
(( java.util.ArrayList ) v1 ).iterator ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Lcom/miui/server/smartpower/IAppState; */
/* .line 154 */
/* .local v3, "appState":Lcom/miui/server/smartpower/IAppState; */
v4 = (( com.miui.server.smartpower.IAppState ) v3 ).isVsible ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->isVsible()Z
/* if-nez v4, :cond_4 */
/* .line 155 */
(( com.miui.server.smartpower.IAppState ) v3 ).getRunningProcessList ( ); // invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 156 */
/* .local v5, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
v6 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getAdj ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* if-lez v6, :cond_3 */
/* .line 157 */
v6 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getAdj ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* const/16 v7, 0xc8 */
/* if-lt v6, v7, :cond_2 */
/* .line 158 */
v6 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getAdj ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I
/* const/16 v7, 0xfa */
/* if-le v6, v7, :cond_3 */
/* .line 159 */
} // :cond_2
v6 = /* invoke-direct {p0, v5}, Lcom/android/server/am/ProcessMemoryCleaner;->isIsolatedByAdj(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
/* if-nez v6, :cond_3 */
/* .line 160 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
/* .line 161 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getProcessName ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
/* .line 160 */
v6 = /* invoke-direct {p0, v6, v7}, Lcom/android/server/am/ProcessMemoryCleaner;->isImportantSubProc(Ljava/lang/String;Ljava/lang/String;)Z */
/* if-nez v6, :cond_3 */
/* .line 162 */
v6 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).isProcessPerceptible ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isProcessPerceptible()Z
/* if-nez v6, :cond_3 */
/* .line 163 */
v6 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).hasForegrundService ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasForegrundService()Z
/* if-nez v6, :cond_3 */
/* .line 164 */
v6 = /* invoke-direct {p0, v5}, Lcom/android/server/am/ProcessMemoryCleaner;->isSystemHighPrioProc(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
/* if-nez v6, :cond_3 */
/* .line 165 */
v6 = (( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getPid ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPid()I
v6 = /* invoke-direct {p0, v6}, Lcom/android/server/am/ProcessMemoryCleaner;->isLastMusicPlayProcess(I)Z */
/* if-nez v6, :cond_3 */
/* .line 166 */
v6 = /* invoke-direct {p0, v5}, Lcom/android/server/am/ProcessMemoryCleaner;->containInWhiteList(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
/* if-nez v6, :cond_3 */
/* .line 167 */
v6 = /* invoke-direct {p0, v3, v5}, Lcom/android/server/am/ProcessMemoryCleaner;->isAutoStartProcess(Lcom/miui/server/smartpower/IAppState;Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z */
/* if-nez v6, :cond_3 */
v6 = this.mSmartPowerService;
/* .line 169 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getPackageName ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v5 ).getProcessName ( ); // invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
/* .line 168 */
v6 = /* const/16 v9, 0x1d8 */
/* if-nez v6, :cond_3 */
/* .line 170 */
/* .line 172 */
} // .end local v5 # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
} // :cond_3
/* .line 174 */
} // .end local v3 # "appState":Lcom/miui/server/smartpower/IAppState;
} // :cond_4
/* goto/16 :goto_0 */
/* .line 175 */
} // :cond_5
v2 = /* invoke-direct {p0, v0, p1, p2}, Lcom/android/server/am/ProcessMemoryCleaner;->cleanUpMemory(Ljava/util/List;J)Z */
} // .end method
public void systemReady ( android.content.Context p0, com.android.server.am.ProcessManagerService p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .line 120 */
/* invoke-super {p0, p1, p2}, Lcom/android/server/am/ProcessCleanerBase;->systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V */
/* .line 121 */
/* sget-boolean v0, Lcom/android/server/am/ProcessMemoryCleaner;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "ProcessMemoryCleaner"; // const-string v0, "ProcessMemoryCleaner"
final String v1 = "ProcessesCleaner init"; // const-string v1, "ProcessesCleaner init"
android.util.Slog .d ( v0,v1 );
/* .line 122 */
} // :cond_0
this.mContext = p1;
/* .line 123 */
this.mPMS = p2;
/* .line 124 */
v0 = this.mHandlerTh;
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 125 */
/* new-instance v0, Lcom/android/server/am/ProcessMemoryCleaner$H; */
v1 = this.mHandlerTh;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/ProcessMemoryCleaner$H;-><init>(Lcom/android/server/am/ProcessMemoryCleaner;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 126 */
v0 = this.mHandlerTh;
/* .line 127 */
v0 = (( android.os.HandlerThread ) v0 ).getThreadId ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I
/* .line 126 */
int v1 = 1; // const/4 v1, 0x1
android.os.Process .setThreadGroupAndCpuset ( v0,v1 );
/* .line 128 */
v0 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v0 ).getProcessPolicy ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
this.mProcessPolicy = v0;
/* .line 129 */
/* const-class v0, Lcom/android/server/am/MiuiMemoryServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/am/MiuiMemoryServiceInternal; */
this.mMiuiMemoryService = v0;
/* .line 130 */
com.android.server.am.ProcMemCleanerStatistics .getInstance ( );
this.mStatistics = v0;
/* .line 131 */
/* sget-wide v0, Landroid/os/spc/PressureStateSettings;->PROC_MEM_EXCEPTION_PSS_LIMIT_KB:J */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->computeMemExceptionThreshold(J)V */
/* .line 132 */
/* iget-wide v0, p0, Lcom/android/server/am/ProcessMemoryCleaner;->mCommonUsedPssLimitKB:J */
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessMemoryCleaner;->computeCommonUsedAppPssThreshold(J)V */
/* .line 133 */
return;
} // .end method
