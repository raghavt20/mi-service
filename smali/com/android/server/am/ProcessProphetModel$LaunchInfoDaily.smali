.class public final Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
.super Ljava/lang/Object;
.source "ProcessProphetModel.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LaunchInfoDaily"
.end annotation


# instance fields
.field public isWeekend:Z

.field public launchUpdateTimes:J

.field launchUsageInThisDay:[Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public zeroTimeStamp:J


# direct methods
.method constructor <init>(J)V
    .locals 2
    .param p1, "zeroTimeStamp"    # J

    .line 724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 719
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->zeroTimeStamp:J

    .line 720
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUpdateTimes:J

    .line 721
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->isWeekend:Z

    .line 722
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/util/HashMap;

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUsageInThisDay:[Ljava/util/HashMap;

    .line 725
    iput-wide p1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->zeroTimeStamp:J

    .line 726
    return-void
.end method

.method private add(Ljava/lang/String;II)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "hourOfDay"    # I
    .param p3, "launchFreq"    # I

    .line 739
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUsageInThisDay:[Ljava/util/HashMap;

    aget-object v1, v0, p2

    if-nez v1, :cond_0

    .line 740
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    aput-object v1, v0, p2

    .line 742
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUsageInThisDay:[Ljava/util/HashMap;

    aget-object v0, v0, p2

    .line 743
    .local v0, "temp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 744
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, p3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 746
    :cond_1
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 748
    :goto_0
    iget-wide v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUpdateTimes:J

    int-to-long v3, p3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUpdateTimes:J

    .line 749
    return-void
.end method


# virtual methods
.method public combine(Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;)V
    .locals 5
    .param p1, "another"    # Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;

    .line 753
    const/4 v0, 0x0

    .local v0, "hourOfDay":I
    :goto_0
    const/16 v1, 0x18

    if-ge v0, v1, :cond_1

    .line 755
    iget-object v1, p1, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUsageInThisDay:[Ljava/util/HashMap;

    aget-object v1, v1, v0

    .line 756
    .local v1, "usageInHour":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 757
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 758
    .local v3, "pkg":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v3, v0, v4}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->add(Ljava/lang/String;II)V

    .line 759
    .end local v3    # "pkg":Ljava/lang/String;
    goto :goto_1

    .line 753
    .end local v1    # "usageInHour":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 762
    .end local v0    # "hourOfDay":I
    :cond_1
    return-void
.end method

.method public dump()V
    .locals 9

    .line 765
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 766
    .local v0, "calendar":Ljava/util/Calendar;
    iget-wide v1, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->zeroTimeStamp:J

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 767
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Launch Usage OneDay of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ProcessProphetModel"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    const/4 v1, 0x0

    .local v1, "hourOfDay":I
    :goto_0
    const/16 v3, 0x18

    if-ge v1, v3, :cond_3

    .line 769
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->launchUsageInThisDay:[Ljava/util/HashMap;

    aget-object v3, v3, v1

    .line 770
    .local v3, "temp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-nez v3, :cond_0

    .line 771
    goto/16 :goto_3

    .line 773
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\thourOfDay="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 774
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " data:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 775
    .local v4, "dumpRes":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 776
    .local v6, "pkg":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 777
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0xc8

    if-le v7, v8, :cond_1

    .line 778
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "......"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 779
    goto :goto_2

    .line 781
    .end local v6    # "pkg":Ljava/lang/String;
    :cond_1
    goto :goto_1

    .line 782
    :cond_2
    :goto_2
    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    .end local v3    # "temp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v4    # "dumpRes":Ljava/lang/String;
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 784
    .end local v1    # "hourOfDay":I
    :cond_3
    return-void
.end method

.method public update(Ljava/lang/String;J)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "launchTimeStamp"    # J

    .line 730
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 731
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 732
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 734
    .local v1, "hourOfDay":I
    const/4 v2, 0x1

    invoke-direct {p0, p1, v1, v2}, Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;->add(Ljava/lang/String;II)V

    .line 735
    return-void
.end method
