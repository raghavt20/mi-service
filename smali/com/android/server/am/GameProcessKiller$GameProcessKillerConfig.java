public class com.android.server.am.GameProcessKiller$GameProcessKillerConfig implements com.android.server.am.IGameProcessAction$IGameProcessActionConfig {
	 /* .source "GameProcessKiller.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/GameProcessKiller; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "GameProcessKillerConfig" */
} // .end annotation
/* # static fields */
private static final java.lang.String CONFIG_PRIO;
private static final Integer DEFAULT_PRIO;
private static final java.lang.String MIN_PROC_STATE;
private static final java.lang.String SKIP_ACTIVE;
private static final java.lang.String SKIP_FOREGROUND;
private static final java.lang.String WHITE_LIST;
/* # instance fields */
Integer mMinProcState;
Integer mPrio;
Boolean mSkipActive;
Boolean mSkipForeground;
java.util.List mWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.server.am.GameProcessKiller$GameProcessKillerConfig ( ) {
/* .locals 1 */
/* .line 153 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 163 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I */
/* .line 164 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mWhiteList = v0;
/* .line 165 */
/* const/16 v0, 0x14 */
/* iput v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mMinProcState:I */
/* .line 166 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipForeground:Z */
/* .line 167 */
/* iput-boolean v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipActive:Z */
return;
} // .end method
/* # virtual methods */
public void addWhiteList ( java.util.List p0, Boolean p1 ) {
/* .locals 1 */
/* .param p2, "append" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 171 */
/* .local p1, "wl":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 172 */
v0 = this.mWhiteList;
/* .line 174 */
} // :cond_0
v0 = this.mWhiteList;
/* .line 175 */
} // :goto_0
v0 = this.mWhiteList;
/* .line 176 */
return;
} // .end method
public Integer getPrio ( ) {
/* .locals 1 */
/* .line 218 */
/* iget v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I */
} // .end method
public void initFromJSON ( org.json.JSONObject p0 ) {
/* .locals 6 */
/* .param p1, "obj" # Lorg/json/JSONObject; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 190 */
/* const-string/jumbo v0, "white-list" */
/* const-string/jumbo v1, "skip-foreground" */
/* const-string/jumbo v2, "skip-active" */
final String v3 = "min-proc-state"; // const-string v3, "min-proc-state"
final String v4 = "prio"; // const-string v4, "prio"
try { // :try_start_0
v5 = (( org.json.JSONObject ) p1 ).has ( v4 ); // invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 191 */
int v5 = 0; // const/4 v5, 0x0
v4 = (( org.json.JSONObject ) p1 ).optInt ( v4, v5 ); // invoke-virtual {p1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v4, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I */
/* .line 193 */
} // :cond_0
v4 = (( org.json.JSONObject ) p1 ).has ( v3 ); // invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 194 */
/* const/16 v4, 0x14 */
v3 = (( org.json.JSONObject ) p1 ).optInt ( v3, v4 ); // invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v3, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mMinProcState:I */
/* .line 196 */
} // :cond_1
v3 = (( org.json.JSONObject ) p1 ).has ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 197 */
v2 = (( org.json.JSONObject ) p1 ).optBoolean ( v2, v4 ); // invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v2, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipActive:Z */
/* .line 199 */
} // :cond_2
v2 = (( org.json.JSONObject ) p1 ).has ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 200 */
v1 = (( org.json.JSONObject ) p1 ).optBoolean ( v1, v4 ); // invoke-virtual {p1, v1, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
/* iput-boolean v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipForeground:Z */
/* .line 202 */
} // :cond_3
v1 = (( org.json.JSONObject ) p1 ).has ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 203 */
(( org.json.JSONObject ) p1 ).optJSONArray ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 204 */
/* .local v0, "tmpArray":Lorg/json/JSONArray; */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 205 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v1, v2, :cond_5 */
/* .line 206 */
v2 = (( org.json.JSONArray ) v0 ).isNull ( v1 ); // invoke-virtual {v0, v1}, Lorg/json/JSONArray;->isNull(I)Z
/* if-nez v2, :cond_4 */
/* .line 207 */
v2 = this.mWhiteList;
(( org.json.JSONArray ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 205 */
} // :cond_4
/* add-int/lit8 v1, v1, 0x1 */
/* .line 213 */
} // .end local v0 # "tmpArray":Lorg/json/JSONArray;
} // .end local v1 # "i":I
} // :cond_5
/* nop */
/* .line 214 */
return;
/* .line 211 */
/* :catch_0 */
/* move-exception v0 */
/* .line 212 */
/* .local v0, "e":Lorg/json/JSONException; */
/* throw v0 */
} // .end method
public void removeWhiteList ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 179 */
/* .local p1, "wl":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez p1, :cond_0 */
/* .line 180 */
v0 = this.mWhiteList;
/* .line 182 */
} // :cond_0
v0 = this.mWhiteList;
/* .line 183 */
} // :goto_0
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 223 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 224 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
final String v1 = "prio="; // const-string v1, "prio="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 225 */
/* iget v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 226 */
final String v1 = ", minProcState="; // const-string v1, ", minProcState="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 227 */
/* iget v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mMinProcState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 228 */
final String v1 = ", skipForeground="; // const-string v1, ", skipForeground="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 229 */
/* iget-boolean v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipForeground:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 230 */
final String v1 = ", skipActive="; // const-string v1, ", skipActive="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 231 */
/* iget-boolean v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipActive:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* .line 232 */
final String v1 = ", whiteList="; // const-string v1, ", whiteList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 233 */
v1 = this.mWhiteList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 234 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
