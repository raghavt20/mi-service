class com.android.server.am.GameProcessCompactor$ProcessCompactInfo {
	 /* .source "GameProcessCompactor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/GameProcessCompactor; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "ProcessCompactInfo" */
} // .end annotation
/* # static fields */
private static final java.lang.String COMPACT_ACTION_FULL;
/* # instance fields */
private Boolean mDied;
private Long mLastCompactTime;
private Integer mLastIncomePct;
private Integer mPid;
/* # direct methods */
 com.android.server.am.GameProcessCompactor$ProcessCompactInfo ( ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 118 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 119 */
/* iput p1, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I */
/* .line 120 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mDied:Z */
/* .line 121 */
return;
} // .end method
private Boolean shouldCompact ( com.android.server.am.GameProcessCompactor p0 ) {
/* .locals 8 */
/* .param p1, "compactor" # Lcom/android/server/am/GameProcessCompactor; */
/* .line 124 */
/* iget-boolean v0, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mDied:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 125 */
	 /* .line 126 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* iget-wide v4, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastCompactTime:J */
/* sub-long/2addr v2, v4 */
(( com.android.server.am.GameProcessCompactor ) p1 ).getIntervalThreshold ( ); // invoke-virtual {p1}, Lcom/android/server/am/GameProcessCompactor;->getIntervalThreshold()J
/* move-result-wide v4 */
/* const-wide/16 v6, 0x3e8 */
/* mul-long/2addr v4, v6 */
/* cmp-long v0, v2, v4 */
/* if-lez v0, :cond_1 */
/* .line 127 */
/* .line 128 */
} // :cond_1
/* iget v0, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastIncomePct:I */
/* int-to-long v2, v0 */
(( com.android.server.am.GameProcessCompactor ) p1 ).getIncomePctThreshold ( ); // invoke-virtual {p1}, Lcom/android/server/am/GameProcessCompactor;->getIncomePctThreshold()J
/* move-result-wide v4 */
/* cmp-long v0, v2, v4 */
/* if-lez v0, :cond_2 */
/* .line 129 */
/* .line 130 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
Long compact ( com.android.server.am.GameProcessCompactor p0 ) {
/* .locals 9 */
/* .param p1, "compactor" # Lcom/android/server/am/GameProcessCompactor; */
/* .line 134 */
/* const-wide/16 v0, -0x1 */
/* .line 135 */
/* .local v0, "income":J */
v2 = /* invoke-direct {p0, p1}, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->shouldCompact(Lcom/android/server/am/GameProcessCompactor;)Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 136 */
/* iget v2, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I */
android.os.Process .getRss ( v2 );
/* .line 137 */
/* .local v2, "rssBefore":[J */
int v3 = 0; // const/4 v3, 0x0
/* aget-wide v4, v2, v3 */
/* const-wide/16 v6, 0x0 */
/* cmp-long v4, v4, v6 */
/* if-gtz v4, :cond_0 */
/* .line 138 */
/* return-wide v0 */
/* .line 139 */
} // :cond_0
com.android.server.am.SystemPressureControllerStub .getInstance ( );
final String v5 = "all"; // const-string v5, "all"
/* iget v6, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I */
(( com.android.server.am.SystemPressureControllerStub ) v4 ).performCompaction ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Lcom/android/server/am/SystemPressureControllerStub;->performCompaction(Ljava/lang/String;I)V
/* .line 140 */
/* iget v4, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I */
android.os.Process .getRss ( v4 );
/* .line 141 */
/* .local v4, "rssAfter":[J */
/* aget-wide v5, v2, v3 */
/* aget-wide v7, v4, v3 */
/* sub-long v0, v5, v7 */
/* .line 142 */
/* const-wide/16 v5, 0x64 */
/* mul-long/2addr v5, v0 */
/* aget-wide v7, v2, v3 */
/* div-long/2addr v5, v7 */
/* long-to-int v3, v5 */
/* iput v3, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastIncomePct:I */
/* .line 143 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
/* iput-wide v5, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastCompactTime:J */
/* .line 145 */
com.android.server.am.GameProcessCompactor .-$$Nest$sfgetTAG ( );
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "compact "; // const-string v6, "compact "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", reclaim mem: "; // const-string v6, ", reclaim mem: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0, v1 ); // invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v6 = ", income pct:"; // const-string v6, ", income pct:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastIncomePct:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v5 );
/* .line 147 */
} // .end local v2 # "rssBefore":[J
} // .end local v4 # "rssAfter":[J
} // :cond_1
/* return-wide v0 */
} // .end method
void notifyDied ( ) {
/* .locals 1 */
/* .line 151 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mDied:Z */
/* .line 152 */
return;
} // .end method
