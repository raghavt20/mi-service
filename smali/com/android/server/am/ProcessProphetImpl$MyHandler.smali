.class public Lcom/android/server/am/ProcessProphetImpl$MyHandler;
.super Landroid/os/Handler;
.source "ProcessProphetImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessProphetImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessProphetImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessProphetImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1087
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    .line 1088
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1089
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 1093
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 1134
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v0}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$fgetmProcessProphetCloud(Lcom/android/server/am/ProcessProphetImpl;)Lcom/android/server/am/ProcessProphetCloud;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->registerProcProphetCloudObserver()V

    .line 1135
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v0}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$fgetmProcessProphetCloud(Lcom/android/server/am/ProcessProphetImpl;)Lcom/android/server/am/ProcessProphetCloud;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateProcProphetCloudControlParas()V

    .line 1136
    goto/16 :goto_0

    .line 1131
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetImpl;->testMTBF()V

    .line 1132
    goto/16 :goto_0

    .line 1128
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v0}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleDump(Lcom/android/server/am/ProcessProphetImpl;)V

    .line 1129
    goto :goto_0

    .line 1125
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleMemPressure(Lcom/android/server/am/ProcessProphetImpl;I)V

    .line 1126
    goto :goto_0

    .line 1122
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v0}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleIdleUpdate(Lcom/android/server/am/ProcessProphetImpl;)V

    .line 1123
    goto :goto_0

    .line 1119
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/am/ProcessRecord;

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleKillProc(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessRecord;)V

    .line 1120
    goto :goto_0

    .line 1116
    :pswitch_7
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleCheckEmptyProcs(Lcom/android/server/am/ProcessProphetImpl;I)V

    .line 1117
    goto :goto_0

    .line 1113
    :pswitch_8
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/am/ProcessRecord;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1, v2}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleProcStarted(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessRecord;I)V

    .line 1114
    goto :goto_0

    .line 1110
    :pswitch_9
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleCopy(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/CharSequence;)V

    .line 1111
    goto :goto_0

    .line 1107
    :pswitch_a
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleLaunchEvent(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;II)V

    .line 1108
    goto :goto_0

    .line 1104
    :pswitch_b
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleAudioFocusChanged(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;)V

    .line 1105
    goto :goto_0

    .line 1101
    :pswitch_c
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v0}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleBluetoothDisConnected(Lcom/android/server/am/ProcessProphetImpl;)V

    .line 1102
    goto :goto_0

    .line 1098
    :pswitch_d
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleBluetoothConnected(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;)V

    .line 1099
    goto :goto_0

    .line 1095
    :pswitch_e
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v0}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mhandleUnlock(Lcom/android/server/am/ProcessProphetImpl;)V

    .line 1096
    nop

    .line 1140
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
