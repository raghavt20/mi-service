.class Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
.super Ljava/lang/Object;
.source "MemoryStandardProcessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryStandardProcessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppInfo"
.end annotation


# instance fields
.field public mAdj:I

.field public mKillTimes:I

.field public mLastKillTime:J

.field public mMemExcelTime:I

.field public final mPackageName:Ljava/lang/String;

.field public final mProcessMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final mUid:I

.field public final mUserId:I

.field final synthetic this$0:Lcom/android/server/am/MemoryStandardProcessControl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MemoryStandardProcessControl;Ljava/lang/String;III)V
    .locals 2
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "userId"    # I
    .param p5, "adj"    # I

    .line 1245
    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1241
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mProcessMap:Ljava/util/Map;

    .line 1242
    const/4 p1, 0x0

    iput p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I

    .line 1243
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mLastKillTime:J

    .line 1246
    iput-object p2, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mPackageName:Ljava/lang/String;

    .line 1247
    iput p3, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUid:I

    .line 1248
    iput p4, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUserId:I

    .line 1249
    iput p5, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    .line 1250
    const/4 p1, 0x1

    iput p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I

    .line 1251
    return-void
.end method


# virtual methods
.method public getTotalPss()I
    .locals 7

    .line 1254
    const/4 v0, 0x0

    .line 1255
    .local v0, "pss":I
    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mProcessMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1256
    .local v2, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    int-to-long v3, v0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v3, v5

    long-to-int v0, v3

    .line 1257
    .end local v2    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    goto :goto_0

    .line 1258
    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1263
    iget-object v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mProcessMap:Ljava/util/Map;

    .line 1264
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUserId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/Object;

    move-result-object v0

    .line 1263
    const-string/jumbo v1, "{ package: %s, processes: %s, uid: %d, userId: %d, adj: %d }"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
