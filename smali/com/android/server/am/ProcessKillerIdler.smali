.class public Lcom/android/server/am/ProcessKillerIdler;
.super Landroid/app/job/JobService;
.source "ProcessKillerIdler.java"


# static fields
.field static final APP_MEM_THRESHOLD:J = 0x12cL

.field static final BACKUP_APP_MEM_THRESHOLD:J = 0x64L

.field static final BACKUP_MEM_THRESHOLD:J = 0x3e8L

.field static final CHECK_FREE_MEM_TIME:J = 0x1499700L

.field private static PROCESS_KILL_JOB_ID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ProcessKillerIdler"

.field static final blackList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static cm:Landroid/content/ComponentName;


# instance fields
.field private mAm:Lcom/android/server/am/ActivityManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 20
    new-instance v0, Landroid/content/ComponentName;

    .line 21
    const-class v1, Lcom/android/server/am/ProcessKillerIdler;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android"

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/am/ProcessKillerIdler;->cm:Landroid/content/ComponentName;

    .line 23
    const/16 v0, 0x64

    sput v0, Lcom/android/server/am/ProcessKillerIdler;->PROCESS_KILL_JOB_ID:I

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessKillerIdler;->blackList:Ljava/util/ArrayList;

    .line 38
    const-string v1, "com.tencent.mm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    const-string v1, "com.tencent.mobileqq"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 42
    nop

    .line 43
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/android/server/am/ProcessKillerIdler;->mAm:Lcom/android/server/am/ActivityManagerService;

    .line 42
    return-void
.end method

.method public static schedule(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 100
    const-string v0, "jobscheduler"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 102
    .local v0, "jobScheduler":Landroid/app/job/JobScheduler;
    new-instance v1, Landroid/app/job/JobInfo$Builder;

    sget v2, Lcom/android/server/am/ProcessKillerIdler;->PROCESS_KILL_JOB_ID:I

    sget-object v3, Lcom/android/server/am/ProcessKillerIdler;->cm:Landroid/content/ComponentName;

    invoke-direct {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 103
    .local v1, "builder":Landroid/app/job/JobInfo$Builder;
    const-wide/32 v2, 0x1499700

    invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    .line 104
    invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 105
    return-void
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 17
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 47
    move-object/from16 v1, p0

    const-string v0, "ProcessKillerIdler"

    const-string v2, "ProcessKillerIdler onStartJob"

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v2, v1, Lcom/android/server/am/ProcessKillerIdler;->mAm:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v2

    .line 49
    :try_start_0
    iget-object v0, v1, Lcom/android/server/am/ProcessKillerIdler;->mAm:Lcom/android/server/am/ActivityManagerService;

    const/16 v3, 0x12c

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/android/server/am/ProcessUtils;->getProcessListByAdj(Lcom/android/server/am/ActivityManagerService;ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 51
    .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 52
    .local v3, "N":I
    const-wide/16 v4, 0x0

    .line 53
    .local v4, "totalBackupProcsMem":J
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v6, "backupProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    const/4 v8, 0x3

    const/4 v9, 0x1

    const-wide/16 v10, 0x400

    if-ge v7, v3, :cond_2

    .line 56
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/server/am/ProcessRecord;

    .line 57
    .local v12, "app":Lcom/android/server/am/ProcessRecord;
    sget-object v13, Lcom/android/server/am/ProcessKillerIdler;->blackList:Ljava/util/ArrayList;

    iget-object v14, v12, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    iget-object v13, v12, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    .line 58
    invoke-virtual {v13}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v13

    div-long/2addr v13, v10

    const-wide/16 v15, 0x12c

    cmp-long v13, v13, v15

    if-lez v13, :cond_0

    .line 60
    const-string v13, "ProcessKillerIdler"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "killing process "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v12, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " pid "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 61
    invoke-virtual {v12}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " size "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v12, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v15}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v15

    div-long v10, v15, v10

    invoke-virtual {v14, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 60
    invoke-static {v13, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const-string v10, "low mem kill"

    invoke-virtual {v12, v10, v8, v9}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V

    .line 64
    goto :goto_1

    .line 67
    :cond_0
    iget-object v8, v12, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v8}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v8

    const/16 v9, 0x190

    if-ne v8, v9, :cond_1

    .line 68
    iget-object v8, v12, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v8}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v8

    add-long/2addr v4, v8

    .line 69
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    .end local v12    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_1
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 74
    .end local v7    # "i":I
    :cond_2
    div-long v12, v4, v10

    const-wide/16 v14, 0x3e8

    cmp-long v7, v12, v14

    if-ltz v7, :cond_4

    .line 75
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v7, v12, :cond_4

    .line 76
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/server/am/ProcessRecord;

    .line 77
    .restart local v12    # "app":Lcom/android/server/am/ProcessRecord;
    iget-object v13, v12, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v13}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v13

    div-long/2addr v13, v10

    const-wide/16 v15, 0x64

    cmp-long v13, v13, v15

    if-lez v13, :cond_3

    .line 79
    const-string v13, "ProcessKillerIdler"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "killing process "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v12, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " pid "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 80
    invoke-virtual {v12}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " size "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v12, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v15}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v15

    div-long v8, v15, v10

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " reason: backup procs\' totalMem is too big, need to kill big mem proc"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 79
    invoke-static {v13, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const-string v8, "low mem kill"

    const/4 v9, 0x3

    const/4 v13, 0x1

    invoke-virtual {v12, v8, v9, v13}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V

    goto :goto_3

    .line 77
    :cond_3
    move v13, v9

    move v9, v8

    .line 75
    .end local v12    # "app":Lcom/android/server/am/ProcessRecord;
    :goto_3
    add-int/lit8 v7, v7, 0x1

    move v8, v9

    move v9, v13

    goto :goto_2

    .line 86
    .end local v0    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    .end local v3    # "N":I
    .end local v4    # "totalBackupProcsMem":J
    .end local v6    # "backupProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    .end local v7    # "i":I
    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    const/4 v0, 0x0

    move-object/from16 v3, p1

    invoke-virtual {v1, v3, v0}, Lcom/android/server/am/ProcessKillerIdler;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 89
    invoke-static/range {p0 .. p0}, Lcom/android/server/am/ProcessKillerIdler;->schedule(Landroid/content/Context;)V

    .line 90
    return v0

    .line 86
    :catchall_0
    move-exception v0

    move-object/from16 v3, p1

    :goto_4
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 2
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 95
    const-string v0, "ProcessKillerIdler"

    const-string v1, "ProcessKillerIdler onStopJob"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v0, 0x0

    return v0
.end method
