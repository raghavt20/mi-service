.class Lcom/android/server/am/AppStateManager$PowerFrozenCallback;
.super Ljava/lang/Object;
.source "AppStateManager.java"

# interfaces
.implements Lcom/miui/server/smartpower/PowerFrozenManager$IFrozenReportCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PowerFrozenCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/AppStateManager;


# direct methods
.method private constructor <init>(Lcom/android/server/am/AppStateManager;)V
    .locals 0

    .line 3253
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/AppStateManager;Lcom/android/server/am/AppStateManager$PowerFrozenCallback-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;-><init>(Lcom/android/server/am/AppStateManager;)V

    return-void
.end method


# virtual methods
.method public reportBinderState(IIIIJ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "tid"    # I
    .param p4, "binderState"    # I
    .param p5, "now"    # J

    .line 3288
    packed-switch p4, :pswitch_data_0

    goto :goto_0

    .line 3296
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive binder state: uid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] pid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] tid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3299
    .local v0, "reason":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1, p1, p2, v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager;IILjava/lang/String;)V

    .line 3300
    goto :goto_0

    .line 3292
    .end local v0    # "reason":Ljava/lang/String;
    :pswitch_1
    goto :goto_0

    .line 3290
    :pswitch_2
    nop

    .line 3304
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public reportBinderTrans(IIIIIZJJ)V
    .locals 2
    .param p1, "dstUid"    # I
    .param p2, "dstPid"    # I
    .param p3, "callerUid"    # I
    .param p4, "callerPid"    # I
    .param p5, "callerTid"    # I
    .param p6, "isOneway"    # Z
    .param p7, "now"    # J
    .param p9, "buffer"    # J

    .line 3268
    if-eqz p6, :cond_0

    iget-object v0, p0, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-virtual {v0, p3, p4}, Lcom/android/server/am/AppStateManager;->isProcessPerceptible(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3269
    return-void

    .line 3271
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive binder trans: dstUid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] dstPid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] callerUid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] callerPid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] callerTid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] isOneway["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] code["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p9, p10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3278
    .local v0, "reason":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1, p1, p2, v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager;IILjava/lang/String;)V

    .line 3279
    return-void
.end method

.method public reportNet(IJ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "now"    # J

    .line 3260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive net request: uid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3261
    .local v0, "reason":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1, p1, v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager;ILjava/lang/String;)V

    .line 3262
    return-void
.end method

.method public reportSignal(IIJ)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "now"    # J

    .line 3256
    return-void
.end method

.method public serviceReady(Z)V
    .locals 0
    .param p1, "ready"    # Z

    .line 3283
    return-void
.end method

.method public thawedByOther(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 3308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "receive other thaw request: uid["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3309
    .local v0, "reason":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1, p1, p2, v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$monReportPowerFrozenSignal(Lcom/android/server/am/AppStateManager;IILjava/lang/String;)V

    .line 3310
    return-void
.end method
