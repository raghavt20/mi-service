.class Lcom/android/server/am/MemoryControlCloud$1;
.super Landroid/database/ContentObserver;
.source "MemoryControlCloud.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MemoryControlCloud;->registerCloudEnableObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryControlCloud;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryControlCloud;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 39
    iput-object p1, p0, Lcom/android/server/am/MemoryControlCloud$1;->this$0:Lcom/android/server/am/MemoryControlCloud;

    iput-object p3, p0, Lcom/android/server/am/MemoryControlCloud$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 42
    const-string v0, "MemoryStandardProcessControl"

    if-eqz p2, :cond_2

    .line 43
    const-string v1, "cloud_memory_standard_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 44
    iget-object v2, p0, Lcom/android/server/am/MemoryControlCloud$1;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, -0x2

    invoke-static {v2, v1, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "str":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    .line 50
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/android/server/am/MemoryControlCloud$1;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v4}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/am/MemoryControlCloud$1;->val$context:Landroid/content/Context;

    .line 51
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v1, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v4, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    goto :goto_0

    .line 53
    :catch_0
    move-exception v1

    .line 54
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cloud CLOUD_MEMORY_STANDARD_ENABLE check failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-object v3, p0, Lcom/android/server/am/MemoryControlCloud$1;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v3}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v3

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    .line 57
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cloud control set received: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/am/MemoryControlCloud$1;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v3}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v3

    iget-boolean v3, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 47
    :cond_1
    :goto_1
    return-void

    .line 59
    .end local v2    # "str":Ljava/lang/String;
    :cond_2
    :goto_2
    return-void
.end method
