.class Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1;
.super Landroid/os/Handler;
.source "MiuiBoosterUtilsStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MiuiBoosterUtilsStubImpl;


# direct methods
.method constructor <init>(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MiuiBoosterUtilsStubImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 327
    iput-object p1, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1;->this$0:Lcom/android/server/am/MiuiBoosterUtilsStubImpl;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 330
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 331
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 337
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;

    .line 339
    .local v0, "info":Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;
    iget-object v1, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1;->this$0:Lcom/android/server/am/MiuiBoosterUtilsStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->-$$Nest$fgetuid(Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;)I

    move-result v2

    invoke-static {v0}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->-$$Nest$fgetreq_tids(Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;)[I

    move-result-object v3

    invoke-static {v0}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->-$$Nest$fgettimeout(Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;)I

    move-result v4

    invoke-static {v0}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;->-$$Nest$fgetlevel(Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->requestThreadPriority(I[III)I

    .line 341
    .end local v0    # "info":Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;
    goto :goto_0

    .line 333
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1;->this$0:Lcom/android/server/am/MiuiBoosterUtilsStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->registerCloudObserver(Landroid/content/Context;)V

    .line 334
    iget-object v0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1;->this$0:Lcom/android/server/am/MiuiBoosterUtilsStubImpl;

    invoke-virtual {v0}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->updateCloudControlParas()V

    .line 335
    nop

    .line 346
    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
