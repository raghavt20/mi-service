.class public Lcom/android/server/am/MutableActiveServicesStubImpl;
.super Ljava/lang/Object;
.source "MutableActiveServicesStubImpl.java"

# interfaces
.implements Lcom/android/server/am/MutableActiveServicesStub;


# instance fields
.field private mIsCallerSystem:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIsCallerSystem()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/android/server/am/MutableActiveServicesStubImpl;->mIsCallerSystem:Z

    return v0
.end method

.method public setIsCallerSystem()V
    .locals 1

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/MutableActiveServicesStubImpl;->mIsCallerSystem:Z

    .line 27
    return-void
.end method

.method public setIsCallerSystem(Lcom/android/server/am/ProcessRecord;)V
    .locals 2
    .param p1, "callerApp"    # Lcom/android/server/am/ProcessRecord;

    .line 21
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/am/MutableActiveServicesStubImpl;->mIsCallerSystem:Z

    .line 22
    return-void
.end method
