.class Lcom/android/server/am/MemoryFreezeCloud$1;
.super Landroid/database/ContentObserver;
.source "MemoryFreezeCloud.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MemoryFreezeCloud;->registerCloudWhiteList(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryFreezeCloud;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryFreezeCloud;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryFreezeCloud;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 63
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeCloud$1;->this$0:Lcom/android/server/am/MemoryFreezeCloud;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 66
    if-eqz p2, :cond_0

    const-string v0, "cloud_memory_freeze_whitelist"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeCloud$1;->this$0:Lcom/android/server/am/MemoryFreezeCloud;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryFreezeCloud;->updateCloudWhiteList()V

    .line 69
    :cond_0
    return-void
.end method
