public class com.android.server.am.BinderProxyMonitorImpl extends com.android.server.am.BinderProxyMonitor {
	 /* .source "BinderProxyMonitorImpl.java" */
	 /* # static fields */
	 private static final java.lang.String BINDER_ALLOC_PREFIX;
	 private static final java.lang.String DUMP_DIR;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final java.util.concurrent.atomic.AtomicBoolean mDumping;
	 private Integer mLastBpCount;
	 private Long mLastDumpTime;
	 private volatile Integer mTrackedUid;
	 /* # direct methods */
	 public static void $r8$lambda$3l9hYlmIIVtJclL9V8UzyD5FSvc ( com.android.server.am.BinderProxyMonitorImpl p0, com.android.server.am.ActivityManagerService p1, com.android.server.am.ProcessRecord p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/BinderProxyMonitorImpl;->lambda$trackProcBinderAllocations$3(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$IL2MxU39JQuCN7pQkrldNEmpEoA ( com.android.server.am.BinderProxyMonitorImpl p0, Integer p1, com.android.server.am.ActivityManagerService p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/BinderProxyMonitorImpl;->lambda$trackBinderAllocations$1(ILcom/android/server/am/ActivityManagerService;)V */
		 return;
	 } // .end method
	 public static void $r8$lambda$JpaoMyO0MTbbPAijfQID3HVQxCQ ( com.android.server.am.BinderProxyMonitorImpl p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ProcessRecord p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/BinderProxyMonitorImpl;->lambda$trackProcBinderAllocations$2(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;)V */
		 return;
	 } // .end method
	 public com.android.server.am.BinderProxyMonitorImpl ( ) {
		 /* .locals 3 */
		 /* .line 29 */
		 /* invoke-direct {p0}, Lcom/android/server/am/BinderProxyMonitor;-><init>()V */
		 /* .line 34 */
		 int v0 = -1; // const/4 v0, -0x1
		 /* iput v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
		 /* .line 35 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastBpCount:I */
		 /* .line 36 */
		 /* const-wide/16 v1, 0x0 */
		 /* iput-wide v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastDumpTime:J */
		 /* .line 37 */
		 /* new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean; */
		 /* invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
		 this.mDumping = v1;
		 return;
	 } // .end method
	 private void doDumpRemoteBinders ( com.android.server.am.ActivityManagerService p0, Integer p1, java.io.FileDescriptor p2, java.io.PrintWriter p3 ) {
		 /* .locals 4 */
		 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
		 /* .param p2, "targetUid" # I */
		 /* .param p3, "fd" # Ljava/io/FileDescriptor; */
		 /* .param p4, "pw" # Ljava/io/PrintWriter; */
		 /* .line 212 */
		 /* if-nez p3, :cond_0 */
		 /* .line 213 */
		 return;
		 /* .line 215 */
	 } // :cond_0
	 v0 = this.mProcLock;
	 /* monitor-enter v0 */
	 /* .line 216 */
	 try { // :try_start_0
		 v1 = this.mProcessList;
		 /* new-instance v2, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda3; */
		 /* invoke-direct {v2, p2, p3, p4}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda3;-><init>(ILjava/io/FileDescriptor;Ljava/io/PrintWriter;)V */
		 int v3 = 1; // const/4 v3, 0x1
		 (( com.android.server.am.ProcessList ) v1 ).forEachLruProcessesLOSP ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/android/server/am/ProcessList;->forEachLruProcessesLOSP(ZLjava/util/function/Consumer;)V
		 /* .line 240 */
		 /* monitor-exit v0 */
		 /* .line 241 */
		 return;
		 /* .line 240 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
	 } // .end method
	 private void dumpBinderProxies ( com.android.server.am.ActivityManagerService p0, java.io.FileDescriptor p1, java.io.PrintWriter p2, Boolean p3, Boolean p4 ) {
		 /* .locals 10 */
		 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
		 /* .param p2, "fd" # Ljava/io/FileDescriptor; */
		 /* .param p3, "pw" # Ljava/io/PrintWriter; */
		 /* .param p4, "persistToFile" # Z */
		 /* .param p5, "force" # Z */
		 /* .line 163 */
		 /* iget v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
		 /* .line 164 */
		 /* .local v0, "uid":I */
		 /* if-gez v0, :cond_0 */
		 /* .line 165 */
		 final String v1 = "Binder allocation tracker not enabled yet"; // const-string v1, "Binder allocation tracker not enabled yet"
		 (( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
		 /* .line 166 */
		 return;
		 /* .line 168 */
	 } // :cond_0
	 v1 = this.mDumping;
	 int v2 = 1; // const/4 v2, 0x1
	 int v3 = 0; // const/4 v3, 0x0
	 v1 = 	 (( java.util.concurrent.atomic.AtomicBoolean ) v1 ).compareAndExchange ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndExchange(ZZ)Z
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 169 */
		 final String v1 = "dumpBinderProxies() is already ongoing..."; // const-string v1, "dumpBinderProxies() is already ongoing..."
		 (( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
		 /* .line 170 */
		 return;
		 /* .line 172 */
	 } // :cond_1
	 android.os.SystemClock .uptimeMillis ( );
	 /* move-result-wide v1 */
	 /* .line 173 */
	 /* .local v1, "currentTime":J */
	 /* if-nez p5, :cond_2 */
	 /* iget-wide v4, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastDumpTime:J */
	 /* sub-long v4, v1, v4 */
	 /* const-wide/32 v6, 0xea60 */
	 /* cmp-long v4, v4, v6 */
	 /* if-gtz v4, :cond_2 */
	 /* .line 174 */
	 final String v4 = "dumpBinderProxies() skipped"; // const-string v4, "dumpBinderProxies() skipped"
	 (( java.io.PrintWriter ) p3 ).print ( v4 ); // invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
	 /* .line 175 */
	 v4 = this.mDumping;
	 (( java.util.concurrent.atomic.AtomicBoolean ) v4 ).set ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
	 /* .line 176 */
	 return;
	 /* .line 178 */
} // :cond_2
/* iput-wide v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastDumpTime:J */
/* .line 179 */
int v4 = 0; // const/4 v4, 0x0
/* .line 180 */
/* .local v4, "fos":Ljava/io/FileOutputStream; */
if ( p4 != null) { // if-eqz p4, :cond_3
	 final String v5 = "/data/miuilog/stability/resleak/binderproxy"; // const-string v5, "/data/miuilog/stability/resleak/binderproxy"
	 v6 = 	 miui.mqsas.scout.ScoutUtils .ensureDumpDir ( v5 );
	 if ( v6 != null) { // if-eqz v6, :cond_3
		 /* .line 181 */
		 final String v6 = ""; // const-string v6, ""
		 int v7 = 2; // const/4 v7, 0x2
		 miui.mqsas.scout.ScoutUtils .removeHistoricalDumps ( v5,v6,v7 );
		 /* .line 182 */
		 /* new-instance v6, Ljava/io/File; */
		 /* new-instance v7, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v8 = "binder_alloc_u"; // const-string v8, "binder_alloc_u"
		 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v8 = "_"; // const-string v8, "_"
		 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v7 ).append ( v1, v2 ); // invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* invoke-direct {v6, v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
		 /* move-object v5, v6 */
		 /* .line 183 */
		 /* .local v5, "persistFile":Ljava/io/File; */
		 /* new-instance v6, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v7 = "Dumping binder proxies to "; // const-string v7, "Dumping binder proxies to "
		 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.io.PrintWriter ) p3 ).println ( v6 ); // invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
		 /* .line 185 */
		 try { // :try_start_0
			 /* new-instance v6, Ljava/io/FileOutputStream; */
			 /* invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
			 /* move-object v4, v6 */
			 /* .line 186 */
			 (( java.io.FileOutputStream ) v4 ).getFD ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;
			 /* move-object p2, v6 */
			 /* .line 187 */
			 /* new-instance v6, Lcom/android/internal/util/FastPrintWriter; */
			 /* invoke-direct {v6, v4}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/OutputStream;)V */
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* move-object p3, v6 */
			 /* .line 192 */
			 /* .line 188 */
			 /* :catch_0 */
			 /* move-exception v6 */
			 /* .line 189 */
			 /* .local v6, "e":Ljava/lang/Exception; */
			 /* new-instance v7, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v8 = "Failed to open "; // const-string v8, "Failed to open "
			 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 final String v8 = ": "; // const-string v8, ": "
			 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.Exception ) v6 ).getMessage ( ); // invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
			 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 (( java.io.PrintWriter ) p3 ).println ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
			 /* .line 190 */
			 v7 = this.mDumping;
			 (( java.util.concurrent.atomic.AtomicBoolean ) v7 ).set ( v3 ); // invoke-virtual {v7, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
			 /* .line 191 */
			 return;
			 /* .line 194 */
		 } // .end local v5 # "persistFile":Ljava/io/File;
	 } // .end local v6 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_0
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v5 */
/* .line 196 */
/* .local v5, "iden":J */
try { // :try_start_1
(( com.android.server.am.ActivityManagerService ) p1 ).dumpBinderProxies ( p3, v3 ); // invoke-virtual {p1, p3, v3}, Lcom/android/server/am/ActivityManagerService;->dumpBinderProxies(Ljava/io/PrintWriter;I)V
/* .line 198 */
final String v7 = "\n\n"; // const-string v7, "\n\n"
(( java.io.PrintWriter ) p3 ).write ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
/* .line 199 */
(( java.io.PrintWriter ) p3 ).flush ( ); // invoke-virtual {p3}, Ljava/io/PrintWriter;->flush()V
/* .line 200 */
/* invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/server/am/BinderProxyMonitorImpl;->doDumpRemoteBinders(Lcom/android/server/am/ActivityManagerService;ILjava/io/FileDescriptor;Ljava/io/PrintWriter;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 204 */
} // :goto_1
android.os.Binder .restoreCallingIdentity ( v5,v6 );
/* .line 205 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 206 */
v7 = this.mDumping;
(( java.util.concurrent.atomic.AtomicBoolean ) v7 ).set ( v3 ); // invoke-virtual {v7, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 207 */
/* .line 204 */
/* :catchall_0 */
/* move-exception v7 */
/* .line 201 */
/* :catch_1 */
/* move-exception v7 */
/* .line 202 */
/* .local v7, "e":Ljava/lang/Exception; */
try { // :try_start_2
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Dump binder allocations failed"; // const-string v9, "Dump binder allocations failed"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v8 ); // invoke-virtual {p3, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
} // .end local v7 # "e":Ljava/lang/Exception;
/* .line 208 */
} // :goto_2
return;
/* .line 204 */
} // :goto_3
android.os.Binder .restoreCallingIdentity ( v5,v6 );
/* .line 205 */
libcore.io.IoUtils .closeQuietly ( v4 );
/* .line 206 */
v8 = this.mDumping;
(( java.util.concurrent.atomic.AtomicBoolean ) v8 ).set ( v3 ); // invoke-virtual {v8, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 207 */
/* throw v7 */
} // .end method
static void lambda$doDumpRemoteBinders$4 ( Integer p0, java.io.FileDescriptor p1, java.io.PrintWriter p2, com.android.server.am.ProcessRecord p3 ) { //synthethic
/* .locals 4 */
/* .param p0, "targetUid" # I */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 218 */
try { // :try_start_0
(( com.android.server.am.ProcessRecord ) p3 ).getThread ( ); // invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
/* .line 219 */
/* .local v0, "thread":Landroid/app/IApplicationThread; */
/* if-nez v0, :cond_0 */
/* .line 220 */
return;
/* .line 222 */
} // :cond_0
/* iget v1, p3, Lcom/android/server/am/ProcessRecord;->uid:I */
/* if-eq v1, p0, :cond_1 */
/* .line 223 */
return;
/* .line 225 */
} // :cond_1
/* new-instance v1, Lcom/android/internal/os/TransferPipe; */
/* invoke-direct {v1}, Lcom/android/internal/os/TransferPipe;-><init>()V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 227 */
/* .local v1, "tp":Lcom/android/internal/os/TransferPipe; */
try { // :try_start_1
(( com.android.server.am.ProcessRecord ) p3 ).getThread ( ); // invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
(( com.android.internal.os.TransferPipe ) v1 ).getWriteFd ( ); // invoke-virtual {v1}, Lcom/android/internal/os/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;
/* .line 228 */
/* const-wide/16 v2, 0x2710 */
(( com.android.internal.os.TransferPipe ) v1 ).go ( p1, v2, v3 ); // invoke-virtual {v1, p1, v2, v3}, Lcom/android/internal/os/TransferPipe;->go(Ljava/io/FileDescriptor;J)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 230 */
try { // :try_start_2
(( com.android.internal.os.TransferPipe ) v1 ).kill ( ); // invoke-virtual {v1}, Lcom/android/internal/os/TransferPipe;->kill()V
/* .line 231 */
/* nop */
/* .line 238 */
} // .end local v0 # "thread":Landroid/app/IApplicationThread;
} // .end local v1 # "tp":Lcom/android/internal/os/TransferPipe;
/* .line 230 */
/* .restart local v0 # "thread":Landroid/app/IApplicationThread; */
/* .restart local v1 # "tp":Lcom/android/internal/os/TransferPipe; */
/* :catchall_0 */
/* move-exception v2 */
(( com.android.internal.os.TransferPipe ) v1 ).kill ( ); // invoke-virtual {v1}, Lcom/android/internal/os/TransferPipe;->kill()V
/* .line 231 */
/* nop */
} // .end local p0 # "targetUid":I
} // .end local p1 # "fd":Ljava/io/FileDescriptor;
} // .end local p2 # "pw":Ljava/io/PrintWriter;
} // .end local p3 # "proc":Lcom/android/server/am/ProcessRecord;
/* throw v2 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 232 */
} // .end local v0 # "thread":Landroid/app/IApplicationThread;
} // .end local v1 # "tp":Lcom/android/internal/os/TransferPipe;
/* .restart local p0 # "targetUid":I */
/* .restart local p1 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p2 # "pw":Ljava/io/PrintWriter; */
/* .restart local p3 # "proc":Lcom/android/server/am/ProcessRecord; */
/* :catch_0 */
/* move-exception v0 */
/* .line 233 */
/* .local v0, "e":Ljava/lang/Exception; */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 234 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failure while dumping binder traces from "; // const-string v2, "Failure while dumping binder traces from "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ".Exception: "; // const-string v2, ".Exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 236 */
(( java.io.PrintWriter ) p2 ).flush ( ); // invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V
/* .line 239 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_0
return;
} // .end method
static void lambda$trackBinderAllocations$0 ( Integer p0, Integer p1, com.android.server.am.ProcessRecord p2 ) { //synthethic
/* .locals 6 */
/* .param p0, "prevTrackedUid" # I */
/* .param p1, "targetUid" # I */
/* .param p2, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 70 */
final String v0 = ".Exception: "; // const-string v0, ".Exception: "
final String v1 = "Failed to enable binder allocation tracking in "; // const-string v1, "Failed to enable binder allocation tracking in "
final String v2 = "BinderProxyMonitor"; // const-string v2, "BinderProxyMonitor"
try { // :try_start_0
(( com.android.server.am.ProcessRecord ) p2 ).getThread ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
/* .line 71 */
/* .local v3, "thread":Landroid/app/IApplicationThread; */
/* if-nez v3, :cond_0 */
/* .line 72 */
return;
/* .line 74 */
} // :cond_0
int v4 = -1; // const/4 v4, -0x1
/* if-eq p0, v4, :cond_1 */
/* iget v5, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* if-ne v5, p0, :cond_1 */
/* .line 75 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Disable binder tracker on process "; // const-string v5, "Disable binder tracker on process "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = (( com.android.server.am.ProcessRecord ) p2 ).getPid ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v4 );
/* .line 76 */
(( com.android.server.am.ProcessRecord ) p2 ).getThread ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
int v5 = 0; // const/4 v5, 0x0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 77 */
return;
/* .line 83 */
} // .end local v3 # "thread":Landroid/app/IApplicationThread;
} // :cond_1
/* nop */
/* .line 85 */
/* if-eq p1, v4, :cond_2 */
try { // :try_start_1
/* iget v3, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* if-ne v3, p1, :cond_2 */
/* .line 86 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Enable binder tracker on process "; // const-string v4, "Enable binder tracker on process "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.android.server.am.ProcessRecord ) p2 ).getPid ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 87 */
(( com.android.server.am.ProcessRecord ) p2 ).getThread ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
int v4 = 1; // const/4 v4, 0x1
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 89 */
/* :catch_0 */
/* move-exception v3 */
/* .line 90 */
/* .local v3, "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v0 );
/* .line 92 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_0
/* nop */
/* .line 93 */
} // :goto_1
return;
/* .line 79 */
/* :catch_1 */
/* move-exception v3 */
/* .line 80 */
/* .restart local v3 # "e":Ljava/lang/Exception; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v0 );
/* .line 82 */
return;
} // .end method
private void lambda$trackBinderAllocations$1 ( Integer p0, com.android.server.am.ActivityManagerService p1 ) { //synthethic
/* .locals 8 */
/* .param p1, "targetUid" # I */
/* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .line 45 */
v0 = android.os.BinderProxy .getProxyCount ( );
/* .line 46 */
/* .local v0, "currBpCount":I */
/* iget v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
/* if-ne p1, v1, :cond_2 */
/* .line 47 */
/* if-lez p1, :cond_1 */
/* iget v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastBpCount:I */
/* sub-int v1, v0, v1 */
/* .line 48 */
/* const/16 v2, 0x3e8 */
/* if-ne p1, v2, :cond_0 */
/* const/16 v2, 0x1770 */
} // :cond_0
/* const/16 v2, 0xbb8 */
} // :goto_0
/* if-lt v1, v2, :cond_1 */
/* .line 49 */
/* iput v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastBpCount:I */
/* .line 50 */
final String v1 = "BinderProxyMonitor"; // const-string v1, "BinderProxyMonitor"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Number of binder proxies sent from uid "; // const-string v3, "Number of binder proxies sent from uid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " reached "; // const-string v3, " reached "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", dump automatically"; // const-string v3, ", dump automatically"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 52 */
/* new-instance v1, Landroid/util/LogPrinter; */
final String v2 = "BinderProxyMonitor"; // const-string v2, "BinderProxyMonitor"
int v3 = 3; // const/4 v3, 0x3
int v4 = 4; // const/4 v4, 0x4
/* invoke-direct {v1, v4, v2, v3}, Landroid/util/LogPrinter;-><init>(ILjava/lang/String;I)V */
/* .line 53 */
/* .local v1, "printer":Landroid/util/LogPrinter; */
try { // :try_start_0
/* new-instance v5, Lcom/android/internal/util/FastPrintWriter; */
/* invoke-direct {v5, v1}, Lcom/android/internal/util/FastPrintWriter;-><init>(Landroid/util/Printer;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 54 */
/* .local v5, "pw":Lcom/android/internal/util/FastPrintWriter; */
try { // :try_start_1
v4 = java.io.FileDescriptor.out;
int v6 = 1; // const/4 v6, 0x1
int v7 = 0; // const/4 v7, 0x0
/* move-object v2, p0 */
/* move-object v3, p2 */
/* invoke-direct/range {v2 ..v7}, Lcom/android/server/am/BinderProxyMonitorImpl;->dumpBinderProxies(Lcom/android/server/am/ActivityManagerService;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;ZZ)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 55 */
try { // :try_start_2
(( com.android.internal.util.FastPrintWriter ) v5 ).close ( ); // invoke-virtual {v5}, Lcom/android/internal/util/FastPrintWriter;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 57 */
} // .end local v5 # "pw":Lcom/android/internal/util/FastPrintWriter;
/* .line 53 */
/* .restart local v5 # "pw":Lcom/android/internal/util/FastPrintWriter; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( com.android.internal.util.FastPrintWriter ) v5 ).close ( ); // invoke-virtual {v5}, Lcom/android/internal/util/FastPrintWriter;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "currBpCount":I
} // .end local v1 # "printer":Landroid/util/LogPrinter;
} // .end local p0 # "this":Lcom/android/server/am/BinderProxyMonitorImpl;
} // .end local p1 # "targetUid":I
} // .end local p2 # "ams":Lcom/android/server/am/ActivityManagerService;
} // :goto_1
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 55 */
} // .end local v5 # "pw":Lcom/android/internal/util/FastPrintWriter;
/* .restart local v0 # "currBpCount":I */
/* .restart local v1 # "printer":Landroid/util/LogPrinter; */
/* .restart local p0 # "this":Lcom/android/server/am/BinderProxyMonitorImpl; */
/* .restart local p1 # "targetUid":I */
/* .restart local p2 # "ams":Lcom/android/server/am/ActivityManagerService; */
/* :catch_0 */
/* move-exception v2 */
/* .line 56 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "BinderProxyMonitor"; // const-string v3, "BinderProxyMonitor"
final String v4 = "Failed to dump binder allocation records"; // const-string v4, "Failed to dump binder allocation records"
android.util.Slog .w ( v3,v4,v2 );
/* .line 59 */
} // .end local v1 # "printer":Landroid/util/LogPrinter;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_2
return;
/* .line 61 */
} // :cond_2
int v1 = -1; // const/4 v1, -0x1
/* if-eq p1, v1, :cond_3 */
/* .line 62 */
final String v1 = "BinderProxyMonitor"; // const-string v1, "BinderProxyMonitor"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Enable binder tracker on uid "; // const-string v3, "Enable binder tracker on uid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 64 */
} // :cond_3
/* iput v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastBpCount:I */
/* .line 65 */
/* iget v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
/* .line 66 */
/* .local v1, "prevTrackedUid":I */
/* iput p1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
/* .line 67 */
v2 = this.mProcLock;
/* monitor-enter v2 */
/* .line 68 */
try { // :try_start_5
v3 = this.mProcessList;
/* new-instance v4, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v4, v1, p1}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda0;-><init>(II)V */
int v5 = 1; // const/4 v5, 0x1
(( com.android.server.am.ProcessList ) v3 ).forEachLruProcessesLOSP ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/am/ProcessList;->forEachLruProcessesLOSP(ZLjava/util/function/Consumer;)V
/* .line 94 */
/* monitor-exit v2 */
/* .line 95 */
return;
/* .line 94 */
/* :catchall_2 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* throw v3 */
} // .end method
private void lambda$trackProcBinderAllocations$2 ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "targetProc" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 111 */
/* if-eq p2, p1, :cond_0 */
/* .line 112 */
return;
/* .line 116 */
} // :cond_0
try { // :try_start_0
/* iget v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_1 */
/* iget v0, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
/* if-ne v0, v1, :cond_1 */
/* .line 117 */
(( com.android.server.am.ProcessRecord ) p2 ).getThread ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
int v1 = 1; // const/4 v1, 0x1
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 122 */
} // :cond_1
/* .line 119 */
/* :catch_0 */
/* move-exception v0 */
/* .line 120 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to enable binder allocation tracking in "; // const-string v2, "Failed to enable binder allocation tracking in "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ".Exception: "; // const-string v2, ".Exception: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BinderProxyMonitor"; // const-string v2, "BinderProxyMonitor"
android.util.Slog .w ( v2,v1 );
/* .line 123 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void lambda$trackProcBinderAllocations$3 ( com.android.server.am.ActivityManagerService p0, com.android.server.am.ProcessRecord p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "targetProc" # Lcom/android/server/am/ProcessRecord; */
/* .line 109 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 110 */
try { // :try_start_0
v1 = this.mProcessList;
/* new-instance v2, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0, p2}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/am/BinderProxyMonitorImpl;Lcom/android/server/am/ProcessRecord;)V */
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.am.ProcessList ) v1 ).forEachLruProcessesLOSP ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/android/server/am/ProcessList;->forEachLruProcessesLOSP(ZLjava/util/function/Consumer;)V
/* .line 124 */
/* monitor-exit v0 */
/* .line 125 */
return;
/* .line 124 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public Boolean handleDumpBinderProxies ( com.android.server.am.ActivityManagerService p0, java.io.FileDescriptor p1, java.io.PrintWriter p2, java.lang.String[] p3, Integer p4 ) {
/* .locals 14 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "fd" # Ljava/io/FileDescriptor; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "opti" # I */
/* .line 131 */
/* move-object/from16 v0, p4 */
android.os.BinderStub .get ( );
v1 = (( android.os.BinderStub ) v1 ).isEnabled ( ); // invoke-virtual {v1}, Landroid/os/BinderStub;->isEnabled()Z
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 132 */
/* .line 134 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 135 */
/* .local v1, "dumpDetailToFile":Z */
int v3 = 0; // const/4 v3, 0x0
/* .line 136 */
/* .local v3, "dumpDetail":Z */
/* move/from16 v4, p5 */
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v0 */
int v11 = 1; // const/4 v11, 0x1
/* if-ge v4, v5, :cond_5 */
/* .line 137 */
final String v5 = "--dump-details-to-file"; // const-string v5, "--dump-details-to-file"
/* aget-object v6, v0, p5 */
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 138 */
int v1 = 1; // const/4 v1, 0x1
/* .line 139 */
/* move-object v12, p0 */
/* move-object v13, p1 */
/* .line 141 */
} // :cond_1
final String v5 = "--dump-details"; // const-string v5, "--dump-details"
/* aget-object v6, v0, p5 */
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 142 */
int v3 = 1; // const/4 v3, 0x1
/* .line 143 */
/* move-object v12, p0 */
/* move-object v13, p1 */
/* .line 145 */
} // :cond_2
final String v5 = "--track-uid"; // const-string v5, "--track-uid"
/* aget-object v6, v0, p5 */
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 146 */
/* add-int/lit8 v2, p5, 0x1 */
/* array-length v5, v0 */
/* if-ge v2, v5, :cond_3 */
/* .line 149 */
/* add-int/lit8 v2, p5, 0x1 */
/* aget-object v2, v0, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
/* .line 150 */
/* .local v2, "uid":I */
/* move-object v12, p0 */
/* move-object v13, p1 */
(( com.android.server.am.BinderProxyMonitorImpl ) p0 ).trackBinderAllocations ( p1, v2 ); // invoke-virtual {p0, p1, v2}, Lcom/android/server/am/BinderProxyMonitorImpl;->trackBinderAllocations(Lcom/android/server/am/ActivityManagerService;I)V
/* .line 151 */
/* .line 147 */
} // .end local v2 # "uid":I
} // :cond_3
/* move-object v12, p0 */
/* move-object v13, p1 */
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
final String v5 = "--trackUid should be followed by a uid"; // const-string v5, "--trackUid should be followed by a uid"
/* invoke-direct {v2, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 145 */
} // :cond_4
/* move-object v12, p0 */
/* move-object v13, p1 */
/* .line 136 */
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
} // :cond_5
/* move-object v12, p0 */
/* move-object v13, p1 */
/* .line 154 */
} // .end local v4 # "i":I
/* if-nez v3, :cond_7 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 158 */
} // :cond_6
/* .line 155 */
} // :cond_7
} // :goto_2
int v10 = 1; // const/4 v10, 0x1
/* move-object v5, p0 */
/* move-object v6, p1 */
/* move-object/from16 v7, p2 */
/* move-object/from16 v8, p3 */
/* move v9, v1 */
/* invoke-direct/range {v5 ..v10}, Lcom/android/server/am/BinderProxyMonitorImpl;->dumpBinderProxies(Lcom/android/server/am/ActivityManagerService;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;ZZ)V */
/* .line 156 */
} // .end method
public void trackBinderAllocations ( com.android.server.am.ActivityManagerService p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "targetUid" # I */
/* .line 41 */
android.os.BinderStub .get ( );
v0 = (( android.os.BinderStub ) v0 ).isEnabled ( ); // invoke-virtual {v0}, Landroid/os/BinderStub;->isEnabled()Z
/* if-nez v0, :cond_0 */
/* .line 42 */
return;
/* .line 44 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/am/BinderProxyMonitorImpl;ILcom/android/server/am/ActivityManagerService;)V */
(( com.android.server.am.ActivityManagerService$MainHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z
/* .line 96 */
return;
} // .end method
public void trackProcBinderAllocations ( com.android.server.am.ActivityManagerService p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 3 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "targetProc" # Lcom/android/server/am/ProcessRecord; */
/* .line 100 */
android.os.BinderStub .get ( );
v0 = (( android.os.BinderStub ) v0 ).isEnabled ( ); // invoke-virtual {v0}, Landroid/os/BinderStub;->isEnabled()Z
/* if-nez v0, :cond_0 */
/* .line 101 */
return;
/* .line 103 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I */
/* .line 104 */
/* .local v0, "currentTrackedUid":I */
/* if-ltz v0, :cond_2 */
/* iget v1, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* if-eq v1, v0, :cond_1 */
/* .line 107 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Enable binder tracker on new process "; // const-string v2, "Enable binder tracker on new process "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.android.server.am.ProcessRecord ) p2 ).getPid ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "BinderProxyMonitor"; // const-string v2, "BinderProxyMonitor"
android.util.Slog .i ( v2,v1 );
/* .line 108 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda4; */
/* invoke-direct {v2, p0, p1, p2}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/am/BinderProxyMonitorImpl;Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;)V */
(( com.android.server.am.ActivityManagerService$MainHandler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ActivityManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z
/* .line 126 */
return;
/* .line 105 */
} // :cond_2
} // :goto_0
return;
} // .end method
