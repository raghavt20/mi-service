public class com.android.server.am.GameMemoryReclaimer {
	 /* .source "GameMemoryReclaimer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/GameMemoryReclaimer$UidPss; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String TAG;
/* # instance fields */
public com.android.server.am.ActivityManagerService mActivityManagerService;
private java.util.Map mAllPackageInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lcom/android/server/am/IGameProcessAction;", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/GameProcessKiller$PackageMemInfo;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mAllProcessInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lcom/android/server/am/IGameProcessAction;", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mCompactInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.Context mContext;
private java.lang.String mCurrentGame;
private final android.os.Handler mHandler;
private java.util.List mProcessActions;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/IGameProcessAction;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.ServiceThread mServiceThread;
/* # direct methods */
public static void $r8$lambda$CrEMA0V0cBHVFedy-6WqzzUQllo ( com.android.server.am.GameMemoryReclaimer p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/GameMemoryReclaimer;->lambda$notifyProcessDied$2(I)V */
return;
} // .end method
public static void $r8$lambda$M8Z9ZozU6ffpGf6QBCyFreoumIo ( com.android.server.am.GameMemoryReclaimer p0, Long p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/GameMemoryReclaimer;->lambda$reclaimBackground$1(J)V */
return;
} // .end method
static java.util.Map -$$Nest$fgetmAllProcessInfos ( com.android.server.am.GameMemoryReclaimer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAllProcessInfos;
} // .end method
static java.util.Map -$$Nest$fgetmCompactInfos ( com.android.server.am.GameMemoryReclaimer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCompactInfos;
} // .end method
static java.util.List -$$Nest$fgetmProcessActions ( com.android.server.am.GameMemoryReclaimer p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessActions;
} // .end method
public com.android.server.am.GameMemoryReclaimer ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .line 54 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 47 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAllProcessInfos = v0;
/* .line 48 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAllPackageInfos = v0;
/* .line 50 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCompactInfos = v0;
/* .line 52 */
int v0 = 0; // const/4 v0, 0x0
this.mCurrentGame = v0;
/* .line 55 */
this.mContext = p1;
/* .line 57 */
this.mActivityManagerService = p2;
/* .line 58 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mProcessActions = v0;
/* .line 60 */
/* new-instance v0, Lcom/android/server/ServiceThread; */
final String v1 = "GameMemoryReclaimer"; // const-string v1, "GameMemoryReclaimer"
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v0, v1, v2, v2}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
this.mServiceThread = v0;
/* .line 61 */
(( com.android.server.ServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V
/* .line 62 */
/* new-instance v1, Landroid/os/Handler; */
(( com.android.server.ServiceThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 63 */
return;
} // .end method
private void filterAllPackageInfos ( ) {
/* .locals 17 */
/* .line 148 */
/* move-object/from16 v1, p0 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 149 */
/* .local v2, "time":J */
v0 = this.mAllPackageInfos;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Ljava/util/List; */
/* .line 150 */
/* .local v4, "v":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessKiller$PackageMemInfo;>;" */
} // .end local v4 # "v":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessKiller$PackageMemInfo;>;"
/* .line 151 */
} // :cond_0
v4 = this.mActivityManagerService;
/* monitor-enter v4 */
/* .line 152 */
try { // :try_start_0
v0 = this.mActivityManagerService;
v0 = this.mProcessList;
v0 = this.mActiveUids;
/* .line 153 */
/* .local v0, "uids":Lcom/android/server/am/ActiveUids; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_1
v6 = (( com.android.server.am.ActiveUids ) v0 ).size ( ); // invoke-virtual {v0}, Lcom/android/server/am/ActiveUids;->size()I
/* if-ge v5, v6, :cond_8 */
/* .line 154 */
v6 = (( com.android.server.am.ActiveUids ) v0 ).keyAt ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/am/ActiveUids;->keyAt(I)I
/* .line 155 */
/* .local v6, "uid":I */
(( com.android.server.am.ActiveUids ) v0 ).valueAt ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/am/ActiveUids;->valueAt(I)Lcom/android/server/am/UidRecord;
/* .line 156 */
/* .local v7, "uidRecord":Lcom/android/server/am/UidRecord; */
v8 = (( com.android.server.am.UidRecord ) v7 ).getCurProcState ( ); // invoke-virtual {v7}, Lcom/android/server/am/UidRecord;->getCurProcState()I
/* .line 157 */
/* .local v8, "uidState":I */
int v9 = 0; // const/4 v9, 0x0
/* .line 158 */
/* .local v9, "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
v10 = this.mProcessActions;
v11 = } // :goto_2
if ( v11 != null) { // if-eqz v11, :cond_7
/* check-cast v11, Lcom/android/server/am/IGameProcessAction; */
/* .line 159 */
/* .local v11, "action":Lcom/android/server/am/IGameProcessAction; */
/* instance-of v12, v11, Lcom/android/server/am/GameProcessKiller; */
/* if-nez v12, :cond_1 */
/* .line 160 */
/* .line 161 */
} // :cond_1
/* move-object v12, v11 */
/* check-cast v12, Lcom/android/server/am/GameProcessKiller; */
/* .line 162 */
/* .local v12, "killer":Lcom/android/server/am/GameProcessKiller; */
v13 = (( com.android.server.am.GameProcessKiller ) v12 ).getMinProcState ( ); // invoke-virtual {v12}, Lcom/android/server/am/GameProcessKiller;->getMinProcState()I
/* if-ge v8, v13, :cond_2 */
/* .line 163 */
/* .line 164 */
} // :cond_2
v13 = (( com.android.server.am.GameProcessKiller ) v12 ).shouldSkip ( v6 ); // invoke-virtual {v12, v6}, Lcom/android/server/am/GameProcessKiller;->shouldSkip(I)Z
if ( v13 != null) { // if-eqz v13, :cond_3
/* .line 165 */
/* .line 168 */
} // :cond_3
/* new-instance v13, Lcom/android/server/am/GameMemoryReclaimer$UidPss; */
int v14 = 0; // const/4 v14, 0x0
/* invoke-direct {v13, v1, v14}, Lcom/android/server/am/GameMemoryReclaimer$UidPss;-><init>(Lcom/android/server/am/GameMemoryReclaimer;Lcom/android/server/am/GameMemoryReclaimer$UidPss-IA;)V */
/* .line 169 */
/* .local v13, "uidPss":Lcom/android/server/am/GameMemoryReclaimer$UidPss; */
/* new-instance v14, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda1; */
/* invoke-direct {v14, v13, v12}, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/am/GameMemoryReclaimer$UidPss;Lcom/android/server/am/GameProcessKiller;)V */
(( com.android.server.am.UidRecord ) v7 ).forEachProcess ( v14 ); // invoke-virtual {v7, v14}, Lcom/android/server/am/UidRecord;->forEachProcess(Ljava/util/function/Consumer;)V
/* .line 174 */
/* iget-boolean v14, v13, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->skip:Z */
if ( v14 != null) { // if-eqz v14, :cond_4
/* .line 175 */
/* .line 176 */
} // :cond_4
/* if-nez v9, :cond_5 */
/* .line 177 */
/* new-instance v14, Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* move-object/from16 v16, v9 */
/* move-object v15, v10 */
} // .end local v9 # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
/* .local v16, "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* iget-wide v9, v13, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->pss:J */
/* invoke-direct {v14, v6, v9, v10, v8}, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;-><init>(IJI)V */
/* move-object v9, v14 */
} // .end local v16 # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
/* .restart local v9 # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* .line 176 */
} // :cond_5
/* move-object/from16 v16, v9 */
/* move-object v15, v10 */
/* .line 178 */
} // :goto_3
v10 = v10 = this.mAllPackageInfos;
if ( v10 != null) { // if-eqz v10, :cond_6
/* .line 179 */
v10 = this.mAllPackageInfos;
/* check-cast v10, Ljava/util/List; */
/* .line 181 */
} // .end local v11 # "action":Lcom/android/server/am/IGameProcessAction;
} // .end local v12 # "killer":Lcom/android/server/am/GameProcessKiller;
} // .end local v13 # "uidPss":Lcom/android/server/am/GameMemoryReclaimer$UidPss;
} // :cond_6
/* move-object v10, v15 */
/* .line 153 */
} // :cond_7
/* move-object/from16 v16, v9 */
} // .end local v9 # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
/* .restart local v16 # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 183 */
} // .end local v5 # "i":I
} // .end local v6 # "uid":I
} // .end local v7 # "uidRecord":Lcom/android/server/am/UidRecord;
} // .end local v8 # "uidState":I
} // .end local v16 # "meminfo":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
} // :cond_8
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 184 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* sub-long/2addr v4, v2 */
/* .line 185 */
} // .end local v2 # "time":J
/* .local v4, "time":J */
final String v2 = "GameMemoryReclaimer"; // const-string v2, "GameMemoryReclaimer"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "spent " */
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v6 = "ms to filter all packages..."; // const-string v6, "ms to filter all packages..."
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 186 */
return;
/* .line 183 */
} // .end local v0 # "uids":Lcom/android/server/am/ActiveUids;
} // .end local v4 # "time":J
/* .restart local v2 # "time":J */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
private void filterAllProcessInfos ( ) {
/* .locals 4 */
/* .line 107 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 108 */
/* .local v0, "time":J */
v2 = this.mAllProcessInfos;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/util/List; */
/* .line 109 */
/* .local v3, "v":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;>;" */
} // .end local v3 # "v":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;>;"
/* .line 110 */
} // :cond_0
/* new-instance v2, Lcom/android/server/am/GameMemoryReclaimer$1; */
/* invoke-direct {v2, p0}, Lcom/android/server/am/GameMemoryReclaimer$1;-><init>(Lcom/android/server/am/GameMemoryReclaimer;)V */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {p0, v2, v3}, Lcom/android/server/am/GameMemoryReclaimer;->getMatchedProcessList(Ljava/lang/Comparable;Ljava/util/List;)Ljava/util/List; */
/* .line 136 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* sub-long/2addr v2, v0 */
/* .line 137 */
} // .end local v0 # "time":J
/* .local v2, "time":J */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "spent " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "ms to filter all processes("; // const-string v1, "ms to filter all processes("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = v1 = this.mCompactInfos;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GameMemoryReclaimer"; // const-string v1, "GameMemoryReclaimer"
android.util.Slog .i ( v1,v0 );
/* .line 138 */
return;
} // .end method
private java.util.List getMatchedProcessList ( java.lang.Comparable p0, java.util.List p1 ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Comparable<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 251 */
/* .local p1, "condition":Ljava/lang/Comparable;, "Ljava/lang/Comparable<Lcom/android/server/am/ProcessRecord;>;" */
/* .local p2, "whitelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 252 */
/* .local v0, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
v1 = this.mActivityManagerService;
/* monitor-enter v1 */
/* .line 253 */
try { // :try_start_0
v2 = this.mActivityManagerService;
v2 = this.mProcessList;
(( com.android.server.am.ProcessList ) v2 ).getProcessNamesLOSP ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
(( com.android.server.am.ProcessList$MyProcessMap ) v2 ).getMap ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;
v2 = (( android.util.ArrayMap ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I
/* .line 254 */
/* .local v2, "NP":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "ip":I */
} // :goto_0
/* if-ge v3, v2, :cond_4 */
/* .line 255 */
v4 = this.mActivityManagerService;
v4 = this.mProcessList;
/* .line 256 */
(( com.android.server.am.ProcessList ) v4 ).getProcessNamesLOSP ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;
(( com.android.server.am.ProcessList$MyProcessMap ) v4 ).getMap ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessList$MyProcessMap;->getMap()Landroid/util/ArrayMap;
(( android.util.ArrayMap ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Landroid/util/SparseArray; */
/* .line 257 */
/* .local v4, "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;" */
v5 = (( android.util.SparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/SparseArray;->size()I
/* .line 258 */
/* .local v5, "NA":I */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "ia":I */
} // :goto_1
/* if-ge v6, v5, :cond_3 */
/* .line 259 */
(( android.util.SparseArray ) v4 ).valueAt ( v6 ); // invoke-virtual {v4, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v7, Lcom/android/server/am/ProcessRecord; */
/* .line 260 */
/* .local v7, "app":Lcom/android/server/am/ProcessRecord; */
v8 = (( com.android.server.am.ProcessRecord ) v7 ).isPersistent ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z
/* if-nez v8, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_0
v8 = v8 = this.processName;
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 261 */
/* .line 263 */
} // :cond_0
v8 = (( com.android.server.am.ProcessRecord ) v7 ).isRemoved ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessRecord;->isRemoved()Z
v8 = /* if-nez v8, :cond_1 */
/* if-nez v8, :cond_2 */
/* .line 264 */
} // :cond_1
(( java.util.ArrayList ) v0 ).add ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 258 */
} // .end local v7 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_2
} // :goto_2
/* add-int/lit8 v6, v6, 0x1 */
/* .line 254 */
} // .end local v4 # "apps":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v5 # "NA":I
} // .end local v6 # "ia":I
} // :cond_3
/* add-int/lit8 v3, v3, 0x1 */
/* .line 268 */
} // .end local v2 # "NP":I
} // .end local v3 # "ip":I
} // :cond_4
/* monitor-exit v1 */
/* .line 269 */
/* .line 268 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
static void lambda$filterAllPackageInfos$0 ( com.android.server.am.GameMemoryReclaimer$UidPss p0, com.android.server.am.GameProcessKiller p1, com.android.server.am.ProcessRecord p2 ) { //synthethic
/* .locals 4 */
/* .param p0, "uidPss" # Lcom/android/server/am/GameMemoryReclaimer$UidPss; */
/* .param p1, "killer" # Lcom/android/server/am/GameProcessKiller; */
/* .param p2, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 170 */
/* iget-boolean v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->skip:Z */
v1 = (( com.android.server.am.GameProcessKiller ) p1 ).shouldSkip ( p2 ); // invoke-virtual {p1, p2}, Lcom/android/server/am/GameProcessKiller;->shouldSkip(Lcom/android/server/am/ProcessRecord;)Z
/* or-int/2addr v0, v1 */
/* iput-boolean v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->skip:Z */
/* .line 171 */
/* iget-boolean v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->skip:Z */
/* if-nez v0, :cond_0 */
/* .line 172 */
/* iget-wide v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->pss:J */
v2 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v2 ).getLastPss ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v2 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/am/GameMemoryReclaimer$UidPss;->pss:J */
/* .line 173 */
} // :cond_0
return;
} // .end method
private void lambda$notifyProcessDied$2 ( Integer p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 274 */
v0 = this.mCompactInfos;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 275 */
v0 = this.mCompactInfos;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo; */
(( com.android.server.am.GameProcessCompactor$ProcessCompactInfo ) v0 ).notifyDied ( ); // invoke-virtual {v0}, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->notifyDied()V
/* .line 276 */
} // :cond_0
return;
} // .end method
private void lambda$reclaimBackground$1 ( Long p0 ) { //synthethic
/* .locals 10 */
/* .param p1, "need" # J */
/* .line 190 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reclaimBackground: "; // const-string v1, "reclaimBackground: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/32 v1, 0x80000 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 191 */
/* const-wide/16 v3, 0x0 */
/* .line 192 */
/* .local v3, "reclaim":J */
/* move-wide v5, p1 */
/* .line 194 */
/* .local v5, "mem":J */
v0 = this.mProcessActions;
/* monitor-enter v0 */
/* .line 195 */
try { // :try_start_0
v7 = v7 = this.mAllProcessInfos;
/* if-lez v7, :cond_0 */
/* .line 196 */
final String v7 = "filterAllProcessInfos"; // const-string v7, "filterAllProcessInfos"
android.os.Trace .traceBegin ( v1,v2,v7 );
/* .line 197 */
/* invoke-direct {p0}, Lcom/android/server/am/GameMemoryReclaimer;->filterAllProcessInfos()V */
/* .line 198 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 200 */
} // :cond_0
v7 = v7 = this.mAllPackageInfos;
/* if-lez v7, :cond_1 */
/* .line 201 */
final String v7 = "filterAllPackageInfos"; // const-string v7, "filterAllPackageInfos"
android.os.Trace .traceBegin ( v1,v2,v7 );
/* .line 202 */
/* invoke-direct {p0}, Lcom/android/server/am/GameMemoryReclaimer;->filterAllPackageInfos()V */
/* .line 203 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 206 */
} // :cond_1
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_0
v8 = v8 = this.mProcessActions;
/* if-ge v7, v8, :cond_3 */
/* .line 207 */
v8 = this.mProcessActions;
/* check-cast v8, Lcom/android/server/am/IGameProcessAction; */
/* move-result-wide v8 */
/* move-wide v3, v8 */
/* .line 208 */
/* cmp-long v8, v3, v5 */
/* if-ltz v8, :cond_2 */
/* .line 209 */
/* .line 210 */
} // :cond_2
/* sub-long/2addr v5, v3 */
/* .line 206 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 212 */
} // .end local v7 # "i":I
} // :cond_3
} // :goto_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 213 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 214 */
return;
/* .line 212 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
/* # virtual methods */
public void addGameProcessCompactor ( com.android.server.am.IGameProcessAction$IGameProcessActionConfig p0 ) {
/* .locals 4 */
/* .param p1, "cfg" # Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig; */
/* .line 95 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 96 */
/* new-instance v0, Lcom/android/server/am/GameProcessCompactor; */
/* move-object v1, p1 */
/* check-cast v1, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/GameProcessCompactor;-><init>(Lcom/android/server/am/GameMemoryReclaimer;Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;)V */
/* .line 97 */
/* .local v0, "compactor":Lcom/android/server/am/GameProcessCompactor; */
v1 = this.mProcessActions;
/* monitor-enter v1 */
/* .line 98 */
try { // :try_start_0
v2 = this.mProcessActions;
/* .line 99 */
v2 = this.mAllProcessInfos;
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 100 */
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 102 */
} // .end local v0 # "compactor":Lcom/android/server/am/GameProcessCompactor;
} // :cond_0
} // :goto_0
return;
} // .end method
public void addGameProcessKiller ( com.android.server.am.IGameProcessAction$IGameProcessActionConfig p0 ) {
/* .locals 4 */
/* .param p1, "cfg" # Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig; */
/* .line 85 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 86 */
/* new-instance v0, Lcom/android/server/am/GameProcessKiller; */
/* move-object v1, p1 */
/* check-cast v1, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/GameProcessKiller;-><init>(Lcom/android/server/am/GameMemoryReclaimer;Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;)V */
/* .line 87 */
/* .local v0, "killer":Lcom/android/server/am/GameProcessKiller; */
v1 = this.mProcessActions;
/* monitor-enter v1 */
/* .line 88 */
try { // :try_start_0
v2 = this.mProcessActions;
/* .line 89 */
v2 = this.mAllPackageInfos;
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 90 */
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 92 */
} // .end local v0 # "killer":Lcom/android/server/am/GameProcessKiller;
} // :cond_0
} // :goto_0
return;
} // .end method
java.util.List filterPackageInfos ( com.android.server.am.IGameProcessAction p0 ) {
/* .locals 2 */
/* .param p1, "killer" # Lcom/android/server/am/IGameProcessAction; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/IGameProcessAction;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/GameProcessKiller$PackageMemInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 219 */
v0 = v0 = this.mAllPackageInfos;
/* if-nez v0, :cond_0 */
/* .line 220 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 221 */
} // :cond_0
v0 = this.mAllPackageInfos;
/* check-cast v0, Ljava/util/List; */
/* .line 222 */
/* .local v0, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessKiller$PackageMemInfo;>;" */
/* new-instance v1, Lcom/android/server/am/GameMemoryReclaimer$2; */
/* invoke-direct {v1, p0}, Lcom/android/server/am/GameMemoryReclaimer$2;-><init>(Lcom/android/server/am/GameMemoryReclaimer;)V */
java.util.Collections .sort ( v0,v1 );
/* .line 230 */
} // .end method
java.util.List filterProcessInfos ( com.android.server.am.IGameProcessAction p0 ) {
/* .locals 1 */
/* .param p1, "compactor" # Lcom/android/server/am/IGameProcessAction; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/IGameProcessAction;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 234 */
v0 = v0 = this.mAllProcessInfos;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 235 */
v0 = this.mAllProcessInfos;
/* check-cast v0, Ljava/util/List; */
/* .line 236 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
} // .end method
java.lang.String getPackageNameByUid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 241 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackagesForUid ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 242 */
/* .local v0, "pkgs":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* array-length v1, v0 */
/* if-lez v1, :cond_0 */
/* .line 243 */
int v1 = 0; // const/4 v1, 0x0
/* aget-object v1, v0, v1 */
/* .local v1, "packageName":Ljava/lang/String; */
/* .line 245 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // :cond_0
java.lang.Integer .toString ( p1 );
/* .line 247 */
/* .restart local v1 # "packageName":Ljava/lang/String; */
} // :goto_0
} // .end method
public void notifyGameBackground ( ) {
/* .locals 2 */
/* .line 76 */
v0 = this.mProcessActions;
/* monitor-enter v0 */
/* .line 77 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
this.mCurrentGame = v1;
/* .line 78 */
v1 = this.mProcessActions;
/* .line 79 */
v1 = this.mAllProcessInfos;
/* .line 80 */
v1 = this.mAllPackageInfos;
/* .line 81 */
/* monitor-exit v0 */
/* .line 82 */
return;
/* .line 81 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyGameForeground ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "game" # Ljava/lang/String; */
/* .line 66 */
v0 = this.mProcessActions;
/* monitor-enter v0 */
/* .line 67 */
try { // :try_start_0
this.mCurrentGame = p1;
/* .line 68 */
v1 = this.mProcessActions;
/* .line 69 */
v1 = this.mAllProcessInfos;
/* .line 70 */
v1 = this.mAllPackageInfos;
/* .line 71 */
final String v1 = "GameMemoryReclaimer"; // const-string v1, "GameMemoryReclaimer"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "reclaim memory for "; // const-string v3, "reclaim memory for "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCurrentGame;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 72 */
/* monitor-exit v0 */
/* .line 73 */
return;
/* .line 72 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyProcessDied ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 273 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/am/GameMemoryReclaimer;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 277 */
return;
} // .end method
public void reclaimBackground ( Long p0 ) {
/* .locals 2 */
/* .param p1, "need" # J */
/* .line 189 */
v0 = this.mHandler;
/* new-instance v1, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/android/server/am/GameMemoryReclaimer$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/GameMemoryReclaimer;J)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 216 */
return;
} // .end method
