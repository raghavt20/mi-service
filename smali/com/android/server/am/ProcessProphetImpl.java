public class com.android.server.am.ProcessProphetImpl implements com.android.server.am.ProcessProphetStub {
	 /* .source "ProcessProphetImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessProphetImpl$MyHandler;, */
	 /* Lcom/android/server/am/ProcessProphetImpl$BinderService;, */
	 /* Lcom/android/server/am/ProcessProphetImpl$MyReceiver;, */
	 /* Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long COPY_INTERVAL;
private static Boolean DEBUG;
private static Long EMPTY_PROCS_PSS_THRESHOLD;
private static final java.lang.String FLAG_PRELOAD;
private static Long MEM_THRESHOLD;
private static final Integer MSG_AUDIO_FOCUS_CHANGED;
private static final Integer MSG_BT_CONNECTED;
private static final Integer MSG_BT_DISCONNECTED;
private static final Integer MSG_COPY;
private static final Integer MSG_DUMP;
private static final Integer MSG_IDLE_UPDATE;
private static final Integer MSG_KILL_PROC;
private static final Integer MSG_LAUNCH_EVENT;
private static final Integer MSG_MEM_PRESSURE;
private static final Integer MSG_MTBF_TEST;
private static final Integer MSG_PROC_CHECK;
private static final Integer MSG_PROC_DIED;
private static final Integer MSG_PROC_START;
private static final Integer MSG_REGISTER_CLOUD_OBSERVER;
private static final Integer MSG_UNLOCK;
private static final Long PREDICTION_INTERVAL;
private static final java.lang.String TAG;
private static Integer TRACK_INTERVAL_TIME;
private static final java.lang.String WHITELIST_DEFAULT_PATH;
private static final mEmptyThreslist;
private static java.util.HashSet sBlackListBTAudio;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.HashSet sBlackListLaunch;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.HashSet sWhiteListEmptyProc;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean afterFirstUnlock;
private Long mAMKilledProcs;
private com.android.server.am.ActivityManagerService mAMS;
private com.android.server.wm.ActivityTaskManagerService mATMS;
public java.util.HashMap mAliveEmptyProcs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mBTConnected;
private android.content.Context mContext;
private Boolean mEnable;
public com.android.server.am.ProcessProphetImpl$MyHandler mHandler;
private android.os.HandlerThread mHandlerThread;
private Long mHits;
private Boolean mInitialized;
private Long mKilledProcs;
private java.lang.String mLastAudioFocusPkg;
private Long mLastCopyUpTime;
private Long mLastEmptyProcStartUpTime;
private java.lang.String mLastLaunchedPkg;
private Long mLastReportTime;
private java.util.ArrayList mModelPredList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Double;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private mModelTrackList;
private final java.lang.Object mModelTrackLock;
private Long mNativeKilledProcs;
private com.android.server.am.ProcessManagerService mPMS;
private com.android.server.am.ProcessProphetCloud mProcessProphetCloud;
private com.android.server.am.ProcessProphetModel mProcessProphetModel;
private Long mStartedProcs;
/* # direct methods */
static Long -$$Nest$fgetmLastCopyUpTime ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastCopyUpTime:J */
/* return-wide v0 */
} // .end method
static com.android.server.am.ProcessProphetCloud -$$Nest$fgetmProcessProphetCloud ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessProphetCloud;
} // .end method
static com.android.server.am.ProcessProphetModel -$$Nest$fgetmProcessProphetModel ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessProphetModel;
} // .end method
static void -$$Nest$fputmLastCopyUpTime ( com.android.server.am.ProcessProphetImpl p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastCopyUpTime:J */
return;
} // .end method
static com.android.internal.app.ProcessProphetInfo -$$Nest$mgetUploadData ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->getUploadData()Lcom/android/internal/app/ProcessProphetInfo; */
} // .end method
static void -$$Nest$mhandleAudioFocusChanged ( com.android.server.am.ProcessProphetImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleAudioFocusChanged(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mhandleBluetoothConnected ( com.android.server.am.ProcessProphetImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleBluetoothConnected(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mhandleBluetoothDisConnected ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handleBluetoothDisConnected()V */
return;
} // .end method
static void -$$Nest$mhandleCheckEmptyProcs ( com.android.server.am.ProcessProphetImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleCheckEmptyProcs(I)V */
return;
} // .end method
static void -$$Nest$mhandleCopy ( com.android.server.am.ProcessProphetImpl p0, java.lang.CharSequence p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleCopy(Ljava/lang/CharSequence;)V */
return;
} // .end method
static void -$$Nest$mhandleDump ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handleDump()V */
return;
} // .end method
static void -$$Nest$mhandleIdleUpdate ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handleIdleUpdate()V */
return;
} // .end method
static void -$$Nest$mhandleKillProc ( com.android.server.am.ProcessProphetImpl p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleKillProc(Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static void -$$Nest$mhandleLaunchEvent ( com.android.server.am.ProcessProphetImpl p0, java.lang.String p1, Integer p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/ProcessProphetImpl;->handleLaunchEvent(Ljava/lang/String;II)V */
return;
} // .end method
static void -$$Nest$mhandleMemPressure ( com.android.server.am.ProcessProphetImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->handleMemPressure(I)V */
return;
} // .end method
static void -$$Nest$mhandlePostInit ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handlePostInit()V */
return;
} // .end method
static void -$$Nest$mhandleProcStarted ( com.android.server.am.ProcessProphetImpl p0, com.android.server.am.ProcessRecord p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessProphetImpl;->handleProcStarted(Lcom/android/server/am/ProcessRecord;I)V */
return;
} // .end method
static void -$$Nest$mhandleUnlock ( com.android.server.am.ProcessProphetImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->handleUnlock()V */
return;
} // .end method
static Boolean -$$Nest$mstartEmptyProc ( com.android.server.am.ProcessProphetImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->startEmptyProc(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$mtryToStartEmptyProc ( com.android.server.am.ProcessProphetImpl p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->tryToStartEmptyProc(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
} // .end method
static com.android.server.am.ProcessProphetImpl ( ) {
/* .locals 4 */
/* .line 90 */
/* const-wide/32 v0, 0x96000 */
/* sput-wide v0, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J */
/* .line 92 */
/* const-wide/32 v0, 0x7d000 */
/* sput-wide v0, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J */
/* .line 94 */
int v0 = 1; // const/4 v0, 0x1
/* .line 97 */
/* const/16 v0, 0x258 */
/* const/16 v1, 0x320 */
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0x1f4 */
/* filled-new-array {v2, v3, v0, v1}, [I */
/* .line 99 */
/* nop */
/* .line 100 */
final String v0 = "persist.sys.procprophet.debug"; // const-string v0, "persist.sys.procprophet.debug"
v0 = android.os.SystemProperties .getBoolean ( v0,v2 );
com.android.server.am.ProcessProphetImpl.DEBUG = (v0!= 0);
/* .line 141 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 142 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 143 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
return;
} // .end method
public com.android.server.am.ProcessProphetImpl ( ) {
/* .locals 4 */
/* .line 67 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 101 */
/* nop */
/* .line 102 */
final String v0 = "persist.sys.procprophet.enable"; // const-string v0, "persist.sys.procprophet.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z */
/* .line 104 */
int v0 = 0; // const/4 v0, 0x0
this.mAMS = v0;
/* .line 105 */
this.mATMS = v0;
/* .line 106 */
this.mPMS = v0;
/* .line 107 */
this.mContext = v0;
/* .line 109 */
this.mHandler = v0;
/* .line 110 */
this.mHandlerThread = v0;
/* .line 111 */
this.mProcessProphetModel = v0;
/* .line 112 */
this.mProcessProphetCloud = v0;
/* .line 114 */
/* iput-boolean v1, p0, Lcom/android/server/am/ProcessProphetImpl;->afterFirstUnlock:Z */
/* .line 115 */
/* iput-boolean v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mBTConnected:Z */
/* .line 116 */
/* iput-boolean v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z */
/* .line 118 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastEmptyProcStartUpTime:J */
/* .line 119 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastCopyUpTime:J */
/* .line 122 */
/* new-instance v2, Ljava/lang/Object; */
/* invoke-direct {v2}, Ljava/lang/Object;-><init>()V */
this.mModelTrackLock = v2;
/* .line 124 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastReportTime:J */
/* .line 125 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mStartedProcs:J */
/* .line 126 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mHits:J */
/* .line 127 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mKilledProcs:J */
/* .line 128 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMKilledProcs:J */
/* .line 129 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mNativeKilledProcs:J */
/* .line 130 */
int v0 = 6; // const/4 v0, 0x6
/* new-array v1, v0, [J */
this.mModelTrackList = v1;
/* .line 132 */
/* new-instance v1, Ljava/util/ArrayList; */
/* .line 134 */
/* const-wide/16 v2, 0x0 */
java.lang.Double .valueOf ( v2,v3 );
java.util.Collections .nCopies ( v0,v2 );
/* invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mModelPredList = v1;
/* .line 137 */
final String v0 = ""; // const-string v0, ""
this.mLastLaunchedPkg = v0;
/* .line 138 */
this.mLastAudioFocusPkg = v0;
/* .line 140 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAliveEmptyProcs = v0;
return;
} // .end method
private Boolean allowToStartNewProc ( ) {
/* .locals 6 */
/* .line 608 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 609 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* iget-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastEmptyProcStartUpTime:J */
/* sub-long/2addr v2, v4 */
/* .line 610 */
/* .local v2, "uptimeDiff":J */
/* const-wide/32 v4, 0x1d4c0 */
/* cmp-long v0, v2, v4 */
/* if-gez v0, :cond_0 */
/* .line 611 */
/* .line 616 */
} // .end local v2 # "uptimeDiff":J
} // :cond_0
/* const-class v0, Landroid/os/BatteryManagerInternal; */
/* .line 617 */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/os/BatteryManagerInternal; */
v0 = (( android.os.BatteryManagerInternal ) v0 ).getBatteryLevel ( ); // invoke-virtual {v0}, Landroid/os/BatteryManagerInternal;->getBatteryLevel()I
/* .line 618 */
/* .local v0, "batteryLevel":I */
/* const/16 v2, 0x14 */
final String v3 = "ProcessProphet"; // const-string v3, "ProcessProphet"
/* if-ge v0, v2, :cond_2 */
/* .line 619 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "batteryLevel low: "; // const-string v4, "batteryLevel low: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 620 */
} // :cond_1
/* .line 624 */
} // :cond_2
v2 = /* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->estimateMemPressure()Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 625 */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* const-string/jumbo v2, "terminate proc start by mem pressure." */
android.util.Slog .d ( v3,v2 );
/* .line 626 */
} // :cond_3
/* .line 629 */
} // :cond_4
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean estimateMemPressure ( ) {
/* .locals 10 */
/* .line 922 */
/* new-instance v0, Lcom/android/internal/util/MemInfoReader; */
/* invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V */
/* .line 923 */
/* .local v0, "minfo":Lcom/android/internal/util/MemInfoReader; */
(( com.android.internal.util.MemInfoReader ) v0 ).readMemInfo ( ); // invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V
/* .line 924 */
(( com.android.internal.util.MemInfoReader ) v0 ).getRawInfo ( ); // invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J
/* .line 925 */
/* .local v1, "rawInfo":[J */
int v2 = 3; // const/4 v2, 0x3
/* aget-wide v2, v1, v2 */
/* const/16 v4, 0x1a */
/* aget-wide v5, v1, v4 */
/* add-long/2addr v2, v5 */
int v5 = 2; // const/4 v5, 0x2
/* aget-wide v5, v1, v5 */
/* add-long/2addr v2, v5 */
/* .line 928 */
/* .local v2, "otherFile":J */
int v5 = 4; // const/4 v5, 0x4
/* aget-wide v5, v1, v5 */
/* const/16 v7, 0x12 */
/* aget-wide v7, v1, v7 */
/* add-long/2addr v5, v7 */
/* aget-wide v7, v1, v4 */
/* add-long/2addr v5, v7 */
/* .line 931 */
/* .local v5, "needRemovedFile":J */
/* cmp-long v4, v2, v5 */
/* if-lez v4, :cond_0 */
/* .line 932 */
/* sub-long/2addr v2, v5 */
/* .line 934 */
} // :cond_0
/* sget-boolean v4, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
final String v7 = "ProcessProphet"; // const-string v7, "ProcessProphet"
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 935 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Other File: "; // const-string v8, "Other File: "
(( java.lang.StringBuilder ) v4 ).append ( v8 ); // invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = "KB."; // const-string v8, "KB."
(( java.lang.StringBuilder ) v4 ).append ( v8 ); // invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v4 );
/* .line 937 */
} // :cond_1
/* sget-wide v8, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J */
/* cmp-long v4, v2, v8 */
/* if-gez v4, :cond_2 */
/* .line 938 */
final String v4 = "mem pressure is critical."; // const-string v4, "mem pressure is critical."
android.util.Slog .i ( v7,v4 );
/* .line 939 */
int v4 = 1; // const/4 v4, 0x1
/* .line 941 */
} // :cond_2
int v4 = 0; // const/4 v4, 0x0
} // .end method
private java.lang.Long getPssByUid ( java.lang.String p0, Integer p1 ) {
/* .locals 22 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 945 */
/* move-object/from16 v0, p1 */
/* move/from16 v1, p2 */
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_5
final String v3 = ""; // const-string v3, ""
v3 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_5 */
/* if-gtz v1, :cond_0 */
/* goto/16 :goto_2 */
/* .line 948 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* .line 949 */
/* .local v3, "startUpTime":J */
/* move-object/from16 v5, p0 */
v6 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v6 ).getProcessRecordListByPackageAndUid ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordListByPackageAndUid(Ljava/lang/String;I)Ljava/util/List;
/* .line 950 */
/* .local v6, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* sget-boolean v7, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
final String v8 = "ms"; // const-string v8, "ms"
final String v9 = "ProcessProphet"; // const-string v9, "ProcessProphet"
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 951 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "get procs by uid consumed "; // const-string v10, "get procs by uid consumed "
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 952 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v10 */
/* sub-long/2addr v10, v3 */
(( java.lang.StringBuilder ) v7 ).append ( v10, v11 ); // invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 951 */
android.util.Slog .i ( v9,v7 );
/* .line 955 */
} // :cond_1
/* const-wide/16 v10, 0x0 */
/* .line 956 */
/* .local v10, "totalPss":J */
v12 = } // :goto_0
if ( v12 != null) { // if-eqz v12, :cond_3
/* check-cast v12, Lcom/android/server/am/ProcessRecord; */
/* .line 957 */
/* .local v12, "app":Lcom/android/server/am/ProcessRecord; */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v15 */
/* .line 958 */
/* .local v15, "startUpTimeGetPss":J */
/* iget v13, v12, Lcom/android/server/am/ProcessRecord;->mPid:I */
android.os.Debug .getPss ( v13,v2,v2 );
/* move-result-wide v13 */
java.lang.Long .valueOf ( v13,v14 );
/* .line 959 */
/* .local v13, "dPss":Ljava/lang/Long; */
(( java.lang.Long ) v13 ).longValue ( ); // invoke-virtual {v13}, Ljava/lang/Long;->longValue()J
/* move-result-wide v19 */
/* add-long v10, v10, v19 */
/* .line 960 */
/* sget-boolean v14, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v14 != null) { // if-eqz v14, :cond_2
/* .line 961 */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "\tEmptyProcs "; // const-string v2, "\tEmptyProcs "
(( java.lang.StringBuilder ) v14 ).append ( v2 ); // invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v14 = this.processName;
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = "("; // const-string v14, "("
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v14, v12, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v14 = "): Pss="; // const-string v14, "): Pss="
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 962 */
(( java.lang.Long ) v13 ).longValue ( ); // invoke-virtual {v13}, Ljava/lang/Long;->longValue()J
/* move-result-wide v20 */
/* move-object v14, v6 */
/* const-wide/16 v17, 0x400 */
} // .end local v6 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .local v14, "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* div-long v5, v20, v17 */
(( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "MB, querying consumed "; // const-string v5, "MB, querying consumed "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 963 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
/* sub-long/2addr v5, v15 */
(( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 961 */
android.util.Slog .i ( v9,v2 );
/* .line 960 */
} // .end local v14 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v6 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
} // :cond_2
/* move-object v14, v6 */
/* .line 965 */
} // .end local v6 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v12 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v13 # "dPss":Ljava/lang/Long;
} // .end local v15 # "startUpTimeGetPss":J
/* .restart local v14 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
} // :goto_1
int v2 = 0; // const/4 v2, 0x0
/* move-object/from16 v5, p0 */
/* move-object v6, v14 */
/* .line 966 */
} // .end local v14 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v6 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
} // :cond_3
/* move-object v14, v6 */
} // .end local v6 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v14 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 967 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Total pss of "; // const-string v5, "Total pss of "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " is "; // const-string v5, " is "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-wide/16 v5, 0x400 */
/* div-long v5, v10, v5 */
(( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "MB, total consumed "; // const-string v5, "MB, total consumed "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 969 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v5 */
/* sub-long/2addr v5, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 967 */
android.util.Slog .i ( v9,v2 );
/* .line 971 */
} // :cond_4
java.lang.Long .valueOf ( v10,v11 );
/* .line 946 */
} // .end local v3 # "startUpTime":J
} // .end local v10 # "totalPss":J
} // .end local v14 # "appList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_5
} // :goto_2
int v2 = 0; // const/4 v2, 0x0
} // .end method
private com.android.internal.app.ProcessProphetInfo getUploadData ( ) {
/* .locals 49 */
/* .line 1158 */
/* move-object/from16 v1, p0 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z */
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1159 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* .line 1160 */
/* .local v2, "curTime":J */
int v4 = 0; // const/4 v4, 0x0
/* .line 1162 */
/* .local v4, "procProphetInfo":Lcom/android/internal/app/ProcessProphetInfo; */
/* iget-wide v5, v1, Lcom/android/server/am/ProcessProphetImpl;->mLastReportTime:J */
/* sub-long v5, v2, v5 */
/* int-to-long v7, v0 */
/* const-wide/32 v9, 0x5265c00 */
/* mul-long/2addr v7, v9 */
/* cmp-long v0, v5, v7 */
/* if-ltz v0, :cond_1 */
/* .line 1163 */
v5 = this.mModelTrackLock;
/* monitor-enter v5 */
/* .line 1164 */
try { // :try_start_0
/* new-instance v0, Lcom/android/internal/app/ProcessProphetInfo; */
/* iget-wide v7, v1, Lcom/android/server/am/ProcessProphetImpl;->mStartedProcs:J */
/* iget-wide v9, v1, Lcom/android/server/am/ProcessProphetImpl;->mHits:J */
/* iget-wide v11, v1, Lcom/android/server/am/ProcessProphetImpl;->mKilledProcs:J */
/* iget-wide v13, v1, Lcom/android/server/am/ProcessProphetImpl;->mAMKilledProcs:J */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move-wide/from16 v41, v2 */
} // .end local v2 # "curTime":J
/* .local v41, "curTime":J */
try { // :try_start_1
/* iget-wide v2, v1, Lcom/android/server/am/ProcessProphetImpl;->mNativeKilledProcs:J */
v6 = this.mModelTrackList;
int v15 = 0; // const/4 v15, 0x0
/* aget-wide v17, v6, v15 */
int v15 = 1; // const/4 v15, 0x1
/* aget-wide v19, v6, v15 */
int v15 = 2; // const/4 v15, 0x2
/* aget-wide v22, v6, v15 */
int v15 = 3; // const/4 v15, 0x3
/* aget-wide v25, v6, v15 */
int v15 = 4; // const/4 v15, 0x4
/* aget-wide v28, v6, v15 */
int v15 = 5; // const/4 v15, 0x5
/* aget-wide v31, v6, v15 */
v6 = this.mModelPredList;
/* .line 1168 */
int v15 = 0; // const/4 v15, 0x0
(( java.util.ArrayList ) v6 ).get ( v15 ); // invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/Double; */
(( java.lang.Double ) v6 ).doubleValue ( ); // invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v34 */
v6 = this.mModelPredList;
int v15 = 1; // const/4 v15, 0x1
(( java.util.ArrayList ) v6 ).get ( v15 ); // invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/Double; */
(( java.lang.Double ) v6 ).doubleValue ( ); // invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v36 */
v6 = this.mModelPredList;
int v15 = 2; // const/4 v15, 0x2
(( java.util.ArrayList ) v6 ).get ( v15 ); // invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/Double; */
(( java.lang.Double ) v6 ).doubleValue ( ); // invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v38 */
v6 = this.mModelPredList;
/* .line 1169 */
int v15 = 3; // const/4 v15, 0x3
(( java.util.ArrayList ) v6 ).get ( v15 ); // invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/Double; */
(( java.lang.Double ) v6 ).doubleValue ( ); // invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v43 */
v6 = this.mModelPredList;
int v15 = 4; // const/4 v15, 0x4
(( java.util.ArrayList ) v6 ).get ( v15 ); // invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/Double; */
(( java.lang.Double ) v6 ).doubleValue ( ); // invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v45 */
v6 = this.mModelPredList;
int v15 = 5; // const/4 v15, 0x5
(( java.util.ArrayList ) v6 ).get ( v15 ); // invoke-virtual {v6, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/Double; */
(( java.lang.Double ) v6 ).doubleValue ( ); // invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v47 */
/* move-object v6, v0 */
/* move-wide v15, v2 */
/* move-wide/from16 v21, v22 */
/* move-wide/from16 v23, v25 */
/* move-wide/from16 v25, v28 */
/* move-wide/from16 v27, v31 */
/* move-wide/from16 v29, v34 */
/* move-wide/from16 v31, v36 */
/* move-wide/from16 v33, v38 */
/* move-wide/from16 v35, v43 */
/* move-wide/from16 v37, v45 */
/* move-wide/from16 v39, v47 */
/* invoke-direct/range {v6 ..v40}, Lcom/android/internal/app/ProcessProphetInfo;-><init>(JJJJJJJJJJJDDDDDD)V */
/* move-object v4, v0 */
/* .line 1170 */
/* monitor-exit v5 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1171 */
/* move-wide/from16 v2, v41 */
} // .end local v41 # "curTime":J
/* .restart local v2 # "curTime":J */
/* iput-wide v2, v1, Lcom/android/server/am/ProcessProphetImpl;->mLastReportTime:J */
/* .line 1172 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1173 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
final String v5 = "get onetrack records by getuploadData"; // const-string v5, "get onetrack records by getuploadData"
android.util.Slog .i ( v0,v5 );
/* .line 1170 */
} // .end local v2 # "curTime":J
/* .restart local v41 # "curTime":J */
/* :catchall_0 */
/* move-exception v0 */
/* move-wide/from16 v2, v41 */
} // .end local v41 # "curTime":J
/* .restart local v2 # "curTime":J */
/* :catchall_1 */
/* move-exception v0 */
} // :goto_0
try { // :try_start_2
/* monitor-exit v5 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v0 */
/* .line 1176 */
} // :cond_1
} // :goto_1
} // .end method
private void handleAudioFocusChanged ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "callingPackageName" # Ljava/lang/String; */
/* .line 427 */
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mBTConnected:Z */
/* if-nez v0, :cond_0 */
/* .line 428 */
return;
/* .line 430 */
} // :cond_0
v0 = this.mLastAudioFocusPkg;
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
v0 = com.android.server.am.ProcessProphetImpl.sBlackListBTAudio;
/* .line 431 */
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 435 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "update audio focus changed to " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.util.Slog .i ( v1,v0 );
/* .line 436 */
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).updateBTAudioEvent ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel;->updateBTAudioEvent(Ljava/lang/String;)V
/* .line 437 */
this.mLastAudioFocusPkg = p1;
/* .line 438 */
return;
/* .line 432 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void handleBluetoothConnected ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "deviceName" # Ljava/lang/String; */
/* .line 499 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mBTConnected:Z */
/* .line 500 */
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).notifyBTConnected ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->notifyBTConnected()V
/* .line 502 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->triggerPrediction()V */
/* .line 503 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Bluetooth Connected, device = "; // const-string v1, "Bluetooth Connected, device = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.util.Slog .i ( v1,v0 );
/* .line 504 */
return;
} // .end method
private void handleBluetoothDisConnected ( ) {
/* .locals 2 */
/* .line 510 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mBTConnected:Z */
/* .line 511 */
final String v0 = ""; // const-string v0, ""
this.mLastAudioFocusPkg = v0;
/* .line 512 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
final String v1 = "Bluetooth DisConnected."; // const-string v1, "Bluetooth DisConnected."
android.util.Slog .i ( v0,v1 );
/* .line 513 */
return;
} // .end method
private void handleCheckEmptyProcs ( Integer p0 ) {
/* .locals 13 */
/* .param p1, "cycleTime" # I */
/* .line 747 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 749 */
/* .local v0, "startUpTime":J */
v2 = this.mProcessProphetModel;
v3 = this.mAliveEmptyProcs;
/* .line 750 */
(( com.android.server.am.ProcessProphetModel ) v2 ).sortEmptyProcs ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetModel;->sortEmptyProcs(Ljava/util/HashMap;)Ljava/util/ArrayList;
/* .line 753 */
/* .local v2, "sortedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;" */
/* const-wide/16 v3, 0x0 */
/* .line 754 */
/* .local v3, "totalPss":J */
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
} // :cond_0
v6 = } // :goto_0
if ( v6 != null) { // if-eqz v6, :cond_5
/* check-cast v6, Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
/* .line 755 */
/* .local v6, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
if ( v6 != null) { // if-eqz v6, :cond_0
v7 = this.pkgName;
/* if-nez v7, :cond_1 */
/* .line 756 */
/* .line 758 */
} // :cond_1
int v7 = 0; // const/4 v7, 0x0
/* .line 759 */
/* .local v7, "app":Lcom/android/server/am/ProcessRecord; */
v8 = this.mAliveEmptyProcs;
/* monitor-enter v8 */
/* .line 760 */
try { // :try_start_0
v9 = this.mAliveEmptyProcs;
v10 = this.pkgName;
(( java.util.HashMap ) v9 ).get ( v10 ); // invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Lcom/android/server/am/ProcessRecord; */
/* move-object v7, v9 */
/* .line 761 */
/* monitor-exit v8 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 762 */
/* if-nez v7, :cond_2 */
/* .line 763 */
/* .line 765 */
} // :cond_2
v8 = this.processName;
v9 = this.info;
/* iget v9, v9, Landroid/content/pm/ApplicationInfo;->uid:I */
/* invoke-direct {p0, v8, v9}, Lcom/android/server/am/ProcessProphetImpl;->getPssByUid(Ljava/lang/String;I)Ljava/lang/Long; */
/* .line 766 */
/* .local v8, "pss":Ljava/lang/Long; */
if ( v8 != null) { // if-eqz v8, :cond_4
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v9 */
/* const-wide/16 v11, 0x0 */
/* cmp-long v9, v9, v11 */
/* if-lez v9, :cond_4 */
/* .line 767 */
v9 = this.mProcessProphetModel;
v10 = this.pkgName;
(( com.android.server.am.ProcessProphetModel ) v9 ).updatePssInRecord ( v10, v8 ); // invoke-virtual {v9, v10, v8}, Lcom/android/server/am/ProcessProphetModel;->updatePssInRecord(Ljava/lang/String;Ljava/lang/Long;)V
/* .line 768 */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v9 */
/* add-long/2addr v9, v3 */
/* sget-wide v11, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J */
/* cmp-long v9, v9, v11 */
/* if-lez v9, :cond_3 */
/* .line 769 */
/* invoke-direct {p0, v7}, Lcom/android/server/am/ProcessProphetImpl;->tryToKillProc(Lcom/android/server/am/ProcessRecord;)V */
/* .line 771 */
} // :cond_3
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v9 */
/* add-long/2addr v3, v9 */
/* .line 774 */
} // .end local v6 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
} // .end local v7 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v8 # "pss":Ljava/lang/Long;
} // :cond_4
} // :goto_1
/* .line 761 */
/* .restart local v6 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
/* .restart local v7 # "app":Lcom/android/server/am/ProcessRecord; */
/* :catchall_0 */
/* move-exception v5 */
try { // :try_start_1
/* monitor-exit v8 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v5 */
/* .line 776 */
} // .end local v6 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
} // .end local v7 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_5
/* add-int/lit8 v5, p1, -0x1 */
/* if-lez v5, :cond_6 */
v5 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* if-lez v5, :cond_6 */
/* .line 777 */
v5 = this.mHandler;
/* add-int/lit8 v6, p1, -0x1 */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* const/16 v9, 0x9 */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v5 ).obtainMessage ( v9, v6, v7, v8 ); // invoke-virtual {v5, v9, v6, v7, v8}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 778 */
/* .local v5, "msg":Landroid/os/Message; */
v6 = this.mHandler;
/* const-wide/16 v7, 0x2710 */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v6 ).sendMessageDelayed ( v5, v7, v8 ); // invoke-virtual {v6, v5, v7, v8}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 781 */
} // .end local v5 # "msg":Landroid/os/Message;
} // :cond_6
/* sget-boolean v5, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 782 */
final String v5 = "ProcessProphet"; // const-string v5, "ProcessProphet"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "handleCheckEmptyProcs consumed "; // const-string v7, "handleCheckEmptyProcs consumed "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 783 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v0 */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms"; // const-string v7, "ms"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 782 */
android.util.Slog .d ( v5,v6 );
/* .line 785 */
} // :cond_7
return;
} // .end method
private void handleCopy ( java.lang.CharSequence p0 ) {
/* .locals 3 */
/* .param p1, "text" # Ljava/lang/CharSequence; */
/* .line 519 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 520 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "copied: "; // const-string v2, "copied: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 522 */
} // :cond_0
v0 = this.mHandler;
int v2 = 6; // const/4 v2, 0x6
v0 = (( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).hasMessages ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 523 */
final String v0 = "copy skip..."; // const-string v0, "copy skip..."
android.util.Slog .i ( v1,v0 );
/* .line 524 */
return;
/* .line 528 */
} // :cond_1
try { // :try_start_0
v0 = this.mATMS;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getFocusedRootTaskInfo ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;
/* .line 529 */
/* .local v0, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
if ( v0 != null) { // if-eqz v0, :cond_4
v2 = this.topActivity;
/* if-nez v2, :cond_2 */
/* .line 534 */
} // :cond_2
v1 = this.topActivity;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 535 */
/* .local v1, "curTopProcName":Ljava/lang/String; */
v2 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v2 ).updateCopyEvent ( p1, v1 ); // invoke-virtual {v2, p1, v1}, Lcom/android/server/am/ProcessProphetModel;->updateCopyEvent(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;
/* .line 536 */
/* .local v2, "matchedPkgName":Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 537 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->triggerPrediction()V */
/* .line 541 */
} // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v1 # "curTopProcName":Ljava/lang/String;
} // .end local v2 # "matchedPkgName":Ljava/lang/String;
} // :cond_3
/* .line 530 */
/* .restart local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
} // :cond_4
} // :goto_0
final String v2 = "getFocusedRootTaskInfo error."; // const-string v2, "getFocusedRootTaskInfo error."
android.util.Slog .e ( v1,v2 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 531 */
return;
/* .line 539 */
} // .end local v0 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 542 */
} // :goto_1
return;
} // .end method
private void handleDump ( ) {
/* .locals 9 */
/* .line 975 */
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).dump ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->dump()V
/* .line 976 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
final String v1 = "Dumping AliveEmptyProcs:"; // const-string v1, "Dumping AliveEmptyProcs:"
android.util.Slog .i ( v0,v1 );
/* .line 977 */
v0 = this.mAliveEmptyProcs;
/* monitor-enter v0 */
/* .line 978 */
try { // :try_start_0
v1 = this.mAliveEmptyProcs;
(( java.util.HashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/ProcessRecord; */
/* .line 979 */
/* .local v2, "app":Lcom/android/server/am/ProcessRecord; */
/* iget v3, v2, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-gtz v3, :cond_0 */
/* .line 980 */
} // :cond_0
/* iget v3, v2, Lcom/android/server/am/ProcessRecord;->mPid:I */
int v4 = 0; // const/4 v4, 0x0
android.os.Debug .getPss ( v3,v4,v4 );
/* move-result-wide v3 */
/* .line 981 */
/* .local v3, "dPss":J */
final String v5 = "ProcessProphet"; // const-string v5, "ProcessProphet"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "\tPss="; // const-string v7, "\tPss="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-wide/16 v7, 0x400 */
/* div-long v7, v3, v7 */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "MB "; // const-string v7, "MB "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v6 );
/* .line 982 */
/* nop */
} // .end local v2 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v3 # "dPss":J
/* .line 983 */
} // :cond_1
/* monitor-exit v0 */
/* .line 984 */
return;
/* .line 983 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void handleIdleUpdate ( ) {
/* .locals 8 */
/* .line 874 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
final String v1 = "IUpdate."; // const-string v1, "IUpdate."
android.util.Slog .i ( v0,v1 );
/* .line 875 */
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).conclude ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->conclude()V
/* .line 878 */
v0 = this.mModelTrackLock;
/* monitor-enter v0 */
/* .line 879 */
try { // :try_start_0
v1 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v1 ).updateModelSizeTrack ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel;->updateModelSizeTrack()[J
this.mModelTrackList = v1;
/* .line 881 */
v1 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v1 ).uploadModelPredProb ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel;->uploadModelPredProb()Ljava/util/ArrayList;
/* .line 882 */
/* .local v1, "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = this.mModelPredList;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_0 */
/* .line 883 */
v3 = this.mModelPredList;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Double; */
(( java.lang.Double ) v4 ).doubleValue ( ); // invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v4 */
(( java.util.ArrayList ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Ljava/lang/Double; */
(( java.lang.Double ) v6 ).doubleValue ( ); // invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v6 */
/* add-double/2addr v4, v6 */
java.lang.Double .valueOf ( v4,v5 );
(( java.util.ArrayList ) v3 ).set ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 882 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 885 */
} // .end local v1 # "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
} // .end local v2 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* .line 886 */
return;
/* .line 885 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void handleKillProc ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 8 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 691 */
/* if-nez p1, :cond_0 */
/* .line 692 */
return;
/* .line 694 */
} // :cond_0
v0 = this.mState;
/* .line 695 */
/* .local v0, "state":Lcom/android/server/am/ProcessStateRecord; */
v1 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* .line 696 */
/* .local v1, "curAdj":I */
v2 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurProcState ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* .line 698 */
/* .local v2, "curProcState":I */
/* sget-boolean v3, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
final String v4 = "ProcessProphet"; // const-string v4, "ProcessProphet"
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 699 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "trying to kill: " */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.processName;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "("; // const-string v5, "("
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ") curAdj="; // const-string v5, ") curAdj="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " curProcState="; // const-string v5, " curProcState="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " adjType="; // const-string v5, " adjType="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 703 */
(( com.android.server.am.ProcessStateRecord ) v0 ).getAdjType ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " isKilled="; // const-string v5, " isKilled="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 704 */
v5 = (( com.android.server.am.ProcessRecord ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* const-string/jumbo v6, "true" */
final String v7 = "false"; // const-string v7, "false"
if ( v5 != null) { // if-eqz v5, :cond_1
/* move-object v5, v6 */
} // :cond_1
/* move-object v5, v7 */
} // :goto_0
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " isKilledByAm="; // const-string v5, " isKilledByAm="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 705 */
v5 = (( com.android.server.am.ProcessRecord ) p1 ).isKilledByAm ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
if ( v5 != null) { // if-eqz v5, :cond_2
} // :cond_2
/* move-object v6, v7 */
} // :goto_1
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 699 */
android.util.Slog .i ( v4,v3 );
/* .line 708 */
} // :cond_3
/* iget v3, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-lez v3, :cond_4 */
v3 = (( com.android.server.am.ProcessRecord ) p1 ).isKilledByAm ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v3, :cond_4 */
v3 = (( com.android.server.am.ProcessRecord ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* if-nez v3, :cond_4 */
/* const/16 v3, 0x13 */
/* if-ne v2, v3, :cond_4 */
/* const/16 v3, 0x320 */
/* if-ne v1, v3, :cond_4 */
/* .line 711 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "killing proc: "; // const-string v5, "killing proc: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 712 */
v3 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v3 ).getProcessKiller ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;
final String v4 = "pp-oom"; // const-string v4, "pp-oom"
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.am.ProcessKiller ) v3 ).killApplication ( p1, v4, v5 ); // invoke-virtual {v3, p1, v4, v5}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z
/* .line 714 */
} // :cond_4
return;
} // .end method
private void handleLaunchEvent ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "launchType" # I */
/* .param p3, "launchDurationMs" # I */
/* .line 455 */
v0 = this.mLastLaunchedPkg;
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 456 */
return;
/* .line 458 */
} // :cond_0
this.mLastLaunchedPkg = p1;
/* .line 460 */
v0 = com.android.server.am.ProcessProphetImpl.sBlackListLaunch;
v0 = (( java.util.HashSet ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 461 */
return;
/* .line 465 */
} // :cond_1
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).updateLaunchEvent ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel;->updateLaunchEvent(Ljava/lang/String;)V
/* .line 466 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 467 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " Launched, type="; // const-string v2, " Launched, type="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", duration="; // const-string v2, ", duration="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 471 */
} // :cond_2
v0 = this.mAliveEmptyProcs;
/* monitor-enter v0 */
/* .line 472 */
try { // :try_start_0
v1 = this.mAliveEmptyProcs;
v1 = (( java.util.HashMap ) v1 ).containsKey ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 473 */
/* iget-wide v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHits:J */
/* const-wide/16 v3, 0x1 */
/* add-long/2addr v1, v3 */
/* iput-wide v1, p0, Lcom/android/server/am/ProcessProphetImpl;->mHits:J */
/* .line 474 */
v1 = this.mAliveEmptyProcs;
(( java.util.HashMap ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 475 */
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "AliveEmptyProcs remove: "; // const-string v3, "AliveEmptyProcs remove: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " as launched."; // const-string v3, " as launched."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 477 */
} // :cond_3
/* monitor-exit v0 */
/* .line 478 */
return;
/* .line 477 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void handleMemPressure ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "newPressureState" # I */
/* .line 905 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->estimateMemPressure()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 906 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
/* const-string/jumbo v1, "trying to kill all empty procs." */
android.util.Slog .i ( v0,v1 );
/* .line 907 */
v0 = this.mAliveEmptyProcs;
/* monitor-enter v0 */
/* .line 908 */
try { // :try_start_0
v1 = this.mAliveEmptyProcs;
(( java.util.HashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/ProcessRecord; */
/* .line 909 */
/* .local v2, "app":Lcom/android/server/am/ProcessRecord; */
/* if-nez v2, :cond_0 */
/* .line 910 */
/* .line 912 */
} // :cond_0
/* invoke-direct {p0, v2}, Lcom/android/server/am/ProcessProphetImpl;->tryToKillProc(Lcom/android/server/am/ProcessRecord;)V */
/* .line 913 */
} // .end local v2 # "app":Lcom/android/server/am/ProcessRecord;
/* .line 914 */
} // :cond_1
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 916 */
} // :cond_2
} // :goto_1
return;
} // .end method
private void handlePostInit ( ) {
/* .locals 8 */
/* .line 186 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 188 */
/* .local v0, "startUpTime":J */
/* new-instance v2, Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.USER_PRESENT"; // const-string v3, "android.intent.action.USER_PRESENT"
/* invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 189 */
/* .local v2, "filter":Landroid/content/IntentFilter; */
final String v3 = "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"; // const-string v3, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"
(( android.content.IntentFilter ) v2 ).addAction ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 190 */
v3 = this.mContext;
/* new-instance v4, Lcom/android/server/am/ProcessProphetImpl$MyReceiver; */
/* invoke-direct {v4, p0}, Lcom/android/server/am/ProcessProphetImpl$MyReceiver;-><init>(Lcom/android/server/am/ProcessProphetImpl;)V */
(( android.content.Context ) v3 ).registerReceiver ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 192 */
/* new-instance v3, Lcom/android/server/am/ProcessProphetModel; */
/* invoke-direct {v3}, Lcom/android/server/am/ProcessProphetModel;-><init>()V */
this.mProcessProphetModel = v3;
/* .line 194 */
v3 = this.mContext;
com.android.server.am.ProcessProphetJobService .schedule ( v3 );
/* .line 197 */
(( com.android.server.am.ProcessProphetImpl ) p0 ).getWhitePackages ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->getWhitePackages()V
/* .line 200 */
v3 = this.mContext;
/* .line 201 */
final String v4 = "clipboard"; // const-string v4, "clipboard"
(( android.content.Context ) v3 ).getSystemService ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v3, Landroid/content/ClipboardManager; */
/* .line 202 */
/* .local v3, "clipboardManager":Landroid/content/ClipboardManager; */
/* new-instance v4, Lcom/android/server/am/ProcessProphetImpl$2; */
/* invoke-direct {v4, p0, v3}, Lcom/android/server/am/ProcessProphetImpl$2;-><init>(Lcom/android/server/am/ProcessProphetImpl;Landroid/content/ClipboardManager;)V */
(( android.content.ClipboardManager ) v3 ).addPrimaryClipChangedListener ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/ClipboardManager;->addPrimaryClipChangedListener(Landroid/content/ClipboardManager$OnPrimaryClipChangedListener;)V
/* .line 222 */
/* new-instance v4, Lcom/android/server/am/ProcessProphetCloud; */
/* invoke-direct {v4}, Lcom/android/server/am/ProcessProphetCloud;-><init>()V */
this.mProcessProphetCloud = v4;
/* .line 223 */
v5 = this.mProcessProphetModel;
v6 = this.mContext;
(( com.android.server.am.ProcessProphetCloud ) v4 ).initCloud ( p0, v5, v6 ); // invoke-virtual {v4, p0, v5, v6}, Lcom/android/server/am/ProcessProphetCloud;->initCloud(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessProphetModel;Landroid/content/Context;)V
/* .line 224 */
v4 = this.mHandler;
/* const/16 v5, 0xf */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v4 ).obtainMessage ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 225 */
/* .local v4, "msgRegisterCloud":Landroid/os/Message; */
v5 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v5 ).sendMessage ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 227 */
int v5 = 1; // const/4 v5, 0x1
/* iput-boolean v5, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z */
/* .line 229 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "post init consumed "; // const-string v6, "post init consumed "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* sub-long/2addr v6, v0 */
(( java.lang.StringBuilder ) v5 ).append ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v6 = "ms"; // const-string v6, "ms"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "ProcessProphet"; // const-string v6, "ProcessProphet"
android.util.Slog .d ( v6,v5 );
/* .line 230 */
return;
} // .end method
private void handleProcStarted ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pid" # I */
/* .line 730 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "preload "; // const-string v2, "preload "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.processName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " success! pid:"; // const-string v2, " success! pid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " uid:"; // const-string v2, " uid:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 732 */
v0 = this.mAliveEmptyProcs;
/* monitor-enter v0 */
/* .line 733 */
try { // :try_start_0
v1 = this.mAliveEmptyProcs;
v2 = this.processName;
(( java.util.HashMap ) v1 ).put ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 734 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 735 */
/* iget-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mStartedProcs:J */
/* const-wide/16 v2, 0x1 */
/* add-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mStartedProcs:J */
/* .line 736 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "add: "; // const-string v2, "add: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 738 */
} // :cond_0
v0 = this.mHandler;
/* const/16 v1, 0x9 */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V
/* .line 739 */
v0 = this.mHandler;
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
int v4 = 3; // const/4 v4, 0x3
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).obtainMessage ( v1, v4, v2, v3 ); // invoke-virtual {v0, v1, v4, v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 740 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0x2710 */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 741 */
return;
/* .line 734 */
} // .end local v0 # "msg":Landroid/os/Message;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void handleUnlock ( ) {
/* .locals 2 */
/* .line 484 */
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->afterFirstUnlock:Z */
/* if-nez v0, :cond_1 */
/* .line 485 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 486 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
/* const-string/jumbo v1, "skip first unlock." */
android.util.Slog .d ( v0,v1 );
/* .line 488 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->afterFirstUnlock:Z */
/* .line 489 */
return;
/* .line 492 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->triggerPrediction()V */
/* .line 493 */
return;
} // .end method
private void initThreshold ( ) {
/* .locals 6 */
/* .line 306 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
try { // :try_start_0
final String v1 = "persist.sys.procprophet.mem_threshold"; // const-string v1, "persist.sys.procprophet.mem_threshold"
/* const/16 v2, 0x258 */
v1 = android.os.SystemProperties .getInt ( v1,v2 );
/* int-to-long v1, v1 */
/* const-wide/16 v3, 0x400 */
/* mul-long/2addr v1, v3 */
/* sput-wide v1, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J */
/* .line 308 */
final String v1 = "persist.sys.procprophet.max_pss"; // const-string v1, "persist.sys.procprophet.max_pss"
v2 = com.android.server.am.ProcessProphetImpl.mEmptyThreslist;
/* .line 309 */
v5 = (( com.android.server.am.ProcessProphetImpl ) p0 ).getRamLayerIndex ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->getRamLayerIndex()I
/* aget v2, v2, v5 */
/* .line 308 */
v1 = android.os.SystemProperties .getInt ( v1,v2 );
/* int-to-long v1, v1 */
/* mul-long/2addr v1, v3 */
/* sput-wide v1, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J */
/* .line 310 */
/* sget-boolean v1, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Init threshold: mem threshold is "; // const-string v2, "Init threshold: mem threshold is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v2, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ".Empty Process PSS Threshold is "; // const-string v2, ".Empty Process PSS Threshold is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v2, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 314 */
} // :cond_0
/* .line 312 */
/* :catch_0 */
/* move-exception v1 */
/* .line 313 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "init mem threshold failure!"; // const-string v2, "init mem threshold failure!"
android.util.Slog .e ( v0,v2 );
/* .line 315 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean startEmptyProc ( java.lang.String p0 ) {
/* .locals 14 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 652 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_4
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* goto/16 :goto_0 */
/* .line 655 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 657 */
/* .local v1, "info":Landroid/content/pm/ApplicationInfo; */
try { // :try_start_0
android.app.AppGlobals .getPackageManager ( );
/* const-wide/16 v3, 0x400 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 662 */
} // .end local v1 # "info":Landroid/content/pm/ApplicationInfo;
/* .local v2, "info":Landroid/content/pm/ApplicationInfo; */
/* nop */
/* .line 663 */
/* if-nez v2, :cond_1 */
/* .line 664 */
/* .line 667 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 668 */
/* .local v1, "newApp":Lcom/android/server/am/ProcessRecord; */
v13 = this.mAMS;
/* monitor-enter v13 */
/* .line 669 */
try { // :try_start_1
v3 = this.mAMS;
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* new-instance v8, Lcom/android/server/am/HostingRecord; */
final String v4 = "ProcessProphet"; // const-string v4, "ProcessProphet"
/* invoke-direct {v8, v4, p1}, Lcom/android/server/am/HostingRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
int v9 = 0; // const/4 v9, 0x0
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
final String v12 = "android"; // const-string v12, "android"
/* move-object v4, p1 */
/* move-object v5, v2 */
/* invoke-virtual/range {v3 ..v12}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILcom/android/server/am/HostingRecord;IZZLjava/lang/String;)Lcom/android/server/am/ProcessRecord; */
/* move-object v1, v3 */
/* .line 673 */
/* if-nez v1, :cond_2 */
/* .line 674 */
final String v3 = "ProcessProphet"; // const-string v3, "ProcessProphet"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "preload "; // const-string v5, "preload "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " failed!"; // const-string v5, " failed!"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* .line 675 */
/* monitor-exit v13 */
/* .line 676 */
} // :cond_2
v3 = (( com.android.server.am.ProcessRecord ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getPid()I
/* if-lez v3, :cond_3 */
/* .line 677 */
final String v3 = "ProcessProphet"; // const-string v3, "ProcessProphet"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "preload "; // const-string v5, "preload "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " existed!"; // const-string v5, " existed!"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 678 */
/* monitor-exit v13 */
/* .line 680 */
} // :cond_3
/* monitor-exit v13 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 681 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* iput-wide v3, p0, Lcom/android/server/am/ProcessProphetImpl;->mLastEmptyProcStartUpTime:J */
/* .line 682 */
int v0 = 1; // const/4 v0, 0x1
/* .line 680 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit v13 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 659 */
} // .end local v2 # "info":Landroid/content/pm/ApplicationInfo;
/* .local v1, "info":Landroid/content/pm/ApplicationInfo; */
/* :catch_0 */
/* move-exception v2 */
/* .line 660 */
/* .local v2, "e":Landroid/os/RemoteException; */
final String v3 = "ProcessProphet"; // const-string v3, "ProcessProphet"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "error in getApplicationInfo!"; // const-string v5, "error in getApplicationInfo!"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* .line 661 */
/* .line 653 */
} // .end local v1 # "info":Landroid/content/pm/ApplicationInfo;
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :cond_4
} // :goto_0
} // .end method
private void triggerPrediction ( ) {
/* .locals 18 */
/* .line 549 */
/* move-object/from16 v1, p0 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 550 */
/* .local v2, "startUpTime":J */
v0 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/ProcessProphetImpl;->allowToStartNewProc()Z */
/* if-nez v0, :cond_0 */
/* .line 551 */
return;
/* .line 554 */
} // :cond_0
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
/* const-string/jumbo v4, "trying to predict." */
android.util.Slog .i ( v0,v4 );
/* .line 556 */
v0 = this.mProcessProphetModel;
v4 = this.mAliveEmptyProcs;
/* .line 557 */
(( com.android.server.am.ProcessProphetModel ) v0 ).getWeightedTab ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/am/ProcessProphetModel;->getWeightedTab(Ljava/util/HashMap;)Ljava/util/ArrayList;
/* .line 559 */
/* .local v4, "sortedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;" */
/* const-wide/16 v5, 0x0 */
/* .line 560 */
/* .local v5, "totalPss":J */
int v0 = 1; // const/4 v0, 0x1
/* .line 562 */
/* .local v0, "allowToStartNextProc":Z */
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
/* move-wide v8, v5 */
/* move v5, v0 */
} // .end local v0 # "allowToStartNextProc":Z
/* .local v5, "allowToStartNextProc":Z */
/* .local v8, "totalPss":J */
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_8
/* move-object v6, v0 */
/* check-cast v6, Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
/* .line 563 */
/* .local v6, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
v10 = this.pkgName;
/* .line 564 */
/* .local v10, "procName":Ljava/lang/String; */
int v11 = 0; // const/4 v11, 0x0
/* .line 565 */
/* .local v11, "app":Lcom/android/server/am/ProcessRecord; */
v12 = this.mAliveEmptyProcs;
/* monitor-enter v12 */
/* .line 566 */
try { // :try_start_0
v0 = this.mAliveEmptyProcs;
(( java.util.HashMap ) v0 ).get ( v10 ); // invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/am/ProcessRecord; */
/* move-object v11, v0 */
/* .line 567 */
/* monitor-exit v12 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 568 */
if ( v11 != null) { // if-eqz v11, :cond_3
/* .line 570 */
v0 = this.info;
/* iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
/* invoke-direct {v1, v10, v0}, Lcom/android/server/am/ProcessProphetImpl;->getPssByUid(Ljava/lang/String;I)Ljava/lang/Long; */
/* .line 571 */
/* .local v0, "pss":Ljava/lang/Long; */
if ( v0 != null) { // if-eqz v0, :cond_2
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* const-wide/16 v14, 0x0 */
/* cmp-long v12, v12, v14 */
/* if-lez v12, :cond_2 */
/* .line 572 */
v12 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v12 ).updatePssInRecord ( v10, v0 ); // invoke-virtual {v12, v10, v0}, Lcom/android/server/am/ProcessProphetModel;->updatePssInRecord(Ljava/lang/String;Ljava/lang/Long;)V
/* .line 573 */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* add-long/2addr v12, v8 */
/* sget-wide v14, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J */
/* cmp-long v12, v12, v14 */
/* if-lez v12, :cond_1 */
/* .line 574 */
/* invoke-direct {v1, v11}, Lcom/android/server/am/ProcessProphetImpl;->tryToKillProc(Lcom/android/server/am/ProcessRecord;)V */
/* .line 576 */
} // :cond_1
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* add-long/2addr v8, v12 */
/* .line 579 */
} // .end local v0 # "pss":Ljava/lang/Long;
} // :cond_2
} // :goto_1
/* goto/16 :goto_2 */
} // :cond_3
if ( v5 != null) { // if-eqz v5, :cond_2
v0 = com.android.server.am.ProcessProphetImpl.sWhiteListEmptyProc;
v0 = (( java.util.HashSet ) v0 ).contains ( v10 ); // invoke-virtual {v0, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 581 */
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).getPssInRecord ( v10 ); // invoke-virtual {v0, v10}, Lcom/android/server/am/ProcessProphetModel;->getPssInRecord(Ljava/lang/String;)Ljava/lang/Long;
/* .line 582 */
/* .local v0, "referencePss":Ljava/lang/Long; */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 583 */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* add-long/2addr v12, v8 */
/* sget-wide v14, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J */
/* cmp-long v12, v12, v14 */
/* if-gtz v12, :cond_7 */
/* .line 584 */
/* sget-boolean v12, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v12 != null) { // if-eqz v12, :cond_4
/* .line 585 */
final String v12 = "ProcessProphet"; // const-string v12, "ProcessProphet"
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v13 ).append ( v10 ); // invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = " has reference pss:"; // const-string v14, " has reference pss:"
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v14 */
/* const-wide/16 v16, 0x400 */
/* div-long v14, v14, v16 */
(( java.lang.StringBuilder ) v13 ).append ( v14, v15 ); // invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v14 = "MB, and is not expected to exceed the threshold."; // const-string v14, "MB, and is not expected to exceed the threshold."
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v12,v13 );
/* .line 588 */
} // :cond_4
v12 = /* invoke-direct {v1, v10}, Lcom/android/server/am/ProcessProphetImpl;->tryToStartEmptyProc(Ljava/lang/String;)Z */
if ( v12 != null) { // if-eqz v12, :cond_7
/* .line 589 */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v12 */
/* add-long/2addr v8, v12 */
/* .line 593 */
} // :cond_5
/* sget-boolean v12, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v12 != null) { // if-eqz v12, :cond_6
final String v12 = "ProcessProphet"; // const-string v12, "ProcessProphet"
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v13 ).append ( v10 ); // invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = " doesn\'t have a reference pss."; // const-string v14, " doesn\'t have a reference pss."
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v12,v13 );
/* .line 594 */
} // :cond_6
v12 = /* invoke-direct {v1, v10}, Lcom/android/server/am/ProcessProphetImpl;->tryToStartEmptyProc(Ljava/lang/String;)Z */
if ( v12 != null) { // if-eqz v12, :cond_7
/* .line 595 */
int v5 = 0; // const/4 v5, 0x0
/* .line 599 */
} // .end local v0 # "referencePss":Ljava/lang/Long;
} // .end local v6 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
} // .end local v10 # "procName":Ljava/lang/String;
} // .end local v11 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_7
} // :goto_2
/* goto/16 :goto_0 */
/* .line 567 */
/* .restart local v6 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
/* .restart local v10 # "procName":Ljava/lang/String; */
/* .restart local v11 # "app":Lcom/android/server/am/ProcessRecord; */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v12 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 600 */
} // .end local v6 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
} // .end local v10 # "procName":Ljava/lang/String;
} // .end local v11 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_8
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "prediction consumed "; // const-string v7, "prediction consumed "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v10 */
/* sub-long/2addr v10, v2 */
(( java.lang.StringBuilder ) v6 ).append ( v10, v11 ); // invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms"; // const-string v7, "ms"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v6 );
/* .line 601 */
return;
} // .end method
private void tryToKillProc ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 686 */
v0 = this.mHandler;
/* const/16 v1, 0xa */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 687 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 688 */
return;
} // .end method
private Boolean tryToStartEmptyProc ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 636 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 640 */
} // :cond_0
v1 = com.android.server.am.ProcessProphetImpl.sWhiteListEmptyProc;
v1 = (( java.util.HashSet ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_1 */
/* .line 641 */
/* .line 644 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "trying to start proc: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.util.Slog .d ( v1,v0 );
/* .line 645 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->startEmptyProc(Ljava/lang/String;)Z */
/* .line 637 */
} // :cond_2
} // :goto_0
} // .end method
/* # virtual methods */
public Integer getRamLayerIndex ( ) {
/* .locals 4 */
/* .line 286 */
android.os.Process .getTotalMemory ( );
/* move-result-wide v0 */
/* const-wide/32 v2, 0x40000000 */
/* div-long/2addr v0, v2 */
/* const-wide/16 v2, 0x1 */
/* add-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* .line 288 */
/* .local v0, "memory_GB":I */
int v1 = 4; // const/4 v1, 0x4
/* if-gt v0, v1, :cond_0 */
/* .line 289 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "index":I */
/* .line 291 */
} // .end local v1 # "index":I
} // :cond_0
/* div-int/lit8 v1, v0, 0x2 */
/* add-int/lit8 v1, v1, -0x2 */
/* .line 292 */
/* .restart local v1 # "index":I */
int v2 = 3; // const/4 v2, 0x3
/* if-lt v1, v2, :cond_1 */
/* .line 293 */
int v1 = 3; // const/4 v1, 0x3
/* .line 296 */
} // :cond_1
} // :goto_0
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 297 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Total mem is "; // const-string v3, "Total mem is "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " index is "; // const-string v3, " index is "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessProphet"; // const-string v3, "ProcessProphet"
android.util.Slog .i ( v3,v2 );
/* .line 299 */
} // :cond_2
} // .end method
public void getWhitePackages ( ) {
/* .locals 16 */
/* .line 325 */
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 326 */
/* .local v2, "startUpTime":J */
int v4 = 0; // const/4 v4, 0x0
/* .line 327 */
/* .local v4, "inputStream":Ljava/io/InputStream; */
int v5 = 0; // const/4 v5, 0x0
/* .line 330 */
/* .local v5, "xmlParser":Lorg/xmlpull/v1/XmlPullParser; */
int v6 = 1; // const/4 v6, 0x1
int v7 = 0; // const/4 v7, 0x0
try { // :try_start_0
/* new-instance v0, Ljava/io/FileInputStream; */
final String v8 = "/product/etc/procprophet.xml"; // const-string v8, "/product/etc/procprophet.xml"
/* invoke-direct {v0, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* move-object v4, v0 */
/* .line 331 */
android.util.Xml .newPullParser ( );
/* move-object v5, v0 */
/* .line 332 */
/* const-string/jumbo v0, "utf-8" */
v0 = /* .line 333 */
/* .line 334 */
/* .local v0, "event":I */
} // :goto_0
/* if-eq v0, v6, :cond_3 */
/* .line 335 */
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 348 */
/* :pswitch_1 */
/* .line 339 */
/* :pswitch_2 */
/* const-string/jumbo v8, "white-emptyproc" */
v8 = (( java.lang.String ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_0
/* .line 340 */
v8 = com.android.server.am.ProcessProphetImpl.sWhiteListEmptyProc;
(( java.util.HashSet ) v8 ).add ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 341 */
} // :cond_0
final String v8 = "black-launch"; // const-string v8, "black-launch"
v8 = (( java.lang.String ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 342 */
v8 = com.android.server.am.ProcessProphetImpl.sBlackListLaunch;
(( java.util.HashSet ) v8 ).add ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 343 */
} // :cond_1
final String v8 = "black-bt"; // const-string v8, "black-bt"
v8 = (( java.lang.String ) v8 ).equals ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 344 */
v8 = com.android.server.am.ProcessProphetImpl.sBlackListBTAudio;
(( java.util.HashSet ) v8 ).add ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 337 */
/* :pswitch_3 */
/* nop */
/* .line 352 */
} // :cond_2
v8 = } // :goto_1
/* :try_end_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move v0, v8 */
/* .line 357 */
} // .end local v0 # "event":I
} // :cond_3
/* nop */
/* .line 359 */
try { // :try_start_1
(( java.io.InputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/InputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 363 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 360 */
/* :catch_0 */
/* move-exception v0 */
/* move-object v8, v0 */
/* move-object v0, v8 */
/* .line 361 */
/* .local v0, "e":Ljava/io/IOException; */
try { // :try_start_2
(( java.io.IOException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v1,v8 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 363 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_2
int v1 = 0; // const/4 v1, 0x0
/* .line 364 */
} // .end local v4 # "inputStream":Ljava/io/InputStream;
/* .local v1, "inputStream":Ljava/io/InputStream; */
/* throw v0 */
/* .line 357 */
} // .end local v1 # "inputStream":Ljava/io/InputStream;
/* .restart local v4 # "inputStream":Ljava/io/InputStream; */
/* :catchall_1 */
/* move-exception v0 */
/* move-object v6, v0 */
/* goto/16 :goto_c */
/* .line 354 */
/* :catch_1 */
/* move-exception v0 */
/* .line 355 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_3
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v1,v8 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 357 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 359 */
try { // :try_start_4
(( java.io.InputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/InputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* .line 363 */
/* nop */
} // :goto_3
int v0 = 0; // const/4 v0, 0x0
/* .line 364 */
} // .end local v4 # "inputStream":Ljava/io/InputStream;
/* .local v0, "inputStream":Ljava/io/InputStream; */
/* .line 363 */
} // .end local v0 # "inputStream":Ljava/io/InputStream;
/* .restart local v4 # "inputStream":Ljava/io/InputStream; */
/* :catchall_2 */
/* move-exception v0 */
/* .line 360 */
/* :catch_2 */
/* move-exception v0 */
/* move-object v8, v0 */
/* move-object v0, v8 */
/* .line 361 */
/* .local v0, "e":Ljava/io/IOException; */
try { // :try_start_5
(( java.io.IOException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v1,v8 );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 363 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_4
int v1 = 0; // const/4 v1, 0x0
/* .line 364 */
} // .end local v4 # "inputStream":Ljava/io/InputStream;
/* .restart local v1 # "inputStream":Ljava/io/InputStream; */
/* throw v0 */
/* .line 357 */
} // .end local v1 # "inputStream":Ljava/io/InputStream;
/* .restart local v4 # "inputStream":Ljava/io/InputStream; */
} // :cond_4
/* move-object v0, v4 */
/* .line 368 */
} // .end local v4 # "inputStream":Ljava/io/InputStream;
/* .local v0, "inputStream":Ljava/io/InputStream; */
} // :goto_5
/* sget-boolean v4, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_b
/* .line 369 */
/* const-string/jumbo v4, "whiteList: " */
/* .line 370 */
/* .local v4, "whiteListDump":Ljava/lang/String; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = com.android.server.am.ProcessProphetImpl.sWhiteListEmptyProc;
v9 = (( java.util.HashSet ) v9 ).size ( ); // invoke-virtual {v9}, Ljava/util/HashSet;->size()I
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 371 */
v8 = com.android.server.am.ProcessProphetImpl.sWhiteListEmptyProc;
(( java.util.HashSet ) v8 ).iterator ( ); // invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v9 = } // :goto_6
final String v10 = "......"; // const-string v10, "......"
/* const/16 v11, 0xc8 */
final String v12 = ","; // const-string v12, ","
final String v13 = " "; // const-string v13, " "
if ( v9 != null) { // if-eqz v9, :cond_6
/* check-cast v9, Ljava/lang/String; */
/* .line 372 */
/* .local v9, "procName":Ljava/lang/String; */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v14 ).append ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v13 ); // invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v9 ); // invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v12 ); // invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 373 */
v14 = (( java.lang.String ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/lang/String;->length()I
/* if-le v14, v11, :cond_5 */
/* .line 374 */
v8 = (( java.lang.String ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/lang/String;->length()I
/* sub-int/2addr v8, v6 */
(( java.lang.String ) v4 ).substring ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 375 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 376 */
/* .line 378 */
} // .end local v9 # "procName":Ljava/lang/String;
} // :cond_5
/* .line 379 */
} // :cond_6
} // :goto_7
v8 = (( java.lang.String ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/lang/String;->length()I
/* sub-int/2addr v8, v6 */
(( java.lang.String ) v4 ).substring ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
android.util.Slog .d ( v1,v8 );
/* .line 381 */
final String v8 = "blackListLaunch: "; // const-string v8, "blackListLaunch: "
/* .line 382 */
/* .local v8, "blackListLaunch":Ljava/lang/String; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v14 = com.android.server.am.ProcessProphetImpl.sBlackListLaunch;
v14 = (( java.util.HashSet ) v14 ).size ( ); // invoke-virtual {v14}, Ljava/util/HashSet;->size()I
(( java.lang.StringBuilder ) v9 ).append ( v14 ); // invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 383 */
v9 = com.android.server.am.ProcessProphetImpl.sBlackListLaunch;
(( java.util.HashSet ) v9 ).iterator ( ); // invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v14 = } // :goto_8
if ( v14 != null) { // if-eqz v14, :cond_8
/* check-cast v14, Ljava/lang/String; */
/* .line 384 */
/* .local v14, "procName":Ljava/lang/String; */
/* new-instance v15, Ljava/lang/StringBuilder; */
/* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v15 ).append ( v8 ); // invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v15 ).append ( v13 ); // invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v15 ).append ( v14 ); // invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v15 ).append ( v12 ); // invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v15 ).toString ( ); // invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 385 */
v15 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
/* if-le v15, v11, :cond_7 */
/* .line 386 */
v9 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
/* sub-int/2addr v9, v6 */
(( java.lang.String ) v8 ).substring ( v7, v9 ); // invoke-virtual {v8, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 387 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 388 */
/* .line 390 */
} // .end local v14 # "procName":Ljava/lang/String;
} // :cond_7
/* .line 391 */
} // :cond_8
} // :goto_9
v9 = (( java.lang.String ) v8 ).length ( ); // invoke-virtual {v8}, Ljava/lang/String;->length()I
/* sub-int/2addr v9, v6 */
(( java.lang.String ) v8 ).substring ( v7, v9 ); // invoke-virtual {v8, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;
android.util.Slog .d ( v1,v9 );
/* .line 393 */
final String v9 = "blackListBT: "; // const-string v9, "blackListBT: "
/* .line 394 */
/* .local v9, "blackListBT":Ljava/lang/String; */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v14 ).append ( v9 ); // invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v15 = com.android.server.am.ProcessProphetImpl.sBlackListBTAudio;
v15 = (( java.util.HashSet ) v15 ).size ( ); // invoke-virtual {v15}, Ljava/util/HashSet;->size()I
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 395 */
v14 = com.android.server.am.ProcessProphetImpl.sBlackListBTAudio;
(( java.util.HashSet ) v14 ).iterator ( ); // invoke-virtual {v14}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v15 = } // :goto_a
if ( v15 != null) { // if-eqz v15, :cond_a
/* check-cast v15, Ljava/lang/String; */
/* .line 396 */
/* .local v15, "procName":Ljava/lang/String; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v13 ); // invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v15 ); // invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 397 */
v7 = (( java.lang.String ) v9 ).length ( ); // invoke-virtual {v9}, Ljava/lang/String;->length()I
/* if-le v7, v11, :cond_9 */
/* .line 398 */
v7 = (( java.lang.String ) v9 ).length ( ); // invoke-virtual {v9}, Ljava/lang/String;->length()I
/* sub-int/2addr v7, v6 */
int v11 = 0; // const/4 v11, 0x0
(( java.lang.String ) v9 ).substring ( v11, v7 ); // invoke-virtual {v9, v11, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 399 */
} // .end local v9 # "blackListBT":Ljava/lang/String;
/* .local v7, "blackListBT":Ljava/lang/String; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 400 */
} // .end local v7 # "blackListBT":Ljava/lang/String;
/* .restart local v9 # "blackListBT":Ljava/lang/String; */
/* .line 402 */
} // .end local v15 # "procName":Ljava/lang/String;
} // :cond_9
int v7 = 0; // const/4 v7, 0x0
/* .line 403 */
} // :cond_a
} // :goto_b
v7 = (( java.lang.String ) v9 ).length ( ); // invoke-virtual {v9}, Ljava/lang/String;->length()I
/* sub-int/2addr v7, v6 */
int v6 = 0; // const/4 v6, 0x0
(( java.lang.String ) v9 ).substring ( v6, v7 ); // invoke-virtual {v9, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
android.util.Slog .d ( v1,v6 );
/* .line 405 */
} // .end local v4 # "whiteListDump":Ljava/lang/String;
} // .end local v8 # "blackListLaunch":Ljava/lang/String;
} // .end local v9 # "blackListBT":Ljava/lang/String;
} // :cond_b
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "getWhitePackages consumed "; // const-string v6, "getWhitePackages consumed "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* sub-long/2addr v6, v2 */
(( java.lang.StringBuilder ) v4 ).append ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v6 = "ms"; // const-string v6, "ms"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 406 */
return;
/* .line 357 */
} // .end local v0 # "inputStream":Ljava/io/InputStream;
/* .local v4, "inputStream":Ljava/io/InputStream; */
} // :goto_c
if ( v4 != null) { // if-eqz v4, :cond_c
/* .line 359 */
try { // :try_start_6
(( java.io.InputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/InputStream;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_3 */
/* .line 363 */
/* nop */
} // :goto_d
int v4 = 0; // const/4 v4, 0x0
/* .line 364 */
/* .line 363 */
/* :catchall_3 */
/* move-exception v0 */
/* .line 360 */
/* :catch_3 */
/* move-exception v0 */
/* move-object v7, v0 */
/* move-object v0, v7 */
/* .line 361 */
/* .local v0, "e":Ljava/io/IOException; */
try { // :try_start_7
(( java.io.IOException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v1,v7 );
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* .line 363 */
/* nop */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_e
int v1 = 0; // const/4 v1, 0x0
/* .line 364 */
} // .end local v4 # "inputStream":Ljava/io/InputStream;
/* .restart local v1 # "inputStream":Ljava/io/InputStream; */
/* throw v0 */
/* .line 366 */
} // .end local v1 # "inputStream":Ljava/io/InputStream;
/* .restart local v4 # "inputStream":Ljava/io/InputStream; */
} // :cond_c
} // :goto_f
/* throw v6 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void init ( android.content.Context p0, com.android.server.am.ActivityManagerService p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .line 147 */
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_1 */
/* .line 150 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 152 */
/* .local v0, "startUpTime":J */
/* new-instance v2, Landroid/os/HandlerThread; */
final String v3 = "ProcessProphet"; // const-string v3, "ProcessProphet"
/* invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v2;
/* .line 153 */
(( android.os.HandlerThread ) v2 ).start ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V
/* .line 154 */
/* new-instance v2, Lcom/android/server/am/ProcessProphetImpl$MyHandler; */
v4 = this.mHandlerThread;
(( android.os.HandlerThread ) v4 ).getLooper ( ); // invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;-><init>(Lcom/android/server/am/ProcessProphetImpl;Landroid/os/Looper;)V */
this.mHandler = v2;
/* .line 156 */
this.mContext = p1;
/* .line 157 */
this.mAMS = p2;
/* .line 158 */
android.app.ActivityTaskManager .getService ( );
/* check-cast v2, Lcom/android/server/wm/ActivityTaskManagerService; */
this.mATMS = v2;
/* .line 159 */
final String v2 = "ProcessManager"; // const-string v2, "ProcessManager"
android.os.ServiceManager .getService ( v2 );
/* check-cast v2, Lcom/android/server/am/ProcessManagerService; */
this.mPMS = v2;
/* .line 160 */
/* new-instance v2, Lcom/android/server/am/ProcessProphetImpl$BinderService; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v2, p0, v4}, Lcom/android/server/am/ProcessProphetImpl$BinderService;-><init>(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessProphetImpl$BinderService-IA;)V */
final String v4 = "procprophet"; // const-string v4, "procprophet"
android.os.ServiceManager .addService ( v4,v2 );
/* .line 163 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->initThreshold()V */
/* .line 165 */
v2 = this.mAMS;
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.mATMS;
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.mPMS;
/* if-nez v2, :cond_1 */
/* .line 172 */
} // :cond_1
v2 = this.mHandler;
/* new-instance v4, Lcom/android/server/am/ProcessProphetImpl$1; */
/* invoke-direct {v4, p0}, Lcom/android/server/am/ProcessProphetImpl$1;-><init>(Lcom/android/server/am/ProcessProphetImpl;)V */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).post ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 179 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "pre init consumed "; // const-string v4, "pre init consumed "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* sub-long/2addr v4, v0 */
(( java.lang.StringBuilder ) v2 ).append ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = "ms"; // const-string v4, "ms"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 180 */
return;
/* .line 166 */
} // :cond_2
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z */
/* .line 167 */
/* iput-boolean v2, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z */
/* .line 168 */
final String v2 = "disable ProcessProphet for dependencies service not available"; // const-string v2, "disable ProcessProphet for dependencies service not available"
android.util.Slog .e ( v3,v2 );
/* .line 169 */
return;
/* .line 148 */
} // .end local v0 # "startUpTime":J
} // :cond_3
} // :goto_1
return;
} // .end method
public Boolean isEnable ( ) {
/* .locals 1 */
/* .line 318 */
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->mInitialized:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetImpl;->afterFirstUnlock:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isNeedProtect ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 9 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 828 */
v0 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isPredictedProc ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->isPredictedProc(Lcom/android/server/am/ProcessRecord;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 831 */
} // :cond_0
v0 = this.processName;
/* .line 832 */
/* .local v0, "processName":Ljava/lang/String; */
v2 = this.mAliveEmptyProcs;
/* monitor-enter v2 */
/* .line 833 */
try { // :try_start_0
v3 = this.mAliveEmptyProcs;
v3 = (( java.util.HashMap ) v3 ).containsKey ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
/* if-nez v3, :cond_1 */
/* .line 834 */
/* monitor-exit v2 */
/* .line 836 */
} // :cond_1
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 838 */
v2 = this.mState;
/* .line 839 */
/* .local v2, "state":Lcom/android/server/am/ProcessStateRecord; */
v3 = (( com.android.server.am.ProcessStateRecord ) v2 ).getCurAdj ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* .line 840 */
/* .local v3, "curAdj":I */
v4 = (( com.android.server.am.ProcessStateRecord ) v2 ).getCurProcState ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* .line 841 */
/* .local v4, "curProcState":I */
/* sget-boolean v5, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
/* const/16 v6, 0x13 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* if-eq v4, v6, :cond_4 */
/* const/16 v5, 0xa */
/* if-eq v4, v5, :cond_4 */
/* const/16 v5, 0xb */
/* if-eq v4, v5, :cond_4 */
/* const/16 v5, 0x14 */
/* if-eq v4, v5, :cond_4 */
/* .line 845 */
final String v5 = "ProcessProphet"; // const-string v5, "ProcessProphet"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "processName = "; // const-string v8, "processName = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " curAdj="; // const-string v8, " curAdj="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " curProcState="; // const-string v8, " curProcState="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " adjType="; // const-string v8, " adjType="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 848 */
(( com.android.server.am.ProcessStateRecord ) v2 ).getAdjType ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " isKilled="; // const-string v8, " isKilled="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 849 */
v8 = (( com.android.server.am.ProcessRecord ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* const-string/jumbo v8, "true" */
} // :cond_2
final String v8 = "false"; // const-string v8, "false"
} // :goto_0
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " isKilledByAm="; // const-string v8, " isKilledByAm="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 850 */
v8 = (( com.android.server.am.ProcessRecord ) p1 ).isKilledByAm ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
if ( v8 != null) { // if-eqz v8, :cond_3
/* const-string/jumbo v8, "true" */
} // :cond_3
final String v8 = "false"; // const-string v8, "false"
} // :goto_1
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 845 */
android.util.Slog .d ( v5,v7 );
/* .line 853 */
} // :cond_4
/* iget v5, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-lez v5, :cond_5 */
v5 = (( com.android.server.am.ProcessRecord ) p1 ).isKilledByAm ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v5, :cond_5 */
v5 = (( com.android.server.am.ProcessRecord ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* if-nez v5, :cond_5 */
/* if-ne v4, v6, :cond_5 */
/* const/16 v5, 0x320 */
/* if-lt v3, v5, :cond_5 */
/* .line 856 */
int v1 = 1; // const/4 v1, 0x1
/* .line 860 */
} // :cond_5
/* .line 836 */
} // .end local v2 # "state":Lcom/android/server/am/ProcessStateRecord;
} // .end local v3 # "curAdj":I
} // .end local v4 # "curProcState":I
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isPredictedProc ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 810 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
v1 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 812 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_2
try { // :try_start_0
(( com.android.server.am.ProcessRecord ) p1 ).getHostingRecord ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getHostingRecord()Lcom/android/server/am/HostingRecord;
/* if-nez v1, :cond_1 */
/* .line 815 */
} // :cond_1
(( com.android.server.am.ProcessRecord ) p1 ).getHostingRecord ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getHostingRecord()Lcom/android/server/am/HostingRecord;
(( com.android.server.am.HostingRecord ) v1 ).getType ( ); // invoke-virtual {v1}, Lcom/android/server/am/HostingRecord;->getType()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 816 */
/* :catch_0 */
/* move-exception v1 */
/* .line 817 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Fail to check hostrecord type."; // const-string v4, "Fail to check hostrecord type."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 819 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* .line 813 */
} // :cond_2
} // :goto_0
} // .end method
public void notifyAudioFocusChanged ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "callingPackageName" # Ljava/lang/String; */
/* .line 413 */
v0 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
/* .line 414 */
return;
/* .line 416 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 417 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Getting audio focus: "; // const-string v1, "Getting audio focus: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.util.Slog .i ( v1,v0 );
/* .line 419 */
} // :cond_1
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 420 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 421 */
return;
} // .end method
public void notifyIdleUpdate ( ) {
/* .locals 2 */
/* .line 865 */
v0 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 866 */
} // :cond_0
v0 = this.mHandler;
/* const/16 v1, 0xb */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 867 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 868 */
return;
} // .end method
public void reportLaunchEvent ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "launchType" # I */
/* .param p3, "launchDurationMs" # I */
/* .line 445 */
v0 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 446 */
} // :cond_0
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).obtainMessage ( v1, p2, p3, p1 ); // invoke-virtual {v0, v1, p2, p3, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 448 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 449 */
return;
} // .end method
public void reportMemPressure ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "newPressureState" # I */
/* .line 893 */
v0 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
/* .line 894 */
return;
/* .line 896 */
} // :cond_0
v0 = this.mHandler;
/* const/16 v1, 0xc */
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).obtainMessage ( v1, p1, v2 ); // invoke-virtual {v0, v1, p1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(III)Landroid/os/Message;
/* .line 897 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessageAtFrontOfQueue ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z
/* .line 898 */
return;
} // .end method
public void reportProcDied ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 6 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 789 */
v0 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 790 */
} // :cond_0
v0 = this.mAliveEmptyProcs;
/* monitor-enter v0 */
/* .line 791 */
try { // :try_start_0
v1 = this.mAliveEmptyProcs;
v2 = this.processName;
v1 = (( java.util.HashMap ) v1 ).containsKey ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 792 */
v1 = (( com.android.server.am.ProcessRecord ) p1 ).isKilledByAm ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* const-wide/16 v2, 0x1 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 793 */
/* iget-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMKilledProcs:J */
/* add-long/2addr v4, v2 */
/* iput-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mAMKilledProcs:J */
/* .line 794 */
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
v5 = this.processName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " is killed by am."; // const-string v5, " is killed by am."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v4 );
/* .line 796 */
} // :cond_1
/* iget-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mNativeKilledProcs:J */
/* add-long/2addr v4, v2 */
/* iput-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mNativeKilledProcs:J */
/* .line 797 */
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
v5 = this.processName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " is killed by native."; // const-string v5, " is killed by native."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v4 );
/* .line 799 */
} // :goto_0
/* iget-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mKilledProcs:J */
/* add-long/2addr v4, v2 */
/* iput-wide v4, p0, Lcom/android/server/am/ProcessProphetImpl;->mKilledProcs:J */
/* .line 800 */
v1 = this.mAliveEmptyProcs;
v2 = this.processName;
(( java.util.HashMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 801 */
/* sget-boolean v1, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "remove: "; // const-string v3, "remove: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.processName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " as died."; // const-string v3, " as died."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 803 */
} // :cond_2
/* monitor-exit v0 */
/* .line 804 */
return;
/* .line 803 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void reportProcStarted ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pid" # I */
/* .line 721 */
v0 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isPredictedProc ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetImpl;->isPredictedProc(Lcom/android/server/am/ProcessRecord;)Z
/* if-nez v0, :cond_0 */
return;
/* .line 722 */
} // :cond_0
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).obtainMessage ( v1, p2, v2, p1 ); // invoke-virtual {v0, v1, p2, v2, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 723 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 724 */
return;
} // .end method
public void testBT ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "targetProcName" # Ljava/lang/String; */
/* .line 1013 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
if ( p1 != null) { // if-eqz p1, :cond_1
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1016 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enter BT test mode, target: "; // const-string v2, "enter BT test mode, target: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1014 */
} // :cond_1
} // :goto_0
/* const-string/jumbo v1, "target proc name error!" */
android.util.Slog .d ( v0,v1 );
/* .line 1019 */
} // :goto_1
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->reset()V
/* .line 1022 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_2
int v1 = 5; // const/4 v1, 0x5
/* if-ge v0, v1, :cond_2 */
/* .line 1023 */
v1 = this.mHandler;
int v2 = 2; // const/4 v2, 0x2
/* const-string/jumbo v3, "testBT" */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).obtainMessage ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1024 */
v1 = this.mHandler;
int v2 = 4; // const/4 v2, 0x4
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).obtainMessage ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1025 */
v1 = this.mHandler;
int v2 = 3; // const/4 v2, 0x3
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1022 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1028 */
} // .end local v0 # "i":I
} // :cond_2
(( com.android.server.am.ProcessProphetImpl ) p0 ).notifyIdleUpdate ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->notifyIdleUpdate()V
/* .line 1029 */
return;
} // .end method
public void testLU ( java.lang.String p0 ) {
/* .locals 7 */
/* .param p1, "targetProcName" # Ljava/lang/String; */
/* .line 990 */
final String v0 = "ProcessProphet"; // const-string v0, "ProcessProphet"
if ( p1 != null) { // if-eqz p1, :cond_1
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 993 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enter LU test mode, target: "; // const-string v2, "enter LU test mode, target: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 991 */
} // :cond_1
} // :goto_0
/* const-string/jumbo v1, "target proc name error!" */
android.util.Slog .d ( v0,v1 );
/* .line 996 */
} // :goto_1
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).reset ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->reset()V
/* .line 999 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_2
int v1 = 5; // const/4 v1, 0x5
/* if-ge v0, v1, :cond_2 */
/* .line 1000 */
v2 = this.mHandler;
/* mul-int/lit8 v3, v0, 0x14 */
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).obtainMessage ( v1, v4, v3, p1 ); // invoke-virtual {v2, v1, v4, v3, p1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 1001 */
/* .local v2, "msg1":Landroid/os/Message; */
v3 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v3 ).sendMessage ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1002 */
v3 = this.mHandler;
/* mul-int/lit8 v5, v0, 0x14 */
final String v6 = "com.miui.home"; // const-string v6, "com.miui.home"
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v3 ).obtainMessage ( v1, v4, v5, v6 ); // invoke-virtual {v3, v1, v4, v5, v6}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 1003 */
/* .local v1, "msg2":Landroid/os/Message; */
v3 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v3 ).sendMessage ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 999 */
} // .end local v1 # "msg2":Landroid/os/Message;
} // .end local v2 # "msg1":Landroid/os/Message;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1006 */
} // .end local v0 # "i":I
} // :cond_2
(( com.android.server.am.ProcessProphetImpl ) p0 ).notifyIdleUpdate ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->notifyIdleUpdate()V
/* .line 1007 */
return;
} // .end method
public void testMTBF ( ) {
/* .locals 8 */
/* .line 1035 */
v0 = (( com.android.server.am.ProcessProphetImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1036 */
v0 = this.mProcessProphetModel;
(( com.android.server.am.ProcessProphetModel ) v0 ).testMTBF ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel;->testMTBF()V
/* .line 1037 */
/* new-instance v0, Ljava/util/Random; */
/* invoke-direct {v0}, Ljava/util/Random;-><init>()V */
/* const/16 v1, 0xa */
v0 = (( java.util.Random ) v0 ).nextInt ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I
/* .line 1038 */
/* .local v0, "randomNum":I */
int v1 = 3; // const/4 v1, 0x3
final String v2 = "MTBF testing:"; // const-string v2, "MTBF testing:"
final String v3 = "ProcessProphet"; // const-string v3, "ProcessProphet"
/* if-ge v0, v1, :cond_0 */
/* .line 1039 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " Prediction"; // const-string v2, " Prediction"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 1040 */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetImpl;->triggerPrediction()V */
/* goto/16 :goto_0 */
/* .line 1041 */
} // :cond_0
int v1 = 6; // const/4 v1, 0x6
/* if-ge v0, v1, :cond_1 */
/* .line 1042 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " BT connection"; // const-string v2, " BT connection"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v1 );
/* .line 1043 */
v1 = this.mHandler;
int v2 = 2; // const/4 v2, 0x2
/* const-string/jumbo v4, "testMTBF" */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).obtainMessage ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1044 */
/* .local v1, "msg":Landroid/os/Message; */
v2 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1045 */
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_1
/* const/16 v4, 0x8 */
/* if-ge v0, v4, :cond_2 */
/* .line 1046 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " copy v.douyin.com"; // const-string v4, " copy v.douyin.com"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1047 */
v2 = this.mHandler;
/* const-string/jumbo v4, "v.douyin.com" */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).obtainMessage ( v1, v4 ); // invoke-virtual {v2, v1, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1048 */
/* .restart local v1 # "msg":Landroid/os/Message; */
v2 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1049 */
} // .end local v1 # "msg":Landroid/os/Message;
/* .line 1050 */
} // :cond_2
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " copy m.tb.cn"; // const-string v4, " copy m.tb.cn"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1051 */
v2 = this.mHandler;
final String v4 = "m.tb.cn"; // const-string v4, "m.tb.cn"
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).obtainMessage ( v1, v4 ); // invoke-virtual {v2, v1, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1052 */
/* .restart local v1 # "msg":Landroid/os/Message; */
v2 = this.mHandler;
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).sendMessage ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 1055 */
} // .end local v1 # "msg":Landroid/os/Message;
} // :goto_0
v1 = this.mHandler;
/* const/16 v2, 0xe */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 1056 */
/* .restart local v1 # "msg":Landroid/os/Message; */
v2 = this.mHandler;
/* int-to-long v4, v0 */
/* const-wide/32 v6, 0xea60 */
/* mul-long/2addr v4, v6 */
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v2 ).sendMessageDelayed ( v1, v4, v5 ); // invoke-virtual {v2, v1, v4, v5}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1057 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MTBF testing: next event after "; // const-string v4, "MTBF testing: next event after "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " mins"; // const-string v4, " mins"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 1059 */
} // .end local v0 # "randomNum":I
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_3
return;
} // .end method
public void updateEnable ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "newEnable" # Z */
/* .line 233 */
/* iput-boolean p1, p0, Lcom/android/server/am/ProcessProphetImpl;->mEnable:Z */
/* .line 234 */
/* if-nez p1, :cond_0 */
/* .line 235 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V
/* .line 236 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V
/* .line 237 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V
/* .line 238 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
(( com.android.server.am.ProcessProphetImpl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->removeMessages(I)V
/* .line 240 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 241 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mEnable change to"; // const-string v1, "mEnable change to"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = "complete."; // const-string v1, "complete."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.util.Slog .i ( v1,v0 );
/* .line 243 */
} // :cond_1
return;
} // .end method
public void updateImplThreshold ( Long p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "newThres" # J */
/* .param p3, "updateName" # Ljava/lang/String; */
/* .line 254 */
final String v0 = "memPress"; // const-string v0, "memPress"
v0 = (( java.lang.String ) p3 ).equals ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const-wide/16 v1, 0x400 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 255 */
/* mul-long/2addr v1, p1 */
/* sput-wide v1, Lcom/android/server/am/ProcessProphetImpl;->MEM_THRESHOLD:J */
/* .line 256 */
} // :cond_0
final String v0 = "emptyMem"; // const-string v0, "emptyMem"
v0 = (( java.lang.String ) p3 ).equals ( v0 ); // invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 257 */
/* mul-long/2addr v1, p1 */
/* sput-wide v1, Lcom/android/server/am/ProcessProphetImpl;->EMPTY_PROCS_PSS_THRESHOLD:J */
/* .line 259 */
} // :cond_1
} // :goto_0
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 260 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " change to "; // const-string v1, " change to "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " complete."; // const-string v1, " complete."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.util.Slog .i ( v1,v0 );
/* .line 262 */
} // :cond_2
return;
} // .end method
public void updateList ( java.lang.String[] p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "newList" # [Ljava/lang/String; */
/* .param p2, "updateName" # Ljava/lang/String; */
/* .line 265 */
/* const-string/jumbo v0, "whitelist" */
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 266 */
/* array-length v0, p1 */
} // :goto_0
/* if-ge v1, v0, :cond_3 */
/* aget-object v2, p1, v1 */
/* .line 267 */
/* .local v2, "str":Ljava/lang/String; */
v3 = com.android.server.am.ProcessProphetImpl.sWhiteListEmptyProc;
v3 = (( java.util.HashSet ) v3 ).contains ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 268 */
v3 = com.android.server.am.ProcessProphetImpl.sWhiteListEmptyProc;
(( java.util.HashSet ) v3 ).remove ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
/* .line 266 */
} // .end local v2 # "str":Ljava/lang/String;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 271 */
} // :cond_1
final String v0 = "blakclist"; // const-string v0, "blakclist"
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 272 */
/* array-length v0, p1 */
} // :goto_1
/* if-ge v1, v0, :cond_3 */
/* aget-object v2, p1, v1 */
/* .line 273 */
/* .restart local v2 # "str":Ljava/lang/String; */
v3 = com.android.server.am.ProcessProphetImpl.sBlackListLaunch;
v3 = (( java.util.HashSet ) v3 ).contains ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
/* if-nez v3, :cond_2 */
/* .line 274 */
v3 = com.android.server.am.ProcessProphetImpl.sBlackListLaunch;
(( java.util.HashSet ) v3 ).add ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 272 */
} // .end local v2 # "str":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v1, v1, 0x1 */
/* .line 278 */
} // :cond_3
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 279 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " change complete."; // const-string v1, " change complete."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.util.Slog .i ( v1,v0 );
/* .line 281 */
} // :cond_4
return;
} // .end method
public void updateTrackInterval ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "intervalT" # I */
/* .line 246 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetImpl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 247 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "TRACK_INTERVAL_TIME"; // const-string v1, "TRACK_INTERVAL_TIME"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "change to"; // const-string v1, "change to"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "complete."; // const-string v1, "complete."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphet"; // const-string v1, "ProcessProphet"
android.util.Slog .i ( v1,v0 );
/* .line 250 */
} // :cond_0
/* .line 251 */
return;
} // .end method
