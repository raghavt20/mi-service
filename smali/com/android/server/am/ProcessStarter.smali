.class public Lcom/android/server/am/ProcessStarter;
.super Ljava/lang/Object;
.source "ProcessStarter.java"


# static fields
.field private static final APP_PROTECTION_TIMEOUT:I = 0x1b7740

.field private static final CAMERA_PACKAGE_NAME:Ljava/lang/String; = "com.android.camera"

.field public static final CONFIG_CAM_LOWMEMDEV_RESTART:Z

.field public static final CONFIG_CAM_RESTART_3G:Z

.field public static final CONFIG_CAM_RESTART_4G:Z

.field private static final DISABLE_PROC_PROTECT_THRESHOLD:I = 0x5

.field private static final DISABLE_RESTART_PROC_THRESHOLD:I = 0x3

.field private static final MAX_PROTECT_APP:I = 0x5

.field private static final PROCESS_RESTART_TIMEOUT:J = 0x7d0L

.field public static final REDUCE_DIED_PROC_COUNT_DELAY:J = 0x927c0L

.field private static final SKIP_PRELOAD_COUNT_LIMIT:I = 0x2

.field static final SKIP_PRELOAD_KILLED_TIME_OUT:J = 0x493e0L

.field private static final SKIP_RESTART_COUNT_LIMIT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ProcessStarter"

.field private static sNoCheckRestartCallerPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sProcRestartCallerWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sProcRestartWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sProtectProcessList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAms:Lcom/android/server/am/ActivityManagerService;

.field private mCameraStartTime:J

.field private mForegroundPackageName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field public mKilledProcessRecordMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLastProcessesInfo:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Lcom/android/server/am/ProcessPriorityInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPms:Lcom/android/server/am/ProcessManagerService;

.field private mProcessDiedRecordMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

.field private mSystemReady:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmAms(Lcom/android/server/am/ProcessStarter;)Lcom/android/server/am/ActivityManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessStarter;->mAms:Lcom/android/server/am/ActivityManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPms(Lcom/android/server/am/ProcessStarter;)Lcom/android/server/am/ProcessManagerService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProcessDiedRecordMap(Lcom/android/server/am/ProcessStarter;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/ProcessStarter;->mProcessDiedRecordMap:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mstartProcessLocked(Lcom/android/server/am/ProcessStarter;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/android/server/am/ProcessRecord;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/ProcessStarter;->startProcessLocked(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/android/server/am/ProcessRecord;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessStarter;->sProcRestartWhiteList:Ljava/util/List;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessStarter;->sProtectProcessList:Ljava/util/List;

    .line 94
    nop

    .line 95
    const-string v1, "persist.sys.cam_lowmem_restart"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_LOWMEMDEV_RESTART:Z

    .line 97
    nop

    .line 98
    const-string v1, "persist.sys.cam_4glowmem_restart"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_RESTART_4G:Z

    .line 100
    nop

    .line 101
    const-string v1, "persist.sys.cam_3glowmem_restart"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_RESTART_3G:Z

    .line 103
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/am/ProcessStarter;->sProcRestartCallerWhiteList:Ljava/util/List;

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/am/ProcessStarter;->sNoCheckRestartCallerPackages:Ljava/util/ArrayList;

    .line 107
    const-string v1, "com.tencent.mm"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v1, "com.tencent.mm:push"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    sget-object v0, Lcom/android/server/am/ProcessStarter;->sNoCheckRestartCallerPackages:Ljava/util/ArrayList;

    const-string v2, "Webview"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    sget-object v0, Lcom/android/server/am/ProcessStarter;->sNoCheckRestartCallerPackages:Ljava/util/ArrayList;

    const-string v2, "SdkSandbox"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    sget-object v0, Lcom/android/server/am/ProcessStarter;->sProcRestartWhiteList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    sget-object v0, Lcom/android/server/am/ProcessStarter;->sProcRestartCallerWhiteList:Ljava/util/List;

    const-string v1, "com.android.systemui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    return-void
.end method

.method public constructor <init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ActivityManagerService;Landroid/os/Handler;)V
    .locals 1
    .param p1, "pms"    # Lcom/android/server/am/ProcessManagerService;
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/ProcessStarter;->mSystemReady:Z

    .line 58
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessStarter;->mKilledProcessRecordMap:Ljava/util/Map;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessStarter;->mProcessDiedRecordMap:Ljava/util/Map;

    .line 78
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessStarter;->mLastProcessesInfo:Landroid/util/SparseArray;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/ProcessStarter;->mForegroundPackageName:Ljava/lang/String;

    .line 116
    iput-object p1, p0, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    .line 117
    iput-object p2, p0, Lcom/android/server/am/ProcessStarter;->mAms:Lcom/android/server/am/ActivityManagerService;

    .line 118
    iput-object p3, p0, Lcom/android/server/am/ProcessStarter;->mHandler:Landroid/os/Handler;

    .line 119
    return-void
.end method

.method private increaseRecordCount(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 394
    .local p2, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 395
    .local v0, "expCount":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 396
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 398
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v0, v1

    invoke-interface {p2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    return-void
.end method

.method private isProcRestartInterceptScenario()Z
    .locals 2

    .line 369
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessStarter;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController;->isGameApp(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mForegroundPackageName:Ljava/lang/String;

    .line 370
    const-string v1, "com.android.camera"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 371
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->isStartingApp()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 369
    :goto_1
    return v0
.end method

.method private isSystemApp(II)Z
    .locals 1
    .param p1, "flag"    # I
    .param p2, "uid"    # I

    .line 363
    and-int/lit16 v0, p1, 0x81

    if-nez v0, :cond_1

    const/16 v0, 0x3e8

    if-ne v0, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public static makeHostingTypeFromFlag(I)Ljava/lang/String;
    .locals 1
    .param p0, "flag"    # I

    .line 551
    packed-switch p0, :pswitch_data_0

    .line 559
    const-string/jumbo v0, "unknown"

    .local v0, "type":Ljava/lang/String;
    goto :goto_0

    .line 556
    .end local v0    # "type":Ljava/lang/String;
    :pswitch_0
    const-string v0, "FastRestart"

    .line 557
    .restart local v0    # "type":Ljava/lang/String;
    goto :goto_0

    .line 553
    .end local v0    # "type":Ljava/lang/String;
    :pswitch_1
    const-string v0, "AI"

    .line 554
    .restart local v0    # "type":Ljava/lang/String;
    nop

    .line 562
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private reduceRecordCountDelay(Ljava/lang/String;Ljava/util/Map;J)V
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;
    .param p3, "delay"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;J)V"
        }
    .end annotation

    .line 376
    .local p2, "recordMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/am/ProcessStarter$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/server/am/ProcessStarter$3;-><init>(Lcom/android/server/am/ProcessStarter;Ljava/util/Map;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 391
    return-void
.end method

.method private startProcessLocked(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/android/server/am/ProcessRecord;
    .locals 20
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "userId"    # I
    .param p4, "hostingType"    # Ljava/lang/String;

    .line 131
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v3, "ProcessStarter"

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    .line 132
    .local v4, "callingPid":I
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    move-object/from16 v0, p1

    move-object v15, v0

    .end local p2    # "processName":Ljava/lang/String;
    .local v0, "processName":Ljava/lang/String;
    goto :goto_0

    .line 132
    .end local v0    # "processName":Ljava/lang/String;
    .restart local p2    # "processName":Ljava/lang/String;
    :cond_0
    move-object/from16 v15, p2

    .line 135
    .end local p2    # "processName":Ljava/lang/String;
    .local v15, "processName":Ljava/lang/String;
    :goto_0
    const/4 v5, 0x0

    .line 137
    .local v5, "info":Landroid/content/pm/ApplicationInfo;
    const/4 v14, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    const-wide/16 v6, 0x400

    move/from16 v13, p3

    invoke-interface {v0, v2, v6, v7, v13}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    .end local v5    # "info":Landroid/content/pm/ApplicationInfo;
    .local v0, "info":Landroid/content/pm/ApplicationInfo;
    nop

    .line 143
    if-eqz v0, :cond_5

    .line 144
    iget-object v5, v1, Lcom/android/server/am/ProcessStarter;->mAms:Lcom/android/server/am/ActivityManagerService;

    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getProcessNamesLOSP()Lcom/android/server/am/ProcessList$MyProcessMap;

    move-result-object v5

    iget v6, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v5, v15, v6}, Lcom/android/server/am/ProcessList$MyProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v5

    move-object/from16 v16, v5

    check-cast v16, Lcom/android/server/am/ProcessRecord;

    .line 145
    .local v16, "app":Lcom/android/server/am/ProcessRecord;
    const-string v5, "process: "

    if-eqz v16, :cond_2

    .line 146
    invoke-virtual/range {v16 .. v16}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 147
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is persistent, skip!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    return-object v14

    .line 150
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " already exits, just protect"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    return-object v16

    .line 154
    :cond_2
    iget-object v6, v1, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v6, v2}, Lcom/android/server/am/ProcessManagerService;->isAppDisabledForPkms(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 155
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is disabled or suspend, skip!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-object v14

    .line 158
    :cond_3
    iget-object v5, v1, Lcom/android/server/am/ProcessStarter;->mAms:Lcom/android/server/am/ActivityManagerService;

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/android/server/am/HostingRecord;

    move-object/from16 v12, p4

    invoke-direct {v10, v12, v15}, Lcom/android/server/am/HostingRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v11, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 161
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/android/server/am/ActivityManagerServiceStub;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v19

    .line 158
    move-object v6, v15

    move-object v7, v0

    move/from16 v12, v17

    move/from16 v13, v18

    move-object v2, v14

    move-object/from16 v14, v19

    invoke-virtual/range {v5 .. v14}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILcom/android/server/am/HostingRecord;IZZLjava/lang/String;)Lcom/android/server/am/ProcessRecord;

    move-result-object v5

    .line 163
    .local v5, "newApp":Lcom/android/server/am/ProcessRecord;
    if-nez v5, :cond_4

    .line 164
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "startProcess :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    return-object v2

    .line 167
    :cond_4
    iget-object v3, v1, Lcom/android/server/am/ProcessStarter;->mAms:Lcom/android/server/am/ActivityManagerService;

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6, v2}, Lcom/android/server/am/ActivityManagerService;->updateLruProcessLocked(Lcom/android/server/am/ProcessRecord;ZLcom/android/server/am/ProcessRecord;)V

    .line 168
    return-object v5

    .line 170
    .end local v5    # "newApp":Lcom/android/server/am/ProcessRecord;
    .end local v16    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_5
    move-object v2, v14

    return-object v2

    .line 139
    .end local v0    # "info":Landroid/content/pm/ApplicationInfo;
    .local v5, "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v0

    move-object v2, v14

    .line 140
    .local v0, "e":Landroid/os/RemoteException;
    const-string v6, "error in getApplicationInfo!"

    invoke-static {v3, v6, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 141
    return-object v2
.end method


# virtual methods
.method addProtectionLocked(Lcom/android/server/am/ProcessRecord;I)V
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "flag"    # I

    .line 516
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 518
    :pswitch_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    sget v1, Lmiui/process/ProcessManager;->AI_MAX_ADJ:I

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    .line 519
    const/16 v0, 0xd

    invoke-static {p1, v0}, Lcom/android/server/am/IProcessPolicy;->setAppMaxProcState(Lcom/android/server/am/ProcessRecord;I)V

    .line 520
    nop

    .line 524
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public foregroundActivityChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 435
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    return-void

    .line 438
    :cond_0
    iput-object p1, p0, Lcom/android/server/am/ProcessStarter;->mForegroundPackageName:Ljava/lang/String;

    .line 439
    const-string v0, "com.android.camera"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessStarter;->mCameraStartTime:J

    .line 442
    :cond_1
    return-void
.end method

.method public frequentlyKilledForPreload(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 421
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mKilledProcessRecordMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 422
    .local v1, "killedProcess":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/ProcessStarter;->mKilledProcessRecordMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 423
    .local v2, "count":Ljava/lang/Integer;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gtz v3, :cond_1

    .line 424
    goto :goto_0

    .line 426
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    .line 427
    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 428
    const/4 v0, 0x1

    return v0

    .line 430
    .end local v1    # "killedProcess":Ljava/lang/String;
    .end local v2    # "count":Ljava/lang/Integer;
    :cond_2
    goto :goto_0

    .line 431
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method public isAllowPreloadProcess(Ljava/util/List;I)Z
    .locals 5
    .param p2, "flag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiui/process/PreloadProcessData;",
            ">;I)Z"
        }
    .end annotation

    .line 403
    .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/PreloadProcessData;>;"
    and-int/lit8 v0, p2, 0x1

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    .line 404
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 405
    .local v0, "dataIterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 406
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/process/PreloadProcessData;

    .line 407
    .local v2, "data":Lmiui/process/PreloadProcessData;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 408
    invoke-virtual {v2}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/am/ProcessStarter;->frequentlyKilledForPreload(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 409
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 410
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip start "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", because of errors or killed by user before"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ProcessStarter"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    .end local v2    # "data":Lmiui/process/PreloadProcessData;
    :cond_0
    goto :goto_0

    .line 415
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 417
    .end local v0    # "dataIterator":Ljava/util/Iterator;
    :cond_3
    return v1
.end method

.method public isAllowRestartProcessLock(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/android/server/am/HostingRecord;)Z
    .locals 16
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "flag"    # I
    .param p3, "uid"    # I
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "callerPackage"    # Ljava/lang/String;
    .param p6, "hostingRecord"    # Lcom/android/server/am/HostingRecord;

    .line 222
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    sget-boolean v6, Landroid/os/spc/PressureStateSettings;->PROCESS_RESTART_INTERCEPT_ENABLE:Z

    const/4 v7, 0x1

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/android/server/am/ProcessStarter;->mSystemReady:Z

    if-eqz v6, :cond_4

    if-eqz p6, :cond_4

    .line 225
    invoke-virtual/range {p6 .. p6}, Lcom/android/server/am/HostingRecord;->getType()Ljava/lang/String;

    move-result-object v6

    const-string v8, "activity"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 226
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/ProcessStarter;->isProcRestartInterceptScenario()Z

    move-result v6

    if-eqz v6, :cond_4

    sget-boolean v6, Lcom/miui/app/smartpower/SmartPowerPolicyConstants;->TESTSUITSPECIFIC:Z

    if-nez v6, :cond_4

    iget-object v6, v0, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    .line 228
    invoke-virtual {v6, v4, v3}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_4

    sget-object v6, Lcom/android/server/am/ProcessStarter;->sProcRestartWhiteList:Ljava/util/List;

    .line 229
    invoke-interface {v6, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    sget-object v6, Lcom/android/server/am/ProcessStarter;->sProcRestartCallerWhiteList:Ljava/util/List;

    .line 230
    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, v0, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    .line 231
    invoke-virtual {v6}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, v0, Lcom/android/server/am/ProcessStarter;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 232
    const/16 v8, 0x1d8

    invoke-interface {v6, v8, v4, v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->isProcessWhiteList(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_0

    .line 238
    :cond_0
    invoke-virtual {v0, v5}, Lcom/android/server/am/ProcessStarter;->isProcessPerceptible(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 239
    return v7

    .line 243
    :cond_1
    const-string v6, "com.android.camera"

    iget-object v8, v0, Lcom/android/server/am/ProcessStarter;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    const/4 v8, 0x0

    const-string v9, "ProcessStarter"

    if-eqz v6, :cond_2

    .line 244
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 245
    .local v10, "nowTime":J
    iget-wide v12, v0, Lcom/android/server/am/ProcessStarter;->mCameraStartTime:J

    sub-long v12, v10, v12

    const-wide/16 v14, 0x1388

    cmp-long v6, v12, v14

    if-gez v6, :cond_2

    .line 247
    invoke-direct {v0, v2, v3}, Lcom/android/server/am/ProcessStarter;->isSystemApp(II)Z

    move-result v6

    if-nez v6, :cond_2

    .line 248
    const-string v6, "camera start scenario!"

    invoke-static {v9, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    return v8

    .line 253
    .end local v10    # "nowTime":J
    :cond_2
    iget-object v6, v0, Lcom/android/server/am/ProcessStarter;->mProcessDiedRecordMap:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 254
    .local v6, "procDiedCount":Ljava/lang/Integer;
    if-eqz v6, :cond_3

    .line 255
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x3

    if-le v10, v11, :cond_3

    .line 256
    invoke-direct {v0, v2, v3}, Lcom/android/server/am/ProcessStarter;->isSystemApp(II)Z

    move-result v10

    if-nez v10, :cond_3

    .line 257
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "proc frequent died! proc = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " callerPkg = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    return v8

    .line 261
    :cond_3
    return v7

    .line 235
    .end local v6    # "procDiedCount":Ljava/lang/Integer;
    :cond_4
    :goto_0
    return v7
.end method

.method public isProcessPerceptible(ILjava/lang/String;)Z
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 466
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 467
    invoke-interface {v0, p1, p2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getLruProcesses(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 468
    .local v0, "runningProcesses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 469
    .local v2, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v2}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isProcessPerceptible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 470
    const/4 v1, 0x1

    return v1

    .line 472
    .end local v2    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_0
    goto :goto_0

    .line 473
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public isProcessPerceptible(Ljava/lang/String;)Z
    .locals 7
    .param p1, "callerPackage"    # Ljava/lang/String;

    .line 477
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAllAppState()Ljava/util/ArrayList;

    move-result-object v0

    .line 478
    .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState;>;"
    const/4 v1, 0x1

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_3

    .line 481
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/smartpower/IAppState;

    .line 482
    .local v3, "appState":Lcom/miui/server/smartpower/IAppState;
    sget-object v4, Lcom/android/server/am/ProcessStarter;->sNoCheckRestartCallerPackages:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 483
    goto :goto_2

    .line 485
    :cond_1
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 486
    goto :goto_0

    .line 489
    :cond_2
    invoke-virtual {v3}, Lcom/miui/server/smartpower/IAppState;->getRunningProcessList()Ljava/util/ArrayList;

    move-result-object v2

    .line 490
    .local v2, "runningProcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 491
    .local v5, "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v5}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->isProcessPerceptible()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 492
    return v1

    .line 494
    .end local v5    # "runningProc":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_3
    goto :goto_1

    .line 495
    :cond_4
    nop

    .line 497
    .end local v2    # "runningProcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    .end local v3    # "appState":Lcom/miui/server/smartpower/IAppState;
    :cond_5
    :goto_2
    const/4 v1, 0x0

    return v1

    .line 479
    :cond_6
    :goto_3
    return v1
.end method

.method public recordDiedProcessIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "uid"    # I

    .line 279
    if-eqz p2, :cond_1

    .line 280
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mProcessDiedRecordMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 281
    .local v0, "count":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 282
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 284
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ProcessStarter;->mProcessDiedRecordMap:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v0, v2

    invoke-interface {v1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    iget-object v1, p0, Lcom/android/server/am/ProcessStarter;->mProcessDiedRecordMap:Ljava/util/Map;

    const-wide/32 v2, 0x927c0

    invoke-direct {p0, p2, v1, v2, v3}, Lcom/android/server/am/ProcessStarter;->reduceRecordCountDelay(Ljava/lang/String;Ljava/util/Map;J)V

    .line 289
    .end local v0    # "count":Ljava/lang/Integer;
    :cond_1
    sget-object v0, Lcom/android/server/am/ProcessStarter;->sProtectProcessList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/am/ProcessStarter$1;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/android/server/am/ProcessStarter$1;-><init>(Lcom/android/server/am/ProcessStarter;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 310
    :cond_2
    return-void
.end method

.method public recordKillProcessIfNeeded(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .line 266
    sget-object v0, Lcom/android/server/am/ProcessPolicy;->sLowMemKillProcReasons:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 267
    .local v1, "lowMemReason":Ljava/lang/String;
    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    iget-object v2, p0, Lcom/android/server/am/ProcessStarter;->mKilledProcessRecordMap:Ljava/util/Map;

    invoke-direct {p0, p1, v2}, Lcom/android/server/am/ProcessStarter;->increaseRecordCount(Ljava/lang/String;Ljava/util/Map;)V

    .line 271
    iget-object v2, p0, Lcom/android/server/am/ProcessStarter;->mKilledProcessRecordMap:Ljava/util/Map;

    const-wide/32 v3, 0x493e0

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/server/am/ProcessStarter;->reduceRecordCountDelay(Ljava/lang/String;Ljava/util/Map;J)V

    .line 274
    .end local v1    # "lowMemReason":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 276
    :cond_1
    return-void
.end method

.method public restartCameraIfNeeded(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "uid"    # I

    .line 314
    const-string v0, "com.android.camera"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    return-void

    .line 318
    :cond_0
    sget v0, Lmiui/os/Build;->TOTAL_RAM:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x4

    if-gt v0, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 319
    .local v0, "isLowMemDevice":Z
    :goto_0
    sget v4, Lmiui/os/Build;->TOTAL_RAM:I

    const/4 v5, 0x3

    if-gt v4, v3, :cond_2

    sget v4, Lmiui/os/Build;->TOTAL_RAM:I

    if-le v4, v5, :cond_2

    move v4, v1

    goto :goto_1

    :cond_2
    move v4, v2

    .line 320
    .local v4, "is4GMemDevice":Z
    :goto_1
    sget v6, Lmiui/os/Build;->TOTAL_RAM:I

    if-gt v6, v5, :cond_3

    sget v5, Lmiui/os/Build;->TOTAL_RAM:I

    const/4 v6, 0x2

    if-le v5, v6, :cond_3

    goto :goto_2

    :cond_3
    move v1, v2

    .line 323
    .local v1, "is3GMemDevice":Z
    :goto_2
    const-string v2, "ProcessStarter"

    if-eqz v0, :cond_7

    .line 324
    const-string v5, " as demanded!"

    if-eqz v4, :cond_5

    sget-boolean v6, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_RESTART_4G:Z

    if-nez v6, :cond_4

    sget-boolean v6, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_LOWMEMDEV_RESTART:Z

    if-eqz v6, :cond_5

    .line 325
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "4G Mem device restart "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 326
    :cond_5
    if-eqz v1, :cond_6

    sget-boolean v6, Lcom/android/server/am/ProcessStarter;->CONFIG_CAM_RESTART_3G:Z

    if-eqz v6, :cond_6

    .line 327
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "3G Mem device restart "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 329
    :cond_6
    const-string/jumbo v3, "skip restart camera due to lowMemDevice"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    return-void

    .line 334
    :cond_7
    :goto_3
    invoke-static {p3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    .line 335
    .local v5, "userId":I
    iget-object v6, p0, Lcom/android/server/am/ProcessStarter;->mAms:Lcom/android/server/am/ActivityManagerService;

    iget-object v6, v6, Lcom/android/server/am/ActivityManagerService;->mUserController:Lcom/android/server/am/UserController;

    invoke-virtual {v6, v5, v3}, Lcom/android/server/am/UserController;->isUserRunning(II)Z

    move-result v3

    if-nez v3, :cond_8

    .line 336
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "user "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " is still locked. Cannot restart process "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    return-void

    .line 341
    :cond_8
    iget-object v2, p0, Lcom/android/server/am/ProcessStarter;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/server/am/ProcessStarter$2;

    invoke-direct {v3, p0, p1, p3, p2}, Lcom/android/server/am/ProcessStarter$2;-><init>(Lcom/android/server/am/ProcessStarter;Ljava/lang/String;ILjava/lang/String;)V

    const-wide/16 v6, 0x7d0

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 360
    return-void
.end method

.method public restartDiedAppOrNot(Lcom/android/server/am/ProcessRecord;ZZ)Z
    .locals 5
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "isHomeApp"    # Z
    .param p3, "allowRestart"    # Z

    .line 445
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->isCtsModeEnable()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_7

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 446
    invoke-static {v0}, Lcom/miui/server/process/ProcessManagerInternal;->checkCtsProcess(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 447
    invoke-static {v0}, Lcom/miui/server/process/ProcessManagerInternal;->checkCtsProcess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_3

    .line 450
    :cond_0
    if-nez p2, :cond_6

    iget-boolean v0, p0, Lcom/android/server/am/ProcessStarter;->mSystemReady:Z

    if-nez v0, :cond_1

    goto :goto_2

    .line 454
    :cond_1
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I

    move-result v0

    const/16 v2, 0xa

    const/4 v3, 0x0

    if-gt v0, v2, :cond_5

    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 455
    invoke-static {v0}, Landroid/os/Process;->isIsolated(I)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    .line 458
    :cond_2
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v2, v4}, Lcom/android/server/am/ProcessManagerService;->isAllowAutoStart(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_4

    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v2, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 459
    invoke-virtual {p0, v0, v2}, Lcom/android/server/am/ProcessStarter;->isProcessPerceptible(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    .line 462
    :cond_3
    return v3

    .line 460
    :cond_4
    :goto_0
    return v1

    .line 456
    :cond_5
    :goto_1
    return v3

    .line 451
    :cond_6
    :goto_2
    return p3

    .line 448
    :cond_7
    :goto_3
    return v1
.end method

.method restoreLastProcessesInfoLocked(I)V
    .locals 6
    .param p1, "flag"    # I

    .line 528
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mLastProcessesInfo:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 529
    .local v0, "lastProcessInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPriorityInfo;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 530
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 531
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessPriorityInfo;

    .line 532
    .local v2, "process":Lcom/android/server/am/ProcessPriorityInfo;
    iget-object v3, p0, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v3}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    iget-object v4, v4, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget-object v5, v2, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    iget v5, v5, Lcom/android/server/am/ProcessRecord;->userId:I

    .line 533
    invoke-virtual {v3, v4, v5}, Lcom/android/server/am/ProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 534
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "user: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    iget v4, v4, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", packageName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    iget-object v4, v4, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " was Locked."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ProcessStarter"

    invoke-static {v4, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v3, v2, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    sget v4, Lmiui/process/ProcessManager;->LOCKED_MAX_ADJ:I

    invoke-virtual {v3, v4}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    .line 537
    iget-object v3, v2, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    sget v4, Lmiui/process/ProcessManager;->LOCKED_MAX_PROCESS_STATE:I

    invoke-static {v3, v4}, Lcom/android/server/am/IProcessPolicy;->setAppMaxProcState(Lcom/android/server/am/ProcessRecord;I)V

    goto :goto_1

    .line 541
    :cond_0
    iget-object v3, v2, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    iget v4, v2, Lcom/android/server/am/ProcessPriorityInfo;->maxAdj:I

    invoke-virtual {v3, v4}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    .line 542
    iget-object v3, v2, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    iget v4, v2, Lcom/android/server/am/ProcessPriorityInfo;->maxProcState:I

    invoke-static {v3, v4}, Lcom/android/server/am/IProcessPolicy;->setAppMaxProcState(Lcom/android/server/am/ProcessRecord;I)V

    .line 530
    .end local v2    # "process":Lcom/android/server/am/ProcessPriorityInfo;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 545
    .end local v1    # "i":I
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 547
    :cond_2
    return-void
.end method

.method saveProcessInfoLocked(Lcom/android/server/am/ProcessRecord;I)V
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "flag"    # I

    .line 502
    new-instance v0, Lcom/android/server/am/ProcessPriorityInfo;

    invoke-direct {v0}, Lcom/android/server/am/ProcessPriorityInfo;-><init>()V

    .line 503
    .local v0, "lastProcess":Lcom/android/server/am/ProcessPriorityInfo;
    iput-object p1, v0, Lcom/android/server/am/ProcessPriorityInfo;->app:Lcom/android/server/am/ProcessRecord;

    .line 504
    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v1

    iput v1, v0, Lcom/android/server/am/ProcessPriorityInfo;->maxAdj:I

    .line 505
    invoke-static {p1}, Lcom/android/server/am/IProcessPolicy;->getAppMaxProcState(Lcom/android/server/am/ProcessRecord;)I

    move-result v1

    iput v1, v0, Lcom/android/server/am/ProcessPriorityInfo;->maxProcState:I

    .line 506
    iget-object v1, p0, Lcom/android/server/am/ProcessStarter;->mLastProcessesInfo:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 507
    .local v1, "lastProcessList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPriorityInfo;>;"
    if-nez v1, :cond_0

    .line 508
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 509
    iget-object v2, p0, Lcom/android/server/am/ProcessStarter;->mLastProcessesInfo:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 511
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 512
    return-void
.end method

.method public startProcesses(Ljava/util/List;IZII)I
    .locals 15
    .param p2, "startProcessCount"    # I
    .param p3, "ignoreMemory"    # Z
    .param p4, "userId"    # I
    .param p5, "flag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiui/process/PreloadProcessData;",
            ">;IZII)I"
        }
    .end annotation

    .line 176
    .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/PreloadProcessData;>;"
    move-object v1, p0

    move/from16 v2, p2

    move/from16 v3, p5

    iget-object v4, v1, Lcom/android/server/am/ProcessStarter;->mAms:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v4

    .line 177
    const/4 v0, 0x0

    .line 178
    .local v0, "mColdStartCount":I
    const/4 v5, 0x0

    .line 179
    .local v5, "protectCount":I
    :try_start_0
    invoke-virtual {p0, v3}, Lcom/android/server/am/ProcessStarter;->restoreLastProcessesInfoLocked(I)V

    .line 180
    iget-object v6, v1, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    iget-object v6, v6, Lcom/android/server/am/ProcessManagerService;->mHandler:Lcom/android/server/am/ProcessManagerService$MainHandler;

    invoke-virtual {v6, v3}, Lcom/android/server/am/ProcessManagerService$MainHandler;->removeMessages(I)V

    .line 181
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/process/PreloadProcessData;

    .line 182
    .local v7, "preloadProcessData":Lmiui/process/PreloadProcessData;
    if-lt v0, v2, :cond_0

    .line 183
    move/from16 v11, p4

    goto :goto_2

    .line 185
    :cond_0
    move-object v8, v7

    .line 186
    .local v8, "data":Lmiui/process/PreloadProcessData;
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 187
    invoke-virtual {v8}, Lmiui/process/PreloadProcessData;->getPackageName()Ljava/lang/String;

    move-result-object v9

    .line 188
    .local v9, "process":Ljava/lang/String;
    nop

    .line 189
    invoke-static/range {p5 .. p5}, Lcom/android/server/am/ProcessStarter;->makeHostingTypeFromFlag(I)Ljava/lang/String;

    move-result-object v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    move/from16 v11, p4

    :try_start_1
    invoke-direct {p0, v9, v9, v11, v10}, Lcom/android/server/am/ProcessStarter;->startProcessLocked(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/android/server/am/ProcessRecord;

    move-result-object v10

    .line 190
    .local v10, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v10, :cond_3

    .line 191
    invoke-virtual {p0, v10, v3}, Lcom/android/server/am/ProcessStarter;->saveProcessInfoLocked(Lcom/android/server/am/ProcessRecord;I)V

    .line 192
    invoke-virtual {p0, v10, v3}, Lcom/android/server/am/ProcessStarter;->addProtectionLocked(Lcom/android/server/am/ProcessRecord;I)V

    .line 193
    add-int/lit8 v5, v5, 0x1

    .line 195
    const/4 v12, 0x5

    if-lt v5, v12, :cond_1

    .line 196
    const-string v6, "ProcessStarter"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "preload and protect processes max limit is: 5, while now count is: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v6, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    goto :goto_2

    .line 200
    :cond_1
    iget-object v12, v1, Lcom/android/server/am/ProcessStarter;->mHandler:Landroid/os/Handler;

    const-wide/32 v13, 0x1b7740

    invoke-virtual {v12, v3, v13, v14}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 186
    .end local v9    # "process":Ljava/lang/String;
    .end local v10    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_2
    move/from16 v11, p4

    .line 203
    .end local v7    # "preloadProcessData":Lmiui/process/PreloadProcessData;
    .end local v8    # "data":Lmiui/process/PreloadProcessData;
    :cond_3
    :goto_1
    goto :goto_0

    .line 181
    :cond_4
    move/from16 v11, p4

    .line 204
    :goto_2
    monitor-exit v4

    return v0

    .line 205
    .end local v0    # "mColdStartCount":I
    .end local v5    # "protectCount":I
    :catchall_0
    move-exception v0

    move/from16 v11, p4

    :goto_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method public systemReady()V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/android/server/am/ProcessStarter;->mPms:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;

    move-result-object v0

    .line 123
    .local v0, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Lcom/android/server/am/ProcessStarter;->sProcRestartWhiteList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 124
    sget-object v2, Lcom/android/server/am/ProcessStarter;->sProcRestartCallerWhiteList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 125
    const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iput-object v2, p0, Lcom/android/server/am/ProcessStarter;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    .line 126
    iput-boolean v1, p0, Lcom/android/server/am/ProcessStarter;->mSystemReady:Z

    .line 127
    return-void
.end method
