public class com.android.server.am.AppStateManager$AppState extends com.miui.server.smartpower.IAppState {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "AppState" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/android/server/am/AppStateManager$AppState$MyHandler;, */
/* Lcom/android/server/am/AppStateManager$AppState$RunningProcess;, */
/* Lcom/android/server/am/AppStateManager$AppState$DependProcInfo; */
/* } */
} // .end annotation
/* # static fields */
private static final Integer MSG_STEP_APP_STATE;
private static final Integer MSG_UPDATE_APPSTATE;
/* # instance fields */
private Integer mAppState;
private Integer mAppSwitchFgCount;
private Boolean mForeground;
private Integer mFreeFormCount;
private com.android.server.am.AppStateManager$AppState$MyHandler mHandler;
private Boolean mHasFgService;
private Boolean mHasProtectProcess;
private Boolean mInSplitMode;
private Boolean mIs64BitApp;
private Boolean mIsAutoStartApp;
private Boolean mIsBackupServiceApp;
private Boolean mIsLockedApp;
private Long mLastTopTime;
private Long mLastUidInteractiveTimeMills;
private Long mLastVideoPlayTimeStamp;
private Integer mLaunchCount;
private Integer mMainProcOomAdj;
private Long mMainProcStartTime;
private Integer mMinAmsProcState;
private Integer mMinOomAdj;
private Integer mMinPriorityScore;
private Integer mMinUsageLevel;
private Integer mOverlayCount;
private java.lang.String mPackageName;
private final java.lang.Object mProcLock;
private Integer mResourceBehavier;
private final android.util.ArrayMap mRunningProcs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/AppStateManager$AppState$RunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mScenarioActions;
private Boolean mSystemApp;
private Boolean mSystemSignature;
private Integer mTopAppCount;
private Long mTotalTimeInForeground;
private final Integer mUid;
private Boolean mWhiteListVideoPlaying;
final com.android.server.am.AppStateManager this$0; //synthetic
/* # direct methods */
static Boolean -$$Nest$fgetmForeground ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z */
} // .end method
static Integer -$$Nest$fgetmFreeFormCount ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mFreeFormCount:I */
} // .end method
static com.android.server.am.AppStateManager$AppState$MyHandler -$$Nest$fgetmHandler ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmInSplitMode ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mInSplitMode:Z */
} // .end method
static Boolean -$$Nest$fgetmIsAutoStartApp ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z */
} // .end method
static Boolean -$$Nest$fgetmIsBackupServiceApp ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsBackupServiceApp:Z */
} // .end method
static Boolean -$$Nest$fgetmIsLockedApp ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z */
} // .end method
static Long -$$Nest$fgetmLastUidInteractiveTimeMills ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmOverlayCount ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mOverlayCount:I */
} // .end method
static Integer -$$Nest$fgetmScenarioActions ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I */
} // .end method
static Boolean -$$Nest$fgetmSystemApp ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z */
} // .end method
static Boolean -$$Nest$fgetmSystemSignature ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z */
} // .end method
static Integer -$$Nest$fgetmTopAppCount ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mTopAppCount:I */
} // .end method
static Integer -$$Nest$fgetmUid ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I */
} // .end method
static void -$$Nest$fputmAppSwitchFgCount ( com.android.server.am.AppStateManager$AppState p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I */
return;
} // .end method
static void -$$Nest$fputmFreeFormCount ( com.android.server.am.AppStateManager$AppState p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mFreeFormCount:I */
return;
} // .end method
static void -$$Nest$fputmInSplitMode ( com.android.server.am.AppStateManager$AppState p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mInSplitMode:Z */
return;
} // .end method
static void -$$Nest$fputmIsAutoStartApp ( com.android.server.am.AppStateManager$AppState p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z */
return;
} // .end method
static void -$$Nest$fputmIsLockedApp ( com.android.server.am.AppStateManager$AppState p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z */
return;
} // .end method
static void -$$Nest$fputmMainProcStartTime ( com.android.server.am.AppStateManager$AppState p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcStartTime:J */
return;
} // .end method
static void -$$Nest$fputmOverlayCount ( com.android.server.am.AppStateManager$AppState p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mOverlayCount:I */
return;
} // .end method
static void -$$Nest$fputmTopAppCount ( com.android.server.am.AppStateManager$AppState p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mTopAppCount:I */
return;
} // .end method
static void -$$Nest$mcomponentEnd ( com.android.server.am.AppStateManager$AppState p0, com.android.server.am.ProcessRecord p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->componentEnd(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mcomponentEnd ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->componentEnd(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mcomponentStart ( com.android.server.am.AppStateManager$AppState p0, com.android.server.am.ProcessRecord p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->componentStart(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mcomponentStart ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->componentStart(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mhibernateAllIfNeeded ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->hibernateAllIfNeeded(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mhibernateAllInactive ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->hibernateAllInactive(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monActivityForegroundLocked ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onActivityForegroundLocked(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monAddToWhiteList ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->onAddToWhiteList()V */
return;
} // .end method
static void -$$Nest$monAddToWhiteList ( com.android.server.am.AppStateManager$AppState p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onAddToWhiteList(I)V */
return;
} // .end method
static void -$$Nest$monApplyOomAdjLocked ( com.android.server.am.AppStateManager$AppState p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onApplyOomAdjLocked(Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static void -$$Nest$monBackupChanged ( com.android.server.am.AppStateManager$AppState p0, Boolean p1, com.android.server.am.ProcessRecord p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->onBackupChanged(ZLcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static void -$$Nest$monBackupServiceAppChanged ( com.android.server.am.AppStateManager$AppState p0, Boolean p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->onBackupServiceAppChanged(ZI)V */
return;
} // .end method
static void -$$Nest$monInputMethodShow ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->onInputMethodShow()V */
return;
} // .end method
static void -$$Nest$monMediaKey ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->onMediaKey()V */
return;
} // .end method
static void -$$Nest$monMediaKey ( com.android.server.am.AppStateManager$AppState p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onMediaKey(I)V */
return;
} // .end method
static void -$$Nest$monRemoveFromWhiteList ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->onRemoveFromWhiteList()V */
return;
} // .end method
static void -$$Nest$monRemoveFromWhiteList ( com.android.server.am.AppStateManager$AppState p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onRemoveFromWhiteList(I)V */
return;
} // .end method
static void -$$Nest$monReportPowerFrozenSignal ( com.android.server.am.AppStateManager$AppState p0, Integer p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState;->onReportPowerFrozenSignal(ILjava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monReportPowerFrozenSignal ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onReportPowerFrozenSignal(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$monSendPendingIntent ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->onSendPendingIntent(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mprocessKilledLocked ( com.android.server.am.AppStateManager$AppState p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->processKilledLocked(Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static void -$$Nest$mprocessStartedLocked ( com.android.server.am.AppStateManager$AppState p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->processStartedLocked(Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static void -$$Nest$mrecordVideoPlayIfNeeded ( com.android.server.am.AppStateManager$AppState p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->recordVideoPlayIfNeeded(Z)V */
return;
} // .end method
static void -$$Nest$msetActivityVisible ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1, com.android.server.wm.ActivityRecord p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->setActivityVisible(Ljava/lang/String;Lcom/android/server/wm/ActivityRecord;Z)V */
return;
} // .end method
static void -$$Nest$msetWindowVisible ( com.android.server.am.AppStateManager$AppState p0, Integer p1, com.android.server.policy.WindowManagerPolicy$WindowState p2, android.view.WindowManager$LayoutParams p3, Boolean p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/AppStateManager$AppState;->setWindowVisible(ILcom/android/server/policy/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Z)V */
return;
} // .end method
static void -$$Nest$mstepAppState ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->stepAppState(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mupdateAppState ( com.android.server.am.AppStateManager$AppState p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->updateAppState(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mupdateCurrentResourceBehavier ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentResourceBehavier()V */
return;
} // .end method
static void -$$Nest$mupdateProcessLocked ( com.android.server.am.AppStateManager$AppState p0, com.android.server.am.ProcessRecord p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->updateProcessLocked(Lcom/android/server/am/ProcessRecord;)V */
return;
} // .end method
static void -$$Nest$mupdateProviderDepends ( com.android.server.am.AppStateManager$AppState p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ProcessRecord p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->updateProviderDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;Z)V */
return;
} // .end method
static void -$$Nest$mupdateServiceDepends ( com.android.server.am.AppStateManager$AppState p0, com.android.server.am.ProcessRecord p1, com.android.server.am.ProcessRecord p2, Boolean p3, Long p4 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/am/AppStateManager$AppState;->updateServiceDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZJ)V */
return;
} // .end method
static void -$$Nest$mupdateVisibility ( com.android.server.am.AppStateManager$AppState p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updateVisibility()V */
return;
} // .end method
private com.android.server.am.AppStateManager$AppState ( ) {
/* .locals 4 */
/* .param p1, "this$0" # Lcom/android/server/am/AppStateManager; */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "context" # Landroid/content/Context; */
/* .line 938 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/miui/server/smartpower/IAppState;-><init>()V */
/* .line 892 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I */
/* .line 893 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mResourceBehavier:I */
/* .line 894 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I */
/* .line 895 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z */
/* .line 896 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsBackupServiceApp:Z */
/* .line 897 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z */
/* .line 898 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z */
/* .line 899 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z */
/* .line 900 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasFgService:Z */
/* .line 901 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mTotalTimeInForeground:J */
/* .line 902 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastTopTime:J */
/* .line 903 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J */
/* .line 904 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcStartTime:J */
/* .line 905 */
int v3 = -1; // const/4 v3, -0x1
/* iput v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* .line 917 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mTopAppCount:I */
/* .line 918 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mFreeFormCount:I */
/* .line 919 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mOverlayCount:I */
/* .line 920 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLaunchCount:I */
/* .line 921 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mInSplitMode:Z */
/* .line 923 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z */
/* .line 924 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastVideoPlayTimeStamp:J */
/* .line 930 */
/* new-instance v1, Landroid/util/ArrayMap; */
/* invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V */
this.mRunningProcs = v1;
/* .line 931 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
this.mProcLock = v1;
/* .line 933 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z */
/* .line 934 */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z */
/* .line 936 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIs64BitApp:Z */
/* .line 939 */
/* iput p2, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I */
/* .line 940 */
this.mPackageName = p3;
/* .line 941 */
/* new-instance v0, Lcom/android/server/am/AppStateManager$AppState$MyHandler; */
com.android.server.am.AppStateManager .-$$Nest$fgetmLooper ( p1 );
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;-><init>(Lcom/android/server/am/AppStateManager$AppState;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 942 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->checkSystemSignature()Z */
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z */
/* .line 943 */
com.android.server.am.AppStateManager .-$$Nest$fgetmPendingInteractiveUids ( p1 );
/* monitor-enter v0 */
/* .line 944 */
try { // :try_start_0
com.android.server.am.AppStateManager .-$$Nest$fgetmPendingInteractiveUids ( p1 );
v1 = (( android.util.SparseArray ) v1 ).contains ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->contains(I)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 945 */
com.android.server.am.AppStateManager .-$$Nest$fgetmPendingInteractiveUids ( p1 );
(( android.util.SparseArray ) v1 ).get ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J */
/* .line 946 */
com.android.server.am.AppStateManager .-$$Nest$fgetmPendingInteractiveUids ( p1 );
(( android.util.SparseArray ) v1 ).remove ( p2 ); // invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V
/* .line 948 */
} // :cond_0
/* monitor-exit v0 */
/* .line 949 */
return;
/* .line 948 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
 com.android.server.am.AppStateManager$AppState ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/AppStateManager$AppState;-><init>(Lcom/android/server/am/AppStateManager;ILjava/lang/String;Landroid/content/Context;)V */
return;
} // .end method
private void changeAppState ( Integer p0, Boolean p1, java.lang.String p2 ) {
/* .locals 11 */
/* .param p1, "state" # I */
/* .param p2, "hasProtectProcess" # Z */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 1782 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 3; // const/4 v1, 0x3
/* if-gt p1, v1, :cond_0 */
/* .line 1783 */
v2 = this.mHandler;
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v2 ).removeMessages ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->removeMessages(I)V
/* .line 1785 */
} // :cond_0
int v2 = 6; // const/4 v2, 0x6
/* if-ne p1, v2, :cond_1 */
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* if-eq v2, p1, :cond_1 */
final String v2 = "hibernate all "; // const-string v2, "hibernate all "
/* .line 1786 */
v2 = (( java.lang.String ) p3 ).startsWith ( v2 ); // invoke-virtual {p3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1787 */
v2 = this.mHandler;
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v2 ).removeMessages ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->removeMessages(I)V
/* .line 1788 */
v2 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v2 );
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v2 ).getHibernateDuration ( p0 ); // invoke-virtual {v2, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getHibernateDuration(Lcom/android/server/am/AppStateManager$AppState;)J
/* move-result-wide v2 */
/* .line 1789 */
/* .local v2, "delay":J */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v2, v4 */
/* if-lez v4, :cond_1 */
/* .line 1790 */
v4 = this.mHandler;
final String v5 = "periodic active"; // const-string v5, "periodic active"
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v4 ).obtainMessage ( v0, v5 ); // invoke-virtual {v4, v0, v5}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1792 */
/* .local v4, "msg":Landroid/os/Message; */
v5 = this.mHandler;
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v5 ).sendMessageDelayed ( v4, v2, v3 ); // invoke-virtual {v5, v4, v2, v3}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1795 */
} // .end local v2 # "delay":J
} // .end local v4 # "msg":Landroid/os/Message;
} // :cond_1
/* if-ne p1, v1, :cond_2 */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v1 );
/* .line 1796 */
v1 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v1 ).isAppHibernationWhiteList ( p0 ); // invoke-virtual {v1, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isAppHibernationWhiteList(Lcom/android/server/am/AppStateManager$AppState;)Z
/* if-nez v1, :cond_2 */
/* .line 1797 */
v1 = this.mHandler;
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).obtainMessage ( v0, p3 ); // invoke-virtual {v1, v0, p3}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1798 */
/* .local v1, "msg":Landroid/os/Message; */
v2 = this.mHandler;
v3 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v3 );
v4 = this.mPackageName;
/* iget v5, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I */
/* .line 1799 */
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v3 ).getInactiveDuration ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getInactiveDuration(Ljava/lang/String;I)J
/* move-result-wide v3 */
/* .line 1798 */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v2 ).sendMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1801 */
} // .end local v1 # "msg":Landroid/os/Message;
} // :cond_2
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* if-ne v1, v0, :cond_3 */
/* if-le p1, v0, :cond_3 */
/* .line 1802 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastTopTime:J */
/* .line 1804 */
} // :cond_3
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmProcessStateCallbacks ( v1 );
/* monitor-enter v1 */
/* .line 1805 */
try { // :try_start_0
v2 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmProcessStateCallbacks ( v2 );
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* move-object v4, v3 */
/* check-cast v4, Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
/* .line 1806 */
/* .local v4, "callback":Lcom/android/server/am/AppStateManager$IProcessStateCallback; */
/* iget v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* iget-boolean v8, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z */
/* move-object v5, p0 */
/* move v7, p1 */
/* move v9, p2 */
/* move-object v10, p3 */
/* invoke-interface/range {v4 ..v10}, Lcom/android/server/am/AppStateManager$IProcessStateCallback;->onAppStateChanged(Lcom/android/server/am/AppStateManager$AppState;IIZZLjava/lang/String;)V */
/* .line 1808 */
} // .end local v4 # "callback":Lcom/android/server/am/AppStateManager$IProcessStateCallback;
/* .line 1809 */
} // :cond_4
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1810 */
/* sget-boolean v1, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1811 */
final String v1 = "SmartPower"; // const-string v1, "SmartPower"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = " move to "; // const-string v3, " move to "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.AppStateManager .appStateToString ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 1813 */
} // :cond_5
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* if-eq v1, p1, :cond_6 */
/* if-ne p1, v0, :cond_6 */
/* .line 1814 */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I */
/* add-int/2addr v1, v0 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I */
/* .line 1816 */
} // :cond_6
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* .line 1817 */
/* iput-boolean p2, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z */
/* .line 1818 */
return;
/* .line 1809 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
private void changeProcessState ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "state" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 1821 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1822 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1823 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmStateLock ( v2 );
/* monitor-enter v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1824 */
try { // :try_start_1
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).getCurrentState ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* if-lez v4, :cond_1 */
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).canHibernate ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z
if ( v4 != null) { // if-eqz v4, :cond_1
int v4 = 5; // const/4 v4, 0x5
/* if-eq p1, v4, :cond_0 */
/* .line 1826 */
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).getCurrentState ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* if-ge v4, p1, :cond_1 */
/* .line 1827 */
} // :cond_0
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mmoveToStateLocked ( v2,p1,p2 );
/* .line 1829 */
} // :cond_1
/* monitor-exit v3 */
/* .line 1830 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1829 */
/* .restart local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/am/AppStateManager$AppState;
} // .end local p1 # "state":I
} // .end local p2 # "reason":Ljava/lang/String;
try { // :try_start_2
/* throw v1 */
/* .line 1831 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .restart local p0 # "this":Lcom/android/server/am/AppStateManager$AppState; */
/* .restart local p1 # "state":I */
/* .restart local p2 # "reason":Ljava/lang/String; */
} // :cond_2
/* monitor-exit v0 */
/* .line 1832 */
return;
/* .line 1831 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
private Boolean checkSystemSignature ( ) {
/* .locals 4 */
/* .line 952 */
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmPkms ( v0 );
/* if-nez v0, :cond_0 */
/* .line 953 */
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
com.android.server.am.AppStateManager .-$$Nest$fputmPkms ( v0,v1 );
/* .line 955 */
} // :cond_0
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmPkms ( v0 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 956 */
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmPkms ( v0 );
final String v2 = "android"; // const-string v2, "android"
v3 = this.mPackageName;
v0 = (( android.content.pm.PackageManager ) v0 ).checkSignatures ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I
/* if-nez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* .line 959 */
} // :cond_2
} // .end method
private void componentEnd ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 1629 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1630 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1631 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mcomponentEnd ( v0,p2 );
/* .line 1633 */
} // :cond_0
return;
} // .end method
private void componentEnd ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 1614 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1615 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1616 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mcomponentEnd ( v2,p1 );
/* .line 1617 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1618 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1619 */
return;
/* .line 1618 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void componentStart ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "processRecord" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 1622 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1623 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1624 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mcomponentStart ( v0,p2 );
/* .line 1626 */
} // :cond_0
return;
} // .end method
private void componentStart ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "content" # Ljava/lang/String; */
/* .line 1596 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1597 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1598 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mcomponentStart ( v2,p1 );
/* .line 1599 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1600 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1601 */
return;
/* .line 1600 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void hibernateAllIfNeeded ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1503 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1504 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1505 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mbecomeInactiveIfNeeded ( v2,p1 );
/* .line 1506 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1507 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1508 */
return;
/* .line 1507 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void hibernateAllInactive ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1511 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1512 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1513 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmStateLock ( v2 );
/* monitor-enter v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1514 */
try { // :try_start_1
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).getCurrentState ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
int v5 = 4; // const/4 v5, 0x4
/* if-lt v4, v5, :cond_0 */
/* .line 1515 */
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).getCurrentState ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
int v5 = 6; // const/4 v5, 0x6
/* if-gt v4, v5, :cond_0 */
/* .line 1516 */
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).getAdj ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getAdj()I
/* if-lez v4, :cond_0 */
/* .line 1517 */
int v4 = 7; // const/4 v4, 0x7
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mmoveToStateLocked ( v2,v4,p1 );
/* .line 1519 */
} // :cond_0
/* monitor-exit v3 */
/* .line 1521 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1519 */
/* .restart local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/am/AppStateManager$AppState;
} // .end local p1 # "reason":Ljava/lang/String;
try { // :try_start_2
/* throw v1 */
/* .line 1522 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .restart local p0 # "this":Lcom/android/server/am/AppStateManager$AppState; */
/* .restart local p1 # "reason":Ljava/lang/String; */
} // :cond_1
/* monitor-exit v0 */
/* .line 1523 */
return;
/* .line 1522 */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v1 */
} // .end method
private Boolean isAppRunningComponent ( ) {
/* .locals 4 */
/* .line 1840 */
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmAMS ( v0 );
/* monitor-enter v0 */
/* .line 1841 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1842 */
/* .local v2, "r":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).getProcessRecord ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;
v3 = /* invoke-direct {p0, v3}, Lcom/android/server/am/AppStateManager$AppState;->isAppRunningComponentLock(Lcom/android/server/am/ProcessRecord;)Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1843 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1845 */
} // .end local v2 # "r":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_0
/* .line 1846 */
} // :cond_1
/* monitor-exit v0 */
/* .line 1848 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1846 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Boolean isAppRunningComponentLock ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 1835 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.mServices;
v0 = (( com.android.server.am.ProcessServiceRecord ) v0 ).numberOfExecutingServices ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessServiceRecord;->numberOfExecutingServices()I
/* if-gtz v0, :cond_0 */
v0 = this.mReceivers;
/* .line 1836 */
v0 = (( com.android.server.am.ProcessReceiverRecord ) v0 ).numberOfCurReceivers ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessReceiverRecord;->numberOfCurReceivers()I
/* if-lez v0, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 1835 */
} // :goto_0
} // .end method
private void onActivityForegroundLocked ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 1682 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1683 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z */
/* if-nez v1, :cond_0 */
/* .line 1684 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState;->setForegroundLocked(Z)V */
/* .line 1685 */
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1686 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monActivityForegroundLocked ( v2,p1 );
/* .line 1687 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1689 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1690 */
return;
/* .line 1689 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onAddToWhiteList ( ) {
/* .locals 3 */
/* .line 1526 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1527 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1528 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monAddToWhiteList ( v2 );
/* .line 1529 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1530 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1531 */
return;
/* .line 1530 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onAddToWhiteList ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 1534 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1535 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1536 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monAddToWhiteList ( v0 );
/* .line 1538 */
} // :cond_0
return;
} // .end method
private void onApplyOomAdjLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .line 1385 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->updateProcessLocked(Lcom/android/server/am/ProcessRecord;)V */
/* .line 1386 */
v0 = (( com.android.server.am.AppStateManager$AppState ) p0 ).isAlive ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1387 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1388 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updatePkgInfoLocked()V */
/* .line 1389 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1391 */
} // :cond_0
} // :goto_0
return;
} // .end method
private void onBackupChanged ( Boolean p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 1 */
/* .param p1, "active" # Z */
/* .param p2, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 1580 */
/* iget v0, p2, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1581 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1582 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monBackupChanged ( v0,p1 );
/* .line 1584 */
} // :cond_0
return;
} // .end method
private void onBackupServiceAppChanged ( Boolean p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "active" # Z */
/* .param p2, "pid" # I */
/* .line 1587 */
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsBackupServiceApp:Z */
/* .line 1588 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1589 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1590 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monBackupServiceAppChanged ( v2,p1 );
/* .line 1591 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1592 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1593 */
return;
/* .line 1592 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onInputMethodShow ( ) {
/* .locals 3 */
/* .line 1572 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1573 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1574 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monInputMethodShow ( v2 );
/* .line 1575 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1576 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1577 */
return;
/* .line 1576 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onMediaKey ( ) {
/* .locals 5 */
/* .line 1563 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J */
/* .line 1564 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1565 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1566 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* iget-wide v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monMediaKey ( v2,v3,v4 );
/* .line 1567 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1568 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1569 */
return;
/* .line 1568 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onMediaKey ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 1556 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1557 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1558 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monMediaKey ( v0,v1,v2 );
/* .line 1560 */
} // :cond_0
return;
} // .end method
private void onRemoveFromWhiteList ( ) {
/* .locals 3 */
/* .line 1541 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1542 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1543 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monRemoveFromWhiteList ( v2 );
/* .line 1544 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1545 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1546 */
return;
/* .line 1545 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onRemoveFromWhiteList ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 1549 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1550 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1551 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monRemoveFromWhiteList ( v0 );
/* .line 1553 */
} // :cond_0
return;
} // .end method
private void onReportPowerFrozenSignal ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 1667 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1668 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1669 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monReportPowerFrozenSignal ( v0,p2 );
/* .line 1671 */
} // :cond_0
return;
} // .end method
private void onReportPowerFrozenSignal ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1674 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1675 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1676 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monReportPowerFrozenSignal ( v2,p1 );
/* .line 1677 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1678 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1679 */
return;
/* .line 1678 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onResourceBehavierChanged ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "resourceBehavier" # I */
/* .line 1037 */
v0 = com.miui.server.smartpower.SmartScenarioManager .calculateScenarioAction ( p1 );
/* .line 1038 */
/* .local v0, "curAction":I */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mResourceBehavier:I */
v1 = com.miui.server.smartpower.SmartScenarioManager .calculateScenarioAction ( v1 );
/* .line 1039 */
/* .local v1, "preAction":I */
/* if-eq v0, v1, :cond_1 */
/* .line 1040 */
/* not-int v2, v0 */
/* and-int/2addr v2, v1 */
com.android.server.am.AppStateManager .-$$Nest$smparseScenatioActions ( v2 );
/* .line 1041 */
/* .local v2, "removedActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 1042 */
/* .local v4, "action":I */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {p0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1043 */
} // .end local v4 # "action":I
/* .line 1044 */
} // :cond_0
/* not-int v3, v1 */
/* and-int/2addr v3, v0 */
com.android.server.am.AppStateManager .-$$Nest$smparseScenatioActions ( v3 );
/* .line 1045 */
/* .local v3, "addedActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* .line 1046 */
/* .local v5, "action":I */
int v6 = 1; // const/4 v6, 0x1
/* invoke-direct {p0, v5, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1047 */
} // .end local v5 # "action":I
/* .line 1049 */
} // .end local v2 # "removedActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v3 # "addedActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_1
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mResourceBehavier:I */
/* .line 1050 */
return;
} // .end method
private void onSendPendingIntent ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "typeName" # Ljava/lang/String; */
/* .line 1604 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "pending intent "; // const-string v1, "pending intent "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1605 */
/* .local v0, "content":Ljava/lang/String; */
v1 = this.mProcLock;
/* monitor-enter v1 */
/* .line 1606 */
try { // :try_start_0
v2 = this.mRunningProcs;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1607 */
/* .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$monSendPendingIntent ( v3,v0 );
/* .line 1608 */
} // .end local v3 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1609 */
} // :cond_0
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1610 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastUidInteractiveTimeMills:J */
/* .line 1611 */
return;
/* .line 1609 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
private void processKilledLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 3 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .line 1336 */
v0 = this.processName;
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1337 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1338 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mprocessKilledLocked ( v0 );
/* .line 1339 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->removeProcessIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
/* .line 1340 */
v1 = (( com.android.server.am.AppStateManager$AppState ) p0 ).isAlive ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1341 */
v1 = this.mProcLock;
/* monitor-enter v1 */
/* .line 1342 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updatePkgInfoLocked()V */
/* .line 1343 */
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 1346 */
} // :cond_0
} // :goto_0
return;
} // .end method
private void processStartedLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 4 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .line 1318 */
v0 = this.processName;
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1319 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* if-nez v0, :cond_1 */
/* .line 1320 */
/* new-instance v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, p1, v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;-><init>(Lcom/android/server/am/AppStateManager$AppState;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/AppStateManager$AppState$RunningProcess-IA;)V */
/* .line 1321 */
} // .end local v0 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v2 = this.mProcLock;
/* monitor-enter v2 */
/* .line 1322 */
try { // :try_start_0
v0 = this.mRunningProcs;
v0 = (( android.util.ArrayMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1323 */
v0 = com.android.server.am.AppStateManager .isSystemApp ( p1 );
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z */
/* .line 1325 */
} // :cond_0
v0 = this.mRunningProcs;
v3 = this.processName;
(( android.util.ArrayMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1326 */
/* monitor-exit v2 */
/* move-object v0, v1 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
/* .line 1328 */
} // .end local v1 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .restart local v0 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
} // :cond_1
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mupdateProcessInfo ( v0,p1 );
/* .line 1330 */
} // :goto_0
v1 = this.mProcLock;
/* monitor-enter v1 */
/* .line 1331 */
try { // :try_start_1
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->updatePkgInfoLocked()V */
/* .line 1332 */
/* monitor-exit v1 */
/* .line 1333 */
return;
/* .line 1332 */
/* :catchall_1 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v2 */
} // .end method
private void recordVideoPlayIfNeeded ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "active" # Z */
/* .line 994 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 995 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 996 */
int v1 = 0; // const/4 v1, 0x0
/* .line 997 */
/* .local v1, "whiteListVideoVisible":Z */
try { // :try_start_0
v2 = this.mRunningProcs;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 998 */
/* .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v4 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mwhiteListVideoVisible ( v3 );
/* or-int/2addr v1, v4 */
/* .line 999 */
} // .end local v3 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1000 */
} // :cond_0
/* iput-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z */
/* .line 1001 */
} // .end local v1 # "whiteListVideoVisible":Z
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1002 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1003 */
final String v0 = "SmartPower"; // const-string v0, "SmartPower"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " WhiteListVideoPlaying "; // const-string v2, " WhiteListVideoPlaying "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1001 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 1006 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1007 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastVideoPlayTimeStamp:J */
/* .line 1008 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z */
/* .line 1009 */
/* sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1010 */
final String v0 = "SmartPower"; // const-string v0, "SmartPower"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " WhiteListVideoPlaying "; // const-string v2, " WhiteListVideoPlaying "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mWhiteListVideoPlaying:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1014 */
} // :cond_2
} // :goto_1
return;
} // .end method
private void removeProcessIfNeeded ( com.android.server.am.AppStateManager$AppState$RunningProcess p0 ) {
/* .locals 3 */
/* .param p1, "proc" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1361 */
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getUid ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I
v0 = android.os.Process .isIsolated ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1362 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1363 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;
(( android.util.ArrayMap ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1364 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1366 */
} // :cond_0
} // :goto_0
return;
} // .end method
private void setActivityVisible ( java.lang.String p0, com.android.server.wm.ActivityRecord p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p3, "visible" # Z */
/* .line 1370 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1371 */
/* .local v0, "process":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1372 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$msetActivityVisible ( v0,p2,p3 );
/* .line 1374 */
} // :cond_0
return;
} // .end method
private void setForegroundLocked ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "foreground" # Z */
/* .line 1496 */
v0 = this.mRunningProcs;
(( android.util.ArrayMap ) v0 ).values ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1497 */
/* .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$msetForeground ( v1,p1 );
/* .line 1498 */
} // .end local v1 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1499 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z */
/* .line 1500 */
return;
} // .end method
private void setWindowVisible ( Integer p0, com.android.server.policy.WindowManagerPolicy$WindowState p1, android.view.WindowManager$LayoutParams p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .param p2, "win" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .param p3, "attrs" # Landroid/view/WindowManager$LayoutParams; */
/* .param p4, "visible" # Z */
/* .line 1378 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1379 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1380 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$msetWindowVisible ( v0,p2,p3,p4 );
/* .line 1382 */
} // :cond_0
return;
} // .end method
private void stepAppState ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1852 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->removeMessages(I)V
/* .line 1853 */
/* const-wide/16 v2, 0x0 */
/* .line 1854 */
/* .local v2, "nextStepDuration":J */
v0 = this.mHandler;
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1855 */
/* .local v0, "msg":Landroid/os/Message; */
/* iget v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
int v5 = 6; // const/4 v5, 0x6
/* packed-switch v4, :pswitch_data_0 */
/* .line 1882 */
/* :pswitch_0 */
int v1 = 5; // const/4 v1, 0x5
/* invoke-direct {p0, v1, p1}, Lcom/android/server/am/AppStateManager$AppState;->changeProcessState(ILjava/lang/String;)V */
/* .line 1883 */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v1 );
v4 = this.mPackageName;
/* .line 1884 */
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v1 ).getMaintenanceDur ( v4 ); // invoke-virtual {v1, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getMaintenanceDur(Ljava/lang/String;)J
/* move-result-wide v2 */
/* .line 1867 */
/* :pswitch_1 */
/* iget v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I */
/* if-gtz v4, :cond_1 */
/* iget v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinAmsProcState:I */
/* if-lt v4, v5, :cond_0 */
/* .line 1869 */
v4 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->isAppRunningComponent()Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1870 */
} // :cond_0
/* const-wide/16 v4, 0x3e8 */
/* .line 1871 */
/* .local v4, "delay":J */
v1 = this.mHandler;
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).sendMessageDelayed ( v0, v4, v5 ); // invoke-virtual {v1, v0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1872 */
return;
/* .line 1874 */
} // .end local v4 # "delay":J
} // :cond_1
int v4 = 7; // const/4 v4, 0x7
/* invoke-direct {p0, v4, p1}, Lcom/android/server/am/AppStateManager$AppState;->changeProcessState(ILjava/lang/String;)V */
/* .line 1875 */
v4 = this.mHandler;
final String v5 = "periodic active"; // const-string v5, "periodic active"
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v4 ).obtainMessage ( v1, v5 ); // invoke-virtual {v4, v1, v5}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 1877 */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v1 );
/* .line 1878 */
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v1 ).getHibernateDuration ( p0 ); // invoke-virtual {v1, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getHibernateDuration(Lcom/android/server/am/AppStateManager$AppState;)J
/* move-result-wide v2 */
/* .line 1879 */
/* .line 1858 */
/* :pswitch_2 */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I */
/* if-gtz v1, :cond_2 */
v1 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState;->isAppRunningComponent()Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1859 */
/* const-wide/16 v4, 0x3e8 */
/* .line 1860 */
/* .restart local v4 # "delay":J */
v1 = this.mHandler;
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).sendMessageDelayed ( v0, v4, v5 ); // invoke-virtual {v1, v0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1861 */
return;
/* .line 1863 */
} // .end local v4 # "delay":J
} // :cond_2
/* invoke-direct {p0, v5, p1}, Lcom/android/server/am/AppStateManager$AppState;->changeProcessState(ILjava/lang/String;)V */
/* .line 1864 */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v1 );
v4 = this.mPackageName;
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v1 ).getIdleDur ( v4 ); // invoke-virtual {v1, v4}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getIdleDur(Ljava/lang/String;)J
/* move-result-wide v2 */
/* .line 1865 */
/* nop */
/* .line 1887 */
} // :goto_0
/* const-wide/16 v4, 0x0 */
/* cmp-long v1, v2, v4 */
/* if-lez v1, :cond_3 */
/* .line 1888 */
v1 = this.mHandler;
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 1890 */
} // :cond_3
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x3 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void updateAppState ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1723 */
int v0 = -1; // const/4 v0, -0x1
/* .line 1724 */
/* .local v0, "minProcessState":I */
int v1 = -1; // const/4 v1, -0x1
/* .line 1725 */
/* .local v1, "minProcessStateExceptProtect":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1726 */
/* .local v2, "hasProtectProcess":Z */
v3 = this.mProcLock;
/* monitor-enter v3 */
/* .line 1727 */
try { // :try_start_0
v4 = this.mRunningProcs;
(( android.util.ArrayMap ) v4 ).values ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_6
/* check-cast v5, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1728 */
/* .local v5, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v6 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v5 ).canHibernate ( ); // invoke-virtual {v5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->canHibernate()Z
/* .line 1729 */
/* .local v6, "canHibernate":Z */
v7 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v5 ).getCurrentState ( ); // invoke-virtual {v5}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* .line 1730 */
/* .local v7, "state":I */
/* if-nez v6, :cond_0 */
int v8 = 1; // const/4 v8, 0x1
} // :cond_0
int v8 = 0; // const/4 v8, 0x0
} // :goto_1
/* or-int/2addr v2, v8 */
/* .line 1731 */
int v8 = -1; // const/4 v8, -0x1
/* if-lez v7, :cond_1 */
/* if-lt v7, v0, :cond_2 */
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
/* if-ne v0, v8, :cond_3 */
/* .line 1734 */
} // :cond_2
/* move v0, v7 */
/* .line 1736 */
} // :cond_3
if ( v6 != null) { // if-eqz v6, :cond_5
/* if-lt v7, v1, :cond_4 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* if-ne v1, v8, :cond_5 */
/* .line 1740 */
} // :cond_4
/* move v1, v7 */
/* .line 1741 */
/* iget-boolean v8, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z */
/* if-nez v8, :cond_5 */
int v8 = 3; // const/4 v8, 0x3
/* if-ne v7, v8, :cond_5 */
/* .line 1742 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$msendBecomeInactiveMsg ( v5,p1 );
/* .line 1745 */
} // .end local v5 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // .end local v6 # "canHibernate":Z
} // .end local v7 # "state":I
} // :cond_5
/* .line 1746 */
} // :cond_6
v4 = java.lang.Math .max ( v0,v1 );
/* move v0, v4 */
/* .line 1747 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1748 */
int v3 = -1; // const/4 v3, -0x1
/* .line 1749 */
/* .local v3, "appState":I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 1773 */
/* :pswitch_0 */
int v3 = 6; // const/4 v3, 0x6
/* .line 1770 */
/* :pswitch_1 */
int v3 = 5; // const/4 v3, 0x5
/* .line 1771 */
/* .line 1767 */
/* :pswitch_2 */
int v3 = 4; // const/4 v3, 0x4
/* .line 1768 */
/* .line 1764 */
/* :pswitch_3 */
int v3 = 3; // const/4 v3, 0x3
/* .line 1765 */
/* .line 1761 */
/* :pswitch_4 */
int v3 = 2; // const/4 v3, 0x2
/* .line 1762 */
/* .line 1757 */
/* :pswitch_5 */
int v3 = 1; // const/4 v3, 0x1
/* .line 1758 */
/* .line 1754 */
/* :pswitch_6 */
int v3 = 0; // const/4 v3, 0x0
/* .line 1755 */
/* .line 1751 */
/* :pswitch_7 */
int v3 = -1; // const/4 v3, -0x1
/* .line 1752 */
/* nop */
/* .line 1776 */
} // :goto_2
/* iget v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* if-ne v3, v4, :cond_7 */
/* iget-boolean v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z */
/* if-eq v4, v2, :cond_8 */
/* .line 1777 */
} // :cond_7
/* invoke-direct {p0, v3, v2, p1}, Lcom/android/server/am/AppStateManager$AppState;->changeAppState(IZLjava/lang/String;)V */
/* .line 1779 */
} // :cond_8
return;
/* .line 1747 */
} // .end local v3 # "appState":I
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v4 */
/* :pswitch_data_0 */
/* .packed-switch -0x1 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void updateCurrentResourceBehavier ( ) {
/* .locals 5 */
/* .line 1017 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1018 */
/* .local v0, "currentBehavier":I */
v1 = this.mProcLock;
/* monitor-enter v1 */
/* .line 1019 */
try { // :try_start_0
v2 = this.mRunningProcs;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1020 */
/* .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v4 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmAudioBehavier ( v3 );
/* or-int/2addr v0, v4 */
/* .line 1021 */
v4 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmGpsBehavier ( v3 );
/* or-int/2addr v0, v4 */
/* .line 1022 */
v4 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmNetBehavier ( v3 );
/* or-int/2addr v0, v4 */
/* .line 1023 */
v4 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmCamBehavier ( v3 );
/* or-int/2addr v0, v4 */
/* .line 1024 */
v4 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmDisplayBehavier ( v3 );
/* or-int/2addr v0, v4 */
/* .line 1025 */
} // .end local v3 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1026 */
} // :cond_0
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1027 */
v1 = this.this$0;
v1 = com.android.server.am.AppStateManager .-$$Nest$fgetmHoldScreenUid ( v1 );
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I */
/* if-ne v1, v2, :cond_1 */
/* .line 1028 */
/* or-int/lit16 v0, v0, 0x400 */
/* .line 1031 */
} // :cond_1
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mResourceBehavier:I */
/* if-eq v0, v1, :cond_2 */
/* .line 1032 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->onResourceBehavierChanged(I)V */
/* .line 1034 */
} // :cond_2
return;
/* .line 1026 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
private void updateCurrentScenarioAction ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "action" # I */
/* .param p2, "active" # Z */
/* .line 968 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I */
/* .line 969 */
/* .local v0, "currentActions":I */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 970 */
/* or-int/2addr v0, p1 */
/* .line 972 */
} // :cond_0
/* not-int v1, p1 */
/* and-int/2addr v0, v1 */
/* .line 974 */
} // :goto_0
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I */
/* if-eq v0, v1, :cond_1 */
/* .line 975 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I */
/* .line 976 */
v1 = this.mHandler;
/* new-instance v2, Lcom/android/server/am/AppStateManager$AppState$1; */
/* invoke-direct {v2, p0, p1, p2}, Lcom/android/server/am/AppStateManager$AppState$1;-><init>(Lcom/android/server/am/AppStateManager$AppState;IZ)V */
(( com.android.server.am.AppStateManager$AppState$MyHandler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/AppStateManager$AppState$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 991 */
} // :cond_1
return;
} // .end method
private void updatePkgInfoLocked ( ) {
/* .locals 10 */
/* .line 1394 */
v0 = this.mRunningProcs;
v0 = (( android.util.ArrayMap ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I
/* if-nez v0, :cond_0 */
return;
/* .line 1396 */
} // :cond_0
/* const/16 v0, -0x2710 */
/* .line 1397 */
/* .local v0, "minAdj":I */
/* const/16 v1, 0x14 */
/* .line 1398 */
/* .local v1, "minAmsProcState":I */
/* const/16 v2, 0x3e8 */
/* .line 1400 */
/* .local v2, "minPriorityScore":I */
/* const/16 v3, 0x9 */
/* .line 1402 */
/* .local v3, "minUsageLevel":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 1403 */
/* .local v4, "hasFgService":Z */
int v5 = 0; // const/4 v5, 0x0
/* .line 1404 */
/* .local v5, "is32BitApp":Z */
v6 = this.mRunningProcs;
(( android.util.ArrayMap ) v6 ).values ( ); // invoke-virtual {v6}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
} // :cond_1
v7 = } // :goto_0
/* const/16 v8, -0x2710 */
if ( v7 != null) { // if-eqz v7, :cond_b
/* check-cast v7, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1405 */
/* .local v7, "r":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v9 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v7 ).getCurrentState ( ); // invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
if ( v9 != null) { // if-eqz v9, :cond_1
v9 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmAdj ( v7 );
/* if-ne v9, v8, :cond_2 */
/* .line 1407 */
/* .line 1409 */
} // :cond_2
v9 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmAdj ( v7 );
/* if-gt v0, v9, :cond_3 */
/* if-ne v0, v8, :cond_4 */
/* .line 1410 */
} // :cond_3
v0 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmAdj ( v7 );
/* .line 1412 */
} // :cond_4
v8 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmAmsProcState ( v7 );
/* if-le v1, v8, :cond_5 */
/* .line 1413 */
v1 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmAmsProcState ( v7 );
/* .line 1415 */
} // :cond_5
v8 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v7 ).getPriorityScore ( ); // invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPriorityScore()I
/* if-le v2, v8, :cond_6 */
/* .line 1416 */
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v7 ).getPriorityScore ( ); // invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPriorityScore()I
/* .line 1418 */
} // :cond_6
v8 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v7 ).getUsageLevel ( ); // invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUsageLevel()I
/* if-le v3, v8, :cond_7 */
/* .line 1419 */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v7 ).getUsageLevel ( ); // invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUsageLevel()I
/* .line 1421 */
} // :cond_7
v8 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v7 ).hasForegrundService ( ); // invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasForegrundService()Z
if ( v8 != null) { // if-eqz v8, :cond_8
/* .line 1422 */
int v4 = 1; // const/4 v4, 0x1
/* .line 1424 */
} // :cond_8
v8 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v7 ).isMainProc ( ); // invoke-virtual {v7}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isMainProc()Z
if ( v8 != null) { // if-eqz v8, :cond_9
/* .line 1425 */
v8 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmAdj ( v7 );
/* iput v8, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcOomAdj:I */
/* .line 1427 */
} // :cond_9
v8 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmIs64BitProcess ( v7 );
/* if-nez v8, :cond_a */
/* .line 1428 */
int v5 = 1; // const/4 v5, 0x1
/* .line 1430 */
} // .end local v7 # "r":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_a
/* .line 1431 */
} // :cond_b
/* iget v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I */
/* if-eq v0, v6, :cond_c */
/* if-eq v0, v8, :cond_c */
/* .line 1432 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I */
/* .line 1434 */
} // :cond_c
/* iget v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinPriorityScore:I */
/* if-eq v2, v6, :cond_d */
/* const/16 v6, 0x3e8 */
/* if-eq v2, v6, :cond_d */
/* .line 1437 */
/* iput v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinPriorityScore:I */
/* .line 1439 */
} // :cond_d
/* xor-int/lit8 v6, v5, 0x1 */
/* iput-boolean v6, p0, Lcom/android/server/am/AppStateManager$AppState;->mIs64BitApp:Z */
/* .line 1440 */
/* iput v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinUsageLevel:I */
/* .line 1441 */
/* iput v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinAmsProcState:I */
/* .line 1442 */
/* iput-boolean v4, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasFgService:Z */
/* .line 1443 */
return;
} // .end method
private void updateProcessLocked ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 3 */
/* .param p1, "record" # Lcom/android/server/am/ProcessRecord; */
/* .line 1349 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1350 */
try { // :try_start_0
v1 = this.processName;
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1351 */
/* .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1352 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mupdateProcessInfo ( v1,p1 );
/* .line 1353 */
/* invoke-direct {p0, v1}, Lcom/android/server/am/AppStateManager$AppState;->removeProcessIfNeeded(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V */
/* .line 1354 */
} // :cond_0
v2 = (( com.android.server.am.ProcessRecord ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* if-nez v2, :cond_1 */
/* .line 1355 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->processStartedLocked(Lcom/android/server/am/ProcessRecord;)V */
/* .line 1357 */
} // .end local v1 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
/* .line 1358 */
return;
/* .line 1357 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void updateProviderDepends ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "client" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "host" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "isConnect" # Z */
/* .line 1659 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1660 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1661 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mupdateProviderDepends ( v0,p2,p3 );
/* .line 1663 */
} // :cond_0
return;
} // .end method
private void updateServiceDepends ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Boolean p2, Long p3 ) {
/* .locals 14 */
/* .param p1, "client" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "service" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "isConnect" # Z */
/* .param p4, "flags" # J */
/* .line 1637 */
/* move-object v6, p0 */
/* move-object v7, p1 */
/* move-object/from16 v8, p2 */
/* iget v0, v7, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1638 */
/* .local v9, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* const-wide/32 v0, 0x4000000 */
/* and-long v0, p4, v0 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
int v1 = 1; // const/4 v1, 0x1
int v4 = 0; // const/4 v4, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v4 */
} // :goto_0
/* move v10, v0 */
/* .line 1639 */
/* .local v10, "fgService":Z */
if ( v9 != null) { // if-eqz v9, :cond_1
/* .line 1640 */
/* move/from16 v11, p3 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mupdateServiceDepends ( v9,v8,v11,v10 );
/* .line 1641 */
} // :cond_1
/* move/from16 v11, p3 */
/* iget v0, v7, Lcom/android/server/am/ProcessRecord;->mPid:I */
v5 = android.os.Process .myPid ( );
/* if-ne v0, v5, :cond_3 */
/* iget v0, v8, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v5, v6, Lcom/android/server/am/AppStateManager$AppState;->mUid:I */
/* if-eq v0, v5, :cond_3 */
/* .line 1642 */
/* const-wide/32 v12, 0x8000 */
/* and-long v12, p4, v12 */
/* cmp-long v0, v12, v2 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* move v5, v1 */
} // :cond_2
/* move v5, v4 */
/* .line 1643 */
/* .local v5, "isJobService":Z */
} // :goto_1
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object/from16 v2, p2 */
/* move/from16 v3, p3 */
/* move v4, v10 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/am/AppStateManager$AppState;->updateSystemServiceDepends(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;ZZZ)V */
/* .line 1645 */
} // .end local v5 # "isJobService":Z
} // :cond_3
} // :goto_2
return;
} // .end method
private void updateSystemServiceDepends ( com.android.server.am.ProcessRecord p0, com.android.server.am.ProcessRecord p1, Boolean p2, Boolean p3, Boolean p4 ) {
/* .locals 7 */
/* .param p1, "client" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "service" # Lcom/android/server/am/ProcessRecord; */
/* .param p3, "isConnect" # Z */
/* .param p4, "fgService" # Z */
/* .param p5, "jobService" # Z */
/* .line 1649 */
if ( p3 != null) { // if-eqz p3, :cond_0
/* if-nez p4, :cond_0 */
/* if-nez p5, :cond_1 */
/* .line 1650 */
} // :cond_0
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v0 );
v3 = this.processName;
v4 = this.processName;
/* iget v5, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v6, p2, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* move v2, p3 */
/* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->onDependChanged(ZLjava/lang/String;Ljava/lang/String;II)V */
/* .line 1655 */
} // :cond_1
return;
} // .end method
private void updateVisibility ( ) {
/* .locals 7 */
/* .line 1446 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1447 */
/* .local v0, "foreground":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 1448 */
/* .local v1, "inSplitMode":Z */
v2 = this.mProcLock;
/* monitor-enter v2 */
/* .line 1449 */
try { // :try_start_0
v3 = this.mRunningProcs;
(( android.util.ArrayMap ) v3 ).values ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1450 */
/* .local v4, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$mupdateVisibility ( v4 );
/* .line 1451 */
v5 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v4 ).isVisible ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isVisible()Z
/* or-int/2addr v0, v5 */
/* .line 1452 */
v5 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v4 ).inSplitMode ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->inSplitMode()Z
/* or-int/2addr v1, v5 */
/* .line 1453 */
v5 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v4 ).inCurrentStack ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->inCurrentStack()Z
/* or-int/2addr v0, v5 */
/* .line 1454 */
} // .end local v4 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1455 */
} // :cond_0
/* iget-boolean v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z */
/* if-eq v0, v3, :cond_1 */
/* .line 1456 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppStateManager$AppState;->setForegroundLocked(Z)V */
/* .line 1457 */
} // :cond_1
/* iget v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mTopAppCount:I */
int v4 = 2; // const/4 v4, 0x2
int v5 = 1; // const/4 v5, 0x1
int v6 = 0; // const/4 v6, 0x0
/* if-lez v3, :cond_2 */
/* .line 1458 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1461 */
} // :cond_2
/* invoke-direct {p0, v4, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1464 */
} // :goto_1
/* iget v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mFreeFormCount:I */
/* const/16 v4, 0x10 */
/* if-lez v3, :cond_3 */
/* .line 1465 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1468 */
} // :cond_3
/* invoke-direct {p0, v4, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1471 */
} // :goto_2
/* iget v3, p0, Lcom/android/server/am/AppStateManager$AppState;->mOverlayCount:I */
/* const/16 v4, 0x20 */
/* if-lez v3, :cond_4 */
/* .line 1472 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1475 */
} // :cond_4
/* invoke-direct {p0, v4, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1478 */
} // :goto_3
/* const/16 v3, 0x8 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1479 */
/* invoke-direct {p0, v3, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1482 */
} // :cond_5
/* invoke-direct {p0, v3, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1485 */
} // :goto_4
/* const v3, 0x8000 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 1486 */
/* invoke-direct {p0, v3, v5}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1489 */
} // :cond_6
/* invoke-direct {p0, v3, v6}, Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V */
/* .line 1492 */
} // :goto_5
/* monitor-exit v2 */
/* .line 1493 */
return;
/* .line 1492 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .line 1694 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1695 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "#"; // const-string v2, "#"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mPackageName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "("; // const-string v2, "("
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ") state="; // const-string v2, ") state="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* .line 1696 */
com.android.server.am.AppStateManager .appStateToString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1695 */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1697 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " minAdj="; // const-string v2, " minAdj="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " minPriority="; // const-string v2, " minPriority="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinPriorityScore:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " hdur="; // const-string v2, " hdur="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v2 );
/* .line 1698 */
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v2 ).getHibernateDuration ( p0 ); // invoke-virtual {v2, p0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getHibernateDuration(Lcom/android/server/am/AppStateManager$AppState;)J
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1697 */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1700 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " proc size="; // const-string v2, " proc size="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mRunningProcs;
v2 = (( android.util.ArrayMap ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1701 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " noRestrict="; // const-string v2, " noRestrict="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v2 );
v3 = this.mPackageName;
/* .line 1702 */
v2 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v2 ).isNoRestrictApp ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isNoRestrictApp(Ljava/lang/String;)Z
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " locked="; // const-string v2, " locked="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " autoStart="; // const-string v2, " autoStart="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " sysSign="; // const-string v2, " sysSign="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " sysApp="; // const-string v2, " sysApp="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " is64Bit="; // const-string v2, " is64Bit="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/am/AppStateManager$AppState;->mIs64BitApp:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1701 */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1708 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.mRunningProcs;
v2 = (( android.util.ArrayMap ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->size()I
/* if-ge v1, v2, :cond_0 */
/* .line 1709 */
v2 = this.mRunningProcs;
(( android.util.ArrayMap ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1710 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
(( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).dump ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 1708 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1712 */
} // .end local v1 # "i":I
} // :cond_0
/* monitor-exit v0 */
/* .line 1713 */
return;
/* .line 1712 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Long getActions ( ) {
/* .locals 2 */
/* .line 964 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mScenarioActions:I */
/* int-to-long v0, v0 */
/* return-wide v0 */
} // .end method
public Integer getAdj ( ) {
/* .locals 1 */
/* .line 1079 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I */
} // .end method
public Integer getAppSwitchFgCount ( ) {
/* .locals 1 */
/* .line 1089 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppSwitchFgCount:I */
} // .end method
Integer getCurrentState ( ) {
/* .locals 1 */
/* .line 1209 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
} // .end method
public android.util.ArrayMap getDependsProcess ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/ArrayMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1290 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
/* .line 1291 */
/* .local v0, "dependsList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;>;" */
v1 = this.mProcLock;
/* monitor-enter v1 */
/* .line 1292 */
try { // :try_start_0
v2 = this.mRunningProcs;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1293 */
/* .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmServiceDependProcs ( v3 );
v4 = (( android.util.ArrayMap ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I
/* if-lez v4, :cond_0 */
/* .line 1294 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmServiceDependProcs ( v3 );
(( android.util.ArrayMap ) v0 ).putAll ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->putAll(Landroid/util/ArrayMap;)V
/* .line 1296 */
} // :cond_0
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmProviderDependProcs ( v3 );
v4 = (( android.util.ArrayMap ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I
/* if-lez v4, :cond_1 */
/* .line 1297 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmProviderDependProcs ( v3 );
(( android.util.ArrayMap ) v0 ).putAll ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->putAll(Landroid/util/ArrayMap;)V
/* .line 1299 */
} // .end local v3 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_1
/* .line 1300 */
} // :cond_2
/* monitor-exit v1 */
/* .line 1301 */
/* .line 1300 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Long getLastTopTime ( ) {
/* .locals 2 */
/* .line 1306 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastTopTime:J */
/* return-wide v0 */
} // .end method
public Long getLastVideoPlayTimeStamp ( ) {
/* .locals 2 */
/* .line 1310 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLastVideoPlayTimeStamp:J */
/* return-wide v0 */
} // .end method
public Integer getLaunchCount ( ) {
/* .locals 1 */
/* .line 1099 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mLaunchCount:I */
} // .end method
public Integer getMainProcAdj ( ) {
/* .locals 1 */
/* .line 1084 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcOomAdj:I */
} // .end method
public Long getMainProcStartTime ( ) {
/* .locals 2 */
/* .line 1314 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMainProcStartTime:J */
/* return-wide v0 */
} // .end method
public Integer getMinAmsProcState ( ) {
/* .locals 1 */
/* .line 1176 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinAmsProcState:I */
} // .end method
public java.lang.String getPackageName ( ) {
/* .locals 1 */
/* .line 1059 */
v0 = this.mPackageName;
} // .end method
public Integer getPriorityScore ( ) {
/* .locals 1 */
/* .line 1069 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinPriorityScore:I */
} // .end method
Integer getProcessState ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 1180 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1181 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1182 */
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).getCurrentState ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* .line 1184 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
public Integer getProcessState ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 1201 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1202 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1203 */
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).getCurrentState ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* .line 1205 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
com.android.server.am.AppStateManager$AppState$RunningProcess getRunningProcess ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .line 1245 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1246 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1247 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).getPid ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I
/* if-ne v3, p1, :cond_0 */
/* .line 1248 */
/* monitor-exit v0 */
/* .line 1250 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_0
/* .line 1251 */
} // :cond_1
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1252 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
com.android.server.am.AppStateManager$AppState$RunningProcess getRunningProcess ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 1239 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1240 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* monitor-exit v0 */
/* .line 1241 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public java.util.ArrayList getRunningProcessList ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/smartpower/IAppState$IRunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1214 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1215 */
/* .local v0, "runningProcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;" */
v1 = this.mProcLock;
/* monitor-enter v1 */
/* .line 1216 */
try { // :try_start_0
v2 = this.mRunningProcs;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1217 */
/* .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getCurrentState ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* if-lez v4, :cond_0 */
/* .line 1218 */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1220 */
} // .end local v3 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_0
/* .line 1221 */
} // :cond_1
/* monitor-exit v1 */
/* .line 1222 */
/* .line 1221 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
java.util.ArrayList getRunningProcessList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/AppStateManager$AppState$RunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1226 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1227 */
/* .local v0, "runningProcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState$RunningProcess;>;" */
v1 = this.mProcLock;
/* monitor-enter v1 */
/* .line 1228 */
try { // :try_start_0
v2 = this.mRunningProcs;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1229 */
/* .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).getCurrentState ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* if-lez v4, :cond_0 */
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmPackageName ( v3 );
/* .line 1230 */
v4 = (( java.lang.String ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 1231 */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1233 */
} // .end local v3 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_0
/* .line 1234 */
} // :cond_1
/* monitor-exit v1 */
/* .line 1235 */
/* .line 1234 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Long getTotalTimeInForeground ( ) {
/* .locals 2 */
/* .line 1109 */
/* iget-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mTotalTimeInForeground:J */
/* return-wide v0 */
} // .end method
public Integer getUid ( ) {
/* .locals 1 */
/* .line 1064 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I */
} // .end method
public Integer getUsageLevel ( ) {
/* .locals 1 */
/* .line 1074 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinUsageLevel:I */
} // .end method
public Boolean hasActivityApp ( ) {
/* .locals 4 */
/* .line 1188 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1189 */
try { // :try_start_0
v1 = this.mRunningProcs;
(( android.util.ArrayMap ) v1 ).values ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1190 */
/* .local v2, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).getCurrentState ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* if-lez v3, :cond_0 */
/* .line 1191 */
v3 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v2 ).hasActivity ( ); // invoke-virtual {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->hasActivity()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1192 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1194 */
} // .end local v2 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
} // :cond_0
/* .line 1195 */
} // :cond_1
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1196 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean hasForegroundService ( ) {
/* .locals 1 */
/* .line 1054 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasFgService:Z */
} // .end method
public Boolean hasProtectProcess ( ) {
/* .locals 1 */
/* .line 1171 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mHasProtectProcess:Z */
} // .end method
public Boolean isAlive ( ) {
/* .locals 2 */
/* .line 1114 */
v0 = this.mProcLock;
/* monitor-enter v0 */
/* .line 1115 */
try { // :try_start_0
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* if-lez v1, :cond_0 */
v1 = this.mRunningProcs;
v1 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
/* if-lez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 1116 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isAutoStartApp ( ) {
/* .locals 1 */
/* .line 1161 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsAutoStartApp:Z */
} // .end method
public Boolean isIdle ( ) {
/* .locals 2 */
/* .line 1121 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
int v1 = 5; // const/4 v1, 0x5
/* if-lt v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isInactive ( ) {
/* .locals 2 */
/* .line 1126 */
/* iget v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
int v1 = 3; // const/4 v1, 0x3
/* if-lt v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isIs64BitApp ( ) {
/* .locals 1 */
/* .line 1151 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIs64BitApp:Z */
} // .end method
public Boolean isLockedApp ( ) {
/* .locals 1 */
/* .line 1166 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mIsLockedApp:Z */
} // .end method
public Boolean isProcessIdle ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .line 1264 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1265 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1266 */
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).getCurrentState ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
int v3 = 5; // const/4 v3, 0x5
/* if-le v2, v3, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 1268 */
} // :cond_1
} // .end method
Boolean isProcessKilled ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 1256 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1257 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1258 */
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).getCurrentState ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getCurrentState()I
/* if-lez v2, :cond_1 */
v2 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).isKilled ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isKilled()Z
if ( v2 != null) { // if-eqz v2, :cond_0
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
/* .line 1260 */
} // :cond_2
} // .end method
public Boolean isProcessPerceptible ( ) {
/* .locals 5 */
/* .line 1131 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1132 */
/* .local v0, "isPerceptible":Z */
v1 = this.mProcLock;
/* monitor-enter v1 */
/* .line 1133 */
try { // :try_start_0
v2 = this.mRunningProcs;
(( android.util.ArrayMap ) v2 ).values ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 1134 */
/* .local v3, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
v4 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v3 ).isProcessPerceptible ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isProcessPerceptible()Z
/* or-int/2addr v0, v4 */
/* .line 1135 */
} // .end local v3 # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1136 */
} // :cond_0
/* monitor-exit v1 */
/* .line 1137 */
/* .line 1136 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean isProcessPerceptible ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .line 1273 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(I)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1274 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1275 */
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).isProcessPerceptible ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isProcessPerceptible()Z
/* .line 1277 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isProcessPerceptible ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 1282 */
(( com.android.server.am.AppStateManager$AppState ) p0 ).getRunningProcess ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/AppStateManager$AppState;->getRunningProcess(Ljava/lang/String;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
/* .line 1283 */
/* .local v0, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1284 */
v1 = (( com.android.server.am.AppStateManager$AppState$RunningProcess ) v0 ).isProcessPerceptible ( ); // invoke-virtual {v0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->isProcessPerceptible()Z
/* .line 1286 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isSystemApp ( ) {
/* .locals 1 */
/* .line 1142 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemApp:Z */
} // .end method
public Boolean isSystemSignature ( ) {
/* .locals 1 */
/* .line 1147 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mSystemSignature:Z */
} // .end method
public Boolean isVsible ( ) {
/* .locals 1 */
/* .line 1156 */
/* iget-boolean v0, p0, Lcom/android/server/am/AppStateManager$AppState;->mForeground:Z */
} // .end method
public void setLaunchCount ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "launchCount" # I */
/* .line 1094 */
/* iput p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mLaunchCount:I */
/* .line 1095 */
return;
} // .end method
public void setTotalTimeInForeground ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalTime" # J */
/* .line 1104 */
/* iput-wide p1, p0, Lcom/android/server/am/AppStateManager$AppState;->mTotalTimeInForeground:J */
/* .line 1105 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 1717 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " state="; // const-string v1, " state="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mAppState:I */
/* .line 1718 */
com.android.server.am.AppStateManager .appStateToString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " adj="; // const-string v1, " adj="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState;->mMinOomAdj:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " proc size="; // const-string v1, " proc size="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRunningProcs;
/* .line 1719 */
v1 = (( android.util.ArrayMap ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1717 */
} // .end method
