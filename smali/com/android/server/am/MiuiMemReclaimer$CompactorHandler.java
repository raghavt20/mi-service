class com.android.server.am.MiuiMemReclaimer$CompactorHandler extends android.os.Handler {
	 /* .source "MiuiMemReclaimer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MiuiMemReclaimer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "CompactorHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.MiuiMemReclaimer this$0; //synthetic
/* # direct methods */
public com.android.server.am.MiuiMemReclaimer$CompactorHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 130 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 134 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 145 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 142 */
/* :pswitch_0 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.MiuiMemReclaimer .-$$Nest$mperformCompactProcesses ( v0,v1 );
/* .line 143 */
/* .line 139 */
/* :pswitch_1 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* iget v2, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.MiuiMemReclaimer .-$$Nest$mperformCompactProcess ( v0,v1,v2 );
/* .line 140 */
/* .line 136 */
/* :pswitch_2 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.MiuiMemReclaimer .reclaimPage ( v0 );
/* .line 137 */
/* nop */
/* .line 148 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
