.class Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;
.super Ljava/lang/Object;
.source "ProcMemCleanerStatistics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcMemCleanerStatistics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KillInfo"
.end annotation


# instance fields
.field private killReason:Ljava/lang/String;

.field private killType:I

.field private name:Ljava/lang/String;

.field private pid:I

.field private priority:I

.field private pss:J

.field final synthetic this$0:Lcom/android/server/am/ProcMemCleanerStatistics;

.field private time:J

.field private uid:I


# direct methods
.method static bridge synthetic -$$Nest$fgettime(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->time:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fputkillReason(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->killReason:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputkillType(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->killType:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputname(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->name:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputpid(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->pid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputpriority(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->priority:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputpss(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->pss:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputtime(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->time:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputuid(Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->uid:I

    return-void
.end method

.method private constructor <init>(Lcom/android/server/am/ProcMemCleanerStatistics;)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->this$0:Lcom/android/server/am/ProcMemCleanerStatistics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/ProcMemCleanerStatistics;Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;-><init>(Lcom/android/server/am/ProcMemCleanerStatistics;)V

    return-void
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .locals 1

    .line 151
    iget v0, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->killType:I

    packed-switch v0, :pswitch_data_0

    .line 157
    const-string v0, "Unknown"

    return-object v0

    .line 155
    :pswitch_0
    const-string v0, "ForceStop"

    return-object v0

    .line 153
    :pswitch_1
    const-string v0, "KillProc"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .line 146
    nop

    .line 147
    invoke-virtual {p0}, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->getType()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->name:Ljava/lang/String;

    iget v2, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->pid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->uid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->time:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget v5, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->priority:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->pss:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/am/ProcMemCleanerStatistics$KillInfo;->killReason:Ljava/lang/String;

    filled-new-array/range {v0 .. v7}, [Ljava/lang/Object;

    move-result-object v0

    .line 146
    const-string v1, "#%s n:%s(%d) u:%d t:%d pri:%s pss:%d r:%s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
