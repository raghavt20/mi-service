public class com.android.server.am.MiuiMemoryService extends android.os.Binder implements com.android.server.am.MiuiMemoryServiceInternal {
	 /* .source "MiuiMemoryService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;, */
	 /* Lcom/android/server/am/MiuiMemoryService$Lifecycle;, */
	 /* Lcom/android/server/am/MiuiMemoryService$ConnectionHandler; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
private static final Object LMK_CAMERA_MODE;
public static final java.lang.String SERVICE_NAME;
private static final java.lang.String TAG;
private static final Integer VMPRESS_POLICY_CHECK_IO;
private static final Integer VMPRESS_POLICY_GRECLAM;
private static final Integer VMPRESS_POLICY_GRECLAM_AND_PM;
private static final Integer VMPRESS_POLICY_GSRECLAM;
private static final Integer VMPRESS_POLICY_PRECLAM;
private static final Integer VMPRESS_POLICY_TO_MI_PM;
private static Boolean sCompactSingleProcEnable;
private static Boolean sCompactionEnable;
private static Long sCompactionMinZramFreeKb;
private static Boolean sWriteEnable;
/* # instance fields */
private android.content.Context mContext;
private java.io.OutputStream mLmkdOutputStream;
private android.net.LocalSocket mLmkdSocket;
private com.android.server.am.MiuiMemReclaimer mReclaimer;
private java.lang.Object mWriteLock;
/* # direct methods */
static void -$$Nest$mhandleReclaimTrigger ( com.android.server.am.MiuiMemoryService p0, Integer p1, Integer p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/MiuiMemoryService;->handleReclaimTrigger(II)V */
	 return;
} // .end method
static com.android.server.am.MiuiMemoryService ( ) {
	 /* .locals 4 */
	 /* .line 37 */
	 /* nop */
	 /* .line 38 */
	 final String v0 = "debug.sys.mms"; // const-string v0, "debug.sys.mms"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.am.MiuiMemoryService.DEBUG = (v0!= 0);
	 /* .line 40 */
	 /* nop */
	 /* .line 41 */
	 final String v0 = "persist.sys.mms.compact_enable"; // const-string v0, "persist.sys.mms.compact_enable"
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.am.MiuiMemoryService.sCompactionEnable = (v0!= 0);
	 /* .line 42 */
	 /* nop */
	 /* .line 43 */
	 final String v0 = "persist.sys.mms.single_compact_enable"; // const-string v0, "persist.sys.mms.single_compact_enable"
	 int v2 = 1; // const/4 v2, 0x1
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v2 );
	 com.android.server.am.MiuiMemoryService.sCompactSingleProcEnable = (v0!= 0);
	 /* .line 44 */
	 /* nop */
	 /* .line 45 */
	 final String v0 = "persist.sys.mms.min_zramfree_kb"; // const-string v0, "persist.sys.mms.min_zramfree_kb"
	 /* const-wide/32 v2, 0x4b000 */
	 android.os.SystemProperties .getLong ( v0,v2,v3 );
	 /* move-result-wide v2 */
	 /* sput-wide v2, Lcom/android/server/am/MiuiMemoryService;->sCompactionMinZramFreeKb:J */
	 /* .line 54 */
	 /* nop */
	 /* .line 55 */
	 final String v0 = "persist.sys.mms.write_lmkd"; // const-string v0, "persist.sys.mms.write_lmkd"
	 v0 = 	 android.os.SystemProperties .getBoolean ( v0,v1 );
	 com.android.server.am.MiuiMemoryService.sWriteEnable = (v0!= 0);
	 /* .line 54 */
	 return;
} // .end method
public com.android.server.am.MiuiMemoryService ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 205 */
	 /* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
	 /* .line 58 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mWriteLock = v0;
	 /* .line 206 */
	 this.mContext = p1;
	 /* .line 207 */
	 /* new-instance v0, Lcom/android/server/am/MiuiMemReclaimer; */
	 /* invoke-direct {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;-><init>(Landroid/content/Context;)V */
	 this.mReclaimer = v0;
	 /* .line 208 */
	 /* new-instance v0, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;-><init>(Lcom/android/server/am/MiuiMemoryService;Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread-IA;)V */
	 /* .line 209 */
	 /* .local v0, "memServer":Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread; */
	 (( com.android.server.am.MiuiMemoryService$MiuiMemServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;->start()V
	 /* .line 210 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemoryService;->initHelper(Landroid/content/Context;)V */
	 /* .line 211 */
	 /* const-class v1, Lcom/android/server/am/MiuiMemoryServiceInternal; */
	 com.android.server.LocalServices .addService ( v1,p0 );
	 /* .line 212 */
	 return;
} // .end method
private void handleReclaimTrigger ( Integer p0, Integer p1 ) {
	 /* .locals 2 */
	 /* .param p1, "vmPressureLevel" # I */
	 /* .param p2, "vmPressurePolicy" # I */
	 /* .line 156 */
	 /* if-ltz p1, :cond_1 */
	 int v0 = 3; // const/4 v0, 0x3
	 /* if-gt p1, v0, :cond_1 */
	 /* if-ltz p2, :cond_1 */
	 /* const/16 v1, 0x11 */
	 /* if-le p2, v1, :cond_0 */
	 /* .line 164 */
} // :cond_0
/* sparse-switch p2, :sswitch_data_0 */
/* .line 179 */
/* :sswitch_0 */
/* invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->triggerProcessClean()V */
/* .line 180 */
/* .line 177 */
/* :sswitch_1 */
/* .line 173 */
/* :sswitch_2 */
(( com.android.server.am.MiuiMemoryService ) p0 ).runGlobalCompaction ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MiuiMemoryService;->runGlobalCompaction(I)V
/* .line 174 */
/* invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->triggerProcessClean()V */
/* .line 175 */
/* .line 170 */
/* :sswitch_3 */
(( com.android.server.am.MiuiMemoryService ) p0 ).runProcsCompaction ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/MiuiMemoryService;->runProcsCompaction(I)V
/* .line 171 */
/* .line 167 */
/* :sswitch_4 */
(( com.android.server.am.MiuiMemoryService ) p0 ).runGlobalCompaction ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MiuiMemoryService;->runGlobalCompaction(I)V
/* .line 168 */
/* nop */
/* .line 184 */
} // :goto_0
return;
/* .line 160 */
} // :cond_1
} // :goto_1
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
final String v1 = "mi_reclaim data is invalid!"; // const-string v1, "mi_reclaim data is invalid!"
android.util.Slog .w ( v0,v1 );
/* .line 161 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_4 */
/* 0x1 -> :sswitch_4 */
/* 0x2 -> :sswitch_3 */
/* 0x3 -> :sswitch_2 */
/* 0x4 -> :sswitch_1 */
/* 0x11 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
private void initHelper ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 215 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z */
final String v1 = "MiuiMemoryService"; // const-string v1, "MiuiMemoryService"
/* if-nez v0, :cond_0 */
/* .line 216 */
final String v0 = "MiuiMemServiceHelper didn\'t init..."; // const-string v0, "MiuiMemServiceHelper didn\'t init..."
android.util.Slog .w ( v1,v0 );
/* .line 217 */
return;
/* .line 220 */
} // :cond_0
/* new-instance v0, Lcom/android/server/am/MiuiMemServiceHelper; */
/* invoke-direct {v0, p1, p0}, Lcom/android/server/am/MiuiMemServiceHelper;-><init>(Landroid/content/Context;Lcom/android/server/am/MiuiMemoryService;)V */
/* .line 221 */
/* .local v0, "memHelper":Lcom/android/server/am/MiuiMemServiceHelper; */
(( com.android.server.am.MiuiMemServiceHelper ) v0 ).startWork ( ); // invoke-virtual {v0}, Lcom/android/server/am/MiuiMemServiceHelper;->startWork()V
/* .line 222 */
/* sget-boolean v2, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 223 */
final String v2 = "MiuiMemServiceHelper init complete"; // const-string v2, "MiuiMemServiceHelper init complete"
android.util.Slog .d ( v1,v2 );
/* .line 225 */
} // :cond_1
return;
} // .end method
private Boolean isZramFreeEnough ( ) {
/* .locals 4 */
/* .line 228 */
android.os.Debug .getZramFreeKb ( );
/* move-result-wide v0 */
/* .line 229 */
/* .local v0, "zramFreeKb":J */
/* sget-wide v2, Lcom/android/server/am/MiuiMemoryService;->sCompactionMinZramFreeKb:J */
/* cmp-long v2, v0, v2 */
/* if-gez v2, :cond_1 */
/* .line 230 */
/* sget-boolean v2, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 231 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Skipping compaction, zramFree too small.zramFree = "; // const-string v3, "Skipping compaction, zramFree too small.zramFree = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = " KB"; // const-string v3, " KB"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiMemoryService"; // const-string v3, "MiuiMemoryService"
android.util.Slog .d ( v3,v2 );
/* .line 234 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 236 */
} // :cond_1
int v2 = 1; // const/4 v2, 0x1
} // .end method
private Boolean openLmkdSocket ( ) {
/* .locals 6 */
/* .line 261 */
v0 = this.mLmkdSocket;
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 262 */
/* .line 265 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
/* new-instance v2, Landroid/net/LocalSocket; */
int v3 = 3; // const/4 v3, 0x3
/* invoke-direct {v2, v3}, Landroid/net/LocalSocket;-><init>(I)V */
this.mLmkdSocket = v2;
/* .line 266 */
/* new-instance v3, Landroid/net/LocalSocketAddress; */
final String v4 = "lmkd"; // const-string v4, "lmkd"
v5 = android.net.LocalSocketAddress$Namespace.RESERVED;
/* invoke-direct {v3, v4, v5}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V */
(( android.net.LocalSocket ) v2 ).connect ( v3 ); // invoke-virtual {v2, v3}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
/* .line 268 */
v2 = this.mLmkdSocket;
(( android.net.LocalSocket ) v2 ).getOutputStream ( ); // invoke-virtual {v2}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;
this.mLmkdOutputStream = v2;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 273 */
/* nop */
/* .line 274 */
v2 = this.mLmkdSocket;
if ( v2 != null) { // if-eqz v2, :cond_1
} // :cond_1
/* move v1, v0 */
} // :goto_0
/* .line 269 */
/* :catch_0 */
/* move-exception v1 */
/* .line 270 */
/* .local v1, "e":Ljava/io/IOException; */
final String v2 = "MiuiMemoryService"; // const-string v2, "MiuiMemoryService"
final String v3 = "Lmkd socket open failed"; // const-string v3, "Lmkd socket open failed"
android.util.Slog .e ( v2,v3 );
/* .line 271 */
int v2 = 0; // const/4 v2, 0x0
this.mLmkdSocket = v2;
/* .line 272 */
} // .end method
private void triggerProcessClean ( ) {
/* .locals 2 */
/* .line 187 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
final String v1 = "Call process cleaner"; // const-string v1, "Call process cleaner"
android.util.Slog .d ( v0,v1 );
/* .line 188 */
} // :cond_0
com.android.server.am.SystemPressureController .getInstance ( );
(( com.android.server.am.SystemPressureController ) v0 ).triggerProcessClean ( ); // invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->triggerProcessClean()V
/* .line 189 */
return;
} // .end method
/* # virtual methods */
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 337 */
v0 = this.mContext;
final String v1 = "MiuiMemoryService"; // const-string v1, "MiuiMemoryService"
v0 = com.android.internal.util.DumpUtils .checkDumpPermission ( v0,v1,p2 );
/* if-nez v0, :cond_0 */
return;
/* .line 338 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z */
/* if-nez v0, :cond_1 */
/* .line 339 */
final String v0 = "Compaction isn\'t enabled!"; // const-string v0, "Compaction isn\'t enabled!"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 340 */
return;
/* .line 342 */
} // :cond_1
v0 = this.mReclaimer;
(( com.android.server.am.MiuiMemReclaimer ) v0 ).dumpCompactionStats ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/server/am/MiuiMemReclaimer;->dumpCompactionStats(Ljava/io/PrintWriter;)V
/* .line 343 */
return;
} // .end method
public void interruptProcCompaction ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 327 */
v0 = this.mReclaimer;
(( com.android.server.am.MiuiMemReclaimer ) v0 ).interruptProcCompaction ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->interruptProcCompaction(I)V
/* .line 328 */
return;
} // .end method
public void interruptProcsCompaction ( ) {
/* .locals 1 */
/* .line 322 */
v0 = this.mReclaimer;
(( com.android.server.am.MiuiMemReclaimer ) v0 ).interruptProcsCompaction ( ); // invoke-virtual {v0}, Lcom/android/server/am/MiuiMemReclaimer;->interruptProcsCompaction()V
/* .line 323 */
return;
} // .end method
public Boolean isCompactNeeded ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "mode" # I */
/* .line 249 */
v0 = this.mReclaimer;
v0 = (( com.android.server.am.MiuiMemReclaimer ) v0 ).isCompactNeeded ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z
} // .end method
public void performCompaction ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .line 332 */
v0 = this.mReclaimer;
(( com.android.server.am.MiuiMemReclaimer ) v0 ).performCompaction ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->performCompaction(Ljava/lang/String;I)V
/* .line 333 */
return;
} // .end method
public void runGlobalCompaction ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "vmPressureLevel" # I */
/* .line 306 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z */
/* if-nez v0, :cond_0 */
/* .line 307 */
return;
/* .line 309 */
} // :cond_0
v0 = this.mReclaimer;
(( com.android.server.am.MiuiMemReclaimer ) v0 ).runGlobalCompaction ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->runGlobalCompaction(I)V
/* .line 310 */
return;
} // .end method
public void runProcCompaction ( com.miui.server.smartpower.IAppState$IRunningProcess p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "proc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .param p2, "mode" # I */
/* .line 241 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactSingleProcEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->isZramFreeEnough()Z */
/* if-nez v0, :cond_0 */
/* .line 244 */
} // :cond_0
v0 = this.mReclaimer;
(( com.android.server.am.MiuiMemReclaimer ) v0 ).runProcCompaction ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->runProcCompaction(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V
/* .line 245 */
return;
/* .line 242 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void runProcsCompaction ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 314 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->isZramFreeEnough()Z */
/* if-nez v0, :cond_0 */
/* .line 317 */
} // :cond_0
v0 = this.mReclaimer;
(( com.android.server.am.MiuiMemReclaimer ) v0 ).runProcsCompaction ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->runProcsCompaction(I)V
/* .line 318 */
return;
/* .line 315 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void setAppStartingMode ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "appStarting" # Z */
/* .line 254 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactSingleProcEnable:Z */
/* if-nez v0, :cond_0 */
/* .line 255 */
return;
/* .line 257 */
} // :cond_0
v0 = this.mReclaimer;
(( com.android.server.am.MiuiMemReclaimer ) v0 ).setAppStartingMode ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->setAppStartingMode(Z)V
/* .line 258 */
return;
} // .end method
public void writeLmkd ( Boolean p0 ) {
/* .locals 6 */
/* .param p1, "isForeground" # Z */
/* .line 279 */
/* sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sWriteEnable:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 280 */
} // :cond_0
v0 = this.mWriteLock;
/* monitor-enter v0 */
/* .line 281 */
try { // :try_start_0
v1 = this.mLmkdSocket;
/* if-nez v1, :cond_1 */
v1 = /* invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->openLmkdSocket()Z */
/* if-nez v1, :cond_1 */
/* .line 282 */
final String v1 = "MiuiMemoryService"; // const-string v1, "MiuiMemoryService"
final String v2 = "Write lmkd failed for socket error!"; // const-string v2, "Write lmkd failed for socket error!"
android.util.Slog .w ( v1,v2 );
/* .line 283 */
/* monitor-exit v0 */
return;
/* .line 285 */
} // :cond_1
/* const/16 v1, 0x8 */
java.nio.ByteBuffer .allocate ( v1 );
/* .line 286 */
/* .local v1, "buf":Ljava/nio/ByteBuffer; */
/* const/16 v2, 0x12 */
(( java.nio.ByteBuffer ) v1 ).putInt ( v2 ); // invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 287 */
int v2 = 0; // const/4 v2, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
int v3 = 1; // const/4 v3, 0x1
} // :cond_2
/* move v3, v2 */
} // :goto_0
(( java.nio.ByteBuffer ) v1 ).putInt ( v3 ); // invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 289 */
try { // :try_start_1
v3 = this.mLmkdOutputStream;
(( java.nio.ByteBuffer ) v1 ).array ( ); // invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B
v5 = (( java.nio.ByteBuffer ) v1 ).position ( ); // invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I
(( java.io.OutputStream ) v3 ).write ( v4, v2, v5 ); // invoke-virtual {v3, v4, v2, v5}, Ljava/io/OutputStream;->write([BII)V
/* .line 290 */
/* sget-boolean v2, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
final String v2 = "MiuiMemoryService"; // const-string v2, "MiuiMemoryService"
final String v3 = "Write lmkd success"; // const-string v3, "Write lmkd success"
android.util.Slog .d ( v2,v3 );
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 291 */
} // :cond_3
try { // :try_start_2
/* monitor-exit v0 */
return;
/* .line 292 */
/* :catch_0 */
/* move-exception v2 */
/* .line 293 */
/* .local v2, "e":Ljava/io/IOException; */
final String v3 = "MiuiMemoryService"; // const-string v3, "MiuiMemoryService"
final String v4 = "Write lmkd failed for IOException"; // const-string v4, "Write lmkd failed for IOException"
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 295 */
try { // :try_start_3
v3 = this.mLmkdSocket;
(( android.net.LocalSocket ) v3 ).close ( ); // invoke-virtual {v3}, Landroid/net/LocalSocket;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 298 */
/* .line 296 */
/* :catch_1 */
/* move-exception v3 */
/* .line 297 */
/* .local v3, "ex":Ljava/io/IOException; */
try { // :try_start_4
final String v4 = "MiuiMemoryService"; // const-string v4, "MiuiMemoryService"
final String v5 = "Close lmkd socket failed for IOException!"; // const-string v5, "Close lmkd socket failed for IOException!"
android.util.Slog .e ( v4,v5 );
/* .line 299 */
} // .end local v3 # "ex":Ljava/io/IOException;
} // :goto_1
int v3 = 0; // const/4 v3, 0x0
this.mLmkdSocket = v3;
/* .line 301 */
} // .end local v1 # "buf":Ljava/nio/ByteBuffer;
} // .end local v2 # "e":Ljava/io/IOException;
/* monitor-exit v0 */
/* .line 302 */
return;
/* .line 301 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* throw v1 */
} // .end method
