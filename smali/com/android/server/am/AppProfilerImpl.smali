.class public Lcom/android/server/am/AppProfilerImpl;
.super Ljava/lang/Object;
.source "AppProfilerImpl.java"

# interfaces
.implements Lcom/android/server/am/AppProfilerStub;


# static fields
.field private static final CMD_DUMPSYS:Ljava/lang/String; = "dumpsys"

.field private static final DEFAULT_DUMP_CPU_TIMEOUT_MILLISECONDS:J = 0xbb8L

.field private static final DUMP_CPU_TIMEOUT_PROPERTY:Ljava/lang/String; = "persist.sys.stability.dump_cpu.timeout"

.field private static final DUMP_PROCS_MEM_INTERVAL_MILLIS:I = 0x493e0

.field private static final LOW_MEM_FACTOR:I = 0xa

.field private static final MAX_LOW_MEM_FILE_NUM:I = 0x5

.field private static final MEMINFO_FORMAT:[I

.field private static final NATIVE_MEM_INFO:Ljava/lang/String; = "native"

.field private static final OOM_MEM_INFO:Ljava/lang/String; = "OomMeminfo"

.field private static final PSS_NORMAL_THRESHOLD:I = 0x4b000

.field public static final STALL_RATIO_FULL:I

.field public static final STALL_RATIO_SOME:I

.field static final STATE_START:I = 0x1

.field static final STATE_STOP:I = 0x2

.field private static final TAG:Ljava/lang/String; = "AppProfilerImpl"

.field private static testTrimMemActivityBg_workaround:Z


# instance fields
.field private final REPORT_OOM_MEMINFO_INTERVAL_MILLIS:J

.field private mLastDumpProcsMemTime:J

.field private mLastMemUsageReportTime:J

.field private mLastReportOomMemTime:J

.field private mMinOomScore:I

.field private mService:Lcom/android/server/am/ActivityManagerService;

.field private pssThresholdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 81
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/am/AppProfilerImpl;->MEMINFO_FORMAT:[I

    .line 98
    const-string v0, "persist.sys.testTrimMemActivityBg.wk.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/AppProfilerImpl;->testTrimMemActivityBg_workaround:Z

    .line 105
    const-string v0, "persist.sys.stall_ratio_some"

    const/16 v1, 0x4b

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/am/AppProfilerImpl;->STALL_RATIO_SOME:I

    .line 108
    const-string v0, "persist.sys.stall_ratio_full"

    const/16 v1, 0x32

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/am/AppProfilerImpl;->STALL_RATIO_FULL:I

    return-void

    nop

    :array_0
    .array-data 4
        0x120
        0x2020
        0xa
        0x120
        0x2020
        0xa
        0x120
        0x2020
        0xa
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I

    .line 104
    const-wide/32 v0, -0x3a980

    iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->mLastDumpProcsMemTime:J

    .line 204
    const-wide/32 v0, 0x36ee80

    iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->REPORT_OOM_MEMINFO_INTERVAL_MILLIS:J

    .line 205
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->mLastReportOomMemTime:J

    return-void
.end method

.method private buildMemInfo(Lcom/android/server/am/MemUsageBuilder;Z)Ljava/lang/String;
    .locals 20
    .param p1, "builder"    # Lcom/android/server/am/MemUsageBuilder;
    .param p2, "isDropbox"    # Z

    .line 478
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x400

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object v3, v0

    .line 479
    .local v3, "dropBuilder":Ljava/lang/StringBuilder;
    const/16 v0, 0xa

    if-eqz p2, :cond_0

    .line 480
    invoke-static {}, Lcom/android/server/ResourcePressureUtil;->currentPsiState()Ljava/lang/String;

    move-result-object v4

    .line 481
    .local v4, "psi":Ljava/lang/String;
    const-string v5, "Subject: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/android/server/am/MemUsageBuilder;->subject:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 482
    const-string v5, "Build: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    iget-object v5, v2, Lcom/android/server/am/MemUsageBuilder;->title:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    .end local v4    # "psi":Ljava/lang/String;
    :cond_0
    iget-object v4, v2, Lcom/android/server/am/MemUsageBuilder;->stack:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 487
    iget-object v4, v2, Lcom/android/server/am/MemUsageBuilder;->topProcs:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    iget-object v4, v2, Lcom/android/server/am/MemUsageBuilder;->fullNative:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    iget-object v4, v2, Lcom/android/server/am/MemUsageBuilder;->fullJava:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 490
    iget-object v4, v2, Lcom/android/server/am/MemUsageBuilder;->summary:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 491
    if-eqz p2, :cond_1

    .line 492
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    move-object v4, v0

    .line 493
    .local v4, "catSw":Ljava/io/StringWriter;
    iget-object v5, v1, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v5

    .line 494
    :try_start_0
    new-instance v8, Lcom/android/internal/util/FastPrintWriter;

    const/16 v0, 0x100

    const/4 v6, 0x0

    invoke-direct {v8, v4, v6, v0}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V

    .line 495
    .local v8, "catPw":Ljava/io/PrintWriter;
    new-array v9, v6, [Ljava/lang/String;

    .line 496
    .local v9, "emptyArgs":[Ljava/lang/String;
    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    .line 497
    iget-object v0, v1, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v14, v0, Lcom/android/server/am/ActivityManagerService;->mProcLock:Lcom/android/server/am/ActivityManagerGlobalLock;

    monitor-enter v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498
    :try_start_1
    iget-object v0, v1, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v6, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    invoke-virtual/range {v6 .. v13}, Lcom/android/server/am/ProcessList;->dumpProcessesLSP(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZLjava/lang/String;I)V

    .line 499
    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500
    :try_start_2
    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    .line 501
    iget-object v0, v1, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v10, v0, Lcom/android/server/am/ActivityManagerService;->mServices:Lcom/android/server/am/ActiveServices;

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v12, v8

    move-object v13, v9

    invoke-virtual/range {v10 .. v16}, Lcom/android/server/am/ActiveServices;->newServiceDumperLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZLjava/lang/String;)Lcom/android/server/am/ActiveServices$ServiceDumper;

    move-result-object v0

    .line 502
    invoke-virtual {v0}, Lcom/android/server/am/ActiveServices$ServiceDumper;->dumpLocked()V

    .line 503
    invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V

    .line 504
    iget-object v0, v1, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v10, v0, Lcom/android/server/am/ActivityManagerService;->mAtmInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

    const-string v11, "activities"

    const/4 v12, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, -0x1

    move-object v13, v8

    move-object v14, v9

    invoke-virtual/range {v10 .. v19}, Lcom/android/server/wm/ActivityTaskManagerInternal;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZZLjava/lang/String;I)V

    .line 506
    invoke-virtual {v8}, Ljava/io/PrintWriter;->flush()V

    .line 507
    .end local v8    # "catPw":Ljava/io/PrintWriter;
    .end local v9    # "emptyArgs":[Ljava/lang/String;
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 508
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 499
    .restart local v8    # "catPw":Ljava/io/PrintWriter;
    .restart local v9    # "emptyArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .end local v3    # "dropBuilder":Ljava/lang/StringBuilder;
    .end local v4    # "catSw":Ljava/io/StringWriter;
    .end local p0    # "this":Lcom/android/server/am/AppProfilerImpl;
    .end local p1    # "builder":Lcom/android/server/am/MemUsageBuilder;
    .end local p2    # "isDropbox":Z
    :try_start_4
    throw v0

    .line 507
    .end local v8    # "catPw":Ljava/io/PrintWriter;
    .end local v9    # "emptyArgs":[Ljava/lang/String;
    .restart local v3    # "dropBuilder":Ljava/lang/StringBuilder;
    .restart local v4    # "catSw":Ljava/io/StringWriter;
    .restart local p0    # "this":Lcom/android/server/am/AppProfilerImpl;
    .restart local p1    # "builder":Lcom/android/server/am/MemUsageBuilder;
    .restart local p2    # "isDropbox":Z
    :catchall_1
    move-exception v0

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 510
    .end local v4    # "catSw":Ljava/io/StringWriter;
    :cond_1
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private collectProcessMemInfos(Ljava/util/function/Predicate;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/function/Predicate<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessMemInfo;",
            ">;"
        }
    .end annotation

    .line 515
    .local p1, "predicate":Ljava/util/function/Predicate;, "Ljava/util/function/Predicate<Lcom/android/server/am/ProcessRecord;>;"
    iget-object v0, p0, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 516
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I

    move-result v1

    .line 517
    .local v1, "lruSize":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 518
    .local v2, "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;"
    iget-object v3, p0, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    new-instance v4, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda1;

    invoke-direct {v4, p1, v2}, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda1;-><init>(Ljava/util/function/Predicate;Ljava/util/ArrayList;)V

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Lcom/android/server/am/ProcessList;->forEachLruProcessesLOSP(ZLjava/util/function/Consumer;)V

    .line 526
    monitor-exit v0

    return-object v2

    .line 527
    .end local v1    # "lruSize":I
    .end local v2    # "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getDumpCpuTimeoutThreshold()J
    .locals 3

    .line 473
    const-string v0, "persist.sys.stability.dump_cpu.timeout"

    const-wide/16 v1, 0xbb8

    invoke-static {v0, v1, v2}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private initPssThresholdMap()V
    .locals 10

    .line 287
    const-string v0, "Init Memory ThresholdMap"

    const-string v1, "AppProfilerImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-object v0, p0, Lcom/android/server/am/AppProfilerImpl;->pssThresholdMap:Ljava/util/HashMap;

    if-nez v0, :cond_3

    .line 289
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/AppProfilerImpl;->pssThresholdMap:Ljava/util/HashMap;

    .line 291
    sget-boolean v0, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z

    if-eqz v0, :cond_0

    .line 292
    const-string v0, "package_pss_threshold_lite"

    .local v0, "listName":Ljava/lang/String;
    goto :goto_0

    .line 294
    .end local v0    # "listName":Ljava/lang/String;
    :cond_0
    const-string v0, "package_pss_threshold"

    .line 297
    .restart local v0    # "listName":Ljava/lang/String;
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    .line 298
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "array"

    const-string v4, "android.miui"

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 300
    .local v2, "pssThresholdId":I
    if-lez v2, :cond_2

    .line 301
    iget-object v3, p0, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 302
    .local v3, "pssThresholdList":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v5, v3

    if-ge v4, v5, :cond_1

    .line 303
    aget-object v5, v3, v4

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 304
    .local v5, "packageThreshold":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    mul-int/lit16 v6, v6, 0x400

    .line 305
    .local v6, "packagePssThreshold":I
    iget-object v7, p0, Lcom/android/server/am/AppProfilerImpl;->pssThresholdMap:Ljava/util/HashMap;

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    nop

    .end local v5    # "packageThreshold":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "packagePssThreshold":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 308
    .end local v4    # "i":I
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " size:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 310
    .end local v3    # "pssThresholdList":[Ljava/lang/String;
    :cond_2
    const-string v3, "No Memory Threshold Id"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    .end local v2    # "pssThresholdId":I
    :goto_2
    goto :goto_3

    .line 312
    :catch_0
    move-exception v1

    .line 315
    .end local v0    # "listName":Ljava/lang/String;
    :cond_3
    :goto_3
    return-void
.end method

.method static synthetic lambda$collectProcessMemInfos$2(Ljava/util/function/Predicate;Ljava/util/ArrayList;Lcom/android/server/am/ProcessRecord;)V
    .locals 9
    .param p0, "predicate"    # Ljava/util/function/Predicate;
    .param p1, "memInfos"    # Ljava/util/ArrayList;
    .param p2, "rec"    # Lcom/android/server/am/ProcessRecord;

    .line 519
    invoke-interface {p0, p2}, Ljava/util/function/Predicate;->test(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 521
    :cond_0
    iget-object v0, p2, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 522
    .local v0, "state":Lcom/android/server/am/ProcessStateRecord;
    new-instance v8, Lcom/android/server/am/ProcessMemInfo;

    iget-object v2, p2, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v3

    .line 523
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I

    move-result v4

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I

    move-result v5

    .line 524
    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->makeAdjReason()Ljava/lang/String;

    move-result-object v7

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/android/server/am/ProcessMemInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)V

    .line 522
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    return-void
.end method

.method static synthetic lambda$dumpCpuInfo$1(Ljava/lang/Runnable;)V
    .locals 2
    .param p0, "runnable"    # Ljava/lang/Runnable;

    .line 452
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "CPU-Dumper"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic lambda$getMemInfo$0(Lcom/android/server/am/ProcessRecord;)Z
    .locals 1
    .param p0, "rec"    # Lcom/android/server/am/ProcessRecord;

    .line 406
    invoke-virtual {p0}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private reportPssRecordIfNeeded(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 4
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "pss"    # J

    .line 318
    iget-object v0, p0, Lcom/android/server/am/AppProfilerImpl;->pssThresholdMap:Ljava/util/HashMap;

    const-string v1, "Package."

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/android/server/am/AppProfilerImpl;->pssThresholdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v2, v0

    cmp-long v0, p3, v2

    if-lez v0, :cond_1

    .line 320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 323
    :cond_0
    const-wide/32 v2, 0x4b000

    cmp-long v0, p3, v2

    if-lez v0, :cond_1

    .line 324
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V

    .line 327
    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public checkMemoryPsi(Z)V
    .locals 3
    .param p1, "watchdog"    # Z

    .line 532
    invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMemPsiCritical()Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    return-void

    .line 535
    :cond_0
    if-eqz p1, :cond_1

    .line 537
    const/16 v0, 0x6d

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->doSysRqInterface(C)V

    goto :goto_0

    .line 539
    :cond_1
    const/16 v0, 0x77

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->doSysRqInterface(C)V

    .line 540
    const/16 v0, 0x6c

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->doSysRqInterface(C)V

    .line 541
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppProfilerImpl;->getMemInfo(Z)Ljava/lang/String;

    move-result-object v0

    .line 542
    .local v0, "meminfo":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Critical memory pressure, dumping memory info \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AppProfilerImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    .end local v0    # "meminfo":Ljava/lang/String;
    :goto_0
    return-void
.end method

.method public dumpCpuInfo(Lcom/android/server/utils/PriorityDump$PriorityDumper;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)Z
    .locals 8
    .param p1, "dumper"    # Lcom/android/server/utils/PriorityDump$PriorityDumper;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "pw"    # Ljava/io/PrintWriter;
    .param p4, "args"    # [Ljava/lang/String;

    .line 445
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v7, Lcom/android/server/am/AppProfilerImpl$1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/AppProfilerImpl$1;-><init>(Lcom/android/server/am/AppProfilerImpl;Lcom/android/server/utils/PriorityDump$PriorityDumper;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-direct {v0, v7}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 452
    .local v0, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<*>;"
    new-instance v1, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1}, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda2;-><init>()V

    .line 453
    .local v1, "executor":Ljava/util/concurrent/Executor;
    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 455
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0}, Lcom/android/server/am/AppProfilerImpl;->getDumpCpuTimeoutThreshold()J

    move-result-wide v3

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v5}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 460
    :catch_0
    move-exception v3

    .line 461
    .local v3, "e":Ljava/util/concurrent/TimeoutException;
    const-string v4, "AppProfilerImpl"

    const-string v5, "dump cpuinfo timeout, interrupted!"

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    invoke-virtual {v0, v2}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 463
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    .line 464
    .local v4, "callingPid":I
    invoke-static {v4}, Lcom/android/server/ScoutHelper;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v5

    .line 465
    .local v5, "procName":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "dumpsys"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 466
    invoke-static {v4}, Landroid/os/Process;->killProcess(I)V

    goto :goto_1

    .line 458
    .end local v3    # "e":Ljava/util/concurrent/TimeoutException;
    .end local v4    # "callingPid":I
    .end local v5    # "procName":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 459
    .local v3, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .end local v3    # "e":Ljava/lang/InterruptedException;
    goto :goto_0

    .line 456
    :catch_2
    move-exception v3

    .line 457
    .local v3, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v3}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V

    .line 468
    .end local v3    # "e":Ljava/util/concurrent/ExecutionException;
    :goto_0
    nop

    .line 469
    :cond_0
    :goto_1
    return v2
.end method

.method public dumpProcsMemInfo()V
    .locals 6

    .line 397
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/am/AppProfilerImpl;->getMemInfo(Z)Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, "dropboxStr":Ljava/lang/String;
    const-string v1, "scout_procs"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/miui/server/stability/ScoutMemoryUtils;->getExceptionFile(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 400
    .local v1, "exFile":Ljava/io/File;
    invoke-static {v0, v1}, Lcom/miui/server/stability/ScoutMemoryUtils;->dumpInfoToFile(Ljava/lang/String;Ljava/io/File;)Z

    move-result v3

    .line 401
    .local v3, "suc":Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dump processes memory to file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Succeeded? "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AppProfilerImpl"

    invoke-static {v5, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const/4 v4, 0x5

    invoke-static {v2, v4}, Lcom/miui/server/stability/ScoutMemoryUtils;->deleteOldFiles(II)V

    .line 403
    return-void
.end method

.method public getMemInfo(Z)Ljava/lang/String;
    .locals 5
    .param p1, "isDropbox"    # Z

    .line 406
    new-instance v0, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda0;-><init>()V

    invoke-direct {p0, v0}, Lcom/android/server/am/AppProfilerImpl;->collectProcessMemInfos(Ljava/util/function/Predicate;)Ljava/util/ArrayList;

    move-result-object v0

    .line 407
    .local v0, "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;"
    const-string v1, "Scout low memory"

    .line 408
    .local v1, "title":Ljava/lang/String;
    new-instance v2, Lcom/android/server/am/MemUsageBuilder;

    iget-object v3, p0, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mAppProfiler:Lcom/android/server/am/AppProfiler;

    const-string v4, "Scout low memory"

    invoke-direct {v2, v3, v0, v4}, Lcom/android/server/am/MemUsageBuilder;-><init>(Lcom/android/server/am/AppProfiler;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 409
    .local v2, "builder":Lcom/android/server/am/MemUsageBuilder;
    invoke-virtual {v2}, Lcom/android/server/am/MemUsageBuilder;->buildAll()V

    .line 410
    invoke-direct {p0, v2, p1}, Lcom/android/server/am/AppProfilerImpl;->buildMemInfo(Lcom/android/server/am/MemUsageBuilder;Z)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public init(Lcom/android/server/am/ActivityManagerService;)V
    .locals 2
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 113
    iput-object p1, p0, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 114
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V

    .line 115
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ActivityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V

    .line 116
    invoke-direct {p0}, Lcom/android/server/am/AppProfilerImpl;->initPssThresholdMap()V

    .line 117
    return-void
.end method

.method public isSystemLowMem()Z
    .locals 10

    .line 330
    const/4 v0, 0x3

    new-array v0, v0, [J

    .line 331
    .local v0, "longs":[J
    sget-object v1, Lcom/android/server/am/AppProfilerImpl;->MEMINFO_FORMAT:[I

    const/4 v2, 0x0

    const-string v3, "/proc/meminfo"

    invoke-static {v3, v1, v2, v0, v2}, Landroid/os/Process;->readProcFile(Ljava/lang/String;[I[Ljava/lang/String;[J[F)Z

    move-result v1

    const-string v2, "AppProfilerImpl"

    const/4 v3, 0x0

    if-nez v1, :cond_0

    .line 332
    const-string v1, "Read file /proc/meminfo failed!"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    return v3

    .line 335
    :cond_0
    const/4 v1, 0x2

    aget-wide v4, v0, v1

    .line 336
    .local v4, "memAvailableKb":J
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    .line 337
    const-string v1, "MemAvailable is 0! This should never happen!"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    return v3

    .line 340
    :cond_1
    aget-wide v1, v0, v3

    .line 345
    .local v1, "memTotalKb":J
    div-long v6, v1, v4

    const-wide/16 v8, 0xa

    cmp-long v6, v6, v8

    if-ltz v6, :cond_2

    const/4 v3, 0x1

    :cond_2
    return v3
.end method

.method public isSystemLowMemPsi()Z
    .locals 3

    .line 416
    :try_start_0
    sget-object v0, Lcom/android/server/PsiParser$ResourceType;->MEM:Lcom/android/server/PsiParser$ResourceType;

    invoke-static {v0}, Lcom/android/server/PsiParser;->currentParsedPsiState(Lcom/android/server/PsiParser$ResourceType;)Lcom/android/server/PsiParser$Psi;

    move-result-object v0

    .line 419
    .local v0, "psi":Lcom/android/server/PsiParser$Psi;
    iget-object v1, v0, Lcom/android/server/PsiParser$Psi;->full:Lcom/android/server/PsiParser$Prop;

    iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg10:F

    const/high16 v2, 0x40000000    # 2.0f

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_1

    iget-object v1, v0, Lcom/android/server/PsiParser$Psi;->some:Lcom/android/server/PsiParser$Prop;

    iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg10:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/high16 v2, 0x41200000    # 10.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 420
    .end local v0    # "psi":Lcom/android/server/PsiParser$Psi;
    :catch_0
    move-exception v0

    .line 422
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Get psi failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Use isSystemLowMem instead."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AppProfilerImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMem()Z

    move-result v1

    return v1
.end method

.method public isSystemLowMemPsiCritical()Z
    .locals 4

    .line 430
    :try_start_0
    sget-object v0, Lcom/android/server/PsiParser$ResourceType;->MEM:Lcom/android/server/PsiParser$ResourceType;

    invoke-static {v0}, Lcom/android/server/PsiParser;->currentParsedPsiState(Lcom/android/server/PsiParser$ResourceType;)Lcom/android/server/PsiParser$Psi;

    move-result-object v0

    .line 431
    .local v0, "psi":Lcom/android/server/PsiParser$Psi;
    iget-object v1, v0, Lcom/android/server/PsiParser$Psi;->some:Lcom/android/server/PsiParser$Prop;

    iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg10:F

    sget v2, Lcom/android/server/am/AppProfilerImpl;->STALL_RATIO_SOME:I

    int-to-float v3, v2

    cmpl-float v1, v1, v3

    if-gez v1, :cond_1

    iget-object v1, v0, Lcom/android/server/PsiParser$Psi;->some:Lcom/android/server/PsiParser$Prop;

    iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg60:F

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gez v1, :cond_1

    iget-object v1, v0, Lcom/android/server/PsiParser$Psi;->full:Lcom/android/server/PsiParser$Prop;

    iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg10:F

    sget v2, Lcom/android/server/am/AppProfilerImpl;->STALL_RATIO_FULL:I

    int-to-float v3, v2

    cmpl-float v1, v1, v3

    if-gez v1, :cond_1

    iget-object v1, v0, Lcom/android/server/PsiParser$Psi;->full:Lcom/android/server/PsiParser$Prop;

    iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg60:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 435
    .end local v0    # "psi":Lcom/android/server/PsiParser$Psi;
    :catch_0
    move-exception v0

    .line 437
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Get psi failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Use isSystemLowMem instead."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AppProfilerImpl"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMem()Z

    move-result v1

    return v1
.end method

.method public reportMemUsage(Ljava/util/ArrayList;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessMemInfo;",
            ">;)V"
        }
    .end annotation

    .line 366
    .local p1, "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;"
    move-object/from16 v1, p0

    new-instance v0, Lcom/android/server/am/ScoutMeminfo;

    invoke-direct {v0}, Lcom/android/server/am/ScoutMeminfo;-><init>()V

    move-object v2, v0

    .line 367
    .local v2, "scoutInfo":Lcom/android/server/am/ScoutMeminfo;
    const-string v3, "Low on memory"

    .line 368
    .local v3, "title":Ljava/lang/String;
    new-instance v0, Lcom/android/server/am/MemUsageBuilder;

    iget-object v4, v1, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    iget-object v4, v4, Lcom/android/server/am/ActivityManagerService;->mAppProfiler:Lcom/android/server/am/AppProfiler;

    const-string v5, "Low on memory"

    move-object/from16 v6, p1

    invoke-direct {v0, v4, v6, v5}, Lcom/android/server/am/MemUsageBuilder;-><init>(Lcom/android/server/am/AppProfiler;Ljava/util/ArrayList;Ljava/lang/String;)V

    move-object v4, v0

    .line 369
    .local v4, "builder":Lcom/android/server/am/MemUsageBuilder;
    invoke-virtual {v4, v2}, Lcom/android/server/am/MemUsageBuilder;->setScoutInfo(Lcom/android/server/am/ScoutMeminfo;)V

    .line 370
    invoke-virtual {v4}, Lcom/android/server/am/MemUsageBuilder;->buildAll()V

    .line 372
    const-string v0, "AppProfilerImpl"

    const-string v5, "Low on memory:"

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    const-string v0, "AppProfilerImpl"

    iget-object v5, v4, Lcom/android/server/am/MemUsageBuilder;->shortNative:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    const-string v0, "AppProfilerImpl"

    iget-object v5, v4, Lcom/android/server/am/MemUsageBuilder;->fullJava:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    const-string v0, "AppProfilerImpl"

    iget-object v5, v4, Lcom/android/server/am/MemUsageBuilder;->summary:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    iget-wide v7, v4, Lcom/android/server/am/MemUsageBuilder;->totalPss:J

    invoke-virtual {v2, v7, v8}, Lcom/android/server/am/ScoutMeminfo;->setTotalPss(J)V

    .line 378
    iget-wide v7, v4, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J

    invoke-virtual {v2, v7, v8}, Lcom/android/server/am/ScoutMeminfo;->setTotalSwapPss(J)V

    .line 379
    iget-wide v7, v4, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J

    invoke-virtual {v2, v7, v8}, Lcom/android/server/am/ScoutMeminfo;->setCachedPss(J)V

    .line 380
    invoke-static {}, Lcom/android/server/am/AppProfilerStub;->getInstance()Lcom/android/server/am/AppProfilerStub;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/server/am/AppProfilerStub;->reportScoutLowMemory(Lcom/android/server/am/ScoutMeminfo;)V

    .line 382
    const/4 v0, 0x1

    invoke-direct {v1, v4, v0}, Lcom/android/server/am/AppProfilerImpl;->buildMemInfo(Lcom/android/server/am/MemUsageBuilder;Z)Ljava/lang/String;

    move-result-object v5

    .line 383
    .local v5, "dropboxStr":Ljava/lang/String;
    const/16 v0, 0x51

    invoke-static {v0}, Lcom/android/internal/util/FrameworkStatsLog;->write(I)V

    .line 384
    iget-object v7, v1, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    const-string v8, "lowmem"

    const/4 v9, 0x0

    const-string/jumbo v10, "system_server"

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    iget-object v14, v4, Lcom/android/server/am/MemUsageBuilder;->subject:Ljava/lang/String;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v15, v5

    invoke-virtual/range {v7 .. v20}, Lcom/android/server/am/ActivityManagerService;->addErrorToDropBox(Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;Ljava/lang/Float;Landroid/os/incremental/IncrementalMetrics;Ljava/util/UUID;)V

    .line 387
    iget-object v7, v1, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v7

    .line 388
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 389
    .local v8, "now":J
    iget-wide v10, v1, Lcom/android/server/am/AppProfilerImpl;->mLastMemUsageReportTime:J

    cmp-long v0, v10, v8

    if-gez v0, :cond_0

    .line 390
    iput-wide v8, v1, Lcom/android/server/am/AppProfilerImpl;->mLastMemUsageReportTime:J

    .line 392
    .end local v8    # "now":J
    :cond_0
    monitor-exit v7

    .line 393
    return-void

    .line 392
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reportMemoryStandardProcessControlKillMessage(Ljava/lang/String;IIJ)V
    .locals 7
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "pid"    # I
    .param p3, "uid"    # I
    .param p4, "pss"    # J

    .line 195
    :try_start_0
    invoke-static {}, Lcom/miui/daemon/performance/PerfShielderManager;->getService()Lcom/android/internal/app/IPerfShielder;

    move-result-object v0

    .line 196
    .local v0, "perfShielder":Lcom/android/internal/app/IPerfShielder;
    if-eqz v0, :cond_0

    .line 197
    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Lcom/android/internal/app/IPerfShielder;->reportKillMessage(Ljava/lang/String;IIJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    .end local v0    # "perfShielder":Lcom/android/internal/app/IPerfShielder;
    :cond_0
    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 201
    :goto_0
    return-void
.end method

.method public reportOomMemRecordIfNeeded(Lcom/android/server/am/ActivityManagerService;Lcom/android/internal/os/ProcessCpuTracker;)V
    .locals 19
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "processCpuTracker"    # Lcom/android/internal/os/ProcessCpuTracker;

    .line 210
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 211
    .local v4, "now":J
    iget-wide v6, v1, Lcom/android/server/am/AppProfilerImpl;->mLastReportOomMemTime:J

    sub-long v6, v4, v6

    const-wide/32 v8, 0x36ee80

    cmp-long v0, v6, v8

    if-gez v0, :cond_0

    .line 212
    return-void

    .line 214
    :cond_0
    iput-wide v4, v1, Lcom/android/server/am/AppProfilerImpl;->mLastReportOomMemTime:J

    .line 216
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    move-object v6, v0

    .line 217
    .local v6, "procs":Landroid/util/SparseIntArray;
    monitor-enter p1

    .line 218
    :try_start_0
    iget-object v0, v2, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v0

    .line 219
    .local v0, "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    .local v7, "i":I
    :goto_0
    if-ltz v7, :cond_1

    .line 220
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/am/ProcessRecord;

    .line 221
    .local v8, "r":Lcom/android/server/am/ProcessRecord;
    iget v9, v8, Lcom/android/server/am/ProcessRecord;->mPid:I

    iget-object v10, v8, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v10}, Lcom/android/server/am/ProcessStateRecord;->getSetAdjWithServices()I

    move-result v10

    invoke-virtual {v6, v9, v10}, Landroid/util/SparseIntArray;->put(II)V

    .line 219
    .end local v8    # "r":Lcom/android/server/am/ProcessRecord;
    add-int/lit8 v7, v7, -0x1

    goto :goto_0

    .line 223
    .end local v0    # "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    .end local v7    # "i":I
    :cond_1
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 225
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    move-object v7, v0

    .line 226
    .local v7, "nativeProcs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/android/server/am/ActivityManagerService;->updateCpuStatsNow()V

    .line 227
    monitor-enter p2

    .line 228
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Lcom/android/internal/os/ProcessCpuTracker;->countStats()I

    move-result v0

    .line 229
    .local v0, "N":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    const-wide/16 v9, 0x0

    if-ge v8, v0, :cond_3

    .line 230
    invoke-virtual {v3, v8}, Lcom/android/internal/os/ProcessCpuTracker;->getStats(I)Lcom/android/internal/os/ProcessCpuTracker$Stats;

    move-result-object v11

    .line 231
    .local v11, "st":Lcom/android/internal/os/ProcessCpuTracker$Stats;
    iget-wide v12, v11, Lcom/android/internal/os/ProcessCpuTracker$Stats;->vsize:J

    cmp-long v9, v12, v9

    if-lez v9, :cond_2

    iget v9, v11, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I

    invoke-virtual {v6, v9}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v9

    if-gez v9, :cond_2

    .line 232
    iget v9, v11, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I

    iget-object v10, v11, Lcom/android/internal/os/ProcessCpuTracker$Stats;->name:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 229
    .end local v11    # "st":Lcom/android/internal/os/ProcessCpuTracker$Stats;
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 235
    .end local v0    # "N":I
    .end local v8    # "i":I
    :cond_3
    monitor-exit p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    sget-object v0, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_LABEL:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [J

    .line 238
    .local v0, "oomPss":[J
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_2
    invoke-virtual {v6}, Landroid/util/SparseIntArray;->size()I

    move-result v11

    const/4 v12, 0x0

    if-ge v8, v11, :cond_7

    .line 239
    invoke-virtual {v6, v8}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v11

    .line 240
    .local v11, "pid":I
    invoke-virtual {v6, v11}, Landroid/util/SparseIntArray;->get(I)I

    move-result v13

    .line 241
    .local v13, "oomAdj":I
    invoke-static {v11, v12, v12}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v14

    .line 242
    .local v14, "myTotalPss":J
    const/4 v12, 0x0

    .local v12, "oomIndex":I
    :goto_3
    array-length v9, v0

    if-ge v12, v9, :cond_6

    .line 243
    array-length v9, v0

    add-int/lit8 v9, v9, -0x1

    if-eq v12, v9, :cond_5

    sget-object v9, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_ADJ:[I

    aget v9, v9, v12

    if-lt v13, v9, :cond_4

    sget-object v9, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_ADJ:[I

    add-int/lit8 v10, v12, 0x1

    aget v9, v9, v10

    if-ge v13, v9, :cond_4

    goto :goto_4

    .line 242
    :cond_4
    add-int/lit8 v12, v12, 0x1

    const-wide/16 v9, 0x0

    goto :goto_3

    .line 246
    :cond_5
    :goto_4
    aget-wide v9, v0, v12

    add-long/2addr v9, v14

    aput-wide v9, v0, v12

    .line 247
    nop

    .line 238
    .end local v11    # "pid":I
    .end local v12    # "oomIndex":I
    .end local v13    # "oomAdj":I
    .end local v14    # "myTotalPss":J
    :cond_6
    add-int/lit8 v8, v8, 0x1

    const-wide/16 v9, 0x0

    goto :goto_2

    .line 252
    .end local v8    # "i":I
    :cond_7
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_5
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v9

    if-ge v8, v9, :cond_9

    .line 253
    invoke-virtual {v7, v8}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v9

    .line 254
    .local v9, "pid":I
    invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 255
    .local v10, "name":Ljava/lang/String;
    invoke-static {v9, v12, v12}, Landroid/os/Debug;->getPss(I[J[J)J

    move-result-wide v13

    .line 256
    .local v13, "myTotalPss":J
    const-wide/16 v15, 0x0

    cmp-long v11, v13, v15

    if-nez v11, :cond_8

    goto :goto_6

    .line 257
    :cond_8
    const/4 v11, 0x0

    aget-wide v17, v0, v11

    add-long v17, v17, v13

    aput-wide v17, v0, v11

    .line 259
    const-string v11, "native"

    invoke-virtual {v1, v10, v11, v13, v14}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V

    .line 252
    .end local v9    # "pid":I
    .end local v10    # "name":Ljava/lang/String;
    .end local v13    # "myTotalPss":J
    :goto_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 262
    .end local v8    # "i":I
    :cond_9
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_7
    sget-object v9, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_LABEL:[Ljava/lang/String;

    array-length v9, v9

    if-ge v8, v9, :cond_b

    .line 263
    aget-wide v9, v0, v8

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-nez v9, :cond_a

    goto :goto_8

    .line 264
    :cond_a
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "OomMeminfo."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/android/server/am/ActivityManagerService;->DUMP_MEM_OOM_LABEL:[Ljava/lang/String;

    aget-object v10, v10, v8

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "OomMeminfo"

    aget-wide v13, v0, v8

    invoke-virtual {v1, v9, v10, v13, v14}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V

    .line 262
    :goto_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 267
    .end local v8    # "i":I
    :cond_b
    return-void

    .line 235
    .end local v0    # "oomPss":[J
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 223
    .end local v7    # "nativeProcs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public reportPackagePss(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;)V
    .locals 9
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 271
    const-wide/16 v0, 0x0

    .line 272
    .local v0, "pkgPss":J
    monitor-enter p1

    .line 273
    :try_start_0
    iget-object v2, p1, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v2

    .line 274
    .local v2, "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 275
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecord;

    .line 276
    .local v4, "r":Lcom/android/server/am/ProcessRecord;
    iget-object v5, v4, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 277
    iget-object v5, v4, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_0

    .line 278
    monitor-exit p1

    return-void

    .line 279
    :cond_0
    iget-object v5, v4, Lcom/android/server/am/ProcessRecord;->mProfile:Lcom/android/server/am/ProcessProfileRecord;

    invoke-virtual {v5}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J

    move-result-wide v5

    add-long/2addr v0, v5

    .line 274
    .end local v4    # "r":Lcom/android/server/am/ProcessRecord;
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 282
    .end local v2    # "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    .end local v3    # "i":I
    :cond_2
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Package."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, p2, v0, v1}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecordIfNeeded(Ljava/lang/String;Ljava/lang/String;J)V

    .line 284
    return-void

    .line 282
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 10
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "pss"    # J

    .line 170
    const-string v0, "1.0"

    .line 171
    .local v0, "versionName":Ljava/lang/String;
    const/4 v1, 0x1

    .line 173
    .local v1, "versionCode":I
    :try_start_0
    const-string v2, "OomMeminfo"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "native"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 174
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    .line 175
    .local v2, "pm":Landroid/content/pm/IPackageManager;
    if-nez v2, :cond_0

    .line 176
    return-void

    .line 179
    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    const-wide/16 v4, 0x0

    invoke-interface {v2, p2, v4, v5, v3}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 180
    .local v3, "pi":Landroid/content/pm/PackageInfo;
    if-nez v3, :cond_1

    .line 181
    return-void

    .line 183
    :cond_1
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object v0, v4

    .line 184
    iget v4, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    move v1, v4

    .line 186
    .end local v2    # "pm":Landroid/content/pm/IPackageManager;
    .end local v3    # "pi":Landroid/content/pm/PackageInfo;
    :cond_2
    invoke-static {}, Lcom/miui/daemon/performance/PerfShielderManager;->getService()Lcom/android/internal/app/IPerfShielder;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    move-wide v6, p3

    move-object v8, v0

    move v9, v1

    invoke-interface/range {v3 .. v9}, Lcom/android/internal/app/IPerfShielder;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    goto :goto_0

    .line 188
    :catch_0
    move-exception v2

    .line 190
    :goto_0
    return-void
.end method

.method public reportScoutLowMemory(I)V
    .locals 8
    .param p1, "adj"    # I

    .line 126
    iget v0, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I

    if-ge p1, v0, :cond_0

    .line 127
    iput p1, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/AppProfilerImpl;->mService:Lcom/android/server/am/ActivityManagerService;

    if-nez v0, :cond_1

    .line 131
    return-void

    .line 134
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 135
    .local v0, "now":J
    const-string v2, "MIUIScout Memory"

    const/16 v3, 0x258

    if-ltz p1, :cond_2

    if-gt p1, v3, :cond_2

    iget-wide v4, p0, Lcom/android/server/am/AppProfilerImpl;->mLastDumpProcsMemTime:J

    const-wide/32 v6, 0x493e0

    add-long/2addr v4, v6

    cmp-long v4, v0, v4

    if-lez v4, :cond_2

    .line 137
    invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMemPsi()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 138
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Scout dump proc mem, current killed adj= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-static {}, Lcom/android/server/ScoutSystemMonitor;->getInstance()Lcom/android/server/ScoutSystemMonitor;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/android/server/ScoutSystemMonitor;->setWorkMessage(I)V

    .line 140
    iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->mLastDumpProcsMemTime:J

    .line 143
    :cond_2
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableScoutMemory()Z

    move-result v4

    if-nez v4, :cond_3

    .line 144
    return-void

    .line 147
    :cond_3
    if-ltz p1, :cond_4

    if-gt p1, v3, :cond_4

    iget-wide v3, p0, Lcom/android/server/am/AppProfilerImpl;->mLastMemUsageReportTime:J

    const-wide/32 v5, 0x2bf20

    add-long/2addr v3, v5

    cmp-long v3, v0, v3

    if-lez v3, :cond_4

    .line 149
    invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMemPsi()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 150
    iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->mLastMemUsageReportTime:J

    .line 151
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Scout report Low memory,currently killed process adj= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-static {}, Lcom/android/server/ScoutSystemMonitor;->getInstance()Lcom/android/server/ScoutSystemMonitor;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/server/ScoutSystemMonitor;->setWorkMessage(I)V

    .line 155
    :cond_4
    return-void
.end method

.method public reportScoutLowMemory(Lcom/android/server/am/ScoutMeminfo;)V
    .locals 1
    .param p1, "scoutMeminfo"    # Lcom/android/server/am/ScoutMeminfo;

    .line 121
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkScoutLowMemory(Lcom/android/server/am/ScoutMeminfo;)V

    .line 122
    return-void
.end method

.method public reportScoutLowMemoryState(I)V
    .locals 4
    .param p1, "state"    # I

    .line 159
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 160
    iget v0, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I

    const/16 v1, 0x258

    if-gt v0, v1, :cond_0

    .line 161
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 162
    .local v0, "timestamp":J
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I

    invoke-virtual {v2, v3, v0, v1}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportLmkKill(IJ)V

    .line 164
    .end local v0    # "timestamp":J
    :cond_0
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I

    .line 166
    :cond_1
    return-void
.end method

.method public testTrimMemActivityBgWk(Ljava/lang/String;)Z
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;

    .line 355
    sget-boolean v0, Lcom/android/server/am/AppProfilerImpl;->testTrimMemActivityBg_workaround:Z

    if-eqz v0, :cond_0

    const-string v0, "com.android.app1:android.app.stubs.TrimMemService:trimmem_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    const-string v0, "AppProfilerImpl"

    const-string/jumbo v1, "special case."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    const/4 v0, 0x1

    return v0

    .line 359
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public updateCameraForegroundState(Z)V
    .locals 1
    .param p1, "isCameraForeground"    # Z

    .line 349
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->updateCameraForegroundState(Z)V

    .line 350
    return-void
.end method
