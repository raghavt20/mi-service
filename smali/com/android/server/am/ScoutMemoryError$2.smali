.class Lcom/android/server/am/ScoutMemoryError$2;
.super Ljava/lang/Object;
.source "ScoutMemoryError.java"

# interfaces
.implements Lcom/android/server/am/MiuiWarnings$WarningCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ScoutMemoryError;->showDisplayMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ScoutMemoryError;

.field final synthetic val$app:Lcom/android/server/am/ProcessRecord;

.field final synthetic val$errorInfo:Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

.field final synthetic val$reason:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/am/ScoutMemoryError;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ScoutMemoryError;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 90
    iput-object p1, p0, Lcom/android/server/am/ScoutMemoryError$2;->this$0:Lcom/android/server/am/ScoutMemoryError;

    iput-object p2, p0, Lcom/android/server/am/ScoutMemoryError$2;->val$app:Lcom/android/server/am/ProcessRecord;

    iput-object p3, p0, Lcom/android/server/am/ScoutMemoryError$2;->val$reason:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/server/am/ScoutMemoryError$2;->val$errorInfo:Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallback(Z)V
    .locals 3
    .param p1, "positive"    # Z

    .line 93
    const-string v0, "ScoutMemoryError"

    if-eqz p1, :cond_0

    .line 94
    const-string/jumbo v1, "showDisplayMemoryErrorDialog ok"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v0, p0, Lcom/android/server/am/ScoutMemoryError$2;->this$0:Lcom/android/server/am/ScoutMemoryError;

    iget-object v1, p0, Lcom/android/server/am/ScoutMemoryError$2;->val$app:Lcom/android/server/am/ProcessRecord;

    iget-object v2, p0, Lcom/android/server/am/ScoutMemoryError$2;->val$reason:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ScoutMemoryError;->scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z

    .line 96
    iget-object v0, p0, Lcom/android/server/am/ScoutMemoryError$2;->val$errorInfo:Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V

    goto :goto_0

    .line 98
    :cond_0
    const-string/jumbo v1, "showDisplayMemoryErrorDialog cancel"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/android/server/am/ScoutMemoryError$2;->val$errorInfo:Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V

    .line 100
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->setDisableState(Z)V

    .line 102
    :goto_0
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->setShowDialogState(Z)V

    .line 103
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->updateLastReportTime()V

    .line 104
    invoke-static {}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->getInstance()Lcom/miui/server/stability/ScoutDisplayMemoryManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ScoutMemoryError$2;->val$errorInfo:Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;

    invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V

    .line 105
    return-void
.end method
