public class com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "ProcessPriorityScore" */
} // .end annotation
/* # static fields */
private static final Long APP_ACTIVE_THRESHOLD_HIGH;
private static final Long APP_ACTIVE_THRESHOLD_LOW;
private static final Long APP_ACTIVE_THRESHOLD_MODERATE;
private static final Integer MAX_APP_WEEK_LAUNCH_COUNT;
private static final Integer MINIMUM_MEMORY_GROWTH_THRESHOLD;
private static final Long MIN_APP_FOREGROUND_EVENT_DURATION;
private static final Integer PROCESS_ACTIVE_LEVEL_CRITICAL;
private static final Integer PROCESS_ACTIVE_LEVEL_HEAVY;
private static final Integer PROCESS_ACTIVE_LEVEL_LOW;
private static final Integer PROCESS_ACTIVE_LEVEL_MODERATE;
private static final Integer PROCESS_MEM_LEVEL_CRITICAL;
private static final Integer PROCESS_MEM_LEVEL_HIGH;
private static final Integer PROCESS_MEM_LEVEL_LOW;
private static final Integer PROCESS_MEM_LEVEL_MODERATE;
public static final Integer PROCESS_PRIORITY_ACTIVE_USAGE_FACTOR;
public static final Integer PROCESS_PRIORITY_LEVEL_UNKNOWN;
public static final Integer PROCESS_PRIORITY_MEM_FACTOR;
public static final Integer PROCESS_PRIORITY_TYPE_FACTOR;
private static final Integer PROCESS_TYPE_LEVEL_MAINPROC_HAS_ACTIVITY;
private static final Integer PROCESS_TYPE_LEVEL_NO_ACTIVITY;
private static final Integer PROCESS_TYPE_LEVEL_SUBPROC_HAS_ACTIVITY;
private static final Integer PROCESS_USAGE_ACTIVE_LEVEL_DEFAULT;
private static final Integer PROCESS_USAGE_LEVEL_AUTOSTART;
public static final Integer PROCESS_USAGE_LEVEL_CRITICAL;
public static final Integer PROCESS_USAGE_LEVEL_LOW;
public static final Integer PROCESS_USAGE_LEVEL_MODERATE;
/* # instance fields */
private final Long MEMORY_GROWTH_THRESHOLD;
private final Long TOTAL_MEMEORY_KB;
private Integer mPriorityScore;
private Integer mUsageLevel;
final com.android.server.am.AppStateManager$AppState$RunningProcess this$2; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmPriorityScore ( com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mPriorityScore:I */
} // .end method
static Integer -$$Nest$fgetmUsageLevel ( com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mUsageLevel:I */
} // .end method
static void -$$Nest$mupdatePriorityScore ( com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->updatePriorityScore()V */
return;
} // .end method
public com.android.server.am.AppStateManager$AppState$RunningProcess$ProcessPriorityScore ( ) {
/* .locals 4 */
/* .param p1, "this$2" # Lcom/android/server/am/AppStateManager$AppState$RunningProcess; */
/* .line 3015 */
this.this$2 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 3051 */
android.os.Process .getTotalMemory ( );
/* move-result-wide v0 */
/* const-wide/16 v2, 0x400 */
/* div-long/2addr v0, v2 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->TOTAL_MEMEORY_KB:J */
/* .line 3053 */
/* const-wide/16 v2, 0x64 */
/* div-long/2addr v0, v2 */
/* .line 3054 */
/* const-wide/32 v2, 0x19000 */
java.lang.Math .max ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->MEMORY_GROWTH_THRESHOLD:J */
/* .line 3053 */
return;
} // .end method
private Integer computeActiveAndUsageStatLevel ( ) {
/* .locals 2 */
/* .line 3105 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeUsageStatLevel()I */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mUsageLevel:I */
/* .line 3107 */
v0 = this.this$2;
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmIsLockedApp ( v0 );
/* if-nez v0, :cond_1 */
v0 = this.this$2;
v0 = this.this$1;
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v0 );
v1 = this.this$2;
v1 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmPid ( v1 );
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).isDependedProcess ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isDependedProcess(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 3113 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeAppActiveLevel()I */
/* .line 3115 */
/* .local v0, "activeLevel":I */
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mUsageLevel:I */
v1 = java.lang.Math .min ( v1,v0 );
/* .line 3109 */
} // .end local v0 # "activeLevel":I
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer computeAppActiveLevel ( ) {
/* .locals 9 */
/* .line 3127 */
v0 = this.this$2;
v0 = this.this$1;
v0 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmIsLockedApp ( v0 );
/* if-nez v0, :cond_5 */
v0 = this.this$2;
v0 = this.this$1;
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v0 );
v1 = this.this$2;
v1 = com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmPid ( v1 );
v0 = (( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).isDependedProcess ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->isDependedProcess(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 3132 */
} // :cond_0
v0 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmProcessRecord ( v0 );
v0 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v0 ).getLastTopTime ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J
/* move-result-wide v0 */
/* .line 3133 */
/* .local v0, "lastTopTime":J */
v2 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmProcessRecord ( v2 );
v2 = this.mState;
/* .line 3134 */
(( com.android.server.am.ProcessStateRecord ) v2 ).getLastSwitchToTopTime ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getLastSwitchToTopTime()J
/* move-result-wide v2 */
/* sub-long v2, v0, v2 */
/* .line 3135 */
/* .local v2, "appTopDuration":J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* sub-long/2addr v4, v0 */
/* .line 3137 */
/* .local v4, "backgroundTime":J */
/* const/16 v6, 0x9 */
/* .line 3138 */
/* .local v6, "activeLevel":I */
/* const-wide/16 v7, 0x1388 */
/* cmp-long v7, v2, v7 */
/* if-lez v7, :cond_3 */
/* .line 3139 */
/* const-wide/32 v7, 0x927c0 */
/* cmp-long v7, v4, v7 */
/* if-gtz v7, :cond_1 */
/* .line 3141 */
int v6 = 1; // const/4 v6, 0x1
/* .line 3142 */
} // :cond_1
/* const-wide/32 v7, 0x1b7740 */
/* cmp-long v7, v4, v7 */
/* if-gtz v7, :cond_2 */
/* .line 3144 */
int v6 = 2; // const/4 v6, 0x2
/* .line 3145 */
} // :cond_2
/* const-wide/32 v7, 0x36ee80 */
/* cmp-long v7, v4, v7 */
/* if-gtz v7, :cond_3 */
/* .line 3147 */
int v6 = 3; // const/4 v6, 0x3
/* .line 3151 */
} // :cond_3
} // :goto_0
/* const/16 v7, 0x9 */
/* if-ne v6, v7, :cond_4 */
v7 = this.this$2;
v7 = this.this$1;
v7 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmIsAutoStartApp ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 3152 */
int v6 = 4; // const/4 v6, 0x4
/* .line 3155 */
} // :cond_4
/* .line 3129 */
} // .end local v0 # "lastTopTime":J
} // .end local v2 # "appTopDuration":J
} // .end local v4 # "backgroundTime":J
} // .end local v6 # "activeLevel":I
} // :cond_5
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Integer computeMemUsageLevel ( ) {
/* .locals 9 */
/* .line 3210 */
v0 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmLastPss ( v0 );
/* move-result-wide v0 */
/* sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL3_PSS_LIMIT_KB:J */
/* cmp-long v0, v0, v2 */
/* if-ltz v0, :cond_0 */
/* .line 3211 */
int v0 = 4; // const/4 v0, 0x4
/* .local v0, "memLevel":I */
/* .line 3212 */
} // .end local v0 # "memLevel":I
} // :cond_0
v0 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmLastPss ( v0 );
/* move-result-wide v0 */
/* sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL2_PSS_LIMIT_KB:J */
/* cmp-long v0, v0, v2 */
/* if-ltz v0, :cond_1 */
/* .line 3213 */
int v0 = 3; // const/4 v0, 0x3
/* .restart local v0 # "memLevel":I */
/* .line 3214 */
} // .end local v0 # "memLevel":I
} // :cond_1
v0 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmLastPss ( v0 );
/* move-result-wide v0 */
/* sget-wide v2, Lcom/miui/app/smartpower/SmartPowerSettings;->PROC_MEM_LVL1_PSS_LIMIT_KB:J */
/* cmp-long v0, v0, v2 */
/* if-ltz v0, :cond_2 */
/* .line 3215 */
int v0 = 2; // const/4 v0, 0x2
/* .restart local v0 # "memLevel":I */
/* .line 3217 */
} // .end local v0 # "memLevel":I
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* .line 3224 */
/* .restart local v0 # "memLevel":I */
} // :goto_0
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmProcessRecord ( v1 );
v1 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v1 ).getInitialIdlePss ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProfileRecord;->getInitialIdlePss()J
/* move-result-wide v1 */
/* .line 3225 */
/* .local v1, "initialIdlePss":J */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v0, v3, :cond_3 */
/* const-wide/16 v3, 0x0 */
/* cmp-long v3, v1, v3 */
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmLastPss ( v3 );
/* move-result-wide v3 */
/* const-wide/16 v5, 0x3 */
/* mul-long/2addr v5, v1 */
/* const-wide/16 v7, 0x2 */
/* div-long/2addr v5, v7 */
/* cmp-long v3, v3, v5 */
/* if-lez v3, :cond_3 */
v3 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmLastPss ( v3 );
/* move-result-wide v3 */
/* iget-wide v5, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->MEMORY_GROWTH_THRESHOLD:J */
/* add-long/2addr v5, v1 */
/* cmp-long v3, v3, v5 */
/* if-lez v3, :cond_3 */
/* .line 3229 */
int v0 = 3; // const/4 v0, 0x3
/* .line 3231 */
} // :cond_3
} // .end method
private Integer computeTypeLevel ( ) {
/* .locals 2 */
/* .line 3195 */
v0 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmProcessRecord ( v0 );
v0 = (( com.android.server.am.ProcessRecord ) v0 ).hasActivities ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z
/* if-nez v0, :cond_0 */
/* .line 3196 */
int v0 = 3; // const/4 v0, 0x3
/* .local v0, "typeLevel":I */
/* .line 3197 */
} // .end local v0 # "typeLevel":I
} // :cond_0
v0 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmProcessName ( v0 );
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmPackageName ( v1 );
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 3199 */
int v0 = 2; // const/4 v0, 0x2
/* .restart local v0 # "typeLevel":I */
/* .line 3202 */
} // .end local v0 # "typeLevel":I
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 3204 */
/* .restart local v0 # "typeLevel":I */
} // :goto_0
} // .end method
private Integer computeUsageStatLevel ( ) {
/* .locals 9 */
/* .line 3159 */
v0 = this.this$2;
v0 = this.this$1;
v0 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmSmartPowerPolicyManager ( v0 );
v1 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmPackageName ( v1 );
/* .line 3160 */
(( com.miui.server.smartpower.SmartPowerPolicyManager ) v0 ).getUsageStatsInfo ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/smartpower/SmartPowerPolicyManager;->getUsageStatsInfo(Ljava/lang/String;)Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;
/* .line 3161 */
/* .local v0, "usageStats":Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo; */
/* const/16 v1, 0x9 */
/* .line 3162 */
/* .local v1, "usageLevel":I */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 3163 */
v2 = this.this$2;
com.android.server.am.AppStateManager$AppState$RunningProcess .-$$Nest$fgetmProcessRecord ( v2 );
v2 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v2 ).getLastTopTime ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getLastTopTime()J
/* move-result-wide v2 */
/* .line 3164 */
(( com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ) v0 ).getLastTimeVisible ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->getLastTimeVisible()J
/* move-result-wide v4 */
/* .line 3163 */
java.lang.Math .max ( v2,v3,v4,v5 );
/* move-result-wide v2 */
/* .line 3166 */
/* .local v2, "lastTimeVisible":J */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* sub-long/2addr v4, v2 */
/* const-wide/32 v6, 0x5265c00 */
/* cmp-long v4, v4, v6 */
int v5 = 1; // const/4 v5, 0x1
int v6 = 0; // const/4 v6, 0x0
/* if-gez v4, :cond_0 */
/* move v4, v5 */
} // :cond_0
/* move v4, v6 */
/* .line 3167 */
/* .local v4, "recentlyLunch":Z */
} // :goto_0
/* nop */
/* .line 3168 */
v7 = (( com.miui.server.smartpower.SmartPowerPolicyManager$UsageStatsInfo ) v0 ).getAppLaunchCount ( ); // invoke-virtual {v0}, Lcom/miui/server/smartpower/SmartPowerPolicyManager$UsageStatsInfo;->getAppLaunchCount()I
/* const/16 v8, 0x15 */
/* if-le v7, v8, :cond_1 */
} // :cond_1
/* move v5, v6 */
/* .line 3172 */
/* .local v5, "recentlyUsage":Z */
} // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 3173 */
int v1 = 5; // const/4 v1, 0x5
/* .line 3174 */
} // :cond_2
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 3176 */
int v1 = 6; // const/4 v1, 0x6
/* .line 3177 */
} // :cond_3
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 3179 */
int v1 = 7; // const/4 v1, 0x7
/* .line 3182 */
} // .end local v2 # "lastTimeVisible":J
} // .end local v4 # "recentlyLunch":Z
} // .end local v5 # "recentlyUsage":Z
} // :cond_4
} // :goto_2
} // .end method
private void updatePriorityScore ( ) {
/* .locals 5 */
/* .line 3094 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeTypeLevel()I */
/* .line 3095 */
/* .local v0, "typeLevel":I */
v1 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeMemUsageLevel()I */
/* .line 3096 */
/* .local v1, "memLevel":I */
v2 = /* invoke-direct {p0}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->computeActiveAndUsageStatLevel()I */
/* .line 3098 */
/* .local v2, "activeUsageLevel":I */
/* mul-int/lit8 v3, v1, 0x1 */
/* mul-int/lit8 v4, v0, 0xa */
/* add-int/2addr v3, v4 */
/* mul-int/lit8 v4, v2, 0x64 */
/* add-int/2addr v3, v4 */
/* iput v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$ProcessPriorityScore;->mPriorityScore:I */
/* .line 3101 */
return;
} // .end method
