.class Lcom/android/server/am/ProcessRecordImpl;
.super Ljava/lang/Object;
.source "ProcessRecordImpl.java"

# interfaces
.implements Lcom/android/server/am/ProcessRecordStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcessRecordImpl$AppPss;
    }
.end annotation


# static fields
.field private static final MEM_THRESHOLD_IN_WHITE_LIST:J = 0x11800L

.field private static final TAG:Ljava/lang/String; = "ProcessRecordInjector"

.field private static final sAppPssUserMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ProcessRecordImpl$AppPss;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sLock:Ljava/lang/Object;

.field private static volatile sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;


# instance fields
.field private final APP_LOG_DIR:Ljava/lang/String;

.field private final LOG_DIR:Ljava/lang/String;

.field private final SYSTEM_LOG_DIR:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessRecordImpl;->sLock:Ljava/lang/Object;

    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/am/ProcessRecordImpl;->sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 75
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/server/am/ProcessRecordImpl;->sAppPssUserMap:Landroid/util/SparseArray;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-string v0, "/data/miuilog/stability/scout"

    iput-object v0, p0, Lcom/android/server/am/ProcessRecordImpl;->LOG_DIR:Ljava/lang/String;

    .line 78
    const-string v0, "/data/miuilog/stability/scout/app"

    iput-object v0, p0, Lcom/android/server/am/ProcessRecordImpl;->APP_LOG_DIR:Ljava/lang/String;

    .line 79
    const-string v0, "/data/miuilog/stability/scout/sys"

    iput-object v0, p0, Lcom/android/server/am/ProcessRecordImpl;->SYSTEM_LOG_DIR:Ljava/lang/String;

    return-void
.end method

.method public static addAppPssIfNeeded(Lcom/miui/server/process/ProcessManagerInternal;Lcom/android/server/am/ProcessRecord;)V
    .locals 13
    .param p0, "pms"    # Lcom/miui/server/process/ProcessManagerInternal;
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 100
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 101
    .local v0, "pkn":Ljava/lang/String;
    const-string v1, "android"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    return-void

    .line 104
    :cond_0
    iget v1, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-static {v0, v1}, Lcom/android/server/am/ProcessUtils;->getPackageLastPss(Ljava/lang/String;I)J

    move-result-wide v7

    .line 105
    .local v7, "pss":J
    sget-object v9, Lcom/android/server/am/ProcessRecordImpl;->sLock:Ljava/lang/Object;

    monitor-enter v9

    .line 106
    :try_start_0
    sget-object v1, Lcom/android/server/am/ProcessRecordImpl;->sAppPssUserMap:Landroid/util/SparseArray;

    iget v2, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    move-object v10, v1

    .line 107
    .local v10, "appPssMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;"
    const-wide/32 v1, 0x11800

    cmp-long v1, v7, v1

    if-ltz v1, :cond_4

    if-eqz v10, :cond_1

    .line 108
    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_4

    .line 109
    :cond_1
    const/4 v1, 0x0

    .line 111
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_1
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    iget v3, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    const-wide/16 v4, 0x0

    invoke-interface {v2, v0, v4, v5, v3}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;JI)Landroid/content/pm/PackageInfo;

    move-result-object v2
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v2

    .line 114
    move-object v11, v1

    goto :goto_0

    .line 112
    :catch_0
    move-exception v2

    .line 113
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v11, v1

    .line 115
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    .end local v2    # "e":Landroid/os/RemoteException;
    .local v11, "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    if-eqz v11, :cond_2

    iget-object v1, v11, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v11, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "unknown"

    :goto_1
    move-object v5, v1

    .line 116
    .local v5, "version":Ljava/lang/String;
    new-instance v12, Lcom/android/server/am/ProcessRecordImpl$AppPss;

    iget v6, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    move-object v1, v12

    move-object v2, v0

    move-wide v3, v7

    invoke-direct/range {v1 .. v6}, Lcom/android/server/am/ProcessRecordImpl$AppPss;-><init>(Ljava/lang/String;JLjava/lang/String;I)V

    move-object v1, v12

    .line 117
    .local v1, "appPss":Lcom/android/server/am/ProcessRecordImpl$AppPss;
    if-nez v10, :cond_3

    .line 118
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move-object v10, v2

    .line 119
    sget-object v2, Lcom/android/server/am/ProcessRecordImpl;->sAppPssUserMap:Landroid/util/SparseArray;

    iget v3, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-virtual {v2, v3, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 121
    :cond_3
    invoke-interface {v10, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    .end local v1    # "appPss":Lcom/android/server/am/ProcessRecordImpl$AppPss;
    .end local v5    # "version":Ljava/lang/String;
    .end local v10    # "appPssMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;"
    .end local v11    # "pi":Landroid/content/pm/PackageInfo;
    :cond_4
    monitor-exit v9

    .line 124
    return-void

    .line 123
    :catchall_0
    move-exception v1

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private createLastAnrFile(Lmiui/mqsas/sdk/event/AnrEvent;Ljava/io/File;)Ljava/io/File;
    .locals 5
    .param p1, "event"    # Lmiui/mqsas/sdk/event/AnrEvent;
    .param p2, "logFile"    # Ljava/io/File;

    .line 253
    const-string v0, "-"

    if-nez p2, :cond_0

    .line 254
    const/4 v0, 0x0

    return-object v0

    .line 256
    :cond_0
    const/4 v1, 0x0

    .line 258
    .local v1, "lastAnrStateFile":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/AnrEvent;->getProcessName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 259
    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/AnrEvent;->getPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-lastAnrState.txt"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "lastAnrFileName":Ljava/lang/String;
    const-string/jumbo v2, "system_server"

    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/AnrEvent;->getProcessName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 261
    new-instance v2, Ljava/io/File;

    const-string v3, "/data/miuilog/stability/scout/sys"

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_0

    .line 263
    :cond_1
    new-instance v2, Ljava/io/File;

    const-string v3, "/data/miuilog/stability/scout/app"

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 266
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 267
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1fd

    const/4 v4, -0x1

    invoke-static {v2, v3, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    .end local v0    # "lastAnrFileName":Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception creating lastAnrState dump file"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessRecordInjector"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    return-object v1
.end method

.method private static getProcessManagerInternal()Lcom/miui/server/process/ProcessManagerInternal;
    .locals 2

    .line 82
    sget-object v0, Lcom/android/server/am/ProcessRecordImpl;->sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_1

    .line 83
    const-class v0, Lcom/android/server/am/ProcessRecordImpl;

    monitor-enter v0

    .line 84
    :try_start_0
    sget-object v1, Lcom/android/server/am/ProcessRecordImpl;->sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v1, :cond_0

    .line 85
    const-class v1, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/process/ProcessManagerInternal;

    sput-object v1, Lcom/android/server/am/ProcessRecordImpl;->sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 87
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 89
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/am/ProcessRecordImpl;->sProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    return-object v0
.end method

.method private reportANR(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;Ljava/lang/String;Lcom/android/server/am/ScoutAnrInfo;)V
    .locals 14
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "eventType"    # Ljava/lang/String;
    .param p3, "process"    # Lcom/android/server/am/ProcessRecord;
    .param p4, "processName"    # Ljava/lang/String;
    .param p5, "activityShortComponentName"    # Ljava/lang/String;
    .param p6, "parentShortCompontnName"    # Ljava/lang/String;
    .param p7, "subject"    # Ljava/lang/String;
    .param p8, "report"    # Ljava/lang/String;
    .param p9, "logFile"    # Ljava/io/File;
    .param p10, "crashInfo"    # Landroid/app/ApplicationErrorReport$CrashInfo;
    .param p11, "headline"    # Ljava/lang/String;
    .param p12, "anrInfo"    # Lcom/android/server/am/ScoutAnrInfo;

    .line 215
    move-object v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    move-object/from16 v4, p9

    new-instance v5, Lmiui/mqsas/sdk/event/AnrEvent;

    invoke-direct {v5}, Lmiui/mqsas/sdk/event/AnrEvent;-><init>()V

    .line 216
    .local v5, "event":Lmiui/mqsas/sdk/event/AnrEvent;
    invoke-virtual/range {p12 .. p12}, Lcom/android/server/am/ScoutAnrInfo;->getpid()I

    move-result v6

    invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setPid(I)V

    .line 217
    invoke-virtual/range {p12 .. p12}, Lcom/android/server/am/ScoutAnrInfo;->getuid()I

    move-result v6

    invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setUid(I)V

    .line 218
    iget v6, v1, Lcom/android/server/am/ProcessRecord;->mPid:I

    sget v7, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    if-ne v6, v7, :cond_0

    const-string/jumbo v6, "system_server"

    goto :goto_0

    :cond_0
    move-object/from16 v6, p4

    :goto_0
    invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setProcessName(Ljava/lang/String;)V

    .line 219
    const-string/jumbo v6, "system"

    move-object/from16 v7, p4

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget v6, v1, Lcom/android/server/am/ProcessRecord;->mPid:I

    sget v8, Lcom/android/server/am/ActivityManagerService;->MY_PID:I

    if-eq v6, v8, :cond_1

    .line 220
    iget-object v6, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_1

    :cond_1
    invoke-virtual {v5}, Lmiui/mqsas/sdk/event/AnrEvent;->getProcessName()Ljava/lang/String;

    move-result-object v6

    .line 219
    :goto_1
    invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setPackageName(Ljava/lang/String;)V

    .line 221
    invoke-virtual/range {p12 .. p12}, Lcom/android/server/am/ScoutAnrInfo;->getTimeStamp()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setTimeStamp(J)V

    .line 222
    move-object/from16 v6, p8

    invoke-virtual {v5, v6}, Lmiui/mqsas/sdk/event/AnrEvent;->setReason(Ljava/lang/String;)V

    .line 223
    move-object/from16 v8, p7

    invoke-virtual {v5, v8}, Lmiui/mqsas/sdk/event/AnrEvent;->setCpuInfo(Ljava/lang/String;)V

    .line 224
    invoke-virtual/range {p12 .. p12}, Lcom/android/server/am/ScoutAnrInfo;->getBgAnr()Z

    move-result v9

    invoke-virtual {v5, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setBgAnr(Z)V

    .line 225
    invoke-virtual/range {p12 .. p12}, Lcom/android/server/am/ScoutAnrInfo;->getBlockSystemState()Z

    move-result v9

    invoke-virtual {v5, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setBlockSystemState(Z)V

    .line 226
    invoke-virtual/range {p12 .. p12}, Lcom/android/server/am/ScoutAnrInfo;->getBinderTransInfo()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setBinderTransactionInfo(Ljava/lang/String;)V

    .line 227
    if-eqz v4, :cond_2

    invoke-virtual/range {p9 .. p9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 228
    invoke-virtual/range {p9 .. p9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lmiui/mqsas/sdk/event/AnrEvent;->setLogName(Ljava/lang/String;)V

    .line 231
    :cond_2
    invoke-direct {p0, v5, v4}, Lcom/android/server/am/ProcessRecordImpl;->createLastAnrFile(Lmiui/mqsas/sdk/event/AnrEvent;Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    .line 233
    .local v9, "lastAnrStateFile":Ljava/io/File;
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 234
    move-object v10, p1

    invoke-direct {p0, p1, v9}, Lcom/android/server/am/ProcessRecordImpl;->saveLastAnrState(Lcom/android/server/am/ActivityManagerService;Ljava/io/File;)V

    .line 235
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 236
    .local v11, "filesNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, " add extra file: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "ProcessRecordInjector"

    invoke-static {v13, v12}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-virtual {v5, v11}, Lmiui/mqsas/sdk/event/AnrEvent;->setExtraFiles(Ljava/util/List;)V

    goto :goto_2

    .line 233
    .end local v11    # "filesNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    move-object v10, p1

    .line 241
    :goto_2
    if-eqz v2, :cond_4

    .line 242
    invoke-virtual {v5, v2}, Lmiui/mqsas/sdk/event/AnrEvent;->setTargetActivity(Ljava/lang/String;)V

    .line 244
    :cond_4
    if-eqz v3, :cond_5

    .line 245
    invoke-virtual {v5, v3}, Lmiui/mqsas/sdk/event/AnrEvent;->setParent(Ljava/lang/String;)V

    .line 247
    :cond_5
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v11

    invoke-virtual {v11, v5}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportAnrEvent(Lmiui/mqsas/sdk/event/AnrEvent;)V

    .line 248
    return-void
.end method

.method private saveLastAnrState(Lcom/android/server/am/ActivityManagerService;Ljava/io/File;)V
    .locals 8
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "file"    # Ljava/io/File;

    .line 276
    const-string v0, "lastanr"

    const-string v1, "\n"

    if-eqz p1, :cond_1

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_2

    .line 279
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v3, 0x1

    invoke-direct {v2, p2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    .local v2, "fout":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v4, Lcom/android/internal/util/FastPrintWriter;

    invoke-direct {v4, v2}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 281
    .local v4, "pw":Lcom/android/internal/util/FastPrintWriter;
    invoke-virtual {v4, v1}, Lcom/android/internal/util/FastPrintWriter;->println(Ljava/lang/String;)V

    .line 282
    new-array v5, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v7, 0x0

    invoke-virtual {p1, v7, v4, v5}, Lcom/android/server/am/ActivityManagerService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 283
    invoke-virtual {v4, v1}, Lcom/android/internal/util/FastPrintWriter;->println(Ljava/lang/String;)V

    .line 284
    iget-object v1, p1, Lcom/android/server/am/ActivityManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v6

    invoke-virtual {v1, v7, v4, v3}, Lcom/android/server/wm/WindowManagerService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 285
    invoke-virtual {v4}, Lcom/android/internal/util/FastPrintWriter;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 286
    .end local v4    # "pw":Lcom/android/internal/util/FastPrintWriter;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 288
    .end local v2    # "fout":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 279
    .restart local v2    # "fout":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v1

    :try_start_4
    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/android/server/am/ProcessRecordImpl;
    .end local p1    # "ams":Lcom/android/server/am/ActivityManagerService;
    .end local p2    # "file":Ljava/io/File;
    :goto_0
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 286
    .end local v2    # "fout":Ljava/io/FileOutputStream;
    .restart local p0    # "this":Lcom/android/server/am/ProcessRecordImpl;
    .restart local p1    # "ams":Lcom/android/server/am/ActivityManagerService;
    .restart local p2    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 287
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ProcessRecordInjector"

    const-string v2, "saveLastAnrState error "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 289
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 277
    :cond_1
    :goto_2
    return-void
.end method


# virtual methods
.method public dumpPeriodHistoryMessage(Lcom/android/server/am/ProcessRecord;JIZ)V
    .locals 3
    .param p1, "process"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "anrTime"    # J
    .param p4, "duration"    # I
    .param p5, "isSystemInputAnr"    # Z

    .line 378
    const-string v0, "ProcessRecordInjector"

    :try_start_0
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 379
    if-nez p5, :cond_0

    .line 380
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v1

    invoke-interface {v1, p2, p3, p4}, Landroid/app/IApplicationThread;->dumpPeriodHistoryMessage(JI)V

    goto :goto_0

    .line 382
    :cond_0
    invoke-static {p2, p3, p4}, Lcom/android/server/ScoutHelper;->dumpUithreadPeriodHistoryMessage(JI)V

    goto :goto_0

    .line 385
    :cond_1
    const-string v1, "Can\'t dumpPeriodHistoryMessage because of null IApplicationThread"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    :goto_0
    goto :goto_1

    .line 387
    :catch_0
    move-exception v1

    .line 388
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "Failed to dumpPeriodHistoryMessage after ANR"

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 390
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method public onANR(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;Ljava/lang/String;Lcom/android/server/am/ScoutAnrInfo;)V
    .locals 15
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "process"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "activityShortComponentName"    # Ljava/lang/String;
    .param p4, "parentShortCompontnName"    # Ljava/lang/String;
    .param p5, "subject"    # Ljava/lang/String;
    .param p6, "report"    # Ljava/lang/String;
    .param p7, "logFile"    # Ljava/io/File;
    .param p8, "crashInfo"    # Landroid/app/ApplicationErrorReport$CrashInfo;
    .param p9, "headline"    # Ljava/lang/String;
    .param p10, "anrInfo"    # Lcom/android/server/am/ScoutAnrInfo;

    .line 202
    move-object/from16 v13, p10

    if-eqz v13, :cond_0

    .line 203
    invoke-virtual/range {p10 .. p10}, Lcom/android/server/am/ScoutAnrInfo;->getBinderTransInfo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/android/server/am/ScoutAnrInfo;->setBinderTransInfo(Ljava/lang/String;)V

    .line 204
    const-string v2, "anr"

    move-object/from16 v14, p2

    iget-object v4, v14, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    invoke-direct/range {v0 .. v12}, Lcom/android/server/am/ProcessRecordImpl;->reportANR(Lcom/android/server/am/ActivityManagerService;Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;Ljava/lang/String;Lcom/android/server/am/ScoutAnrInfo;)V

    goto :goto_0

    .line 202
    :cond_0
    move-object/from16 v14, p2

    .line 207
    :goto_0
    return-void
.end method

.method public reportAppPss()V
    .locals 5

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 128
    .local v0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;"
    sget-object v1, Lcom/android/server/am/ProcessRecordImpl;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    sget-object v2, Lcom/android/server/am/ProcessRecordImpl;->sAppPssUserMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 130
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 131
    .local v2, "size":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 132
    sget-object v4, Lcom/android/server/am/ProcessRecordImpl;->sAppPssUserMap:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 133
    .local v4, "appPssMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;"
    if-eqz v4, :cond_0

    .line 134
    invoke-interface {v0, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 131
    .end local v4    # "appPssMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/android/server/am/ProcessRecordImpl$AppPss;>;"
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 137
    .end local v3    # "i":I
    :cond_1
    sget-object v3, Lcom/android/server/am/ProcessRecordImpl;->sAppPssUserMap:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    .line 139
    .end local v2    # "size":I
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 141
    return-void

    .line 143
    :cond_3
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/server/am/ProcessRecordImpl$1;

    invoke-direct {v2, p0, v0}, Lcom/android/server/am/ProcessRecordImpl$1;-><init>(Lcom/android/server/am/ProcessRecordImpl;Ljava/util/Map;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 171
    return-void

    .line 139
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public scoutAppAddBinderCallChainNativePids(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 308
    .local p2, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p3, "binderCallChainNativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 309
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 310
    .local v1, "nativePid":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 311
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    .end local v1    # "nativePid":I
    :cond_0
    goto :goto_0

    .line 315
    :cond_1
    return-void
.end method

.method public scoutAppCheckBinderCallChain(Ljava/lang/String;IILjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/server/am/ScoutAnrInfo;)V
    .locals 8
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "Pid"    # I
    .param p3, "systemPid"    # I
    .param p4, "annotation"    # Ljava/lang/String;
    .param p7, "anrInfo"    # Lcom/android/server/am/ScoutAnrInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/android/server/am/ScoutAnrInfo;",
            ")V"
        }
    .end annotation

    .line 321
    .local p5, "firstPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p6, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 322
    .local v0, "scoutJavaPids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object v1, v2

    .line 323
    .local v1, "scoutNativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v2, Lcom/android/server/ScoutHelper$ScoutBinderInfo;

    const/4 v3, 0x1

    const-string v4, "MIUIScout ANR"

    invoke-direct {v2, p2, p3, v3, v4}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;-><init>(IIILjava/lang/String;)V

    .line 325
    .local v2, "scoutBinderInfo":Lcom/android/server/ScoutHelper$ScoutBinderInfo;
    const/4 v5, 0x0

    .line 326
    .local v5, "isBlockSystem":Z
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    invoke-static {p2, v2, v0, v1}, Lcom/android/server/ScoutHelper;->checkBinderCallPidList(ILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v5

    .line 329
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "Broadcast"

    invoke-virtual {p4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 330
    invoke-static {p2, p3, v2, v0, v1}, Lcom/android/server/ScoutHelper;->checkAsyncBinderCallPidList(IILcom/android/server/ScoutHelper$ScoutBinderInfo;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v6

    if-nez v6, :cond_1

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :cond_1
    :goto_0
    move v5, v3

    .line 334
    :cond_2
    invoke-static {p2, v4}, Lcom/android/server/ScoutHelper;->printfProcBinderInfo(ILjava/lang/String;)V

    .line 335
    invoke-static {p3, v4}, Lcom/android/server/ScoutHelper;->printfProcBinderInfo(ILjava/lang/String;)V

    .line 337
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getBinderTransInfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 338
    invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getProcInfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 337
    invoke-virtual {p7, v3}, Lcom/android/server/am/ScoutAnrInfo;->setBinderTransInfo(Ljava/lang/String;)V

    .line 339
    invoke-virtual {v2}, Lcom/android/server/ScoutHelper$ScoutBinderInfo;->getDThreadState()Z

    move-result v3

    invoke-virtual {p7, v3}, Lcom/android/server/am/ScoutAnrInfo;->setDThreadState(Z)V

    .line 341
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const-string v4, "Dump Trace: add java proc "

    if-lez v3, :cond_4

    .line 342
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 343
    .local v6, "javaPid":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p5, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 344
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    .end local v6    # "javaPid":I
    :cond_3
    goto :goto_1

    .line 350
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 351
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 352
    .local v6, "nativePid":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 353
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    .end local v6    # "nativePid":I
    :cond_5
    goto :goto_2

    .line 358
    :cond_6
    invoke-virtual {p7, v5}, Lcom/android/server/am/ScoutAnrInfo;->setBlockSystemState(Z)V

    .line 359
    return-void
.end method

.method public scoutAppCheckDumpKernelTrace(Ljava/lang/String;Lcom/android/server/am/ScoutAnrInfo;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "anrInfo"    # Lcom/android/server/am/ScoutAnrInfo;

    .line 363
    sget-boolean v0, Lcom/android/server/ScoutHelper;->SYSRQ_ANR_D_THREAD:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/android/server/am/ScoutAnrInfo;->getDThreadState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    const/16 v0, 0x77

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->doSysRqInterface(C)V

    .line 365
    const/16 v0, 0x6c

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->doSysRqInterface(C)V

    .line 366
    sget-boolean v0, Lcom/android/server/ScoutHelper;->PANIC_ANR_D_THREAD:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/server/ScoutHelper;->isDebugpolicyed(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    const-wide/16 v0, 0xbb8

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    .line 369
    const-string v0, "Trigge Panic Crash when anr Process has D state thread"

    invoke-static {p1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    const/16 v0, 0x63

    invoke-static {v0}, Lcom/android/server/ScoutHelper;->doSysRqInterface(C)V

    .line 373
    :cond_0
    return-void
.end method

.method public scoutAppUpdateAnrInfo(Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ScoutAnrInfo;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "process"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "anrInfo"    # Lcom/android/server/am/ScoutAnrInfo;

    .line 300
    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v0

    invoke-virtual {p3, v0}, Lcom/android/server/am/ScoutAnrInfo;->setPid(I)V

    .line 301
    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getStartUid()I

    move-result v0

    invoke-virtual {p3, v0}, Lcom/android/server/am/ScoutAnrInfo;->setuid(I)V

    .line 302
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p3, v0, v1}, Lcom/android/server/am/ScoutAnrInfo;->setTimeStamp(J)V

    .line 303
    return-void
.end method

.method public skipAppErrorDialog(Lcom/android/server/am/ProcessRecord;)Z
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 292
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    if-eq v0, v1, :cond_2

    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_SUBSCREEN_DEVICE:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 294
    const-string v1, "com.xiaomi.misubscreenui"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 295
    :cond_0
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 292
    :goto_0
    return v0
.end method
