.class public Lcom/android/server/am/SchedBoostManagerInternalStubImpl;
.super Ljava/lang/Object;
.source "SchedBoostManagerInternalStubImpl.java"

# interfaces
.implements Lcom/android/server/am/SchedBoostManagerInternalStub;


# instance fields
.field private mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    if-nez v0, :cond_0

    .line 30
    const-class v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    iput-object v0, p0, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->mSchedBoostService:Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    return-object v0
.end method


# virtual methods
.method public boostHomeAnim(JI)V
    .locals 1
    .param p1, "duration"    # J
    .param p3, "mode"    # I

    .line 25
    invoke-direct {p0}, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->boostHomeAnim(JI)V

    .line 26
    return-void
.end method

.method public setRenderThreadTid(Lcom/android/server/wm/WindowProcessController;)V
    .locals 1
    .param p1, "wpc"    # Lcom/android/server/wm/WindowProcessController;

    .line 20
    invoke-direct {p0}, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->setRenderThreadTid(Lcom/android/server/wm/WindowProcessController;)V

    .line 21
    return-void
.end method

.method public setSchedMode(Ljava/lang/String;IIIJ)V
    .locals 6
    .param p1, "procName"    # Ljava/lang/String;
    .param p2, "pid"    # I
    .param p3, "rtid"    # I
    .param p4, "schedMode"    # I
    .param p5, "timeout"    # J

    .line 15
    invoke-direct {p0}, Lcom/android/server/am/SchedBoostManagerInternalStubImpl;->getSchedBoostService()Lcom/miui/server/rtboost/SchedBoostManagerInternal;

    move-result-object v0

    filled-new-array {p2, p3}, [I

    move-result-object v1

    move-wide v2, p5

    move-object v4, p1

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/miui/server/rtboost/SchedBoostManagerInternal;->beginSchedThreads([IJLjava/lang/String;I)V

    .line 16
    return-void
.end method
