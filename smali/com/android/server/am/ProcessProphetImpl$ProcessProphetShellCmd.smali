.class Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;
.super Landroid/os/ShellCommand;
.source "ProcessProphetImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProcessProphetShellCmd"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessProphetImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/am/ProcessProphetImpl;)V
    .locals 0

    .line 1179
    iput-object p1, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/ProcessProphetImpl;Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;-><init>(Lcom/android/server/am/ProcessProphetImpl;)V

    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 6
    .param p1, "cmd"    # Ljava/lang/String;

    .line 1183
    if-nez p1, :cond_0

    .line 1184
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 1186
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1188
    .local v0, "pw":Ljava/io/PrintWriter;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x4

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto/16 :goto_0

    :sswitch_0
    const-string/jumbo v2, "start"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto/16 :goto_1

    :sswitch_1
    const-string v2, "btoff"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x5

    goto/16 :goto_1

    :sswitch_2
    const-string v2, "idle"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_1

    :sswitch_3
    const-string v2, "dump"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_4
    const-string v2, "bton"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v5

    goto :goto_1

    :sswitch_5
    const-string/jumbo v2, "testLU"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x7

    goto :goto_1

    :sswitch_6
    const-string/jumbo v2, "testCP"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x9

    goto :goto_1

    :sswitch_7
    const-string/jumbo v2, "testBT"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x8

    goto :goto_1

    :sswitch_8
    const-string/jumbo v2, "testMTBF"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xa

    goto :goto_1

    :sswitch_9
    const-string v2, "pressure"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x6

    goto :goto_1

    :sswitch_a
    const-string v2, "force-start"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v4

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 1242
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_3

    .line 1238
    :pswitch_0
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->testMTBF()V

    .line 1239
    goto/16 :goto_2

    .line 1232
    :pswitch_1
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1233
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-static {v2}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$fgetmProcessProphetModel(Lcom/android/server/am/ProcessProphetImpl;)Lcom/android/server/am/ProcessProphetModel;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetModel;->testCP(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1226
    :pswitch_2
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1227
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->testBT(Ljava/lang/String;)V

    goto :goto_2

    .line 1220
    :pswitch_3
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->isEnable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1221
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->testLU(Ljava/lang/String;)V

    goto :goto_2

    .line 1216
    :pswitch_4
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v2, v5}, Lcom/android/server/am/ProcessProphetImpl;->reportMemPressure(I)V

    .line 1217
    goto :goto_2

    .line 1211
    :pswitch_5
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v2, v2, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1212
    .local v2, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v3, v3, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1213
    goto :goto_2

    .line 1206
    .end local v2    # "msg":Landroid/os/Message;
    :pswitch_6
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v2, v2, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const-string v3, "cmd"

    invoke-virtual {v2, v4, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 1207
    .restart local v2    # "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v3, v3, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    invoke-virtual {v3, v2}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1208
    goto :goto_2

    .line 1202
    .end local v2    # "msg":Landroid/os/Message;
    :pswitch_7
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {v2}, Lcom/android/server/am/ProcessProphetImpl;->notifyIdleUpdate()V

    .line 1203
    goto :goto_2

    .line 1198
    :pswitch_8
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mstartEmptyProc(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;)Z

    .line 1199
    goto :goto_2

    .line 1194
    :pswitch_9
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->peekNextArg()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/am/ProcessProphetImpl;->-$$Nest$mtryToStartEmptyProc(Lcom/android/server/am/ProcessProphetImpl;Ljava/lang/String;)Z

    .line 1195
    goto :goto_2

    .line 1190
    :pswitch_a
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v2, v2, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->this$0:Lcom/android/server/am/ProcessProphetImpl;

    iget-object v3, v3, Lcom/android/server/am/ProcessProphetImpl;->mHandler:Lcom/android/server/am/ProcessProphetImpl$MyHandler;

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1191
    nop

    .line 1247
    :cond_2
    :goto_2
    goto :goto_4

    .line 1242
    :goto_3
    return v1

    .line 1244
    :catch_0
    move-exception v2

    .line 1245
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error occurred. Check logcat for details. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1246
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error running shell command! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ShellCommand"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_4
    return v1

    :sswitch_data_0
    .sparse-switch
        -0x5c50b900 -> :sswitch_a
        -0x4c11e9bb -> :sswitch_9
        -0x445e1043 -> :sswitch_8
        -0x34488f9c -> :sswitch_7
        -0x34488f81 -> :sswitch_6
        -0x34488e65 -> :sswitch_5
        0x2e4db1 -> :sswitch_4
        0x2f39f4 -> :sswitch_3
        0x313fd4 -> :sswitch_2
        0x59b67dd -> :sswitch_1
        0x68ac462 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 2

    .line 1253
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetImpl$ProcessProphetShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1254
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Process Prophet commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1255
    return-void
.end method
