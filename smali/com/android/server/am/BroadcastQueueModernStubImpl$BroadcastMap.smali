.class Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;
.super Ljava/lang/Object;
.source "BroadcastQueueModernStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/BroadcastQueueModernStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BroadcastMap"
.end annotation


# instance fields
.field private action:Ljava/lang/String;

.field private packageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;->action:Ljava/lang/String;

    .line 146
    iput-object p2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;->packageName:Ljava/lang/String;

    .line 147
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .line 150
    instance-of v0, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;

    if-eqz v0, :cond_1

    .line 151
    move-object v0, p1

    check-cast v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;

    .line 153
    .local v0, "broadcastMapObj":Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;->action:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;->action:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;->packageName:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;->packageName:Ljava/lang/String;

    .line 154
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 153
    :goto_0
    return v1

    .line 156
    .end local v0    # "broadcastMapObj":Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;->action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", packageName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method
