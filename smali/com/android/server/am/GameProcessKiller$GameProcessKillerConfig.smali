.class public Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;
.super Ljava/lang/Object;
.source "GameProcessKiller.java"

# interfaces
.implements Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/GameProcessKiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameProcessKillerConfig"
.end annotation


# static fields
.field private static final CONFIG_PRIO:Ljava/lang/String; = "prio"

.field private static final DEFAULT_PRIO:I = 0x0

.field private static final MIN_PROC_STATE:Ljava/lang/String; = "min-proc-state"

.field private static final SKIP_ACTIVE:Ljava/lang/String; = "skip-active"

.field private static final SKIP_FOREGROUND:Ljava/lang/String; = "skip-foreground"

.field private static final WHITE_LIST:Ljava/lang/String; = "white-list"


# instance fields
.field mMinProcState:I

.field mPrio:I

.field mSkipActive:Z

.field mSkipForeground:Z

.field mWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    .line 165
    const/16 v0, 0x14

    iput v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mMinProcState:I

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipForeground:Z

    .line 167
    iput-boolean v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipActive:Z

    return-void
.end method


# virtual methods
.method public addWhiteList(Ljava/util/List;Z)V
    .locals 1
    .param p2, "append"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 171
    .local p1, "wl":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 172
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 175
    :goto_0
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 176
    return-void
.end method

.method public getPrio()I
    .locals 1

    .line 218
    iget v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I

    return v0
.end method

.method public initFromJSON(Lorg/json/JSONObject;)V
    .locals 6
    .param p1, "obj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 190
    const-string/jumbo v0, "white-list"

    const-string/jumbo v1, "skip-foreground"

    const-string/jumbo v2, "skip-active"

    const-string v3, "min-proc-state"

    const-string v4, "prio"

    :try_start_0
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 191
    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I

    .line 193
    :cond_0
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 194
    const/16 v4, 0x14

    invoke-virtual {p1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mMinProcState:I

    .line 196
    :cond_1
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    .line 197
    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipActive:Z

    .line 199
    :cond_2
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 200
    invoke-virtual {p1, v1, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipForeground:Z

    .line 202
    :cond_3
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 203
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 204
    .local v0, "tmpArray":Lorg/json/JSONArray;
    if-eqz v0, :cond_5

    .line 205
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 206
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 207
    iget-object v2, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 213
    .end local v0    # "tmpArray":Lorg/json/JSONArray;
    .end local v1    # "i":I
    :cond_5
    nop

    .line 214
    return-void

    .line 211
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Lorg/json/JSONException;
    throw v0
.end method

.method public removeWhiteList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 179
    .local p1, "wl":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 180
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 183
    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 224
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "prio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    iget v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 226
    const-string v1, ", minProcState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    iget v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mMinProcState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 228
    const-string v1, ", skipForeground="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    iget-boolean v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipForeground:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 230
    const-string v1, ", skipActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    iget-boolean v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipActive:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 232
    const-string v1, ", whiteList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    iget-object v1, p0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mWhiteList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 234
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
