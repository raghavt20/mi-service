class com.android.server.am.ProcessProphetCloud$1 extends android.database.ContentObserver {
	 /* .source "ProcessProphetCloud.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessProphetCloud;->registerProcProphetCloudObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessProphetCloud this$0; //synthetic
/* # direct methods */
 com.android.server.am.ProcessProphetCloud$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessProphetCloud; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 56 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 60 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 /* .line 61 */
	 final String v0 = "cloud_procprophet_enable"; // const-string v0, "cloud_procprophet_enable"
	 android.provider.Settings$System .getUriFor ( v0 );
	 /* .line 60 */
	 v0 = 	 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 62 */
		 v0 = this.this$0;
		 (( com.android.server.am.ProcessProphetCloud ) v0 ).updateEnableCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateEnableCloudControlParas()V
		 /* .line 63 */
		 return;
		 /* .line 66 */
	 } // :cond_0
	 if ( p2 != null) { // if-eqz p2, :cond_1
		 /* .line 67 */
		 final String v0 = "cloud_procprophet_interval_time"; // const-string v0, "cloud_procprophet_interval_time"
		 android.provider.Settings$System .getUriFor ( v0 );
		 /* .line 66 */
		 v0 = 		 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 68 */
			 v0 = this.this$0;
			 (( com.android.server.am.ProcessProphetCloud ) v0 ).updateTrackTimeCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateTrackTimeCloudControlParas()V
			 /* .line 69 */
			 return;
			 /* .line 72 */
		 } // :cond_1
		 if ( p2 != null) { // if-eqz p2, :cond_2
			 /* .line 73 */
			 final String v0 = "cloud_procprophet_startfreq_thresh"; // const-string v0, "cloud_procprophet_startfreq_thresh"
			 android.provider.Settings$System .getUriFor ( v0 );
			 /* .line 72 */
			 v0 = 			 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
			 if ( v0 != null) { // if-eqz v0, :cond_2
				 /* .line 74 */
				 v0 = this.this$0;
				 (( com.android.server.am.ProcessProphetCloud ) v0 ).updateStartProcFreqThresCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateStartProcFreqThresCloudControlParas()V
				 /* .line 75 */
				 return;
				 /* .line 78 */
			 } // :cond_2
			 if ( p2 != null) { // if-eqz p2, :cond_3
				 /* .line 79 */
				 final String v0 = "cloud_procprophet_mempres_thresh"; // const-string v0, "cloud_procprophet_mempres_thresh"
				 android.provider.Settings$System .getUriFor ( v0 );
				 /* .line 78 */
				 v0 = 				 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_3
					 /* .line 80 */
					 v0 = this.this$0;
					 (( com.android.server.am.ProcessProphetCloud ) v0 ).updateMemThresCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateMemThresCloudControlParas()V
					 /* .line 81 */
					 return;
					 /* .line 84 */
				 } // :cond_3
				 if ( p2 != null) { // if-eqz p2, :cond_4
					 /* .line 85 */
					 final String v0 = "cloud_procprophet_emptymem_thresh"; // const-string v0, "cloud_procprophet_emptymem_thresh"
					 android.provider.Settings$System .getUriFor ( v0 );
					 /* .line 84 */
					 v0 = 					 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
					 if ( v0 != null) { // if-eqz v0, :cond_4
						 /* .line 86 */
						 v0 = this.this$0;
						 (( com.android.server.am.ProcessProphetCloud ) v0 ).updateEmptyMemThresCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateEmptyMemThresCloudControlParas()V
						 /* .line 87 */
						 return;
						 /* .line 90 */
					 } // :cond_4
					 if ( p2 != null) { // if-eqz p2, :cond_5
						 /* .line 91 */
						 final String v0 = "cloud_procprophet_start_limitlist"; // const-string v0, "cloud_procprophet_start_limitlist"
						 android.provider.Settings$System .getUriFor ( v0 );
						 /* .line 90 */
						 v0 = 						 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
						 if ( v0 != null) { // if-eqz v0, :cond_5
							 /* .line 92 */
							 v0 = this.this$0;
							 (( com.android.server.am.ProcessProphetCloud ) v0 ).updateStartProcLimitCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateStartProcLimitCloudControlParas()V
							 /* .line 93 */
							 return;
							 /* .line 96 */
						 } // :cond_5
						 if ( p2 != null) { // if-eqz p2, :cond_6
							 /* .line 97 */
							 final String v0 = "cloud_procprophet_launch_blacklist"; // const-string v0, "cloud_procprophet_launch_blacklist"
							 android.provider.Settings$System .getUriFor ( v0 );
							 /* .line 96 */
							 v0 = 							 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
							 if ( v0 != null) { // if-eqz v0, :cond_6
								 /* .line 98 */
								 v0 = this.this$0;
								 (( com.android.server.am.ProcessProphetCloud ) v0 ).updateLaunchProcCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetCloud;->updateLaunchProcCloudControlParas()V
								 /* .line 99 */
								 return;
								 /* .line 101 */
							 } // :cond_6
							 return;
						 } // .end method
