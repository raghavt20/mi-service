class com.android.server.am.MiProcessTracker$ProcessRecordController {
	 /* .source "MiProcessTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MiProcessTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ProcessRecordController" */
} // .end annotation
/* # static fields */
private static final Long APP_MAX_SWITCH_BACKGROUND_TIME;
/* # instance fields */
private java.util.HashMap mLastTimeKillReason;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mRetentionFilterKillReasonList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.am.MiProcessTracker this$0; //synthetic
/* # direct methods */
static Boolean -$$Nest$misAppFirstStart ( com.android.server.am.MiProcessTracker$ProcessRecordController p0, com.miui.server.smartpower.IAppState p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->isAppFirstStart(Lcom/miui/server/smartpower/IAppState;)Z */
} // .end method
static void -$$Nest$mrecordKillProcessIfNeed ( com.android.server.am.MiProcessTracker$ProcessRecordController p0, java.lang.String p1, java.lang.String p2, Integer p3, java.lang.String p4, Integer p5, Long p6, Long p7 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p9}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->recordKillProcessIfNeed(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJ)V */
return;
} // .end method
private com.android.server.am.MiProcessTracker$ProcessRecordController ( ) {
/* .locals 8 */
/* .param p1, "this$0" # Lcom/android/server/am/MiProcessTracker; */
/* .line 277 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 275 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mLastTimeKillReason = v0;
/* .line 278 */
final String v1 = "OneKeyClean"; // const-string v1, "OneKeyClean"
final String v2 = "ForceClean"; // const-string v2, "ForceClean"
final String v3 = "GarbageClean"; // const-string v3, "GarbageClean"
final String v4 = "LockScreenClean"; // const-string v4, "LockScreenClean"
final String v5 = "GameClean"; // const-string v5, "GameClean"
final String v6 = "OptimizationClean"; // const-string v6, "OptimizationClean"
final String v7 = "SwipeUpClean"; // const-string v7, "SwipeUpClean"
/* invoke-static/range {v1 ..v7}, Ljava/util/List;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List; */
this.mRetentionFilterKillReasonList = v0;
/* .line 286 */
return;
} // .end method
 com.android.server.am.MiProcessTracker$ProcessRecordController ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;-><init>(Lcom/android/server/am/MiProcessTracker;)V */
return;
} // .end method
private Boolean isAppFirstStart ( com.miui.server.smartpower.IAppState p0 ) {
/* .locals 6 */
/* .param p1, "appState" # Lcom/miui/server/smartpower/IAppState; */
/* .line 335 */
v0 = this.mLastTimeKillReason;
(( com.miui.server.smartpower.IAppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
v0 = (( java.util.HashMap ) v0 ).containsKey ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 336 */
/* .line 338 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
(( com.miui.server.smartpower.IAppState ) p1 ).getLastTopTime ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->getLastTopTime()J
/* move-result-wide v4 */
/* sub-long/2addr v2, v4 */
/* .line 339 */
/* .local v2, "backgroundTime":J */
/* const-wide/32 v4, 0x5265c00 */
/* cmp-long v0, v2, v4 */
/* if-lez v0, :cond_1 */
/* .line 340 */
/* .line 342 */
} // :cond_1
v0 = this.mLastTimeKillReason;
(( com.miui.server.smartpower.IAppState ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/String; */
v0 = /* invoke-direct {p0, v0}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->needFilterReason(Ljava/lang/String;)Z */
} // .end method
private Boolean needFilterReason ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 346 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 347 */
v0 = this.mRetentionFilterKillReasonList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 348 */
/* .local v1, "filterReason":Ljava/lang/String; */
v2 = (( java.lang.String ) p1 ).contains ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 349 */
int v0 = 1; // const/4 v0, 0x1
/* .line 351 */
} // .end local v1 # "filterReason":Ljava/lang/String;
} // :cond_0
/* .line 353 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void recordKillProcessIfNeed ( java.lang.String p0, java.lang.String p1, Integer p2, java.lang.String p3, Integer p4, Long p5, Long p6 ) {
/* .locals 27 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "processName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .param p4, "reason" # Ljava/lang/String; */
/* .param p5, "adj" # I */
/* .param p6, "pss" # J */
/* .param p8, "rss" # J */
/* .line 295 */
/* move-object/from16 v15, p0 */
/* move-object/from16 v14, p1 */
/* move-object/from16 v13, p2 */
/* move/from16 v11, p3 */
/* move-object/from16 v12, p4 */
v0 = this.this$0;
com.android.server.am.MiProcessTracker .-$$Nest$fgetmSmartPowerService ( v0 );
/* .line 296 */
/* .line 297 */
/* .local v16, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
if ( v16 != null) { // if-eqz v16, :cond_0
v2 = /* invoke-virtual/range {v16 ..v16}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v2, v0 */
} // :cond_0
/* move v2, v1 */
} // :goto_0
/* move v9, v2 */
/* .line 298 */
/* .local v9, "hasActivities":Z */
if ( v16 != null) { // if-eqz v16, :cond_1
/* .line 299 */
v2 = /* invoke-virtual/range {v16 ..v16}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasForegrundService()Z */
if ( v2 != null) { // if-eqz v2, :cond_1
} // :cond_1
/* move v0, v1 */
} // :goto_1
/* move v10, v0 */
/* .line 300 */
/* .local v10, "hasForegroundServices":Z */
v0 = (( java.lang.String ) v13 ).equals ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
if ( v9 != null) { // if-eqz v9, :cond_2
} // :cond_2
/* move/from16 v25, v9 */
/* move/from16 v26, v10 */
/* move-object v2, v12 */
/* move-object v1, v14 */
/* goto/16 :goto_7 */
/* .line 301 */
} // :cond_3
} // :goto_2
v0 = this.this$0;
com.android.server.am.MiProcessTracker .-$$Nest$fgetmSmartPowerService ( v0 );
/* .line 302 */
/* .line 303 */
/* .local v17, "appState":Lcom/miui/server/smartpower/IAppState; */
/* if-nez v17, :cond_4 */
return;
/* .line 304 */
} // :cond_4
/* const-wide/16 v0, 0x0 */
/* .line 305 */
/* .local v0, "residentDuration":J */
/* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->getLastTopTime()J */
/* move-result-wide v18 */
/* .line 306 */
/* .local v18, "backgroundTime":J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v18, v2 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 308 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* sub-long v0, v2, v18 */
/* move-wide v7, v0 */
/* .line 306 */
} // :cond_5
/* move-wide v7, v0 */
/* .line 310 */
} // .end local v0 # "residentDuration":J
/* .local v7, "residentDuration":J */
} // :goto_3
/* const/16 v0, 0x1b */
/* new-array v5, v0, [J */
/* .line 311 */
/* .local v5, "memInfo":[J */
android.os.Debug .getMemInfo ( v5 );
/* .line 312 */
/* const/16 v0, 0x13 */
/* aget-wide v3, v5, v0 */
/* .line 313 */
/* .local v3, "memAvail":J */
/* sget-boolean v0, Lcom/android/server/am/MiProcessTracker;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 314 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reportKillProcessIfNeed packageName = "; // const-string v1, "reportKillProcessIfNeed packageName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " processName = "; // const-string v1, " processName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " reason = "; // const-string v1, " reason = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " adj:"; // const-string v1, " adj:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v6, p5 */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " pss:"; // const-string v1, " pss:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-wide/from16 v1, p6 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " rss:"; // const-string v1, " rss:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-wide/from16 v1, p8 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " memAvail:"; // const-string v1, " memAvail:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " residentDuration:"; // const-string v1, " residentDuration:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7, v8 ); // invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " AppSwitchFgCount:"; // const-string v1, " AppSwitchFgCount:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 319 */
v1 = /* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->getAppSwitchFgCount()I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " hasActivity:"; // const-string v1, " hasActivity:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " hasForegroundService:"; // const-string v1, " hasForegroundService:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 314 */
final String v1 = "MiProcessTracker"; // const-string v1, "MiProcessTracker"
android.util.Slog .i ( v1,v0 );
/* .line 313 */
} // :cond_6
/* move/from16 v6, p5 */
/* .line 323 */
} // :goto_4
v0 = /* invoke-direct {v15, v12}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->needFilterReason(Ljava/lang/String;)Z */
/* if-nez v0, :cond_9 */
/* .line 324 */
v0 = /* invoke-virtual/range {v17 ..v17}, Lcom/miui/server/smartpower/IAppState;->getAppSwitchFgCount()I */
/* if-gtz v0, :cond_8 */
/* if-nez v9, :cond_8 */
if ( v10 != null) { // if-eqz v10, :cond_7
} // :cond_7
/* move-wide/from16 v20, v3 */
/* move-object/from16 v22, v5 */
/* move-wide/from16 v23, v7 */
/* move/from16 v25, v9 */
/* move/from16 v26, v10 */
/* .line 326 */
} // :cond_8
} // :goto_5
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v2, p1 */
/* move-wide/from16 v20, v3 */
} // .end local v3 # "memAvail":J
/* .local v20, "memAvail":J */
/* move-object/from16 v3, p4 */
/* move/from16 v4, p5 */
/* move-object/from16 v22, v5 */
} // .end local v5 # "memInfo":[J
/* .local v22, "memInfo":[J */
/* move-wide/from16 v5, p6 */
/* move-wide/from16 v23, v7 */
} // .end local v7 # "residentDuration":J
/* .local v23, "residentDuration":J */
/* move-wide/from16 v7, p8 */
/* move/from16 v25, v9 */
/* move/from16 v26, v10 */
} // .end local v9 # "hasActivities":Z
} // .end local v10 # "hasForegroundServices":Z
/* .local v25, "hasActivities":Z */
/* .local v26, "hasForegroundServices":Z */
/* move-wide/from16 v9, v20 */
/* move-wide/from16 v11, v23 */
/* move/from16 v13, v25 */
/* move/from16 v14, v26 */
/* invoke-direct/range {v0 ..v14}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->reportKillProcessEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJZZ)V */
/* .line 323 */
} // .end local v20 # "memAvail":J
} // .end local v22 # "memInfo":[J
} // .end local v23 # "residentDuration":J
} // .end local v25 # "hasActivities":Z
} // .end local v26 # "hasForegroundServices":Z
/* .restart local v3 # "memAvail":J */
/* .restart local v5 # "memInfo":[J */
/* .restart local v7 # "residentDuration":J */
/* .restart local v9 # "hasActivities":Z */
/* .restart local v10 # "hasForegroundServices":Z */
} // :cond_9
/* move-wide/from16 v20, v3 */
/* move-object/from16 v22, v5 */
/* move-wide/from16 v23, v7 */
/* move/from16 v25, v9 */
/* move/from16 v26, v10 */
/* .line 330 */
} // .end local v3 # "memAvail":J
} // .end local v5 # "memInfo":[J
} // .end local v7 # "residentDuration":J
} // .end local v9 # "hasActivities":Z
} // .end local v10 # "hasForegroundServices":Z
/* .restart local v20 # "memAvail":J */
/* .restart local v22 # "memInfo":[J */
/* .restart local v23 # "residentDuration":J */
/* .restart local v25 # "hasActivities":Z */
/* .restart local v26 # "hasForegroundServices":Z */
} // :goto_6
v0 = this.mLastTimeKillReason;
/* move-object/from16 v1, p1 */
/* move-object/from16 v2, p4 */
(( java.util.HashMap ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 332 */
} // .end local v17 # "appState":Lcom/miui/server/smartpower/IAppState;
} // .end local v18 # "backgroundTime":J
} // .end local v20 # "memAvail":J
} // .end local v22 # "memInfo":[J
} // .end local v23 # "residentDuration":J
} // :goto_7
return;
} // .end method
private void reportKillProcessEvent ( java.lang.String p0, java.lang.String p1, java.lang.String p2, Integer p3, Long p4, Long p5, Long p6, Long p7, Boolean p8, Boolean p9 ) {
/* .locals 17 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "reason" # Ljava/lang/String; */
/* .param p4, "adj" # I */
/* .param p5, "pss" # J */
/* .param p7, "rss" # J */
/* .param p9, "memAvail" # J */
/* .param p11, "residentDuration" # J */
/* .param p13, "hasActivities" # Z */
/* .param p14, "hasForegroundServices" # Z */
/* .line 360 */
/* move-object/from16 v1, p0 */
v0 = this.this$0;
com.android.server.am.MiProcessTracker .-$$Nest$fgetmPerfShielder ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 362 */
try { // :try_start_0
v0 = this.this$0;
com.android.server.am.MiProcessTracker .-$$Nest$fgetmPerfShielder ( v0 );
/* move-object/from16 v3, p1 */
/* move-object/from16 v4, p2 */
/* move-object/from16 v5, p3 */
/* move/from16 v6, p4 */
/* move-wide/from16 v7, p5 */
/* move-wide/from16 v9, p7 */
/* move-wide/from16 v11, p9 */
/* move-wide/from16 v13, p11 */
/* move/from16 v15, p13 */
/* move/from16 v16, p14 */
/* invoke-interface/range {v2 ..v16}, Lcom/android/internal/app/IPerfShielder;->reportKillProcessEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJZZ)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 366 */
/* .line 364 */
/* :catch_0 */
/* move-exception v0 */
/* .line 365 */
/* .local v0, "e":Landroid/os/RemoteException; */
final String v2 = "MiProcessTracker"; // const-string v2, "MiProcessTracker"
final String v3 = "mPerfShielder is dead"; // const-string v3, "mPerfShielder is dead"
android.util.Slog .e ( v2,v3,v0 );
/* .line 368 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
return;
} // .end method
