.class Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;
.super Landroid/os/ShellCommand;
.source "MemoryStandardProcessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryStandardProcessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MemoryStandardProcessControlCmd"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryStandardProcessControl;


# direct methods
.method private constructor <init>(Lcom/android/server/am/MemoryStandardProcessControl;)V
    .locals 0

    .line 1137
    iput-object p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/MemoryStandardProcessControl;Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;)V

    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 13
    .param p1, "cmd"    # Ljava/lang/String;

    .line 1141
    if-nez p1, :cond_0

    .line 1142
    invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 1144
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1146
    .local v0, "pw":Ljava/io/PrintWriter;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x1

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string/jumbo v2, "standard"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_1

    :sswitch_1
    const-string v2, "debug"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    goto :goto_1

    :sswitch_2
    const-string v2, "dump"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_3
    const-string v2, "pressure"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v5

    goto :goto_1

    :sswitch_4
    const-string v2, "enable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v2, :cond_1

    move v2, v4

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    const-string v6, "false"

    const-string/jumbo v7, "true"

    packed-switch v2, :pswitch_data_0

    .line 1217
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_4

    .line 1204
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    .line 1205
    .local v2, "str":Ljava/lang/String;
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1206
    sput-boolean v5, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    .line 1207
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-static {v3}, Lcom/android/server/am/MemoryStandardProcessControl;->-$$Nest$mdebugModifyKillStrategy(Lcom/android/server/am/MemoryStandardProcessControl;)V

    .line 1208
    const-string v3, "debug on, enable mspc and screen off kill"

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1209
    :cond_2
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1210
    sput-boolean v1, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    .line 1211
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iput v1, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I

    .line 1212
    const-string v3, "debug off, disable mspc and screen off kill"

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1193
    .end local v2    # "str":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    .line 1194
    .restart local v2    # "str":Ljava/lang/String;
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1195
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iput-boolean v5, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    .line 1196
    const-string v3, "mspc enabled"

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1197
    :cond_3
    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1198
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iput-boolean v1, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z

    .line 1199
    const-string v3, "mspc disabled"

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1164
    .end local v2    # "str":Ljava/lang/String;
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    .line 1165
    .local v2, "mode":Ljava/lang/String;
    const-string v6, "list"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1167
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v3, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    iget-object v4, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v4, v4, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1168
    const-string v3, "please check logcat"

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1169
    :cond_4
    const-string v6, "add"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const-string v7, ","

    if-eqz v6, :cond_5

    .line 1171
    :try_start_2
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1172
    .local v4, "strs":[Ljava/lang/String;
    aget-object v9, v4, v1

    .line 1173
    .local v9, "packageName":Ljava/lang/String;
    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 1174
    .local v7, "pss":J
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1175
    .local v5, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    aget-object v3, v4, v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1176
    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v6, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    const/4 v11, 0x0

    move-object v10, v5

    invoke-virtual/range {v6 .. v11}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .end local v4    # "strs":[Ljava/lang/String;
    .end local v5    # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "pss":J
    .end local v9    # "packageName":Ljava/lang/String;
    goto :goto_2

    .line 1177
    :cond_5
    const-string v6, "add-native"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1179
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1180
    .local v6, "strs":[Ljava/lang/String;
    aget-object v10, v6, v1

    .line 1181
    .local v10, "packageName":Ljava/lang/String;
    aget-object v5, v6, v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1182
    .local v8, "pss":J
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1183
    .restart local v5    # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    aget-object v3, v6, v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184
    const/4 v3, 0x0

    .line 1185
    .local v3, "parent":Ljava/lang/String;
    array-length v7, v6

    if-le v7, v4, :cond_6

    .line 1186
    aget-object v4, v6, v4

    move-object v3, v4

    .line 1188
    :cond_6
    iget-object v4, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v7, v4, Lcom/android/server/am/MemoryStandardProcessControl;->mMemoryStandardMap:Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;

    move-object v11, v5

    move-object v12, v3

    invoke-virtual/range {v7 .. v12}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateNativeMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 1189
    .end local v3    # "parent":Ljava/lang/String;
    .end local v5    # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "strs":[Ljava/lang/String;
    .end local v8    # "pss":J
    .end local v10    # "packageName":Ljava/lang/String;
    goto :goto_3

    .line 1177
    :cond_7
    :goto_2
    goto :goto_3

    .line 1153
    .end local v2    # "mode":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1154
    .local v2, "str":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1156
    .local v3, "presssureState":I
    :try_start_3
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move v3, v4

    .line 1157
    iget-object v4, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-virtual {v4, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->reportMemPressure(I)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 1160
    goto :goto_3

    .line 1158
    :catch_0
    move-exception v4

    .line 1159
    .local v4, "e":Ljava/lang/NumberFormatException;
    :try_start_4
    const-string v5, "pressure is invalid"

    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1161
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    goto :goto_3

    .line 1148
    .end local v2    # "str":Ljava/lang/String;
    .end local v3    # "presssureState":I
    :pswitch_4
    iget-object v2, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v2, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    iget-object v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->this$0:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v3, v3, Lcom/android/server/am/MemoryStandardProcessControl;->mHandler:Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 1149
    nop

    .line 1222
    :cond_8
    :goto_3
    goto :goto_5

    .line 1217
    :goto_4
    return v1

    .line 1219
    :catch_1
    move-exception v2

    .line 1220
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error occurred. Check logcat for details. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1221
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error running shell command! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ShellCommand"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1223
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_5
    return v1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4d6ada7d -> :sswitch_4
        -0x4c11e9bb -> :sswitch_3
        0x2f39f4 -> :sswitch_2
        0x5b09653 -> :sswitch_1
        0x4e3d1ebd -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 2

    .line 1228
    invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1229
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Memory Standard Process Control commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1230
    return-void
.end method
