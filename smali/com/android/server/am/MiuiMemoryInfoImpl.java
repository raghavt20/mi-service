public class com.android.server.am.MiuiMemoryInfoImpl implements com.android.server.am.MiuiMemoryInfoStub {
	 /* .source "MiuiMemoryInfoImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 static com.android.server.am.ActivityManagerService sAms;
	 private static Long sTotalMem;
	 /* # direct methods */
	 static com.android.server.am.MiuiMemoryInfoImpl ( ) {
		 /* .locals 2 */
		 /* .line 16 */
		 android.os.Process .getTotalMemory ( );
		 /* move-result-wide v0 */
		 /* sput-wide v0, Lcom/android/server/am/MiuiMemoryInfoImpl;->sTotalMem:J */
		 /* .line 36 */
		 int v0 = 0; // const/4 v0, 0x0
		 return;
	 } // .end method
	 public com.android.server.am.MiuiMemoryInfoImpl ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static Long getCachePss ( ) {
		 /* .locals 8 */
		 /* .line 38 */
		 v0 = com.android.server.am.MiuiMemoryInfoImpl.sAms;
		 /* if-nez v0, :cond_0 */
		 /* .line 39 */
		 android.app.ActivityManagerNative .getDefault ( );
		 /* .line 40 */
		 /* .local v0, "ams":Landroid/app/IActivityManager; */
		 /* instance-of v1, v0, Lcom/android/server/am/ActivityManagerService; */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 41 */
			 /* move-object v1, v0 */
			 /* check-cast v1, Lcom/android/server/am/ActivityManagerService; */
			 /* .line 44 */
		 } // .end local v0 # "ams":Landroid/app/IActivityManager;
	 } // :cond_0
	 /* const-wide/16 v0, 0x0 */
	 /* .line 45 */
	 /* .local v0, "cachePss":J */
	 v2 = com.android.server.am.MiuiMemoryInfoImpl.sAms;
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 /* .line 46 */
		 int v3 = 0; // const/4 v3, 0x0
		 int v4 = 0; // const/4 v4, 0x0
		 (( com.android.server.am.ActivityManagerService ) v2 ).collectProcesses ( v3, v4, v4, v3 ); // invoke-virtual {v2, v3, v4, v4, v3}, Lcom/android/server/am/ActivityManagerService;->collectProcesses(Ljava/io/PrintWriter;IZ[Ljava/lang/String;)Ljava/util/ArrayList;
		 /* .line 47 */
		 /* .local v2, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
		 if ( v2 != null) { // if-eqz v2, :cond_2
			 /* .line 48 */
			 (( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
		 v4 = 		 } // :goto_0
		 if ( v4 != null) { // if-eqz v4, :cond_2
			 /* check-cast v4, Lcom/android/server/am/ProcessRecord; */
			 /* .line 49 */
			 /* .local v4, "proc":Lcom/android/server/am/ProcessRecord; */
			 v5 = com.android.server.am.MiuiMemoryInfoImpl.sAms;
			 v5 = this.mProcLock;
			 /* monitor-enter v5 */
			 /* .line 50 */
			 try { // :try_start_0
				 v6 = this.mState;
				 v6 = 				 (( com.android.server.am.ProcessStateRecord ) v6 ).getSetProcState ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I
				 /* const/16 v7, 0xf */
				 /* if-lt v6, v7, :cond_1 */
				 /* .line 51 */
				 v6 = this.mProfile;
				 (( com.android.server.am.ProcessProfileRecord ) v6 ).getLastPss ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
				 /* move-result-wide v6 */
				 /* add-long/2addr v0, v6 */
				 /* .line 53 */
			 } // :cond_1
			 /* monitor-exit v5 */
			 /* .line 54 */
		 } // .end local v4 # "proc":Lcom/android/server/am/ProcessRecord;
		 /* .line 53 */
		 /* .restart local v4 # "proc":Lcom/android/server/am/ProcessRecord; */
		 /* :catchall_0 */
		 /* move-exception v3 */
		 /* monitor-exit v5 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v3 */
		 /* .line 57 */
	 } // .end local v2 # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v4 # "proc":Lcom/android/server/am/ProcessRecord;
} // :cond_2
/* const-wide/16 v2, 0x400 */
/* mul-long/2addr v2, v0 */
/* return-wide v2 */
} // .end method
public static Long getCachedLostRam ( ) {
/* .locals 2 */
/* .line 62 */
com.android.server.am.MiuiMemoryInfoImpl .getNativeCachedLostMemory ( );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private static native Long getNativeCachedLostMemory ( ) {
} // .end method
/* # virtual methods */
public Long getFreeMemory ( ) {
/* .locals 14 */
/* .line 67 */
/* new-instance v0, Lcom/android/internal/util/MemInfoReader; */
/* invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V */
/* .line 68 */
/* .local v0, "minfo":Lcom/android/internal/util/MemInfoReader; */
(( com.android.internal.util.MemInfoReader ) v0 ).readMemInfo ( ); // invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V
/* .line 69 */
(( com.android.internal.util.MemInfoReader ) v0 ).getRawInfo ( ); // invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J
/* .line 70 */
/* .local v1, "rawInfo":[J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "MEMINFO_KRECLAIMABLE: "; // const-string v3, "MEMINFO_KRECLAIMABLE: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v3, 0xf */
/* aget-wide v4, v1, v3 */
(( java.lang.StringBuilder ) v2 ).append ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = ", MEMINFO_SLAB_RECLAIMABLE: "; // const-string v4, ", MEMINFO_SLAB_RECLAIMABLE: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v4 = 6; // const/4 v4, 0x6
/* aget-wide v5, v1, v4 */
(( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = ", MEMINFO_BUFFERS: "; // const-string v5, ", MEMINFO_BUFFERS: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v5 = 2; // const/4 v5, 0x2
/* aget-wide v6, v1, v5 */
(( java.lang.StringBuilder ) v2 ).append ( v6, v7 ); // invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v6 = ", MEMINFO_CACHED: "; // const-string v6, ", MEMINFO_CACHED: "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v6 = 3; // const/4 v6, 0x3
/* aget-wide v7, v1, v6 */
(( java.lang.StringBuilder ) v2 ).append ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = ", MEMINFO_FREE: "; // const-string v7, ", MEMINFO_FREE: "
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v7 = 1; // const/4 v7, 0x1
/* aget-wide v8, v1, v7 */
(( java.lang.StringBuilder ) v2 ).append ( v8, v9 ); // invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "MiuiMemoryInfoImpl"; // const-string v8, "MiuiMemoryInfoImpl"
android.util.Slog .d ( v8,v2 );
/* .line 75 */
/* aget-wide v2, v1, v3 */
/* .line 76 */
/* .local v2, "kReclaimable":J */
/* const-wide/16 v9, 0x0 */
/* cmp-long v9, v2, v9 */
/* if-nez v9, :cond_0 */
/* .line 77 */
/* aget-wide v2, v1, v4 */
/* .line 79 */
} // :cond_0
/* aget-wide v4, v1, v5 */
/* add-long/2addr v4, v2 */
/* aget-wide v9, v1, v6 */
/* add-long/2addr v4, v9 */
/* aget-wide v6, v1, v7 */
/* add-long/2addr v4, v6 */
/* const-wide/16 v6, 0x400 */
/* mul-long/2addr v4, v6 */
/* .line 81 */
/* .local v4, "cache":J */
com.android.server.am.MiuiMemoryInfoImpl .getCachedLostRam ( );
/* move-result-wide v6 */
/* .line 82 */
/* .local v6, "lostCache":J */
/* add-long v9, v4, v6 */
com.android.server.am.MiuiMemoryInfoImpl .getCachePss ( );
/* move-result-wide v11 */
/* add-long/2addr v9, v11 */
/* .line 83 */
/* .local v9, "free":J */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "cache: "; // const-string v12, "cache: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v4, v5 ); // invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v12 = ", cachePss: "; // const-string v12, ", cachePss: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.MiuiMemoryInfoImpl .getCachePss ( );
/* move-result-wide v12 */
(( java.lang.StringBuilder ) v11 ).append ( v12, v13 ); // invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v12 = ", lostcache: "; // const-string v12, ", lostcache: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v6, v7 ); // invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v11 );
/* .line 84 */
/* sget-wide v11, Lcom/android/server/am/MiuiMemoryInfoImpl;->sTotalMem:J */
/* cmp-long v8, v9, v11 */
/* if-ltz v8, :cond_1 */
/* .line 86 */
/* add-long v9, v4, v6 */
/* .line 88 */
} // :cond_1
/* return-wide v9 */
} // .end method
public Long getMoreCachedSizeKb ( com.android.internal.util.MemInfoReader p0 ) {
/* .locals 7 */
/* .param p1, "infos" # Lcom/android/internal/util/MemInfoReader; */
/* .line 24 */
(( com.android.internal.util.MemInfoReader ) p1 ).getRawInfo ( ); // invoke-virtual {p1}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J
/* .line 25 */
/* .local v0, "rawInfo":[J */
/* const/16 v1, 0xf */
/* aget-wide v1, v0, v1 */
/* .line 29 */
/* .local v1, "kReclaimable":J */
/* const-wide/16 v3, 0x0 */
/* cmp-long v3, v1, v3 */
/* if-nez v3, :cond_0 */
/* .line 30 */
int v3 = 6; // const/4 v3, 0x6
/* aget-wide v1, v0, v3 */
/* .line 32 */
} // :cond_0
int v3 = 2; // const/4 v3, 0x2
/* aget-wide v3, v0, v3 */
/* add-long/2addr v3, v1 */
int v5 = 3; // const/4 v5, 0x3
/* aget-wide v5, v0, v5 */
/* add-long/2addr v3, v5 */
/* return-wide v3 */
} // .end method
