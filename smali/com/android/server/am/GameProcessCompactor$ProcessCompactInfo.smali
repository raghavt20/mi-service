.class Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;
.super Ljava/lang/Object;
.source "GameProcessCompactor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/GameProcessCompactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ProcessCompactInfo"
.end annotation


# static fields
.field private static final COMPACT_ACTION_FULL:Ljava/lang/String; = "all"


# instance fields
.field private mDied:Z

.field private mLastCompactTime:J

.field private mLastIncomePct:I

.field private mPid:I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "pid"    # I

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput p1, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mDied:Z

    .line 121
    return-void
.end method

.method private shouldCompact(Lcom/android/server/am/GameProcessCompactor;)Z
    .locals 8
    .param p1, "compactor"    # Lcom/android/server/am/GameProcessCompactor;

    .line 124
    iget-boolean v0, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mDied:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 125
    return v1

    .line 126
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastCompactTime:J

    sub-long/2addr v2, v4

    invoke-virtual {p1}, Lcom/android/server/am/GameProcessCompactor;->getIntervalThreshold()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 127
    return v1

    .line 128
    :cond_1
    iget v0, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastIncomePct:I

    int-to-long v2, v0

    invoke-virtual {p1}, Lcom/android/server/am/GameProcessCompactor;->getIncomePctThreshold()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 129
    return v1

    .line 130
    :cond_2
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method compact(Lcom/android/server/am/GameProcessCompactor;)J
    .locals 9
    .param p1, "compactor"    # Lcom/android/server/am/GameProcessCompactor;

    .line 134
    const-wide/16 v0, -0x1

    .line 135
    .local v0, "income":J
    invoke-direct {p0, p1}, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->shouldCompact(Lcom/android/server/am/GameProcessCompactor;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    iget v2, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I

    invoke-static {v2}, Landroid/os/Process;->getRss(I)[J

    move-result-object v2

    .line 137
    .local v2, "rssBefore":[J
    const/4 v3, 0x0

    aget-wide v4, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    .line 138
    return-wide v0

    .line 139
    :cond_0
    invoke-static {}, Lcom/android/server/am/SystemPressureControllerStub;->getInstance()Lcom/android/server/am/SystemPressureControllerStub;

    move-result-object v4

    const-string v5, "all"

    iget v6, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I

    invoke-virtual {v4, v5, v6}, Lcom/android/server/am/SystemPressureControllerStub;->performCompaction(Ljava/lang/String;I)V

    .line 140
    iget v4, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I

    invoke-static {v4}, Landroid/os/Process;->getRss(I)[J

    move-result-object v4

    .line 141
    .local v4, "rssAfter":[J
    aget-wide v5, v2, v3

    aget-wide v7, v4, v3

    sub-long v0, v5, v7

    .line 142
    const-wide/16 v5, 0x64

    mul-long/2addr v5, v0

    aget-wide v7, v2, v3

    div-long/2addr v5, v7

    long-to-int v3, v5

    iput v3, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastIncomePct:I

    .line 143
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastCompactTime:J

    .line 145
    invoke-static {}, Lcom/android/server/am/GameProcessCompactor;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compact "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mPid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", reclaim mem: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", income pct:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mLastIncomePct:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    .end local v2    # "rssBefore":[J
    .end local v4    # "rssAfter":[J
    :cond_1
    return-wide v0
.end method

.method notifyDied()V
    .locals 1

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/am/GameProcessCompactor$ProcessCompactInfo;->mDied:Z

    .line 152
    return-void
.end method
