.class Lcom/android/server/am/ProcessRecordImpl$1;
.super Ljava/lang/Object;
.source "ProcessRecordImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessRecordImpl;->reportAppPss()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessRecordImpl;

.field final synthetic val$map:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessRecordImpl;Ljava/util/Map;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessRecordImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 143
    iput-object p1, p0, Lcom/android/server/am/ProcessRecordImpl$1;->this$0:Lcom/android/server/am/ProcessRecordImpl;

    iput-object p2, p0, Lcom/android/server/am/ProcessRecordImpl$1;->val$map:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v0, "jsons":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessRecordImpl$1;->val$map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 148
    .local v1, "pkns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 149
    .local v3, "pkn":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/am/ProcessRecordImpl$1;->val$map:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessRecordImpl$AppPss;

    .line 150
    .local v4, "appPss":Lcom/android/server/am/ProcessRecordImpl$AppPss;
    if-nez v4, :cond_0

    .line 151
    goto :goto_0

    .line 153
    :cond_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 155
    .local v5, "object":Lorg/json/JSONObject;
    :try_start_0
    const-string v6, "packageName"

    iget-object v7, v4, Lcom/android/server/am/ProcessRecordImpl$AppPss;->pkn:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 156
    const-string/jumbo v6, "totalPss"

    iget-object v7, v4, Lcom/android/server/am/ProcessRecordImpl$AppPss;->pss:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 157
    const-string/jumbo v6, "versionName"

    iget-object v7, v4, Lcom/android/server/am/ProcessRecordImpl$AppPss;->version:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 158
    const-string v6, "model"

    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 159
    const-string/jumbo v6, "userId"

    iget-object v7, v4, Lcom/android/server/am/ProcessRecordImpl$AppPss;->user:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 160
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    goto :goto_1

    .line 161
    :catch_0
    move-exception v6

    .line 162
    .local v6, "e":Lorg/json/JSONException;
    invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V

    .line 164
    .end local v3    # "pkn":Ljava/lang/String;
    .end local v4    # "appPss":Lcom/android/server/am/ProcessRecordImpl$AppPss;
    .end local v5    # "object":Lorg/json/JSONObject;
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_1
    goto :goto_0

    .line 165
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 166
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v2

    const-string v3, "mqs_whiteapp_lowmem_monitor_63691000"

    const/4 v4, 0x0

    const-string v5, "appPss"

    invoke-virtual {v2, v5, v0, v3, v4}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportEventsV2(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V

    .line 169
    :cond_2
    return-void
.end method
