public class com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo {
	 /* .source "MemoryFreezeStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryFreezeStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "RamFrozenAppInfo" */
} // .end annotation
/* # instance fields */
private Boolean mFrozen;
private java.lang.String mPackageName;
private Boolean mPendingFreeze;
private Integer mPid;
private Integer mUid;
private Long mlastActivityStopTime;
final com.android.server.am.MemoryFreezeStubImpl this$0; //synthetic
/* # direct methods */
static Boolean -$$Nest$fgetmFrozen ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mFrozen:Z */
} // .end method
static java.lang.String -$$Nest$fgetmPackageName ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageName;
} // .end method
static Boolean -$$Nest$fgetmPendingFreeze ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPendingFreeze:Z */
} // .end method
static Integer -$$Nest$fgetmPid ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPid:I */
} // .end method
static Integer -$$Nest$fgetmUid ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mUid:I */
} // .end method
static Long -$$Nest$fgetmlastActivityStopTime ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mlastActivityStopTime:J */
/* return-wide v0 */
} // .end method
static void -$$Nest$fputmFrozen ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mFrozen:Z */
return;
} // .end method
static void -$$Nest$fputmPendingFreeze ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPendingFreeze:Z */
return;
} // .end method
static void -$$Nest$fputmPid ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPid:I */
return;
} // .end method
static void -$$Nest$fputmlastActivityStopTime ( com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mlastActivityStopTime:J */
return;
} // .end method
public com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryFreezeStubImpl; */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 663 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 664 */
/* iput p2, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mUid:I */
/* .line 665 */
this.mPackageName = p3;
/* .line 666 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPendingFreeze:Z */
/* .line 667 */
/* iput-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mFrozen:Z */
/* .line 668 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mlastActivityStopTime:J */
/* .line 669 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPid:I */
/* .line 670 */
return;
} // .end method
