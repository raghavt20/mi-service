.class Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;
.super Ljava/lang/Object;
.source "AppStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->updateProcessInfo(Lcom/android/server/am/ProcessRecord;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

.field final synthetic val$record:Lcom/android/server/am/ProcessRecord;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;Lcom/android/server/am/ProcessRecord;)V
    .locals 0
    .param p1, "this$2"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 2027
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iput-object p2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->val$record:Lcom/android/server/am/ProcessRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 2030
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmContext(Lcom/android/server/am/AppStateManager;)Landroid/content/Context;

    move-result-object v1

    .line 2031
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPackageName(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->val$record:Lcom/android/server/am/ProcessRecord;

    iget-object v3, v3, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 2030
    invoke-static {v1, v2, v3}, Landroid/miui/AppOpsUtils;->getApplicationAutoStart(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmIsAutoStartApp(Lcom/android/server/am/AppStateManager$AppState;Z)V

    .line 2034
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmProcessPolicy(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/IProcessPolicy;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPackageName(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUserId(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v4

    invoke-interface {v1, v3, v4}, Lcom/android/server/am/IProcessPolicy;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fputmIsLockedApp(Lcom/android/server/am/AppStateManager$AppState;Z)V

    .line 2036
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v3

    iget-object v4, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v4}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/miui/server/smartpower/AppPowerResourceManager;->registerCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 2039
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->registerCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 2042
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v3

    const/4 v4, 0x3

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->registerCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 2045
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmPid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v3

    const/4 v4, 0x6

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/miui/server/smartpower/AppPowerResourceManager;->registerCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;II)V

    .line 2048
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmAppPowerResourceManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/AppPowerResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmResourcesCallback(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)Lcom/android/server/am/AppStateManager$AppState$RunningProcess$AppPowerResourceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$RunningProcess$1;->this$2:Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->-$$Nest$fgetmUid(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)I

    move-result v2

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/smartpower/AppPowerResourceManager;->registerCallback(ILcom/miui/server/smartpower/AppPowerResource$IAppPowerResourceCallback;I)V

    .line 2051
    return-void
.end method
