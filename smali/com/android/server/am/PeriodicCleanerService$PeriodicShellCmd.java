class com.android.server.am.PeriodicCleanerService$PeriodicShellCmd extends android.os.ShellCommand {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/PeriodicCleanerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PeriodicShellCmd" */
} // .end annotation
/* # instance fields */
com.android.server.am.PeriodicCleanerService mService;
final com.android.server.am.PeriodicCleanerService this$0; //synthetic
/* # direct methods */
public com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ( ) {
/* .locals 0 */
/* .param p2, "service" # Lcom/android/server/am/PeriodicCleanerService; */
/* .line 1748 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
/* .line 1749 */
this.mService = p2;
/* .line 1750 */
return;
} // .end method
private void runClean ( java.io.PrintWriter p0 ) {
/* .locals 7 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1798 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1799 */
/* .local v0, "opt":Ljava/lang/String; */
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).getNextOption ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextOption()Ljava/lang/String;
/* move-object v0, v1 */
/* if-nez v1, :cond_0 */
/* .line 1800 */
/* const-string/jumbo v1, "trigger clean package by periodic" */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1801 */
v1 = this.this$0;
com.android.server.am.PeriodicCleanerService .-$$Nest$mcleanPackageByPeriodic ( v1 );
/* goto/16 :goto_4 */
/* .line 1803 */
} // :cond_0
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* packed-switch v1, :pswitch_data_0 */
} // :cond_1
/* :pswitch_0 */
final String v1 = "-r"; // const-string v1, "-r"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
final String v2 = "error: invalid option: "; // const-string v2, "error: invalid option: "
/* packed-switch v1, :pswitch_data_1 */
/* .line 1837 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1838 */
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).onHelp ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->onHelp()V
/* goto/16 :goto_4 */
/* .line 1805 */
/* :pswitch_1 */
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextArgRequired()Ljava/lang/String;
/* .line 1806 */
/* .local v1, "reason":Ljava/lang/String; */
int v3 = -1; // const/4 v3, -0x1
/* .line 1807 */
/* .local v3, "pressure":I */
final String v4 = "periodic"; // const-string v4, "periodic"
v4 = (( java.lang.String ) v4 ).equals ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1808 */
v2 = this.this$0;
com.android.server.am.PeriodicCleanerService .-$$Nest$mcleanPackageByPeriodic ( v2 );
/* goto/16 :goto_4 */
/* .line 1810 */
} // :cond_2
final String v4 = "pressure-low"; // const-string v4, "pressure-low"
v4 = (( java.lang.String ) v4 ).equals ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1811 */
int v3 = 0; // const/4 v3, 0x0
/* .line 1812 */
} // :cond_3
final String v4 = "pressure-min"; // const-string v4, "pressure-min"
v4 = (( java.lang.String ) v4 ).equals ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1813 */
int v3 = 1; // const/4 v3, 0x1
/* .line 1814 */
} // :cond_4
final String v4 = "pressure-critical"; // const-string v4, "pressure-critical"
v4 = (( java.lang.String ) v4 ).equals ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 1815 */
int v3 = 2; // const/4 v3, 0x2
/* .line 1820 */
} // :goto_2
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).peekNextArg ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->peekNextArg()Ljava/lang/String;
/* const-string/jumbo v5, "trigger clean package by " */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 1821 */
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextArgRequired()Ljava/lang/String;
/* .line 1822 */
/* .local v4, "reasonTime":Ljava/lang/String; */
/* const-string/jumbo v6, "time" */
v6 = (( java.lang.String ) v6 ).equals ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 1823 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "-"; // const-string v5, "-"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1824 */
v2 = this.this$0;
com.android.server.am.PeriodicCleanerService .-$$Nest$mcleanPackageByTime ( v2,v3 );
/* .line 1826 */
} // :cond_5
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1827 */
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).onHelp ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->onHelp()V
/* .line 1829 */
} // .end local v4 # "reasonTime":Ljava/lang/String;
} // :goto_3
/* .line 1830 */
} // :cond_6
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1831 */
v2 = this.this$0;
final String v4 = "debug-pressure"; // const-string v4, "debug-pressure"
com.android.server.am.PeriodicCleanerService .-$$Nest$mcleanPackageByPressure ( v2,v3,v4 );
/* .line 1834 */
/* .line 1817 */
} // :cond_7
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "error: invalid reason: "; // const-string v4, "error: invalid reason: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1818 */
return;
/* .line 1841 */
} // .end local v1 # "reason":Ljava/lang/String;
} // .end local v3 # "pressure":I
} // :goto_4
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x5e5 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 1754 */
/* if-nez p1, :cond_0 */
/* .line 1755 */
v0 = (( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 1757 */
} // :cond_0
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1759 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v2 = "debug"; // const-string v2, "debug"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 3; // const/4 v2, 0x3
/* :sswitch_1 */
final String v2 = "clean"; // const-string v2, "clean"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 1; // const/4 v2, 0x1
/* :sswitch_2 */
final String v2 = "dump"; // const-string v2, "dump"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v1 */
/* :sswitch_3 */
/* const-string/jumbo v2, "updatelist" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 4; // const/4 v2, 0x4
/* :sswitch_4 */
final String v2 = "enable"; // const-string v2, "enable"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 2; // const/4 v2, 0x2
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 1788 */
v1 = (( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 1783 */
/* :pswitch_0 */
v2 = this.this$0;
com.android.server.am.PeriodicCleanerService .-$$Nest$mupdateProcStatsList ( v2 );
/* .line 1784 */
final String v2 = "periodic cleaner update list"; // const-string v2, "periodic cleaner update list"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1785 */
/* .line 1777 */
/* :pswitch_1 */
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Boolean .parseBoolean ( v2 );
/* .line 1778 */
/* .local v2, "debug":Z */
com.android.server.am.PeriodicCleanerService .-$$Nest$sfputDEBUG ( v2 );
/* .line 1779 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "periodic cleaner debug enabled: "; // const-string v4, "periodic cleaner debug enabled: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1780 */
/* .line 1770 */
} // .end local v2 # "debug":Z
/* :pswitch_2 */
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Boolean .parseBoolean ( v2 );
/* .line 1771 */
/* .local v2, "enable":Z */
v3 = this.mService;
com.android.server.am.PeriodicCleanerService .-$$Nest$fputmEnable ( v3,v2 );
/* .line 1772 */
v3 = this.mService;
com.android.server.am.PeriodicCleanerService .-$$Nest$fputmReady ( v3,v2 );
/* .line 1773 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "periodic cleaner enabled: "; // const-string v4, "periodic cleaner enabled: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1774 */
/* .line 1766 */
} // .end local v2 # "enable":Z
/* :pswitch_3 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->runClean(Ljava/io/PrintWriter;)V */
/* .line 1767 */
/* .line 1761 */
/* :pswitch_4 */
v2 = this.mService;
com.android.server.am.PeriodicCleanerService .-$$Nest$mdumpFgLru ( v2,v0 );
/* .line 1762 */
v2 = this.mService;
com.android.server.am.PeriodicCleanerService .-$$Nest$mdumpCleanHistory ( v2,v0 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1763 */
/* nop */
/* .line 1793 */
} // :goto_2
/* .line 1788 */
} // :goto_3
/* .line 1790 */
/* :catch_0 */
/* move-exception v2 */
/* .line 1791 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Error occurred.Check logcat for details."; // const-string v4, "Error occurred.Check logcat for details."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v2 ).getMessage ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1792 */
final String v3 = "PeriodicCleaner"; // const-string v3, "PeriodicCleaner"
final String v4 = "Error running shell command"; // const-string v4, "Error running shell command"
android.util.Slog .e ( v3,v4,v2 );
/* .line 1794 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_4
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4d6ada7d -> :sswitch_4 */
/* -0x1198a319 -> :sswitch_3 */
/* 0x2f39f4 -> :sswitch_2 */
/* 0x5a5b649 -> :sswitch_1 */
/* 0x5b09653 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onHelp ( ) {
/* .locals 3 */
/* .line 1846 */
(( com.android.server.am.PeriodicCleanerService$PeriodicShellCmd ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 1847 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "Periodic Cleaner commands:"; // const-string v1, "Periodic Cleaner commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1848 */
final String v1 = " help"; // const-string v1, " help"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1849 */
final String v1 = " Print this help text."; // const-string v1, " Print this help text."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1850 */
final String v1 = ""; // const-string v1, ""
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1851 */
final String v2 = " dump"; // const-string v2, " dump"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1852 */
final String v2 = " Print fg-lru and clean history."; // const-string v2, " Print fg-lru and clean history."
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1853 */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1854 */
final String v2 = " clean [-r REASON]"; // const-string v2, " clean [-r REASON]"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1855 */
final String v2 = " Trigger clean action."; // const-string v2, " Trigger clean action."
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1856 */
final String v2 = " -r: select clean reason"; // const-string v2, " -r: select clean reason"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1857 */
final String v2 = " REASON is one of:"; // const-string v2, " REASON is one of:"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1858 */
final String v2 = " periodic"; // const-string v2, " periodic"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1859 */
final String v2 = " pressure-low"; // const-string v2, " pressure-low"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1860 */
final String v2 = " pressure-min"; // const-string v2, " pressure-min"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1861 */
final String v2 = " pressure-critical"; // const-string v2, " pressure-critical"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1862 */
final String v2 = " pressure-low time"; // const-string v2, " pressure-low time"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1863 */
final String v2 = " pressure-min time"; // const-string v2, " pressure-min time"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1864 */
final String v2 = " pressure-critical time"; // const-string v2, " pressure-critical time"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1865 */
final String v2 = " default reason is periodic if no REASON"; // const-string v2, " default reason is periodic if no REASON"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1866 */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1867 */
final String v2 = " enable [true|false]"; // const-string v2, " enable [true|false]"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1868 */
final String v2 = " Enable/Disable peridic cleaner."; // const-string v2, " Enable/Disable peridic cleaner."
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1869 */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1870 */
final String v2 = " debug [true|false]"; // const-string v2, " debug [true|false]"
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1871 */
final String v2 = " Enable/Disable debug config."; // const-string v2, " Enable/Disable debug config."
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1872 */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1873 */
final String v1 = " updatelist"; // const-string v1, " updatelist"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1874 */
final String v1 = " Update highfrequencyapp and highmemoryapp list."; // const-string v1, " Update highfrequencyapp and highmemoryapp list."
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1875 */
return;
} // .end method
