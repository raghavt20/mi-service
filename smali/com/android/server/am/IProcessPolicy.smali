.class public interface abstract Lcom/android/server/am/IProcessPolicy;
.super Ljava/lang/Object;
.source "IProcessPolicy.java"


# static fields
.field public static final FLAG_CLOUD_WHITE_LIST:I = 0x4

.field public static final FLAG_DISABLE_FORCE_STOP:I = 0x20

.field public static final FLAG_DISABLE_TRIM_MEMORY:I = 0x10

.field public static final FLAG_DYNAMIC_WHITE_LIST:I = 0x2

.field public static final FLAG_ENABLE_CALL_PROTECT:I = 0x40

.field public static final FLAG_ENTERPRISE_APP_LIST:I = 0x1000

.field public static final FLAG_FAST_BOOT_APP_LIST:I = 0x800

.field public static final FLAG_NEED_TRACE_LIST:I = 0x80

.field public static final FLAG_SECRETLY_PROTECT_APP_LIST:I = 0x400

.field public static final FLAG_STATIC_WHILTE_LIST:I = 0x1

.field public static final FLAG_USER_DEFINED_LIST:I = 0x8

.field public static final FLAG_USER_REQUEST_CLEAN_WHITE_LIST:I = 0x2000

.field public static final REASON_ANR:Ljava/lang/String; = "anr"

.field public static final REASON_AUTO_IDLE_KILL:Ljava/lang/String; = "AutoIdleKill"

.field public static final REASON_AUTO_LOCK_OFF_CLEAN:Ljava/lang/String; = "AutoLockOffClean"

.field public static final REASON_AUTO_LOCK_OFF_CLEAN_BY_PRIORITY:Ljava/lang/String; = "AutoLockOffCleanByPriority"

.field public static final REASON_AUTO_POWER_KILL:Ljava/lang/String; = "AutoPowerKill"

.field public static final REASON_AUTO_SLEEP_CLEAN:Ljava/lang/String; = "AutoSleepClean"

.field public static final REASON_AUTO_SYSTEM_ABNORMAL_CLEAN:Ljava/lang/String; = "AutoSystemAbnormalClean"

.field public static final REASON_AUTO_THERMAL_KILL:Ljava/lang/String; = "AutoThermalKill"

.field public static final REASON_AUTO_THERMAL_KILL_ALL_LEVEL_1:Ljava/lang/String; = "AutoThermalKillAll1"

.field public static final REASON_AUTO_THERMAL_KILL_ALL_LEVEL_2:Ljava/lang/String; = "AutoThermalKillAll2"

.field public static final REASON_CRASH:Ljava/lang/String; = "crash"

.field public static final REASON_DISPLAY_SIZE_CHANGED:Ljava/lang/String; = "DisplaySizeChanged"

.field public static final REASON_FORCE_CLEAN:Ljava/lang/String; = "ForceClean"

.field public static final REASON_GAME_CLEAN:Ljava/lang/String; = "GameClean"

.field public static final REASON_GARBAGE_CLEAN:Ljava/lang/String; = "GarbageClean"

.field public static final REASON_LOCK_SCREEN_CLEAN:Ljava/lang/String; = "LockScreenClean"

.field public static final REASON_LOW_MEMO:Ljava/lang/String; = "lowMemory"

.field public static final REASON_MIUI_MEMO_SERVICE:Ljava/lang/String; = "MiuiMemoryService"

.field public static final REASON_ONE_KEY_CLEAN:Ljava/lang/String; = "OneKeyClean"

.field public static final REASON_OPTIMIZATION_CLEAN:Ljava/lang/String; = "OptimizationClean"

.field public static final REASON_SCREEN_OFF_CPU_CHECK_KILL:Ljava/lang/String; = "ScreenOffCPUCheckKill"

.field public static final REASON_SWIPE_UP_CLEAN:Ljava/lang/String; = "SwipeUpClean"

.field public static final REASON_UNKNOWN:Ljava/lang/String; = "Unknown"

.field public static final REASON_USER_DEFINED:Ljava/lang/String; = "UserDefined"

.field public static final TAG_PM:Ljava/lang/String; = "ProcessManager"

.field public static final TAG_WITH_CLASS_NAME:Z = false

.field public static final USER_ALL:I = -0x64


# direct methods
.method public static getAppMaxProcState(Lcom/android/server/am/ProcessRecord;)I
    .locals 1
    .param p0, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 87
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    iget v0, v0, Lcom/android/server/am/ProcessStateRecord;->mMaxProcState:I

    return v0
.end method

.method public static getMemoryThresholdForFastBooApp()Ljava/lang/Long;
    .locals 6

    .line 70
    invoke-static {}, Landroid/os/Process;->getTotalMemory()J

    move-result-wide v0

    const-wide/32 v2, 0x40000000

    div-long/2addr v0, v2

    .line 71
    .local v0, "deviceTotalMem":J
    const-wide/16 v2, 0x0

    .line 72
    .local v2, "memoryThreshold":J
    const-wide/16 v4, 0x4

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 73
    const-wide/32 v2, 0xc8000

    goto :goto_0

    .line 74
    :cond_0
    const-wide/16 v4, 0x8

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    .line 75
    const-wide/32 v2, 0x177000

    goto :goto_0

    .line 77
    :cond_1
    const-wide/32 v2, 0x1f4000

    .line 79
    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4
.end method

.method public static setAppMaxProcState(Lcom/android/server/am/ProcessRecord;I)V
    .locals 1
    .param p0, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p1, "targetMaxProcState"    # I

    .line 83
    iget-object v0, p0, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    iput p1, v0, Lcom/android/server/am/ProcessStateRecord;->mMaxProcState:I

    .line 84
    return-void
.end method


# virtual methods
.method public abstract getActiveUidList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isLockedApplication(Ljava/lang/String;I)Z
.end method
