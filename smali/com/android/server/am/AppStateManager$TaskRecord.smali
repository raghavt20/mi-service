.class Lcom/android/server/am/AppStateManager$TaskRecord;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TaskRecord"
.end annotation


# instance fields
.field mLastPkg:Ljava/lang/String;

.field mLastUid:I

.field final mProcesses:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Lcom/android/server/am/AppStateManager$AppState$RunningProcess;",
            ">;"
        }
    .end annotation
.end field

.field mTaskId:I

.field final synthetic this$0:Lcom/android/server/am/AppStateManager;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/am/AppStateManager;
    .param p2, "taskId"    # I

    .line 3407
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3404
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastPkg:Ljava/lang/String;

    .line 3405
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mProcesses:Landroid/util/ArraySet;

    .line 3408
    iput p2, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I

    .line 3409
    return-void
.end method


# virtual methods
.method addProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V
    .locals 1
    .param p1, "process"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 3416
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 3417
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getUid()I

    move-result v0

    iput v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastUid:I

    .line 3418
    invoke-virtual {p1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastPkg:Ljava/lang/String;

    .line 3419
    return-void
.end method

.method dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 3445
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    Task:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastUid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastPkg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3446
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 3447
    .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "        "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3448
    .end local v1    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    goto :goto_0

    .line 3449
    :cond_0
    return-void
.end method

.method getTaskId()I
    .locals 1

    .line 3412
    iget v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I

    return v0
.end method

.method isProcessInTask(II)Z
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 3422
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 3423
    .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getPid()I

    move-result v2

    if-ne v2, p2, :cond_0

    .line 3424
    const/4 v0, 0x1

    return v0

    .line 3426
    .end local v1    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_0
    goto :goto_0

    .line 3427
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method isProcessInTask(Ljava/lang/String;)Z
    .locals 3
    .param p1, "prcName"    # Ljava/lang/String;

    .line 3431
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;

    .line 3432
    .local v1, "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    invoke-virtual {v1}, Lcom/android/server/am/AppStateManager$AppState$RunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3433
    const/4 v0, 0x1

    return v0

    .line 3435
    .end local v1    # "proc":Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    :cond_0
    goto :goto_0

    .line 3436
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 3441
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaskRecord{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastUid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastPkg:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$TaskRecord;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
