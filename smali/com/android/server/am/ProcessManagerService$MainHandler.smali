.class Lcom/android/server/am/ProcessManagerService$MainHandler;
.super Landroid/os/Handler;
.source "ProcessManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MainHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/am/ProcessManagerService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 243
    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$MainHandler;->this$0:Lcom/android/server/am/ProcessManagerService;

    .line 244
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    .line 245
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 249
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 251
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$MainHandler;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmActivityManagerService(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v0

    monitor-enter v0

    .line 252
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$MainHandler;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v1}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmProcessStarter(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessStarter;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessStarter;->restoreLastProcessesInfoLocked(I)V

    .line 253
    monitor-exit v0

    .line 254
    goto :goto_0

    .line 253
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 258
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
