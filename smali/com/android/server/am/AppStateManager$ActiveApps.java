class com.android.server.am.AppStateManager$ActiveApps {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ActiveApps" */
} // .end annotation
/* # instance fields */
private final android.util.SparseArray mActiveApps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/am/AppStateManager$AppState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.am.AppStateManager this$0; //synthetic
/* # direct methods */
 com.android.server.am.AppStateManager$ActiveApps ( ) {
/* .locals 0 */
/* .line 811 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 809 */
/* new-instance p1, Landroid/util/SparseArray; */
/* invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V */
this.mActiveApps = p1;
/* .line 811 */
return;
} // .end method
/* # virtual methods */
void clear ( ) {
/* .locals 1 */
/* .line 830 */
v0 = this.mActiveApps;
(( android.util.SparseArray ) v0 ).clear ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
/* .line 831 */
return;
} // .end method
void dump ( java.io.PrintWriter p0, java.lang.String[] p1, Integer p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "args" # [Ljava/lang/String; */
/* .param p3, "opti" # I */
/* .param p4, "dpUid" # I */
/* .line 855 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mActiveApps;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v0, v1, :cond_2 */
/* .line 856 */
v1 = this.mActiveApps;
v1 = (( android.util.SparseArray ) v1 ).keyAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 857 */
/* .local v1, "uid":I */
v2 = this.mActiveApps;
(( android.util.SparseArray ) v2 ).valueAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/AppStateManager$AppState; */
/* .line 858 */
/* .local v2, "app":Lcom/android/server/am/AppStateManager$AppState; */
if ( p4 != null) { // if-eqz p4, :cond_0
/* .line 859 */
/* if-ne v1, p4, :cond_1 */
/* .line 860 */
(( com.android.server.am.AppStateManager$AppState ) v2 ).dump ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 863 */
} // :cond_0
(( com.android.server.am.AppStateManager$AppState ) v2 ).dump ( p1, p2, p3 ); // invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/am/AppStateManager$AppState;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
/* .line 855 */
} // .end local v1 # "uid":I
} // .end local v2 # "app":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_1
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 866 */
} // .end local v0 # "i":I
} // :cond_2
return;
} // .end method
com.android.server.am.AppStateManager$AppState get ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 825 */
v0 = this.mActiveApps;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/am/AppStateManager$AppState; */
} // .end method
Integer indexOfKey ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 850 */
v0 = this.mActiveApps;
v0 = (( android.util.SparseArray ) v0 ).indexOfKey ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I
} // .end method
Integer keyAt ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "index" # I */
/* .line 845 */
v0 = this.mActiveApps;
v0 = (( android.util.SparseArray ) v0 ).keyAt ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->keyAt(I)I
} // .end method
void put ( Integer p0, com.android.server.am.AppStateManager$AppState p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "value" # Lcom/android/server/am/AppStateManager$AppState; */
/* .line 815 */
v0 = this.mActiveApps;
(( android.util.SparseArray ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 816 */
return;
} // .end method
void remove ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 820 */
v0 = this.mActiveApps;
(( android.util.SparseArray ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 821 */
return;
} // .end method
Integer size ( ) {
/* .locals 1 */
/* .line 835 */
v0 = this.mActiveApps;
v0 = (( android.util.SparseArray ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/SparseArray;->size()I
} // .end method
com.android.server.am.AppStateManager$AppState valueAt ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "index" # I */
/* .line 840 */
v0 = this.mActiveApps;
(( android.util.SparseArray ) v0 ).valueAt ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/am/AppStateManager$AppState; */
} // .end method
