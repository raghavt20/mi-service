public class com.android.server.am.MemoryFreezeStubImpl$MyHandler extends android.os.Handler {
	 /* .source "MemoryFreezeStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryFreezeStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "MyHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryFreezeStubImpl this$0; //synthetic
/* # direct methods */
public com.android.server.am.MemoryFreezeStubImpl$MyHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryFreezeStubImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 767 */
this.this$0 = p1;
/* .line 768 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 769 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 772 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 779 */
/* :pswitch_0 */
v0 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$fgetmMemoryFreezeCloud ( v0 );
v1 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$fgetmContext ( v1 );
(( com.android.server.am.MemoryFreezeCloud ) v0 ).registerCloudWhiteList ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeCloud;->registerCloudWhiteList(Landroid/content/Context;)V
/* .line 780 */
v0 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$fgetmMemoryFreezeCloud ( v0 );
v1 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$fgetmContext ( v1 );
(( com.android.server.am.MemoryFreezeCloud ) v0 ).registerMemfreezeOperation ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeCloud;->registerMemfreezeOperation(Landroid/content/Context;)V
/* .line 781 */
/* .line 800 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$mcheckAndKill ( v0 );
/* .line 801 */
/* .line 796 */
/* :pswitch_2 */
v0 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$mcheckAndWriteBack ( v0 );
/* .line 797 */
/* .line 792 */
/* :pswitch_3 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$mcheckAndReclaim ( v0,v1 );
/* .line 793 */
/* .line 788 */
/* :pswitch_4 */
v0 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$mcheckUnused ( v0 );
/* .line 789 */
/* .line 784 */
/* :pswitch_5 */
v0 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$fgetmContext ( v0 );
(( com.android.server.am.MemoryFreezeStubImpl ) v0 ).registerMiuiFreezeObserver ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->registerMiuiFreezeObserver(Landroid/content/Context;)V
/* .line 785 */
/* .line 774 */
/* :pswitch_6 */
v0 = this.this$0;
com.android.server.am.MemoryFreezeStubImpl .-$$Nest$fgetmContext ( v0 );
(( com.android.server.am.MemoryFreezeStubImpl ) v0 ).registerCloudObserver ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->registerCloudObserver(Landroid/content/Context;)V
/* .line 775 */
v0 = this.this$0;
(( com.android.server.am.MemoryFreezeStubImpl ) v0 ).updateCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->updateCloudControlParas()V
/* .line 776 */
/* nop */
/* .line 806 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
