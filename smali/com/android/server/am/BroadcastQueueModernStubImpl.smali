.class public Lcom/android/server/am/BroadcastQueueModernStubImpl;
.super Ljava/lang/Object;
.source "BroadcastQueueModernStubImpl.java"

# interfaces
.implements Lcom/android/server/am/BroadcastQueueModernStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;,
        Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;,
        Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    }
.end annotation


# static fields
.field private static final ABNORMAL_BROADCAST_RATE:F = 0.6f

.field private static final ACTION_C2DM:Ljava/lang/String; = "com.google.android.c2dm.intent.RECEIVE"

.field private static final ACTION_MIPUSH_MESSAGE_ARRIVED:Ljava/lang/String; = "com.xiaomi.mipush.MESSAGE_ARRIVED"

.field private static final ACTION_NFC:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ACTIVE_ORDERED_BROADCAST_LIMIT:I

.field private static BR_LIST:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiui/mqsas/sdk/event/BroadcastEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z = true

.field private static final DEFER_CACHED_WHITE_LIST:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "android.intent.extra.PACKAGE_NAME"

.field public static final FLAG_IMMUTABLE:I = 0x4000000

.field private static final IS_STABLE_VERSION:Z

.field private static final MAX_QUANTITY:I = 0x1e

.field public static final OP_PROCESS_OUTGOING_CALLS:I = 0x36

.field static final TAG:Ljava/lang/String; = "BroadcastQueueInjector"

.field private static broadcastDispatcherFeature:Lcom/android/server/am/BroadcastDispatcherFeatureImpl;

.field private static volatile mBRHandler:Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;

.field private static mBroadcastMap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;",
            ">;"
        }
    .end annotation
.end field

.field private static mDispatchThreshold:J

.field private static mFinishDeno:I

.field private static mIndex:I

.field private static final mInternationalSpecialAction:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final mObject:Ljava/lang/Object;

.field private static sAbnormalBroadcastWarning:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static final sSpecialSkipAction:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSystemBootCompleted:Z

.field private static final sSystemSkipAction:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActivityRequestId:I

.field private mAmService:Lcom/android/server/am/ActivityManagerService;

.field private mContext:Landroid/content/Context;

.field private mSecurityInternal:Lmiui/security/SecurityManagerInternal;

.field private pm:Landroid/os/PowerManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/am/BroadcastQueueModernStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smisSystemBootCompleted()Z
    .locals 1

    invoke-static {}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isSystemBootCompleted()Z

    move-result v0

    return v0
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mInternationalSpecialAction:Ljava/util/List;

    .line 79
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const-string v1, "android.intent.action.LOCKED_BOOT_COMPLETED"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->BR_LIST:Ljava/util/ArrayList;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBroadcastMap:Ljava/util/ArrayList;

    .line 108
    const/4 v0, 0x0

    sput v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mIndex:I

    .line 109
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mObject:Ljava/lang/Object;

    .line 112
    const-string v1, "persist.broadcast.time"

    const-wide/16 v2, 0xbb8

    invoke-static {v1, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    sput-wide v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mDispatchThreshold:J

    .line 113
    const-string v1, "persist.broadcast.count"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mFinishDeno:I

    .line 114
    sget-boolean v1, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    sput-boolean v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->IS_STABLE_VERSION:Z

    .line 117
    const-string v1, "persist.activebr.limit"

    const/16 v2, 0x3e8

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->ACTIVE_ORDERED_BROADCAST_LIMIT:I

    .line 119
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sAbnormalBroadcastWarning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 129
    new-instance v1, Lcom/android/server/am/BroadcastDispatcherFeatureImpl;

    invoke-direct {v1}, Lcom/android/server/am/BroadcastDispatcherFeatureImpl;-><init>()V

    sput-object v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->broadcastDispatcherFeature:Lcom/android/server/am/BroadcastDispatcherFeatureImpl;

    .line 195
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSystemSkipAction:Ljava/util/ArrayList;

    .line 196
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    sput-object v2, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSpecialSkipAction:Ljava/util/Set;

    .line 197
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    sput-object v3, Lcom/android/server/am/BroadcastQueueModernStubImpl;->ACTION_NFC:Ljava/util/Set;

    .line 199
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    sput-object v4, Lcom/android/server/am/BroadcastQueueModernStubImpl;->DEFER_CACHED_WHITE_LIST:Ljava/util/Set;

    .line 202
    const-string v5, "android.accounts.LOGIN_ACCOUNTS_PRE_CHANGED"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    const-string v5, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    const-string v5, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    const-string v5, "com.android.updater.action.UPDATE_SUCCESSED"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    const-string v5, "com.android.updater.action.OTA_UPDATE_SUCCESSED"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    const-string v5, "miui.media.AUDIO_VOIP_RECORD_STATE_CHANGED_ACTION"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    const-string v5, "com.android.settings.stylus.STYLUS_STATE_SOC"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    const-string v5, "com.android.settings.stylus.STYLUS_BATTERY_NOTIFY"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    const-string v5, "com.android.settings.stylus.STYLUS_PLACE_ERROR"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    const-string v5, "com.xiaomi.bluetooth.action.KEYBOARD_ATTACH"

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    const-string v5, "is_pad"

    invoke-static {v5, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const-string v0, "miui.intent.action.ACTION_POGO_CONNECTED_STATE"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_0
    const-string v0, "android.intent.action.MEDIA_BUTTON"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 216
    const-string v0, "android.appwidget.action.APPWIDGET_ENABLED"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 217
    const-string v0, "android.appwidget.action.APPWIDGET_DISABLED"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 218
    const-string v0, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 219
    const-string v0, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220
    const-string v0, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 221
    const-string v0, "android.appwidget.action.APPWIDGET_RESTORED"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 222
    const-string v0, "miui.intent.action.contentcatcher"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 223
    const-string v0, "com.miui.contentextension.action.PACKAGE"

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 224
    const-string v0, "com.miui.nfc.action.TRANSACTION"

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 225
    const-string v0, "com.miui.intent.action.SWIPE_CARD"

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 226
    const-string v0, "com.miui.nfc.action.RF_ON"

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 227
    const-string v0, "com.miui.intent.action.DOUBLE_CLICK"

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 228
    const-string v0, "com.android.nfc_extras.action.RF_FIELD_OFF_DETECTED"

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 229
    const-string v0, "android.nfc.action.TRANSACTION_DETECTED"

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 231
    const-string v0, "com.xiaomi.market"

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 232
    const-string v0, "com.xiaomi.mipicks"

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 233
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->pm:Landroid/os/PowerManager;

    return-void
.end method

.method private checkSpecialAction(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
    .locals 3
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "action"    # Ljava/lang/String;

    .line 248
    if-eqz p2, :cond_1

    sget-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSpecialSkipAction:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->ACTION_NFC:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    const-string v0, "com.xiaomi.mipush.RECEIVE_MESSAGE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    :cond_0
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v0

    iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    const/4 v2, 0x1

    invoke-static {v2, v0, v1, p2}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->notifyPowerKeeperEvent(IIILjava/lang/String;)V

    .line 251
    return v2

    .line 253
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public static checkTime(JLjava/lang/String;)V
    .locals 6
    .param p0, "startTime"    # J
    .param p2, "where"    # Ljava/lang/String;

    .line 882
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 884
    .local v0, "now":J
    sub-long v2, v0, p0

    const-wide/16 v4, 0xbb8

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 885
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slow operation: processNextBroadcast "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v0, p0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms so far, now at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BroadcastQueueInjector"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    :cond_0
    return-void
.end method

.method private forceStopAbnormalApp(Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;)V
    .locals 5
    .param p1, "r"    # Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;

    .line 745
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v0

    .line 746
    :try_start_0
    const-string v1, "BroadcastQueueInjector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "force-stop abnormal app:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " userId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    const-class v1, Landroid/app/ActivityManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManagerInternal;

    iget-object v2, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    iget v3, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I

    const-string v4, "abnormal ordered broadcast"

    .line 748
    invoke-virtual {v1, v2, v3, v4}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 750
    monitor-exit v0

    .line 751
    return-void

    .line 750
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getAbnormalBroadcastByCountIfExisted(Ljava/util/List;)Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/am/BroadcastRecord;",
            ">;)",
            "Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;"
        }
    .end annotation

    .line 800
    .local p1, "broadcasts":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/BroadcastRecord;>;"
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 802
    .local v0, "startTime":J
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 803
    .local v2, "abnormalBroadcastMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/BroadcastRecord;

    .line 804
    .local v4, "r":Lcom/android/server/am/BroadcastRecord;
    const-string v5, "android"

    iget-object v6, v4, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v4, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 805
    const-string v6, "com.google.android.gms"

    invoke-static {v6, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 806
    goto :goto_0

    .line 809
    :cond_1
    new-instance v5, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;

    invoke-direct {v5, v4}, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;-><init>(Lcom/android/server/am/BroadcastRecord;)V

    .line 810
    .local v5, "abnormalRecord":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 811
    .local v6, "count":Ljava/lang/Integer;
    const/4 v7, 0x1

    if-nez v6, :cond_2

    .line 812
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 814
    :cond_2
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/2addr v8, v7

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 816
    .end local v4    # "r":Lcom/android/server/am/BroadcastRecord;
    .end local v5    # "abnormalRecord":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    .end local v6    # "count":Ljava/lang/Integer;
    :goto_1
    goto :goto_0

    .line 818
    :cond_3
    const/4 v3, 0x0

    .line 819
    .local v3, "recordWithMaxCount":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    const/4 v4, 0x0

    .line 820
    .local v4, "maxCount":I
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 821
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;Ljava/lang/Integer;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v7, v4, :cond_4

    .line 822
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    move-object v3, v7

    check-cast v3, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;

    .line 823
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 825
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;Ljava/lang/Integer;>;"
    :cond_4
    goto :goto_2

    .line 827
    :cond_5
    const-string v5, "BroadcastQueueInjector"

    if-eqz v3, :cond_7

    iget-object v6, v3, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    sget v6, Lcom/android/server/am/BroadcastQueueModernStubImpl;->ACTIVE_ORDERED_BROADCAST_LIMIT:I

    if-ge v4, v6, :cond_6

    goto :goto_3

    .line 834
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "found abnormal broadcast in list by max count:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cost:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 835
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v0

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 834
    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    return-object v3

    .line 829
    :cond_7
    :goto_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "the max number of same broadcasts in queue is not large enough:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " with count:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    const/4 v5, 0x0

    return-object v5
.end method

.method private getAbnormalBroadcastByRateIfExists(Ljava/util/List;)Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/am/BroadcastRecord;",
            ">;)",
            "Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;"
        }
    .end annotation

    .line 755
    .local p1, "broadcasts":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/BroadcastRecord;>;"
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 757
    .local v0, "startTime":J
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/BroadcastRecord;

    .line 758
    .local v2, "result":Lcom/android/server/am/BroadcastRecord;
    const/4 v3, 0x1

    .line 759
    .local v3, "count":I
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 760
    if-nez v3, :cond_0

    .line 761
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v2, v5

    check-cast v2, Lcom/android/server/am/BroadcastRecord;

    .line 762
    const/4 v3, 0x1

    goto :goto_1

    .line 763
    :cond_0
    iget-object v5, v2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/BroadcastRecord;

    iget-object v6, v6, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, v2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 764
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/BroadcastRecord;

    iget-object v6, v6, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget v5, v2, Lcom/android/server/am/BroadcastRecord;->userId:I

    .line 765
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/BroadcastRecord;

    iget v6, v6, Lcom/android/server/am/BroadcastRecord;->userId:I

    if-ne v5, v6, :cond_1

    .line 766
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 768
    :cond_1
    add-int/lit8 v3, v3, -0x1

    .line 759
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 772
    .end local v4    # "i":I
    :cond_2
    iget-object v4, v2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    const-string v6, "BroadcastQueueInjector"

    if-nez v4, :cond_7

    iget-object v4, v2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 773
    const-string v7, "android"

    invoke-static {v4, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, v2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 774
    const-string v7, "com.google.android.gms"

    invoke-static {v4, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    int-to-float v4, v3

    .line 775
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    int-to-float v7, v7

    const v8, 0x3e4cccd0    # 0.20000005f

    mul-float/2addr v7, v8

    cmpg-float v4, v4, v7

    if-gez v4, :cond_3

    goto/16 :goto_3

    .line 780
    :cond_3
    const/4 v3, 0x0

    .line 781
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/BroadcastRecord;

    .line 782
    .local v7, "r":Lcom/android/server/am/BroadcastRecord;
    iget-object v8, v2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v7, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, v2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    iget-object v9, v7, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 783
    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 784
    add-int/lit8 v3, v3, 0x1

    .line 786
    .end local v7    # "r":Lcom/android/server/am/BroadcastRecord;
    :cond_4
    goto :goto_2

    .line 788
    :cond_5
    int-to-float v4, v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    int-to-float v7, v7

    const v8, 0x3f19999a    # 0.6f

    mul-float/2addr v7, v8

    cmpg-float v4, v4, v7

    if-gez v4, :cond_6

    .line 789
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "abnormal broadcast not found with count:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    return-object v5

    .line 793
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "found abnormal broadcast in list by rate:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cost:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 794
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v0

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 793
    invoke-static {v6, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    new-instance v4, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;

    invoke-direct {v4, v2}, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;-><init>(Lcom/android/server/am/BroadcastRecord;)V

    return-object v4

    .line 776
    :cond_7
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "abnormal broadcast not found with first loop count:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " with caller:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    return-object v5
.end method

.method static getBRReportHandler()Landroid/os/Handler;
    .locals 4

    .line 489
    sget-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBRHandler:Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;

    if-nez v0, :cond_1

    .line 490
    sget-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mObject:Ljava/lang/Object;

    monitor-enter v0

    .line 491
    :try_start_0
    sget-object v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBRHandler:Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;

    if-nez v1, :cond_0

    .line 492
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "brreport-thread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 493
    .local v1, "mBRThread":Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 494
    new-instance v2, Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;-><init>(Landroid/os/Looper;)V

    sput-object v2, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBRHandler:Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;

    .line 496
    .end local v1    # "mBRThread":Landroid/os/HandlerThread;
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 498
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBRHandler:Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;

    return-object v0
.end method

.method static getInstance()Lcom/android/server/am/BroadcastQueueModernStubImpl;
    .locals 1

    .line 132
    invoke-static {}, Lcom/android/server/am/BroadcastQueueModernStub;->get()Lcom/android/server/am/BroadcastQueueModernStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;

    return-object v0
.end method

.method private getNextRequestIdLocked()I
    .locals 2

    .line 650
    iget v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mActivityRequestId:I

    const v1, 0x7fffffff

    if-lt v0, v1, :cond_0

    .line 651
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mActivityRequestId:I

    .line 653
    :cond_0
    iget v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mActivityRequestId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mActivityRequestId:I

    .line 654
    return v0
.end method

.method private getPackageLabelLocked(Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;)Ljava/lang/String;
    .locals 4
    .param p1, "r"    # Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;

    .line 687
    const/4 v0, 0x0

    .line 688
    .local v0, "label":Ljava/lang/String;
    iget-object v1, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    iget v2, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I

    invoke-direct {p0, v1, v2}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    .line 689
    .local v1, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/am/PackageList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 690
    iget-object v2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 691
    .local v2, "labelChar":Ljava/lang/CharSequence;
    if-eqz v2, :cond_0

    .line 692
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 696
    .end local v2    # "labelChar":Ljava/lang/CharSequence;
    :cond_0
    if-nez v0, :cond_1

    .line 697
    iget-object v0, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 699
    :cond_1
    return-object v0
.end method

.method private getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
    .locals 3
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 703
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 704
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ProcessRecord;

    .line 705
    .local v1, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v1, Lcom/android/server/am/ProcessRecord;->userId:I

    if-ne v2, p2, :cond_0

    .line 707
    return-object v1

    .line 703
    .end local v1    # "app":Lcom/android/server/am/ProcessRecord;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 710
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private isInternationalSpecialAction(Ljava/lang/String;Landroid/content/pm/ResolveInfo;)Z
    .locals 5
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "info"    # Landroid/content/pm/ResolveInfo;

    .line 404
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mInternationalSpecialAction:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mContext:Landroid/content/Context;

    iget-object v2, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v3, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v4, "BroadcastQueueImpl#specifyBroadcast"

    invoke-static {v0, v2, v3, v4}, Landroid/miui/AppOpsUtils;->getApplicationSpecialBroadcast(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)I

    move-result v0

    .line 409
    .local v0, "autoStartMode":I
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 405
    .end local v0    # "autoStartMode":I
    :cond_2
    :goto_0
    return v1
.end method

.method private static isSystemBootCompleted()Z
    .locals 2

    .line 502
    sget-boolean v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSystemBootCompleted:Z

    if-nez v0, :cond_0

    .line 503
    const-string/jumbo v0, "sys.boot_completed"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSystemBootCompleted:Z

    .line 505
    :cond_0
    sget-boolean v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSystemBootCompleted:Z

    return v0
.end method

.method private static notifyPowerKeeperEvent(IIILjava/lang/String;)V
    .locals 0
    .param p0, "state"    # I
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "action"    # Ljava/lang/String;

    .line 245
    return-void
.end method

.method static onBroadcastFinished(Landroid/content/Intent;Ljava/lang/String;IJJJJI)V
    .locals 20
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "caller"    # Ljava/lang/String;
    .param p2, "callingPid"    # I
    .param p3, "enTime"    # J
    .param p5, "disTime"    # J
    .param p7, "finTime"    # J
    .param p9, "mTimeoutPeriod"    # J
    .param p11, "receiverSize"    # I

    .line 512
    move-wide/from16 v0, p3

    move-wide/from16 v2, p5

    move-wide/from16 v4, p7

    move/from16 v6, p11

    sget-boolean v7, Lcom/android/server/am/BroadcastQueueModernStubImpl;->IS_STABLE_VERSION:Z

    if-eqz v7, :cond_0

    return-void

    .line 513
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 514
    .local v7, "action":Ljava/lang/String;
    if-nez v7, :cond_1

    const-string v7, "null"

    .line 515
    :cond_1
    if-nez p1, :cond_2

    const-string v8, "android"

    goto :goto_0

    :cond_2
    move-object/from16 v8, p1

    .line 516
    .end local p1    # "caller":Ljava/lang/String;
    .local v8, "caller":Ljava/lang/String;
    :goto_0
    move/from16 v9, p2

    .line 517
    .local v9, "pid":I
    const-string v10, ""

    .line 519
    .local v10, "reason":Ljava/lang/String;
    move-object v11, v8

    .line 520
    .local v11, "packageName":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 521
    .local v12, "timeStamp":J
    sub-long v14, v2, v0

    sget-wide v16, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mDispatchThreshold:J

    cmp-long v14, v14, v16

    if-gez v14, :cond_4

    sub-long v16, v4, v2

    move-wide/from16 v18, v12

    .end local v12    # "timeStamp":J
    .local v18, "timeStamp":J
    int-to-long v12, v6

    mul-long v12, v12, p9

    sget v14, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mFinishDeno:I

    int-to-long v14, v14

    div-long/2addr v12, v14

    cmp-long v12, v16, v12

    if-ltz v12, :cond_3

    goto :goto_1

    :cond_3
    const/4 v12, 0x0

    goto :goto_2

    .end local v18    # "timeStamp":J
    .restart local v12    # "timeStamp":J
    :cond_4
    move-wide/from16 v18, v12

    .end local v12    # "timeStamp":J
    .restart local v18    # "timeStamp":J
    :goto_1
    const/4 v12, 0x1

    .line 523
    .local v12, "needUpdate":Z
    :goto_2
    sget v13, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mIndex:I

    const/16 v14, 0x1e

    if-ltz v13, :cond_5

    if-le v13, v14, :cond_6

    :cond_5
    const/4 v13, 0x0

    :cond_6
    sput v13, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mIndex:I

    .line 524
    new-instance v13, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;

    invoke-direct {v13, v7, v8}, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    .local v13, "broadcastMap":Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;
    sget v15, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mIndex:I

    if-eqz v15, :cond_8

    if-gt v15, v14, :cond_8

    sget-object v15, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBroadcastMap:Ljava/util/ArrayList;

    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 526
    sget-object v14, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBroadcastMap:Ljava/util/ArrayList;

    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v14

    .line 527
    .local v14, "index":I
    sget-object v15, Lcom/android/server/am/BroadcastQueueModernStubImpl;->BR_LIST:Ljava/util/ArrayList;

    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lmiui/mqsas/sdk/event/BroadcastEvent;

    .line 528
    .local v15, "BE":Lmiui/mqsas/sdk/event/BroadcastEvent;
    invoke-virtual {v15}, Lmiui/mqsas/sdk/event/BroadcastEvent;->addCount()V

    .line 529
    move-object/from16 v16, v8

    move/from16 v17, v9

    .end local v8    # "caller":Ljava/lang/String;
    .end local v9    # "pid":I
    .local v16, "caller":Ljava/lang/String;
    .local v17, "pid":I
    sub-long v8, v4, v0

    invoke-virtual {v15, v8, v9}, Lmiui/mqsas/sdk/event/BroadcastEvent;->addTotalTime(J)V

    .line 530
    if-eqz v12, :cond_7

    .line 531
    invoke-virtual {v15, v10}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setReason(Ljava/lang/String;)V

    .line 532
    invoke-virtual {v15, v0, v1}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setEnTime(J)V

    .line 533
    invoke-virtual {v15, v2, v3}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setDisTime(J)V

    .line 534
    invoke-virtual {v15, v4, v5}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setFinTime(J)V

    .line 535
    invoke-virtual {v15, v6}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setNumReceivers(I)V

    .line 537
    .end local v14    # "index":I
    .end local v15    # "BE":Lmiui/mqsas/sdk/event/BroadcastEvent;
    :cond_7
    move-object/from16 v9, v16

    move/from16 v14, v17

    move-wide/from16 v0, v18

    goto/16 :goto_3

    .line 525
    .end local v16    # "caller":Ljava/lang/String;
    .end local v17    # "pid":I
    .restart local v8    # "caller":Ljava/lang/String;
    .restart local v9    # "pid":I
    :cond_8
    move-object/from16 v16, v8

    move/from16 v17, v9

    .line 538
    .end local v8    # "caller":Ljava/lang/String;
    .end local v9    # "pid":I
    .restart local v16    # "caller":Ljava/lang/String;
    .restart local v17    # "pid":I
    sget v8, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mIndex:I

    if-lt v8, v14, :cond_9

    .line 539
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v8

    .line 540
    .local v8, "message":Landroid/os/Message;
    const/4 v9, 0x1

    iput v9, v8, Landroid/os/Message;->what:I

    .line 541
    new-instance v9, Landroid/content/pm/ParceledListSlice;

    sget-object v14, Lcom/android/server/am/BroadcastQueueModernStubImpl;->BR_LIST:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/ArrayList;

    invoke-direct {v9, v14}, Landroid/content/pm/ParceledListSlice;-><init>(Ljava/util/List;)V

    iput-object v9, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 542
    invoke-static {}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->getBRReportHandler()Landroid/os/Handler;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 543
    sget-object v9, Lcom/android/server/am/BroadcastQueueModernStubImpl;->BR_LIST:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 544
    sget-object v9, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBroadcastMap:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 545
    const/4 v9, 0x0

    sput v9, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mIndex:I

    .line 547
    .end local v8    # "message":Landroid/os/Message;
    :cond_9
    sget-object v8, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mBroadcastMap:Ljava/util/ArrayList;

    invoke-virtual {v8, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 548
    sget v8, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mIndex:I

    const/4 v9, 0x1

    add-int/2addr v8, v9

    sput v8, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mIndex:I

    .line 549
    new-instance v8, Lmiui/mqsas/sdk/event/BroadcastEvent;

    invoke-direct {v8}, Lmiui/mqsas/sdk/event/BroadcastEvent;-><init>()V

    .line 550
    .local v8, "broadcast":Lmiui/mqsas/sdk/event/BroadcastEvent;
    const/16 v9, 0x40

    invoke-virtual {v8, v9}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setType(I)V

    .line 551
    if-eqz v12, :cond_a

    .line 552
    invoke-virtual {v8, v10}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setReason(Ljava/lang/String;)V

    .line 553
    invoke-virtual {v8, v0, v1}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setEnTime(J)V

    .line 554
    invoke-virtual {v8, v2, v3}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setDisTime(J)V

    .line 555
    invoke-virtual {v8, v4, v5}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setFinTime(J)V

    .line 556
    invoke-virtual {v8, v6}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setNumReceivers(I)V

    .line 558
    :cond_a
    invoke-virtual {v8, v7}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setAction(Ljava/lang/String;)V

    .line 559
    move-object/from16 v9, v16

    .end local v16    # "caller":Ljava/lang/String;
    .local v9, "caller":Ljava/lang/String;
    invoke-virtual {v8, v9}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setCallerPackage(Ljava/lang/String;)V

    .line 560
    const/4 v14, 0x1

    invoke-virtual {v8, v14}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setCount(I)V

    .line 561
    sub-long v14, v4, v0

    invoke-virtual {v8, v14, v15}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setTotalTime(J)V

    .line 562
    move/from16 v14, v17

    .end local v17    # "pid":I
    .local v14, "pid":I
    invoke-virtual {v8, v14}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setPid(I)V

    .line 564
    invoke-virtual {v8, v11}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setPackageName(Ljava/lang/String;)V

    .line 565
    move-wide/from16 v0, v18

    .end local v18    # "timeStamp":J
    .local v0, "timeStamp":J
    invoke-virtual {v8, v0, v1}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setTimeStamp(J)V

    .line 566
    const/4 v15, 0x1

    invoke-virtual {v8, v15}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setSystem(Z)V

    .line 567
    sget-object v15, Lcom/android/server/am/BroadcastQueueModernStubImpl;->BR_LIST:Ljava/util/ArrayList;

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    .end local v8    # "broadcast":Lmiui/mqsas/sdk/event/BroadcastEvent;
    :goto_3
    return-void
.end method

.method private processAbnormalBroadcast(Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;Ljava/lang/String;I)V
    .locals 3
    .param p1, "r"    # Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
    .param p2, "packageLabel"    # Ljava/lang/String;
    .param p3, "count"    # I

    .line 715
    const/4 v0, 0x0

    .line 716
    .local v0, "showDialogSuccess":Z
    sget v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->ACTIVE_ORDERED_BROADCAST_LIMIT:I

    mul-int/lit8 v1, v1, 0x3

    if-ge p3, v1, :cond_0

    .line 717
    const-string v1, "BroadcastQueueInjector"

    const-string v2, "abnormal ordered broadcast, showWarningDialog"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    :cond_0
    if-nez v0, :cond_1

    .line 739
    invoke-direct {p0, p1}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->forceStopAbnormalApp(Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;)V

    .line 740
    sget-object v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sAbnormalBroadcastWarning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 742
    :cond_1
    return-void
.end method

.method private shouldStopBroadcastDispatch(Landroid/content/pm/ResolveInfo;ZLandroid/content/Intent;)Z
    .locals 10
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;
    .param p2, "note"    # Z
    .param p3, "callee"    # Landroid/content/Intent;

    .line 414
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->processName:Ljava/lang/String;

    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/server/wm/WindowProcessUtils;->isPackageRunning(Lcom/android/server/wm/ActivityTaskManagerService;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    .line 417
    .local v0, "isRunning":Z
    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 418
    return v1

    .line 420
    :cond_0
    iget-object v2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mContext:Landroid/content/Context;

    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 421
    if-eqz p2, :cond_1

    const-string v5, "BroadcastQueueModernStubImpl#checkApplicationAutoStart#"

    goto :goto_0

    .line 422
    :cond_1
    const-string v5, "BroadcastQueueModernStubImpl#specifyBroadcast"

    .line 420
    :goto_0
    invoke-static {v2, v3, v4, v5}, Landroid/miui/AppOpsUtils;->noteApplicationAutoStart(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)I

    move-result v2

    .line 423
    .local v2, "autoStartMode":I
    iget-object v3, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-nez v3, :cond_2

    .line 424
    const-class v3, Lmiui/security/SecurityManagerInternal;

    invoke-static {v3}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/security/SecurityManagerInternal;

    iput-object v3, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    .line 426
    :cond_2
    if-eqz p3, :cond_3

    iget-object v4, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mSecurityInternal:Lmiui/security/SecurityManagerInternal;

    if-eqz v4, :cond_3

    .line 427
    const/4 v5, 0x6

    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-wide/16 v7, 0x1

    .line 429
    invoke-static {p3}, Lmiui/security/AppBehavior;->parseIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v9

    .line 427
    invoke-virtual/range {v4 .. v9}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V

    .line 431
    :cond_3
    if-nez v2, :cond_4

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :goto_1
    return v1
.end method


# virtual methods
.method public checkApplicationAutoStart(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/BroadcastRecord;Landroid/content/pm/ResolveInfo;)Z
    .locals 11
    .param p1, "bq"    # Lcom/android/server/am/BroadcastQueue;
    .param p2, "r"    # Lcom/android/server/am/BroadcastRecord;
    .param p3, "info"    # Landroid/content/pm/ResolveInfo;

    .line 339
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_0

    iget-object v0, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_0

    iget-object v0, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Lcom/miui/server/process/ProcessManagerInternal;->checkCtsProcess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    return v1

    .line 347
    :cond_0
    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    return v1

    .line 350
    :cond_1
    iget-object v0, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 351
    .local v0, "action":Ljava/lang/String;
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_2

    const-string v2, "com.google.android.c2dm.intent.RECEIVE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 352
    return v1

    .line 356
    :cond_2
    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    const-string v3, "BroadcastQueueInjector"

    const/4 v4, 0x0

    if-eqz v2, :cond_3

    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    if-eqz v2, :cond_3

    sget-object v2, Lcom/android/server/am/ActivityManagerServiceImpl;->WIDGET_PROVIDER_WHITE_LIST:Ljava/util/List;

    iget-object v5, p2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 357
    invoke-interface {v2, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 358
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ":widgetProvider"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 359
    .local v2, "widgetProcessName":Ljava/lang/String;
    iget-object v5, p2, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    iget-object v5, v5, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 360
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MIUILOG- Reject widget call from "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    return v4

    .line 366
    .end local v2    # "widgetProcessName":Ljava/lang/String;
    :cond_3
    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    if-eqz v2, :cond_5

    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    iget v2, v2, Lcom/android/server/am/ProcessRecord;->uid:I

    const/16 v5, 0x403

    if-ne v2, v5, :cond_5

    sget-object v2, Lcom/android/server/am/BroadcastQueueModernStubImpl;->ACTION_NFC:Ljava/util/Set;

    .line 367
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    .line 368
    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 369
    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/am/PendingIntentRecordImpl;->containsPendingIntent(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 370
    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/android/server/am/PendingIntentRecordImpl;->exemptTemporarily(Ljava/lang/String;Z)V

    .line 372
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MIUILOG- Allow NFC start appliction "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " action: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    return v1

    .line 377
    :cond_5
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v5

    iget-object v6, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    iget-object v7, p2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 378
    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    if-eqz v2, :cond_6

    iget-object v2, p2, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    iget-object v2, v2, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    :goto_0
    move-object v8, v2

    iget v10, p2, Lcom/android/server/am/BroadcastRecord;->userId:I

    .line 377
    move-object v9, p3

    invoke-virtual/range {v5 .. v10}, Lmiui/security/WakePathChecker;->checkBroadcastWakePath(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ResolveInfo;I)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 379
    iget-object v2, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v2, v1

    if-nez v2, :cond_8

    iget-object v2, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 380
    invoke-static {v2}, Lmiui/content/pm/PreloadedAppPolicy;->isProtectedDataApp(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    goto :goto_1

    :cond_7
    move v2, v4

    goto :goto_2

    :cond_8
    :goto_1
    move v2, v1

    .line 381
    .local v2, "isSystem":Z
    :goto_2
    const-string v5, "com.xiaomi.mipush.MESSAGE_ARRIVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 382
    .local v5, "isMessageArrived":Z
    iget-object v6, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    if-nez v6, :cond_d

    if-nez v5, :cond_9

    if-eqz v2, :cond_9

    iget-object v6, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    .line 383
    invoke-virtual {v6}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_d

    :cond_9
    if-eqz v2, :cond_a

    sget-object v6, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSystemSkipAction:Ljava/util/ArrayList;

    .line 384
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_d

    :cond_a
    iget-object v6, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    .line 385
    invoke-virtual {v6}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_b

    move v6, v1

    goto :goto_3

    :cond_b
    move v6, v4

    :goto_3
    iget-object v7, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-direct {p0, p3, v6, v7}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->shouldStopBroadcastDispatch(Landroid/content/pm/ResolveInfo;ZLandroid/content/Intent;)Z

    move-result v6

    if-nez v6, :cond_d

    .line 386
    invoke-direct {p0, v0, p3}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isInternationalSpecialAction(Ljava/lang/String;Landroid/content/pm/ResolveInfo;)Z

    move-result v6

    if-eqz v6, :cond_c

    goto :goto_4

    .line 389
    :cond_c
    const-string v1, " auto start"

    .line 391
    .end local v2    # "isSystem":Z
    .end local v5    # "isMessageArrived":Z
    .local v1, "reason":Ljava/lang/String;
    goto :goto_5

    .line 387
    .end local v1    # "reason":Ljava/lang/String;
    .restart local v2    # "isSystem":Z
    .restart local v5    # "isMessageArrived":Z
    :cond_d
    :goto_4
    return v1

    .line 392
    .end local v2    # "isSystem":Z
    .end local v5    # "isMessageArrived":Z
    :cond_e
    const-string v1, " wake path"

    .line 394
    .restart local v1    # "reason":Ljava/lang/String;
    :goto_5
    if-eqz p3, :cond_f

    .line 395
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to launch app "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " for broadcast "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ": process is not permitted to "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    :cond_f
    return v4
.end method

.method public checkReceiverAppDealBroadcast(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;Z)Z
    .locals 15
    .param p1, "bq"    # Lcom/android/server/am/BroadcastQueue;
    .param p2, "r"    # Lcom/android/server/am/BroadcastRecord;
    .param p3, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p4, "isStatic"    # Z

    .line 259
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    .line 263
    const/4 v2, 0x1

    if-eqz v1, :cond_7

    if-eqz v0, :cond_7

    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    if-nez v3, :cond_0

    move-object v5, p0

    goto/16 :goto_2

    .line 264
    :cond_0
    iget-object v3, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 265
    .local v3, "action":Ljava/lang/String;
    const-string v4, ""

    .line 266
    .local v4, "callee":Ljava/lang/String;
    const-string v5, ""

    .line 267
    .local v5, "className":Ljava/lang/String;
    iget-object v12, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    .line 268
    .local v12, "intent":Landroid/content/Intent;
    invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 269
    invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    .line 270
    invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 273
    :cond_1
    iget-object v6, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    if-eqz v6, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 274
    iget-object v6, v1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object v13, v4

    goto :goto_0

    .line 276
    :cond_2
    move-object v13, v4

    .end local v4    # "callee":Ljava/lang/String;
    .local v13, "callee":Ljava/lang/String;
    :goto_0
    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_3

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 277
    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->curReceiver:Landroid/content/pm/ActivityInfo;

    iget-object v5, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object v14, v5

    goto :goto_1

    .line 279
    :cond_3
    move-object v14, v5

    .end local v5    # "className":Ljava/lang/String;
    .local v14, "className":Ljava/lang/String;
    :goto_1
    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    invoke-static {v13, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 280
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v4

    iget-object v7, v0, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    iget v9, v0, Lcom/android/server/am/BroadcastRecord;->userId:I

    const/16 v10, 0x2000

    const/4 v11, 0x1

    move-object v5, v3

    move-object v6, v14

    move-object v8, v13

    invoke-virtual/range {v4 .. v11}, Lmiui/security/WakePathChecker;->calleeAliveMatchBlackRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 283
    const/4 v2, 0x0

    return v2

    .line 286
    :cond_4
    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    if-eqz v4, :cond_5

    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->callerApp:Lcom/android/server/am/ProcessRecord;

    iget v4, v4, Lcom/android/server/am/ProcessRecord;->uid:I

    const/16 v5, 0x403

    if-ne v4, v5, :cond_5

    sget-object v4, Lcom/android/server/am/BroadcastQueueModernStubImpl;->ACTION_NFC:Ljava/util/Set;

    .line 287
    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    .line 288
    invoke-virtual {v4}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    .line 289
    invoke-virtual {v4}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/server/am/PendingIntentRecordImpl;->containsPendingIntent(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 290
    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/android/server/am/PendingIntentRecordImpl;->exemptTemporarily(Ljava/lang/String;Z)V

    .line 291
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MIUILOG- Allow NFC start appliction "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " background start"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BroadcastQueueInjector"

    invoke-static {v5, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_5
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v4

    if-nez v4, :cond_6

    .line 294
    return v2

    .line 296
    :cond_6
    iget-object v4, v0, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    move-object v5, p0

    invoke-direct {p0, v1, v4}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->checkSpecialAction(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z

    .line 297
    return v2

    .line 263
    .end local v3    # "action":Ljava/lang/String;
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v13    # "callee":Ljava/lang/String;
    .end local v14    # "className":Ljava/lang/String;
    :cond_7
    move-object v5, p0

    :goto_2
    return v2
.end method

.method public checkReceiverIfRestricted(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;Z)Z
    .locals 8
    .param p1, "bq"    # Lcom/android/server/am/BroadcastQueue;
    .param p2, "r"    # Lcom/android/server/am/BroadcastRecord;
    .param p3, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p4, "isStatic"    # Z

    .line 904
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    if-nez v0, :cond_0

    goto :goto_0

    .line 908
    :cond_0
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v2

    iget-object v3, p2, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    iget v4, p2, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    iget-object v5, p2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    iget v6, p3, Lcom/android/server/am/ProcessRecord;->uid:I

    iget-object v7, p3, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Lcom/miui/server/greeze/GreezeManagerInternal;->isRestrictReceiver(Landroid/content/Intent;ILjava/lang/String;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 910
    const/4 v0, 0x1

    return v0

    .line 912
    :cond_1
    return v1

    .line 905
    :cond_2
    :goto_0
    return v1
.end method

.method public checkSkipReceiver(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/BroadcastProcessQueue;Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;ZLjava/lang/Object;)Z
    .locals 1
    .param p1, "bq"    # Lcom/android/server/am/BroadcastQueue;
    .param p2, "queue"    # Lcom/android/server/am/BroadcastProcessQueue;
    .param p3, "r"    # Lcom/android/server/am/BroadcastRecord;
    .param p4, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p5, "isStatic"    # Z
    .param p6, "info"    # Ljava/lang/Object;

    .line 437
    invoke-virtual {p2}, Lcom/android/server/am/BroadcastProcessQueue;->isProcessWarm()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    invoke-virtual {p0, p1, p3, p4, p5}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->checkReceiverAppDealBroadcast(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    .line 439
    :cond_0
    instance-of v0, p6, Landroid/content/pm/ResolveInfo;

    if-eqz v0, :cond_1

    .line 440
    move-object v0, p6

    check-cast v0, Landroid/content/pm/ResolveInfo;

    invoke-virtual {p0, p1, p3, v0}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->checkApplicationAutoStart(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/BroadcastRecord;Landroid/content/pm/ResolveInfo;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    .line 442
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .line 136
    iput-object p1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    .line 137
    iput-object p2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mContext:Landroid/content/Context;

    .line 138
    return-void
.end method

.method public isInDeferCachedWhiteList(Ljava/lang/String;Lcom/android/server/am/BroadcastRecord;)Z
    .locals 3
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "r"    # Lcom/android/server/am/BroadcastRecord;

    .line 916
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    if-nez p2, :cond_0

    goto :goto_1

    .line 919
    :cond_0
    sget-object v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->DEFER_CACHED_WHITE_LIST:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p2, Lcom/android/server/am/BroadcastRecord;->callingPid:I

    .line 920
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    if-eq v0, v2, :cond_1

    iget-object v0, p2, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 921
    const-string v2, "android"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    nop

    .line 919
    :goto_0
    return v1

    .line 917
    :cond_3
    :goto_1
    return v1
.end method

.method isSKipNotifySms(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;I)Z
    .locals 6
    .param p1, "r"    # Lcom/android/server/am/BroadcastRecord;
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "appOp"    # I

    .line 456
    const-string v0, "BroadcastQueueInjector"

    const/16 v1, 0x10

    const/4 v2, 0x0

    if-eq p4, v1, :cond_0

    .line 457
    return v2

    .line 459
    :cond_0
    iget-object v1, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    .line 461
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 462
    return v2

    .line 466
    :cond_1
    :try_start_0
    const-string v3, "miui.intent.SERVICE_NUMBER"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 467
    iget-object v3, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    const/16 v4, 0x2722

    invoke-virtual {v3, v4, p2, p3}, Lcom/android/server/appop/AppOpsService;->checkOperation(IILjava/lang/String;)I

    move-result v3

    .line 469
    .local v3, "mode":I
    if-eqz v3, :cond_2

    .line 470
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MIUILOG- Sms Filter packageName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " uid "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    const/4 v0, 0x1

    return v0

    .line 476
    .end local v3    # "mode":I
    :cond_2
    goto :goto_0

    .line 474
    :catch_0
    move-exception v3

    .line 475
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "isSKipNotifySms"

    invoke-static {v0, v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 477
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    return v2
.end method

.method public isSkip(Lcom/android/server/am/BroadcastRecord;Landroid/content/pm/ResolveInfo;I)Z
    .locals 2
    .param p1, "r"    # Lcom/android/server/am/BroadcastRecord;
    .param p2, "info"    # Landroid/content/pm/ResolveInfo;
    .param p3, "appOp"    # I

    .line 447
    iget-object v0, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isSKipNotifySms(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isSkip(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/BroadcastFilter;I)Z
    .locals 2
    .param p1, "r"    # Lcom/android/server/am/BroadcastRecord;
    .param p2, "filter"    # Lcom/android/server/am/BroadcastFilter;
    .param p3, "appOp"    # I

    .line 452
    iget-object v0, p2, Lcom/android/server/am/BroadcastFilter;->receiverList:Lcom/android/server/am/ReceiverList;

    iget v0, v0, Lcom/android/server/am/ReceiverList;->uid:I

    iget-object v1, p2, Lcom/android/server/am/BroadcastFilter;->packageName:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isSKipNotifySms(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method isSkipForUser(Landroid/content/pm/ResolveInfo;Z)Z
    .locals 3
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;
    .param p2, "skip"    # Z

    .line 481
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 482
    .local v0, "userId":I
    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/android/server/am/ActivityManagerService;->isUserRunning(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 483
    const/4 p2, 0x1

    .line 485
    :cond_0
    return p2
.end method

.method public noteOperationLocked(IILjava/lang/String;Landroid/os/Handler;Lcom/android/server/am/BroadcastRecord;)I
    .locals 26
    .param p1, "appOp"    # I
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "handler"    # Landroid/os/Handler;
    .param p5, "receiverRecord"    # Lcom/android/server/am/BroadcastRecord;

    .line 573
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p5

    iget-object v5, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v5, v5, Lcom/android/server/am/ActivityManagerService;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    invoke-virtual {v5, v1, v2, v3}, Lcom/android/server/appop/AppOpsService;->checkOperation(IILjava/lang/String;)I

    move-result v5

    .line 574
    .local v5, "mode":I
    const/4 v6, 0x5

    if-eq v5, v6, :cond_0

    .line 575
    return v5

    .line 578
    :cond_0
    invoke-static/range {p2 .. p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v6

    .line 579
    .local v6, "userId":I
    const/16 v7, 0x3e7

    if-ne v6, v7, :cond_1

    .line 580
    return v5

    .line 583
    :cond_1
    invoke-virtual {v0, v4, v2, v3, v1}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isSKipNotifySms(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 584
    return v5

    .line 587
    :cond_2
    iget-object v7, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v7, v7, Lcom/android/server/am/ActivityManagerService;->mAppOpsService:Lcom/android/server/appop/AppOpsService;

    const/4 v8, 0x1

    invoke-virtual {v7, v1, v2, v3, v8}, Lcom/android/server/appop/AppOpsService;->setMode(IILjava/lang/String;I)V

    .line 589
    new-instance v7, Landroid/content/Intent;

    const-string v9, "com.miui.intent.action.REQUEST_PERMISSIONS"

    invoke-direct {v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 590
    .local v7, "intent":Landroid/content/Intent;
    const-string v9, "com.lbe.security.miui"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    const/high16 v9, 0x18800000

    invoke-virtual {v7, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 593
    const-string v9, "android.intent.extra.PACKAGE_NAME"

    invoke-virtual {v7, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 594
    const-string v9, "android.intent.extra.UID"

    invoke-virtual {v7, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 595
    const-string v9, "op"

    invoke-virtual {v7, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 596
    iget-boolean v9, v4, Lcom/android/server/am/BroadcastRecord;->sticky:Z

    if-nez v9, :cond_7

    .line 597
    iget-object v9, v4, Lcom/android/server/am/BroadcastRecord;->callerPackage:Ljava/lang/String;

    .line 598
    .local v9, "callerPackage":Ljava/lang/String;
    iget v15, v4, Lcom/android/server/am/BroadcastRecord;->callingUid:I

    .line 599
    .local v15, "callingUid":I
    if-nez v9, :cond_5

    .line 600
    if-nez v15, :cond_3

    .line 601
    const-string v9, "root"

    goto :goto_0

    .line 602
    :cond_3
    const/16 v10, 0x7d0

    if-ne v15, v10, :cond_4

    .line 603
    const-string v9, "com.android.shell"

    goto :goto_0

    .line 604
    :cond_4
    const/16 v10, 0x3e8

    if-ne v15, v10, :cond_5

    .line 605
    const-string v9, "android"

    .line 608
    :cond_5
    :goto_0
    if-nez v9, :cond_6

    .line 609
    return v5

    .line 613
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->getNextRequestIdLocked()I

    move-result v23

    .line 614
    .local v23, "requestCode":I
    new-instance v10, Landroid/content/Intent;

    iget-object v11, v4, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    move-object v14, v10

    .line 615
    .local v14, "intentNew":Landroid/content/Intent;
    invoke-virtual {v14, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 616
    iget-object v10, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mAmService:Lcom/android/server/am/ActivityManagerService;

    iget-object v10, v10, Lcom/android/server/am/ActivityManagerService;->mPendingIntentController:Lcom/android/server/am/PendingIntentController;

    const/4 v11, 0x1

    const/4 v13, 0x0

    iget v12, v4, Lcom/android/server/am/BroadcastRecord;->userId:I

    const/16 v16, 0x0

    const/16 v17, 0x0

    filled-new-array {v14}, [Landroid/content/Intent;

    move-result-object v19

    iget-object v8, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mContext:Landroid/content/Context;

    .line 623
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v14, v8}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v8

    filled-new-array {v8}, [Ljava/lang/String;

    move-result-object v20

    const/high16 v21, 0x4c000000    # 3.3554432E7f

    const/16 v22, 0x0

    .line 616
    move v8, v12

    move-object v12, v9

    move-object/from16 v24, v14

    .end local v14    # "intentNew":Landroid/content/Intent;
    .local v24, "intentNew":Landroid/content/Intent;
    move v14, v15

    move/from16 v25, v15

    .end local v15    # "callingUid":I
    .local v25, "callingUid":I
    move v15, v8

    move/from16 v18, v23

    invoke-virtual/range {v10 .. v22}, Lcom/android/server/am/PendingIntentController;->getIntentSender(ILjava/lang/String;Ljava/lang/String;IILandroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;)Lcom/android/server/am/PendingIntentRecord;

    move-result-object v8

    .line 628
    .local v8, "target":Landroid/content/IIntentSender;
    new-instance v10, Landroid/content/IntentSender;

    invoke-direct {v10, v8}, Landroid/content/IntentSender;-><init>(Landroid/content/IIntentSender;)V

    const-string v11, "android.intent.extra.INTENT"

    invoke-virtual {v7, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 631
    .end local v8    # "target":Landroid/content/IIntentSender;
    .end local v9    # "callerPackage":Ljava/lang/String;
    .end local v23    # "requestCode":I
    .end local v24    # "intentNew":Landroid/content/Intent;
    .end local v25    # "callingUid":I
    :cond_7
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MIUILOG - Launching Request permission [Broadcast] uid : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  pkg : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " op : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "BroadcastQueueInjector"

    invoke-static {v9, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    const/16 v8, 0x36

    if-ne v1, v8, :cond_8

    const-wide/16 v8, 0x5dc

    goto :goto_1

    :cond_8
    const-wide/16 v8, 0xa

    .line 635
    .local v8, "delay":J
    :goto_1
    new-instance v10, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;

    invoke-direct {v10, v0, v7, v2}, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;-><init>(Lcom/android/server/am/BroadcastQueueModernStubImpl;Landroid/content/Intent;I)V

    move-object/from16 v11, p4

    invoke-virtual {v11, v10, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 646
    const/4 v10, 0x1

    return v10
.end method

.method public notifyFinishBroadcast(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;)V
    .locals 5
    .param p1, "r"    # Lcom/android/server/am/BroadcastRecord;
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 891
    iget-boolean v0, p1, Lcom/android/server/am/BroadcastRecord;->ordered:Z

    if-nez v0, :cond_0

    return-void

    .line 892
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 893
    .local v0, "action":Ljava/lang/String;
    const/4 v1, -0x1

    .line 894
    .local v1, "uid":I
    if-eqz p2, :cond_1

    .line 895
    iget v1, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    .line 897
    :cond_1
    iget-object v2, p1, Lcom/android/server/am/BroadcastRecord;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v2

    const/high16 v3, 0x10000000

    and-int/2addr v2, v3

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    move v2, v3

    .line 899
    .local v2, "isforeground":Z
    :goto_0
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerInternal;->getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->updateOrderBCStatus(Ljava/lang/String;IZZ)V

    .line 900
    return-void
.end method
