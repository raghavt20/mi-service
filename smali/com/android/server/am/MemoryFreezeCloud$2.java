class com.android.server.am.MemoryFreezeCloud$2 extends android.database.ContentObserver {
	 /* .source "MemoryFreezeCloud.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MemoryFreezeCloud;->registerMemfreezeOperation(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryFreezeCloud this$0; //synthetic
/* # direct methods */
 com.android.server.am.MemoryFreezeCloud$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryFreezeCloud; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 76 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 79 */
if ( p2 != null) { // if-eqz p2, :cond_0
	 final String v0 = "cloud_memory_freeze_policy"; // const-string v0, "cloud_memory_freeze_policy"
	 android.provider.Settings$System .getUriFor ( v0 );
	 v0 = 	 (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 80 */
		 v0 = this.this$0;
		 (( com.android.server.am.MemoryFreezeCloud ) v0 ).updateMemfreezeOperation ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryFreezeCloud;->updateMemfreezeOperation()V
		 /* .line 83 */
	 } // :cond_0
	 return;
} // .end method
