class com.android.server.am.ActivityManagerServiceImpl$2 extends java.lang.Thread {
	 /* .source "ActivityManagerServiceImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ActivityManagerServiceImpl;->dumpSystemTraces(Ljava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ActivityManagerServiceImpl this$0; //synthetic
final java.io.File val$systemTraceFile; //synthetic
final java.util.ArrayList val$tempArray; //synthetic
/* # direct methods */
 com.android.server.am.ActivityManagerServiceImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ActivityManagerServiceImpl; */
/* .line 1164 */
this.this$0 = p1;
this.val$tempArray = p2;
this.val$systemTraceFile = p3;
/* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 7 */
/* .line 1167 */
v0 = this.val$tempArray;
(( java.util.ArrayList ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 1168 */
/* .local v1, "dumpPath":Ljava/lang/String; */
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1169 */
/* .local v2, "dumpFile":Ljava/io/File; */
com.android.server.am.ActivityManagerServiceImpl .-$$Nest$sfgetrequestDumpTraceCount ( );
(( java.util.concurrent.atomic.AtomicInteger ) v3 ).getAndDecrement ( ); // invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I
/* .line 1170 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "requestDumpTraceCount delete one, now is "; // const-string v4, "requestDumpTraceCount delete one, now is "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.ActivityManagerServiceImpl .-$$Nest$sfgetrequestDumpTraceCount ( );
/* .line 1171 */
(( java.util.concurrent.atomic.AtomicInteger ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1170 */
final String v4 = "MIUIScout App"; // const-string v4, "MIUIScout App"
android.util.Slog .d ( v4,v3 );
/* .line 1173 */
try { // :try_start_0
	 v3 = 	 (( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 1174 */
		 (( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
		 /* const/16 v5, 0x180 */
		 int v6 = -1; // const/4 v6, -0x1
		 android.os.FileUtils .setPermissions ( v3,v5,v6,v6 );
		 /* .line 1176 */
		 v3 = this.val$systemTraceFile;
		 v3 = 		 android.os.FileUtils .copyFile ( v3,v2 );
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 /* .line 1177 */
			 /* new-instance v3, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v5 = "Success copying system_server trace to path"; // const-string v5, "Success copying system_server trace to path"
			 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .i ( v4,v3 );
			 /* .line 1180 */
		 } // :cond_0
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v5 = "Fail to copy system_server trace to path"; // const-string v5, "Fail to copy system_server trace to path"
		 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .w ( v4,v3 );
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 1189 */
	 } // :goto_1
	 /* .line 1184 */
} // :cond_1
/* .line 1186 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1187 */
/* .local v3, "e":Ljava/io/IOException; */
final String v5 = "Exception occurs while copying system scout trace file:"; // const-string v5, "Exception occurs while copying system scout trace file:"
android.util.Slog .w ( v4,v5,v3 );
/* .line 1190 */
} // .end local v1 # "dumpPath":Ljava/lang/String;
} // .end local v2 # "dumpFile":Ljava/io/File;
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_2
/* goto/16 :goto_0 */
/* .line 1191 */
} // :cond_2
return;
} // .end method
