public class com.android.server.am.ProcessProphetJobService extends android.app.job.JobService {
	 /* .source "ProcessProphetJobService.java" */
	 /* # static fields */
	 private static Boolean DEBUG;
	 private static final Integer JOBID;
	 private static final java.lang.String TAG;
	 private static android.content.Context sContext;
	 /* # direct methods */
	 static com.android.server.am.ProcessProphetJobService ( ) {
		 /* .locals 2 */
		 /* .line 16 */
		 /* nop */
		 /* .line 17 */
		 final String v0 = "persist.sys.procprophet.debug"; // const-string v0, "persist.sys.procprophet.debug"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.android.server.am.ProcessProphetJobService.DEBUG = (v0!= 0);
		 /* .line 19 */
		 int v0 = 0; // const/4 v0, 0x0
		 return;
	 } // .end method
	 public com.android.server.am.ProcessProphetJobService ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V */
		 return;
	 } // .end method
	 public static void schedule ( android.content.Context p0 ) {
		 /* .locals 9 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 22 */
		 final String v0 = "ProcessProphetJob"; // const-string v0, "ProcessProphetJob"
		 /* .line 23 */
		 /* new-instance v1, Landroid/app/job/JobInfo$Builder; */
		 /* new-instance v2, Landroid/content/ComponentName; */
		 /* const-class v3, Lcom/android/server/am/ProcessProphetJobService; */
		 /* invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V */
		 /* const/16 v3, 0x27fc */
		 /* invoke-direct {v1, v3, v2}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
		 /* .line 26 */
		 /* .local v1, "builder":Landroid/app/job/JobInfo$Builder; */
		 /* const/16 v2, 0xc */
		 /* .line 27 */
		 /* .local v2, "cycle":I */
		 /* sget-boolean v3, Lcom/android/server/am/ProcessProphetJobService;->DEBUG:Z */
		 int v4 = 1; // const/4 v4, 0x1
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 /* .line 28 */
			 /* int-to-long v5, v2 */
			 /* const-wide/32 v7, 0xea60 */
			 /* mul-long/2addr v5, v7 */
			 (( android.app.job.JobInfo$Builder ) v1 ).setMinimumLatency ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;
			 /* .line 30 */
		 } // :cond_0
		 /* int-to-long v5, v2 */
		 /* const-wide/32 v7, 0x36ee80 */
		 /* mul-long/2addr v5, v7 */
		 (( android.app.job.JobInfo$Builder ) v1 ).setMinimumLatency ( v5, v6 ); // invoke-virtual {v1, v5, v6}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;
		 /* .line 31 */
		 (( android.app.job.JobInfo$Builder ) v1 ).setRequiresDeviceIdle ( v4 ); // invoke-virtual {v1, v4}, Landroid/app/job/JobInfo$Builder;->setRequiresDeviceIdle(Z)Landroid/app/job/JobInfo$Builder;
		 /* .line 32 */
		 (( android.app.job.JobInfo$Builder ) v1 ).setRequiresBatteryNotLow ( v4 ); // invoke-virtual {v1, v4}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;
		 /* .line 34 */
	 } // :goto_0
	 final String v3 = "jobscheduler"; // const-string v3, "jobscheduler"
	 (( android.content.Context ) p0 ).getSystemService ( v3 ); // invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v3, Landroid/app/job/JobScheduler; */
	 /* .line 36 */
	 /* .local v3, "js":Landroid/app/job/JobScheduler; */
	 try { // :try_start_0
		 (( android.app.job.JobInfo$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
		 v5 = 		 (( android.app.job.JobScheduler ) v3 ).schedule ( v5 ); // invoke-virtual {v3, v5}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
		 /* if-ne v5, v4, :cond_2 */
		 /* .line 37 */
		 /* sget-boolean v4, Lcom/android/server/am/ProcessProphetJobService;->DEBUG:Z */
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 final String v5 = "Job has been built & will start after "; // const-string v5, "Job has been built & will start after "
		 if ( v4 != null) { // if-eqz v4, :cond_1
			 /* .line 38 */
			 try { // :try_start_1
				 /* new-instance v4, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
				 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 final String v5 = " minutes."; // const-string v5, " minutes."
				 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .i ( v0,v4 );
				 /* .line 40 */
			 } // :cond_1
			 /* new-instance v4, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
			 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 final String v5 = " hours."; // const-string v5, " hours."
			 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .i ( v0,v4 );
			 /* .line 43 */
		 } // :cond_2
		 final String v4 = "Fail to schedule."; // const-string v4, "Fail to schedule."
		 android.util.Slog .e ( v0,v4 );
		 /* :try_end_1 */
		 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .line 47 */
	 } // :goto_1
	 /* .line 45 */
	 /* :catch_0 */
	 /* move-exception v4 */
	 /* .line 46 */
	 /* .local v4, "e":Ljava/lang/Exception; */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "Fail to schedule by: "; // const-string v6, "Fail to schedule by: "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v0,v5 );
	 /* .line 48 */
} // .end local v4 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
/* # virtual methods */
public void onDestroy ( ) {
/* .locals 2 */
/* .line 65 */
/* invoke-super {p0}, Landroid/app/job/JobService;->onDestroy()V */
/* .line 66 */
v0 = com.android.server.am.ProcessProphetJobService.sContext;
com.android.server.am.ProcessProphetJobService .schedule ( v0 );
/* .line 67 */
final String v0 = "ProcessProphetJob"; // const-string v0, "ProcessProphetJob"
final String v1 = "Job has been destroyed & rescheduled."; // const-string v1, "Job has been destroyed & rescheduled."
android.util.Slog .i ( v0,v1 );
/* .line 68 */
return;
} // .end method
public Boolean onStartJob ( android.app.job.JobParameters p0 ) {
/* .locals 2 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 52 */
final String v0 = "ProcessProphetJob"; // const-string v0, "ProcessProphetJob"
final String v1 = "Job executed."; // const-string v1, "Job executed."
android.util.Slog .i ( v0,v1 );
/* .line 53 */
com.android.server.am.ProcessProphetStub .getInstance ( );
/* .line 54 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean onStopJob ( android.app.job.JobParameters p0 ) {
/* .locals 2 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 59 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Stop job, reason: "; // const-string v1, "Stop job, reason: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( android.app.job.JobParameters ) p1 ).getStopReason ( ); // invoke-virtual {p1}, Landroid/app/job/JobParameters;->getStopReason()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphetJob"; // const-string v1, "ProcessProphetJob"
android.util.Slog .w ( v1,v0 );
/* .line 60 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
