.class public Lcom/android/server/am/ProcessSceneCleaner;
.super Lcom/android/server/am/ProcessCleanerBase;
.source "ProcessSceneCleaner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcessSceneCleaner$H;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ProcessSceneCleaner"


# instance fields
.field private mAMS:Lcom/android/server/am/ActivityManagerService;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

.field private mPMS:Lcom/android/server/am/ProcessManagerService;

.field private mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

.field private mSysPressureCtrl:Lcom/android/server/am/SystemPressureController;


# direct methods
.method static bridge synthetic -$$Nest$mhandleKillAll(Lcom/android/server/am/ProcessSceneCleaner;Lmiui/process/ProcessConfig;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessSceneCleaner;->handleKillAll(Lmiui/process/ProcessConfig;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleKillAny(Lcom/android/server/am/ProcessSceneCleaner;Lmiui/process/ProcessConfig;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessSceneCleaner;->handleKillAny(Lmiui/process/ProcessConfig;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleSwipeKill(Lcom/android/server/am/ProcessSceneCleaner;Lmiui/process/ProcessConfig;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessSceneCleaner;->handleSwipeKill(Lmiui/process/ProcessConfig;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Lcom/android/server/am/ActivityManagerService;)V
    .locals 0
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 41
    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;-><init>(Lcom/android/server/am/ActivityManagerService;)V

    .line 42
    iput-object p1, p0, Lcom/android/server/am/ProcessSceneCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    .line 43
    return-void
.end method

.method private getKillPackageList(Landroid/util/ArrayMap;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 269
    .local p1, "killingPackageMaps":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 270
    .local v0, "killPackage":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/util/ArrayMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 271
    invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 272
    .local v2, "killLevel":I
    invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 273
    .local v3, "killingPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 274
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 275
    .local v5, "pkg":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    .end local v5    # "pkg":Ljava/lang/String;
    goto :goto_1

    .line 270
    .end local v2    # "killLevel":I
    .end local v3    # "killingPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 279
    .end local v1    # "i":I
    :cond_1
    return-object v0
.end method

.method private handleKillAll(Lmiui/process/ProcessConfig;)V
    .locals 21
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 128
    move-object/from16 v9, p0

    move-object/from16 v10, p1

    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    if-nez v0, :cond_0

    .line 129
    return-void

    .line 131
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v11

    .line 132
    .local v11, "policy":I
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v12

    .line 133
    .local v12, "reason":Ljava/lang/String;
    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mAMS:Lcom/android/server/am/ActivityManagerService;

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v0}, Lcom/android/server/wm/WindowProcessUtils;->getPerceptibleRecentAppList(Lcom/android/server/wm/ActivityTaskManagerService;)Ljava/util/Map;

    move-result-object v13

    .line 135
    .local v13, "fgTaskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v9, v10, v0}, Lcom/android/server/am/ProcessSceneCleaner;->getProcessPolicyWhiteList(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;)Ljava/util/List;

    move-result-object v14

    .line 136
    .local v14, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v13, :cond_1

    if-eqz v14, :cond_1

    .line 137
    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v14, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 139
    :cond_1
    const/4 v0, 0x2

    if-ne v11, v0, :cond_2

    .line 140
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    iget-object v1, v9, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v9, v0, v1}, Lcom/android/server/am/ProcessSceneCleaner;->removeAllTasks(ILcom/android/server/am/ProcessManagerService;)V

    goto :goto_0

    .line 142
    :cond_2
    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v9, v10, v0, v14, v13}, Lcom/android/server/am/ProcessSceneCleaner;->removeTasksIfNeeded(Lmiui/process/ProcessConfig;Lcom/android/server/am/ProcessPolicy;Ljava/util/List;Ljava/util/Map;)V

    .line 144
    :goto_0
    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getLruProcesses()Ljava/util/ArrayList;

    move-result-object v15

    .line 145
    .local v15, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 146
    .local v8, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    const/4 v0, 0x1

    .line 147
    .local v0, "isForceStop":Z
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v17

    .line 148
    .local v17, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 150
    invoke-static {}, Lcom/android/server/wm/MiuiSoScManagerStub;->get()Lcom/android/server/wm/MiuiSoScManagerStub;

    move-result-object v1

    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiSoScManagerStub;->isInSoScSingleMode(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 151
    goto :goto_1

    .line 153
    :cond_3
    iget-object v1, v9, Lcom/android/server/am/ProcessSceneCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessPolicy;->getOneKeyCleanWhiteList()Ljava/util/HashMap;

    move-result-object v1

    .line 154
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 153
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 155
    const/4 v0, 0x0

    .line 156
    iget-object v1, v9, Lcom/android/server/am/ProcessSceneCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessPolicy;->getOneKeyCleanWhiteList()Ljava/util/HashMap;

    move-result-object v1

    .line 157
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 157
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 158
    goto :goto_1

    .line 157
    :cond_4
    move/from16 v18, v0

    goto :goto_2

    .line 162
    :cond_5
    move/from16 v18, v0

    .end local v0    # "isForceStop":Z
    .local v18, "isForceStop":Z
    :goto_2
    invoke-virtual {v9, v8}, Lcom/android/server/am/ProcessSceneCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 163
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v14, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 164
    iget-object v5, v9, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    const-string v6, "ProcessSceneCleaner"

    iget-object v7, v9, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    iget-object v4, v9, Lcom/android/server/am/ProcessSceneCleaner;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move v2, v11

    move-object v3, v12

    move-object/from16 v19, v4

    move/from16 v4, v18

    move-object/from16 v20, v8

    .end local v8    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .local v20, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    move-object/from16 v8, v19

    invoke-virtual/range {v0 .. v8}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V

    goto :goto_3

    .line 163
    .end local v20    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v8    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_6
    move-object/from16 v20, v8

    .end local v8    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v20    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    goto :goto_3

    .line 162
    .end local v20    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v8    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_7
    move-object/from16 v20, v8

    .line 166
    .end local v8    # "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v17    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v18    # "isForceStop":Z
    :goto_3
    goto/16 :goto_1

    .line 167
    :cond_8
    return-void
.end method

.method private handleKillAny(Lmiui/process/ProcessConfig;)V
    .locals 22
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 234
    move-object/from16 v9, p0

    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    if-nez v0, :cond_0

    .line 235
    return-void

    .line 237
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->isUserIdInvalid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "userId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessSceneCleaner"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    return-void

    .line 241
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v10

    .line 242
    .local v10, "policy":I
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getKillingPackageMaps()Landroid/util/ArrayMap;

    move-result-object v11

    .line 245
    .local v11, "killingPackageMaps":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;"
    move-object/from16 v12, p1

    invoke-direct {v9, v11, v12}, Lcom/android/server/am/ProcessSceneCleaner;->removeTasksByPackages(Landroid/util/ArrayMap;Lmiui/process/ProcessConfig;)V

    .line 247
    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mSmartPowerService:Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getLruProcesses()Ljava/util/ArrayList;

    move-result-object v13

    .line 248
    .local v13, "runningAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/smartpower/IAppState$IRunningProcess;>;"
    invoke-direct {v9, v11}, Lcom/android/server/am/ProcessSceneCleaner;->getKillPackageList(Landroid/util/ArrayMap;)Ljava/util/Map;

    move-result-object v14

    .line 249
    .local v14, "killPackages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v14}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v15

    .line 250
    .local v15, "killPkg":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    .line 251
    .local v8, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessRecord()Lcom/android/server/am/ProcessRecord;

    move-result-object v17

    .line 252
    .local v17, "app":Lcom/android/server/am/ProcessRecord;
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 253
    .local v7, "pkgName":Ljava/lang/String;
    invoke-interface {v15, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 254
    invoke-virtual {v8}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getAdj()I

    move-result v0

    if-lez v0, :cond_4

    .line 255
    invoke-virtual {v9, v8}, Lcom/android/server/am/ProcessSceneCleaner;->isCurrentProcessInBackup(Lcom/miui/server/smartpower/IAppState$IRunningProcess;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 256
    invoke-interface {v14, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 257
    .local v6, "killLevel":I
    const/16 v0, 0x64

    if-eq v6, v0, :cond_2

    .line 258
    invoke-virtual {v9, v10}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, v9, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    iget-object v5, v9, Lcom/android/server/am/ProcessSceneCleaner;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;ILandroid/os/Handler;Landroid/content/Context;)V

    goto :goto_1

    .line 260
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v2

    invoke-virtual {v9, v10}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    iget-object v5, v9, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    const-string v18, "ProcessSceneCleaner"

    iget-object v1, v9, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v20, v1

    move-object/from16 v1, v17

    move/from16 v21, v6

    .end local v6    # "killLevel":I
    .local v21, "killLevel":I
    move-object/from16 v6, v18

    move-object/from16 v18, v7

    .end local v7    # "pkgName":Ljava/lang/String;
    .local v18, "pkgName":Ljava/lang/String;
    move-object/from16 v7, v20

    move-object/from16 v20, v8

    .end local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .local v20, "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    move-object/from16 v8, v19

    invoke-virtual/range {v0 .. v8}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V

    goto :goto_1

    .line 255
    .end local v18    # "pkgName":Ljava/lang/String;
    .end local v20    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v21    # "killLevel":I
    .restart local v7    # "pkgName":Ljava/lang/String;
    .restart local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_3
    move-object/from16 v18, v7

    move-object/from16 v20, v8

    .end local v7    # "pkgName":Ljava/lang/String;
    .end local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v18    # "pkgName":Ljava/lang/String;
    .restart local v20    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    goto :goto_1

    .line 254
    .end local v18    # "pkgName":Ljava/lang/String;
    .end local v20    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v7    # "pkgName":Ljava/lang/String;
    .restart local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_4
    move-object/from16 v18, v7

    move-object/from16 v20, v8

    .end local v7    # "pkgName":Ljava/lang/String;
    .end local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v18    # "pkgName":Ljava/lang/String;
    .restart local v20    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    goto :goto_1

    .line 253
    .end local v18    # "pkgName":Ljava/lang/String;
    .end local v20    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .restart local v7    # "pkgName":Ljava/lang/String;
    .restart local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    :cond_5
    move-object/from16 v18, v7

    move-object/from16 v20, v8

    .line 264
    .end local v7    # "pkgName":Ljava/lang/String;
    .end local v8    # "runningApp":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .end local v17    # "app":Lcom/android/server/am/ProcessRecord;
    :goto_1
    goto :goto_0

    .line 265
    :cond_6
    return-void
.end method

.method private handleSwipeKill(Lmiui/process/ProcessConfig;)Z
    .locals 18
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 170
    move-object/from16 v9, p0

    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->isUserIdInvalid()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_9

    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->isTaskIdInvalid()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object/from16 v14, p1

    goto/16 :goto_3

    .line 175
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getKillingPackage()Ljava/lang/String;

    move-result-object v10

    .line 176
    .local v10, "packageName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getTaskId()I

    move-result v11

    .line 177
    .local v11, "taskId":I
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v12

    .line 180
    .local v12, "killReason":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->isRemoveTaskNeeded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getTaskId()I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/android/server/am/ProcessSceneCleaner;->removeTaskIfNeeded(I)V

    .line 184
    :cond_1
    iget-object v0, v9, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getUserId()I

    move-result v2

    invoke-virtual {v0, v10, v2}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordList(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v13

    .line 185
    .local v13, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    if-eqz v13, :cond_8

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object/from16 v14, p1

    goto :goto_2

    .line 188
    :cond_2
    invoke-direct {v9, v13, v11}, Lcom/android/server/am/ProcessSceneCleaner;->isAppHasOtherTask(Ljava/util/List;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189
    move-object/from16 v14, p1

    invoke-direct {v9, v11, v14}, Lcom/android/server/am/ProcessSceneCleaner;->killAppForHasOtherTask(ILmiui/process/ProcessConfig;)Z

    move-result v0

    return v0

    .line 191
    :cond_3
    move-object/from16 v14, p1

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/android/server/am/ProcessRecord;

    .line 192
    .local v8, "proc":Lcom/android/server/am/ProcessRecord;
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_4

    iget-object v0, v8, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_4
    iget-object v0, v8, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v8, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 193
    invoke-virtual {v9, v0, v1}, Lcom/android/server/am/ProcessSceneCleaner;->isCurrentProcessInBackup(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 194
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v2

    const/4 v4, 0x1

    iget-object v5, v9, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    const-string v6, "ProcessSceneCleaner"

    iget-object v7, v9, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    iget-object v3, v9, Lcom/android/server/am/ProcessSceneCleaner;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object v1, v8

    move-object/from16 v16, v3

    move-object v3, v12

    move-object/from16 v17, v8

    .end local v8    # "proc":Lcom/android/server/am/ProcessRecord;
    .local v17, "proc":Lcom/android/server/am/ProcessRecord;
    move-object/from16 v8, v16

    invoke-virtual/range {v0 .. v8}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V

    goto :goto_1

    .line 193
    .end local v17    # "proc":Lcom/android/server/am/ProcessRecord;
    .restart local v8    # "proc":Lcom/android/server/am/ProcessRecord;
    :cond_5
    move-object/from16 v17, v8

    .line 196
    .end local v8    # "proc":Lcom/android/server/am/ProcessRecord;
    :cond_6
    :goto_1
    goto :goto_0

    .line 197
    :cond_7
    const/4 v0, 0x1

    return v0

    .line 185
    :cond_8
    move-object/from16 v14, p1

    .line 186
    :goto_2
    return v1

    .line 170
    .end local v10    # "packageName":Ljava/lang/String;
    .end local v11    # "taskId":I
    .end local v12    # "killReason":Ljava/lang/String;
    .end local v13    # "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    :cond_9
    move-object/from16 v14, p1

    .line 171
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userId:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getUserId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " or taskId:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 172
    invoke-virtual/range {p1 .. p1}, Lmiui/process/ProcessConfig;->getTaskId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is invalid"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 171
    const-string v2, "ProcessSceneCleaner"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    return v1
.end method

.method private isAppHasOtherTask(Ljava/util/List;I)Z
    .locals 4
    .param p2, "taskId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/server/am/ProcessRecord;",
            ">;I)Z"
        }
    .end annotation

    .line 201
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
    const/4 v0, 0x0

    .line 202
    .local v0, "appHasOtherTask":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessRecord;

    .line 203
    .local v2, "proc":Lcom/android/server/am/ProcessRecord;
    nop

    .line 204
    invoke-virtual {v2}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;

    move-result-object v3

    .line 203
    invoke-static {v3, p2}, Lcom/android/server/wm/WindowProcessUtils;->isProcessHasActivityInOtherTaskLocked(Lcom/android/server/wm/WindowProcessController;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 205
    const/4 v0, 0x1

    .line 207
    .end local v2    # "proc":Lcom/android/server/am/ProcessRecord;
    :cond_0
    goto :goto_0

    .line 208
    :cond_1
    return v0
.end method

.method private killAppForHasOtherTask(ILmiui/process/ProcessConfig;)Z
    .locals 12
    .param p1, "taskId"    # I
    .param p2, "config"    # Lmiui/process/ProcessConfig;

    .line 212
    const/4 v0, 0x0

    .line 213
    .local v0, "taskTopApp":Lcom/android/server/am/ProcessRecord;
    invoke-static {p1}, Lcom/android/server/wm/WindowProcessUtils;->getTaskTopApp(I)Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 214
    .local v1, "wpc":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_0

    .line 215
    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mOwner:Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, Lcom/android/server/am/ProcessRecord;

    .line 217
    :cond_0
    if-eqz v0, :cond_2

    .line 219
    nop

    .line 221
    invoke-virtual {v0}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;

    move-result-object v2

    .line 220
    invoke-static {v2, p1}, Lcom/android/server/wm/WindowProcessUtils;->isProcessHasActivityInOtherTaskLocked(Lcom/android/server/wm/WindowProcessController;I)Z

    move-result v11

    .line 223
    .local v11, "processHasOtherTask":Z
    if-nez v11, :cond_2

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->mServices:Lcom/android/server/am/ProcessServiceRecord;

    .line 224
    invoke-virtual {v2}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-object v2, v0, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v3, v0, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 225
    invoke-virtual {p0, v2, v3}, Lcom/android/server/am/ProcessSceneCleaner;->isCurrentProcessInBackup(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 226
    invoke-virtual {p2}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v4

    invoke-virtual {p2}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/server/am/ProcessSceneCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    const-string v8, "ProcessSceneCleaner"

    iget-object v9, p0, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    iget-object v10, p0, Lcom/android/server/am/ProcessSceneCleaner;->mContext:Landroid/content/Context;

    move-object v2, p0

    move-object v3, v0

    invoke-virtual/range {v2 .. v10}, Lcom/android/server/am/ProcessSceneCleaner;->killOnce(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;ZLcom/android/server/am/ProcessManagerService;Ljava/lang/String;Landroid/os/Handler;Landroid/content/Context;)V

    .line 230
    .end local v11    # "processHasOtherTask":Z
    :cond_2
    const/4 v2, 0x1

    return v2
.end method

.method private removeTasksByPackages(Landroid/util/ArrayMap;Lmiui/process/ProcessConfig;)V
    .locals 5
    .param p2, "config"    # Lmiui/process/ProcessConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;",
            "Lmiui/process/ProcessConfig;",
            ")V"
        }
    .end annotation

    .line 284
    .local p1, "packageMaps":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {p2}, Lmiui/process/ProcessConfig;->isRemoveTaskNeeded()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 286
    .local v0, "removedTasksInPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/util/ArrayMap;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 287
    invoke-virtual {p1, v1}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 288
    .local v2, "killLevel":I
    const/16 v3, 0x65

    if-eq v2, v3, :cond_0

    .line 289
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 290
    .local v3, "killedPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 291
    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 286
    .end local v2    # "killLevel":I
    .end local v3    # "killedPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 296
    .end local v1    # "i":I
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 297
    .local v1, "pkgIterator":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 298
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 299
    .local v2, "pkg":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {p0, v2, v3}, Lcom/android/server/am/ProcessSceneCleaner;->isTrimMemoryEnable(Ljava/lang/String;Lcom/android/server/am/ProcessManagerService;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 300
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 302
    .end local v2    # "pkg":Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 303
    :cond_3
    invoke-virtual {p2}, Lmiui/process/ProcessConfig;->getUserId()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/am/ProcessSceneCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/server/am/ProcessSceneCleaner;->removeTasksInPackages(Ljava/util/List;ILcom/android/server/am/ProcessPolicy;)V

    .line 305
    .end local v0    # "removedTasksInPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "pkgIterator":Ljava/util/Iterator;
    :cond_4
    return-void
.end method


# virtual methods
.method createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;
    .locals 1
    .param p1, "event"    # I
    .param p2, "config"    # Lmiui/process/ProcessConfig;
    .param p3, "handler"    # Landroid/os/Handler;

    .line 308
    invoke-virtual {p3, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 309
    .local v0, "msg":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 310
    return-object v0
.end method

.method public sceneKillProcess(Lmiui/process/ProcessConfig;)Z
    .locals 3
    .param p1, "config"    # Lmiui/process/ProcessConfig;

    .line 55
    iget-object v0, p0, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    if-nez p1, :cond_0

    goto :goto_1

    .line 58
    :cond_0
    const/4 v0, 0x0

    .line 59
    .local v0, "success":Z
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 68
    :pswitch_1
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getKillingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    return v1

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    const/4 v2, 0x2

    invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessSceneCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessSceneCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 72
    const/4 v0, 0x1

    .line 73
    goto :goto_0

    .line 77
    :pswitch_2
    invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getKillingPackageMaps()Landroid/util/ArrayMap;

    move-result-object v2

    if-nez v2, :cond_2

    .line 78
    return v1

    .line 80
    :cond_2
    iget-object v1, p0, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    const/4 v2, 0x3

    invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessSceneCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessSceneCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 81
    const/4 v0, 0x1

    .line 82
    goto :goto_0

    .line 64
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    const/4 v2, 0x1

    invoke-virtual {p0, v2, p1, v1}, Lcom/android/server/am/ProcessSceneCleaner;->createMessage(ILmiui/process/ProcessConfig;Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessSceneCleaner$H;->sendMessage(Landroid/os/Message;)Z

    .line 65
    const/4 v0, 0x1

    .line 66
    nop

    .line 86
    :goto_0
    return v0

    .line 56
    .end local v0    # "success":Z
    :cond_3
    :goto_1
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public systemReady(Lcom/android/server/am/ProcessManagerService;Landroid/os/Looper;Landroid/content/Context;)V
    .locals 1
    .param p1, "pms"    # Lcom/android/server/am/ProcessManagerService;
    .param p2, "myLooper"    # Landroid/os/Looper;
    .param p3, "context"    # Landroid/content/Context;

    .line 46
    invoke-super {p0, p3, p1}, Lcom/android/server/am/ProcessCleanerBase;->systemReady(Landroid/content/Context;Lcom/android/server/am/ProcessManagerService;)V

    .line 47
    iput-object p3, p0, Lcom/android/server/am/ProcessSceneCleaner;->mContext:Landroid/content/Context;

    .line 48
    iput-object p1, p0, Lcom/android/server/am/ProcessSceneCleaner;->mPMS:Lcom/android/server/am/ProcessManagerService;

    .line 49
    invoke-virtual {p1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessSceneCleaner;->mProcessPolicy:Lcom/android/server/am/ProcessPolicy;

    .line 50
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessSceneCleaner;->mSysPressureCtrl:Lcom/android/server/am/SystemPressureController;

    .line 51
    new-instance v0, Lcom/android/server/am/ProcessSceneCleaner$H;

    invoke-direct {v0, p0, p2}, Lcom/android/server/am/ProcessSceneCleaner$H;-><init>(Lcom/android/server/am/ProcessSceneCleaner;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessSceneCleaner;->mHandler:Lcom/android/server/am/ProcessSceneCleaner$H;

    .line 52
    return-void
.end method
