.class final Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;
.super Landroid/os/Handler;
.source "MimdManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MimdManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MimdManagerHandler"
.end annotation


# instance fields
.field final MSG_APP_DIED:I

.field final MSG_APP_FOREGROUND_CHANGE:I

.field final MSG_MIMD_MANAGER_INIT:I

.field final MSG_SLEEP_MODE_ENTRY:I

.field final MSG_START_PROCESS:I

.field final synthetic this$0:Lcom/android/server/am/MimdManagerServiceImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/am/MimdManagerServiceImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 142
    iput-object p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    .line 143
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 136
    const/4 p1, 0x0

    iput p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->MSG_MIMD_MANAGER_INIT:I

    .line 137
    const/4 p1, 0x1

    iput p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->MSG_START_PROCESS:I

    .line 138
    const/4 p1, 0x2

    iput p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->MSG_APP_FOREGROUND_CHANGE:I

    .line 139
    const/4 p1, 0x3

    iput p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->MSG_APP_DIED:I

    .line 140
    const/4 p1, 0x4

    iput p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->MSG_SLEEP_MODE_ENTRY:I

    .line 144
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/MimdManagerServiceImpl;Landroid/os/Looper;Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 148
    iget v0, p1, Landroid/os/Message;->what:I

    .line 149
    .local v0, "what":I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;

    .line 150
    .local v1, "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;
    packed-switch v0, :pswitch_data_0

    .line 167
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mimd manager service received wrong handler message:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MimdManagerServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 164
    :pswitch_0
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v2}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$mentrySleepMode(Lcom/android/server/am/MimdManagerServiceImpl;)V

    .line 165
    goto :goto_0

    .line 161
    :pswitch_1
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    iget v3, v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I

    iget v4, v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I

    invoke-static {v2, v3, v4}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$mappDied(Lcom/android/server/am/MimdManagerServiceImpl;II)V

    .line 162
    goto :goto_0

    .line 158
    :pswitch_2
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    iget v3, v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I

    iget v4, v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I

    iget-boolean v5, v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->foreground:Z

    invoke-static {v2, v3, v4, v5}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$mforegroundChanged(Lcom/android/server/am/MimdManagerServiceImpl;IIZ)V

    .line 159
    goto :goto_0

    .line 155
    :pswitch_3
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    iget v3, v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I

    iget v4, v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I

    iget-object v5, v1, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$monAppProcessStart(Lcom/android/server/am/MimdManagerServiceImpl;IILjava/lang/String;)V

    .line 156
    goto :goto_0

    .line 152
    :pswitch_4
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v2}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$mmimdManagerInit(Lcom/android/server/am/MimdManagerServiceImpl;)V

    .line 153
    nop

    .line 170
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
