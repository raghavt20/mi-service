.class public Lcom/android/server/am/MimdManagerServiceImpl;
.super Ljava/lang/Object;
.source "MimdManagerServiceImpl.java"

# interfaces
.implements Lcom/android/server/am/MimdManagerServiceStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;,
        Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;,
        Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
    }
.end annotation


# static fields
.field private static final ACTION_CODE_SHIFT:J = 0x3e8L

.field private static final ACTION_CODE_SLEEP:J = 0x1L

.field public static final ACTION_SLEEP_CHANGED:Ljava/lang/String; = "com.miui.powerkeeper_sleep_changed"

.field private static final DBG_MIMD:Z

.field public static final EXTRA_STATE:Ljava/lang/String; = "state"

.field private static final MEMCG_PROCS_NODE:Ljava/lang/String; = "cgroup.procs"

.field private static final POLICY_TYPE_APPBG:J = 0x3L

.field private static final POLICY_TYPE_APPDIE:J = 0x4L

.field private static final POLICY_TYPE_APPFG:J = 0x2L

.field private static final POLICY_TYPE_MISC:J = 0x1L

.field private static final POLICY_TYPE_NONE:J = 0x0L

.field public static final STATE_ENTER_SLEEP:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MimdManagerServiceImpl"

.field private static final UID_CODE_SHIFT:J = 0x186a0L

.field private static final UID_PREFIX:Ljava/lang/String; = "uid_"

.field private static mAppMemCgroupPath:Ljava/lang/String;

.field private static mPolicyControlMaskLocal:I

.field private static sEnableMimdService:Z


# instance fields
.field private final MIMD_CONFIG_PATH:Ljava/lang/String;

.field private final MIMD_HWSERVICE_TRIGGER_PATH:Ljava/lang/String;

.field private appIndexMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;",
            ">;"
        }
    .end annotation
.end field

.field filter:Landroid/content/IntentFilter;

.field private mActivityManagerService:Landroid/app/IActivityManager;

.field private mContext:Landroid/content/Context;

.field private final mDieLock:Ljava/lang/Object;

.field private final mFgLock:Ljava/lang/Object;

.field public mHandlerThread:Landroid/os/HandlerThread;

.field private mMimdManagerHandler:Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

.field private final mPolicyControlLock:Ljava/lang/Object;

.field private final mProcessObserver:Landroid/app/IProcessObserver$Stub;

.field private mSleepModeReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mMimdManagerHandler:Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mappDied(Lcom/android/server/am/MimdManagerServiceImpl;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->appDied(II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mentrySleepMode(Lcom/android/server/am/MimdManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->entrySleepMode()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mforegroundChanged(Lcom/android/server/am/MimdManagerServiceImpl;IIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/MimdManagerServiceImpl;->foregroundChanged(IIZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mmimdManagerInit(Lcom/android/server/am/MimdManagerServiceImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->mimdManagerInit()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monAppProcessStart(Lcom/android/server/am/MimdManagerServiceImpl;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/MimdManagerServiceImpl;->onAppProcessStart(IILjava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 50
    const-string v0, "/dev/memcg/mimd"

    sput-object v0, Lcom/android/server/am/MimdManagerServiceImpl;->mAppMemCgroupPath:Ljava/lang/String;

    .line 55
    const-string v0, "MimdManagerServiceImpl"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    .line 67
    const/4 v0, 0x0

    sput v0, Lcom/android/server/am/MimdManagerServiceImpl;->mPolicyControlMaskLocal:I

    .line 73
    sput-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->sEnableMimdService:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-string v0, "/sys/module/perf_helper/mimd/mimdtrigger"

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->MIMD_HWSERVICE_TRIGGER_PATH:Ljava/lang/String;

    .line 54
    const-string v0, "/odm/etc/mimdconfig"

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->MIMD_CONFIG_PATH:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mPolicyControlLock:Ljava/lang/Object;

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mFgLock:Ljava/lang/Object;

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mDieLock:Ljava/lang/Object;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mMimdManagerHandler:Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    .line 72
    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    .line 198
    new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/am/MimdManagerServiceImpl$1;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mSleepModeReceiver:Landroid/content/BroadcastReceiver;

    .line 318
    new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/am/MimdManagerServiceImpl$2;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mProcessObserver:Landroid/app/IProcessObserver$Stub;

    return-void
.end method

.method private ParseMimdconfigAppmap()Z
    .locals 23

    .line 422
    move-object/from16 v1, p0

    const-string v0, "index:"

    const-string v2, "app:"

    const-string v3, "read app map failed"

    new-instance v4, Ljava/io/File;

    const-string v5, "/odm/etc/mimdconfig"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 423
    .local v4, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 424
    .local v5, "ret":Z
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    const-string v7, " "

    const-string v8, "MimdManagerServiceImpl"

    if-eqz v6, :cond_a

    .line 425
    const/4 v6, 0x0

    .line 427
    .local v6, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v9, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    invoke-direct {v10, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v9, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v6, v9

    .line 428
    const/4 v9, 0x0

    .line 429
    .local v9, "tmpString":Ljava/lang/String;
    const/4 v10, 0x0

    .line 430
    .local v10, "appMapFlag":Z
    :goto_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-object v9, v11

    if-eqz v11, :cond_8

    .line 431
    const/4 v11, 0x0

    if-nez v10, :cond_1

    .line 432
    :try_start_1
    const-string v12, "[\\s]+"

    invoke-virtual {v9, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 433
    .local v12, "words":[Ljava/lang/String;
    aget-object v11, v12, v11

    const-string v13, "app_map"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 434
    const/4 v10, 0x1

    goto :goto_0

    .line 433
    :cond_0
    move-object/from16 v18, v0

    move-object/from16 v17, v2

    move-object/from16 v19, v4

    move/from16 v20, v5

    goto/16 :goto_2

    .line 468
    .end local v9    # "tmpString":Ljava/lang/String;
    .end local v10    # "appMapFlag":Z
    .end local v12    # "words":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    move-object v2, v0

    move-object/from16 v19, v4

    move/from16 v20, v5

    goto/16 :goto_7

    .line 465
    :catch_0
    move-exception v0

    move-object/from16 v19, v4

    move/from16 v20, v5

    goto/16 :goto_6

    .line 438
    .restart local v9    # "tmpString":Ljava/lang/String;
    .restart local v10    # "appMapFlag":Z
    :cond_1
    const/4 v12, 0x1

    if-ne v10, v12, :cond_2

    const-string/jumbo v13, "}"

    invoke-virtual {v9, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ltz v13, :cond_2

    .line 439
    move-object/from16 v19, v4

    move/from16 v20, v5

    goto/16 :goto_3

    .line 441
    :cond_2
    :try_start_2
    invoke-virtual {v9, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    if-ltz v13, :cond_7

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    if-ltz v13, :cond_7

    .line 442
    const-string v13, "\\s+"

    invoke-static {v13}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v13

    .line 443
    .local v13, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v13, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v14

    .line 444
    .local v14, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v14, v7}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    move-object v9, v15

    .line 445
    invoke-virtual {v9, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 446
    .local v15, "sStr":[Ljava/lang/String;
    array-length v12, v15

    const/4 v11, 0x2

    if-ne v12, v11, :cond_6

    .line 447
    const/4 v11, 0x0

    aget-object v12, v15, v11

    invoke-virtual {v12, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v11

    .line 448
    .local v11, "pkgPos":I
    move-object/from16 v17, v2

    const/4 v12, 0x1

    aget-object v2, v15, v12

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 449
    .local v2, "idxPos":I
    if-ltz v11, :cond_5

    if-ltz v2, :cond_5

    .line 450
    move-object/from16 v18, v0

    const/4 v12, 0x0

    aget-object v0, v15, v12

    invoke-virtual {v0, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 451
    .local v0, "pkgName":Ljava/lang/String;
    const/4 v12, 0x1

    aget-object v12, v15, v12

    invoke-virtual {v12, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    .line 453
    .local v12, "pkgIndex":Ljava/lang/String;
    move/from16 v16, v2

    .end local v2    # "idxPos":I
    .local v16, "idxPos":I
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object/from16 v19, v4

    .end local v4    # "file":Ljava/io/File;
    .local v19, "file":Ljava/io/File;
    const/4 v4, 0x4

    if-le v2, v4, :cond_4

    :try_start_3
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v4, 0x6

    if-le v2, v4, :cond_4

    .line 454
    const-string v2, "[0-9]*"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 455
    .local v2, "patternNum":Ljava/util/regex/Pattern;
    move/from16 v20, v5

    .end local v5    # "ret":Z
    .local v20, "ret":Z
    :try_start_4
    invoke-virtual {v12, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 456
    .local v5, "isNum":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 457
    new-instance v4, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;

    move-object/from16 v22, v2

    const/4 v2, 0x6

    .end local v2    # "patternNum":Ljava/util/regex/Pattern;
    .local v22, "patternNum":Ljava/util/regex/Pattern;
    invoke-virtual {v12, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v21, v5

    const/4 v5, 0x0

    .end local v5    # "isNum":Ljava/util/regex/Matcher;
    .local v21, "isNum":Ljava/util/regex/Matcher;
    invoke-direct {v4, v1, v2, v5}, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;II)V

    move-object v2, v4

    .line 458
    .local v2, "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
    iget-object v4, v1, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_1

    .line 456
    .end local v21    # "isNum":Ljava/util/regex/Matcher;
    .end local v22    # "patternNum":Ljava/util/regex/Pattern;
    .local v2, "patternNum":Ljava/util/regex/Pattern;
    .restart local v5    # "isNum":Ljava/util/regex/Matcher;
    :cond_3
    move-object/from16 v22, v2

    move-object/from16 v21, v5

    .end local v2    # "patternNum":Ljava/util/regex/Pattern;
    .end local v5    # "isNum":Ljava/util/regex/Matcher;
    .restart local v21    # "isNum":Ljava/util/regex/Matcher;
    .restart local v22    # "patternNum":Ljava/util/regex/Pattern;
    goto :goto_1

    .line 465
    .end local v0    # "pkgName":Ljava/lang/String;
    .end local v9    # "tmpString":Ljava/lang/String;
    .end local v10    # "appMapFlag":Z
    .end local v11    # "pkgPos":I
    .end local v12    # "pkgIndex":Ljava/lang/String;
    .end local v13    # "pattern":Ljava/util/regex/Pattern;
    .end local v14    # "matcher":Ljava/util/regex/Matcher;
    .end local v15    # "sStr":[Ljava/lang/String;
    .end local v16    # "idxPos":I
    .end local v21    # "isNum":Ljava/util/regex/Matcher;
    .end local v22    # "patternNum":Ljava/util/regex/Pattern;
    :catch_1
    move-exception v0

    goto/16 :goto_6

    .line 468
    .end local v20    # "ret":Z
    .local v5, "ret":Z
    :catchall_1
    move-exception v0

    move/from16 v20, v5

    move-object v2, v0

    .end local v5    # "ret":Z
    .restart local v20    # "ret":Z
    goto/16 :goto_7

    .line 465
    .end local v20    # "ret":Z
    .restart local v5    # "ret":Z
    :catch_2
    move-exception v0

    move/from16 v20, v5

    .end local v5    # "ret":Z
    .restart local v20    # "ret":Z
    goto/16 :goto_6

    .line 453
    .end local v20    # "ret":Z
    .restart local v0    # "pkgName":Ljava/lang/String;
    .restart local v5    # "ret":Z
    .restart local v9    # "tmpString":Ljava/lang/String;
    .restart local v10    # "appMapFlag":Z
    .restart local v11    # "pkgPos":I
    .restart local v12    # "pkgIndex":Ljava/lang/String;
    .restart local v13    # "pattern":Ljava/util/regex/Pattern;
    .restart local v14    # "matcher":Ljava/util/regex/Matcher;
    .restart local v15    # "sStr":[Ljava/lang/String;
    .restart local v16    # "idxPos":I
    :cond_4
    move/from16 v20, v5

    .end local v5    # "ret":Z
    .restart local v20    # "ret":Z
    goto :goto_1

    .line 449
    .end local v0    # "pkgName":Ljava/lang/String;
    .end local v12    # "pkgIndex":Ljava/lang/String;
    .end local v16    # "idxPos":I
    .end local v19    # "file":Ljava/io/File;
    .end local v20    # "ret":Z
    .local v2, "idxPos":I
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "ret":Z
    :cond_5
    move-object/from16 v18, v0

    move/from16 v16, v2

    move-object/from16 v19, v4

    move/from16 v20, v5

    .end local v2    # "idxPos":I
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "ret":Z
    .restart local v16    # "idxPos":I
    .restart local v19    # "file":Ljava/io/File;
    .restart local v20    # "ret":Z
    goto :goto_1

    .line 446
    .end local v11    # "pkgPos":I
    .end local v16    # "idxPos":I
    .end local v19    # "file":Ljava/io/File;
    .end local v20    # "ret":Z
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "ret":Z
    :cond_6
    move-object/from16 v18, v0

    move-object/from16 v17, v2

    move-object/from16 v19, v4

    move/from16 v20, v5

    .line 463
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "ret":Z
    .end local v13    # "pattern":Ljava/util/regex/Pattern;
    .end local v14    # "matcher":Ljava/util/regex/Matcher;
    .end local v15    # "sStr":[Ljava/lang/String;
    .restart local v19    # "file":Ljava/io/File;
    .restart local v20    # "ret":Z
    :goto_1
    move-object/from16 v2, v17

    move-object/from16 v0, v18

    move-object/from16 v4, v19

    move/from16 v5, v20

    goto/16 :goto_0

    .line 441
    .end local v19    # "file":Ljava/io/File;
    .end local v20    # "ret":Z
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "ret":Z
    :cond_7
    move-object/from16 v18, v0

    move-object/from16 v17, v2

    move-object/from16 v19, v4

    move/from16 v20, v5

    .line 430
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "ret":Z
    .restart local v19    # "file":Ljava/io/File;
    .restart local v20    # "ret":Z
    :goto_2
    move-object/from16 v2, v17

    move-object/from16 v0, v18

    move-object/from16 v4, v19

    move/from16 v5, v20

    goto/16 :goto_0

    .end local v19    # "file":Ljava/io/File;
    .end local v20    # "ret":Z
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "ret":Z
    :cond_8
    move-object/from16 v19, v4

    move/from16 v20, v5

    .line 468
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "ret":Z
    .end local v9    # "tmpString":Ljava/lang/String;
    .end local v10    # "appMapFlag":Z
    .restart local v19    # "file":Ljava/io/File;
    .restart local v20    # "ret":Z
    :goto_3
    nop

    .line 470
    :try_start_5
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 473
    :goto_4
    goto/16 :goto_9

    .line 471
    :catch_3
    move-exception v0

    move-object v2, v0

    move-object v0, v2

    .line 472
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_4

    .line 468
    .end local v19    # "file":Ljava/io/File;
    .end local v20    # "ret":Z
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "ret":Z
    :catchall_2
    move-exception v0

    move-object/from16 v19, v4

    move/from16 v20, v5

    move-object v2, v0

    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "ret":Z
    .restart local v19    # "file":Ljava/io/File;
    .restart local v20    # "ret":Z
    goto :goto_7

    .line 465
    .end local v19    # "file":Ljava/io/File;
    .end local v20    # "ret":Z
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "ret":Z
    :catch_4
    move-exception v0

    move-object/from16 v19, v4

    move/from16 v20, v5

    .line 466
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "ret":Z
    .restart local v0    # "e":Ljava/io/IOException;
    .restart local v19    # "file":Ljava/io/File;
    .restart local v20    # "ret":Z
    :goto_6
    :try_start_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 468
    nop

    .end local v0    # "e":Ljava/io/IOException;
    if-eqz v6, :cond_b

    .line 470
    :try_start_7
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_4

    .line 471
    :catch_5
    move-exception v0

    move-object v2, v0

    move-object v0, v2

    .line 472
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_5

    .line 468
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v0

    move-object v2, v0

    :goto_7
    if-eqz v6, :cond_9

    .line 470
    :try_start_8
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 473
    goto :goto_8

    .line 471
    :catch_6
    move-exception v0

    move-object v4, v0

    move-object v0, v4

    .line 472
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    .end local v0    # "e":Ljava/io/IOException;
    :cond_9
    :goto_8
    throw v2

    .line 424
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .end local v19    # "file":Ljava/io/File;
    .end local v20    # "ret":Z
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "ret":Z
    :cond_a
    move-object/from16 v19, v4

    move/from16 v20, v5

    .line 478
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "ret":Z
    .restart local v19    # "file":Ljava/io/File;
    .restart local v20    # "ret":Z
    :cond_b
    :goto_9
    iget-object v0, v1, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_e

    .line 479
    iget-object v0, v1, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 480
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;>;"
    sget-boolean v3, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    if-eqz v3, :cond_c

    .line 481
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;

    iget v4, v4, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mAppIdx:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v8, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;>;"
    :cond_c
    goto :goto_a

    .line 483
    :cond_d
    const/4 v5, 0x1

    .end local v20    # "ret":Z
    .restart local v5    # "ret":Z
    goto :goto_b

    .line 478
    .end local v5    # "ret":Z
    .restart local v20    # "ret":Z
    :cond_e
    move/from16 v5, v20

    .line 486
    .end local v20    # "ret":Z
    .restart local v5    # "ret":Z
    :goto_b
    return v5
.end method

.method private PolicyControlTrigger(J)Z
    .locals 6
    .param p1, "mPolicyType"    # J

    .line 501
    iget-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mPolicyControlLock:Ljava/lang/Object;

    monitor-enter v0

    .line 502
    const/4 v1, 0x0

    .line 503
    .local v1, "mIsTrigger":I
    const-wide/16 v2, 0x3e8

    :try_start_0
    rem-long v2, p1, v2

    long-to-int v2, v2

    .line 504
    .local v2, "i":I
    sget v3, Lcom/android/server/am/MimdManagerServiceImpl;->mPolicyControlMaskLocal:I

    .line 506
    .local v3, "mTmpMask":I
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    const/4 v5, 0x0

    if-nez v4, :cond_0

    .line 507
    monitor-exit v0

    return v5

    .line 510
    :cond_0
    :goto_0
    add-int/lit8 v4, v2, -0x1

    .end local v2    # "i":I
    .local v4, "i":I
    if-eqz v2, :cond_1

    .line 511
    div-int/lit8 v2, v3, 0xa

    move v3, v2

    move v2, v4

    goto :goto_0

    .line 512
    :cond_1
    rem-int/lit8 v2, v3, 0xa

    move v1, v2

    .line 514
    if-nez v1, :cond_2

    .line 515
    monitor-exit v0

    return v5

    .line 518
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->TriggerPolicy(J)V

    .line 519
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 520
    .end local v1    # "mIsTrigger":I
    .end local v3    # "mTmpMask":I
    .end local v4    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private TriggerPolicy(J)V
    .locals 7
    .param p1, "mPolicyType"    # J

    .line 524
    const-string v0, "close mimdtrigger failed"

    const-string v1, "MimdManagerServiceImpl"

    new-instance v2, Ljava/io/File;

    const-string v3, "/sys/module/perf_helper/mimd/mimdtrigger"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 525
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 526
    const/4 v3, 0x0

    .line 528
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v3, v4

    .line 529
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 530
    sget-boolean v4, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    if-eqz v4, :cond_0

    .line 531
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "write mimdtrigger string "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535
    :cond_0
    nop

    .line 537
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 540
    :goto_0
    goto :goto_4

    .line 538
    :catch_0
    move-exception v4

    .line 539
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 535
    :catchall_0
    move-exception v4

    goto :goto_2

    .line 532
    :catch_1
    move-exception v4

    .line 533
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "write mimdtrigger failed"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 535
    nop

    .end local v4    # "e":Ljava/lang/Exception;
    if-eqz v3, :cond_2

    .line 537
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 538
    :catch_2
    move-exception v4

    .line 539
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 535
    .end local v4    # "e":Ljava/io/IOException;
    :goto_2
    if-eqz v3, :cond_1

    .line 537
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 540
    goto :goto_3

    .line 538
    :catch_3
    move-exception v5

    .line 539
    .local v5, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    .end local v5    # "e":Ljava/io/IOException;
    :cond_1
    :goto_3
    throw v4

    .line 544
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    :goto_4
    return-void
.end method

.method private appDied(II)V
    .locals 12
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 299
    iget-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mDieLock:Ljava/lang/Object;

    monitor-enter v0

    .line 300
    :try_start_0
    invoke-direct {p0, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->getPackageNameByUid(I)Ljava/lang/String;

    move-result-object v1

    .line 301
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 302
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;

    .line 303
    .local v2, "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
    iget v3, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I

    if-lez v3, :cond_0

    iget v3, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I

    if-ne v3, p1, :cond_0

    .line 304
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " died"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x8

    invoke-static {v4, v5, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 305
    iget v3, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mAppIdx:I

    .line 308
    .local v3, "appidx":I
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->UpdatePid(I)V

    .line 309
    int-to-long v6, v3

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x4

    add-long/2addr v6, v8

    int-to-long v8, p2

    const-wide/32 v10, 0x186a0

    mul-long/2addr v8, v10

    add-long/2addr v6, v8

    .line 310
    .local v6, "tmpPT":J
    invoke-direct {p0, v6, v7}, Lcom/android/server/am/MimdManagerServiceImpl;->PolicyControlTrigger(J)Z

    .line 311
    const-string v8, "MimdManagerServiceImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ProcessDied, pid=:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " uid="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " type="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-static {v4, v5}, Landroid/os/Trace;->traceEnd(J)V

    .line 315
    .end local v1    # "pkgName":Ljava/lang/String;
    .end local v2    # "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
    .end local v3    # "appidx":I
    .end local v6    # "tmpPT":J
    :cond_0
    monitor-exit v0

    .line 316
    return-void

    .line 315
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private checkAppCgroup(I)Z
    .locals 6
    .param p1, "uid"    # I

    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/server/am/MimdManagerServiceImpl;->mAppMemCgroupPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "uid_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 360
    .local v0, "uidPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 361
    .local v1, "appCgDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 362
    return v3

    .line 365
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v2

    const-string v4, "MimdManagerServiceImpl"

    const/4 v5, 0x0

    if-eqz v2, :cond_2

    .line 366
    invoke-virtual {v1, v3, v5}, Ljava/io/File;->setReadable(ZZ)Z

    .line 367
    invoke-virtual {v1, v3, v3}, Ljava/io/File;->setWritable(ZZ)Z

    .line 368
    invoke-virtual {v1, v3, v5}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 369
    sget-boolean v2, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    if-eqz v2, :cond_1

    .line 370
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "create app cgroup succeed, path:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_1
    return v3

    .line 373
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "create app cgroup failed, path:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    return v5
.end method

.method private static deleteFolder(Ljava/io/File;)V
    .locals 4
    .param p0, "folder"    # Ljava/io/File;

    .line 378
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 380
    .local v0, "files":[Ljava/io/File;
    if-eqz v0, :cond_0

    .line 381
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 382
    .local v3, "file":Ljava/io/File;
    invoke-static {v3}, Lcom/android/server/am/MimdManagerServiceImpl;->deleteFolder(Ljava/io/File;)V

    .line 381
    .end local v3    # "file":Ljava/io/File;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 386
    .end local v0    # "files":[Ljava/io/File;
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 387
    return-void
.end method

.method private entrySleepMode()V
    .locals 5

    .line 213
    const-string v0, "entry sleep"

    const-wide/16 v1, 0x8

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 214
    const-wide/16 v3, 0x3e9

    invoke-direct {p0, v3, v4}, Lcom/android/server/am/MimdManagerServiceImpl;->PolicyControlTrigger(J)Z

    move-result v0

    .line 215
    .local v0, "res":Z
    sget-boolean v3, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 216
    const-string v3, "MimdManagerServiceImpl"

    const-string v4, "enable sleep mode to reclaim memory!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_0
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    .line 218
    return-void
.end method

.method private foregroundChanged(IIZ)V
    .locals 12
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "foreground"    # Z

    .line 268
    iget-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mFgLock:Ljava/lang/Object;

    monitor-enter v0

    .line 269
    :try_start_0
    invoke-direct {p0, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->getPackageNameByUid(I)Ljava/lang/String;

    move-result-object v1

    .line 270
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 271
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to fg "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x8

    invoke-static {v3, v4, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 272
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;

    .line 273
    .local v2, "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
    iget v5, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mPid:I

    if-nez v5, :cond_0

    .line 274
    invoke-virtual {v2, p1}, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->UpdatePid(I)V

    .line 276
    :cond_0
    iget v5, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mUid:I

    if-eq v5, p2, :cond_1

    .line 277
    iget v5, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mUid:I

    invoke-static {v5}, Lcom/android/server/am/MimdManagerServiceImpl;->removeEmptyMemcgDir(I)V

    .line 278
    iput p2, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mUid:I

    .line 281
    :cond_1
    iget v5, v2, Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;->mAppIdx:I

    .line 283
    .local v5, "appidx":I
    const-wide/32 v6, 0x186a0

    const-wide/16 v8, 0x3e8

    if-eqz p3, :cond_2

    .line 284
    int-to-long v10, v5

    mul-long/2addr v10, v8

    const-wide/16 v8, 0x2

    add-long/2addr v10, v8

    int-to-long v8, p2

    mul-long/2addr v8, v6

    add-long/2addr v10, v8

    .line 285
    .local v10, "tmpPT":J
    sget-boolean v6, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    if-eqz v6, :cond_3

    .line 286
    const-string v6, "MimdManagerServiceImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Foreground changed, foreground pid=:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Pkg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 288
    .end local v10    # "tmpPT":J
    :cond_2
    int-to-long v10, v5

    mul-long/2addr v10, v8

    const-wide/16 v8, 0x3

    add-long/2addr v10, v8

    int-to-long v8, p2

    mul-long/2addr v8, v6

    add-long/2addr v10, v8

    .line 289
    .restart local v10    # "tmpPT":J
    sget-boolean v6, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    if-eqz v6, :cond_3

    .line 290
    const-string v6, "MimdManagerServiceImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Foreground changed, background pid=:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Pkg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :cond_3
    :goto_0
    invoke-direct {p0, v10, v11}, Lcom/android/server/am/MimdManagerServiceImpl;->PolicyControlTrigger(J)Z

    .line 293
    invoke-static {v3, v4}, Landroid/os/Trace;->traceEnd(J)V

    .line 295
    .end local v1    # "pkgName":Ljava/lang/String;
    .end local v2    # "appMsg":Lcom/android/server/am/MimdManagerServiceImpl$AppMsg;
    .end local v5    # "appidx":I
    .end local v10    # "tmpPT":J
    :cond_4
    monitor-exit v0

    .line 296
    return-void

    .line 295
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getPackageNameByUid(I)Ljava/lang/String;
    .locals 2
    .param p1, "uid"    # I

    .line 491
    iget-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 492
    .local v0, "pkgs":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 493
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .local v1, "packageName":Ljava/lang/String;
    goto :goto_0

    .line 495
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 497
    .restart local v1    # "packageName":Ljava/lang/String;
    :goto_0
    return-object v1
.end method

.method private getPolicyControlMaskLocal()V
    .locals 15

    .line 229
    const-string v0, "PolicyControlMaskLocal:"

    const-string v1, "read PolicyControlMaskLocal failed"

    const-string v2, "MimdManagerServiceImpl"

    const/4 v3, 0x0

    .line 230
    .local v3, "br":Ljava/io/BufferedReader;
    new-instance v4, Ljava/io/File;

    const-string v5, "/odm/etc/mimdconfig"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 231
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 232
    const/4 v5, 0x0

    .line 234
    .local v5, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/FileReader;

    invoke-direct {v7, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v5, v6

    .line 235
    const/4 v6, 0x0

    .line 236
    .local v6, "tmpString":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    move-object v6, v7

    if-eqz v7, :cond_2

    .line 237
    invoke-virtual {v6, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    if-ltz v7, :cond_0

    .line 238
    const-string v7, "\\s+"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 239
    .local v7, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v7, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    .line 240
    .local v8, "matcher":Ljava/util/regex/Matcher;
    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    move-object v6, v9

    .line 241
    invoke-virtual {v6, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 242
    .local v9, "idx":I
    if-ltz v9, :cond_1

    .line 243
    invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 244
    .local v10, "pcmStr":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v11

    const/16 v12, 0x17

    if-le v11, v12, :cond_1

    .line 245
    const-string v11, "[0-9]*"

    invoke-static {v11}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v11

    .line 246
    .local v11, "patternNum":Ljava/util/regex/Pattern;
    invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    .line 247
    .local v13, "isNum":Ljava/util/regex/Matcher;
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->matches()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 248
    invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    sput v12, Lcom/android/server/am/MimdManagerServiceImpl;->mPolicyControlMaskLocal:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    .end local v7    # "pattern":Ljava/util/regex/Pattern;
    .end local v8    # "matcher":Ljava/util/regex/Matcher;
    .end local v9    # "idx":I
    .end local v10    # "pcmStr":Ljava/lang/String;
    .end local v11    # "patternNum":Ljava/util/regex/Pattern;
    .end local v13    # "isNum":Ljava/util/regex/Matcher;
    :cond_1
    goto :goto_0

    .line 256
    .end local v6    # "tmpString":Ljava/lang/String;
    :cond_2
    nop

    .line 258
    :try_start_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 261
    :goto_1
    goto :goto_5

    .line 259
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    :goto_2
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 256
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 253
    :catch_1
    move-exception v0

    .line 254
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 256
    nop

    .end local v0    # "e":Ljava/io/IOException;
    if-eqz v5, :cond_4

    .line 258
    :try_start_3
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 259
    :catch_2
    move-exception v0

    .line 260
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_2

    .line 256
    .end local v0    # "e":Ljava/io/IOException;
    :goto_3
    if-eqz v5, :cond_3

    .line 258
    :try_start_4
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 261
    goto :goto_4

    .line 259
    :catch_3
    move-exception v6

    .line 260
    .local v6, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    .end local v6    # "e":Ljava/io/IOException;
    :cond_3
    :goto_4
    throw v0

    .line 265
    .end local v5    # "reader":Ljava/io/BufferedReader;
    :cond_4
    :goto_5
    return-void
.end method

.method private getProcessNameByPid(I)Ljava/lang/String;
    .locals 1
    .param p1, "pid"    # I

    .line 345
    const/4 v0, 0x0

    .line 346
    .local v0, "processName":Ljava/lang/String;
    invoke-static {p1}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 347
    return-object v0
.end method

.method private mimdManagerInit()V
    .locals 5

    .line 174
    sget-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->sEnableMimdService:Z

    if-eqz v0, :cond_1

    .line 175
    invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->policyControlInit()V

    .line 176
    sget-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    const-string v1, "MimdManagerServiceImpl"

    if-eqz v0, :cond_0

    .line 177
    const-string v2, "init per-app memcg reclaim succeed!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_0
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.miui.powerkeeper_sleep_changed"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl;->filter:Landroid/content/IntentFilter;

    .line 180
    iget-object v3, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mSleepModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 181
    if-eqz v0, :cond_1

    .line 182
    const-string v0, "register sleep mode receiver succeed!"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_1
    return-void
.end method

.method private onAppProcessStart(IILjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "pkg"    # Ljava/lang/String;

    .line 351
    iget-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->appIndexMap:Ljava/util/Map;

    invoke-direct {p0, p2}, Lcom/android/server/am/MimdManagerServiceImpl;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/server/am/MimdManagerServiceImpl;->checkAppCgroup(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/server/am/MimdManagerServiceImpl;->mAppMemCgroupPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "uid_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 354
    .local v0, "uidPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "cgroup.procs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/am/MimdManagerServiceImpl;->writeToSys(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    .end local v0    # "uidPath":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private policyControlInit()V
    .locals 3

    .line 186
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/module/perf_helper/mimd/mimdtrigger"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 187
    .local v0, "mTriggerFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->getPolicyControlMaskLocal()V

    .line 189
    invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->ParseMimdconfigAppmap()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mActivityManagerService:Landroid/app/IActivityManager;

    .line 191
    invoke-direct {p0}, Lcom/android/server/am/MimdManagerServiceImpl;->registeForegroundReceiver()V

    goto :goto_0

    .line 194
    :cond_0
    const-string v1, "MimdManagerServiceImpl"

    const-string v2, "mimd trigger file not find!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_1
    :goto_0
    return-void
.end method

.method private registeForegroundReceiver()V
    .locals 3

    .line 222
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mActivityManagerService:Landroid/app/IActivityManager;

    iget-object v1, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mProcessObserver:Landroid/app/IProcessObserver$Stub;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    goto :goto_0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MimdManagerServiceImpl"

    const-string v2, "mimd manager service registerProcessObserver failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method private static removeEmptyMemcgDir(I)V
    .locals 4
    .param p0, "uid"    # I

    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/server/am/MimdManagerServiceImpl;->mAppMemCgroupPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "uid_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 391
    .local v0, "uidPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 392
    .local v1, "appCgDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 393
    invoke-static {v1}, Lcom/android/server/am/MimdManagerServiceImpl;->deleteFolder(Ljava/io/File;)V

    .line 394
    sget-boolean v2, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    if-eqz v2, :cond_0

    .line 395
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove app cgroup succeed,path:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MimdManagerServiceImpl"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    :cond_0
    return-void

    .line 398
    :cond_1
    return-void
.end method

.method private static writeToSys(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "content"    # Ljava/lang/String;

    .line 401
    const-string v0, "close path failed, err:"

    const-string v1, "MimdManagerServiceImpl"

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 402
    .local v2, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 403
    .local v3, "writer":Ljava/io/FileWriter;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 405
    :try_start_0
    new-instance v4, Ljava/io/FileWriter;

    invoke-direct {v4, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    move-object v3, v4

    .line 406
    invoke-virtual {v3, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    nop

    .line 412
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 415
    :goto_0
    goto :goto_4

    .line 413
    :catch_0
    move-exception v4

    .line 414
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 410
    :catchall_0
    move-exception v4

    goto :goto_2

    .line 407
    :catch_1
    move-exception v4

    .line 408
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "open or write failed, err:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 410
    nop

    .end local v4    # "e":Ljava/io/IOException;
    if-eqz v3, :cond_1

    .line 412
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 413
    :catch_2
    move-exception v4

    .line 414
    .restart local v4    # "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    .line 410
    .end local v4    # "e":Ljava/io/IOException;
    :goto_2
    if-eqz v3, :cond_0

    .line 412
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 415
    goto :goto_3

    .line 413
    :catch_3
    move-exception v5

    .line 414
    .local v5, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    .end local v5    # "e":Ljava/io/IOException;
    :cond_0
    :goto_3
    throw v4

    .line 419
    :cond_1
    :goto_4
    return-void
.end method


# virtual methods
.method public onProcessStart(IILjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 125
    sget-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->sEnableMimdService:Z

    if-eqz v0, :cond_0

    .line 126
    new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;

    invoke-direct {v0, p0}, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;)V

    .line 127
    .local v0, "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;
    iput p2, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->pid:I

    .line 128
    iput p1, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->uid:I

    .line 129
    iput-object p3, v0, Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;->packageName:Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mMimdManagerHandler:Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 131
    .local v1, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mMimdManagerHandler:Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 133
    .end local v0    # "info":Lcom/android/server/am/MimdManagerServiceImpl$AppInfo;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public systemReady(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 107
    const-string v0, "persist.sys.mimd.reclaim.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MimdManagerServiceImpl;->sEnableMimdService:Z

    .line 108
    iput-object p1, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mContext:Landroid/content/Context;

    .line 109
    const-string v2, "MimdManagerServiceImpl"

    if-nez v0, :cond_0

    .line 110
    const-string v0, "All Features are disabled, do not create HandlerThread!"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return-void

    .line 113
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v3, "mimd"

    invoke-direct {v0, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 114
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 115
    new-instance v0, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    iget-object v3, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, p0, v3, v4}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;-><init>(Lcom/android/server/am/MimdManagerServiceImpl;Landroid/os/Looper;Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler-IA;)V

    iput-object v0, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mMimdManagerHandler:Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    .line 117
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 118
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/android/server/am/MimdManagerServiceImpl;->mMimdManagerHandler:Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 119
    sget-boolean v1, Lcom/android/server/am/MimdManagerServiceImpl;->DBG_MIMD:Z

    if-eqz v1, :cond_1

    .line 120
    const-string v1, "Create MimdManagerService succeed!"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_1
    return-void
.end method
