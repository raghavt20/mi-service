public class com.android.server.am.PendingIntentControllerStubImpl implements com.android.server.am.PendingIntentControllerStub {
	 /* .source "PendingIntentControllerStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer DEFAULT_PENDINGINTENT_ERROR_THRESHOLD;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 public Integer PENDINGINTENT_ERROR_THRESHOLD;
	 /* # direct methods */
	 public com.android.server.am.PendingIntentControllerStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 9 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 16 */
		 /* const/16 v0, 0x2710 */
		 /* iput v0, p0, Lcom/android/server/am/PendingIntentControllerStubImpl;->PENDINGINTENT_ERROR_THRESHOLD:I */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void checkIsTooManyPendingIntent ( Integer p0, Integer p1, com.android.server.am.PendingIntentRecord p2, android.util.SparseIntArray p3 ) {
		 /* .locals 4 */
		 /* .param p1, "newCount" # I */
		 /* .param p2, "uid" # I */
		 /* .param p3, "pir" # Lcom/android/server/am/PendingIntentRecord; */
		 /* .param p4, "intentsPerUid" # Landroid/util/SparseIntArray; */
		 /* .line 19 */
		 /* iget v0, p0, Lcom/android/server/am/PendingIntentControllerStubImpl;->PENDINGINTENT_ERROR_THRESHOLD:I */
		 /* if-lt p1, v0, :cond_1 */
		 /* .line 20 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "Too many PendingIntent created for uid "; // const-string v1, "Too many PendingIntent created for uid "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", aborting "; // const-string v1, ", aborting "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.key;
		 /* .line 21 */
		 (( com.android.server.am.PendingIntentRecord$Key ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/server/am/PendingIntentRecord$Key;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 22 */
		 /* .local v0, "msg":Ljava/lang/String; */
		 final String v1 = "PendingIntentControllerStubImpl"; // const-string v1, "PendingIntentControllerStubImpl"
		 android.util.Slog .w ( v1,v0 );
		 /* .line 25 */
		 /* const/16 v2, 0x3e8 */
		 /* if-ne p2, v2, :cond_0 */
		 v2 = 		 android.os.Binder .getCallingPid ( );
		 v3 = 		 android.os.Process .myPid ( );
		 /* if-ne v2, v3, :cond_0 */
		 /* .line 26 */
		 final String v2 = "Too many PendingIntent created for system_server"; // const-string v2, "Too many PendingIntent created for system_server"
		 android.util.Slog .wtf ( v1,v2 );
		 /* .line 28 */
	 } // :cond_0
	 /* add-int/lit8 v1, p1, -0x1 */
	 (( android.util.SparseIntArray ) p4 ).put ( p2, v1 ); // invoke-virtual {p4, p2, v1}, Landroid/util/SparseIntArray;->put(II)V
	 /* .line 29 */
	 /* new-instance v1, Ljava/lang/SecurityException; */
	 /* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
	 /* throw v1 */
	 /* .line 32 */
} // .end local v0 # "msg":Ljava/lang/String;
} // :cond_1
} // :goto_0
return;
} // .end method
