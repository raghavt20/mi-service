public class com.android.server.am.AppProfilerImpl implements com.android.server.am.AppProfilerStub {
	 /* .source "AppProfilerImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String CMD_DUMPSYS;
	 private static final Long DEFAULT_DUMP_CPU_TIMEOUT_MILLISECONDS;
	 private static final java.lang.String DUMP_CPU_TIMEOUT_PROPERTY;
	 private static final Integer DUMP_PROCS_MEM_INTERVAL_MILLIS;
	 private static final Integer LOW_MEM_FACTOR;
	 private static final Integer MAX_LOW_MEM_FILE_NUM;
	 private static final MEMINFO_FORMAT;
	 private static final java.lang.String NATIVE_MEM_INFO;
	 private static final java.lang.String OOM_MEM_INFO;
	 private static final Integer PSS_NORMAL_THRESHOLD;
	 public static final Integer STALL_RATIO_FULL;
	 public static final Integer STALL_RATIO_SOME;
	 static final Integer STATE_START;
	 static final Integer STATE_STOP;
	 private static final java.lang.String TAG;
	 private static Boolean testTrimMemActivityBg_workaround;
	 /* # instance fields */
	 private final Long REPORT_OOM_MEMINFO_INTERVAL_MILLIS;
	 private Long mLastDumpProcsMemTime;
	 private Long mLastMemUsageReportTime;
	 private Long mLastReportOomMemTime;
	 private Integer mMinOomScore;
	 private com.android.server.am.ActivityManagerService mService;
	 private java.util.HashMap pssThresholdMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashMap<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.am.AppProfilerImpl ( ) {
/* .locals 2 */
/* .line 81 */
/* const/16 v0, 0x9 */
/* new-array v0, v0, [I */
/* fill-array-data v0, :array_0 */
/* .line 98 */
final String v0 = "persist.sys.testTrimMemActivityBg.wk.enable"; // const-string v0, "persist.sys.testTrimMemActivityBg.wk.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.AppProfilerImpl.testTrimMemActivityBg_workaround = (v0!= 0);
/* .line 105 */
final String v0 = "persist.sys.stall_ratio_some"; // const-string v0, "persist.sys.stall_ratio_some"
/* const/16 v1, 0x4b */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 108 */
final String v0 = "persist.sys.stall_ratio_full"; // const-string v0, "persist.sys.stall_ratio_full"
/* const/16 v1, 0x32 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x120 */
/* 0x2020 */
/* 0xa */
/* 0x120 */
/* 0x2020 */
/* 0xa */
/* 0x120 */
/* 0x2020 */
/* 0xa */
} // .end array-data
} // .end method
public com.android.server.am.AppProfilerImpl ( ) {
/* .locals 2 */
/* .line 62 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 78 */
/* const/16 v0, 0x3e9 */
/* iput v0, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I */
/* .line 104 */
/* const-wide/32 v0, -0x3a980 */
/* iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->mLastDumpProcsMemTime:J */
/* .line 204 */
/* const-wide/32 v0, 0x36ee80 */
/* iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->REPORT_OOM_MEMINFO_INTERVAL_MILLIS:J */
/* .line 205 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->mLastReportOomMemTime:J */
return;
} // .end method
private java.lang.String buildMemInfo ( com.android.server.am.MemUsageBuilder p0, Boolean p1 ) {
/* .locals 20 */
/* .param p1, "builder" # Lcom/android/server/am/MemUsageBuilder; */
/* .param p2, "isDropbox" # Z */
/* .line 478 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* const/16 v3, 0x400 */
/* invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V */
/* move-object v3, v0 */
/* .line 479 */
/* .local v3, "dropBuilder":Ljava/lang/StringBuilder; */
/* const/16 v0, 0xa */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 480 */
com.android.server.ResourcePressureUtil .currentPsiState ( );
/* .line 481 */
/* .local v4, "psi":Ljava/lang/String; */
final String v5 = "Subject: "; // const-string v5, "Subject: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.subject;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 482 */
final String v5 = "Build: "; // const-string v5, "Build: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = android.os.Build.FINGERPRINT;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = "\n\n"; // const-string v6, "\n\n"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 483 */
v5 = this.title;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ":"; // const-string v6, ":"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 484 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 486 */
} // .end local v4 # "psi":Ljava/lang/String;
} // :cond_0
v4 = this.stack;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "\n\n"; // const-string v5, "\n\n"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 487 */
v4 = this.topProcs;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "\n"; // const-string v5, "\n"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 488 */
v4 = this.fullNative;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 489 */
v4 = this.fullJava;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 490 */
v4 = this.summary;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 491 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 492 */
/* new-instance v0, Ljava/io/StringWriter; */
/* invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V */
/* move-object v4, v0 */
/* .line 493 */
/* .local v4, "catSw":Ljava/io/StringWriter; */
v5 = this.mService;
/* monitor-enter v5 */
/* .line 494 */
try { // :try_start_0
/* new-instance v8, Lcom/android/internal/util/FastPrintWriter; */
/* const/16 v0, 0x100 */
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v8, v4, v6, v0}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V */
/* .line 495 */
/* .local v8, "catPw":Ljava/io/PrintWriter; */
/* new-array v9, v6, [Ljava/lang/String; */
/* .line 496 */
/* .local v9, "emptyArgs":[Ljava/lang/String; */
(( java.io.PrintWriter ) v8 ).println ( ); // invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V
/* .line 497 */
v0 = this.mService;
v14 = this.mProcLock;
/* monitor-enter v14 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 498 */
try { // :try_start_1
	 v0 = this.mService;
	 v6 = this.mProcessList;
	 int v7 = 0; // const/4 v7, 0x0
	 int v10 = 0; // const/4 v10, 0x0
	 int v11 = 0; // const/4 v11, 0x0
	 int v12 = 0; // const/4 v12, 0x0
	 int v13 = -1; // const/4 v13, -0x1
	 /* invoke-virtual/range {v6 ..v13}, Lcom/android/server/am/ProcessList;->dumpProcessesLSP(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZLjava/lang/String;I)V */
	 /* .line 499 */
	 /* monitor-exit v14 */
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 500 */
	 try { // :try_start_2
		 (( java.io.PrintWriter ) v8 ).println ( ); // invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V
		 /* .line 501 */
		 v0 = this.mService;
		 v10 = this.mServices;
		 int v11 = 0; // const/4 v11, 0x0
		 int v14 = 0; // const/4 v14, 0x0
		 int v15 = 0; // const/4 v15, 0x0
		 /* const/16 v16, 0x0 */
		 /* move-object v12, v8 */
		 /* move-object v13, v9 */
		 /* invoke-virtual/range {v10 ..v16}, Lcom/android/server/am/ActiveServices;->newServiceDumperLocked(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZLjava/lang/String;)Lcom/android/server/am/ActiveServices$ServiceDumper; */
		 /* .line 502 */
		 (( com.android.server.am.ActiveServices$ServiceDumper ) v0 ).dumpLocked ( ); // invoke-virtual {v0}, Lcom/android/server/am/ActiveServices$ServiceDumper;->dumpLocked()V
		 /* .line 503 */
		 (( java.io.PrintWriter ) v8 ).println ( ); // invoke-virtual {v8}, Ljava/io/PrintWriter;->println()V
		 /* .line 504 */
		 v0 = this.mService;
		 v10 = this.mAtmInternal;
		 final String v11 = "activities"; // const-string v11, "activities"
		 int v12 = 0; // const/4 v12, 0x0
		 int v15 = 0; // const/4 v15, 0x0
		 /* const/16 v16, 0x0 */
		 /* const/16 v17, 0x0 */
		 /* const/16 v18, 0x0 */
		 /* const/16 v19, -0x1 */
		 /* move-object v13, v8 */
		 /* move-object v14, v9 */
		 /* invoke-virtual/range {v10 ..v19}, Lcom/android/server/wm/ActivityTaskManagerInternal;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZZLjava/lang/String;I)V */
		 /* .line 506 */
		 (( java.io.PrintWriter ) v8 ).flush ( ); // invoke-virtual {v8}, Ljava/io/PrintWriter;->flush()V
		 /* .line 507 */
	 } // .end local v8 # "catPw":Ljava/io/PrintWriter;
} // .end local v9 # "emptyArgs":[Ljava/lang/String;
/* monitor-exit v5 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 508 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 499 */
/* .restart local v8 # "catPw":Ljava/io/PrintWriter; */
/* .restart local v9 # "emptyArgs":[Ljava/lang/String; */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_3
	 /* monitor-exit v14 */
	 /* :try_end_3 */
	 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v3 # "dropBuilder":Ljava/lang/StringBuilder;
} // .end local v4 # "catSw":Ljava/io/StringWriter;
} // .end local p0 # "this":Lcom/android/server/am/AppProfilerImpl;
} // .end local p1 # "builder":Lcom/android/server/am/MemUsageBuilder;
} // .end local p2 # "isDropbox":Z
try { // :try_start_4
/* throw v0 */
/* .line 507 */
} // .end local v8 # "catPw":Ljava/io/PrintWriter;
} // .end local v9 # "emptyArgs":[Ljava/lang/String;
/* .restart local v3 # "dropBuilder":Ljava/lang/StringBuilder; */
/* .restart local v4 # "catSw":Ljava/io/StringWriter; */
/* .restart local p0 # "this":Lcom/android/server/am/AppProfilerImpl; */
/* .restart local p1 # "builder":Lcom/android/server/am/MemUsageBuilder; */
/* .restart local p2 # "isDropbox":Z */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit v5 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v0 */
/* .line 510 */
} // .end local v4 # "catSw":Ljava/io/StringWriter;
} // :cond_1
} // :goto_0
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private java.util.ArrayList collectProcessMemInfos ( java.util.function.Predicate p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/function/Predicate<", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;)", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/ProcessMemInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 515 */
/* .local p1, "predicate":Ljava/util/function/Predicate;, "Ljava/util/function/Predicate<Lcom/android/server/am/ProcessRecord;>;" */
v0 = this.mService;
/* monitor-enter v0 */
/* .line 516 */
try { // :try_start_0
v1 = this.mService;
v1 = this.mProcessList;
v1 = (( com.android.server.am.ProcessList ) v1 ).getLruSizeLOSP ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I
/* .line 517 */
/* .local v1, "lruSize":I */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 518 */
/* .local v2, "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;" */
v3 = this.mService;
v3 = this.mProcessList;
/* new-instance v4, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v4, p1, v2}, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda1;-><init>(Ljava/util/function/Predicate;Ljava/util/ArrayList;)V */
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.am.ProcessList ) v3 ).forEachLruProcessesLOSP ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lcom/android/server/am/ProcessList;->forEachLruProcessesLOSP(ZLjava/util/function/Consumer;)V
/* .line 526 */
/* monitor-exit v0 */
/* .line 527 */
} // .end local v1 # "lruSize":I
} // .end local v2 # "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private Long getDumpCpuTimeoutThreshold ( ) {
/* .locals 3 */
/* .line 473 */
final String v0 = "persist.sys.stability.dump_cpu.timeout"; // const-string v0, "persist.sys.stability.dump_cpu.timeout"
/* const-wide/16 v1, 0xbb8 */
android.os.SystemProperties .getLong ( v0,v1,v2 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private void initPssThresholdMap ( ) {
/* .locals 10 */
/* .line 287 */
final String v0 = "Init Memory ThresholdMap"; // const-string v0, "Init Memory ThresholdMap"
final String v1 = "AppProfilerImpl"; // const-string v1, "AppProfilerImpl"
android.util.Slog .d ( v1,v0 );
/* .line 288 */
v0 = this.pssThresholdMap;
/* if-nez v0, :cond_3 */
/* .line 289 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.pssThresholdMap = v0;
/* .line 291 */
/* sget-boolean v0, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 292 */
final String v0 = "package_pss_threshold_lite"; // const-string v0, "package_pss_threshold_lite"
/* .local v0, "listName":Ljava/lang/String; */
/* .line 294 */
} // .end local v0 # "listName":Ljava/lang/String;
} // :cond_0
final String v0 = "package_pss_threshold"; // const-string v0, "package_pss_threshold"
/* .line 297 */
/* .restart local v0 # "listName":Ljava/lang/String; */
} // :goto_0
try { // :try_start_0
v2 = this.mService;
v2 = this.mContext;
/* .line 298 */
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v3 = "array"; // const-string v3, "array"
final String v4 = "android.miui"; // const-string v4, "android.miui"
v2 = (( android.content.res.Resources ) v2 ).getIdentifier ( v0, v3, v4 ); // invoke-virtual {v2, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 300 */
/* .local v2, "pssThresholdId":I */
/* if-lez v2, :cond_2 */
/* .line 301 */
v3 = this.mService;
v3 = this.mContext;
(( android.content.Context ) v3 ).getResources ( ); // invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v3 ).getStringArray ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 302 */
/* .local v3, "pssThresholdList":[Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_1
/* array-length v5, v3 */
/* if-ge v4, v5, :cond_1 */
/* .line 303 */
/* aget-object v5, v3, v4 */
final String v6 = ","; // const-string v6, ","
(( java.lang.String ) v5 ).split ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v5 );
/* .line 304 */
/* .local v5, "packageThreshold":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v6 = 1; // const/4 v6, 0x1
/* check-cast v6, Ljava/lang/String; */
v6 = java.lang.Integer .parseInt ( v6 );
/* mul-int/lit16 v6, v6, 0x400 */
/* .line 305 */
/* .local v6, "packagePssThreshold":I */
v7 = this.pssThresholdMap;
int v8 = 0; // const/4 v8, 0x0
/* check-cast v8, Ljava/lang/String; */
java.lang.Integer .valueOf ( v6 );
(( java.util.HashMap ) v7 ).put ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 302 */
/* nop */
} // .end local v5 # "packageThreshold":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v6 # "packagePssThreshold":I
/* add-int/lit8 v4, v4, 0x1 */
/* .line 308 */
} // .end local v4 # "i":I
} // :cond_1
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " size:"; // const-string v5, " size:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* array-length v5, v3 */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v4 );
/* .line 310 */
} // .end local v3 # "pssThresholdList":[Ljava/lang/String;
} // :cond_2
final String v3 = "No Memory Threshold Id"; // const-string v3, "No Memory Threshold Id"
android.util.Slog .d ( v1,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 313 */
} // .end local v2 # "pssThresholdId":I
} // :goto_2
/* .line 312 */
/* :catch_0 */
/* move-exception v1 */
/* .line 315 */
} // .end local v0 # "listName":Ljava/lang/String;
} // :cond_3
} // :goto_3
return;
} // .end method
static void lambda$collectProcessMemInfos$2 ( java.util.function.Predicate p0, java.util.ArrayList p1, com.android.server.am.ProcessRecord p2 ) { //synthethic
/* .locals 9 */
/* .param p0, "predicate" # Ljava/util/function/Predicate; */
/* .param p1, "memInfos" # Ljava/util/ArrayList; */
/* .param p2, "rec" # Lcom/android/server/am/ProcessRecord; */
v0 = /* .line 519 */
/* if-nez v0, :cond_0 */
return;
/* .line 521 */
} // :cond_0
v0 = this.mState;
/* .line 522 */
/* .local v0, "state":Lcom/android/server/am/ProcessStateRecord; */
/* new-instance v8, Lcom/android/server/am/ProcessMemInfo; */
v2 = this.processName;
v3 = (( com.android.server.am.ProcessRecord ) p2 ).getPid ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I
/* .line 523 */
v4 = (( com.android.server.am.ProcessStateRecord ) v0 ).getSetAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
v5 = (( com.android.server.am.ProcessStateRecord ) v0 ).getSetProcState ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetProcState()I
/* .line 524 */
(( com.android.server.am.ProcessStateRecord ) v0 ).getAdjType ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getAdjType()Ljava/lang/String;
(( com.android.server.am.ProcessStateRecord ) v0 ).makeAdjReason ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->makeAdjReason()Ljava/lang/String;
/* move-object v1, v8 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/am/ProcessMemInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;)V */
/* .line 522 */
(( java.util.ArrayList ) p1 ).add ( v8 ); // invoke-virtual {p1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 525 */
return;
} // .end method
static void lambda$dumpCpuInfo$1 ( java.lang.Runnable p0 ) { //synthethic
/* .locals 2 */
/* .param p0, "runnable" # Ljava/lang/Runnable; */
/* .line 452 */
/* new-instance v0, Ljava/lang/Thread; */
final String v1 = "CPU-Dumper"; // const-string v1, "CPU-Dumper"
/* invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V */
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
return;
} // .end method
static Boolean lambda$getMemInfo$0 ( com.android.server.am.ProcessRecord p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "rec" # Lcom/android/server/am/ProcessRecord; */
/* .line 406 */
(( com.android.server.am.ProcessRecord ) p0 ).getThread ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void reportPssRecordIfNeeded ( java.lang.String p0, java.lang.String p1, Long p2 ) {
/* .locals 4 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "pss" # J */
/* .line 318 */
v0 = this.pssThresholdMap;
final String v1 = "Package."; // const-string v1, "Package."
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.util.HashMap ) v0 ).containsKey ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 319 */
v0 = this.pssThresholdMap;
(( java.util.HashMap ) v0 ).get ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* int-to-long v2, v0 */
/* cmp-long v0, p3, v2 */
/* if-lez v0, :cond_1 */
/* .line 320 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.am.AppProfilerImpl ) p0 ).reportPssRecord ( v0, p2, p3, p4 ); // invoke-virtual {p0, v0, p2, p3, p4}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V
/* .line 323 */
} // :cond_0
/* const-wide/32 v2, 0x4b000 */
/* cmp-long v0, p3, v2 */
/* if-lez v0, :cond_1 */
/* .line 324 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.am.AppProfilerImpl ) p0 ).reportPssRecord ( v0, p2, p3, p4 ); // invoke-virtual {p0, v0, p2, p3, p4}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V
/* .line 327 */
} // :cond_1
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void checkMemoryPsi ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "watchdog" # Z */
/* .line 532 */
v0 = (( com.android.server.am.AppProfilerImpl ) p0 ).isSystemLowMemPsiCritical ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMemPsiCritical()Z
/* if-nez v0, :cond_0 */
/* .line 533 */
return;
/* .line 535 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 537 */
/* const/16 v0, 0x6d */
com.android.server.ScoutHelper .doSysRqInterface ( v0 );
/* .line 539 */
} // :cond_1
/* const/16 v0, 0x77 */
com.android.server.ScoutHelper .doSysRqInterface ( v0 );
/* .line 540 */
/* const/16 v0, 0x6c */
com.android.server.ScoutHelper .doSysRqInterface ( v0 );
/* .line 541 */
int v0 = 0; // const/4 v0, 0x0
(( com.android.server.am.AppProfilerImpl ) p0 ).getMemInfo ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppProfilerImpl;->getMemInfo(Z)Ljava/lang/String;
/* .line 542 */
/* .local v0, "meminfo":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Critical memory pressure, dumping memory info \n"; // const-string v2, "Critical memory pressure, dumping memory info \n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AppProfilerImpl"; // const-string v2, "AppProfilerImpl"
android.util.Slog .i ( v2,v1 );
/* .line 544 */
} // .end local v0 # "meminfo":Ljava/lang/String;
} // :goto_0
return;
} // .end method
public Boolean dumpCpuInfo ( com.android.server.utils.PriorityDump$PriorityDumper p0, java.io.FileDescriptor p1, java.io.PrintWriter p2, java.lang.String[] p3 ) {
/* .locals 8 */
/* .param p1, "dumper" # Lcom/android/server/utils/PriorityDump$PriorityDumper; */
/* .param p2, "fd" # Ljava/io/FileDescriptor; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .line 445 */
/* new-instance v0, Ljava/util/concurrent/FutureTask; */
/* new-instance v7, Lcom/android/server/am/AppProfilerImpl$1; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/AppProfilerImpl$1;-><init>(Lcom/android/server/am/AppProfilerImpl;Lcom/android/server/utils/PriorityDump$PriorityDumper;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V */
/* invoke-direct {v0, v7}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V */
/* .line 452 */
/* .local v0, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<*>;" */
/* new-instance v1, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda2; */
/* invoke-direct {v1}, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda2;-><init>()V */
/* .line 453 */
/* .local v1, "executor":Ljava/util/concurrent/Executor; */
/* .line 455 */
int v2 = 1; // const/4 v2, 0x1
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/am/AppProfilerImpl;->getDumpCpuTimeoutThreshold()J */
/* move-result-wide v3 */
v5 = java.util.concurrent.TimeUnit.MILLISECONDS;
(( java.util.concurrent.FutureTask ) v0 ).get ( v3, v4, v5 ); // invoke-virtual {v0, v3, v4, v5}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/InterruptedException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 460 */
/* :catch_0 */
/* move-exception v3 */
/* .line 461 */
/* .local v3, "e":Ljava/util/concurrent/TimeoutException; */
final String v4 = "AppProfilerImpl"; // const-string v4, "AppProfilerImpl"
final String v5 = "dump cpuinfo timeout, interrupted!"; // const-string v5, "dump cpuinfo timeout, interrupted!"
android.util.Slog .w ( v4,v5 );
/* .line 462 */
(( java.util.concurrent.FutureTask ) v0 ).cancel ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z
/* .line 463 */
v4 = android.os.Binder .getCallingPid ( );
/* .line 464 */
/* .local v4, "callingPid":I */
com.android.server.ScoutHelper .getProcessCmdline ( v4 );
/* .line 465 */
/* .local v5, "procName":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v6, :cond_0 */
final String v6 = "dumpsys"; // const-string v6, "dumpsys"
v6 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 466 */
android.os.Process .killProcess ( v4 );
/* .line 458 */
} // .end local v3 # "e":Ljava/util/concurrent/TimeoutException;
} // .end local v4 # "callingPid":I
} // .end local v5 # "procName":Ljava/lang/String;
/* :catch_1 */
/* move-exception v3 */
/* .line 459 */
/* .local v3, "e":Ljava/lang/InterruptedException; */
(( java.lang.InterruptedException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V
} // .end local v3 # "e":Ljava/lang/InterruptedException;
/* .line 456 */
/* :catch_2 */
/* move-exception v3 */
/* .line 457 */
/* .local v3, "e":Ljava/util/concurrent/ExecutionException; */
(( java.util.concurrent.ExecutionException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V
/* .line 468 */
} // .end local v3 # "e":Ljava/util/concurrent/ExecutionException;
} // :goto_0
/* nop */
/* .line 469 */
} // :cond_0
} // :goto_1
} // .end method
public void dumpProcsMemInfo ( ) {
/* .locals 6 */
/* .line 397 */
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.am.AppProfilerImpl ) p0 ).getMemInfo ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/AppProfilerImpl;->getMemInfo(Z)Ljava/lang/String;
/* .line 398 */
/* .local v0, "dropboxStr":Ljava/lang/String; */
final String v1 = "scout_procs"; // const-string v1, "scout_procs"
int v2 = 3; // const/4 v2, 0x3
com.miui.server.stability.ScoutMemoryUtils .getExceptionFile ( v1,v2 );
/* .line 400 */
/* .local v1, "exFile":Ljava/io/File; */
v3 = com.miui.server.stability.ScoutMemoryUtils .dumpInfoToFile ( v0,v1 );
/* .line 401 */
/* .local v3, "suc":Z */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Dump processes memory to file: "; // const-string v5, "Dump processes memory to file: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = ".Succeeded? "; // const-string v5, ".Succeeded? "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "AppProfilerImpl"; // const-string v5, "AppProfilerImpl"
android.util.Slog .w ( v5,v4 );
/* .line 402 */
int v4 = 5; // const/4 v4, 0x5
com.miui.server.stability.ScoutMemoryUtils .deleteOldFiles ( v2,v4 );
/* .line 403 */
return;
} // .end method
public java.lang.String getMemInfo ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "isDropbox" # Z */
/* .line 406 */
/* new-instance v0, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/am/AppProfilerImpl$$ExternalSyntheticLambda0;-><init>()V */
/* invoke-direct {p0, v0}, Lcom/android/server/am/AppProfilerImpl;->collectProcessMemInfos(Ljava/util/function/Predicate;)Ljava/util/ArrayList; */
/* .line 407 */
/* .local v0, "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;" */
final String v1 = "Scout low memory"; // const-string v1, "Scout low memory"
/* .line 408 */
/* .local v1, "title":Ljava/lang/String; */
/* new-instance v2, Lcom/android/server/am/MemUsageBuilder; */
v3 = this.mService;
v3 = this.mAppProfiler;
final String v4 = "Scout low memory"; // const-string v4, "Scout low memory"
/* invoke-direct {v2, v3, v0, v4}, Lcom/android/server/am/MemUsageBuilder;-><init>(Lcom/android/server/am/AppProfiler;Ljava/util/ArrayList;Ljava/lang/String;)V */
/* .line 409 */
/* .local v2, "builder":Lcom/android/server/am/MemUsageBuilder; */
(( com.android.server.am.MemUsageBuilder ) v2 ).buildAll ( ); // invoke-virtual {v2}, Lcom/android/server/am/MemUsageBuilder;->buildAll()V
/* .line 410 */
/* invoke-direct {p0, v2, p1}, Lcom/android/server/am/AppProfilerImpl;->buildMemInfo(Lcom/android/server/am/MemUsageBuilder;Z)Ljava/lang/String; */
} // .end method
public void init ( com.android.server.am.ActivityManagerService p0 ) {
/* .locals 2 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .line 113 */
this.mService = p1;
/* .line 114 */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
v1 = this.mContext;
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v0 ).init ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
/* .line 115 */
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
v1 = this.mContext;
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v0 ).init ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
/* .line 116 */
/* invoke-direct {p0}, Lcom/android/server/am/AppProfilerImpl;->initPssThresholdMap()V */
/* .line 117 */
return;
} // .end method
public Boolean isSystemLowMem ( ) {
/* .locals 10 */
/* .line 330 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v0, v0, [J */
/* .line 331 */
/* .local v0, "longs":[J */
v1 = com.android.server.am.AppProfilerImpl.MEMINFO_FORMAT;
int v2 = 0; // const/4 v2, 0x0
final String v3 = "/proc/meminfo"; // const-string v3, "/proc/meminfo"
v1 = android.os.Process .readProcFile ( v3,v1,v2,v0,v2 );
final String v2 = "AppProfilerImpl"; // const-string v2, "AppProfilerImpl"
int v3 = 0; // const/4 v3, 0x0
/* if-nez v1, :cond_0 */
/* .line 332 */
final String v1 = "Read file /proc/meminfo failed!"; // const-string v1, "Read file /proc/meminfo failed!"
android.util.Slog .e ( v2,v1 );
/* .line 333 */
/* .line 335 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* aget-wide v4, v0, v1 */
/* .line 336 */
/* .local v4, "memAvailableKb":J */
/* const-wide/16 v6, 0x0 */
/* cmp-long v1, v4, v6 */
/* if-nez v1, :cond_1 */
/* .line 337 */
final String v1 = "MemAvailable is 0! This should never happen!"; // const-string v1, "MemAvailable is 0! This should never happen!"
android.util.Slog .e ( v2,v1 );
/* .line 338 */
/* .line 340 */
} // :cond_1
/* aget-wide v1, v0, v3 */
/* .line 345 */
/* .local v1, "memTotalKb":J */
/* div-long v6, v1, v4 */
/* const-wide/16 v8, 0xa */
/* cmp-long v6, v6, v8 */
/* if-ltz v6, :cond_2 */
int v3 = 1; // const/4 v3, 0x1
} // :cond_2
} // .end method
public Boolean isSystemLowMemPsi ( ) {
/* .locals 3 */
/* .line 416 */
try { // :try_start_0
v0 = com.android.server.PsiParser$ResourceType.MEM;
com.android.server.PsiParser .currentParsedPsiState ( v0 );
/* .line 419 */
/* .local v0, "psi":Lcom/android/server/PsiParser$Psi; */
v1 = this.full;
/* iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg10:F */
/* const/high16 v2, 0x40000000 # 2.0f */
/* cmpl-float v1, v1, v2 */
/* if-gtz v1, :cond_1 */
v1 = this.some;
/* iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg10:F */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* const/high16 v2, 0x41200000 # 10.0f */
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
/* .line 420 */
} // .end local v0 # "psi":Lcom/android/server/PsiParser$Psi;
/* :catch_0 */
/* move-exception v0 */
/* .line 422 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Get psi failed: "; // const-string v2, "Get psi failed: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ".Use isSystemLowMem instead."; // const-string v2, ".Use isSystemLowMem instead."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AppProfilerImpl"; // const-string v2, "AppProfilerImpl"
android.util.Slog .w ( v2,v1 );
/* .line 423 */
v1 = (( com.android.server.am.AppProfilerImpl ) p0 ).isSystemLowMem ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMem()Z
} // .end method
public Boolean isSystemLowMemPsiCritical ( ) {
/* .locals 4 */
/* .line 430 */
try { // :try_start_0
v0 = com.android.server.PsiParser$ResourceType.MEM;
com.android.server.PsiParser .currentParsedPsiState ( v0 );
/* .line 431 */
/* .local v0, "psi":Lcom/android/server/PsiParser$Psi; */
v1 = this.some;
/* iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg10:F */
/* int-to-float v3, v2 */
/* cmpl-float v1, v1, v3 */
/* if-gez v1, :cond_1 */
v1 = this.some;
/* iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg60:F */
/* int-to-float v2, v2 */
/* cmpl-float v1, v1, v2 */
/* if-gez v1, :cond_1 */
v1 = this.full;
/* iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg10:F */
/* int-to-float v3, v2 */
/* cmpl-float v1, v1, v3 */
/* if-gez v1, :cond_1 */
v1 = this.full;
/* iget v1, v1, Lcom/android/server/PsiParser$Prop;->avg60:F */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* int-to-float v2, v2 */
/* cmpl-float v1, v1, v2 */
/* if-ltz v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
/* .line 435 */
} // .end local v0 # "psi":Lcom/android/server/PsiParser$Psi;
/* :catch_0 */
/* move-exception v0 */
/* .line 437 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Get psi failed: "; // const-string v2, "Get psi failed: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ".Use isSystemLowMem instead."; // const-string v2, ".Use isSystemLowMem instead."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AppProfilerImpl"; // const-string v2, "AppProfilerImpl"
android.util.Slog .w ( v2,v1 );
/* .line 438 */
v1 = (( com.android.server.am.AppProfilerImpl ) p0 ).isSystemLowMem ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMem()Z
} // .end method
public void reportMemUsage ( java.util.ArrayList p0 ) {
/* .locals 21 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/ProcessMemInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 366 */
/* .local p1, "memInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessMemInfo;>;" */
/* move-object/from16 v1, p0 */
/* new-instance v0, Lcom/android/server/am/ScoutMeminfo; */
/* invoke-direct {v0}, Lcom/android/server/am/ScoutMeminfo;-><init>()V */
/* move-object v2, v0 */
/* .line 367 */
/* .local v2, "scoutInfo":Lcom/android/server/am/ScoutMeminfo; */
final String v3 = "Low on memory"; // const-string v3, "Low on memory"
/* .line 368 */
/* .local v3, "title":Ljava/lang/String; */
/* new-instance v0, Lcom/android/server/am/MemUsageBuilder; */
v4 = this.mService;
v4 = this.mAppProfiler;
final String v5 = "Low on memory"; // const-string v5, "Low on memory"
/* move-object/from16 v6, p1 */
/* invoke-direct {v0, v4, v6, v5}, Lcom/android/server/am/MemUsageBuilder;-><init>(Lcom/android/server/am/AppProfiler;Ljava/util/ArrayList;Ljava/lang/String;)V */
/* move-object v4, v0 */
/* .line 369 */
/* .local v4, "builder":Lcom/android/server/am/MemUsageBuilder; */
(( com.android.server.am.MemUsageBuilder ) v4 ).setScoutInfo ( v2 ); // invoke-virtual {v4, v2}, Lcom/android/server/am/MemUsageBuilder;->setScoutInfo(Lcom/android/server/am/ScoutMeminfo;)V
/* .line 370 */
(( com.android.server.am.MemUsageBuilder ) v4 ).buildAll ( ); // invoke-virtual {v4}, Lcom/android/server/am/MemUsageBuilder;->buildAll()V
/* .line 372 */
final String v0 = "AppProfilerImpl"; // const-string v0, "AppProfilerImpl"
final String v5 = "Low on memory:"; // const-string v5, "Low on memory:"
android.util.Slog .i ( v0,v5 );
/* .line 373 */
final String v0 = "AppProfilerImpl"; // const-string v0, "AppProfilerImpl"
v5 = this.shortNative;
android.util.Slog .i ( v0,v5 );
/* .line 374 */
final String v0 = "AppProfilerImpl"; // const-string v0, "AppProfilerImpl"
v5 = this.fullJava;
android.util.Slog .i ( v0,v5 );
/* .line 375 */
final String v0 = "AppProfilerImpl"; // const-string v0, "AppProfilerImpl"
v5 = this.summary;
android.util.Slog .i ( v0,v5 );
/* .line 377 */
/* iget-wide v7, v4, Lcom/android/server/am/MemUsageBuilder;->totalPss:J */
(( com.android.server.am.ScoutMeminfo ) v2 ).setTotalPss ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Lcom/android/server/am/ScoutMeminfo;->setTotalPss(J)V
/* .line 378 */
/* iget-wide v7, v4, Lcom/android/server/am/MemUsageBuilder;->totalSwapPss:J */
(( com.android.server.am.ScoutMeminfo ) v2 ).setTotalSwapPss ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Lcom/android/server/am/ScoutMeminfo;->setTotalSwapPss(J)V
/* .line 379 */
/* iget-wide v7, v4, Lcom/android/server/am/MemUsageBuilder;->cachedPss:J */
(( com.android.server.am.ScoutMeminfo ) v2 ).setCachedPss ( v7, v8 ); // invoke-virtual {v2, v7, v8}, Lcom/android/server/am/ScoutMeminfo;->setCachedPss(J)V
/* .line 380 */
com.android.server.am.AppProfilerStub .getInstance ( );
/* .line 382 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {v1, v4, v0}, Lcom/android/server/am/AppProfilerImpl;->buildMemInfo(Lcom/android/server/am/MemUsageBuilder;Z)Ljava/lang/String; */
/* .line 383 */
/* .local v5, "dropboxStr":Ljava/lang/String; */
/* const/16 v0, 0x51 */
com.android.internal.util.FrameworkStatsLog .write ( v0 );
/* .line 384 */
v7 = this.mService;
final String v8 = "lowmem"; // const-string v8, "lowmem"
int v9 = 0; // const/4 v9, 0x0
/* const-string/jumbo v10, "system_server" */
int v11 = 0; // const/4 v11, 0x0
int v12 = 0; // const/4 v12, 0x0
int v13 = 0; // const/4 v13, 0x0
v14 = this.subject;
/* const/16 v16, 0x0 */
/* const/16 v17, 0x0 */
/* const/16 v18, 0x0 */
/* const/16 v19, 0x0 */
/* const/16 v20, 0x0 */
/* move-object v15, v5 */
/* invoke-virtual/range {v7 ..v20}, Lcom/android/server/am/ActivityManagerService;->addErrorToDropBox(Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Landroid/app/ApplicationErrorReport$CrashInfo;Ljava/lang/Float;Landroid/os/incremental/IncrementalMetrics;Ljava/util/UUID;)V */
/* .line 387 */
v7 = this.mService;
/* monitor-enter v7 */
/* .line 388 */
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v8 */
/* .line 389 */
/* .local v8, "now":J */
/* iget-wide v10, v1, Lcom/android/server/am/AppProfilerImpl;->mLastMemUsageReportTime:J */
/* cmp-long v0, v10, v8 */
/* if-gez v0, :cond_0 */
/* .line 390 */
/* iput-wide v8, v1, Lcom/android/server/am/AppProfilerImpl;->mLastMemUsageReportTime:J */
/* .line 392 */
} // .end local v8 # "now":J
} // :cond_0
/* monitor-exit v7 */
/* .line 393 */
return;
/* .line 392 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v7 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
public void reportMemoryStandardProcessControlKillMessage ( java.lang.String p0, Integer p1, Integer p2, Long p3 ) {
/* .locals 7 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "pid" # I */
/* .param p3, "uid" # I */
/* .param p4, "pss" # J */
/* .line 195 */
try { // :try_start_0
com.miui.daemon.performance.PerfShielderManager .getService ( );
/* .line 196 */
/* .local v0, "perfShielder":Lcom/android/internal/app/IPerfShielder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 197 */
/* move-object v1, v0 */
/* move-object v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move-wide v5, p4 */
/* invoke-interface/range {v1 ..v6}, Lcom/android/internal/app/IPerfShielder;->reportKillMessage(Ljava/lang/String;IIJ)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 200 */
} // .end local v0 # "perfShielder":Lcom/android/internal/app/IPerfShielder;
} // :cond_0
/* .line 199 */
/* :catch_0 */
/* move-exception v0 */
/* .line 201 */
} // :goto_0
return;
} // .end method
public void reportOomMemRecordIfNeeded ( com.android.server.am.ActivityManagerService p0, com.android.internal.os.ProcessCpuTracker p1 ) {
/* .locals 19 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "processCpuTracker" # Lcom/android/internal/os/ProcessCpuTracker; */
/* .line 210 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, p2 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* .line 211 */
/* .local v4, "now":J */
/* iget-wide v6, v1, Lcom/android/server/am/AppProfilerImpl;->mLastReportOomMemTime:J */
/* sub-long v6, v4, v6 */
/* const-wide/32 v8, 0x36ee80 */
/* cmp-long v0, v6, v8 */
/* if-gez v0, :cond_0 */
/* .line 212 */
return;
/* .line 214 */
} // :cond_0
/* iput-wide v4, v1, Lcom/android/server/am/AppProfilerImpl;->mLastReportOomMemTime:J */
/* .line 216 */
/* new-instance v0, Landroid/util/SparseIntArray; */
/* invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V */
/* move-object v6, v0 */
/* .line 217 */
/* .local v6, "procs":Landroid/util/SparseIntArray; */
/* monitor-enter p1 */
/* .line 218 */
try { // :try_start_0
v0 = this.mProcessList;
(( com.android.server.am.ProcessList ) v0 ).getLruProcessesLOSP ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
/* .line 219 */
/* .local v0, "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
v7 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v7, v7, -0x1 */
/* .local v7, "i":I */
} // :goto_0
/* if-ltz v7, :cond_1 */
/* .line 220 */
(( java.util.ArrayList ) v0 ).get ( v7 ); // invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v8, Lcom/android/server/am/ProcessRecord; */
/* .line 221 */
/* .local v8, "r":Lcom/android/server/am/ProcessRecord; */
/* iget v9, v8, Lcom/android/server/am/ProcessRecord;->mPid:I */
v10 = this.mState;
v10 = (( com.android.server.am.ProcessStateRecord ) v10 ).getSetAdjWithServices ( ); // invoke-virtual {v10}, Lcom/android/server/am/ProcessStateRecord;->getSetAdjWithServices()I
(( android.util.SparseIntArray ) v6 ).put ( v9, v10 ); // invoke-virtual {v6, v9, v10}, Landroid/util/SparseIntArray;->put(II)V
/* .line 219 */
} // .end local v8 # "r":Lcom/android/server/am/ProcessRecord;
/* add-int/lit8 v7, v7, -0x1 */
/* .line 223 */
} // .end local v0 # "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v7 # "i":I
} // :cond_1
/* monitor-exit p1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 225 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* move-object v7, v0 */
/* .line 226 */
/* .local v7, "nativeProcs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;" */
/* invoke-virtual/range {p1 ..p1}, Lcom/android/server/am/ActivityManagerService;->updateCpuStatsNow()V */
/* .line 227 */
/* monitor-enter p2 */
/* .line 228 */
try { // :try_start_1
v0 = /* invoke-virtual/range {p2 ..p2}, Lcom/android/internal/os/ProcessCpuTracker;->countStats()I */
/* .line 229 */
/* .local v0, "N":I */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "i":I */
} // :goto_1
/* const-wide/16 v9, 0x0 */
/* if-ge v8, v0, :cond_3 */
/* .line 230 */
(( com.android.internal.os.ProcessCpuTracker ) v3 ).getStats ( v8 ); // invoke-virtual {v3, v8}, Lcom/android/internal/os/ProcessCpuTracker;->getStats(I)Lcom/android/internal/os/ProcessCpuTracker$Stats;
/* .line 231 */
/* .local v11, "st":Lcom/android/internal/os/ProcessCpuTracker$Stats; */
/* iget-wide v12, v11, Lcom/android/internal/os/ProcessCpuTracker$Stats;->vsize:J */
/* cmp-long v9, v12, v9 */
/* if-lez v9, :cond_2 */
/* iget v9, v11, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
v9 = (( android.util.SparseIntArray ) v6 ).indexOfKey ( v9 ); // invoke-virtual {v6, v9}, Landroid/util/SparseIntArray;->indexOfKey(I)I
/* if-gez v9, :cond_2 */
/* .line 232 */
/* iget v9, v11, Lcom/android/internal/os/ProcessCpuTracker$Stats;->pid:I */
v10 = this.name;
(( android.util.SparseArray ) v7 ).put ( v9, v10 ); // invoke-virtual {v7, v9, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 229 */
} // .end local v11 # "st":Lcom/android/internal/os/ProcessCpuTracker$Stats;
} // :cond_2
/* add-int/lit8 v8, v8, 0x1 */
/* .line 235 */
} // .end local v0 # "N":I
} // .end local v8 # "i":I
} // :cond_3
/* monitor-exit p2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 237 */
v0 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_LABEL;
/* array-length v0, v0 */
/* new-array v0, v0, [J */
/* .line 238 */
/* .local v0, "oomPss":[J */
int v8 = 0; // const/4 v8, 0x0
/* .restart local v8 # "i":I */
} // :goto_2
v11 = (( android.util.SparseIntArray ) v6 ).size ( ); // invoke-virtual {v6}, Landroid/util/SparseIntArray;->size()I
int v12 = 0; // const/4 v12, 0x0
/* if-ge v8, v11, :cond_7 */
/* .line 239 */
v11 = (( android.util.SparseIntArray ) v6 ).keyAt ( v8 ); // invoke-virtual {v6, v8}, Landroid/util/SparseIntArray;->keyAt(I)I
/* .line 240 */
/* .local v11, "pid":I */
v13 = (( android.util.SparseIntArray ) v6 ).get ( v11 ); // invoke-virtual {v6, v11}, Landroid/util/SparseIntArray;->get(I)I
/* .line 241 */
/* .local v13, "oomAdj":I */
android.os.Debug .getPss ( v11,v12,v12 );
/* move-result-wide v14 */
/* .line 242 */
/* .local v14, "myTotalPss":J */
int v12 = 0; // const/4 v12, 0x0
/* .local v12, "oomIndex":I */
} // :goto_3
/* array-length v9, v0 */
/* if-ge v12, v9, :cond_6 */
/* .line 243 */
/* array-length v9, v0 */
/* add-int/lit8 v9, v9, -0x1 */
/* if-eq v12, v9, :cond_5 */
v9 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_ADJ;
/* aget v9, v9, v12 */
/* if-lt v13, v9, :cond_4 */
v9 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_ADJ;
/* add-int/lit8 v10, v12, 0x1 */
/* aget v9, v9, v10 */
/* if-ge v13, v9, :cond_4 */
/* .line 242 */
} // :cond_4
/* add-int/lit8 v12, v12, 0x1 */
/* const-wide/16 v9, 0x0 */
/* .line 246 */
} // :cond_5
} // :goto_4
/* aget-wide v9, v0, v12 */
/* add-long/2addr v9, v14 */
/* aput-wide v9, v0, v12 */
/* .line 247 */
/* nop */
/* .line 238 */
} // .end local v11 # "pid":I
} // .end local v12 # "oomIndex":I
} // .end local v13 # "oomAdj":I
} // .end local v14 # "myTotalPss":J
} // :cond_6
/* add-int/lit8 v8, v8, 0x1 */
/* const-wide/16 v9, 0x0 */
/* .line 252 */
} // .end local v8 # "i":I
} // :cond_7
int v8 = 0; // const/4 v8, 0x0
/* .restart local v8 # "i":I */
} // :goto_5
v9 = (( android.util.SparseArray ) v7 ).size ( ); // invoke-virtual {v7}, Landroid/util/SparseArray;->size()I
/* if-ge v8, v9, :cond_9 */
/* .line 253 */
v9 = (( android.util.SparseArray ) v7 ).keyAt ( v8 ); // invoke-virtual {v7, v8}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 254 */
/* .local v9, "pid":I */
(( android.util.SparseArray ) v7 ).get ( v9 ); // invoke-virtual {v7, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v10, Ljava/lang/String; */
/* .line 255 */
/* .local v10, "name":Ljava/lang/String; */
android.os.Debug .getPss ( v9,v12,v12 );
/* move-result-wide v13 */
/* .line 256 */
/* .local v13, "myTotalPss":J */
/* const-wide/16 v15, 0x0 */
/* cmp-long v11, v13, v15 */
/* if-nez v11, :cond_8 */
/* .line 257 */
} // :cond_8
int v11 = 0; // const/4 v11, 0x0
/* aget-wide v17, v0, v11 */
/* add-long v17, v17, v13 */
/* aput-wide v17, v0, v11 */
/* .line 259 */
final String v11 = "native"; // const-string v11, "native"
(( com.android.server.am.AppProfilerImpl ) v1 ).reportPssRecord ( v10, v11, v13, v14 ); // invoke-virtual {v1, v10, v11, v13, v14}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V
/* .line 252 */
} // .end local v9 # "pid":I
} // .end local v10 # "name":Ljava/lang/String;
} // .end local v13 # "myTotalPss":J
} // :goto_6
/* add-int/lit8 v8, v8, 0x1 */
/* .line 262 */
} // .end local v8 # "i":I
} // :cond_9
int v8 = 0; // const/4 v8, 0x0
/* .restart local v8 # "i":I */
} // :goto_7
v9 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_LABEL;
/* array-length v9, v9 */
/* if-ge v8, v9, :cond_b */
/* .line 263 */
/* aget-wide v9, v0, v8 */
/* const-wide/16 v11, 0x0 */
/* cmp-long v9, v9, v11 */
/* if-nez v9, :cond_a */
/* .line 264 */
} // :cond_a
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "OomMeminfo."; // const-string v10, "OomMeminfo."
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = com.android.server.am.ActivityManagerService.DUMP_MEM_OOM_LABEL;
/* aget-object v10, v10, v8 */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v10 = "OomMeminfo"; // const-string v10, "OomMeminfo"
/* aget-wide v13, v0, v8 */
(( com.android.server.am.AppProfilerImpl ) v1 ).reportPssRecord ( v9, v10, v13, v14 ); // invoke-virtual {v1, v9, v10, v13, v14}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;J)V
/* .line 262 */
} // :goto_8
/* add-int/lit8 v8, v8, 0x1 */
/* .line 267 */
} // .end local v8 # "i":I
} // :cond_b
return;
/* .line 235 */
} // .end local v0 # "oomPss":[J
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit p2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 223 */
} // .end local v7 # "nativeProcs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
/* :catchall_1 */
/* move-exception v0 */
try { // :try_start_3
/* monitor-exit p1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v0 */
} // .end method
public void reportPackagePss ( com.android.server.am.ActivityManagerService p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 271 */
/* const-wide/16 v0, 0x0 */
/* .line 272 */
/* .local v0, "pkgPss":J */
/* monitor-enter p1 */
/* .line 273 */
try { // :try_start_0
v2 = this.mProcessList;
(( com.android.server.am.ProcessList ) v2 ).getLruProcessesLOSP ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
/* .line 274 */
/* .local v2, "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
v3 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v3, v3, -0x1 */
/* .local v3, "i":I */
} // :goto_0
/* if-ltz v3, :cond_2 */
/* .line 275 */
(( java.util.ArrayList ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/ProcessRecord; */
/* .line 276 */
/* .local v4, "r":Lcom/android/server/am/ProcessRecord; */
v5 = this.info;
v5 = this.packageName;
v5 = (( java.lang.String ) v5 ).equals ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 277 */
v5 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v5 ).getLastPss ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x0 */
/* cmp-long v5, v5, v7 */
/* if-nez v5, :cond_0 */
/* .line 278 */
/* monitor-exit p1 */
return;
/* .line 279 */
} // :cond_0
v5 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v5 ).getLastPss ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v5 */
/* add-long/2addr v0, v5 */
/* .line 274 */
} // .end local v4 # "r":Lcom/android/server/am/ProcessRecord;
} // :cond_1
/* add-int/lit8 v3, v3, -0x1 */
/* .line 282 */
} // .end local v2 # "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v3 # "i":I
} // :cond_2
/* monitor-exit p1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 283 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Package."; // const-string v3, "Package."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v2, p2, v0, v1}, Lcom/android/server/am/AppProfilerImpl;->reportPssRecordIfNeeded(Ljava/lang/String;Ljava/lang/String;J)V */
/* .line 284 */
return;
/* .line 282 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit p1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public void reportPssRecord ( java.lang.String p0, java.lang.String p1, Long p2 ) {
/* .locals 10 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "pss" # J */
/* .line 170 */
final String v0 = "1.0"; // const-string v0, "1.0"
/* .line 171 */
/* .local v0, "versionName":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* .line 173 */
/* .local v1, "versionCode":I */
try { // :try_start_0
final String v2 = "OomMeminfo"; // const-string v2, "OomMeminfo"
v2 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
final String v2 = "native"; // const-string v2, "native"
v2 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_2 */
/* .line 174 */
android.app.AppGlobals .getPackageManager ( );
/* .line 175 */
/* .local v2, "pm":Landroid/content/pm/IPackageManager; */
/* if-nez v2, :cond_0 */
/* .line 176 */
return;
/* .line 179 */
} // :cond_0
v3 = android.app.ActivityManager .getCurrentUser ( );
/* const-wide/16 v4, 0x0 */
/* .line 180 */
/* .local v3, "pi":Landroid/content/pm/PackageInfo; */
/* if-nez v3, :cond_1 */
/* .line 181 */
return;
/* .line 183 */
} // :cond_1
v4 = this.versionName;
/* move-object v0, v4 */
/* .line 184 */
/* iget v4, v3, Landroid/content/pm/PackageInfo;->versionCode:I */
/* move v1, v4 */
/* .line 186 */
} // .end local v2 # "pm":Landroid/content/pm/IPackageManager;
} // .end local v3 # "pi":Landroid/content/pm/PackageInfo;
} // :cond_2
com.miui.daemon.performance.PerfShielderManager .getService ( );
/* move-object v4, p1 */
/* move-object v5, p2 */
/* move-wide v6, p3 */
/* move-object v8, v0 */
/* move v9, v1 */
/* invoke-interface/range {v3 ..v9}, Lcom/android/internal/app/IPerfShielder;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 189 */
/* .line 188 */
/* :catch_0 */
/* move-exception v2 */
/* .line 190 */
} // :goto_0
return;
} // .end method
public void reportScoutLowMemory ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "adj" # I */
/* .line 126 */
/* iget v0, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I */
/* if-ge p1, v0, :cond_0 */
/* .line 127 */
/* iput p1, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I */
/* .line 130 */
} // :cond_0
v0 = this.mService;
/* if-nez v0, :cond_1 */
/* .line 131 */
return;
/* .line 134 */
} // :cond_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 135 */
/* .local v0, "now":J */
final String v2 = "MIUIScout Memory"; // const-string v2, "MIUIScout Memory"
/* const/16 v3, 0x258 */
/* if-ltz p1, :cond_2 */
/* if-gt p1, v3, :cond_2 */
/* iget-wide v4, p0, Lcom/android/server/am/AppProfilerImpl;->mLastDumpProcsMemTime:J */
/* const-wide/32 v6, 0x493e0 */
/* add-long/2addr v4, v6 */
/* cmp-long v4, v0, v4 */
/* if-lez v4, :cond_2 */
/* .line 137 */
v4 = (( com.android.server.am.AppProfilerImpl ) p0 ).isSystemLowMemPsi ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMemPsi()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 138 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Scout dump proc mem, current killed adj= "; // const-string v5, "Scout dump proc mem, current killed adj= "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v4 );
/* .line 139 */
com.android.server.ScoutSystemMonitor .getInstance ( );
int v5 = 1; // const/4 v5, 0x1
(( com.android.server.ScoutSystemMonitor ) v4 ).setWorkMessage ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/ScoutSystemMonitor;->setWorkMessage(I)V
/* .line 140 */
/* iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->mLastDumpProcsMemTime:J */
/* .line 143 */
} // :cond_2
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
v4 = (( com.miui.server.stability.ScoutDisplayMemoryManager ) v4 ).isEnableScoutMemory ( ); // invoke-virtual {v4}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->isEnableScoutMemory()Z
/* if-nez v4, :cond_3 */
/* .line 144 */
return;
/* .line 147 */
} // :cond_3
/* if-ltz p1, :cond_4 */
/* if-gt p1, v3, :cond_4 */
/* iget-wide v3, p0, Lcom/android/server/am/AppProfilerImpl;->mLastMemUsageReportTime:J */
/* const-wide/32 v5, 0x2bf20 */
/* add-long/2addr v3, v5 */
/* cmp-long v3, v0, v3 */
/* if-lez v3, :cond_4 */
/* .line 149 */
v3 = (( com.android.server.am.AppProfilerImpl ) p0 ).isSystemLowMemPsi ( ); // invoke-virtual {p0}, Lcom/android/server/am/AppProfilerImpl;->isSystemLowMemPsi()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 150 */
/* iput-wide v0, p0, Lcom/android/server/am/AppProfilerImpl;->mLastMemUsageReportTime:J */
/* .line 151 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Scout report Low memory,currently killed process adj= "; // const-string v4, "Scout report Low memory,currently killed process adj= "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 153 */
com.android.server.ScoutSystemMonitor .getInstance ( );
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.ScoutSystemMonitor ) v2 ).setWorkMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/ScoutSystemMonitor;->setWorkMessage(I)V
/* .line 155 */
} // :cond_4
return;
} // .end method
public void reportScoutLowMemory ( com.android.server.am.ScoutMeminfo p0 ) {
/* .locals 1 */
/* .param p1, "scoutMeminfo" # Lcom/android/server/am/ScoutMeminfo; */
/* .line 121 */
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v0 ).checkScoutLowMemory ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->checkScoutLowMemory(Lcom/android/server/am/ScoutMeminfo;)V
/* .line 122 */
return;
} // .end method
public void reportScoutLowMemoryState ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "state" # I */
/* .line 159 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p1, v0, :cond_1 */
/* .line 160 */
/* iget v0, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I */
/* const/16 v1, 0x258 */
/* if-gt v0, v1, :cond_0 */
/* .line 161 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 162 */
/* .local v0, "timestamp":J */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
/* iget v3, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I */
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v2 ).reportLmkKill ( v3, v0, v1 ); // invoke-virtual {v2, v3, v0, v1}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportLmkKill(IJ)V
/* .line 164 */
} // .end local v0 # "timestamp":J
} // :cond_0
/* const/16 v0, 0x3e9 */
/* iput v0, p0, Lcom/android/server/am/AppProfilerImpl;->mMinOomScore:I */
/* .line 166 */
} // :cond_1
return;
} // .end method
public Boolean testTrimMemActivityBgWk ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .line 355 */
/* sget-boolean v0, Lcom/android/server/am/AppProfilerImpl;->testTrimMemActivityBg_workaround:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "com.android.app1:android.app.stubs.TrimMemService:trimmem_"; // const-string v0, "com.android.app1:android.app.stubs.TrimMemService:trimmem_"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 356 */
final String v0 = "AppProfilerImpl"; // const-string v0, "AppProfilerImpl"
/* const-string/jumbo v1, "special case." */
android.util.Slog .i ( v0,v1 );
/* .line 357 */
int v0 = 1; // const/4 v0, 0x1
/* .line 359 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void updateCameraForegroundState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "isCameraForeground" # Z */
/* .line 349 */
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v0 ).updateCameraForegroundState ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->updateCameraForegroundState(Z)V
/* .line 350 */
return;
} // .end method
