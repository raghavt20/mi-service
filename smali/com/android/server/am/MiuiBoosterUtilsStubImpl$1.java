class com.android.server.am.MiuiBoosterUtilsStubImpl$1 extends android.os.Handler {
	 /* .source "MiuiBoosterUtilsStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->init(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.MiuiBoosterUtilsStubImpl this$0; //synthetic
/* # direct methods */
 com.android.server.am.MiuiBoosterUtilsStubImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/MiuiBoosterUtilsStubImpl; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 327 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 330 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 331 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 337 */
/* :pswitch_0 */
v0 = this.obj;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 338 */
	 v0 = this.obj;
	 /* check-cast v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo; */
	 /* .line 339 */
	 /* .local v0, "info":Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo; */
	 v1 = this.this$0;
	 v2 = 	 com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo .-$$Nest$fgetuid ( v0 );
	 com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo .-$$Nest$fgetreq_tids ( v0 );
	 v4 = 	 com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo .-$$Nest$fgettimeout ( v0 );
	 v5 = 	 com.android.server.am.MiuiBoosterUtilsStubImpl$MiuiBoosterInfo .-$$Nest$fgetlevel ( v0 );
	 (( com.android.server.am.MiuiBoosterUtilsStubImpl ) v1 ).requestThreadPriority ( v2, v3, v4, v5 ); // invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->requestThreadPriority(I[III)I
	 /* .line 341 */
} // .end local v0 # "info":Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;
/* .line 333 */
/* :pswitch_1 */
v0 = this.this$0;
com.android.server.am.MiuiBoosterUtilsStubImpl .-$$Nest$fgetmContext ( v0 );
(( com.android.server.am.MiuiBoosterUtilsStubImpl ) v0 ).registerCloudObserver ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->registerCloudObserver(Landroid/content/Context;)V
/* .line 334 */
v0 = this.this$0;
(( com.android.server.am.MiuiBoosterUtilsStubImpl ) v0 ).updateCloudControlParas ( ); // invoke-virtual {v0}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->updateCloudControlParas()V
/* .line 335 */
/* nop */
/* .line 346 */
} // :cond_0
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
