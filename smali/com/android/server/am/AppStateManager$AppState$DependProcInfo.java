class com.android.server.am.AppStateManager$AppState$DependProcInfo {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager$AppState; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DependProcInfo" */
} // .end annotation
/* # instance fields */
private Integer mPid;
private java.lang.String mProcessName;
private Integer mUid;
final com.android.server.am.AppStateManager$AppState this$1; //synthetic
/* # direct methods */
static Integer -$$Nest$fgetmPid ( com.android.server.am.AppStateManager$AppState$DependProcInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mPid:I */
} // .end method
static java.lang.String -$$Nest$fgetmProcessName ( com.android.server.am.AppStateManager$AppState$DependProcInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessName;
} // .end method
static Integer -$$Nest$fgetmUid ( com.android.server.am.AppStateManager$AppState$DependProcInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mUid:I */
} // .end method
private com.android.server.am.AppStateManager$AppState$DependProcInfo ( ) {
/* .locals 1 */
/* .param p1, "this$1" # Lcom/android/server/am/AppStateManager$AppState; */
/* .param p2, "uid" # I */
/* .param p3, "pid" # I */
/* .param p4, "processName" # Ljava/lang/String; */
/* .line 3240 */
this.this$1 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 3237 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mUid:I */
/* .line 3238 */
/* iput v0, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mPid:I */
/* .line 3241 */
/* iput p2, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mUid:I */
/* .line 3242 */
/* iput p3, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mPid:I */
/* .line 3243 */
this.mProcessName = p4;
/* .line 3244 */
return;
} // .end method
 com.android.server.am.AppStateManager$AppState$DependProcInfo ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;-><init>(Lcom/android/server/am/AppStateManager$AppState;IILjava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 3248 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mProcessName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "("; // const-string v1, "("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$DependProcInfo;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
