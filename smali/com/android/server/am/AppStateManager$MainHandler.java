class com.android.server.am.AppStateManager$MainHandler extends android.os.Handler {
	 /* .source "AppStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MainHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.AppStateManager this$0; //synthetic
/* # direct methods */
 com.android.server.am.AppStateManager$MainHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 3314 */
this.this$0 = p1;
/* .line 3315 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 3316 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 9 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 3320 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 3321 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_5 */
/* .line 3353 */
/* :pswitch_0 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 3354 */
/* .local v0, "userId":I */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmAppLock ( v1 );
/* monitor-enter v1 */
/* .line 3355 */
try { // :try_start_0
	 /* new-instance v2, Ljava/util/ArrayList; */
	 /* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 3356 */
	 /* .local v2, "targerUids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
	 int v3 = 0; // const/4 v3, 0x0
	 /* .local v3, "i":I */
} // :goto_0
v4 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmActiveApps ( v4 );
v4 = (( com.android.server.am.AppStateManager$ActiveApps ) v4 ).size ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I
/* if-ge v3, v4, :cond_1 */
/* .line 3357 */
v4 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmActiveApps ( v4 );
(( com.android.server.am.AppStateManager$ActiveApps ) v4 ).valueAt ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 3358 */
/* .local v4, "app":Lcom/android/server/am/AppStateManager$AppState; */
v5 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmUid ( v4 );
v5 = android.os.UserHandle .getUserId ( v5 );
/* if-ne v5, v0, :cond_0 */
v5 = (( com.android.server.am.AppStateManager$AppState ) v4 ).isAlive ( ); // invoke-virtual {v4}, Lcom/android/server/am/AppStateManager$AppState;->isAlive()Z
/* if-nez v5, :cond_0 */
/* .line 3359 */
v5 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmUid ( v4 );
java.lang.Integer .valueOf ( v5 );
(( java.util.ArrayList ) v2 ).add ( v5 ); // invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 3356 */
} // .end local v4 # "app":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 3362 */
} // .end local v3 # "i":I
} // :cond_1
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 3363 */
/* .local v4, "uid":I */
v5 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmActiveApps ( v5 );
(( com.android.server.am.AppStateManager$ActiveApps ) v5 ).remove ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/am/AppStateManager$ActiveApps;->remove(I)V
/* .line 3364 */
} // .end local v4 # "uid":I
/* .line 3365 */
} // .end local v2 # "targerUids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // :cond_2
/* monitor-exit v1 */
/* .line 3366 */
/* goto/16 :goto_5 */
/* .line 3365 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 3333 */
} // .end local v0 # "userId":I
/* :pswitch_1 */
v0 = this.obj;
/* check-cast v0, Lcom/android/server/am/AppStateManager$AppState; */
/* .line 3334 */
/* .local v0, "fromApp":Lcom/android/server/am/AppStateManager$AppState; */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* .line 3335 */
/* .local v1, "bundle":Landroid/os/Bundle; */
final String v2 = "reason"; // const-string v2, "reason"
(( android.os.Bundle ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 3336 */
/* .local v2, "reason":Ljava/lang/String; */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 3337 */
/* .local v3, "targetApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState;>;" */
v4 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmAppLock ( v4 );
/* monitor-enter v4 */
/* .line 3338 */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_2
try { // :try_start_1
v6 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmActiveApps ( v6 );
v6 = (( com.android.server.am.AppStateManager$ActiveApps ) v6 ).size ( ); // invoke-virtual {v6}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I
/* if-ge v5, v6, :cond_4 */
/* .line 3339 */
v6 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmActiveApps ( v6 );
(( com.android.server.am.AppStateManager$ActiveApps ) v6 ).valueAt ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 3340 */
/* .local v6, "app":Lcom/android/server/am/AppStateManager$AppState; */
/* if-eq v6, v0, :cond_3 */
v7 = com.android.server.am.AppStateManager$AppState .-$$Nest$fgetmUid ( v6 );
v7 = android.os.Process .isCoreUid ( v7 );
/* if-nez v7, :cond_3 */
/* .line 3341 */
v7 = (( com.android.server.am.AppStateManager$AppState ) v6 ).getCurrentState ( ); // invoke-virtual {v6}, Lcom/android/server/am/AppStateManager$AppState;->getCurrentState()I
int v8 = 3; // const/4 v8, 0x3
/* if-lt v7, v8, :cond_3 */
/* .line 3342 */
v7 = (( com.android.server.am.AppStateManager$AppState ) v6 ).getCurrentState ( ); // invoke-virtual {v6}, Lcom/android/server/am/AppStateManager$AppState;->getCurrentState()I
int v8 = 5; // const/4 v8, 0x5
/* if-gt v7, v8, :cond_3 */
/* .line 3343 */
(( java.util.ArrayList ) v3 ).add ( v6 ); // invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 3338 */
} // .end local v6 # "app":Lcom/android/server/am/AppStateManager$AppState;
} // :cond_3
/* add-int/lit8 v5, v5, 0x1 */
/* .line 3346 */
} // .end local v5 # "i":I
} // :cond_4
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 3347 */
(( java.util.ArrayList ) v3 ).iterator ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_5
/* check-cast v5, Lcom/android/server/am/AppStateManager$AppState; */
/* .line 3348 */
/* .local v5, "app":Lcom/android/server/am/AppStateManager$AppState; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "hibernate all "; // const-string v7, "hibernate all "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.am.AppStateManager$AppState .-$$Nest$mhibernateAllInactive ( v5,v6 );
/* .line 3349 */
} // .end local v5 # "app":Lcom/android/server/am/AppStateManager$AppState;
/* .line 3350 */
} // :cond_5
/* .line 3346 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_2
/* monitor-exit v4 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v5 */
/* .line 3323 */
} // .end local v0 # "fromApp":Lcom/android/server/am/AppStateManager$AppState;
} // .end local v1 # "bundle":Landroid/os/Bundle;
} // .end local v2 # "reason":Ljava/lang/String;
} // .end local v3 # "targetApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/AppStateManager$AppState;>;"
/* :pswitch_2 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/String; */
/* .line 3324 */
/* .local v0, "reason":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmAppLock ( v1 );
/* monitor-enter v1 */
/* .line 3325 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_4
try { // :try_start_3
v3 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmActiveApps ( v3 );
v3 = (( com.android.server.am.AppStateManager$ActiveApps ) v3 ).size ( ); // invoke-virtual {v3}, Lcom/android/server/am/AppStateManager$ActiveApps;->size()I
/* if-ge v2, v3, :cond_6 */
/* .line 3326 */
v3 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$fgetmActiveApps ( v3 );
(( com.android.server.am.AppStateManager$ActiveApps ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$ActiveApps;->valueAt(I)Lcom/android/server/am/AppStateManager$AppState;
/* .line 3327 */
/* .local v3, "app":Lcom/android/server/am/AppStateManager$AppState; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "hibernate all "; // const-string v5, "hibernate all "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.am.AppStateManager$AppState .-$$Nest$mhibernateAllIfNeeded ( v3,v4 );
/* .line 3325 */
} // .end local v3 # "app":Lcom/android/server/am/AppStateManager$AppState;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 3329 */
} // .end local v2 # "i":I
} // :cond_6
/* monitor-exit v1 */
/* .line 3330 */
/* .line 3329 */
/* :catchall_2 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* throw v2 */
/* .line 3371 */
} // .end local v0 # "reason":Ljava/lang/String;
} // :goto_5
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
