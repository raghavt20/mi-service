.class final Lcom/android/server/am/MiuiMemServiceHelper;
.super Landroid/content/BroadcastReceiver;
.source "MiuiMemServiceHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;
    }
.end annotation


# static fields
.field private static final CHARGING_RECLAIM_DELAYED:J = 0xea60L

.field private static final DEBUG:Z

.field private static final SCREENOFF_RECLAIM_DELAYED:J = 0x124f80L

.field private static final TAG:Ljava/lang/String; = "MiuiMemoryService"


# instance fields
.field private mFilter:Landroid/content/IntentFilter;

.field private mIsCharging:Z

.field private mIsScreenOff:Z

.field private mMemService:Lcom/android/server/am/MiuiMemoryService;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mSetIsSatisfied:Z

.field private mWorkHandler:Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;

.field final mWorkThread:Landroid/os/HandlerThread;


# direct methods
.method static bridge synthetic -$$Nest$fgetmMemService(Lcom/android/server/am/MiuiMemServiceHelper;)Lcom/android/server/am/MiuiMemoryService;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mMemService:Lcom/android/server/am/MiuiMemoryService;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 21
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    sput-boolean v0, Lcom/android/server/am/MiuiMemServiceHelper;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/am/MiuiMemoryService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/android/server/am/MiuiMemoryService;

    .line 52
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 53
    const-class v0, Landroid/os/PowerManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mPowerManager:Landroid/os/PowerManager;

    .line 54
    iput-object p2, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mMemService:Lcom/android/server/am/MiuiMemoryService;

    .line 55
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MiuiMemoryService_Helper"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkThread:Landroid/os/HandlerThread;

    .line 57
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v0, :cond_0

    .line 58
    invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemServiceHelper;->registerBroadcast(Landroid/content/Context;)V

    goto :goto_0

    .line 60
    :cond_0
    const-string v0, "MiuiMemoryService"

    const-string v1, "Register broadcast failed!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :goto_0
    return-void
.end method

.method private registerBroadcast(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 71
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mFilter:Landroid/content/IntentFilter;

    .line 73
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DREAMING_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DREAMING_STOPPED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 85
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 93
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_1
    const-string v1, "android.intent.action.DREAMING_STOPPED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v3

    goto :goto_1

    :sswitch_2
    const-string v1, "android.intent.action.DREAMING_STARTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_1

    :sswitch_3
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    goto :goto_1

    :sswitch_4
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_5
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 114
    return-void

    .line 111
    :pswitch_0
    iput-boolean v3, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsCharging:Z

    .line 112
    goto :goto_2

    .line 108
    :pswitch_1
    iput-boolean v4, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsCharging:Z

    .line 109
    goto :goto_2

    .line 105
    :pswitch_2
    iput-boolean v4, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsScreenOff:Z

    .line 106
    goto :goto_2

    .line 97
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 98
    return-void

    .line 101
    :cond_1
    :pswitch_4
    iput-boolean v3, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsScreenOff:Z

    .line 102
    nop

    .line 117
    :goto_2
    sget-boolean v1, Lcom/android/server/am/MiuiMemServiceHelper;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 118
    iget-boolean v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsCharging:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v5, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsScreenOff:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    filled-new-array {v0, v1, v5}, [Ljava/lang/Object;

    move-result-object v1

    const-string v5, "Received:%s, cur:%s,%s"

    invoke-static {v5, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "MiuiMemoryService"

    invoke-static {v5, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_2
    iget-boolean v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsScreenOff:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkHandler:Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;

    invoke-virtual {v1, v2}, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 122
    iget-object v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkHandler:Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;

    const-wide/32 v5, 0x124f80

    invoke-virtual {v1, v2, v5, v6}, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_3

    .line 125
    :cond_3
    iget-object v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkHandler:Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;

    invoke-virtual {v1, v2}, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;->removeMessages(I)V

    .line 126
    iget-object v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mMemService:Lcom/android/server/am/MiuiMemoryService;

    invoke-virtual {v1}, Lcom/android/server/am/MiuiMemoryService;->interruptProcsCompaction()V

    .line 129
    :goto_3
    iget-boolean v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsCharging:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mIsScreenOff:Z

    if-eqz v1, :cond_4

    move v3, v4

    :cond_4
    move v1, v3

    .line 130
    .local v1, "curIsSatisfied":Z
    iget-boolean v2, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mSetIsSatisfied:Z

    if-eq v1, v2, :cond_6

    .line 131
    iget-object v2, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkHandler:Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;

    invoke-virtual {v2, v4}, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;->removeMessages(I)V

    .line 132
    if-eqz v1, :cond_5

    .line 133
    iget-object v2, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkHandler:Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;

    const-wide/32 v5, 0xea60

    invoke-virtual {v2, v4, v5, v6}, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_4

    .line 136
    :cond_5
    iget-object v2, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mMemService:Lcom/android/server/am/MiuiMemoryService;

    invoke-virtual {v2}, Lcom/android/server/am/MiuiMemoryService;->interruptProcsCompaction()V

    .line 138
    :goto_4
    iput-boolean v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mSetIsSatisfied:Z

    .line 140
    :cond_6
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ed8ea7f -> :sswitch_5
        -0x7073f927 -> :sswitch_4
        -0x56ac2893 -> :sswitch_3
        0xe98bfe6 -> :sswitch_2
        0xf5d1132 -> :sswitch_1
        0x3cbf870b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public startWork()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 66
    new-instance v0, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;

    iget-object v1, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;-><init>(Lcom/android/server/am/MiuiMemServiceHelper;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkHandler:Lcom/android/server/am/MiuiMemServiceHelper$WorkHandler;

    .line 67
    iget-object v0, p0, Lcom/android/server/am/MiuiMemServiceHelper;->mWorkThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/Process;->setThreadGroupAndCpuset(II)V

    .line 68
    return-void
.end method
