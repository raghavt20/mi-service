.class public Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;
.super Landroid/os/Handler;
.source "MemoryFreezeStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryFreezeStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryFreezeStubImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/am/MemoryFreezeStubImpl;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryFreezeStubImpl;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 767
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    .line 768
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 769
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 772
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 779
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$fgetmMemoryFreezeCloud(Lcom/android/server/am/MemoryFreezeStubImpl;)Lcom/android/server/am/MemoryFreezeCloud;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/am/MemoryFreezeStubImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeCloud;->registerCloudWhiteList(Landroid/content/Context;)V

    .line 780
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$fgetmMemoryFreezeCloud(Lcom/android/server/am/MemoryFreezeStubImpl;)Lcom/android/server/am/MemoryFreezeCloud;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/am/MemoryFreezeStubImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeCloud;->registerMemfreezeOperation(Landroid/content/Context;)V

    .line 781
    goto :goto_0

    .line 800
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$mcheckAndKill(Lcom/android/server/am/MemoryFreezeStubImpl;)V

    .line 801
    goto :goto_0

    .line 796
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$mcheckAndWriteBack(Lcom/android/server/am/MemoryFreezeStubImpl;)V

    .line 797
    goto :goto_0

    .line 792
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$mcheckAndReclaim(Lcom/android/server/am/MemoryFreezeStubImpl;I)V

    .line 793
    goto :goto_0

    .line 788
    :pswitch_4
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$mcheckUnused(Lcom/android/server/am/MemoryFreezeStubImpl;)V

    .line 789
    goto :goto_0

    .line 784
    :pswitch_5
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/am/MemoryFreezeStubImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->registerMiuiFreezeObserver(Landroid/content/Context;)V

    .line 785
    goto :goto_0

    .line 774
    :pswitch_6
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/am/MemoryFreezeStubImpl;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->registerCloudObserver(Landroid/content/Context;)V

    .line 775
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-virtual {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->updateCloudControlParas()V

    .line 776
    nop

    .line 806
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
