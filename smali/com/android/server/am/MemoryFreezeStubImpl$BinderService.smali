.class final Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;
.super Landroid/os/Binder;
.source "MemoryFreezeStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryFreezeStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BinderService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryFreezeStubImpl;


# direct methods
.method private constructor <init>(Lcom/android/server/am/MemoryFreezeStubImpl;)V
    .locals 0

    .line 698
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/MemoryFreezeStubImpl;Lcom/android/server/am/MemoryFreezeStubImpl$BinderService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;)V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 700
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/am/MemoryFreezeStubImpl;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "MFZ"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 701
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-static {v0, p2}, Lcom/android/server/am/MemoryFreezeStubImpl;->-$$Nest$mdump(Lcom/android/server/am/MemoryFreezeStubImpl;Ljava/io/PrintWriter;)V

    .line 702
    return-void
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 708
    new-instance v0, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;

    iget-object v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-direct {v0, v1, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Lcom/android/server/am/MemoryFreezeStubImpl;)V

    .line 709
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 710
    return-void
.end method
