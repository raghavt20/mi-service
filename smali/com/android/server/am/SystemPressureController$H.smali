.class Lcom/android/server/am/SystemPressureController$H;
.super Landroid/os/Handler;
.source "SystemPressureController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/SystemPressureController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/SystemPressureController;


# direct methods
.method public constructor <init>(Lcom/android/server/am/SystemPressureController;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 129
    iput-object p1, p0, Lcom/android/server/am/SystemPressureController$H;->this$0:Lcom/android/server/am/SystemPressureController;

    .line 130
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 131
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 135
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 136
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 148
    :pswitch_0
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController$H;->this$0:Lcom/android/server/am/SystemPressureController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/am/SystemPressureController;->updateScreenState(Z)V

    .line 149
    goto :goto_0

    .line 144
    :pswitch_1
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController$H;->this$0:Lcom/android/server/am/SystemPressureController;

    invoke-static {v0}, Lcom/android/server/am/SystemPressureController;->-$$Nest$fgetmContext(Lcom/android/server/am/SystemPressureController;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/am/SystemPressureController;->-$$Nest$mregisterCloudObserver(Lcom/android/server/am/SystemPressureController;Landroid/content/Context;)V

    .line 145
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController$H;->this$0:Lcom/android/server/am/SystemPressureController;

    invoke-static {v0}, Lcom/android/server/am/SystemPressureController;->-$$Nest$mupdateCloudControlData(Lcom/android/server/am/SystemPressureController;)V

    .line 146
    goto :goto_0

    .line 141
    :pswitch_2
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController$H;->this$0:Lcom/android/server/am/SystemPressureController;

    invoke-static {v0}, Lcom/android/server/am/SystemPressureController;->-$$Nest$mresetStartingAppState(Lcom/android/server/am/SystemPressureController;)V

    .line 142
    goto :goto_0

    .line 138
    :pswitch_3
    iget-object v0, p0, Lcom/android/server/am/SystemPressureController$H;->this$0:Lcom/android/server/am/SystemPressureController;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/server/am/SystemPressureController;->-$$Nest$mcleanUpMemory(Lcom/android/server/am/SystemPressureController;J)V

    .line 139
    nop

    .line 153
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
