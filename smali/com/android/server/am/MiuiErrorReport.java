public class com.android.server.am.MiuiErrorReport {
	 /* .source "MiuiErrorReport.java" */
	 /* # static fields */
	 private static final java.lang.String ACTION_FC_PREVIEW;
	 private static final java.lang.String ERROR_TYPE_FC;
	 private static final java.lang.String EXTRA_FC_PREVIEW;
	 private static final java.lang.String JSON_ERROR_TYPE;
	 private static final java.lang.String JSON_EXCEPTION_CLASS;
	 private static final java.lang.String JSON_EXCEPTION_SOURCE_METHOD;
	 private static final java.lang.String JSON_PACKAGE_NAME;
	 private static final java.lang.String JSON_STACK_TRACK;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.android.server.am.MiuiErrorReport ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static org.json.JSONObject getExceptionData ( java.lang.String p0, android.app.ApplicationErrorReport$CrashInfo p1 ) {
		 /* .locals 4 */
		 /* .param p0, "packageName" # Ljava/lang/String; */
		 /* .param p1, "crashInfo" # Landroid/app/ApplicationErrorReport$CrashInfo; */
		 /* .line 43 */
		 /* new-instance v0, Lorg/json/JSONObject; */
		 /* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
		 /* .line 45 */
		 /* .local v0, "json":Lorg/json/JSONObject; */
		 try { // :try_start_0
			 final String v1 = "package_name"; // const-string v1, "package_name"
			 (( org.json.JSONObject ) v0 ).put ( v1, p0 ); // invoke-virtual {v0, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
			 /* .line 46 */
			 final String v1 = "error_type"; // const-string v1, "error_type"
			 final String v2 = "fc"; // const-string v2, "fc"
			 (( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
			 /* .line 47 */
			 final String v1 = "exception_class"; // const-string v1, "exception_class"
			 v2 = this.exceptionClassName;
			 (( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
			 /* .line 48 */
			 final String v1 = "exception_source_method"; // const-string v1, "exception_source_method"
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 v3 = this.throwClassName;
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 final String v3 = "."; // const-string v3, "."
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v3 = this.throwMethodName;
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 (( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
			 /* .line 50 */
			 /* const-string/jumbo v1, "stack_track" */
			 v2 = this.stackTrace;
			 (( org.json.JSONObject ) v0 ).put ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 53 */
			 /* .line 51 */
			 /* :catch_0 */
			 /* move-exception v1 */
			 /* .line 52 */
			 /* .local v1, "e":Ljava/lang/Exception; */
			 final String v2 = "MiuiErrorReport"; // const-string v2, "MiuiErrorReport"
			 final String v3 = "Fail to getExceptionData"; // const-string v3, "Fail to getExceptionData"
			 android.util.Log .w ( v2,v3,v1 );
			 /* .line 54 */
		 } // .end local v1 # "e":Ljava/lang/Exception;
	 } // :goto_0
} // .end method
public static void startFcPreviewActivity ( android.content.Context p0, java.lang.String p1, android.app.ApplicationErrorReport$CrashInfo p2 ) {
	 /* .locals 4 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .param p1, "packageName" # Ljava/lang/String; */
	 /* .param p2, "crashInfo" # Landroid/app/ApplicationErrorReport$CrashInfo; */
	 /* .line 27 */
	 /* if-nez p2, :cond_0 */
	 /* .line 28 */
	 return;
	 /* .line 30 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 31 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "miui.intent.action.FC_PREVIEW"; // const-string v1, "miui.intent.action.FC_PREVIEW"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 32 */
com.android.server.am.MiuiErrorReport .getExceptionData ( p1,p2 );
(( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
final String v2 = "extra_fc_report"; // const-string v2, "extra_fc_report"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 33 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 35 */
try { // :try_start_0
	 v1 = android.os.UserHandle.CURRENT;
	 (( android.content.Context ) p0 ).startActivityAsUser ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 38 */
	 /* .line 36 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 37 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 final String v2 = "MiuiErrorReport"; // const-string v2, "MiuiErrorReport"
	 final String v3 = "Failed to start preview activity"; // const-string v3, "Failed to start preview activity"
	 android.util.Slog .i ( v2,v3,v1 );
	 /* .line 39 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
