public class com.android.server.am.DumpScoutTraceThread extends java.lang.Thread {
	 /* .source "DumpScoutTraceThread.java" */
	 /* # static fields */
	 private static final java.lang.String APP_LOG_DIR;
	 private static Integer NUMBER_OF_CORES;
	 public static final Integer SOCKET_BUFFER_SIZE;
	 public static final java.lang.String SOCKET_NAME;
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.am.ActivityManagerServiceImpl mAMSImpl;
	 /* # direct methods */
	 public static void $r8$lambda$P9I22cukx4b4Fbe-h2DFudvv5fY ( com.android.server.am.DumpScoutTraceThread p0, Boolean p1, java.lang.String p2, java.lang.String p3, java.lang.String p4, Integer p5, Integer[] p6 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct/range {p0 ..p6}, Lcom/android/server/am/DumpScoutTraceThread;->lambda$run$0(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[I)V */
		 return;
	 } // .end method
	 static com.android.server.am.DumpScoutTraceThread ( ) {
		 /* .locals 1 */
		 /* .line 30 */
		 java.lang.Runtime .getRuntime ( );
		 v0 = 		 (( java.lang.Runtime ) v0 ).availableProcessors ( ); // invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I
		 return;
	 } // .end method
	 public com.android.server.am.DumpScoutTraceThread ( ) {
		 /* .locals 0 */
		 /* .param p1, "name" # Ljava/lang/String; */
		 /* .param p2, "AMSImpl" # Lcom/android/server/am/ActivityManagerServiceImpl; */
		 /* .line 36 */
		 /* invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V */
		 /* .line 37 */
		 this.mAMSImpl = p2;
		 /* .line 38 */
		 return;
	 } // .end method
	 private void lambda$run$0 ( Boolean p0, java.lang.String p1, java.lang.String p2, java.lang.String p3, Integer p4, Integer[] p5 ) { //synthethic
		 /* .locals 23 */
		 /* .param p1, "isSystemBlock" # Z */
		 /* .param p2, "fileNamePerfix" # Ljava/lang/String; */
		 /* .param p3, "timestamp" # Ljava/lang/String; */
		 /* .param p4, "path" # Ljava/lang/String; */
		 /* .param p5, "pid" # I */
		 /* .param p6, "finalPids" # [I */
		 /* .line 88 */
		 /* move-object/from16 v1, p0 */
		 /* move-object/from16 v2, p2 */
		 /* move-object/from16 v3, p3 */
		 /* move-object/from16 v4, p4 */
		 /* move/from16 v5, p5 */
		 /* move-object/from16 v6, p6 */
		 final String v0 = "/"; // const-string v0, "/"
		 final String v7 = "DumpScoutTraceThread"; // const-string v7, "DumpScoutTraceThread"
		 if ( p1 != null) { // if-eqz p1, :cond_0
			 /* .line 90 */
			 /* new-instance v8, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
			 (( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 final String v9 = "-system_server-trace"; // const-string v9, "-system_server-trace"
			 (( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* .line 91 */
			 /* .local v8, "systemFileName":Ljava/lang/String; */
			 /* new-instance v9, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
			 (( java.lang.StringBuilder ) v9 ).append ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v9 ).append ( v0 ); // invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v9 ).append ( v8 ); // invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* .line 92 */
			 /* .local v9, "systemFilePath":Ljava/lang/String; */
			 v10 = this.mAMSImpl;
			 (( com.android.server.am.ActivityManagerServiceImpl ) v10 ).dumpSystemTraces ( v9 ); // invoke-virtual {v10, v9}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpSystemTraces(Ljava/lang/String;)V
			 /* .line 93 */
		 } // .end local v8 # "systemFileName":Ljava/lang/String;
	 } // .end local v9 # "systemFilePath":Ljava/lang/String;
	 /* .line 94 */
} // :cond_0
final String v8 = "Don\'t need to dump system_server trace."; // const-string v8, "Don\'t need to dump system_server trace."
android.util.Slog .w ( v7,v8 );
/* .line 97 */
} // :goto_0
com.android.server.ScoutHelper .CheckDState ( v7,v5 );
v8 = (( java.lang.Boolean ) v8 ).booleanValue ( ); // invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 98 */
/* .local v8, "result":Z */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v2 ); // invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = "-self-trace"; // const-string v10, "-self-trace"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 99 */
/* .local v9, "selfFileName":Ljava/lang/String; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v4 ); // invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v0 ); // invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 100 */
/* .local v10, "selfFilePath":Ljava/lang/String; */
v11 = this.mAMSImpl;
final String v12 = "App Scout Exception"; // const-string v12, "App Scout Exception"
(( com.android.server.am.ActivityManagerServiceImpl ) v11 ).dumpOneProcessTraces ( v5, v10, v12 ); // invoke-virtual {v11, v5, v10, v12}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpOneProcessTraces(ILjava/lang/String;Ljava/lang/String;)Ljava/io/File;
/* .line 103 */
/* .local v11, "selfTraceFile":Ljava/io/File; */
if ( v11 != null) { // if-eqz v11, :cond_1
/* .line 104 */
final String v12 = "Dump scout self trace file successfully!"; // const-string v12, "Dump scout self trace file successfully!"
android.util.Slog .d ( v7,v12 );
/* .line 106 */
} // :cond_1
final String v12 = "Dump scout self trace file fail!"; // const-string v12, "Dump scout self trace file fail!"
android.util.Slog .w ( v7,v12 );
/* .line 109 */
} // :goto_1
(( com.android.server.am.DumpScoutTraceThread ) v1 ).dumpLockedMethodLongEvents ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/am/DumpScoutTraceThread;->dumpLockedMethodLongEvents(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
/* .line 111 */
if ( v6 != null) { // if-eqz v6, :cond_8
/* array-length v13, v6 */
/* if-lez v13, :cond_8 */
/* .line 113 */
/* array-length v13, v6 */
int v14 = 0; // const/4 v14, 0x0
} // :goto_2
/* if-ge v14, v13, :cond_2 */
/* aget v15, v6, v14 */
/* .line 114 */
/* .local v15, "temppid":I */
com.android.server.ScoutHelper .CheckDState ( v7,v15 );
/* .line 113 */
} // .end local v15 # "temppid":I
/* add-int/lit8 v14, v14, 0x1 */
/* .line 118 */
} // :cond_2
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v13 ).append ( v2 ); // invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v3 ); // invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = "-other-trace"; // const-string v14, "-other-trace"
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 119 */
/* .local v13, "fileName":Ljava/lang/String; */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v14 ).append ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v0 ); // invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v13 ); // invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 121 */
/* .local v14, "filePath":Ljava/lang/String; */
/* new-instance v15, Ljava/util/ArrayList; */
int v12 = 3; // const/4 v12, 0x3
/* invoke-direct {v15, v12}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 122 */
/* .local v15, "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5, v12}, Ljava/util/ArrayList;-><init>(I)V */
/* .line 123 */
/* .local v5, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
int v12 = 0; // const/4 v12, 0x0
/* .local v12, "i":I */
} // :goto_3
/* move/from16 v21, v8 */
} // .end local v8 # "result":Z
/* .local v21, "result":Z */
/* array-length v8, v6 */
/* if-ge v12, v8, :cond_6 */
/* .line 124 */
/* aget v8, v6, v12 */
v8 = com.android.server.ScoutHelper .getOomAdjOfPid ( v7,v8 );
/* .line 125 */
/* .local v8, "adj":I */
/* move-object/from16 v22, v9 */
} // .end local v9 # "selfFileName":Ljava/lang/String;
/* .local v22, "selfFileName":Ljava/lang/String; */
v9 = com.android.server.ScoutHelper .checkIsJavaOrNativeProcess ( v8 );
/* .line 126 */
/* .local v9, "isJavaOrNativeProcess":I */
/* if-nez v9, :cond_3 */
/* .line 127 */
/* .line 128 */
} // :cond_3
/* move/from16 v16, v8 */
} // .end local v8 # "adj":I
/* .local v16, "adj":I */
int v8 = 1; // const/4 v8, 0x1
/* if-ne v9, v8, :cond_4 */
/* .line 129 */
/* aget v8, v6, v12 */
java.lang.Integer .valueOf ( v8 );
(( java.util.ArrayList ) v15 ).add ( v8 ); // invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 130 */
} // :cond_4
int v8 = 2; // const/4 v8, 0x2
/* if-ne v9, v8, :cond_5 */
/* .line 131 */
/* aget v8, v6, v12 */
java.lang.Integer .valueOf ( v8 );
(( java.util.ArrayList ) v5 ).add ( v8 ); // invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 123 */
} // .end local v9 # "isJavaOrNativeProcess":I
} // .end local v16 # "adj":I
} // :cond_5
} // :goto_4
/* add-int/lit8 v12, v12, 0x1 */
/* move/from16 v8, v21 */
/* move-object/from16 v9, v22 */
} // .end local v22 # "selfFileName":Ljava/lang/String;
/* .local v9, "selfFileName":Ljava/lang/String; */
} // :cond_6
/* move-object/from16 v22, v9 */
/* .line 135 */
} // .end local v9 # "selfFileName":Ljava/lang/String;
} // .end local v12 # "i":I
/* .restart local v22 # "selfFileName":Ljava/lang/String; */
v8 = this.mAMSImpl;
/* const/16 v17, 0x0 */
final String v19 = "App Scout Exception"; // const-string v19, "App Scout Exception"
/* move-object v9, v15 */
} // .end local v15 # "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
/* .local v9, "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;" */
/* move-object v15, v8 */
/* move-object/from16 v16, v9 */
/* move-object/from16 v18, v5 */
/* move-object/from16 v20, v14 */
/* invoke-virtual/range {v15 ..v20}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpAppStackTraces(Ljava/util/ArrayList;Landroid/util/SparseArray;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File; */
/* .line 137 */
/* .local v8, "appTraceFile":Ljava/io/File; */
if ( v8 != null) { // if-eqz v8, :cond_7
/* .line 138 */
final String v12 = "Dump scout other trace file successfully !"; // const-string v12, "Dump scout other trace file successfully !"
android.util.Slog .d ( v7,v12 );
/* .line 140 */
} // :cond_7
final String v12 = "Dump scout other trace file fail !"; // const-string v12, "Dump scout other trace file fail !"
android.util.Slog .w ( v7,v12 );
/* .line 142 */
} // .end local v5 # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v8 # "appTraceFile":Ljava/io/File;
} // .end local v9 # "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
} // .end local v13 # "fileName":Ljava/lang/String;
} // .end local v14 # "filePath":Ljava/lang/String;
} // :goto_5
/* .line 111 */
} // .end local v21 # "result":Z
} // .end local v22 # "selfFileName":Ljava/lang/String;
/* .local v8, "result":Z */
/* .local v9, "selfFileName":Ljava/lang/String; */
} // :cond_8
/* move/from16 v21, v8 */
/* move-object/from16 v22, v9 */
/* .line 143 */
} // .end local v8 # "result":Z
} // .end local v9 # "selfFileName":Ljava/lang/String;
/* .restart local v21 # "result":Z */
/* .restart local v22 # "selfFileName":Ljava/lang/String; */
final String v5 = "dumpAPPStackTraces: pids is null"; // const-string v5, "dumpAPPStackTraces: pids is null"
android.util.Slog .w ( v7,v5 );
/* .line 147 */
} // :goto_6
v5 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 148 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "_walkstack"; // const-string v8, "_walkstack"
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 149 */
/* .local v5, "walkstackFileName":Ljava/lang/String; */
/* new-instance v8, Ljava/io/File; */
final String v9 = "/data/miuilog/stability/scout/app"; // const-string v9, "/data/miuilog/stability/scout/app"
/* invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 150 */
/* .local v8, "appScoutDir":Ljava/io/File; */
v9 = (( java.io.File ) v8 ).exists ( ); // invoke-virtual {v8}, Ljava/io/File;->exists()Z
if ( v9 != null) { // if-eqz v9, :cond_b
/* .line 151 */
/* new-instance v9, Lcom/android/server/am/DumpScoutTraceThread$1; */
/* invoke-direct {v9, v1, v5}, Lcom/android/server/am/DumpScoutTraceThread$1;-><init>(Lcom/android/server/am/DumpScoutTraceThread;Ljava/lang/String;)V */
(( java.io.File ) v8 ).listFiles ( v9 ); // invoke-virtual {v8, v9}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;
/* .line 161 */
/* .local v9, "pathArray":[Ljava/io/File; */
if ( v9 != null) { // if-eqz v9, :cond_b
/* array-length v12, v9 */
/* if-lez v12, :cond_b */
/* .line 162 */
/* new-instance v12, Ljava/io/File; */
int v13 = 0; // const/4 v13, 0x0
/* aget-object v13, v9, v13 */
(( java.io.File ) v13 ).getAbsolutePath ( ); // invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 163 */
/* .local v12, "walkstackFileOld":Ljava/io/File; */
/* new-instance v13, Ljava/io/File; */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v14 ).append ( v4 ); // invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).append ( v0 ); // invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = "-miui-self-trace"; // const-string v14, "-miui-self-trace"
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 165 */
/* .local v13, "walkstackFile":Ljava/io/File; */
try { // :try_start_0
v0 = (( java.io.File ) v13 ).createNewFile ( ); // invoke-virtual {v13}, Ljava/io/File;->createNewFile()Z
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 166 */
(( java.io.File ) v13 ).getAbsolutePath ( ); // invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* const/16 v14, 0x180 */
int v15 = -1; // const/4 v15, -0x1
android.os.FileUtils .setPermissions ( v0,v14,v15,v15 );
/* .line 167 */
v0 = android.os.FileUtils .copyFile ( v12,v13 );
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 168 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "Success copying walkstack trace to path"; // const-string v14, "Success copying walkstack trace to path"
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v13 ).getAbsolutePath ( ); // invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v0 );
/* .line 169 */
(( java.io.File ) v12 ).delete ( ); // invoke-virtual {v12}, Ljava/io/File;->delete()Z
/* .line 171 */
} // :cond_9
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "Fail to copy walkstack trace to path"; // const-string v14, "Fail to copy walkstack trace to path"
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) v13 ).getAbsolutePath ( ); // invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v7,v0 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 176 */
} // :cond_a
} // :goto_7
/* .line 174 */
/* :catch_0 */
/* move-exception v0 */
/* .line 175 */
/* .local v0, "e":Ljava/io/IOException; */
final String v14 = "Exception occurs while copying walkstack trace file:"; // const-string v14, "Exception occurs while copying walkstack trace file:"
android.util.Slog .w ( v7,v14,v0 );
/* .line 181 */
} // .end local v0 # "e":Ljava/io/IOException;
} // .end local v5 # "walkstackFileName":Ljava/lang/String;
} // .end local v8 # "appScoutDir":Ljava/io/File;
} // .end local v9 # "pathArray":[Ljava/io/File;
} // .end local v12 # "walkstackFileOld":Ljava/io/File;
} // .end local v13 # "walkstackFile":Ljava/io/File;
} // :cond_b
} // :goto_8
return;
} // .end method
/* # virtual methods */
void dumpLockedMethodLongEvents ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 7 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "fileNamePerfix" # Ljava/lang/String; */
/* .param p3, "timestamp" # Ljava/lang/String; */
/* .line 200 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "-critical-section-trace"; // const-string v1, "-critical-section-trace"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 201 */
/* .local v0, "fileName":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/"; // const-string v2, "/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 202 */
/* .local v1, "filePath":Ljava/lang/String; */
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 204 */
/* .local v2, "traceFile":Ljava/io/File; */
try { // :try_start_0
v3 = (( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 205 */
(( java.io.File ) v2 ).getAbsolutePath ( ); // invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* const/16 v4, 0x180 */
int v5 = -1; // const/4 v5, -0x1
android.os.FileUtils .setPermissions ( v3,v4,v5,v5 );
/* .line 207 */
} // :cond_0
com.android.server.LockPerfStub .getInstance ( );
/* const/16 v4, 0x3c */
/* const/16 v5, 0x14 */
int v6 = 0; // const/4 v6, 0x0
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 210 */
/* .line 208 */
/* :catch_0 */
/* move-exception v3 */
/* .line 209 */
/* .local v3, "e":Ljava/io/IOException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Exception creating scout file: "; // const-string v5, "Exception creating scout file: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "DumpScoutTraceThread"; // const-string v5, "DumpScoutTraceThread"
android.util.Slog .w ( v5,v4,v3 );
/* .line 211 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
public void run ( ) {
/* .locals 21 */
/* .line 42 */
final String v1 = "DumpScoutTraceThread finally shutdown"; // const-string v1, "DumpScoutTraceThread finally shutdown"
final String v2 = "DumpScoutTraceThread"; // const-string v2, "DumpScoutTraceThread"
/* new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor; */
/* const-wide/16 v6, 0x1 */
v8 = java.util.concurrent.TimeUnit.SECONDS;
/* new-instance v9, Ljava/util/concurrent/LinkedBlockingQueue; */
/* invoke-direct {v9}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V */
/* move-object v3, v0 */
/* move v4, v5 */
/* invoke-direct/range {v3 ..v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V */
/* .line 48 */
/* .local v3, "threadPool":Ljava/util/concurrent/ThreadPoolExecutor; */
int v0 = 1; // const/4 v0, 0x1
(( java.util.concurrent.ThreadPoolExecutor ) v3 ).allowCoreThreadTimeOut ( v0 ); // invoke-virtual {v3, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V
/* .line 50 */
int v4 = 0; // const/4 v4, 0x0
/* .line 53 */
/* .local v4, "socketServer":Landroid/net/LocalServerSocket; */
try { // :try_start_0
/* new-instance v0, Landroid/net/LocalServerSocket; */
final String v5 = "scouttrace"; // const-string v5, "scouttrace"
/* invoke-direct {v0, v5}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V */
/* move-object v4, v0 */
/* .line 54 */
final String v0 = "Already create local socket"; // const-string v0, "Already create local socket"
android.util.Slog .w ( v2,v0 );
/* .line 56 */
} // :cond_0
} // :goto_0
(( android.net.LocalServerSocket ) v4 ).accept ( ); // invoke-virtual {v4}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;
/* .line 59 */
/* .local v0, "socketClient":Landroid/net/LocalSocket; */
/* const-string/jumbo v5, "waiting mqs client socket to connect..." */
android.util.Slog .e ( v2,v5 );
/* .line 60 */
(( android.net.LocalSocket ) v0 ).getInputStream ( ); // invoke-virtual {v0}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
/* .line 62 */
/* .local v5, "inputStream":Ljava/io/InputStream; */
/* new-instance v6, Ljava/io/BufferedReader; */
/* new-instance v7, Ljava/io/InputStreamReader; */
/* invoke-direct {v7, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* .line 63 */
/* .local v6, "reader":Ljava/io/BufferedReader; */
(( java.io.BufferedReader ) v6 ).readLine ( ); // invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* .line 64 */
/* .local v7, "scoutInfo":Ljava/lang/String; */
int v8 = 0; // const/4 v8, 0x0
/* .line 65 */
/* .local v8, "jsonObject":Lorg/json/JSONObject; */
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 66 */
/* new-instance v9, Lorg/json/JSONObject; */
/* invoke-direct {v9, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* move-object v8, v9 */
/* .line 70 */
final String v9 = "pid"; // const-string v9, "pid"
(( org.json.JSONObject ) v8 ).getString ( v9 ); // invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v9 = java.lang.Integer .parseInt ( v9 );
/* .line 71 */
/* .local v9, "pid":I */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "get data from socket "; // const-string v11, "get data from socket "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v10 );
/* .line 72 */
final String v10 = "pidlist"; // const-string v10, "pidlist"
(( org.json.JSONObject ) v8 ).getString ( v10 ); // invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* move-object v11, v10 */
/* .line 73 */
/* .local v11, "pidlist":Ljava/lang/String; */
int v10 = 0; // const/4 v10, 0x0
/* .line 74 */
/* .local v10, "pids":[I */
final String v12 = "null"; // const-string v12, "null"
v12 = (( java.lang.String ) v11 ).equals ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v12, :cond_2 */
/* .line 75 */
final String v12 = "#"; // const-string v12, "#"
(( java.lang.String ) v11 ).split ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 76 */
/* .local v12, "pidArray":[Ljava/lang/String; */
/* array-length v13, v12 */
/* new-array v13, v13, [I */
/* move-object v10, v13 */
/* .line 77 */
int v13 = 0; // const/4 v13, 0x0
/* .local v13, "i":I */
} // :goto_1
/* array-length v14, v12 */
/* if-ge v13, v14, :cond_1 */
/* .line 78 */
/* aget-object v14, v12, v13 */
v14 = java.lang.Integer .parseInt ( v14 );
/* aput v14, v10, v13 */
/* .line 77 */
/* add-int/lit8 v13, v13, 0x1 */
} // :cond_1
/* move-object/from16 v18, v10 */
/* .line 74 */
} // .end local v12 # "pidArray":[Ljava/lang/String;
} // .end local v13 # "i":I
} // :cond_2
/* move-object/from16 v18, v10 */
/* .line 81 */
} // .end local v10 # "pids":[I
/* .local v18, "pids":[I */
} // :goto_2
final String v10 = "path"; // const-string v10, "path"
(( org.json.JSONObject ) v8 ).getString ( v10 ); // invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 82 */
/* .local v15, "path":Ljava/lang/String; */
/* const-string/jumbo v10, "timestamp" */
(( org.json.JSONObject ) v8 ).getString ( v10 ); // invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 83 */
/* .local v14, "timestamp":Ljava/lang/String; */
final String v10 = "fileNamePerfix"; // const-string v10, "fileNamePerfix"
(( org.json.JSONObject ) v8 ).getString ( v10 ); // invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 84 */
/* .local v13, "fileNamePerfix":Ljava/lang/String; */
final String v10 = "isSystemBlock"; // const-string v10, "isSystemBlock"
(( org.json.JSONObject ) v8 ).getString ( v10 ); // invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
v12 = java.lang.Boolean .parseBoolean ( v10 );
/* .line 86 */
/* .local v12, "isSystemBlock":Z */
/* move-object/from16 v17, v18 */
/* .line 87 */
/* .local v17, "finalPids":[I */
/* new-instance v10, Lcom/android/server/am/DumpScoutTraceThread$$ExternalSyntheticLambda0; */
/* move-object/from16 v19, v10 */
/* move-object/from16 v20, v11 */
} // .end local v11 # "pidlist":Ljava/lang/String;
/* .local v20, "pidlist":Ljava/lang/String; */
/* move-object/from16 v11, p0 */
/* move/from16 v16, v9 */
/* invoke-direct/range {v10 ..v17}, Lcom/android/server/am/DumpScoutTraceThread$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/DumpScoutTraceThread;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[I)V */
/* move-object/from16 v10, v19 */
(( java.util.concurrent.ThreadPoolExecutor ) v3 ).execute ( v10 ); // invoke-virtual {v3, v10}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 182 */
} // .end local v0 # "socketClient":Landroid/net/LocalSocket;
} // .end local v5 # "inputStream":Ljava/io/InputStream;
} // .end local v6 # "reader":Ljava/io/BufferedReader;
} // .end local v7 # "scoutInfo":Ljava/lang/String;
} // .end local v8 # "jsonObject":Lorg/json/JSONObject;
} // .end local v9 # "pid":I
} // .end local v12 # "isSystemBlock":Z
} // .end local v13 # "fileNamePerfix":Ljava/lang/String;
} // .end local v14 # "timestamp":Ljava/lang/String;
} // .end local v15 # "path":Ljava/lang/String;
} // .end local v17 # "finalPids":[I
} // .end local v18 # "pids":[I
} // .end local v20 # "pidlist":Ljava/lang/String;
/* goto/16 :goto_0 */
/* .line 186 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object v5, v0 */
/* .line 183 */
/* :catch_0 */
/* move-exception v0 */
/* .line 184 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v5 = "Exception creating scout dump scout trace file:"; // const-string v5, "Exception creating scout dump scout trace file:"
android.util.Slog .w ( v2,v5,v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 186 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
android.util.Slog .w ( v2,v1 );
/* .line 187 */
(( java.util.concurrent.ThreadPoolExecutor ) v3 ).shutdown ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V
/* .line 188 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 190 */
try { // :try_start_2
(( android.net.LocalServerSocket ) v4 ).close ( ); // invoke-virtual {v4}, Landroid/net/LocalServerSocket;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 193 */
} // :goto_3
/* .line 191 */
/* :catch_1 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move-object v0, v1 */
/* .line 192 */
/* .local v0, "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
} // .end local v0 # "e":Ljava/io/IOException;
/* .line 197 */
} // :cond_3
} // :goto_4
return;
/* .line 186 */
} // :goto_5
android.util.Slog .w ( v2,v1 );
/* .line 187 */
(( java.util.concurrent.ThreadPoolExecutor ) v3 ).shutdown ( ); // invoke-virtual {v3}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V
/* .line 188 */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 190 */
try { // :try_start_3
(( android.net.LocalServerSocket ) v4 ).close ( ); // invoke-virtual {v4}, Landroid/net/LocalServerSocket;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 193 */
/* .line 191 */
/* :catch_2 */
/* move-exception v0 */
/* move-object v1, v0 */
/* move-object v0, v1 */
/* .line 192 */
/* .restart local v0 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
/* .line 195 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_4
} // :goto_6
/* throw v5 */
} // .end method
