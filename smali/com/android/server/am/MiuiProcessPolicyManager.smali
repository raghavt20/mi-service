.class public final Lcom/android/server/am/MiuiProcessPolicyManager;
.super Ljava/lang/Object;
.source "MiuiProcessPolicyManager.java"


# static fields
.field private static final ACORE_PROCESS_NAME:Ljava/lang/String; = "android.process.acore"

.field private static final DEATH_COUNT_LIMIT:I

.field private static final ENABLE:Z

.field private static final ENABLE_MEMORY_OPTIMIZE:Z

.field private static final ENABLE_PROMOTE_SUBPROCESS:Z

.field private static final TAG:Ljava/lang/String; = "MiuiProcessPolicyManager"

.field private static sInstance:Lcom/android/server/am/MiuiProcessPolicyManager;

.field private static volatile sPmInstance:Lcom/android/server/am/ProcessManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 19
    const-string v0, "persist.am.enable_ppm"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiProcessPolicyManager;->ENABLE:Z

    .line 21
    nop

    .line 22
    const-string v0, "persist.am.enable_promote_sub"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiProcessPolicyManager;->ENABLE_PROMOTE_SUBPROCESS:Z

    .line 24
    nop

    .line 25
    const-string v0, "persist.miui.mopt.acore.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiProcessPolicyManager;->ENABLE_MEMORY_OPTIMIZE:Z

    .line 28
    const-string v0, "persist.am.death_limit"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/am/MiuiProcessPolicyManager;->DEATH_COUNT_LIMIT:I

    .line 31
    new-instance v0, Lcom/android/server/am/MiuiProcessPolicyManager;

    invoke-direct {v0}, Lcom/android/server/am/MiuiProcessPolicyManager;-><init>()V

    sput-object v0, Lcom/android/server/am/MiuiProcessPolicyManager;->sInstance:Lcom/android/server/am/MiuiProcessPolicyManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/android/server/am/MiuiProcessPolicyManager;
    .locals 1

    .line 34
    sget-object v0, Lcom/android/server/am/MiuiProcessPolicyManager;->sInstance:Lcom/android/server/am/MiuiProcessPolicyManager;

    return-object v0
.end method

.method private getProcessManagerService()Lcom/android/server/am/ProcessManagerService;
    .locals 1

    .line 38
    sget-object v0, Lcom/android/server/am/MiuiProcessPolicyManager;->sPmInstance:Lcom/android/server/am/ProcessManagerService;

    if-nez v0, :cond_0

    .line 39
    const-string v0, "ProcessManager"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessManagerService;

    sput-object v0, Lcom/android/server/am/MiuiProcessPolicyManager;->sPmInstance:Lcom/android/server/am/ProcessManagerService;

    .line 41
    :cond_0
    sget-object v0, Lcom/android/server/am/MiuiProcessPolicyManager;->sPmInstance:Lcom/android/server/am/ProcessManagerService;

    return-object v0
.end method

.method private isInWhiteList(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 127
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    if-nez p2, :cond_0

    goto :goto_0

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/android/server/am/MiuiProcessPolicyManager;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v1

    .line 131
    .local v1, "policy":Lcom/android/server/am/ProcessPolicy;
    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessPolicy;->isInProcessStaticWhiteList(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-nez v2, :cond_1

    .line 132
    invoke-virtual {v1, v3}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;

    move-result-object v2

    .line 133
    invoke-interface {v2, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v3

    .line 131
    :cond_2
    return v0

    .line 128
    .end local v1    # "policy":Lcom/android/server/am/ProcessPolicy;
    :cond_3
    :goto_0
    return v0
.end method

.method private isLockedProcess(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 103
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/android/server/am/MiuiProcessPolicyManager;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v1

    if-nez v1, :cond_0

    .line 104
    return v0

    .line 107
    :cond_0
    sget-boolean v1, Lcom/android/server/am/MiuiProcessPolicyManager;->ENABLE:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/server/am/MiuiProcessPolicyManager;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/android/server/am/ProcessManagerService;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 108
    :catch_0
    move-exception v1

    .line 109
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 111
    .end local v1    # "e":Landroid/os/RemoteException;
    return v0
.end method

.method private isMemoryOptProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;

    .line 137
    sget-boolean v0, Lcom/android/server/am/MiuiProcessPolicyManager;->ENABLE_MEMORY_OPTIMIZE:Z

    if-eqz v0, :cond_0

    const-string v0, "android.process.acore"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isSecretlyProtectProcess(Ljava/lang/String;)Z
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;

    .line 116
    invoke-direct {p0}, Lcom/android/server/am/MiuiProcessPolicyManager;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 117
    return v1

    .line 120
    :cond_0
    sget-boolean v0, Lcom/android/server/am/MiuiProcessPolicyManager;->ENABLE:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/am/MiuiProcessPolicyManager;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessPolicy;->isInSecretlyProtectList(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method


# virtual methods
.method public isNeedTraceProcess(Lcom/android/server/am/ProcessRecord;)Z
    .locals 2
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 49
    invoke-direct {p0}, Lcom/android/server/am/MiuiProcessPolicyManager;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 50
    const/4 v0, 0x0

    return v0

    .line 53
    :cond_0
    sget-boolean v0, Lcom/android/server/am/MiuiProcessPolicyManager;->ENABLE:Z

    if-eqz v0, :cond_1

    .line 54
    nop

    .line 53
    invoke-direct {p0}, Lcom/android/server/am/MiuiProcessPolicyManager;->getProcessManagerService()Lcom/android/server/am/ProcessManagerService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 54
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z

    move-result v0

    .line 53
    :goto_0
    return v0
.end method

.method public promoteImportantProcAdj(Lcom/android/server/am/ProcessRecord;)V
    .locals 7
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 58
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v0

    if-gtz v0, :cond_0

    .line 59
    return-void

    .line 61
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/server/am/ProcessRecord;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/MiuiProcessPolicyManager;->isInWhiteList(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    const-string v1, ", maxProcState to + "

    const/4 v2, 0x0

    const-string v3, " maxAdj to "

    const-string v4, "promote "

    const-string v5, "MiuiProcessPolicyManager"

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 62
    invoke-direct {p0, v0}, Lcom/android/server/am/MiuiProcessPolicyManager;->isMemoryOptProcess(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 63
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v0

    const/16 v6, 0x320

    if-le v0, v6, :cond_1

    .line 64
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0, v6}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    .line 67
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 68
    invoke-virtual {v6}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v6

    invoke-static {v6, v2}, Lcom/android/server/am/ProcessList;->makeOomAdjString(IZ)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 70
    invoke-static {p1}, Lcom/android/server/am/IProcessPolicy;->getAppMaxProcState(Lcom/android/server/am/ProcessRecord;)I

    move-result v6

    .line 69
    invoke-static {v6}, Lcom/android/server/am/ProcessList;->makeProcStateString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-static {v5, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_2
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/am/MiuiProcessPolicyManager;->isSecretlyProtectProcess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 73
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v0

    sget v6, Lmiui/process/ProcessManager;->SEC_PROTECT_MAX_ADJ:I

    if-le v0, v6, :cond_3

    .line 74
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    sget v6, Lmiui/process/ProcessManager;->SEC_PROTECT_MAX_ADJ:I

    invoke-virtual {v0, v6}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    .line 76
    :cond_3
    invoke-static {p1}, Lcom/android/server/am/IProcessPolicy;->getAppMaxProcState(Lcom/android/server/am/ProcessRecord;)I

    move-result v0

    sget v6, Lmiui/process/ProcessManager;->SEC_PROTECT_MAX_PROCESS_STATE:I

    if-le v0, v6, :cond_4

    .line 78
    sget v0, Lmiui/process/ProcessManager;->SEC_PROTECT_MAX_PROCESS_STATE:I

    invoke-static {p1, v0}, Lcom/android/server/am/IProcessPolicy;->setAppMaxProcState(Lcom/android/server/am/ProcessRecord;I)V

    .line 81
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 82
    invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v3

    invoke-static {v3, v2}, Lcom/android/server/am/ProcessList;->makeOomAdjString(IZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 84
    invoke-static {p1}, Lcom/android/server/am/IProcessPolicy;->getAppMaxProcState(Lcom/android/server/am/ProcessRecord;)I

    move-result v1

    .line 83
    invoke-static {v1}, Lcom/android/server/am/ProcessList;->makeProcStateString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-static {v5, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :cond_5
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iget v6, p1, Lcom/android/server/am/ProcessRecord;->userId:I

    invoke-direct {p0, v0, v6}, Lcom/android/server/am/MiuiProcessPolicyManager;->isLockedProcess(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 86
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v0

    sget v6, Lmiui/process/ProcessManager;->LOCKED_MAX_ADJ:I

    if-le v0, v6, :cond_6

    .line 87
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    sget v6, Lmiui/process/ProcessManager;->LOCKED_MAX_ADJ:I

    invoke-virtual {v0, v6}, Lcom/android/server/am/ProcessStateRecord;->setMaxAdj(I)V

    .line 89
    :cond_6
    invoke-static {p1}, Lcom/android/server/am/IProcessPolicy;->getAppMaxProcState(Lcom/android/server/am/ProcessRecord;)I

    move-result v0

    sget v6, Lmiui/process/ProcessManager;->LOCKED_MAX_PROCESS_STATE:I

    if-le v0, v6, :cond_7

    .line 90
    sget v0, Lmiui/process/ProcessManager;->LOCKED_MAX_PROCESS_STATE:I

    invoke-static {p1, v0}, Lcom/android/server/am/IProcessPolicy;->setAppMaxProcState(Lcom/android/server/am/ProcessRecord;I)V

    .line 93
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lcom/android/server/am/ProcessRecord;->mState:Lcom/android/server/am/ProcessStateRecord;

    .line 94
    invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getMaxAdj()I

    move-result v3

    invoke-static {v3, v2}, Lcom/android/server/am/ProcessList;->makeOomAdjString(IZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 96
    invoke-static {p1}, Lcom/android/server/am/IProcessPolicy;->getAppMaxProcState(Lcom/android/server/am/ProcessRecord;)I

    move-result v1

    .line 95
    invoke-static {v1}, Lcom/android/server/am/ProcessList;->makeProcStateString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-static {v5, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_8
    :goto_0
    return-void
.end method
