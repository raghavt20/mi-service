.class public interface abstract Lcom/android/server/am/AppStateManager$IProcessStateCallback;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IProcessStateCallback"
.end annotation


# virtual methods
.method public abstract onAppStateChanged(Lcom/android/server/am/AppStateManager$AppState;IIZZLjava/lang/String;)V
.end method

.method public abstract onProcessStateChanged(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;IILjava/lang/String;)V
.end method
