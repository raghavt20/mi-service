.class Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;
.super Landroid/os/ShellCommand;
.source "PeriodicCleanerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/PeriodicCleanerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PeriodicShellCmd"
.end annotation


# instance fields
.field mService:Lcom/android/server/am/PeriodicCleanerService;

.field final synthetic this$0:Lcom/android/server/am/PeriodicCleanerService;


# direct methods
.method public constructor <init>(Lcom/android/server/am/PeriodicCleanerService;Lcom/android/server/am/PeriodicCleanerService;)V
    .locals 0
    .param p2, "service"    # Lcom/android/server/am/PeriodicCleanerService;

    .line 1748
    iput-object p1, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    .line 1749
    iput-object p2, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->mService:Lcom/android/server/am/PeriodicCleanerService;

    .line 1750
    return-void
.end method

.method private runClean(Ljava/io/PrintWriter;)V
    .locals 7
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 1798
    const/4 v0, 0x0

    .line 1799
    .local v0, "opt":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextOption()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    if-nez v1, :cond_0

    .line 1800
    const-string/jumbo v1, "trigger clean package by periodic"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1801
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mcleanPackageByPeriodic(Lcom/android/server/am/PeriodicCleanerService;)V

    goto/16 :goto_4

    .line 1803
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_1
    goto :goto_0

    :pswitch_0
    const-string v1, "-r"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    const-string v2, "error: invalid option: "

    packed-switch v1, :pswitch_data_1

    .line 1837
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1838
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->onHelp()V

    goto/16 :goto_4

    .line 1805
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v1

    .line 1806
    .local v1, "reason":Ljava/lang/String;
    const/4 v3, -0x1

    .line 1807
    .local v3, "pressure":I
    const-string v4, "periodic"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1808
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v2}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mcleanPackageByPeriodic(Lcom/android/server/am/PeriodicCleanerService;)V

    goto/16 :goto_4

    .line 1810
    :cond_2
    const-string v4, "pressure-low"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1811
    const/4 v3, 0x0

    goto :goto_2

    .line 1812
    :cond_3
    const-string v4, "pressure-min"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1813
    const/4 v3, 0x1

    goto :goto_2

    .line 1814
    :cond_4
    const-string v4, "pressure-critical"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1815
    const/4 v3, 0x2

    .line 1820
    :goto_2
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->peekNextArg()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "trigger clean package by "

    if-eqz v4, :cond_6

    .line 1821
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v4

    .line 1822
    .local v4, "reasonTime":Ljava/lang/String;
    const-string/jumbo v6, "time"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1823
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "-"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1824
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v2, v3}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mcleanPackageByTime(Lcom/android/server/am/PeriodicCleanerService;I)V

    goto :goto_3

    .line 1826
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1827
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->onHelp()V

    .line 1829
    .end local v4    # "reasonTime":Ljava/lang/String;
    :goto_3
    goto :goto_4

    .line 1830
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1831
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    const-string v4, "debug-pressure"

    invoke-static {v2, v3, v4}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mcleanPackageByPressure(Lcom/android/server/am/PeriodicCleanerService;ILjava/lang/String;)V

    .line 1834
    goto :goto_4

    .line 1817
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error: invalid reason: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1818
    return-void

    .line 1841
    .end local v1    # "reason":Ljava/lang/String;
    .end local v3    # "pressure":I
    :goto_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x5e5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .line 1754
    if-nez p1, :cond_0

    .line 1755
    invoke-virtual {p0, p1}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 1757
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1759
    .local v0, "pw":Ljava/io/PrintWriter;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v2, "debug"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_1
    const-string v2, "clean"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "dump"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    goto :goto_1

    :sswitch_3
    const-string/jumbo v2, "updatelist"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    goto :goto_1

    :sswitch_4
    const-string v2, "enable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 1788
    invoke-virtual {p0, p1}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 1783
    :pswitch_0
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v2}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mupdateProcStatsList(Lcom/android/server/am/PeriodicCleanerService;)V

    .line 1784
    const-string v2, "periodic cleaner update list"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1785
    goto :goto_2

    .line 1777
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 1778
    .local v2, "debug":Z
    invoke-static {v2}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$sfputDEBUG(Z)V

    .line 1779
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "periodic cleaner debug enabled: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1780
    goto :goto_2

    .line 1770
    .end local v2    # "debug":Z
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 1771
    .local v2, "enable":Z
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->mService:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v3, v2}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fputmEnable(Lcom/android/server/am/PeriodicCleanerService;Z)V

    .line 1772
    iget-object v3, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->mService:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v3, v2}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fputmReady(Lcom/android/server/am/PeriodicCleanerService;Z)V

    .line 1773
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "periodic cleaner enabled: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1774
    goto :goto_2

    .line 1766
    .end local v2    # "enable":Z
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->runClean(Ljava/io/PrintWriter;)V

    .line 1767
    goto :goto_2

    .line 1761
    :pswitch_4
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->mService:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v2, v0}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mdumpFgLru(Lcom/android/server/am/PeriodicCleanerService;Ljava/io/PrintWriter;)V

    .line 1762
    iget-object v2, p0, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->mService:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v2, v0}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$mdumpCleanHistory(Lcom/android/server/am/PeriodicCleanerService;Ljava/io/PrintWriter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1763
    nop

    .line 1793
    :goto_2
    goto :goto_4

    .line 1788
    :goto_3
    return v1

    .line 1790
    :catch_0
    move-exception v2

    .line 1791
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error occurred. Check logcat for details. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1792
    const-string v3, "PeriodicCleaner"

    const-string v4, "Error running shell command"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1794
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_4
    return v1

    :sswitch_data_0
    .sparse-switch
        -0x4d6ada7d -> :sswitch_4
        -0x1198a319 -> :sswitch_3
        0x2f39f4 -> :sswitch_2
        0x5a5b649 -> :sswitch_1
        0x5b09653 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 3

    .line 1846
    invoke-virtual {p0}, Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1847
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Periodic Cleaner commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1848
    const-string v1, "  help"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1849
    const-string v1, "    Print this help text."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1850
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1851
    const-string v2, "  dump"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1852
    const-string v2, "    Print fg-lru and clean history."

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1853
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1854
    const-string v2, "  clean [-r REASON]"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1855
    const-string v2, "    Trigger clean action."

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1856
    const-string v2, "      -r: select clean reason"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1857
    const-string v2, "          REASON is one of:"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1858
    const-string v2, "            periodic"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1859
    const-string v2, "            pressure-low"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1860
    const-string v2, "            pressure-min"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1861
    const-string v2, "            pressure-critical"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1862
    const-string v2, "            pressure-low time"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1863
    const-string v2, "            pressure-min time"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1864
    const-string v2, "            pressure-critical time"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1865
    const-string v2, "          default reason is periodic if no REASON"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1866
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1867
    const-string v2, "  enable [true|false]"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1868
    const-string v2, "    Enable/Disable peridic cleaner."

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1869
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1870
    const-string v2, "  debug [true|false]"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1871
    const-string v2, "    Enable/Disable debug config."

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1872
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1873
    const-string v1, "  updatelist"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1874
    const-string v1, "    Update highfrequencyapp and highmemoryapp list."

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1875
    return-void
.end method
