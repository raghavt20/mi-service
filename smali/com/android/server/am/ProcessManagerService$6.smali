.class Lcom/android/server/am/ProcessManagerService$6;
.super Ljava/lang/Object;
.source "ProcessManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/ProcessManagerService;->enableBinderMonitor(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;

.field final synthetic val$enable:I

.field final synthetic val$pid:I


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessManagerService;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/ProcessManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1066
    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$6;->this$0:Lcom/android/server/am/ProcessManagerService;

    iput p2, p0, Lcom/android/server/am/ProcessManagerService$6;->val$pid:I

    iput p3, p0, Lcom/android/server/am/ProcessManagerService$6;->val$enable:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1070
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$6;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmBinderDelayWriter(Lcom/android/server/am/ProcessManagerService;)Ljava/io/FileWriter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/server/am/ProcessManagerService$6;->val$pid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/am/ProcessManagerService$6;->val$enable:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 1071
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$6;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmBinderDelayWriter(Lcom/android/server/am/ProcessManagerService;)Ljava/io/FileWriter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileWriter;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074
    goto :goto_0

    .line 1072
    :catch_0
    move-exception v0

    .line 1073
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ProcessManager"

    const-string v2, "error write exception log"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
