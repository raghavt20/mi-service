.class Lcom/android/server/am/PeriodicCleanerService$6;
.super Landroid/content/BroadcastReceiver;
.source "PeriodicCleanerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/PeriodicCleanerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/PeriodicCleanerService;


# direct methods
.method constructor <init>(Lcom/android/server/am/PeriodicCleanerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/PeriodicCleanerService;

    .line 1731
    iput-object p1, p0, Lcom/android/server/am/PeriodicCleanerService$6;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1734
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1735
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x2

    if-eqz v1, :cond_0

    .line 1736
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$6;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fputmScreenState(Lcom/android/server/am/PeriodicCleanerService;I)V

    .line 1737
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$6;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fgetmHandler(Lcom/android/server/am/PeriodicCleanerService;)Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->removeMessages(I)V

    goto :goto_0

    .line 1738
    :cond_0
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1739
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$6;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v1, v2}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fputmScreenState(Lcom/android/server/am/PeriodicCleanerService;I)V

    .line 1740
    iget-object v1, p0, Lcom/android/server/am/PeriodicCleanerService$6;->this$0:Lcom/android/server/am/PeriodicCleanerService;

    invoke-static {v1}, Lcom/android/server/am/PeriodicCleanerService;->-$$Nest$fgetmHandler(Lcom/android/server/am/PeriodicCleanerService;)Lcom/android/server/am/PeriodicCleanerService$MyHandler;

    move-result-object v1

    const/4 v2, 0x3

    const-wide/32 v3, 0xea60

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1742
    :cond_1
    :goto_0
    return-void
.end method
