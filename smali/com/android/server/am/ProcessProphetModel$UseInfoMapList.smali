.class public final Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
.super Ljava/lang/Object;
.source "ProcessProphetModel.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessProphetModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UseInfoMapList"
.end annotation


# instance fields
.field private doubleDF:Ljava/text/DecimalFormat;

.field myList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;",
            ">;"
        }
    .end annotation
.end field

.field myMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;",
            ">;"
        }
    .end annotation
.end field

.field private needUpdateList:Z

.field public total:D


# direct methods
.method static bridge synthetic -$$Nest$mupdateList(Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;)Ljava/util/ArrayList;
    .locals 0

    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->updateList()Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>()V
    .locals 2

    .line 567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    .line 569
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    .line 570
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->needUpdateList:Z

    .line 571
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.####"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->doubleDF:Ljava/text/DecimalFormat;

    .line 572
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D

    return-void
.end method

.method private updateList()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;",
            ">;"
        }
    .end annotation

    .line 593
    iget-boolean v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->needUpdateList:Z

    if-nez v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    return-object v0

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 597
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 598
    .local v1, "pkg":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    .end local v1    # "pkg":Ljava/lang/String;
    goto :goto_0

    .line 600
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 601
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->needUpdateList:Z

    .line 602
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 664
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 665
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 666
    return-void
.end method

.method public dump()V
    .locals 2

    .line 697
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->toProbString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphetModel"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    return-void
.end method

.method public get(Ljava/lang/String;)D
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 612
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 613
    .local v0, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    if-eqz v0, :cond_0

    .line 614
    iget-wide v1, v0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    return-wide v1

    .line 616
    :cond_0
    const-wide/16 v1, 0x0

    return-wide v1
.end method

.method public getMapSize()I
    .locals 1

    .line 607
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public getPkgPos(Ljava/lang/String;)I
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 710
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->updateList()Ljava/util/ArrayList;

    .line 711
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getProb(Ljava/lang/String;)D
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 621
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->get(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 622
    .local v0, "freq":Ljava/lang/Double;
    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I

    move-result v3

    if-nez v3, :cond_0

    .line 623
    return-wide v1

    .line 625
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->get(Ljava/lang/String;)D

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D

    div-double/2addr v1, v3

    return-wide v1
.end method

.method public getProbsGreaterThan(D)Ljava/util/ArrayList;
    .locals 8
    .param p1, "threshold"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D)",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;",
            ">;"
        }
    .end annotation

    .line 650
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->updateList()Ljava/util/ArrayList;

    .line 651
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 652
    .local v0, "topProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 653
    .local v2, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    iget-wide v3, v2, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    iget-wide v5, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 654
    .local v3, "prob":Ljava/lang/Double;
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I

    move-result v4

    if-ltz v4, :cond_0

    .line 655
    new-instance v4, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    iget-object v5, v2, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v4, v5, v6, v7}, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;-><init>(Ljava/lang/String;D)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 659
    .end local v2    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .end local v3    # "prob":Ljava/lang/Double;
    goto :goto_0

    .line 660
    :cond_0
    return-object v0
.end method

.method public getTopProb()Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .locals 1

    .line 630
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getTopProb(I)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    move-result-object v0

    return-object v0
.end method

.method public getTopProb(I)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    .locals 8
    .param p1, "top"    # I

    .line 635
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->updateList()Ljava/util/ArrayList;

    .line 636
    const/4 v0, 0x0

    .line 637
    .local v0, "topPkgName":Ljava/lang/String;
    const-wide/16 v1, 0x0

    .line 639
    .local v1, "topProb":D
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 640
    .local v3, "target":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    iget-object v4, v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    move-object v0, v4

    .line 641
    iget-wide v4, v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    iget-wide v6, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    div-double v1, v4, v6

    .line 644
    .end local v3    # "target":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    goto :goto_0

    .line 642
    :catch_0
    move-exception v3

    .line 645
    :goto_0
    new-instance v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    invoke-direct {v3, v0, v1, v2}, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;-><init>(Ljava/lang/String;D)V

    return-object v3
.end method

.method public getUseInfoMapTimes()J
    .locals 6

    .line 701
    const-wide/16 v0, 0x0

    .line 702
    .local v0, "uiMapTimes":D
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 703
    .local v3, "mmEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    iget-wide v4, v4, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    add-double/2addr v0, v4

    .line 704
    .end local v3    # "mmEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    goto :goto_0

    .line 705
    :cond_0
    double-to-long v2, v0

    return-wide v2
.end method

.method public toProbString()Ljava/lang/String;
    .locals 8

    .line 683
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->updateList()Ljava/util/ArrayList;

    .line 684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->doubleDF:Ljava/text/DecimalFormat;

    iget-wide v2, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 685
    .local v0, "ret":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 686
    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 687
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "......"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 688
    goto :goto_1

    .line 690
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Top"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 691
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    iget-object v3, v3, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 692
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->doubleDF:Ljava/text/DecimalFormat;

    iget-object v4, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    iget-wide v4, v4, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    iget-wide v6, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 685
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 694
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 670
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->updateList()Ljava/util/ArrayList;

    .line 671
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->doubleDF:Ljava/text/DecimalFormat;

    iget-wide v2, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 672
    .local v0, "ret":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 673
    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 674
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "......"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 675
    goto :goto_1

    .line 677
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Top"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 678
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 672
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 680
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return-object v0
.end method

.method public update(Lcom/android/server/am/ProcessProphetModel$PkgValuePair;)V
    .locals 3
    .param p1, "pkgValuePair"    # Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 588
    iget-object v0, p1, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    iget-wide v1, p1, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V

    .line 589
    return-void
.end method

.method public update(Ljava/lang/String;D)V
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "useTime"    # D

    .line 576
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 577
    .local v0, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    if-nez v0, :cond_0

    .line 578
    new-instance v1, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;-><init>(Ljava/lang/String;D)V

    move-object v0, v1

    goto :goto_0

    .line 580
    :cond_0
    iget-wide v1, v0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    add-double/2addr v1, p2

    iput-wide v1, v0, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    .line 582
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->myMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    iget-wide v1, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D

    add-double/2addr v1, p2

    iput-wide v1, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->total:D

    .line 584
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->needUpdateList:Z

    .line 585
    return-void
.end method
