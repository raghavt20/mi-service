class com.android.server.am.ScoutMemoryError$2 implements com.android.server.am.MiuiWarnings$WarningCallback {
	 /* .source "ScoutMemoryError.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ScoutMemoryError;->showDisplayMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Ljava/lang/String;Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ScoutMemoryError this$0; //synthetic
final com.android.server.am.ProcessRecord val$app; //synthetic
final com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo val$errorInfo; //synthetic
final java.lang.String val$reason; //synthetic
/* # direct methods */
 com.android.server.am.ScoutMemoryError$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ScoutMemoryError; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 90 */
this.this$0 = p1;
this.val$app = p2;
this.val$reason = p3;
this.val$errorInfo = p4;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onCallback ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "positive" # Z */
/* .line 93 */
final String v0 = "ScoutMemoryError"; // const-string v0, "ScoutMemoryError"
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 94 */
/* const-string/jumbo v1, "showDisplayMemoryErrorDialog ok" */
android.util.Slog .d ( v0,v1 );
/* .line 95 */
v0 = this.this$0;
v1 = this.val$app;
v2 = this.val$reason;
(( com.android.server.am.ScoutMemoryError ) v0 ).scheduleCrashApp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/am/ScoutMemoryError;->scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
/* .line 96 */
v0 = this.val$errorInfo;
int v1 = 4; // const/4 v1, 0x4
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V
/* .line 98 */
} // :cond_0
/* const-string/jumbo v1, "showDisplayMemoryErrorDialog cancel" */
android.util.Slog .d ( v0,v1 );
/* .line 99 */
v0 = this.val$errorInfo;
int v1 = 5; // const/4 v1, 0x5
(( com.miui.server.stability.ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;->setAction(I)V
/* .line 100 */
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v0 ).setDisableState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->setDisableState(Z)V
/* .line 102 */
} // :goto_0
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v0 ).setShowDialogState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->setShowDialogState(Z)V
/* .line 103 */
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v0 ).updateLastReportTime ( ); // invoke-virtual {v0}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->updateLastReportTime()V
/* .line 104 */
com.miui.server.stability.ScoutDisplayMemoryManager .getInstance ( );
v1 = this.val$errorInfo;
(( com.miui.server.stability.ScoutDisplayMemoryManager ) v0 ).reportDisplayMemoryLeakEvent ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/stability/ScoutDisplayMemoryManager;->reportDisplayMemoryLeakEvent(Lcom/miui/server/stability/ScoutDisplayMemoryManager$DiaplayMemoryErrorInfo;)V
/* .line 105 */
return;
} // .end method
