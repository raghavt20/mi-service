.class Lcom/android/server/am/AppStateManager$CurrentTaskStack;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CurrentTaskStack"
.end annotation


# instance fields
.field private final mTaskRecords:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lcom/android/server/am/AppStateManager$TaskRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mTopTask:Lcom/android/server/am/AppStateManager$TaskRecord;

.field final synthetic this$0:Lcom/android/server/am/AppStateManager;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/am/AppStateManager;

    .line 3452
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3453
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    return-void
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "opti"    # I

    .line 3496
    const-string v0, "#tasks"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3497
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    monitor-enter v0

    .line 3498
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$TaskRecord;

    .line 3499
    .local v2, "task":Lcom/android/server/am/AppStateManager$TaskRecord;
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/server/am/AppStateManager$TaskRecord;->dump(Ljava/io/PrintWriter;[Ljava/lang/String;I)V

    .line 3500
    .end local v2    # "task":Lcom/android/server/am/AppStateManager$TaskRecord;
    goto :goto_0

    .line 3501
    :cond_0
    monitor-exit v0

    .line 3502
    return-void

    .line 3501
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method isProcessInTaskStack(II)Z
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 3474
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    monitor-enter v0

    .line 3475
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$TaskRecord;

    .line 3476
    .local v2, "task":Lcom/android/server/am/AppStateManager$TaskRecord;
    invoke-virtual {v2, p1, p2}, Lcom/android/server/am/AppStateManager$TaskRecord;->isProcessInTask(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3477
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 3479
    .end local v2    # "task":Lcom/android/server/am/AppStateManager$TaskRecord;
    :cond_0
    goto :goto_0

    .line 3480
    :cond_1
    monitor-exit v0

    .line 3481
    const/4 v0, 0x0

    return v0

    .line 3480
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method isProcessInTaskStack(Ljava/lang/String;)Z
    .locals 4
    .param p1, "prcName"    # Ljava/lang/String;

    .line 3485
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    monitor-enter v0

    .line 3486
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/AppStateManager$TaskRecord;

    .line 3487
    .local v2, "task":Lcom/android/server/am/AppStateManager$TaskRecord;
    invoke-virtual {v2, p1}, Lcom/android/server/am/AppStateManager$TaskRecord;->isProcessInTask(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3488
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 3490
    .end local v2    # "task":Lcom/android/server/am/AppStateManager$TaskRecord;
    :cond_0
    goto :goto_0

    .line 3491
    :cond_1
    monitor-exit v0

    .line 3492
    const/4 v0, 0x0

    return v0

    .line 3491
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method updateIfNeeded(ILcom/android/server/am/AppStateManager$AppState$RunningProcess;ILjava/lang/String;)V
    .locals 3
    .param p1, "taskId"    # I
    .param p2, "proc"    # Lcom/android/server/am/AppStateManager$AppState$RunningProcess;
    .param p3, "launchedFromUid"    # I
    .param p4, "launchedFromPkg"    # Ljava/lang/String;

    .line 3458
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    monitor-enter v0

    .line 3459
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTopTask:Lcom/android/server/am/AppStateManager$TaskRecord;

    if-eqz v1, :cond_0

    iget v1, v1, Lcom/android/server/am/AppStateManager$TaskRecord;->mTaskId:I

    if-eq v1, p1, :cond_2

    .line 3460
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTopTask:Lcom/android/server/am/AppStateManager$TaskRecord;

    if-eqz v1, :cond_1

    iget v1, v1, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastUid:I

    if-eq p3, v1, :cond_1

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTopTask:Lcom/android/server/am/AppStateManager$TaskRecord;

    iget-object v1, v1, Lcom/android/server/am/AppStateManager$TaskRecord;->mLastPkg:Ljava/lang/String;

    .line 3461
    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3462
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->clear()V

    .line 3464
    :cond_1
    new-instance v1, Lcom/android/server/am/AppStateManager$TaskRecord;

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-direct {v1, v2, p1}, Lcom/android/server/am/AppStateManager$TaskRecord;-><init>(Lcom/android/server/am/AppStateManager;I)V

    iput-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTopTask:Lcom/android/server/am/AppStateManager$TaskRecord;

    .line 3465
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTaskRecords:Ljava/util/Stack;

    invoke-virtual {v2, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3467
    :cond_2
    iget-object v1, p0, Lcom/android/server/am/AppStateManager$CurrentTaskStack;->mTopTask:Lcom/android/server/am/AppStateManager$TaskRecord;

    if-eqz v1, :cond_3

    .line 3468
    invoke-virtual {v1, p2}, Lcom/android/server/am/AppStateManager$TaskRecord;->addProcess(Lcom/android/server/am/AppStateManager$AppState$RunningProcess;)V

    .line 3470
    :cond_3
    monitor-exit v0

    .line 3471
    return-void

    .line 3470
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
