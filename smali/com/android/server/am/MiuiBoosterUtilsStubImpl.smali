.class public Lcom/android/server/am/MiuiBoosterUtilsStubImpl;
.super Ljava/lang/Object;
.source "MiuiBoosterUtilsStubImpl.java"

# interfaces
.implements Lcom/android/server/am/MiuiBoosterUtilsStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;
    }
.end annotation


# static fields
.field private static final CLOUD_MIUIBOOSTER_PRIO:Ljava/lang/String; = "cloud_schedboost_enable"

.field public static final COLD_LAUNCH_ATT_RT_DURATION_MS:I = 0x7d0

.field public static final COLD_LAUNCH_REN_RT_DURATION_MS:I = 0x3e8

.field private static final DEBUG:Z

.field public static final HOT_LAUNCH_RT_DURATION_MS:I = 0x1f4

.field private static final MSG_REGISTER_CLOUD_OBSERVER:I = 0x1

.field private static final REQUEST_THREAD_PRIORITY:I = 0x2

.field public static final SCROLL_RT_DURATION_MS:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "MiuiBoosterUtils"

.field public static final THREAD_PRIORITY_NORMAL_MAX:I = 0x64

.field private static final defaultPkg:Ljava/lang/String; = "systemCall"

.field private static enable_miuibooster:Z

.field private static enable_miuibooster_launch:Z

.field private static hasPermission:Z

.field private static isInit:Z

.field private static sService:Landroid/os/IBinder;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mH:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 31
    const-string v0, "persist.miuibooster.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    .line 35
    nop

    .line 36
    const-string v0, "persist.sys.miuibooster.rtmode"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    .line 37
    nop

    .line 38
    const-string v0, "persist.sys.miuibooster.launch.rtmode"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster_launch:Z

    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 40
    sput-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    .line 41
    sput-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->hasPermission:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public cancelBindCore(II)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "req_id"    # I

    .line 225
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v1, "MiuiBoosterUtils"

    if-eqz v0, :cond_0

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "thread affinity is canceled, uid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ,req_id: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 229
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 231
    .local v2, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v3, :cond_1

    const-string v3, "miuiboosterservice"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    :cond_1
    sput-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 232
    if-nez v3, :cond_4

    .line 233
    const-string v3, "miuibooster service is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    if-eqz v0, :cond_2

    .line 246
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 248
    :cond_2
    if-eqz v2, :cond_3

    .line 249
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 234
    :cond_3
    return-void

    .line 236
    :cond_4
    :try_start_1
    const-string v1, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 237
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 238
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 239
    sget-object v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/16 v3, 0x16

    const/4 v4, 0x0

    invoke-interface {v1, v3, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245
    if-eqz v0, :cond_5

    .line 246
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 248
    :cond_5
    if-eqz v2, :cond_8

    .line 249
    goto :goto_0

    .line 245
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 240
    :catch_0
    move-exception v1

    .line 241
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    if-eqz v3, :cond_6

    .line 242
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 245
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    if-eqz v0, :cond_7

    .line 246
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 248
    :cond_7
    if-eqz v2, :cond_8

    .line 249
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 252
    :cond_8
    return-void

    .line 245
    :goto_1
    if-eqz v0, :cond_9

    .line 246
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 248
    :cond_9
    if-eqz v2, :cond_a

    .line 249
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 251
    :cond_a
    throw v1
.end method

.method public cancelCpuHighFreq(I)V
    .locals 5
    .param p1, "uid"    # I

    .line 292
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v1, "MiuiBoosterUtils"

    if-eqz v0, :cond_0

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CpuHighFreq is canceled, uid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 296
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 298
    .local v2, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v3, :cond_1

    const-string v3, "miuiboosterservice"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    :cond_1
    sput-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 299
    if-nez v3, :cond_4

    .line 300
    const-string v3, "miuibooster service is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    if-eqz v0, :cond_2

    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 314
    :cond_2
    if-eqz v2, :cond_3

    .line 315
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 301
    :cond_3
    return-void

    .line 303
    :cond_4
    :try_start_1
    const-string v1, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 304
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 305
    sget-object v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-interface {v1, v3, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    if-eqz v0, :cond_5

    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 314
    :cond_5
    if-eqz v2, :cond_8

    .line 315
    goto :goto_0

    .line 311
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 306
    :catch_0
    move-exception v1

    .line 307
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    if-eqz v3, :cond_6

    .line 308
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    if-eqz v0, :cond_7

    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 314
    :cond_7
    if-eqz v2, :cond_8

    .line 315
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 318
    :cond_8
    return-void

    .line 311
    :goto_1
    if-eqz v0, :cond_9

    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 314
    :cond_9
    if-eqz v2, :cond_a

    .line 315
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 317
    :cond_a
    throw v1
.end method

.method public cancelThreadPriority(II)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "req_id"    # I

    .line 149
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v1, "MiuiBoosterUtils"

    if-eqz v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "thread_priority is canceled, uid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ,req_id: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 153
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 155
    .local v2, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v3, :cond_1

    const-string v3, "miuiboosterservice"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    :cond_1
    sput-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 156
    if-nez v3, :cond_4

    .line 157
    const-string v3, "miuibooster service is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    if-eqz v0, :cond_2

    .line 170
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 172
    :cond_2
    if-eqz v2, :cond_3

    .line 173
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 158
    :cond_3
    return-void

    .line 160
    :cond_4
    :try_start_1
    const-string v1, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 161
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 162
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 163
    sget-object v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/16 v3, 0x1c

    const/4 v4, 0x0

    invoke-interface {v1, v3, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    if-eqz v0, :cond_5

    .line 170
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 172
    :cond_5
    if-eqz v2, :cond_8

    .line 173
    goto :goto_0

    .line 169
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 164
    :catch_0
    move-exception v1

    .line 165
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    if-eqz v3, :cond_6

    .line 166
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 169
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    if-eqz v0, :cond_7

    .line 170
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 172
    :cond_7
    if-eqz v2, :cond_8

    .line 173
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 176
    :cond_8
    return-void

    .line 169
    :goto_1
    if-eqz v0, :cond_9

    .line 170
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 172
    :cond_9
    if-eqz v2, :cond_a

    .line 173
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 175
    :cond_a
    throw v1
.end method

.method public checkPermission(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 64
    const/4 v0, -0x1

    .line 65
    .local v0, "ret":I
    sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v2, "MiuiBoosterUtils"

    if-eqz v1, :cond_0

    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "check_permission ,uid: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,package_name: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_0
    sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->hasPermission:Z

    if-eqz v1, :cond_1

    .line 69
    return v1

    .line 71
    :cond_1
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 72
    .local v1, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 74
    .local v3, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v4, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v4, :cond_2

    const-string v4, "miuiboosterservice"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    :cond_2
    sput-object v4, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 75
    const/4 v5, 0x0

    if-nez v4, :cond_5

    .line 76
    const-string v4, "miuibooster service is null"

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    nop

    .line 86
    if-eqz v1, :cond_3

    .line 87
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 89
    :cond_3
    if-eqz v3, :cond_4

    .line 90
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V

    .line 91
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 92
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 77
    :cond_4
    return v5

    .line 79
    :cond_5
    :try_start_1
    const-string v2, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    sget-object v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/4 v4, 0x2

    invoke-interface {v2, v4, v1, v3, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    if-eqz v1, :cond_6

    .line 87
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 89
    :cond_6
    if-eqz v3, :cond_8

    .line 90
    goto :goto_0

    .line 86
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 83
    :catch_0
    move-exception v2

    .line 84
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    .end local v2    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_7

    .line 87
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 89
    :cond_7
    if-eqz v3, :cond_8

    .line 90
    :goto_0
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V

    .line 91
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 92
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 95
    :cond_8
    if-nez v0, :cond_9

    .line 96
    sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->hasPermission:Z

    return v2

    .line 98
    :cond_9
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->hasPermission:Z

    .line 99
    return v2

    .line 86
    :goto_1
    if-eqz v1, :cond_a

    .line 87
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 89
    :cond_a
    if-eqz v3, :cond_b

    .line 90
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V

    .line 91
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 92
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 94
    :cond_b
    throw v2
.end method

.method public init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 321
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    if-eqz v0, :cond_0

    .line 322
    return-void

    .line 324
    :cond_0
    iput-object p1, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mContext:Landroid/content/Context;

    .line 325
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MiuiBoosterUtilsTh"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mHandlerThread:Landroid/os/HandlerThread;

    .line 326
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 327
    new-instance v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1;

    iget-object v1, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$1;-><init>(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mH:Landroid/os/Handler;

    .line 348
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 349
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mH:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 350
    sput-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    .line 351
    const-string v1, "MiuiBoosterUtils"

    const-string v2, "init MiuiBoosterUtils"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    return-void
.end method

.method public notifyForegroundAppChanged(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 380
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v1, "MiuiBoosterUtils"

    if-eqz v0, :cond_0

    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyForegroundAppChanged: packageName="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 385
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 387
    .local v2, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v3, :cond_1

    const-string v3, "miuiboosterservice"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    :cond_1
    sput-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 388
    if-nez v3, :cond_4

    .line 389
    const-string v3, "miuibooster service is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    if-eqz v0, :cond_2

    .line 399
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 401
    :cond_2
    if-eqz v2, :cond_3

    .line 402
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 390
    :cond_3
    return-void

    .line 392
    :cond_4
    :try_start_1
    const-string v1, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 393
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 394
    sget-object v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/16 v3, 0x34

    const/4 v4, 0x0

    invoke-interface {v1, v3, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    if-eqz v0, :cond_5

    .line 399
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 401
    :cond_5
    if-eqz v2, :cond_7

    .line 402
    goto :goto_0

    .line 398
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 395
    :catch_0
    move-exception v1

    .line 396
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 398
    .end local v1    # "e":Ljava/lang/Exception;
    if-eqz v0, :cond_6

    .line 399
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 401
    :cond_6
    if-eqz v2, :cond_7

    .line 402
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 405
    :cond_7
    return-void

    .line 398
    :goto_1
    if-eqz v0, :cond_8

    .line 399
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 401
    :cond_8
    if-eqz v2, :cond_9

    .line 402
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 404
    :cond_9
    throw v1
.end method

.method public notifyPowerModeChanged(Z)V
    .locals 5
    .param p1, "isPowerMode"    # Z

    .line 408
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v1, "MiuiBoosterUtils"

    if-eqz v0, :cond_0

    .line 409
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyPowerModeStatus: isPowerMode="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 413
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 415
    .local v2, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v3, :cond_1

    const-string v3, "miuiboosterservice"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    :cond_1
    sput-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 416
    if-nez v3, :cond_4

    .line 417
    const-string v3, "miuibooster service is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    if-eqz v0, :cond_2

    .line 427
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 429
    :cond_2
    if-eqz v2, :cond_3

    .line 430
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 418
    :cond_3
    return-void

    .line 420
    :cond_4
    :try_start_1
    const-string v1, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 421
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeBoolean(Z)V

    .line 422
    sget-object v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/16 v3, 0x39

    const/4 v4, 0x0

    invoke-interface {v1, v3, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 426
    if-eqz v0, :cond_5

    .line 427
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 429
    :cond_5
    if-eqz v2, :cond_7

    .line 430
    goto :goto_0

    .line 426
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 423
    :catch_0
    move-exception v1

    .line 424
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 426
    .end local v1    # "e":Ljava/lang/Exception;
    if-eqz v0, :cond_6

    .line 427
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 429
    :cond_6
    if-eqz v2, :cond_7

    .line 430
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 433
    :cond_7
    return-void

    .line 426
    :goto_1
    if-eqz v0, :cond_8

    .line 427
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 429
    :cond_8
    if-eqz v2, :cond_9

    .line 430
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 432
    :cond_9
    throw v1
.end method

.method public registerCloudObserver(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .line 355
    new-instance v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$2;

    iget-object v1, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mH:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$2;-><init>(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;Landroid/os/Handler;)V

    .line 363
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 364
    const-string v2, "cloud_schedboost_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 363
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 366
    const-string v1, "MiuiBoosterUtils"

    const-string v2, "registerCloudObserver: cloud_schedboost_enable"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    return-void
.end method

.method public requestBindCore(I[III)I
    .locals 6
    .param p1, "uid"    # I
    .param p2, "req_tids"    # [I
    .param p3, "timeout"    # I
    .param p4, "level"    # I

    .line 179
    const/4 v0, -0x1

    .line 180
    .local v0, "ret":I
    sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v2, "MiuiBoosterUtils"

    if-eqz v1, :cond_0

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "thread affinity is reuqesting, uid:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,req_tids: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,timeout:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_0
    const-string/jumbo v1, "systemCall"

    invoke-virtual {p0, v1, p1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->checkPermission(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 184
    const-string v1, "check_permission : false"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    return v0

    .line 187
    :cond_1
    sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    if-eqz v1, :cond_f

    sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    if-nez v1, :cond_2

    goto/16 :goto_4

    .line 191
    :cond_2
    if-nez p2, :cond_3

    return v0

    .line 192
    :cond_3
    array-length v1, p2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v1, :cond_5

    aget v5, p2, v4

    .line 193
    .local v5, "tid":I
    if-gtz v5, :cond_4

    return v0

    .line 192
    .end local v5    # "tid":I
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 195
    :cond_5
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 196
    .local v1, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 198
    .local v4, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v5, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v5, :cond_6

    const-string v5, "miuiboosterservice"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    :cond_6
    sput-object v5, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 199
    if-nez v5, :cond_9

    .line 200
    const-string v3, "miuibooster service is null"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    nop

    .line 212
    if-eqz v1, :cond_7

    .line 213
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 215
    :cond_7
    if-eqz v4, :cond_8

    .line 216
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 217
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 218
    .end local v0    # "ret":I
    .local v2, "ret":I
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    goto :goto_1

    .line 215
    .end local v2    # "ret":I
    .restart local v0    # "ret":I
    :cond_8
    move v2, v0

    .line 201
    .end local v0    # "ret":I
    .restart local v2    # "ret":I
    :goto_1
    return v0

    .line 203
    .end local v2    # "ret":I
    .restart local v0    # "ret":I
    :cond_9
    :try_start_1
    const-string v2, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 204
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 205
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 206
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 207
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 208
    sget-object v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/16 v5, 0x15

    invoke-interface {v2, v5, v1, v4, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    if-eqz v1, :cond_a

    .line 213
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 215
    :cond_a
    if-eqz v4, :cond_c

    .line 216
    goto :goto_2

    .line 212
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 209
    :catch_0
    move-exception v2

    .line 210
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 212
    .end local v2    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_b

    .line 213
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 215
    :cond_b
    if-eqz v4, :cond_c

    .line 216
    :goto_2
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 217
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 218
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 221
    :cond_c
    return v0

    .line 212
    :goto_3
    if-eqz v1, :cond_d

    .line 213
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 215
    :cond_d
    if-eqz v4, :cond_e

    .line 216
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 217
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 218
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 220
    :cond_e
    throw v2

    .line 188
    .end local v1    # "data":Landroid/os/Parcel;
    .end local v4    # "reply":Landroid/os/Parcel;
    :cond_f
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable_miuibooster : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,isInit: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    return v0
.end method

.method public requestCpuHighFreq(III)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "level"    # I
    .param p3, "timeout"    # I

    .line 255
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v1, "MiuiBoosterUtils"

    if-eqz v0, :cond_0

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CpuHighFreq is reuqesting, uid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ,level: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ,timeout:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_0
    const-string/jumbo v0, "systemCall"

    invoke-virtual {p0, v0, p1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->checkPermission(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    const-string v0, "check_permission : false"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    return-void

    .line 262
    :cond_1
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    if-eqz v0, :cond_c

    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    if-nez v0, :cond_2

    goto :goto_2

    .line 266
    :cond_2
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 267
    .local v0, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 269
    .local v2, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v3, :cond_3

    const-string v3, "miuiboosterservice"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    :cond_3
    sput-object v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 270
    if-nez v3, :cond_6

    .line 271
    const-string v3, "miuibooster service is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 282
    if-eqz v0, :cond_4

    .line 283
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 285
    :cond_4
    if-eqz v2, :cond_5

    .line 286
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 272
    :cond_5
    return-void

    .line 274
    :cond_6
    :try_start_1
    const-string v1, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 276
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 277
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 278
    sget-object v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-interface {v1, v3, v0, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282
    if-eqz v0, :cond_7

    .line 283
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 285
    :cond_7
    if-eqz v2, :cond_9

    .line 286
    goto :goto_0

    .line 282
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 279
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 282
    .end local v1    # "e":Ljava/lang/Exception;
    if-eqz v0, :cond_8

    .line 283
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 285
    :cond_8
    if-eqz v2, :cond_9

    .line 286
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 289
    :cond_9
    return-void

    .line 282
    :goto_1
    if-eqz v0, :cond_a

    .line 283
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 285
    :cond_a
    if-eqz v2, :cond_b

    .line 286
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 288
    :cond_b
    throw v1

    .line 263
    .end local v0    # "data":Landroid/os/Parcel;
    .end local v2    # "reply":Landroid/os/Parcel;
    :cond_c
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enable_miuibooster : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ,isInit: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    return-void
.end method

.method public requestLaunchThreadPriority(I[III)V
    .locals 8
    .param p1, "uid"    # I
    .param p2, "req_tids"    # [I
    .param p3, "timeout"    # I
    .param p4, "level"    # I

    .line 49
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster_launch:Z

    const-string v1, "MiuiBoosterUtils"

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enable_miuibooster_launch : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster_launch:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    return-void

    .line 53
    :cond_0
    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    if-nez v0, :cond_1

    goto :goto_0

    .line 57
    :cond_1
    new-instance v0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;

    move-object v2, v0

    move-object v3, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v2 .. v7}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;-><init>(Lcom/android/server/am/MiuiBoosterUtilsStubImpl;I[III)V

    .line 58
    .local v0, "info":Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;
    iget-object v1, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mH:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 59
    .local v1, "msg":Landroid/os/Message;
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 60
    iget-object v2, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mH:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 61
    return-void

    .line 54
    .end local v0    # "info":Lcom/android/server/am/MiuiBoosterUtilsStubImpl$MiuiBoosterInfo;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enable_miuibooster : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ,isInit: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method

.method public requestThreadPriority(I[III)I
    .locals 6
    .param p1, "uid"    # I
    .param p2, "req_tids"    # [I
    .param p3, "timeout"    # I
    .param p4, "level"    # I

    .line 103
    const/4 v0, -0x1

    .line 104
    .local v0, "ret":I
    sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->DEBUG:Z

    const-string v2, "MiuiBoosterUtils"

    if-eqz v1, :cond_0

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "thread_priority is reuqesting, uid:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,req_tids: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,timeout:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_0
    sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    if-eqz v1, :cond_f

    sget-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    if-nez v1, :cond_1

    goto/16 :goto_4

    .line 111
    :cond_1
    const-string/jumbo v1, "systemCall"

    invoke-virtual {p0, v1, p1}, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->checkPermission(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 112
    const-string v1, "check_permission : false"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return v0

    .line 115
    :cond_2
    if-nez p2, :cond_3

    return v0

    .line 116
    :cond_3
    array-length v1, p2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v1, :cond_5

    aget v5, p2, v4

    .line 117
    .local v5, "tid":I
    if-gtz v5, :cond_4

    return v0

    .line 116
    .end local v5    # "tid":I
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 119
    :cond_5
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 120
    .local v1, "data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 122
    .local v4, "reply":Landroid/os/Parcel;
    :try_start_0
    sget-object v5, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    if-nez v5, :cond_6

    const-string v5, "miuiboosterservice"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    :cond_6
    sput-object v5, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    .line 123
    if-nez v5, :cond_9

    .line 124
    const-string v3, "miuibooster service is null"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    nop

    .line 136
    if-eqz v1, :cond_7

    .line 137
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 139
    :cond_7
    if-eqz v4, :cond_8

    .line 140
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 141
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 142
    .end local v0    # "ret":I
    .local v2, "ret":I
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    goto :goto_1

    .line 139
    .end local v2    # "ret":I
    .restart local v0    # "ret":I
    :cond_8
    move v2, v0

    .line 125
    .end local v0    # "ret":I
    .restart local v2    # "ret":I
    :goto_1
    return v0

    .line 127
    .end local v2    # "ret":I
    .restart local v0    # "ret":I
    :cond_9
    :try_start_1
    const-string v2, "com.miui.performance.IMiuiBoosterManager"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 130
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    sget-object v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->sService:Landroid/os/IBinder;

    const/16 v5, 0x1b

    invoke-interface {v2, v5, v1, v4, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    if-eqz v1, :cond_a

    .line 137
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 139
    :cond_a
    if-eqz v4, :cond_c

    .line 140
    goto :goto_2

    .line 136
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 133
    :catch_0
    move-exception v2

    .line 134
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 136
    .end local v2    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_b

    .line 137
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 139
    :cond_b
    if-eqz v4, :cond_c

    .line 140
    :goto_2
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 141
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 142
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 145
    :cond_c
    return v0

    .line 136
    :goto_3
    if-eqz v1, :cond_d

    .line 137
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 139
    :cond_d
    if-eqz v4, :cond_e

    .line 140
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 141
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 142
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 144
    :cond_e
    throw v2

    .line 108
    .end local v1    # "data":Landroid/os/Parcel;
    .end local v4    # "reply":Landroid/os/Parcel;
    :cond_f
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable_miuibooster : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,isInit: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v3, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->isInit:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return v0
.end method

.method public updateCloudControlParas()V
    .locals 3

    .line 370
    iget-object v0, p0, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->mContext:Landroid/content/Context;

    .line 371
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 370
    const-string v1, "cloud_schedboost_enable"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "cloudMiuiboosterPara":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 373
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    .line 375
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateCloudControlParas cloudMiuiboosterPara: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,enable_miuibooster: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/server/am/MiuiBoosterUtilsStubImpl;->enable_miuibooster:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiBoosterUtils"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    return-void
.end method
