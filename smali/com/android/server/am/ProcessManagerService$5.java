class com.android.server.am.ProcessManagerService$5 implements java.lang.Runnable {
	 /* .source "ProcessManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessManagerService;->notifyActivityChanged(Landroid/content/ComponentName;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessManagerService this$0; //synthetic
final android.content.ComponentName val$curActivityComponent; //synthetic
/* # direct methods */
 com.android.server.am.ProcessManagerService$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1035 */
this.this$0 = p1;
this.val$curActivityComponent = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 2 */
/* .line 1038 */
v0 = this.this$0;
com.android.server.am.ProcessManagerService .-$$Nest$fgetmForegroundInfoManager ( v0 );
v1 = this.val$curActivityComponent;
(( com.android.server.wm.ForegroundInfoManager ) v0 ).notifyActivityChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/ForegroundInfoManager;->notifyActivityChanged(Landroid/content/ComponentName;)V
/* .line 1039 */
v0 = this.val$curActivityComponent;
/* filled-new-array {v0}, [Ljava/lang/Object; */
final String v1 = "notifyActivityChanged"; // const-string v1, "notifyActivityChanged"
com.android.server.camera.CameraOpt .callMethod ( v1,v0 );
/* .line 1040 */
return;
} // .end method
