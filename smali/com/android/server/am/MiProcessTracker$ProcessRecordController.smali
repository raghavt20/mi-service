.class Lcom/android/server/am/MiProcessTracker$ProcessRecordController;
.super Ljava/lang/Object;
.source "MiProcessTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiProcessTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProcessRecordController"
.end annotation


# static fields
.field private static final APP_MAX_SWITCH_BACKGROUND_TIME:J = 0x5265c00L


# instance fields
.field private mLastTimeKillReason:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRetentionFilterKillReasonList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/server/am/MiProcessTracker;


# direct methods
.method static bridge synthetic -$$Nest$misAppFirstStart(Lcom/android/server/am/MiProcessTracker$ProcessRecordController;Lcom/miui/server/smartpower/IAppState;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->isAppFirstStart(Lcom/miui/server/smartpower/IAppState;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mrecordKillProcessIfNeed(Lcom/android/server/am/MiProcessTracker$ProcessRecordController;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->recordKillProcessIfNeed(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJ)V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/am/MiProcessTracker;)V
    .locals 8
    .param p1, "this$0"    # Lcom/android/server/am/MiProcessTracker;

    .line 277
    iput-object p1, p0, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->mLastTimeKillReason:Ljava/util/HashMap;

    .line 278
    const-string v1, "OneKeyClean"

    const-string v2, "ForceClean"

    const-string v3, "GarbageClean"

    const-string v4, "LockScreenClean"

    const-string v5, "GameClean"

    const-string v6, "OptimizationClean"

    const-string v7, "SwipeUpClean"

    invoke-static/range {v1 .. v7}, Ljava/util/List;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->mRetentionFilterKillReasonList:Ljava/util/List;

    .line 286
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/MiProcessTracker;Lcom/android/server/am/MiProcessTracker$ProcessRecordController-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;-><init>(Lcom/android/server/am/MiProcessTracker;)V

    return-void
.end method

.method private isAppFirstStart(Lcom/miui/server/smartpower/IAppState;)Z
    .locals 6
    .param p1, "appState"    # Lcom/miui/server/smartpower/IAppState;

    .line 335
    iget-object v0, p0, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->mLastTimeKillReason:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 336
    return v1

    .line 338
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->getLastTopTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 339
    .local v2, "backgroundTime":J
    const-wide/32 v4, 0x5265c00

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 340
    return v1

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->mLastTimeKillReason:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->needFilterReason(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private needFilterReason(Ljava/lang/String;)Z
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .line 346
    if-eqz p1, :cond_1

    .line 347
    iget-object v0, p0, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->mRetentionFilterKillReasonList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 348
    .local v1, "filterReason":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 349
    const/4 v0, 0x1

    return v0

    .line 351
    .end local v1    # "filterReason":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 353
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private recordKillProcessIfNeed(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 27
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "reason"    # Ljava/lang/String;
    .param p5, "adj"    # I
    .param p6, "pss"    # J
    .param p8, "rss"    # J

    .line 295
    move-object/from16 v15, p0

    move-object/from16 v14, p1

    move-object/from16 v13, p2

    move/from16 v11, p3

    move-object/from16 v12, p4

    iget-object v0, v15, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v0}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmSmartPowerService(Lcom/android/server/am/MiProcessTracker;)Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    move-result-object v0

    .line 296
    invoke-interface {v0, v11, v13}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getRunningProcess(ILjava/lang/String;)Lcom/miui/server/smartpower/IAppState$IRunningProcess;

    move-result-object v16

    .line 297
    .local v16, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz v16, :cond_0

    invoke-virtual/range {v16 .. v16}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasActivity()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    move v9, v2

    .line 298
    .local v9, "hasActivities":Z
    if-eqz v16, :cond_1

    .line 299
    invoke-virtual/range {v16 .. v16}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->hasForegrundService()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    move v10, v0

    .line 300
    .local v10, "hasForegroundServices":Z
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz v9, :cond_2

    goto :goto_2

    :cond_2
    move/from16 v25, v9

    move/from16 v26, v10

    move-object v2, v12

    move-object v1, v14

    goto/16 :goto_7

    .line 301
    :cond_3
    :goto_2
    iget-object v0, v15, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v0}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmSmartPowerService(Lcom/android/server/am/MiProcessTracker;)Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    move-result-object v0

    .line 302
    invoke-interface {v0, v11}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->getAppState(I)Lcom/miui/server/smartpower/IAppState;

    move-result-object v17

    .line 303
    .local v17, "appState":Lcom/miui/server/smartpower/IAppState;
    if-nez v17, :cond_4

    return-void

    .line 304
    :cond_4
    const-wide/16 v0, 0x0

    .line 305
    .local v0, "residentDuration":J
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->getLastTopTime()J

    move-result-wide v18

    .line 306
    .local v18, "backgroundTime":J
    const-wide/16 v2, 0x0

    cmp-long v2, v18, v2

    if-eqz v2, :cond_5

    .line 308
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v18

    move-wide v7, v0

    goto :goto_3

    .line 306
    :cond_5
    move-wide v7, v0

    .line 310
    .end local v0    # "residentDuration":J
    .local v7, "residentDuration":J
    :goto_3
    const/16 v0, 0x1b

    new-array v5, v0, [J

    .line 311
    .local v5, "memInfo":[J
    invoke-static {v5}, Landroid/os/Debug;->getMemInfo([J)V

    .line 312
    const/16 v0, 0x13

    aget-wide v3, v5, v0

    .line 313
    .local v3, "memAvail":J
    sget-boolean v0, Lcom/android/server/am/MiProcessTracker;->DEBUG:Z

    if-eqz v0, :cond_6

    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reportKillProcessIfNeed packageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " processName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " reason = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adj:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v6, p5

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pss:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-wide/from16 v1, p6

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rss:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-wide/from16 v1, p8

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " memAvail:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " residentDuration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AppSwitchFgCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 319
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->getAppSwitchFgCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hasActivity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hasForegroundService:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 314
    const-string v1, "MiProcessTracker"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 313
    :cond_6
    move/from16 v6, p5

    .line 323
    :goto_4
    invoke-direct {v15, v12}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->needFilterReason(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 324
    invoke-virtual/range {v17 .. v17}, Lcom/miui/server/smartpower/IAppState;->getAppSwitchFgCount()I

    move-result v0

    if-gtz v0, :cond_8

    if-nez v9, :cond_8

    if-eqz v10, :cond_7

    goto :goto_5

    :cond_7
    move-wide/from16 v20, v3

    move-object/from16 v22, v5

    move-wide/from16 v23, v7

    move/from16 v25, v9

    move/from16 v26, v10

    goto :goto_6

    .line 326
    :cond_8
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-wide/from16 v20, v3

    .end local v3    # "memAvail":J
    .local v20, "memAvail":J
    move-object/from16 v3, p4

    move/from16 v4, p5

    move-object/from16 v22, v5

    .end local v5    # "memInfo":[J
    .local v22, "memInfo":[J
    move-wide/from16 v5, p6

    move-wide/from16 v23, v7

    .end local v7    # "residentDuration":J
    .local v23, "residentDuration":J
    move-wide/from16 v7, p8

    move/from16 v25, v9

    move/from16 v26, v10

    .end local v9    # "hasActivities":Z
    .end local v10    # "hasForegroundServices":Z
    .local v25, "hasActivities":Z
    .local v26, "hasForegroundServices":Z
    move-wide/from16 v9, v20

    move-wide/from16 v11, v23

    move/from16 v13, v25

    move/from16 v14, v26

    invoke-direct/range {v0 .. v14}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->reportKillProcessEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJZZ)V

    goto :goto_6

    .line 323
    .end local v20    # "memAvail":J
    .end local v22    # "memInfo":[J
    .end local v23    # "residentDuration":J
    .end local v25    # "hasActivities":Z
    .end local v26    # "hasForegroundServices":Z
    .restart local v3    # "memAvail":J
    .restart local v5    # "memInfo":[J
    .restart local v7    # "residentDuration":J
    .restart local v9    # "hasActivities":Z
    .restart local v10    # "hasForegroundServices":Z
    :cond_9
    move-wide/from16 v20, v3

    move-object/from16 v22, v5

    move-wide/from16 v23, v7

    move/from16 v25, v9

    move/from16 v26, v10

    .line 330
    .end local v3    # "memAvail":J
    .end local v5    # "memInfo":[J
    .end local v7    # "residentDuration":J
    .end local v9    # "hasActivities":Z
    .end local v10    # "hasForegroundServices":Z
    .restart local v20    # "memAvail":J
    .restart local v22    # "memInfo":[J
    .restart local v23    # "residentDuration":J
    .restart local v25    # "hasActivities":Z
    .restart local v26    # "hasForegroundServices":Z
    :goto_6
    iget-object v0, v15, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->mLastTimeKillReason:Ljava/util/HashMap;

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    .end local v17    # "appState":Lcom/miui/server/smartpower/IAppState;
    .end local v18    # "backgroundTime":J
    .end local v20    # "memAvail":J
    .end local v22    # "memInfo":[J
    .end local v23    # "residentDuration":J
    :goto_7
    return-void
.end method

.method private reportKillProcessEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJZZ)V
    .locals 17
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "adj"    # I
    .param p5, "pss"    # J
    .param p7, "rss"    # J
    .param p9, "memAvail"    # J
    .param p11, "residentDuration"    # J
    .param p13, "hasActivities"    # Z
    .param p14, "hasForegroundServices"    # Z

    .line 360
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v0}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmPerfShielder(Lcom/android/server/am/MiProcessTracker;)Lcom/android/internal/app/IPerfShielder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 362
    :try_start_0
    iget-object v0, v1, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->this$0:Lcom/android/server/am/MiProcessTracker;

    invoke-static {v0}, Lcom/android/server/am/MiProcessTracker;->-$$Nest$fgetmPerfShielder(Lcom/android/server/am/MiProcessTracker;)Lcom/android/internal/app/IPerfShielder;

    move-result-object v2

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move-wide/from16 v7, p5

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    move-wide/from16 v13, p11

    move/from16 v15, p13

    move/from16 v16, p14

    invoke-interface/range {v2 .. v16}, Lcom/android/internal/app/IPerfShielder;->reportKillProcessEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    goto :goto_0

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MiProcessTracker"

    const-string v3, "mPerfShielder is dead"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 368
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    return-void
.end method
