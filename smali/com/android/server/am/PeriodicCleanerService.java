public class com.android.server.am.PeriodicCleanerService extends com.android.server.SystemService {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/PeriodicCleanerService$BinderService;, */
	 /* Lcom/android/server/am/PeriodicCleanerService$LocalService;, */
	 /* Lcom/android/server/am/PeriodicCleanerService$MyHandler;, */
	 /* Lcom/android/server/am/PeriodicCleanerService$CleanInfo;, */
	 /* Lcom/android/server/am/PeriodicCleanerService$MyEvent;, */
	 /* Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;, */
	 /* Lcom/android/server/am/PeriodicCleanerService$PeriodicShellCmd; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer AGGREGATE_HOURS;
private static final Integer CACHED_APP_MIN_ADJ;
private static final java.lang.String CLOUD_PERIODIC_ENABLE;
private static final java.lang.String CLOUD_PERIODIC_GAME_ENABLE;
private static final java.lang.String CLOUD_PERIODIC_WHITE_LIST;
private static Boolean DEBUG;
private static final Integer DEVICE_MEM_TYPE_COUNT;
private static final Integer HISTORY_SIZE;
private static final Integer KILL_LEVEL_FORCE_STOP;
private static final Integer KILL_LEVEL_UNKOWN;
private static final Long MAX_MEMORY_VALUE;
private static final Integer MAX_PREVIOUS_TIME;
private static final Integer MEM_NO_PRESSURE;
private static final Integer MEM_PRESSURE_COUNT;
private static final Integer MEM_PRESSURE_CRITICAL;
private static final Integer MEM_PRESSURE_LOW;
private static final Integer MEM_PRESSURE_MIN;
private static final Integer MININUM_AGING_THRESHOLD;
private static final Integer MSG_REPORT_CLEAN_PROCESS;
private static final Integer MSG_REPORT_EVENT;
private static final Integer MSG_REPORT_PRESSURE;
private static final Integer MSG_REPORT_START_PROCESS;
private static final Integer MSG_SCREEN_OFF;
private static final java.lang.String PACKAGE_NAME_CAMERA;
private static final java.lang.String PERIODIC_DEBUG_PROP;
private static final java.lang.String PERIODIC_ENABLE_PROP;
private static final java.lang.String PERIODIC_MEM_THRES_PROP;
private static final java.lang.String PERIODIC_START_PROCESS_ENABLE_PROP;
private static final java.lang.String PERIODIC_TIME_THRES_PROP;
private static final Integer SCREEN_STATE_OFF;
private static final Integer SCREEN_STATE_ON;
private static final Integer SCREEN_STATE_UNKOWN;
private static final java.lang.Integer SYSTEM_UID_OBJ;
private static final java.lang.String TAG;
private static final java.lang.String TIME_FORMAT_PATTERN;
private static final Long UPDATE_PROCSTATS_PERIOD;
private static java.util.List sCleanWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final I sDefaultActiveLength;
private static final I sDefaultCacheLevel;
private static sDefaultTimeLevel;
private static java.util.List sGameApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static sGamePlayAppNumArray;
private static java.util.Set sHighFrequencyApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sHighMemoryApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List sHomeOrRecents;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static sOverTimeAppNumArray;
private static java.util.List sSystemRelatedPkgs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private com.android.server.wm.ActivityTaskManagerService mATMS;
private java.lang.Class mAndroidOsDebugClz;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private Integer mAppMemThreshold;
private com.android.server.audio.AudioService mAudioService;
private java.lang.Class mAudioServiceClz;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.PeriodicCleanerService$BinderService mBinderService;
private java.lang.Class mClassProcessState;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.am.PeriodicCleanerService$CleanInfo mCleanHistory;
private Integer mCleanHistoryIndex;
private android.content.Context mContext;
private java.lang.reflect.Field mDebugClz_Field_MEMINFO_BUFFERS;
private java.lang.reflect.Field mDebugClz_Field_MEMINFO_CACHED;
private java.lang.reflect.Field mDebugClz_Field_MEMINFO_SHMEM;
private java.lang.reflect.Field mDebugClz_Field_MEMINFO_SWAPCACHED;
private java.lang.reflect.Field mDebugClz_Field_MEMINFO_UNEVICTABLE;
private Integer mDebugClz_MEMINFO_BUFFERS;
private Integer mDebugClz_MEMINFO_CACHED;
private Integer mDebugClz_MEMINFO_SHMEM;
private Integer mDebugClz_MEMINFO_SWAPCACHED;
private Integer mDebugClz_MEMINFO_UNEVICTABLE;
private volatile Boolean mEnable;
private volatile Boolean mEnableFgTrim;
private volatile Boolean mEnableGameClean;
private volatile Boolean mEnableStartProcess;
private Integer mFgTrimTheshold;
private java.lang.reflect.Field mFieldProcessState;
private Integer mGamePlayAppNum;
private java.lang.reflect.Method mGetAllAudioFocusMethod;
private java.lang.reflect.Method mGetVisibleWindowOwnerMethod;
private com.android.server.am.PeriodicCleanerService$MyHandler mHandler;
private volatile Boolean mHighDevice;
private java.lang.Class mIApplicationThreadClz;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
private Long mLastCleanByPressure;
private java.lang.String mLastNonSystemFgPkg;
private Integer mLastNonSystemFgUid;
private Long mLastUpdateTime;
private java.util.List mLastVisibleUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.PeriodicCleanerInternalStub mLocalService;
private final java.lang.Object mLock;
private Integer mLruActiveLength;
final java.util.ArrayList mLruPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mOverTimeAppNum;
private com.android.server.pm.PackageManagerService$IPackageManagerImpl mPKMS;
private com.android.server.am.ProcessManagerService mPMS;
private mPressureAgingThreshold;
private mPressureCacheThreshold;
private mPressureTimeThreshold;
private com.android.internal.app.procstats.IProcessStats mProcessStats;
private volatile Boolean mReady;
private android.content.BroadcastReceiver mReceiver;
private java.lang.reflect.Method mScheduleAggressiveTrimMethod;
private volatile Integer mScreenState;
private android.os.HandlerThread mThread;
private android.app.usage.UsageStatsManager mUSM;
private com.android.server.wm.WindowManagerInternal mWMS;
private java.lang.Class mWindowManagerInternalClz;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.am.PeriodicCleanerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmEnable ( com.android.server.am.PeriodicCleanerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
} // .end method
static Boolean -$$Nest$fgetmEnableGameClean ( com.android.server.am.PeriodicCleanerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z */
} // .end method
static com.android.server.am.PeriodicCleanerService$MyHandler -$$Nest$fgetmHandler ( com.android.server.am.PeriodicCleanerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static void -$$Nest$fputmEnable ( com.android.server.am.PeriodicCleanerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
return;
} // .end method
static void -$$Nest$fputmEnableGameClean ( com.android.server.am.PeriodicCleanerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z */
return;
} // .end method
static void -$$Nest$fputmReady ( com.android.server.am.PeriodicCleanerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z */
return;
} // .end method
static void -$$Nest$fputmScreenState ( com.android.server.am.PeriodicCleanerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/android/server/am/PeriodicCleanerService;->mScreenState:I */
return;
} // .end method
static void -$$Nest$mcleanPackageByPeriodic ( com.android.server.am.PeriodicCleanerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByPeriodic()V */
return;
} // .end method
static void -$$Nest$mcleanPackageByPressure ( com.android.server.am.PeriodicCleanerService p0, Integer p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByPressure(ILjava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mcleanPackageByTime ( com.android.server.am.PeriodicCleanerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByTime(I)V */
return;
} // .end method
static void -$$Nest$mdumpCleanHistory ( com.android.server.am.PeriodicCleanerService p0, java.io.PrintWriter p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->dumpCleanHistory(Ljava/io/PrintWriter;)V */
return;
} // .end method
static void -$$Nest$mdumpFgLru ( com.android.server.am.PeriodicCleanerService p0, java.io.PrintWriter p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->dumpFgLru(Ljava/io/PrintWriter;)V */
return;
} // .end method
static void -$$Nest$mhandleScreenOff ( com.android.server.am.PeriodicCleanerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->handleScreenOff()V */
return;
} // .end method
static void -$$Nest$mreportCleanProcess ( com.android.server.am.PeriodicCleanerService p0, Integer p1, Integer p2, java.lang.String p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/PeriodicCleanerService;->reportCleanProcess(IILjava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mreportEvent ( com.android.server.am.PeriodicCleanerService p0, com.android.server.am.PeriodicCleanerService$MyEvent p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->reportEvent(Lcom/android/server/am/PeriodicCleanerService$MyEvent;)V */
return;
} // .end method
static void -$$Nest$mreportMemPressure ( com.android.server.am.PeriodicCleanerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->reportMemPressure(I)V */
return;
} // .end method
static void -$$Nest$mreportStartProcess ( com.android.server.am.PeriodicCleanerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->reportStartProcess(Ljava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mupdateProcStatsList ( com.android.server.am.PeriodicCleanerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateProcStatsList()V */
return;
} // .end method
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
} // .end method
static java.util.List -$$Nest$sfgetsCleanWhiteList ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.PeriodicCleanerService.sCleanWhiteList;
} // .end method
static void -$$Nest$sfputDEBUG ( Boolean p0 ) { //bridge//synthethic
/* .locals 0 */
com.android.server.am.PeriodicCleanerService.DEBUG = (p0!= 0);
return;
} // .end method
static com.android.server.am.PeriodicCleanerService ( ) {
/* .locals 10 */
/* .line 138 */
/* const v0, 0x927c0 */
/* const v1, 0x493e0 */
/* const v2, 0xdbba0 */
/* filled-new-array {v2, v0, v1}, [I */
/* .line 139 */
int v0 = 7; // const/4 v0, 0x7
/* new-array v1, v0, [I */
/* fill-array-data v1, :array_0 */
/* .line 140 */
/* new-array v1, v0, [I */
/* fill-array-data v1, :array_1 */
/* .line 142 */
int v1 = 3; // const/4 v1, 0x3
int v2 = 2; // const/4 v2, 0x2
/* filled-new-array {v1, v2, v2}, [I */
/* filled-new-array {v1, v2, v2}, [I */
int v2 = 4; // const/4 v2, 0x4
int v5 = 5; // const/4 v5, 0x5
/* filled-new-array {v5, v2, v1}, [I */
int v2 = 6; // const/4 v2, 0x6
/* filled-new-array {v0, v2, v5}, [I */
/* const/16 v2, 0x9 */
/* const/16 v5, 0x8 */
/* filled-new-array {v2, v5, v0}, [I */
/* const/16 v0, 0xa */
/* filled-new-array {v0, v2, v5}, [I */
/* const/16 v0, 0xd */
/* const/16 v2, 0xc */
/* const/16 v5, 0xe */
/* filled-new-array {v5, v0, v2}, [I */
/* move-object v5, v1 */
/* filled-new-array/range {v3 ..v9}, [[I */
/* .line 144 */
/* const v0, 0x5a550 */
/* const v1, 0x46cd0 */
/* const v2, 0x6ddd0 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x72bf0 */
/* const v1, 0x5f370 */
/* const v2, 0x86470 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0xd9490 */
/* const v1, 0xc5c10 */
/* const v2, 0xecd10 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x11da50 */
/* const v1, 0x10a1d0 */
/* const v2, 0x1312d0 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x17f4d0 */
/* const v1, 0x16bc50 */
/* const v2, 0x192d50 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x1e0f50 */
/* const v1, 0x1cd6d0 */
/* const v2, 0x1f47d0 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x2429d0 */
/* const v1, 0x22f150 */
/* const v2, 0x256250 */
/* filled-new-array {v2, v0, v1}, [I */
/* filled-new-array/range {v3 ..v9}, [[I */
/* .line 148 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 149 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 150 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 151 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 152 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 153 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 155 */
final String v0 = "persist.sys.periodic.debug"; // const-string v0, "persist.sys.periodic.debug"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.PeriodicCleanerService.DEBUG = (v0!= 0);
/* .line 156 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x1f4 */
} // :cond_0
/* const/16 v0, 0x64 */
} // :goto_0
/* .line 157 */
/* new-instance v0, Ljava/lang/Integer; */
/* const/16 v1, 0x3e8 */
/* invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x2 */
/* 0x3 */
/* 0x3 */
/* 0x4 */
/* 0x6 */
/* 0x6 */
/* 0x6 */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x2 */
/* 0x2 */
/* 0x2 */
/* 0x4 */
/* 0x5 */
/* 0x6 */
/* 0x7 */
} // .end array-data
} // .end method
public com.android.server.am.PeriodicCleanerService ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 323 */
/* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
/* .line 165 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z */
/* .line 166 */
/* iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mHighDevice:Z */
/* .line 167 */
/* iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableFgTrim:Z */
/* .line 168 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z */
/* .line 169 */
final String v2 = "persist.sys.periodic.u.enable"; // const-string v2, "persist.sys.periodic.u.enable"
v2 = android.os.SystemProperties .getBoolean ( v2,v0 );
/* iput-boolean v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
/* .line 170 */
/* nop */
/* .line 171 */
final String v2 = "persist.sys.periodic.u.startprocess.enable"; // const-string v2, "persist.sys.periodic.u.startprocess.enable"
v2 = android.os.SystemProperties .getBoolean ( v2,v0 );
/* iput-boolean v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableStartProcess:Z */
/* .line 172 */
final String v2 = "persist.sys.periodic.mem_threshold"; // const-string v2, "persist.sys.periodic.mem_threshold"
/* const/16 v3, 0x258 */
v2 = android.os.SystemProperties .getInt ( v2,v3 );
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mAppMemThreshold:I */
/* .line 174 */
/* new-instance v2, Ljava/lang/Object; */
/* invoke-direct {v2}, Ljava/lang/Object;-><init>()V */
this.mLock = v2;
/* .line 175 */
/* new-instance v2, Landroid/os/HandlerThread; */
final String v3 = "PeriodicCleaner"; // const-string v3, "PeriodicCleaner"
/* invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mThread = v2;
/* .line 176 */
int v2 = 0; // const/4 v2, 0x0
this.mContext = v2;
/* .line 177 */
this.mBinderService = v2;
/* .line 178 */
this.mLocalService = v2;
/* .line 179 */
this.mHandler = v2;
/* .line 180 */
this.mPMS = v2;
/* .line 181 */
this.mAMS = v2;
/* .line 182 */
this.mATMS = v2;
/* .line 183 */
this.mWMS = v2;
/* .line 184 */
this.mAudioService = v2;
/* .line 185 */
this.mPKMS = v2;
/* .line 186 */
this.mUSM = v2;
/* .line 189 */
this.mAudioServiceClz = v2;
/* .line 190 */
this.mWindowManagerInternalClz = v2;
/* .line 191 */
this.mGetAllAudioFocusMethod = v2;
/* .line 192 */
this.mGetVisibleWindowOwnerMethod = v2;
/* .line 194 */
this.mAndroidOsDebugClz = v2;
/* .line 195 */
this.mClassProcessState = v2;
/* .line 196 */
this.mFieldProcessState = v2;
/* .line 197 */
this.mDebugClz_Field_MEMINFO_CACHED = v2;
/* .line 198 */
this.mDebugClz_Field_MEMINFO_SWAPCACHED = v2;
/* .line 199 */
this.mDebugClz_Field_MEMINFO_BUFFERS = v2;
/* .line 200 */
this.mDebugClz_Field_MEMINFO_SHMEM = v2;
/* .line 201 */
this.mDebugClz_Field_MEMINFO_UNEVICTABLE = v2;
/* .line 202 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_CACHED:I */
/* .line 203 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SWAPCACHED:I */
/* .line 204 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_BUFFERS:I */
/* .line 205 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SHMEM:I */
/* .line 206 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_UNEVICTABLE:I */
/* .line 208 */
this.mIApplicationThreadClz = v2;
/* .line 209 */
this.mScheduleAggressiveTrimMethod = v2;
/* .line 211 */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
this.mLruPackages = v3;
/* .line 213 */
/* new-array v3, v3, [Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
this.mCleanHistory = v3;
/* .line 214 */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
this.mLastVisibleUids = v3;
/* .line 216 */
this.mLastNonSystemFgPkg = v2;
/* .line 217 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgUid:I */
/* .line 218 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
/* .line 219 */
int v2 = 3; // const/4 v2, 0x3
/* new-array v3, v2, [I */
this.mPressureTimeThreshold = v3;
/* .line 220 */
/* new-array v3, v2, [I */
this.mPressureAgingThreshold = v3;
/* .line 221 */
/* new-array v3, v2, [I */
this.mPressureCacheThreshold = v3;
/* .line 222 */
/* const-wide/16 v3, 0x0 */
/* iput-wide v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastCleanByPressure:J */
/* .line 223 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistoryIndex:I */
/* .line 224 */
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mScreenState:I */
/* .line 225 */
/* iput v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mFgTrimTheshold:I */
/* .line 226 */
/* iput-wide v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastUpdateTime:J */
/* .line 227 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mOverTimeAppNum:I */
/* .line 228 */
/* iput v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mGamePlayAppNum:I */
/* .line 1731 */
/* new-instance v0, Lcom/android/server/am/PeriodicCleanerService$6; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/PeriodicCleanerService$6;-><init>(Lcom/android/server/am/PeriodicCleanerService;)V */
this.mReceiver = v0;
/* .line 324 */
this.mContext = p1;
/* .line 325 */
v0 = this.mThread;
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 326 */
/* new-instance v0, Lcom/android/server/am/PeriodicCleanerService$MyHandler; */
v1 = this.mThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;-><init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 327 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->init()V */
/* .line 328 */
return;
} // .end method
private void addCleanHistory ( com.android.server.am.PeriodicCleanerService$CleanInfo p0 ) {
/* .locals 3 */
/* .param p1, "cInfo" # Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
/* .line 612 */
v0 = (( com.android.server.am.PeriodicCleanerService$CleanInfo ) p1 ).isValid ( ); // invoke-virtual {p1}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->isValid()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 613 */
v0 = this.mCleanHistory;
/* monitor-enter v0 */
/* .line 614 */
try { // :try_start_0
v1 = this.mCleanHistory;
/* iget v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistoryIndex:I */
/* aput-object p1, v1, v2 */
/* .line 615 */
/* add-int/lit8 v2, v2, 0x1 */
/* rem-int/2addr v2, v1 */
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistoryIndex:I */
/* .line 616 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 618 */
} // :cond_0
} // :goto_0
return;
} // .end method
private Boolean canCleanPackage ( com.android.server.am.ProcessRecord p0, Integer p1, java.util.HashMap p2, java.util.List p3, java.util.List p4, java.util.List p5, Boolean p6 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pressure" # I */
/* .param p7, "isCameraForeground" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* "I", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;Z)Z" */
/* } */
} // .end annotation
/* .line 1462 */
/* .local p3, "dynWhitelist":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
/* .local p4, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p5, "activeAudioUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p6, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v0 = this.info;
v0 = this.packageName;
/* .line 1463 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = (( com.android.server.am.PeriodicCleanerService ) p0 ).isSystemRelated ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/PeriodicCleanerService;->isSystemRelated(Lcom/android/server/am/ProcessRecord;)Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1464 */
/* .line 1466 */
} // :cond_0
v1 = /* invoke-direct {p0, v0, p3}, Lcom/android/server/am/PeriodicCleanerService;->isDynWhitelist(Ljava/lang/String;Ljava/util/HashMap;)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1467 */
/* .line 1469 */
} // :cond_1
v1 = /* invoke-direct {p0, p1, p4, p5, p6}, Lcom/android/server/am/PeriodicCleanerService;->isActive(Lcom/android/server/am/ProcessRecord;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1470 */
/* .line 1472 */
} // :cond_2
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/PeriodicCleanerService;->isImportant(Lcom/android/server/am/ProcessRecord;I)Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1473 */
/* .line 1475 */
} // :cond_3
if ( p7 != null) { // if-eqz p7, :cond_4
v1 = /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/PeriodicCleanerService;->isSkipClean(Lcom/android/server/am/ProcessRecord;I)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1476 */
/* .line 1478 */
} // :cond_4
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean checkEnableFgTrim ( ) {
/* .locals 5 */
/* .line 491 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
final String v2 = "android.app.IApplicationThread"; // const-string v2, "android.app.IApplicationThread"
java.lang.Class .forName ( v2 );
this.mIApplicationThreadClz = v2;
/* .line 492 */
final String v3 = "scheduleAggressiveMemoryTrim"; // const-string v3, "scheduleAggressiveMemoryTrim"
/* new-array v4, v1, [Ljava/lang/Class; */
/* .line 493 */
(( java.lang.Class ) v2 ).getDeclaredMethod ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mScheduleAggressiveTrimMethod = v2;
/* :try_end_0 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 494 */
int v0 = 1; // const/4 v0, 0x1
/* .line 498 */
/* :catch_0 */
/* move-exception v2 */
/* .line 499 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "check failed: "; // const-string v4, "check failed: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 500 */
/* .line 495 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v2 */
/* .line 496 */
/* .local v2, "nme":Ljava/lang/NoSuchMethodException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "interface missing: "; // const-string v4, "interface missing: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 497 */
} // .end method
private void checkPressureAndClean ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1100 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getMemPressureLevel()I */
/* .line 1101 */
/* .local v0, "pressure":I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_0 */
/* .line 1102 */
/* invoke-direct {p0, v0, p1}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByPressure(ILjava/lang/String;)V */
/* .line 1103 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastCleanByPressure:J */
/* .line 1105 */
} // :cond_0
return;
} // .end method
private static void checkTime ( Long p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p0, "startTime" # J */
/* .param p2, "where" # Ljava/lang/String; */
/* .line 1594 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1595 */
/* .local v0, "now":J */
/* sub-long v2, v0, p0 */
/* sget-boolean v4, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* const/16 v4, 0x32 */
} // :cond_0
/* const/16 v4, 0xc8 */
} // :goto_0
/* int-to-long v4, v4 */
/* cmp-long v2, v2, v4 */
/* if-lez v2, :cond_1 */
/* .line 1596 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Slow operation: "; // const-string v3, "Slow operation: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sub-long v3, v0, p0 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms so far, now at "; // const-string v3, "ms so far, now at "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "PeriodicCleaner"; // const-string v3, "PeriodicCleaner"
android.util.Slog .w ( v3,v2 );
/* .line 1598 */
} // :cond_1
return;
} // .end method
private void cleanPackageByGamePlay ( Integer p0, java.lang.String p1 ) {
/* .locals 15 */
/* .param p1, "activeNum" # I */
/* .param p2, "startProcessName" # Ljava/lang/String; */
/* .line 1053 */
/* move-object v7, p0 */
/* move/from16 v8, p1 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "game:"; // const-string v1, "game:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v9, p2 */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1054 */
/* .local v10, "reason":Ljava/lang/String; */
/* const/16 v0, 0x64 */
int v1 = -1; // const/4 v1, -0x1
/* invoke-direct {p0, v8, v0, v1, v10}, Lcom/android/server/am/PeriodicCleanerService;->doClean(IIILjava/lang/String;)V */
/* .line 1057 */
/* invoke-direct/range {p0 ..p2}, Lcom/android/server/am/PeriodicCleanerService;->findOtherGamePackageLocked(ILjava/lang/String;)Ljava/util/List; */
/* .line 1058 */
v0 = /* .local v11, "gamePackagesUid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* if-nez v0, :cond_0 */
/* .line 1059 */
return;
/* .line 1060 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "PeriodicCleaner("; // const-string v2, "PeriodicCleaner("
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "|" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "|muti_game)" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1061 */
/* .local v12, "longReason":Ljava/lang/String; */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* move-object v13, v0 */
/* .line 1062 */
/* .local v13, "killedArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_6
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 1063 */
/* .local v1, "uid":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1064 */
/* .local v2, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v3 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v3 ).getProcessRecordByUid ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;
/* .line 1065 */
/* if-nez v2, :cond_1 */
/* .line 1066 */
} // :cond_1
} // :cond_2
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_5
/* check-cast v4, Lcom/android/server/am/ProcessRecord; */
/* .line 1067 */
/* .local v4, "app":Lcom/android/server/am/ProcessRecord; */
v5 = (( com.android.server.am.ProcessRecord ) v4 ).isKilledByAm ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v5, :cond_2 */
(( com.android.server.am.ProcessRecord ) v4 ).getThread ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v5 != null) { // if-eqz v5, :cond_2
/* iget-boolean v5, v4, Lcom/android/server/am/ProcessRecord;->isolated:Z */
/* if-nez v5, :cond_2 */
/* .line 1068 */
v5 = (( com.android.server.am.PeriodicCleanerService ) p0 ).isSystemRelated ( v4 ); // invoke-virtual {p0, v4}, Lcom/android/server/am/PeriodicCleanerService;->isSystemRelated(Lcom/android/server/am/ProcessRecord;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 1069 */
/* .line 1071 */
} // :cond_3
v5 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v5 ).getProcessKiller ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;
int v6 = 0; // const/4 v6, 0x0
(( com.android.server.am.ProcessKiller ) v5 ).killApplication ( v4, v12, v6 ); // invoke-virtual {v5, v4, v12, v6}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z
/* .line 1072 */
/* iget v5, v4, Lcom/android/server/am/ProcessRecord;->userId:I */
(( android.util.SparseArray ) v13 ).get ( v5 ); // invoke-virtual {v13, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v5, Ljava/util/ArrayList; */
/* .line 1073 */
/* .local v5, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* if-nez v5, :cond_4 */
/* .line 1074 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
/* move-object v5, v6 */
/* .line 1075 */
/* iget v6, v4, Lcom/android/server/am/ProcessRecord;->userId:I */
(( android.util.SparseArray ) v13 ).put ( v6, v5 ); // invoke-virtual {v13, v6, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1077 */
} // :cond_4
v6 = this.processName;
(( java.util.ArrayList ) v5 ).add ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1078 */
} // .end local v4 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v5 # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* .line 1079 */
} // .end local v1 # "uid":I
} // .end local v2 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_5
/* .line 1080 */
} // :cond_6
/* new-instance v14, Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
int v4 = -1; // const/4 v4, -0x1
int v5 = -1; // const/4 v5, -0x1
final String v6 = "muti_game"; // const-string v6, "muti_game"
/* move-object v0, v14 */
/* move-object v1, p0 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V */
/* .line 1081 */
/* .local v0, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_2
v2 = (( android.util.SparseArray ) v13 ).size ( ); // invoke-virtual {v13}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_7 */
/* .line 1082 */
v2 = (( android.util.SparseArray ) v13 ).keyAt ( v1 ); // invoke-virtual {v13, v1}, Landroid/util/SparseArray;->keyAt(I)I
(( android.util.SparseArray ) v13 ).valueAt ( v1 ); // invoke-virtual {v13, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/util/List; */
(( com.android.server.am.PeriodicCleanerService$CleanInfo ) v0 ).addCleanList ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->addCleanList(ILjava/util/List;)V
/* .line 1081 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1084 */
} // .end local v1 # "i":I
} // :cond_7
/* invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V */
/* .line 1085 */
return;
} // .end method
private void cleanPackageByPeriodic ( ) {
/* .locals 4 */
/* .line 1048 */
/* iget v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
int v1 = -1; // const/4 v1, -0x1
final String v2 = "cycle"; // const-string v2, "cycle"
/* const/16 v3, 0x64 */
/* invoke-direct {p0, v0, v3, v1, v2}, Lcom/android/server/am/PeriodicCleanerService;->doClean(IIILjava/lang/String;)V */
/* .line 1049 */
return;
} // .end method
private void cleanPackageByPressure ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pressure" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 1088 */
v0 = this.mPressureAgingThreshold;
/* aget v0, v0, p1 */
/* const/16 v1, 0x64 */
/* invoke-direct {p0, v0, v1, p1, p2}, Lcom/android/server/am/PeriodicCleanerService;->doClean(IIILjava/lang/String;)V */
/* .line 1089 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->reCheckPressureAndClean()V */
/* .line 1090 */
return;
} // .end method
private void cleanPackageByTime ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pressure" # I */
/* .line 1044 */
/* iget v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
/* const/16 v1, 0x64 */
/* const-string/jumbo v2, "time" */
/* invoke-direct {p0, v0, v1, p1, v2}, Lcom/android/server/am/PeriodicCleanerService;->doClean(IIILjava/lang/String;)V */
/* .line 1045 */
return;
} // .end method
private Boolean dependencyCheck ( ) {
/* .locals 5 */
/* .line 448 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
final String v2 = "com.android.server.audio.AudioService"; // const-string v2, "com.android.server.audio.AudioService"
java.lang.Class .forName ( v2 );
this.mAudioServiceClz = v2;
/* .line 449 */
final String v3 = "getAllAudioFocus"; // const-string v3, "getAllAudioFocus"
/* new-array v4, v1, [Ljava/lang/Class; */
(( java.lang.Class ) v2 ).getDeclaredMethod ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mGetAllAudioFocusMethod = v2;
/* .line 452 */
final String v2 = "com.android.server.wm.WindowManagerInternal"; // const-string v2, "com.android.server.wm.WindowManagerInternal"
/* .line 453 */
java.lang.Class .forName ( v2 );
this.mWindowManagerInternalClz = v2;
/* .line 454 */
final String v3 = "getVisibleWindowOwner"; // const-string v3, "getVisibleWindowOwner"
/* new-array v4, v1, [Ljava/lang/Class; */
/* .line 455 */
(( java.lang.Class ) v2 ).getDeclaredMethod ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mGetVisibleWindowOwnerMethod = v2;
/* .line 458 */
final String v2 = "android.os.Debug"; // const-string v2, "android.os.Debug"
java.lang.Class .forName ( v2 );
this.mAndroidOsDebugClz = v2;
/* .line 459 */
final String v3 = "MEMINFO_CACHED"; // const-string v3, "MEMINFO_CACHED"
(( java.lang.Class ) v2 ).getField ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
this.mDebugClz_Field_MEMINFO_CACHED = v2;
/* .line 460 */
v2 = this.mAndroidOsDebugClz;
final String v3 = "MEMINFO_SWAPCACHED"; // const-string v3, "MEMINFO_SWAPCACHED"
(( java.lang.Class ) v2 ).getField ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
this.mDebugClz_Field_MEMINFO_SWAPCACHED = v2;
/* .line 461 */
v2 = this.mAndroidOsDebugClz;
final String v3 = "MEMINFO_BUFFERS"; // const-string v3, "MEMINFO_BUFFERS"
(( java.lang.Class ) v2 ).getField ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
this.mDebugClz_Field_MEMINFO_BUFFERS = v2;
/* .line 462 */
v2 = this.mAndroidOsDebugClz;
final String v3 = "MEMINFO_SHMEM"; // const-string v3, "MEMINFO_SHMEM"
(( java.lang.Class ) v2 ).getField ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
this.mDebugClz_Field_MEMINFO_SHMEM = v2;
/* .line 463 */
v2 = this.mAndroidOsDebugClz;
final String v3 = "MEMINFO_UNEVICTABLE"; // const-string v3, "MEMINFO_UNEVICTABLE"
(( java.lang.Class ) v2 ).getField ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
this.mDebugClz_Field_MEMINFO_UNEVICTABLE = v2;
/* .line 465 */
v2 = this.mDebugClz_Field_MEMINFO_CACHED;
int v3 = 0; // const/4 v3, 0x0
v2 = (( java.lang.reflect.Field ) v2 ).getInt ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_CACHED:I */
/* .line 466 */
v2 = this.mDebugClz_Field_MEMINFO_SWAPCACHED;
v2 = (( java.lang.reflect.Field ) v2 ).getInt ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SWAPCACHED:I */
/* .line 467 */
v2 = this.mDebugClz_Field_MEMINFO_BUFFERS;
v2 = (( java.lang.reflect.Field ) v2 ).getInt ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_BUFFERS:I */
/* .line 468 */
v2 = this.mDebugClz_Field_MEMINFO_SHMEM;
v2 = (( java.lang.reflect.Field ) v2 ).getInt ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SHMEM:I */
/* .line 469 */
v2 = this.mDebugClz_Field_MEMINFO_UNEVICTABLE;
v2 = (( java.lang.reflect.Field ) v2 ).getInt ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_UNEVICTABLE:I */
/* .line 472 */
final String v2 = "com.android.internal.app.procstats.ProcessState"; // const-string v2, "com.android.internal.app.procstats.ProcessState"
java.lang.Class .forName ( v2 );
this.mClassProcessState = v2;
/* .line 473 */
final String v3 = "mPssTable"; // const-string v3, "mPssTable"
(( java.lang.Class ) v2 ).getDeclaredField ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
this.mFieldProcessState = v2;
/* .line 474 */
int v3 = 1; // const/4 v3, 0x1
(( java.lang.reflect.Field ) v2 ).setAccessible ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* :try_end_0 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/NoSuchFieldException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 476 */
/* .line 483 */
/* :catch_0 */
/* move-exception v2 */
/* .line 484 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "dependency check failed: "; // const-string v4, "dependency check failed: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 485 */
/* .line 480 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v2 */
/* .line 481 */
/* .local v2, "nfe":Ljava/lang/NoSuchFieldException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "dependent field missing: "; // const-string v4, "dependent field missing: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 482 */
/* .line 477 */
} // .end local v2 # "nfe":Ljava/lang/NoSuchFieldException;
/* :catch_2 */
/* move-exception v2 */
/* .line 478 */
/* .local v2, "nme":Ljava/lang/NoSuchMethodException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "dependent interface missing: "; // const-string v4, "dependent interface missing: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 479 */
} // .end method
private void doClean ( Integer p0, Integer p1, Integer p2, java.lang.String p3 ) {
/* .locals 22 */
/* .param p1, "thresHold" # I */
/* .param p2, "killLevel" # I */
/* .param p3, "pressure" # I */
/* .param p4, "reason" # Ljava/lang/String; */
/* .line 1187 */
/* move-object/from16 v7, p0 */
/* move/from16 v8, p3 */
/* move-object/from16 v0, p4 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v9 */
/* .line 1188 */
/* .local v9, "startTime":J */
int v1 = 0; // const/4 v1, 0x0
/* .line 1189 */
/* .local v1, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
/* const-string/jumbo v2, "time" */
v2 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1190 */
/* invoke-direct {v7, v8}, Lcom/android/server/am/PeriodicCleanerService;->findOverTimePackageLocked(I)Landroid/util/SparseArray; */
/* move-object v11, v1 */
/* .line 1192 */
} // :cond_0
/* invoke-direct/range {p0 ..p1}, Lcom/android/server/am/PeriodicCleanerService;->findAgingPackageLocked(I)Landroid/util/SparseArray; */
/* move-object v11, v1 */
/* .line 1193 */
} // .end local v1 # "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
/* .local v11, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
/* .line 1194 */
/* .local v1, "processIndex":I */
final String v2 = "game:"; // const-string v2, "game:"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v1 = 4; // const/4 v1, 0x4
/* .line 1195 */
} // :cond_1
final String v2 = "pressure:startprocess:"; // const-string v2, "pressure:startprocess:"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* const/16 v1, 0x15 */
} // :cond_2
/* move v12, v1 */
/* .line 1196 */
} // .end local v1 # "processIndex":I
/* .local v12, "processIndex":I */
int v1 = -1; // const/4 v1, -0x1
/* if-le v12, v1, :cond_3 */
/* .line 1197 */
/* add-int/lit8 v1, v12, 0x1 */
(( java.lang.String ) v0 ).substring ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 1198 */
/* .local v1, "currentPackage":Ljava/lang/String; */
/* invoke-direct {v7, v11, v1}, Lcom/android/server/am/PeriodicCleanerService;->excludeCurrentProcess(Landroid/util/SparseArray;Ljava/lang/String;)V */
/* .line 1199 */
int v2 = 0; // const/4 v2, 0x0
(( java.lang.String ) v0 ).substring ( v2, v12 ); // invoke-virtual {v0, v2, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* move-object v13, v0 */
} // .end local p4 # "reason":Ljava/lang/String;
/* .local v0, "reason":Ljava/lang/String; */
/* .line 1196 */
} // .end local v0 # "reason":Ljava/lang/String;
} // .end local v1 # "currentPackage":Ljava/lang/String;
/* .restart local p4 # "reason":Ljava/lang/String; */
} // :cond_3
/* move-object v13, v0 */
/* .line 1201 */
} // .end local p4 # "reason":Ljava/lang/String;
/* .local v13, "reason":Ljava/lang/String; */
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "PeriodicCleaner("; // const-string v1, "PeriodicCleaner("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v14, p1 */
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "|" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1202 */
/* .local v15, "longReason":Ljava/lang/String; */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/PeriodicCleanerService;->getVisibleWindowOwner()Ljava/util/List; */
/* .line 1203 */
/* .local v6, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/PeriodicCleanerService;->getAudioActiveUids()Ljava/util/List; */
/* .line 1204 */
/* .local v5, "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/PeriodicCleanerService;->getLocationActiveUids()Ljava/util/List; */
/* .line 1205 */
/* .local v4, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
final String v0 = "finish get active and visible uids"; // const-string v0, "finish get active and visible uids"
com.android.server.am.PeriodicCleanerService .checkTime ( v9,v10,v0 );
/* .line 1206 */
/* sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1207 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AgingPacakges: "; // const-string v1, "AgingPacakges: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", LocationActiveUids: "; // const-string v1, ", LocationActiveUids: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", VisibleUids: "; // const-string v1, ", VisibleUids: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", AudioActiveUids: "; // const-string v1, ", AudioActiveUids: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PeriodicCleaner"; // const-string v1, "PeriodicCleaner"
android.util.Slog .d ( v1,v0 );
/* .line 1212 */
} // :cond_4
/* new-instance v16, Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* move-object/from16 v0, v16 */
/* move-object/from16 v1, p0 */
/* move-object/from16 v17, v4 */
} // .end local v4 # "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .local v17, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* move/from16 v4, p3 */
/* move-object/from16 v18, v5 */
} // .end local v5 # "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .local v18, "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* move/from16 v5, p1 */
/* move-object/from16 v19, v6 */
} // .end local v6 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .local v19, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* move-object v6, v13 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V */
/* move-object/from16 v6, v16 */
/* .line 1213 */
/* .local v6, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
int v0 = 0; // const/4 v0, 0x0
/* move v5, v0 */
/* .local v5, "i":I */
} // :goto_2
v0 = (( android.util.SparseArray ) v11 ).size ( ); // invoke-virtual {v11}, Landroid/util/SparseArray;->size()I
/* if-ge v5, v0, :cond_6 */
/* .line 1214 */
v4 = (( android.util.SparseArray ) v11 ).keyAt ( v5 ); // invoke-virtual {v11, v5}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 1215 */
/* .local v4, "userId":I */
/* nop */
/* .line 1216 */
(( android.util.SparseArray ) v11 ).valueAt ( v5 ); // invoke-virtual {v11, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* move-object v3, v0 */
/* check-cast v3, Ljava/util/ArrayList; */
/* .line 1215 */
/* move-object/from16 v0, p0 */
/* move v1, v4 */
/* move/from16 v2, p3 */
/* move v8, v4 */
} // .end local v4 # "userId":I
/* .local v8, "userId":I */
/* move-object/from16 v4, v19 */
/* move/from16 v16, v5 */
} // .end local v5 # "i":I
/* .local v16, "i":I */
/* move-object/from16 v5, v18 */
/* move-wide/from16 v20, v9 */
/* move-object v9, v6 */
} // .end local v6 # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
/* .local v9, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
/* .local v20, "startTime":J */
/* move-object/from16 v6, v17 */
/* invoke-direct/range {v0 ..v6}, Lcom/android/server/am/PeriodicCleanerService;->filterOutKillablePackages(IILjava/util/ArrayList;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/ArrayList; */
/* .line 1217 */
/* .local v0, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v1 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-lez v1, :cond_5 */
/* .line 1218 */
/* move/from16 v1, p2 */
/* invoke-direct {v7, v8, v0, v1, v15}, Lcom/android/server/am/PeriodicCleanerService;->killTargets(ILjava/util/ArrayList;ILjava/lang/String;)V */
/* .line 1219 */
(( com.android.server.am.PeriodicCleanerService$CleanInfo ) v9 ).addCleanList ( v8, v0 ); // invoke-virtual {v9, v8, v0}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->addCleanList(ILjava/util/List;)V
/* .line 1217 */
} // :cond_5
/* move/from16 v1, p2 */
/* .line 1213 */
} // .end local v0 # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // .end local v8 # "userId":I
} // :goto_3
/* add-int/lit8 v5, v16, 0x1 */
/* move/from16 v8, p3 */
/* move-object v6, v9 */
/* move-wide/from16 v9, v20 */
} // .end local v16 # "i":I
/* .restart local v5 # "i":I */
} // .end local v20 # "startTime":J
/* .restart local v6 # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
/* .local v9, "startTime":J */
} // :cond_6
/* move-wide/from16 v20, v9 */
/* move-object v9, v6 */
/* .line 1222 */
} // .end local v5 # "i":I
} // .end local v6 # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
/* .local v9, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
/* .restart local v20 # "startTime":J */
/* invoke-direct {v7, v9}, Lcom/android/server/am/PeriodicCleanerService;->addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V */
/* .line 1223 */
return;
} // .end method
private void doFgTrim ( Integer p0 ) {
/* .locals 13 */
/* .param p1, "threshold" # I */
/* .line 1108 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1109 */
/* .local v0, "startTime":J */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getVisibleWindowOwner()Ljava/util/List; */
/* .line 1110 */
/* .local v2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
final String v3 = "finish doFgTrim#getVisibleWindowOwner"; // const-string v3, "finish doFgTrim#getVisibleWindowOwner"
com.android.server.am.PeriodicCleanerService .checkTime ( v0,v1,v3 );
/* .line 1112 */
/* new-instance v3, Ljava/util/HashMap; */
/* invoke-direct {v3}, Ljava/util/HashMap;-><init>()V */
/* .line 1113 */
/* .local v3, "trimTargets":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;>;" */
v4 = this.mLock;
/* monitor-enter v4 */
/* .line 1114 */
try { // :try_start_0
v5 = this.mLruPackages;
v5 = (( java.util.ArrayList ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
/* .line 1115 */
/* .local v5, "N":I */
int v6 = 0; // const/4 v6, 0x0
/* .line 1116 */
/* .local v6, "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* add-int/lit8 v7, v5, -0x1 */
/* sub-int/2addr v7, p1 */
/* .local v7, "i":I */
} // :goto_0
/* if-ltz v7, :cond_1 */
/* .line 1117 */
v8 = this.mLruPackages;
(( java.util.ArrayList ) v8 ).get ( v7 ); // invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v8, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* move-object v6, v8 */
/* .line 1118 */
v8 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmFgTrimDone ( v6 );
/* if-nez v8, :cond_0 */
/* .line 1119 */
v8 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUid ( v6 );
java.lang.Integer .valueOf ( v8 );
(( java.util.HashMap ) v3 ).put ( v8, v6 ); // invoke-virtual {v3, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1116 */
} // :cond_0
/* add-int/lit8 v7, v7, -0x1 */
/* .line 1122 */
} // .end local v5 # "N":I
} // .end local v6 # "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
} // .end local v7 # "i":I
} // :cond_1
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1123 */
v4 = (( java.util.HashMap ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->size()I
/* if-gtz v4, :cond_2 */
return;
/* .line 1124 */
} // :cond_2
final String v4 = "finish doFgTrim#getTrimTargets"; // const-string v4, "finish doFgTrim#getTrimTargets"
com.android.server.am.PeriodicCleanerService .checkTime ( v0,v1,v4 );
/* .line 1126 */
v5 = this.mAMS;
/* monitor-enter v5 */
/* .line 1127 */
try { // :try_start_1
v4 = this.mAMS;
v4 = this.mProcessList;
v4 = (( com.android.server.am.ProcessList ) v4 ).getLruSizeLOSP ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I
/* .line 1128 */
/* .local v4, "N":I */
int v6 = 0; // const/4 v6, 0x0
/* .line 1129 */
/* .local v6, "target":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* add-int/lit8 v7, v4, -0x1 */
/* .restart local v7 # "i":I */
} // :goto_1
/* if-ltz v7, :cond_8 */
/* .line 1130 */
v8 = this.mAMS;
v8 = this.mProcessList;
(( com.android.server.am.ProcessList ) v8 ).getLruProcessesLOSP ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
(( java.util.ArrayList ) v8 ).get ( v7 ); // invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v8, Lcom/android/server/am/ProcessRecord; */
/* .line 1131 */
/* .local v8, "app":Lcom/android/server/am/ProcessRecord; */
v9 = (( com.android.server.am.ProcessRecord ) v8 ).isKilledByAm ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v9, :cond_7 */
(( com.android.server.am.ProcessRecord ) v8 ).getThread ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
/* if-nez v9, :cond_3 */
/* .line 1132 */
/* goto/16 :goto_3 */
/* .line 1134 */
} // :cond_3
/* iget-boolean v9, v8, Lcom/android/server/am/ProcessRecord;->isolated:Z */
/* if-nez v9, :cond_6 */
/* iget v9, v8, Lcom/android/server/am/ProcessRecord;->uid:I */
v9 = java.lang.Integer .valueOf ( v9 );
if ( v9 != null) { // if-eqz v9, :cond_4
/* .line 1140 */
} // :cond_4
/* iget v9, v8, Lcom/android/server/am/ProcessRecord;->uid:I */
java.lang.Integer .valueOf ( v9 );
(( java.util.HashMap ) v3 ).get ( v9 ); // invoke-virtual {v3, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v9, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object v6, v9 */
/* .line 1141 */
/* if-nez v6, :cond_5 */
/* .line 1142 */
/* .line 1146 */
} // :cond_5
try { // :try_start_2
v9 = this.mScheduleAggressiveTrimMethod;
(( com.android.server.am.ProcessRecord ) v8 ).getThread ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
int v11 = 0; // const/4 v11, 0x0
/* new-array v11, v11, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v9 ).invoke ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1147 */
int v9 = 1; // const/4 v9, 0x1
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fputmFgTrimDone ( v6,v9 );
/* .line 1148 */
final String v9 = "PeriodicCleaner"; // const-string v9, "PeriodicCleaner"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "send aggressive trim to " */
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v9,v10 );
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 1151 */
/* .line 1149 */
/* :catch_0 */
/* move-exception v9 */
/* .line 1150 */
/* .local v9, "e":Ljava/lang/Exception; */
try { // :try_start_3
final String v10 = "PeriodicCleaner"; // const-string v10, "PeriodicCleaner"
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "doFgTrim#scheduleAggressiveMemoryTrim: "; // const-string v12, "doFgTrim#scheduleAggressiveMemoryTrim: "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v10,v11 );
/* .line 1135 */
} // .end local v9 # "e":Ljava/lang/Exception;
} // :cond_6
} // :goto_2
/* sget-boolean v9, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v9 != null) { // if-eqz v9, :cond_7
/* .line 1136 */
final String v9 = "PeriodicCleaner"; // const-string v9, "PeriodicCleaner"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v8 ); // invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v11 = " is isolated or uid has visible window."; // const-string v11, " is isolated or uid has visible window."
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v9,v10 );
/* .line 1129 */
} // .end local v8 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_7
} // :goto_3
/* add-int/lit8 v7, v7, -0x1 */
/* goto/16 :goto_1 */
/* .line 1153 */
} // .end local v7 # "i":I
} // :cond_8
final String v7 = "finish doFgTrim"; // const-string v7, "finish doFgTrim"
com.android.server.am.PeriodicCleanerService .checkTime ( v0,v1,v7 );
/* .line 1154 */
} // .end local v4 # "N":I
} // .end local v6 # "target":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
/* monitor-exit v5 */
/* .line 1155 */
return;
/* .line 1154 */
/* :catchall_0 */
/* move-exception v4 */
/* monitor-exit v5 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v4 */
/* .line 1122 */
/* :catchall_1 */
/* move-exception v5 */
try { // :try_start_4
/* monitor-exit v4 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v5 */
} // .end method
private void dumpCleanHistory ( java.io.PrintWriter p0 ) {
/* .locals 8 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1640 */
final String v0 = "\n---- CleanHistory ----"; // const-string v0, "\n---- CleanHistory ----"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1641 */
v0 = this.mCleanHistory;
/* monitor-enter v0 */
/* .line 1642 */
try { // :try_start_0
/* iget v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mCleanHistoryIndex:I */
/* add-int/lit8 v1, v1, -0x1 */
/* .line 1643 */
/* .local v1, "index":I */
/* new-instance v2, Ljava/text/SimpleDateFormat; */
final String v3 = "HH:mm:ss.SSS"; // const-string v3, "HH:mm:ss.SSS"
/* invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 1644 */
/* .local v2, "formater":Ljava/text/SimpleDateFormat; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v4, :cond_1 */
/* .line 1645 */
/* add-int v5, v1, v4 */
/* rem-int/2addr v5, v4 */
/* move v1, v5 */
/* .line 1646 */
v4 = this.mCleanHistory;
/* aget-object v4, v4, v1 */
/* if-nez v4, :cond_0 */
/* goto/16 :goto_1 */
/* .line 1647 */
} // :cond_0
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "#"; // const-string v5, "#"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1648 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v5, Ljava/util/Date; */
v6 = this.mCleanHistory;
/* aget-object v6, v6, v1 */
com.android.server.am.PeriodicCleanerService$CleanInfo .-$$Nest$fgetmCleanTime ( v6 );
/* move-result-wide v6 */
/* invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v2 ).format ( v5 ); // invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1649 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mCleanHistory;
/* aget-object v5, v5, v1 */
v5 = com.android.server.am.PeriodicCleanerService$CleanInfo .-$$Nest$fgetmPressure ( v5 );
/* invoke-direct {p0, v5}, Lcom/android/server/am/PeriodicCleanerService;->pressureToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1650 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mCleanHistory;
/* aget-object v5, v5, v1 */
v5 = com.android.server.am.PeriodicCleanerService$CleanInfo .-$$Nest$fgetmAgingThresHold ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1651 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mCleanHistory;
/* aget-object v5, v5, v1 */
com.android.server.am.PeriodicCleanerService$CleanInfo .-$$Nest$fgetmReason ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1652 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mCleanHistory;
/* aget-object v5, v5, v1 */
com.android.server.am.PeriodicCleanerService$CleanInfo .-$$Nest$fgetmCleanList ( v5 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1653 */
/* add-int/lit8 v1, v1, -0x1 */
/* .line 1644 */
/* add-int/lit8 v3, v3, 0x1 */
/* goto/16 :goto_0 */
/* .line 1655 */
} // .end local v1 # "index":I
} // .end local v2 # "formater":Ljava/text/SimpleDateFormat;
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1656 */
final String v0 = "---- End of CleanHistory ----"; // const-string v0, "---- End of CleanHistory ----"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1657 */
return;
/* .line 1655 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void dumpFgLru ( java.io.PrintWriter p0 ) {
/* .locals 6 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 1601 */
final String v0 = "\n---- Foreground-LRU ----"; // const-string v0, "\n---- Foreground-LRU ----"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1602 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getMemPressureLevel()I */
/* .line 1603 */
/* .local v0, "curPressure":I */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 1604 */
try { // :try_start_0
final String v2 = "Settings:"; // const-string v2, "Settings:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1606 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " Enable="; // const-string v3, " Enable="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1607 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " Ready="; // const-string v3, " Ready="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1608 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " FgTrim="; // const-string v3, " FgTrim="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableFgTrim:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1609 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " Debug="; // const-string v3, " Debug="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v3, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1610 */
/* iget-boolean v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableFgTrim:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1611 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " FgTrimThreshold="; // const-string v3, " FgTrimThreshold="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mFgTrimTheshold:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1613 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " mOverTimeAppNum="; // const-string v3, " mOverTimeAppNum="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mOverTimeAppNum:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1614 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " mGamePlayAppNum="; // const-string v3, " mGamePlayAppNum="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mGamePlayAppNum:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1615 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " LruLengthLimit="; // const-string v3, " LruLengthLimit="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1617 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " AvailableLow="; // const-string v3, " AvailableLow="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mPressureCacheThreshold;
int v4 = 0; // const/4 v4, 0x0
/* aget v3, v3, v4 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "Kb"; // const-string v3, "Kb"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1618 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " AvailableMin="; // const-string v3, " AvailableMin="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mPressureCacheThreshold;
int v4 = 1; // const/4 v4, 0x1
/* aget v3, v3, v4 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "Kb"; // const-string v3, "Kb"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 1619 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " AvailableCritical="; // const-string v3, " AvailableCritical="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mPressureCacheThreshold;
int v5 = 2; // const/4 v5, 0x2
/* aget v3, v3, v5 */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "Kb"; // const-string v3, "Kb"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1621 */
final String v2 = "Package Usage LRU:"; // const-string v2, "Package Usage LRU:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1622 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " CurLength:"; // const-string v3, " CurLength:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLruPackages;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " CurrentPressure:"; // const-string v3, " CurrentPressure:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->pressureToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1623 */
v2 = this.mLruPackages;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* sub-int/2addr v2, v4 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_1 */
/* .line 1624 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mLruPackages;
(( java.util.ArrayList ) v4 ).get ( v2 ); // invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
(( com.android.server.am.PeriodicCleanerService$PackageUseInfo ) v4 ).toString ( ); // invoke-virtual {v4}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1623 */
/* add-int/lit8 v2, v2, -0x1 */
/* .line 1626 */
} // .end local v2 # "i":I
} // :cond_1
final String v2 = "High Memory Usage List:"; // const-string v2, "High Memory Usage List:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1627 */
v2 = com.android.server.am.PeriodicCleanerService.sHighMemoryApp;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/String; */
/* .line 1628 */
/* .local v3, "str":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1629 */
} // .end local v3 # "str":Ljava/lang/String;
/* .line 1630 */
} // :cond_2
final String v2 = "High Frequency Usage List:"; // const-string v2, "High Frequency Usage List:"
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1631 */
v2 = com.android.server.am.PeriodicCleanerService.sHighFrequencyApp;
v3 = } // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Ljava/lang/String; */
/* .line 1632 */
/* .restart local v3 # "str":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1633 */
} // .end local v3 # "str":Ljava/lang/String;
/* .line 1634 */
} // :cond_3
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1635 */
final String v1 = "---- End of Foreground-LRU ----"; // const-string v1, "---- End of Foreground-LRU ----"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1637 */
return;
/* .line 1634 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
private void excludeCurrentProcess ( android.util.SparseArray p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p2, "currentPackage" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;>;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1317 */
/* .local p1, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez p2, :cond_0 */
/* .line 1318 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( android.util.SparseArray ) p1 ).size ( ); // invoke-virtual {p1}, Landroid/util/SparseArray;->size()I
/* if-ge v0, v1, :cond_2 */
/* .line 1319 */
(( android.util.SparseArray ) p1 ).get ( v0 ); // invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/util/ArrayList; */
/* .line 1320 */
/* .local v1, "pkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* if-nez v1, :cond_1 */
/* .line 1321 */
} // :cond_1
(( java.util.ArrayList ) v1 ).remove ( p2 ); // invoke-virtual {v1, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1318 */
} // .end local v1 # "pkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1323 */
} // .end local v0 # "i":I
} // :cond_2
return;
/* .line 1317 */
} // :cond_3
} // :goto_2
return;
} // .end method
private java.util.ArrayList filterOutKillablePackages ( Integer p0, Integer p1, java.util.ArrayList p2, java.util.List p3, java.util.List p4, java.util.List p5 ) {
/* .locals 20 */
/* .param p1, "userId" # I */
/* .param p2, "pressure" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(II", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1399 */
/* .local p3, "agingPkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* .local p4, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p5, "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p6, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* move-object/from16 v9, p0 */
/* move/from16 v10, p1 */
/* move-object/from16 v11, p3 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v12 */
/* .line 1400 */
/* .local v12, "startTime":J */
v0 = this.mPMS;
/* .line 1401 */
(( com.android.server.am.ProcessManagerService ) v0 ).getProcessPolicy ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
v1 = this.mContext;
(( com.android.server.am.ProcessPolicy ) v0 ).updateDynamicWhiteList ( v1, v10 ); // invoke-virtual {v0, v1, v10}, Lcom/android/server/am/ProcessPolicy;->updateDynamicWhiteList(Landroid/content/Context;I)Ljava/util/HashMap;
/* .line 1402 */
/* .local v14, "dynWhitelist":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
final String v0 = "finish updateDynamicWhiteList"; // const-string v0, "finish updateDynamicWhiteList"
com.android.server.am.PeriodicCleanerService .checkTime ( v12,v13,v0 );
/* .line 1404 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v15, v0 */
/* .line 1405 */
/* .local v15, "killList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v16 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/PeriodicCleanerService;->isCameraForegroundCase()Z */
/* .line 1406 */
/* .local v16, "isCameraForeground":Z */
v8 = this.mAMS;
/* monitor-enter v8 */
/* .line 1407 */
try { // :try_start_0
v0 = this.mAMS;
v0 = this.mProcessList;
v0 = (( com.android.server.am.ProcessList ) v0 ).getLruSizeLOSP ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I
/* .line 1408 */
/* .local v0, "N":I */
/* add-int/lit8 v1, v0, -0x1 */
/* move v7, v1 */
/* .local v7, "i":I */
} // :goto_0
/* if-ltz v7, :cond_5 */
/* .line 1409 */
v1 = this.mAMS;
v1 = this.mProcessList;
(( com.android.server.am.ProcessList ) v1 ).getLruProcessesLOSP ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
(( java.util.ArrayList ) v1 ).get ( v7 ); // invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/ProcessRecord; */
/* move-object v6, v1 */
/* .line 1410 */
/* .local v6, "app":Lcom/android/server/am/ProcessRecord; */
v1 = (( com.android.server.am.ProcessRecord ) v6 ).isKilledByAm ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v1, :cond_3 */
v1 = (( com.android.server.am.ProcessRecord ) v6 ).isKilled ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* if-nez v1, :cond_3 */
(( com.android.server.am.ProcessRecord ) v6 ).getThread ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget v1, v6, Lcom/android/server/am/ProcessRecord;->userId:I */
/* if-ne v1, v10, :cond_3 */
v1 = this.info;
v1 = this.packageName;
/* .line 1411 */
v1 = (( java.util.ArrayList ) v11 ).contains ( v1 ); // invoke-virtual {v11, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* iget-boolean v1, v6, Lcom/android/server/am/ProcessRecord;->isolated:Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1412 */
/* move/from16 v17, v0 */
/* move/from16 v18, v7 */
/* move-object/from16 v19, v8 */
/* .line 1414 */
} // :cond_0
/* move-object/from16 v1, p0 */
/* move-object v2, v6 */
/* move/from16 v3, p2 */
/* move-object v4, v14 */
/* move-object/from16 v5, p4 */
/* move/from16 v17, v0 */
/* move-object v0, v6 */
} // .end local v6 # "app":Lcom/android/server/am/ProcessRecord;
/* .local v0, "app":Lcom/android/server/am/ProcessRecord; */
/* .local v17, "N":I */
/* move-object/from16 v6, p5 */
/* move/from16 v18, v7 */
} // .end local v7 # "i":I
/* .local v18, "i":I */
/* move-object/from16 v7, p6 */
/* move-object/from16 v19, v8 */
/* move/from16 v8, v16 */
try { // :try_start_1
v1 = /* invoke-direct/range {v1 ..v8}, Lcom/android/server/am/PeriodicCleanerService;->canCleanPackage(Lcom/android/server/am/ProcessRecord;ILjava/util/HashMap;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1417 */
v1 = this.info;
v1 = this.packageName;
v1 = (( java.util.ArrayList ) v15 ).contains ( v1 ); // invoke-virtual {v15, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v1, :cond_4 */
/* .line 1418 */
v1 = this.info;
v1 = this.packageName;
(( java.util.ArrayList ) v15 ).add ( v1 ); // invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1422 */
} // :cond_1
v1 = this.info;
v1 = this.packageName;
(( java.util.ArrayList ) v11 ).remove ( v1 ); // invoke-virtual {v11, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1423 */
v1 = this.info;
v1 = this.packageName;
(( java.util.ArrayList ) v15 ).remove ( v1 ); // invoke-virtual {v15, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1411 */
} // .end local v17 # "N":I
} // .end local v18 # "i":I
/* .local v0, "N":I */
/* .restart local v6 # "app":Lcom/android/server/am/ProcessRecord; */
/* .restart local v7 # "i":I */
} // :cond_2
/* move/from16 v17, v0 */
/* move-object v0, v6 */
/* move/from16 v18, v7 */
/* move-object/from16 v19, v8 */
} // .end local v6 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v7 # "i":I
/* .local v0, "app":Lcom/android/server/am/ProcessRecord; */
/* .restart local v17 # "N":I */
/* .restart local v18 # "i":I */
/* .line 1410 */
} // .end local v17 # "N":I
} // .end local v18 # "i":I
/* .local v0, "N":I */
/* .restart local v6 # "app":Lcom/android/server/am/ProcessRecord; */
/* .restart local v7 # "i":I */
} // :cond_3
/* move/from16 v17, v0 */
/* move-object v0, v6 */
/* move/from16 v18, v7 */
/* move-object/from16 v19, v8 */
/* .line 1408 */
} // .end local v0 # "N":I
} // .end local v6 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v7 # "i":I
/* .restart local v17 # "N":I */
/* .restart local v18 # "i":I */
} // :cond_4
} // :goto_1
/* add-int/lit8 v7, v18, -0x1 */
/* move/from16 v0, v17 */
/* move-object/from16 v8, v19 */
} // .end local v18 # "i":I
/* .restart local v7 # "i":I */
/* goto/16 :goto_0 */
} // .end local v17 # "N":I
/* .restart local v0 # "N":I */
} // :cond_5
/* move/from16 v17, v0 */
/* move/from16 v18, v7 */
/* move-object/from16 v19, v8 */
/* .line 1426 */
} // .end local v0 # "N":I
} // .end local v7 # "i":I
/* .restart local v17 # "N":I */
final String v0 = "finish filterOutKillablePackages"; // const-string v0, "finish filterOutKillablePackages"
com.android.server.am.PeriodicCleanerService .checkTime ( v12,v13,v0 );
/* .line 1427 */
} // .end local v17 # "N":I
/* monitor-exit v19 */
/* .line 1428 */
/* .line 1427 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v19, v8 */
} // :goto_2
/* monitor-exit v19 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v0 */
/* :catchall_1 */
/* move-exception v0 */
} // .end method
private Integer finalLruActiveLength ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "length" # I */
/* .line 505 */
int v0 = 3; // const/4 v0, 0x3
/* if-le p1, v0, :cond_0 */
/* .line 506 */
/* .line 508 */
} // :cond_0
} // .end method
private android.util.SparseArray findAgingPackageLocked ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "threshold" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 1265 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 1266 */
/* .local v0, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 1267 */
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* .line 1268 */
/* .local v2, "startTime":J */
v4 = this.mLruPackages;
v4 = (( java.util.ArrayList ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
/* sub-int/2addr v4, p1 */
/* .line 1269 */
/* .local v4, "size":I */
/* if-lez v4, :cond_1 */
/* .line 1270 */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
/* if-ge v5, v4, :cond_1 */
/* .line 1271 */
v6 = this.mLruPackages;
(( java.util.ArrayList ) v6 ).get ( v5 ); // invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* .line 1272 */
/* .local v6, "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
v7 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUserId ( v6 );
(( android.util.SparseArray ) v0 ).get ( v7 ); // invoke-virtual {v0, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v7, Ljava/util/ArrayList; */
/* .line 1273 */
/* .local v7, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* if-nez v7, :cond_0 */
/* .line 1274 */
/* new-instance v8, Ljava/util/ArrayList; */
/* invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V */
/* move-object v7, v8 */
/* .line 1275 */
v8 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUserId ( v6 );
(( android.util.SparseArray ) v0 ).put ( v8, v7 ); // invoke-virtual {v0, v8, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1277 */
} // :cond_0
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmPackageName ( v6 );
(( java.util.ArrayList ) v7 ).add ( v8 ); // invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1270 */
/* nop */
} // .end local v6 # "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
} // .end local v7 # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* add-int/lit8 v5, v5, 0x1 */
/* .line 1280 */
} // .end local v5 # "i":I
} // :cond_1
final String v5 = "finish findAgingPackageLocked"; // const-string v5, "finish findAgingPackageLocked"
com.android.server.am.PeriodicCleanerService .checkTime ( v2,v3,v5 );
/* .line 1281 */
} // .end local v2 # "startTime":J
} // .end local v4 # "size":I
/* monitor-exit v1 */
/* .line 1282 */
/* .line 1281 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private java.util.List findOtherGamePackageLocked ( Integer p0, java.lang.String p1 ) {
/* .locals 9 */
/* .param p1, "threshold" # I */
/* .param p2, "startProcessName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1289 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1291 */
/* .local v0, "startTime":J */
int v2 = 0; // const/4 v2, 0x0
/* .line 1292 */
/* .local v2, "lruPrevPackages":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;>;" */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
/* .line 1293 */
/* .local v3, "gamePackagesUid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v4 = this.mLock;
/* monitor-enter v4 */
/* .line 1294 */
try { // :try_start_0
v5 = this.mLruPackages;
v5 = (( java.util.ArrayList ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
/* .line 1295 */
/* .local v5, "lruSize":I */
/* if-lez v5, :cond_1 */
/* .line 1296 */
v6 = this.mLruPackages;
v6 = (( java.util.ArrayList ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->size()I
/* .line 1297 */
/* .local v6, "end":I */
v7 = this.mLruPackages;
v7 = (( java.util.ArrayList ) v7 ).size ( ); // invoke-virtual {v7}, Ljava/util/ArrayList;->size()I
/* sub-int/2addr v7, p1 */
/* .line 1298 */
/* .local v7, "start":I */
/* if-gez v7, :cond_0 */
int v7 = 0; // const/4 v7, 0x0
/* .line 1299 */
} // :cond_0
v8 = this.mLruPackages;
(( java.util.ArrayList ) v8 ).subList ( v7, v6 ); // invoke-virtual {v8, v7, v6}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;
/* move-object v2, v8 */
/* .line 1301 */
} // .end local v5 # "lruSize":I
} // .end local v6 # "end":I
} // .end local v7 # "start":I
} // :cond_1
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1304 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1305 */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_3
/* check-cast v5, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* .line 1306 */
/* .local v5, "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
v6 = com.android.server.am.PeriodicCleanerService.sGameApp;
v6 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmPackageName ( v5 );
if ( v6 != null) { // if-eqz v6, :cond_2
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmPackageName ( v5 );
/* .line 1307 */
v6 = (( java.lang.String ) p2 ).equals ( v6 ); // invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v6, :cond_2 */
/* .line 1308 */
v6 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUid ( v5 );
java.lang.Integer .valueOf ( v6 );
/* .line 1310 */
} // .end local v5 # "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
} // :cond_2
/* .line 1312 */
} // :cond_3
final String v4 = "finish findOtherGamePackageLocked"; // const-string v4, "finish findOtherGamePackageLocked"
com.android.server.am.PeriodicCleanerService .checkTime ( v0,v1,v4 );
/* .line 1313 */
/* .line 1301 */
/* :catchall_0 */
/* move-exception v5 */
try { // :try_start_1
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v5 */
} // .end method
private android.util.SparseArray findOverTimePackageLocked ( Integer p0 ) {
/* .locals 12 */
/* .param p1, "pressure" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 1230 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 1231 */
/* .local v0, "agingPkgs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 1233 */
try { // :try_start_0
v2 = this.mLruPackages;
v2 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mOverTimeAppNum:I */
/* sub-int/2addr v2, v3 */
/* .line 1234 */
/* .local v2, "size":I */
/* if-gez v2, :cond_0 */
/* monitor-exit v1 */
/* .line 1235 */
} // :cond_0
v3 = this.mLruPackages;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* .line 1236 */
/* .local v3, "tmpInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* if-ge v4, v2, :cond_3 */
/* .line 1237 */
v5 = this.mLruPackages;
(( java.util.ArrayList ) v5 ).get ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* .line 1238 */
/* .local v5, "histroyInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmBackgroundTime ( v3 );
/* move-result-wide v6 */
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmBackgroundTime ( v5 );
/* move-result-wide v8 */
/* sub-long/2addr v6, v8 */
/* .line 1239 */
/* .local v6, "diffTime":J */
/* sget-boolean v8, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_1
/* .line 1240 */
final String v8 = "PeriodicCleaner"; // const-string v8, "PeriodicCleaner"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "OverTimeValue:"; // const-string v10, "OverTimeValue:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmBackgroundTime ( v3 );
/* move-result-wide v10 */
(( java.lang.StringBuilder ) v9 ).append ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = "-"; // const-string v10, "-"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmBackgroundTime ( v5 );
/* move-result-wide v10 */
(( java.lang.StringBuilder ) v9 ).append ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = "="; // const-string v10, "="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6, v7 ); // invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = "?"; // const-string v10, "?"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = com.android.server.am.PeriodicCleanerService.sDefaultTimeLevel;
/* aget v10, v10, p1 */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v9 );
/* .line 1243 */
final String v8 = "PeriodicCleaner"; // const-string v8, "PeriodicCleaner"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "OverTimePackage:"; // const-string v10, "OverTimePackage:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmPackageName ( v3 );
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ":"; // const-string v10, ":"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmPackageName ( v5 );
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v9 );
/* .line 1246 */
} // :cond_1
v8 = com.android.server.am.PeriodicCleanerService.sDefaultTimeLevel;
/* aget v8, v8, p1 */
/* int-to-long v8, v8 */
/* cmp-long v8, v6, v8 */
/* if-ltz v8, :cond_3 */
/* .line 1247 */
v8 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUserId ( v5 );
(( android.util.SparseArray ) v0 ).get ( v8 ); // invoke-virtual {v0, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v8, Ljava/util/ArrayList; */
/* .line 1248 */
/* .local v8, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* if-nez v8, :cond_2 */
/* .line 1249 */
/* new-instance v9, Ljava/util/ArrayList; */
/* invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V */
/* move-object v8, v9 */
/* .line 1250 */
v9 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUserId ( v5 );
(( android.util.SparseArray ) v0 ).put ( v9, v8 ); // invoke-virtual {v0, v9, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1252 */
} // :cond_2
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmPackageName ( v5 );
(( java.util.ArrayList ) v8 ).add ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1236 */
/* nop */
} // .end local v5 # "histroyInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
} // .end local v6 # "diffTime":J
} // .end local v8 # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* add-int/lit8 v4, v4, 0x1 */
/* goto/16 :goto_0 */
/* .line 1257 */
} // .end local v2 # "size":I
} // .end local v3 # "tmpInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
} // .end local v4 # "i":I
} // :cond_3
/* monitor-exit v1 */
/* .line 1258 */
/* .line 1257 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private java.util.List getAllAudioFocus ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1357 */
try { // :try_start_0
v0 = this.mGetAllAudioFocusMethod;
v1 = this.mAudioService;
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v0 ).invoke ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1358 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1359 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getAllAudioFocus: "; // const-string v2, "getAllAudioFocus: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "PeriodicCleaner"; // const-string v2, "PeriodicCleaner"
android.util.Slog .e ( v2,v1 );
/* .line 1361 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
} // .end method
private java.util.HashMap getAllPackagePss ( ) {
/* .locals 8 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 765 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 767 */
/* .local v1, "packagePss":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;" */
try { // :try_start_0
v2 = this.mProcessStats;
/* sget-wide v3, Lcom/android/internal/app/procstats/ProcessStats;->COMMIT_PERIOD:J */
/* const-wide/16 v5, 0x2 */
/* div-long/2addr v3, v5 */
/* const-wide/32 v5, 0x5265c00 */
/* sub-long/2addr v5, v3 */
/* .line 769 */
/* .local v2, "pfd":Landroid/os/ParcelFileDescriptor; */
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 770 */
final String v4 = "open file error."; // const-string v4, "open file error."
android.util.Slog .e ( v0,v4 );
/* .line 771 */
/* .line 773 */
} // :cond_0
/* new-instance v4, Lcom/android/internal/app/procstats/ProcessStats; */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v4, v5}, Lcom/android/internal/app/procstats/ProcessStats;-><init>(Z)V */
/* .line 774 */
/* .local v4, "stats":Lcom/android/internal/app/procstats/ProcessStats; */
/* new-instance v5, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream; */
/* invoke-direct {v5, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V */
/* .line 775 */
/* .local v5, "stream":Ljava/io/InputStream; */
(( com.android.internal.app.procstats.ProcessStats ) v4 ).read ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/internal/app/procstats/ProcessStats;->read(Ljava/io/InputStream;)V
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 777 */
try { // :try_start_1
(( java.io.InputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/InputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 779 */
/* .line 778 */
/* :catch_0 */
/* move-exception v6 */
/* .line 780 */
} // :goto_0
try { // :try_start_2
v6 = this.mReadError;
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 781 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Failure reading process stats: "; // const-string v7, "Failure reading process stats: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mReadError;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v6 );
/* .line 782 */
/* .line 784 */
} // :cond_1
/* invoke-direct {p0, v4}, Lcom/android/server/am/PeriodicCleanerService;->getAllPackagePssDetail(Lcom/android/internal/app/procstats/ProcessStats;)Ljava/util/HashMap; */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_1 */
/* move-object v1, v0 */
/* .line 787 */
} // .end local v2 # "pfd":Landroid/os/ParcelFileDescriptor;
} // .end local v4 # "stats":Lcom/android/internal/app/procstats/ProcessStats;
} // .end local v5 # "stream":Ljava/io/InputStream;
/* .line 785 */
/* :catch_1 */
/* move-exception v2 */
/* .line 786 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "RemoteException:"; // const-string v4, "RemoteException:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 788 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :goto_1
} // .end method
private java.util.HashMap getAllPackagePssDetail ( com.android.internal.app.procstats.ProcessStats p0 ) {
/* .locals 34 */
/* .param p1, "stats" # Lcom/android/internal/app/procstats/ProcessStats; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/internal/app/procstats/ProcessStats;", */
/* ")", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 792 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* move-object v1, v0 */
/* .line 793 */
/* .local v1, "packagePss":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;" */
/* move-object/from16 v2, p1 */
v0 = this.mPackages;
/* .line 794 */
(( com.android.internal.app.ProcessMap ) v0 ).getMap ( ); // invoke-virtual {v0}, Lcom/android/internal/app/ProcessMap;->getMap()Landroid/util/ArrayMap;
/* .line 795 */
/* .local v3, "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;" */
v4 = (( android.util.ArrayMap ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/ArrayMap;->size()I
/* .line 797 */
/* .local v4, "NPKG":I */
int v0 = 0; // const/4 v0, 0x0
/* move v5, v0 */
/* .local v5, "ip":I */
} // :goto_0
/* if-ge v5, v4, :cond_7 */
/* .line 798 */
(( android.util.ArrayMap ) v3 ).keyAt ( v5 ); // invoke-virtual {v3, v5}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;
/* move-object v6, v0 */
/* check-cast v6, Ljava/lang/String; */
/* .line 799 */
/* .local v6, "pkgName":Ljava/lang/String; */
(( android.util.ArrayMap ) v3 ).valueAt ( v5 ); // invoke-virtual {v3, v5}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* move-object v7, v0 */
/* check-cast v7, Landroid/util/SparseArray; */
/* .line 800 */
/* .local v7, "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
v8 = (( android.util.SparseArray ) v7 ).size ( ); // invoke-virtual {v7}, Landroid/util/SparseArray;->size()I
/* .line 801 */
/* .local v8, "NUID":I */
/* const-wide/16 v9, 0x0 */
/* .line 803 */
/* .local v9, "maxPackagePssAve":J */
int v0 = 0; // const/4 v0, 0x0
/* move-wide v10, v9 */
/* move v9, v0 */
/* .local v9, "iu":I */
/* .local v10, "maxPackagePssAve":J */
} // :goto_1
/* if-ge v9, v8, :cond_6 */
/* .line 804 */
v12 = (( android.util.SparseArray ) v7 ).keyAt ( v9 ); // invoke-virtual {v7, v9}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 805 */
/* .local v12, "uid":I */
(( android.util.SparseArray ) v7 ).valueAt ( v9 ); // invoke-virtual {v7, v9}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* move-object v13, v0 */
/* check-cast v13, Landroid/util/LongSparseArray; */
/* .line 806 */
/* .local v13, "vpkgs":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;" */
v14 = (( android.util.LongSparseArray ) v13 ).size ( ); // invoke-virtual {v13}, Landroid/util/LongSparseArray;->size()I
/* .line 807 */
/* .local v14, "NVERS":I */
/* const-wide/16 v15, 0x0 */
/* .line 809 */
/* .local v15, "tmpMaxPackagePssAve":J */
int v0 = 0; // const/4 v0, 0x0
/* move-wide/from16 v16, v15 */
/* move v15, v0 */
/* .local v15, "iv":I */
/* .local v16, "tmpMaxPackagePssAve":J */
} // :goto_2
/* if-ge v15, v14, :cond_4 */
/* .line 810 */
(( android.util.LongSparseArray ) v13 ).keyAt ( v15 ); // invoke-virtual {v13, v15}, Landroid/util/LongSparseArray;->keyAt(I)J
/* move-result-wide v18 */
/* .line 811 */
/* .local v18, "vers":J */
(( android.util.LongSparseArray ) v13 ).valueAt ( v15 ); // invoke-virtual {v13, v15}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* move-object v2, v0 */
/* check-cast v2, Lcom/android/internal/app/procstats/ProcessStats$PackageState; */
/* .line 812 */
/* .local v2, "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState; */
v0 = this.mProcesses;
/* move-object/from16 v20, v3 */
} // .end local v3 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
/* .local v20, "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;" */
v3 = (( android.util.ArrayMap ) v0 ).size ( ); // invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I
/* .line 813 */
/* .local v3, "NPROCS":I */
/* const-wide/16 v21, 0x0 */
/* .line 815 */
/* .local v21, "tmpMaxPackagePssAveVer":J */
int v0 = 0; // const/4 v0, 0x0
/* move/from16 v23, v4 */
/* move v4, v0 */
/* .local v4, "iproc":I */
/* .local v23, "NPKG":I */
} // :goto_3
/* if-ge v4, v3, :cond_2 */
/* .line 817 */
try { // :try_start_0
v0 = this.mProcesses;
(( android.util.ArrayMap ) v0 ).valueAt ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;
/* check-cast v0, Lcom/android/internal/app/procstats/ProcessState; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .line 818 */
/* .local v0, "proc":Lcom/android/internal/app/procstats/ProcessState; */
/* move-object/from16 v24, v2 */
/* move/from16 v25, v3 */
/* move-object/from16 v2, p0 */
} // .end local v2 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
} // .end local v3 # "NPROCS":I
/* .local v24, "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState; */
/* .local v25, "NPROCS":I */
try { // :try_start_1
v3 = this.mFieldProcessState;
(( java.lang.reflect.Field ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/android/internal/app/procstats/PssTable; */
/* .line 820 */
/* .local v3, "mPssTable":Lcom/android/internal/app/procstats/PssTable; */
v26 = (( com.android.internal.app.procstats.PssTable ) v3 ).getKeyCount ( ); // invoke-virtual {v3}, Lcom/android/internal/app/procstats/PssTable;->getKeyCount()I
/* move/from16 v27, v26 */
/* .line 821 */
/* .local v27, "Num":I */
/* const-wide/16 v28, 0x0 */
/* .line 827 */
/* .local v28, "maxSingleProcPssAve":J */
/* const/16 v26, 0x0 */
/* move-object/from16 v30, v0 */
/* move/from16 v0, v26 */
/* .local v0, "ivalue":I */
/* .local v30, "proc":Lcom/android/internal/app/procstats/ProcessState; */
} // :goto_4
/* move/from16 v2, v27 */
} // .end local v27 # "Num":I
/* .local v2, "Num":I */
/* if-ge v0, v2, :cond_1 */
/* .line 828 */
v26 = (( com.android.internal.app.procstats.PssTable ) v3 ).getKeyAt ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/internal/app/procstats/PssTable;->getKeyAt(I)I
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* move/from16 v27, v26 */
/* .line 829 */
/* .local v27, "key":I */
/* move/from16 v26, v2 */
} // .end local v2 # "Num":I
/* .local v26, "Num":I */
int v2 = 2; // const/4 v2, 0x2
/* move-object/from16 v31, v7 */
/* move/from16 v7, v27 */
} // .end local v27 # "key":I
/* .local v7, "key":I */
/* .local v31, "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
try { // :try_start_2
(( com.android.internal.app.procstats.PssTable ) v3 ).getValue ( v7, v2 ); // invoke-virtual {v3, v7, v2}, Lcom/android/internal/app/procstats/PssTable;->getValue(II)J
/* move-result-wide v32 */
/* cmp-long v27, v32, v28 */
/* if-ltz v27, :cond_0 */
/* .line 830 */
(( com.android.internal.app.procstats.PssTable ) v3 ).getValue ( v7, v2 ); // invoke-virtual {v3, v7, v2}, Lcom/android/internal/app/procstats/PssTable;->getValue(II)J
/* move-result-wide v32 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* move-wide/from16 v27, v32 */
/* move-wide/from16 v28, v27 */
/* .line 827 */
} // .end local v7 # "key":I
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* move-object/from16 v2, p0 */
/* move/from16 v27, v26 */
/* move-object/from16 v7, v31 */
/* .line 834 */
} // .end local v0 # "ivalue":I
} // .end local v3 # "mPssTable":Lcom/android/internal/app/procstats/PssTable;
} // .end local v26 # "Num":I
} // .end local v28 # "maxSingleProcPssAve":J
} // .end local v30 # "proc":Lcom/android/internal/app/procstats/ProcessState;
/* :catch_0 */
/* move-exception v0 */
/* .line 827 */
} // .end local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .restart local v0 # "ivalue":I */
/* .restart local v2 # "Num":I */
/* .restart local v3 # "mPssTable":Lcom/android/internal/app/procstats/PssTable; */
/* .local v7, "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
/* .restart local v28 # "maxSingleProcPssAve":J */
/* .restart local v30 # "proc":Lcom/android/internal/app/procstats/ProcessState; */
} // :cond_1
/* move/from16 v26, v2 */
/* move-object/from16 v31, v7 */
/* .line 833 */
} // .end local v0 # "ivalue":I
} // .end local v2 # "Num":I
} // .end local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .restart local v26 # "Num":I */
/* .restart local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
/* add-long v21, v21, v28 */
/* .line 836 */
} // .end local v3 # "mPssTable":Lcom/android/internal/app/procstats/PssTable;
} // .end local v26 # "Num":I
} // .end local v28 # "maxSingleProcPssAve":J
} // .end local v30 # "proc":Lcom/android/internal/app/procstats/ProcessState;
/* .line 834 */
} // .end local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .restart local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v31, v7 */
} // .end local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .restart local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
} // .end local v24 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
} // .end local v25 # "NPROCS":I
} // .end local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .local v2, "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState; */
/* .local v3, "NPROCS":I */
/* .restart local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v24, v2 */
/* move/from16 v25, v3 */
/* move-object/from16 v31, v7 */
/* .line 835 */
} // .end local v2 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
} // .end local v3 # "NPROCS":I
} // .end local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v24 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState; */
/* .restart local v25 # "NPROCS":I */
/* .restart local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
} // :goto_5
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "ProcessState#mPssTable error "; // const-string v3, "ProcessState#mPssTable error "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "PeriodicCleaner"; // const-string v3, "PeriodicCleaner"
android.util.Slog .e ( v3,v2 );
/* .line 815 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_6
/* add-int/lit8 v4, v4, 0x1 */
/* move-object/from16 v2, v24 */
/* move/from16 v3, v25 */
/* move-object/from16 v7, v31 */
/* goto/16 :goto_3 */
} // .end local v24 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
} // .end local v25 # "NPROCS":I
} // .end local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .restart local v2 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState; */
/* .restart local v3 # "NPROCS":I */
/* .restart local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
} // :cond_2
/* move-object/from16 v24, v2 */
/* move/from16 v25, v3 */
/* move-object/from16 v31, v7 */
/* .line 838 */
} // .end local v2 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
} // .end local v3 # "NPROCS":I
} // .end local v4 # "iproc":I
} // .end local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .restart local v24 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState; */
/* .restart local v25 # "NPROCS":I */
/* .restart local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
/* cmp-long v0, v21, v16 */
/* if-ltz v0, :cond_3 */
/* .line 839 */
/* move-wide/from16 v2, v21 */
/* move-wide/from16 v16, v2 */
/* .line 809 */
} // .end local v18 # "vers":J
} // .end local v21 # "tmpMaxPackagePssAveVer":J
} // .end local v24 # "pkgState":Lcom/android/internal/app/procstats/ProcessStats$PackageState;
} // .end local v25 # "NPROCS":I
} // :cond_3
/* add-int/lit8 v15, v15, 0x1 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, v20 */
/* move/from16 v4, v23 */
/* move-object/from16 v7, v31 */
/* goto/16 :goto_2 */
} // .end local v20 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
} // .end local v23 # "NPKG":I
} // .end local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .local v3, "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;" */
/* .local v4, "NPKG":I */
/* .restart local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
} // :cond_4
/* move-object/from16 v20, v3 */
/* move/from16 v23, v4 */
/* move-object/from16 v31, v7 */
/* .line 842 */
} // .end local v3 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
} // .end local v4 # "NPKG":I
} // .end local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
} // .end local v15 # "iv":I
/* .restart local v20 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;" */
/* .restart local v23 # "NPKG":I */
/* .restart local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
/* cmp-long v0, v16, v10 */
/* if-ltz v0, :cond_5 */
/* .line 843 */
/* move-wide/from16 v2, v16 */
/* move-wide v10, v2 */
/* .line 803 */
} // .end local v12 # "uid":I
} // .end local v13 # "vpkgs":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;"
} // .end local v14 # "NVERS":I
} // .end local v16 # "tmpMaxPackagePssAve":J
} // :cond_5
/* add-int/lit8 v9, v9, 0x1 */
/* move-object/from16 v2, p1 */
/* move-object/from16 v3, v20 */
/* move/from16 v4, v23 */
/* move-object/from16 v7, v31 */
/* goto/16 :goto_1 */
} // .end local v20 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
} // .end local v23 # "NPKG":I
} // .end local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* .restart local v3 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;" */
/* .restart local v4 # "NPKG":I */
/* .restart local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
} // :cond_6
/* move-object/from16 v20, v3 */
/* move/from16 v23, v4 */
/* move-object/from16 v31, v7 */
/* .line 846 */
} // .end local v3 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
} // .end local v4 # "NPKG":I
} // .end local v7 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
} // .end local v9 # "iu":I
/* .restart local v20 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;" */
/* .restart local v23 # "NPKG":I */
/* .restart local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;" */
java.lang.Long .valueOf ( v10,v11 );
(( java.util.HashMap ) v1 ).put ( v6, v0 ); // invoke-virtual {v1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 797 */
} // .end local v6 # "pkgName":Ljava/lang/String;
} // .end local v8 # "NUID":I
} // .end local v10 # "maxPackagePssAve":J
} // .end local v31 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;"
/* add-int/lit8 v5, v5, 0x1 */
/* move-object/from16 v2, p1 */
/* goto/16 :goto_0 */
/* .line 848 */
} // .end local v5 # "ip":I
} // .end local v20 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;"
} // .end local v23 # "NPKG":I
/* .restart local v3 # "pkgMap":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/String;Landroid/util/SparseArray<Landroid/util/LongSparseArray<Lcom/android/internal/app/procstats/ProcessStats$PackageState;>;>;>;" */
/* .restart local v4 # "NPKG":I */
} // :cond_7
} // .end method
private java.util.List getAudioActiveUids ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1330 */
v0 = this.mAudioService;
/* .line 1331 */
(( com.android.server.audio.AudioService ) v0 ).getActivePlaybackConfigurations ( ); // invoke-virtual {v0}, Lcom/android/server/audio/AudioService;->getActivePlaybackConfigurations()Ljava/util/List;
/* .line 1332 */
/* .local v0, "activePlayers":Ljava/util/List;, "Ljava/util/List<Landroid/media/AudioPlaybackConfiguration;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1333 */
/* .local v1, "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Landroid/media/AudioPlaybackConfiguration; */
/* .line 1334 */
/* .local v3, "conf":Landroid/media/AudioPlaybackConfiguration; */
v4 = (( android.media.AudioPlaybackConfiguration ) v3 ).getPlayerState ( ); // invoke-virtual {v3}, Landroid/media/AudioPlaybackConfiguration;->getPlayerState()I
/* .line 1335 */
/* .local v4, "state":I */
int v5 = 2; // const/4 v5, 0x2
/* if-eq v4, v5, :cond_0 */
int v5 = 3; // const/4 v5, 0x3
/* if-ne v4, v5, :cond_1 */
/* .line 1336 */
} // :cond_0
v5 = (( android.media.AudioPlaybackConfiguration ) v3 ).getClientUid ( ); // invoke-virtual {v3}, Landroid/media/AudioPlaybackConfiguration;->getClientUid()I
java.lang.Integer .valueOf ( v5 );
/* .line 1338 */
} // .end local v3 # "conf":Landroid/media/AudioPlaybackConfiguration;
} // .end local v4 # "state":I
} // :cond_1
/* .line 1340 */
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getAllAudioFocus()Ljava/util/List; */
/* .line 1341 */
/* .local v2, "focusUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Ljava/lang/Integer; */
/* .line 1342 */
v5 = /* .local v4, "uid":Ljava/lang/Integer; */
/* if-nez v5, :cond_3 */
/* .line 1343 */
/* .line 1345 */
} // .end local v4 # "uid":Ljava/lang/Integer;
} // :cond_3
/* .line 1346 */
} // :cond_4
/* sget-boolean v3, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
v3 = if ( v3 != null) { // if-eqz v3, :cond_5
/* if-lez v3, :cond_5 */
v3 = /* .line 1347 */
/* new-array v3, v3, [I */
/* .line 1348 */
/* .local v3, "uids":[I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
v5 = } // :goto_2
/* if-ge v4, v5, :cond_5 */
/* .line 1349 */
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* aput v5, v3, v4 */
/* .line 1348 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1352 */
} // .end local v3 # "uids":[I
} // .end local v4 # "i":I
} // :cond_5
} // .end method
private java.util.List getLocationActiveUids ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1377 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1378 */
/* .local v0, "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v1 = this.mPMS;
/* .line 1379 */
(( com.android.server.am.ProcessManagerService ) v1 ).getProcessPolicy ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
int v2 = 3; // const/4 v2, 0x3
(( com.android.server.am.ProcessPolicy ) v1 ).getActiveUidRecordList ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->getActiveUidRecordList(I)Ljava/util/List;
/* .line 1380 */
/* .local v1, "activeUids":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;" */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1381 */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 1382 */
/* .local v3, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
/* .line 1383 */
/* .local v4, "uid":I */
v5 = java.lang.Integer .valueOf ( v4 );
/* if-nez v5, :cond_0 */
/* .line 1384 */
java.lang.Integer .valueOf ( v4 );
/* .line 1386 */
} // .end local v3 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // .end local v4 # "uid":I
} // :cond_0
/* .line 1388 */
} // :cond_1
} // .end method
private Integer getMemPressureLevel ( ) {
/* .locals 9 */
/* .line 1159 */
/* new-instance v0, Lcom/android/internal/util/MemInfoReader; */
/* invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V */
/* .line 1160 */
/* .local v0, "minfo":Lcom/android/internal/util/MemInfoReader; */
(( com.android.internal.util.MemInfoReader ) v0 ).readMemInfo ( ); // invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V
/* .line 1161 */
(( com.android.internal.util.MemInfoReader ) v0 ).getRawInfo ( ); // invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J
/* .line 1162 */
/* .local v1, "rawInfo":[J */
/* iget v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_CACHED:I */
/* aget-wide v2, v1, v2 */
/* iget v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SWAPCACHED:I */
/* aget-wide v5, v1, v4 */
/* add-long/2addr v2, v5 */
/* iget v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_BUFFERS:I */
/* aget-wide v5, v1, v5 */
/* add-long/2addr v2, v5 */
/* .line 1165 */
/* .local v2, "otherFile":J */
/* iget v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_SHMEM:I */
/* aget-wide v5, v1, v5 */
/* iget v7, p0, Lcom/android/server/am/PeriodicCleanerService;->mDebugClz_MEMINFO_UNEVICTABLE:I */
/* aget-wide v7, v1, v7 */
/* add-long/2addr v5, v7 */
/* aget-wide v7, v1, v4 */
/* add-long/2addr v5, v7 */
/* .line 1168 */
/* .local v5, "needRemovedFile":J */
/* cmp-long v4, v2, v5 */
/* if-lez v4, :cond_0 */
/* .line 1169 */
/* sub-long/2addr v2, v5 */
/* .line 1171 */
} // :cond_0
v4 = this.mPressureCacheThreshold;
int v7 = 0; // const/4 v7, 0x0
/* aget v7, v4, v7 */
/* int-to-long v7, v7 */
/* cmp-long v7, v2, v7 */
/* if-ltz v7, :cond_1 */
/* .line 1172 */
int v4 = -1; // const/4 v4, -0x1
/* .local v4, "level":I */
/* .line 1173 */
} // .end local v4 # "level":I
} // :cond_1
int v7 = 1; // const/4 v7, 0x1
/* aget v7, v4, v7 */
/* int-to-long v7, v7 */
/* cmp-long v7, v2, v7 */
/* if-ltz v7, :cond_2 */
/* .line 1174 */
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "level":I */
/* .line 1175 */
} // .end local v4 # "level":I
} // :cond_2
int v7 = 2; // const/4 v7, 0x2
/* aget v4, v4, v7 */
/* int-to-long v7, v4 */
/* cmp-long v4, v2, v7 */
/* if-ltz v4, :cond_3 */
/* .line 1176 */
int v4 = 1; // const/4 v4, 0x1
/* .restart local v4 # "level":I */
/* .line 1178 */
} // .end local v4 # "level":I
} // :cond_3
int v4 = 2; // const/4 v4, 0x2
/* .line 1180 */
/* .restart local v4 # "level":I */
} // :goto_0
/* sget-boolean v7, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 1181 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Other File: "; // const-string v8, "Other File: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2, v3 ); // invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = "KB.Mem Pressure Level: "; // const-string v8, "KB.Mem Pressure Level: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "PeriodicCleaner"; // const-string v8, "PeriodicCleaner"
android.util.Slog .i ( v8,v7 );
/* .line 1183 */
} // :cond_4
} // .end method
private java.util.HashMap getPackageForegroundTime ( ) {
/* .locals 17 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 878 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
/* .line 879 */
/* .local v6, "currentTime":J */
/* const-wide/32 v0, 0x5265c00 */
/* sub-long v0, v6, v0 */
/* .line 880 */
/* .local v0, "startTime":J */
/* const-wide/16 v8, 0x0 */
/* cmp-long v2, v0, v8 */
/* if-gez v2, :cond_0 */
/* const-wide/16 v0, 0x0 */
} // :cond_0
/* move-wide v10, v0 */
/* .line 881 */
} // .end local v0 # "startTime":J
/* .local v10, "startTime":J */
/* move-object/from16 v12, p0 */
v0 = this.mUSM;
int v1 = 4; // const/4 v1, 0x4
/* move-wide v2, v10 */
/* move-wide v4, v6 */
/* invoke-virtual/range {v0 ..v5}, Landroid/app/usage/UsageStatsManager;->queryUsageStats(IJJ)Ljava/util/List; */
/* .line 884 */
/* .local v0, "stats":Ljava/util/List;, "Ljava/util/List<Landroid/app/usage/UsageStats;>;" */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* .line 885 */
/* .local v1, "packageForegroundTime":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_0
/* if-ge v2, v3, :cond_5 */
/* .line 886 */
/* check-cast v3, Landroid/app/usage/UsageStats; */
/* .line 887 */
/* .local v3, "us":Landroid/app/usage/UsageStats; */
/* if-nez v3, :cond_1 */
/* .line 889 */
} // :cond_1
(( android.app.usage.UsageStats ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/app/usage/UsageStats;->getPackageName()Ljava/lang/String;
/* .line 890 */
/* .local v4, "packageName":Ljava/lang/String; */
/* if-nez v4, :cond_2 */
/* .line 892 */
} // :cond_2
(( android.app.usage.UsageStats ) v3 ).getTotalTimeInForeground ( ); // invoke-virtual {v3}, Landroid/app/usage/UsageStats;->getTotalTimeInForeground()J
/* move-result-wide v13 */
/* .line 893 */
/* .local v13, "totalTime":J */
/* cmp-long v5, v13, v8 */
/* if-gtz v5, :cond_3 */
/* .line 895 */
} // :cond_3
(( java.util.HashMap ) v1 ).get ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Long; */
/* .line 896 */
/* .local v5, "foregroundTime":Ljava/lang/Long; */
/* if-nez v5, :cond_4 */
/* .line 897 */
java.lang.Long .valueOf ( v13,v14 );
(( java.util.HashMap ) v1 ).put ( v4, v15 ); // invoke-virtual {v1, v4, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 899 */
} // :cond_4
(( java.lang.Long ) v5 ).longValue ( ); // invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
/* move-result-wide v15 */
/* add-long/2addr v15, v13 */
/* invoke-static/range {v15 ..v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long; */
(( java.util.HashMap ) v1 ).put ( v4, v15 ); // invoke-virtual {v1, v4, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 885 */
} // .end local v3 # "us":Landroid/app/usage/UsageStats;
} // .end local v4 # "packageName":Ljava/lang/String;
} // .end local v5 # "foregroundTime":Ljava/lang/Long;
} // .end local v13 # "totalTime":J
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 903 */
} // .end local v2 # "i":I
} // :cond_5
} // .end method
private java.util.List getVisibleWindowOwner ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1366 */
try { // :try_start_0
v0 = this.mGetVisibleWindowOwnerMethod;
v1 = this.mWMS;
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v0 ).invoke ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* .line 1368 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
this.mLastVisibleUids = v0;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1369 */
/* .line 1370 */
} // .end local v0 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 1371 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getVisibleWindowOwner: "; // const-string v2, "getVisibleWindowOwner: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "PeriodicCleaner"; // const-string v2, "PeriodicCleaner"
android.util.Slog .e ( v2,v1 );
/* .line 1373 */
} // .end local v0 # "e":Ljava/lang/Exception;
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
} // .end method
private void handleScreenOff ( ) {
/* .locals 18 */
/* .line 907 */
/* move-object/from16 v8, p0 */
/* iget v0, v8, Lcom/android/server/am/PeriodicCleanerService;->mScreenState:I */
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_0 */
/* .line 908 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
final String v1 = "screen on when deap clean, skip"; // const-string v1, "screen on when deap clean, skip"
android.util.Slog .d ( v0,v1 );
/* .line 909 */
return;
/* .line 911 */
} // :cond_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v9 */
/* .line 912 */
/* .local v9, "startTime":J */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v11, v0 */
/* .line 913 */
/* .local v11, "victimList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v12, v0 */
/* .line 914 */
/* .local v12, "killed":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v13, v0 */
/* .line 915 */
/* .local v13, "topUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v2 = this.mLock;
/* monitor-enter v2 */
/* .line 916 */
try { // :try_start_0
v0 = this.mLruPackages;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 917 */
/* .local v0, "N":I */
/* add-int/lit8 v3, v0, -0x1 */
/* .local v3, "i":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "j":I */
} // :goto_0
/* if-ltz v3, :cond_1 */
/* if-ge v4, v1, :cond_1 */
/* .line 918 */
v5 = this.mLruPackages;
(( java.util.ArrayList ) v5 ).get ( v3 ); // invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
v5 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUid ( v5 );
java.lang.Integer .valueOf ( v5 );
/* .line 917 */
/* add-int/lit8 v3, v3, -0x1 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 920 */
} // .end local v0 # "N":I
} // .end local v3 # "i":I
} // .end local v4 # "j":I
} // :cond_1
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 921 */
v0 = this.mLastVisibleUids;
v2 = com.android.server.am.PeriodicCleanerService.SYSTEM_UID_OBJ;
/* .line 922 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/PeriodicCleanerService;->getAudioActiveUids()Ljava/util/List; */
/* .line 923 */
/* .local v14, "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/PeriodicCleanerService;->getLocationActiveUids()Ljava/util/List; */
/* .line 924 */
/* .local v15, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 925 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TopPkg="; // const-string v3, "TopPkg="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v13 ); // invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", AudioActive="; // const-string v3, ", AudioActive="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", LocationActive="; // const-string v3, ", LocationActive="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v15 ); // invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", LastVisible="; // const-string v3, ", LastVisible="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLastVisibleUids;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 930 */
} // :cond_2
v3 = this.mAMS;
/* monitor-enter v3 */
/* .line 931 */
try { // :try_start_1
v0 = this.mAMS;
v0 = this.mProcessList;
v0 = (( com.android.server.am.ProcessList ) v0 ).getLruSizeLOSP ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I
/* .line 932 */
/* .restart local v0 # "N":I */
/* add-int/lit8 v2, v0, -0x1 */
/* .local v2, "i":I */
} // :goto_1
/* if-ltz v2, :cond_4 */
/* .line 933 */
v4 = this.mAMS;
v4 = this.mProcessList;
(( com.android.server.am.ProcessList ) v4 ).getLruProcessesLOSP ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
(( java.util.ArrayList ) v4 ).get ( v2 ); // invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/ProcessRecord; */
/* .line 935 */
/* .local v4, "app":Lcom/android/server/am/ProcessRecord; */
v5 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v5 ).getLastPss ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v5 */
/* const-wide/16 v16, 0x400 */
/* div-long v5, v5, v16 */
/* iget v7, v8, Lcom/android/server/am/PeriodicCleanerService;->mAppMemThreshold:I */
/* move/from16 v17, v2 */
} // .end local v2 # "i":I
/* .local v17, "i":I */
/* int-to-long v1, v7 */
/* cmp-long v1, v5, v1 */
/* if-lez v1, :cond_3 */
v1 = this.mProfile;
/* .line 936 */
v1 = (( com.android.server.am.ProcessProfileRecord ) v1 ).getPid ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProfileRecord;->getPid()I
/* if-eq v1, v2, :cond_3 */
/* iget v1, v4, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 938 */
v1 = java.lang.Integer .valueOf ( v1 );
/* if-nez v1, :cond_3 */
/* iget v1, v4, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 939 */
v1 = java.lang.Integer .valueOf ( v1 );
/* if-nez v1, :cond_3 */
/* iget v1, v4, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 940 */
v1 = java.lang.Integer .valueOf ( v1 );
/* if-nez v1, :cond_3 */
/* .line 941 */
/* .line 932 */
} // .end local v4 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_3
/* add-int/lit8 v2, v17, -0x1 */
int v1 = 2; // const/4 v1, 0x2
} // .end local v17 # "i":I
/* .restart local v2 # "i":I */
} // :cond_4
/* move/from16 v17, v2 */
/* .line 944 */
} // .end local v2 # "i":I
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_2
/* if-ge v1, v2, :cond_6 */
/* .line 945 */
/* iget v2, v8, Lcom/android/server/am/PeriodicCleanerService;->mScreenState:I */
int v4 = 2; // const/4 v4, 0x2
/* if-eq v2, v4, :cond_5 */
/* .line 946 */
final String v2 = "PeriodicCleaner"; // const-string v2, "PeriodicCleaner"
final String v4 = "screen on when deap clean, abort"; // const-string v4, "screen on when deap clean, abort"
android.util.Slog .w ( v2,v4 );
/* .line 947 */
/* .line 949 */
} // :cond_5
/* check-cast v2, Lcom/android/server/am/ProcessRecord; */
/* .line 950 */
/* .local v2, "app":Lcom/android/server/am/ProcessRecord; */
final String v5 = "PeriodicCleaner"; // const-string v5, "PeriodicCleaner"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Kill process "; // const-string v7, "Kill process "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.processName;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", pid "; // const-string v7, ", pid "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mProfile;
/* .line 951 */
v7 = (( com.android.server.am.ProcessProfileRecord ) v7 ).getPid ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessProfileRecord;->getPid()I
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", pss "; // const-string v7, ", pss "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.mProfile;
/* move-object/from16 v17, v5 */
(( com.android.server.am.ProcessProfileRecord ) v7 ).getLastPss ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v4 */
(( java.lang.StringBuilder ) v6 ).append ( v4, v5 ); // invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = " for abnormal mem usage."; // const-string v5, " for abnormal mem usage."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 950 */
/* move-object/from16 v5, v17 */
android.util.Slog .w ( v5,v4 );
/* .line 953 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "deap clean "; // const-string v5, "deap clean "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v5 ).getLastPss ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v5 */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v5 = 5; // const/4 v5, 0x5
int v6 = 1; // const/4 v6, 0x1
(( com.android.server.am.ProcessRecord ) v2 ).killLocked ( v4, v5, v6 ); // invoke-virtual {v2, v4, v5, v6}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V
/* .line 954 */
v4 = this.processName;
/* .line 944 */
/* nop */
} // .end local v2 # "app":Lcom/android/server/am/ProcessRecord;
/* add-int/lit8 v1, v1, 0x1 */
/* goto/16 :goto_2 */
/* .line 956 */
} // .end local v0 # "N":I
} // .end local v1 # "i":I
} // :cond_6
} // :goto_3
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
v0 = /* .line 957 */
/* if-lez v0, :cond_7 */
/* .line 958 */
/* new-instance v0, Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
/* .line 959 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
int v5 = -1; // const/4 v5, -0x1
int v6 = 2; // const/4 v6, 0x2
final String v7 = "deap"; // const-string v7, "deap"
/* move-object v1, v0 */
/* move-object/from16 v2, p0 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V */
/* .line 960 */
/* .local v0, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.am.PeriodicCleanerService$CleanInfo ) v0 ).addCleanList ( v1, v12 ); // invoke-virtual {v0, v1, v12}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->addCleanList(ILjava/util/List;)V
/* .line 961 */
/* invoke-direct {v8, v0}, Lcom/android/server/am/PeriodicCleanerService;->addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V */
/* .line 963 */
} // .end local v0 # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
} // :cond_7
final String v0 = "finish deap clean"; // const-string v0, "finish deap clean"
com.android.server.am.PeriodicCleanerService .checkTime ( v9,v10,v0 );
/* .line 964 */
return;
/* .line 956 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit v3 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 920 */
} // .end local v14 # "audioActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v15 # "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* :catchall_1 */
/* move-exception v0 */
try { // :try_start_3
/* monitor-exit v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v0 */
} // .end method
private void init ( ) {
/* .locals 9 */
/* .line 388 */
/* iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 389 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->dependencyCheck()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 390 */
/* iput-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
/* .line 391 */
return;
/* .line 393 */
} // :cond_0
android.os.Process .getTotalMemory ( );
/* move-result-wide v2 */
/* const-wide/32 v4, 0x40000000 */
/* div-long/2addr v2, v4 */
/* const-wide/16 v4, 0x1 */
/* add-long/2addr v2, v4 */
/* long-to-int v0, v2 */
/* .line 394 */
/* .local v0, "totalMemGB":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 395 */
/* .local v2, "index":I */
int v3 = 4; // const/4 v3, 0x4
int v4 = 1; // const/4 v4, 0x1
/* if-le v0, v3, :cond_1 */
/* .line 396 */
/* iput-boolean v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mHighDevice:Z */
/* .line 398 */
/* div-int/lit8 v2, v0, 0x2 */
/* .line 399 */
int v3 = 2; // const/4 v3, 0x2
/* iput v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mFgTrimTheshold:I */
/* .line 402 */
} // :cond_1
/* add-int/lit8 v2, v0, -0x2 */
/* .line 403 */
/* iput v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mFgTrimTheshold:I */
/* .line 406 */
} // :goto_0
int v3 = 6; // const/4 v3, 0x6
/* if-le v2, v3, :cond_2 */
int v2 = 6; // const/4 v2, 0x6
/* .line 407 */
} // :cond_2
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->loadLruLengthConfig(I)Z */
/* if-nez v3, :cond_3 */
/* .line 408 */
/* invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->loadDefLruLengthConfig(I)V */
/* .line 410 */
} // :cond_3
v3 = /* invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->loadCacheLevelConfig(I)Z */
/* if-nez v3, :cond_4 */
/* .line 411 */
/* invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->loadDefCacheLevelConfig(I)V */
/* .line 413 */
} // :cond_4
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->loadDefTimeLevelConfig()V */
/* .line 414 */
v3 = com.android.server.am.PeriodicCleanerService.sOverTimeAppNumArray;
/* aget v3, v3, v2 */
/* iput v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mOverTimeAppNum:I */
/* .line 415 */
v3 = com.android.server.am.PeriodicCleanerService.sGamePlayAppNumArray;
/* aget v3, v3, v2 */
/* iput v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mGamePlayAppNum:I */
/* .line 417 */
final String v3 = "persist.sys.periodic.u.fgtrim"; // const-string v3, "persist.sys.periodic.u.fgtrim"
v3 = android.os.SystemProperties .getBoolean ( v3,v1 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 418 */
v3 = /* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->checkEnableFgTrim()Z */
if ( v3 != null) { // if-eqz v3, :cond_5
/* const/16 v3, 0x8 */
/* if-ge v0, v3, :cond_5 */
/* move v1, v4 */
} // :cond_5
/* nop */
} // :goto_1
/* iput-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableFgTrim:Z */
/* .line 421 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 422 */
/* .local v1, "r":Landroid/content/res/Resources; */
/* nop */
/* .line 423 */
/* const v3, 0x110300b2 */
(( android.content.res.Resources ) v1 ).getStringArray ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 424 */
/* .local v3, "homeOrRecentsList":[Ljava/lang/String; */
/* nop */
/* .line 425 */
/* const v4, 0x110300b3 */
(( android.content.res.Resources ) v1 ).getStringArray ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 426 */
/* .local v4, "systemRelatedPkgList":[Ljava/lang/String; */
/* nop */
/* .line 427 */
/* const v5, 0x110300b0 */
(( android.content.res.Resources ) v1 ).getStringArray ( v5 ); // invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 428 */
/* .local v5, "cleanWhiteList":[Ljava/lang/String; */
/* nop */
/* .line 429 */
/* const v6, 0x110300b1 */
(( android.content.res.Resources ) v1 ).getStringArray ( v6 ); // invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 431 */
/* .local v6, "gameList":[Ljava/lang/String; */
v7 = com.android.server.am.PeriodicCleanerService.sHomeOrRecents;
java.util.Arrays .asList ( v3 );
/* .line 432 */
v7 = com.android.server.am.PeriodicCleanerService.sSystemRelatedPkgs;
java.util.Arrays .asList ( v4 );
/* .line 433 */
v7 = com.android.server.am.PeriodicCleanerService.sCleanWhiteList;
java.util.Arrays .asList ( v5 );
/* .line 434 */
v7 = com.android.server.am.PeriodicCleanerService.sGameApp;
java.util.Arrays .asList ( v6 );
/* .line 436 */
/* sget-boolean v7, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 437 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "HomeOrRecents pkgs: "; // const-string v8, "HomeOrRecents pkgs: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = com.android.server.am.PeriodicCleanerService.sHomeOrRecents;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = "\nSystemRelated pkgs: "; // const-string v8, "\nSystemRelated pkgs: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = com.android.server.am.PeriodicCleanerService.sSystemRelatedPkgs;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = "\nCleanWhiteList pkgs: "; // const-string v8, "\nCleanWhiteList pkgs: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = com.android.server.am.PeriodicCleanerService.sCleanWhiteList;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = "\nGame pkgs: "; // const-string v8, "\nGame pkgs: "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = com.android.server.am.PeriodicCleanerService.sGameApp;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "PeriodicCleaner"; // const-string v8, "PeriodicCleaner"
android.util.Slog .d ( v8,v7 );
/* .line 443 */
} // .end local v0 # "totalMemGB":I
} // .end local v1 # "r":Landroid/content/res/Resources;
} // .end local v2 # "index":I
} // .end local v3 # "homeOrRecentsList":[Ljava/lang/String;
} // .end local v4 # "systemRelatedPkgList":[Ljava/lang/String;
} // .end local v5 # "cleanWhiteList":[Ljava/lang/String;
} // .end local v6 # "gameList":[Ljava/lang/String;
} // :cond_6
return;
} // .end method
private Boolean isActive ( com.android.server.am.ProcessRecord p0, java.util.List p1, java.util.List p2, java.util.List p3 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1516 */
/* .local p2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p3, "activeAudioUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p4, "locationActiveUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v0 = this.info;
v0 = this.packageName;
/* .line 1517 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
final String v2 = "PeriodicCleaner"; // const-string v2, "PeriodicCleaner"
int v3 = 1; // const/4 v3, 0x1
/* if-ltz v1, :cond_4 */
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v1 = java.lang.Integer .valueOf ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1523 */
} // :cond_0
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v1 = java.lang.Integer .valueOf ( v1 );
/* if-nez v1, :cond_2 */
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v1 = java.lang.Integer .valueOf ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1529 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 1524 */
} // :cond_2
} // :goto_0
/* sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1525 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.info;
v4 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is audio or gps active."; // const-string v4, " is audio or gps active."
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 1527 */
} // :cond_3
/* .line 1518 */
} // :cond_4
} // :goto_1
/* sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1519 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.info;
v4 = this.packageName;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is persistent or owning visible window."; // const-string v4, " is persistent or owning visible window."
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 1521 */
} // :cond_5
} // .end method
private Boolean isCameraForegroundCase ( ) {
/* .locals 6 */
/* .line 1437 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mATMS;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getFocusedRootTaskInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1438 */
/* .local v1, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
final String v2 = "PeriodicCleaner"; // const-string v2, "PeriodicCleaner"
if ( v1 != null) { // if-eqz v1, :cond_3
try { // :try_start_1
v3 = this.topActivity;
/* if-nez v3, :cond_0 */
/* .line 1442 */
} // :cond_0
v3 = this.topActivity;
(( android.content.ComponentName ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1443 */
/* .local v3, "packageName":Ljava/lang/String; */
/* sget-boolean v4, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1444 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "current focused package: "; // const-string v5, "current focused package: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v4 );
/* .line 1446 */
} // :cond_1
final String v2 = "com.android.camera"; // const-string v2, "com.android.camera"
v2 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1447 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1451 */
} // .end local v1 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v3 # "packageName":Ljava/lang/String;
} // :cond_2
/* .line 1439 */
/* .restart local v1 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
} // :cond_3
} // :goto_0
final String v3 = "get getFocusedStackInfo error."; // const-string v3, "get getFocusedStackInfo error."
android.util.Slog .e ( v2,v3 );
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 1440 */
/* .line 1449 */
} // .end local v1 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 1452 */
} // :goto_1
} // .end method
private Boolean isDynWhitelist ( java.lang.String p0, java.util.HashMap p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 1533 */
/* .local p2, "dynWhitelist":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
v0 = (( java.util.HashMap ) p2 ).containsKey ( p1 ); // invoke-virtual {p2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1534 */
/* sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1535 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " is dynamic whitelist."; // const-string v1, " is dynamic whitelist."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PeriodicCleaner"; // const-string v1, "PeriodicCleaner"
android.util.Slog .d ( v1,v0 );
/* .line 1537 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 1539 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static Boolean isHomeOrRecents ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 1681 */
v0 = v0 = com.android.server.am.PeriodicCleanerService.sHomeOrRecents;
} // .end method
public static Boolean isHomeOrRecentsToKeepAlive ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 2006 */
v0 = v0 = com.android.server.am.PeriodicCleanerService.sHomeOrRecents;
} // .end method
private Boolean isImportant ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pressure" # I */
/* .line 1547 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
int v1 = 1; // const/4 v1, 0x1
/* if-gtz p2, :cond_1 */
v2 = this.mServices;
v2 = (( com.android.server.am.ProcessServiceRecord ) v2 ).hasForegroundServices ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessServiceRecord;->hasForegroundServices()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1548 */
/* sget-boolean v2, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1549 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.info;
v3 = this.packageName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " has ForegroundService."; // const-string v3, " has ForegroundService."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 1551 */
} // :cond_0
/* .line 1553 */
} // :cond_1
/* if-gt p2, v1, :cond_3 */
v2 = this.info;
v2 = this.packageName;
/* iget v3, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
v2 = miui.process.ProcessManager .isLockedApplication ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 1555 */
/* sget-boolean v2, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 1556 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.info;
v3 = this.packageName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " isLocked."; // const-string v3, " isLocked."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 1558 */
} // :cond_2
/* .line 1560 */
} // :cond_3
v2 = this.mState;
(( com.android.server.am.ProcessStateRecord ) v2 ).getAdjSource ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getAdjSource()Ljava/lang/Object;
/* instance-of v2, v2, Lcom/android/server/am/ProcessRecord; */
if ( v2 != null) { // if-eqz v2, :cond_5
v2 = this.mState;
/* .line 1561 */
(( com.android.server.am.ProcessStateRecord ) v2 ).getAdjSource ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getAdjSource()Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/ProcessRecord; */
/* iget v2, v2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v3, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgUid:I */
/* if-ne v2, v3, :cond_5 */
/* .line 1562 */
/* sget-boolean v2, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 1563 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Last top pkg "; // const-string v3, "Last top pkg "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLastNonSystemFgPkg;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " depend "; // const-string v3, " depend "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v2 );
/* .line 1565 */
} // :cond_4
/* .line 1567 */
} // :cond_5
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isSkipClean ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pressure" # I */
/* .line 1505 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p2, v0, :cond_0 */
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getCurAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* const/16 v1, 0x384 */
/* if-lt v0, v1, :cond_0 */
/* .line 1506 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1508 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private static Boolean isSystemPackage ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 1685 */
v0 = v0 = com.android.server.am.PeriodicCleanerService.sSystemRelatedPkgs;
/* if-nez v0, :cond_1 */
v0 = com.android.server.am.PeriodicCleanerService .isHomeOrRecents ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private static Boolean isWhiteListPackage ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 1689 */
v0 = v0 = com.android.server.am.PeriodicCleanerService.sCleanWhiteList;
} // .end method
private void killTargets ( Integer p0, java.util.ArrayList p1, Integer p2, java.lang.String p3 ) {
/* .locals 7 */
/* .param p1, "userId" # I */
/* .param p3, "killLevel" # I */
/* .param p4, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;I", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1574 */
/* .local p2, "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v0 = (( java.util.ArrayList ) p2 ).size ( ); // invoke-virtual {p2}, Ljava/util/ArrayList;->size()I
/* if-gtz v0, :cond_0 */
/* .line 1575 */
return;
/* .line 1578 */
} // :cond_0
try { // :try_start_0
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1579 */
/* .local v0, "startTime":J */
/* new-instance v2, Landroid/util/ArrayMap; */
/* invoke-direct {v2}, Landroid/util/ArrayMap;-><init>()V */
/* .line 1580 */
/* .local v2, "killList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;" */
java.lang.Integer .valueOf ( p3 );
(( android.util.ArrayMap ) v2 ).put ( v3, p2 ); // invoke-virtual {v2, v3, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 1581 */
/* new-instance v3, Lmiui/process/ProcessConfig; */
/* const/16 v4, 0xa */
/* invoke-direct {v3, v4, p1, v2, p4}, Lmiui/process/ProcessConfig;-><init>(IILandroid/util/ArrayMap;Ljava/lang/String;)V */
/* .line 1583 */
/* .local v3, "config":Lmiui/process/ProcessConfig; */
v4 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v4 ).kill ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/am/ProcessManagerService;->kill(Lmiui/process/ProcessConfig;)Z
/* .line 1584 */
/* sget-boolean v4, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1585 */
final String v4 = "PeriodicCleaner"; // const-string v4, "PeriodicCleaner"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "User"; // const-string v6, "User"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ": Clean "; // const-string v6, ": Clean "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.util.ArrayList ) p2 ).toString ( ); // invoke-virtual {p2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " for "; // const-string v6, " for "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p4 ); // invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 1587 */
} // :cond_1
final String v4 = "finish clean victim packages"; // const-string v4, "finish clean victim packages"
com.android.server.am.PeriodicCleanerService .checkTime ( v0,v1,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1590 */
} // .end local v0 # "startTime":J
} // .end local v2 # "killList":Landroid/util/ArrayMap;, "Landroid/util/ArrayMap<Ljava/lang/Integer;Ljava/util/List<Ljava/lang/String;>;>;"
} // .end local v3 # "config":Lmiui/process/ProcessConfig;
/* .line 1588 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1589 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 1591 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private Boolean loadCacheLevelConfig ( Integer p0 ) {
/* .locals 10 */
/* .param p1, "memIndex" # I */
/* .line 539 */
final String v0 = "persist.sys.periodic.cache_level_config"; // const-string v0, "persist.sys.periodic.cache_level_config"
android.os.SystemProperties .get ( v0 );
/* .line 540 */
/* .local v0, "value":Ljava/lang/String; */
final String v1 = ":"; // const-string v1, ":"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 541 */
/* .local v1, "memArray":[Ljava/lang/String; */
/* array-length v2, v1 */
int v3 = 7; // const/4 v3, 0x7
int v4 = 0; // const/4 v4, 0x0
/* if-ne v2, v3, :cond_4 */
/* aget-object v2, v1, p1 */
v2 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* if-nez v2, :cond_0 */
/* .line 545 */
} // :cond_0
/* aget-object v2, v1, p1 */
final String v3 = ","; // const-string v3, ","
(( java.lang.String ) v2 ).split ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 546 */
/* .local v2, "pressureArray":[Ljava/lang/String; */
/* array-length v3, v2 */
int v5 = 3; // const/4 v5, 0x3
/* if-eq v3, v5, :cond_1 */
/* .line 547 */
/* .line 550 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .line 551 */
/* .local v3, "cache":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 552 */
/* .local v5, "index":I */
/* array-length v6, v2 */
/* move v7, v4 */
} // :goto_0
/* if-ge v7, v6, :cond_3 */
/* aget-object v8, v2, v7 */
/* .line 553 */
/* .local v8, "str":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 555 */
try { // :try_start_0
v9 = java.lang.Integer .parseInt ( v8 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v3, v9 */
/* .line 558 */
/* nop */
/* .line 559 */
/* if-gtz v3, :cond_2 */
/* .line 560 */
} // :cond_2
v9 = this.mPressureCacheThreshold;
/* aput v3, v9, v5 */
/* .line 561 */
/* nop */
} // .end local v8 # "str":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 552 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 556 */
/* .restart local v8 # "str":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v6 */
/* .line 557 */
/* .local v6, "e":Ljava/lang/Exception; */
/* .line 563 */
} // .end local v6 # "e":Ljava/lang/Exception;
} // .end local v8 # "str":Ljava/lang/String;
} // :cond_3
int v4 = 1; // const/4 v4, 0x1
/* .line 542 */
} // .end local v2 # "pressureArray":[Ljava/lang/String;
} // .end local v3 # "cache":I
} // .end local v5 # "index":I
} // :cond_4
} // :goto_1
} // .end method
private void loadDefCacheLevelConfig ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "memIndex" # I */
/* .line 574 */
v0 = this.mPressureCacheThreshold;
v1 = com.android.server.am.PeriodicCleanerService.sDefaultCacheLevel;
/* aget-object v1, v1, p1 */
int v2 = 0; // const/4 v2, 0x0
/* aget v3, v1, v2 */
/* aput v3, v0, v2 */
/* .line 575 */
int v2 = 1; // const/4 v2, 0x1
/* aget v3, v1, v2 */
/* aput v3, v0, v2 */
/* .line 576 */
int v2 = 2; // const/4 v2, 0x2
/* aget v1, v1, v2 */
/* aput v1, v0, v2 */
/* .line 577 */
return;
} // .end method
private void loadDefLruLengthConfig ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "memIndex" # I */
/* .line 567 */
v0 = com.android.server.am.PeriodicCleanerService.sDefaultActiveLength;
/* aget-object v0, v0, p1 */
int v1 = 0; // const/4 v1, 0x0
/* aget v2, v0, v1 */
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
/* .line 568 */
v3 = this.mPressureAgingThreshold;
/* aput v2, v3, v1 */
/* .line 569 */
int v1 = 1; // const/4 v1, 0x1
/* aget v2, v0, v1 */
/* aput v2, v3, v1 */
/* .line 570 */
int v1 = 2; // const/4 v1, 0x2
/* aget v0, v0, v1 */
/* aput v0, v3, v1 */
/* .line 571 */
return;
} // .end method
private void loadDefTimeLevelConfig ( ) {
/* .locals 4 */
/* .line 580 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateTimeLevel()V */
/* .line 581 */
v0 = this.mPressureTimeThreshold;
v1 = com.android.server.am.PeriodicCleanerService.sDefaultTimeLevel;
int v2 = 0; // const/4 v2, 0x0
/* aget v3, v1, v2 */
/* aput v3, v0, v2 */
/* .line 582 */
int v2 = 1; // const/4 v2, 0x1
/* aget v3, v1, v2 */
/* aput v3, v0, v2 */
/* .line 583 */
int v2 = 2; // const/4 v2, 0x2
/* aget v1, v1, v2 */
/* aput v1, v0, v2 */
/* .line 584 */
return;
} // .end method
private Boolean loadLruLengthConfig ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "memIndex" # I */
/* .line 514 */
final String v0 = "persist.sys.periodic.lru_active_length"; // const-string v0, "persist.sys.periodic.lru_active_length"
android.os.SystemProperties .get ( v0 );
/* .line 515 */
/* .local v0, "value":Ljava/lang/String; */
final String v1 = ":"; // const-string v1, ":"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 516 */
/* .local v1, "memArray":[Ljava/lang/String; */
/* array-length v2, v1 */
int v3 = 7; // const/4 v3, 0x7
int v4 = 0; // const/4 v4, 0x0
/* if-eq v2, v3, :cond_0 */
/* .line 517 */
/* .line 520 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 522 */
/* .local v2, "len":I */
try { // :try_start_0
/* aget-object v3, v1, p1 */
v3 = java.lang.Integer .parseInt ( v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v2, v3 */
/* .line 525 */
/* nop */
/* .line 526 */
/* if-gtz v2, :cond_1 */
/* .line 527 */
/* .line 530 */
} // :cond_1
/* iput v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
/* .line 531 */
v3 = this.mPressureAgingThreshold;
v5 = /* invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->finalLruActiveLength(I)I */
/* aput v5, v3, v4 */
/* .line 532 */
v3 = this.mPressureAgingThreshold;
/* iget v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
/* mul-int/lit8 v4, v4, 0x3 */
/* div-int/lit8 v4, v4, 0x4 */
v4 = /* invoke-direct {p0, v4}, Lcom/android/server/am/PeriodicCleanerService;->finalLruActiveLength(I)I */
int v5 = 1; // const/4 v5, 0x1
/* aput v4, v3, v5 */
/* .line 533 */
v3 = this.mPressureAgingThreshold;
/* iget v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
int v6 = 2; // const/4 v6, 0x2
/* div-int/2addr v4, v6 */
v4 = /* invoke-direct {p0, v4}, Lcom/android/server/am/PeriodicCleanerService;->finalLruActiveLength(I)I */
/* aput v4, v3, v6 */
/* .line 534 */
/* .line 523 */
/* :catch_0 */
/* move-exception v3 */
/* .line 524 */
/* .local v3, "e":Ljava/lang/Exception; */
} // .end method
private com.android.server.am.PeriodicCleanerService$PackageUseInfo obtainPackageUseInfo ( Integer p0, java.lang.String p1, Long p2 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "backgroundTime" # J */
/* .line 1036 */
/* new-instance v6, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* move-object v0, v6 */
/* move-object v1, p0 */
/* move v2, p1 */
/* move-object v3, p2 */
/* move-wide v4, p3 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;ILjava/lang/String;J)V */
} // .end method
private void onActivityManagerReady ( ) {
/* .locals 2 */
/* .line 251 */
final String v0 = "ProcessManager"; // const-string v0, "ProcessManager"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/am/ProcessManagerService; */
this.mPMS = v0;
/* .line 252 */
final String v0 = "activity"; // const-string v0, "activity"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
this.mAMS = v0;
/* .line 253 */
android.app.ActivityTaskManager .getService ( );
/* check-cast v0, Lcom/android/server/wm/ActivityTaskManagerService; */
this.mATMS = v0;
/* .line 254 */
/* const-class v0, Lcom/android/server/wm/WindowManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerInternal; */
this.mWMS = v0;
/* .line 255 */
final String v0 = "package"; // const-string v0, "package"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl; */
this.mPKMS = v0;
/* .line 256 */
final String v0 = "audio"; // const-string v0, "audio"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/audio/AudioService; */
this.mAudioService = v0;
/* .line 257 */
/* nop */
/* .line 258 */
final String v0 = "procstats"; // const-string v0, "procstats"
android.os.ServiceManager .getService ( v0 );
/* .line 257 */
com.android.internal.app.procstats.IProcessStats$Stub .asInterface ( v0 );
this.mProcessStats = v0;
/* .line 259 */
v0 = this.mContext;
/* const-string/jumbo v1, "usagestats" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/usage/UsageStatsManager; */
this.mUSM = v0;
/* .line 260 */
v1 = this.mPMS;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mAMS;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mATMS;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mWMS;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mProcessStats;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mPKMS;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.mAudioService;
if ( v1 != null) { // if-eqz v1, :cond_0
/* if-nez v0, :cond_1 */
/* .line 262 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
/* .line 263 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
final String v1 = "disable periodic for dependencies service not available"; // const-string v1, "disable periodic for dependencies service not available"
android.util.Slog .w ( v0,v1 );
/* .line 271 */
} // :cond_1
return;
} // .end method
private void onBootComplete ( ) {
/* .locals 7 */
/* .line 274 */
/* iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 275 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->registerCloudObserver(Landroid/content/Context;)V */
/* .line 276 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_periodic_enable"; // const-string v1, "cloud_periodic_enable"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
final String v3 = "PeriodicCleaner"; // const-string v3, "PeriodicCleaner"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 278 */
v0 = this.mContext;
/* .line 280 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 279 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 278 */
v0 = java.lang.Boolean .parseBoolean ( v0 );
/* iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
/* .line 283 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set enable state from database: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v0 );
/* .line 285 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnable:Z */
/* iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z */
/* .line 288 */
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->registerCloudWhiteListObserver(Landroid/content/Context;)V */
/* .line 289 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_periodic_white_list"; // const-string v1, "cloud_periodic_white_list"
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 291 */
v0 = this.mContext;
/* .line 292 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 291 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 295 */
/* .local v0, "whiteListStr":Ljava/lang/String; */
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 296 */
/* .local v1, "whiteList":[Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v1 */
/* if-ge v4, v5, :cond_2 */
/* .line 297 */
v5 = com.android.server.am.PeriodicCleanerService.sCleanWhiteList;
v5 = /* aget-object v6, v1, v4 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 298 */
} // :cond_1
v5 = com.android.server.am.PeriodicCleanerService.sCleanWhiteList;
/* aget-object v6, v1, v4 */
/* .line 296 */
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 300 */
} // .end local v4 # "i":I
} // :cond_2
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "set white list from database, current list: " */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = com.android.server.am.PeriodicCleanerService.sCleanWhiteList;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* .line 304 */
} // .end local v0 # "whiteListStr":Ljava/lang/String;
} // .end local v1 # "whiteList":[Ljava/lang/String;
} // :cond_3
v0 = this.mContext;
/* invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->registerCloudGameObserver(Landroid/content/Context;)V */
/* .line 305 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_periodic_game_enable"; // const-string v1, "cloud_periodic_game_enable"
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 307 */
v0 = this.mContext;
/* .line 309 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 308 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 307 */
v0 = java.lang.Boolean .parseBoolean ( v0 );
/* iput-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z */
/* .line 312 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set game enable state from database: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v0 );
/* .line 315 */
} // :cond_4
/* iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mHighDevice:Z */
/* if-nez v0, :cond_5 */
/* .line 316 */
v0 = this.mHandler;
/* const/16 v1, 0x13 */
final String v2 = "cch-empty"; // const-string v2, "cch-empty"
int v3 = 6; // const/4 v3, 0x6
/* const/16 v4, 0x384 */
(( com.android.server.am.PeriodicCleanerService$MyHandler ) v0 ).obtainMessage ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 318 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0x2710 */
(( com.android.server.am.PeriodicCleanerService$MyHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/PeriodicCleanerService$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 320 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_5
return;
} // .end method
private java.lang.String pressureToString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pressure" # I */
/* .line 1660 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1661 */
/* .local v0, "ret":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* .line 1675 */
/* const-string/jumbo v0, "unkown" */
/* .line 1672 */
/* :pswitch_0 */
final String v0 = "critical"; // const-string v0, "critical"
/* .line 1673 */
/* .line 1669 */
/* :pswitch_1 */
final String v0 = "min"; // const-string v0, "min"
/* .line 1670 */
/* .line 1666 */
/* :pswitch_2 */
final String v0 = "low"; // const-string v0, "low"
/* .line 1667 */
/* .line 1663 */
/* :pswitch_3 */
final String v0 = "normal"; // const-string v0, "normal"
/* .line 1664 */
/* nop */
/* .line 1677 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch -0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void reCheckPressureAndClean ( ) {
/* .locals 2 */
/* .line 1093 */
v0 = /* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getMemPressureLevel()I */
/* .line 1094 */
/* .local v0, "pressure":I */
int v1 = -1; // const/4 v1, -0x1
/* if-eq v0, v1, :cond_0 */
/* .line 1095 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByTime(I)V */
/* .line 1097 */
} // :cond_0
return;
} // .end method
private void registerCloudGameObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 371 */
/* new-instance v0, Lcom/android/server/am/PeriodicCleanerService$3; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/PeriodicCleanerService$3;-><init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 382 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 383 */
final String v2 = "cloud_periodic_game_enable"; // const-string v2, "cloud_periodic_game_enable"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 382 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 385 */
return;
} // .end method
private void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 331 */
/* new-instance v0, Lcom/android/server/am/PeriodicCleanerService$1; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/PeriodicCleanerService$1;-><init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 343 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 344 */
final String v2 = "cloud_periodic_enable"; // const-string v2, "cloud_periodic_enable"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 343 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 346 */
return;
} // .end method
private void registerCloudWhiteListObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 349 */
/* new-instance v0, Lcom/android/server/am/PeriodicCleanerService$2; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/PeriodicCleanerService$2;-><init>(Lcom/android/server/am/PeriodicCleanerService;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 365 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 366 */
final String v2 = "cloud_periodic_white_list"; // const-string v2, "cloud_periodic_white_list"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 365 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 368 */
return;
} // .end method
private void reportCleanProcess ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 11 */
/* .param p1, "minAdj" # I */
/* .param p2, "procState" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 653 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "PeriodicCleaner("; // const-string v1, "PeriodicCleaner("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 654 */
/* .local v0, "longReason":Ljava/lang/String; */
/* new-instance v1, Landroid/util/SparseArray; */
/* invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V */
/* .line 655 */
/* .local v1, "killedArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/ArrayList<Ljava/lang/String;>;>;" */
v2 = this.mAMS;
/* monitor-enter v2 */
/* .line 656 */
try { // :try_start_0
v3 = this.mAMS;
v3 = this.mProcessList;
v3 = (( com.android.server.am.ProcessList ) v3 ).getLruSizeLOSP ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I
/* .line 657 */
/* .local v3, "N":I */
/* add-int/lit8 v4, v3, -0x2 */
/* .local v4, "i":I */
} // :goto_0
/* if-ltz v4, :cond_3 */
/* .line 658 */
v5 = this.mAMS;
v5 = this.mProcessList;
(( com.android.server.am.ProcessList ) v5 ).getLruProcessesLOSP ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
(( java.util.ArrayList ) v5 ).get ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, Lcom/android/server/am/ProcessRecord; */
/* .line 659 */
/* .local v5, "app":Lcom/android/server/am/ProcessRecord; */
v6 = (( com.android.server.am.ProcessRecord ) v5 ).isKilledByAm ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v6, :cond_2 */
(( com.android.server.am.ProcessRecord ) v5 ).getThread ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v6 != null) { // if-eqz v6, :cond_2
/* iget-boolean v6, v5, Lcom/android/server/am/ProcessRecord;->isolated:Z */
/* if-nez v6, :cond_2 */
/* .line 660 */
v6 = (( com.android.server.am.ProcessRecord ) v5 ).hasActivities ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessRecord;->hasActivities()Z
/* if-nez v6, :cond_2 */
v6 = (( com.android.server.am.PeriodicCleanerService ) p0 ).isSystemRelated ( v5 ); // invoke-virtual {p0, v5}, Lcom/android/server/am/PeriodicCleanerService;->isSystemRelated(Lcom/android/server/am/ProcessRecord;)Z
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 661 */
/* .line 663 */
} // :cond_0
v6 = this.mState;
/* .line 664 */
/* .local v6, "state":Lcom/android/server/am/ProcessStateRecord; */
v7 = (( com.android.server.am.ProcessStateRecord ) v6 ).getCurAdj ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* if-lt v7, p1, :cond_2 */
v7 = (( com.android.server.am.ProcessStateRecord ) v6 ).getCurProcState ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
/* if-lt v7, p2, :cond_2 */
/* .line 665 */
/* const/16 v7, 0xd */
int v8 = 1; // const/4 v8, 0x1
(( com.android.server.am.ProcessRecord ) v5 ).killLocked ( v0, v7, v8 ); // invoke-virtual {v5, v0, v7, v8}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V
/* .line 666 */
/* iget v7, v5, Lcom/android/server/am/ProcessRecord;->userId:I */
(( android.util.SparseArray ) v1 ).get ( v7 ); // invoke-virtual {v1, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v7, Ljava/util/ArrayList; */
/* .line 667 */
/* .local v7, "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* if-nez v7, :cond_1 */
/* .line 668 */
/* new-instance v8, Ljava/util/ArrayList; */
/* invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V */
/* move-object v7, v8 */
/* .line 669 */
/* iget v8, v5, Lcom/android/server/am/ProcessRecord;->userId:I */
(( android.util.SparseArray ) v1 ).put ( v8, v7 ); // invoke-virtual {v1, v8, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 671 */
} // :cond_1
v8 = this.processName;
(( java.util.ArrayList ) v7 ).add ( v8 ); // invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 657 */
} // .end local v5 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v6 # "state":Lcom/android/server/am/ProcessStateRecord;
} // .end local v7 # "userTargets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, -0x1 */
/* .line 674 */
} // .end local v3 # "N":I
} // .end local v4 # "i":I
} // :cond_3
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 675 */
/* new-instance v2, Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
int v8 = -1; // const/4 v8, -0x1
int v9 = -1; // const/4 v9, -0x1
/* move-object v4, v2 */
/* move-object v5, p0 */
/* move-object v10, p3 */
/* invoke-direct/range {v4 ..v10}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;-><init>(Lcom/android/server/am/PeriodicCleanerService;JIILjava/lang/String;)V */
/* .line 676 */
/* .local v2, "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_2
v4 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v3, v4, :cond_4 */
/* .line 677 */
v4 = (( android.util.SparseArray ) v1 ).keyAt ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/SparseArray;->keyAt(I)I
(( android.util.SparseArray ) v1 ).valueAt ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Ljava/util/List; */
(( com.android.server.am.PeriodicCleanerService$CleanInfo ) v2 ).addCleanList ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Lcom/android/server/am/PeriodicCleanerService$CleanInfo;->addCleanList(ILjava/util/List;)V
/* .line 676 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 679 */
} // .end local v3 # "i":I
} // :cond_4
/* invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->addCleanHistory(Lcom/android/server/am/PeriodicCleanerService$CleanInfo;)V */
/* .line 680 */
return;
/* .line 674 */
} // .end local v2 # "cInfo":Lcom/android/server/am/PeriodicCleanerService$CleanInfo;
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
} // .end method
private void reportEvent ( com.android.server.am.PeriodicCleanerService$MyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Lcom/android/server/am/PeriodicCleanerService$MyEvent; */
/* .line 622 */
v0 = this.mPackage;
/* .line 623 */
/* .local v0, "packageName":Ljava/lang/String; */
/* iget-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
if ( v0 != null) { // if-eqz v0, :cond_6
/* iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I */
/* const/16 v2, 0x3e8 */
/* if-eq v1, v2, :cond_6 */
/* .line 625 */
v1 = com.android.server.am.PeriodicCleanerService .isSystemPackage ( v0 );
/* if-nez v1, :cond_6 */
/* iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mEventType:I */
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_0 */
/* .line 629 */
} // :cond_0
/* iget-boolean v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mFullScreen:Z */
/* if-nez v1, :cond_2 */
/* .line 630 */
/* sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 631 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/"; // const-string v2, "/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mClass;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " isn\'t fullscreen, skip."; // const-string v2, " isn\'t fullscreen, skip."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "PeriodicCleaner"; // const-string v2, "PeriodicCleaner"
android.util.Slog .d ( v2,v1 );
/* .line 633 */
} // :cond_1
return;
/* .line 635 */
} // :cond_2
v1 = this.mLastNonSystemFgPkg;
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I */
/* iget v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgUid:I */
/* if-eq v1, v2, :cond_5 */
/* .line 636 */
} // :cond_3
this.mLastNonSystemFgPkg = v0;
/* .line 637 */
/* iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I */
/* iput v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastNonSystemFgUid:I */
/* .line 638 */
/* iget v1, p1, Lcom/android/server/am/PeriodicCleanerService$MyEvent;->mUid:I */
v1 = /* invoke-direct {p0, v1, v0}, Lcom/android/server/am/PeriodicCleanerService;->updateLruPackageLocked(ILjava/lang/String;)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* iget-boolean v1, p0, Lcom/android/server/am/PeriodicCleanerService;->mHighDevice:Z */
/* if-nez v1, :cond_4 */
/* .line 639 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByPeriodic()V */
/* .line 642 */
} // :cond_4
final String v1 = "pressure"; // const-string v1, "pressure"
/* invoke-direct {p0, v1}, Lcom/android/server/am/PeriodicCleanerService;->checkPressureAndClean(Ljava/lang/String;)V */
/* .line 649 */
} // :cond_5
} // :goto_0
return;
/* .line 627 */
} // :cond_6
} // :goto_1
return;
} // .end method
private void reportMemPressure ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "pressureState" # I */
/* .line 684 */
/* iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 3; // const/4 v0, 0x3
/* if-ne p1, v0, :cond_1 */
/* .line 685 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 686 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastCleanByPressure:J */
/* sub-long v2, v0, v2 */
/* const-wide/16 v4, 0x2710 */
/* cmp-long v2, v2, v4 */
/* if-lez v2, :cond_1 */
/* .line 687 */
/* sget-boolean v2, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 688 */
final String v2 = "PeriodicCleaner"; // const-string v2, "PeriodicCleaner"
/* const-string/jumbo v3, "try to clean for MEM_PRESSURE_HIGH." */
android.util.Slog .d ( v2,v3 );
/* .line 690 */
} // :cond_0
final String v2 = "pressure"; // const-string v2, "pressure"
/* invoke-direct {p0, v2}, Lcom/android/server/am/PeriodicCleanerService;->checkPressureAndClean(Ljava/lang/String;)V */
/* .line 693 */
} // .end local v0 # "now":J
} // :cond_1
return;
} // .end method
private void reportStartProcess ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "processInfo" # Ljava/lang/String; */
/* .line 696 */
/* iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mReady:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* iget-boolean v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableStartProcess:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 697 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 698 */
/* .local v0, "now":J */
final String v2 = ":"; // const-string v2, ":"
v2 = (( java.lang.String ) p1 ).lastIndexOf ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
/* .line 699 */
/* .local v2, "lastIndex":I */
/* add-int/lit8 v3, v2, 0x1 */
(( java.lang.String ) p1 ).substring ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 700 */
/* .local v3, "hostType":Ljava/lang/String; */
final String v4 = "activity"; // const-string v4, "activity"
v4 = (( java.lang.String ) v3 ).contains ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 701 */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.String ) p1 ).substring ( v4, v2 ); // invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
/* .line 702 */
/* .local v4, "processName":Ljava/lang/String; */
/* iget-boolean v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mEnableGameClean:Z */
final String v6 = "PeriodicCleaner"; // const-string v6, "PeriodicCleaner"
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = v5 = com.android.server.am.PeriodicCleanerService.sGameApp;
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 703 */
/* sget-boolean v5, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 704 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "reportStartGame "; // const-string v7, "reportStartGame "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v5 );
/* .line 706 */
} // :cond_0
/* iget v5, p0, Lcom/android/server/am/PeriodicCleanerService;->mGamePlayAppNum:I */
/* invoke-direct {p0, v5, v4}, Lcom/android/server/am/PeriodicCleanerService;->cleanPackageByGamePlay(ILjava/lang/String;)V */
/* .line 707 */
} // :cond_1
v5 = v5 = com.android.server.am.PeriodicCleanerService.sHighFrequencyApp;
/* if-nez v5, :cond_2 */
v5 = com.android.server.am.PeriodicCleanerService.sHighMemoryApp;
v5 = /* .line 708 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 709 */
} // :cond_2
/* sget-boolean v5, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 710 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "reportStartProcess "; // const-string v7, "reportStartProcess "
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v5 );
/* .line 712 */
} // :cond_3
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "pressure:startprocess:"; // const-string v6, "pressure:startprocess:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 713 */
/* .local v5, "reason":Ljava/lang/String; */
/* invoke-direct {p0, v5}, Lcom/android/server/am/PeriodicCleanerService;->checkPressureAndClean(Ljava/lang/String;)V */
/* .line 716 */
} // .end local v4 # "processName":Ljava/lang/String;
} // .end local v5 # "reason":Ljava/lang/String;
} // :cond_4
} // :goto_0
/* iget-wide v4, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastUpdateTime:J */
/* sub-long v4, v0, v4 */
/* const-wide/32 v6, 0x2932e00 */
/* cmp-long v4, v4, v6 */
/* if-ltz v4, :cond_5 */
/* .line 717 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateProcStatsList()V */
/* .line 718 */
/* iput-wide v0, p0, Lcom/android/server/am/PeriodicCleanerService;->mLastUpdateTime:J */
/* .line 721 */
} // .end local v0 # "now":J
} // .end local v2 # "lastIndex":I
} // .end local v3 # "hostType":Ljava/lang/String;
} // :cond_5
return;
} // .end method
private void updateHighFrequencyAppList ( ) {
/* .locals 7 */
/* .line 852 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getPackageForegroundTime()Ljava/util/HashMap; */
/* .line 853 */
/* .local v0, "packageForegroundTime":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* .line 854 */
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 856 */
/* .local v1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;>;" */
/* new-instance v2, Lcom/android/server/am/PeriodicCleanerService$5; */
/* invoke-direct {v2, p0}, Lcom/android/server/am/PeriodicCleanerService$5;-><init>(Lcom/android/server/am/PeriodicCleanerService;)V */
v2 = /* .line 863 */
/* .line 865 */
/* .local v2, "maxSize":I */
int v3 = 5; // const/4 v3, 0x5
/* if-le v2, v3, :cond_0 */
} // :cond_0
/* move v3, v2 */
} // :goto_0
/* move v2, v3 */
/* .line 866 */
v3 = com.android.server.am.PeriodicCleanerService.sHighFrequencyApp;
/* .line 867 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_1
/* if-ge v3, v2, :cond_2 */
/* .line 868 */
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 869 */
/* .local v4, "packageName":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;" */
v5 = com.android.server.am.PeriodicCleanerService.sHighFrequencyApp;
/* check-cast v6, Ljava/lang/String; */
/* .line 870 */
/* sget-boolean v5, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 871 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "update highFrequencyApp: " */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ":"; // const-string v6, ":"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 872 */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 871 */
final String v6 = "PeriodicCleaner"; // const-string v6, "PeriodicCleaner"
android.util.Slog .d ( v6,v5 );
/* .line 867 */
} // .end local v4 # "packageName":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 875 */
} // .end local v3 # "i":I
} // :cond_2
return;
} // .end method
private void updateHighMemoryAppList ( ) {
/* .locals 13 */
/* .line 730 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 731 */
/* .local v0, "startTime":J */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->getAllPackagePss()Ljava/util/HashMap; */
/* .line 732 */
/* .local v2, "packagePss":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;" */
final String v3 = "finish get all package pss."; // const-string v3, "finish get all package pss."
com.android.server.am.PeriodicCleanerService .checkTime ( v0,v1,v3 );
/* .line 733 */
/* if-nez v2, :cond_0 */
return;
/* .line 734 */
} // :cond_0
/* new-instance v3, Ljava/util/ArrayList; */
/* .line 735 */
(( java.util.HashMap ) v2 ).entrySet ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
/* invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 737 */
/* .local v3, "listPkgPss":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;>;" */
/* new-instance v4, Lcom/android/server/am/PeriodicCleanerService$4; */
/* invoke-direct {v4, p0}, Lcom/android/server/am/PeriodicCleanerService$4;-><init>(Lcom/android/server/am/PeriodicCleanerService;)V */
/* .line 743 */
int v4 = 0; // const/4 v4, 0x0
/* .line 744 */
/* .local v4, "index":I */
int v5 = 1; // const/4 v5, 0x1
/* .line 746 */
/* .local v5, "isNeedClear":Z */
v7 = } // :goto_0
final String v8 = "PeriodicCleaner"; // const-string v8, "PeriodicCleaner"
if ( v7 != null) { // if-eqz v7, :cond_3
/* check-cast v7, Ljava/util/Map$Entry; */
/* .line 747 */
/* .local v7, "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;" */
/* check-cast v9, Ljava/lang/Long; */
(( java.lang.Long ) v9 ).longValue ( ); // invoke-virtual {v9}, Ljava/lang/Long;->longValue()J
/* move-result-wide v9 */
/* const-wide/32 v11, 0x64000 */
/* cmp-long v9, v9, v11 */
/* if-ltz v9, :cond_3 */
int v9 = 5; // const/4 v9, 0x5
/* if-ge v4, v9, :cond_3 */
/* .line 748 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 749 */
v9 = com.android.server.am.PeriodicCleanerService.sHighMemoryApp;
/* .line 750 */
int v5 = 0; // const/4 v5, 0x0
/* .line 752 */
} // :cond_1
/* sget-boolean v9, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v9 != null) { // if-eqz v9, :cond_2
/* .line 753 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "update highMemoryApp: " */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v10, Ljava/lang/String; */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ":"; // const-string v10, ":"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 754 */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 753 */
android.util.Slog .d ( v8,v9 );
/* .line 756 */
} // :cond_2
v8 = com.android.server.am.PeriodicCleanerService.sHighMemoryApp;
/* check-cast v9, Ljava/lang/String; */
/* .line 759 */
/* nop */
} // .end local v7 # "mapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
/* add-int/lit8 v4, v4, 0x1 */
/* .line 760 */
/* .line 761 */
} // :cond_3
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "highmemoryapp:"; // const-string v7, "highmemoryapp:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = com.android.server.am.PeriodicCleanerService.sHighMemoryApp;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v6 );
/* .line 762 */
return;
} // .end method
private Boolean updateLruPackageLocked ( Integer p0, java.lang.String p1 ) {
/* .locals 16 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 989 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v3, p2 */
int v4 = -1; // const/4 v4, -0x1
/* .line 990 */
/* .local v4, "newFgIndex":I */
v5 = /* invoke-static/range {p1 ..p1}, Landroid/os/UserHandle;->getUserId(I)I */
/* .line 991 */
/* .local v5, "userId":I */
int v6 = 0; // const/4 v6, 0x0
/* .line 992 */
/* .local v6, "N":I */
int v7 = 0; // const/4 v7, 0x0
/* .line 993 */
/* .local v7, "info":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
int v8 = 0; // const/4 v8, 0x0
/* .line 994 */
/* .local v8, "newInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
int v9 = 0; // const/4 v9, 0x0
/* .line 995 */
/* .local v9, "modifyInfo":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v10 */
/* .line 996 */
/* .local v10, "backgroundTime":J */
v12 = this.mLock;
/* monitor-enter v12 */
/* .line 997 */
try { // :try_start_0
v0 = this.mLruPackages;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* move v6, v0 */
/* .line 999 */
/* add-int/lit8 v0, v6, -0x2 */
/* .local v0, "i":I */
} // :goto_0
/* const-wide/16 v14, 0x0 */
/* if-ltz v0, :cond_1 */
/* .line 1000 */
v13 = this.mLruPackages;
(( java.util.ArrayList ) v13 ).get ( v0 ); // invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v13, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* move-object v7, v13 */
/* .line 1002 */
v13 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUserId ( v7 );
/* if-ne v13, v5, :cond_0 */
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmPackageName ( v7 );
v13 = (( java.lang.String ) v13 ).equals ( v3 ); // invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_0
/* .line 1003 */
v13 = this.mLruPackages;
(( java.util.ArrayList ) v13 ).remove ( v7 ); // invoke-virtual {v13, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
/* .line 1004 */
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fputmBackgroundTime ( v7,v14,v15 );
/* .line 1005 */
int v13 = 0; // const/4 v13, 0x0
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fputmFgTrimDone ( v7,v13 );
/* .line 1006 */
v13 = this.mLruPackages;
(( java.util.ArrayList ) v13 ).add ( v7 ); // invoke-virtual {v13, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1007 */
/* move v4, v0 */
/* .line 1010 */
v13 = this.mLruPackages;
/* add-int/lit8 v14, v6, -0x2 */
(( java.util.ArrayList ) v13 ).get ( v14 ); // invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v13, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* move-object v9, v13 */
/* .line 1011 */
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fputmBackgroundTime ( v9,v10,v11 );
/* .line 1012 */
/* .line 999 */
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 1015 */
} // .end local v0 # "i":I
} // :cond_1
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
int v13 = -1; // const/4 v13, -0x1
/* if-ne v4, v13, :cond_3 */
/* .line 1016 */
/* const-wide/16 v13, 0x0 */
/* invoke-direct {v1, v2, v3, v13, v14}, Lcom/android/server/am/PeriodicCleanerService;->obtainPackageUseInfo(ILjava/lang/String;J)Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* move-object v8, v13 */
/* .line 1017 */
v13 = this.mLruPackages;
(( java.util.ArrayList ) v13 ).add ( v8 ); // invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 1018 */
v13 = this.mLruPackages;
v13 = (( java.util.ArrayList ) v13 ).size ( ); // invoke-virtual {v13}, Ljava/util/ArrayList;->size()I
/* move v6, v13 */
/* .line 1019 */
int v13 = 2; // const/4 v13, 0x2
/* if-lt v6, v13, :cond_2 */
/* .line 1020 */
v13 = this.mLruPackages;
/* add-int/lit8 v14, v6, -0x2 */
(( java.util.ArrayList ) v13 ).get ( v14 ); // invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v13, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* move-object v9, v13 */
/* .line 1021 */
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fputmBackgroundTime ( v9,v10,v11 );
/* .line 1023 */
} // :cond_2
/* monitor-exit v12 */
/* .line 1025 */
} // :cond_3
/* monitor-exit v12 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1026 */
if ( v7 != null) { // if-eqz v7, :cond_4
v12 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUid ( v7 );
/* if-eq v12, v2, :cond_4 */
/* .line 1027 */
final String v12 = "PeriodicCleaner"; // const-string v12, "PeriodicCleaner"
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v13 ).append ( v3 ); // invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = " re-installed, NewAppId="; // const-string v14, " re-installed, NewAppId="
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1028 */
v14 = /* invoke-static/range {p1 ..p1}, Landroid/os/UserHandle;->getAppId(I)I */
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v14 = ", OldAppId="; // const-string v14, ", OldAppId="
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v14 = com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmUid ( v7 );
/* .line 1029 */
v14 = android.os.UserHandle .getAppId ( v14 );
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1027 */
android.util.Slog .d ( v12,v13 );
/* .line 1030 */
(( com.android.server.am.PeriodicCleanerService$PackageUseInfo ) v7 ).updateUid ( v2 ); // invoke-virtual {v7, v2}, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;->updateUid(I)V
/* .line 1032 */
} // :cond_4
/* sub-int v12, v6, v4 */
/* iget v13, v1, Lcom/android/server/am/PeriodicCleanerService;->mLruActiveLength:I */
/* if-le v12, v13, :cond_5 */
/* move v13, v0 */
} // :cond_5
int v13 = 0; // const/4 v13, 0x0
} // :goto_2
/* .line 1025 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v12 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
private void updateProcStatsList ( ) {
/* .locals 2 */
/* .line 724 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateHighMemoryAppList()V */
/* .line 725 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->updateHighFrequencyAppList()V */
/* .line 726 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
/* const-string/jumbo v1, "update high memory and frequency app list success" */
android.util.Slog .d ( v0,v1 );
/* .line 727 */
return;
} // .end method
private void updateTimeLevel ( ) {
/* .locals 11 */
/* .line 587 */
final String v0 = "persist.sys.periodic.time_threshold"; // const-string v0, "persist.sys.periodic.time_threshold"
int v1 = 0; // const/4 v1, 0x0
android.os.SystemProperties .get ( v0,v1 );
/* .line 588 */
/* .local v0, "prop":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
return;
/* .line 589 */
} // :cond_0
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 590 */
/* .local v1, "timeLevelString":[Ljava/lang/String; */
/* array-length v2, v1 */
int v3 = 3; // const/4 v3, 0x3
/* if-eq v2, v3, :cond_1 */
return;
/* .line 592 */
} // :cond_1
/* array-length v2, v1 */
/* new-array v2, v2, [I */
/* .line 593 */
/* .local v2, "timeLevelInt":[I */
int v4 = 0; // const/4 v4, 0x0
/* .line 594 */
/* .local v4, "time_threshold":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 595 */
/* .local v5, "index":I */
/* array-length v6, v1 */
int v7 = 0; // const/4 v7, 0x0
/* move v8, v7 */
} // :goto_0
/* if-ge v8, v6, :cond_3 */
/* aget-object v9, v1, v8 */
/* .line 596 */
/* .local v9, "str":Ljava/lang/String; */
int v4 = 0; // const/4 v4, 0x0
/* .line 598 */
try { // :try_start_0
v10 = java.lang.Integer .parseInt ( v9 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v4, v10 */
/* .line 602 */
/* nop */
/* .line 603 */
/* if-gtz v4, :cond_2 */
return;
/* .line 604 */
} // :cond_2
/* aput v4, v2, v5 */
/* .line 605 */
/* nop */
} // .end local v9 # "str":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 595 */
/* add-int/lit8 v8, v8, 0x1 */
/* .line 599 */
/* .restart local v9 # "str":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v3 */
/* .line 601 */
/* .local v3, "e":Ljava/lang/Exception; */
return;
/* .line 607 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // .end local v9 # "str":Ljava/lang/String;
} // :cond_3
v6 = com.android.server.am.PeriodicCleanerService.sDefaultTimeLevel;
java.lang.System .arraycopy ( v2,v7,v6,v7,v3 );
/* .line 608 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Override mPressureTimeThreshold with: "; // const-string v6, "Override mPressureTimeThreshold with: "
(( java.lang.StringBuilder ) v3 ).append ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "PeriodicCleaner"; // const-string v6, "PeriodicCleaner"
android.util.Slog .d ( v6,v3 );
/* .line 609 */
return;
} // .end method
/* # virtual methods */
public Boolean isPreviousApp ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 970 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 971 */
/* .local v0, "curTime":J */
v2 = this.mLock;
/* monitor-enter v2 */
/* .line 972 */
try { // :try_start_0
v3 = this.mLruPackages;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* .line 973 */
/* .local v3, "N":I */
int v4 = 2; // const/4 v4, 0x2
/* if-lt v3, v4, :cond_0 */
/* .line 974 */
v4 = this.mLruPackages;
/* add-int/lit8 v5, v3, -0x2 */
(( java.util.ArrayList ) v4 ).get ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
/* .line 975 */
/* .local v4, "previousApp":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo; */
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmPackageName ( v4 );
v5 = (( java.lang.String ) v5 ).equals ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 976 */
com.android.server.am.PeriodicCleanerService$PackageUseInfo .-$$Nest$fgetmBackgroundTime ( v4 );
/* move-result-wide v5 */
/* sub-long v5, v0, v5 */
/* const-wide/32 v7, 0x493e0 */
/* cmp-long v5, v5, v7 */
/* if-gtz v5, :cond_0 */
/* .line 977 */
/* monitor-exit v2 */
int v2 = 1; // const/4 v2, 0x1
/* .line 980 */
} // .end local v3 # "N":I
} // .end local v4 # "previousApp":Lcom/android/server/am/PeriodicCleanerService$PackageUseInfo;
} // :cond_0
/* monitor-exit v2 */
/* .line 981 */
int v2 = 0; // const/4 v2, 0x0
/* .line 980 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
public Boolean isSystemRelated ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 1482 */
v0 = this.info;
v0 = this.packageName;
/* .line 1483 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = com.android.server.am.PeriodicCleanerService .isSystemPackage ( v0 );
final String v2 = "PeriodicCleaner"; // const-string v2, "PeriodicCleaner"
int v3 = 1; // const/4 v3, 0x1
/* if-nez v1, :cond_3 */
v1 = com.android.server.am.PeriodicCleanerService .isWhiteListPackage ( v0 );
/* if-nez v1, :cond_3 */
v1 = this.info;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 1484 */
v1 = android.os.Process .isApplicationUid ( v1 );
/* if-nez v1, :cond_0 */
/* .line 1490 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_2
final String v1 = "com.google.android"; // const-string v1, "com.google.android"
v1 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1491 */
/* sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1492 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", skip for google."; // const-string v4, ", skip for google."
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 1494 */
} // :cond_1
/* .line 1496 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* .line 1485 */
} // :cond_3
} // :goto_0
/* sget-boolean v1, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 1486 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " is in exclude list."; // const-string v4, " is in exclude list."
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v1 );
/* .line 1488 */
} // :cond_4
} // .end method
public void onBootPhase ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "phase" # I */
/* .line 243 */
/* const/16 v0, 0x226 */
/* if-ne p1, v0, :cond_0 */
/* .line 244 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->onActivityManagerReady()V */
/* .line 245 */
} // :cond_0
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_1 */
/* .line 246 */
/* invoke-direct {p0}, Lcom/android/server/am/PeriodicCleanerService;->onBootComplete()V */
/* .line 248 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void onStart ( ) {
/* .locals 2 */
/* .line 232 */
/* sget-boolean v0, Lcom/android/server/am/PeriodicCleanerService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 233 */
final String v0 = "PeriodicCleaner"; // const-string v0, "PeriodicCleaner"
final String v1 = "Starting PeriodicCleaner"; // const-string v1, "Starting PeriodicCleaner"
android.util.Slog .i ( v0,v1 );
/* .line 235 */
} // :cond_0
/* new-instance v0, Lcom/android/server/am/PeriodicCleanerService$BinderService; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/PeriodicCleanerService$BinderService;-><init>(Lcom/android/server/am/PeriodicCleanerService;Lcom/android/server/am/PeriodicCleanerService$BinderService-IA;)V */
this.mBinderService = v0;
/* .line 236 */
/* new-instance v0, Lcom/android/server/am/PeriodicCleanerService$LocalService; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/PeriodicCleanerService$LocalService;-><init>(Lcom/android/server/am/PeriodicCleanerService;Lcom/android/server/am/PeriodicCleanerService$LocalService-IA;)V */
this.mLocalService = v0;
/* .line 237 */
/* const-class v1, Lcom/android/server/am/PeriodicCleanerInternalStub; */
(( com.android.server.am.PeriodicCleanerService ) p0 ).publishLocalService ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/android/server/am/PeriodicCleanerService;->publishLocalService(Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 238 */
final String v0 = "periodic"; // const-string v0, "periodic"
v1 = this.mBinderService;
(( com.android.server.am.PeriodicCleanerService ) p0 ).publishBinderService ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/am/PeriodicCleanerService;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V
/* .line 239 */
return;
} // .end method
