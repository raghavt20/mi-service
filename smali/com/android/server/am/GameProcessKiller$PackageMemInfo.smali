.class Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
.super Ljava/lang/Object;
.source "GameProcessKiller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/GameProcessKiller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PackageMemInfo"
.end annotation


# instance fields
.field public mMemSize:J

.field public mState:I

.field public mUid:I


# direct methods
.method public constructor <init>(IJI)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "pss"    # J
    .param p4, "state"    # I

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput p1, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mUid:I

    .line 132
    iput-wide p2, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mMemSize:J

    .line 133
    iput p4, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mState:I

    .line 134
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    iget v1, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 145
    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    iget-wide v1, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mMemSize:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 147
    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget v1, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
