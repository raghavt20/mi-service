public class com.android.server.am.PendingIntentRecordImpl implements com.android.server.am.PendingIntentRecordStub {
	 /* .source "PendingIntentRecordImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static final java.lang.Runnable sClearPendingCallback;
	 private static final Integer sDefaultClearTime;
	 private static final android.util.ArraySet sIgnorePackages;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/ArraySet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final java.lang.Object sLock;
private static final android.os.Handler sMessageHandler;
private static final java.util.Set sPendingPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private miui.greeze.IGreezeManager mIGreezeManager;
private java.util.List mLauncherApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mSystemUIApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.am.PendingIntentRecordImpl ( ) {
/* .locals 2 */
/* .line 50 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 52 */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 53 */
final String v1 = "com.miui.notification"; // const-string v1, "com.miui.notification"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 54 */
final String v1 = "com.android.keyguard"; // const-string v1, "com.android.keyguard"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 55 */
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 56 */
final String v1 = "com.mi.android.globallauncher"; // const-string v1, "com.mi.android.globallauncher"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 57 */
final String v1 = "com.xiaomi.xmsf"; // const-string v1, "com.xiaomi.xmsf"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 58 */
final String v1 = "com.google.android.wearable.app.cn"; // const-string v1, "com.google.android.wearable.app.cn"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 59 */
final String v1 = "com.xiaomi.mirror"; // const-string v1, "com.xiaomi.mirror"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 60 */
final String v1 = "com.miui.personalassistant"; // const-string v1, "com.miui.personalassistant"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 61 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 68 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
/* .line 69 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 70 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* .line 72 */
/* new-instance v0, Lcom/android/server/am/PendingIntentRecordImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/am/PendingIntentRecordImpl$$ExternalSyntheticLambda0;-><init>()V */
return;
} // .end method
public com.android.server.am.PendingIntentRecordImpl ( ) {
/* .locals 3 */
/* .line 41 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
this.mIGreezeManager = v0;
/* .line 80 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
final String v2 = "com.miui.notification"; // const-string v2, "com.miui.notification"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mSystemUIApp = v0;
/* .line 82 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
final String v2 = "com.mi.android.globallauncher"; // const-string v2, "com.mi.android.globallauncher"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mLauncherApp = v0;
return;
} // .end method
public static Boolean containsPendingIntent ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .line 200 */
v0 = com.android.server.am.PendingIntentRecordImpl.sLock;
/* monitor-enter v0 */
/* .line 201 */
try { // :try_start_0
v1 = v1 = com.android.server.am.PendingIntentRecordImpl.sPendingPackages;
/* monitor-exit v0 */
/* .line 202 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static void exemptTemporarily ( java.lang.String p0, Boolean p1 ) {
/* .locals 5 */
/* .param p0, "packageName" # Ljava/lang/String; */
/* .param p1, "ignoreSource" # Z */
/* .line 177 */
v0 = android.text.TextUtils .isEmpty ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 178 */
return;
/* .line 181 */
} // :cond_0
try { // :try_start_0
v0 = android.os.Binder .getCallingPid ( );
/* .line 182 */
/* .local v0, "callingPid":I */
/* if-nez p1, :cond_1 */
/* .line 183 */
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 184 */
/* .local v1, "pkg":Ljava/lang/String; */
v2 = com.android.server.am.PendingIntentRecordImpl.sIgnorePackages;
v2 = (( android.util.ArraySet ) v2 ).contains ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 185 */
return;
/* .line 188 */
} // .end local v1 # "pkg":Ljava/lang/String;
} // :cond_1
v1 = com.android.server.am.PendingIntentRecordImpl.sLock;
/* monitor-enter v1 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 189 */
try { // :try_start_1
v2 = com.android.server.am.PendingIntentRecordImpl.sPendingPackages;
/* .line 190 */
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 191 */
try { // :try_start_2
/* const-class v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* .line 192 */
v1 = com.android.server.am.PendingIntentRecordImpl.sMessageHandler;
v2 = com.android.server.am.PendingIntentRecordImpl.sClearPendingCallback;
(( android.os.Handler ) v1 ).removeCallbacks ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 193 */
/* const-wide/16 v3, 0x1388 */
(( android.os.Handler ) v1 ).postDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 196 */
/* nop */
} // .end local v0 # "callingPid":I
/* .line 190 */
/* .restart local v0 # "callingPid":I */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local p0 # "packageName":Ljava/lang/String;
} // .end local p1 # "ignoreSource":Z
try { // :try_start_4
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 194 */
} // .end local v0 # "callingPid":I
/* .restart local p0 # "packageName":Ljava/lang/String; */
/* .restart local p1 # "ignoreSource":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 195 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "PendingIntentRecordImpl"; // const-string v1, "PendingIntentRecordImpl"
final String v2 = "exempt temporarily error!"; // const-string v2, "exempt temporarily error!"
android.util.Log .e ( v1,v2,v0 );
/* .line 197 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private miui.greeze.IGreezeManager getGreeze ( ) {
/* .locals 1 */
/* .line 85 */
v0 = this.mIGreezeManager;
/* if-nez v0, :cond_0 */
/* .line 86 */
/* nop */
/* .line 87 */
final String v0 = "greezer"; // const-string v0, "greezer"
android.os.ServiceManager .getService ( v0 );
/* .line 86 */
miui.greeze.IGreezeManager$Stub .asInterface ( v0 );
this.mIGreezeManager = v0;
/* .line 88 */
} // :cond_0
v0 = this.mIGreezeManager;
} // .end method
public static com.android.server.am.PendingIntentRecordImpl getInstance ( ) {
/* .locals 1 */
/* .line 47 */
com.android.server.am.PendingIntentRecordStub .get ( );
/* check-cast v0, Lcom/android/server/am/PendingIntentRecordImpl; */
} // .end method
private static java.lang.String getTargetPkg ( com.android.server.am.PendingIntentRecord$Key p0, android.content.Intent p1 ) {
/* .locals 9 */
/* .param p0, "key" # Lcom/android/server/am/PendingIntentRecord$Key; */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/Exception; */
/* } */
} // .end annotation
/* .line 206 */
(( android.content.Intent ) p1 ).getPackage ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
/* .line 207 */
/* .local v0, "targetPkg":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 208 */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* .line 209 */
/* .local v1, "component":Landroid/content/ComponentName; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 210 */
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 214 */
} // .end local v1 # "component":Landroid/content/ComponentName;
} // :cond_0
/* if-nez v0, :cond_5 */
/* .line 215 */
android.app.AppGlobals .getPackageManager ( );
/* .line 216 */
/* .local v7, "pm":Landroid/content/pm/IPackageManager; */
v8 = android.os.UserHandle .getCallingUserId ( );
/* .line 217 */
/* .local v8, "userId":I */
/* iget v1, p0, Lcom/android/server/am/PendingIntentRecord$Key;->type:I */
int v2 = 4; // const/4 v2, 0x4
/* if-ne v1, v2, :cond_2 */
/* .line 218 */
int v3 = 0; // const/4 v3, 0x0
/* const-wide/16 v4, 0x400 */
/* move-object v1, v7 */
/* move-object v2, p1 */
/* move v6, v8 */
/* invoke-interface/range {v1 ..v6}, Landroid/content/pm/IPackageManager;->resolveService(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ResolveInfo; */
/* .line 220 */
/* .local v1, "resolveIntent":Landroid/content/pm/ResolveInfo; */
if ( v1 != null) { // if-eqz v1, :cond_1
v2 = this.serviceInfo;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 221 */
v2 = this.serviceInfo;
v0 = this.packageName;
/* .line 223 */
} // .end local v1 # "resolveIntent":Landroid/content/pm/ResolveInfo;
} // :cond_1
/* .line 224 */
} // :cond_2
int v3 = 0; // const/4 v3, 0x0
/* const-wide/16 v4, 0x400 */
/* move-object v1, v7 */
/* move-object v2, p1 */
/* move v6, v8 */
/* invoke-interface/range {v1 ..v6}, Landroid/content/pm/IPackageManager;->queryIntentReceivers(Landroid/content/Intent;Ljava/lang/String;JI)Landroid/content/pm/ParceledListSlice; */
/* .line 226 */
/* .local v1, "qeury":Ljava/lang/Object; */
/* if-nez v1, :cond_3 */
/* .line 227 */
int v2 = 0; // const/4 v2, 0x0
/* .line 229 */
} // :cond_3
int v2 = 0; // const/4 v2, 0x0
/* .line 230 */
/* .local v2, "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
/* move-object v3, v1 */
/* check-cast v3, Landroid/content/pm/ParceledListSlice; */
/* .line 231 */
/* .local v3, "parceledList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/content/pm/ResolveInfo;>;" */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 232 */
(( android.content.pm.ParceledListSlice ) v3 ).getList ( ); // invoke-virtual {v3}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
/* .line 234 */
} // :cond_4
v4 = if ( v2 != null) { // if-eqz v2, :cond_5
int v5 = 1; // const/4 v5, 0x1
/* if-ne v4, v5, :cond_5 */
/* .line 235 */
int v4 = 0; // const/4 v4, 0x0
/* check-cast v4, Landroid/content/pm/ResolveInfo; */
/* .line 236 */
/* .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo; */
v5 = this.activityInfo;
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 237 */
v5 = this.activityInfo;
v0 = this.packageName;
/* .line 242 */
} // .end local v1 # "qeury":Ljava/lang/Object;
} // .end local v2 # "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
} // .end local v3 # "parceledList":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/content/pm/ResolveInfo;>;"
} // .end local v4 # "resolveInfo":Landroid/content/pm/ResolveInfo;
} // .end local v7 # "pm":Landroid/content/pm/IPackageManager;
} // .end local v8 # "userId":I
} // :cond_5
} // :goto_0
/* if-nez v0, :cond_6 */
/* .line 243 */
v0 = this.packageName;
/* .line 245 */
} // :cond_6
} // .end method
private Integer getUidByPackageName ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 109 */
android.app.AppGlobals .getPackageManager ( );
/* .line 111 */
/* .local v0, "pm":Landroid/content/pm/IPackageManager; */
/* const-wide/16 v1, 0x0 */
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
/* .line 112 */
/* .local v1, "appinfo":Landroid/content/pm/ApplicationInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 113 */
/* iget v2, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 116 */
} // .end local v1 # "appinfo":Landroid/content/pm/ApplicationInfo;
} // :cond_0
/* .line 114 */
/* :catch_0 */
/* move-exception v1 */
/* .line 115 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "not find packageName :"; // const-string v3, "not find packageName :"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "PendingIntentRecordImpl"; // const-string v3, "PendingIntentRecordImpl"
android.util.Log .i ( v3,v2 );
/* .line 117 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // .end method
static void lambda$static$0 ( ) { //synthethic
/* .locals 2 */
/* .line 73 */
v0 = com.android.server.am.PendingIntentRecordImpl.sLock;
/* monitor-enter v0 */
/* .line 74 */
try { // :try_start_0
v1 = com.android.server.am.PendingIntentRecordImpl.sPendingPackages;
/* .line 75 */
/* monitor-exit v0 */
/* .line 76 */
return;
/* .line 75 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void notifyGreeze ( java.lang.String p0, java.lang.String p1, com.android.server.am.PendingIntentRecord$Key p2 ) {
/* .locals 6 */
/* .param p1, "targetPkg" # Ljava/lang/String; */
/* .param p2, "pkg" # Ljava/lang/String; */
/* .param p3, "key" # Lcom/android/server/am/PendingIntentRecord$Key; */
/* .line 91 */
v0 = v0 = this.mSystemUIApp;
/* if-nez v0, :cond_0 */
v0 = v0 = this.mLauncherApp;
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v0, p3, Lcom/android/server/am/PendingIntentRecord$Key;->type:I */
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_3 */
/* .line 93 */
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/am/PendingIntentRecordImpl;->getGreeze()Lmiui/greeze/IGreezeManager; */
/* if-nez v0, :cond_1 */
/* .line 94 */
return;
/* .line 95 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notifyGreeze pendingtent from "; // const-string v1, "notifyGreeze pendingtent from "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " -> "; // const-string v1, " -> "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PendingIntentRecordImpl"; // const-string v1, "PendingIntentRecordImpl"
android.util.Log .d ( v1,v0 );
/* .line 97 */
try { // :try_start_0
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/PendingIntentRecordImpl;->getUidByPackageName(Ljava/lang/String;)I */
/* .line 98 */
/* .local v0, "uid":I */
v2 = /* invoke-direct {p0}, Lcom/android/server/am/PendingIntentRecordImpl;->getGreeze()Lmiui/greeze/IGreezeManager; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 99 */
int v2 = 1; // const/4 v2, 0x1
/* new-array v2, v2, [I */
/* .line 100 */
/* .local v2, "uids":[I */
int v3 = 0; // const/4 v3, 0x0
/* aput v0, v2, v3 */
/* .line 101 */
/* invoke-direct {p0}, Lcom/android/server/am/PendingIntentRecordImpl;->getGreeze()Lmiui/greeze/IGreezeManager; */
final String v4 = "notifi"; // const-string v4, "notifi"
/* const/16 v5, 0x3e8 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 105 */
} // .end local v0 # "uid":I
} // .end local v2 # "uids":[I
} // :cond_2
/* .line 103 */
/* :catch_0 */
/* move-exception v0 */
/* .line 104 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "notifyGreeze error"; // const-string v2, "notifyGreeze error"
android.util.Log .e ( v1,v2 );
/* .line 107 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean checkRunningCompatibility ( android.content.Intent p0, java.lang.String p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 6 */
/* .param p1, "service" # Landroid/content/Intent; */
/* .param p2, "resolvedType" # Ljava/lang/String; */
/* .param p3, "callingUid" # I */
/* .param p4, "callingPid" # I */
/* .param p5, "userId" # I */
/* .line 251 */
com.android.server.am.ActivityManagerServiceStub .get ( );
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* move v5, p5 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lcom/android/server/am/ActivityManagerServiceStub;->checkRunningCompatibility(Landroid/content/Intent;Ljava/lang/String;III)Z */
} // .end method
public void exemptTemporarily ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 173 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.am.PendingIntentRecordImpl .exemptTemporarily ( p1,v0 );
/* .line 174 */
return;
} // .end method
public void preSendPendingIntentInner ( com.android.server.am.PendingIntentRecord$Key p0, android.content.Intent p1 ) {
/* .locals 12 */
/* .param p1, "key" # Lcom/android/server/am/PendingIntentRecord$Key; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 123 */
if ( p2 != null) { // if-eqz p2, :cond_7
/* if-nez p1, :cond_0 */
/* goto/16 :goto_5 */
/* .line 127 */
} // :cond_0
try { // :try_start_0
v0 = android.os.Binder .getCallingPid ( );
/* .line 128 */
/* .local v0, "callingPid":I */
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 129 */
/* .local v1, "pkg":Ljava/lang/String; */
com.android.server.am.PendingIntentRecordImpl .getTargetPkg ( p1,p2 );
/* .line 131 */
/* .local v2, "targetPkg":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v3 = "android"; // const-string v3, "android"
v3 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_1 */
/* .line 132 */
/* invoke-direct {p0, v2, v1, p1}, Lcom/android/server/am/PendingIntentRecordImpl;->notifyGreeze(Ljava/lang/String;Ljava/lang/String;Lcom/android/server/am/PendingIntentRecord$Key;)V */
/* .line 134 */
} // :cond_1
v3 = com.android.server.am.PendingIntentRecordImpl.sIgnorePackages;
v3 = (( android.util.ArraySet ) v3 ).contains ( v1 ); // invoke-virtual {v3, v1}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* if-nez v3, :cond_2 */
/* .line 135 */
return;
/* .line 137 */
} // :cond_2
final String v3 = "com.miui.home"; // const-string v3, "com.miui.home"
v3 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_4 */
final String v3 = "com.mi.android.globallauncher"; // const-string v3, "com.mi.android.globallauncher"
/* .line 138 */
v3 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
} // :cond_3
int v3 = 0; // const/4 v3, 0x0
} // :cond_4
} // :goto_0
int v3 = 1; // const/4 v3, 0x1
} // :goto_1
/* move v10, v3 */
/* .line 139 */
/* .local v10, "isHome":Z */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 140 */
return;
/* .line 144 */
} // :cond_5
/* iget v3, p1, Lcom/android/server/am/PendingIntentRecord$Key;->type:I */
/* packed-switch v3, :pswitch_data_0 */
/* .line 155 */
/* :pswitch_0 */
return;
/* .line 152 */
/* :pswitch_1 */
/* const/16 v3, 0x8 */
/* .line 153 */
/* .local v3, "wakeType":I */
/* move v11, v3 */
/* .line 146 */
} // .end local v3 # "wakeType":I
/* :pswitch_2 */
int v3 = 1; // const/4 v3, 0x1
/* .line 147 */
/* .restart local v3 # "wakeType":I */
/* move v11, v3 */
/* .line 149 */
} // .end local v3 # "wakeType":I
/* :pswitch_3 */
int v3 = 2; // const/4 v3, 0x2
/* .line 150 */
/* .restart local v3 # "wakeType":I */
/* move v11, v3 */
/* .line 157 */
} // .end local v3 # "wakeType":I
/* .local v11, "wakeType":I */
} // :goto_2
/* const-class v3, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v3 );
/* check-cast v3, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* .line 158 */
miui.security.WakePathChecker .getInstance ( );
if ( v10 != null) { // if-eqz v10, :cond_6
final String v4 = "appwidget"; // const-string v4, "appwidget"
} // :cond_6
final String v4 = "notification"; // const-string v4, "notification"
/* .line 159 */
} // :goto_3
v7 = android.os.UserHandle .myUserId ( );
/* iget v8, p1, Lcom/android/server/am/PendingIntentRecord$Key;->userId:I */
int v9 = 1; // const/4 v9, 0x1
/* .line 158 */
/* move-object v5, v2 */
/* move v6, v11 */
/* invoke-virtual/range {v3 ..v9}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 161 */
v3 = com.android.server.am.PendingIntentRecordImpl.sLock;
/* monitor-enter v3 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 162 */
try { // :try_start_1
v4 = com.android.server.am.PendingIntentRecordImpl.sPendingPackages;
/* .line 163 */
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 164 */
try { // :try_start_2
v3 = com.android.server.am.PendingIntentRecordImpl.sMessageHandler;
v4 = com.android.server.am.PendingIntentRecordImpl.sClearPendingCallback;
(( android.os.Handler ) v3 ).removeCallbacks ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 165 */
/* const-wide/16 v5, 0x1388 */
(( android.os.Handler ) v3 ).postDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 168 */
/* nop */
} // .end local v0 # "callingPid":I
} // .end local v1 # "pkg":Ljava/lang/String;
} // .end local v2 # "targetPkg":Ljava/lang/String;
} // .end local v10 # "isHome":Z
} // .end local v11 # "wakeType":I
/* .line 163 */
/* .restart local v0 # "callingPid":I */
/* .restart local v1 # "pkg":Ljava/lang/String; */
/* .restart local v2 # "targetPkg":Ljava/lang/String; */
/* .restart local v10 # "isHome":Z */
/* .restart local v11 # "wakeType":I */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_3
/* monitor-exit v3 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/am/PendingIntentRecordImpl;
} // .end local p1 # "key":Lcom/android/server/am/PendingIntentRecord$Key;
} // .end local p2 # "intent":Landroid/content/Intent;
try { // :try_start_4
/* throw v4 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 166 */
} // .end local v0 # "callingPid":I
} // .end local v1 # "pkg":Ljava/lang/String;
} // .end local v2 # "targetPkg":Ljava/lang/String;
} // .end local v10 # "isHome":Z
} // .end local v11 # "wakeType":I
/* .restart local p0 # "this":Lcom/android/server/am/PendingIntentRecordImpl; */
/* .restart local p1 # "key":Lcom/android/server/am/PendingIntentRecord$Key; */
/* .restart local p2 # "intent":Landroid/content/Intent; */
/* :catch_0 */
/* move-exception v0 */
/* .line 167 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "PendingIntentRecordImpl"; // const-string v1, "PendingIntentRecordImpl"
final String v2 = "preSendInner error"; // const-string v2, "preSendInner error"
android.util.Log .e ( v1,v2,v0 );
/* .line 169 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_4
return;
/* .line 124 */
} // :cond_7
} // :goto_5
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
