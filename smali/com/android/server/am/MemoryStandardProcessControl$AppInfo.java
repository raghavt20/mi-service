class com.android.server.am.MemoryStandardProcessControl$AppInfo {
	 /* .source "MemoryStandardProcessControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryStandardProcessControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AppInfo" */
} // .end annotation
/* # instance fields */
public Integer mAdj;
public Integer mKillTimes;
public Long mLastKillTime;
public Integer mMemExcelTime;
public final java.lang.String mPackageName;
public final java.util.Map mProcessMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public final Integer mUid;
public final Integer mUserId;
final com.android.server.am.MemoryStandardProcessControl this$0; //synthetic
/* # direct methods */
public com.android.server.am.MemoryStandardProcessControl$AppInfo ( ) {
/* .locals 2 */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .param p4, "userId" # I */
/* .param p5, "adj" # I */
/* .line 1245 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1241 */
/* new-instance p1, Ljava/util/HashMap; */
/* invoke-direct {p1}, Ljava/util/HashMap;-><init>()V */
this.mProcessMap = p1;
/* .line 1242 */
int p1 = 0; // const/4 p1, 0x0
/* iput p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I */
/* .line 1243 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mLastKillTime:J */
/* .line 1246 */
this.mPackageName = p2;
/* .line 1247 */
/* iput p3, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUid:I */
/* .line 1248 */
/* iput p4, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUserId:I */
/* .line 1249 */
/* iput p5, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
/* .line 1250 */
int p1 = 1; // const/4 p1, 0x1
/* iput p1, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* .line 1251 */
return;
} // .end method
/* # virtual methods */
public Integer getTotalPss ( ) {
/* .locals 7 */
/* .line 1254 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1255 */
/* .local v0, "pss":I */
v1 = this.mProcessMap;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1256 */
/* .local v2, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;" */
/* int-to-long v3, v0 */
/* check-cast v5, Ljava/lang/Long; */
(( java.lang.Long ) v5 ).longValue ( ); // invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
/* move-result-wide v5 */
/* add-long/2addr v3, v5 */
/* long-to-int v0, v3 */
/* .line 1257 */
} // .end local v2 # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
/* .line 1258 */
} // :cond_0
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 1263 */
v0 = this.mPackageName;
v1 = this.mProcessMap;
/* .line 1264 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* iget v2, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUid:I */
java.lang.Integer .valueOf ( v2 );
/* iget v3, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUserId:I */
java.lang.Integer .valueOf ( v3 );
/* iget v4, p0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
java.lang.Integer .valueOf ( v4 );
/* filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/Object; */
/* .line 1263 */
/* const-string/jumbo v1, "{ package: %s, processes: %s, uid: %d, userId: %d, adj: %d }" */
java.lang.String .format ( v1,v0 );
} // .end method
