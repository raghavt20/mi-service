class com.android.server.am.AppStateManager$PowerFrozenCallback implements com.miui.server.smartpower.PowerFrozenManager$IFrozenReportCallback {
	 /* .source "AppStateManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/AppStateManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "PowerFrozenCallback" */
} // .end annotation
/* # instance fields */
final com.android.server.am.AppStateManager this$0; //synthetic
/* # direct methods */
private com.android.server.am.AppStateManager$PowerFrozenCallback ( ) {
/* .locals 0 */
/* .line 3253 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.am.AppStateManager$PowerFrozenCallback ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/AppStateManager$PowerFrozenCallback;-><init>(Lcom/android/server/am/AppStateManager;)V */
return;
} // .end method
/* # virtual methods */
public void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "tid" # I */
/* .param p4, "binderState" # I */
/* .param p5, "now" # J */
/* .line 3288 */
/* packed-switch p4, :pswitch_data_0 */
/* .line 3296 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive binder state: uid["; // const-string v1, "receive binder state: uid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "] pid["; // const-string v1, "] pid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "] tid["; // const-string v1, "] tid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 3299 */
/* .local v0, "reason":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$monReportPowerFrozenSignal ( v1,p1,p2,v0 );
/* .line 3300 */
/* .line 3292 */
} // .end local v0 # "reason":Ljava/lang/String;
/* :pswitch_1 */
/* .line 3290 */
/* :pswitch_2 */
/* nop */
/* .line 3304 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Long p7 ) {
/* .locals 2 */
/* .param p1, "dstUid" # I */
/* .param p2, "dstPid" # I */
/* .param p3, "callerUid" # I */
/* .param p4, "callerPid" # I */
/* .param p5, "callerTid" # I */
/* .param p6, "isOneway" # Z */
/* .param p7, "now" # J */
/* .param p9, "buffer" # J */
/* .line 3268 */
if ( p6 != null) { // if-eqz p6, :cond_0
v0 = this.this$0;
v0 = (( com.android.server.am.AppStateManager ) v0 ).isProcessPerceptible ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Lcom/android/server/am/AppStateManager;->isProcessPerceptible(II)Z
/* if-nez v0, :cond_0 */
/* .line 3269 */
return;
/* .line 3271 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive binder trans: dstUid["; // const-string v1, "receive binder trans: dstUid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "] dstPid["; // const-string v1, "] dstPid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "] callerUid["; // const-string v1, "] callerUid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "] callerPid["; // const-string v1, "] callerPid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p4 ); // invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "] callerTid["; // const-string v1, "] callerTid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p5 ); // invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "] isOneway["; // const-string v1, "] isOneway["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p6 ); // invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = "] code["; // const-string v1, "] code["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p9, p10 ); // invoke-virtual {v0, p9, p10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 3278 */
/* .local v0, "reason":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$monReportPowerFrozenSignal ( v1,p1,p2,v0 );
/* .line 3279 */
return;
} // .end method
public void reportNet ( Integer p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "now" # J */
/* .line 3260 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive net request: uid["; // const-string v1, "receive net request: uid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 3261 */
/* .local v0, "reason":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$monReportPowerFrozenSignal ( v1,p1,v0 );
/* .line 3262 */
return;
} // .end method
public void reportSignal ( Integer p0, Integer p1, Long p2 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "now" # J */
/* .line 3256 */
return;
} // .end method
public void serviceReady ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "ready" # Z */
/* .line 3283 */
return;
} // .end method
public void thawedByOther ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 3308 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "receive other thaw request: uid["; // const-string v1, "receive other thaw request: uid["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 3309 */
/* .local v0, "reason":Ljava/lang/String; */
v1 = this.this$0;
com.android.server.am.AppStateManager .-$$Nest$monReportPowerFrozenSignal ( v1,p1,p2,v0 );
/* .line 3310 */
return;
} // .end method
