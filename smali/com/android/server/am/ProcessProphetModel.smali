.class public Lcom/android/server/am/ProcessProphetModel;
.super Ljava/lang/Object;
.source "ProcessProphetModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;,
        Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;,
        Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;,
        Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;,
        Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;,
        Lcom/android/server/am/ProcessProphetModel$PkgValuePair;,
        Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily;
    }
.end annotation


# static fields
.field private static final BT_MODEL_NAME:Ljava/lang/String; = "bt.model"

.field private static final BT_OF_TRACK:I = 0x14

.field private static final BT_PROBTAB_NAME:Ljava/lang/String; = "bt.prob"

.field private static BT_PROB_THRESHOLD:D = 0.0

.field private static final BT_PROTECTION_TIME:J = 0x1d4c0L

.field private static CP_PROB_THRESHOLD:D = 0.0

.field private static final CP_PROTECTION_TIME:J = 0x1d4c0L

.field private static final DAY_OF_TRACK:I = 0x7

.field private static DEBUG:Z = false

.field private static final LU_MODEL_NAME:Ljava/lang/String; = "launchusage.model"

.field private static final LU_PROBTAB_NAME:Ljava/lang/String; = "launchusage.prob"

.field private static LU_PROB_THRESHOLD:D = 0.0

.field private static final PSS_MODEL_NAME:Ljava/lang/String; = "pss.model"

.field private static final SAVE_PATH:Ljava/lang/String; = "/data/system/procprophet/"

.field private static final SLICE_IN_MILLIS:J = 0x36ee80L

.field private static final SLICE_OF_DAY:I = 0x18

.field private static final TAG:Ljava/lang/String; = "ProcessProphetModel"


# instance fields
.field private mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

.field private mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

.field public mBlackListBTAudio:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mBlackListLaunch:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

.field private mEmptyProcPss:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

.field private mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

.field private mLastBlueToothTime:J

.field private mLastCopyedTime:J

.field private mLastMatchedPkg:Ljava/lang/String;

.field public mWhiteListEmptyProc:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$sfgetDEBUG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$smisWeekend(Ljava/util/Calendar;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/server/am/ProcessProphetModel;->isWeekend(Ljava/util/Calendar;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 34
    nop

    .line 35
    const-string v0, "persist.sys.procprophet.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    .line 45
    nop

    .line 46
    const-string v0, "persist.sys.procprophet.lu_threshold"

    const/16 v1, 0x3c

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D

    .line 56
    nop

    .line 57
    const-string v0, "persist.sys.procprophet.bt_threshold"

    const/16 v1, 0x46

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-double v4, v0

    div-double/2addr v4, v2

    sput-wide v4, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D

    .line 64
    nop

    .line 65
    const-string v0, "persist.sys.procprophet.cp_threshold"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D

    .line 64
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    .line 44
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    .line 52
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    .line 53
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 54
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastBlueToothTime:J

    .line 60
    new-instance v2, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-direct {v2}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;-><init>()V

    iput-object v2, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    .line 61
    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mWhiteListEmptyProc:Ljava/util/HashSet;

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBlackListLaunch:Ljava/util/HashSet;

    .line 69
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBlackListBTAudio:Ljava/util/HashSet;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    .line 75
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 76
    .local v0, "startUpTime":J
    invoke-direct {p0}, Lcom/android/server/am/ProcessProphetModel;->init()V

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Model init consumed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessProphetModel"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    return-void
.end method

.method private init()V
    .locals 5

    .line 83
    const-string v0, "ProcessProphetModel"

    :try_start_0
    const-string v1, "launchusage.model"

    invoke-virtual {p0, v1}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    goto :goto_0

    .line 84
    :catch_0
    move-exception v1

    .line 85
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load LU model: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    if-nez v1, :cond_1

    .line 88
    const-string v1, "Failed to load LU model, return a new Model."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v1, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    invoke-direct {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    .line 94
    :cond_1
    :try_start_1
    const-string v1, "bt.model"

    invoke-virtual {p0, v1}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 97
    goto :goto_1

    .line 95
    :catch_1
    move-exception v1

    .line 96
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load BT model: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    if-nez v1, :cond_3

    .line 99
    const-string v1, "Failed to load BT model, return a new Model."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    new-instance v1, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    invoke-direct {v1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;-><init>()V

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    .line 105
    :cond_3
    const/4 v1, 0x0

    :try_start_2
    const-string v2, "launchusage.prob"

    invoke-virtual {p0, v2}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    iput-object v2, p0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 109
    goto :goto_2

    .line 106
    :catch_2
    move-exception v2

    .line 107
    .local v2, "e":Ljava/lang/Exception;
    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    .line 108
    sget-boolean v3, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load LU probtab: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_2
    :try_start_3
    const-string v2, "bt.prob"

    invoke-virtual {p0, v2}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    iput-object v2, p0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 117
    goto :goto_3

    .line 114
    :catch_3
    move-exception v2

    .line 115
    .restart local v2    # "e":Ljava/lang/Exception;
    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 116
    sget-boolean v1, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load BT probtab: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_3
    :try_start_4
    const-string v1, "pss.model"

    invoke-virtual {p0, v1}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iput-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 124
    goto :goto_4

    .line 122
    :catch_4
    move-exception v1

    .line 123
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v2, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load PSS model: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_4
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    if-nez v1, :cond_7

    .line 126
    const-string v1, "Failed to load PSS model, return a new Model."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    .line 129
    :cond_7
    return-void
.end method

.method private static isWeekend(Ljava/util/Calendar;)Z
    .locals 4
    .param p0, "mycalendar"    # Ljava/util/Calendar;

    .line 788
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    .line 789
    .local v0, "dayOfWeek":I
    const-string v2, "ProcessProphetModel"

    if-eqz v0, :cond_2

    const/4 v3, 0x6

    if-ne v0, v3, :cond_0

    goto :goto_0

    .line 793
    :cond_0
    sget-boolean v1, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "Today is workday."

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    :cond_1
    const/4 v1, 0x0

    return v1

    .line 790
    :cond_2
    :goto_0
    sget-boolean v3, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v3, :cond_3

    const-string v3, "Today is weekend."

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    :cond_3
    return v1
.end method

.method private probFusion(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)D
    .locals 16
    .param p1, "procName"    # Ljava/lang/String;
    .param p2, "weightLU"    # Ljava/lang/Double;
    .param p3, "weightBT"    # Ljava/lang/Double;
    .param p4, "weightCP"    # Ljava/lang/Double;

    .line 388
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-wide/16 v2, 0x0

    .line 395
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 388
    if-eqz v1, :cond_8

    const-string v5, ""

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    goto/16 :goto_3

    .line 391
    :cond_0
    const-wide/16 v2, 0x0

    .line 392
    .local v2, "probLU":D
    const-wide/16 v5, 0x0

    .line 393
    .local v5, "probBT":D
    const-wide/16 v7, 0x0

    .line 395
    .local v7, "probCP":D
    iget-object v9, v0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    if-eqz v9, :cond_1

    move-object/from16 v9, p2

    invoke-virtual {v9, v4}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I

    move-result v10

    if-eqz v10, :cond_2

    .line 396
    iget-object v10, v0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    invoke-virtual {v10, v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->getProb(Ljava/lang/String;)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    move-result-object v10

    .line 397
    .local v10, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    iget-wide v11, v10, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v13

    mul-double v2, v11, v13

    goto :goto_0

    .line 395
    .end local v10    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    :cond_1
    move-object/from16 v9, p2

    .line 400
    :cond_2
    :goto_0
    iget-object v10, v0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    if-eqz v10, :cond_3

    move-object/from16 v10, p3

    invoke-virtual {v10, v4}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I

    move-result v11

    if-eqz v11, :cond_4

    .line 401
    iget-object v11, v0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-virtual {v11, v1}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProb(Ljava/lang/String;)D

    move-result-wide v11

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v13

    mul-double v5, v11, v13

    goto :goto_1

    .line 400
    :cond_3
    move-object/from16 v10, p3

    .line 404
    :cond_4
    :goto_1
    iget-object v11, v0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    if-eqz v11, :cond_5

    move-object/from16 v11, p4

    invoke-virtual {v11, v4}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, v0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    .line 405
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 406
    iget-object v4, v0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-virtual {v4, v1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->getProb(Ljava/lang/String;)D

    move-result-wide v12

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    mul-double v7, v12, v14

    goto :goto_2

    .line 404
    :cond_5
    move-object/from16 v11, p4

    .line 408
    :cond_6
    :goto_2
    new-instance v4, Ljava/text/DecimalFormat;

    const-string v12, "#.####"

    invoke-direct {v4, v12}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 409
    .local v4, "doubleDF":Ljava/text/DecimalFormat;
    sget-boolean v12, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v12, :cond_7

    .line 410
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "\tprob fusion: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 411
    invoke-virtual {v4, v5, v6}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 410
    const-string v13, "ProcessProphetModel"

    invoke-static {v13, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_7
    add-double v12, v2, v5

    add-double/2addr v12, v7

    return-wide v12

    .line 388
    .end local v2    # "probLU":D
    .end local v4    # "doubleDF":Ljava/text/DecimalFormat;
    .end local v5    # "probBT":D
    .end local v7    # "probCP":D
    :cond_8
    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    .line 389
    :goto_3
    return-wide v2
.end method

.method private saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V
    .locals 10
    .param p1, "model"    # Ljava/io/Serializable;
    .param p2, "modelName"    # Ljava/lang/String;

    .line 420
    const-string v0, "error when closing fos.  "

    const-string v1, "error when closing oos.  "

    new-instance v2, Ljava/io/File;

    const-string v3, "/data/system/procprophet/"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 421
    .local v2, "dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    const-string v5, "ProcessProphetModel"

    if-nez v4, :cond_0

    .line 422
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_0

    .line 423
    const-string v0, "mk dir fail."

    invoke-static {v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    return-void

    .line 428
    :cond_0
    const/4 v4, 0x0

    .line 429
    .local v4, "fileOut":Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    .line 431
    .local v6, "objOut":Ljava/io/ObjectOutputStream;
    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v8, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v4, v7

    .line 432
    new-instance v3, Ljava/io/ObjectOutputStream;

    invoke-direct {v3, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v6, v3

    .line 433
    invoke-virtual {v6, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 434
    sget-boolean v3, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Save "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " done."

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    :cond_1
    nop

    .line 440
    :try_start_1
    invoke-virtual {v6}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 443
    goto :goto_0

    .line 441
    :catch_0
    move-exception v3

    .line 442
    .local v3, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    .end local v3    # "e":Ljava/io/IOException;
    :goto_0
    nop

    .line 447
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 450
    :goto_1
    goto :goto_4

    .line 448
    :catch_1
    move-exception v1

    .line 449
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v1    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 438
    :catchall_0
    move-exception v3

    goto :goto_5

    .line 435
    :catch_2
    move-exception v3

    .line 436
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    sget-boolean v7, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to save "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 438
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    if-eqz v6, :cond_3

    .line 440
    :try_start_4
    invoke-virtual {v6}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 443
    goto :goto_3

    .line 441
    :catch_3
    move-exception v3

    .line 442
    .local v3, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    .end local v3    # "e":Ljava/io/IOException;
    :cond_3
    :goto_3
    if-eqz v4, :cond_4

    .line 447
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 448
    :catch_4
    move-exception v1

    .line 449
    .restart local v1    # "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_2

    .line 453
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    :goto_4
    return-void

    .line 438
    :goto_5
    if-eqz v6, :cond_5

    .line 440
    :try_start_6
    invoke-virtual {v6}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 443
    goto :goto_6

    .line 441
    :catch_5
    move-exception v7

    .line 442
    .local v7, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    .end local v7    # "e":Ljava/io/IOException;
    :cond_5
    :goto_6
    if-eqz v4, :cond_6

    .line 447
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 450
    goto :goto_7

    .line 448
    :catch_6
    move-exception v1

    .line 449
    .restart local v1    # "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    .end local v1    # "e":Ljava/io/IOException;
    :cond_6
    :goto_7
    throw v3
.end method


# virtual methods
.method public conclude()V
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->conclude()Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    .line 214
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    const-string v1, "launchusage.model"

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    const-string v1, "launchusage.prob"

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->conclude()Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 220
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    const-string v1, "bt.model"

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    const-string v1, "bt.prob"

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    const-string v1, "pss.model"

    invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method public dump()V
    .locals 9

    .line 240
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.####"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 241
    .local v0, "doubleDF":Ljava/text/DecimalFormat;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Probability threshold of LU, BT and CP: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v2, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D

    .line 242
    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v3, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D

    .line 243
    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v2, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D

    .line 244
    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 241
    const-string v2, "ProcessProphetModel"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dump()V

    .line 250
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dump()V

    .line 253
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    if-eqz v1, :cond_0

    .line 254
    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->dump()V

    .line 258
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    if-eqz v1, :cond_1

    .line 259
    const-string v1, "Dumping Bluetooth Prob Tab."

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->dump()V

    .line 267
    const-string v1, "Dumping EmptyProcPss:"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 269
    .local v3, "procName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Pss="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, 0x400

    div-long/2addr v5, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "MB"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    .end local v3    # "procName":Ljava/lang/String;
    goto :goto_0

    .line 271
    :cond_2
    return-void
.end method

.method public getPssInRecord(Ljava/lang/String;)Ljava/lang/Long;
    .locals 1
    .param p1, "procName"    # Ljava/lang/String;

    .line 227
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getWeightedTab(Ljava/util/HashMap;)Ljava/util/ArrayList;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ProcessRecord;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;",
            ">;"
        }
    .end annotation

    .line 277
    .local p1, "aliveEmptyProcs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ProcessRecord;>;"
    move-object/from16 v1, p0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 280
    .local v2, "currentTimeStamp":J
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 281
    .local v4, "weightLU":Ljava/lang/Double;
    const/4 v0, 0x0

    .line 282
    .local v0, "topLUProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    const/4 v5, 0x0

    .line 283
    .local v5, "curProbTab":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    iget-object v6, v1, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    if-eqz v6, :cond_3

    .line 284
    invoke-virtual {v6}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->getCurProbTab()Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    move-result-object v5

    .line 285
    if-eqz v5, :cond_2

    .line 286
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-static {v6}, Lcom/android/server/am/ProcessProphetModel;->isWeekend(Ljava/util/Calendar;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 287
    sget-wide v9, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D

    div-double/2addr v9, v7

    invoke-virtual {v5, v9, v10}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProbsGreaterThan(D)Ljava/util/ArrayList;

    move-result-object v0

    .line 288
    sget-boolean v6, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v6, :cond_1

    const-string v6, "ProcessProphetModel"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "topLUProbs\' threshold is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-wide v10, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D

    div-double/2addr v10, v7

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 290
    :cond_0
    sget-wide v9, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D

    invoke-virtual {v5, v9, v10}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProbsGreaterThan(D)Ljava/util/ArrayList;

    move-result-object v0

    .line 291
    sget-boolean v6, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v6, :cond_1

    const-string v6, "ProcessProphetModel"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "topLUProbs\' threshold is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-wide v10, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :cond_1
    :goto_0
    move-object v6, v5

    move-object v5, v0

    goto :goto_1

    .line 285
    :cond_2
    move-object v6, v5

    move-object v5, v0

    goto :goto_1

    .line 283
    :cond_3
    move-object v6, v5

    move-object v5, v0

    .line 295
    .end local v0    # "topLUProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    .local v5, "topLUProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    .local v6, "curProbTab":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    :goto_1
    sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v0, :cond_4

    const-string v0, "ProcessProphetModel"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "topLUProbs: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :cond_4
    const-wide/16 v9, 0x0

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 299
    .local v0, "weightBT":Ljava/lang/Double;
    const/4 v11, 0x0

    .line 300
    .local v11, "topBTProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    iget-object v12, v1, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    const-wide/32 v13, 0x1d4c0

    if-eqz v12, :cond_5

    iget-wide v9, v1, Lcom/android/server/am/ProcessProphetModel;->mLastBlueToothTime:J

    sub-long v9, v2, v9

    cmp-long v9, v9, v13

    if-gtz v9, :cond_5

    .line 302
    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 303
    iget-object v9, v1, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    sget-wide v7, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D

    invoke-virtual {v9, v7, v8}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProbsGreaterThan(D)Ljava/util/ArrayList;

    move-result-object v11

    move-object v7, v0

    goto :goto_2

    .line 305
    :cond_5
    move-object v7, v0

    .end local v0    # "weightBT":Ljava/lang/Double;
    .local v7, "weightBT":Ljava/lang/Double;
    :goto_2
    sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v0, :cond_6

    const-string v0, "ProcessProphetModel"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "topBTProbs: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_6
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    move-object v8, v0

    .line 309
    .local v8, "allowToStartProcs":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz v5, :cond_7

    .line 310
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 311
    .local v9, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    iget-object v10, v9, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 312
    .end local v9    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    goto :goto_3

    .line 314
    :cond_7
    if-eqz v11, :cond_8

    .line 315
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;

    .line 316
    .restart local v9    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    iget-object v10, v9, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->pkgName:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 317
    .end local v9    # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
    goto :goto_4

    .line 319
    :cond_8
    const-wide/16 v9, 0x0

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 320
    .local v0, "weightCP":Ljava/lang/Double;
    iget-wide v9, v1, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J

    sub-long v9, v2, v9

    cmp-long v9, v9, v13

    if-gtz v9, :cond_9

    iget-object v9, v1, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    if-eqz v9, :cond_9

    const-string v10, ""

    .line 321
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_9

    .line 322
    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 323
    iget-object v9, v1, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-object v9, v0

    goto :goto_5

    .line 327
    :cond_9
    move-object v9, v0

    .end local v0    # "weightCP":Ljava/lang/Double;
    .local v9, "weightCP":Ljava/lang/Double;
    :goto_5
    monitor-enter p1

    .line 328
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 329
    .local v10, "procName":Ljava/lang/String;
    invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 330
    nop

    .end local v10    # "procName":Ljava/lang/String;
    goto :goto_6

    .line 331
    :cond_a
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v0, :cond_b

    const-string v0, "ProcessProphetModel"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "current weight: weightBT="

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, ", weightCP="

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_b
    new-instance v0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V

    .line 336
    .local v0, "probMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 337
    .local v12, "procName":Ljava/lang/String;
    invoke-direct {v1, v12, v4, v7, v9}, Lcom/android/server/am/ProcessProphetModel;->probFusion(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)D

    move-result-wide v13

    .line 338
    .local v13, "score":D
    invoke-virtual {v0, v12, v13, v14}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V

    .line 339
    .end local v12    # "procName":Ljava/lang/String;
    .end local v13    # "score":D
    goto :goto_7

    .line 341
    :cond_c
    invoke-static {v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->-$$Nest$mupdateList(Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;)Ljava/util/ArrayList;

    move-result-object v10

    .line 342
    .local v10, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    const-string v12, "ProcessProphetModel"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getWeightedTab consumed "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 343
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v2

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "ms,"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 342
    invoke-static {v12, v13}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    return-object v10

    .line 331
    .end local v0    # "probMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    .end local v10    # "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;
    .locals 11
    .param p1, "modelName"    # Ljava/lang/String;

    .line 459
    const-string v0, "Failed to load "

    const-string v1, "error when closing fis.  "

    const-string v2, "error when closing ois.  "

    const-string v3, "ProcessProphetModel"

    const/4 v4, 0x0

    .line 460
    .local v4, "model":Ljava/lang/Object;
    const/4 v5, 0x0

    .line 461
    .local v5, "fileInput":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 463
    .local v6, "objInput":Ljava/io/ObjectInputStream;
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/data/system/procprophet/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v5, v7

    .line 464
    new-instance v7, Ljava/io/ObjectInputStream;

    invoke-direct {v7, v5}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v6, v7

    .line 465
    invoke-virtual {v6}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v0

    .line 471
    nop

    .line 473
    :try_start_1
    invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 476
    goto :goto_0

    .line 474
    :catch_0
    move-exception v0

    .line 475
    .local v0, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    nop

    .line 480
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 483
    :goto_1
    goto/16 :goto_5

    .line 481
    :catch_1
    move-exception v0

    .line 482
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 471
    :catchall_0
    move-exception v0

    goto/16 :goto_6

    .line 468
    :catch_2
    move-exception v7

    .line 469
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    sget-boolean v8, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ". "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 471
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_0
    if-eqz v6, :cond_1

    .line 473
    :try_start_4
    invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 476
    goto :goto_3

    .line 474
    :catch_3
    move-exception v0

    .line 475
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :goto_3
    if-eqz v5, :cond_4

    .line 480
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 481
    :catch_4
    move-exception v0

    .line 482
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_2

    .line 466
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v7

    .line 467
    .local v7, "e":Ljava/io/FileNotFoundException;
    :try_start_6
    sget-boolean v8, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v8, :cond_2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ". Not Found."

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 471
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_2
    if-eqz v6, :cond_3

    .line 473
    :try_start_7
    invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 476
    goto :goto_4

    .line 474
    :catch_6
    move-exception v0

    .line 475
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    :goto_4
    if-eqz v5, :cond_4

    .line 480
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    goto/16 :goto_1

    .line 481
    :catch_7
    move-exception v0

    .line 482
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_2

    .line 486
    .end local v0    # "e":Ljava/io/IOException;
    :cond_4
    :goto_5
    if-eqz v4, :cond_5

    .line 487
    sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Load "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " done."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_5
    sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Loaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :cond_6
    return-object v4

    .line 471
    :goto_6
    if-eqz v6, :cond_7

    .line 473
    :try_start_9
    invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    .line 476
    goto :goto_7

    .line 474
    :catch_8
    move-exception v7

    .line 475
    .local v7, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    .end local v7    # "e":Ljava/io/IOException;
    :cond_7
    :goto_7
    if-eqz v5, :cond_8

    .line 480
    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 483
    goto :goto_8

    .line 481
    :catch_9
    move-exception v2

    .line 482
    .local v2, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    .end local v2    # "e":Ljava/io/IOException;
    :cond_8
    :goto_8
    throw v0
.end method

.method public notifyBTConnected()V
    .locals 2

    .line 173
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastBlueToothTime:J

    .line 174
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->notifyBTConnected()V

    .line 175
    return-void
.end method

.method public reset()V
    .locals 1

    .line 498
    new-instance v0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    .line 499
    new-instance v0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    .line 500
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    .line 501
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTProbTab:Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    .line 502
    new-instance v0, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    .line 503
    return-void
.end method

.method public sortEmptyProcs(Ljava/util/HashMap;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/ProcessRecord;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;",
            ">;"
        }
    .end annotation

    .line 352
    .local p1, "aliveEmptyProcs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ProcessRecord;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 355
    .local v0, "currentTimeStamp":J
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 356
    .local v2, "weightLU":Ljava/lang/Double;
    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    .line 357
    .local v5, "weightBT":Ljava/lang/Double;
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 358
    .local v3, "weightCP":Ljava/lang/Double;
    iget-wide v6, p0, Lcom/android/server/am/ProcessProphetModel;->mLastBlueToothTime:J

    sub-long v6, v0, v6

    const-wide/32 v8, 0x1d4c0

    cmp-long v4, v6, v8

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    if-gtz v4, :cond_0

    .line 359
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    .line 361
    :cond_0
    iget-wide v10, p0, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J

    sub-long v10, v0, v10

    cmp-long v4, v10, v8

    if-gtz v4, :cond_1

    .line 362
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 364
    :cond_1
    sget-boolean v4, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v4, :cond_2

    const-string v4, "ProcessProphetModel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "current weight: weightBT="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", weightCP="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    :cond_2
    new-instance v4, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;

    invoke-direct {v4}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V

    .line 368
    .local v4, "probMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
    monitor-enter p1

    .line 369
    :try_start_0
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/ProcessRecord;

    .line 370
    .local v7, "app":Lcom/android/server/am/ProcessRecord;
    iget-object v8, v7, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    .line 371
    .local v8, "procName":Ljava/lang/String;
    invoke-direct {p0, v8, v2, v5, v3}, Lcom/android/server/am/ProcessProphetModel;->probFusion(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)D

    move-result-wide v9

    .line 372
    .local v9, "score":D
    invoke-virtual {v4, v8, v9, v10}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V

    .line 373
    .end local v7    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v8    # "procName":Ljava/lang/String;
    .end local v9    # "score":D
    goto :goto_0

    .line 374
    :cond_3
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    invoke-static {v4}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->-$$Nest$mupdateList(Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;)Ljava/util/ArrayList;

    move-result-object v6

    .line 377
    .local v6, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    const-string v7, "ProcessProphetModel"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "sortEmptyProcs consumed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 378
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v0

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "ms,"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 377
    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    return-object v6

    .line 374
    .end local v6    # "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method public testCP(Ljava/lang/String;)V
    .locals 3
    .param p1, "targetProcName"    # Ljava/lang/String;

    .line 509
    const-string v0, "ProcessProphetModel"

    if-eqz p1, :cond_2

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 513
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enter CP test mode, target proc name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetModel;->reset()V

    .line 519
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 520
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchEvent(Ljava/lang/String;)V

    .line 521
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchLaunchEvent(Ljava/lang/String;)V

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 523
    .end local v0    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchEvent(Ljava/lang/String;)V

    .line 525
    invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetModel;->conclude()V

    .line 526
    return-void

    .line 510
    :cond_2
    :goto_1
    const-string/jumbo v1, "target proc name error!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    return-void
.end method

.method public testMTBF()V
    .locals 2

    .line 532
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D

    .line 533
    sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D

    .line 534
    sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D

    .line 535
    return-void
.end method

.method public updateBTAudioEvent(Ljava/lang/String;)V
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 181
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->update(Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method public updateCopyEvent(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "curTopProcName"    # Ljava/lang/String;

    .line 189
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J

    .line 191
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->match(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    .line 192
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->getProb(Ljava/lang/String;)D

    move-result-wide v0

    .line 193
    .local v0, "prob":D
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "match: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", prob: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", curTop: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProcessProphetModel"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 199
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    iget-object v3, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchEvent(Ljava/lang/String;)V

    .line 200
    sget-wide v2, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 201
    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    return-object v2

    .line 205
    :cond_0
    const/4 v2, 0x0

    return-object v2
.end method

.method public updateLaunchEvent(Ljava/lang/String;)V
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 157
    if-eqz p1, :cond_3

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/server/am/ProcessProphetModel;->mLUProbTab:Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;

    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->update(Ljava/lang/String;JLcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;)V

    .line 164
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x1d4c0

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    .line 165
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 166
    sget-boolean v1, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "matchLaunch: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ProcessProphetModel"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    iget-object v2, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchLaunchEvent(Ljava/lang/String;)V

    .line 168
    iput-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastMatchedPkg:Ljava/lang/String;

    .line 170
    :cond_2
    return-void

    .line 158
    :cond_3
    :goto_0
    return-void
.end method

.method public updateModelSizeTrack()[J
    .locals 4

    .line 143
    const/4 v0, 0x6

    new-array v0, v0, [J

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->getAllDays()J

    move-result-wide v1

    const/4 v3, 0x0

    aput-wide v1, v0, v3

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->getAllLUUpdateTimes()J

    move-result-wide v1

    const/4 v3, 0x1

    aput-wide v1, v0, v3

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    .line 144
    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->getBTConnectedTimes()J

    move-result-wide v1

    const/4 v3, 0x2

    aput-wide v1, v0, v3

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mBTUsageModel:Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->getAllBTUpdateTimes()J

    move-result-wide v1

    const/4 v3, 0x3

    aput-wide v1, v0, v3

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    .line 145
    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->getAllCopyTimes()J

    move-result-wide v1

    const/4 v3, 0x4

    aput-wide v1, v0, v3

    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mCopyPatternModel:Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->getCopyLaunchedTimes()J

    move-result-wide v1

    const/4 v3, 0x5

    aput-wide v1, v0, v3

    .line 143
    return-object v0
.end method

.method public updateModelThreshold([Ljava/lang/String;)V
    .locals 7
    .param p1, "newThresList"    # [Ljava/lang/String;

    .line 132
    const/4 v0, 0x0

    aget-object v1, p1, v0

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    sput-wide v1, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D

    .line 133
    const/4 v1, 0x1

    aget-object v2, p1, v1

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    div-double/2addr v5, v3

    sput-wide v5, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D

    .line 134
    const/4 v2, 0x2

    aget-object v5, p1, v2

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    div-double/2addr v5, v3

    sput-wide v5, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D

    .line 135
    sget-boolean v3, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 136
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PROB_THRESHOLD change to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v0, p1, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " complete."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ProcessProphetModel"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    return-void
.end method

.method public updatePssInRecord(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6
    .param p1, "procName"    # Ljava/lang/String;
    .param p2, "pss"    # Ljava/lang/Long;

    .line 231
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 232
    .local v0, "rawPss":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 233
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/ProcessProphetModel;->mEmptyProcPss:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    :goto_0
    return-void
.end method

.method public uploadModelPredProb()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLUModel:Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->getTopModelPredict()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
