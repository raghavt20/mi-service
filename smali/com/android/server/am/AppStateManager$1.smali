.class Lcom/android/server/am/AppStateManager$1;
.super Landroid/content/BroadcastReceiver;
.source "AppStateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AppStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/AppStateManager;


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/AppStateManager;

    .line 238
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$1;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 241
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    nop

    .line 244
    const-string v1, "android.intent.extra.user_handle"

    const/16 v2, -0x2710

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 245
    .local v1, "userId":I
    if-eq v1, v2, :cond_0

    .line 246
    iget-object v2, p0, Lcom/android/server/am/AppStateManager$1;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v2}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmMainHandler(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$MainHandler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/server/am/AppStateManager$MainHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 247
    .local v2, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/AppStateManager$1;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v3}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmMainHandler(Lcom/android/server/am/AppStateManager;)Lcom/android/server/am/AppStateManager$MainHandler;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/server/am/AppStateManager$MainHandler;->sendMessage(Landroid/os/Message;)Z

    .line 250
    .end local v1    # "userId":I
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method
