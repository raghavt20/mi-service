public class com.android.server.am.ProcessListStubImpl implements com.android.server.am.ProcessListStub {
	 /* .source "ProcessListStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String APP_START_TIMEOUT_DUMP_DIR;
	 private static final java.lang.String APP_START_TIMEOUT_DUMP_PREFIX;
	 private static final Integer DEF_MIN_EXTRA_FREE_KB;
	 private static final Boolean ENABLE_MI_EXTRA_FREE;
	 private static final Boolean ENABLE_MI_SIZE_EXTRA_FREE;
	 private static final Boolean ENABLE_MI_SIZE_EXTRA_FREE_GAME_ONLY;
	 private static final java.lang.String KILL_REASON_START_TIMEOUT;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private Boolean mGameMode;
	 private Integer mLastDisplayHeight;
	 private Integer mLastDisplayWidth;
	 com.android.server.am.PeriodicCleanerInternalStub mPeriodicCleaner;
	 com.miui.server.process.ProcessManagerInternal mPmi;
	 private Integer mSystemRenderThreadTid;
	 /* # direct methods */
	 static com.android.server.am.ProcessListStubImpl ( ) {
		 /* .locals 3 */
		 /* .line 41 */
		 final String v0 = "persist.sys.spc.extra_free_enable"; // const-string v0, "persist.sys.spc.extra_free_enable"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.android.server.am.ProcessListStubImpl.ENABLE_MI_EXTRA_FREE = (v0!= 0);
		 /* .line 43 */
		 final String v0 = "persist.sys.spc.extra_free_kbytes"; // const-string v0, "persist.sys.spc.extra_free_kbytes"
		 /* const v2, 0xb13f */
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v2 );
		 /* .line 45 */
		 final String v0 = "persist.sys.spc.mi_extra_free_game_only"; // const-string v0, "persist.sys.spc.mi_extra_free_game_only"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.android.server.am.ProcessListStubImpl.ENABLE_MI_SIZE_EXTRA_FREE_GAME_ONLY = (v0!= 0);
		 /* .line 47 */
		 final String v0 = "persist.sys.spc.mi_extra_free_enable"; // const-string v0, "persist.sys.spc.mi_extra_free_enable"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.android.server.am.ProcessListStubImpl.ENABLE_MI_SIZE_EXTRA_FREE = (v0!= 0);
		 return;
	 } // .end method
	 public com.android.server.am.ProcessListStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 33 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 35 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mPmi = v0;
		 /* .line 36 */
		 this.mPeriodicCleaner = v0;
		 /* .line 37 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayWidth:I */
		 /* .line 38 */
		 /* iput v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayHeight:I */
		 /* .line 39 */
		 /* iput-boolean v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mGameMode:Z */
		 /* .line 55 */
		 /* iput v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mSystemRenderThreadTid:I */
		 return;
	 } // .end method
	 public static com.android.server.am.ProcessListStubImpl getInstance ( ) {
		 /* .locals 1 */
		 /* .line 58 */
		 /* const-class v0, Lcom/android/server/am/ProcessListStub; */
		 com.miui.base.MiuiStubUtil .getImpl ( v0 );
		 /* check-cast v0, Lcom/android/server/am/ProcessListStubImpl; */
	 } // .end method
	 public static Object getProcState ( Integer p0 ) {
		 /* .locals 4 */
		 /* .param p0, "pid" # I */
		 /* .line 126 */
		 try { // :try_start_0
			 /* new-instance v0, Ljava/io/BufferedReader; */
			 /* new-instance v1, Ljava/io/FileReader; */
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v3 = "/proc/"; // const-string v3, "/proc/"
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 final String v3 = "/stat"; // const-string v3, "/stat"
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 /* invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
			 /* invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
			 /* :try_end_0 */
			 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 128 */
			 /* .local v0, "bufferedReader":Ljava/io/BufferedReader; */
			 try { // :try_start_1
				 (( java.io.BufferedReader ) v0 ).readLine ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
				 /* .line 129 */
				 /* .local v1, "stat":Ljava/lang/String; */
				 final String v2 = ")"; // const-string v2, ")"
				 v2 = 				 (( java.lang.String ) v1 ).indexOf ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
				 /* add-int/lit8 v2, v2, 0x2 */
				 v2 = 				 (( java.lang.String ) v1 ).charAt ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C
				 /* :try_end_1 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* .line 130 */
				 try { // :try_start_2
					 (( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
					 /* :try_end_2 */
					 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
					 /* .line 129 */
					 /* .line 126 */
				 } // .end local v1 # "stat":Ljava/lang/String;
				 /* :catchall_0 */
				 /* move-exception v1 */
				 try { // :try_start_3
					 (( java.io.BufferedReader ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
					 /* :try_end_3 */
					 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
					 /* :catchall_1 */
					 /* move-exception v2 */
					 try { // :try_start_4
						 (( java.lang.Throwable ) v1 ).addSuppressed ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
					 } // .end local p0 # "pid":I
				 } // :goto_0
				 /* throw v1 */
				 /* :try_end_4 */
				 /* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
				 /* .line 130 */
			 } // .end local v0 # "bufferedReader":Ljava/io/BufferedReader;
			 /* .restart local p0 # "pid":I */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 131 */
			 /* .local v0, "e":Ljava/io/IOException; */
			 /* new-instance v1, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v2 = "fail to get proc state of pid "; // const-string v2, "fail to get proc state of pid "
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v2 = "ProcessListStubImpl"; // const-string v2, "ProcessListStubImpl"
			 android.util.Log .e ( v2,v1,v0 );
			 /* .line 132 */
			 int v1 = 0; // const/4 v1, 0x0
		 } // .end method
		 /* # virtual methods */
		 public Integer computeExtraFreeKbytes ( Long p0, Integer p1 ) {
			 /* .locals 2 */
			 /* .param p1, "totalMemMb" # J */
			 /* .param p3, "oldReserve" # I */
			 /* .line 203 */
			 /* sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_EXTRA_FREE:Z */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* const-wide/16 v0, 0x1800 */
				 /* cmp-long v0, p1, v0 */
				 /* if-lez v0, :cond_0 */
				 /* .line 204 */
				 v0 = 				 java.lang.Math .max ( v0,p3 );
				 /* .line 206 */
			 } // :cond_0
		 } // .end method
		 public void dumpWhenAppStartTimeout ( com.android.server.am.ProcessRecord p0 ) {
			 /* .locals 8 */
			 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
			 /* .line 101 */
			 v0 = 			 miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
			 /* if-nez v0, :cond_0 */
			 /* .line 102 */
			 return;
			 /* .line 104 */
		 } // :cond_0
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "pid "; // const-string v1, "pid "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = " "; // const-string v1, " "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.processName;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v1 = " stated timeout and its state is "; // const-string v1, " stated timeout and its state is "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
		 /* .line 105 */
		 v1 = 		 com.android.server.am.ProcessListStubImpl .getProcState ( v1 );
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 104 */
		 final String v1 = "ProcessListStubImpl"; // const-string v1, "ProcessListStubImpl"
		 android.util.Log .d ( v1,v0 );
		 /* .line 106 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "/data/miuilog/stability/scout/app/app_start_timeout_"; // const-string v2, "/data/miuilog/stability/scout/app/app_start_timeout_"
		 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v3 = this.processName;
		 (( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v3 = "_"; // const-string v3, "_"
		 (( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v4 = ".trace"; // const-string v4, ".trace"
		 (( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 108 */
		 /* .local v0, "dumpFile":Ljava/lang/String; */
		 /* iget v4, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
		 int v5 = 1; // const/4 v5, 0x1
		 v4 = 		 android.os.Debug .dumpJavaBacktraceToFileTimeout ( v4,v0,v5 );
		 final String v6 = " because of app started timeout"; // const-string v6, " because of app started timeout"
		 if ( v4 != null) { // if-eqz v4, :cond_1
			 /* .line 109 */
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 /* const-string/jumbo v3, "succeed to dump java trace to " */
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .d ( v1,v2 );
			 /* goto/16 :goto_0 */
			 /* .line 112 */
		 } // :cond_1
		 /* new-instance v4, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v7 = "fail to dump java trace for "; // const-string v7, "fail to dump java trace for "
		 (( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v7 = this.processName;
		 (( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v7 = " when it started timeout, try to dump native trace"; // const-string v7, " when it started timeout, try to dump native trace"
		 (( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .e ( v1,v4 );
		 /* .line 114 */
		 /* new-instance v4, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
		 (( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v4 = this.processName;
		 (( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v3, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v3 = ".native.trace"; // const-string v3, ".native.trace"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 116 */
		 /* iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
		 v2 = 		 android.os.Debug .dumpNativeBacktraceToFileTimeout ( v2,v0,v5 );
		 if ( v2 != null) { // if-eqz v2, :cond_2
			 /* .line 117 */
			 /* new-instance v2, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
			 /* const-string/jumbo v3, "succeed to dump native trace to " */
			 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .d ( v1,v2 );
			 /* .line 120 */
		 } // :cond_2
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "fail to dump native trace for "; // const-string v3, "fail to dump native trace for "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v3 = this.processName;
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .e ( v1,v2 );
		 /* .line 123 */
	 } // :goto_0
	 return;
} // .end method
public Integer getLastDisplayHeight ( ) {
	 /* .locals 1 */
	 /* .line 269 */
	 /* iget v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayHeight:I */
} // .end method
public Integer getLastDisplayWidth ( ) {
	 /* .locals 1 */
	 /* .line 274 */
	 /* iget v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayWidth:I */
} // .end method
public Integer getSystemRenderThreadTid ( ) {
	 /* .locals 1 */
	 /* .line 168 */
	 /* iget v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mSystemRenderThreadTid:I */
} // .end method
public void hookAppZygoteStart ( android.content.pm.ApplicationInfo p0 ) {
	 /* .locals 2 */
	 /* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
	 /* .line 246 */
	 com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
	 int v1 = 1; // const/4 v1, 0x1
	 (( com.miui.server.greeze.GreezeManagerInternal ) v0 ).handleAppZygoteStart ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/miui/server/greeze/GreezeManagerInternal;->handleAppZygoteStart(Landroid/content/pm/ApplicationInfo;Z)V
	 /* .line 247 */
	 return;
} // .end method
public Boolean isAllowRestartProcessLock ( java.lang.String p0, Integer p1, Integer p2, java.lang.String p3, java.lang.String p4, com.android.server.am.HostingRecord p5 ) {
	 /* .locals 8 */
	 /* .param p1, "processName" # Ljava/lang/String; */
	 /* .param p2, "flag" # I */
	 /* .param p3, "uid" # I */
	 /* .param p4, "pkgName" # Ljava/lang/String; */
	 /* .param p5, "callerPackage" # Ljava/lang/String; */
	 /* .param p6, "hostingRecord" # Lcom/android/server/am/HostingRecord; */
	 /* .line 179 */
	 v0 = this.mPmi;
	 /* if-nez v0, :cond_0 */
	 /* .line 180 */
	 /* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
	 com.android.server.LocalServices .getService ( v0 );
	 /* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
	 this.mPmi = v0;
	 /* .line 181 */
	 /* if-nez v0, :cond_0 */
	 /* .line 182 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 185 */
} // :cond_0
v1 = this.mPmi;
/* move-object v2, p1 */
/* move v3, p2 */
/* move v4, p3 */
/* move-object v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p6 */
v0 = /* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/process/ProcessManagerInternal;->isAllowRestartProcessLock(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/android/server/am/HostingRecord;)Z */
} // .end method
public Boolean isGameMode ( ) {
/* .locals 1 */
/* .line 279 */
/* iget-boolean v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mGameMode:Z */
} // .end method
public Boolean isNeedTraceProcess ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 173 */
com.android.server.am.MiuiProcessPolicyManager .getInstance ( );
v0 = (( com.android.server.am.MiuiProcessPolicyManager ) v0 ).isNeedTraceProcess ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiProcessPolicyManager;->isNeedTraceProcess(Lcom/android/server/am/ProcessRecord;)Z
} // .end method
public Boolean needSetMiSizeExtraFree ( ) {
/* .locals 1 */
/* .line 251 */
/* sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE_GAME_ONLY:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* iget-boolean v0, p0, Lcom/android/server/am/ProcessListStubImpl;->mGameMode:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_0
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean needSetMiSizeExtraFreeForGameMode ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "gameMode" # Z */
/* .line 257 */
/* iput-boolean p1, p0, Lcom/android/server/am/ProcessListStubImpl;->mGameMode:Z */
/* .line 258 */
/* sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/android/server/am/ProcessListStubImpl;->ENABLE_MI_SIZE_EXTRA_FREE_GAME_ONLY:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void notifyAmsProcessKill ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 91 */
v0 = this.mPmi;
/* if-nez v0, :cond_0 */
/* .line 92 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
this.mPmi = v0;
/* .line 93 */
/* if-nez v0, :cond_0 */
/* .line 94 */
return;
/* .line 97 */
} // :cond_0
v0 = this.mPmi;
(( com.miui.server.process.ProcessManagerInternal ) v0 ).notifyAmsProcessKill ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/process/ProcessManagerInternal;->notifyAmsProcessKill(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 98 */
return;
} // .end method
public void notifyLmkProcessKill ( Integer p0, Integer p1, Long p2, Integer p3, Integer p4, Integer p5, Integer p6, java.lang.String p7 ) {
/* .locals 12 */
/* .param p1, "uid" # I */
/* .param p2, "oomScore" # I */
/* .param p3, "rssInBytes" # J */
/* .param p5, "freeMemKb" # I */
/* .param p6, "freeSwapKb" # I */
/* .param p7, "killReason" # I */
/* .param p8, "thrashing" # I */
/* .param p9, "processName" # Ljava/lang/String; */
/* .line 140 */
/* move-object v0, p0 */
v1 = this.mPmi;
/* if-nez v1, :cond_0 */
/* .line 141 */
/* const-class v1, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/server/process/ProcessManagerInternal; */
this.mPmi = v1;
/* .line 142 */
/* if-nez v1, :cond_0 */
/* .line 143 */
return;
/* .line 146 */
} // :cond_0
v2 = this.mPmi;
/* move v3, p1 */
/* move v4, p2 */
/* move-wide v5, p3 */
/* move/from16 v7, p5 */
/* move/from16 v8, p6 */
/* move/from16 v9, p7 */
/* move/from16 v10, p8 */
/* move-object/from16 v11, p9 */
/* invoke-virtual/range {v2 ..v11}, Lcom/miui/server/process/ProcessManagerInternal;->notifyLmkProcessKill(IIJIIIILjava/lang/String;)V */
/* .line 148 */
return;
} // .end method
public void notifyProcessDied ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 152 */
v0 = this.mPmi;
/* if-nez v0, :cond_0 */
/* .line 153 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
this.mPmi = v0;
/* .line 154 */
/* if-nez v0, :cond_0 */
/* .line 155 */
return;
/* .line 158 */
} // :cond_0
v0 = this.mPmi;
(( com.miui.server.process.ProcessManagerInternal ) v0 ).notifyProcessDied ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V
/* .line 159 */
return;
} // .end method
public void notifyProcessStarted ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "pid" # I */
/* .line 63 */
com.android.server.am.MiuiProcessPolicyManager .getInstance ( );
(( com.android.server.am.MiuiProcessPolicyManager ) v0 ).promoteImportantProcAdj ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiProcessPolicyManager;->promoteImportantProcAdj(Lcom/android/server/am/ProcessRecord;)V
/* .line 64 */
com.android.server.am.ProcessProphetStub .getInstance ( );
/* .line 66 */
java.lang.Integer .valueOf ( p2 );
v1 = this.info;
/* iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I */
java.lang.Integer .valueOf ( v1 );
v2 = this.info;
v2 = this.packageName;
v3 = this.processName;
/* filled-new-array {v0, v1, v2, v3}, [Ljava/lang/Object; */
final String v1 = "notifyProcessStarted"; // const-string v1, "notifyProcessStarted"
com.android.server.camera.CameraOpt .callMethod ( v1,v0 );
/* .line 69 */
v0 = this.mPmi;
/* if-nez v0, :cond_0 */
/* .line 70 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
this.mPmi = v0;
/* .line 71 */
/* if-nez v0, :cond_0 */
/* .line 72 */
return;
/* .line 75 */
} // :cond_0
v0 = this.mPmi;
(( com.miui.server.process.ProcessManagerInternal ) v0 ).notifyProcessStarted ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->notifyProcessStarted(Lcom/android/server/am/ProcessRecord;)V
/* .line 76 */
com.android.server.net.NetworkManagementServiceStub .getInstance ( );
v1 = this.info;
v1 = this.packageName;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 79 */
v0 = this.mPeriodicCleaner;
/* if-nez v0, :cond_1 */
/* .line 80 */
/* const-class v0, Lcom/android/server/am/PeriodicCleanerInternalStub; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/am/PeriodicCleanerInternalStub; */
this.mPeriodicCleaner = v0;
/* .line 81 */
/* if-nez v0, :cond_1 */
/* .line 82 */
return;
/* .line 85 */
} // :cond_1
v0 = this.mPeriodicCleaner;
v1 = this.processName;
(( com.android.server.am.ProcessRecord ) p1 ).getHostingRecord ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getHostingRecord()Lcom/android/server/am/HostingRecord;
(( com.android.server.am.HostingRecord ) v2 ).getType ( ); // invoke-virtual {v2}, Lcom/android/server/am/HostingRecord;->getType()Ljava/lang/String;
/* .line 87 */
return;
} // .end method
public Boolean restartDiedAppOrNot ( com.android.server.am.ProcessRecord p0, Boolean p1, Boolean p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "isHomeApp" # Z */
/* .param p3, "allowRestart" # Z */
/* .param p4, "fromBinderDied" # Z */
/* .line 192 */
v0 = this.mPmi;
/* if-nez v0, :cond_0 */
/* .line 193 */
/* const-class v0, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
this.mPmi = v0;
/* .line 194 */
/* if-nez v0, :cond_0 */
/* .line 195 */
/* .line 198 */
} // :cond_0
v0 = this.mPmi;
v0 = (( com.miui.server.process.ProcessManagerInternal ) v0 ).restartDiedAppOrNot ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/process/ProcessManagerInternal;->restartDiedAppOrNot(Lcom/android/server/am/ProcessRecord;ZZZ)Z
} // .end method
public void setLastDisplayWidthAndHeight ( Integer p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "lastDisplayWidth" # I */
/* .param p2, "lastDisplayHeight" # I */
/* .line 263 */
/* iput p1, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayWidth:I */
/* .line 264 */
/* iput p2, p0, Lcom/android/server/am/ProcessListStubImpl;->mLastDisplayHeight:I */
/* .line 265 */
return;
} // .end method
public void setSystemRenderThreadTidLocked ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "tid" # I */
/* .line 164 */
/* iput p1, p0, Lcom/android/server/am/ProcessListStubImpl;->mSystemRenderThreadTid:I */
/* .line 165 */
return;
} // .end method
public void writePerfEvent ( Integer p0, java.lang.String p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "procName" # Ljava/lang/String; */
/* .param p3, "minOomScore" # I */
/* .param p4, "oomScore" # I */
/* .param p5, "killReason" # I */
/* .line 212 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/dev/mi_exception_log"; // const-string v1, "/dev/mi_exception_log"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 213 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 214 */
/* new-instance v1, Lorg/json/JSONObject; */
/* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
/* .line 216 */
/* .local v1, "perfjson":Lorg/json/JSONObject; */
try { // :try_start_0
final String v2 = "EventID"; // const-string v2, "EventID"
/* const v3, 0x35c37939 */
(( org.json.JSONObject ) v1 ).put ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 217 */
final String v2 = "ProcessName"; // const-string v2, "ProcessName"
(( org.json.JSONObject ) v1 ).put ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 218 */
final String v2 = "UID"; // const-string v2, "UID"
(( org.json.JSONObject ) v1 ).put ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 219 */
final String v2 = "MinOomAdj"; // const-string v2, "MinOomAdj"
(( org.json.JSONObject ) v1 ).put ( v2, p3 ); // invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 220 */
final String v2 = "OomAdj"; // const-string v2, "OomAdj"
(( org.json.JSONObject ) v1 ).put ( v2, p4 ); // invoke-virtual {v1, v2, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 221 */
final String v2 = "Reason"; // const-string v2, "Reason"
(( org.json.JSONObject ) v1 ).put ( v2, p5 ); // invoke-virtual {v1, v2, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 224 */
/* .line 222 */
/* :catch_0 */
/* move-exception v2 */
/* .line 223 */
/* .local v2, "e":Lorg/json/JSONException; */
final String v3 = "ProcessListStubImpl"; // const-string v3, "ProcessListStubImpl"
final String v4 = "error put event"; // const-string v4, "error put event"
android.util.Slog .w ( v3,v4 );
/* .line 225 */
} // .end local v2 # "e":Lorg/json/JSONException;
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
/* .line 227 */
/* .local v2, "fos":Ljava/io/FileOutputStream; */
try { // :try_start_1
/* new-instance v3, Ljava/io/FileOutputStream; */
/* invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v2, v3 */
/* .line 228 */
(( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( java.lang.String ) v3 ).getBytes ( ); // invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v2 ).write ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V
/* .line 229 */
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 233 */
/* nop */
/* .line 235 */
try { // :try_start_2
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 233 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 230 */
/* :catch_1 */
/* move-exception v3 */
/* .line 231 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_3
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 233 */
} // .end local v3 # "e":Ljava/io/IOException;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 235 */
try { // :try_start_4
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 238 */
} // :goto_1
/* .line 236 */
/* :catch_2 */
/* move-exception v3 */
/* .line 237 */
/* .restart local v3 # "e":Ljava/io/IOException; */
(( java.io.IOException ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
} // .end local v3 # "e":Ljava/io/IOException;
/* .line 233 */
} // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 235 */
try { // :try_start_5
(( java.io.FileOutputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_3 */
/* .line 238 */
/* .line 236 */
/* :catch_3 */
/* move-exception v4 */
/* .line 237 */
/* .local v4, "e":Ljava/io/IOException; */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
/* .line 240 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_3
/* throw v3 */
/* .line 242 */
} // .end local v1 # "perfjson":Lorg/json/JSONObject;
} // .end local v2 # "fos":Ljava/io/FileOutputStream;
} // :cond_1
} // :goto_4
return;
} // .end method
