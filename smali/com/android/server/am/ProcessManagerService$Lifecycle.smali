.class public final Lcom/android/server/am/ProcessManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "ProcessManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/am/ProcessManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 165
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 166
    new-instance v0, Lcom/android/server/am/ProcessManagerService;

    invoke-direct {v0, p1}, Lcom/android/server/am/ProcessManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/am/ProcessManagerService$Lifecycle;->mService:Lcom/android/server/am/ProcessManagerService;

    .line 167
    return-void
.end method


# virtual methods
.method public onBootPhase(I)V
    .locals 1
    .param p1, "phase"    # I

    .line 175
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 176
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/am/SystemPressureController;->mProcessCleaner:Lcom/android/server/am/ProcessMemoryCleaner;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessMemoryCleaner;->onBootPhase()V

    .line 178
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 171
    const-string v0, "ProcessManager"

    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$Lifecycle;->mService:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/am/ProcessManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 172
    return-void
.end method
