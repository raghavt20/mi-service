public abstract class com.android.server.am.ProcessCleanerBase {
	 /* .source "ProcessCleanerBase.java" */
	 /* # static fields */
	 private static final java.lang.String DESK_CLOCK_PROCESS_NAME;
	 private static final java.lang.String HOME_PROCESS_NAME;
	 private static final java.lang.String RADIO_PROCESS_NAME;
	 private static final java.lang.String RADIO_TURN_OFF_INTENT;
	 public static final Integer SMART_POWER_PROTECT_APP_FLAGS;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 protected com.android.server.am.ActivityManagerService mAMS;
	 protected com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
	 /* # direct methods */
	 public com.android.server.am.ProcessCleanerBase ( ) {
		 /* .locals 0 */
		 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
		 /* .line 48 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 49 */
		 this.mAMS = p1;
		 /* .line 50 */
		 return;
	 } // .end method
	 public static Boolean isSystemApp ( com.android.server.am.ProcessRecord p0 ) {
		 /* .locals 3 */
		 /* .param p0, "app" # Lcom/android/server/am/ProcessRecord; */
		 /* .line 57 */
		 int v0 = 0; // const/4 v0, 0x0
		 if ( p0 != null) { // if-eqz p0, :cond_2
			 v1 = this.info;
			 if ( v1 != null) { // if-eqz v1, :cond_2
				 /* .line 58 */
				 v1 = this.info;
				 /* iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I */
				 /* and-int/lit16 v1, v1, 0x81 */
				 /* if-nez v1, :cond_0 */
				 /* const/16 v1, 0x3e8 */
				 /* iget v2, p0, Lcom/android/server/am/ProcessRecord;->uid:I */
				 /* if-ne v1, v2, :cond_1 */
			 } // :cond_0
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_1
		 /* .line 62 */
	 } // :cond_2
} // .end method
private void printKillLog ( com.android.server.am.ProcessRecord p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
	 /* .locals 4 */
	 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
	 /* .param p2, "killLevel" # I */
	 /* .param p3, "reason" # Ljava/lang/String; */
	 /* .param p4, "killTag" # Ljava/lang/String; */
	 /* .line 137 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 (( com.android.server.am.ProcessCleanerBase ) p0 ).killLevelToString ( p2 ); // invoke-virtual {p0, p2}, Lcom/android/server/am/ProcessCleanerBase;->killLevelToString(I)Ljava/lang/String;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 138 */
	 /* const/16 v1, 0x68 */
	 /* if-ne p2, v1, :cond_0 */
	 /* .line 139 */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = " pkgName="; // const-string v2, " pkgName="
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.info;
	 v2 = this.packageName;
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " procName="; // const-string v2, " procName="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.processName;
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 140 */
/* .local v0, "levelString":Ljava/lang/String; */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
java.lang.Integer .valueOf ( v1 );
v2 = this.mState;
/* .line 141 */
v2 = (( com.android.server.am.ProcessStateRecord ) v2 ).getCurProcState ( ); // invoke-virtual {v2}, Lcom/android/server/am/ProcessStateRecord;->getCurProcState()I
java.lang.Integer .valueOf ( v2 );
/* filled-new-array {v1, v2}, [Ljava/lang/Object; */
/* .line 140 */
final String v2 = "AS:%d%d"; // const-string v2, "AS:%d%d"
java.lang.String .format ( v2,v1 );
/* .line 142 */
/* .local v1, "info":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ": "; // const-string v3, ": "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " info="; // const-string v3, " info="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( p4,v2 );
/* .line 143 */
return;
} // .end method
/* # virtual methods */
Boolean checkKillFMApp ( com.android.server.am.ProcessRecord p0, android.os.Handler p1, android.content.Context p2 ) {
/* .locals 2 */
/* .param p1, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 146 */
if ( p2 != null) { // if-eqz p2, :cond_0
v0 = this.processName;
final String v1 = "com.miui.fmservice:remote"; // const-string v1, "com.miui.fmservice:remote"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 147 */
	 /* new-instance v0, Landroid/content/Intent; */
	 final String v1 = "miui.intent.action.TURN_OFF"; // const-string v1, "miui.intent.action.TURN_OFF"
	 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* .line 148 */
	 /* .local v0, "intent":Landroid/content/Intent; */
	 /* const/high16 v1, 0x10000000 */
	 (( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
	 /* .line 149 */
	 /* new-instance v1, Lcom/android/server/am/ProcessCleanerBase$1; */
	 /* invoke-direct {v1, p0, p3, v0, p1}, Lcom/android/server/am/ProcessCleanerBase$1;-><init>(Lcom/android/server/am/ProcessCleanerBase;Landroid/content/Context;Landroid/content/Intent;Lcom/android/server/am/ProcessRecord;)V */
	 (( android.os.Handler ) p2 ).post ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
	 /* .line 155 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* .line 157 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
protected Boolean checkProcessDied ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 131 */
v0 = this.mAMS;
/* monitor-enter v0 */
/* .line 132 */
if ( p1 != null) { // if-eqz p1, :cond_1
try { // :try_start_0
	 (( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 v1 = 		 (( com.android.server.am.ProcessRecord ) p1 ).isKilled ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
		 } // :cond_0
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 133 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* .line 132 */
	 } // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
/* monitor-exit v0 */
/* .line 133 */
} // :goto_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
void forceStopPackage ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 262 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "forceStopPackage pck:"; // const-string v1, "forceStopPackage pck:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " r:"; // const-string v1, " r:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/16 v1, 0x40 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 264 */
/* const-class v0, Landroid/app/ActivityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/app/ActivityManagerInternal; */
/* .line 265 */
(( android.app.ActivityManagerInternal ) v0 ).forceStopPackage ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 266 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 267 */
return;
} // .end method
java.lang.String getKillReason ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "policy" # I */
/* .line 330 */
int v0 = 0; // const/4 v0, 0x0
/* .line 331 */
/* .local v0, "reason":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* .line 381 */
/* :pswitch_0 */
final String v0 = "Unknown"; // const-string v0, "Unknown"
/* .line 351 */
/* :pswitch_1 */
final String v0 = "ScreenOffCPUCheckKill"; // const-string v0, "ScreenOffCPUCheckKill"
/* .line 352 */
/* .line 336 */
/* :pswitch_2 */
final String v0 = "AutoThermalKillAll2"; // const-string v0, "AutoThermalKillAll2"
/* .line 337 */
/* .line 333 */
/* :pswitch_3 */
final String v0 = "AutoThermalKillAll1"; // const-string v0, "AutoThermalKillAll1"
/* .line 334 */
/* .line 354 */
/* :pswitch_4 */
final String v0 = "AutoSystemAbnormalClean"; // const-string v0, "AutoSystemAbnormalClean"
/* .line 355 */
/* .line 348 */
/* :pswitch_5 */
final String v0 = "AutoSleepClean"; // const-string v0, "AutoSleepClean"
/* .line 349 */
/* .line 345 */
/* :pswitch_6 */
final String v0 = "AutoIdleKill"; // const-string v0, "AutoIdleKill"
/* .line 346 */
/* .line 342 */
/* :pswitch_7 */
final String v0 = "AutoThermalKill"; // const-string v0, "AutoThermalKill"
/* .line 343 */
/* .line 339 */
/* :pswitch_8 */
final String v0 = "AutoPowerKill"; // const-string v0, "AutoPowerKill"
/* .line 340 */
/* .line 378 */
/* :pswitch_9 */
final String v0 = "UserDefined"; // const-string v0, "UserDefined"
/* .line 379 */
/* .line 360 */
/* :pswitch_a */
final String v0 = "SwipeUpClean"; // const-string v0, "SwipeUpClean"
/* .line 361 */
/* .line 375 */
/* :pswitch_b */
final String v0 = "GarbageClean"; // const-string v0, "GarbageClean"
/* .line 376 */
/* .line 366 */
/* :pswitch_c */
final String v0 = "OptimizationClean"; // const-string v0, "OptimizationClean"
/* .line 367 */
/* .line 363 */
/* :pswitch_d */
final String v0 = "GameClean"; // const-string v0, "GameClean"
/* .line 364 */
/* .line 372 */
/* :pswitch_e */
final String v0 = "LockScreenClean"; // const-string v0, "LockScreenClean"
/* .line 373 */
/* .line 369 */
/* :pswitch_f */
final String v0 = "ForceClean"; // const-string v0, "ForceClean"
/* .line 370 */
/* .line 357 */
/* :pswitch_10 */
final String v0 = "OneKeyClean"; // const-string v0, "OneKeyClean"
/* .line 358 */
/* nop */
/* .line 383 */
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_0 */
/* :pswitch_4 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
java.util.List getProcessPolicyWhiteList ( miui.process.ProcessConfig p0, com.android.server.am.ProcessPolicy p1 ) {
/* .locals 3 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .param p2, "mProcessPolicy" # Lcom/android/server/am/ProcessPolicy; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lmiui/process/ProcessConfig;", */
/* "Lcom/android/server/am/ProcessPolicy;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 285 */
v0 = (( miui.process.ProcessConfig ) p1 ).getPolicy ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I
/* .line 286 */
/* .local v0, "policy":I */
(( miui.process.ProcessConfig ) p1 ).getWhiteList ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getWhiteList()Ljava/util/List;
/* .line 287 */
/* .local v1, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez v1, :cond_0 */
/* .line 288 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v1, v2 */
/* .line 290 */
} // :cond_0
/* const/16 v2, 0xe */
/* if-eq v0, v2, :cond_2 */
/* const/16 v2, 0x10 */
/* if-eq v0, v2, :cond_2 */
/* const/16 v2, 0xf */
/* if-ne v0, v2, :cond_1 */
/* .line 294 */
} // :cond_1
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_3 */
/* .line 295 */
final String v2 = "com.miui.home"; // const-string v2, "com.miui.home"
/* .line 293 */
} // :cond_2
} // :goto_0
final String v2 = "com.android.deskclock"; // const-string v2, "com.android.deskclock"
/* .line 297 */
} // :cond_3
} // :goto_1
(( com.android.server.am.ProcessCleanerBase ) p0 ).printWhiteList ( p1, v1 ); // invoke-virtual {p0, p1, v1}, Lcom/android/server/am/ProcessCleanerBase;->printWhiteList(Lmiui/process/ProcessConfig;Ljava/util/List;)V
/* .line 298 */
} // .end method
Boolean isAudioOrGPSApp ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 230 */
com.android.server.am.SystemPressureController .getInstance ( );
v0 = (( com.android.server.am.SystemPressureController ) v0 ).isAudioOrGPSApp ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/SystemPressureController;->isAudioOrGPSApp(I)Z
} // .end method
Boolean isAudioOrGPSProc ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 234 */
com.android.server.am.SystemPressureController .getInstance ( );
v0 = (( com.android.server.am.SystemPressureController ) v0 ).isAudioOrGPSProc ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/SystemPressureController;->isAudioOrGPSProc(II)Z
} // .end method
Boolean isCurrentProcessInBackup ( com.miui.server.smartpower.IAppState$IRunningProcess p0 ) {
/* .locals 2 */
/* .param p1, "runningProc" # Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
/* .line 204 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
(( com.miui.server.smartpower.IAppState$IRunningProcess ) p1 ).getProcessName ( ); // invoke-virtual {p1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getProcessName()Ljava/lang/String;
v0 = (( com.android.server.am.ProcessCleanerBase ) p0 ).isCurrentProcessInBackup ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/android/server/am/ProcessCleanerBase;->isCurrentProcessInBackup(Ljava/lang/String;Ljava/lang/String;)Z
} // .end method
Boolean isCurrentProcessInBackup ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "processName" # Ljava/lang/String; */
/* .line 208 */
v0 = this.mSmartPowerService;
v0 = /* const/16 v1, 0x40 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 210 */
int v0 = 1; // const/4 v0, 0x1
/* .line 212 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
Boolean isForceStopEnable ( com.android.server.am.ProcessRecord p0, Integer p1, com.android.server.am.ProcessManagerService p2 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "policy" # I */
/* .param p3, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .line 226 */
v0 = (( com.android.server.am.ProcessManagerService ) p3 ).isForceStopEnable ( p1, p2 ); // invoke-virtual {p3, p1, p2}, Lcom/android/server/am/ProcessManagerService;->isForceStopEnable(Lcom/android/server/am/ProcessRecord;I)Z
} // .end method
Boolean isInWhiteListLock ( com.android.server.am.ProcessRecord p0, Integer p1, Integer p2, com.android.server.am.ProcessManagerService p3 ) {
/* .locals 1 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "userId" # I */
/* .param p3, "policy" # I */
/* .param p4, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .line 222 */
v0 = (( com.android.server.am.ProcessManagerService ) p4 ).isInWhiteList ( p1, p2, p3 ); // invoke-virtual {p4, p1, p2, p3}, Lcom/android/server/am/ProcessManagerService;->isInWhiteList(Lcom/android/server/am/ProcessRecord;II)Z
} // .end method
Boolean isTrimMemoryEnable ( java.lang.String p0, com.android.server.am.ProcessManagerService p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .line 216 */
v0 = (( com.android.server.am.ProcessManagerService ) p2 ).isTrimMemoryEnable ( p1 ); // invoke-virtual {p2, p1}, Lcom/android/server/am/ProcessManagerService;->isTrimMemoryEnable(Ljava/lang/String;)Z
} // .end method
void killApplicationLock ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 271 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "killLocked pid:"; // const-string v1, "killLocked pid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 272 */
v1 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " r:"; // const-string v1, " r:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 271 */
/* const-wide/16 v1, 0x40 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 273 */
/* const/16 v0, 0xd */
int v3 = 1; // const/4 v3, 0x1
(( com.android.server.am.ProcessRecord ) p1 ).killLocked ( p2, v0, v3 ); // invoke-virtual {p1, p2, v0, v3}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V
/* .line 274 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 275 */
return;
} // .end method
void killBackgroundApplication ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 278 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "killBackgroundProcesses pid:"; // const-string v1, "killBackgroundProcesses pid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 279 */
v1 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " r:"; // const-string v1, " r:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 278 */
/* const-wide/16 v1, 0x40 */
android.os.Trace .traceBegin ( v1,v2,v0 );
/* .line 280 */
v0 = this.mAMS;
v3 = this.info;
v3 = this.packageName;
/* iget v4, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
(( com.android.server.am.ActivityManagerService ) v0 ).killBackgroundProcesses ( v3, v4, p2 ); // invoke-virtual {v0, v3, v4, p2}, Lcom/android/server/am/ActivityManagerService;->killBackgroundProcesses(Ljava/lang/String;ILjava/lang/String;)V
/* .line 281 */
android.os.Trace .traceEnd ( v1,v2 );
/* .line 282 */
return;
} // .end method
java.lang.String killLevelToString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "level" # I */
/* .line 387 */
final String v0 = ""; // const-string v0, ""
/* .line 388 */
/* .local v0, "killLevel":Ljava/lang/String; */
/* packed-switch p1, :pswitch_data_0 */
/* .line 399 */
/* :pswitch_0 */
final String v0 = "force-stop"; // const-string v0, "force-stop"
/* .line 400 */
/* .line 396 */
/* :pswitch_1 */
final String v0 = "kill"; // const-string v0, "kill"
/* .line 397 */
/* .line 393 */
/* :pswitch_2 */
final String v0 = "kill-background"; // const-string v0, "kill-background"
/* .line 394 */
/* .line 390 */
/* :pswitch_3 */
/* const-string/jumbo v0, "trim-memory" */
/* .line 391 */
/* .line 402 */
/* :pswitch_4 */
final String v0 = "none"; // const-string v0, "none"
/* .line 403 */
/* nop */
/* .line 406 */
} // :goto_0
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x64 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
void killOnce ( com.android.server.am.ProcessRecord p0, Integer p1, java.lang.String p2, Boolean p3, com.android.server.am.ProcessManagerService p4, java.lang.String p5, android.os.Handler p6, android.content.Context p7 ) {
/* .locals 14 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "policy" # I */
/* .param p3, "killReason" # Ljava/lang/String; */
/* .param p4, "canForceStop" # Z */
/* .param p5, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .param p6, "killTag" # Ljava/lang/String; */
/* .param p7, "handler" # Landroid/os/Handler; */
/* .param p8, "context" # Landroid/content/Context; */
/* .line 68 */
/* move-object v7, p0 */
/* move-object v8, p1 */
/* move/from16 v9, p2 */
/* move-object/from16 v10, p5 */
/* if-nez v8, :cond_0 */
/* .line 69 */
return;
/* .line 71 */
} // :cond_0
/* const/16 v1, 0x64 */
/* .line 73 */
/* .local v1, "killLevel":I */
try { // :try_start_0
v2 = this.mAMS;
/* monitor-enter v2 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .line 75 */
try { // :try_start_1
v0 = this.mAMS;
v0 = this.mActivityTaskManager;
v3 = this.info;
v3 = this.packageName;
v0 = com.android.server.wm.WindowProcessUtils .isRunningOnCarDisplay ( v0,v3 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 77 */
/* monitor-exit v2 */
return;
/* .line 80 */
} // :cond_1
v0 = (( com.android.server.am.ProcessRecord ) p1 ).isPersistent ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->isPersistent()Z
/* if-nez v0, :cond_5 */
v0 = this.mState;
/* .line 81 */
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).getSetAdj ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
/* if-ltz v0, :cond_5 */
/* iget v0, v8, Lcom/android/server/am/ProcessRecord;->userId:I */
/* .line 82 */
v0 = (( com.android.server.am.ProcessCleanerBase ) p0 ).isInWhiteListLock ( p1, v0, v9, v10 ); // invoke-virtual {p0, p1, v0, v9, v10}, Lcom/android/server/am/ProcessCleanerBase;->isInWhiteListLock(Lcom/android/server/am/ProcessRecord;IILcom/android/server/am/ProcessManagerService;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 87 */
} // :cond_2
v0 = (( com.android.server.am.ProcessCleanerBase ) p0 ).isForceStopEnable ( p1, v9, v10 ); // invoke-virtual {p0, p1, v9, v10}, Lcom/android/server/am/ProcessCleanerBase;->isForceStopEnable(Lcom/android/server/am/ProcessRecord;ILcom/android/server/am/ProcessManagerService;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
if ( p4 != null) { // if-eqz p4, :cond_3
/* .line 88 */
/* const/16 v0, 0x68 */
/* move v11, v0 */
} // .end local v1 # "killLevel":I
/* .local v0, "killLevel":I */
/* .line 89 */
} // .end local v0 # "killLevel":I
/* .restart local v1 # "killLevel":I */
} // :cond_3
int v0 = 3; // const/4 v0, 0x3
/* if-ne v9, v0, :cond_4 */
/* .line 90 */
/* const/16 v0, 0x66 */
/* move v11, v0 */
} // .end local v1 # "killLevel":I
/* .restart local v0 # "killLevel":I */
/* .line 92 */
} // .end local v0 # "killLevel":I
/* .restart local v1 # "killLevel":I */
} // :cond_4
/* const/16 v0, 0x67 */
/* move v11, v0 */
} // .end local v1 # "killLevel":I
/* .restart local v0 # "killLevel":I */
/* .line 83 */
} // .end local v0 # "killLevel":I
/* .restart local v1 # "killLevel":I */
} // :cond_5
} // :goto_0
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).hasOverlayUi ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->hasOverlayUi()Z
/* if-nez v0, :cond_6 */
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).hasTopUi ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->hasTopUi()Z
/* if-nez v0, :cond_6 */
v0 = this.mState;
v0 = (( com.android.server.am.ProcessStateRecord ) v0 ).hasShownUi ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessStateRecord;->hasShownUi()Z
/* if-nez v0, :cond_6 */
v0 = this.info;
v0 = this.packageName;
/* .line 84 */
v0 = (( com.android.server.am.ProcessCleanerBase ) p0 ).isTrimMemoryEnable ( v0, v10 ); // invoke-virtual {p0, v0, v10}, Lcom/android/server/am/ProcessCleanerBase;->isTrimMemoryEnable(Ljava/lang/String;Lcom/android/server/am/ProcessManagerService;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 85 */
/* const/16 v0, 0x65 */
/* move v11, v0 */
} // .end local v1 # "killLevel":I
/* .restart local v0 # "killLevel":I */
/* .line 94 */
} // .end local v0 # "killLevel":I
/* .restart local v1 # "killLevel":I */
} // :cond_6
/* move v11, v1 */
} // .end local v1 # "killLevel":I
/* .local v11, "killLevel":I */
} // :goto_1
/* const/16 v0, 0x65 */
/* if-le v11, v0, :cond_7 */
/* .line 95 */
/* move-object/from16 v12, p3 */
/* move-object/from16 v13, p6 */
try { // :try_start_2
/* invoke-direct {p0, p1, v11, v12, v13}, Lcom/android/server/am/ProcessCleanerBase;->printKillLog(Lcom/android/server/am/ProcessRecord;ILjava/lang/String;Ljava/lang/String;)V */
/* .line 94 */
} // :cond_7
/* move-object/from16 v12, p3 */
/* move-object/from16 v13, p6 */
/* .line 97 */
} // :goto_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 98 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object/from16 v3, p3 */
/* move v4, v11 */
/* move-object/from16 v5, p7 */
/* move-object/from16 v6, p8 */
try { // :try_start_3
/* invoke-virtual/range {v1 ..v6}, Lcom/android/server/am/ProcessCleanerBase;->killOnce(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;ILandroid/os/Handler;Landroid/content/Context;)V */
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 101 */
/* .line 99 */
/* :catch_0 */
/* move-exception v0 */
/* move v1, v11 */
/* .line 97 */
/* :catchall_0 */
/* move-exception v0 */
/* move v1, v11 */
} // .end local v11 # "killLevel":I
/* .restart local v1 # "killLevel":I */
/* :catchall_1 */
/* move-exception v0 */
/* move-object/from16 v12, p3 */
/* move-object/from16 v13, p6 */
} // :goto_3
try { // :try_start_4
/* monitor-exit v2 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
} // .end local v1 # "killLevel":I
} // .end local p0 # "this":Lcom/android/server/am/ProcessCleanerBase;
} // .end local p1 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local p2 # "policy":I
} // .end local p3 # "killReason":Ljava/lang/String;
} // .end local p4 # "canForceStop":Z
} // .end local p5 # "pms":Lcom/android/server/am/ProcessManagerService;
} // .end local p6 # "killTag":Ljava/lang/String;
} // .end local p7 # "handler":Landroid/os/Handler;
} // .end local p8 # "context":Landroid/content/Context;
try { // :try_start_5
/* throw v0 */
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_1 */
/* .line 99 */
/* .restart local v1 # "killLevel":I */
/* .restart local p0 # "this":Lcom/android/server/am/ProcessCleanerBase; */
/* .restart local p1 # "app":Lcom/android/server/am/ProcessRecord; */
/* .restart local p2 # "policy":I */
/* .restart local p3 # "killReason":Ljava/lang/String; */
/* .restart local p4 # "canForceStop":Z */
/* .restart local p5 # "pms":Lcom/android/server/am/ProcessManagerService; */
/* .restart local p6 # "killTag":Ljava/lang/String; */
/* .restart local p7 # "handler":Landroid/os/Handler; */
/* .restart local p8 # "context":Landroid/content/Context; */
/* :catch_1 */
/* move-exception v0 */
/* .line 97 */
/* :catchall_2 */
/* move-exception v0 */
/* .line 99 */
/* :catch_2 */
/* move-exception v0 */
/* move-object/from16 v12, p3 */
/* move-object/from16 v13, p6 */
/* .line 100 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_4
final String v2 = "ProcessCleanerBase"; // const-string v2, "ProcessCleanerBase"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "killOnce:reason "; // const-string v4, "killOnce:reason "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* move v11, v1 */
/* .line 102 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end local v1 # "killLevel":I
/* .restart local v11 # "killLevel":I */
} // :goto_5
return;
} // .end method
void killOnce ( com.android.server.am.ProcessRecord p0, java.lang.String p1, Integer p2, android.os.Handler p3, android.content.Context p4 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .param p3, "killLevel" # I */
/* .param p4, "handler" # Landroid/os/Handler; */
/* .param p5, "context" # Landroid/content/Context; */
/* .line 107 */
try { // :try_start_0
v0 = (( com.android.server.am.ProcessCleanerBase ) p0 ).checkProcessDied ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;->checkProcessDied(Lcom/android/server/am/ProcessRecord;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 108 */
return;
/* .line 111 */
} // :cond_0
v0 = (( com.android.server.am.ProcessCleanerBase ) p0 ).checkKillFMApp ( p1, p4, p5 ); // invoke-virtual {p0, p1, p4, p5}, Lcom/android/server/am/ProcessCleanerBase;->checkKillFMApp(Lcom/android/server/am/ProcessRecord;Landroid/os/Handler;Landroid/content/Context;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 112 */
return;
/* .line 114 */
} // :cond_1
/* const/16 v0, 0x65 */
/* if-ne p3, v0, :cond_2 */
/* .line 115 */
(( com.android.server.am.ProcessCleanerBase ) p0 ).trimMemory ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;->trimMemory(Lcom/android/server/am/ProcessRecord;)V
/* .line 116 */
} // :cond_2
/* const/16 v0, 0x66 */
/* if-ne p3, v0, :cond_3 */
/* .line 117 */
(( com.android.server.am.ProcessCleanerBase ) p0 ).killBackgroundApplication ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ProcessCleanerBase;->killBackgroundApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 118 */
} // :cond_3
/* const/16 v0, 0x67 */
/* if-ne p3, v0, :cond_4 */
/* .line 119 */
v0 = this.mAMS;
/* monitor-enter v0 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 120 */
try { // :try_start_1
(( com.android.server.am.ProcessCleanerBase ) p0 ).killApplicationLock ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/android/server/am/ProcessCleanerBase;->killApplicationLock(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
/* .line 121 */
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local p0 # "this":Lcom/android/server/am/ProcessCleanerBase;
} // .end local p1 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local p2 # "reason":Ljava/lang/String;
} // .end local p3 # "killLevel":I
} // .end local p4 # "handler":Landroid/os/Handler;
} // .end local p5 # "context":Landroid/content/Context;
try { // :try_start_2
/* throw v1 */
/* .line 122 */
/* .restart local p0 # "this":Lcom/android/server/am/ProcessCleanerBase; */
/* .restart local p1 # "app":Lcom/android/server/am/ProcessRecord; */
/* .restart local p2 # "reason":Ljava/lang/String; */
/* .restart local p3 # "killLevel":I */
/* .restart local p4 # "handler":Landroid/os/Handler; */
/* .restart local p5 # "context":Landroid/content/Context; */
} // :cond_4
/* const/16 v0, 0x68 */
/* if-ne p3, v0, :cond_5 */
/* .line 123 */
v0 = this.info;
v0 = this.packageName;
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
(( com.android.server.am.ProcessCleanerBase ) p0 ).forceStopPackage ( v0, v1, p2 ); // invoke-virtual {p0, v0, v1, p2}, Lcom/android/server/am/ProcessCleanerBase;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 127 */
} // :cond_5
} // :goto_0
/* .line 125 */
/* :catch_0 */
/* move-exception v0 */
/* .line 126 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "ProcessCleanerBase"; // const-string v1, "ProcessCleanerBase"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "killOnce:reason "; // const-string v3, "killOnce:reason "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 128 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
void printWhiteList ( miui.process.ProcessConfig p0, java.util.List p1 ) {
/* .locals 4 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lmiui/process/ProcessConfig;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 319 */
/* .local p2, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* sget-boolean v0, Landroid/os/spc/PressureStateSettings;->DEBUG_ALL:Z */
/* if-nez v0, :cond_0 */
/* .line 320 */
return;
/* .line 322 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "reason="; // const-string v1, "reason="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( miui.process.ProcessConfig ) p1 ).getPolicy ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I
(( com.android.server.am.ProcessCleanerBase ) p0 ).getKillReason ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/ProcessCleanerBase;->getKillReason(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " whiteList="; // const-string v1, " whiteList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 323 */
/* .local v0, "info":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 324 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 323 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 326 */
} // .end local v1 # "i":I
} // :cond_1
final String v1 = "ProcessCleanerBase"; // const-string v1, "ProcessCleanerBase"
android.util.Slog .d ( v1,v0 );
/* .line 327 */
return;
} // .end method
void removeAllTasks ( Integer p0, com.android.server.am.ProcessManagerService p1 ) {
/* .locals 5 */
/* .param p1, "userId" # I */
/* .param p2, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .line 161 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 163 */
/* .local v0, "token":J */
try { // :try_start_0
(( com.android.server.am.ProcessManagerService ) p2 ).getInternal ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessManagerService;->getInternal()Lcom/miui/server/process/ProcessManagerInternal;
v3 = this.mAMS;
v3 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .removeAllTasks ( v2,p1,v3 );
/* .line 164 */
/* if-nez p1, :cond_0 */
/* .line 165 */
(( com.android.server.am.ProcessManagerService ) p2 ).getInternal ( ); // invoke-virtual {p2}, Lcom/android/server/am/ProcessManagerService;->getInternal()Lcom/miui/server/process/ProcessManagerInternal;
v3 = this.mAMS;
v3 = this.mActivityTaskManager;
/* const/16 v4, 0x3e7 */
com.android.server.wm.WindowProcessUtils .removeAllTasks ( v2,v4,v3 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 168 */
} // :cond_0
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 169 */
/* nop */
/* .line 170 */
return;
/* .line 168 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 169 */
/* throw v2 */
} // .end method
void removeTaskIfNeeded ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "taskId" # I */
/* .line 185 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 187 */
/* .local v0, "token":J */
try { // :try_start_0
v2 = this.mAMS;
v2 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .removeTask ( p1,v2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 189 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 190 */
/* nop */
/* .line 191 */
return;
/* .line 189 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 190 */
/* throw v2 */
} // .end method
void removeTasksIfNeeded ( java.util.List p0, java.util.Set p1, java.util.List p2, java.util.List p3, com.android.server.am.ProcessPolicy p4 ) {
/* .locals 9 */
/* .param p5, "procPolicy" # Lcom/android/server/am/ProcessPolicy; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Lcom/android/server/am/ProcessPolicy;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 175 */
/* .local p1, "taskIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p2, "whiteTaskSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local p3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p4, "whiteListTaskId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 177 */
/* .local v0, "token":J */
try { // :try_start_0
v2 = this.mAMS;
v6 = this.mActivityTaskManager;
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p5 */
/* move-object v7, p3 */
/* move-object v8, p4 */
/* invoke-static/range {v3 ..v8}, Lcom/android/server/wm/WindowProcessUtils;->removeTasks(Ljava/util/List;Ljava/util/Set;Lcom/android/server/am/IProcessPolicy;Lcom/android/server/wm/ActivityTaskManagerService;Ljava/util/List;Ljava/util/List;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 180 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 181 */
/* nop */
/* .line 182 */
return;
/* .line 180 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 181 */
/* throw v2 */
} // .end method
void removeTasksIfNeeded ( miui.process.ProcessConfig p0, com.android.server.am.ProcessPolicy p1, java.util.List p2, java.util.Map p3 ) {
/* .locals 10 */
/* .param p1, "config" # Lmiui/process/ProcessConfig; */
/* .param p2, "mProcessPolicy" # Lcom/android/server/am/ProcessPolicy; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lmiui/process/ProcessConfig;", */
/* "Lcom/android/server/am/ProcessPolicy;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 303 */
/* .local p3, "policyWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p4, "fgTaskPackageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;" */
(( miui.process.ProcessConfig ) p1 ).getWhiteListTaskId ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getWhiteListTaskId()Ljava/util/List;
/* .line 304 */
/* .local v9, "whiteListTaskId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v0 = (( miui.process.ProcessConfig ) p1 ).isRemoveTaskNeeded ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->isRemoveTaskNeeded()Z
if ( v0 != null) { // if-eqz v0, :cond_2
(( miui.process.ProcessConfig ) p1 ).getRemovingTaskIdList ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getRemovingTaskIdList()Ljava/util/List;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 305 */
if ( p4 != null) { // if-eqz p4, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* move-object v2, v0 */
/* .line 306 */
/* .local v2, "fgTaskIdSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
v0 = (( miui.process.ProcessConfig ) p1 ).getPolicy ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getPolicy()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 307 */
v0 = this.mAMS;
v0 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .getAllTaskIdList ( v0 );
/* .line 309 */
/* .local v6, "taskList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* move-object v0, p0 */
/* move-object v1, v6 */
/* move-object v3, p3 */
/* move-object v4, v9 */
/* move-object v5, p2 */
/* invoke-virtual/range {v0 ..v5}, Lcom/android/server/am/ProcessCleanerBase;->removeTasksIfNeeded(Ljava/util/List;Ljava/util/Set;Ljava/util/List;Ljava/util/List;Lcom/android/server/am/ProcessPolicy;)V */
/* .line 311 */
} // .end local v6 # "taskList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .line 312 */
} // :cond_1
(( miui.process.ProcessConfig ) p1 ).getRemovingTaskIdList ( ); // invoke-virtual {p1}, Lmiui/process/ProcessConfig;->getRemovingTaskIdList()Ljava/util/List;
/* move-object v3, p0 */
/* move-object v5, v2 */
/* move-object v6, p3 */
/* move-object v7, v9 */
/* move-object v8, p2 */
/* invoke-virtual/range {v3 ..v8}, Lcom/android/server/am/ProcessCleanerBase;->removeTasksIfNeeded(Ljava/util/List;Ljava/util/Set;Ljava/util/List;Ljava/util/List;Lcom/android/server/am/ProcessPolicy;)V */
/* .line 316 */
} // .end local v2 # "fgTaskIdSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :cond_2
} // :goto_1
return;
} // .end method
void removeTasksInPackages ( java.util.List p0, Integer p1, com.android.server.am.ProcessPolicy p2 ) {
/* .locals 3 */
/* .param p2, "userId" # I */
/* .param p3, "procPolicy" # Lcom/android/server/am/ProcessPolicy; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I", */
/* "Lcom/android/server/am/ProcessPolicy;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 194 */
/* .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 196 */
/* .local v0, "token":J */
try { // :try_start_0
v2 = this.mAMS;
v2 = this.mActivityTaskManager;
com.android.server.wm.WindowProcessUtils .removeTasksInPackages ( p1,p2,p3,v2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 199 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 200 */
/* nop */
/* .line 201 */
return;
/* .line 199 */
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 200 */
/* throw v2 */
} // .end method
void scheduleTrimMemory ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "level" # I */
/* .line 250 */
v0 = this.mAMS;
/* monitor-enter v0 */
/* .line 251 */
try { // :try_start_0
v1 = (( com.android.server.am.ProcessCleanerBase ) p0 ).checkProcessDied ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessCleanerBase;->checkProcessDied(Lcom/android/server/am/ProcessRecord;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v1, :cond_0 */
/* .line 253 */
try { // :try_start_1
(( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 256 */
/* .line 254 */
/* :catch_0 */
/* move-exception v1 */
/* .line 255 */
/* .local v1, "e":Landroid/os/RemoteException; */
try { // :try_start_2
(( android.os.RemoteException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 258 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
/* monitor-exit v0 */
/* .line 259 */
return;
/* .line 258 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
public void systemReady ( android.content.Context p0, com.android.server.am.ProcessManagerService p1 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "pms" # Lcom/android/server/am/ProcessManagerService; */
/* .line 53 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v0;
/* .line 54 */
return;
} // .end method
void trimMemory ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 238 */
(( com.android.server.am.ProcessRecord ) p1 ).getWindowProcessController ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
v0 = (( com.android.server.wm.WindowProcessController ) v0 ).isInterestingToUser ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isInterestingToUser()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 239 */
return;
/* .line 242 */
} // :cond_0
v0 = this.info;
v0 = this.packageName;
final String v1 = "android"; // const-string v1, "android"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 243 */
/* const/16 v0, 0x3c */
(( com.android.server.am.ProcessCleanerBase ) p0 ).scheduleTrimMemory ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/am/ProcessCleanerBase;->scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V
/* .line 245 */
} // :cond_1
/* const/16 v0, 0x50 */
(( com.android.server.am.ProcessCleanerBase ) p0 ).scheduleTrimMemory ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/android/server/am/ProcessCleanerBase;->scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V
/* .line 247 */
} // :goto_0
return;
} // .end method
