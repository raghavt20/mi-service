class com.android.server.am.BroadcastQueueModernStubImpl$1 implements java.lang.Runnable {
	 /* .source "BroadcastQueueModernStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/BroadcastQueueModernStubImpl;->noteOperationLocked(IILjava/lang/String;Landroid/os/Handler;Lcom/android/server/am/BroadcastRecord;)I */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.BroadcastQueueModernStubImpl this$0; //synthetic
final android.content.Intent val$intent; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.android.server.am.BroadcastQueueModernStubImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/BroadcastQueueModernStubImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 635 */
this.this$0 = p1;
this.val$intent = p2;
/* iput p3, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;->val$uid:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 639 */
try { // :try_start_0
v0 = this.this$0;
com.android.server.am.BroadcastQueueModernStubImpl .-$$Nest$fgetmContext ( v0 );
v1 = this.val$intent;
/* new-instance v2, Landroid/os/UserHandle; */
/* iget v3, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;->val$uid:I */
/* .line 640 */
v3 = android.os.UserHandle .getUserId ( v3 );
/* invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V */
/* .line 639 */
(( android.content.Context ) v0 ).startActivityAsUser ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 643 */
/* .line 641 */
/* :catch_0 */
/* move-exception v0 */
/* .line 644 */
} // :goto_0
return;
} // .end method
