.class Lcom/android/server/am/AppStateManager$AppState$1;
.super Ljava/lang/Object;
.source "AppStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/AppStateManager$AppState;->updateCurrentScenarioAction(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/server/am/AppStateManager$AppState;

.field final synthetic val$action:I

.field final synthetic val$active:Z


# direct methods
.method constructor <init>(Lcom/android/server/am/AppStateManager$AppState;IZ)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/server/am/AppStateManager$AppState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 976
    iput-object p1, p0, Lcom/android/server/am/AppStateManager$AppState$1;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iput p2, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$action:I

    iput-boolean p3, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$active:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 979
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$1;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-object v0, v0, Lcom/android/server/am/AppStateManager$AppState;->this$0:Lcom/android/server/am/AppStateManager;

    invoke-static {v0}, Lcom/android/server/am/AppStateManager;->-$$Nest$fgetmSmartScenarioManager(Lcom/android/server/am/AppStateManager;)Lcom/miui/server/smartpower/SmartScenarioManager;

    move-result-object v0

    iget v1, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$action:I

    iget-object v2, p0, Lcom/android/server/am/AppStateManager$AppState$1;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-boolean v3, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$active:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/smartpower/SmartScenarioManager;->onAppActionChanged(ILcom/android/server/am/AppStateManager$AppState;Z)V

    .line 980
    sget-boolean v0, Lcom/android/server/am/AppStateManager;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 981
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$1;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " actions("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/AppStateManager$AppState$1;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    invoke-static {v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$fgetmScenarioActions(Lcom/android/server/am/AppStateManager$AppState;)I

    move-result v1

    .line 982
    invoke-static {v1}, Lcom/miui/server/smartpower/SmartScenarioManager;->actionTypeToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 981
    const-string v1, "SmartPower"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 985
    :cond_0
    iget v0, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$action:I

    const/16 v1, 0x400

    if-ne v0, v1, :cond_1

    .line 986
    iget-object v0, p0, Lcom/android/server/am/AppStateManager$AppState$1;->this$1:Lcom/android/server/am/AppStateManager$AppState;

    iget-boolean v1, p0, Lcom/android/server/am/AppStateManager$AppState$1;->val$active:Z

    invoke-static {v0, v1}, Lcom/android/server/am/AppStateManager$AppState;->-$$Nest$mrecordVideoPlayIfNeeded(Lcom/android/server/am/AppStateManager$AppState;Z)V

    .line 988
    :cond_1
    return-void
.end method
