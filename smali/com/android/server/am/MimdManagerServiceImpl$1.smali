.class Lcom/android/server/am/MimdManagerServiceImpl$1;
.super Landroid/content/BroadcastReceiver;
.source "MimdManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MimdManagerServiceImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MimdManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/am/MimdManagerServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MimdManagerServiceImpl;

    .line 198
    iput-object p1, p0, Lcom/android/server/am/MimdManagerServiceImpl$1;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 201
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.miui.powerkeeper_sleep_changed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    const-string/jumbo v1, "state"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 204
    .local v1, "state":I
    const/4 v2, 0x1

    if-ne v2, v1, :cond_0

    .line 205
    iget-object v2, p0, Lcom/android/server/am/MimdManagerServiceImpl$1;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v2}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/MimdManagerServiceImpl$1;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v3}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 206
    .local v2, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/am/MimdManagerServiceImpl$1;->this$0:Lcom/android/server/am/MimdManagerServiceImpl;

    invoke-static {v3}, Lcom/android/server/am/MimdManagerServiceImpl;->-$$Nest$fgetmMimdManagerHandler(Lcom/android/server/am/MimdManagerServiceImpl;)Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/server/am/MimdManagerServiceImpl$MimdManagerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 209
    .end local v1    # "state":I
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method
