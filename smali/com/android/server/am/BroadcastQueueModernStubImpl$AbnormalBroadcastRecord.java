class com.android.server.am.BroadcastQueueModernStubImpl$AbnormalBroadcastRecord {
	 /* .source "BroadcastQueueModernStubImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/BroadcastQueueModernStubImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "AbnormalBroadcastRecord" */
} // .end annotation
/* # instance fields */
java.lang.String action;
java.lang.String callerPackage;
Integer userId;
/* # direct methods */
 com.android.server.am.BroadcastQueueModernStubImpl$AbnormalBroadcastRecord ( ) {
/* .locals 1 */
/* .param p1, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .line 845 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 846 */
v0 = this.intent;
(( android.content.Intent ) v0 ).getAction ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;
this.action = v0;
/* .line 847 */
v0 = this.callerPackage;
this.callerPackage = v0;
/* .line 848 */
/* iget v0, p1, Lcom/android/server/am/BroadcastRecord;->userId:I */
/* iput v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I */
/* .line 849 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 853 */
/* instance-of v0, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 854 */
	 /* move-object v0, p1 */
	 /* check-cast v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
	 /* .line 855 */
	 /* .local v0, "r":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
	 v1 = this.action;
	 v2 = this.action;
	 v1 = 	 android.text.TextUtils .equals ( v1,v2 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 v1 = this.callerPackage;
		 v2 = this.callerPackage;
		 /* .line 856 */
		 v1 = 		 android.text.TextUtils .equals ( v1,v2 );
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* iget v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I */
			 /* iget v2, v0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I */
			 /* if-ne v1, v2, :cond_0 */
			 int v1 = 1; // const/4 v1, 0x1
		 } // :cond_0
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 855 */
	 } // :goto_0
	 /* .line 859 */
} // .end local v0 # "r":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
} // :cond_1
v0 = /* invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z */
} // .end method
public Integer hashCode ( ) {
/* .locals 4 */
/* .line 864 */
int v0 = 1; // const/4 v0, 0x1
/* .line 865 */
/* .local v0, "hashCode":I */
/* mul-int/lit8 v1, v0, 0x1f */
v2 = this.action;
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* move v2, v3 */
} // :cond_0
v2 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
} // :goto_0
/* add-int/2addr v1, v2 */
/* .line 866 */
} // .end local v0 # "hashCode":I
/* .local v1, "hashCode":I */
/* mul-int/lit8 v0, v1, 0x1f */
v2 = this.callerPackage;
/* if-nez v2, :cond_1 */
} // :cond_1
v3 = (( java.lang.String ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/String;->hashCode()I
} // :goto_1
/* add-int/2addr v0, v3 */
/* .line 867 */
} // .end local v1 # "hashCode":I
/* .restart local v0 # "hashCode":I */
/* mul-int/lit8 v1, v0, 0x1f */
/* iget v2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I */
java.lang.Integer .valueOf ( v2 );
v2 = (( java.lang.Integer ) v2 ).hashCode ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I
/* add-int/2addr v1, v2 */
/* .line 868 */
} // .end local v0 # "hashCode":I
/* .restart local v1 # "hashCode":I */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 873 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AbnormalBroadcastRecord{action=\'"; // const-string v1, "AbnormalBroadcastRecord{action=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.action;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", callerPackage=\'"; // const-string v2, ", callerPackage=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.callerPackage;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", userId="; // const-string v1, ", userId="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
