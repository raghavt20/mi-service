.class Lcom/android/server/am/ProcessPowerCleaner$H;
.super Landroid/os/Handler;
.source "ProcessPowerCleaner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessPowerCleaner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field public static final AUTO_LOCK_OFF_EVENT:I = 0x4

.field public static final KILL_ALL_EVENT:I = 0x2

.field public static final SCREEN_OFF_EVENT:I = 0x10

.field public static final SCREEN_OFF_FROZEN_EVENT:I = 0x5

.field public static final SCREEN_ON_EVENT:I = 0xf

.field public static final THERMAL_KILL_ALL_EVENT:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessPowerCleaner;


# direct methods
.method public constructor <init>(Lcom/android/server/am/ProcessPowerCleaner;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 99
    iput-object p1, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    .line 100
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 101
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 105
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 106
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/process/ProcessConfig;

    .line 107
    .local v0, "config":Lmiui/process/ProcessConfig;
    iget v1, p1, Landroid/os/Message;->what:I

    const-wide/32 v2, 0x80000

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_1

    .line 132
    :sswitch_0
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v1}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$mhandleScreenOffEvent(Lcom/android/server/am/ProcessPowerCleaner;)V

    .line 133
    goto/16 :goto_1

    .line 129
    :sswitch_1
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v1}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$mresetLockOffConfig(Lcom/android/server/am/ProcessPowerCleaner;)V

    .line 130
    goto/16 :goto_1

    .line 135
    :sswitch_2
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v1}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$mpowerFrozenAll(Lcom/android/server/am/ProcessPowerCleaner;)V

    .line 136
    goto/16 :goto_1

    .line 123
    :sswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PowerScreenOffKill:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    .line 124
    const/16 v5, 0x16

    invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 125
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-virtual {v1}, Lcom/android/server/am/ProcessPowerCleaner;->handleAutoLockOff()V

    .line 126
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 127
    goto :goto_1

    .line 115
    :sswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PowerKillAll:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    .line 116
    invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 117
    nop

    .line 118
    invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v1

    const/16 v4, 0x14

    if-eq v1, v4, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 119
    .local v1, "isKillSystemProc":Z
    :goto_0
    iget-object v4, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v4, v0, v1}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$mhandleKillAll(Lcom/android/server/am/ProcessPowerCleaner;Lmiui/process/ProcessConfig;Z)V

    .line 120
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 121
    goto :goto_1

    .line 109
    .end local v1    # "isKillSystemProc":Z
    :sswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PowerThermalKillProc:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    .line 110
    invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v2, v3, v1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/android/server/am/ProcessPowerCleaner$H;->this$0:Lcom/android/server/am/ProcessPowerCleaner;

    invoke-static {v1, v0}, Lcom/android/server/am/ProcessPowerCleaner;->-$$Nest$mhandleThermalKillProc(Lcom/android/server/am/ProcessPowerCleaner;Lmiui/process/ProcessConfig;)V

    .line 112
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 113
    nop

    .line 140
    :goto_1
    return-void

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_4
        0x4 -> :sswitch_3
        0x5 -> :sswitch_2
        0xf -> :sswitch_1
        0x10 -> :sswitch_0
    .end sparse-switch
.end method
