class com.android.server.am.PeriodicCleanerService$2 extends android.database.ContentObserver {
	 /* .source "PeriodicCleanerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/PeriodicCleanerService;->registerCloudWhiteListObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.PeriodicCleanerService this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.android.server.am.PeriodicCleanerService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/PeriodicCleanerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 349 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 352 */
if ( p2 != null) { // if-eqz p2, :cond_2
	 final String v0 = "cloud_periodic_white_list"; // const-string v0, "cloud_periodic_white_list"
	 android.provider.Settings$System .getUriFor ( v0 );
	 v1 = 	 (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 353 */
		 v1 = this.val$context;
		 /* .line 354 */
		 (( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 /* .line 353 */
		 int v2 = -2; // const/4 v2, -0x2
		 android.provider.Settings$System .getStringForUser ( v1,v0,v2 );
		 /* .line 356 */
		 /* .local v0, "whiteListStr":Ljava/lang/String; */
		 final String v1 = ","; // const-string v1, ","
		 (( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
		 /* .line 357 */
		 /* .local v1, "whiteList":[Ljava/lang/String; */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .local v2, "i":I */
	 } // :goto_0
	 /* array-length v3, v1 */
	 /* if-ge v2, v3, :cond_1 */
	 /* .line 358 */
	 com.android.server.am.PeriodicCleanerService .-$$Nest$sfgetsCleanWhiteList ( );
	 v3 = 	 /* aget-object v4, v1, v2 */
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* .line 359 */
	 } // :cond_0
	 com.android.server.am.PeriodicCleanerService .-$$Nest$sfgetsCleanWhiteList ( );
	 /* aget-object v4, v1, v2 */
	 /* .line 357 */
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 361 */
} // .end local v2 # "i":I
} // :cond_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "cloud white list received, current list: "; // const-string v3, "cloud white list received, current list: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.PeriodicCleanerService .-$$Nest$sfgetsCleanWhiteList ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "PeriodicCleaner"; // const-string v3, "PeriodicCleaner"
android.util.Slog .w ( v3,v2 );
/* .line 363 */
} // .end local v0 # "whiteListStr":Ljava/lang/String;
} // .end local v1 # "whiteList":[Ljava/lang/String;
} // :cond_2
return;
} // .end method
