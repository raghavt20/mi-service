public class com.android.server.am.MemoryControlCloud {
	 /* .source "MemoryControlCloud.java" */
	 /* # static fields */
	 private static final java.lang.String CLOUD_MEMORY_STANDARD_APPHEAP_ENABLE;
	 private static final java.lang.String CLOUD_MEMORY_STANDARD_ENABLE;
	 private static final java.lang.String CLOUD_MEMORY_STANDARD_PROC_LIST;
	 private static final java.lang.String CLOUD_MEMORY_STANDARD_PROC_LIST_JSON;
	 private static final java.lang.String CLOUD_MEMORY_STANDRAD_APPHEAP_LIST_JSON;
	 private static final java.lang.String CLOUD_MRMORY_STANDARD_APPHEAP_HANDLE_TIME;
	 private static final java.lang.String CLOUD_SCREEN_OFF_STRATEGY;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.am.MemoryStandardProcessControl mspc;
	 /* # direct methods */
	 static com.android.server.am.MemoryStandardProcessControl -$$Nest$fgetmspc ( com.android.server.am.MemoryControlCloud p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mspc;
	 } // .end method
	 com.android.server.am.MemoryControlCloud ( ) {
		 /* .locals 1 */
		 /* .line 27 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 25 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mspc = v0;
		 /* .line 27 */
		 return;
	 } // .end method
	 private void getCloudAppHeapHandleTime ( android.content.Context p0 ) {
		 /* .locals 5 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 171 */
		 final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
		 /* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->registerCloudAppHeapHandleTimeObserver(Landroid/content/Context;)V */
		 /* .line 172 */
		 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
		 final String v2 = "cloud_memory_standard_appheap_handle_time"; // const-string v2, "cloud_memory_standard_appheap_handle_time"
		 int v3 = -2; // const/4 v3, -0x2
		 android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 175 */
			 try { // :try_start_0
				 (( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
				 android.provider.Settings$System .getStringForUser ( v1,v2,v3 );
				 /* .line 177 */
				 /* .local v1, "str":Ljava/lang/String; */
				 java.lang.Long .parseLong ( v1 );
				 /* move-result-wide v2 */
				 /* .line 178 */
				 /* .local v2, "handleTime":J */
				 v4 = this.mspc;
				 /* iput-wide v2, v4, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J */
				 /* :try_end_0 */
				 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .line 181 */
			 } // .end local v1 # "str":Ljava/lang/String;
		 } // .end local v2 # "handleTime":J
		 /* .line 179 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 180 */
		 /* .local v1, "e":Ljava/lang/Exception; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "cloud CLOUD_MRMORY_STANDARD_APPHEAP_HANDLE_TIME check failed: "; // const-string v3, "cloud CLOUD_MRMORY_STANDARD_APPHEAP_HANDLE_TIME check failed: "
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .e ( v0,v2 );
		 /* .line 182 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "cloud app heap handle time received: "; // const-string v2, "cloud app heap handle time received: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mspc;
/* iget-wide v2, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 184 */
} // :cond_0
return;
} // .end method
private void getCloudAppHeapList ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 240 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->registerCloudAppHeapListObserver(Landroid/content/Context;)V */
/* .line 241 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_memory_standard_appheap_list_json"; // const-string v1, "cloud_memory_standard_appheap_list_json"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 243 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 245 */
/* .local v0, "str":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
	 final String v1 = ""; // const-string v1, ""
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 248 */
	 } // :cond_0
	 v1 = this.mspc;
	 (( com.android.server.am.MemoryStandardProcessControl ) v1 ).callUpdateCloudAppHeapConfig ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->callUpdateCloudAppHeapConfig(Ljava/lang/String;)V
	 /* .line 246 */
} // :cond_1
} // :goto_0
return;
/* .line 250 */
} // .end local v0 # "str":Ljava/lang/String;
} // :cond_2
} // :goto_1
return;
} // .end method
private void getCloudProcList ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 207 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->registerCloudProcListObserver(Landroid/content/Context;)V */
/* .line 208 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_memory_standard_proc_list_json"; // const-string v1, "cloud_memory_standard_proc_list_json"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 210 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 212 */
/* .local v0, "str":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 215 */
} // :cond_0
v1 = this.mspc;
(( com.android.server.am.MemoryStandardProcessControl ) v1 ).callUpdateCloudConfig ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->callUpdateCloudConfig(Ljava/lang/String;)V
/* .line 213 */
} // :cond_1
} // :goto_0
return;
/* .line 217 */
} // .end local v0 # "str":Ljava/lang/String;
} // :cond_2
} // :goto_1
return;
} // .end method
private void getCloudSwitch ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 108 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->registerCloudEnableObserver(Landroid/content/Context;)V */
/* .line 109 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_memory_standard_enable"; // const-string v1, "cloud_memory_standard_enable"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
int v3 = 0; // const/4 v3, 0x0
final String v4 = "MemoryStandardProcessControl"; // const-string v4, "MemoryStandardProcessControl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 112 */
try { // :try_start_0
v0 = this.mspc;
/* .line 113 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v5,v1,v2 );
/* .line 112 */
v1 = java.lang.Boolean .parseBoolean ( v1 );
/* iput-boolean v1, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 118 */
/* .line 115 */
/* :catch_0 */
/* move-exception v0 */
/* .line 116 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "cloud CLOUD_MEMORY_STANDARD_ENABLE check failed: "; // const-string v5, "cloud CLOUD_MEMORY_STANDARD_ENABLE check failed: "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v1 );
/* .line 117 */
v1 = this.mspc;
/* iput-boolean v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
/* .line 119 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set enable state from database: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mspc;
/* iget-boolean v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v0 );
/* .line 121 */
} // :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_memory_standard_screen_off_strategy"; // const-string v1, "cloud_memory_standard_screen_off_strategy"
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 124 */
try { // :try_start_1
v0 = this.mspc;
/* .line 125 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v5,v1,v2 );
/* .line 124 */
v1 = java.lang.Integer .parseInt ( v1 );
/* iput v1, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 131 */
/* .line 127 */
/* :catch_1 */
/* move-exception v0 */
/* .line 128 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "cloud mScreenOffStrategy check failed: "; // const-string v5, "cloud mScreenOffStrategy check failed: "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v1 );
/* .line 129 */
v1 = this.mspc;
/* iput v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I */
/* .line 130 */
v1 = this.mspc;
/* iput-boolean v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
/* .line 132 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set screen off strategy from database: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mspc;
/* iget v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v0 );
/* .line 134 */
} // :cond_1
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_memory_standard_appheap_enable"; // const-string v1, "cloud_memory_standard_appheap_enable"
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 137 */
try { // :try_start_2
v0 = this.mspc;
/* .line 138 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v5,v1,v2 );
/* .line 137 */
v1 = java.lang.Boolean .parseBoolean ( v1 );
/* iput-boolean v1, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 143 */
/* .line 140 */
/* :catch_2 */
/* move-exception v0 */
/* .line 141 */
/* .restart local v0 # "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "cloud CLOUD_MEMORY_STANDARD_APPHEAP_ENABLE check failed: "; // const-string v2, "cloud CLOUD_MEMORY_STANDARD_APPHEAP_ENABLE check failed: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v1 );
/* .line 142 */
v1 = this.mspc;
/* iput-boolean v3, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
/* .line 144 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set appheap enable state from database: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mspc;
/* iget-boolean v1, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v0 );
/* .line 146 */
} // :cond_2
return;
} // .end method
private void registerCloudAppHeapHandleTimeObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 149 */
/* new-instance v0, Lcom/android/server/am/MemoryControlCloud$4; */
v1 = this.mspc;
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/MemoryControlCloud$4;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 165 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 166 */
final String v2 = "cloud_memory_standard_appheap_handle_time"; // const-string v2, "cloud_memory_standard_appheap_handle_time"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 165 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 168 */
return;
} // .end method
private void registerCloudAppHeapListObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 221 */
/* new-instance v0, Lcom/android/server/am/MemoryControlCloud$6; */
v1 = this.mspc;
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/MemoryControlCloud$6;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 234 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 235 */
final String v2 = "cloud_memory_standard_appheap_list_json"; // const-string v2, "cloud_memory_standard_appheap_list_json"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 234 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 237 */
return;
} // .end method
private void registerCloudEnableObserver ( android.content.Context p0 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 39 */
/* new-instance v0, Lcom/android/server/am/MemoryControlCloud$1; */
v1 = this.mspc;
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/MemoryControlCloud$1;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 61 */
/* .local v0, "enableObserver":Landroid/database/ContentObserver; */
/* new-instance v1, Lcom/android/server/am/MemoryControlCloud$2; */
v2 = this.mspc;
v2 = this.mHandler;
/* invoke-direct {v1, p0, v2, p1}, Lcom/android/server/am/MemoryControlCloud$2;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 79 */
/* .local v1, "strategyObserver":Landroid/database/ContentObserver; */
/* new-instance v2, Lcom/android/server/am/MemoryControlCloud$3; */
v3 = this.mspc;
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3, p1}, Lcom/android/server/am/MemoryControlCloud$3;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 96 */
/* .local v2, "appheapObserver":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 97 */
final String v4 = "cloud_memory_standard_enable"; // const-string v4, "cloud_memory_standard_enable"
android.provider.Settings$System .getUriFor ( v4 );
/* .line 96 */
int v5 = 0; // const/4 v5, 0x0
int v6 = -2; // const/4 v6, -0x2
(( android.content.ContentResolver ) v3 ).registerContentObserver ( v4, v5, v0, v6 ); // invoke-virtual {v3, v4, v5, v0, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 99 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 100 */
final String v4 = "cloud_memory_standard_screen_off_strategy"; // const-string v4, "cloud_memory_standard_screen_off_strategy"
android.provider.Settings$System .getUriFor ( v4 );
/* .line 99 */
(( android.content.ContentResolver ) v3 ).registerContentObserver ( v4, v5, v1, v6 ); // invoke-virtual {v3, v4, v5, v1, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 102 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 103 */
final String v4 = "cloud_memory_standard_appheap_enable"; // const-string v4, "cloud_memory_standard_appheap_enable"
android.provider.Settings$System .getUriFor ( v4 );
/* .line 102 */
(( android.content.ContentResolver ) v3 ).registerContentObserver ( v4, v5, v2, v6 ); // invoke-virtual {v3, v4, v5, v2, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 105 */
return;
} // .end method
private void registerCloudProcListObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 188 */
/* new-instance v0, Lcom/android/server/am/MemoryControlCloud$5; */
v1 = this.mspc;
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/android/server/am/MemoryControlCloud$5;-><init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 201 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 202 */
final String v2 = "cloud_memory_standard_proc_list_json"; // const-string v2, "cloud_memory_standard_proc_list_json"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 201 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 204 */
return;
} // .end method
/* # virtual methods */
public Boolean initCloud ( android.content.Context p0, com.android.server.am.MemoryStandardProcessControl p1 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "mspc" # Lcom/android/server/am/MemoryStandardProcessControl; */
/* .line 30 */
this.mspc = p2;
/* .line 31 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->getCloudSwitch(Landroid/content/Context;)V */
/* .line 32 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->getCloudProcList(Landroid/content/Context;)V */
/* .line 33 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->getCloudAppHeapList(Landroid/content/Context;)V */
/* .line 34 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryControlCloud;->getCloudAppHeapHandleTime(Landroid/content/Context;)V */
/* .line 35 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
