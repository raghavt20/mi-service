.class Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;
.super Landroid/os/ShellCommand;
.source "SmartPowerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/SmartPowerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SmartPowerShellCommand"
.end annotation


# instance fields
.field mService:Lcom/android/server/am/SmartPowerService;


# direct methods
.method constructor <init>(Lcom/android/server/am/SmartPowerService;)V
    .locals 0
    .param p1, "service"    # Lcom/android/server/am/SmartPowerService;

    .line 1411
    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    .line 1412
    iput-object p1, p0, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->mService:Lcom/android/server/am/SmartPowerService;

    .line 1413
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;

    .line 1417
    invoke-virtual {p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    .line 1418
    .local v0, "fd":Ljava/io/FileDescriptor;
    invoke-virtual {p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v1

    .line 1419
    .local v1, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->getAllArgs()[Ljava/lang/String;

    move-result-object v2

    .line 1420
    .local v2, "args":[Ljava/lang/String;
    if-nez p1, :cond_0

    .line 1421
    invoke-virtual {p0, p1}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v3

    return v3

    .line 1424
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->mService:Lcom/android/server/am/SmartPowerService;

    invoke-virtual {v3, v0, v1, v2}, Lcom/android/server/am/SmartPowerService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1427
    goto :goto_0

    .line 1425
    :catch_0
    move-exception v3

    .line 1426
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1428
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v3, -0x1

    return v3
.end method

.method public onHelp()V
    .locals 2

    .line 1433
    invoke-virtual {p0}, Lcom/android/server/am/SmartPowerService$SmartPowerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 1434
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string/jumbo v1, "smart power (smartpower) commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1435
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 1436
    const-string v1, "  policy"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1437
    const-string v1, "  appstate"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1438
    const-string v1, "  fz"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1439
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 1440
    return-void
.end method
