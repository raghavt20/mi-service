.class final Lcom/android/server/am/ProcessManagerService$LocalService;
.super Lcom/miui/server/process/ProcessManagerInternal;
.source "ProcessManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/ProcessManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/ProcessManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/am/ProcessManagerService;)V
    .locals 0

    .line 1328
    iput-object p1, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-direct {p0}, Lcom/miui/server/process/ProcessManagerInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessManagerService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/am/ProcessManagerService$LocalService;-><init>(Lcom/android/server/am/ProcessManagerService;)V

    return-void
.end method


# virtual methods
.method public checkAppFgServices(I)Z
    .locals 2
    .param p1, "pid"    # I

    .line 1425
    invoke-virtual {p0, p1}, Lcom/android/server/am/ProcessManagerService$LocalService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    .line 1426
    .local v0, "app":Lcom/android/server/am/ProcessRecord;
    if-eqz v0, :cond_0

    .line 1427
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v1, v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$misAppHasForegroundServices(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)Z

    move-result v1

    return v1

    .line 1428
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public forceStopPackage(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "evenForeground"    # Z

    .line 1493
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/ProcessKiller;->forceStopPackage(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)V

    .line 1494
    return-void
.end method

.method public forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 1367
    const-class v0, Landroid/app/ActivityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManagerInternal;

    .line 1368
    invoke-virtual {v0, p1, p2, p3}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 1369
    return-void
.end method

.method public getAllRunningProcessInfo()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lmiui/process/RunningProcessInfo;",
            ">;"
        }
    .end annotation

    .line 1407
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1408
    .local v0, "processInfoList":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    iget-object v1, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v1}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmActivityManagerService(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    monitor-enter v1

    .line 1409
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v2}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmActivityManagerService(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v2

    iget-object v2, v2, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    invoke-virtual {v2}, Lcom/android/server/am/ActivityManagerService$PidMap;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 1410
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v3}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmActivityManagerService(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/am/ActivityManagerService;->mPidsSelfLocked:Lcom/android/server/am/ActivityManagerService$PidMap;

    invoke-virtual {v3, v2}, Lcom/android/server/am/ActivityManagerService$PidMap;->valueAt(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v3

    .line 1411
    .local v3, "proc":Lcom/android/server/am/ProcessRecord;
    if-nez v3, :cond_0

    goto :goto_1

    .line 1412
    :cond_0
    iget-object v4, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v4, v3}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$mgenerateRunningProcessInfo(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)Lmiui/process/RunningProcessInfo;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1409
    .end local v3    # "proc":Lcom/android/server/am/ProcessRecord;
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 1414
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1

    return-object v0

    .line 1415
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getForegroundInfo()Lmiui/process/ForegroundInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1468
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getForegroundInfo()Lmiui/process/ForegroundInfo;

    move-result-object v0

    return-object v0
.end method

.method public getMiuiApplicationThread(I)Lmiui/process/IMiuiApplicationThread;
    .locals 1
    .param p1, "pid"    # I

    .line 1397
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmMiuiApplicationThreadManager(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/MiuiApplicationThreadManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiApplicationThreadManager;->getMiuiApplicationThread(I)Lmiui/process/IMiuiApplicationThread;

    move-result-object v0

    return-object v0
.end method

.method public getMultiWindowForegroundAppInfoLocked()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .line 1392
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmActivityManagerService(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mActivityTaskManager:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-static {v0}, Lcom/android/server/wm/WindowProcessUtils;->getMultiWindowForegroundAppInfoLocked(Lcom/android/server/wm/ActivityTaskManagerService;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    return-object v0
.end method

.method public getProcessPolicy()Lcom/android/server/am/IProcessPolicy;
    .locals 1

    .line 1513
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    return-object v0
.end method

.method public getProcessRecord(Ljava/lang/String;)Lcom/android/server/am/ProcessRecord;
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;

    .line 1478
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecord(Ljava/lang/String;)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    return-object v0
.end method

.method public getProcessRecord(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 1473
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessManagerService;->getProcessRecord(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    return-object v0
.end method

.method public getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;
    .locals 1
    .param p1, "pid"    # I

    .line 1483
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v0

    return-object v0
.end method

.method public isAllowRestartProcessLock(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/android/server/am/HostingRecord;)Z
    .locals 8
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "flag"    # I
    .param p3, "uid"    # I
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "callerPackage"    # Ljava/lang/String;
    .param p6, "hostingRecord"    # Lcom/android/server/am/HostingRecord;

    .line 1434
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmProcessStarter(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessStarter;

    move-result-object v1

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/android/server/am/ProcessStarter;->isAllowRestartProcessLock(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/android/server/am/HostingRecord;)Z

    move-result v0

    return v0
.end method

.method public isForegroundApp(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 1337
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmForegroundInfoManager(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/wm/ForegroundInfoManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/wm/ForegroundInfoManager;->isForegroundApp(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isInWhiteList(Lcom/android/server/am/ProcessRecord;II)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "userId"    # I
    .param p3, "policy"    # I

    .line 1508
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/ProcessManagerService;->isInWhiteList(Lcom/android/server/am/ProcessRecord;II)Z

    move-result v0

    return v0
.end method

.method public isLockedApplication(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 1463
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessManagerService;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public isPackageFastBootEnable(Ljava/lang/String;IZ)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "checkPass"    # Z

    .line 1402
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmProcessPolicy(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/ProcessPolicy;->isFastBootEnable(Ljava/lang/String;IZ)Z

    move-result v0

    return v0
.end method

.method public isTrimMemoryEnable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1458
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->isTrimMemoryEnable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "evenForeground"    # Z

    .line 1503
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z

    .line 1504
    return-void
.end method

.method public notifyActivityChanged(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "curComponentActivity"    # Landroid/content/ComponentName;

    .line 1347
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyActivityChanged(Landroid/content/ComponentName;)V

    .line 1348
    return-void
.end method

.method public notifyAmsProcessKill(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "reason"    # Ljava/lang/String;

    .line 1357
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0, p1, p2}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$mnotifyAmsProcessKill(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V

    .line 1358
    return-void
.end method

.method public notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/android/server/wm/FgActivityChangedInfo;

    .line 1332
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyForegroundInfoChanged(Lcom/android/server/wm/FgActivityChangedInfo;)V

    .line 1333
    return-void
.end method

.method public notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/android/server/wm/FgWindowChangedInfo;

    .line 1342
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessManagerService;->notifyForegroundWindowChanged(Lcom/android/server/wm/FgWindowChangedInfo;)V

    .line 1343
    return-void
.end method

.method public notifyLmkProcessKill(IIJIIIILjava/lang/String;)V
    .locals 10
    .param p1, "uid"    # I
    .param p2, "oomScore"    # I
    .param p3, "rssInBytes"    # J
    .param p5, "freeMemKb"    # I
    .param p6, "freeSwapKb"    # I
    .param p7, "killReason"    # I
    .param p8, "thrashing"    # I
    .param p9, "processName"    # Ljava/lang/String;

    .line 1452
    invoke-static {}, Lcom/android/server/am/MiProcessTracker;->getInstance()Lcom/android/server/am/MiProcessTracker;

    move-result-object v0

    move v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/android/server/am/MiProcessTracker;->recordLmkKillProcess(IIJIIIILjava/lang/String;)V

    .line 1454
    return-void
.end method

.method public notifyProcessDied(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1362
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0, p1}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$mnotifyProcessDied(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)V

    .line 1363
    return-void
.end method

.method public notifyProcessStarted(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;

    .line 1352
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0, p1}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$mnotifyProcessStarted(Lcom/android/server/am/ProcessManagerService;Lcom/android/server/am/ProcessRecord;)V

    .line 1353
    return-void
.end method

.method public restartDiedAppOrNot(Lcom/android/server/am/ProcessRecord;ZZZ)Z
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "isHomeApp"    # Z
    .param p3, "allowRestart"    # Z
    .param p4, "fromBinderDied"    # Z

    .line 1441
    if-eqz p4, :cond_0

    .line 1442
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmProcessStarter(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessStarter;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/server/am/ProcessStarter;->restartDiedAppOrNot(Lcom/android/server/am/ProcessRecord;ZZ)Z

    move-result v0

    return v0

    .line 1444
    :cond_0
    return p3
.end method

.method public setProcessMaxAdjLock(ILcom/android/server/am/ProcessRecord;II)V
    .locals 1
    .param p1, "userId"    # I
    .param p2, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p3, "maxAdj"    # I
    .param p4, "maxProcState"    # I

    .line 1488
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/am/ProcessManagerService;->setProcessMaxAdjLock(ILcom/android/server/am/ProcessRecord;II)V

    .line 1489
    return-void
.end method

.method public setSpeedTestState(Z)V
    .locals 1
    .param p1, "state"    # Z

    .line 1420
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmPreloadAppController(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/PreloadAppControllerImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/PreloadAppControllerImpl;->setSpeedTestState(Z)V

    .line 1421
    return-void
.end method

.method public trimMemory(Lcom/android/server/am/ProcessRecord;Z)V
    .locals 1
    .param p1, "app"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "evenForeground"    # Z

    .line 1498
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-virtual {v0}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ProcessKiller;->trimMemory(Lcom/android/server/am/ProcessRecord;Z)V

    .line 1499
    return-void
.end method

.method public updateEnterpriseWhiteList(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "isAdd"    # Z

    .line 1373
    iget-object v0, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v0}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmProcessPolicy(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessPolicy;

    move-result-object v0

    .line 1374
    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Lcom/android/server/am/ProcessPolicy;->getWhiteList(I)Ljava/util/List;

    move-result-object v0

    .line 1375
    .local v0, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    if-eqz p2, :cond_0

    .line 1376
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1377
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1378
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v3}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmProcessPolicy(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessPolicy;

    move-result-object v3

    invoke-virtual {v3, v1, v0, v2}, Lcom/android/server/am/ProcessPolicy;->addWhiteList(ILjava/util/List;Z)V

    goto :goto_0

    .line 1382
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1383
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1384
    iget-object v3, p0, Lcom/android/server/am/ProcessManagerService$LocalService;->this$0:Lcom/android/server/am/ProcessManagerService;

    invoke-static {v3}, Lcom/android/server/am/ProcessManagerService;->-$$Nest$fgetmProcessPolicy(Lcom/android/server/am/ProcessManagerService;)Lcom/android/server/am/ProcessPolicy;

    move-result-object v3

    invoke-virtual {v3, v1, v0, v2}, Lcom/android/server/am/ProcessPolicy;->addWhiteList(ILjava/util/List;Z)V

    .line 1388
    :cond_1
    :goto_0
    return-void
.end method
