.class public Lcom/android/server/am/MiuiProcessStubImpl;
.super Ljava/lang/Object;
.source "MiuiProcessStubImpl.java"

# interfaces
.implements Lcom/android/server/am/MiuiProcessStub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLaunchRtSchedDurationMs()J
    .locals 2

    .line 15
    const-wide/16 v0, 0x1f4

    return-wide v0
.end method

.method public getLiteAnimRtSchedDurationMs()J
    .locals 2

    .line 31
    const-wide/16 v0, 0x3e8

    return-wide v0
.end method

.method public getSchedModeAnimatorRt()I
    .locals 1

    .line 11
    const/4 v0, 0x1

    return v0
.end method

.method public getSchedModeBindBigCore()I
    .locals 1

    .line 46
    const/4 v0, 0x7

    return v0
.end method

.method public getSchedModeHomeAnimation()I
    .locals 1

    .line 39
    const/4 v0, 0x4

    return v0
.end method

.method public getSchedModeNormal()I
    .locals 1

    .line 19
    const/4 v0, 0x0

    return v0
.end method

.method public getSchedModeTouchRt()I
    .locals 1

    .line 35
    const/4 v0, 0x2

    return v0
.end method

.method public getSchedModeUnlock()I
    .locals 1

    .line 42
    const/4 v0, 0x5

    return v0
.end method

.method public getScrollRtSchedDurationMs()J
    .locals 2

    .line 23
    const-wide/16 v0, 0x1388

    return-wide v0
.end method

.method public getTouchRtSchedDurationMs()J
    .locals 2

    .line 27
    const-wide/16 v0, 0x7d0

    return-wide v0
.end method
