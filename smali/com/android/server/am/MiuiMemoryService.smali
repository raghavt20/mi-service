.class public Lcom/android/server/am/MiuiMemoryService;
.super Landroid/os/Binder;
.source "MiuiMemoryService.java"

# interfaces
.implements Lcom/android/server/am/MiuiMemoryServiceInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;,
        Lcom/android/server/am/MiuiMemoryService$Lifecycle;,
        Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;
    }
.end annotation


# static fields
.field public static final DEBUG:Z

.field private static final LMK_CAMERA_MODE:B = 0x12t

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui.memory.service"

.field private static final TAG:Ljava/lang/String; = "MiuiMemoryService"

.field private static final VMPRESS_POLICY_CHECK_IO:I = 0x4

.field private static final VMPRESS_POLICY_GRECLAM:I = 0x0

.field private static final VMPRESS_POLICY_GRECLAM_AND_PM:I = 0x3

.field private static final VMPRESS_POLICY_GSRECLAM:I = 0x1

.field private static final VMPRESS_POLICY_PRECLAM:I = 0x2

.field private static final VMPRESS_POLICY_TO_MI_PM:I = 0x11

.field private static sCompactSingleProcEnable:Z

.field private static sCompactionEnable:Z

.field private static sCompactionMinZramFreeKb:J

.field private static sWriteEnable:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLmkdOutputStream:Ljava/io/OutputStream;

.field private mLmkdSocket:Landroid/net/LocalSocket;

.field private mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

.field private mWriteLock:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$mhandleReclaimTrigger(Lcom/android/server/am/MiuiMemoryService;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/MiuiMemoryService;->handleReclaimTrigger(II)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 37
    nop

    .line 38
    const-string v0, "debug.sys.mms"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    .line 40
    nop

    .line 41
    const-string v0, "persist.sys.mms.compact_enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z

    .line 42
    nop

    .line 43
    const-string v0, "persist.sys.mms.single_compact_enable"

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactSingleProcEnable:Z

    .line 44
    nop

    .line 45
    const-string v0, "persist.sys.mms.min_zramfree_kb"

    const-wide/32 v2, 0x4b000

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/android/server/am/MiuiMemoryService;->sCompactionMinZramFreeKb:J

    .line 54
    nop

    .line 55
    const-string v0, "persist.sys.mms.write_lmkd"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sWriteEnable:Z

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 205
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mWriteLock:Ljava/lang/Object;

    .line 206
    iput-object p1, p0, Lcom/android/server/am/MiuiMemoryService;->mContext:Landroid/content/Context;

    .line 207
    new-instance v0, Lcom/android/server/am/MiuiMemReclaimer;

    invoke-direct {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    .line 208
    new-instance v0, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;-><init>(Lcom/android/server/am/MiuiMemoryService;Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread-IA;)V

    .line 209
    .local v0, "memServer":Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;
    invoke-virtual {v0}, Lcom/android/server/am/MiuiMemoryService$MiuiMemServiceThread;->start()V

    .line 210
    invoke-direct {p0, p1}, Lcom/android/server/am/MiuiMemoryService;->initHelper(Landroid/content/Context;)V

    .line 211
    const-class v1, Lcom/android/server/am/MiuiMemoryServiceInternal;

    invoke-static {v1, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 212
    return-void
.end method

.method private handleReclaimTrigger(II)V
    .locals 2
    .param p1, "vmPressureLevel"    # I
    .param p2, "vmPressurePolicy"    # I

    .line 156
    if-ltz p1, :cond_1

    const/4 v0, 0x3

    if-gt p1, v0, :cond_1

    if-ltz p2, :cond_1

    const/16 v1, 0x11

    if-le p2, v1, :cond_0

    goto :goto_1

    .line 164
    :cond_0
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 179
    :sswitch_0
    invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->triggerProcessClean()V

    .line 180
    goto :goto_0

    .line 177
    :sswitch_1
    goto :goto_0

    .line 173
    :sswitch_2
    invoke-virtual {p0, p1}, Lcom/android/server/am/MiuiMemoryService;->runGlobalCompaction(I)V

    .line 174
    invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->triggerProcessClean()V

    .line 175
    goto :goto_0

    .line 170
    :sswitch_3
    invoke-virtual {p0, v0}, Lcom/android/server/am/MiuiMemoryService;->runProcsCompaction(I)V

    .line 171
    goto :goto_0

    .line 167
    :sswitch_4
    invoke-virtual {p0, p1}, Lcom/android/server/am/MiuiMemoryService;->runGlobalCompaction(I)V

    .line 168
    nop

    .line 184
    :goto_0
    return-void

    .line 160
    :cond_1
    :goto_1
    const-string v0, "MiuiMemoryService"

    const-string v1, "mi_reclaim data is invalid!"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_4
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0x4 -> :sswitch_1
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method private initHelper(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 215
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z

    const-string v1, "MiuiMemoryService"

    if-nez v0, :cond_0

    .line 216
    const-string v0, "MiuiMemServiceHelper didn\'t init..."

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    return-void

    .line 220
    :cond_0
    new-instance v0, Lcom/android/server/am/MiuiMemServiceHelper;

    invoke-direct {v0, p1, p0}, Lcom/android/server/am/MiuiMemServiceHelper;-><init>(Landroid/content/Context;Lcom/android/server/am/MiuiMemoryService;)V

    .line 221
    .local v0, "memHelper":Lcom/android/server/am/MiuiMemServiceHelper;
    invoke-virtual {v0}, Lcom/android/server/am/MiuiMemServiceHelper;->startWork()V

    .line 222
    sget-boolean v2, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 223
    const-string v2, "MiuiMemServiceHelper init complete"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_1
    return-void
.end method

.method private isZramFreeEnough()Z
    .locals 4

    .line 228
    invoke-static {}, Landroid/os/Debug;->getZramFreeKb()J

    move-result-wide v0

    .line 229
    .local v0, "zramFreeKb":J
    sget-wide v2, Lcom/android/server/am/MiuiMemoryService;->sCompactionMinZramFreeKb:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 230
    sget-boolean v2, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 231
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Skipping compaction, zramFree too small. zramFree = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " KB"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiMemoryService"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_0
    const/4 v2, 0x0

    return v2

    .line 236
    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method private openLmkdSocket()Z
    .locals 6

    .line 261
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdSocket:Landroid/net/LocalSocket;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 262
    return v1

    .line 265
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Landroid/net/LocalSocket;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/net/LocalSocket;-><init>(I)V

    iput-object v2, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdSocket:Landroid/net/LocalSocket;

    .line 266
    new-instance v3, Landroid/net/LocalSocketAddress;

    const-string v4, "lmkd"

    sget-object v5, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v3, v4, v5}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    invoke-virtual {v2, v3}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 268
    iget-object v2, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdSocket:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdOutputStream:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    nop

    .line 274
    iget-object v2, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdSocket:Landroid/net/LocalSocket;

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v0

    :goto_0
    return v1

    .line 269
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "MiuiMemoryService"

    const-string v3, "Lmkd socket open failed"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdSocket:Landroid/net/LocalSocket;

    .line 272
    return v0
.end method

.method private triggerProcessClean()V
    .locals 2

    .line 187
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "MiuiMemoryService"

    const-string v1, "Call process cleaner"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_0
    invoke-static {}, Lcom/android/server/am/SystemPressureController;->getInstance()Lcom/android/server/am/SystemPressureController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/SystemPressureController;->triggerProcessClean()V

    .line 189
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 337
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mContext:Landroid/content/Context;

    const-string v1, "MiuiMemoryService"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 338
    :cond_0
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z

    if-nez v0, :cond_1

    .line 339
    const-string v0, "Compaction isn\'t enabled!"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 340
    return-void

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0, p2}, Lcom/android/server/am/MiuiMemReclaimer;->dumpCompactionStats(Ljava/io/PrintWriter;)V

    .line 343
    return-void
.end method

.method public interruptProcCompaction(I)V
    .locals 1
    .param p1, "pid"    # I

    .line 327
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->interruptProcCompaction(I)V

    .line 328
    return-void
.end method

.method public interruptProcsCompaction()V
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0}, Lcom/android/server/am/MiuiMemReclaimer;->interruptProcsCompaction()V

    .line 323
    return-void
.end method

.method public isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z
    .locals 1
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "mode"    # I

    .line 249
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->isCompactNeeded(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)Z

    move-result v0

    return v0
.end method

.method public performCompaction(Ljava/lang/String;I)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "pid"    # I

    .line 332
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->performCompaction(Ljava/lang/String;I)V

    .line 333
    return-void
.end method

.method public runGlobalCompaction(I)V
    .locals 1
    .param p1, "vmPressureLevel"    # I

    .line 306
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z

    if-nez v0, :cond_0

    .line 307
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->runGlobalCompaction(I)V

    .line 310
    return-void
.end method

.method public runProcCompaction(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V
    .locals 1
    .param p1, "proc"    # Lcom/miui/server/smartpower/IAppState$IRunningProcess;
    .param p2, "mode"    # I

    .line 241
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactSingleProcEnable:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->isZramFreeEnough()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/MiuiMemReclaimer;->runProcCompaction(Lcom/miui/server/smartpower/IAppState$IRunningProcess;I)V

    .line 245
    return-void

    .line 242
    :cond_1
    :goto_0
    return-void
.end method

.method public runProcsCompaction(I)V
    .locals 1
    .param p1, "mode"    # I

    .line 314
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactionEnable:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->isZramFreeEnough()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->runProcsCompaction(I)V

    .line 318
    return-void

    .line 315
    :cond_1
    :goto_0
    return-void
.end method

.method public setAppStartingMode(Z)V
    .locals 1
    .param p1, "appStarting"    # Z

    .line 254
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sCompactSingleProcEnable:Z

    if-nez v0, :cond_0

    .line 255
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mReclaimer:Lcom/android/server/am/MiuiMemReclaimer;

    invoke-virtual {v0, p1}, Lcom/android/server/am/MiuiMemReclaimer;->setAppStartingMode(Z)V

    .line 258
    return-void
.end method

.method public writeLmkd(Z)V
    .locals 6
    .param p1, "isForeground"    # Z

    .line 279
    sget-boolean v0, Lcom/android/server/am/MiuiMemoryService;->sWriteEnable:Z

    if-nez v0, :cond_0

    return-void

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/android/server/am/MiuiMemoryService;->mWriteLock:Ljava/lang/Object;

    monitor-enter v0

    .line 281
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdSocket:Landroid/net/LocalSocket;

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/server/am/MiuiMemoryService;->openLmkdSocket()Z

    move-result v1

    if-nez v1, :cond_1

    .line 282
    const-string v1, "MiuiMemoryService"

    const-string v2, "Write lmkd failed for socket error!"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    monitor-exit v0

    return-void

    .line 285
    :cond_1
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 286
    .local v1, "buf":Ljava/nio/ByteBuffer;
    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 287
    const/4 v2, 0x0

    if-eqz p1, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    move v3, v2

    :goto_0
    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    :try_start_1
    iget-object v3, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    invoke-virtual {v3, v4, v2, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 290
    sget-boolean v2, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z

    if-eqz v2, :cond_3

    const-string v2, "MiuiMemoryService"

    const-string v3, "Write lmkd success"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291
    :cond_3
    :try_start_2
    monitor-exit v0

    return-void

    .line 292
    :catch_0
    move-exception v2

    .line 293
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "MiuiMemoryService"

    const-string v4, "Write lmkd failed for IOException"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 295
    :try_start_3
    iget-object v3, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdSocket:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 298
    goto :goto_1

    .line 296
    :catch_1
    move-exception v3

    .line 297
    .local v3, "ex":Ljava/io/IOException;
    :try_start_4
    const-string v4, "MiuiMemoryService"

    const-string v5, "Close lmkd socket failed for IOException!"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    .end local v3    # "ex":Ljava/io/IOException;
    :goto_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/server/am/MiuiMemoryService;->mLmkdSocket:Landroid/net/LocalSocket;

    .line 301
    .end local v1    # "buf":Ljava/nio/ByteBuffer;
    .end local v2    # "e":Ljava/io/IOException;
    monitor-exit v0

    .line 302
    return-void

    .line 301
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method
