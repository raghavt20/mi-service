.class Lcom/android/server/am/BroadcastQueueModernStubImpl$1;
.super Ljava/lang/Object;
.source "BroadcastQueueModernStubImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/BroadcastQueueModernStubImpl;->noteOperationLocked(IILjava/lang/String;Landroid/os/Handler;Lcom/android/server/am/BroadcastRecord;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/BroadcastQueueModernStubImpl;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/android/server/am/BroadcastQueueModernStubImpl;Landroid/content/Intent;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/BroadcastQueueModernStubImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 635
    iput-object p1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;->this$0:Lcom/android/server/am/BroadcastQueueModernStubImpl;

    iput-object p2, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;->val$intent:Landroid/content/Intent;

    iput p3, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;->val$uid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 639
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;->this$0:Lcom/android/server/am/BroadcastQueueModernStubImpl;

    invoke-static {v0}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->-$$Nest$fgetmContext(Lcom/android/server/am/BroadcastQueueModernStubImpl;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;->val$intent:Landroid/content/Intent;

    new-instance v2, Landroid/os/UserHandle;

    iget v3, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;->val$uid:I

    .line 640
    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    .line 639
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 643
    goto :goto_0

    .line 641
    :catch_0
    move-exception v0

    .line 644
    :goto_0
    return-void
.end method
