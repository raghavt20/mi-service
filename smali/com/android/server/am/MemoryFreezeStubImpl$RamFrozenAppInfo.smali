.class public Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
.super Ljava/lang/Object;
.source "MemoryFreezeStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MemoryFreezeStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RamFrozenAppInfo"
.end annotation


# instance fields
.field private mFrozen:Z

.field private mPackageName:Ljava/lang/String;

.field private mPendingFreeze:Z

.field private mPid:I

.field private mUid:I

.field private mlastActivityStopTime:J

.field final synthetic this$0:Lcom/android/server/am/MemoryFreezeStubImpl;


# direct methods
.method static bridge synthetic -$$Nest$fgetmFrozen(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mFrozen:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageName(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingFreeze(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPendingFreeze:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPid(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUid(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)I
    .locals 0

    iget p0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mUid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmlastActivityStopTime(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mlastActivityStopTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fputmFrozen(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mFrozen:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPendingFreeze(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPendingFreeze:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPid(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;I)V
    .locals 0

    iput p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmlastActivityStopTime(Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mlastActivityStopTime:J

    return-void
.end method

.method public constructor <init>(Lcom/android/server/am/MemoryFreezeStubImpl;ILjava/lang/String;)V
    .locals 2
    .param p1, "this$0"    # Lcom/android/server/am/MemoryFreezeStubImpl;
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 663
    iput-object p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->this$0:Lcom/android/server/am/MemoryFreezeStubImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 664
    iput p2, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mUid:I

    .line 665
    iput-object p3, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPackageName:Ljava/lang/String;

    .line 666
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPendingFreeze:Z

    .line 667
    iput-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mFrozen:Z

    .line 668
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mlastActivityStopTime:J

    .line 669
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;->mPid:I

    .line 670
    return-void
.end method
