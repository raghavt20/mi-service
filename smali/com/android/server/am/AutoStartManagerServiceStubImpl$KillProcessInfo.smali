.class final Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;
.super Ljava/lang/Object;
.source "AutoStartManagerServiceStubImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/AutoStartManagerServiceStubImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "KillProcessInfo"
.end annotation


# instance fields
.field pid:I

.field proc:Lcom/android/server/am/ProcessRecord;

.field processName:Ljava/lang/String;

.field uid:I


# direct methods
.method constructor <init>(Lcom/android/server/am/ProcessRecord;)V
    .locals 1
    .param p1, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    nop

    .line 172
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I

    iput v0, p0, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->uid:I

    .line 173
    iget v0, p1, Lcom/android/server/am/ProcessRecord;->mPid:I

    iput v0, p0, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->pid:I

    .line 174
    iget-object v0, p1, Lcom/android/server/am/ProcessRecord;->processName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/am/AutoStartManagerServiceStubImpl$KillProcessInfo;->processName:Ljava/lang/String;

    .line 175
    return-void
.end method
