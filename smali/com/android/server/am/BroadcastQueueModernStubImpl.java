public class com.android.server.am.BroadcastQueueModernStubImpl implements com.android.server.am.BroadcastQueueModernStub {
	 /* .source "BroadcastQueueModernStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;, */
	 /* Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;, */
	 /* Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Float ABNORMAL_BROADCAST_RATE;
private static final java.lang.String ACTION_C2DM;
private static final java.lang.String ACTION_MIPUSH_MESSAGE_ARRIVED;
private static final java.util.Set ACTION_NFC;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final Integer ACTIVE_ORDERED_BROADCAST_LIMIT;
private static java.util.ArrayList BR_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lmiui/mqsas/sdk/event/BroadcastEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final Boolean DEBUG;
private static final java.util.Set DEFER_CACHED_WHITE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.lang.String EXTRA_PACKAGE_NAME;
public static final Integer FLAG_IMMUTABLE;
private static final Boolean IS_STABLE_VERSION;
private static final Integer MAX_QUANTITY;
public static final Integer OP_PROCESS_OUTGOING_CALLS;
static final java.lang.String TAG;
private static com.android.server.am.BroadcastDispatcherFeatureImpl broadcastDispatcherFeature;
private static volatile com.android.server.am.BroadcastQueueModernStubImpl$BRReportHandler mBRHandler;
private static java.util.ArrayList mBroadcastMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Long mDispatchThreshold;
private static Integer mFinishDeno;
private static Integer mIndex;
private static final java.util.List mInternationalSpecialAction;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.Object mObject;
private static java.util.concurrent.atomic.AtomicBoolean sAbnormalBroadcastWarning;
private static final java.util.Set sSpecialSkipAction;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Boolean sSystemBootCompleted;
private static final java.util.ArrayList sSystemSkipAction;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Integer mActivityRequestId;
private com.android.server.am.ActivityManagerService mAmService;
private android.content.Context mContext;
private miui.security.SecurityManagerInternal mSecurityInternal;
private android.os.PowerManager pm;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.android.server.am.BroadcastQueueModernStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$smisSystemBootCompleted ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.android.server.am.BroadcastQueueModernStubImpl .isSystemBootCompleted ( );
} // .end method
static com.android.server.am.BroadcastQueueModernStubImpl ( ) {
/* .locals 6 */
/* .line 77 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 79 */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
/* .line 80 */
final String v1 = "android.intent.action.LOCKED_BOOT_COMPLETED"; // const-string v1, "android.intent.action.LOCKED_BOOT_COMPLETED"
/* .line 105 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 107 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 108 */
int v0 = 0; // const/4 v0, 0x0
/* .line 109 */
/* new-instance v1, Ljava/lang/Object; */
/* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
/* .line 112 */
final String v1 = "persist.broadcast.time"; // const-string v1, "persist.broadcast.time"
/* const-wide/16 v2, 0xbb8 */
android.os.SystemProperties .getLong ( v1,v2,v3 );
/* move-result-wide v1 */
/* sput-wide v1, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mDispatchThreshold:J */
/* .line 113 */
final String v1 = "persist.broadcast.count"; // const-string v1, "persist.broadcast.count"
int v2 = 5; // const/4 v2, 0x5
v1 = android.os.SystemProperties .getInt ( v1,v2 );
/* .line 114 */
/* sget-boolean v1, Lmiui/os/Build;->IS_STABLE_VERSION:Z */
com.android.server.am.BroadcastQueueModernStubImpl.IS_STABLE_VERSION = (v1!= 0);
/* .line 117 */
final String v1 = "persist.activebr.limit"; // const-string v1, "persist.activebr.limit"
/* const/16 v2, 0x3e8 */
v1 = android.os.SystemProperties .getInt ( v1,v2 );
/* .line 119 */
/* new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean; */
/* invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V */
/* .line 129 */
/* new-instance v1, Lcom/android/server/am/BroadcastDispatcherFeatureImpl; */
/* invoke-direct {v1}, Lcom/android/server/am/BroadcastDispatcherFeatureImpl;-><init>()V */
/* .line 195 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 196 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* .line 197 */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 199 */
/* new-instance v4, Ljava/util/HashSet; */
/* invoke-direct {v4}, Ljava/util/HashSet;-><init>()V */
/* .line 202 */
final String v5 = "android.accounts.LOGIN_ACCOUNTS_PRE_CHANGED"; // const-string v5, "android.accounts.LOGIN_ACCOUNTS_PRE_CHANGED"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 203 */
final String v5 = "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"; // const-string v5, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 204 */
final String v5 = "android.provider.Telephony.SECRET_CODE"; // const-string v5, "android.provider.Telephony.SECRET_CODE"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 205 */
final String v5 = "com.android.updater.action.UPDATE_SUCCESSED"; // const-string v5, "com.android.updater.action.UPDATE_SUCCESSED"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 206 */
final String v5 = "com.android.updater.action.OTA_UPDATE_SUCCESSED"; // const-string v5, "com.android.updater.action.OTA_UPDATE_SUCCESSED"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 207 */
final String v5 = "miui.media.AUDIO_VOIP_RECORD_STATE_CHANGED_ACTION"; // const-string v5, "miui.media.AUDIO_VOIP_RECORD_STATE_CHANGED_ACTION"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 208 */
final String v5 = "com.android.settings.stylus.STYLUS_STATE_SOC"; // const-string v5, "com.android.settings.stylus.STYLUS_STATE_SOC"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 209 */
final String v5 = "com.android.settings.stylus.STYLUS_BATTERY_NOTIFY"; // const-string v5, "com.android.settings.stylus.STYLUS_BATTERY_NOTIFY"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 210 */
final String v5 = "com.android.settings.stylus.STYLUS_PLACE_ERROR"; // const-string v5, "com.android.settings.stylus.STYLUS_PLACE_ERROR"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 211 */
final String v5 = "com.xiaomi.bluetooth.action.KEYBOARD_ATTACH"; // const-string v5, "com.xiaomi.bluetooth.action.KEYBOARD_ATTACH"
(( java.util.ArrayList ) v1 ).add ( v5 ); // invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 212 */
final String v5 = "is_pad"; // const-string v5, "is_pad"
v0 = miui.util.FeatureParser .getBoolean ( v5,v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 213 */
final String v0 = "miui.intent.action.ACTION_POGO_CONNECTED_STATE"; // const-string v0, "miui.intent.action.ACTION_POGO_CONNECTED_STATE"
(( java.util.ArrayList ) v1 ).add ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 215 */
} // :cond_0
final String v0 = "android.intent.action.MEDIA_BUTTON"; // const-string v0, "android.intent.action.MEDIA_BUTTON"
/* .line 216 */
final String v0 = "android.appwidget.action.APPWIDGET_ENABLED"; // const-string v0, "android.appwidget.action.APPWIDGET_ENABLED"
/* .line 217 */
final String v0 = "android.appwidget.action.APPWIDGET_DISABLED"; // const-string v0, "android.appwidget.action.APPWIDGET_DISABLED"
/* .line 218 */
final String v0 = "android.appwidget.action.APPWIDGET_UPDATE"; // const-string v0, "android.appwidget.action.APPWIDGET_UPDATE"
/* .line 219 */
final String v0 = "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"; // const-string v0, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"
/* .line 220 */
final String v0 = "android.appwidget.action.APPWIDGET_DELETED"; // const-string v0, "android.appwidget.action.APPWIDGET_DELETED"
/* .line 221 */
final String v0 = "android.appwidget.action.APPWIDGET_RESTORED"; // const-string v0, "android.appwidget.action.APPWIDGET_RESTORED"
/* .line 222 */
final String v0 = "miui.intent.action.contentcatcher"; // const-string v0, "miui.intent.action.contentcatcher"
/* .line 223 */
final String v0 = "com.miui.contentextension.action.PACKAGE"; // const-string v0, "com.miui.contentextension.action.PACKAGE"
/* .line 224 */
final String v0 = "com.miui.nfc.action.TRANSACTION"; // const-string v0, "com.miui.nfc.action.TRANSACTION"
/* .line 225 */
final String v0 = "com.miui.intent.action.SWIPE_CARD"; // const-string v0, "com.miui.intent.action.SWIPE_CARD"
/* .line 226 */
final String v0 = "com.miui.nfc.action.RF_ON"; // const-string v0, "com.miui.nfc.action.RF_ON"
/* .line 227 */
final String v0 = "com.miui.intent.action.DOUBLE_CLICK"; // const-string v0, "com.miui.intent.action.DOUBLE_CLICK"
/* .line 228 */
final String v0 = "com.android.nfc_extras.action.RF_FIELD_OFF_DETECTED"; // const-string v0, "com.android.nfc_extras.action.RF_FIELD_OFF_DETECTED"
/* .line 229 */
final String v0 = "android.nfc.action.TRANSACTION_DETECTED"; // const-string v0, "android.nfc.action.TRANSACTION_DETECTED"
/* .line 231 */
final String v0 = "com.xiaomi.market"; // const-string v0, "com.xiaomi.market"
/* .line 232 */
final String v0 = "com.xiaomi.mipicks"; // const-string v0, "com.xiaomi.mipicks"
/* .line 233 */
return;
} // .end method
public com.android.server.am.BroadcastQueueModernStubImpl ( ) {
/* .locals 1 */
/* .line 72 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 122 */
int v0 = 0; // const/4 v0, 0x0
this.pm = v0;
return;
} // .end method
private Boolean checkSpecialAction ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 248 */
if ( p2 != null) { // if-eqz p2, :cond_1
v0 = v0 = com.android.server.am.BroadcastQueueModernStubImpl.sSpecialSkipAction;
/* if-nez v0, :cond_0 */
v0 = v0 = com.android.server.am.BroadcastQueueModernStubImpl.ACTION_NFC;
/* if-nez v0, :cond_0 */
/* .line 249 */
final String v0 = "com.xiaomi.mipush.RECEIVE_MESSAGE"; // const-string v0, "com.xiaomi.mipush.RECEIVE_MESSAGE"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 250 */
} // :cond_0
v0 = (( com.android.server.am.ProcessRecord ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getPid()I
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
int v2 = 1; // const/4 v2, 0x1
com.android.server.am.BroadcastQueueModernStubImpl .notifyPowerKeeperEvent ( v2,v0,v1,p2 );
/* .line 251 */
/* .line 253 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static void checkTime ( Long p0, java.lang.String p1 ) {
/* .locals 6 */
/* .param p0, "startTime" # J */
/* .param p2, "where" # Ljava/lang/String; */
/* .line 882 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 884 */
/* .local v0, "now":J */
/* sub-long v2, v0, p0 */
/* const-wide/16 v4, 0xbb8 */
/* cmp-long v2, v2, v4 */
/* if-lez v2, :cond_0 */
/* .line 885 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Slow operation: processNextBroadcast "; // const-string v3, "Slow operation: processNextBroadcast "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sub-long v3, v0, p0 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms so far, now at "; // const-string v3, "ms so far, now at "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "BroadcastQueueInjector"; // const-string v3, "BroadcastQueueInjector"
android.util.Slog .w ( v3,v2 );
/* .line 888 */
} // :cond_0
return;
} // .end method
private void forceStopAbnormalApp ( com.android.server.am.BroadcastQueueModernStubImpl$AbnormalBroadcastRecord p0 ) {
/* .locals 5 */
/* .param p1, "r" # Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
/* .line 745 */
v0 = this.mAmService;
/* monitor-enter v0 */
/* .line 746 */
try { // :try_start_0
final String v1 = "BroadcastQueueInjector"; // const-string v1, "BroadcastQueueInjector"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "force-stop abnormal app:"; // const-string v3, "force-stop abnormal app:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.callerPackage;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " userId:"; // const-string v3, " userId:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 747 */
/* const-class v1, Landroid/app/ActivityManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Landroid/app/ActivityManagerInternal; */
v2 = this.callerPackage;
/* iget v3, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I */
final String v4 = "abnormal ordered broadcast"; // const-string v4, "abnormal ordered broadcast"
/* .line 748 */
(( android.app.ActivityManagerInternal ) v1 ).forceStopPackage ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 750 */
/* monitor-exit v0 */
/* .line 751 */
return;
/* .line 750 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.android.server.am.BroadcastQueueModernStubImpl$AbnormalBroadcastRecord getAbnormalBroadcastByCountIfExisted ( java.util.List p0 ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/BroadcastRecord;", */
/* ">;)", */
/* "Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;" */
/* } */
} // .end annotation
/* .line 800 */
/* .local p1, "broadcasts":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/BroadcastRecord;>;" */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 802 */
/* .local v0, "startTime":J */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* .line 803 */
/* .local v2, "abnormalBroadcastMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;Ljava/lang/Integer;>;" */
} // :cond_0
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Lcom/android/server/am/BroadcastRecord; */
/* .line 804 */
/* .local v4, "r":Lcom/android/server/am/BroadcastRecord; */
final String v5 = "android"; // const-string v5, "android"
v6 = this.callerPackage;
v5 = android.text.TextUtils .equals ( v5,v6 );
/* if-nez v5, :cond_0 */
v5 = this.callerPackage;
/* .line 805 */
final String v6 = "com.google.android.gms"; // const-string v6, "com.google.android.gms"
v5 = android.text.TextUtils .equals ( v6,v5 );
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 806 */
/* .line 809 */
} // :cond_1
/* new-instance v5, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
/* invoke-direct {v5, v4}, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;-><init>(Lcom/android/server/am/BroadcastRecord;)V */
/* .line 810 */
/* .local v5, "abnormalRecord":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
/* check-cast v6, Ljava/lang/Integer; */
/* .line 811 */
/* .local v6, "count":Ljava/lang/Integer; */
int v7 = 1; // const/4 v7, 0x1
/* if-nez v6, :cond_2 */
/* .line 812 */
java.lang.Integer .valueOf ( v7 );
/* .line 814 */
} // :cond_2
v8 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
/* add-int/2addr v8, v7 */
java.lang.Integer .valueOf ( v8 );
/* .line 816 */
} // .end local v4 # "r":Lcom/android/server/am/BroadcastRecord;
} // .end local v5 # "abnormalRecord":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;
} // .end local v6 # "count":Ljava/lang/Integer;
} // :goto_1
/* .line 818 */
} // :cond_3
int v3 = 0; // const/4 v3, 0x0
/* .line 819 */
/* .local v3, "recordWithMaxCount":Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
int v4 = 0; // const/4 v4, 0x0
/* .line 820 */
/* .local v4, "maxCount":I */
v6 = } // :goto_2
if ( v6 != null) { // if-eqz v6, :cond_5
/* check-cast v6, Ljava/util/Map$Entry; */
/* .line 821 */
/* .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;Ljava/lang/Integer;>;" */
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* if-le v7, v4, :cond_4 */
/* .line 822 */
/* move-object v3, v7 */
/* check-cast v3, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
/* .line 823 */
/* check-cast v7, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 825 */
} // .end local v6 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;Ljava/lang/Integer;>;"
} // :cond_4
/* .line 827 */
} // :cond_5
final String v5 = "BroadcastQueueInjector"; // const-string v5, "BroadcastQueueInjector"
if ( v3 != null) { // if-eqz v3, :cond_7
v6 = this.callerPackage;
v6 = android.text.TextUtils .isEmpty ( v6 );
/* if-nez v6, :cond_7 */
/* if-ge v4, v6, :cond_6 */
/* .line 834 */
} // :cond_6
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "found abnormal broadcast in list by max count:"; // const-string v7, "found abnormal broadcast in list by max count:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = " cost:"; // const-string v7, " cost:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 835 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v0 */
(( java.lang.StringBuilder ) v6 ).append ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = " ms"; // const-string v7, " ms"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 834 */
android.util.Slog .d ( v5,v6 );
/* .line 837 */
/* .line 829 */
} // :cond_7
} // :goto_3
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "the max number of same broadcasts in queue is not large enough:" */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = " with count:"; // const-string v7, " with count:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 831 */
int v5 = 0; // const/4 v5, 0x0
} // .end method
private com.android.server.am.BroadcastQueueModernStubImpl$AbnormalBroadcastRecord getAbnormalBroadcastByRateIfExists ( java.util.List p0 ) {
/* .locals 10 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/BroadcastRecord;", */
/* ">;)", */
/* "Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;" */
/* } */
} // .end annotation
/* .line 755 */
/* .local p1, "broadcasts":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/BroadcastRecord;>;" */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 757 */
/* .local v0, "startTime":J */
int v2 = 0; // const/4 v2, 0x0
/* check-cast v2, Lcom/android/server/am/BroadcastRecord; */
/* .line 758 */
/* .local v2, "result":Lcom/android/server/am/BroadcastRecord; */
int v3 = 1; // const/4 v3, 0x1
/* .line 759 */
/* .local v3, "count":I */
int v4 = 1; // const/4 v4, 0x1
/* .local v4, "i":I */
v5 = } // :goto_0
/* if-ge v4, v5, :cond_2 */
/* .line 760 */
/* if-nez v3, :cond_0 */
/* .line 761 */
/* move-object v2, v5 */
/* check-cast v2, Lcom/android/server/am/BroadcastRecord; */
/* .line 762 */
int v3 = 1; // const/4 v3, 0x1
/* .line 763 */
} // :cond_0
v5 = this.intent;
(( android.content.Intent ) v5 ).getAction ( ); // invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* check-cast v6, Lcom/android/server/am/BroadcastRecord; */
v6 = this.intent;
(( android.content.Intent ) v6 ).getAction ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v5 = android.text.TextUtils .equals ( v5,v6 );
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = this.callerPackage;
/* .line 764 */
/* check-cast v6, Lcom/android/server/am/BroadcastRecord; */
v6 = this.callerPackage;
v5 = android.text.TextUtils .equals ( v5,v6 );
if ( v5 != null) { // if-eqz v5, :cond_1
/* iget v5, v2, Lcom/android/server/am/BroadcastRecord;->userId:I */
/* .line 765 */
/* check-cast v6, Lcom/android/server/am/BroadcastRecord; */
/* iget v6, v6, Lcom/android/server/am/BroadcastRecord;->userId:I */
/* if-ne v5, v6, :cond_1 */
/* .line 766 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 768 */
} // :cond_1
/* add-int/lit8 v3, v3, -0x1 */
/* .line 759 */
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 772 */
} // .end local v4 # "i":I
} // :cond_2
v4 = this.callerPackage;
v4 = android.text.TextUtils .isEmpty ( v4 );
int v5 = 0; // const/4 v5, 0x0
final String v6 = "BroadcastQueueInjector"; // const-string v6, "BroadcastQueueInjector"
/* if-nez v4, :cond_7 */
v4 = this.callerPackage;
/* .line 773 */
final String v7 = "android"; // const-string v7, "android"
v4 = android.text.TextUtils .equals ( v4,v7 );
/* if-nez v4, :cond_7 */
v4 = this.callerPackage;
/* .line 774 */
final String v7 = "com.google.android.gms"; // const-string v7, "com.google.android.gms"
v4 = android.text.TextUtils .equals ( v4,v7 );
/* if-nez v4, :cond_7 */
/* int-to-float v4, v3 */
v7 = /* .line 775 */
/* int-to-float v7, v7 */
/* const v8, 0x3e4cccd0 # 0.20000005f */
/* mul-float/2addr v7, v8 */
/* cmpg-float v4, v4, v7 */
/* if-gez v4, :cond_3 */
/* goto/16 :goto_3 */
/* .line 780 */
} // :cond_3
int v3 = 0; // const/4 v3, 0x0
/* .line 781 */
v7 = } // :goto_2
if ( v7 != null) { // if-eqz v7, :cond_5
/* check-cast v7, Lcom/android/server/am/BroadcastRecord; */
/* .line 782 */
/* .local v7, "r":Lcom/android/server/am/BroadcastRecord; */
v8 = this.intent;
(( android.content.Intent ) v8 ).getAction ( ); // invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v9 = this.intent;
(( android.content.Intent ) v9 ).getAction ( ); // invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v8 = android.text.TextUtils .equals ( v8,v9 );
if ( v8 != null) { // if-eqz v8, :cond_4
v8 = this.callerPackage;
v9 = this.callerPackage;
/* .line 783 */
v8 = android.text.TextUtils .equals ( v8,v9 );
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 784 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 786 */
} // .end local v7 # "r":Lcom/android/server/am/BroadcastRecord;
} // :cond_4
/* .line 788 */
} // :cond_5
v7 = /* int-to-float v4, v3 */
/* int-to-float v7, v7 */
/* const v8, 0x3f19999a # 0.6f */
/* mul-float/2addr v7, v8 */
/* cmpg-float v4, v4, v7 */
/* if-gez v4, :cond_6 */
/* .line 789 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "abnormal broadcast not found with count:"; // const-string v7, "abnormal broadcast not found with count:"
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v4 );
/* .line 790 */
/* .line 793 */
} // :cond_6
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "found abnormal broadcast in list by rate:"; // const-string v5, "found abnormal broadcast in list by rate:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " cost:"; // const-string v5, " cost:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 794 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v0 */
(( java.lang.StringBuilder ) v4 ).append ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = " ms"; // const-string v5, " ms"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 793 */
android.util.Slog .d ( v6,v4 );
/* .line 795 */
/* new-instance v4, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
/* invoke-direct {v4, v2}, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;-><init>(Lcom/android/server/am/BroadcastRecord;)V */
/* .line 776 */
} // :cond_7
} // :goto_3
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "abnormal broadcast not found with first loop count:"; // const-string v7, "abnormal broadcast not found with first loop count:"
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " with caller:"; // const-string v7, " with caller:"
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v4 );
/* .line 777 */
} // .end method
static android.os.Handler getBRReportHandler ( ) {
/* .locals 4 */
/* .line 489 */
v0 = com.android.server.am.BroadcastQueueModernStubImpl.mBRHandler;
/* if-nez v0, :cond_1 */
/* .line 490 */
v0 = com.android.server.am.BroadcastQueueModernStubImpl.mObject;
/* monitor-enter v0 */
/* .line 491 */
try { // :try_start_0
v1 = com.android.server.am.BroadcastQueueModernStubImpl.mBRHandler;
/* if-nez v1, :cond_0 */
/* .line 492 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "brreport-thread"; // const-string v2, "brreport-thread"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 493 */
/* .local v1, "mBRThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 494 */
/* new-instance v2, Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler; */
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, v3}, Lcom/android/server/am/BroadcastQueueModernStubImpl$BRReportHandler;-><init>(Landroid/os/Looper;)V */
/* .line 496 */
} // .end local v1 # "mBRThread":Landroid/os/HandlerThread;
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 498 */
} // :cond_1
} // :goto_0
v0 = com.android.server.am.BroadcastQueueModernStubImpl.mBRHandler;
} // .end method
static com.android.server.am.BroadcastQueueModernStubImpl getInstance ( ) {
/* .locals 1 */
/* .line 132 */
com.android.server.am.BroadcastQueueModernStub .get ( );
/* check-cast v0, Lcom/android/server/am/BroadcastQueueModernStubImpl; */
} // .end method
private Integer getNextRequestIdLocked ( ) {
/* .locals 2 */
/* .line 650 */
/* iget v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mActivityRequestId:I */
/* const v1, 0x7fffffff */
/* if-lt v0, v1, :cond_0 */
/* .line 651 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mActivityRequestId:I */
/* .line 653 */
} // :cond_0
/* iget v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mActivityRequestId:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mActivityRequestId:I */
/* .line 654 */
} // .end method
private java.lang.String getPackageLabelLocked ( com.android.server.am.BroadcastQueueModernStubImpl$AbnormalBroadcastRecord p0 ) {
/* .locals 4 */
/* .param p1, "r" # Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
/* .line 687 */
int v0 = 0; // const/4 v0, 0x0
/* .line 688 */
/* .local v0, "label":Ljava/lang/String; */
v1 = this.callerPackage;
/* iget v2, p1, Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;->userId:I */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord; */
/* .line 689 */
/* .local v1, "app":Lcom/android/server/am/ProcessRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
(( com.android.server.am.ProcessRecord ) v1 ).getPkgList ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getPkgList()Lcom/android/server/am/PackageList;
v2 = (( com.android.server.am.PackageList ) v2 ).size ( ); // invoke-virtual {v2}, Lcom/android/server/am/PackageList;->size()I
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_0 */
/* .line 690 */
v2 = this.mContext;
(( android.content.Context ) v2 ).getPackageManager ( ); // invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v3 = this.info;
(( android.content.pm.PackageManager ) v2 ).getApplicationLabel ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
/* .line 691 */
/* .local v2, "labelChar":Ljava/lang/CharSequence; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 692 */
/* .line 696 */
} // .end local v2 # "labelChar":Ljava/lang/CharSequence;
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 697 */
v0 = this.callerPackage;
/* .line 699 */
} // :cond_1
} // .end method
private com.android.server.am.ProcessRecord getProcessRecordLocked ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 703 */
v0 = this.mAmService;
v0 = this.mProcessList;
v0 = (( com.android.server.am.ProcessList ) v0 ).getLruSizeLOSP ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruSizeLOSP()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 704 */
v1 = this.mAmService;
v1 = this.mProcessList;
(( com.android.server.am.ProcessList ) v1 ).getLruProcessesLOSP ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/ProcessRecord; */
/* .line 705 */
/* .local v1, "app":Lcom/android/server/am/ProcessRecord; */
(( com.android.server.am.ProcessRecord ) v1 ).getThread ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.processName;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget v2, v1, Lcom/android/server/am/ProcessRecord;->userId:I */
/* if-ne v2, p2, :cond_0 */
/* .line 707 */
/* .line 703 */
} // .end local v1 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 710 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean isInternationalSpecialAction ( java.lang.String p0, android.content.pm.ResolveInfo p1 ) {
/* .locals 5 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "info" # Landroid/content/pm/ResolveInfo; */
/* .line 404 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = v0 = com.android.server.am.BroadcastQueueModernStubImpl.mInternationalSpecialAction;
/* if-nez v0, :cond_0 */
/* .line 407 */
} // :cond_0
v0 = this.mContext;
v2 = this.activityInfo;
v2 = this.applicationInfo;
v2 = this.packageName;
v3 = this.activityInfo;
v3 = this.applicationInfo;
/* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
final String v4 = "BroadcastQueueImpl#specifyBroadcast"; // const-string v4, "BroadcastQueueImpl#specifyBroadcast"
v0 = android.miui.AppOpsUtils .getApplicationSpecialBroadcast ( v0,v2,v3,v4 );
/* .line 409 */
/* .local v0, "autoStartMode":I */
/* if-nez v0, :cond_1 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* .line 405 */
} // .end local v0 # "autoStartMode":I
} // :cond_2
} // :goto_0
} // .end method
private static Boolean isSystemBootCompleted ( ) {
/* .locals 2 */
/* .line 502 */
/* sget-boolean v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSystemBootCompleted:Z */
/* if-nez v0, :cond_0 */
/* .line 503 */
/* const-string/jumbo v0, "sys.boot_completed" */
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.android.server.am.BroadcastQueueModernStubImpl.sSystemBootCompleted = (v0!= 0);
/* .line 505 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/BroadcastQueueModernStubImpl;->sSystemBootCompleted:Z */
} // .end method
private static void notifyPowerKeeperEvent ( Integer p0, Integer p1, Integer p2, java.lang.String p3 ) {
/* .locals 0 */
/* .param p0, "state" # I */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "action" # Ljava/lang/String; */
/* .line 245 */
return;
} // .end method
static void onBroadcastFinished ( android.content.Intent p0, java.lang.String p1, Integer p2, Long p3, Long p4, Long p5, Long p6, Integer p7 ) {
/* .locals 20 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .param p1, "caller" # Ljava/lang/String; */
/* .param p2, "callingPid" # I */
/* .param p3, "enTime" # J */
/* .param p5, "disTime" # J */
/* .param p7, "finTime" # J */
/* .param p9, "mTimeoutPeriod" # J */
/* .param p11, "receiverSize" # I */
/* .line 512 */
/* move-wide/from16 v0, p3 */
/* move-wide/from16 v2, p5 */
/* move-wide/from16 v4, p7 */
/* move/from16 v6, p11 */
/* sget-boolean v7, Lcom/android/server/am/BroadcastQueueModernStubImpl;->IS_STABLE_VERSION:Z */
if ( v7 != null) { // if-eqz v7, :cond_0
return;
/* .line 513 */
} // :cond_0
/* invoke-virtual/range {p0 ..p0}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
/* .line 514 */
/* .local v7, "action":Ljava/lang/String; */
/* if-nez v7, :cond_1 */
final String v7 = "null"; // const-string v7, "null"
/* .line 515 */
} // :cond_1
/* if-nez p1, :cond_2 */
final String v8 = "android"; // const-string v8, "android"
} // :cond_2
/* move-object/from16 v8, p1 */
/* .line 516 */
} // .end local p1 # "caller":Ljava/lang/String;
/* .local v8, "caller":Ljava/lang/String; */
} // :goto_0
/* move/from16 v9, p2 */
/* .line 517 */
/* .local v9, "pid":I */
final String v10 = ""; // const-string v10, ""
/* .line 519 */
/* .local v10, "reason":Ljava/lang/String; */
/* move-object v11, v8 */
/* .line 520 */
/* .local v11, "packageName":Ljava/lang/String; */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v12 */
/* .line 521 */
/* .local v12, "timeStamp":J */
/* sub-long v14, v2, v0 */
/* sget-wide v16, Lcom/android/server/am/BroadcastQueueModernStubImpl;->mDispatchThreshold:J */
/* cmp-long v14, v14, v16 */
/* if-gez v14, :cond_4 */
/* sub-long v16, v4, v2 */
/* move-wide/from16 v18, v12 */
} // .end local v12 # "timeStamp":J
/* .local v18, "timeStamp":J */
/* int-to-long v12, v6 */
/* mul-long v12, v12, p9 */
/* int-to-long v14, v14 */
/* div-long/2addr v12, v14 */
/* cmp-long v12, v16, v12 */
/* if-ltz v12, :cond_3 */
} // :cond_3
int v12 = 0; // const/4 v12, 0x0
} // .end local v18 # "timeStamp":J
/* .restart local v12 # "timeStamp":J */
} // :cond_4
/* move-wide/from16 v18, v12 */
} // .end local v12 # "timeStamp":J
/* .restart local v18 # "timeStamp":J */
} // :goto_1
int v12 = 1; // const/4 v12, 0x1
/* .line 523 */
/* .local v12, "needUpdate":Z */
} // :goto_2
/* const/16 v14, 0x1e */
/* if-ltz v13, :cond_5 */
/* if-le v13, v14, :cond_6 */
} // :cond_5
int v13 = 0; // const/4 v13, 0x0
} // :cond_6
/* .line 524 */
/* new-instance v13, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap; */
/* invoke-direct {v13, v7, v8}, Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 525 */
/* .local v13, "broadcastMap":Lcom/android/server/am/BroadcastQueueModernStubImpl$BroadcastMap; */
if ( v15 != null) { // if-eqz v15, :cond_8
/* if-gt v15, v14, :cond_8 */
v15 = com.android.server.am.BroadcastQueueModernStubImpl.mBroadcastMap;
v15 = (( java.util.ArrayList ) v15 ).contains ( v13 ); // invoke-virtual {v15, v13}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v15 != null) { // if-eqz v15, :cond_8
/* .line 526 */
v14 = com.android.server.am.BroadcastQueueModernStubImpl.mBroadcastMap;
v14 = (( java.util.ArrayList ) v14 ).indexOf ( v13 ); // invoke-virtual {v14, v13}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I
/* .line 527 */
/* .local v14, "index":I */
v15 = com.android.server.am.BroadcastQueueModernStubImpl.BR_LIST;
(( java.util.ArrayList ) v15 ).get ( v14 ); // invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v15, Lmiui/mqsas/sdk/event/BroadcastEvent; */
/* .line 528 */
/* .local v15, "BE":Lmiui/mqsas/sdk/event/BroadcastEvent; */
(( miui.mqsas.sdk.event.BroadcastEvent ) v15 ).addCount ( ); // invoke-virtual {v15}, Lmiui/mqsas/sdk/event/BroadcastEvent;->addCount()V
/* .line 529 */
/* move-object/from16 v16, v8 */
/* move/from16 v17, v9 */
} // .end local v8 # "caller":Ljava/lang/String;
} // .end local v9 # "pid":I
/* .local v16, "caller":Ljava/lang/String; */
/* .local v17, "pid":I */
/* sub-long v8, v4, v0 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v15 ).addTotalTime ( v8, v9 ); // invoke-virtual {v15, v8, v9}, Lmiui/mqsas/sdk/event/BroadcastEvent;->addTotalTime(J)V
/* .line 530 */
if ( v12 != null) { // if-eqz v12, :cond_7
/* .line 531 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v15 ).setReason ( v10 ); // invoke-virtual {v15, v10}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setReason(Ljava/lang/String;)V
/* .line 532 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v15 ).setEnTime ( v0, v1 ); // invoke-virtual {v15, v0, v1}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setEnTime(J)V
/* .line 533 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v15 ).setDisTime ( v2, v3 ); // invoke-virtual {v15, v2, v3}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setDisTime(J)V
/* .line 534 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v15 ).setFinTime ( v4, v5 ); // invoke-virtual {v15, v4, v5}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setFinTime(J)V
/* .line 535 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v15 ).setNumReceivers ( v6 ); // invoke-virtual {v15, v6}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setNumReceivers(I)V
/* .line 537 */
} // .end local v14 # "index":I
} // .end local v15 # "BE":Lmiui/mqsas/sdk/event/BroadcastEvent;
} // :cond_7
/* move-object/from16 v9, v16 */
/* move/from16 v14, v17 */
/* move-wide/from16 v0, v18 */
/* goto/16 :goto_3 */
/* .line 525 */
} // .end local v16 # "caller":Ljava/lang/String;
} // .end local v17 # "pid":I
/* .restart local v8 # "caller":Ljava/lang/String; */
/* .restart local v9 # "pid":I */
} // :cond_8
/* move-object/from16 v16, v8 */
/* move/from16 v17, v9 */
/* .line 538 */
} // .end local v8 # "caller":Ljava/lang/String;
} // .end local v9 # "pid":I
/* .restart local v16 # "caller":Ljava/lang/String; */
/* .restart local v17 # "pid":I */
/* if-lt v8, v14, :cond_9 */
/* .line 539 */
android.os.Message .obtain ( );
/* .line 540 */
/* .local v8, "message":Landroid/os/Message; */
int v9 = 1; // const/4 v9, 0x1
/* iput v9, v8, Landroid/os/Message;->what:I */
/* .line 541 */
/* new-instance v9, Landroid/content/pm/ParceledListSlice; */
v14 = com.android.server.am.BroadcastQueueModernStubImpl.BR_LIST;
(( java.util.ArrayList ) v14 ).clone ( ); // invoke-virtual {v14}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;
/* check-cast v14, Ljava/util/ArrayList; */
/* invoke-direct {v9, v14}, Landroid/content/pm/ParceledListSlice;-><init>(Ljava/util/List;)V */
this.obj = v9;
/* .line 542 */
com.android.server.am.BroadcastQueueModernStubImpl .getBRReportHandler ( );
(( android.os.Handler ) v9 ).sendMessage ( v8 ); // invoke-virtual {v9, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 543 */
v9 = com.android.server.am.BroadcastQueueModernStubImpl.BR_LIST;
(( java.util.ArrayList ) v9 ).clear ( ); // invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V
/* .line 544 */
v9 = com.android.server.am.BroadcastQueueModernStubImpl.mBroadcastMap;
(( java.util.ArrayList ) v9 ).clear ( ); // invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V
/* .line 545 */
int v9 = 0; // const/4 v9, 0x0
/* .line 547 */
} // .end local v8 # "message":Landroid/os/Message;
} // :cond_9
v8 = com.android.server.am.BroadcastQueueModernStubImpl.mBroadcastMap;
(( java.util.ArrayList ) v8 ).add ( v13 ); // invoke-virtual {v8, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 548 */
int v9 = 1; // const/4 v9, 0x1
/* add-int/2addr v8, v9 */
/* .line 549 */
/* new-instance v8, Lmiui/mqsas/sdk/event/BroadcastEvent; */
/* invoke-direct {v8}, Lmiui/mqsas/sdk/event/BroadcastEvent;-><init>()V */
/* .line 550 */
/* .local v8, "broadcast":Lmiui/mqsas/sdk/event/BroadcastEvent; */
/* const/16 v9, 0x40 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setType ( v9 ); // invoke-virtual {v8, v9}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setType(I)V
/* .line 551 */
if ( v12 != null) { // if-eqz v12, :cond_a
/* .line 552 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setReason ( v10 ); // invoke-virtual {v8, v10}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setReason(Ljava/lang/String;)V
/* .line 553 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setEnTime ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setEnTime(J)V
/* .line 554 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setDisTime ( v2, v3 ); // invoke-virtual {v8, v2, v3}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setDisTime(J)V
/* .line 555 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setFinTime ( v4, v5 ); // invoke-virtual {v8, v4, v5}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setFinTime(J)V
/* .line 556 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setNumReceivers ( v6 ); // invoke-virtual {v8, v6}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setNumReceivers(I)V
/* .line 558 */
} // :cond_a
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setAction ( v7 ); // invoke-virtual {v8, v7}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setAction(Ljava/lang/String;)V
/* .line 559 */
/* move-object/from16 v9, v16 */
} // .end local v16 # "caller":Ljava/lang/String;
/* .local v9, "caller":Ljava/lang/String; */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setCallerPackage ( v9 ); // invoke-virtual {v8, v9}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setCallerPackage(Ljava/lang/String;)V
/* .line 560 */
int v14 = 1; // const/4 v14, 0x1
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setCount ( v14 ); // invoke-virtual {v8, v14}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setCount(I)V
/* .line 561 */
/* sub-long v14, v4, v0 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setTotalTime ( v14, v15 ); // invoke-virtual {v8, v14, v15}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setTotalTime(J)V
/* .line 562 */
/* move/from16 v14, v17 */
} // .end local v17 # "pid":I
/* .local v14, "pid":I */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setPid ( v14 ); // invoke-virtual {v8, v14}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setPid(I)V
/* .line 564 */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setPackageName ( v11 ); // invoke-virtual {v8, v11}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setPackageName(Ljava/lang/String;)V
/* .line 565 */
/* move-wide/from16 v0, v18 */
} // .end local v18 # "timeStamp":J
/* .local v0, "timeStamp":J */
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setTimeStamp ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setTimeStamp(J)V
/* .line 566 */
int v15 = 1; // const/4 v15, 0x1
(( miui.mqsas.sdk.event.BroadcastEvent ) v8 ).setSystem ( v15 ); // invoke-virtual {v8, v15}, Lmiui/mqsas/sdk/event/BroadcastEvent;->setSystem(Z)V
/* .line 567 */
v15 = com.android.server.am.BroadcastQueueModernStubImpl.BR_LIST;
(( java.util.ArrayList ) v15 ).add ( v8 ); // invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 569 */
} // .end local v8 # "broadcast":Lmiui/mqsas/sdk/event/BroadcastEvent;
} // :goto_3
return;
} // .end method
private void processAbnormalBroadcast ( com.android.server.am.BroadcastQueueModernStubImpl$AbnormalBroadcastRecord p0, java.lang.String p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord; */
/* .param p2, "packageLabel" # Ljava/lang/String; */
/* .param p3, "count" # I */
/* .line 715 */
int v0 = 0; // const/4 v0, 0x0
/* .line 716 */
/* .local v0, "showDialogSuccess":Z */
/* mul-int/lit8 v1, v1, 0x3 */
/* if-ge p3, v1, :cond_0 */
/* .line 717 */
final String v1 = "BroadcastQueueInjector"; // const-string v1, "BroadcastQueueInjector"
final String v2 = "abnormal ordered broadcast, showWarningDialog"; // const-string v2, "abnormal ordered broadcast, showWarningDialog"
android.util.Slog .d ( v1,v2 );
/* .line 738 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 739 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->forceStopAbnormalApp(Lcom/android/server/am/BroadcastQueueModernStubImpl$AbnormalBroadcastRecord;)V */
/* .line 740 */
v1 = com.android.server.am.BroadcastQueueModernStubImpl.sAbnormalBroadcastWarning;
int v2 = 0; // const/4 v2, 0x0
(( java.util.concurrent.atomic.AtomicBoolean ) v1 ).set ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
/* .line 742 */
} // :cond_1
return;
} // .end method
private Boolean shouldStopBroadcastDispatch ( android.content.pm.ResolveInfo p0, Boolean p1, android.content.Intent p2 ) {
/* .locals 10 */
/* .param p1, "info" # Landroid/content/pm/ResolveInfo; */
/* .param p2, "note" # Z */
/* .param p3, "callee" # Landroid/content/Intent; */
/* .line 414 */
v0 = this.mAmService;
v0 = this.mActivityTaskManager;
v1 = this.activityInfo;
v1 = this.applicationInfo;
v1 = this.packageName;
v2 = this.activityInfo;
v2 = this.processName;
v3 = this.activityInfo;
v3 = this.applicationInfo;
/* iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
v0 = com.android.server.wm.WindowProcessUtils .isPackageRunning ( v0,v1,v2,v3 );
/* .line 417 */
/* .local v0, "isRunning":Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 418 */
/* .line 420 */
} // :cond_0
v2 = this.mContext;
v3 = this.activityInfo;
v3 = this.applicationInfo;
v3 = this.packageName;
v4 = this.activityInfo;
v4 = this.applicationInfo;
/* iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 421 */
if ( p2 != null) { // if-eqz p2, :cond_1
final String v5 = "BroadcastQueueModernStubImpl#checkApplicationAutoStart#"; // const-string v5, "BroadcastQueueModernStubImpl#checkApplicationAutoStart#"
/* .line 422 */
} // :cond_1
final String v5 = "BroadcastQueueModernStubImpl#specifyBroadcast"; // const-string v5, "BroadcastQueueModernStubImpl#specifyBroadcast"
/* .line 420 */
} // :goto_0
v2 = android.miui.AppOpsUtils .noteApplicationAutoStart ( v2,v3,v4,v5 );
/* .line 423 */
/* .local v2, "autoStartMode":I */
v3 = this.mSecurityInternal;
/* if-nez v3, :cond_2 */
/* .line 424 */
/* const-class v3, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v3 );
/* check-cast v3, Lmiui/security/SecurityManagerInternal; */
this.mSecurityInternal = v3;
/* .line 426 */
} // :cond_2
if ( p3 != null) { // if-eqz p3, :cond_3
v4 = this.mSecurityInternal;
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 427 */
int v5 = 6; // const/4 v5, 0x6
v3 = this.activityInfo;
v3 = this.applicationInfo;
v6 = this.packageName;
/* const-wide/16 v7, 0x1 */
/* .line 429 */
miui.security.AppBehavior .parseIntent ( p3 );
/* .line 427 */
/* invoke-virtual/range {v4 ..v9}, Lmiui/security/SecurityManagerInternal;->recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V */
/* .line 431 */
} // :cond_3
/* if-nez v2, :cond_4 */
} // :cond_4
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
} // .end method
/* # virtual methods */
public Boolean checkApplicationAutoStart ( com.android.server.am.BroadcastQueue p0, com.android.server.am.BroadcastRecord p1, android.content.pm.ResolveInfo p2 ) {
/* .locals 11 */
/* .param p1, "bq" # Lcom/android/server/am/BroadcastQueue; */
/* .param p2, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .param p3, "info" # Landroid/content/pm/ResolveInfo; */
/* .line 339 */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
v0 = this.activityInfo;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.activityInfo;
v0 = this.applicationInfo;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.activityInfo;
v0 = this.applicationInfo;
v0 = this.packageName;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 342 */
v0 = this.activityInfo;
v0 = this.applicationInfo;
v0 = this.packageName;
v0 = com.miui.server.process.ProcessManagerInternal .checkCtsProcess ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 344 */
/* .line 347 */
} // :cond_0
v0 = android.miui.AppOpsUtils .isXOptMode ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 348 */
/* .line 350 */
} // :cond_1
v0 = this.intent;
(( android.content.Intent ) v0 ).getAction ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 351 */
/* .local v0, "action":Ljava/lang/String; */
/* sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
final String v2 = "com.google.android.c2dm.intent.RECEIVE"; // const-string v2, "com.google.android.c2dm.intent.RECEIVE"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 352 */
/* .line 356 */
} // :cond_2
v2 = this.callerApp;
final String v3 = "BroadcastQueueInjector"; // const-string v3, "BroadcastQueueInjector"
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = this.callerPackage;
if ( v2 != null) { // if-eqz v2, :cond_3
v2 = com.android.server.am.ActivityManagerServiceImpl.WIDGET_PROVIDER_WHITE_LIST;
v5 = this.callerPackage;
v2 = /* .line 357 */
/* if-nez v2, :cond_3 */
/* .line 358 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v5 = this.callerPackage;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ":widgetProvider"; // const-string v5, ":widgetProvider"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 359 */
/* .local v2, "widgetProcessName":Ljava/lang/String; */
v5 = this.callerApp;
v5 = this.processName;
v5 = (( java.lang.String ) v2 ).equals ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 360 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MIUILOG- Reject widget call from "; // const-string v5, "MIUILOG- Reject widget call from "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.callerPackage;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v1 );
/* .line 361 */
/* .line 366 */
} // .end local v2 # "widgetProcessName":Ljava/lang/String;
} // :cond_3
v2 = this.callerApp;
if ( v2 != null) { // if-eqz v2, :cond_5
v2 = this.callerApp;
/* iget v2, v2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* const/16 v5, 0x403 */
/* if-ne v2, v5, :cond_5 */
v2 = com.android.server.am.BroadcastQueueModernStubImpl.ACTION_NFC;
v2 = /* .line 367 */
if ( v2 != null) { // if-eqz v2, :cond_5
v2 = this.intent;
/* .line 368 */
(( android.content.Intent ) v2 ).getPackage ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
v2 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v2, :cond_5 */
/* .line 369 */
v2 = this.intent;
(( android.content.Intent ) v2 ).getPackage ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
v2 = com.android.server.am.PendingIntentRecordImpl .containsPendingIntent ( v2 );
/* if-nez v2, :cond_4 */
/* .line 370 */
v2 = this.intent;
(( android.content.Intent ) v2 ).getPackage ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
com.android.server.am.PendingIntentRecordImpl .exemptTemporarily ( v2,v1 );
/* .line 372 */
} // :cond_4
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "MIUILOG- Allow NFC start appliction "; // const-string v4, "MIUILOG- Allow NFC start appliction "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.intent;
(( android.content.Intent ) v4 ).getPackage ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " action: "; // const-string v4, " action: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 373 */
/* .line 377 */
} // :cond_5
miui.security.WakePathChecker .getInstance ( );
v6 = this.intent;
v7 = this.callerPackage;
/* .line 378 */
v2 = this.callerApp;
if ( v2 != null) { // if-eqz v2, :cond_6
v2 = this.callerApp;
v2 = this.info;
} // :cond_6
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* move-object v8, v2 */
/* iget v10, p2, Lcom/android/server/am/BroadcastRecord;->userId:I */
/* .line 377 */
/* move-object v9, p3 */
v2 = /* invoke-virtual/range {v5 ..v10}, Lmiui/security/WakePathChecker;->checkBroadcastWakePath(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ResolveInfo;I)Z */
if ( v2 != null) { // if-eqz v2, :cond_e
/* .line 379 */
v2 = this.activityInfo;
v2 = this.applicationInfo;
/* iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I */
/* and-int/2addr v2, v1 */
/* if-nez v2, :cond_8 */
v2 = this.activityInfo;
v2 = this.applicationInfo;
v2 = this.packageName;
/* .line 380 */
v2 = miui.content.pm.PreloadedAppPolicy .isProtectedDataApp ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_7
} // :cond_7
/* move v2, v4 */
} // :cond_8
} // :goto_1
/* move v2, v1 */
/* .line 381 */
/* .local v2, "isSystem":Z */
} // :goto_2
final String v5 = "com.xiaomi.mipush.MESSAGE_ARRIVED"; // const-string v5, "com.xiaomi.mipush.MESSAGE_ARRIVED"
v5 = (( java.lang.String ) v5 ).equals ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 382 */
/* .local v5, "isMessageArrived":Z */
v6 = this.intent;
(( android.content.Intent ) v6 ).getComponent ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
/* if-nez v6, :cond_d */
/* if-nez v5, :cond_9 */
if ( v2 != null) { // if-eqz v2, :cond_9
v6 = this.intent;
/* .line 383 */
(( android.content.Intent ) v6 ).getPackage ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
/* if-nez v6, :cond_d */
} // :cond_9
if ( v2 != null) { // if-eqz v2, :cond_a
v6 = com.android.server.am.BroadcastQueueModernStubImpl.sSystemSkipAction;
/* .line 384 */
v6 = (( java.util.ArrayList ) v6 ).contains ( v0 ); // invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v6, :cond_d */
} // :cond_a
v6 = this.intent;
/* .line 385 */
(( android.content.Intent ) v6 ).getPackage ( ); // invoke-virtual {v6}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
/* if-nez v6, :cond_b */
/* move v6, v1 */
} // :cond_b
/* move v6, v4 */
} // :goto_3
v7 = this.intent;
v6 = /* invoke-direct {p0, p3, v6, v7}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->shouldStopBroadcastDispatch(Landroid/content/pm/ResolveInfo;ZLandroid/content/Intent;)Z */
/* if-nez v6, :cond_d */
/* .line 386 */
v6 = /* invoke-direct {p0, v0, p3}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isInternationalSpecialAction(Ljava/lang/String;Landroid/content/pm/ResolveInfo;)Z */
if ( v6 != null) { // if-eqz v6, :cond_c
/* .line 389 */
} // :cond_c
final String v1 = " auto start"; // const-string v1, " auto start"
/* .line 391 */
} // .end local v2 # "isSystem":Z
} // .end local v5 # "isMessageArrived":Z
/* .local v1, "reason":Ljava/lang/String; */
/* .line 387 */
} // .end local v1 # "reason":Ljava/lang/String;
/* .restart local v2 # "isSystem":Z */
/* .restart local v5 # "isMessageArrived":Z */
} // :cond_d
} // :goto_4
/* .line 392 */
} // .end local v2 # "isSystem":Z
} // .end local v5 # "isMessageArrived":Z
} // :cond_e
final String v1 = " wake path"; // const-string v1, " wake path"
/* .line 394 */
/* .restart local v1 # "reason":Ljava/lang/String; */
} // :goto_5
if ( p3 != null) { // if-eqz p3, :cond_f
/* .line 395 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Unable to launch app "; // const-string v5, "Unable to launch app "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.activityInfo;
v5 = this.applicationInfo;
v5 = this.packageName;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "/"; // const-string v5, "/"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.activityInfo;
v5 = this.applicationInfo;
/* iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " for broadcast "; // const-string v5, " for broadcast "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.intent;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = ": process is not permitted to "; // const-string v5, ": process is not permitted to "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v2 );
/* .line 400 */
} // :cond_f
} // .end method
public Boolean checkReceiverAppDealBroadcast ( com.android.server.am.BroadcastQueue p0, com.android.server.am.BroadcastRecord p1, com.android.server.am.ProcessRecord p2, Boolean p3 ) {
/* .locals 15 */
/* .param p1, "bq" # Lcom/android/server/am/BroadcastQueue; */
/* .param p2, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .param p3, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p4, "isStatic" # Z */
/* .line 259 */
/* move-object/from16 v0, p2 */
/* move-object/from16 v1, p3 */
/* .line 263 */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_7
if ( v0 != null) { // if-eqz v0, :cond_7
v3 = this.intent;
/* if-nez v3, :cond_0 */
/* move-object v5, p0 */
/* goto/16 :goto_2 */
/* .line 264 */
} // :cond_0
v3 = this.intent;
(( android.content.Intent ) v3 ).getAction ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 265 */
/* .local v3, "action":Ljava/lang/String; */
final String v4 = ""; // const-string v4, ""
/* .line 266 */
/* .local v4, "callee":Ljava/lang/String; */
final String v5 = ""; // const-string v5, ""
/* .line 267 */
/* .local v5, "className":Ljava/lang/String; */
v12 = this.intent;
/* .line 268 */
/* .local v12, "intent":Landroid/content/Intent; */
(( android.content.Intent ) v12 ).getComponent ( ); // invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 269 */
(( android.content.Intent ) v12 ).getComponent ( ); // invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v6 ).getClassName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
/* .line 270 */
(( android.content.Intent ) v12 ).getComponent ( ); // invoke-virtual {v12}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v6 ).getPackageName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 273 */
} // :cond_1
v6 = this.info;
if ( v6 != null) { // if-eqz v6, :cond_2
v6 = android.text.TextUtils .isEmpty ( v4 );
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 274 */
v6 = this.info;
v4 = this.packageName;
/* move-object v13, v4 */
/* .line 276 */
} // :cond_2
/* move-object v13, v4 */
} // .end local v4 # "callee":Ljava/lang/String;
/* .local v13, "callee":Ljava/lang/String; */
} // :goto_0
v4 = this.curReceiver;
if ( v4 != null) { // if-eqz v4, :cond_3
v4 = android.text.TextUtils .isEmpty ( v5 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 277 */
v4 = this.curReceiver;
v5 = this.name;
/* move-object v14, v5 */
/* .line 279 */
} // :cond_3
/* move-object v14, v5 */
} // .end local v5 # "className":Ljava/lang/String;
/* .local v14, "className":Ljava/lang/String; */
} // :goto_1
v4 = this.callerPackage;
v4 = android.text.TextUtils .equals ( v13,v4 );
/* if-nez v4, :cond_4 */
/* .line 280 */
miui.security.WakePathChecker .getInstance ( );
v7 = this.callerPackage;
/* iget v9, v0, Lcom/android/server/am/BroadcastRecord;->userId:I */
/* const/16 v10, 0x2000 */
int v11 = 1; // const/4 v11, 0x1
/* move-object v5, v3 */
/* move-object v6, v14 */
/* move-object v8, v13 */
v4 = /* invoke-virtual/range {v4 ..v11}, Lmiui/security/WakePathChecker;->calleeAliveMatchBlackRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 283 */
int v2 = 0; // const/4 v2, 0x0
/* .line 286 */
} // :cond_4
v4 = this.callerApp;
if ( v4 != null) { // if-eqz v4, :cond_5
v4 = this.callerApp;
/* iget v4, v4, Lcom/android/server/am/ProcessRecord;->uid:I */
/* const/16 v5, 0x403 */
/* if-ne v4, v5, :cond_5 */
v4 = com.android.server.am.BroadcastQueueModernStubImpl.ACTION_NFC;
v4 = /* .line 287 */
if ( v4 != null) { // if-eqz v4, :cond_5
v4 = this.intent;
/* .line 288 */
(( android.content.Intent ) v4 ).getPackage ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
v4 = android.text.TextUtils .isEmpty ( v4 );
/* if-nez v4, :cond_5 */
v4 = this.intent;
/* .line 289 */
(( android.content.Intent ) v4 ).getPackage ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
v4 = com.android.server.am.PendingIntentRecordImpl .containsPendingIntent ( v4 );
/* if-nez v4, :cond_5 */
/* .line 290 */
v4 = this.intent;
(( android.content.Intent ) v4 ).getPackage ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
com.android.server.am.PendingIntentRecordImpl .exemptTemporarily ( v4,v2 );
/* .line 291 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MIUILOG- Allow NFC start appliction "; // const-string v5, "MIUILOG- Allow NFC start appliction "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.intent;
(( android.content.Intent ) v5 ).getPackage ( ); // invoke-virtual {v5}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " background start"; // const-string v5, " background start"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "BroadcastQueueInjector"; // const-string v5, "BroadcastQueueInjector"
android.util.Slog .i ( v5,v4 );
/* .line 293 */
} // :cond_5
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
/* if-nez v4, :cond_6 */
/* .line 294 */
/* .line 296 */
} // :cond_6
v4 = this.intent;
(( android.content.Intent ) v4 ).getAction ( ); // invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* move-object v5, p0 */
/* invoke-direct {p0, v1, v4}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->checkSpecialAction(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z */
/* .line 297 */
/* .line 263 */
} // .end local v3 # "action":Ljava/lang/String;
} // .end local v12 # "intent":Landroid/content/Intent;
} // .end local v13 # "callee":Ljava/lang/String;
} // .end local v14 # "className":Ljava/lang/String;
} // :cond_7
/* move-object v5, p0 */
} // :goto_2
} // .end method
public Boolean checkReceiverIfRestricted ( com.android.server.am.BroadcastQueue p0, com.android.server.am.BroadcastRecord p1, com.android.server.am.ProcessRecord p2, Boolean p3 ) {
/* .locals 8 */
/* .param p1, "bq" # Lcom/android/server/am/BroadcastQueue; */
/* .param p2, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .param p3, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p4, "isStatic" # Z */
/* .line 904 */
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
if ( p3 != null) { // if-eqz p3, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
v0 = this.intent;
/* if-nez v0, :cond_0 */
/* .line 908 */
} // :cond_0
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
v3 = this.intent;
/* iget v4, p2, Lcom/android/server/am/BroadcastRecord;->callingUid:I */
v5 = this.callerPackage;
/* iget v6, p3, Lcom/android/server/am/ProcessRecord;->uid:I */
v7 = this.processName;
v0 = /* invoke-virtual/range {v2 ..v7}, Lcom/miui/server/greeze/GreezeManagerInternal;->isRestrictReceiver(Landroid/content/Intent;ILjava/lang/String;ILjava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 910 */
int v0 = 1; // const/4 v0, 0x1
/* .line 912 */
} // :cond_1
/* .line 905 */
} // :cond_2
} // :goto_0
} // .end method
public Boolean checkSkipReceiver ( com.android.server.am.BroadcastQueue p0, com.android.server.am.BroadcastProcessQueue p1, com.android.server.am.BroadcastRecord p2, com.android.server.am.ProcessRecord p3, Boolean p4, java.lang.Object p5 ) {
/* .locals 1 */
/* .param p1, "bq" # Lcom/android/server/am/BroadcastQueue; */
/* .param p2, "queue" # Lcom/android/server/am/BroadcastProcessQueue; */
/* .param p3, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .param p4, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p5, "isStatic" # Z */
/* .param p6, "info" # Ljava/lang/Object; */
/* .line 437 */
v0 = (( com.android.server.am.BroadcastProcessQueue ) p2 ).isProcessWarm ( ); // invoke-virtual {p2}, Lcom/android/server/am/BroadcastProcessQueue;->isProcessWarm()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 438 */
v0 = (( com.android.server.am.BroadcastQueueModernStubImpl ) p0 ).checkReceiverAppDealBroadcast ( p1, p3, p4, p5 ); // invoke-virtual {p0, p1, p3, p4, p5}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->checkReceiverAppDealBroadcast(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;Z)Z
/* xor-int/lit8 v0, v0, 0x1 */
/* .line 439 */
} // :cond_0
/* instance-of v0, p6, Landroid/content/pm/ResolveInfo; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 440 */
/* move-object v0, p6 */
/* check-cast v0, Landroid/content/pm/ResolveInfo; */
v0 = (( com.android.server.am.BroadcastQueueModernStubImpl ) p0 ).checkApplicationAutoStart ( p1, p3, v0 ); // invoke-virtual {p0, p1, p3, v0}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->checkApplicationAutoStart(Lcom/android/server/am/BroadcastQueue;Lcom/android/server/am/BroadcastRecord;Landroid/content/pm/ResolveInfo;)Z
/* xor-int/lit8 v0, v0, 0x1 */
/* .line 442 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
void init ( com.android.server.am.ActivityManagerService p0, android.content.Context p1 ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 136 */
this.mAmService = p1;
/* .line 137 */
this.mContext = p2;
/* .line 138 */
return;
} // .end method
public Boolean isInDeferCachedWhiteList ( java.lang.String p0, com.android.server.am.BroadcastRecord p1 ) {
/* .locals 3 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .line 916 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_3 */
/* if-nez p2, :cond_0 */
/* .line 919 */
} // :cond_0
v0 = v0 = com.android.server.am.BroadcastQueueModernStubImpl.DEFER_CACHED_WHITE_LIST;
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget v0, p2, Lcom/android/server/am/BroadcastRecord;->callingPid:I */
/* .line 920 */
v2 = android.os.Process .myPid ( );
/* if-eq v0, v2, :cond_1 */
v0 = this.callerPackage;
/* .line 921 */
final String v2 = "android"; // const-string v2, "android"
v0 = android.text.TextUtils .equals ( v2,v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
/* nop */
/* .line 919 */
} // :goto_0
/* .line 917 */
} // :cond_3
} // :goto_1
} // .end method
Boolean isSKipNotifySms ( com.android.server.am.BroadcastRecord p0, Integer p1, java.lang.String p2, Integer p3 ) {
/* .locals 6 */
/* .param p1, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "appOp" # I */
/* .line 456 */
final String v0 = "BroadcastQueueInjector"; // const-string v0, "BroadcastQueueInjector"
/* const/16 v1, 0x10 */
int v2 = 0; // const/4 v2, 0x0
/* if-eq p4, v1, :cond_0 */
/* .line 457 */
/* .line 459 */
} // :cond_0
v1 = this.intent;
/* .line 461 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v3 = "android.provider.Telephony.SMS_RECEIVED"; // const-string v3, "android.provider.Telephony.SMS_RECEIVED"
(( android.content.Intent ) v1 ).getAction ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_1 */
/* .line 462 */
/* .line 466 */
} // :cond_1
try { // :try_start_0
final String v3 = "miui.intent.SERVICE_NUMBER"; // const-string v3, "miui.intent.SERVICE_NUMBER"
v3 = (( android.content.Intent ) v1 ).getBooleanExtra ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 467 */
v3 = this.mAmService;
v3 = this.mAppOpsService;
/* const/16 v4, 0x2722 */
v3 = (( com.android.server.appop.AppOpsService ) v3 ).checkOperation ( v4, p2, p3 ); // invoke-virtual {v3, v4, p2, p3}, Lcom/android/server/appop/AppOpsService;->checkOperation(IILjava/lang/String;)I
/* .line 469 */
/* .local v3, "mode":I */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 470 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MIUILOG- Sms Filter packageName : "; // const-string v5, "MIUILOG- Sms Filter packageName : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p3 ); // invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " uid "; // const-string v5, " uid "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 471 */
int v0 = 1; // const/4 v0, 0x1
/* .line 476 */
} // .end local v3 # "mode":I
} // :cond_2
/* .line 474 */
/* :catch_0 */
/* move-exception v3 */
/* .line 475 */
/* .local v3, "e":Ljava/lang/Exception; */
final String v4 = "isSKipNotifySms"; // const-string v4, "isSKipNotifySms"
android.util.Slog .e ( v0,v4,v3 );
/* .line 477 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public Boolean isSkip ( com.android.server.am.BroadcastRecord p0, android.content.pm.ResolveInfo p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .param p2, "info" # Landroid/content/pm/ResolveInfo; */
/* .param p3, "appOp" # I */
/* .line 447 */
v0 = this.activityInfo;
v0 = this.applicationInfo;
/* iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
v1 = this.activityInfo;
v1 = this.packageName;
v0 = (( com.android.server.am.BroadcastQueueModernStubImpl ) p0 ).isSKipNotifySms ( p1, v0, v1, p3 ); // invoke-virtual {p0, p1, v0, v1, p3}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isSKipNotifySms(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;I)Z
} // .end method
public Boolean isSkip ( com.android.server.am.BroadcastRecord p0, com.android.server.am.BroadcastFilter p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .param p2, "filter" # Lcom/android/server/am/BroadcastFilter; */
/* .param p3, "appOp" # I */
/* .line 452 */
v0 = this.receiverList;
/* iget v0, v0, Lcom/android/server/am/ReceiverList;->uid:I */
v1 = this.packageName;
v0 = (( com.android.server.am.BroadcastQueueModernStubImpl ) p0 ).isSKipNotifySms ( p1, v0, v1, p3 ); // invoke-virtual {p0, p1, v0, v1, p3}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isSKipNotifySms(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;I)Z
} // .end method
Boolean isSkipForUser ( android.content.pm.ResolveInfo p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "info" # Landroid/content/pm/ResolveInfo; */
/* .param p2, "skip" # Z */
/* .line 481 */
v0 = this.activityInfo;
v0 = this.applicationInfo;
/* iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
v0 = android.os.UserHandle .getUserId ( v0 );
/* .line 482 */
/* .local v0, "userId":I */
v1 = this.mAmService;
int v2 = 0; // const/4 v2, 0x0
v1 = (( com.android.server.am.ActivityManagerService ) v1 ).isUserRunning ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/android/server/am/ActivityManagerService;->isUserRunning(II)Z
/* if-nez v1, :cond_0 */
/* .line 483 */
int p2 = 1; // const/4 p2, 0x1
/* .line 485 */
} // :cond_0
} // .end method
public Integer noteOperationLocked ( Integer p0, Integer p1, java.lang.String p2, android.os.Handler p3, com.android.server.am.BroadcastRecord p4 ) {
/* .locals 26 */
/* .param p1, "appOp" # I */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "handler" # Landroid/os/Handler; */
/* .param p5, "receiverRecord" # Lcom/android/server/am/BroadcastRecord; */
/* .line 573 */
/* move-object/from16 v0, p0 */
/* move/from16 v1, p1 */
/* move/from16 v2, p2 */
/* move-object/from16 v3, p3 */
/* move-object/from16 v4, p5 */
v5 = this.mAmService;
v5 = this.mAppOpsService;
v5 = (( com.android.server.appop.AppOpsService ) v5 ).checkOperation ( v1, v2, v3 ); // invoke-virtual {v5, v1, v2, v3}, Lcom/android/server/appop/AppOpsService;->checkOperation(IILjava/lang/String;)I
/* .line 574 */
/* .local v5, "mode":I */
int v6 = 5; // const/4 v6, 0x5
/* if-eq v5, v6, :cond_0 */
/* .line 575 */
/* .line 578 */
} // :cond_0
v6 = /* invoke-static/range {p2 ..p2}, Landroid/os/UserHandle;->getUserId(I)I */
/* .line 579 */
/* .local v6, "userId":I */
/* const/16 v7, 0x3e7 */
/* if-ne v6, v7, :cond_1 */
/* .line 580 */
/* .line 583 */
} // :cond_1
v7 = (( com.android.server.am.BroadcastQueueModernStubImpl ) v0 ).isSKipNotifySms ( v4, v2, v3, v1 ); // invoke-virtual {v0, v4, v2, v3, v1}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->isSKipNotifySms(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;I)Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 584 */
/* .line 587 */
} // :cond_2
v7 = this.mAmService;
v7 = this.mAppOpsService;
int v8 = 1; // const/4 v8, 0x1
(( com.android.server.appop.AppOpsService ) v7 ).setMode ( v1, v2, v3, v8 ); // invoke-virtual {v7, v1, v2, v3, v8}, Lcom/android/server/appop/AppOpsService;->setMode(IILjava/lang/String;I)V
/* .line 589 */
/* new-instance v7, Landroid/content/Intent; */
final String v9 = "com.miui.intent.action.REQUEST_PERMISSIONS"; // const-string v9, "com.miui.intent.action.REQUEST_PERMISSIONS"
/* invoke-direct {v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 590 */
/* .local v7, "intent":Landroid/content/Intent; */
final String v9 = "com.lbe.security.miui"; // const-string v9, "com.lbe.security.miui"
(( android.content.Intent ) v7 ).setPackage ( v9 ); // invoke-virtual {v7, v9}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 591 */
/* const/high16 v9, 0x18800000 */
(( android.content.Intent ) v7 ).addFlags ( v9 ); // invoke-virtual {v7, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 593 */
final String v9 = "android.intent.extra.PACKAGE_NAME"; // const-string v9, "android.intent.extra.PACKAGE_NAME"
(( android.content.Intent ) v7 ).putExtra ( v9, v3 ); // invoke-virtual {v7, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 594 */
final String v9 = "android.intent.extra.UID"; // const-string v9, "android.intent.extra.UID"
(( android.content.Intent ) v7 ).putExtra ( v9, v2 ); // invoke-virtual {v7, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 595 */
final String v9 = "op"; // const-string v9, "op"
(( android.content.Intent ) v7 ).putExtra ( v9, v1 ); // invoke-virtual {v7, v9, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 596 */
/* iget-boolean v9, v4, Lcom/android/server/am/BroadcastRecord;->sticky:Z */
/* if-nez v9, :cond_7 */
/* .line 597 */
v9 = this.callerPackage;
/* .line 598 */
/* .local v9, "callerPackage":Ljava/lang/String; */
/* iget v15, v4, Lcom/android/server/am/BroadcastRecord;->callingUid:I */
/* .line 599 */
/* .local v15, "callingUid":I */
/* if-nez v9, :cond_5 */
/* .line 600 */
/* if-nez v15, :cond_3 */
/* .line 601 */
final String v9 = "root"; // const-string v9, "root"
/* .line 602 */
} // :cond_3
/* const/16 v10, 0x7d0 */
/* if-ne v15, v10, :cond_4 */
/* .line 603 */
final String v9 = "com.android.shell"; // const-string v9, "com.android.shell"
/* .line 604 */
} // :cond_4
/* const/16 v10, 0x3e8 */
/* if-ne v15, v10, :cond_5 */
/* .line 605 */
final String v9 = "android"; // const-string v9, "android"
/* .line 608 */
} // :cond_5
} // :goto_0
/* if-nez v9, :cond_6 */
/* .line 609 */
/* .line 613 */
} // :cond_6
v23 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/BroadcastQueueModernStubImpl;->getNextRequestIdLocked()I */
/* .line 614 */
/* .local v23, "requestCode":I */
/* new-instance v10, Landroid/content/Intent; */
v11 = this.intent;
/* invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V */
/* move-object v14, v10 */
/* .line 615 */
/* .local v14, "intentNew":Landroid/content/Intent; */
(( android.content.Intent ) v14 ).setPackage ( v3 ); // invoke-virtual {v14, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 616 */
v10 = this.mAmService;
v10 = this.mPendingIntentController;
int v11 = 1; // const/4 v11, 0x1
int v13 = 0; // const/4 v13, 0x0
/* iget v12, v4, Lcom/android/server/am/BroadcastRecord;->userId:I */
/* const/16 v16, 0x0 */
/* const/16 v17, 0x0 */
/* filled-new-array {v14}, [Landroid/content/Intent; */
v8 = this.mContext;
/* .line 623 */
(( android.content.Context ) v8 ).getContentResolver ( ); // invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
(( android.content.Intent ) v14 ).resolveType ( v8 ); // invoke-virtual {v14, v8}, Landroid/content/Intent;->resolveType(Landroid/content/ContentResolver;)Ljava/lang/String;
/* filled-new-array {v8}, [Ljava/lang/String; */
/* const/high16 v21, 0x4c000000 # 3.3554432E7f */
/* const/16 v22, 0x0 */
/* .line 616 */
/* move v8, v12 */
/* move-object v12, v9 */
/* move-object/from16 v24, v14 */
} // .end local v14 # "intentNew":Landroid/content/Intent;
/* .local v24, "intentNew":Landroid/content/Intent; */
/* move v14, v15 */
/* move/from16 v25, v15 */
} // .end local v15 # "callingUid":I
/* .local v25, "callingUid":I */
/* move v15, v8 */
/* move/from16 v18, v23 */
/* invoke-virtual/range {v10 ..v22}, Lcom/android/server/am/PendingIntentController;->getIntentSender(ILjava/lang/String;Ljava/lang/String;IILandroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;)Lcom/android/server/am/PendingIntentRecord; */
/* .line 628 */
/* .local v8, "target":Landroid/content/IIntentSender; */
/* new-instance v10, Landroid/content/IntentSender; */
/* invoke-direct {v10, v8}, Landroid/content/IntentSender;-><init>(Landroid/content/IIntentSender;)V */
final String v11 = "android.intent.extra.INTENT"; // const-string v11, "android.intent.extra.INTENT"
(( android.content.Intent ) v7 ).putExtra ( v11, v10 ); // invoke-virtual {v7, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 631 */
} // .end local v8 # "target":Landroid/content/IIntentSender;
} // .end local v9 # "callerPackage":Ljava/lang/String;
} // .end local v23 # "requestCode":I
} // .end local v24 # "intentNew":Landroid/content/Intent;
} // .end local v25 # "callingUid":I
} // :cond_7
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "MIUILOG - Launching Request permission [Broadcast] uid : "; // const-string v9, "MIUILOG - Launching Request permission [Broadcast] uid : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " pkg : "; // const-string v9, " pkg : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3 ); // invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " op : "; // const-string v9, " op : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v9 = "BroadcastQueueInjector"; // const-string v9, "BroadcastQueueInjector"
android.util.Slog .i ( v9,v8 );
/* .line 634 */
/* const/16 v8, 0x36 */
/* if-ne v1, v8, :cond_8 */
/* const-wide/16 v8, 0x5dc */
} // :cond_8
/* const-wide/16 v8, 0xa */
/* .line 635 */
/* .local v8, "delay":J */
} // :goto_1
/* new-instance v10, Lcom/android/server/am/BroadcastQueueModernStubImpl$1; */
/* invoke-direct {v10, v0, v7, v2}, Lcom/android/server/am/BroadcastQueueModernStubImpl$1;-><init>(Lcom/android/server/am/BroadcastQueueModernStubImpl;Landroid/content/Intent;I)V */
/* move-object/from16 v11, p4 */
(( android.os.Handler ) v11 ).postDelayed ( v10, v8, v9 ); // invoke-virtual {v11, v10, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 646 */
int v10 = 1; // const/4 v10, 0x1
} // .end method
public void notifyFinishBroadcast ( com.android.server.am.BroadcastRecord p0, com.android.server.am.ProcessRecord p1 ) {
/* .locals 5 */
/* .param p1, "r" # Lcom/android/server/am/BroadcastRecord; */
/* .param p2, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 891 */
/* iget-boolean v0, p1, Lcom/android/server/am/BroadcastRecord;->ordered:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 892 */
} // :cond_0
v0 = this.intent;
(( android.content.Intent ) v0 ).getAction ( ); // invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 893 */
/* .local v0, "action":Ljava/lang/String; */
int v1 = -1; // const/4 v1, -0x1
/* .line 894 */
/* .local v1, "uid":I */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 895 */
/* iget v1, p2, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 897 */
} // :cond_1
v2 = this.intent;
v2 = (( android.content.Intent ) v2 ).getFlags ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I
/* const/high16 v3, 0x10000000 */
/* and-int/2addr v2, v3 */
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_2
int v2 = 1; // const/4 v2, 0x1
} // :cond_2
/* move v2, v3 */
/* .line 899 */
/* .local v2, "isforeground":Z */
} // :goto_0
com.miui.server.greeze.GreezeManagerInternal .getInstance ( );
(( com.miui.server.greeze.GreezeManagerInternal ) v4 ).updateOrderBCStatus ( v0, v1, v2, v3 ); // invoke-virtual {v4, v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerInternal;->updateOrderBCStatus(Ljava/lang/String;IZZ)V
/* .line 900 */
return;
} // .end method
