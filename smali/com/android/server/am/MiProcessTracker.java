public class com.android.server.am.MiProcessTracker {
	 /* .source "MiProcessTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MiProcessTracker$AppStartRecordController;, */
	 /* Lcom/android/server/am/MiProcessTracker$ProcessRecordController; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer COMPACTION;
private static final Integer CONT_LOW_WMARK;
private static final Integer CRITICAL_KILL;
public static final Boolean DEBUG;
private static final Integer DIRECT_RECL_AND_LOW_MEM;
private static final Integer DIRECT_RECL_AND_THRASHING;
private static final Integer DIRECT_RECL_AND_THROT;
private static final java.lang.String LMKD_COMPACTION;
private static final java.lang.String LMKD_CONT_LOW_WMARK;
private static final java.lang.String LMKD_CRITICAL_KILL;
private static final java.lang.String LMKD_DIRECT_RECL_AND_LOW_MEM;
private static final java.lang.String LMKD_DIRECT_RECL_AND_THRASHING;
private static final java.lang.String LMKD_DIRECT_RECL_AND_THROT;
private static final java.lang.String LMKD_KILL_REASON_CAMERA;
private static final java.lang.String LMKD_KILL_REASON_UNKNOWN;
private static final java.lang.String LMKD_LOW_FILECACHE_AFTER_THRASHING;
private static final java.lang.String LMKD_LOW_MEM_AND_SWAP;
private static final java.lang.String LMKD_LOW_MEM_AND_SWAP_UTIL;
private static final java.lang.String LMKD_LOW_MEM_AND_THRASHING;
private static final java.lang.String LMKD_LOW_SWAP_AND_LOW_FILE;
private static final java.lang.String LMKD_LOW_SWAP_AND_THRASHING;
private static final java.lang.String LMKD_PERCEPTIBLE_PROTECT;
private static final java.lang.String LMKD_PRESSURE_AFTER_KILL;
private static final java.lang.String LMKD_WILL_THROTTLED;
private static final Integer LOW_FILECACHE_AFTER_THRASHING;
private static final Integer LOW_MEM_AND_SWAP;
private static final Integer LOW_MEM_AND_SWAP_UTIL;
private static final Integer LOW_MEM_AND_THRASHING;
private static final Integer LOW_SWAP_AND_LOW_FILE;
private static final Integer LOW_SWAP_AND_THRASHING;
private static final Integer MINFREE_MODE;
private static final Integer MI_NEW_KILL_REASON_BASE;
private static final Integer MI_UPDATE_KILL_REASON_BASE;
private static final Integer PERCEPTIBLE_PROTECT;
private static final Integer PRESSURE_AFTER_KILL;
public static final java.lang.String TAG;
private static final Integer WILL_THROTTLED;
public static Boolean sEnable;
public static com.android.server.am.MiProcessTracker sInstance;
public static java.util.HashSet trackerPkgWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final com.android.server.am.MiProcessTracker$AppStartRecordController mAppStartRecordController;
private com.android.internal.app.IPerfShielder mPerfShielder;
private final com.android.server.am.MiProcessTracker$ProcessRecordController mProcessRecordController;
private com.miui.app.smartpower.SmartPowerServiceInternal mSmartPowerService;
private Boolean mSystemReady;
/* # direct methods */
static com.android.internal.app.IPerfShielder -$$Nest$fgetmPerfShielder ( com.android.server.am.MiProcessTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPerfShielder;
} // .end method
static com.android.server.am.MiProcessTracker$ProcessRecordController -$$Nest$fgetmProcessRecordController ( com.android.server.am.MiProcessTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mProcessRecordController;
} // .end method
static com.miui.app.smartpower.SmartPowerServiceInternal -$$Nest$fgetmSmartPowerService ( com.android.server.am.MiProcessTracker p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSmartPowerService;
} // .end method
static com.android.server.am.MiProcessTracker ( ) {
/* .locals 2 */
/* .line 28 */
/* nop */
/* .line 29 */
final String v0 = "persist.sys.process.tracker.enable"; // const-string v0, "persist.sys.process.tracker.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.MiProcessTracker.DEBUG = (v0!= 0);
/* .line 31 */
/* sget-boolean v0, Landroid/os/spc/PressureStateSettings;->PROCESS_TRACKER_ENABLE:Z */
com.android.server.am.MiProcessTracker.sEnable = (v0!= 0);
/* .line 33 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
return;
} // .end method
private com.android.server.am.MiProcessTracker ( ) {
/* .locals 2 */
/* .line 117 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 111 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z */
/* .line 118 */
/* new-instance v0, Lcom/android/server/am/MiProcessTracker$AppStartRecordController; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiProcessTracker$AppStartRecordController;-><init>(Lcom/android/server/am/MiProcessTracker;Lcom/android/server/am/MiProcessTracker$AppStartRecordController-IA;)V */
this.mAppStartRecordController = v0;
/* .line 119 */
/* new-instance v0, Lcom/android/server/am/MiProcessTracker$ProcessRecordController; */
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;-><init>(Lcom/android/server/am/MiProcessTracker;Lcom/android/server/am/MiProcessTracker$ProcessRecordController-IA;)V */
this.mProcessRecordController = v0;
/* .line 120 */
return;
} // .end method
public static com.android.server.am.MiProcessTracker getInstance ( ) {
/* .locals 1 */
/* .line 123 */
v0 = com.android.server.am.MiProcessTracker.sInstance;
/* if-nez v0, :cond_0 */
/* .line 124 */
/* new-instance v0, Lcom/android/server/am/MiProcessTracker; */
/* invoke-direct {v0}, Lcom/android/server/am/MiProcessTracker;-><init>()V */
/* .line 126 */
} // :cond_0
v0 = com.android.server.am.MiProcessTracker.sInstance;
} // .end method
private java.lang.String getLmkdKillReason ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "reason" # I */
/* .line 372 */
/* const/16 v0, 0xfa */
final String v1 = "lmkdUnknown"; // const-string v1, "lmkdUnknown"
/* if-le p1, v0, :cond_0 */
/* .line 373 */
/* add-int/lit16 v0, p1, -0xfa */
/* packed-switch v0, :pswitch_data_0 */
/* .line 379 */
/* .line 377 */
/* :pswitch_0 */
final String v0 = "lmkdWillThrottled"; // const-string v0, "lmkdWillThrottled"
/* .line 375 */
/* :pswitch_1 */
final String v0 = "lmkdContLowLwmark"; // const-string v0, "lmkdContLowLwmark"
/* .line 381 */
} // :cond_0
/* const/16 v0, 0xc8 */
/* if-le p1, v0, :cond_1 */
/* .line 382 */
/* add-int/lit16 p1, p1, -0xc8 */
/* .line 384 */
} // :cond_1
/* packed-switch p1, :pswitch_data_1 */
/* .line 414 */
/* .line 412 */
/* :pswitch_2 */
final String v0 = "cameraKill"; // const-string v0, "cameraKill"
/* .line 410 */
/* :pswitch_3 */
final String v0 = "lmkdPerceptibleProtect"; // const-string v0, "lmkdPerceptibleProtect"
/* .line 408 */
/* :pswitch_4 */
final String v0 = "lmkdLowSwapAndLowFile"; // const-string v0, "lmkdLowSwapAndLowFile"
/* .line 406 */
/* :pswitch_5 */
final String v0 = "lmkdDirectReclAndLowMem"; // const-string v0, "lmkdDirectReclAndLowMem"
/* .line 404 */
/* :pswitch_6 */
final String v0 = "lmkdDirectReclAndThrot"; // const-string v0, "lmkdDirectReclAndThrot"
/* .line 402 */
/* :pswitch_7 */
final String v0 = "lmkdCompaction"; // const-string v0, "lmkdCompaction"
/* .line 400 */
/* :pswitch_8 */
final String v0 = "lmkdLowFileCacheAfterThrashing"; // const-string v0, "lmkdLowFileCacheAfterThrashing"
/* .line 398 */
/* :pswitch_9 */
final String v0 = "lmkdLowMemAndSwapUtil"; // const-string v0, "lmkdLowMemAndSwapUtil"
/* .line 396 */
/* :pswitch_a */
final String v0 = "lmkdDirectReclAndThrashing"; // const-string v0, "lmkdDirectReclAndThrashing"
/* .line 394 */
/* :pswitch_b */
final String v0 = "lmkdLowMemAndThrashing"; // const-string v0, "lmkdLowMemAndThrashing"
/* .line 392 */
/* :pswitch_c */
final String v0 = "lmkdLowMemAndSwap"; // const-string v0, "lmkdLowMemAndSwap"
/* .line 390 */
/* :pswitch_d */
final String v0 = "lmkdLowSwapAndThrashing"; // const-string v0, "lmkdLowSwapAndThrashing"
/* .line 388 */
/* :pswitch_e */
final String v0 = "lmkdCriticalKill"; // const-string v0, "lmkdCriticalKill"
/* .line 386 */
/* :pswitch_f */
final String v0 = "lmkdPressureAfterKill"; // const-string v0, "lmkdPressureAfterKill"
/* :pswitch_data_0 */
/* .packed-switch 0x3 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public void onActivityLaunched ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .param p4, "launchState" # I */
/* .line 149 */
/* iget-boolean v0, p0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z */
/* if-nez v0, :cond_0 */
/* .line 150 */
return;
/* .line 152 */
} // :cond_0
v0 = this.mAppStartRecordController;
com.android.server.am.MiProcessTracker$AppStartRecordController .-$$Nest$monActivityLaunched ( v0,p1,p2,p3,p4 );
/* .line 153 */
return;
} // .end method
public void recordAmKillProcess ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
/* .locals 18 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 156 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* iget-boolean v2, v0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z */
/* if-nez v2, :cond_0 */
/* .line 157 */
return;
/* .line 159 */
} // :cond_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 160 */
v2 = this.processName;
/* .line 161 */
/* .local v2, "processName":Ljava/lang/String; */
v3 = this.info;
v13 = this.packageName;
/* .line 162 */
/* .local v13, "packageName":Ljava/lang/String; */
v3 = this.info;
/* iget v14, v3, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 163 */
/* .local v14, "uid":I */
v3 = this.mState;
v15 = (( com.android.server.am.ProcessStateRecord ) v3 ).getSetAdj ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessStateRecord;->getSetAdj()I
/* .line 164 */
/* .local v15, "adj":I */
v3 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v3 ).getLastPss ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v16 */
/* .line 165 */
/* .local v16, "pss":J */
v3 = this.mProcessRecordController;
/* const-wide/16 v11, 0x0 */
/* move-object v4, v13 */
/* move-object v5, v2 */
/* move v6, v14 */
/* move-object/from16 v7, p2 */
/* move v8, v15 */
/* move-wide/from16 v9, v16 */
/* invoke-static/range {v3 ..v12}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->-$$Nest$mrecordKillProcessIfNeed(Lcom/android/server/am/MiProcessTracker$ProcessRecordController;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJ)V */
/* .line 168 */
} // .end local v2 # "processName":Ljava/lang/String;
} // .end local v13 # "packageName":Ljava/lang/String;
} // .end local v14 # "uid":I
} // .end local v15 # "adj":I
} // .end local v16 # "pss":J
} // :cond_1
return;
} // .end method
public void recordLmkKillProcess ( Integer p0, Integer p1, Long p2, Integer p3, Integer p4, Integer p5, Integer p6, java.lang.String p7 ) {
/* .locals 17 */
/* .param p1, "uid" # I */
/* .param p2, "oomScore" # I */
/* .param p3, "rssInBytes" # J */
/* .param p5, "freeMemKb" # I */
/* .param p6, "freeSwapKb" # I */
/* .param p7, "killReason" # I */
/* .param p8, "thrashing" # I */
/* .param p9, "processName" # Ljava/lang/String; */
/* .line 173 */
/* move-object/from16 v0, p0 */
/* iget-boolean v1, v0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z */
/* if-nez v1, :cond_0 */
/* .line 174 */
return;
/* .line 176 */
} // :cond_0
v1 = this.mSmartPowerService;
/* .line 177 */
/* move/from16 v12, p1 */
/* move-object/from16 v13, p9 */
/* .line 178 */
/* .local v1, "runningProcess":Lcom/miui/server/smartpower/IAppState$IRunningProcess; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 179 */
(( com.miui.server.smartpower.IAppState$IRunningProcess ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Lcom/miui/server/smartpower/IAppState$IRunningProcess;->getPackageName()Ljava/lang/String;
/* .line 180 */
/* .local v14, "packageName":Ljava/lang/String; */
/* move/from16 v15, p7 */
/* invoke-direct {v0, v15}, Lcom/android/server/am/MiProcessTracker;->getLmkdKillReason(I)Ljava/lang/String; */
/* .line 181 */
/* .local v16, "reason":Ljava/lang/String; */
v2 = this.mProcessRecordController;
/* const-wide/16 v8, 0x0 */
/* const-wide/16 v3, 0x400 */
/* div-long v10, p3, v3 */
/* move-object v3, v14 */
/* move-object/from16 v4, p9 */
/* move/from16 v5, p1 */
/* move-object/from16 v6, v16 */
/* move/from16 v7, p2 */
/* invoke-static/range {v2 ..v11}, Lcom/android/server/am/MiProcessTracker$ProcessRecordController;->-$$Nest$mrecordKillProcessIfNeed(Lcom/android/server/am/MiProcessTracker$ProcessRecordController;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJ)V */
/* .line 178 */
} // .end local v14 # "packageName":Ljava/lang/String;
} // .end local v16 # "reason":Ljava/lang/String;
} // :cond_1
/* move/from16 v15, p7 */
/* .line 184 */
} // :goto_0
return;
} // .end method
public void systemReady ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 130 */
/* sget-boolean v0, Lcom/android/server/am/MiProcessTracker;->sEnable:Z */
/* if-nez v0, :cond_0 */
/* .line 131 */
return;
/* .line 133 */
} // :cond_0
com.miui.daemon.performance.PerfShielderManager .getService ( );
this.mPerfShielder = v0;
/* .line 134 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
this.mSmartPowerService = v0;
/* .line 135 */
v0 = com.android.server.am.MiProcessTracker.trackerPkgWhiteList;
/* .line 136 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300ad */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 135 */
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 137 */
v0 = com.android.server.am.MiProcessTracker.trackerPkgWhiteList;
/* .line 138 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300bb */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 137 */
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 139 */
v0 = com.android.server.am.MiProcessTracker.trackerPkgWhiteList;
/* .line 140 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300ac */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 139 */
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 141 */
v0 = com.android.server.am.MiProcessTracker.trackerPkgWhiteList;
/* .line 142 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110300ba */
(( android.content.res.Resources ) v1 ).getStringArray ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 141 */
java.util.Arrays .asList ( v1 );
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 144 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/MiProcessTracker;->mSystemReady:Z */
/* .line 145 */
return;
} // .end method
