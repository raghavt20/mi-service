.class public Lcom/android/server/am/AppErrorDialogImpl;
.super Lcom/android/server/am/AppErrorDialogStub;
.source "AppErrorDialogImpl.java"


# instance fields
.field private mCrashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

.field private mCrashPackage:Ljava/lang/String;

.field private mDialog:Lcom/android/server/am/AppErrorDialog;

.field private final mReported:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public static synthetic $r8$lambda$VAjEk5XpMvIZpCLxECcApzP_6Z8(Lcom/android/server/am/AppErrorDialogImpl;Landroid/content/Context;Landroid/os/Message;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/am/AppErrorDialogImpl;->lambda$onInit$0(Landroid/content/Context;Landroid/os/Message;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monReportClicked(Lcom/android/server/am/AppErrorDialogImpl;Landroid/content/Context;Landroid/os/Message;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/AppErrorDialogImpl;->onReportClicked(Landroid/content/Context;Landroid/os/Message;Z)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 20
    invoke-direct {p0}, Lcom/android/server/am/AppErrorDialogStub;-><init>()V

    .line 23
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/server/am/AppErrorDialogImpl;->mReported:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private synthetic lambda$onInit$0(Landroid/content/Context;Landroid/os/Message;Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reportMsg"    # Landroid/os/Message;
    .param p3, "dialog1"    # Landroid/content/DialogInterface;
    .param p4, "which"    # I

    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/server/am/AppErrorDialogImpl;->onReportClicked(Landroid/content/Context;Landroid/os/Message;Z)V

    return-void
.end method

.method private onReportClicked(Landroid/content/Context;Landroid/os/Message;Z)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "reportMsg"    # Landroid/os/Message;
    .param p3, "jumpToPreview"    # Z

    .line 73
    iget-object v0, p0, Lcom/android/server/am/AppErrorDialogImpl;->mReported:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    return-void

    .line 76
    :cond_0
    if-eqz p3, :cond_1

    .line 77
    iget-object v0, p0, Lcom/android/server/am/AppErrorDialogImpl;->mCrashPackage:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/am/AppErrorDialogImpl;->mCrashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    invoke-static {p1, v0, v1}, Lcom/android/server/am/MiuiErrorReport;->startFcPreviewActivity(Landroid/content/Context;Ljava/lang/String;Landroid/app/ApplicationErrorReport$CrashInfo;)V

    .line 79
    :cond_1
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    .line 80
    return-void
.end method


# virtual methods
.method onCreate()Z
    .locals 4

    .line 62
    iget-object v0, p0, Lcom/android/server/am/AppErrorDialogImpl;->mDialog:Lcom/android/server/am/AppErrorDialog;

    .line 63
    invoke-virtual {v0}, Lcom/android/server/am/AppErrorDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "id"

    const-string v2, "miuix.stub"

    const-string v3, "message"

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 64
    .local v0, "msgViewId":I
    iget-object v1, p0, Lcom/android/server/am/AppErrorDialogImpl;->mDialog:Lcom/android/server/am/AppErrorDialog;

    invoke-virtual {v1, v0}, Lcom/android/server/am/AppErrorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 65
    .local v1, "msgView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 66
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 68
    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method onInit(Lcom/android/server/am/AppErrorDialog;Landroid/os/Message;Landroid/os/Message;)V
    .locals 9
    .param p1, "dialog"    # Lcom/android/server/am/AppErrorDialog;
    .param p2, "forceQuitMsg"    # Landroid/os/Message;
    .param p3, "reportMsg"    # Landroid/os/Message;

    .line 29
    iput-object p1, p0, Lcom/android/server/am/AppErrorDialogImpl;->mDialog:Lcom/android/server/am/AppErrorDialog;

    .line 30
    invoke-virtual {p1}, Lcom/android/server/am/AppErrorDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 31
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 32
    .local v1, "res":Landroid/content/res/Resources;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x110f01e9

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 33
    .local v2, "dialogBody":Ljava/lang/String;
    const v3, 0x110f01ea

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "dialogDumpToPreview":Ljava/lang/String;
    new-instance v4, Landroid/text/SpannableStringBuilder;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 36
    .local v4, "textBuilder":Landroid/text/SpannableStringBuilder;
    new-instance v5, Lcom/android/server/am/AppErrorDialogImpl$1;

    invoke-direct {v5, p0, v0, p3}, Lcom/android/server/am/AppErrorDialogImpl$1;-><init>(Lcom/android/server/am/AppErrorDialogImpl;Landroid/content/Context;Landroid/os/Message;)V

    .line 42
    .local v5, "span":Landroid/text/style/ClickableSpan;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    .line 43
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    .line 42
    const/16 v8, 0x21

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 45
    invoke-virtual {p1, v4}, Lcom/android/server/am/AppErrorDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 46
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Lcom/android/server/am/AppErrorDialog;->setCancelable(Z)V

    .line 47
    nop

    .line 48
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x110f0028

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    new-instance v7, Lcom/android/server/am/AppErrorDialogImpl$$ExternalSyntheticLambda0;

    invoke-direct {v7, p0, v0, p3}, Lcom/android/server/am/AppErrorDialogImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/AppErrorDialogImpl;Landroid/content/Context;Landroid/os/Message;)V

    .line 47
    const/4 v8, -0x1

    invoke-virtual {p1, v8, v6, v7}, Lcom/android/server/am/AppErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 50
    nop

    .line 51
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x1040000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    .line 50
    const/4 v7, -0x2

    invoke-virtual {p1, v7, v6, p2}, Lcom/android/server/am/AppErrorDialog;->setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V

    .line 53
    return-void
.end method

.method setCrashInfo(Ljava/lang/String;Landroid/app/ApplicationErrorReport$CrashInfo;)V
    .locals 0
    .param p1, "crashPkg"    # Ljava/lang/String;
    .param p2, "crashInfo"    # Landroid/app/ApplicationErrorReport$CrashInfo;

    .line 57
    iput-object p1, p0, Lcom/android/server/am/AppErrorDialogImpl;->mCrashPackage:Ljava/lang/String;

    .line 58
    iput-object p2, p0, Lcom/android/server/am/AppErrorDialogImpl;->mCrashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    .line 59
    return-void
.end method
