public class com.android.server.am.GameProcessKiller implements com.android.server.am.IGameProcessAction {
	 /* .source "GameProcessKiller.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;, */
	 /* Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String KILL_REASON;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.am.GameProcessKiller$GameProcessKillerConfig mConfig;
private com.android.server.am.GameMemoryReclaimer mReclaimer;
/* # direct methods */
static com.android.server.am.GameProcessKiller ( ) {
	 /* .locals 1 */
	 /* .line 31 */
	 /* const-class v0, Lcom/android/server/am/GameProcessKiller; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 return;
} // .end method
 com.android.server.am.GameProcessKiller ( ) {
	 /* .locals 0 */
	 /* .param p1, "reclaimer" # Lcom/android/server/am/GameMemoryReclaimer; */
	 /* .param p2, "cfg" # Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig; */
	 /* .line 38 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 39 */
	 this.mReclaimer = p1;
	 /* .line 40 */
	 this.mConfig = p2;
	 /* .line 41 */
	 return;
} // .end method
private Boolean isUidSystem ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p1, "uid" # I */
	 /* .line 45 */
	 /* const v0, 0x186a0 */
	 /* rem-int/2addr p1, v0 */
	 /* .line 47 */
	 /* const/16 v0, 0x2710 */
	 /* if-ge p1, v0, :cond_0 */
	 int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
public Long doAction ( Long p0 ) {
/* .locals 16 */
/* .param p1, "need" # J */
/* .line 99 */
/* move-object/from16 v1, p0 */
/* const-wide/16 v2, 0x0 */
/* .line 100 */
/* .local v2, "reclaim":J */
int v0 = 0; // const/4 v0, 0x0
/* .line 105 */
/* .local v0, "num":I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "doKill:"; // const-string v5, "doKill:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mConfig;
/* iget v5, v5, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const-wide/32 v5, 0x80000 */
android.os.Trace .traceBegin ( v5,v6,v4 );
/* .line 106 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* .line 107 */
/* .local v7, "time":J */
v4 = this.mReclaimer;
(( com.android.server.am.GameMemoryReclaimer ) v4 ).filterPackageInfos ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/am/GameMemoryReclaimer;->filterPackageInfos(Lcom/android/server/am/IGameProcessAction;)Ljava/util/List;
/* .line 108 */
/* .local v4, "packageList":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/GameProcessKiller$PackageMemInfo;>;" */
/* const-class v9, Landroid/app/ActivityManagerInternal; */
/* .line 109 */
com.android.server.LocalServices .getService ( v9 );
/* check-cast v9, Landroid/app/ActivityManagerInternal; */
/* .line 110 */
/* .local v9, "amInternal":Landroid/app/ActivityManagerInternal; */
v10 = if ( v4 != null) { // if-eqz v4, :cond_2
/* if-lez v10, :cond_2 */
/* .line 111 */
/* move-wide v11, v2 */
/* move v2, v0 */
} // .end local v0 # "num":I
/* .local v2, "num":I */
/* .local v11, "reclaim":J */
v0 = } // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* move-object v3, v0 */
/* check-cast v3, Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* .line 112 */
/* .local v3, "pkg":Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
v0 = this.mReclaimer;
/* iget v13, v3, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mUid:I */
(( com.android.server.am.GameMemoryReclaimer ) v0 ).getPackageNameByUid ( v13 ); // invoke-virtual {v0, v13}, Lcom/android/server/am/GameMemoryReclaimer;->getPackageNameByUid(I)Ljava/lang/String;
/* .line 113 */
/* .local v13, "name":Ljava/lang/String; */
v0 = this.mReclaimer;
v14 = this.mActivityManagerService;
/* monitor-enter v14 */
/* .line 114 */
/* nop */
/* .line 115 */
try { // :try_start_0
v0 = android.os.UserHandle .getCallingUserId ( );
final String v15 = "game memory reclaim"; // const-string v15, "game memory reclaim"
/* .line 114 */
(( android.app.ActivityManagerInternal ) v9 ).forceStopPackage ( v13, v0, v15 ); // invoke-virtual {v9, v13, v0, v15}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 116 */
/* monitor-exit v14 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 117 */
/* iget-wide v14, v3, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mMemSize:J */
/* add-long/2addr v11, v14 */
/* .line 118 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 119 */
/* cmp-long v0, v11, p1 */
/* if-ltz v0, :cond_0 */
/* .line 120 */
/* move v0, v2 */
/* move-wide v2, v11 */
/* .line 121 */
} // .end local v3 # "pkg":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
} // :cond_0
/* .line 116 */
/* .restart local v3 # "pkg":Lcom/android/server/am/GameProcessKiller$PackageMemInfo; */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v14 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 111 */
} // .end local v3 # "pkg":Lcom/android/server/am/GameProcessKiller$PackageMemInfo;
} // .end local v13 # "name":Ljava/lang/String;
} // :cond_1
/* move v0, v2 */
/* move-wide v2, v11 */
/* .line 123 */
} // .end local v11 # "reclaim":J
/* .restart local v0 # "num":I */
/* .local v2, "reclaim":J */
} // :cond_2
} // :goto_1
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v10 */
/* sub-long/2addr v10, v7 */
/* .line 124 */
} // .end local v7 # "time":J
/* .local v10, "time":J */
v7 = com.android.server.am.GameProcessKiller.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "kill "; // const-string v12, "kill "
(( java.lang.StringBuilder ) v8 ).append ( v12 ); // invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v12 = " packages, and reclaim mem: "; // const-string v12, " packages, and reclaim mem: "
(( java.lang.StringBuilder ) v8 ).append ( v12 ); // invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v2, v3 ); // invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v12 = ", during "; // const-string v12, ", during "
(( java.lang.StringBuilder ) v8 ).append ( v12 ); // invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v10, v11 ); // invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v12 = "ms"; // const-string v12, "ms"
(( java.lang.StringBuilder ) v8 ).append ( v12 ); // invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v8 );
/* .line 125 */
android.os.Trace .traceEnd ( v5,v6 );
/* .line 126 */
/* return-wide v2 */
} // .end method
Integer getMinProcState ( ) {
/* .locals 1 */
/* .line 51 */
v0 = this.mConfig;
/* iget v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mMinProcState:I */
} // .end method
Integer getPrio ( ) {
/* .locals 1 */
/* .line 55 */
v0 = this.mConfig;
/* iget v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mPrio:I */
} // .end method
Boolean shouldSkip ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 59 */
v0 = /* invoke-direct {p0, p1}, Lcom/android/server/am/GameProcessKiller;->isUidSystem(I)Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 60 */
/* .line 62 */
} // :cond_0
v0 = this.mConfig;
/* iget-boolean v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipActive:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 63 */
com.miui.server.process.ProcessManagerInternal .getInstance ( );
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getProcessPolicy ( ); // invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessPolicy()Lcom/android/server/am/IProcessPolicy;
/* .line 64 */
int v2 = 3; // const/4 v2, 0x3
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 65 */
/* .line 69 */
} // :cond_1
try { // :try_start_0
com.miui.server.process.ProcessManagerInternal .getInstance ( );
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getForegroundInfo ( ); // invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getForegroundInfo()Lmiui/process/ForegroundInfo;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 72 */
/* .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo; */
/* .line 70 */
} // .end local v0 # "foregroundInfo":Lmiui/process/ForegroundInfo;
/* :catch_0 */
/* move-exception v0 */
/* .line 71 */
/* .local v0, "ex":Landroid/os/RemoteException; */
int v2 = 0; // const/4 v2, 0x0
/* move-object v0, v2 */
/* .line 73 */
/* .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo; */
} // :goto_0
/* if-nez v0, :cond_2 */
/* .line 74 */
/* .line 76 */
} // :cond_2
/* iget v2, v0, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
/* if-eq p1, v2, :cond_4 */
/* iget v2, v0, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I */
/* if-ne p1, v2, :cond_3 */
/* .line 79 */
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
/* .line 78 */
} // :cond_4
} // :goto_1
} // .end method
public Boolean shouldSkip ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 4 */
/* .param p1, "proc" # Lcom/android/server/am/ProcessRecord; */
/* .line 84 */
/* iget v0, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
v0 = (( com.android.server.am.GameProcessKiller ) p0 ).shouldSkip ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/am/GameProcessKiller;->shouldSkip(I)Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 85 */
/* .line 86 */
} // :cond_0
com.miui.server.process.ProcessManagerInternal .getInstance ( );
(( com.miui.server.process.ProcessManagerInternal ) v0 ).getProcessPolicy ( ); // invoke-virtual {v0}, Lcom/miui/server/process/ProcessManagerInternal;->getProcessPolicy()Lcom/android/server/am/IProcessPolicy;
v2 = this.info;
v2 = this.packageName;
/* .line 87 */
v0 = v3 = android.os.UserHandle .getCallingUserId ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 88 */
/* .line 89 */
} // :cond_1
v0 = this.mConfig;
v0 = this.mWhiteList;
v2 = this.info;
v0 = v2 = this.packageName;
/* if-nez v0, :cond_4 */
v0 = this.mConfig;
v0 = this.mWhiteList;
v0 = v2 = this.processName;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 91 */
} // :cond_2
v0 = this.mConfig;
/* iget-boolean v0, v0, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->mSkipForeground:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
(( com.android.server.am.ProcessRecord ) p1 ).getWindowProcessController ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
v0 = (( com.android.server.wm.WindowProcessController ) v0 ).isInterestingToUser ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isInterestingToUser()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 92 */
/* .line 94 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
/* .line 90 */
} // :cond_4
} // :goto_0
} // .end method
