class com.android.server.am.MiuiMemoryService$ConnectionHandler implements java.lang.Runnable {
	 /* .source "MiuiMemoryService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MiuiMemoryService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ConnectionHandler" */
} // .end annotation
/* # instance fields */
private java.io.InputStreamReader mInput;
private Boolean mIsContinue;
private android.net.LocalSocket mSocket;
final com.android.server.am.MiuiMemoryService this$0; //synthetic
/* # direct methods */
public com.android.server.am.MiuiMemoryService$ConnectionHandler ( ) {
/* .locals 0 */
/* .param p2, "clientSocket" # Landroid/net/LocalSocket; */
/* .line 109 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 106 */
int p1 = 1; // const/4 p1, 0x1
/* iput-boolean p1, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mIsContinue:Z */
/* .line 107 */
int p1 = 0; // const/4 p1, 0x0
this.mInput = p1;
/* .line 110 */
this.mSocket = p2;
/* .line 111 */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 9 */
/* .line 120 */
final String v0 = "mi_reclaim connection: "; // const-string v0, "mi_reclaim connection: "
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mi_reclaim new connection: "; // const-string v2, "mi_reclaim new connection: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mSocket;
(( android.net.LocalSocket ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiMemoryService"; // const-string v2, "MiuiMemoryService"
android.util.Slog .d ( v2,v1 );
/* .line 121 */
int v1 = 0; // const/4 v1, 0x0
/* .line 123 */
/* .local v1, "bufferedReader":Ljava/io/BufferedReader; */
try { // :try_start_0
	 /* new-instance v3, Ljava/io/InputStreamReader; */
	 v4 = this.mSocket;
	 (( android.net.LocalSocket ) v4 ).getInputStream ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
	 /* invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
	 this.mInput = v3;
	 /* .line 124 */
	 /* new-instance v3, Ljava/io/BufferedReader; */
	 v4 = this.mInput;
	 /* invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
	 /* move-object v1, v3 */
	 /* .line 125 */
} // :goto_0
/* iget-boolean v3, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mIsContinue:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
	 /* .line 126 */
	 (( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
	 /* .line 127 */
	 /* .local v3, "data":Ljava/lang/String; */
	 final String v4 = ":"; // const-string v4, ":"
	 (( java.lang.String ) v3 ).split ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
	 /* .line 128 */
	 /* .local v4, "result":[Ljava/lang/String; */
	 int v5 = 0; // const/4 v5, 0x0
	 /* aget-object v6, v4, v5 */
	 if ( v6 != null) { // if-eqz v6, :cond_2
		 int v6 = 1; // const/4 v6, 0x1
		 /* aget-object v7, v4, v6 */
		 /* if-nez v7, :cond_0 */
		 /* .line 132 */
	 } // :cond_0
	 /* aget-object v5, v4, v5 */
	 (( java.lang.String ) v5 ).trim ( ); // invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
	 v5 = 	 java.lang.Integer .parseInt ( v5 );
	 /* .line 133 */
	 /* .local v5, "vmPressureLevel":I */
	 /* aget-object v6, v4, v6 */
	 (( java.lang.String ) v6 ).trim ( ); // invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
	 v6 = 	 java.lang.Integer .parseInt ( v6 );
	 /* .line 134 */
	 /* .local v6, "vmPressurePolicy":I */
	 /* sget-boolean v7, Lcom/android/server/am/MiuiMemoryService;->DEBUG:Z */
	 if ( v7 != null) { // if-eqz v7, :cond_1
		 /* .line 135 */
		 /* new-instance v7, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v8, "vmPressureLevel = " */
		 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v8 = " , vmPressurePolicy = "; // const-string v8, " , vmPressurePolicy = "
		 (( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .d ( v2,v7 );
		 /* .line 138 */
	 } // :cond_1
	 v7 = this.this$0;
	 com.android.server.am.MiuiMemoryService .-$$Nest$mhandleReclaimTrigger ( v7,v5,v6 );
	 /* .line 139 */
} // .end local v3 # "data":Ljava/lang/String;
} // .end local v4 # "result":[Ljava/lang/String;
} // .end local v5 # "vmPressureLevel":I
} // .end local v6 # "vmPressurePolicy":I
/* .line 129 */
/* .restart local v3 # "data":Ljava/lang/String; */
/* .restart local v4 # "result":[Ljava/lang/String; */
} // :cond_2
} // :goto_1
final String v5 = "Received mi_reclaim data error"; // const-string v5, "Received mi_reclaim data error"
android.util.Slog .e ( v2,v5 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 130 */
/* .line 147 */
} // .end local v3 # "data":Ljava/lang/String;
} // .end local v4 # "result":[Ljava/lang/String;
} // :cond_3
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 150 */
} // :cond_4
} // :goto_2
/* .line 148 */
/* :catch_0 */
/* move-exception v0 */
/* .line 151 */
/* .line 146 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 142 */
/* :catch_1 */
/* move-exception v3 */
/* .line 143 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( com.android.server.am.MiuiMemoryService$ConnectionHandler ) p0 ).terminate ( ); // invoke-virtual {p0}, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->terminate()V
/* .line 144 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).toString ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " Exception"; // const-string v4, " Exception"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v0 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 147 */
} // .end local v3 # "e":Ljava/lang/Exception;
if ( v1 != null) { // if-eqz v1, :cond_4
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 140 */
/* :catch_2 */
/* move-exception v3 */
/* .line 141 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_4
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).toString ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " IOException"; // const-string v4, " IOException"
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v0 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 147 */
} // .end local v3 # "e":Ljava/io/IOException;
if ( v1 != null) { // if-eqz v1, :cond_4
try { // :try_start_5
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 152 */
} // :goto_3
return;
/* .line 147 */
} // :goto_4
if ( v1 != null) { // if-eqz v1, :cond_5
try { // :try_start_6
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .line 148 */
/* :catch_3 */
/* move-exception v2 */
/* .line 150 */
} // :cond_5
} // :goto_5
/* nop */
/* .line 151 */
} // :goto_6
/* throw v0 */
} // .end method
public void terminate ( ) {
/* .locals 2 */
/* .line 114 */
final String v0 = "MiuiMemoryService"; // const-string v0, "MiuiMemoryService"
final String v1 = "Reclaim trigger terminate!"; // const-string v1, "Reclaim trigger terminate!"
android.util.Slog .d ( v0,v1 );
/* .line 115 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/MiuiMemoryService$ConnectionHandler;->mIsContinue:Z */
/* .line 116 */
return;
} // .end method
