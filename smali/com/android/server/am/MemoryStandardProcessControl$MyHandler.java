class com.android.server.am.MemoryStandardProcessControl$MyHandler extends android.os.Handler {
	 /* .source "MemoryStandardProcessControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryStandardProcessControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MyHandler" */
} // .end annotation
/* # instance fields */
final com.android.server.am.MemoryStandardProcessControl this$0; //synthetic
/* # direct methods */
public com.android.server.am.MemoryStandardProcessControl$MyHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 813 */
this.this$0 = p1;
/* .line 814 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 815 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 819 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "MemoryStandardProcessControl"; // const-string v1, "MemoryStandardProcessControl"
/* packed-switch v0, :pswitch_data_0 */
/* .line 833 */
/* :pswitch_0 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
v2 = this.obj;
/* check-cast v2, Ljava/lang/String; */
com.android.server.am.MemoryStandardProcessControl .-$$Nest$mhandleAppHeapWhenBackground ( v0,v1,v2 );
/* .line 834 */
/* .line 841 */
/* :pswitch_1 */
v0 = this.this$0;
v0 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 842 */
v0 = this.this$0;
v0 = this.mAppHeapStandard;
(( java.util.HashMap ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 836 */
/* :pswitch_2 */
v0 = this.this$0;
v0 = this.mWarnedAppMap;
(( java.util.HashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 837 */
/* .local v2, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
(( com.android.server.am.MemoryStandardProcessControl$AppInfo ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* .line 838 */
} // .end local v2 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
/* .line 839 */
} // :cond_0
/* .line 830 */
/* :pswitch_3 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.android.server.am.MemoryStandardProcessControl .-$$Nest$mhandleMemPressure ( v0,v1 );
/* .line 831 */
/* .line 827 */
/* :pswitch_4 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.am.MemoryStandardProcessControl ) v0 ).killProcess ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->killProcess(Z)V
/* .line 828 */
/* .line 824 */
/* :pswitch_5 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.am.MemoryStandardProcessControl ) v0 ).killProcess ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->killProcess(Z)V
/* .line 825 */
/* .line 821 */
/* :pswitch_6 */
v0 = this.this$0;
(( com.android.server.am.MemoryStandardProcessControl ) v0 ).periodicMemoryDetection ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl;->periodicMemoryDetection()V
/* .line 822 */
/* nop */
/* .line 846 */
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
