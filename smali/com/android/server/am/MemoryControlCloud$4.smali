.class Lcom/android/server/am/MemoryControlCloud$4;
.super Landroid/database/ContentObserver;
.source "MemoryControlCloud.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MemoryControlCloud;->registerCloudAppHeapHandleTimeObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryControlCloud;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryControlCloud;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 149
    iput-object p1, p0, Lcom/android/server/am/MemoryControlCloud$4;->this$0:Lcom/android/server/am/MemoryControlCloud;

    iput-object p3, p0, Lcom/android/server/am/MemoryControlCloud$4;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 152
    const-string v0, "MemoryStandardProcessControl"

    if-eqz p2, :cond_0

    const-string v1, "cloud_memory_standard_appheap_handle_time"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/MemoryControlCloud$4;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, -0x2

    invoke-static {v2, v1, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 156
    .local v1, "str":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 157
    .local v2, "handleTime":J
    iget-object v4, p0, Lcom/android/server/am/MemoryControlCloud$4;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v4}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v4

    iput-wide v2, v4, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    .end local v1    # "str":Ljava/lang/String;
    .end local v2    # "handleTime":J
    goto :goto_0

    .line 158
    :catch_0
    move-exception v1

    .line 159
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cloud CLOUD_MRMORY_STANDARD_APPHEAP_HANDLE_TIME check failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set app heap handle time from database: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/MemoryControlCloud$4;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v2}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v2

    iget-wide v2, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_0
    return-void
.end method
