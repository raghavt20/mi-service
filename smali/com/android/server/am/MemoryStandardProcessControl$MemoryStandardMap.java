public class com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap {
	 /* .source "MemoryStandardProcessControl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/MemoryStandardProcessControl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "MemoryStandardMap" */
} // .end annotation
/* # instance fields */
private java.util.Set mNativeProcessCmds;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mStandardMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.am.MemoryStandardProcessControl this$0; //synthetic
/* # direct methods */
static java.util.Set -$$Nest$fgetmNativeProcessCmds ( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNativeProcessCmds;
} // .end method
public com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/am/MemoryStandardProcessControl; */
/* .line 1299 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 1301 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mStandardMap = v0;
/* .line 1303 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mNativeProcessCmds = v0;
return;
} // .end method
/* # virtual methods */
public void clear ( ) {
/* .locals 1 */
/* .line 1358 */
v0 = this.mStandardMap;
/* .line 1359 */
v0 = this.mNativeProcessCmds;
/* .line 1360 */
return;
} // .end method
public Boolean containsPackage ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1354 */
v0 = v0 = this.mStandardMap;
} // .end method
public java.util.Set getNativeProcessCmds ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1334 */
v0 = this.mNativeProcessCmds;
} // .end method
public Long getPss ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1342 */
v0 = this.mStandardMap;
/* check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
/* .line 1343 */
/* .local v0, "memoryStandard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1344 */
/* iget-wide v1, v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->mPss:J */
/* return-wide v1 */
/* .line 1346 */
} // :cond_0
/* const-wide/16 v1, 0x0 */
/* return-wide v1 */
} // .end method
public com.android.server.am.MemoryStandardProcessControl$MemoryStandard getStandard ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1338 */
v0 = this.mStandardMap;
/* check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
} // .end method
public Boolean isEmpty ( ) {
/* .locals 1 */
/* .line 1350 */
v0 = v0 = this.mStandardMap;
} // .end method
public void removeMemoryStandard ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1321 */
v0 = this.mStandardMap;
/* check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
/* .line 1322 */
/* .local v0, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
/* if-nez v0, :cond_0 */
/* .line 1323 */
return;
/* .line 1326 */
} // :cond_0
v1 = v1 = this.mNativeProcessCmds;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 1327 */
v1 = this.mNativeProcessCmds;
/* .line 1330 */
} // :cond_1
v1 = this.mStandardMap;
/* .line 1331 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 6 */
/* .line 1364 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1365 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
final String v1 = "Memory Standard:\n"; // const-string v1, "Memory Standard:\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1366 */
v1 = this.mStandardMap;
v2 = } // :goto_0
final String v3 = "\n"; // const-string v3, "\n"
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 1367 */
/* .local v2, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;>;" */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v5, Ljava/lang/String; */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ": "; // const-string v5, ": "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v5, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandard ) v5 ).toString ( ); // invoke-virtual {v5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1368 */
} // .end local v2 # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;>;"
/* .line 1369 */
} // :cond_0
final String v1 = "Native Procs:\n"; // const-string v1, "Native Procs:\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1370 */
v1 = this.mNativeProcessCmds;
/* .line 1371 */
/* .local v1, "nativeProcIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1372 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* check-cast v4, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1374 */
} // :cond_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public void updateMemoryStandard ( Long p0, java.lang.String p1, java.util.List p2, java.lang.String p3 ) {
/* .locals 7 */
/* .param p1, "pss" # J */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p5, "parent" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1307 */
/* .local p4, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* new-instance v6, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
v1 = this.this$0;
/* move-object v0, v6 */
/* move-wide v2, p1 */
/* move-object v4, p4 */
/* move-object v5, p5 */
/* invoke-direct/range {v0 ..v5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;JLjava/util/List;Ljava/lang/String;)V */
/* .line 1308 */
/* .local v0, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
v1 = this.mStandardMap;
/* .line 1309 */
return;
} // .end method
public void updateNativeMemoryStandard ( Long p0, java.lang.String p1, java.util.List p2, java.lang.String p3 ) {
/* .locals 1 */
/* .param p1, "pss" # J */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p5, "parent" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 1313 */
/* .local p4, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-virtual/range {p0 ..p5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V */
/* .line 1315 */
v0 = v0 = this.mNativeProcessCmds;
/* if-nez v0, :cond_0 */
/* .line 1316 */
v0 = this.mNativeProcessCmds;
/* .line 1318 */
} // :cond_0
return;
} // .end method
