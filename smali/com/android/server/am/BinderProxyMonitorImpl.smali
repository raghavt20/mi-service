.class public Lcom/android/server/am/BinderProxyMonitorImpl;
.super Lcom/android/server/am/BinderProxyMonitor;
.source "BinderProxyMonitorImpl.java"


# static fields
.field private static final BINDER_ALLOC_PREFIX:Ljava/lang/String; = "binder_alloc_u"

.field private static final DUMP_DIR:Ljava/lang/String; = "/data/miuilog/stability/resleak/binderproxy"

.field private static final TAG:Ljava/lang/String; = "BinderProxyMonitor"


# instance fields
.field private final mDumping:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mLastBpCount:I

.field private mLastDumpTime:J

.field private volatile mTrackedUid:I


# direct methods
.method public static synthetic $r8$lambda$3l9hYlmIIVtJclL9V8UzyD5FSvc(Lcom/android/server/am/BinderProxyMonitorImpl;Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/BinderProxyMonitorImpl;->lambda$trackProcBinderAllocations$3(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method public static synthetic $r8$lambda$IL2MxU39JQuCN7pQkrldNEmpEoA(Lcom/android/server/am/BinderProxyMonitorImpl;ILcom/android/server/am/ActivityManagerService;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/BinderProxyMonitorImpl;->lambda$trackBinderAllocations$1(ILcom/android/server/am/ActivityManagerService;)V

    return-void
.end method

.method public static synthetic $r8$lambda$JpaoMyO0MTbbPAijfQID3HVQxCQ(Lcom/android/server/am/BinderProxyMonitorImpl;Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/server/am/BinderProxyMonitorImpl;->lambda$trackProcBinderAllocations$2(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 29
    invoke-direct {p0}, Lcom/android/server/am/BinderProxyMonitor;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastBpCount:I

    .line 36
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastDumpTime:J

    .line 37
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mDumping:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private doDumpRemoteBinders(Lcom/android/server/am/ActivityManagerService;ILjava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "targetUid"    # I
    .param p3, "fd"    # Ljava/io/FileDescriptor;
    .param p4, "pw"    # Ljava/io/PrintWriter;

    .line 212
    if-nez p3, :cond_0

    .line 213
    return-void

    .line 215
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ActivityManagerService;->mProcLock:Lcom/android/server/am/ActivityManagerGlobalLock;

    monitor-enter v0

    .line 216
    :try_start_0
    iget-object v1, p1, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    new-instance v2, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda3;

    invoke-direct {v2, p2, p3, p4}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda3;-><init>(ILjava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Lcom/android/server/am/ProcessList;->forEachLruProcessesLOSP(ZLjava/util/function/Consumer;)V

    .line 240
    monitor-exit v0

    .line 241
    return-void

    .line 240
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private dumpBinderProxies(Lcom/android/server/am/ActivityManagerService;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;ZZ)V
    .locals 10
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "pw"    # Ljava/io/PrintWriter;
    .param p4, "persistToFile"    # Z
    .param p5, "force"    # Z

    .line 163
    iget v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    .line 164
    .local v0, "uid":I
    if-gez v0, :cond_0

    .line 165
    const-string v1, "Binder allocation tracker not enabled yet"

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 166
    return-void

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mDumping:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndExchange(ZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    const-string v1, "dumpBinderProxies() is already ongoing..."

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 170
    return-void

    .line 172
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 173
    .local v1, "currentTime":J
    if-nez p5, :cond_2

    iget-wide v4, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastDumpTime:J

    sub-long v4, v1, v4

    const-wide/32 v6, 0xea60

    cmp-long v4, v4, v6

    if-gtz v4, :cond_2

    .line 174
    const-string v4, "dumpBinderProxies() skipped"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 175
    iget-object v4, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mDumping:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 176
    return-void

    .line 178
    :cond_2
    iput-wide v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastDumpTime:J

    .line 179
    const/4 v4, 0x0

    .line 180
    .local v4, "fos":Ljava/io/FileOutputStream;
    if-eqz p4, :cond_3

    const-string v5, "/data/miuilog/stability/resleak/binderproxy"

    invoke-static {v5}, Lmiui/mqsas/scout/ScoutUtils;->ensureDumpDir(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 181
    const-string v6, ""

    const/4 v7, 0x2

    invoke-static {v5, v6, v7}, Lmiui/mqsas/scout/ScoutUtils;->removeHistoricalDumps(Ljava/lang/String;Ljava/lang/String;I)V

    .line 182
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "binder_alloc_u"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    .line 183
    .local v5, "persistFile":Ljava/io/File;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Dumping binder proxies to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 185
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v4, v6

    .line 186
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    move-object p2, v6

    .line 187
    new-instance v6, Lcom/android/internal/util/FastPrintWriter;

    invoke-direct {v6, v4}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p3, v6

    .line 192
    goto :goto_0

    .line 188
    :catch_0
    move-exception v6

    .line 189
    .local v6, "e":Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to open "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 190
    iget-object v7, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mDumping:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v7, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 191
    return-void

    .line 194
    .end local v5    # "persistFile":Ljava/io/File;
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v5

    .line 196
    .local v5, "iden":J
    :try_start_1
    invoke-virtual {p1, p3, v3}, Lcom/android/server/am/ActivityManagerService;->dumpBinderProxies(Ljava/io/PrintWriter;I)V

    .line 198
    const-string v7, "\n\n"

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p3}, Ljava/io/PrintWriter;->flush()V

    .line 200
    invoke-direct {p0, p1, v0, p2, p3}, Lcom/android/server/am/BinderProxyMonitorImpl;->doDumpRemoteBinders(Lcom/android/server/am/ActivityManagerService;ILjava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    :goto_1
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 205
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 206
    iget-object v7, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mDumping:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v7, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 207
    goto :goto_2

    .line 204
    :catchall_0
    move-exception v7

    goto :goto_3

    .line 201
    :catch_1
    move-exception v7

    .line 202
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Dump binder allocations failed"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v7    # "e":Ljava/lang/Exception;
    goto :goto_1

    .line 208
    :goto_2
    return-void

    .line 204
    :goto_3
    invoke-static {v5, v6}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 205
    invoke-static {v4}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 206
    iget-object v8, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mDumping:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v8, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 207
    throw v7
.end method

.method static synthetic lambda$doDumpRemoteBinders$4(ILjava/io/FileDescriptor;Ljava/io/PrintWriter;Lcom/android/server/am/ProcessRecord;)V
    .locals 4
    .param p0, "targetUid"    # I
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 218
    :try_start_0
    invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v0

    .line 219
    .local v0, "thread":Landroid/app/IApplicationThread;
    if-nez v0, :cond_0

    .line 220
    return-void

    .line 222
    :cond_0
    iget v1, p3, Lcom/android/server/am/ProcessRecord;->uid:I

    if-eq v1, p0, :cond_1

    .line 223
    return-void

    .line 225
    :cond_1
    new-instance v1, Lcom/android/internal/os/TransferPipe;

    invoke-direct {v1}, Lcom/android/internal/os/TransferPipe;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    .local v1, "tp":Lcom/android/internal/os/TransferPipe;
    :try_start_1
    invoke-virtual {p3}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/internal/os/TransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/app/IApplicationThread;->dumpBinderAllocations(Landroid/os/ParcelFileDescriptor;)V

    .line 228
    const-wide/16 v2, 0x2710

    invoke-virtual {v1, p1, v2, v3}, Lcom/android/internal/os/TransferPipe;->go(Ljava/io/FileDescriptor;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    :try_start_2
    invoke-virtual {v1}, Lcom/android/internal/os/TransferPipe;->kill()V

    .line 231
    nop

    .line 238
    .end local v0    # "thread":Landroid/app/IApplicationThread;
    .end local v1    # "tp":Lcom/android/internal/os/TransferPipe;
    goto :goto_0

    .line 230
    .restart local v0    # "thread":Landroid/app/IApplicationThread;
    .restart local v1    # "tp":Lcom/android/internal/os/TransferPipe;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/android/internal/os/TransferPipe;->kill()V

    .line 231
    nop

    .end local p0    # "targetUid":I
    .end local p1    # "fd":Ljava/io/FileDescriptor;
    .end local p2    # "pw":Ljava/io/PrintWriter;
    .end local p3    # "proc":Lcom/android/server/am/ProcessRecord;
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 232
    .end local v0    # "thread":Landroid/app/IApplicationThread;
    .end local v1    # "tp":Lcom/android/internal/os/TransferPipe;
    .restart local p0    # "targetUid":I
    .restart local p1    # "fd":Ljava/io/FileDescriptor;
    .restart local p2    # "pw":Ljava/io/PrintWriter;
    .restart local p3    # "proc":Lcom/android/server/am/ProcessRecord;
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/Exception;
    if-eqz p2, :cond_2

    .line 234
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failure while dumping binder traces from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".  Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 236
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    .line 239
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic lambda$trackBinderAllocations$0(IILcom/android/server/am/ProcessRecord;)V
    .locals 6
    .param p0, "prevTrackedUid"    # I
    .param p1, "targetUid"    # I
    .param p2, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 70
    const-string v0, ".  Exception: "

    const-string v1, "Failed to enable binder allocation tracking in "

    const-string v2, "BinderProxyMonitor"

    :try_start_0
    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v3

    .line 71
    .local v3, "thread":Landroid/app/IApplicationThread;
    if-nez v3, :cond_0

    .line 72
    return-void

    .line 74
    :cond_0
    const/4 v4, -0x1

    if-eq p0, v4, :cond_1

    iget v5, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    if-ne v5, p0, :cond_1

    .line 75
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Disable binder tracker on process "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/app/IApplicationThread;->trackBinderAllocations(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 77
    return-void

    .line 83
    .end local v3    # "thread":Landroid/app/IApplicationThread;
    :cond_1
    nop

    .line 85
    if-eq p1, v4, :cond_2

    :try_start_1
    iget v3, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    if-ne v3, p1, :cond_2

    .line 86
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Enable binder tracker on process "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/app/IApplicationThread;->trackBinderAllocations(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v3

    .line 90
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 92
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_0
    nop

    .line 93
    :goto_1
    return-void

    .line 79
    :catch_1
    move-exception v3

    .line 80
    .restart local v3    # "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    return-void
.end method

.method private synthetic lambda$trackBinderAllocations$1(ILcom/android/server/am/ActivityManagerService;)V
    .locals 8
    .param p1, "targetUid"    # I
    .param p2, "ams"    # Lcom/android/server/am/ActivityManagerService;

    .line 45
    invoke-static {}, Landroid/os/BinderProxy;->getProxyCount()I

    move-result v0

    .line 46
    .local v0, "currBpCount":I
    iget v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    if-ne p1, v1, :cond_2

    .line 47
    if-lez p1, :cond_1

    iget v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastBpCount:I

    sub-int v1, v0, v1

    .line 48
    const/16 v2, 0x3e8

    if-ne p1, v2, :cond_0

    const/16 v2, 0x1770

    goto :goto_0

    :cond_0
    const/16 v2, 0xbb8

    :goto_0
    if-lt v1, v2, :cond_1

    .line 49
    iput v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastBpCount:I

    .line 50
    const-string v1, "BinderProxyMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Number of binder proxies sent from uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reached "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", dump automatically"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    new-instance v1, Landroid/util/LogPrinter;

    const-string v2, "BinderProxyMonitor"

    const/4 v3, 0x3

    const/4 v4, 0x4

    invoke-direct {v1, v4, v2, v3}, Landroid/util/LogPrinter;-><init>(ILjava/lang/String;I)V

    .line 53
    .local v1, "printer":Landroid/util/LogPrinter;
    :try_start_0
    new-instance v5, Lcom/android/internal/util/FastPrintWriter;

    invoke-direct {v5, v1}, Lcom/android/internal/util/FastPrintWriter;-><init>(Landroid/util/Printer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .local v5, "pw":Lcom/android/internal/util/FastPrintWriter;
    :try_start_1
    sget-object v4, Ljava/io/FileDescriptor;->out:Ljava/io/FileDescriptor;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p2

    invoke-direct/range {v2 .. v7}, Lcom/android/server/am/BinderProxyMonitorImpl;->dumpBinderProxies(Lcom/android/server/am/ActivityManagerService;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;ZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    :try_start_2
    invoke-virtual {v5}, Lcom/android/internal/util/FastPrintWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 57
    .end local v5    # "pw":Lcom/android/internal/util/FastPrintWriter;
    goto :goto_2

    .line 53
    .restart local v5    # "pw":Lcom/android/internal/util/FastPrintWriter;
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v5}, Lcom/android/internal/util/FastPrintWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "currBpCount":I
    .end local v1    # "printer":Landroid/util/LogPrinter;
    .end local p0    # "this":Lcom/android/server/am/BinderProxyMonitorImpl;
    .end local p1    # "targetUid":I
    .end local p2    # "ams":Lcom/android/server/am/ActivityManagerService;
    :goto_1
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 55
    .end local v5    # "pw":Lcom/android/internal/util/FastPrintWriter;
    .restart local v0    # "currBpCount":I
    .restart local v1    # "printer":Landroid/util/LogPrinter;
    .restart local p0    # "this":Lcom/android/server/am/BinderProxyMonitorImpl;
    .restart local p1    # "targetUid":I
    .restart local p2    # "ams":Lcom/android/server/am/ActivityManagerService;
    :catch_0
    move-exception v2

    .line 56
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "BinderProxyMonitor"

    const-string v4, "Failed to dump binder allocation records"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 59
    .end local v1    # "printer":Landroid/util/LogPrinter;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-void

    .line 61
    :cond_2
    const/4 v1, -0x1

    if-eq p1, v1, :cond_3

    .line 62
    const-string v1, "BinderProxyMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enable binder tracker on uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :cond_3
    iput v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mLastBpCount:I

    .line 65
    iget v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    .line 66
    .local v1, "prevTrackedUid":I
    iput p1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    .line 67
    iget-object v2, p2, Lcom/android/server/am/ActivityManagerService;->mProcLock:Lcom/android/server/am/ActivityManagerGlobalLock;

    monitor-enter v2

    .line 68
    :try_start_5
    iget-object v3, p2, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    new-instance v4, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda0;

    invoke-direct {v4, v1, p1}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda0;-><init>(II)V

    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/android/server/am/ProcessList;->forEachLruProcessesLOSP(ZLjava/util/function/Consumer;)V

    .line 94
    monitor-exit v2

    .line 95
    return-void

    .line 94
    :catchall_2
    move-exception v3

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v3
.end method

.method private synthetic lambda$trackProcBinderAllocations$2(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ProcessRecord;)V
    .locals 3
    .param p1, "targetProc"    # Lcom/android/server/am/ProcessRecord;
    .param p2, "proc"    # Lcom/android/server/am/ProcessRecord;

    .line 111
    if-eq p2, p1, :cond_0

    .line 112
    return-void

    .line 116
    :cond_0
    :try_start_0
    iget v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v0, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    iget v1, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    if-ne v0, v1, :cond_1

    .line 117
    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/app/IApplicationThread;->trackBinderAllocations(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :cond_1
    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to enable binder allocation tracking in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".  Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BinderProxyMonitor"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private synthetic lambda$trackProcBinderAllocations$3(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;)V
    .locals 4
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "targetProc"    # Lcom/android/server/am/ProcessRecord;

    .line 109
    iget-object v0, p1, Lcom/android/server/am/ActivityManagerService;->mProcLock:Lcom/android/server/am/ActivityManagerGlobalLock;

    monitor-enter v0

    .line 110
    :try_start_0
    iget-object v1, p1, Lcom/android/server/am/ActivityManagerService;->mProcessList:Lcom/android/server/am/ProcessList;

    new-instance v2, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, p2}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/am/BinderProxyMonitorImpl;Lcom/android/server/am/ProcessRecord;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Lcom/android/server/am/ProcessList;->forEachLruProcessesLOSP(ZLjava/util/function/Consumer;)V

    .line 124
    monitor-exit v0

    .line 125
    return-void

    .line 124
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public handleDumpBinderProxies(Lcom/android/server/am/ActivityManagerService;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;I)Z
    .locals 14
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "pw"    # Ljava/io/PrintWriter;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "opti"    # I

    .line 131
    move-object/from16 v0, p4

    invoke-static {}, Landroid/os/BinderStub;->get()Landroid/os/BinderStub;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/BinderStub;->isEnabled()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 132
    return v2

    .line 134
    :cond_0
    const/4 v1, 0x0

    .line 135
    .local v1, "dumpDetailToFile":Z
    const/4 v3, 0x0

    .line 136
    .local v3, "dumpDetail":Z
    move/from16 v4, p5

    .local v4, "i":I
    :goto_0
    array-length v5, v0

    const/4 v11, 0x1

    if-ge v4, v5, :cond_5

    .line 137
    const-string v5, "--dump-details-to-file"

    aget-object v6, v0, p5

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 138
    const/4 v1, 0x1

    .line 139
    move-object v12, p0

    move-object v13, p1

    goto :goto_1

    .line 141
    :cond_1
    const-string v5, "--dump-details"

    aget-object v6, v0, p5

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 142
    const/4 v3, 0x1

    .line 143
    move-object v12, p0

    move-object v13, p1

    goto :goto_1

    .line 145
    :cond_2
    const-string v5, "--track-uid"

    aget-object v6, v0, p5

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 146
    add-int/lit8 v2, p5, 0x1

    array-length v5, v0

    if-ge v2, v5, :cond_3

    .line 149
    add-int/lit8 v2, p5, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 150
    .local v2, "uid":I
    move-object v12, p0

    move-object v13, p1

    invoke-virtual {p0, p1, v2}, Lcom/android/server/am/BinderProxyMonitorImpl;->trackBinderAllocations(Lcom/android/server/am/ActivityManagerService;I)V

    .line 151
    return v11

    .line 147
    .end local v2    # "uid":I
    :cond_3
    move-object v12, p0

    move-object v13, p1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v5, "--trackUid should be followed by a uid"

    invoke-direct {v2, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 145
    :cond_4
    move-object v12, p0

    move-object v13, p1

    .line 136
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    move-object v12, p0

    move-object v13, p1

    .line 154
    .end local v4    # "i":I
    if-nez v3, :cond_7

    if-eqz v1, :cond_6

    goto :goto_2

    .line 158
    :cond_6
    return v2

    .line 155
    :cond_7
    :goto_2
    const/4 v10, 0x1

    move-object v5, p0

    move-object v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move v9, v1

    invoke-direct/range {v5 .. v10}, Lcom/android/server/am/BinderProxyMonitorImpl;->dumpBinderProxies(Lcom/android/server/am/ActivityManagerService;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;ZZ)V

    .line 156
    return v11
.end method

.method public trackBinderAllocations(Lcom/android/server/am/ActivityManagerService;I)V
    .locals 2
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "targetUid"    # I

    .line 41
    invoke-static {}, Landroid/os/BinderStub;->get()Landroid/os/BinderStub;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/BinderStub;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    return-void

    .line 44
    :cond_0
    iget-object v0, p1, Lcom/android/server/am/ActivityManagerService;->mHandler:Lcom/android/server/am/ActivityManagerService$MainHandler;

    new-instance v1, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p2, p1}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/am/BinderProxyMonitorImpl;ILcom/android/server/am/ActivityManagerService;)V

    invoke-virtual {v0, v1}, Lcom/android/server/am/ActivityManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z

    .line 96
    return-void
.end method

.method public trackProcBinderAllocations(Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;)V
    .locals 3
    .param p1, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "targetProc"    # Lcom/android/server/am/ProcessRecord;

    .line 100
    invoke-static {}, Landroid/os/BinderStub;->get()Landroid/os/BinderStub;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/BinderStub;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    return-void

    .line 103
    :cond_0
    iget v0, p0, Lcom/android/server/am/BinderProxyMonitorImpl;->mTrackedUid:I

    .line 104
    .local v0, "currentTrackedUid":I
    if-ltz v0, :cond_2

    iget v1, p2, Lcom/android/server/am/ProcessRecord;->uid:I

    if-eq v1, v0, :cond_1

    goto :goto_0

    .line 107
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enable binder tracker on new process "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/server/am/ProcessRecord;->getPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BinderProxyMonitor"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v1, p1, Lcom/android/server/am/ActivityManagerService;->mHandler:Lcom/android/server/am/ActivityManagerService$MainHandler;

    new-instance v2, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/server/am/BinderProxyMonitorImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/server/am/BinderProxyMonitorImpl;Lcom/android/server/am/ActivityManagerService;Lcom/android/server/am/ProcessRecord;)V

    invoke-virtual {v1, v2}, Lcom/android/server/am/ActivityManagerService$MainHandler;->post(Ljava/lang/Runnable;)Z

    .line 126
    return-void

    .line 105
    :cond_2
    :goto_0
    return-void
.end method
