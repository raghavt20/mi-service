.class Lcom/android/server/am/MemoryControlCloud$3;
.super Landroid/database/ContentObserver;
.source "MemoryControlCloud.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/am/MemoryControlCloud;->registerCloudEnableObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/am/MemoryControlCloud;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/server/am/MemoryControlCloud;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/am/MemoryControlCloud;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 79
    iput-object p1, p0, Lcom/android/server/am/MemoryControlCloud$3;->this$0:Lcom/android/server/am/MemoryControlCloud;

    iput-object p3, p0, Lcom/android/server/am/MemoryControlCloud$3;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 82
    const-string v0, "MemoryStandardProcessControl"

    if-eqz p2, :cond_0

    .line 83
    const-string v1, "cloud_memory_standard_appheap_enable"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/MemoryControlCloud$3;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v2}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/MemoryControlCloud$3;->val$context:Landroid/content/Context;

    .line 86
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, -0x2

    invoke-static {v3, v1, v4}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    goto :goto_0

    .line 88
    :catch_0
    move-exception v1

    .line 89
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cloud mAppHeapEnable check failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v2, p0, Lcom/android/server/am/MemoryControlCloud$3;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v2}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v2

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z

    .line 92
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cloud app heap control received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/MemoryControlCloud$3;->this$0:Lcom/android/server/am/MemoryControlCloud;

    invoke-static {v2}, Lcom/android/server/am/MemoryControlCloud;->-$$Nest$fgetmspc(Lcom/android/server/am/MemoryControlCloud;)Lcom/android/server/am/MemoryStandardProcessControl;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    return-void
.end method
