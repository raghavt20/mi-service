public class com.android.server.am.MiuiProcessStubImpl implements com.android.server.am.MiuiProcessStub {
	 /* .source "MiuiProcessStubImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.am.MiuiProcessStubImpl ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Long getLaunchRtSchedDurationMs ( ) {
		 /* .locals 2 */
		 /* .line 15 */
		 /* const-wide/16 v0, 0x1f4 */
		 /* return-wide v0 */
	 } // .end method
	 public Long getLiteAnimRtSchedDurationMs ( ) {
		 /* .locals 2 */
		 /* .line 31 */
		 /* const-wide/16 v0, 0x3e8 */
		 /* return-wide v0 */
	 } // .end method
	 public Integer getSchedModeAnimatorRt ( ) {
		 /* .locals 1 */
		 /* .line 11 */
		 int v0 = 1; // const/4 v0, 0x1
	 } // .end method
	 public Integer getSchedModeBindBigCore ( ) {
		 /* .locals 1 */
		 /* .line 46 */
		 int v0 = 7; // const/4 v0, 0x7
	 } // .end method
	 public Integer getSchedModeHomeAnimation ( ) {
		 /* .locals 1 */
		 /* .line 39 */
		 int v0 = 4; // const/4 v0, 0x4
	 } // .end method
	 public Integer getSchedModeNormal ( ) {
		 /* .locals 1 */
		 /* .line 19 */
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 public Integer getSchedModeTouchRt ( ) {
		 /* .locals 1 */
		 /* .line 35 */
		 int v0 = 2; // const/4 v0, 0x2
	 } // .end method
	 public Integer getSchedModeUnlock ( ) {
		 /* .locals 1 */
		 /* .line 42 */
		 int v0 = 5; // const/4 v0, 0x5
	 } // .end method
	 public Long getScrollRtSchedDurationMs ( ) {
		 /* .locals 2 */
		 /* .line 23 */
		 /* const-wide/16 v0, 0x1388 */
		 /* return-wide v0 */
	 } // .end method
	 public Long getTouchRtSchedDurationMs ( ) {
		 /* .locals 2 */
		 /* .line 27 */
		 /* const-wide/16 v0, 0x7d0 */
		 /* return-wide v0 */
	 } // .end method
