class com.android.server.am.GameProcessKiller$PackageMemInfo {
	 /* .source "GameProcessKiller.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/GameProcessKiller; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "PackageMemInfo" */
} // .end annotation
/* # instance fields */
public Long mMemSize;
public Integer mState;
public Integer mUid;
/* # direct methods */
public com.android.server.am.GameProcessKiller$PackageMemInfo ( ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "pss" # J */
/* .param p4, "state" # I */
/* .line 130 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 131 */
/* iput p1, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mUid:I */
/* .line 132 */
/* iput-wide p2, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mMemSize:J */
/* .line 133 */
/* iput p4, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mState:I */
/* .line 134 */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 142 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 143 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* const-string/jumbo v1, "uid=" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 144 */
/* iget v1, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 145 */
final String v1 = ", size="; // const-string v1, ", size="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 146 */
/* iget-wide v1, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mMemSize:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 147 */
final String v1 = ", state="; // const-string v1, ", state="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 148 */
/* iget v1, p0, Lcom/android/server/am/GameProcessKiller$PackageMemInfo;->mState:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 149 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
