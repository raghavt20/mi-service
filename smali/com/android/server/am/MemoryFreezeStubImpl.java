public class com.android.server.am.MemoryFreezeStubImpl implements com.android.server.am.MemoryFreezeStub {
	 /* .source "MemoryFreezeStubImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;, */
	 /* Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;, */
	 /* Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;, */
	 /* Lcom/android/server/am/MemoryFreezeStubImpl$MemFreezeShellCmd; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLOUD_MEMFREEZE_ENABLE;
private static Boolean DEBUG;
private static final Long DEFAULT_FREEZER_DEBOUNCE_TIMEOUT;
private static final java.lang.String EXTM_ENABLE_PROP;
private static final Integer FLUSH_STAT_INDEX;
private static final java.lang.String FLUSH_STAT_PATH;
private static final java.lang.String MEMFREEZE_DEBUG_PROP;
private static final java.lang.String MEMFREEZE_ENABLE_PROP;
private static final java.lang.String MEMFREEZE_POLICY_PROP;
private static final Integer MSG_CHECK_KILL;
private static final Integer MSG_CHECK_RECLAIM;
private static final Integer MSG_CHECK_UNUSED;
private static final Integer MSG_CHECK_WRITE_BACK;
private static final Integer MSG_REGISTER_CLOUD_OBSERVER;
private static final Integer MSG_REGISTER_MIUIFREEZE_OBSERVER;
private static final Integer MSG_REGISTER_WHITE_LIST_OBSERVER;
private static final java.lang.String PROCESS_FREEZE_LIST;
private static final java.lang.String TAG;
private static final java.lang.String WHITELIST_CLOUD_PATH;
private static final java.lang.String WHITELIST_DEFAULT_PATH;
private static java.util.List sBlackListApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Long sThreshholdKill;
private static Long sThreshholdReclaim;
private static Long sThreshholdWriteback;
private static java.util.List sWhiteListApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private volatile Boolean extmEnable;
private Boolean isInit;
private com.android.server.am.ActivityManagerService mAMS;
private com.android.server.wm.ActivityTaskManagerService mATMS;
private com.android.server.am.MemoryFreezeStubImpl$BinderService mBinderService;
private android.content.Context mContext;
private volatile Boolean mEnable;
private volatile Long mFreezerDebounceTimeout;
public com.android.server.am.MemoryFreezeStubImpl$MyHandler mHandler;
private android.os.HandlerThread mHandlerThread;
private Integer mKillingUID;
private java.lang.String mLastStartPackage;
private Long mLastStartPackageTime;
private com.android.server.am.MemoryFreezeCloud mMemoryFreezeCloud;
private com.android.server.am.ProcessManagerService mPMS;
private java.util.List mProcessFrozenUid;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.HashMap mRunningPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mUnusedCheckTime;
private Long mUnusedKillTime;
private com.android.server.wm.WindowManagerInternal mWMInternal;
private android.os.storage.IStorageManager storageManager;
/* # direct methods */
public static void $r8$lambda$XB3BsXcRhHSs1Ucjkb71JqOB1H0 ( com.android.server.am.MemoryFreezeStubImpl p0, Long p1, java.util.List p2, java.lang.Integer p3, com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct/range {p0 ..p5}, Lcom/android/server/am/MemoryFreezeStubImpl;->lambda$checkUnused$0(JLjava/util/List;Ljava/lang/Integer;Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;)V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.android.server.am.MemoryFreezeStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static com.android.server.am.MemoryFreezeCloud -$$Nest$fgetmMemoryFreezeCloud ( com.android.server.am.MemoryFreezeStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMemoryFreezeCloud;
} // .end method
static void -$$Nest$fputmEnable ( com.android.server.am.MemoryFreezeStubImpl p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mEnable:Z */
return;
} // .end method
static void -$$Nest$mcheckAndKill ( com.android.server.am.MemoryFreezeStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->checkAndKill()V */
return;
} // .end method
static void -$$Nest$mcheckAndReclaim ( com.android.server.am.MemoryFreezeStubImpl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl;->checkAndReclaim(I)V */
return;
} // .end method
static void -$$Nest$mcheckAndWriteBack ( com.android.server.am.MemoryFreezeStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->checkAndWriteBack()V */
return;
} // .end method
static void -$$Nest$mcheckUnused ( com.android.server.am.MemoryFreezeStubImpl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->checkUnused()V */
return;
} // .end method
static void -$$Nest$mdump ( com.android.server.am.MemoryFreezeStubImpl p0, java.io.PrintWriter p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryFreezeStubImpl;->dump(Ljava/io/PrintWriter;)V */
return;
} // .end method
static com.android.server.am.MemoryFreezeStubImpl ( ) {
/* .locals 2 */
/* .line 84 */
/* const-wide/32 v0, 0x200000 */
/* sput-wide v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J */
/* .line 85 */
/* const-wide/32 v0, 0x300000 */
/* sput-wide v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J */
/* .line 86 */
/* const-wide/32 v0, 0x400000 */
/* sput-wide v0, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J */
/* .line 88 */
final String v0 = "persist.miui.extm.debug_enable"; // const-string v0, "persist.miui.extm.debug_enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.MemoryFreezeStubImpl.DEBUG = (v0!= 0);
/* .line 89 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 90 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
return;
} // .end method
public com.android.server.am.MemoryFreezeStubImpl ( ) {
/* .locals 4 */
/* .line 63 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 92 */
final String v0 = "persist.miui.extm.enable"; // const-string v0, "persist.miui.extm.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
} // :cond_0
/* move v2, v1 */
} // :goto_0
/* iput-boolean v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z */
/* .line 93 */
final String v0 = "persist.sys.mfz.enable"; // const-string v0, "persist.sys.mfz.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mEnable:Z */
/* .line 94 */
/* const-wide/32 v2, 0x1d4c0 */
/* iput-wide v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mFreezerDebounceTimeout:J */
/* .line 95 */
int v0 = 0; // const/4 v0, 0x0
this.mAMS = v0;
/* .line 96 */
this.mWMInternal = v0;
/* .line 97 */
this.mATMS = v0;
/* .line 98 */
this.mPMS = v0;
/* .line 99 */
this.storageManager = v0;
/* .line 100 */
this.mBinderService = v0;
/* .line 101 */
/* iput-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->isInit:Z */
/* .line 102 */
this.mContext = v0;
/* .line 103 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.mRunningPackages = v1;
/* .line 104 */
/* new-instance v1, Landroid/os/HandlerThread; */
final String v2 = "MFZ"; // const-string v2, "MFZ"
/* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v1;
/* .line 105 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mProcessFrozenUid = v1;
/* .line 106 */
this.mHandler = v0;
/* .line 107 */
this.mMemoryFreezeCloud = v0;
/* .line 108 */
this.mLastStartPackage = v0;
/* .line 109 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackageTime:J */
/* .line 110 */
/* const-wide/32 v0, 0x240c8400 */
/* iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedCheckTime:J */
/* .line 111 */
/* const-wide v0, 0x90321000L */
/* iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedKillTime:J */
/* .line 112 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mKillingUID:I */
return;
} // .end method
private void checkAndKill ( ) {
/* .locals 11 */
/* .line 485 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->countBgMem()J */
/* move-result-wide v0 */
/* sget-wide v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
/* .line 486 */
final String v0 = "MFZ"; // const-string v0, "MFZ"
final String v1 = "don\'t need to kill."; // const-string v1, "don\'t need to kill."
android.util.Slog .i ( v0,v1 );
/* .line 487 */
return;
/* .line 490 */
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
/* .line 491 */
/* .local v0, "needKillUid":I */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* iget-wide v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mFreezerDebounceTimeout:J */
/* sub-long/2addr v1, v3 */
/* .line 492 */
/* .local v1, "needKillLastStopTime":J */
v3 = this.mRunningPackages;
/* monitor-enter v3 */
/* .line 493 */
try { // :try_start_0
v4 = this.mRunningPackages;
(( java.util.HashMap ) v4 ).keySet ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* .line 494 */
/* .local v5, "uid":I */
v6 = this.mRunningPackages;
java.lang.Integer .valueOf ( v5 );
(( java.util.HashMap ) v6 ).get ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* .line 495 */
/* .local v6, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmlastActivityStopTime ( v6 );
/* move-result-wide v7 */
/* const-wide/16 v9, 0x0 */
/* cmp-long v7, v7, v9 */
/* if-lez v7, :cond_1 */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmlastActivityStopTime ( v6 );
/* move-result-wide v7 */
/* cmp-long v7, v7, v1 */
/* if-gez v7, :cond_1 */
/* .line 497 */
/* move v0, v5 */
/* .line 498 */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmlastActivityStopTime ( v6 );
/* move-result-wide v7 */
/* move-wide v1, v7 */
/* .line 500 */
} // .end local v5 # "uid":I
} // .end local v6 # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
} // :cond_1
/* .line 501 */
} // :cond_2
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 502 */
/* if-lez v0, :cond_5 */
/* .line 503 */
v3 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v3 ).getProcessRecordByUid ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Lcom/android/server/am/ProcessRecord; */
/* .line 504 */
/* .local v4, "app":Lcom/android/server/am/ProcessRecord; */
if ( v4 != null) { // if-eqz v4, :cond_3
v5 = this.mState;
v5 = (( com.android.server.am.ProcessStateRecord ) v5 ).getCurAdj ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* const/16 v6, 0x2bc */
/* if-lt v5, v6, :cond_3 */
/* .line 505 */
v5 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v5 ).getProcessKiller ( ); // invoke-virtual {v5}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;
final String v6 = "mfz-overmem"; // const-string v6, "mfz-overmem"
int v7 = 0; // const/4 v7, 0x0
(( com.android.server.am.ProcessKiller ) v5 ).killApplication ( v4, v6, v7 ); // invoke-virtual {v5, v4, v6, v7}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z
/* .line 506 */
final String v5 = "MFZ"; // const-string v5, "MFZ"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Killing "; // const-string v7, "Killing "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.processName;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = "("; // const-string v7, "("
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ") since overmem."; // const-string v7, ") since overmem."
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 508 */
} // .end local v4 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_3
/* .line 509 */
} // :cond_4
/* iput v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mKillingUID:I */
/* .line 515 */
return;
/* .line 511 */
} // :cond_5
int v3 = -1; // const/4 v3, -0x1
/* iput v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mKillingUID:I */
/* .line 512 */
final String v3 = "MFZ"; // const-string v3, "MFZ"
final String v4 = "Stop checking and killing as no apps to kill."; // const-string v4, "Stop checking and killing as no apps to kill."
android.util.Slog .w ( v3,v4 );
/* .line 513 */
return;
/* .line 501 */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v4 */
} // .end method
private void checkAndReclaim ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .line 395 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->countBgMem()J */
/* move-result-wide v0 */
/* sget-wide v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
/* .line 396 */
final String v0 = "MFZ"; // const-string v0, "MFZ"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "don\'t need to PR "; // const-string v2, "don\'t need to PR "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 397 */
return;
/* .line 400 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
/* .line 401 */
/* .local v0, "pkgName":Ljava/lang/String; */
v1 = this.mRunningPackages;
/* monitor-enter v1 */
/* .line 402 */
try { // :try_start_0
v2 = this.mRunningPackages;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* .line 403 */
/* .local v2, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* if-nez v2, :cond_1 */
/* .line 404 */
/* monitor-exit v1 */
return;
/* .line 406 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fputmPendingFreeze ( v2,v3 );
/* .line 407 */
int v3 = 1; // const/4 v3, 0x1
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fputmFrozen ( v2,v3 );
/* .line 408 */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmPackageName ( v2 );
/* move-object v0, v4 */
/* .line 409 */
} // .end local v2 # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 410 */
v1 = this.mWMInternal;
(( com.android.server.wm.WindowManagerInternal ) v1 ).getVisibleWindowOwner ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerInternal;->getVisibleWindowOwner()Ljava/util/List;
/* .line 411 */
/* .local v2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v1 = java.lang.Integer .valueOf ( p1 );
int v4 = -1; // const/4 v4, -0x1
/* if-eq v1, v4, :cond_2 */
/* .line 412 */
final String v1 = "MFZ"; // const-string v1, "MFZ"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "don\'t PR "; // const-string v4, "don\'t PR "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " as visible."; // const-string v4, " as visible."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v3 );
/* .line 413 */
return;
/* .line 416 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
/* .line 417 */
/* .local v1, "app":Lcom/android/server/am/ProcessRecord; */
v4 = this.mAMS;
/* monitor-enter v4 */
/* .line 418 */
try { // :try_start_1
v5 = this.mAMS;
(( com.android.server.am.ActivityManagerService ) v5 ).getProcessRecordLocked ( v0, p1 ); // invoke-virtual {v5, v0, p1}, Lcom/android/server/am/ActivityManagerService;->getProcessRecordLocked(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
/* move-object v1, v5 */
/* .line 419 */
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 420 */
/* if-nez v1, :cond_3 */
/* .line 421 */
final String v3 = "MFZ"; // const-string v3, "MFZ"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "didn\'t find process record of "; // const-string v5, "didn\'t find process record of "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "("; // const-string v5, "("
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ")"; // const-string v5, ")"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 422 */
return;
/* .line 424 */
} // :cond_3
final String v4 = "MFZ"; // const-string v4, "MFZ"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "PR "; // const-string v6, "PR "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = ",uid:"; // const-string v6, ",uid:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ",pid:"; // const-string v6, ",pid:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v1, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 425 */
v4 = this.mAMS;
v4 = this.mOomAdjuster;
v4 = this.mCachedAppOptimizer;
/* new-instance v5, Lcom/android/server/am/MemoryFreezeStubImpl$3; */
/* invoke-direct {v5, p0}, Lcom/android/server/am/MemoryFreezeStubImpl$3;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;)V */
(( com.android.server.am.CachedAppOptimizer ) v4 ).compactMemoryFreezeApp ( v1, v3, v5 ); // invoke-virtual {v4, v1, v3, v5}, Lcom/android/server/am/CachedAppOptimizer;->compactMemoryFreezeApp(Lcom/android/server/am/ProcessRecord;ZLjava/lang/Runnable;)V
/* .line 432 */
return;
/* .line 419 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_2
/* monitor-exit v4 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v3 */
/* .line 409 */
} // .end local v1 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v2 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* :catchall_1 */
/* move-exception v2 */
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v2 */
} // .end method
private void checkAndWriteBack ( ) {
/* .locals 4 */
/* .line 448 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->countBgMem()J */
/* move-result-wide v0 */
/* sget-wide v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J */
/* cmp-long v0, v0, v2 */
final String v1 = "MFZ"; // const-string v1, "MFZ"
/* if-gez v0, :cond_0 */
/* .line 449 */
final String v0 = "don\'t need to WB."; // const-string v0, "don\'t need to WB."
android.util.Slog .i ( v1,v0 );
/* .line 450 */
return;
/* .line 452 */
} // :cond_0
v0 = this.storageManager;
/* if-nez v0, :cond_1 */
/* .line 453 */
return;
/* .line 456 */
} // :cond_1
try { // :try_start_0
final String v0 = "WB in progress"; // const-string v0, "WB in progress"
android.util.Slog .d ( v1,v0 );
/* .line 457 */
v0 = this.storageManager;
/* new-instance v2, Lcom/android/server/am/MemoryFreezeStubImpl$4; */
/* invoke-direct {v2, p0}, Lcom/android/server/am/MemoryFreezeStubImpl$4;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;)V */
int v3 = 2; // const/4 v3, 0x2
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 472 */
/* .line 469 */
/* :catch_0 */
/* move-exception v0 */
/* .line 470 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v2 = "WB error"; // const-string v2, "WB error"
android.util.Slog .e ( v1,v2 );
/* .line 471 */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 473 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void checkUnused ( ) {
/* .locals 10 */
/* .line 372 */
v0 = this.mPMS;
/* if-nez v0, :cond_0 */
return;
/* .line 373 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 374 */
/* .local v0, "now":J */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 375 */
/* .local v2, "killList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v3 = this.mRunningPackages;
/* monitor-enter v3 */
/* .line 376 */
try { // :try_start_0
v4 = this.mRunningPackages;
/* new-instance v5, Lcom/android/server/am/MemoryFreezeStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v5, p0, v0, v1, v2}, Lcom/android/server/am/MemoryFreezeStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;JLjava/util/List;)V */
(( java.util.HashMap ) v4 ).forEach ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/HashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 382 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 383 */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 384 */
/* .local v4, "uid":I */
v5 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v5 ).getProcessRecordByUid ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/am/ProcessManagerService;->getProcessRecordByUid(I)Ljava/util/List;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_2
/* check-cast v6, Lcom/android/server/am/ProcessRecord; */
/* .line 385 */
/* .local v6, "app":Lcom/android/server/am/ProcessRecord; */
if ( v6 != null) { // if-eqz v6, :cond_1
v7 = this.mState;
v7 = (( com.android.server.am.ProcessStateRecord ) v7 ).getCurAdj ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* const/16 v8, 0x2bc */
/* if-lt v7, v8, :cond_1 */
/* .line 386 */
v7 = this.mPMS;
(( com.android.server.am.ProcessManagerService ) v7 ).getProcessKiller ( ); // invoke-virtual {v7}, Lcom/android/server/am/ProcessManagerService;->getProcessKiller()Lcom/android/server/am/ProcessKiller;
final String v8 = "mfz-overtime"; // const-string v8, "mfz-overtime"
int v9 = 0; // const/4 v9, 0x0
(( com.android.server.am.ProcessKiller ) v7 ).killApplication ( v6, v8, v9 ); // invoke-virtual {v7, v6, v8, v9}, Lcom/android/server/am/ProcessKiller;->killApplication(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;Z)Z
/* .line 387 */
final String v7 = "MFZ"; // const-string v7, "MFZ"
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "kill "; // const-string v9, "kill "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v9 = this.processName;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = "("; // const-string v9, "("
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, v6, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = ") since overtime."; // const-string v9, ") since overtime."
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v8 );
/* .line 389 */
} // .end local v6 # "app":Lcom/android/server/am/ProcessRecord;
} // :cond_1
/* .line 390 */
} // .end local v4 # "uid":I
} // :cond_2
/* .line 391 */
} // :cond_3
v3 = this.mHandler;
int v4 = 3; // const/4 v4, 0x3
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v3 ).obtainMessage ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* iget-wide v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedCheckTime:J */
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v3 ).sendMessageDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 392 */
return;
/* .line 382 */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v4 */
} // .end method
private Long countBgFlush ( ) {
/* .locals 10 */
/* .line 540 */
/* const-wide/16 v0, 0x0 */
/* .line 541 */
/* .local v0, "flushCount":J */
/* new-instance v2, Ljava/io/File; */
final String v3 = "/sys/block/zram0/mfz_compr_data_size"; // const-string v3, "/sys/block/zram0/mfz_compr_data_size"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 542 */
/* .local v2, "file":Ljava/io/File; */
int v3 = 0; // const/4 v3, 0x0
/* .line 544 */
/* .local v3, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v4, Ljava/io/BufferedReader; */
/* new-instance v5, Ljava/io/FileReader; */
/* invoke-direct {v5, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v3, v4 */
/* .line 545 */
(( java.io.BufferedReader ) v3 ).readLine ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* .line 546 */
/* .local v4, "line":Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 547 */
(( java.lang.String ) v4 ).trim ( ); // invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
final String v6 = " +"; // const-string v6, " +"
(( java.lang.String ) v5 ).split ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 548 */
/* .local v5, "res":[Ljava/lang/String; */
/* array-length v6, v5 */
int v7 = 1; // const/4 v7, 0x1
/* if-lt v6, v7, :cond_0 */
int v6 = 0; // const/4 v6, 0x0
/* aget-object v7, v5, v6 */
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 549 */
/* aget-object v6, v5, v6 */
java.lang.Long .valueOf ( v6 );
(( java.lang.Long ) v6 ).longValue ( ); // invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
/* move-result-wide v6 */
/* const-wide/16 v8, 0x400 */
/* div-long/2addr v6, v8 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-wide v0, v6 */
/* .line 555 */
} // .end local v4 # "line":Ljava/lang/String;
} // .end local v5 # "res":[Ljava/lang/String;
} // :cond_0
/* nop */
/* .line 557 */
try { // :try_start_1
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 560 */
} // :goto_0
/* .line 558 */
/* :catch_0 */
/* move-exception v4 */
/* .line 559 */
/* .local v4, "e":Ljava/io/IOException; */
(( java.io.IOException ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
} // .end local v4 # "e":Ljava/io/IOException;
/* .line 555 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 552 */
/* :catch_1 */
/* move-exception v4 */
/* .line 553 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 555 */
} // .end local v4 # "e":Ljava/lang/Exception;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 557 */
try { // :try_start_3
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 563 */
} // :cond_1
} // :goto_1
/* return-wide v0 */
/* .line 555 */
} // :goto_2
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 557 */
try { // :try_start_4
(( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 560 */
/* .line 558 */
/* :catch_2 */
/* move-exception v5 */
/* .line 559 */
/* .local v5, "e":Ljava/io/IOException; */
(( java.io.IOException ) v5 ).printStackTrace ( ); // invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
/* .line 562 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_3
/* throw v4 */
} // .end method
private Long countBgMem ( ) {
/* .locals 12 */
/* .line 518 */
/* const-wide/16 v0, 0x0 */
/* .line 519 */
/* .local v0, "pssTotal":J */
v2 = this.mWMInternal;
(( com.android.server.wm.WindowManagerInternal ) v2 ).getVisibleWindowOwner ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerInternal;->getVisibleWindowOwner()Ljava/util/List;
/* .line 520 */
/* .local v2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v3 = this.mRunningPackages;
/* monitor-enter v3 */
/* .line 521 */
try { // :try_start_0
v4 = this.mRunningPackages;
(( java.util.HashMap ) v4 ).keySet ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* .line 522 */
/* .local v5, "uid":I */
v6 = this.mRunningPackages;
java.lang.Integer .valueOf ( v5 );
(( java.util.HashMap ) v6 ).get ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* .line 523 */
/* .local v6, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
v7 = com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmPid ( v6 );
/* if-lez v7, :cond_1 */
v7 = java.lang.Integer .valueOf ( v5 );
int v8 = -1; // const/4 v8, -0x1
/* if-ne v7, v8, :cond_1 */
/* .line 524 */
v7 = com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmPid ( v6 );
android.os.Process .getPss ( v7 );
/* move-result-wide v7 */
/* const-wide/16 v9, 0x400 */
/* div-long/2addr v7, v9 */
/* .line 525 */
/* .local v7, "pss":J */
/* sget-boolean v9, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z */
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 526 */
final String v9 = "MFZ"; // const-string v9, "MFZ"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "pss: "; // const-string v11, "pss: "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v7, v8 ); // invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v11 = " "; // const-string v11, " "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmPackageName ( v6 );
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ",uid:"; // const-string v11, ",uid:"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v5 ); // invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = ",pid:"; // const-string v11, ",pid:"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v11 = com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmPid ( v6 );
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v9,v10 );
/* .line 529 */
} // :cond_0
/* add-long/2addr v0, v7 */
/* .line 531 */
} // .end local v5 # "uid":I
} // .end local v6 # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
} // .end local v7 # "pss":J
} // :cond_1
/* .line 532 */
} // :cond_2
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 533 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->countBgFlush()J */
/* move-result-wide v3 */
/* .line 534 */
/* .local v3, "flushTotal":J */
/* add-long v5, v0, v3 */
/* .line 535 */
/* .local v5, "countBgMem":J */
final String v7 = "MFZ"; // const-string v7, "MFZ"
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "mem count: "; // const-string v9, "mem count: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0, v1 ); // invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = " + "; // const-string v9, " + "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v3, v4 ); // invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = " = "; // const-string v9, " = "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5, v6 ); // invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v7,v8 );
/* .line 536 */
/* return-wide v5 */
/* .line 532 */
} // .end local v3 # "flushTotal":J
} // .end local v5 # "countBgMem":J
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v4 */
} // .end method
private void dump ( java.io.PrintWriter p0 ) {
/* .locals 10 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 674 */
final String v0 = "---- Start of MEMFREEZE ----"; // const-string v0, "---- Start of MEMFREEZE ----"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 675 */
final String v0 = "App State:"; // const-string v0, "App State:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 676 */
v0 = this.mRunningPackages;
/* monitor-enter v0 */
/* .line 677 */
try { // :try_start_0
v1 = this.mRunningPackages;
(( java.util.HashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 678 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;>;" */
/* check-cast v3, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* .line 679 */
/* .local v3, "tmpAppInfo":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V
/* .line 680 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "%-25s"; // const-string v5, "%-25s"
int v6 = 1; // const/4 v6, 0x1
/* new-array v7, v6, [Ljava/lang/Object; */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmPackageName ( v3 );
int v9 = 0; // const/4 v9, 0x0
/* aput-object v8, v7, v9 */
java.lang.String .format ( v5,v7 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 681 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " Frozen="; // const-string v5, " Frozen="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "%-5s"; // const-string v5, "%-5s"
/* new-array v7, v6, [Ljava/lang/Object; */
v8 = com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmFrozen ( v3 );
java.lang.Boolean .valueOf ( v8 );
/* aput-object v8, v7, v9 */
java.lang.String .format ( v5,v7 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 682 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " Pending="; // const-string v5, " Pending="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "%-5s"; // const-string v5, "%-5s"
/* new-array v6, v6, [Ljava/lang/Object; */
v7 = com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmPendingFreeze ( v3 );
java.lang.Boolean .valueOf ( v7 );
/* aput-object v7, v6, v9 */
java.lang.String .format ( v5,v6 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 683 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;>;"
} // .end local v3 # "tmpAppInfo":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
/* goto/16 :goto_0 */
/* .line 684 */
} // :cond_0
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 685 */
v1 = this.mProcessFrozenUid;
/* monitor-enter v1 */
/* .line 686 */
try { // :try_start_1
final String v0 = "Process Frozen:"; // const-string v0, "Process Frozen:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 687 */
final String v0 = " Current Frozen Uid:"; // const-string v0, " Current Frozen Uid:"
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 688 */
v0 = v0 = this.mProcessFrozenUid;
/* .line 689 */
/* .local v0, "size":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
/* if-ge v2, v0, :cond_1 */
/* .line 690 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mProcessFrozenUid;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 689 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 692 */
} // .end local v0 # "size":I
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 693 */
final String v0 = "\n Total(Y/N) : "; // const-string v0, "\n Total(Y/N) : "
(( java.io.PrintWriter ) p1 ).print ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 694 */
final String v0 = "\n---- End of MEMFREEZE ----"; // const-string v0, "\n---- End of MEMFREEZE ----"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 695 */
return;
/* .line 692 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 684 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v1 */
} // .end method
public static com.android.server.am.MemoryFreezeStubImpl getInstance ( ) {
/* .locals 1 */
/* .line 120 */
/* const-class v0, Lcom/android/server/am/MemoryFreezeStub; */
com.miui.base.MiuiStubUtil .getImpl ( v0 );
/* check-cast v0, Lcom/android/server/am/MemoryFreezeStubImpl; */
} // .end method
private static android.os.storage.IStorageManager getStorageManager ( ) {
/* .locals 3 */
/* .line 167 */
try { // :try_start_0
final String v0 = "mount"; // const-string v0, "mount"
/* .line 168 */
android.os.ServiceManager .getService ( v0 );
/* .line 167 */
android.os.storage.IStorageManager$Stub .asInterface ( v0 );
/* :try_end_0 */
/* .catch Ljava/lang/VerifyError; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 169 */
/* :catch_0 */
/* move-exception v0 */
/* .line 170 */
/* .local v0, "e":Ljava/lang/VerifyError; */
final String v1 = "MFZ"; // const-string v1, "MFZ"
final String v2 = "get StorageManager error"; // const-string v2, "get StorageManager error"
android.util.Slog .e ( v1,v2 );
/* .line 171 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void lambda$checkUnused$0 ( Long p0, java.util.List p1, java.lang.Integer p2, com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo p3 ) { //synthethic
/* .locals 4 */
/* .param p1, "now" # J */
/* .param p3, "killList" # Ljava/util/List; */
/* .param p4, "key" # Ljava/lang/Integer; */
/* .param p5, "value" # Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* .line 377 */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmlastActivityStopTime ( p5 );
/* move-result-wide v0 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmlastActivityStopTime ( p5 );
/* move-result-wide v0 */
/* sub-long v0, p1, v0 */
/* iget-wide v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedKillTime:J */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
/* .line 379 */
v0 = com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fgetmUid ( p5 );
java.lang.Integer .valueOf ( v0 );
/* .line 381 */
} // :cond_0
return;
} // .end method
private void updateThreshhold ( ) {
/* .locals 7 */
/* .line 176 */
final String v0 = ","; // const-string v0, ","
final String v1 = "persist.sys.mfz.mem_policy"; // const-string v1, "persist.sys.mfz.mem_policy"
final String v2 = "2048,3072,4096"; // const-string v2, "2048,3072,4096"
android.os.SystemProperties .get ( v1,v2 );
/* .line 178 */
/* .local v1, "prop":Ljava/lang/String; */
try { // :try_start_0
(( java.lang.String ) v1 ).split ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 179 */
/* .local v2, "res":[Ljava/lang/String; */
/* array-length v3, v2 */
int v4 = 3; // const/4 v4, 0x3
/* if-ne v3, v4, :cond_0 */
/* .line 180 */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v3, v2, v3 */
java.lang.Long .valueOf ( v3 );
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v3 */
/* const-wide/16 v5, 0x400 */
/* mul-long/2addr v3, v5 */
/* sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J */
/* .line 181 */
int v3 = 1; // const/4 v3, 0x1
/* aget-object v3, v2, v3 */
java.lang.Long .valueOf ( v3 );
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v3 */
/* mul-long/2addr v3, v5 */
/* sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J */
/* .line 182 */
int v3 = 2; // const/4 v3, 0x2
/* aget-object v3, v2, v3 */
java.lang.Long .valueOf ( v3 );
(( java.lang.Long ) v3 ).longValue ( ); // invoke-virtual {v3}, Ljava/lang/Long;->longValue()J
/* move-result-wide v3 */
/* mul-long/2addr v3, v5 */
/* sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 189 */
} // .end local v2 # "res":[Ljava/lang/String;
} // :cond_0
/* .line 184 */
/* :catch_0 */
/* move-exception v2 */
/* .line 185 */
/* .local v2, "e":Ljava/lang/Exception; */
/* const-wide/32 v3, 0x200000 */
/* sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J */
/* .line 186 */
/* const-wide/32 v3, 0x300000 */
/* sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J */
/* .line 187 */
/* const-wide/32 v3, 0x400000 */
/* sput-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J */
/* .line 188 */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 190 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mfz policy: "; // const-string v3, "mfz policy: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdReclaim:J */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v3, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdWriteback:J */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v2, Lcom/android/server/am/MemoryFreezeStubImpl;->sThreshholdKill:J */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MFZ"; // const-string v2, "MFZ"
android.util.Slog .i ( v2,v0 );
/* .line 192 */
return;
} // .end method
/* # virtual methods */
public void activityResumed ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "processName" # Ljava/lang/String; */
/* .line 328 */
v0 = (( com.android.server.am.MemoryFreezeStubImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 329 */
} // :cond_0
v0 = v0 = com.android.server.am.MemoryFreezeStubImpl.sWhiteListApp;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 330 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 331 */
/* .local v0, "now":J */
v2 = this.mLastStartPackage;
v2 = (( java.lang.String ) p2 ).equals ( v2 ); // invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* iget-wide v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackageTime:J */
/* sub-long v2, v0, v2 */
/* const-wide/16 v4, 0x1388 */
/* cmp-long v2, v2, v4 */
/* if-gtz v2, :cond_1 */
/* .line 334 */
return;
/* .line 336 */
} // :cond_1
/* iput-wide v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mLastStartPackageTime:J */
/* .line 337 */
this.mLastStartPackage = p2;
/* .line 338 */
v2 = this.mRunningPackages;
/* monitor-enter v2 */
/* .line 339 */
try { // :try_start_0
v3 = this.mRunningPackages;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* .line 340 */
/* .local v3, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* if-nez v3, :cond_2 */
/* .line 341 */
/* new-instance v4, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* invoke-direct {v4, p0, p1, p2}, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;ILjava/lang/String;)V */
/* move-object v3, v4 */
/* .line 342 */
v4 = this.mRunningPackages;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v4 ).put ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 344 */
} // :cond_2
v4 = this.mHandler;
java.lang.Integer .valueOf ( p1 );
int v6 = 4; // const/4 v6, 0x4
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v4 ).removeEqualMessages ( v6, v5 ); // invoke-virtual {v4, v6, v5}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V
/* .line 345 */
int v4 = 0; // const/4 v4, 0x0
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fputmPendingFreeze ( v3,v4 );
/* .line 346 */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fputmFrozen ( v3,v4 );
/* .line 348 */
} // .end local v3 # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
} // :goto_0
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 349 */
final String v2 = "MFZ"; // const-string v2, "MFZ"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "activityResumed packageName:"; // const-string v4, "activityResumed packageName:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v4, "|" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ",uid:"; // const-string v4, ",uid:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 348 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_1
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v3 */
/* .line 351 */
} // .end local v0 # "now":J
} // :cond_3
} // :goto_1
return;
} // .end method
public void activityStopped ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 355 */
v0 = (( com.android.server.am.MemoryFreezeStubImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 356 */
} // :cond_0
v0 = v0 = com.android.server.am.MemoryFreezeStubImpl.sWhiteListApp;
/* if-nez v0, :cond_1 */
return;
/* .line 357 */
} // :cond_1
v0 = this.mRunningPackages;
/* monitor-enter v0 */
/* .line 358 */
try { // :try_start_0
v1 = this.mRunningPackages;
java.lang.Integer .valueOf ( p1 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* .line 359 */
/* .local v1, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 360 */
int v2 = 1; // const/4 v2, 0x1
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fputmPendingFreeze ( v1,v2 );
/* .line 361 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fputmlastActivityStopTime ( v1,v2,v3 );
/* .line 362 */
com.android.server.am.MemoryFreezeStubImpl$RamFrozenAppInfo .-$$Nest$fputmPid ( v1,p2 );
/* .line 364 */
} // .end local v1 # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 365 */
v0 = this.mHandler;
java.lang.Integer .valueOf ( p1 );
int v2 = 4; // const/4 v2, 0x4
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).removeEqualMessages ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V
/* .line 366 */
v0 = this.mHandler;
java.lang.Integer .valueOf ( p1 );
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).obtainMessage ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 367 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* iget-wide v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mFreezerDebounceTimeout:J */
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 368 */
final String v1 = "MFZ"; // const-string v1, "MFZ"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "activityStopped packageName:"; // const-string v3, "activityStopped packageName:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ",uid:"; // const-string v3, ",uid:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ",pid:"; // const-string v3, ",pid:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 369 */
return;
/* .line 364 */
} // .end local v0 # "msg":Landroid/os/Message;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void extmFlushFinished ( ) {
/* .locals 4 */
/* .line 476 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
v0 = (( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 477 */
final String v0 = "MFZ"; // const-string v0, "MFZ"
final String v2 = "remove kill msg"; // const-string v2, "remove kill msg"
android.util.Slog .d ( v0,v2 );
/* .line 478 */
v0 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeMessages(I)V
/* .line 480 */
} // :cond_0
v0 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 481 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0x2710 */
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 482 */
return;
} // .end method
public void getWhitePackages ( ) {
/* .locals 7 */
/* .line 195 */
final String v0 = "/data/system/mfz.xml"; // const-string v0, "/data/system/mfz.xml"
/* .line 196 */
/* .local v0, "path":Ljava/lang/String; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
final String v2 = "MFZ"; // const-string v2, "MFZ"
/* if-nez v1, :cond_0 */
/* .line 197 */
final String v0 = "/product/etc/mfz.xml"; // const-string v0, "/product/etc/mfz.xml"
/* .line 198 */
final String v1 = "looking for default xml."; // const-string v1, "looking for default xml."
android.util.Slog .i ( v2,v1 );
/* .line 200 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 201 */
/* .local v1, "inputStream":Ljava/io/InputStream; */
int v3 = 0; // const/4 v3, 0x0
/* .line 204 */
/* .local v3, "xmlParser":Lorg/xmlpull/v1/XmlPullParser; */
try { // :try_start_0
/* new-instance v4, Ljava/io/FileInputStream; */
/* invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V */
/* move-object v1, v4 */
/* .line 205 */
android.util.Xml .newPullParser ( );
/* move-object v3, v4 */
/* .line 206 */
/* const-string/jumbo v4, "utf-8" */
v4 = /* .line 207 */
/* .line 208 */
/* .local v4, "event":I */
} // :goto_0
int v5 = 1; // const/4 v5, 0x1
/* if-eq v4, v5, :cond_2 */
/* .line 209 */
/* packed-switch v4, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 218 */
/* :pswitch_1 */
/* .line 213 */
/* :pswitch_2 */
final String v5 = "designated-package"; // const-string v5, "designated-package"
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 214 */
v5 = com.android.server.am.MemoryFreezeStubImpl.sWhiteListApp;
int v6 = 0; // const/4 v6, 0x0
/* .line 211 */
/* :pswitch_3 */
/* nop */
/* .line 222 */
} // :cond_1
v5 = } // :goto_1
/* :try_end_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move v4, v5 */
/* .line 227 */
} // .end local v4 # "event":I
} // :cond_2
/* nop */
/* .line 229 */
try { // :try_start_1
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 233 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 230 */
/* :catch_0 */
/* move-exception v4 */
/* .line 231 */
/* .local v4, "e":Ljava/io/IOException; */
try { // :try_start_2
(( java.io.IOException ) v4 ).getMessage ( ); // invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v2,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 233 */
/* nop */
} // .end local v4 # "e":Ljava/io/IOException;
} // :goto_2
int v1 = 0; // const/4 v1, 0x0
/* .line 234 */
/* throw v2 */
/* .line 227 */
/* :catchall_1 */
/* move-exception v4 */
/* .line 224 */
/* :catch_1 */
/* move-exception v4 */
/* .line 225 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_3
(( java.lang.Exception ) v4 ).getMessage ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v2,v5 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 227 */
/* nop */
} // .end local v4 # "e":Ljava/lang/Exception;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 229 */
try { // :try_start_4
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_2 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* .line 233 */
/* nop */
} // :goto_3
int v1 = 0; // const/4 v1, 0x0
/* .line 234 */
/* .line 233 */
/* :catchall_2 */
/* move-exception v2 */
/* .line 230 */
/* :catch_2 */
/* move-exception v4 */
/* .line 231 */
/* .local v4, "e":Ljava/io/IOException; */
try { // :try_start_5
(( java.io.IOException ) v4 ).getMessage ( ); // invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v2,v5 );
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 233 */
/* nop */
} // .end local v4 # "e":Ljava/io/IOException;
} // :goto_4
int v1 = 0; // const/4 v1, 0x0
/* .line 234 */
/* throw v2 */
/* .line 237 */
} // :cond_3
} // :goto_5
return;
/* .line 227 */
} // :goto_6
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 229 */
try { // :try_start_6
(( java.io.InputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/InputStream;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_3 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_3 */
/* .line 233 */
/* nop */
} // :goto_7
int v1 = 0; // const/4 v1, 0x0
/* .line 234 */
/* .line 233 */
/* :catchall_3 */
/* move-exception v2 */
/* .line 230 */
/* :catch_3 */
/* move-exception v5 */
/* .line 231 */
/* .local v5, "e":Ljava/io/IOException; */
try { // :try_start_7
(( java.io.IOException ) v5 ).getMessage ( ); // invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v2,v6 );
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* .line 233 */
/* nop */
} // .end local v5 # "e":Ljava/io/IOException;
} // :goto_8
int v1 = 0; // const/4 v1, 0x0
/* .line 234 */
/* throw v2 */
/* .line 236 */
} // :cond_4
} // :goto_9
/* throw v4 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void init ( android.content.Context p0, com.android.server.am.ActivityManagerService p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .line 126 */
final String v0 = "ro.miui.build.region"; // const-string v0, "ro.miui.build.region"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
final String v1 = "cn"; // const-string v1, "cn"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 127 */
/* .local v0, "isCnVersion":Z */
/* if-nez v0, :cond_0 */
return;
/* .line 128 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->isInit:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
return;
/* .line 129 */
} // :cond_1
/* iget-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z */
/* if-nez v1, :cond_2 */
return;
/* .line 130 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mEnable:Z */
/* if-nez v1, :cond_3 */
return;
/* .line 131 */
} // :cond_3
this.mContext = p1;
/* .line 132 */
/* new-instance v1, Lcom/android/server/am/MemoryFreezeCloud; */
/* invoke-direct {v1}, Lcom/android/server/am/MemoryFreezeCloud;-><init>()V */
this.mMemoryFreezeCloud = v1;
/* .line 133 */
(( com.android.server.am.MemoryFreezeCloud ) v1 ).initCloud ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/MemoryFreezeCloud;->initCloud(Landroid/content/Context;)V
/* .line 134 */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
/* .line 135 */
/* new-instance v1, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler; */
v2 = this.mHandlerThread;
(( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 137 */
(( com.android.server.am.MemoryFreezeStubImpl ) p0 ).getWhitePackages ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->getWhitePackages()V
/* .line 138 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->updateThreshhold()V */
/* .line 140 */
v1 = this.mHandler;
int v2 = 1; // const/4 v2, 0x1
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v1 ).obtainMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 141 */
/* .local v1, "msgRegisterCloud":Landroid/os/Message; */
v3 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v3 ).sendMessage ( v1 ); // invoke-virtual {v3, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 147 */
v3 = this.mHandler;
int v4 = 3; // const/4 v4, 0x3
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v3 ).obtainMessage ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 148 */
/* .local v3, "msgCheckUnused":Landroid/os/Message; */
v4 = this.mHandler;
/* iget-wide v5, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mUnusedCheckTime:J */
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v4 ).sendMessageDelayed ( v3, v5, v6 ); // invoke-virtual {v4, v3, v5, v6}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 149 */
v4 = this.mHandler;
int v5 = 7; // const/4 v5, 0x7
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v4 ).obtainMessage ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 150 */
/* .local v4, "msgRegisterWhiteList":Landroid/os/Message; */
v5 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v5 ).sendMessage ( v4 ); // invoke-virtual {v5, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 152 */
/* new-instance v5, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService; */
int v6 = 0; // const/4 v6, 0x0
/* invoke-direct {v5, p0, v6}, Lcom/android/server/am/MemoryFreezeStubImpl$BinderService;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Lcom/android/server/am/MemoryFreezeStubImpl$BinderService-IA;)V */
this.mBinderService = v5;
/* .line 153 */
final String v6 = "memfreeze"; // const-string v6, "memfreeze"
android.os.ServiceManager .addService ( v6,v5 );
/* .line 155 */
this.mAMS = p2;
/* .line 156 */
/* const-class v5, Lcom/android/server/wm/WindowManagerInternal; */
com.android.server.LocalServices .getService ( v5 );
/* check-cast v5, Lcom/android/server/wm/WindowManagerInternal; */
this.mWMInternal = v5;
/* .line 157 */
v5 = this.mActivityTaskManager;
this.mATMS = v5;
/* .line 158 */
final String v5 = "ProcessManager"; // const-string v5, "ProcessManager"
android.os.ServiceManager .getService ( v5 );
/* check-cast v5, Lcom/android/server/am/ProcessManagerService; */
this.mPMS = v5;
/* .line 159 */
com.android.server.am.MemoryFreezeStubImpl .getStorageManager ( );
this.storageManager = v5;
/* .line 160 */
/* iput-boolean v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->isInit:Z */
/* .line 161 */
/* sget-boolean v2, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 162 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "complete init, default applist: "; // const-string v5, "complete init, default applist: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = com.android.server.am.MemoryFreezeStubImpl.sWhiteListApp;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MFZ"; // const-string v5, "MFZ"
android.util.Slog .d ( v5,v2 );
/* .line 164 */
} // :cond_4
return;
} // .end method
public Boolean isEnable ( ) {
/* .locals 1 */
/* .line 116 */
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->isInit:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isMemoryFreezeWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 319 */
v0 = (( com.android.server.am.MemoryFreezeStubImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 320 */
} // :cond_0
v0 = v0 = com.android.server.am.MemoryFreezeStubImpl.sWhiteListApp;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 321 */
int v0 = 1; // const/4 v0, 0x1
/* .line 323 */
} // :cond_1
} // .end method
public Boolean isNeedProtect ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 3 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 568 */
v0 = (( com.android.server.am.MemoryFreezeStubImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 569 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 570 */
} // :cond_1
v0 = this.info;
v0 = this.packageName;
/* .line 571 */
/* .local v0, "packageName":Ljava/lang/String; */
v2 = v2 = com.android.server.am.MemoryFreezeStubImpl.sWhiteListApp;
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = v2 = com.android.server.am.MemoryFreezeStubImpl.sBlackListApp;
/* if-nez v2, :cond_2 */
/* .line 572 */
int v1 = 1; // const/4 v1, 0x1
/* .line 574 */
} // :cond_2
} // .end method
public void processReclaimFinished ( ) {
/* .locals 4 */
/* .line 435 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
v0 = (( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->hasMessages(I)Z
final String v2 = "MFZ"; // const-string v2, "MFZ"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 436 */
final String v0 = "remove kill msg"; // const-string v0, "remove kill msg"
android.util.Slog .d ( v2,v0 );
/* .line 437 */
v0 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeMessages(I)V
/* .line 439 */
} // :cond_0
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
v0 = (( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 440 */
final String v0 = "remove WB msg"; // const-string v0, "remove WB msg"
android.util.Slog .d ( v2,v0 );
/* .line 441 */
v0 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeMessages(I)V
/* .line 443 */
} // :cond_1
v0 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 444 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* const-wide/16 v2, 0x2710 */
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 445 */
return;
} // .end method
public void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 304 */
/* new-instance v0, Lcom/android/server/am/MemoryFreezeStubImpl$2; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$2;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Landroid/os/Handler;)V */
/* .line 312 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 313 */
final String v2 = "cloud_memFreeze_control"; // const-string v2, "cloud_memFreeze_control"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 312 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 315 */
return;
} // .end method
public void registerMiuiFreezeObserver ( android.content.Context p0 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 290 */
/* new-instance v0, Lcom/android/server/am/MemoryFreezeStubImpl$1; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryFreezeStubImpl$1;-><init>(Lcom/android/server/am/MemoryFreezeStubImpl;Landroid/os/Handler;)V */
/* .line 298 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 299 */
final String v2 = "miui_freeze"; // const-string v2, "miui_freeze"
android.provider.Settings$Secure .getUriFor ( v2 );
/* .line 298 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 301 */
return;
} // .end method
public void reportAppDied ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 7 */
/* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
/* .line 579 */
v0 = (( com.android.server.am.MemoryFreezeStubImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 580 */
} // :cond_0
/* if-nez p1, :cond_1 */
return;
/* .line 581 */
} // :cond_1
v0 = this.processName;
/* .line 582 */
/* .local v0, "processName":Ljava/lang/String; */
/* iget v1, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
/* .line 583 */
/* .local v1, "uid":I */
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* .line 584 */
/* .local v2, "pid":I */
if ( v0 != null) { // if-eqz v0, :cond_3
v3 = v3 = com.android.server.am.MemoryFreezeStubImpl.sWhiteListApp;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 585 */
v3 = this.mHandler;
int v4 = 4; // const/4 v4, 0x4
java.lang.Integer .valueOf ( v1 );
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v3 ).removeEqualMessages ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V
/* .line 586 */
v3 = this.mRunningPackages;
/* monitor-enter v3 */
/* .line 587 */
try { // :try_start_0
v4 = this.mRunningPackages;
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v4 ).get ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
/* .line 588 */
/* .local v4, "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo; */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 589 */
v5 = this.mRunningPackages;
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v5 ).remove ( v6 ); // invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 591 */
} // .end local v4 # "target":Lcom/android/server/am/MemoryFreezeStubImpl$RamFrozenAppInfo;
} // :cond_2
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 592 */
final String v3 = "MFZ"; // const-string v3, "MFZ"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "report "; // const-string v5, "report "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " is died, uid:"; // const-string v5, " is died, uid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ",pid:"; // const-string v5, ",pid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 593 */
/* iget v3, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->mKillingUID:I */
/* if-ne v1, v3, :cond_3 */
/* .line 595 */
v3 = this.mHandler;
int v4 = 6; // const/4 v4, 0x6
v3 = (( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v3 ).hasMessages ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->hasMessages(I)Z
/* if-nez v3, :cond_3 */
/* .line 596 */
v3 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v3 ).obtainMessage ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 597 */
/* .local v3, "msg":Landroid/os/Message; */
v4 = this.mHandler;
(( com.android.server.am.MemoryFreezeStubImpl$MyHandler ) v4 ).sendMessage ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/server/am/MemoryFreezeStubImpl$MyHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 591 */
} // .end local v3 # "msg":Landroid/os/Message;
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v4 */
/* .line 601 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void updateCloudControlParas ( ) {
/* .locals 3 */
/* .line 240 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cloud_memFreeze_control"; // const-string v1, "cloud_memFreeze_control"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 242 */
v0 = this.mContext;
/* .line 243 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 242 */
android.provider.Settings$System .getStringForUser ( v0,v1,v2 );
/* .line 246 */
/* .local v0, "enableStr":Ljava/lang/String; */
final String v1 = "0"; // const-string v1, "0"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* xor-int/lit8 v1, v1, 0x1 */
/* iput-boolean v1, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z */
/* .line 247 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "set enable state from database: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/android/server/am/MemoryFreezeStubImpl;->extmEnable:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MFZ"; // const-string v2, "MFZ"
android.util.Slog .w ( v2,v1 );
/* .line 249 */
} // .end local v0 # "enableStr":Ljava/lang/String;
} // :cond_0
return;
} // .end method
public void updateFrozedUids ( java.util.List p0, java.util.List p1 ) {
/* .locals 10 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 605 */
/* .local p1, "freezelist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p2, "thawlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v0 = (( com.android.server.am.MemoryFreezeStubImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 606 */
} // :cond_0
/* const-string/jumbo v0, "update process freeze list:" */
/* .line 608 */
v1 = /* .local v0, "mUpdateFrozenInfo":Ljava/lang/String; */
/* if-nez v1, :cond_3 */
v1 = /* .line 609 */
/* .line 610 */
/* .local v1, "size":I */
v2 = this.mProcessFrozenUid;
/* monitor-enter v2 */
/* .line 611 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v1, :cond_2 */
/* .line 612 */
try { // :try_start_0
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 613 */
/* .local v4, "value":I */
v5 = this.mProcessFrozenUid;
v5 = java.lang.Integer .valueOf ( v4 );
/* if-nez v5, :cond_1 */
/* .line 614 */
v5 = this.mProcessFrozenUid;
java.lang.Integer .valueOf ( v4 );
/* .line 611 */
} // .end local v4 # "value":I
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 617 */
} // .end local v3 # "i":I
} // :cond_2
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
/* .line 620 */
} // .end local v1 # "size":I
} // :cond_3
v1 = } // :goto_1
/* if-nez v1, :cond_6 */
v1 = /* .line 621 */
/* .line 622 */
/* .restart local v1 # "size":I */
v2 = this.mProcessFrozenUid;
/* monitor-enter v2 */
/* .line 623 */
int v3 = 0; // const/4 v3, 0x0
/* .restart local v3 # "i":I */
} // :goto_2
/* if-ge v3, v1, :cond_5 */
/* .line 624 */
try { // :try_start_1
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 625 */
/* .restart local v4 # "value":I */
v5 = this.mProcessFrozenUid;
v5 = java.lang.Integer .valueOf ( v4 );
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 626 */
v5 = this.mProcessFrozenUid;
/* new-instance v6, Ljava/lang/Integer; */
/* invoke-direct {v6, v4}, Ljava/lang/Integer;-><init>(I)V */
/* .line 623 */
} // .end local v4 # "value":I
} // :cond_4
/* add-int/lit8 v3, v3, 0x1 */
/* .line 629 */
} // .end local v3 # "i":I
} // :cond_5
/* monitor-exit v2 */
/* :catchall_1 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v3 */
/* .line 632 */
} // .end local v1 # "size":I
} // :cond_6
} // :goto_3
v1 = this.mProcessFrozenUid;
/* monitor-enter v1 */
/* .line 633 */
try { // :try_start_2
v2 = v2 = this.mProcessFrozenUid;
/* .line 634 */
/* .local v2, "size":I */
/* if-lez v2, :cond_a */
/* .line 635 */
int v3 = 0; // const/4 v3, 0x0
/* .restart local v3 # "i":I */
} // :goto_4
/* if-ge v3, v2, :cond_9 */
/* .line 636 */
v4 = this.mProcessFrozenUid;
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 637 */
/* .restart local v4 # "value":I */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v4 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 638 */
/* .local v5, "subStr":Ljava/lang/String; */
v6 = this.mContext;
(( android.content.Context ) v6 ).getPackageManager ( ); // invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v6 ).getPackagesForUid ( v4 ); // invoke-virtual {v6, v4}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 639 */
/* .local v6, "packages":[Ljava/lang/String; */
/* if-nez v6, :cond_7 */
/* .line 640 */
} // :cond_7
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "j":I */
} // :goto_5
/* array-length v8, v6 */
/* if-ge v7, v8, :cond_8 */
/* .line 641 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v9, "|" */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v9, v6, v7 */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v5, v8 */
/* .line 640 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 643 */
} // .end local v7 # "j":I
} // :cond_8
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v0, v7 */
/* .line 635 */
} // .end local v4 # "value":I
} // .end local v5 # "subStr":Ljava/lang/String;
} // .end local v6 # "packages":[Ljava/lang/String;
} // :goto_6
/* add-int/lit8 v3, v3, 0x1 */
} // .end local v3 # "i":I
} // :cond_9
/* .line 646 */
} // :cond_a
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " null"; // const-string v4, " null"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* move-object v0, v3 */
/* .line 649 */
} // .end local v2 # "size":I
} // :goto_7
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* .line 650 */
/* sget-boolean v1, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 651 */
final String v1 = "MFZ"; // const-string v1, "MFZ"
android.util.Slog .d ( v1,v0 );
/* .line 653 */
} // :cond_b
return;
/* .line 649 */
/* :catchall_2 */
/* move-exception v2 */
try { // :try_start_3
/* monitor-exit v1 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
/* throw v2 */
} // .end method
public void updateMiuiFreezeInfo ( ) {
/* .locals 11 */
/* .line 252 */
v0 = (( com.android.server.am.MemoryFreezeStubImpl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryFreezeStubImpl;->isEnable()Z
/* if-nez v0, :cond_0 */
return;
/* .line 253 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "miui_freeze"; // const-string v1, "miui_freeze"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$Secure .getStringForUser ( v0,v1,v2 );
/* .line 255 */
/* .local v0, "miuiFreezeStr":Ljava/lang/String; */
/* sget-boolean v1, Lcom/android/server/am/MemoryFreezeStubImpl;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 256 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SettingsContentObserver change str="; // const-string v2, "SettingsContentObserver change str="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MFZ"; // const-string v2, "MFZ"
android.util.Slog .d ( v2,v1 );
/* .line 258 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 259 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 260 */
/* .local v1, "thawList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 261 */
/* .local v2, "freezeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
final String v3 = ""; // const-string v3, ""
/* .local v3, "freezeSub":Ljava/lang/String; */
final String v4 = ""; // const-string v4, ""
/* .line 263 */
/* .local v4, "thawSub":Ljava/lang/String; */
/* const-string/jumbo v5, "thaw" */
v5 = (( java.lang.String ) v0 ).contains ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
final String v6 = "freeze"; // const-string v6, "freeze"
final String v7 = ";freeze:"; // const-string v7, ";freeze:"
int v8 = 1; // const/4 v8, 0x1
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 264 */
/* const-string/jumbo v5, "thaw:" */
(( java.lang.String ) v0 ).split ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v9, v9, v8 */
(( java.lang.String ) v9 ).split ( v7 ); // invoke-virtual {v9, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v10 = 0; // const/4 v10, 0x0
/* aget-object v4, v9, v10 */
/* .line 265 */
v6 = (( java.lang.String ) v0 ).contains ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 266 */
(( java.lang.String ) v0 ).split ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v5, v5, v8 */
(( java.lang.String ) v5 ).split ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v3, v5, v8 */
/* .line 268 */
} // :cond_2
v5 = (( java.lang.String ) v0 ).contains ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 269 */
(( java.lang.String ) v0 ).split ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* aget-object v3, v5, v8 */
/* .line 272 */
} // :cond_3
} // :goto_0
v5 = (( java.lang.String ) v4 ).length ( ); // invoke-virtual {v4}, Ljava/lang/String;->length()I
final String v6 = ", "; // const-string v6, ", "
/* if-lez v5, :cond_4 */
/* .line 273 */
(( java.lang.String ) v4 ).split ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 274 */
/* .local v5, "tmpStr":[Ljava/lang/String; */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_1
/* array-length v8, v5 */
/* if-ge v7, v8, :cond_4 */
/* .line 275 */
/* aget-object v8, v5, v7 */
v8 = java.lang.Integer .parseInt ( v8 );
java.lang.Integer .valueOf ( v8 );
/* .line 274 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 279 */
} // .end local v5 # "tmpStr":[Ljava/lang/String;
} // .end local v7 # "i":I
} // :cond_4
v5 = (( java.lang.String ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/lang/String;->length()I
/* if-lez v5, :cond_5 */
/* .line 280 */
(( java.lang.String ) v3 ).split ( v6 ); // invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 281 */
/* .restart local v5 # "tmpStr":[Ljava/lang/String; */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "i":I */
} // :goto_2
/* array-length v7, v5 */
/* if-ge v6, v7, :cond_5 */
/* .line 282 */
/* aget-object v7, v5, v6 */
v7 = java.lang.Integer .parseInt ( v7 );
java.lang.Integer .valueOf ( v7 );
/* .line 281 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 285 */
} // .end local v5 # "tmpStr":[Ljava/lang/String;
} // .end local v6 # "i":I
} // :cond_5
(( com.android.server.am.MemoryFreezeStubImpl ) p0 ).updateFrozedUids ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/android/server/am/MemoryFreezeStubImpl;->updateFrozedUids(Ljava/util/List;Ljava/util/List;)V
/* .line 287 */
} // .end local v1 # "thawList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v2 # "freezeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v3 # "freezeSub":Ljava/lang/String;
} // .end local v4 # "thawSub":Ljava/lang/String;
} // :cond_6
return;
} // .end method
