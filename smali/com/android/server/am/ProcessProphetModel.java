public class com.android.server.am.ProcessProphetModel {
	 /* .source "ProcessProphetModel.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;, */
	 /* Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;, */
	 /* Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;, */
	 /* Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;, */
	 /* Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;, */
	 /* Lcom/android/server/am/ProcessProphetModel$PkgValuePair;, */
	 /* Lcom/android/server/am/ProcessProphetModel$LaunchInfoDaily; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String BT_MODEL_NAME;
private static final Integer BT_OF_TRACK;
private static final java.lang.String BT_PROBTAB_NAME;
private static Double BT_PROB_THRESHOLD;
private static final Long BT_PROTECTION_TIME;
private static Double CP_PROB_THRESHOLD;
private static final Long CP_PROTECTION_TIME;
private static final Integer DAY_OF_TRACK;
private static Boolean DEBUG;
private static final java.lang.String LU_MODEL_NAME;
private static final java.lang.String LU_PROBTAB_NAME;
private static Double LU_PROB_THRESHOLD;
private static final java.lang.String PSS_MODEL_NAME;
private static final java.lang.String SAVE_PATH;
private static final Long SLICE_IN_MILLIS;
private static final Integer SLICE_OF_DAY;
private static final java.lang.String TAG;
/* # instance fields */
private com.android.server.am.ProcessProphetModel$UseInfoMapList mBTProbTab;
private com.android.server.am.ProcessProphetModel$BlueToothUsageModel mBTUsageModel;
public java.util.HashSet mBlackListBTAudio;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.HashSet mBlackListLaunch;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.ProcessProphetModel$CopyPatternModel mCopyPatternModel;
private java.util.HashMap mEmptyProcPss;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.ProcessProphetModel$LaunchUsageModel mLUModel;
private com.android.server.am.ProcessProphetModel$LaunchUsageProbTab mLUProbTab;
private Long mLastBlueToothTime;
private Long mLastCopyedTime;
private java.lang.String mLastMatchedPkg;
public java.util.HashSet mWhiteListEmptyProc;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static Boolean -$$Nest$sfgetDEBUG ( ) { //bridge//synthethic
/* .locals 1 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
} // .end method
static Boolean -$$Nest$smisWeekend ( java.util.Calendar p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = com.android.server.am.ProcessProphetModel .isWeekend ( p0 );
} // .end method
static com.android.server.am.ProcessProphetModel ( ) {
/* .locals 6 */
/* .line 34 */
/* nop */
/* .line 35 */
final String v0 = "persist.sys.procprophet.debug"; // const-string v0, "persist.sys.procprophet.debug"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.ProcessProphetModel.DEBUG = (v0!= 0);
/* .line 45 */
/* nop */
/* .line 46 */
final String v0 = "persist.sys.procprophet.lu_threshold"; // const-string v0, "persist.sys.procprophet.lu_threshold"
/* const/16 v1, 0x3c */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* int-to-double v0, v0 */
/* const-wide/high16 v2, 0x4059000000000000L # 100.0 */
/* div-double/2addr v0, v2 */
/* sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D */
/* .line 56 */
/* nop */
/* .line 57 */
final String v0 = "persist.sys.procprophet.bt_threshold"; // const-string v0, "persist.sys.procprophet.bt_threshold"
/* const/16 v1, 0x46 */
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* int-to-double v4, v0 */
/* div-double/2addr v4, v2 */
/* sput-wide v4, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D */
/* .line 64 */
/* nop */
/* .line 65 */
final String v0 = "persist.sys.procprophet.cp_threshold"; // const-string v0, "persist.sys.procprophet.cp_threshold"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* int-to-double v0, v0 */
/* div-double/2addr v0, v2 */
/* sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D */
/* .line 64 */
return;
} // .end method
public com.android.server.am.ProcessProphetModel ( ) {
/* .locals 5 */
/* .line 74 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 43 */
int v0 = 0; // const/4 v0, 0x0
this.mLUModel = v0;
/* .line 44 */
this.mLUProbTab = v0;
/* .line 52 */
this.mBTUsageModel = v0;
/* .line 53 */
this.mBTProbTab = v0;
/* .line 54 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastBlueToothTime:J */
/* .line 60 */
/* new-instance v2, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel; */
/* invoke-direct {v2}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;-><init>()V */
this.mCopyPatternModel = v2;
/* .line 61 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J */
/* .line 63 */
final String v0 = ""; // const-string v0, ""
this.mLastMatchedPkg = v0;
/* .line 67 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mWhiteListEmptyProc = v0;
/* .line 68 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mBlackListLaunch = v0;
/* .line 69 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mBlackListBTAudio = v0;
/* .line 72 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mEmptyProcPss = v0;
/* .line 75 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 76 */
/* .local v0, "startUpTime":J */
/* invoke-direct {p0}, Lcom/android/server/am/ProcessProphetModel;->init()V */
/* .line 77 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Model init consumed "; // const-string v3, "Model init consumed "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* sub-long/2addr v3, v0 */
(( java.lang.StringBuilder ) v2 ).append ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms"; // const-string v3, "ms"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessProphetModel"; // const-string v3, "ProcessProphetModel"
android.util.Slog .d ( v3,v2 );
/* .line 78 */
return;
} // .end method
private void init ( ) {
/* .locals 5 */
/* .line 83 */
final String v0 = "ProcessProphetModel"; // const-string v0, "ProcessProphetModel"
try { // :try_start_0
final String v1 = "launchusage.model"; // const-string v1, "launchusage.model"
(( com.android.server.am.ProcessProphetModel ) p0 ).loadModelFromDisk ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel; */
this.mLUModel = v1;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 86 */
/* .line 84 */
/* :catch_0 */
/* move-exception v1 */
/* .line 85 */
/* .local v1, "e":Ljava/lang/Exception; */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to load LU model: "; // const-string v3, "Failed to load LU model: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 87 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
v1 = this.mLUModel;
/* if-nez v1, :cond_1 */
/* .line 88 */
final String v1 = "Failed to load LU model, return a new Model."; // const-string v1, "Failed to load LU model, return a new Model."
android.util.Slog .w ( v0,v1 );
/* .line 89 */
/* new-instance v1, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel; */
/* invoke-direct {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;-><init>()V */
this.mLUModel = v1;
/* .line 94 */
} // :cond_1
try { // :try_start_1
final String v1 = "bt.model"; // const-string v1, "bt.model"
(( com.android.server.am.ProcessProphetModel ) p0 ).loadModelFromDisk ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel; */
this.mBTUsageModel = v1;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 97 */
/* .line 95 */
/* :catch_1 */
/* move-exception v1 */
/* .line 96 */
/* .restart local v1 # "e":Ljava/lang/Exception; */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to load BT model: "; // const-string v3, "Failed to load BT model: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 98 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_1
v1 = this.mBTUsageModel;
/* if-nez v1, :cond_3 */
/* .line 99 */
final String v1 = "Failed to load BT model, return a new Model."; // const-string v1, "Failed to load BT model, return a new Model."
android.util.Slog .w ( v0,v1 );
/* .line 100 */
/* new-instance v1, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel; */
/* invoke-direct {v1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;-><init>()V */
this.mBTUsageModel = v1;
/* .line 105 */
} // :cond_3
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_2
final String v2 = "launchusage.prob"; // const-string v2, "launchusage.prob"
(( com.android.server.am.ProcessProphetModel ) p0 ).loadModelFromDisk ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab; */
this.mLUProbTab = v2;
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_2 */
/* .line 109 */
/* .line 106 */
/* :catch_2 */
/* move-exception v2 */
/* .line 107 */
/* .local v2, "e":Ljava/lang/Exception; */
this.mLUProbTab = v1;
/* .line 108 */
/* sget-boolean v3, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_4
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to load LU probtab: "; // const-string v4, "Failed to load LU probtab: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v3 );
/* .line 113 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_4
} // :goto_2
try { // :try_start_3
final String v2 = "bt.prob"; // const-string v2, "bt.prob"
(( com.android.server.am.ProcessProphetModel ) p0 ).loadModelFromDisk ( v2 ); // invoke-virtual {p0, v2}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList; */
this.mBTProbTab = v2;
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_3 */
/* .line 117 */
/* .line 114 */
/* :catch_3 */
/* move-exception v2 */
/* .line 115 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
this.mBTProbTab = v1;
/* .line 116 */
/* sget-boolean v1, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to load BT probtab: "; // const-string v3, "Failed to load BT probtab: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 121 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_5
} // :goto_3
try { // :try_start_4
final String v1 = "pss.model"; // const-string v1, "pss.model"
(( com.android.server.am.ProcessProphetModel ) p0 ).loadModelFromDisk ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/ProcessProphetModel;->loadModelFromDisk(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/HashMap; */
this.mEmptyProcPss = v1;
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_4 */
/* .line 124 */
/* .line 122 */
/* :catch_4 */
/* move-exception v1 */
/* .line 123 */
/* .restart local v1 # "e":Ljava/lang/Exception; */
/* sget-boolean v2, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_6
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to load PSS model: "; // const-string v3, "Failed to load PSS model: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 125 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_6
} // :goto_4
v1 = this.mEmptyProcPss;
/* if-nez v1, :cond_7 */
/* .line 126 */
final String v1 = "Failed to load PSS model, return a new Model."; // const-string v1, "Failed to load PSS model, return a new Model."
android.util.Slog .w ( v0,v1 );
/* .line 127 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mEmptyProcPss = v0;
/* .line 129 */
} // :cond_7
return;
} // .end method
private static Boolean isWeekend ( java.util.Calendar p0 ) {
/* .locals 4 */
/* .param p0, "mycalendar" # Ljava/util/Calendar; */
/* .line 788 */
int v0 = 7; // const/4 v0, 0x7
v0 = (( java.util.Calendar ) p0 ).get ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I
int v1 = 1; // const/4 v1, 0x1
/* sub-int/2addr v0, v1 */
/* .line 789 */
/* .local v0, "dayOfWeek":I */
final String v2 = "ProcessProphetModel"; // const-string v2, "ProcessProphetModel"
if ( v0 != null) { // if-eqz v0, :cond_2
int v3 = 6; // const/4 v3, 0x6
/* if-ne v0, v3, :cond_0 */
/* .line 793 */
} // :cond_0
/* sget-boolean v1, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "Today is workday."; // const-string v1, "Today is workday."
android.util.Slog .i ( v2,v1 );
/* .line 794 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 790 */
} // :cond_2
} // :goto_0
/* sget-boolean v3, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_3
final String v3 = "Today is weekend."; // const-string v3, "Today is weekend."
android.util.Slog .i ( v2,v3 );
/* .line 791 */
} // :cond_3
} // .end method
private Double probFusion ( java.lang.String p0, java.lang.Double p1, java.lang.Double p2, java.lang.Double p3 ) {
/* .locals 16 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .param p2, "weightLU" # Ljava/lang/Double; */
/* .param p3, "weightBT" # Ljava/lang/Double; */
/* .param p4, "weightCP" # Ljava/lang/Double; */
/* .line 388 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* const-wide/16 v2, 0x0 */
/* .line 395 */
java.lang.Double .valueOf ( v2,v3 );
/* .line 388 */
if ( v1 != null) { // if-eqz v1, :cond_8
final String v5 = ""; // const-string v5, ""
v5 = (( java.lang.String ) v1 ).equals ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* move-object/from16 v9, p2 */
/* move-object/from16 v10, p3 */
/* move-object/from16 v11, p4 */
/* goto/16 :goto_3 */
/* .line 391 */
} // :cond_0
/* const-wide/16 v2, 0x0 */
/* .line 392 */
/* .local v2, "probLU":D */
/* const-wide/16 v5, 0x0 */
/* .line 393 */
/* .local v5, "probBT":D */
/* const-wide/16 v7, 0x0 */
/* .line 395 */
/* .local v7, "probCP":D */
v9 = this.mLUProbTab;
if ( v9 != null) { // if-eqz v9, :cond_1
/* move-object/from16 v9, p2 */
v10 = (( java.lang.Double ) v9 ).compareTo ( v4 ); // invoke-virtual {v9, v4}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 396 */
v10 = this.mLUProbTab;
(( com.android.server.am.ProcessProphetModel$LaunchUsageProbTab ) v10 ).getProb ( v1 ); // invoke-virtual {v10, v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->getProb(Ljava/lang/String;)Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
/* .line 397 */
/* .local v10, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
/* iget-wide v11, v10, Lcom/android/server/am/ProcessProphetModel$PkgValuePair;->value:D */
/* invoke-virtual/range {p2 ..p2}, Ljava/lang/Double;->doubleValue()D */
/* move-result-wide v13 */
/* mul-double v2, v11, v13 */
/* .line 395 */
} // .end local v10 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
} // :cond_1
/* move-object/from16 v9, p2 */
/* .line 400 */
} // :cond_2
} // :goto_0
v10 = this.mBTProbTab;
if ( v10 != null) { // if-eqz v10, :cond_3
/* move-object/from16 v10, p3 */
v11 = (( java.lang.Double ) v10 ).compareTo ( v4 ); // invoke-virtual {v10, v4}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I
if ( v11 != null) { // if-eqz v11, :cond_4
/* .line 401 */
v11 = this.mBTProbTab;
(( com.android.server.am.ProcessProphetModel$UseInfoMapList ) v11 ).getProb ( v1 ); // invoke-virtual {v11, v1}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProb(Ljava/lang/String;)D
/* move-result-wide v11 */
/* invoke-virtual/range {p3 ..p3}, Ljava/lang/Double;->doubleValue()D */
/* move-result-wide v13 */
/* mul-double v5, v11, v13 */
/* .line 400 */
} // :cond_3
/* move-object/from16 v10, p3 */
/* .line 404 */
} // :cond_4
} // :goto_1
v11 = this.mCopyPatternModel;
if ( v11 != null) { // if-eqz v11, :cond_5
/* move-object/from16 v11, p4 */
v4 = (( java.lang.Double ) v11 ).compareTo ( v4 ); // invoke-virtual {v11, v4}, Ljava/lang/Double;->compareTo(Ljava/lang/Double;)I
if ( v4 != null) { // if-eqz v4, :cond_6
v4 = this.mLastMatchedPkg;
/* .line 405 */
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 406 */
v4 = this.mCopyPatternModel;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v4 ).getProb ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->getProb(Ljava/lang/String;)D
/* move-result-wide v12 */
/* invoke-virtual/range {p4 ..p4}, Ljava/lang/Double;->doubleValue()D */
/* move-result-wide v14 */
/* mul-double v7, v12, v14 */
/* .line 404 */
} // :cond_5
/* move-object/from16 v11, p4 */
/* .line 408 */
} // :cond_6
} // :goto_2
/* new-instance v4, Ljava/text/DecimalFormat; */
final String v12 = "#.####"; // const-string v12, "#.####"
/* invoke-direct {v4, v12}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V */
/* .line 409 */
/* .local v4, "doubleDF":Ljava/text/DecimalFormat; */
/* sget-boolean v12, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v12 != null) { // if-eqz v12, :cond_7
/* .line 410 */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "\tprob fusion: "; // const-string v13, "\tprob fusion: "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v1 ); // invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v13 = "["; // const-string v13, "["
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.text.DecimalFormat ) v4 ).format ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v13 = ", "; // const-string v13, ", "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 411 */
(( java.text.DecimalFormat ) v4 ).format ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v14 ); // invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.text.DecimalFormat ) v4 ).format ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v13 = "]"; // const-string v13, "]"
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 410 */
final String v13 = "ProcessProphetModel"; // const-string v13, "ProcessProphetModel"
android.util.Slog .d ( v13,v12 );
/* .line 413 */
} // :cond_7
/* add-double v12, v2, v5 */
/* add-double/2addr v12, v7 */
/* return-wide v12 */
/* .line 388 */
} // .end local v2 # "probLU":D
} // .end local v4 # "doubleDF":Ljava/text/DecimalFormat;
} // .end local v5 # "probBT":D
} // .end local v7 # "probCP":D
} // :cond_8
/* move-object/from16 v9, p2 */
/* move-object/from16 v10, p3 */
/* move-object/from16 v11, p4 */
/* .line 389 */
} // :goto_3
/* return-wide v2 */
} // .end method
private void saveModelToDisk ( java.io.Serializable p0, java.lang.String p1 ) {
/* .locals 10 */
/* .param p1, "model" # Ljava/io/Serializable; */
/* .param p2, "modelName" # Ljava/lang/String; */
/* .line 420 */
final String v0 = "error when closing fos."; // const-string v0, "error when closing fos."
final String v1 = "error when closing oos."; // const-string v1, "error when closing oos."
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/system/procprophet/"; // const-string v3, "/data/system/procprophet/"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 421 */
/* .local v2, "dir":Ljava/io/File; */
v4 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
final String v5 = "ProcessProphetModel"; // const-string v5, "ProcessProphetModel"
/* if-nez v4, :cond_0 */
/* .line 422 */
v4 = (( java.io.File ) v2 ).mkdirs ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
/* if-nez v4, :cond_0 */
/* .line 423 */
final String v0 = "mk dir fail."; // const-string v0, "mk dir fail."
android.util.Slog .w ( v5,v0 );
/* .line 424 */
return;
/* .line 428 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .line 429 */
/* .local v4, "fileOut":Ljava/io/FileOutputStream; */
int v6 = 0; // const/4 v6, 0x0
/* .line 431 */
/* .local v6, "objOut":Ljava/io/ObjectOutputStream; */
try { // :try_start_0
/* new-instance v7, Ljava/io/FileOutputStream; */
/* new-instance v8, Ljava/io/File; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v9 ).append ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v8, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v7, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v4, v7 */
/* .line 432 */
/* new-instance v3, Ljava/io/ObjectOutputStream; */
/* invoke-direct {v3, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* move-object v6, v3 */
/* .line 433 */
(( java.io.ObjectOutputStream ) v6 ).writeObject ( p1 ); // invoke-virtual {v6, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
/* .line 434 */
/* sget-boolean v3, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Save "; // const-string v7, "Save "
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " done."; // const-string v7, " done."
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v5,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 438 */
} // :cond_1
/* nop */
/* .line 440 */
try { // :try_start_1
(( java.io.ObjectOutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/ObjectOutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 443 */
/* .line 441 */
/* :catch_0 */
/* move-exception v3 */
/* .line 442 */
/* .local v3, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v1 );
/* .line 445 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_0
/* nop */
/* .line 447 */
try { // :try_start_2
(( java.io.FileOutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 450 */
} // :goto_1
/* .line 448 */
/* :catch_1 */
/* move-exception v1 */
/* .line 449 */
/* .local v1, "e":Ljava/io/IOException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_2
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v0 );
} // .end local v1 # "e":Ljava/io/IOException;
/* .line 438 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 435 */
/* :catch_2 */
/* move-exception v3 */
/* .line 436 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_3
/* sget-boolean v7, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_2
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Failed to save "; // const-string v8, "Failed to save "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p2 ); // invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v7,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 438 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :cond_2
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 440 */
try { // :try_start_4
(( java.io.ObjectOutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/ObjectOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 443 */
/* .line 441 */
/* :catch_3 */
/* move-exception v3 */
/* .line 442 */
/* .local v3, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v1 );
/* .line 445 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :cond_3
} // :goto_3
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 447 */
try { // :try_start_5
(( java.io.FileOutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 448 */
/* :catch_4 */
/* move-exception v1 */
/* .line 449 */
/* .restart local v1 # "e":Ljava/io/IOException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 453 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_4
} // :goto_4
return;
/* .line 438 */
} // :goto_5
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 440 */
try { // :try_start_6
(( java.io.ObjectOutputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/ObjectOutputStream;->close()V
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_5 */
/* .line 443 */
/* .line 441 */
/* :catch_5 */
/* move-exception v7 */
/* .line 442 */
/* .local v7, "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v1 );
/* .line 445 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :cond_5
} // :goto_6
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 447 */
try { // :try_start_7
(( java.io.FileOutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_6 */
/* .line 450 */
/* .line 448 */
/* :catch_6 */
/* move-exception v1 */
/* .line 449 */
/* .restart local v1 # "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v5,v0 );
/* .line 452 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :cond_6
} // :goto_7
/* throw v3 */
} // .end method
/* # virtual methods */
public void conclude ( ) {
/* .locals 2 */
/* .line 213 */
v0 = this.mLUModel;
(( com.android.server.am.ProcessProphetModel$LaunchUsageModel ) v0 ).conclude ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->conclude()Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;
this.mLUProbTab = v0;
/* .line 214 */
v0 = this.mLUModel;
final String v1 = "launchusage.model"; // const-string v1, "launchusage.model"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V */
/* .line 215 */
v0 = this.mLUProbTab;
final String v1 = "launchusage.prob"; // const-string v1, "launchusage.prob"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V */
/* .line 219 */
v0 = this.mBTUsageModel;
(( com.android.server.am.ProcessProphetModel$BlueToothUsageModel ) v0 ).conclude ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->conclude()Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
this.mBTProbTab = v0;
/* .line 220 */
v0 = this.mBTUsageModel;
final String v1 = "bt.model"; // const-string v1, "bt.model"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V */
/* .line 221 */
v0 = this.mBTProbTab;
final String v1 = "bt.prob"; // const-string v1, "bt.prob"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V */
/* .line 223 */
v0 = this.mEmptyProcPss;
final String v1 = "pss.model"; // const-string v1, "pss.model"
/* invoke-direct {p0, v0, v1}, Lcom/android/server/am/ProcessProphetModel;->saveModelToDisk(Ljava/io/Serializable;Ljava/lang/String;)V */
/* .line 224 */
return;
} // .end method
public void dump ( ) {
/* .locals 9 */
/* .line 240 */
/* new-instance v0, Ljava/text/DecimalFormat; */
final String v1 = "#.####"; // const-string v1, "#.####"
/* invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V */
/* .line 241 */
/* .local v0, "doubleDF":Ljava/text/DecimalFormat; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Probability threshold of LU, BT and CP: ["; // const-string v2, "Probability threshold of LU, BT and CP: ["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v2, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D */
/* .line 242 */
(( java.text.DecimalFormat ) v0 ).format ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v3, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D */
/* .line 243 */
(( java.text.DecimalFormat ) v0 ).format ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v2, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D */
/* .line 244 */
(( java.text.DecimalFormat ) v0 ).format ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "]"; // const-string v2, "]"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 241 */
final String v2 = "ProcessProphetModel"; // const-string v2, "ProcessProphetModel"
android.util.Slog .i ( v2,v1 );
/* .line 247 */
v1 = this.mLUModel;
(( com.android.server.am.ProcessProphetModel$LaunchUsageModel ) v1 ).dump ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->dump()V
/* .line 250 */
v1 = this.mBTUsageModel;
(( com.android.server.am.ProcessProphetModel$BlueToothUsageModel ) v1 ).dump ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->dump()V
/* .line 253 */
v1 = this.mLUProbTab;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 254 */
(( com.android.server.am.ProcessProphetModel$LaunchUsageProbTab ) v1 ).dump ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->dump()V
/* .line 258 */
} // :cond_0
v1 = this.mBTProbTab;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 259 */
final String v1 = "Dumping Bluetooth Prob Tab."; // const-string v1, "Dumping Bluetooth Prob Tab."
android.util.Slog .i ( v2,v1 );
/* .line 260 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = ""; // const-string v3, ""
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mBTProbTab;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v1 );
/* .line 264 */
} // :cond_1
v1 = this.mCopyPatternModel;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v1 ).dump ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->dump()V
/* .line 267 */
final String v1 = "Dumping EmptyProcPss:"; // const-string v1, "Dumping EmptyProcPss:"
android.util.Slog .i ( v2,v1 );
/* .line 268 */
v1 = this.mEmptyProcPss;
(( java.util.HashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/String; */
/* .line 269 */
/* .local v3, "procName":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "\t"; // const-string v5, "\t"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " Pss="; // const-string v5, " Pss="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mEmptyProcPss;
(( java.util.HashMap ) v5 ).get ( v3 ); // invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Long; */
(( java.lang.Long ) v5 ).longValue ( ); // invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x400 */
/* div-long/2addr v5, v7 */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "MB"; // const-string v5, "MB"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v4 );
/* .line 270 */
} // .end local v3 # "procName":Ljava/lang/String;
/* .line 271 */
} // :cond_2
return;
} // .end method
public java.lang.Long getPssInRecord ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .line 227 */
v0 = this.mEmptyProcPss;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
} // .end method
public java.util.ArrayList getWeightedTab ( java.util.HashMap p0 ) {
/* .locals 16 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;)", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 277 */
/* .local p1, "aliveEmptyProcs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ProcessRecord;>;" */
/* move-object/from16 v1, p0 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
/* .line 280 */
/* .local v2, "currentTimeStamp":J */
/* const-wide/high16 v4, 0x3ff0000000000000L # 1.0 */
java.lang.Double .valueOf ( v4,v5 );
/* .line 281 */
/* .local v4, "weightLU":Ljava/lang/Double; */
int v0 = 0; // const/4 v0, 0x0
/* .line 282 */
/* .local v0, "topLUProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;" */
int v5 = 0; // const/4 v5, 0x0
/* .line 283 */
/* .local v5, "curProbTab":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList; */
v6 = this.mLUProbTab;
/* const-wide/high16 v7, 0x4000000000000000L # 2.0 */
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 284 */
(( com.android.server.am.ProcessProphetModel$LaunchUsageProbTab ) v6 ).getCurProbTab ( ); // invoke-virtual {v6}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;->getCurProbTab()Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
/* .line 285 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 286 */
java.util.Calendar .getInstance ( );
v6 = com.android.server.am.ProcessProphetModel .isWeekend ( v6 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 287 */
/* sget-wide v9, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D */
/* div-double/2addr v9, v7 */
(( com.android.server.am.ProcessProphetModel$UseInfoMapList ) v5 ).getProbsGreaterThan ( v9, v10 ); // invoke-virtual {v5, v9, v10}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProbsGreaterThan(D)Ljava/util/ArrayList;
/* .line 288 */
/* sget-boolean v6, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_1
final String v6 = "ProcessProphetModel"; // const-string v6, "ProcessProphetModel"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "topLUProbs\' threshold is " */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v10, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D */
/* div-double/2addr v10, v7 */
(( java.lang.StringBuilder ) v9 ).append ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v9 );
/* .line 290 */
} // :cond_0
/* sget-wide v9, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D */
(( com.android.server.am.ProcessProphetModel$UseInfoMapList ) v5 ).getProbsGreaterThan ( v9, v10 ); // invoke-virtual {v5, v9, v10}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProbsGreaterThan(D)Ljava/util/ArrayList;
/* .line 291 */
/* sget-boolean v6, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_1
final String v6 = "ProcessProphetModel"; // const-string v6, "ProcessProphetModel"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "topLUProbs\' threshold is " */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v10, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D */
(( java.lang.StringBuilder ) v9 ).append ( v10, v11 ); // invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v6,v9 );
/* .line 295 */
} // :cond_1
} // :goto_0
/* move-object v6, v5 */
/* move-object v5, v0 */
/* .line 285 */
} // :cond_2
/* move-object v6, v5 */
/* move-object v5, v0 */
/* .line 283 */
} // :cond_3
/* move-object v6, v5 */
/* move-object v5, v0 */
/* .line 295 */
} // .end local v0 # "topLUProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
/* .local v5, "topLUProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;" */
/* .local v6, "curProbTab":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList; */
} // :goto_1
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
final String v0 = "ProcessProphetModel"; // const-string v0, "ProcessProphetModel"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "topLUProbs: " */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v9 );
/* .line 298 */
} // :cond_4
/* const-wide/16 v9, 0x0 */
java.lang.Double .valueOf ( v9,v10 );
/* .line 299 */
/* .local v0, "weightBT":Ljava/lang/Double; */
int v11 = 0; // const/4 v11, 0x0
/* .line 300 */
/* .local v11, "topBTProbs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;" */
v12 = this.mBTProbTab;
/* const-wide/32 v13, 0x1d4c0 */
if ( v12 != null) { // if-eqz v12, :cond_5
/* iget-wide v9, v1, Lcom/android/server/am/ProcessProphetModel;->mLastBlueToothTime:J */
/* sub-long v9, v2, v9 */
/* cmp-long v9, v9, v13 */
/* if-gtz v9, :cond_5 */
/* .line 302 */
java.lang.Double .valueOf ( v7,v8 );
/* .line 303 */
v9 = this.mBTProbTab;
/* sget-wide v7, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D */
(( com.android.server.am.ProcessProphetModel$UseInfoMapList ) v9 ).getProbsGreaterThan ( v7, v8 ); // invoke-virtual {v9, v7, v8}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->getProbsGreaterThan(D)Ljava/util/ArrayList;
/* move-object v7, v0 */
/* .line 305 */
} // :cond_5
/* move-object v7, v0 */
} // .end local v0 # "weightBT":Ljava/lang/Double;
/* .local v7, "weightBT":Ljava/lang/Double; */
} // :goto_2
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
final String v0 = "ProcessProphetModel"; // const-string v0, "ProcessProphetModel"
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "topBTProbs: " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v8 );
/* .line 308 */
} // :cond_6
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* move-object v8, v0 */
/* .line 309 */
/* .local v8, "allowToStartProcs":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;" */
if ( v5 != null) { // if-eqz v5, :cond_7
/* .line 310 */
(( java.util.ArrayList ) v5 ).iterator ( ); // invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v9 = } // :goto_3
if ( v9 != null) { // if-eqz v9, :cond_7
/* check-cast v9, Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
/* .line 311 */
/* .local v9, "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
v10 = this.pkgName;
(( java.util.HashSet ) v8 ).add ( v10 ); // invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 312 */
} // .end local v9 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
/* .line 314 */
} // :cond_7
if ( v11 != null) { // if-eqz v11, :cond_8
/* .line 315 */
(( java.util.ArrayList ) v11 ).iterator ( ); // invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v9 = } // :goto_4
if ( v9 != null) { // if-eqz v9, :cond_8
/* check-cast v9, Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
/* .line 316 */
/* .restart local v9 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair; */
v10 = this.pkgName;
(( java.util.HashSet ) v8 ).add ( v10 ); // invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 317 */
} // .end local v9 # "p":Lcom/android/server/am/ProcessProphetModel$PkgValuePair;
/* .line 319 */
} // :cond_8
/* const-wide/16 v9, 0x0 */
java.lang.Double .valueOf ( v9,v10 );
/* .line 320 */
/* .local v0, "weightCP":Ljava/lang/Double; */
/* iget-wide v9, v1, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J */
/* sub-long v9, v2, v9 */
/* cmp-long v9, v9, v13 */
/* if-gtz v9, :cond_9 */
v9 = this.mLastMatchedPkg;
if ( v9 != null) { // if-eqz v9, :cond_9
final String v10 = ""; // const-string v10, ""
/* .line 321 */
v9 = (( java.lang.String ) v9 ).equals ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v9, :cond_9 */
/* .line 322 */
/* const-wide/high16 v9, 0x4000000000000000L # 2.0 */
java.lang.Double .valueOf ( v9,v10 );
/* .line 323 */
v9 = this.mLastMatchedPkg;
(( java.util.HashSet ) v8 ).add ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* move-object v9, v0 */
/* .line 327 */
} // :cond_9
/* move-object v9, v0 */
} // .end local v0 # "weightCP":Ljava/lang/Double;
/* .local v9, "weightCP":Ljava/lang/Double; */
} // :goto_5
/* monitor-enter p1 */
/* .line 328 */
try { // :try_start_0
/* invoke-virtual/range {p1 ..p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set; */
v10 = } // :goto_6
if ( v10 != null) { // if-eqz v10, :cond_a
/* check-cast v10, Ljava/lang/String; */
/* .line 329 */
/* .local v10, "procName":Ljava/lang/String; */
(( java.util.HashSet ) v8 ).add ( v10 ); // invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 330 */
/* nop */
} // .end local v10 # "procName":Ljava/lang/String;
/* .line 331 */
} // :cond_a
/* monitor-exit p1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 334 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_b
final String v0 = "ProcessProphetModel"; // const-string v0, "ProcessProphetModel"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "current weight: weightBT="; // const-string v12, "current weight: weightBT="
(( java.lang.StringBuilder ) v10 ).append ( v12 ); // invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v7 ); // invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v12 = ", weightCP="; // const-string v12, ", weightCP="
(( java.lang.StringBuilder ) v10 ).append ( v12 ); // invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v9 ); // invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v10 );
/* .line 335 */
} // :cond_b
/* new-instance v0, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList; */
/* invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V */
/* .line 336 */
/* .local v0, "probMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList; */
(( java.util.HashSet ) v8 ).iterator ( ); // invoke-virtual {v8}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
v12 = } // :goto_7
if ( v12 != null) { // if-eqz v12, :cond_c
/* check-cast v12, Ljava/lang/String; */
/* .line 337 */
/* .local v12, "procName":Ljava/lang/String; */
/* invoke-direct {v1, v12, v4, v7, v9}, Lcom/android/server/am/ProcessProphetModel;->probFusion(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)D */
/* move-result-wide v13 */
/* .line 338 */
/* .local v13, "score":D */
(( com.android.server.am.ProcessProphetModel$UseInfoMapList ) v0 ).update ( v12, v13, v14 ); // invoke-virtual {v0, v12, v13, v14}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V
/* .line 339 */
} // .end local v12 # "procName":Ljava/lang/String;
} // .end local v13 # "score":D
/* .line 341 */
} // :cond_c
com.android.server.am.ProcessProphetModel$UseInfoMapList .-$$Nest$mupdateList ( v0 );
/* .line 342 */
/* .local v10, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;" */
final String v12 = "ProcessProphetModel"; // const-string v12, "ProcessProphetModel"
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "getWeightedTab consumed "; // const-string v14, "getWeightedTab consumed "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 343 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v14 */
/* sub-long/2addr v14, v2 */
(( java.lang.StringBuilder ) v13 ).append ( v14, v15 ); // invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v14 = "ms,"; // const-string v14, "ms,"
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 342 */
android.util.Slog .d ( v12,v13 );
/* .line 345 */
/* .line 331 */
} // .end local v0 # "probMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;
} // .end local v10 # "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit p1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
public java.lang.Object loadModelFromDisk ( java.lang.String p0 ) {
/* .locals 11 */
/* .param p1, "modelName" # Ljava/lang/String; */
/* .line 459 */
final String v0 = "Failed to load "; // const-string v0, "Failed to load "
final String v1 = "error when closing fis."; // const-string v1, "error when closing fis."
final String v2 = "error when closing ois."; // const-string v2, "error when closing ois."
final String v3 = "ProcessProphetModel"; // const-string v3, "ProcessProphetModel"
int v4 = 0; // const/4 v4, 0x0
/* .line 460 */
/* .local v4, "model":Ljava/lang/Object; */
int v5 = 0; // const/4 v5, 0x0
/* .line 461 */
/* .local v5, "fileInput":Ljava/io/FileInputStream; */
int v6 = 0; // const/4 v6, 0x0
/* .line 463 */
/* .local v6, "objInput":Ljava/io/ObjectInputStream; */
try { // :try_start_0
/* new-instance v7, Ljava/io/FileInputStream; */
/* new-instance v8, Ljava/io/File; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "/data/system/procprophet/"; // const-string v10, "/data/system/procprophet/"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( p1 ); // invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v7, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v5, v7 */
/* .line 464 */
/* new-instance v7, Ljava/io/ObjectInputStream; */
/* invoke-direct {v7, v5}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V */
/* move-object v6, v7 */
/* .line 465 */
(( java.io.ObjectInputStream ) v6 ).readObject ( ); // invoke-virtual {v6}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/io/FileNotFoundException; {:try_start_0 ..:try_end_0} :catch_5 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v4, v0 */
/* .line 471 */
/* nop */
/* .line 473 */
try { // :try_start_1
(( java.io.ObjectInputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 476 */
/* .line 474 */
/* :catch_0 */
/* move-exception v0 */
/* .line 475 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v2 );
/* .line 478 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
/* nop */
/* .line 480 */
try { // :try_start_2
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .line 483 */
} // :goto_1
/* goto/16 :goto_5 */
/* .line 481 */
/* :catch_1 */
/* move-exception v0 */
/* .line 482 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
} // :goto_2
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v1 );
} // .end local v0 # "e":Ljava/io/IOException;
/* .line 471 */
/* :catchall_0 */
/* move-exception v0 */
/* goto/16 :goto_6 */
/* .line 468 */
/* :catch_2 */
/* move-exception v7 */
/* .line 469 */
/* .local v7, "e":Ljava/lang/Exception; */
try { // :try_start_3
/* sget-boolean v8, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_0
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "."; // const-string v8, "."
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v0 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 471 */
} // .end local v7 # "e":Ljava/lang/Exception;
} // :cond_0
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 473 */
try { // :try_start_4
(( java.io.ObjectInputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 476 */
/* .line 474 */
/* :catch_3 */
/* move-exception v0 */
/* .line 475 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v2 );
/* .line 478 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_1
} // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 480 */
try { // :try_start_5
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_5 */
/* .catch Ljava/io/IOException; {:try_start_5 ..:try_end_5} :catch_4 */
/* .line 481 */
/* :catch_4 */
/* move-exception v0 */
/* .line 482 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 466 */
} // .end local v0 # "e":Ljava/io/IOException;
/* :catch_5 */
/* move-exception v7 */
/* .line 467 */
/* .local v7, "e":Ljava/io/FileNotFoundException; */
try { // :try_start_6
/* sget-boolean v8, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_2
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ".Not Found."; // const-string v8, ".Not Found."
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v0 );
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_0 */
/* .line 471 */
} // .end local v7 # "e":Ljava/io/FileNotFoundException;
} // :cond_2
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 473 */
try { // :try_start_7
(( java.io.ObjectInputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
/* :try_end_7 */
/* .catch Ljava/io/IOException; {:try_start_7 ..:try_end_7} :catch_6 */
/* .line 476 */
/* .line 474 */
/* :catch_6 */
/* move-exception v0 */
/* .line 475 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v2 );
/* .line 478 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_3
} // :goto_4
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 480 */
try { // :try_start_8
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_7 */
/* goto/16 :goto_1 */
/* .line 481 */
/* :catch_7 */
/* move-exception v0 */
/* .line 482 */
/* .restart local v0 # "e":Ljava/io/IOException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* goto/16 :goto_2 */
/* .line 486 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_4
} // :goto_5
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 487 */
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Load "; // const-string v1, "Load "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " done."; // const-string v1, " done."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v0 );
/* .line 489 */
} // :cond_5
/* sget-boolean v0, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Loaded: "; // const-string v1, "Loaded: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v0 );
/* .line 490 */
} // :cond_6
/* .line 471 */
} // :goto_6
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 473 */
try { // :try_start_9
(( java.io.ObjectInputStream ) v6 ).close ( ); // invoke-virtual {v6}, Ljava/io/ObjectInputStream;->close()V
/* :try_end_9 */
/* .catch Ljava/io/IOException; {:try_start_9 ..:try_end_9} :catch_8 */
/* .line 476 */
/* .line 474 */
/* :catch_8 */
/* move-exception v7 */
/* .line 475 */
/* .local v7, "e":Ljava/io/IOException; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v2 );
/* .line 478 */
} // .end local v7 # "e":Ljava/io/IOException;
} // :cond_7
} // :goto_7
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 480 */
try { // :try_start_a
(( java.io.FileInputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
/* :try_end_a */
/* .catch Ljava/io/IOException; {:try_start_a ..:try_end_a} :catch_9 */
/* .line 483 */
/* .line 481 */
/* :catch_9 */
/* move-exception v2 */
/* .line 482 */
/* .local v2, "e":Ljava/io/IOException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v1 );
/* .line 485 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_8
} // :goto_8
/* throw v0 */
} // .end method
public void notifyBTConnected ( ) {
/* .locals 2 */
/* .line 173 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastBlueToothTime:J */
/* .line 174 */
v0 = this.mBTUsageModel;
(( com.android.server.am.ProcessProphetModel$BlueToothUsageModel ) v0 ).notifyBTConnected ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->notifyBTConnected()V
/* .line 175 */
return;
} // .end method
public void reset ( ) {
/* .locals 1 */
/* .line 498 */
/* new-instance v0, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel; */
/* invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;-><init>()V */
this.mLUModel = v0;
/* .line 499 */
/* new-instance v0, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel; */
/* invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;-><init>()V */
this.mBTUsageModel = v0;
/* .line 500 */
int v0 = 0; // const/4 v0, 0x0
this.mLUProbTab = v0;
/* .line 501 */
this.mBTProbTab = v0;
/* .line 502 */
/* new-instance v0, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel; */
/* invoke-direct {v0}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;-><init>()V */
this.mCopyPatternModel = v0;
/* .line 503 */
return;
} // .end method
public java.util.ArrayList sortEmptyProcs ( java.util.HashMap p0 ) {
/* .locals 12 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/ProcessRecord;", */
/* ">;)", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/android/server/am/ProcessProphetModel$PkgValuePair;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 352 */
/* .local p1, "aliveEmptyProcs":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/ProcessRecord;>;" */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 355 */
/* .local v0, "currentTimeStamp":J */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
java.lang.Double .valueOf ( v2,v3 );
/* .line 356 */
/* .local v2, "weightLU":Ljava/lang/Double; */
/* const-wide/16 v3, 0x0 */
java.lang.Double .valueOf ( v3,v4 );
/* .line 357 */
/* .local v5, "weightBT":Ljava/lang/Double; */
java.lang.Double .valueOf ( v3,v4 );
/* .line 358 */
/* .local v3, "weightCP":Ljava/lang/Double; */
/* iget-wide v6, p0, Lcom/android/server/am/ProcessProphetModel;->mLastBlueToothTime:J */
/* sub-long v6, v0, v6 */
/* const-wide/32 v8, 0x1d4c0 */
/* cmp-long v4, v6, v8 */
/* const-wide/high16 v6, 0x4000000000000000L # 2.0 */
/* if-gtz v4, :cond_0 */
/* .line 359 */
java.lang.Double .valueOf ( v6,v7 );
/* .line 361 */
} // :cond_0
/* iget-wide v10, p0, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J */
/* sub-long v10, v0, v10 */
/* cmp-long v4, v10, v8 */
/* if-gtz v4, :cond_1 */
/* .line 362 */
java.lang.Double .valueOf ( v6,v7 );
/* .line 364 */
} // :cond_1
/* sget-boolean v4, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
final String v4 = "ProcessProphetModel"; // const-string v4, "ProcessProphetModel"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "current weight: weightBT="; // const-string v7, "current weight: weightBT="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = ", weightCP="; // const-string v7, ", weightCP="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v6 );
/* .line 367 */
} // :cond_2
/* new-instance v4, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList; */
/* invoke-direct {v4}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;-><init>()V */
/* .line 368 */
/* .local v4, "probMapList":Lcom/android/server/am/ProcessProphetModel$UseInfoMapList; */
/* monitor-enter p1 */
/* .line 369 */
try { // :try_start_0
(( java.util.HashMap ) p1 ).values ( ); // invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_3
/* check-cast v7, Lcom/android/server/am/ProcessRecord; */
/* .line 370 */
/* .local v7, "app":Lcom/android/server/am/ProcessRecord; */
v8 = this.processName;
/* .line 371 */
/* .local v8, "procName":Ljava/lang/String; */
/* invoke-direct {p0, v8, v2, v5, v3}, Lcom/android/server/am/ProcessProphetModel;->probFusion(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;)D */
/* move-result-wide v9 */
/* .line 372 */
/* .local v9, "score":D */
(( com.android.server.am.ProcessProphetModel$UseInfoMapList ) v4 ).update ( v8, v9, v10 ); // invoke-virtual {v4, v8, v9, v10}, Lcom/android/server/am/ProcessProphetModel$UseInfoMapList;->update(Ljava/lang/String;D)V
/* .line 373 */
} // .end local v7 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v8 # "procName":Ljava/lang/String;
} // .end local v9 # "score":D
/* .line 374 */
} // :cond_3
/* monitor-exit p1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 376 */
com.android.server.am.ProcessProphetModel$UseInfoMapList .-$$Nest$mupdateList ( v4 );
/* .line 377 */
/* .local v6, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;" */
final String v7 = "ProcessProphetModel"; // const-string v7, "ProcessProphetModel"
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "sortEmptyProcs consumed " */
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 378 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v9 */
/* sub-long/2addr v9, v0 */
(( java.lang.StringBuilder ) v8 ).append ( v9, v10 ); // invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v9 = "ms,"; // const-string v9, "ms,"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 377 */
android.util.Slog .d ( v7,v8 );
/* .line 380 */
/* .line 374 */
} // .end local v6 # "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessProphetModel$PkgValuePair;>;"
/* :catchall_0 */
/* move-exception v6 */
try { // :try_start_1
/* monitor-exit p1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v6 */
} // .end method
public void testCP ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "targetProcName" # Ljava/lang/String; */
/* .line 509 */
final String v0 = "ProcessProphetModel"; // const-string v0, "ProcessProphetModel"
if ( p1 != null) { // if-eqz p1, :cond_2
final String v1 = ""; // const-string v1, ""
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 513 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "enter CP test mode, target proc name: "; // const-string v2, "enter CP test mode, target proc name: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 516 */
(( com.android.server.am.ProcessProphetModel ) p0 ).reset ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetModel;->reset()V
/* .line 519 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
int v1 = 4; // const/4 v1, 0x4
/* if-ge v0, v1, :cond_1 */
/* .line 520 */
v1 = this.mCopyPatternModel;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v1 ).updateMatchEvent ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchEvent(Ljava/lang/String;)V
/* .line 521 */
v1 = this.mCopyPatternModel;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v1 ).updateMatchLaunchEvent ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchLaunchEvent(Ljava/lang/String;)V
/* .line 519 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 523 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mCopyPatternModel;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v0 ).updateMatchEvent ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchEvent(Ljava/lang/String;)V
/* .line 525 */
(( com.android.server.am.ProcessProphetModel ) p0 ).conclude ( ); // invoke-virtual {p0}, Lcom/android/server/am/ProcessProphetModel;->conclude()V
/* .line 526 */
return;
/* .line 510 */
} // :cond_2
} // :goto_1
/* const-string/jumbo v1, "target proc name error!" */
android.util.Slog .d ( v0,v1 );
/* .line 511 */
return;
} // .end method
public void testMTBF ( ) {
/* .locals 2 */
/* .line 532 */
/* const-wide/16 v0, 0x0 */
/* sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D */
/* .line 533 */
/* sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D */
/* .line 534 */
/* sput-wide v0, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D */
/* .line 535 */
return;
} // .end method
public void updateBTAudioEvent ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 181 */
v0 = this.mBTUsageModel;
(( com.android.server.am.ProcessProphetModel$BlueToothUsageModel ) v0 ).update ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->update(Ljava/lang/String;)V
/* .line 182 */
return;
} // .end method
public java.lang.String updateCopyEvent ( java.lang.CharSequence p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "text" # Ljava/lang/CharSequence; */
/* .param p2, "curTopProcName" # Ljava/lang/String; */
/* .line 189 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J */
/* .line 191 */
v0 = this.mCopyPatternModel;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v0 ).match ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->match(Ljava/lang/CharSequence;)Ljava/lang/String;
this.mLastMatchedPkg = v0;
/* .line 192 */
v1 = this.mCopyPatternModel;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v1 ).getProb ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->getProb(Ljava/lang/String;)D
/* move-result-wide v0 */
/* .line 193 */
/* .local v0, "prob":D */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "match: "; // const-string v3, "match: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLastMatchedPkg;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", prob: "; // const-string v3, ", prob: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v3 = ", curTop: "; // const-string v3, ", curTop: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ProcessProphetModel"; // const-string v3, "ProcessProphetModel"
android.util.Slog .i ( v3,v2 );
/* .line 198 */
v2 = this.mLastMatchedPkg;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 199 */
v2 = this.mCopyPatternModel;
v3 = this.mLastMatchedPkg;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v2 ).updateMatchEvent ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchEvent(Ljava/lang/String;)V
/* .line 200 */
/* sget-wide v2, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D */
/* cmpl-double v2, v0, v2 */
/* if-lez v2, :cond_0 */
/* .line 201 */
v2 = this.mLastMatchedPkg;
/* .line 205 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
public void updateLaunchEvent ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 157 */
if ( p1 != null) { // if-eqz p1, :cond_3
final String v0 = ""; // const-string v0, ""
v1 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 161 */
} // :cond_0
v1 = this.mLUModel;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
v4 = this.mLUProbTab;
(( com.android.server.am.ProcessProphetModel$LaunchUsageModel ) v1 ).update ( p1, v2, v3, v4 ); // invoke-virtual {v1, p1, v2, v3, v4}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->update(Ljava/lang/String;JLcom/android/server/am/ProcessProphetModel$LaunchUsageProbTab;)V
/* .line 164 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* iget-wide v3, p0, Lcom/android/server/am/ProcessProphetModel;->mLastCopyedTime:J */
/* sub-long/2addr v1, v3 */
/* const-wide/32 v3, 0x1d4c0 */
/* cmp-long v1, v1, v3 */
/* if-gez v1, :cond_2 */
v1 = this.mLastMatchedPkg;
/* .line 165 */
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 166 */
/* sget-boolean v1, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "matchLaunch: "; // const-string v2, "matchLaunch: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mLastMatchedPkg;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "ProcessProphetModel"; // const-string v2, "ProcessProphetModel"
android.util.Slog .i ( v2,v1 );
/* .line 167 */
} // :cond_1
v1 = this.mCopyPatternModel;
v2 = this.mLastMatchedPkg;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v1 ).updateMatchLaunchEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->updateMatchLaunchEvent(Ljava/lang/String;)V
/* .line 168 */
this.mLastMatchedPkg = v0;
/* .line 170 */
} // :cond_2
return;
/* .line 158 */
} // :cond_3
} // :goto_0
return;
} // .end method
public updateModelSizeTrack ( ) {
/* .locals 4 */
/* .line 143 */
int v0 = 6; // const/4 v0, 0x6
/* new-array v0, v0, [J */
v1 = this.mLUModel;
(( com.android.server.am.ProcessProphetModel$LaunchUsageModel ) v1 ).getAllDays ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->getAllDays()J
/* move-result-wide v1 */
int v3 = 0; // const/4 v3, 0x0
/* aput-wide v1, v0, v3 */
v1 = this.mLUModel;
(( com.android.server.am.ProcessProphetModel$LaunchUsageModel ) v1 ).getAllLUUpdateTimes ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->getAllLUUpdateTimes()J
/* move-result-wide v1 */
int v3 = 1; // const/4 v3, 0x1
/* aput-wide v1, v0, v3 */
v1 = this.mBTUsageModel;
/* .line 144 */
(( com.android.server.am.ProcessProphetModel$BlueToothUsageModel ) v1 ).getBTConnectedTimes ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->getBTConnectedTimes()J
/* move-result-wide v1 */
int v3 = 2; // const/4 v3, 0x2
/* aput-wide v1, v0, v3 */
v1 = this.mBTUsageModel;
(( com.android.server.am.ProcessProphetModel$BlueToothUsageModel ) v1 ).getAllBTUpdateTimes ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$BlueToothUsageModel;->getAllBTUpdateTimes()J
/* move-result-wide v1 */
int v3 = 3; // const/4 v3, 0x3
/* aput-wide v1, v0, v3 */
v1 = this.mCopyPatternModel;
/* .line 145 */
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v1 ).getAllCopyTimes ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->getAllCopyTimes()J
/* move-result-wide v1 */
int v3 = 4; // const/4 v3, 0x4
/* aput-wide v1, v0, v3 */
v1 = this.mCopyPatternModel;
(( com.android.server.am.ProcessProphetModel$CopyPatternModel ) v1 ).getCopyLaunchedTimes ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessProphetModel$CopyPatternModel;->getCopyLaunchedTimes()J
/* move-result-wide v1 */
int v3 = 5; // const/4 v3, 0x5
/* aput-wide v1, v0, v3 */
/* .line 143 */
} // .end method
public void updateModelThreshold ( java.lang.String[] p0 ) {
/* .locals 7 */
/* .param p1, "newThresList" # [Ljava/lang/String; */
/* .line 132 */
int v0 = 0; // const/4 v0, 0x0
/* aget-object v1, p1, v0 */
java.lang.Double .parseDouble ( v1 );
/* move-result-wide v1 */
/* const-wide/high16 v3, 0x4059000000000000L # 100.0 */
/* div-double/2addr v1, v3 */
/* sput-wide v1, Lcom/android/server/am/ProcessProphetModel;->LU_PROB_THRESHOLD:D */
/* .line 133 */
int v1 = 1; // const/4 v1, 0x1
/* aget-object v2, p1, v1 */
java.lang.Double .parseDouble ( v2 );
/* move-result-wide v5 */
/* div-double/2addr v5, v3 */
/* sput-wide v5, Lcom/android/server/am/ProcessProphetModel;->BT_PROB_THRESHOLD:D */
/* .line 134 */
int v2 = 2; // const/4 v2, 0x2
/* aget-object v5, p1, v2 */
java.lang.Double .parseDouble ( v5 );
/* move-result-wide v5 */
/* div-double/2addr v5, v3 */
/* sput-wide v5, Lcom/android/server/am/ProcessProphetModel;->CP_PROB_THRESHOLD:D */
/* .line 135 */
/* sget-boolean v3, Lcom/android/server/am/ProcessProphetModel;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 136 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "PROB_THRESHOLD change to "; // const-string v4, "PROB_THRESHOLD change to "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v0, p1, v0 */
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v1, p1, v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-object v1, p1, v2 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " complete."; // const-string v1, " complete."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "ProcessProphetModel"; // const-string v1, "ProcessProphetModel"
android.util.Slog .i ( v1,v0 );
/* .line 139 */
} // :cond_0
return;
} // .end method
public void updatePssInRecord ( java.lang.String p0, java.lang.Long p1 ) {
/* .locals 6 */
/* .param p1, "procName" # Ljava/lang/String; */
/* .param p2, "pss" # Ljava/lang/Long; */
/* .line 231 */
v0 = this.mEmptyProcPss;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
/* .line 232 */
/* .local v0, "rawPss":Ljava/lang/Long; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 233 */
v1 = this.mEmptyProcPss;
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
(( java.lang.Long ) p2 ).longValue ( ); // invoke-virtual {p2}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* add-long/2addr v2, v4 */
/* const-wide/16 v4, 0x2 */
/* div-long/2addr v2, v4 */
java.lang.Long .valueOf ( v2,v3 );
(( java.util.HashMap ) v1 ).put ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 235 */
} // :cond_0
v1 = this.mEmptyProcPss;
(( java.util.HashMap ) v1 ).put ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 237 */
} // :goto_0
return;
} // .end method
public java.util.ArrayList uploadModelPredProb ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Double;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 150 */
v0 = this.mLUModel;
(( com.android.server.am.ProcessProphetModel$LaunchUsageModel ) v0 ).getTopModelPredict ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessProphetModel$LaunchUsageModel;->getTopModelPredict()Ljava/util/ArrayList;
} // .end method
