.class public Lcom/android/server/am/MemoryControlJobService;
.super Landroid/app/job/JobService;
.source "MemoryControlJobService.java"


# static fields
.field private static final DETECTION_JOB_ID:I = 0x13c05

.field private static final KILL_JOB_ID:I = 0x13c06

.field private static PERIODIC_DETECTION_TIME:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "MemoryStandardProcessControl"

.field private static mspc:Lcom/android/server/am/MemoryStandardProcessControl;

.field private static sContext:Landroid/content/Context;


# instance fields
.field private final SCREEN_STATE_OFF:I

.field public mFinishCallback:Ljava/lang/Runnable;

.field private mJobParams:Landroid/app/job/JobParameters;

.field private mStarted:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmJobParams(Lcom/android/server/am/MemoryControlJobService;)Landroid/app/job/JobParameters;
    .locals 0

    iget-object p0, p0, Lcom/android/server/am/MemoryControlJobService;->mJobParams:Landroid/app/job/JobParameters;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStarted(Lcom/android/server/am/MemoryControlJobService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/am/MemoryControlJobService;->mStarted:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmStarted(Lcom/android/server/am/MemoryControlJobService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/am/MemoryControlJobService;->mStarted:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetsContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/android/server/am/MemoryControlJobService;->sContext:Landroid/content/Context;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 22
    const-wide/32 v0, 0x124f80

    sput-wide v0, Lcom/android/server/am/MemoryControlJobService;->PERIODIC_DETECTION_TIME:J

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    .line 24
    sput-object v0, Lcom/android/server/am/MemoryControlJobService;->sContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 18
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    .line 25
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/am/MemoryControlJobService;->SCREEN_STATE_OFF:I

    .line 29
    new-instance v0, Lcom/android/server/am/MemoryControlJobService$1;

    invoke-direct {v0, p0}, Lcom/android/server/am/MemoryControlJobService$1;-><init>(Lcom/android/server/am/MemoryControlJobService;)V

    iput-object v0, p0, Lcom/android/server/am/MemoryControlJobService;->mFinishCallback:Ljava/lang/Runnable;

    return-void
.end method

.method public static detectionSchedule(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .line 46
    sget-object v0, Lcom/android/server/am/MemoryControlJobService;->sContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 47
    sput-object p0, Lcom/android/server/am/MemoryControlJobService;->sContext:Landroid/content/Context;

    .line 49
    :cond_0
    const-string v0, "MemoryStandardProcessControl"

    const-string v1, "detectionSchedule is called"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/android/server/am/MemoryControlJobService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android"

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .local v0, "serviceName":Landroid/content/ComponentName;
    const-string v1, "jobscheduler"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/job/JobScheduler;

    .line 52
    .local v1, "js":Landroid/app/job/JobScheduler;
    new-instance v2, Landroid/app/job/JobInfo$Builder;

    const v3, 0x13c05

    invoke-direct {v2, v3, v0}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 53
    .local v2, "detectionBuilder":Landroid/app/job/JobInfo$Builder;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;

    .line 54
    sget-wide v3, Lcom/android/server/am/MemoryControlJobService;->PERIODIC_DETECTION_TIME:J

    invoke-virtual {v2, v3, v4}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    .line 55
    invoke-virtual {v2}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 56
    return-void
.end method

.method public static killSchedule(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .line 59
    sget-object v0, Lcom/android/server/am/MemoryControlJobService;->sContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 60
    sput-object p0, Lcom/android/server/am/MemoryControlJobService;->sContext:Landroid/content/Context;

    .line 62
    :cond_0
    const-string v0, "MemoryStandardProcessControl"

    const-string v1, "killSchedule is called"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/android/server/am/MemoryControlJobService;->offsetFromTodayMidnight(II)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 64
    .local v2, "tomorrow3AM":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v2, v4

    .line 65
    .local v4, "nextScheduleTime":J
    new-instance v0, Landroid/content/ComponentName;

    const-class v6, Lcom/android/server/am/MemoryControlJobService;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "android"

    invoke-direct {v0, v7, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .local v0, "serviceName":Landroid/content/ComponentName;
    const-string v6, "jobscheduler"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/job/JobScheduler;

    .line 67
    .local v6, "js":Landroid/app/job/JobScheduler;
    new-instance v7, Landroid/app/job/JobInfo$Builder;

    const v8, 0x13c06

    invoke-direct {v7, v8, v0}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 68
    .local v7, "killBuilder":Landroid/app/job/JobInfo$Builder;
    invoke-virtual {v7, v1}, Landroid/app/job/JobInfo$Builder;->setRequiresBatteryNotLow(Z)Landroid/app/job/JobInfo$Builder;

    .line 69
    invoke-virtual {v7, v4, v5}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    .line 70
    invoke-virtual {v7}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 71
    return-void
.end method

.method private static offsetFromTodayMidnight(II)Ljava/util/Calendar;
    .locals 3
    .param p0, "nDays"    # I
    .param p1, "nHours"    # I

    .line 118
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 119
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 120
    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 121
    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 122
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 123
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 124
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p0}, Ljava/util/Calendar;->add(II)V

    .line 125
    return-object v0
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 6
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 75
    sget-object v0, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    if-nez v0, :cond_0

    .line 76
    invoke-static {}, Lcom/android/server/am/MemoryStandardProcessControlStub;->getInstance()Lcom/android/server/am/MemoryStandardProcessControlStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl;

    sput-object v0, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    .line 79
    :cond_0
    sget-object v0, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget v0, v0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I

    .line 80
    .local v0, "screenState":I
    sget-object v1, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-virtual {v1}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z

    move-result v1

    .line 81
    .local v1, "enable":Z
    const/4 v2, 0x0

    if-eqz v1, :cond_4

    .line 82
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v3

    const v4, 0x13c05

    const/4 v5, 0x2

    if-ne v3, v4, :cond_2

    .line 83
    if-eq v0, v5, :cond_1

    .line 85
    sget-object v3, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-virtual {v3}, Lcom/android/server/am/MemoryStandardProcessControl;->callPeriodicDetection()V

    .line 87
    :cond_1
    return v2

    .line 88
    :cond_2
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v3

    const v4, 0x13c06

    if-ne v3, v4, :cond_4

    .line 89
    const/4 v2, 0x1

    if-ne v0, v5, :cond_3

    .line 91
    sget-object v3, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    invoke-virtual {v3, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->callKillProcess(Z)V

    .line 93
    :cond_3
    iput-object p1, p0, Lcom/android/server/am/MemoryControlJobService;->mJobParams:Landroid/app/job/JobParameters;

    .line 94
    iget-object v3, p0, Lcom/android/server/am/MemoryControlJobService;->mFinishCallback:Ljava/lang/Runnable;

    monitor-enter v3

    .line 95
    :try_start_0
    iput-boolean v2, p0, Lcom/android/server/am/MemoryControlJobService;->mStarted:Z

    .line 96
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    sget-object v3, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v4, p0, Lcom/android/server/am/MemoryControlJobService;->mFinishCallback:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl;->callRunnable(Ljava/lang/Runnable;)V

    .line 98
    return v2

    .line 96
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 101
    :cond_4
    return v2
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 4
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .line 106
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v0

    .line 107
    .local v0, "jobId":I
    const v1, 0x13c06

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 108
    iput-boolean v2, p0, Lcom/android/server/am/MemoryControlJobService;->mStarted:Z

    .line 109
    sget-object v1, Lcom/android/server/am/MemoryControlJobService;->mspc:Lcom/android/server/am/MemoryStandardProcessControl;

    iget-object v3, p0, Lcom/android/server/am/MemoryControlJobService;->mFinishCallback:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->callRunnable(Ljava/lang/Runnable;)V

    .line 111
    :cond_0
    sget-boolean v1, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStopJob, jobId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MemoryStandardProcessControl"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_1
    return v2
.end method
