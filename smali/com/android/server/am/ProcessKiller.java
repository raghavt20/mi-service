public class com.android.server.am.ProcessKiller {
	 /* .source "ProcessKiller.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.android.server.am.ActivityManagerService mActivityManagerService;
	 /* # direct methods */
	 public com.android.server.am.ProcessKiller ( ) {
		 /* .locals 0 */
		 /* .param p1, "ams" # Lcom/android/server/am/ActivityManagerService; */
		 /* .line 21 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 22 */
		 this.mActivityManagerService = p1;
		 /* .line 23 */
		 return;
	 } // .end method
	 private void forceStopPackage ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
		 /* .locals 1 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .param p2, "userId" # I */
		 /* .param p3, "reason" # Ljava/lang/String; */
		 /* .line 79 */
		 /* const-class v0, Landroid/app/ActivityManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Landroid/app/ActivityManagerInternal; */
		 /* .line 80 */
		 (( android.app.ActivityManagerInternal ) v0 ).forceStopPackage ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Landroid/app/ActivityManagerInternal;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
		 /* .line 81 */
		 return;
	 } // .end method
	 private Boolean isInterestingToUser ( com.android.server.am.ProcessRecord p0 ) {
		 /* .locals 1 */
		 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
		 /* .line 26 */
		 (( com.android.server.am.ProcessRecord ) p1 ).getWindowProcessController ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getWindowProcessController()Lcom/android/server/wm/WindowProcessController;
		 v0 = 		 (( com.android.server.wm.WindowProcessController ) v0 ).isInterestingToUser ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowProcessController;->isInterestingToUser()Z
	 } // .end method
	 private void killLocked ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
		 /* .locals 2 */
		 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
		 /* .param p2, "reason" # Ljava/lang/String; */
		 /* .line 73 */
		 if ( p1 != null) { // if-eqz p1, :cond_0
			 /* .line 74 */
			 /* const/16 v0, 0xd */
			 int v1 = 1; // const/4 v1, 0x1
			 (( com.android.server.am.ProcessRecord ) p1 ).killLocked ( p2, v0, v1 ); // invoke-virtual {p1, p2, v0, v1}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V
			 /* .line 76 */
		 } // :cond_0
		 return;
	 } // .end method
	 public static Boolean scheduleCrashApp ( com.android.server.am.ActivityManagerService p0, com.android.server.am.ProcessRecord p1, java.lang.String p2 ) {
		 /* .locals 3 */
		 /* .param p0, "mService" # Lcom/android/server/am/ActivityManagerService; */
		 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
		 /* .param p2, "reason" # Ljava/lang/String; */
		 /* .line 84 */
		 int v0 = 0; // const/4 v0, 0x0
		 if ( p1 != null) { // if-eqz p1, :cond_0
			 /* .line 85 */
			 (( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
			 /* .line 86 */
			 /* .local v1, "thread":Landroid/app/IApplicationThread; */
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 87 */
				 /* monitor-enter p0 */
				 /* .line 89 */
				 int v2 = 0; // const/4 v2, 0x0
				 try { // :try_start_0
					 (( com.android.server.am.ProcessRecord ) p1 ).scheduleCrashLocked ( p2, v0, v2 ); // invoke-virtual {p1, p2, v0, v2}, Lcom/android/server/am/ProcessRecord;->scheduleCrashLocked(Ljava/lang/String;ILandroid/os/Bundle;)V
					 /* .line 90 */
					 /* monitor-exit p0 */
					 int v0 = 1; // const/4 v0, 0x1
					 /* .line 91 */
					 /* :catchall_0 */
					 /* move-exception v0 */
					 /* monitor-exit p0 */
					 /* :try_end_0 */
					 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
					 /* throw v0 */
					 /* .line 94 */
				 } // .end local v1 # "thread":Landroid/app/IApplicationThread;
			 } // :cond_0
		 } // .end method
		 private void scheduleTrimMemory ( com.android.server.am.ProcessRecord p0, Integer p1 ) {
			 /* .locals 1 */
			 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
			 /* .param p2, "level" # I */
			 /* .line 63 */
			 if ( p1 != null) { // if-eqz p1, :cond_0
				 (( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* .line 65 */
					 try { // :try_start_0
						 (( com.android.server.am.ProcessRecord ) p1 ).getThread ( ); // invoke-virtual {p1}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
						 /* :try_end_0 */
						 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
						 /* .line 68 */
						 /* .line 66 */
						 /* :catch_0 */
						 /* move-exception v0 */
						 /* .line 67 */
						 /* .local v0, "e":Landroid/os/RemoteException; */
						 (( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
						 /* .line 70 */
					 } // .end local v0 # "e":Landroid/os/RemoteException;
				 } // :cond_0
			 } // :goto_0
			 return;
		 } // .end method
		 /* # virtual methods */
		 public void forceStopPackage ( com.android.server.am.ProcessRecord p0, java.lang.String p1, Boolean p2 ) {
			 /* .locals 2 */
			 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
			 /* .param p2, "reason" # Ljava/lang/String; */
			 /* .param p3, "evenForeground" # Z */
			 /* .line 30 */
			 /* if-nez p3, :cond_0 */
			 v0 = 			 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessKiller;->isInterestingToUser(Lcom/android/server/am/ProcessRecord;)Z */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 31 */
				 return;
				 /* .line 33 */
			 } // :cond_0
			 v0 = this.info;
			 v0 = this.packageName;
			 /* iget v1, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
			 /* invoke-direct {p0, v0, v1, p2}, Lcom/android/server/am/ProcessKiller;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V */
			 /* .line 34 */
			 return;
		 } // .end method
		 public Boolean killApplication ( com.android.server.am.ProcessRecord p0, java.lang.String p1, Boolean p2 ) {
			 /* .locals 2 */
			 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
			 /* .param p2, "reason" # Ljava/lang/String; */
			 /* .param p3, "evenForeground" # Z */
			 /* .line 37 */
			 v0 = this.mActivityManagerService;
			 /* monitor-enter v0 */
			 /* .line 38 */
			 /* if-nez p3, :cond_0 */
			 try { // :try_start_0
				 v1 = 				 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessKiller;->isInterestingToUser(Lcom/android/server/am/ProcessRecord;)Z */
				 if ( v1 != null) { // if-eqz v1, :cond_0
					 /* .line 39 */
					 /* monitor-exit v0 */
					 int v0 = 0; // const/4 v0, 0x0
					 /* .line 41 */
				 } // :cond_0
				 /* invoke-direct {p0, p1, p2}, Lcom/android/server/am/ProcessKiller;->killLocked(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)V */
				 /* .line 42 */
				 /* monitor-exit v0 */
				 int v0 = 1; // const/4 v0, 0x1
				 /* .line 43 */
				 /* :catchall_0 */
				 /* move-exception v1 */
				 /* monitor-exit v0 */
				 /* :try_end_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* throw v1 */
			 } // .end method
			 public void killBackgroundApplication ( com.android.server.am.ProcessRecord p0, java.lang.String p1 ) {
				 /* .locals 3 */
				 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
				 /* .param p2, "reason" # Ljava/lang/String; */
				 /* .line 47 */
				 v0 = this.mActivityManagerService;
				 v1 = this.info;
				 v1 = this.packageName;
				 /* iget v2, p1, Lcom/android/server/am/ProcessRecord;->userId:I */
				 (( com.android.server.am.ActivityManagerService ) v0 ).killBackgroundProcesses ( v1, v2, p2 ); // invoke-virtual {v0, v1, v2, p2}, Lcom/android/server/am/ActivityManagerService;->killBackgroundProcesses(Ljava/lang/String;ILjava/lang/String;)V
				 /* .line 48 */
				 return;
			 } // .end method
			 public void trimMemory ( com.android.server.am.ProcessRecord p0, Boolean p1 ) {
				 /* .locals 2 */
				 /* .param p1, "app" # Lcom/android/server/am/ProcessRecord; */
				 /* .param p2, "evenForeground" # Z */
				 /* .line 51 */
				 /* if-nez p2, :cond_0 */
				 v0 = 				 /* invoke-direct {p0, p1}, Lcom/android/server/am/ProcessKiller;->isInterestingToUser(Lcom/android/server/am/ProcessRecord;)Z */
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* .line 52 */
					 return;
					 /* .line 55 */
				 } // :cond_0
				 v0 = this.info;
				 v0 = this.packageName;
				 final String v1 = "android"; // const-string v1, "android"
				 v0 = 				 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_1
					 /* .line 56 */
					 /* const/16 v0, 0x3c */
					 /* invoke-direct {p0, p1, v0}, Lcom/android/server/am/ProcessKiller;->scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V */
					 /* .line 58 */
				 } // :cond_1
				 /* const/16 v0, 0x50 */
				 /* invoke-direct {p0, p1, v0}, Lcom/android/server/am/ProcessKiller;->scheduleTrimMemory(Lcom/android/server/am/ProcessRecord;I)V */
				 /* .line 60 */
			 } // :goto_0
			 return;
		 } // .end method
