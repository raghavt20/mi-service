.class public final Lcom/android/server/am/MiuiMemoryService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiMemoryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/am/MiuiMemoryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/android/server/am/MiuiMemoryService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 195
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 196
    new-instance v0, Lcom/android/server/am/MiuiMemoryService;

    invoke-direct {v0, p1}, Lcom/android/server/am/MiuiMemoryService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/am/MiuiMemoryService$Lifecycle;->mService:Lcom/android/server/am/MiuiMemoryService;

    .line 197
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 201
    const-string v0, "miui.memory.service"

    iget-object v1, p0, Lcom/android/server/am/MiuiMemoryService$Lifecycle;->mService:Lcom/android/server/am/MiuiMemoryService;

    invoke-virtual {p0, v0, v1}, Lcom/android/server/am/MiuiMemoryService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 202
    return-void
.end method
