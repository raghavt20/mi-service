public class com.android.server.am.MemoryStandardProcessControl implements com.android.server.am.MemoryStandardProcessControlStub {
	 /* .source "MemoryStandardProcessControl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;, */
	 /* Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;, */
	 /* Lcom/android/server/am/MemoryStandardProcessControl$BinderService;, */
	 /* Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;, */
	 /* Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;, */
	 /* Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;, */
	 /* Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardProcessControlCmd; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ADJ_WATERLINE;
public static Boolean DEBUG;
private static final java.lang.String DEFAULT_CONFIG_FILE_PATH;
public static final Integer EVENT_TAGS;
private static final Long HANDLE_MEM_PRESSURE_INTERVAL;
private static final Long KILLING_DURATION_THRESHOLD;
private static final Integer MAX_CONTINUE_DETECT_EXCEPTION;
private static final Integer MAX_PROCESS_KILL_TIMES;
private static final java.lang.String MEMORY_SCREENOFF_STRATEGY;
private static final java.lang.String MEMORY_STANDARD_APPHEAP_ENABLE_REOP;
private static final java.lang.String MEMORY_STANDARD_DEBUG_PROP;
private static final java.lang.String MEMORY_STANDARD_ENABLE_PROP;
private static final Integer MEM_NO_PRESSURE;
private static final Integer MEM_PRESSURE_COUNT;
private static final Integer MEM_PRESSURE_CRITICAL;
private static final Integer MEM_PRESSURE_LOW;
private static final Integer MEM_PRESSURE_MIN;
private static final Integer MSG_DUMP_STANDARD;
private static final Integer MSG_DUMP_WARNED_APP;
private static final Integer MSG_KILL_HIGH_PRIORITY_PROCESSES;
private static final Integer MSG_PERIODIC_DETECTION;
private static final Integer MSG_PERIODIC_KILL;
private static final Integer MSG_REPORT_APP_HEAP;
private static final Integer MSG_REPORT_PRESSURE;
private static final Integer SCREEN_STATE_OFF;
private static final Integer SCREEN_STATE_ON;
private static final Integer SCREEN_STATE_UNKOWN;
private static final java.lang.String TAG;
private static final I sDefaultCacheLevel;
private static sDefaultMemoryStandardArray;
/* # instance fields */
private com.android.server.am.ActivityManagerService mAMS;
private com.android.server.wm.ActivityTaskManagerService mATMS;
public volatile Boolean mAppHeapEnable;
public volatile Long mAppHeapHandleTime;
public java.util.HashMap mAppHeapStandard;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.audio.AudioService mAudioService;
private android.content.Context mContext;
private Long mDefaultMemoryStandard;
public volatile Boolean mEnable;
public com.android.server.am.MemoryStandardProcessControl$MyHandler mHandler;
private android.os.HandlerThread mHandlerThread;
private Boolean mIsInit;
private Boolean mLastMemDetectionHasKilled;
private Long mLastMemPressureTime;
public com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap mMemoryStandardMap;
private Long mNeedReleaseMemSize;
private com.android.server.am.ProcessManagerService mPMS;
private android.app.ApplicationPackageManager mPackageManager;
private mPressureCacheThreshold;
private android.content.BroadcastReceiver mReceiver;
private volatile Boolean mScreenDetection;
public volatile Integer mScreenOffStrategy;
public volatile Integer mScreenState;
private Boolean mTriggerKill;
private com.android.server.wm.WindowManagerInternal mWMS;
public java.util.HashMap mWarnedAppMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$mdebugModifyKillStrategy ( com.android.server.am.MemoryStandardProcessControl p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->debugModifyKillStrategy()V */
return;
} // .end method
static void -$$Nest$mhandleAppHeapWhenBackground ( com.android.server.am.MemoryStandardProcessControl p0, Integer p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/am/MemoryStandardProcessControl;->handleAppHeapWhenBackground(ILjava/lang/String;)V */
return;
} // .end method
static void -$$Nest$mhandleMemPressure ( com.android.server.am.MemoryStandardProcessControl p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl;->handleMemPressure(I)V */
return;
} // .end method
static com.android.server.am.MemoryStandardProcessControl ( ) {
/* .locals 10 */
/* .line 115 */
int v0 = 7; // const/4 v0, 0x7
/* new-array v0, v0, [J */
/* fill-array-data v0, :array_0 */
/* .line 117 */
/* const v0, 0x5a550 */
/* const v1, 0x46cd0 */
/* const v2, 0x6ddd0 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x72bf0 */
/* const v1, 0x5f370 */
/* const v2, 0x86470 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x8b290 */
/* const v1, 0x77a10 */
/* const v2, 0x9eb10 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x11da50 */
/* const v1, 0x10a1d0 */
/* const v2, 0x1312d0 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x17f4d0 */
/* const v1, 0x16bc50 */
/* const v2, 0x192d50 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x1e0f50 */
/* const v1, 0x1cd6d0 */
/* const v2, 0x1f47d0 */
/* filled-new-array {v2, v0, v1}, [I */
/* const v0, 0x2429d0 */
/* const v1, 0x22f150 */
/* const v2, 0x256250 */
/* filled-new-array {v2, v0, v1}, [I */
/* filled-new-array/range {v3 ..v9}, [[I */
/* .line 125 */
final String v0 = "persist.sys.memory_standard.debug"; // const-string v0, "persist.sys.memory_standard.debug"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.am.MemoryStandardProcessControl.DEBUG = (v0!= 0);
return;
/* nop */
/* :array_0 */
/* .array-data 8 */
/* 0x100000 */
/* 0x100000 */
/* 0x180000 */
/* 0x180000 */
/* 0x200000 */
/* 0x200000 */
/* 0x200000 */
} // .end array-data
} // .end method
public com.android.server.am.MemoryStandardProcessControl ( ) {
/* .locals 6 */
/* .line 84 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 120 */
final String v0 = "persist.sys.memory_standard.enable"; // const-string v0, "persist.sys.memory_standard.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
/* .line 121 */
final String v0 = "persist.sys.memory_standard.appheap.enable"; // const-string v0, "persist.sys.memory_standard.appheap.enable"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
/* .line 122 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenDetection:Z */
/* .line 123 */
final String v0 = "persist.sys.memory_standard.strategy"; // const-string v0, "persist.sys.memory_standard.strategy"
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* iput v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I */
/* .line 127 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v2 = "MemoryStandardProcessControl"; // const-string v2, "MemoryStandardProcessControl"
/* invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 128 */
int v0 = 3; // const/4 v0, 0x3
/* iput v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I */
/* .line 130 */
/* new-array v0, v0, [I */
this.mPressureCacheThreshold = v0;
/* .line 131 */
/* iput-boolean v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mIsInit:Z */
/* .line 132 */
/* iput-boolean v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mTriggerKill:Z */
/* .line 133 */
/* const-wide/32 v2, 0x200000 */
/* iput-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mDefaultMemoryStandard:J */
/* .line 134 */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J */
/* .line 136 */
/* const-wide/32 v4, 0x124f80 */
/* iput-wide v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J */
/* .line 138 */
/* iput-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemPressureTime:J */
/* .line 140 */
/* iput-boolean v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z */
/* .line 142 */
/* new-instance v0, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;)V */
this.mMemoryStandardMap = v0;
/* .line 143 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mWarnedAppMap = v0;
/* .line 145 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mAppHeapStandard = v0;
/* .line 147 */
int v0 = 0; // const/4 v0, 0x0
this.mContext = v0;
/* .line 148 */
this.mHandler = v0;
/* .line 149 */
this.mAMS = v0;
/* .line 150 */
this.mATMS = v0;
/* .line 151 */
this.mPMS = v0;
/* .line 152 */
this.mAudioService = v0;
/* .line 153 */
this.mPackageManager = v0;
/* .line 154 */
this.mWMS = v0;
/* .line 794 */
/* new-instance v0, Lcom/android/server/am/MemoryStandardProcessControl$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/am/MemoryStandardProcessControl$1;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;)V */
this.mReceiver = v0;
return;
} // .end method
private Boolean canKill ( Boolean p0, com.android.server.am.MemoryStandardProcessControl$AppInfo p1, java.util.List p2, java.util.List p3 ) {
/* .locals 11 */
/* .param p1, "canKillhighPriorityProcess" # Z */
/* .param p2, "info" # Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(Z", */
/* "Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 611 */
/* .local p3, "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local p4, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* iget v0, p2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
/* .line 612 */
/* .local v0, "adj":I */
/* const/16 v1, 0xfa */
int v2 = 0; // const/4 v2, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 614 */
/* if-le v0, v1, :cond_1 */
/* .line 615 */
/* .line 619 */
} // :cond_0
/* if-gt v0, v1, :cond_1 */
/* .line 620 */
/* .line 623 */
} // :cond_1
v1 = this.mPackageName;
/* .line 625 */
/* .local v1, "packageName":Ljava/lang/String; */
v3 = this.mMemoryStandardMap;
v3 = com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap .-$$Nest$fgetmNativeProcessCmds ( v3 );
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 627 */
v2 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v2 ).getStandard ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getStandard(Ljava/lang/String;)Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
/* .line 628 */
/* .local v2, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
if ( v2 != null) { // if-eqz v2, :cond_2
v3 = this.mParent;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 630 */
v3 = this.mParent;
v3 = (( com.android.server.am.MemoryStandardProcessControl ) p0 ).isForegroundApp ( v3 ); // invoke-virtual {p0, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->isForegroundApp(Ljava/lang/String;)Z
/* xor-int/2addr v3, v4 */
/* .line 632 */
} // :cond_2
/* .line 635 */
} // .end local v2 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
} // :cond_3
/* iget v3, p2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mUid:I */
/* .line 636 */
/* .local v3, "uid":I */
v5 = (( com.android.server.am.MemoryStandardProcessControl ) p0 ).isForegroundApp ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->isForegroundApp(Ljava/lang/String;)Z
/* .line 637 */
/* .local v5, "foreground":Z */
v6 = java.lang.Integer .valueOf ( v3 );
/* .line 638 */
/* .local v6, "active":Z */
v7 = java.lang.Integer .valueOf ( v3 );
/* .line 639 */
/* .local v7, "visible":Z */
/* sget-boolean v8, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 640 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "packageName: "; // const-string v9, "packageName: "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v1 ); // invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v9 = "MemoryStandardProcessControl"; // const-string v9, "MemoryStandardProcessControl"
android.util.Slog .d ( v9,v8 );
/* .line 641 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "isForegroundApp(packageName): "; // const-string v10, "isForegroundApp(packageName): "
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v5 ); // invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v9,v8 );
/* .line 642 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "activeUids.contains(uid): "; // const-string v10, "activeUids.contains(uid): "
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v9,v8 );
/* .line 643 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "visibleUids.contains(uid): " */
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v9,v8 );
/* .line 645 */
} // :cond_4
/* if-nez v5, :cond_6 */
/* if-nez v6, :cond_6 */
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 648 */
} // :cond_5
/* .line 646 */
} // :cond_6
} // :goto_0
} // .end method
private void closeIO ( java.io.Closeable p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T::", */
/* "Ljava/io/Closeable;", */
/* ">(TT;)V" */
/* } */
} // .end annotation
/* .line 1119 */
/* .local p1, "io":Ljava/io/Closeable;, "TT;" */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1121 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1124 */
/* .line 1122 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1123 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "close io failed: "; // const-string v2, "close io failed: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.IOException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MemoryStandardProcessControl"; // const-string v2, "MemoryStandardProcessControl"
android.util.Slog .e ( v2,v1 );
/* .line 1126 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :cond_0
} // :goto_0
return;
} // .end method
private void debugModifyKillStrategy ( ) {
/* .locals 1 */
/* .line 266 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
/* .line 267 */
/* iput v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenOffStrategy:I */
/* .line 268 */
return;
} // .end method
private void doClean ( java.util.Set p0, Boolean p1 ) {
/* .locals 35 */
/* .param p2, "canKillhighPriorityProcess" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 437 */
/* .local p1, "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* move-object/from16 v1, p0 */
/* move-object/from16 v2, p1 */
v0 = /* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getMemPressureLevel()I */
int v3 = 2; // const/4 v3, 0x2
/* if-ge v0, v3, :cond_0 */
/* sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
/* if-nez v0, :cond_0 */
/* .line 438 */
return;
/* .line 441 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v3, v0 */
/* .line 442 */
/* .local v3, "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;" */
/* const-wide/16 v4, 0x0 */
/* .line 445 */
/* .local v4, "memoryReleased":J */
try { // :try_start_0
v6 = this.mAMS;
/* monitor-enter v6 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 446 */
try { // :try_start_1
v0 = this.mAMS;
v0 = this.mProcessList;
(( com.android.server.am.ProcessList ) v0 ).getLruProcessesLOSP ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
/* .line 447 */
v7 = /* .local v0, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* .line 448 */
/* .local v7, "N":I */
/* add-int/lit8 v8, v7, -0x1 */
/* .local v8, "i":I */
} // :goto_0
int v12 = 0; // const/4 v12, 0x0
/* if-ltz v8, :cond_8 */
/* .line 449 */
/* check-cast v14, Lcom/android/server/am/ProcessRecord; */
/* .line 450 */
/* .local v14, "app":Lcom/android/server/am/ProcessRecord; */
v15 = this.mState;
v15 = (( com.android.server.am.ProcessStateRecord ) v15 ).getCurAdj ( ); // invoke-virtual {v15}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* .line 451 */
/* .local v15, "adj":I */
/* iget v9, v14, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* .line 452 */
/* .local v9, "pid":I */
v13 = this.processName;
/* .line 453 */
/* .local v13, "processName":Ljava/lang/String; */
v10 = this.info;
v10 = this.packageName;
/* .line 455 */
/* .local v10, "packageName":Ljava/lang/String; */
v11 = if ( v10 != null) { // if-eqz v10, :cond_6
if ( v11 != null) { // if-eqz v11, :cond_6
v11 = (( com.android.server.am.ProcessRecord ) v14 ).isKilledByAm ( ); // invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v11, :cond_6 */
/* .line 456 */
v11 = (( com.android.server.am.ProcessRecord ) v14 ).isKilled ( ); // invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* if-nez v11, :cond_5 */
(( com.android.server.am.ProcessRecord ) v14 ).getThread ( ); // invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v11 != null) { // if-eqz v11, :cond_5
/* iget-boolean v11, v14, Lcom/android/server/am/ProcessRecord;->isolated:Z */
if ( v11 != null) { // if-eqz v11, :cond_1
/* .line 457 */
/* move-object/from16 v22, v0 */
/* goto/16 :goto_1 */
/* .line 459 */
} // :cond_1
/* const/16 v11, 0xfa */
/* if-ge v15, v11, :cond_2 */
/* if-nez p2, :cond_2 */
/* .line 460 */
final String v11 = "MemoryStandardProcessControl"; // const-string v11, "MemoryStandardProcessControl"
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v22, v0 */
} // .end local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .local v22, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
final String v0 = "Process adj has a high priority and cannot be killed:"; // const-string v0, "Process adj has a high priority and cannot be killed:"
(( java.lang.StringBuilder ) v12 ).append ( v0 ); // invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = "; adj:"; // const-string v12, "; adj:"
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v0 );
/* .line 461 */
/* goto/16 :goto_1 */
/* .line 459 */
} // .end local v22 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
} // :cond_2
/* move-object/from16 v22, v0 */
/* .line 463 */
} // .end local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v22 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
v0 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v0 ).getStandard ( v10 ); // invoke-virtual {v0, v10}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getStandard(Ljava/lang/String;)Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
/* .line 465 */
/* .local v0, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
if ( v0 != null) { // if-eqz v0, :cond_3
v11 = v11 = this.mProcesses;
/* if-nez v11, :cond_3 */
/* .line 466 */
/* goto/16 :goto_1 */
/* .line 468 */
} // :cond_3
/* iget v11, v14, Lcom/android/server/am/ProcessRecord;->mPid:I */
android.os.Debug .getPss ( v11,v12,v12 );
/* move-result-wide v16 */
/* const-wide/16 v18, 0x400 */
/* div-long v16, v16, v18 */
/* move-wide/from16 v23, v16 */
/* .line 469 */
/* .local v23, "procPss":J */
/* add-long v4, v4, v23 */
/* .line 471 */
com.android.server.am.AppProfilerStub .getInstance ( );
/* iget v11, v14, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* iget v12, v14, Lcom/android/server/am/ProcessRecord;->uid:I */
/* move-object/from16 v17, v13 */
/* move/from16 v18, v11 */
/* move/from16 v19, v12 */
/* move-wide/from16 v20, v23 */
/* invoke-interface/range {v16 ..v21}, Lcom/android/server/am/AppProfilerStub;->reportMemoryStandardProcessControlKillMessage(Ljava/lang/String;IIJ)V */
/* .line 473 */
/* const/16 v11, 0xfa */
/* if-ge v15, v11, :cond_4 */
/* .line 474 */
/* new-instance v11, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo; */
/* iget v12, v14, Lcom/android/server/am/ProcessRecord;->userId:I */
/* invoke-direct {v11, v1, v10, v13, v12}, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Ljava/lang/String;Ljava/lang/String;I)V */
/* .line 476 */
} // :cond_4
int v11 = 1; // const/4 v11, 0x1
/* iput-boolean v11, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z */
/* .line 477 */
final String v12 = "MemoryStandardProcessContral"; // const-string v12, "MemoryStandardProcessContral"
/* move-object/from16 v16, v0 */
} // .end local v0 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
/* .local v16, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
/* const/16 v0, 0xd */
(( com.android.server.am.ProcessRecord ) v14 ).killLocked ( v12, v0, v11 ); // invoke-virtual {v14, v12, v0, v11}, Lcom/android/server/am/ProcessRecord;->killLocked(Ljava/lang/String;IZ)V
/* .line 478 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v11, "standardkill:" */
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {v1, v14}, Lcom/android/server/am/MemoryStandardProcessControl;->killedReason(Lcom/android/server/am/ProcessRecord;)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v11, 0x13c04 */
android.util.EventLog .writeEvent ( v11,v0 );
/* .line 479 */
final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "kill process, processName:"; // const-string v12, "kill process, processName:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v13 ); // invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = "; pid:"; // const-string v12, "; pid:"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v12, v14, Lcom/android/server/am/ProcessRecord;->mPid:I */
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v11 );
/* .line 481 */
/* .line 483 */
v0 = this.mWarnedAppMap;
(( java.util.HashMap ) v0 ).get ( v10 ); // invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 484 */
/* .local v0, "killedAppInfo":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
int v11 = 0; // const/4 v11, 0x0
/* iput v11, v0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* .line 485 */
/* const/16 v11, 0x3e9 */
/* iput v11, v0, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
/* .line 486 */
/* iget-wide v11, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J */
/* cmp-long v11, v4, v11 */
/* if-lez v11, :cond_7 */
/* sget-boolean v11, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
/* if-nez v11, :cond_7 */
/* .line 487 */
/* .line 456 */
} // .end local v16 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
} // .end local v22 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v23 # "procPss":J
/* .local v0, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
} // :cond_5
/* move-object/from16 v22, v0 */
} // .end local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v22 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* .line 455 */
} // .end local v22 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
} // :cond_6
/* move-object/from16 v22, v0 */
/* .line 448 */
} // .end local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v9 # "pid":I
} // .end local v10 # "packageName":Ljava/lang/String;
} // .end local v13 # "processName":Ljava/lang/String;
} // .end local v14 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v15 # "adj":I
/* .restart local v22 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
} // :cond_7
} // :goto_1
/* add-int/lit8 v8, v8, -0x1 */
/* move-object/from16 v0, v22 */
/* goto/16 :goto_0 */
} // .end local v22 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
/* .restart local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
} // :cond_8
/* move-object/from16 v22, v0 */
/* .line 490 */
} // .end local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v7 # "N":I
} // .end local v8 # "i":I
} // :goto_2
/* monitor-exit v6 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 492 */
int v0 = 0; // const/4 v0, 0x0
/* move v6, v0 */
/* .local v6, "i":I */
} // :goto_3
v0 = try { // :try_start_2
/* if-ge v6, v0, :cond_9 */
/* .line 493 */
final String v0 = "MemoryStandardPullUp"; // const-string v0, "MemoryStandardPullUp"
/* move-object v7, v0 */
/* .line 494 */
/* .local v7, "hostingType":Ljava/lang/String; */
/* check-cast v0, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo; */
/* move-object v8, v0 */
/* .line 495 */
/* .local v8, "procInfo":Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo; */
android.app.AppGlobals .getPackageManager ( );
v9 = this.mPackageName;
/* iget v10, v8, Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;->mUserId:I */
/* const-wide/16 v11, 0x400 */
/* .line 497 */
/* .local v27, "info":Landroid/content/pm/ApplicationInfo; */
v9 = this.mAMS;
/* monitor-enter v9 */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 498 */
try { // :try_start_3
v0 = this.mAMS;
v10 = this.mProcessName;
/* const/16 v28, 0x0 */
/* const/16 v29, 0x0 */
/* new-instance v11, Lcom/android/server/am/HostingRecord; */
v12 = this.mProcessName;
/* invoke-direct {v11, v7, v12}, Lcom/android/server/am/HostingRecord;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* const/16 v31, 0x1 */
/* const/16 v32, 0x0 */
/* const/16 v33, 0x0 */
final String v34 = "android"; // const-string v34, "android"
/* move-object/from16 v25, v0 */
/* move-object/from16 v26, v10 */
/* move-object/from16 v30, v11 */
/* invoke-virtual/range {v25 ..v34}, Lcom/android/server/am/ActivityManagerService;->startProcessLocked(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILcom/android/server/am/HostingRecord;IZZLjava/lang/String;)Lcom/android/server/am/ProcessRecord; */
/* .line 502 */
/* monitor-exit v9 */
/* .line 492 */
} // .end local v7 # "hostingType":Ljava/lang/String;
} // .end local v8 # "procInfo":Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;
} // .end local v27 # "info":Landroid/content/pm/ApplicationInfo;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 502 */
/* .restart local v7 # "hostingType":Ljava/lang/String; */
/* .restart local v8 # "procInfo":Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo; */
/* .restart local v27 # "info":Landroid/content/pm/ApplicationInfo; */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v9 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
} // .end local v3 # "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;"
} // .end local v4 # "memoryReleased":J
} // .end local p0 # "this":Lcom/android/server/am/MemoryStandardProcessControl;
} // .end local p1 # "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local p2 # "canKillhighPriorityProcess":Z
try { // :try_start_4
/* throw v0 */
/* .line 506 */
} // .end local v6 # "i":I
} // .end local v7 # "hostingType":Ljava/lang/String;
} // .end local v8 # "procInfo":Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;
} // .end local v27 # "info":Landroid/content/pm/ApplicationInfo;
/* .restart local v3 # "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;" */
/* .restart local v4 # "memoryReleased":J */
/* .restart local p0 # "this":Lcom/android/server/am/MemoryStandardProcessControl; */
/* .restart local p1 # "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .restart local p2 # "canKillhighPriorityProcess":Z */
} // :cond_9
v0 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v0 ).getNativeProcessCmds ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getNativeProcessCmds()Ljava/util/Set;
/* .line 507 */
v6 = /* .local v0, "nativeCmds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* if-nez v6, :cond_a */
/* .line 508 */
return;
/* .line 510 */
} // :cond_a
/* invoke-interface/range {p1 ..p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator; */
/* .line 512 */
/* .local v6, "procs":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v7 = } // :goto_4
if ( v7 != null) { // if-eqz v7, :cond_e
/* iget-wide v7, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J */
/* cmp-long v7, v4, v7 */
/* if-lez v7, :cond_b */
/* sget-boolean v7, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_e
/* .line 513 */
} // :cond_b
/* check-cast v7, Ljava/lang/String; */
/* .line 514 */
v8 = /* .local v7, "proc":Ljava/lang/String; */
/* if-nez v8, :cond_c */
/* .line 515 */
/* .line 517 */
} // :cond_c
/* filled-new-array {v7}, [Ljava/lang/String; */
android.os.Process .getPidsForCommands ( v8 );
/* .line 518 */
/* .local v8, "pids":[I */
if ( v8 != null) { // if-eqz v8, :cond_d
/* array-length v9, v8 */
/* if-lez v9, :cond_d */
/* .line 519 */
int v9 = 0; // const/4 v9, 0x0
/* aget v10, v8, v9 */
/* move v9, v10 */
/* .line 520 */
/* .restart local v9 # "pid":I */
int v10 = 0; // const/4 v10, 0x0
android.os.Debug .getPss ( v9,v10,v10 );
/* move-result-wide v11 */
/* const-wide/16 v13, 0x400 */
/* div-long/2addr v11, v13 */
/* .line 521 */
/* .local v11, "procPss":J */
int v15 = 1; // const/4 v15, 0x1
/* iput-boolean v15, v1, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z */
/* .line 522 */
android.os.Process .killProcess ( v9 );
/* .line 523 */
/* add-long/2addr v4, v11 */
/* .line 524 */
final String v10 = "MemoryStandardProcessControl"; // const-string v10, "MemoryStandardProcessControl"
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "kill native process, processName:"; // const-string v14, "kill native process, processName:"
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v7 ); // invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = "; pid:"; // const-string v14, "; pid:"
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v9 ); // invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v10,v13 );
/* .line 526 */
v10 = this.mWarnedAppMap;
(( java.util.HashMap ) v10 ).get ( v7 ); // invoke-virtual {v10, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v10, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 527 */
/* .local v10, "killedAppInfo":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
int v13 = 0; // const/4 v13, 0x0
/* iput v13, v10, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* .line 528 */
/* const/16 v14, -0x3e8 */
/* iput v14, v10, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 518 */
} // .end local v9 # "pid":I
} // .end local v10 # "killedAppInfo":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
} // .end local v11 # "procPss":J
} // :cond_d
int v13 = 0; // const/4 v13, 0x0
int v15 = 1; // const/4 v15, 0x1
/* .line 530 */
} // .end local v7 # "proc":Ljava/lang/String;
} // .end local v8 # "pids":[I
} // :goto_5
/* .line 533 */
} // .end local v0 # "nativeCmds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local v6 # "procs":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // :cond_e
/* .line 490 */
/* :catchall_1 */
/* move-exception v0 */
try { // :try_start_5
/* monitor-exit v6 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
} // .end local v3 # "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;"
} // .end local v4 # "memoryReleased":J
} // .end local p0 # "this":Lcom/android/server/am/MemoryStandardProcessControl;
} // .end local p1 # "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
} // .end local p2 # "canKillhighPriorityProcess":Z
try { // :try_start_6
/* throw v0 */
/* :try_end_6 */
/* .catch Ljava/lang/Exception; {:try_start_6 ..:try_end_6} :catch_0 */
/* .line 531 */
/* .restart local v3 # "recordRestartProcs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/MemoryStandardProcessControl$RestartProcessInfo;>;" */
/* .restart local v4 # "memoryReleased":J */
/* .restart local p0 # "this":Lcom/android/server/am/MemoryStandardProcessControl; */
/* .restart local p1 # "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .restart local p2 # "canKillhighPriorityProcess":Z */
/* :catch_0 */
/* move-exception v0 */
/* .line 532 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 534 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_6
return;
} // .end method
private java.util.List getActiveUids ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 582 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 583 */
/* .local v0, "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v1 = this.mPMS;
/* .line 584 */
(( com.android.server.am.ProcessManagerService ) v1 ).getProcessPolicy ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessManagerService;->getProcessPolicy()Lcom/android/server/am/ProcessPolicy;
int v2 = 3; // const/4 v2, 0x3
(( com.android.server.am.ProcessPolicy ) v1 ).getActiveUidRecordList ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/am/ProcessPolicy;->getActiveUidRecordList(I)Ljava/util/List;
/* .line 585 */
/* .local v1, "activeUids":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;>;" */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 586 */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* .line 587 */
/* .local v3, "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord; */
/* iget v4, v3, Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;->uid:I */
/* .line 588 */
/* .local v4, "uid":I */
v5 = java.lang.Integer .valueOf ( v4 );
/* if-nez v5, :cond_0 */
/* .line 589 */
java.lang.Integer .valueOf ( v4 );
/* .line 591 */
} // .end local v3 # "r":Lcom/android/server/am/ProcessPolicy$ActiveUidRecord;
} // .end local v4 # "uid":I
} // :cond_0
/* .line 593 */
} // :cond_1
/* sget-boolean v2, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
v2 = if ( v2 != null) { // if-eqz v2, :cond_3
/* if-lez v2, :cond_3 */
v2 = /* .line 594 */
/* new-array v2, v2, [I */
/* .line 595 */
/* .local v2, "debugUids":[I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v4 = } // :goto_1
/* if-ge v3, v4, :cond_2 */
/* .line 596 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* aput v4, v2, v3 */
/* .line 595 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 598 */
} // .end local v3 # "i":I
} // :cond_2
v3 = this.mPackageManager;
(( android.app.ApplicationPackageManager ) v3 ).getNamesForUids ( v2 ); // invoke-virtual {v3, v2}, Landroid/app/ApplicationPackageManager;->getNamesForUids([I)[Ljava/lang/String;
/* .line 599 */
/* .local v3, "pkgs":[Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "LocationActivePkgs: "; // const-string v5, "LocationActivePkgs: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .asList ( v3 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MemoryStandardProcessControl"; // const-string v5, "MemoryStandardProcessControl"
android.util.Slog .d ( v5,v4 );
/* .line 601 */
} // .end local v2 # "debugUids":[I
} // .end local v3 # "pkgs":[Ljava/lang/String;
} // :cond_3
} // .end method
private Integer getMemPressureLevel ( ) {
/* .locals 10 */
/* .line 235 */
/* new-instance v0, Lcom/android/internal/util/MemInfoReader; */
/* invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V */
/* .line 236 */
/* .local v0, "minfo":Lcom/android/internal/util/MemInfoReader; */
(( com.android.internal.util.MemInfoReader ) v0 ).readMemInfo ( ); // invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V
/* .line 237 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J */
/* .line 238 */
(( com.android.internal.util.MemInfoReader ) v0 ).getRawInfo ( ); // invoke-virtual {v0}, Lcom/android/internal/util/MemInfoReader;->getRawInfo()[J
/* .line 239 */
/* .local v1, "rawInfo":[J */
int v2 = 3; // const/4 v2, 0x3
/* aget-wide v2, v1, v2 */
/* const/16 v4, 0x1a */
/* aget-wide v5, v1, v4 */
/* add-long/2addr v2, v5 */
int v5 = 2; // const/4 v5, 0x2
/* aget-wide v6, v1, v5 */
/* add-long/2addr v2, v6 */
/* .line 242 */
/* .local v2, "otherFile":J */
int v6 = 4; // const/4 v6, 0x4
/* aget-wide v6, v1, v6 */
/* const/16 v8, 0x12 */
/* aget-wide v8, v1, v8 */
/* add-long/2addr v6, v8 */
/* aget-wide v8, v1, v4 */
/* add-long/2addr v6, v8 */
/* .line 245 */
/* .local v6, "needRemovedFile":J */
/* cmp-long v4, v2, v6 */
/* if-lez v4, :cond_0 */
/* .line 246 */
/* sub-long/2addr v2, v6 */
/* .line 248 */
} // :cond_0
v4 = this.mPressureCacheThreshold;
int v8 = 0; // const/4 v8, 0x0
/* aget v8, v4, v8 */
/* int-to-long v8, v8 */
/* cmp-long v8, v2, v8 */
/* if-ltz v8, :cond_1 */
/* .line 249 */
int v4 = -1; // const/4 v4, -0x1
/* .line 250 */
} // :cond_1
int v8 = 1; // const/4 v8, 0x1
/* aget v8, v4, v8 */
/* int-to-long v8, v8 */
/* cmp-long v8, v2, v8 */
/* if-ltz v8, :cond_2 */
/* .line 251 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "level":I */
/* .line 252 */
} // .end local v4 # "level":I
} // :cond_2
/* aget v4, v4, v5 */
/* int-to-long v8, v4 */
/* cmp-long v5, v2, v8 */
/* if-ltz v5, :cond_3 */
/* .line 253 */
int v4 = 1; // const/4 v4, 0x1
/* .restart local v4 # "level":I */
/* .line 255 */
} // .end local v4 # "level":I
} // :cond_3
/* int-to-long v4, v4 */
/* sub-long/2addr v4, v2 */
/* iput-wide v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mNeedReleaseMemSize:J */
/* .line 256 */
int v4 = 2; // const/4 v4, 0x2
/* .line 258 */
/* .restart local v4 # "level":I */
} // :goto_0
/* sget-boolean v5, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 259 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Other File: "; // const-string v8, "Other File: "
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2, v3 ); // invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = "KB.Mem Pressure Level: "; // const-string v8, "KB.Mem Pressure Level: "
(( java.lang.StringBuilder ) v5 ).append ( v8 ); // invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v8 = "MemoryStandardProcessControl"; // const-string v8, "MemoryStandardProcessControl"
android.util.Slog .i ( v8,v5 );
/* .line 261 */
} // :cond_4
} // .end method
private java.util.List getVisibleWindowOwner ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 568 */
v0 = this.mWMS;
(( com.android.server.wm.WindowManagerInternal ) v0 ).getVisibleWindowOwner ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerInternal;->getVisibleWindowOwner()Ljava/util/List;
/* .line 570 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* sget-boolean v1, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
v1 = if ( v1 != null) { // if-eqz v1, :cond_1
/* if-lez v1, :cond_1 */
v1 = /* .line 571 */
/* new-array v1, v1, [I */
/* .line 572 */
/* .local v1, "debugUids":[I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_0
/* if-ge v2, v3, :cond_0 */
/* .line 573 */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* aput v3, v1, v2 */
/* .line 572 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 575 */
} // .end local v2 # "i":I
} // :cond_0
v2 = this.mPackageManager;
(( android.app.ApplicationPackageManager ) v2 ).getNamesForUids ( v1 ); // invoke-virtual {v2, v1}, Landroid/app/ApplicationPackageManager;->getNamesForUids([I)[Ljava/lang/String;
/* .line 576 */
/* .local v2, "pkgs":[Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "VisibleWindowPkgs: "; // const-string v4, "VisibleWindowPkgs: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .asList ( v2 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MemoryStandardProcessControl"; // const-string v4, "MemoryStandardProcessControl"
android.util.Slog .d ( v4,v3 );
/* .line 578 */
} // .end local v1 # "debugUids":[I
} // .end local v2 # "pkgs":[Ljava/lang/String;
} // :cond_1
} // .end method
private void handleAppHeapWhenBackground ( Integer p0, java.lang.String p1 ) {
/* .locals 18 */
/* .param p1, "pid" # I */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 676 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move-object/from16 v9, p2 */
int v0 = 0; // const/4 v0, 0x0
/* .line 677 */
/* .local v0, "appHeap":I */
/* new-instance v3, Landroid/os/Debug$MemoryInfo; */
/* invoke-direct {v3}, Landroid/os/Debug$MemoryInfo;-><init>()V */
/* move-object v10, v3 */
/* .line 678 */
/* .local v10, "memInfo":Landroid/os/Debug$MemoryInfo; */
v3 = android.os.Debug .getMemoryInfo ( v2,v10 );
/* if-nez v3, :cond_0 */
/* .line 679 */
return;
/* .line 681 */
} // :cond_0
v3 = (( android.os.Debug$MemoryInfo ) v10 ).getSummaryJavaHeap ( ); // invoke-virtual {v10}, Landroid/os/Debug$MemoryInfo;->getSummaryJavaHeap()I
/* div-int/lit16 v11, v3, 0x400 */
/* .line 682 */
} // .end local v0 # "appHeap":I
/* .local v11, "appHeap":I */
final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "handle app heap when background, appHeap is "; // const-string v4, "handle app heap when background, appHeap is "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v11 ); // invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 685 */
int v3 = -1; // const/4 v3, -0x1
/* .line 686 */
/* .local v3, "uid":I */
int v4 = -1; // const/4 v4, -0x1
/* .line 687 */
/* .local v4, "userId":I */
int v5 = 0; // const/4 v5, 0x0
/* .line 688 */
/* .local v5, "finded":Z */
v6 = this.mAMS;
/* monitor-enter v6 */
/* .line 689 */
try { // :try_start_0
v0 = this.mAMS;
v0 = this.mProcessList;
(( com.android.server.am.ProcessList ) v0 ).getLruProcessesLOSP ( ); // invoke-virtual {v0}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
/* .line 690 */
v7 = /* .local v0, "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;" */
/* .line 691 */
/* .local v7, "N":I */
int v8 = 0; // const/4 v8, 0x0
/* .line 692 */
/* .local v8, "app":Lcom/android/server/am/ProcessRecord; */
/* add-int/lit8 v12, v7, -0x1 */
/* .local v12, "i":I */
} // :goto_0
/* if-ltz v12, :cond_4 */
/* .line 693 */
/* check-cast v13, Lcom/android/server/am/ProcessRecord; */
/* move-object v8, v13 */
/* .line 694 */
v13 = this.processName;
/* .line 695 */
/* .local v13, "processName":Ljava/lang/String; */
v14 = (( java.lang.String ) v9 ).equals ( v13 ); // invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v14, :cond_1 */
/* .line 697 */
/* nop */
/* .line 692 */
} // .end local v13 # "processName":Ljava/lang/String;
/* add-int/lit8 v12, v12, -0x1 */
/* .line 699 */
/* .restart local v13 # "processName":Ljava/lang/String; */
} // :cond_1
/* iget v14, v8, Lcom/android/server/am/ProcessRecord;->uid:I */
/* move v3, v14 */
/* .line 700 */
/* iget v14, v8, Lcom/android/server/am/ProcessRecord;->userId:I */
/* move v4, v14 */
/* .line 701 */
int v5 = 1; // const/4 v5, 0x1
/* .line 702 */
/* iget v14, v8, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* if-ne v14, v2, :cond_3 */
v14 = (( com.android.server.am.ProcessRecord ) v8 ).isKilledByAm ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v14, :cond_3 */
v14 = (( com.android.server.am.ProcessRecord ) v8 ).isKilled ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* if-nez v14, :cond_3 */
/* .line 703 */
(( com.android.server.am.ProcessRecord ) v8 ).getThread ( ); // invoke-virtual {v8}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v14 != null) { // if-eqz v14, :cond_3
/* iget-boolean v14, v8, Lcom/android/server/am/ProcessRecord;->isolated:Z */
if ( v14 != null) { // if-eqz v14, :cond_2
} // :cond_2
/* move v12, v3 */
/* move v13, v4 */
/* move v14, v5 */
/* .line 704 */
} // :cond_3
} // :goto_1
/* monitor-exit v6 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
return;
/* .line 692 */
} // .end local v13 # "processName":Ljava/lang/String;
} // :cond_4
/* move v12, v3 */
/* move v13, v4 */
/* move v14, v5 */
/* .line 708 */
} // .end local v0 # "records":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/ProcessRecord;>;"
} // .end local v3 # "uid":I
} // .end local v4 # "userId":I
} // .end local v5 # "finded":Z
} // .end local v7 # "N":I
} // .end local v8 # "app":Lcom/android/server/am/ProcessRecord;
/* .local v12, "uid":I */
/* .local v13, "userId":I */
/* .local v14, "finded":Z */
} // :goto_2
try { // :try_start_1
/* monitor-exit v6 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 712 */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getVisibleWindowOwner()Ljava/util/List; */
/* .line 713 */
/* .local v0, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* invoke-direct/range {p0 ..p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getActiveUids()Ljava/util/List; */
/* .line 714 */
/* .local v15, "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
if ( v14 != null) { // if-eqz v14, :cond_8
v3 = this.mAppHeapStandard;
(( java.util.HashMap ) v3 ).get ( v9 ); // invoke-virtual {v3, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* if-le v11, v3, :cond_8 */
/* .line 715 */
v3 = (( com.android.server.am.MemoryStandardProcessControl ) v1 ).isForegroundApp ( v9 ); // invoke-virtual {v1, v9}, Lcom/android/server/am/MemoryStandardProcessControl;->isForegroundApp(Ljava/lang/String;)Z
/* if-nez v3, :cond_7 */
/* .line 716 */
v3 = java.lang.Integer .valueOf ( v12 );
/* if-nez v3, :cond_6 */
/* .line 717 */
v3 = java.lang.Integer .valueOf ( v12 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* move-object/from16 v17, v0 */
/* .line 720 */
} // :cond_5
v3 = this.mAMS;
int v4 = 0; // const/4 v4, 0x0
final String v5 = "MemoryStandardProcessControl: OutOfHeapStandard"; // const-string v5, "MemoryStandardProcessControl: OutOfHeapStandard"
(( com.android.server.am.ActivityManagerService ) v3 ).forceStopPackage ( v9, v13, v4, v5 ); // invoke-virtual {v3, v9, v13, v4, v5}, Lcom/android/server/am/ActivityManagerService;->forceStopPackage(Ljava/lang/String;IILjava/lang/String;)V
/* .line 722 */
final String v8 = "#%s n:%s(%d) u:%d pss:%d r:%s"; // const-string v8, "#%s n:%s(%d) u:%d pss:%d r:%s"
final String v3 = "KillProc"; // const-string v3, "KillProc"
/* .line 723 */
/* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
java.lang.Integer .valueOf ( v12 );
java.lang.Integer .valueOf ( v11 );
final String v16 = "OutOfHeapStandard"; // const-string v16, "OutOfHeapStandard"
/* move-object/from16 v4, p2 */
/* move-object/from16 v17, v0 */
/* move-object v0, v8 */
} // .end local v0 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .local v17, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* move-object/from16 v8, v16 */
/* filled-new-array/range {v3 ..v8}, [Ljava/lang/Object; */
/* .line 722 */
java.lang.String .format ( v0,v3 );
/* .line 724 */
/* .local v0, "killReason":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "overHeapkill:"; // const-string v4, "overHeapkill:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const v4, 0x13c04 */
android.util.EventLog .writeEvent ( v4,v3 );
/* .line 725 */
final String v3 = "MemoryStandardProcessControl"; // const-string v3, "MemoryStandardProcessControl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "over heap kill, processName:"; // const-string v5, "over heap kill, processName:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v9 ); // invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "; pid:"; // const-string v5, "; pid:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 726 */
return;
/* .line 716 */
} // .end local v17 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .local v0, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
} // :cond_6
/* move-object/from16 v17, v0 */
} // .end local v0 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .restart local v17 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .line 715 */
} // .end local v17 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .restart local v0 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
} // :cond_7
/* move-object/from16 v17, v0 */
} // .end local v0 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .restart local v17 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .line 714 */
} // .end local v17 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .restart local v0 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
} // :cond_8
/* move-object/from16 v17, v0 */
/* .line 718 */
} // .end local v0 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .restart local v17 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
} // :goto_3
return;
/* .line 708 */
} // .end local v15 # "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v17 # "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* :catchall_0 */
/* move-exception v0 */
/* move v3, v12 */
/* move v4, v13 */
/* move v5, v14 */
} // .end local v12 # "uid":I
} // .end local v13 # "userId":I
} // .end local v14 # "finded":Z
/* .restart local v3 # "uid":I */
/* .restart local v4 # "userId":I */
/* .restart local v5 # "finded":Z */
/* :catchall_1 */
/* move-exception v0 */
} // :goto_4
try { // :try_start_2
/* monitor-exit v6 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v0 */
} // .end method
private void handleMemPressure ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pressureState" # I */
/* .line 657 */
/* sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
final String v1 = "MemoryStandardProcessControl"; // const-string v1, "MemoryStandardProcessControl"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 658 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "current pressure is "; // const-string v2, "current pressure is "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 661 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getMemPressureLevel()I */
int v2 = 2; // const/4 v2, 0x2
/* if-ge v0, v2, :cond_1 */
/* sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
/* if-nez v0, :cond_1 */
/* .line 662 */
return;
/* .line 664 */
} // :cond_1
(( com.android.server.am.MemoryStandardProcessControl ) p0 ).periodicMemoryDetection ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->periodicMemoryDetection()V
/* .line 666 */
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 667 */
v0 = this.mContext;
com.android.server.am.MemoryControlJobService .detectionSchedule ( v0 );
/* .line 668 */
/* sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 669 */
final String v0 = "app killed by memory pressure, reschedule detection job"; // const-string v0, "app killed by memory pressure, reschedule detection job"
android.util.Slog .d ( v1,v0 );
/* .line 672 */
} // :cond_2
return;
} // .end method
private void initGlobalParams ( android.content.Context p0, com.android.server.am.ActivityManagerService p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .param p3, "index" # I */
/* .line 213 */
v0 = com.android.server.am.MemoryStandardProcessControl.sDefaultMemoryStandardArray;
/* aget-wide v0, v0, p3 */
/* iput-wide v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mDefaultMemoryStandard:J */
/* .line 214 */
/* invoke-direct {p0, p3}, Lcom/android/server/am/MemoryStandardProcessControl;->loadDefCacheLevelConfig(I)V */
/* .line 215 */
v0 = this.mHandlerThread;
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 216 */
/* new-instance v0, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler; */
v1 = this.mHandlerThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 217 */
this.mAMS = p2;
/* .line 218 */
v0 = this.mActivityTaskManager;
this.mATMS = v0;
/* .line 219 */
final String v0 = "ProcessManager"; // const-string v0, "ProcessManager"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/am/ProcessManagerService; */
this.mPMS = v0;
/* .line 220 */
(( android.content.Context ) p1 ).getApplicationContext ( ); // invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* check-cast v0, Landroid/app/ApplicationPackageManager; */
this.mPackageManager = v0;
/* .line 221 */
/* const-class v0, Lcom/android/server/wm/WindowManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerInternal; */
this.mWMS = v0;
/* .line 222 */
final String v0 = "audio"; // const-string v0, "audio"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/audio/AudioService; */
this.mAudioService = v0;
/* .line 223 */
return;
} // .end method
private java.lang.String killedReason ( com.android.server.am.ProcessRecord p0 ) {
/* .locals 6 */
/* .param p1, "pr" # Lcom/android/server/am/ProcessRecord; */
/* .line 652 */
final String v0 = "KillProc"; // const-string v0, "KillProc"
v1 = this.processName;
/* iget v2, p1, Lcom/android/server/am/ProcessRecord;->mPid:I */
/* .line 653 */
java.lang.Integer .valueOf ( v2 );
/* iget v3, p1, Lcom/android/server/am/ProcessRecord;->uid:I */
java.lang.Integer .valueOf ( v3 );
v4 = this.mProfile;
(( com.android.server.am.ProcessProfileRecord ) v4 ).getLastPss ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessProfileRecord;->getLastPss()J
/* move-result-wide v4 */
java.lang.Long .valueOf ( v4,v5 );
final String v5 = "OutOfMemoryStandard"; // const-string v5, "OutOfMemoryStandard"
/* filled-new-array/range {v0 ..v5}, [Ljava/lang/Object; */
/* .line 652 */
final String v1 = "#%s n:%s(%d) u:%d pss:%d r:%s"; // const-string v1, "#%s n:%s(%d) u:%d pss:%d r:%s"
java.lang.String .format ( v1,v0 );
} // .end method
private void loadDefCacheLevelConfig ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "memIndex" # I */
/* .line 227 */
v0 = this.mPressureCacheThreshold;
v1 = com.android.server.am.MemoryStandardProcessControl.sDefaultCacheLevel;
/* aget-object v1, v1, p1 */
int v2 = 0; // const/4 v2, 0x0
/* aget v3, v1, v2 */
/* aput v3, v0, v2 */
/* .line 228 */
int v2 = 1; // const/4 v2, 0x1
/* aget v3, v1, v2 */
/* aput v3, v0, v2 */
/* .line 229 */
int v2 = 2; // const/4 v2, 0x2
/* aget v1, v1, v2 */
/* aput v1, v0, v2 */
/* .line 230 */
return;
} // .end method
private Boolean loadDefaultConfigFromFile ( ) {
/* .locals 9 */
/* .line 1004 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/product/etc/mspc.default.json"; // const-string v1, "/product/etc/mspc.default.json"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1005 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 1006 */
/* .line 1008 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 1009 */
/* .local v1, "inputStream":Ljava/io/InputStream; */
int v3 = 0; // const/4 v3, 0x0
/* .line 1010 */
/* .local v3, "inputStreamReader":Ljava/io/InputStreamReader; */
int v4 = 0; // const/4 v4, 0x0
/* .line 1012 */
/* .local v4, "jsonReader":Landroid/util/JsonReader; */
v5 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v5 ).clear ( ); // invoke-virtual {v5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->clear()V
/* .line 1014 */
/* sget-boolean v5, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
final String v5 = "lite"; // const-string v5, "lite"
} // :cond_1
final String v5 = "miui"; // const-string v5, "miui"
/* .line 1016 */
/* .local v5, "systemType":Ljava/lang/String; */
} // :goto_0
try { // :try_start_0
/* new-instance v6, Ljava/io/FileInputStream; */
/* invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v1, v6 */
/* .line 1017 */
/* new-instance v6, Ljava/io/InputStreamReader; */
/* invoke-direct {v6, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* move-object v3, v6 */
/* .line 1018 */
/* new-instance v6, Landroid/util/JsonReader; */
/* invoke-direct {v6, v3}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V */
/* move-object v4, v6 */
/* .line 1020 */
(( android.util.JsonReader ) v4 ).beginObject ( ); // invoke-virtual {v4}, Landroid/util/JsonReader;->beginObject()V
/* .line 1021 */
} // :goto_1
v6 = (( android.util.JsonReader ) v4 ).hasNext ( ); // invoke-virtual {v4}, Landroid/util/JsonReader;->hasNext()Z
if ( v6 != null) { // if-eqz v6, :cond_5
/* .line 1022 */
(( android.util.JsonReader ) v4 ).nextName ( ); // invoke-virtual {v4}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;
/* .line 1024 */
/* .local v6, "name":Ljava/lang/String; */
final String v7 = "aging"; // const-string v7, "aging"
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 1025 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigAging(Landroid/util/JsonReader;Ljava/lang/String;)V */
/* .line 1026 */
} // :cond_2
final String v7 = "common"; // const-string v7, "common"
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 1027 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigCommon(Landroid/util/JsonReader;Ljava/lang/String;)V */
/* .line 1028 */
} // :cond_3
final String v7 = "heap"; // const-string v7, "heap"
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 1029 */
/* invoke-direct {p0, v4, v5}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigHeap(Landroid/util/JsonReader;Ljava/lang/String;)V */
/* .line 1031 */
} // :cond_4
(( android.util.JsonReader ) v4 ).skipValue ( ); // invoke-virtual {v4}, Landroid/util/JsonReader;->skipValue()V
/* .line 1033 */
} // .end local v6 # "name":Ljava/lang/String;
} // :goto_2
/* .line 1034 */
} // :cond_5
(( android.util.JsonReader ) v4 ).endObject ( ); // invoke-virtual {v4}, Landroid/util/JsonReader;->endObject()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1041 */
/* invoke-direct {p0, v4}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1042 */
/* invoke-direct {p0, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1043 */
/* invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1044 */
/* nop */
/* .line 1045 */
int v2 = 1; // const/4 v2, 0x1
/* .line 1041 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 1035 */
/* :catch_0 */
/* move-exception v6 */
/* .line 1036 */
/* .local v6, "e":Ljava/io/IOException; */
try { // :try_start_1
/* sget-boolean v7, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 1037 */
final String v7 = "MemoryStandardProcessControl"; // const-string v7, "MemoryStandardProcessControl"
final String v8 = "load config from default config file failed, invalid config"; // const-string v8, "load config from default config file failed, invalid config"
android.util.Slog .e ( v7,v8 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1039 */
} // :cond_6
/* nop */
/* .line 1041 */
/* invoke-direct {p0, v4}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1042 */
/* invoke-direct {p0, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1043 */
/* invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1039 */
/* .line 1041 */
} // .end local v6 # "e":Ljava/io/IOException;
} // :goto_3
/* invoke-direct {p0, v4}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1042 */
/* invoke-direct {p0, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1043 */
/* invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1044 */
/* throw v2 */
} // .end method
private void parseConfigAging ( android.util.JsonReader p0, java.lang.String p1 ) {
/* .locals 12 */
/* .param p1, "jsonReader" # Landroid/util/JsonReader; */
/* .param p2, "systemType" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 851 */
(( android.util.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V
/* .line 852 */
} // :cond_0
} // :goto_0
v0 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 853 */
(( android.util.JsonReader ) p1 ).beginObject ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V
/* .line 854 */
int v0 = 0; // const/4 v0, 0x0
/* .line 855 */
/* .local v0, "packageName":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 856 */
/* .local v1, "parent":Ljava/lang/String; */
/* const-wide/16 v2, 0x0 */
/* .line 857 */
/* .local v2, "pss":J */
int v4 = 0; // const/4 v4, 0x0
/* .line 858 */
/* .local v4, "isNative":Z */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 859 */
/* .local v5, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
} // :goto_1
v6 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 860 */
(( android.util.JsonReader ) p1 ).nextName ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;
/* .line 861 */
/* .local v6, "key":Ljava/lang/String; */
final String v7 = "name"; // const-string v7, "name"
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 863 */
(( android.util.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
/* .line 864 */
} // :cond_1
final String v7 = "processes"; // const-string v7, "processes"
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 866 */
(( android.util.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V
/* .line 867 */
} // :goto_2
v7 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 868 */
(( android.util.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
/* .line 870 */
} // :cond_2
(( android.util.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V
/* .line 871 */
} // :cond_3
v7 = (( java.lang.String ) v6 ).equals ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 873 */
(( android.util.JsonReader ) p1 ).nextLong ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J
/* move-result-wide v2 */
/* .line 874 */
} // :cond_4
final String v7 = "parent"; // const-string v7, "parent"
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 876 */
(( android.util.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
/* .line 877 */
} // :cond_5
final String v7 = "native"; // const-string v7, "native"
v7 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_6
/* .line 879 */
v4 = (( android.util.JsonReader ) p1 ).nextBoolean ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z
/* .line 882 */
} // :cond_6
(( android.util.JsonReader ) p1 ).skipValue ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V
/* .line 884 */
} // .end local v6 # "key":Ljava/lang/String;
} // :goto_3
/* .line 885 */
} // :cond_7
(( android.util.JsonReader ) p1 ).endObject ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V
/* .line 887 */
/* const-wide/16 v6, 0x0 */
/* cmp-long v6, v2, v6 */
if ( v6 != null) { // if-eqz v6, :cond_0
/* if-nez v0, :cond_8 */
/* .line 888 */
/* .line 890 */
} // :cond_8
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 892 */
/* .line 893 */
v6 = this.mMemoryStandardMap;
/* move-wide v7, v2 */
/* move-object v9, v0 */
/* move-object v10, v5 */
/* move-object v11, v1 */
/* invoke-virtual/range {v6 ..v11}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateNativeMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V */
/* .line 895 */
} // :cond_9
v6 = this.mMemoryStandardMap;
/* move-wide v7, v2 */
/* move-object v9, v0 */
/* move-object v10, v5 */
/* move-object v11, v1 */
/* invoke-virtual/range {v6 ..v11}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V */
/* .line 897 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v1 # "parent":Ljava/lang/String;
} // .end local v2 # "pss":J
} // .end local v4 # "isNative":Z
} // .end local v5 # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :goto_4
/* goto/16 :goto_0 */
/* .line 898 */
} // :cond_a
(( android.util.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V
/* .line 899 */
return;
} // .end method
private void parseConfigCommon ( android.util.JsonReader p0, java.lang.String p1 ) {
/* .locals 13 */
/* .param p1, "jsonReader" # Landroid/util/JsonReader; */
/* .param p2, "systemType" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 903 */
(( android.util.JsonReader ) p1 ).beginObject ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V
/* .line 904 */
/* const-wide/16 v0, 0x0 */
/* .line 905 */
/* .local v0, "pss":J */
} // :goto_0
v2 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v2 != null) { // if-eqz v2, :cond_c
/* .line 906 */
(( android.util.JsonReader ) p1 ).nextName ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;
/* .line 907 */
/* .local v8, "key":Ljava/lang/String; */
v2 = (( java.lang.String ) v8 ).equals ( p2 ); // invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 909 */
(( android.util.JsonReader ) p1 ).nextLong ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J
/* move-result-wide v0 */
/* goto/16 :goto_6 */
/* .line 910 */
} // :cond_0
final String v2 = "apps"; // const-string v2, "apps"
v2 = (( java.lang.String ) v8 ).equals ( v2 ); // invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 911 */
(( android.util.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V
/* .line 912 */
} // :cond_1
} // :goto_1
v2 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 913 */
(( android.util.JsonReader ) p1 ).beginObject ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V
/* .line 914 */
int v2 = 0; // const/4 v2, 0x0
/* .line 915 */
/* .local v2, "packageName":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* .line 916 */
/* .local v3, "parent":Ljava/lang/String; */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* move-object v9, v4 */
/* .line 917 */
/* .local v9, "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
int v4 = 0; // const/4 v4, 0x0
/* move-object v10, v2 */
/* move-object v11, v3 */
/* move v12, v4 */
/* .line 918 */
} // .end local v2 # "packageName":Ljava/lang/String;
} // .end local v3 # "parent":Ljava/lang/String;
/* .local v10, "packageName":Ljava/lang/String; */
/* .local v11, "parent":Ljava/lang/String; */
/* .local v12, "isNative":Z */
} // :goto_2
v2 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 919 */
(( android.util.JsonReader ) p1 ).nextName ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;
/* .line 920 */
/* .local v2, "pKey":Ljava/lang/String; */
final String v3 = "name"; // const-string v3, "name"
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 922 */
(( android.util.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
/* move-object v10, v3 */
} // .end local v10 # "packageName":Ljava/lang/String;
/* .local v3, "packageName":Ljava/lang/String; */
/* .line 923 */
} // .end local v3 # "packageName":Ljava/lang/String;
/* .restart local v10 # "packageName":Ljava/lang/String; */
} // :cond_2
final String v3 = "processes"; // const-string v3, "processes"
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 925 */
(( android.util.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V
/* .line 926 */
} // :goto_3
v3 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 927 */
(( android.util.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
/* .line 929 */
} // :cond_3
(( android.util.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V
/* .line 930 */
} // :cond_4
final String v3 = "native"; // const-string v3, "native"
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 931 */
v3 = (( android.util.JsonReader ) p1 ).nextBoolean ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z
/* move v12, v3 */
} // .end local v12 # "isNative":Z
/* .local v3, "isNative":Z */
/* .line 932 */
} // .end local v3 # "isNative":Z
/* .restart local v12 # "isNative":Z */
} // :cond_5
final String v3 = "parent"; // const-string v3, "parent"
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 933 */
(( android.util.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
/* move-object v11, v3 */
} // .end local v11 # "parent":Ljava/lang/String;
/* .local v3, "parent":Ljava/lang/String; */
/* .line 935 */
} // .end local v3 # "parent":Ljava/lang/String;
/* .restart local v11 # "parent":Ljava/lang/String; */
} // :cond_6
(( android.util.JsonReader ) p1 ).skipValue ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V
/* .line 937 */
} // .end local v2 # "pKey":Ljava/lang/String;
} // :goto_4
/* .line 939 */
} // :cond_7
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v0, v2 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* if-nez v10, :cond_8 */
/* .line 940 */
/* .line 942 */
} // :cond_8
if ( v12 != null) { // if-eqz v12, :cond_9
/* .line 944 */
/* .line 945 */
v2 = this.mMemoryStandardMap;
/* move-wide v3, v0 */
/* move-object v5, v10 */
/* move-object v6, v9 */
/* move-object v7, v11 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateNativeMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V */
/* .line 947 */
} // :cond_9
v2 = this.mMemoryStandardMap;
/* move-wide v3, v0 */
/* move-object v5, v10 */
/* move-object v6, v9 */
/* move-object v7, v11 */
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->updateMemoryStandard(JLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V */
/* .line 949 */
} // :goto_5
(( android.util.JsonReader ) p1 ).endObject ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V
/* .line 950 */
} // .end local v9 # "processes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // .end local v10 # "packageName":Ljava/lang/String;
} // .end local v11 # "parent":Ljava/lang/String;
} // .end local v12 # "isNative":Z
/* goto/16 :goto_1 */
/* .line 951 */
} // :cond_a
(( android.util.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V
/* .line 953 */
} // :cond_b
(( android.util.JsonReader ) p1 ).skipValue ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V
/* .line 955 */
} // .end local v8 # "key":Ljava/lang/String;
} // :goto_6
/* goto/16 :goto_0 */
/* .line 956 */
} // :cond_c
(( android.util.JsonReader ) p1 ).endObject ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V
/* .line 957 */
return;
} // .end method
private void parseConfigHeap ( android.util.JsonReader p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "jsonReader" # Landroid/util/JsonReader; */
/* .param p2, "systemType" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 960 */
(( android.util.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V
/* .line 961 */
} // :cond_0
} // :goto_0
v0 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 962 */
(( android.util.JsonReader ) p1 ).beginObject ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V
/* .line 963 */
int v0 = 0; // const/4 v0, 0x0
/* .line 964 */
/* .local v0, "packageName":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
/* .line 965 */
/* .local v1, "appHeap":I */
} // :goto_1
v2 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 966 */
(( android.util.JsonReader ) p1 ).nextName ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;
/* .line 967 */
/* .local v2, "key":Ljava/lang/String; */
final String v3 = "name"; // const-string v3, "name"
v3 = (( java.lang.String ) v2 ).equals ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 969 */
(( android.util.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
/* .line 970 */
} // :cond_1
v3 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 972 */
v1 = (( android.util.JsonReader ) p1 ).nextInt ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I
/* .line 975 */
} // :cond_2
(( android.util.JsonReader ) p1 ).skipValue ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V
/* .line 977 */
} // .end local v2 # "key":Ljava/lang/String;
} // :goto_2
/* .line 978 */
} // :cond_3
(( android.util.JsonReader ) p1 ).endObject ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V
/* .line 980 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* if-nez v0, :cond_4 */
/* .line 981 */
/* .line 983 */
} // :cond_4
v2 = this.mAppHeapStandard;
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v2 ).put ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 984 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v1 # "appHeap":I
/* .line 985 */
} // :cond_5
(( android.util.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V
/* .line 986 */
return;
} // .end method
private void parseConfigRemove ( android.util.JsonReader p0 ) {
/* .locals 2 */
/* .param p1, "jsonReader" # Landroid/util/JsonReader; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 991 */
(( android.util.JsonReader ) p1 ).beginArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V
/* .line 992 */
} // :goto_0
v0 = (( android.util.JsonReader ) p1 ).hasNext ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 993 */
(( android.util.JsonReader ) p1 ).nextString ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;
/* .line 995 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v1 ).removeMemoryStandard ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->removeMemoryStandard(Ljava/lang/String;)V
/* .line 997 */
v1 = this.mWarnedAppMap;
(( java.util.HashMap ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 998 */
} // .end local v0 # "packageName":Ljava/lang/String;
/* .line 999 */
} // :cond_0
(( android.util.JsonReader ) p1 ).endArray ( ); // invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V
/* .line 1000 */
return;
} // .end method
private void resetRecordDataIfNeeded ( ) {
/* .locals 8 */
/* .line 553 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 555 */
/* .local v0, "now":J */
v2 = this.mWarnedAppMap;
(( java.util.HashMap ) v2 ).values ( ); // invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 556 */
/* .local v3, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* iget-wide v4, v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mLastKillTime:J */
/* sub-long v4, v0, v4 */
/* const-wide/32 v6, 0x5265c00 */
/* cmp-long v4, v4, v6 */
/* if-lez v4, :cond_0 */
/* iget v4, v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I */
int v5 = 3; // const/4 v5, 0x3
/* if-le v4, v5, :cond_0 */
/* .line 558 */
int v4 = 0; // const/4 v4, 0x0
/* iput v4, v3, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I */
/* .line 559 */
/* sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 560 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
v5 = this.mPackageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " long time never killed, reset kill times"; // const-string v5, " long time never killed, reset kill times"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MemoryStandardProcessControl"; // const-string v5, "MemoryStandardProcessControl"
android.util.Slog .d ( v5,v4 );
/* .line 563 */
} // .end local v3 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
} // :cond_0
/* .line 564 */
} // :cond_1
return;
} // .end method
/* # virtual methods */
public void callKillProcess ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "killHighPriority" # Z */
/* .line 774 */
if ( p1 != null) { // if-eqz p1, :cond_0
int v0 = 3; // const/4 v0, 0x3
} // :cond_0
int v0 = 2; // const/4 v0, 0x2
/* .line 775 */
/* .local v0, "msgId":I */
} // :goto_0
v1 = this.mHandler;
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v1 ).removeMessages ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeMessages(I)V
/* .line 776 */
v1 = this.mHandler;
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v1 ).obtainMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 777 */
return;
} // .end method
public void callPeriodicDetection ( ) {
/* .locals 2 */
/* .line 768 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeMessages(I)V
/* .line 769 */
v0 = this.mHandler;
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(I)Landroid/os/Message;
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 770 */
return;
} // .end method
public void callRunnable ( java.lang.Runnable p0 ) {
/* .locals 1 */
/* .param p1, "callback" # Ljava/lang/Runnable; */
/* .line 790 */
v0 = this.mHandler;
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v0 ).post ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->post(Ljava/lang/Runnable;)Z
/* .line 791 */
return;
} // .end method
public void callUpdateCloudAppHeapConfig ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "cloudStr" # Ljava/lang/String; */
/* .line 786 */
(( com.android.server.am.MemoryStandardProcessControl ) p0 ).loadAppHeapConfigFromCloud ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl;->loadAppHeapConfigFromCloud(Ljava/lang/String;)V
/* .line 787 */
return;
} // .end method
public void callUpdateCloudConfig ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "cloudStr" # Ljava/lang/String; */
/* .line 781 */
(( com.android.server.am.MemoryStandardProcessControl ) p0 ).loadConfigFromCloud ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/am/MemoryStandardProcessControl;->loadConfigFromCloud(Ljava/lang/String;)V
/* .line 782 */
return;
} // .end method
public Boolean init ( android.content.Context p0, com.android.server.am.ActivityManagerService p1 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .line 162 */
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_7
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mIsInit:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_1 */
/* .line 166 */
} // :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->loadDefaultConfigFromFile()Z */
/* if-nez v0, :cond_1 */
/* .line 167 */
/* iput-boolean v1, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
/* .line 168 */
/* .line 170 */
} // :cond_1
this.mContext = p1;
/* .line 171 */
android.os.Process .getTotalMemory ( );
/* move-result-wide v0 */
/* const-wide/32 v2, 0x40000000 */
/* div-long/2addr v0, v2 */
/* const-wide/16 v2, 0x1 */
/* add-long/2addr v0, v2 */
/* long-to-int v0, v0 */
/* .line 172 */
/* .local v0, "totalMemGB":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 173 */
/* .local v1, "index":I */
/* const/16 v2, 0xc */
int v3 = 1; // const/4 v3, 0x1
/* if-le v0, v2, :cond_2 */
/* .line 175 */
v2 = com.android.server.am.MemoryStandardProcessControl.sDefaultMemoryStandardArray;
/* array-length v2, v2 */
/* sub-int/2addr v2, v3 */
} // .end local v1 # "index":I
/* .local v2, "index":I */
/* .line 176 */
} // .end local v2 # "index":I
/* .restart local v1 # "index":I */
} // :cond_2
int v2 = 4; // const/4 v2, 0x4
/* if-le v0, v2, :cond_3 */
/* .line 178 */
/* div-int/lit8 v2, v0, 0x2 */
} // .end local v1 # "index":I
/* .restart local v2 # "index":I */
/* .line 181 */
} // .end local v2 # "index":I
/* .restart local v1 # "index":I */
} // :cond_3
/* add-int/lit8 v2, v0, -0x2 */
/* .line 183 */
} // .end local v1 # "index":I
/* .restart local v2 # "index":I */
} // :goto_0
/* invoke-direct {p0, p1, p2, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->initGlobalParams(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;I)V */
/* .line 184 */
/* new-instance v1, Lcom/android/server/am/MemoryControlCloud; */
/* invoke-direct {v1}, Lcom/android/server/am/MemoryControlCloud;-><init>()V */
/* .line 185 */
/* .local v1, "cloudControl":Lcom/android/server/am/MemoryControlCloud; */
(( com.android.server.am.MemoryControlCloud ) v1 ).initCloud ( p1, p0 ); // invoke-virtual {v1, p1, p0}, Lcom/android/server/am/MemoryControlCloud;->initCloud(Landroid/content/Context;Lcom/android/server/am/MemoryStandardProcessControl;)Z
/* .line 188 */
/* iget-boolean v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenDetection:Z */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 189 */
/* new-instance v4, Landroid/content/IntentFilter; */
/* invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V */
/* .line 190 */
/* .local v4, "filter":Landroid/content/IntentFilter; */
final String v5 = "android.intent.action.SCREEN_ON"; // const-string v5, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v4 ).addAction ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 191 */
final String v5 = "android.intent.action.SCREEN_OFF"; // const-string v5, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v4 ).addAction ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 192 */
v5 = this.mReceiver;
(( android.content.Context ) p1 ).registerReceiver ( v5, v4 ); // invoke-virtual {p1, v5, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 194 */
} // .end local v4 # "filter":Landroid/content/IntentFilter;
} // :cond_4
/* sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 195 */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->debugModifyKillStrategy()V */
/* .line 199 */
} // :cond_5
v4 = this.mContext;
com.android.server.am.MemoryControlJobService .detectionSchedule ( v4 );
/* .line 200 */
v4 = this.mContext;
com.android.server.am.MemoryControlJobService .killSchedule ( v4 );
/* .line 201 */
/* new-instance v4, Lcom/android/server/am/MemoryStandardProcessControl$BinderService; */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v4, p0, v5}, Lcom/android/server/am/MemoryStandardProcessControl$BinderService;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Lcom/android/server/am/MemoryStandardProcessControl$BinderService-IA;)V */
final String v5 = "mspc"; // const-string v5, "mspc"
android.os.ServiceManager .addService ( v5,v4 );
/* .line 203 */
/* sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 204 */
final String v4 = "MemoryStandardProcessControl"; // const-string v4, "MemoryStandardProcessControl"
final String v5 = "init mspc2.0 success"; // const-string v5, "init mspc2.0 success"
android.util.Slog .d ( v4,v5 );
/* .line 207 */
} // :cond_6
/* iput-boolean v3, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mIsInit:Z */
/* .line 208 */
/* .line 163 */
} // .end local v0 # "totalMemGB":I
} // .end local v1 # "cloudControl":Lcom/android/server/am/MemoryControlCloud;
} // .end local v2 # "index":I
} // :cond_7
} // :goto_1
} // .end method
public Boolean isEnable ( ) {
/* .locals 1 */
/* .line 157 */
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mIsInit:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isForegroundApp ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 539 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mATMS;
(( com.android.server.wm.ActivityTaskManagerService ) v1 ).getFocusedRootTaskInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerService;->getFocusedRootTaskInfo()Landroid/app/ActivityTaskManager$RootTaskInfo;
/* .line 540 */
/* .local v1, "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = this.topActivity;
/* if-nez v2, :cond_0 */
/* .line 544 */
} // :cond_0
v2 = this.topActivity;
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 545 */
/* .local v2, "topPackageName":Ljava/lang/String; */
v3 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
int v0 = 1; // const/4 v0, 0x1
/* .line 548 */
} // .end local v1 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
} // .end local v2 # "topPackageName":Ljava/lang/String;
} // :cond_1
/* .line 541 */
/* .restart local v1 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo; */
} // :cond_2
} // :goto_0
final String v2 = "MemoryStandardProcessControl"; // const-string v2, "MemoryStandardProcessControl"
final String v3 = "get getFocusedStackInfo error."; // const-string v3, "get getFocusedStackInfo error."
android.util.Slog .e ( v2,v3 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 542 */
/* .line 546 */
} // .end local v1 # "info":Landroid/app/ActivityTaskManager$RootTaskInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 549 */
} // :goto_1
} // .end method
public void killProcess ( Boolean p0 ) {
/* .locals 9 */
/* .param p1, "canKillhighPriorityProcess" # Z */
/* .line 396 */
v0 = (( com.android.server.am.MemoryStandardProcessControl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z
/* if-nez v0, :cond_0 */
/* .line 397 */
return;
/* .line 399 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "canKillhighPriorityProcess: "; // const-string v1, "canKillhighPriorityProcess: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MemoryStandardProcessControl"; // const-string v1, "MemoryStandardProcessControl"
android.util.Slog .d ( v1,v0 );
/* .line 400 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 401 */
/* iget v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v0, v2, :cond_2 */
/* .line 402 */
final String v0 = "screen on when detect, skip"; // const-string v0, "screen on when detect, skip"
android.util.Slog .d ( v1,v0 );
/* .line 403 */
return;
/* .line 406 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mTriggerKill:Z */
/* .line 408 */
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getActiveUids()Ljava/util/List; */
/* .line 409 */
/* .local v0, "activeUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->getVisibleWindowOwner()Ljava/util/List; */
/* .line 411 */
/* .local v2, "visibleUids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 412 */
/* .local v3, "deletePackageNameSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v4 = this.mWarnedAppMap;
(( java.util.HashMap ) v4 ).values ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_6
/* check-cast v5, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 413 */
/* .local v5, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* iget v6, v5, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
int v7 = 3; // const/4 v7, 0x3
/* if-ge v6, v7, :cond_3 */
/* .line 414 */
/* .line 416 */
} // :cond_3
v6 = this.mPackageName;
/* .line 418 */
/* .local v6, "packageName":Ljava/lang/String; */
v7 = /* invoke-direct {p0, p1, v5, v0, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->canKill(ZLcom/android/server/am/MemoryStandardProcessControl$AppInfo;Ljava/util/List;Ljava/util/List;)Z */
/* if-nez v7, :cond_4 */
/* .line 419 */
/* .line 421 */
} // :cond_4
/* sget-boolean v7, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 422 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "add to the kill list, packageName:"; // const-string v8, "add to the kill list, packageName:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mPackageName;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "; mMemExcelTime:"; // const-string v8, "; mMemExcelTime:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v8, v5, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v7 );
/* .line 425 */
} // :cond_5
/* .line 426 */
} // .end local v5 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
} // .end local v6 # "packageName":Ljava/lang/String;
/* .line 428 */
v1 = } // :cond_6
/* if-lez v1, :cond_7 */
/* .line 429 */
/* invoke-direct {p0, v3, p1}, Lcom/android/server/am/MemoryStandardProcessControl;->doClean(Ljava/util/Set;Z)V */
/* .line 432 */
} // :cond_7
/* invoke-direct {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->resetRecordDataIfNeeded()V */
/* .line 433 */
return;
} // .end method
public void loadAppHeapConfigFromCloud ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 1085 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1086 */
/* .local v0, "stringReader":Ljava/io/StringReader; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1087 */
/* .local v1, "jsonReader":Landroid/util/JsonReader; */
/* sget-boolean v2, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
final String v2 = "lite"; // const-string v2, "lite"
} // :cond_0
final String v2 = "miui"; // const-string v2, "miui"
/* .line 1089 */
/* .local v2, "systemType":Ljava/lang/String; */
} // :goto_0
v3 = this.mAppHeapStandard;
(( java.util.HashMap ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->clear()V
/* .line 1091 */
v3 = this.mHandler;
int v4 = 7; // const/4 v4, 0x7
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v3 ).removeMessages ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeMessages(I)V
/* .line 1093 */
try { // :try_start_0
/* new-instance v3, Ljava/io/StringReader; */
/* invoke-direct {v3, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V */
/* move-object v0, v3 */
/* .line 1094 */
/* new-instance v3, Landroid/util/JsonReader; */
/* invoke-direct {v3, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v3 */
/* .line 1095 */
(( android.util.JsonReader ) v1 ).beginObject ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->beginObject()V
/* .line 1096 */
} // :goto_1
v3 = (( android.util.JsonReader ) v1 ).hasNext ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->hasNext()Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1097 */
(( android.util.JsonReader ) v1 ).nextName ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;
/* .line 1098 */
/* .local v3, "name":Ljava/lang/String; */
final String v4 = "heap"; // const-string v4, "heap"
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1100 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigHeap(Landroid/util/JsonReader;Ljava/lang/String;)V */
/* .line 1102 */
} // :cond_1
(( android.util.JsonReader ) v1 ).skipValue ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->skipValue()V
/* .line 1104 */
} // .end local v3 # "name":Ljava/lang/String;
} // :goto_2
/* .line 1105 */
} // :cond_2
(( android.util.JsonReader ) v1 ).endObject ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->endObject()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1113 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 1106 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1108 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_1
/* sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1109 */
final String v4 = "MemoryStandardProcessControl"; // const-string v4, "MemoryStandardProcessControl"
final String v5 = "load app heap config from cloud or database failed, invalid config"; // const-string v5, "load app heap config from cloud or database failed, invalid config"
android.util.Slog .e ( v4,v5 );
/* .line 1111 */
} // :cond_3
int v4 = 0; // const/4 v4, 0x0
/* iput-boolean v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1113 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_3
/* invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1114 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1115 */
/* nop */
/* .line 1116 */
return;
/* .line 1113 */
} // :goto_4
/* invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1114 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1115 */
/* throw v3 */
} // .end method
public void loadConfigFromCloud ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "str" # Ljava/lang/String; */
/* .line 1049 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1050 */
/* .local v0, "stringReader":Ljava/io/StringReader; */
int v1 = 0; // const/4 v1, 0x0
/* .line 1051 */
/* .local v1, "jsonReader":Landroid/util/JsonReader; */
/* sget-boolean v2, Lmiui/util/DeviceLevel;->IS_MIUI_LITE_VERSION:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
final String v2 = "lite"; // const-string v2, "lite"
} // :cond_0
final String v2 = "miui"; // const-string v2, "miui"
/* .line 1053 */
/* .local v2, "systemType":Ljava/lang/String; */
} // :goto_0
try { // :try_start_0
/* new-instance v3, Ljava/io/StringReader; */
/* invoke-direct {v3, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V */
/* move-object v0, v3 */
/* .line 1054 */
/* new-instance v3, Landroid/util/JsonReader; */
/* invoke-direct {v3, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v3 */
/* .line 1055 */
(( android.util.JsonReader ) v1 ).beginObject ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->beginObject()V
/* .line 1056 */
} // :goto_1
v3 = (( android.util.JsonReader ) v1 ).hasNext ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->hasNext()Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1057 */
(( android.util.JsonReader ) v1 ).nextName ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;
/* .line 1058 */
/* .local v3, "name":Ljava/lang/String; */
final String v4 = "aging"; // const-string v4, "aging"
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1060 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigAging(Landroid/util/JsonReader;Ljava/lang/String;)V */
/* .line 1061 */
} // :cond_1
final String v4 = "common"; // const-string v4, "common"
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1063 */
/* invoke-direct {p0, v1, v2}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigCommon(Landroid/util/JsonReader;Ljava/lang/String;)V */
/* .line 1064 */
} // :cond_2
final String v4 = "remove"; // const-string v4, "remove"
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1066 */
/* invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->parseConfigRemove(Landroid/util/JsonReader;)V */
/* .line 1068 */
} // :cond_3
(( android.util.JsonReader ) v1 ).skipValue ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->skipValue()V
/* .line 1070 */
} // .end local v3 # "name":Ljava/lang/String;
} // :goto_2
/* .line 1071 */
} // :cond_4
(( android.util.JsonReader ) v1 ).endObject ( ); // invoke-virtual {v1}, Landroid/util/JsonReader;->endObject()V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1079 */
/* :catchall_0 */
/* move-exception v3 */
/* .line 1072 */
/* :catch_0 */
/* move-exception v3 */
/* .line 1074 */
/* .local v3, "e":Ljava/io/IOException; */
try { // :try_start_1
/* sget-boolean v4, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 1075 */
final String v4 = "MemoryStandardProcessControl"; // const-string v4, "MemoryStandardProcessControl"
final String v5 = "load config from cloud or database failed, invalid config"; // const-string v5, "load config from cloud or database failed, invalid config"
android.util.Slog .e ( v4,v5 );
/* .line 1077 */
} // :cond_5
int v4 = 0; // const/4 v4, 0x0
/* iput-boolean v4, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mEnable:Z */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1079 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_3
/* invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1080 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1081 */
/* nop */
/* .line 1082 */
return;
/* .line 1079 */
} // :goto_4
/* invoke-direct {p0, v1}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1080 */
/* invoke-direct {p0, v0}, Lcom/android/server/am/MemoryStandardProcessControl;->closeIO(Ljava/io/Closeable;)V */
/* .line 1081 */
/* throw v3 */
} // .end method
public void periodicMemoryDetection ( ) {
/* .locals 20 */
/* .line 272 */
/* move-object/from16 v7, p0 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z */
if ( v0 != null) { // if-eqz v0, :cond_15
v0 = this.mMemoryStandardMap;
v0 = (( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_a */
/* .line 275 */
} // :cond_0
/* sget-boolean v0, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 276 */
final String v0 = "MemoryStandardProcessControl"; // const-string v0, "MemoryStandardProcessControl"
/* const-string/jumbo v1, "start memory detection" */
android.util.Slog .d ( v0,v1 );
/* .line 279 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemDetectionHasKilled:Z */
/* .line 280 */
v1 = this.mWarnedAppMap;
(( java.util.HashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 282 */
/* .local v2, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
v3 = this.mProcessMap;
/* .line 283 */
/* const/16 v3, 0x3e9 */
/* iput v3, v2, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
/* .line 284 */
} // .end local v2 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
/* .line 286 */
} // :cond_2
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object v8, v1 */
/* .line 287 */
/* .local v8, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 288 */
/* .local v1, "N":I */
v2 = this.mAMS;
/* monitor-enter v2 */
/* .line 289 */
try { // :try_start_0
v3 = this.mAMS;
v3 = this.mProcessList;
(( com.android.server.am.ProcessList ) v3 ).getLruProcessesLOSP ( ); // invoke-virtual {v3}, Lcom/android/server/am/ProcessList;->getLruProcessesLOSP()Ljava/util/ArrayList;
/* .line 290 */
/* .local v3, "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;" */
v4 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* move v9, v4 */
/* .line 291 */
} // .end local v1 # "N":I
/* .local v9, "N":I */
/* add-int/lit8 v1, v9, -0x1 */
/* .local v1, "i":I */
} // :goto_1
/* if-ltz v1, :cond_3 */
/* .line 292 */
try { // :try_start_1
(( java.util.ArrayList ) v3 ).get ( v1 ); // invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/am/ProcessRecord; */
(( java.util.ArrayList ) v8 ).add ( v4 ); // invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 291 */
/* add-int/lit8 v1, v1, -0x1 */
/* .line 294 */
} // .end local v1 # "i":I
} // .end local v3 # "lruProcs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_3
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 297 */
/* add-int/lit8 v1, v9, -0x1 */
/* move v10, v1 */
/* .local v10, "i":I */
} // :goto_2
/* const-wide/16 v11, 0x400 */
int v13 = 0; // const/4 v13, 0x0
/* if-ltz v10, :cond_b */
/* .line 298 */
(( java.util.ArrayList ) v8 ).get ( v10 ); // invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v14, v1 */
/* check-cast v14, Lcom/android/server/am/ProcessRecord; */
/* .line 299 */
/* .local v14, "app":Lcom/android/server/am/ProcessRecord; */
v1 = this.mState;
v1 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* if-gtz v1, :cond_4 */
/* .line 300 */
/* goto/16 :goto_4 */
/* .line 302 */
} // :cond_4
v1 = (( com.android.server.am.ProcessRecord ) v14 ).isKilledByAm ( ); // invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->isKilledByAm()Z
/* if-nez v1, :cond_a */
v1 = (( com.android.server.am.ProcessRecord ) v14 ).isKilled ( ); // invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->isKilled()Z
/* if-nez v1, :cond_a */
/* .line 303 */
(( com.android.server.am.ProcessRecord ) v14 ).getThread ( ); // invoke-virtual {v14}, Lcom/android/server/am/ProcessRecord;->getThread()Landroid/app/IApplicationThread;
if ( v1 != null) { // if-eqz v1, :cond_a
/* iget-boolean v1, v14, Lcom/android/server/am/ProcessRecord;->isolated:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 304 */
/* goto/16 :goto_4 */
/* .line 306 */
} // :cond_5
v1 = this.info;
v15 = this.packageName;
/* .line 307 */
/* .local v15, "packageName":Ljava/lang/String; */
v1 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v1 ).getStandard ( v15 ); // invoke-virtual {v1, v15}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getStandard(Ljava/lang/String;)Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
/* .line 309 */
/* .local v6, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 310 */
v5 = this.processName;
/* .line 312 */
/* .local v5, "processName":Ljava/lang/String; */
v1 = v1 = this.mProcesses;
/* if-nez v1, :cond_6 */
/* .line 313 */
/* goto/16 :goto_4 */
/* .line 315 */
} // :cond_6
v1 = this.mWarnedAppMap;
(( java.util.HashMap ) v1 ).get ( v15 ); // invoke-virtual {v1, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object/from16 v16, v1 */
/* check-cast v16, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 316 */
/* .local v16, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* if-nez v16, :cond_7 */
/* .line 317 */
/* new-instance v17, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* iget v4, v14, Lcom/android/server/am/ProcessRecord;->uid:I */
/* iget v3, v14, Lcom/android/server/am/ProcessRecord;->userId:I */
v1 = this.mState;
/* .line 318 */
v18 = (( com.android.server.am.ProcessStateRecord ) v1 ).getCurAdj ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* move-object/from16 v1, v17 */
/* move-object/from16 v2, p0 */
/* move/from16 v19, v3 */
/* move-object v3, v15 */
/* move-object v0, v5 */
} // .end local v5 # "processName":Ljava/lang/String;
/* .local v0, "processName":Ljava/lang/String; */
/* move/from16 v5, v19 */
/* move-object/from16 v19, v6 */
} // .end local v6 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
/* .local v19, "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
/* move/from16 v6, v18 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Ljava/lang/String;III)V */
/* .line 319 */
} // .end local v16 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
/* .local v1, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
v2 = this.mWarnedAppMap;
(( java.util.HashMap ) v2 ).put ( v15, v1 ); // invoke-virtual {v2, v15, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 316 */
} // .end local v0 # "processName":Ljava/lang/String;
} // .end local v1 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
} // .end local v19 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
/* .restart local v5 # "processName":Ljava/lang/String; */
/* .restart local v6 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
/* .restart local v16 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
} // :cond_7
/* move-object v0, v5 */
/* move-object/from16 v19, v6 */
} // .end local v5 # "processName":Ljava/lang/String;
} // .end local v6 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
/* .restart local v0 # "processName":Ljava/lang/String; */
/* .restart local v19 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
/* move-object/from16 v1, v16 */
/* .line 321 */
} // .end local v16 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
/* .restart local v1 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
} // :goto_3
/* iget v2, v14, Lcom/android/server/am/ProcessRecord;->mPid:I */
android.os.Debug .getPss ( v2,v13,v13 );
/* move-result-wide v2 */
/* div-long/2addr v2, v11 */
/* .line 322 */
/* .local v2, "appPss":J */
v4 = this.mProcessMap;
java.lang.Long .valueOf ( v2,v3 );
/* .line 324 */
v4 = this.mState;
v4 = (( com.android.server.am.ProcessStateRecord ) v4 ).getCurAdj ( ); // invoke-virtual {v4}, Lcom/android/server/am/ProcessStateRecord;->getCurAdj()I
/* .line 325 */
/* .local v4, "newAdj":I */
/* iget v5, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
/* if-le v5, v4, :cond_8 */
/* .line 326 */
/* iput v4, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
/* .line 328 */
} // :cond_8
/* sget-boolean v5, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 329 */
final String v5 = "MemoryStandardProcessControl"; // const-string v5, "MemoryStandardProcessControl"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = "package = "; // const-string v11, "package = "
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v15 ); // invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", process = "; // const-string v11, ", process = "
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = ", adj = "; // const-string v11, ", adj = "
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v11, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
(( java.lang.StringBuilder ) v6 ).append ( v11 ); // invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 309 */
} // .end local v0 # "processName":Ljava/lang/String;
} // .end local v1 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
} // .end local v2 # "appPss":J
} // .end local v4 # "newAdj":I
} // .end local v19 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
/* .restart local v6 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard; */
} // :cond_9
/* move-object/from16 v19, v6 */
/* .line 297 */
} // .end local v6 # "standard":Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandard;
} // .end local v14 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v15 # "packageName":Ljava/lang/String;
} // :cond_a
} // :goto_4
/* add-int/lit8 v10, v10, -0x1 */
int v0 = 0; // const/4 v0, 0x0
/* goto/16 :goto_2 */
/* .line 336 */
} // .end local v10 # "i":I
} // :cond_b
v0 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v0 ).getNativeProcessCmds ( ); // invoke-virtual {v0}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getNativeProcessCmds()Ljava/util/Set;
/* .line 337 */
/* .local v0, "nativeProcIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v1 = } // :goto_5
if ( v1 != null) { // if-eqz v1, :cond_e
/* .line 338 */
/* move-object v10, v1 */
/* check-cast v10, Ljava/lang/String; */
/* .line 339 */
/* .local v10, "cmd":Ljava/lang/String; */
/* filled-new-array {v10}, [Ljava/lang/String; */
android.os.Process .getPidsForCommands ( v1 );
/* .line 340 */
/* .local v14, "pids":[I */
if ( v14 != null) { // if-eqz v14, :cond_d
/* array-length v1, v14 */
/* if-lez v1, :cond_d */
/* .line 342 */
int v1 = 0; // const/4 v1, 0x0
/* aget v15, v14, v1 */
/* .line 344 */
/* .local v15, "pid":I */
android.os.Debug .getPss ( v15,v13,v13 );
/* move-result-wide v1 */
/* div-long v16, v1, v11 */
/* .line 345 */
/* .local v16, "appPss":J */
v1 = this.mWarnedAppMap;
(( java.util.HashMap ) v1 ).get ( v10 ); // invoke-virtual {v1, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object/from16 v18, v1 */
/* check-cast v18, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 346 */
/* .local v18, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* if-nez v18, :cond_c */
/* .line 347 */
/* new-instance v19, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* const/16 v4, 0x3e8 */
int v5 = 0; // const/4 v5, 0x0
/* const/16 v6, -0x3e8 */
/* move-object/from16 v1, v19 */
/* move-object/from16 v2, p0 */
/* move-object v3, v10 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;-><init>(Lcom/android/server/am/MemoryStandardProcessControl;Ljava/lang/String;III)V */
/* .line 348 */
} // .end local v18 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
/* .restart local v1 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
v2 = this.mWarnedAppMap;
(( java.util.HashMap ) v2 ).put ( v10, v1 ); // invoke-virtual {v2, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 346 */
} // .end local v1 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
/* .restart local v18 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
} // :cond_c
/* move-object/from16 v1, v18 */
/* .line 350 */
} // .end local v18 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
/* .restart local v1 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
} // :goto_6
/* const/16 v2, -0x3e8 */
/* iput v2, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
/* .line 351 */
v2 = this.mProcessMap;
/* invoke-static/range {v16 ..v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long; */
/* .line 352 */
/* sget-boolean v2, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_d
/* .line 353 */
final String v2 = "MemoryStandardProcessControl"; // const-string v2, "MemoryStandardProcessControl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "package = "; // const-string v4, "package = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", process = "; // const-string v4, ", process = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v10 ); // invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", adj = "; // const-string v4, ", adj = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, v1, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mAdj:I */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 357 */
} // .end local v1 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
} // .end local v10 # "cmd":Ljava/lang/String;
} // .end local v14 # "pids":[I
} // .end local v15 # "pid":I
} // .end local v16 # "appPss":J
} // :cond_d
/* goto/16 :goto_5 */
/* .line 360 */
} // :cond_e
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
/* .line 361 */
/* .local v1, "now":J */
v3 = this.mWarnedAppMap;
(( java.util.HashMap ) v3 ).values ( ); // invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v4 = } // :goto_7
if ( v4 != null) { // if-eqz v4, :cond_13
/* check-cast v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
/* .line 362 */
/* .local v4, "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo; */
v5 = this.mPackageName;
/* .line 363 */
/* .local v5, "packageName":Ljava/lang/String; */
/* iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I */
int v10 = 3; // const/4 v10, 0x3
/* if-le v6, v10, :cond_f */
/* sget-boolean v6, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
/* if-nez v6, :cond_f */
/* .line 365 */
final String v6 = "MemoryStandardProcessControl"; // const-string v6, "MemoryStandardProcessControl"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v5 ); // invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " frequently killed, skip"; // const-string v11, " frequently killed, skip"
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v10 );
/* .line 366 */
/* .line 368 */
} // :cond_f
v6 = (( com.android.server.am.MemoryStandardProcessControl$AppInfo ) v4 ).getTotalPss ( ); // invoke-virtual {v4}, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->getTotalPss()I
/* int-to-long v11, v6 */
/* .line 369 */
/* .local v11, "totalPss":J */
v6 = this.mMemoryStandardMap;
(( com.android.server.am.MemoryStandardProcessControl$MemoryStandardMap ) v6 ).getPss ( v5 ); // invoke-virtual {v6, v5}, Lcom/android/server/am/MemoryStandardProcessControl$MemoryStandardMap;->getPss(Ljava/lang/String;)J
/* move-result-wide v13 */
/* .line 370 */
/* .local v13, "standardPss":J */
/* cmp-long v6, v11, v13 */
int v15 = 1; // const/4 v15, 0x1
/* if-lez v6, :cond_11 */
/* .line 371 */
/* iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* add-int/2addr v6, v15 */
/* iput v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* .line 372 */
/* iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* if-lt v6, v10, :cond_10 */
/* .line 373 */
/* iput-boolean v15, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mTriggerKill:Z */
/* .line 376 */
/* iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I */
/* add-int/2addr v6, v15 */
/* iput v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mKillTimes:I */
/* .line 377 */
/* iput-wide v1, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mLastKillTime:J */
/* .line 379 */
} // :cond_10
final String v6 = "MemoryStandardProcessControl"; // const-string v6, "MemoryStandardProcessControl"
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v10 ).append ( v5 ); // invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v15 = " is out of standard, pss is "; // const-string v15, " is out of standard, pss is "
(( java.lang.StringBuilder ) v10 ).append ( v15 ); // invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v11, v12 ); // invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v6,v10 );
/* .line 380 */
} // :cond_11
/* iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* if-lez v6, :cond_12 */
/* .line 382 */
/* iget v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* sub-int/2addr v6, v15 */
/* iput v6, v4, Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;->mMemExcelTime:I */
/* .line 384 */
} // .end local v4 # "info":Lcom/android/server/am/MemoryStandardProcessControl$AppInfo;
} // .end local v5 # "packageName":Ljava/lang/String;
} // .end local v11 # "totalPss":J
} // .end local v13 # "standardPss":J
} // :cond_12
} // :goto_8
/* .line 386 */
} // :cond_13
/* iget-boolean v3, v7, Lcom/android/server/am/MemoryStandardProcessControl;->mTriggerKill:Z */
if ( v3 != null) { // if-eqz v3, :cond_14
/* .line 387 */
int v3 = 0; // const/4 v3, 0x0
(( com.android.server.am.MemoryStandardProcessControl ) v7 ).killProcess ( v3 ); // invoke-virtual {v7, v3}, Lcom/android/server/am/MemoryStandardProcessControl;->killProcess(Z)V
/* .line 389 */
} // :cond_14
return;
/* .line 294 */
} // .end local v0 # "nativeProcIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
} // .end local v1 # "now":J
/* :catchall_0 */
/* move-exception v0 */
/* move v1, v9 */
} // .end local v9 # "N":I
/* .local v1, "N":I */
/* :catchall_1 */
/* move-exception v0 */
} // :goto_9
try { // :try_start_2
/* monitor-exit v2 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* throw v0 */
/* .line 273 */
} // .end local v1 # "N":I
} // .end local v8 # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
} // :cond_15
} // :goto_a
return;
} // .end method
public void reportAppResumed ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 758 */
v0 = (( com.android.server.am.MemoryStandardProcessControl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
/* if-nez v0, :cond_0 */
/* .line 761 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
v0 = this.mAppHeapStandard;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 762 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v0 ).removeEqualMessages ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V
/* .line 764 */
} // :cond_1
return;
/* .line 759 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void reportAppStopped ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 746 */
v0 = (( com.android.server.am.MemoryStandardProcessControl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapEnable:Z */
/* if-nez v0, :cond_0 */
/* .line 749 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_2
v0 = this.mAppHeapStandard;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 750 */
v0 = this.mHandler;
int v1 = 7; // const/4 v1, 0x7
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v0 ).removeEqualMessages ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->removeEqualMessages(ILjava/lang/Object;)V
/* .line 751 */
v0 = this.mHandler;
int v2 = 0; // const/4 v2, 0x0
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v0 ).obtainMessage ( v1, p1, v2, p2 ); // invoke-virtual {v0, v1, p1, v2, p2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;
/* .line 752 */
/* .local v0, "msg":Landroid/os/Message; */
v1 = this.mHandler;
/* sget-boolean v2, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* const-wide/16 v2, 0x1388 */
} // :cond_1
/* iget-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mAppHeapHandleTime:J */
} // :goto_0
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v1 ).sendMessageDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 754 */
} // .end local v0 # "msg":Landroid/os/Message;
} // :cond_2
return;
/* .line 747 */
} // :cond_3
} // :goto_1
return;
} // .end method
public void reportMemPressure ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "pressureState" # I */
/* .line 730 */
v0 = (( com.android.server.am.MemoryStandardProcessControl ) p0 ).isEnable ( ); // invoke-virtual {p0}, Lcom/android/server/am/MemoryStandardProcessControl;->isEnable()Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mScreenState:I */
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_0 */
/* .line 733 */
} // :cond_0
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 735 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemPressureTime:J */
/* sub-long v2, v0, v2 */
/* const-wide/32 v4, 0x1d4c0 */
/* cmp-long v2, v2, v4 */
/* if-lez v2, :cond_1 */
int v2 = 3; // const/4 v2, 0x3
/* if-ge p1, v2, :cond_2 */
} // :cond_1
/* sget-boolean v2, Lcom/android/server/am/MemoryStandardProcessControl;->DEBUG:Z */
/* if-nez v2, :cond_2 */
/* .line 737 */
return;
/* .line 739 */
} // :cond_2
/* iput-wide v0, p0, Lcom/android/server/am/MemoryStandardProcessControl;->mLastMemPressureTime:J */
/* .line 740 */
v2 = this.mHandler;
int v3 = 4; // const/4 v3, 0x4
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v2 ).obtainMessage ( v3, p1, v4 ); // invoke-virtual {v2, v3, p1, v4}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->obtainMessage(III)Landroid/os/Message;
/* .line 741 */
/* .local v2, "msg":Landroid/os/Message; */
v3 = this.mHandler;
(( com.android.server.am.MemoryStandardProcessControl$MyHandler ) v3 ).sendMessageAtFrontOfQueue ( v2 ); // invoke-virtual {v3, v2}, Lcom/android/server/am/MemoryStandardProcessControl$MyHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z
/* .line 742 */
return;
/* .line 731 */
} // .end local v0 # "now":J
} // .end local v2 # "msg":Landroid/os/Message;
} // :cond_3
} // :goto_0
return;
} // .end method
