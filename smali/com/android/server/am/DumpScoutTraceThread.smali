.class public Lcom/android/server/am/DumpScoutTraceThread;
.super Ljava/lang/Thread;
.source "DumpScoutTraceThread.java"


# static fields
.field private static final APP_LOG_DIR:Ljava/lang/String; = "/data/miuilog/stability/scout/app"

.field private static NUMBER_OF_CORES:I = 0x0

.field public static final SOCKET_BUFFER_SIZE:I = 0x100

.field public static final SOCKET_NAME:Ljava/lang/String; = "scouttrace"

.field public static final TAG:Ljava/lang/String; = "DumpScoutTraceThread"


# instance fields
.field private mAMSImpl:Lcom/android/server/am/ActivityManagerServiceImpl;


# direct methods
.method public static synthetic $r8$lambda$P9I22cukx4b4Fbe-h2DFudvv5fY(Lcom/android/server/am/DumpScoutTraceThread;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[I)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/server/am/DumpScoutTraceThread;->lambda$run$0(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 30
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lcom/android/server/am/DumpScoutTraceThread;->NUMBER_OF_CORES:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/android/server/am/ActivityManagerServiceImpl;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "AMSImpl"    # Lcom/android/server/am/ActivityManagerServiceImpl;

    .line 36
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 37
    iput-object p2, p0, Lcom/android/server/am/DumpScoutTraceThread;->mAMSImpl:Lcom/android/server/am/ActivityManagerServiceImpl;

    .line 38
    return-void
.end method

.method private synthetic lambda$run$0(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[I)V
    .locals 23
    .param p1, "isSystemBlock"    # Z
    .param p2, "fileNamePerfix"    # Ljava/lang/String;
    .param p3, "timestamp"    # Ljava/lang/String;
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "pid"    # I
    .param p6, "finalPids"    # [I

    .line 88
    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    const-string v0, "/"

    const-string v7, "DumpScoutTraceThread"

    if-eqz p1, :cond_0

    .line 90
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-system_server-trace"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 91
    .local v8, "systemFileName":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 92
    .local v9, "systemFilePath":Ljava/lang/String;
    iget-object v10, v1, Lcom/android/server/am/DumpScoutTraceThread;->mAMSImpl:Lcom/android/server/am/ActivityManagerServiceImpl;

    invoke-virtual {v10, v9}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpSystemTraces(Ljava/lang/String;)V

    .line 93
    .end local v8    # "systemFileName":Ljava/lang/String;
    .end local v9    # "systemFilePath":Ljava/lang/String;
    goto :goto_0

    .line 94
    :cond_0
    const-string v8, "Don\'t need to dump system_server trace."

    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :goto_0
    invoke-static {v7, v5}, Lcom/android/server/ScoutHelper;->CheckDState(Ljava/lang/String;I)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 98
    .local v8, "result":Z
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "-self-trace"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 99
    .local v9, "selfFileName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 100
    .local v10, "selfFilePath":Ljava/lang/String;
    iget-object v11, v1, Lcom/android/server/am/DumpScoutTraceThread;->mAMSImpl:Lcom/android/server/am/ActivityManagerServiceImpl;

    const-string v12, "App Scout Exception"

    invoke-virtual {v11, v5, v10, v12}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpOneProcessTraces(ILjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v11

    .line 103
    .local v11, "selfTraceFile":Ljava/io/File;
    if-eqz v11, :cond_1

    .line 104
    const-string v12, "Dump scout self trace file successfully!"

    invoke-static {v7, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 106
    :cond_1
    const-string v12, "Dump scout self trace file fail!"

    invoke-static {v7, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :goto_1
    invoke-virtual {v1, v4, v2, v3}, Lcom/android/server/am/DumpScoutTraceThread;->dumpLockedMethodLongEvents(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    if-eqz v6, :cond_8

    array-length v13, v6

    if-lez v13, :cond_8

    .line 113
    array-length v13, v6

    const/4 v14, 0x0

    :goto_2
    if-ge v14, v13, :cond_2

    aget v15, v6, v14

    .line 114
    .local v15, "temppid":I
    invoke-static {v7, v15}, Lcom/android/server/ScoutHelper;->CheckDState(Ljava/lang/String;I)Ljava/lang/Boolean;

    .line 113
    .end local v15    # "temppid":I
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 118
    :cond_2
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "-other-trace"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 119
    .local v13, "fileName":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 121
    .local v14, "filePath":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    const/4 v12, 0x3

    invoke-direct {v15, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 122
    .local v15, "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 123
    .local v5, "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_3
    move/from16 v21, v8

    .end local v8    # "result":Z
    .local v21, "result":Z
    array-length v8, v6

    if-ge v12, v8, :cond_6

    .line 124
    aget v8, v6, v12

    invoke-static {v7, v8}, Lcom/android/server/ScoutHelper;->getOomAdjOfPid(Ljava/lang/String;I)I

    move-result v8

    .line 125
    .local v8, "adj":I
    move-object/from16 v22, v9

    .end local v9    # "selfFileName":Ljava/lang/String;
    .local v22, "selfFileName":Ljava/lang/String;
    invoke-static {v8}, Lcom/android/server/ScoutHelper;->checkIsJavaOrNativeProcess(I)I

    move-result v9

    .line 126
    .local v9, "isJavaOrNativeProcess":I
    if-nez v9, :cond_3

    .line 127
    goto :goto_4

    .line 128
    :cond_3
    move/from16 v16, v8

    .end local v8    # "adj":I
    .local v16, "adj":I
    const/4 v8, 0x1

    if-ne v9, v8, :cond_4

    .line 129
    aget v8, v6, v12

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 130
    :cond_4
    const/4 v8, 0x2

    if-ne v9, v8, :cond_5

    .line 131
    aget v8, v6, v12

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    .end local v9    # "isJavaOrNativeProcess":I
    .end local v16    # "adj":I
    :cond_5
    :goto_4
    add-int/lit8 v12, v12, 0x1

    move/from16 v8, v21

    move-object/from16 v9, v22

    goto :goto_3

    .end local v22    # "selfFileName":Ljava/lang/String;
    .local v9, "selfFileName":Ljava/lang/String;
    :cond_6
    move-object/from16 v22, v9

    .line 135
    .end local v9    # "selfFileName":Ljava/lang/String;
    .end local v12    # "i":I
    .restart local v22    # "selfFileName":Ljava/lang/String;
    iget-object v8, v1, Lcom/android/server/am/DumpScoutTraceThread;->mAMSImpl:Lcom/android/server/am/ActivityManagerServiceImpl;

    const/16 v17, 0x0

    const-string v19, "App Scout Exception"

    move-object v9, v15

    .end local v15    # "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local v9, "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object v15, v8

    move-object/from16 v16, v9

    move-object/from16 v18, v5

    move-object/from16 v20, v14

    invoke-virtual/range {v15 .. v20}, Lcom/android/server/am/ActivityManagerServiceImpl;->dumpAppStackTraces(Ljava/util/ArrayList;Landroid/util/SparseArray;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    .line 137
    .local v8, "appTraceFile":Ljava/io/File;
    if-eqz v8, :cond_7

    .line 138
    const-string v12, "Dump scout other trace file successfully !"

    invoke-static {v7, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 140
    :cond_7
    const-string v12, "Dump scout other trace file fail !"

    invoke-static {v7, v12}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    .end local v5    # "nativePids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v8    # "appTraceFile":Ljava/io/File;
    .end local v9    # "javapids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v13    # "fileName":Ljava/lang/String;
    .end local v14    # "filePath":Ljava/lang/String;
    :goto_5
    goto :goto_6

    .line 111
    .end local v21    # "result":Z
    .end local v22    # "selfFileName":Ljava/lang/String;
    .local v8, "result":Z
    .local v9, "selfFileName":Ljava/lang/String;
    :cond_8
    move/from16 v21, v8

    move-object/from16 v22, v9

    .line 143
    .end local v8    # "result":Z
    .end local v9    # "selfFileName":Ljava/lang/String;
    .restart local v21    # "result":Z
    .restart local v22    # "selfFileName":Ljava/lang/String;
    const-string v5, "dumpAPPStackTraces: pids is null"

    invoke-static {v7, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :goto_6
    invoke-static {}, Lmiui/mqsas/scout/ScoutUtils;->isLibraryTest()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 148
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "_walkstack"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 149
    .local v5, "walkstackFileName":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    const-string v9, "/data/miuilog/stability/scout/app"

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v8, "appScoutDir":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 151
    new-instance v9, Lcom/android/server/am/DumpScoutTraceThread$1;

    invoke-direct {v9, v1, v5}, Lcom/android/server/am/DumpScoutTraceThread$1;-><init>(Lcom/android/server/am/DumpScoutTraceThread;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v9

    .line 161
    .local v9, "pathArray":[Ljava/io/File;
    if-eqz v9, :cond_b

    array-length v12, v9

    if-lez v12, :cond_b

    .line 162
    new-instance v12, Ljava/io/File;

    const/4 v13, 0x0

    aget-object v13, v9, v13

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 163
    .local v12, "walkstackFileOld":Ljava/io/File;
    new-instance v13, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v14, "-miui-self-trace"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 165
    .local v13, "walkstackFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v13}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 166
    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const/16 v14, 0x180

    const/4 v15, -0x1

    invoke-static {v0, v14, v15, v15}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 167
    invoke-static {v12, v13}, Landroid/os/FileUtils;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Success copying walkstack trace to path"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    goto :goto_7

    .line 171
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Fail to copy walkstack trace to path"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :cond_a
    :goto_7
    goto :goto_8

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Ljava/io/IOException;
    const-string v14, "Exception occurs while copying walkstack trace file:"

    invoke-static {v7, v14, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 181
    .end local v0    # "e":Ljava/io/IOException;
    .end local v5    # "walkstackFileName":Ljava/lang/String;
    .end local v8    # "appScoutDir":Ljava/io/File;
    .end local v9    # "pathArray":[Ljava/io/File;
    .end local v12    # "walkstackFileOld":Ljava/io/File;
    .end local v13    # "walkstackFile":Ljava/io/File;
    :cond_b
    :goto_8
    return-void
.end method


# virtual methods
.method dumpLockedMethodLongEvents(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "fileNamePerfix"    # Ljava/lang/String;
    .param p3, "timestamp"    # Ljava/lang/String;

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-critical-section-trace"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "fileName":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "filePath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 204
    .local v2, "traceFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 205
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x180

    const/4 v5, -0x1

    invoke-static {v3, v4, v5, v5}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 207
    :cond_0
    invoke-static {}, Lcom/android/server/LockPerfStub;->getInstance()Lcom/android/server/LockPerfStub;

    move-result-object v3

    const/16 v4, 0x3c

    const/16 v5, 0x14

    const/4 v6, 0x0

    invoke-interface {v3, v2, v6, v4, v5}, Lcom/android/server/LockPerfStub;->dumpRecentEvents(Ljava/io/File;ZII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    goto :goto_0

    .line 208
    :catch_0
    move-exception v3

    .line 209
    .local v3, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception creating scout file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "DumpScoutTraceThread"

    invoke-static {v5, v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 211
    .end local v3    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method public run()V
    .locals 21

    .line 42
    const-string v1, "DumpScoutTraceThread finally shutdown"

    const-string v2, "DumpScoutTraceThread"

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v5, Lcom/android/server/am/DumpScoutTraceThread;->NUMBER_OF_CORES:I

    const-wide/16 v6, 0x1

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v9, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v9}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move-object v3, v0

    move v4, v5

    invoke-direct/range {v3 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 48
    .local v3, "threadPool":Ljava/util/concurrent/ThreadPoolExecutor;
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 50
    const/4 v4, 0x0

    .line 53
    .local v4, "socketServer":Landroid/net/LocalServerSocket;
    :try_start_0
    new-instance v0, Landroid/net/LocalServerSocket;

    const-string v5, "scouttrace"

    invoke-direct {v0, v5}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    move-object v4, v0

    .line 54
    const-string v0, "Already create local socket"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_0
    :goto_0
    invoke-virtual {v4}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v0

    .line 59
    .local v0, "socketClient":Landroid/net/LocalSocket;
    const-string/jumbo v5, "waiting mqs client socket to connect..."

    invoke-static {v2, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-virtual {v0}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 62
    .local v5, "inputStream":Ljava/io/InputStream;
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-direct {v7, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 63
    .local v6, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .line 64
    .local v7, "scoutInfo":Ljava/lang/String;
    const/4 v8, 0x0

    .line 65
    .local v8, "jsonObject":Lorg/json/JSONObject;
    if-eqz v7, :cond_0

    .line 66
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object v8, v9

    .line 70
    const-string v9, "pid"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 71
    .local v9, "pid":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "get data from socket "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const-string v10, "pidlist"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object v11, v10

    .line 73
    .local v11, "pidlist":Ljava/lang/String;
    const/4 v10, 0x0

    .line 74
    .local v10, "pids":[I
    const-string v12, "null"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 75
    const-string v12, "#"

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 76
    .local v12, "pidArray":[Ljava/lang/String;
    array-length v13, v12

    new-array v13, v13, [I

    move-object v10, v13

    .line 77
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    array-length v14, v12

    if-ge v13, v14, :cond_1

    .line 78
    aget-object v14, v12, v13

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    aput v14, v10, v13

    .line 77
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v18, v10

    goto :goto_2

    .line 74
    .end local v12    # "pidArray":[Ljava/lang/String;
    .end local v13    # "i":I
    :cond_2
    move-object/from16 v18, v10

    .line 81
    .end local v10    # "pids":[I
    .local v18, "pids":[I
    :goto_2
    const-string v10, "path"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 82
    .local v15, "path":Ljava/lang/String;
    const-string/jumbo v10, "timestamp"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 83
    .local v14, "timestamp":Ljava/lang/String;
    const-string v10, "fileNamePerfix"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 84
    .local v13, "fileNamePerfix":Ljava/lang/String;
    const-string v10, "isSystemBlock"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 86
    .local v12, "isSystemBlock":Z
    move-object/from16 v17, v18

    .line 87
    .local v17, "finalPids":[I
    new-instance v10, Lcom/android/server/am/DumpScoutTraceThread$$ExternalSyntheticLambda0;

    move-object/from16 v19, v10

    move-object/from16 v20, v11

    .end local v11    # "pidlist":Ljava/lang/String;
    .local v20, "pidlist":Ljava/lang/String;
    move-object/from16 v11, p0

    move/from16 v16, v9

    invoke-direct/range {v10 .. v17}, Lcom/android/server/am/DumpScoutTraceThread$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/am/DumpScoutTraceThread;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[I)V

    move-object/from16 v10, v19

    invoke-virtual {v3, v10}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    .end local v0    # "socketClient":Landroid/net/LocalSocket;
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .end local v7    # "scoutInfo":Ljava/lang/String;
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .end local v9    # "pid":I
    .end local v12    # "isSystemBlock":Z
    .end local v13    # "fileNamePerfix":Ljava/lang/String;
    .end local v14    # "timestamp":Ljava/lang/String;
    .end local v15    # "path":Ljava/lang/String;
    .end local v17    # "finalPids":[I
    .end local v18    # "pids":[I
    .end local v20    # "pidlist":Ljava/lang/String;
    goto/16 :goto_0

    .line 186
    :catchall_0
    move-exception v0

    move-object v5, v0

    goto :goto_5

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "Exception creating scout dump scout trace file:"

    invoke-static {v2, v5, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    nop

    .end local v0    # "e":Ljava/lang/Exception;
    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-virtual {v3}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 188
    if-eqz v4, :cond_3

    .line 190
    :try_start_2
    invoke-virtual {v4}, Landroid/net/LocalServerSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 193
    :goto_3
    goto :goto_4

    .line 191
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v1

    .line 192
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .end local v0    # "e":Ljava/io/IOException;
    goto :goto_3

    .line 197
    :cond_3
    :goto_4
    return-void

    .line 186
    :goto_5
    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-virtual {v3}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 188
    if-eqz v4, :cond_4

    .line 190
    :try_start_3
    invoke-virtual {v4}, Landroid/net/LocalServerSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 193
    goto :goto_6

    .line 191
    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v1

    .line 192
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 195
    .end local v0    # "e":Ljava/io/IOException;
    :cond_4
    :goto_6
    throw v5
.end method
