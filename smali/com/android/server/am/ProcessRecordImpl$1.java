class com.android.server.am.ProcessRecordImpl$1 implements java.lang.Runnable {
	 /* .source "ProcessRecordImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/am/ProcessRecordImpl;->reportAppPss()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.am.ProcessRecordImpl this$0; //synthetic
final java.util.Map val$map; //synthetic
/* # direct methods */
 com.android.server.am.ProcessRecordImpl$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/am/ProcessRecordImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 143 */
this.this$0 = p1;
this.val$map = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 8 */
/* .line 146 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 147 */
/* .local v0, "jsons":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = this.val$map;
/* .line 148 */
/* .local v1, "pkns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/String; */
/* .line 149 */
/* .local v3, "pkn":Ljava/lang/String; */
v4 = this.val$map;
/* check-cast v4, Lcom/android/server/am/ProcessRecordImpl$AppPss; */
/* .line 150 */
/* .local v4, "appPss":Lcom/android/server/am/ProcessRecordImpl$AppPss; */
/* if-nez v4, :cond_0 */
/* .line 151 */
/* .line 153 */
} // :cond_0
/* new-instance v5, Lorg/json/JSONObject; */
/* invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V */
/* .line 155 */
/* .local v5, "object":Lorg/json/JSONObject; */
try { // :try_start_0
final String v6 = "packageName"; // const-string v6, "packageName"
v7 = this.pkn;
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 156 */
/* const-string/jumbo v6, "totalPss" */
v7 = this.pss;
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 157 */
/* const-string/jumbo v6, "versionName" */
v7 = this.version;
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 158 */
final String v6 = "model"; // const-string v6, "model"
v7 = android.os.Build.MODEL;
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 159 */
/* const-string/jumbo v6, "userId" */
v7 = this.user;
(( org.json.JSONObject ) v5 ).put ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 160 */
(( org.json.JSONObject ) v5 ).toString ( ); // invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 163 */
/* .line 161 */
/* :catch_0 */
/* move-exception v6 */
/* .line 162 */
/* .local v6, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v6 ).printStackTrace ( ); // invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V
/* .line 164 */
} // .end local v3 # "pkn":Ljava/lang/String;
} // .end local v4 # "appPss":Lcom/android/server/am/ProcessRecordImpl$AppPss;
} // .end local v5 # "object":Lorg/json/JSONObject;
} // .end local v6 # "e":Lorg/json/JSONException;
} // :goto_1
/* .line 165 */
v2 = } // :cond_1
/* if-lez v2, :cond_2 */
/* .line 166 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
final String v3 = "mqs_whiteapp_lowmem_monitor_63691000"; // const-string v3, "mqs_whiteapp_lowmem_monitor_63691000"
int v4 = 0; // const/4 v4, 0x0
final String v5 = "appPss"; // const-string v5, "appPss"
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v2 ).reportEventsV2 ( v5, v0, v3, v4 ); // invoke-virtual {v2, v5, v0, v3, v4}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportEventsV2(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V
/* .line 169 */
} // :cond_2
return;
} // .end method
