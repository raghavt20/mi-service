class com.android.server.am.ProcessPowerCleaner$H extends android.os.Handler {
	 /* .source "ProcessPowerCleaner.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/am/ProcessPowerCleaner; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
public static final Integer AUTO_LOCK_OFF_EVENT;
public static final Integer KILL_ALL_EVENT;
public static final Integer SCREEN_OFF_EVENT;
public static final Integer SCREEN_OFF_FROZEN_EVENT;
public static final Integer SCREEN_ON_EVENT;
public static final Integer THERMAL_KILL_ALL_EVENT;
/* # instance fields */
final com.android.server.am.ProcessPowerCleaner this$0; //synthetic
/* # direct methods */
public com.android.server.am.ProcessPowerCleaner$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 99 */
this.this$0 = p1;
/* .line 100 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 101 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 105 */
/* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
/* .line 106 */
v0 = this.obj;
/* check-cast v0, Lmiui/process/ProcessConfig; */
/* .line 107 */
/* .local v0, "config":Lmiui/process/ProcessConfig; */
/* iget v1, p1, Landroid/os/Message;->what:I */
/* const-wide/32 v2, 0x80000 */
/* sparse-switch v1, :sswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 132 */
/* :sswitch_0 */
v1 = this.this$0;
com.android.server.am.ProcessPowerCleaner .-$$Nest$mhandleScreenOffEvent ( v1 );
/* .line 133 */
/* goto/16 :goto_1 */
/* .line 129 */
/* :sswitch_1 */
v1 = this.this$0;
com.android.server.am.ProcessPowerCleaner .-$$Nest$mresetLockOffConfig ( v1 );
/* .line 130 */
/* goto/16 :goto_1 */
/* .line 135 */
/* :sswitch_2 */
v1 = this.this$0;
com.android.server.am.ProcessPowerCleaner .-$$Nest$mpowerFrozenAll ( v1 );
/* .line 136 */
/* goto/16 :goto_1 */
/* .line 123 */
/* :sswitch_3 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "PowerScreenOffKill:"; // const-string v4, "PowerScreenOffKill:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
/* .line 124 */
/* const/16 v5, 0x16 */
(( com.android.server.am.ProcessPowerCleaner ) v4 ).getKillReason ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 123 */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 125 */
v1 = this.this$0;
(( com.android.server.am.ProcessPowerCleaner ) v1 ).handleAutoLockOff ( ); // invoke-virtual {v1}, Lcom/android/server/am/ProcessPowerCleaner;->handleAutoLockOff()V
/* .line 126 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 127 */
/* .line 115 */
/* :sswitch_4 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "PowerKillAll:"; // const-string v4, "PowerKillAll:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
/* .line 116 */
v5 = (( miui.process.ProcessConfig ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I
(( com.android.server.am.ProcessPowerCleaner ) v4 ).getKillReason ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 115 */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 117 */
/* nop */
/* .line 118 */
v1 = (( miui.process.ProcessConfig ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I
/* const/16 v4, 0x14 */
/* if-eq v1, v4, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 119 */
/* .local v1, "isKillSystemProc":Z */
} // :goto_0
v4 = this.this$0;
com.android.server.am.ProcessPowerCleaner .-$$Nest$mhandleKillAll ( v4,v0,v1 );
/* .line 120 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 121 */
/* .line 109 */
} // .end local v1 # "isKillSystemProc":Z
/* :sswitch_5 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "PowerThermalKillProc:"; // const-string v4, "PowerThermalKillProc:"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.this$0;
/* .line 110 */
v5 = (( miui.process.ProcessConfig ) v0 ).getPolicy ( ); // invoke-virtual {v0}, Lmiui/process/ProcessConfig;->getPolicy()I
(( com.android.server.am.ProcessPowerCleaner ) v4 ).getKillReason ( v5 ); // invoke-virtual {v4, v5}, Lcom/android/server/am/ProcessPowerCleaner;->getKillReason(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 109 */
android.os.Trace .traceBegin ( v2,v3,v1 );
/* .line 111 */
v1 = this.this$0;
com.android.server.am.ProcessPowerCleaner .-$$Nest$mhandleThermalKillProc ( v1,v0 );
/* .line 112 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 113 */
/* nop */
/* .line 140 */
} // :goto_1
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_5 */
/* 0x2 -> :sswitch_4 */
/* 0x4 -> :sswitch_3 */
/* 0x5 -> :sswitch_2 */
/* 0xf -> :sswitch_1 */
/* 0x10 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
